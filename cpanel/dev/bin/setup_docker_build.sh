SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

## Build Mysql Storage
echo "Build Mysql Storage"
sudo docker run -d --name=novogenia-ordermanager-dev-mysql-storage novogenia-ordermanager-dev/mysql true

## Build Mysql
echo "Build Mysql"
sudo docker run -d --name=novogenia-ordermanager-dev-mysql -p $APP_MYSQL_PORT:3306 \
    --volumes-from=novogenia-ordermanager-dev-mysql-storage \
    -v $APP_HOME/sources/www/:/var/www/ \
    novogenia-ordermanager-dev/mysql

## Build Nginx+PHP
echo "Build Web"
sudo docker run -d --name=novogenia-ordermanager-dev-web \
  -v $APP_HOME/sources/www/:/var/www/ \
  --link novogenia-ordermanager-dev-mysql:db \
  -p $APP_WEB_OM_PORT:80 \
  -p $APP_WEB_OM_CD_PORT:81 \
  novogenia-ordermanager-dev/web

## Build Report generator
echo "Build Report Generator"
sudo docker run -d --name=novogenia-ordermanager-dev-report-generator \
  -v $APP_HOME/sources/www/:/var/www/ \
  --link novogenia-ordermanager-dev-mysql:db \
  novogenia-ordermanager-dev/report-generator
