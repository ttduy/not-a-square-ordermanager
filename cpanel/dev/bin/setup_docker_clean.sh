SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

sudo docker stop novogenia-ordermanager-dev-mysql
sudo docker rm novogenia-ordermanager-dev-mysql
sudo docker stop novogenia-ordermanager-dev-web
sudo docker rm novogenia-ordermanager-dev-web
sudo docker stop novogenia-ordermanager-dev-report-generator
sudo docker rm novogenia-ordermanager-dev-report-generator

