SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

## Build Mysql
echo "Build Mysql"
cd $APP_HOME/cpanel/dev/docker/mysql
. ./build.sh

## Build Web
echo "Build Web"
cd $APP_HOME/cpanel/dev/docker/web
. ./build.sh

## Build Report Generator
echo "Build Report Generator"
cd $APP_HOME/cpanel/dev/docker/reportGenerator
. ./build.sh
