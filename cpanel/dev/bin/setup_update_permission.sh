SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

## UPDATE Web component permission
cd $APP_HOME/sources/www/ordermanager/app
mkdir cache > /dev/null 2>&1
chmod 0777 ./cache
mkdir file_cache > /dev/null 2>&1
chmod 0777 ./file_cache
mkdir logs > /dev/null 2>&1
chmod 0777 ./logs
mkdir spool > /dev/null 2>&1

## UPDATE Web component permission
cd $APP_HOME/sources/www/ordermanager-report-generator/app
mkdir cache > /dev/null 2>&1
chmod 0777 ./cache
mkdir file_cache > /dev/null 2>&1
chmod 0777 ./file_cache
mkdir logs > /dev/null 2>&1
chmod 0777 ./logs
mkdir spool > /dev/null 2>&1

## UPDATE Web component permission
cd $APP_HOME/sources/www/ordermanager-customer-dashboard/app
mkdir cache > /dev/null 2>&1
chmod 0777 ./cache
mkdir file_cache > /dev/null 2>&1
chmod 0777 ./file_cache
mkdir logs > /dev/null 2>&1
chmod 0777 ./logs
mkdir spool > /dev/null 2>&1
