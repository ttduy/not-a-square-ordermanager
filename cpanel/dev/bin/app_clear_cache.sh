SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

## DELETE CACHE FOLDER
rm -rf $APP_HOME/sources/www/ordermanager/app/cache/dev > /dev/null 2>&1
rm -rf $APP_HOME/sources/www/ordermanager-customer-dashboard/app/cache/dev > /dev/null 2>&1
rm -rf $APP_HOME/sources/www/ordermanager-report-generator/app/cache/dev > /dev/null 2>&1