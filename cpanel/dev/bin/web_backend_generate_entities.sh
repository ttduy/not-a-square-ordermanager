SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

sudo docker exec novogenia-ordermanager-dev-web sh /var/www/ordermanager/bin/generate_entities.sh
sudo docker exec novogenia-ordermanager-dev-web sh /var/www/ordermanager-customer-dashboard/bin/generate_db.sh
