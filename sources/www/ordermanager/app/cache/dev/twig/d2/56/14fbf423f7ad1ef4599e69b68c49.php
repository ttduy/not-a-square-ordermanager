<?php

/* DesignStarlightBundle:Layout:PopupPage.html.twig */
class __TwigTemplate_d25614fbf423f7ad1ef4599e69b68c49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle::Base.html.twig");

        $this->blocks = array(
            'ContentHeader' => array($this, 'block_ContentHeader'),
            'MainContent' => array($this, 'block_MainContent'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle::Base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_ContentHeader($context, array $blocks = array())
    {
    }

    // line 6
    public function block_MainContent($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"mainwrapper\">
    <div class=\"mainwrapperinner\"> 
        <div style=\"margin: 20px 20px 20px 20px\">
            ";
        // line 10
        $this->displayBlock('MainContentInner', $context, $blocks);
        // line 12
        echo "        </div>
    </div>
</div>
";
    }

    // line 10
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 11
        echo "            ";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Layout:PopupPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 11,  50 => 10,  41 => 10,  36 => 7,  33 => 6,  28 => 3,  211 => 65,  201 => 64,  195 => 63,  185 => 55,  176 => 51,  172 => 50,  169 => 49,  166 => 48,  160 => 46,  156 => 45,  152 => 44,  148 => 43,  144 => 42,  140 => 41,  136 => 40,  132 => 39,  128 => 38,  124 => 37,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  104 => 32,  100 => 31,  92 => 26,  88 => 25,  83 => 23,  79 => 22,  74 => 20,  70 => 19,  66 => 18,  60 => 15,  54 => 12,  48 => 9,  43 => 12,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
