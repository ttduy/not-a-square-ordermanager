<?php

/* LeepAdminBundle:Bill:app_billing_invoice_customer.html.twig */
class __TwigTemplate_cd381bc1b7201d8626d31af513ee1e31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_billing_invoice_customer_row' => array($this, 'block_app_billing_invoice_customer_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_billing_invoice_customer_row', $context, $blocks);
    }

    public function block_app_billing_invoice_customer_row($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBillingInvoiceCustomer"), 'row');
        echo "
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nameCustomer"), 'row');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "totalAmount"), 'row');
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "overrideAmount"), 'row');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "haveInvoiceAddress"), 'row');
        echo "    
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "orderInformation"), 'row');
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressStreet"), 'row');
        echo "
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressPostCode"), 'row');
        echo "
    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressCity"), 'row');
        echo "
    ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressIdCountry"), 'row');
        echo "
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressTelephone"), 'row');
        echo "
    ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressFax"), 'row');
        echo "
    ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressUid"), 'row');
        echo "

    <script type=\"text/javascript\">
        function showOrderDetail() {
            var idBillingInvoiceCustomer = jQuery('#";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBillingInvoiceCustomer"), "vars"), "id"), "html", null, true);
        echo "').val();

            var url = '";
        // line 20
        echo twig_escape_filter($this->env, $this->getContext($context, "viewOrderEntriesLink"), "html", null, true);
        echo "?idBillingInvoiceCustomer=' + idBillingInvoiceCustomer;
            jQuery.colorbox({
                iframe: true,
                width:  \"90%\",
                height: \"90%\",
                href:  url,
                onClosed:function(){
                    location.reload();
                }
            });
            return false;
        }

        jQuery(document).ready(function() {
            jQuery('#";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBillingInvoiceCustomer"), "vars"), "id"), "html", null, true);
        echo "').change(function() {
                jQuery('#field_row_form_customer_totalAmount').find('span').html('Loading..');
                jQuery('#";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "overrideAmount"), "vars"), "id"), "html", null, true);
        echo "').val('');

                var targetUrl = '";
        // line 38
        echo twig_escape_filter($this->env, $this->getContext($context, "getBillingInvoiceCustomer"), "html", null, true);
        echo "?id=' + jQuery(this).val();

                jQuery.ajax({
                    url: targetUrl,
                    type: \"POST\",
                    context: document.body
                }).done(function(data) {
                    if (data == 'FAIL') {
                        alert(\"ERROR! CAN'T LOAD DATA\");
                        return;
                    }
                    var invoiceCustomer = JSON.parse(data);
                    jQuery('#field_row_form_customer_totalAmount').find('span').html(invoiceCustomer['totalAmount'] + '&nbsp;');
                    jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "overrideAmount"), "vars"), "id"), "html", null, true);
        echo "').val(invoiceCustomer['overrideAmount']);
                    if (invoiceCustomer['invoiceAddressIsUsed'] == true) {
                        jQuery('#form_customer_invoiceAddressStreet').val(invoiceCustomer['invoiceAddressStreet']);
                        jQuery('#form_customer_invoiceAddressPostCode').val(invoiceCustomer['invoiceAddressPostCode']);
                        jQuery('#form_customer_invoiceAddressCity').val(invoiceCustomer['invoiceAddressCity']);
                        jQuery('#form_customer_invoiceAddressIdCountry').val(invoiceCustomer['invoiceAddressIdCountry']);
                        jQuery('#form_customer_invoiceAddressTelephone').val(invoiceCustomer['invoiceAddressTelephone']);
                        jQuery('#form_customer_invoiceAddressFax').val(invoiceCustomer['invoiceAddressFax']);
                        jQuery('#form_customer_invoiceAddressUid').val(invoiceCustomer['invoiceAddressUid']);
                        jQuery('#form_orderSelector_haveInvoiceAddress').val(invoiceCustomer['haveInvoiceAddress']);
                    }
                    else {
                        jQuery('#form_customer_invoiceAddressStreet').val('');
                        jQuery('#form_customer_invoiceAddressPostCode').val('');
                        jQuery('#form_customer_invoiceAddressCity').val('');
                        jQuery('#form_customer_invoiceAddressIdCountry').val('');
                        jQuery('#form_customer_invoiceAddressTelephone').val('');
                        jQuery('#form_customer_invoiceAddressFax').val('');
                        jQuery('#form_customer_invoiceAddressUid').val('');
                        jQuery('#form_orderSelector_haveInvoiceAddress').val(invoiceCustomer['haveInvoiceAddress']);
                    }
                }).error(function(jqXHR, textStatus, errorThrown ) {
                    alert(\"ERROR! CAN'T LOAD DATA\");
                });
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Bill:app_billing_invoice_customer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  112 => 38,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 12,  143 => 67,  128 => 51,  125 => 54,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 7,  99 => 28,  87 => 22,  83 => 21,  70 => 46,  62 => 26,  43 => 12,  89 => 39,  75 => 30,  72 => 17,  38 => 6,  82 => 34,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 11,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 13,  66 => 13,  56 => 12,  54 => 15,  49 => 8,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 69,  137 => 66,  135 => 18,  129 => 60,  127 => 49,  119 => 48,  116 => 56,  110 => 46,  104 => 44,  101 => 43,  97 => 67,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 20,  78 => 14,  52 => 12,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 13,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 15,  117 => 48,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 59,  80 => 18,  74 => 39,  55 => 24,  50 => 14,  44 => 17,  39 => 7,  33 => 4,  26 => 2,  68 => 35,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 24,  35 => 11,  31 => 4,  28 => 4,  170 => 83,  165 => 51,  65 => 12,  63 => 18,  59 => 25,  57 => 10,  53 => 9,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}
