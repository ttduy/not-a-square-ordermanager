<?php

/* LeepAdminBundle:DistributionChannel:manage_gene.html.twig */
class __TwigTemplate_7b60a957ea5a46704413e38cde532ae2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:PopupPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:PopupPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/style.darkblue.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/geno_form_quick_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <!--[if IE 9]>
        <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie9.css"), "html", null, true);
        echo "\"/>
    <![endif]-->
    <!--[if IE 8]>
        <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie8.css"), "html", null, true);
        echo "\"/>
    <![endif]-->
    <!--[if IE 7]>
        <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie7.css"), "html", null, true);
        echo "\"/>
    <![endif]-->

    <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery-ui-1.11.4.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery-migrate-1.2.1.min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery.colorbox-min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/custom/general.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!--[if lt IE 9]>
        <script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
    <![endif]-->
    <!--[if lte IE 8]><script language=\"javascript\" type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
    <script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/js/searchable_cache_box.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/sweet-alert.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/redactor/redactor.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/table/table.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/fontcolor/fontcolor.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/fontfamily/fontfamily.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/fontsize/fontsize.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/fullscreen/fullscreen.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/imagemanager/imagemanager.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor1025/plugins/textdirection/textdirection.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/redactor/redactor.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 48
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 49
        echo "    <div class=\"content\">
    ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "header"));
        foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
            // line 51
            echo "        <h3><span style=\"position: relative; margin-left: 4px; top: -4px; width:200px\">
            Checking gene</span>
        </span></h3>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 55
        echo "    <br>
    <form action=\"#\">
        Gene ID: <input type=\"text\" name=\"geneId\" placeholder=\"Insert the gene ID\" id='geneId'> &nbsp <input type=\"submit\" style=\"padding: 2px 10px\" value='Find' onClick=\"GetID()\">
    </form> 
    
    <script type=\"text/javascript\">
        function GetID(){
            var geneId = jQuery('#geneId').val();
            '";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "header"));
        foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
            echo "'
                var targetUrl = '";
            // line 64
            echo twig_escape_filter($this->env, $this->getContext($context, "statisticUrl"), "html", null, true);
            echo "?geneId=' + geneId + '&dcId=' + '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "header"), "id", array(), "array"), "html", null, true);
            echo "';
            '";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 65
        echo "'
            window.open(targetUrl);
        }

    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:DistributionChannel:manage_gene.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 65,  201 => 64,  195 => 63,  185 => 55,  176 => 51,  172 => 50,  169 => 49,  166 => 48,  160 => 46,  156 => 45,  152 => 44,  148 => 43,  144 => 42,  140 => 41,  136 => 40,  132 => 39,  128 => 38,  124 => 37,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  104 => 32,  100 => 31,  92 => 26,  88 => 25,  83 => 23,  79 => 22,  74 => 20,  70 => 19,  66 => 18,  60 => 15,  54 => 12,  48 => 9,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
