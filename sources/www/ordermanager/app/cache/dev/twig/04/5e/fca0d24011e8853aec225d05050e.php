<?php

/* form_div_layout.html.twig */
class __TwigTemplate_045efca0d24011e8853aec225d05050e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_widget' => array($this, 'block_form_widget'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'form_widget_compound' => array($this, 'block_form_widget_compound'),
            'collection_widget' => array($this, 'block_collection_widget'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'choice_widget' => array($this, 'block_choice_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_options' => array($this, 'block_choice_widget_options'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'number_widget' => array($this, 'block_number_widget'),
            'integer_widget' => array($this, 'block_integer_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'url_widget' => array($this, 'block_url_widget'),
            'search_widget' => array($this, 'block_search_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'password_widget' => array($this, 'block_password_widget'),
            'hidden_widget' => array($this, 'block_hidden_widget'),
            'email_widget' => array($this, 'block_email_widget'),
            'form_label' => array($this, 'block_form_label'),
            'repeated_row' => array($this, 'block_repeated_row'),
            'form_row' => array($this, 'block_form_row'),
            'hidden_row' => array($this, 'block_hidden_row'),
            'form_enctype' => array($this, 'block_form_enctype'),
            'form_errors' => array($this, 'block_form_errors'),
            'form_rest' => array($this, 'block_form_rest'),
            'form_rows' => array($this, 'block_form_rows'),
            'widget_attributes' => array($this, 'block_widget_attributes'),
            'widget_container_attributes' => array($this, 'block_widget_container_attributes'),
            'generic_label' => array($this, 'block_generic_label'),
            'widget_choice_options' => array($this, 'block_widget_choice_options'),
            'field_widget' => array($this, 'block_field_widget'),
            'field_label' => array($this, 'block_field_label'),
            'field_row' => array($this, 'block_field_row'),
            'field_enctype' => array($this, 'block_field_enctype'),
            'field_errors' => array($this, 'block_field_errors'),
            'field_rest' => array($this, 'block_field_rest'),
            'field_rows' => array($this, 'block_field_rows'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_widget', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('form_widget_compound', $context, $blocks);
        // line 31
        echo "
";
        // line 32
        $this->displayBlock('collection_widget', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 46
        echo "
";
        // line 47
        $this->displayBlock('choice_widget', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 67
        echo "
";
        // line 68
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 86
        echo "
";
        // line 87
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 101
        echo "
";
        // line 102
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 107
        echo "
";
        // line 108
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 113
        echo "
";
        // line 114
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 128
        echo "
";
        // line 129
        $this->displayBlock('date_widget', $context, $blocks);
        // line 144
        echo "
";
        // line 145
        $this->displayBlock('time_widget', $context, $blocks);
        // line 156
        echo "
";
        // line 157
        $this->displayBlock('number_widget', $context, $blocks);
        // line 164
        echo "
";
        // line 165
        $this->displayBlock('integer_widget', $context, $blocks);
        // line 171
        echo "
";
        // line 172
        $this->displayBlock('money_widget', $context, $blocks);
        // line 177
        echo "
";
        // line 178
        $this->displayBlock('url_widget', $context, $blocks);
        // line 184
        echo "
";
        // line 185
        $this->displayBlock('search_widget', $context, $blocks);
        // line 191
        echo "
";
        // line 192
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 198
        echo "
";
        // line 199
        $this->displayBlock('password_widget', $context, $blocks);
        // line 205
        echo "
";
        // line 206
        $this->displayBlock('hidden_widget', $context, $blocks);
        // line 212
        echo "
";
        // line 213
        $this->displayBlock('email_widget', $context, $blocks);
        // line 219
        echo "
";
        // line 221
        echo "
";
        // line 222
        $this->displayBlock('form_label', $context, $blocks);
        // line 236
        echo "
";
        // line 238
        echo "
";
        // line 239
        $this->displayBlock('repeated_row', $context, $blocks);
        // line 248
        echo "
";
        // line 249
        $this->displayBlock('form_row', $context, $blocks);
        // line 258
        echo "
";
        // line 259
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 262
        echo "
";
        // line 264
        echo "
";
        // line 265
        $this->displayBlock('form_enctype', $context, $blocks);
        // line 270
        echo "
";
        // line 271
        $this->displayBlock('form_errors', $context, $blocks);
        // line 286
        echo "
";
        // line 287
        $this->displayBlock('form_rest', $context, $blocks);
        // line 296
        echo "
";
        // line 298
        echo "
";
        // line 299
        $this->displayBlock('form_rows', $context, $blocks);
        // line 306
        echo "
";
        // line 307
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('widget_container_attributes', $context, $blocks);
        // line 320
        echo "
";
        // line 322
        echo "
";
        // line 323
        $this->displayBlock('generic_label', $context, $blocks);
        // line 324
        $this->displayBlock('widget_choice_options', $context, $blocks);
        // line 325
        $this->displayBlock('field_widget', $context, $blocks);
        // line 326
        $this->displayBlock('field_label', $context, $blocks);
        // line 327
        $this->displayBlock('field_row', $context, $blocks);
        // line 328
        $this->displayBlock('field_enctype', $context, $blocks);
        // line 329
        $this->displayBlock('field_errors', $context, $blocks);
        // line 330
        $this->displayBlock('field_rest', $context, $blocks);
        // line 331
        $this->displayBlock('field_rows', $context, $blocks);
    }

    // line 3
    public function block_form_widget($context, array $blocks = array())
    {
        // line 4
        ob_start();
        // line 5
        echo "    ";
        if ($this->getContext($context, "compound")) {
            // line 6
            echo "        ";
            $this->displayBlock("form_widget_compound", $context, $blocks);
            echo "
    ";
        } else {
            // line 8
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 13
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 14
        ob_start();
        // line 15
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 16
        echo "    <input type=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "type"), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ((!twig_test_empty($this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\" ";
        }
        echo "/>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 20
    public function block_form_widget_compound($context, array $blocks = array())
    {
        // line 21
        ob_start();
        // line 22
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 23
        if (twig_test_empty($this->getAttribute($this->getContext($context, "form"), "parent"))) {
            // line 24
            echo "            ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
        ";
        }
        // line 26
        echo "        ";
        $this->displayBlock("form_rows", $context, $blocks);
        echo "
        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 32
    public function block_collection_widget($context, array $blocks = array())
    {
        // line 33
        ob_start();
        // line 34
        echo "    ";
        if (array_key_exists("prototype", $context)) {
            // line 35
            echo "        ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("data-prototype" => $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "prototype"), 'row')));
            // line 36
            echo "    ";
        }
        // line 37
        echo "    ";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 41
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 42
        ob_start();
        // line 43
        echo "    <textarea ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
        echo "</textarea>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 47
    public function block_choice_widget($context, array $blocks = array())
    {
        // line 48
        ob_start();
        // line 49
        echo "    ";
        if ($this->getContext($context, "expanded")) {
            // line 50
            echo "        ";
            $this->displayBlock("choice_widget_expanded", $context, $blocks);
            echo "
    ";
        } else {
            // line 52
            echo "        ";
            $this->displayBlock("choice_widget_collapsed", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 57
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 58
        ob_start();
        // line 59
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
    ";
        // line 60
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 61
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'widget');
            echo "
        ";
            // line 62
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'label');
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 64
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 68
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 69
        ob_start();
        // line 70
        echo "    <select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ($this->getContext($context, "multiple")) {
            echo " multiple=\"multiple\"";
        }
        echo ">
        ";
        // line 71
        if ((!(null === $this->getContext($context, "empty_value")))) {
            // line 72
            echo "            <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "empty_value"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
            echo "</option>
        ";
        }
        // line 74
        echo "        ";
        if ((twig_length_filter($this->env, $this->getContext($context, "preferred_choices")) > 0)) {
            // line 75
            echo "            ";
            $context["options"] = $this->getContext($context, "preferred_choices");
            // line 76
            echo "            ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
            ";
            // line 77
            if (((twig_length_filter($this->env, $this->getContext($context, "choices")) > 0) && (!(null === $this->getContext($context, "separator"))))) {
                // line 78
                echo "                <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "separator"), "html", null, true);
                echo "</option>
            ";
            }
            // line 80
            echo "        ";
        }
        // line 81
        echo "        ";
        $context["options"] = $this->getContext($context, "choices");
        // line 82
        echo "        ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
    </select>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 87
    public function block_choice_widget_options($context, array $blocks = array())
    {
        // line 88
        ob_start();
        // line 89
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "options"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 90
            echo "        ";
            if (twig_test_iterable($this->getContext($context, "choice"))) {
                // line 91
                echo "            <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "group_label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "\">
                ";
                // line 92
                $context["options"] = $this->getContext($context, "choice");
                // line 93
                echo "                ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
            </optgroup>
        ";
            } else {
                // line 96
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "choice"), "value"), "html", null, true);
                echo "\"";
                if ($this->env->getExtension('form')->isSelectedChoice($this->getContext($context, "choice"), $this->getContext($context, "value"))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "choice"), "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "</option>
        ";
            }
            // line 98
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 102
    public function block_checkbox_widget($context, array $blocks = array())
    {
        // line 103
        ob_start();
        // line 104
        echo "    <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\"";
        }
        if ($this->getContext($context, "checked")) {
            echo " checked=\"checked\"";
        }
        echo " />
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 108
    public function block_radio_widget($context, array $blocks = array())
    {
        // line 109
        ob_start();
        // line 110
        echo "    <input type=\"radio\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\"";
        }
        if ($this->getContext($context, "checked")) {
            echo " checked=\"checked\"";
        }
        echo " />
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 114
    public function block_datetime_widget($context, array $blocks = array())
    {
        // line 115
        ob_start();
        // line 116
        echo "    ";
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 117
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 119
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 120
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'errors');
            echo "
            ";
            // line 121
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "time"), 'errors');
            echo "
            ";
            // line 122
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'widget');
            echo "
            ";
            // line 123
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "time"), 'widget');
            echo "
        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 129
    public function block_date_widget($context, array $blocks = array())
    {
        // line 130
        ob_start();
        // line 131
        echo "    ";
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 132
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 134
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 135
            echo strtr($this->getContext($context, "date_pattern"), array("{{ year }}" =>             // line 136
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "year"), 'widget'), "{{ month }}" =>             // line 137
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "month"), 'widget'), "{{ day }}" =>             // line 138
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "day"), 'widget')));
            // line 139
            echo "
        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 145
    public function block_time_widget($context, array $blocks = array())
    {
        // line 146
        ob_start();
        // line 147
        echo "    ";
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 148
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 150
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 151
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "hour"), 'widget', array("attr" => array("size" => "1")));
            echo ":";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "minute"), 'widget', array("attr" => array("size" => "1")));
            if ($this->getContext($context, "with_seconds")) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "second"), 'widget', array("attr" => array("size" => "1")));
            }
            // line 152
            echo "        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 157
    public function block_number_widget($context, array $blocks = array())
    {
        // line 158
        ob_start();
        // line 159
        echo "    ";
        // line 160
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 161
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 165
    public function block_integer_widget($context, array $blocks = array())
    {
        // line 166
        ob_start();
        // line 167
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "number")) : ("number"));
        // line 168
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 172
    public function block_money_widget($context, array $blocks = array())
    {
        // line 173
        ob_start();
        // line 174
        echo "    ";
        echo strtr($this->getContext($context, "money_pattern"), array("{{ widget }}" => $this->renderBlock("form_widget_simple", $context, $blocks)));
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 178
    public function block_url_widget($context, array $blocks = array())
    {
        // line 179
        ob_start();
        // line 180
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "url")) : ("url"));
        // line 181
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 185
    public function block_search_widget($context, array $blocks = array())
    {
        // line 186
        ob_start();
        // line 187
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "search")) : ("search"));
        // line 188
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 192
    public function block_percent_widget($context, array $blocks = array())
    {
        // line 193
        ob_start();
        // line 194
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 195
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo " %
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 199
    public function block_password_widget($context, array $blocks = array())
    {
        // line 200
        ob_start();
        // line 201
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "password")) : ("password"));
        // line 202
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 206
    public function block_hidden_widget($context, array $blocks = array())
    {
        // line 207
        ob_start();
        // line 208
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "hidden")) : ("hidden"));
        // line 209
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 213
    public function block_email_widget($context, array $blocks = array())
    {
        // line 214
        ob_start();
        // line 215
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "email")) : ("email"));
        // line 216
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 222
    public function block_form_label($context, array $blocks = array())
    {
        // line 223
        ob_start();
        // line 224
        echo "    ";
        if ((!$this->getContext($context, "compound"))) {
            // line 225
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id")));
            // line 226
            echo "    ";
        }
        // line 227
        echo "    ";
        if ($this->getContext($context, "required")) {
            // line 228
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class"), "")) : ("")) . " required"))));
            // line 229
            echo "    ";
        }
        // line 230
        echo "    ";
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 231
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
            // line 232
            echo "    ";
        }
        // line 233
        echo "    <label";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "</label>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 239
    public function block_repeated_row($context, array $blocks = array())
    {
        // line 240
        ob_start();
        // line 241
        echo "    ";
        // line 245
        echo "    ";
        $this->displayBlock("form_rows", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 249
    public function block_form_row($context, array $blocks = array())
    {
        // line 250
        ob_start();
        // line 251
        echo "    <div>
        ";
        // line 252
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
        echo "
        ";
        // line 253
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
        ";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 259
    public function block_hidden_row($context, array $blocks = array())
    {
        // line 260
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
";
    }

    // line 265
    public function block_form_enctype($context, array $blocks = array())
    {
        // line 266
        ob_start();
        // line 267
        echo "    ";
        if ($this->getContext($context, "multipart")) {
            echo "enctype=\"multipart/form-data\"";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 271
    public function block_form_errors($context, array $blocks = array())
    {
        // line 272
        ob_start();
        // line 273
        echo "    ";
        if ((twig_length_filter($this->env, $this->getContext($context, "errors")) > 0)) {
            // line 274
            echo "    <ul>
        ";
            // line 275
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 276
                echo "            <li>";
                echo twig_escape_filter($this->env, (((null === $this->getAttribute($this->getContext($context, "error"), "messagePluralization"))) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messagePluralization"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators"))), "html", null, true);
                // line 280
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 282
            echo "    </ul>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 287
    public function block_form_rest($context, array $blocks = array())
    {
        // line 288
        ob_start();
        // line 289
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 290
            echo "        ";
            if ((!$this->getAttribute($this->getContext($context, "child"), "rendered"))) {
                // line 291
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'row');
                echo "
        ";
            }
            // line 293
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 299
    public function block_form_rows($context, array $blocks = array())
    {
        // line 300
        ob_start();
        // line 301
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 302
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'row');
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 307
    public function block_widget_attributes($context, array $blocks = array())
    {
        // line 308
        ob_start();
        // line 309
        echo "    id=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
        echo "\"";
        if ($this->getContext($context, "read_only")) {
            echo " readonly=\"readonly\"";
        }
        if ($this->getContext($context, "disabled")) {
            echo " disabled=\"disabled\"";
        }
        if ($this->getContext($context, "required")) {
            echo " required=\"required\"";
        }
        if ($this->getContext($context, "max_length")) {
            echo " maxlength=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "max_length"), "html", null, true);
            echo "\"";
        }
        if ($this->getContext($context, "pattern")) {
            echo " pattern=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "pattern"), "html", null, true);
            echo "\"";
        }
        // line 310
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            if (twig_in_filter($this->getContext($context, "attrname"), array(0 => "placeholder", 1 => "title"))) {
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "attrvalue"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "\" ";
            } else {
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                echo "\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 314
    public function block_widget_container_attributes($context, array $blocks = array())
    {
        // line 315
        ob_start();
        // line 316
        echo "    ";
        if ((!twig_test_empty($this->getContext($context, "id")))) {
            echo "id=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "\" ";
        }
        // line 317
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\" ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 323
    public function block_generic_label($context, array $blocks = array())
    {
        $this->displayBlock("form_label", $context, $blocks);
    }

    // line 324
    public function block_widget_choice_options($context, array $blocks = array())
    {
        $this->displayBlock("choice_widget_options", $context, $blocks);
    }

    // line 325
    public function block_field_widget($context, array $blocks = array())
    {
        $this->displayBlock("form_widget_simple", $context, $blocks);
    }

    // line 326
    public function block_field_label($context, array $blocks = array())
    {
        $this->displayBlock("form_label", $context, $blocks);
    }

    // line 327
    public function block_field_row($context, array $blocks = array())
    {
        $this->displayBlock("form_row", $context, $blocks);
    }

    // line 328
    public function block_field_enctype($context, array $blocks = array())
    {
        $this->displayBlock("form_enctype", $context, $blocks);
    }

    // line 329
    public function block_field_errors($context, array $blocks = array())
    {
        $this->displayBlock("form_errors", $context, $blocks);
    }

    // line 330
    public function block_field_rest($context, array $blocks = array())
    {
        $this->displayBlock("form_rest", $context, $blocks);
    }

    // line 331
    public function block_field_rows($context, array $blocks = array())
    {
        $this->displayBlock("form_rows", $context, $blocks);
    }

    public function getTemplateName()
    {
        return "form_div_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1174 => 331,  1168 => 330,  1162 => 329,  1156 => 328,  1150 => 327,  1144 => 326,  1138 => 325,  1132 => 324,  1126 => 323,  1110 => 317,  1103 => 316,  1098 => 314,  1050 => 309,  1048 => 308,  1033 => 302,  1028 => 301,  1026 => 300,  1014 => 293,  1008 => 291,  1005 => 290,  1000 => 289,  995 => 287,  988 => 282,  978 => 276,  974 => 275,  971 => 274,  968 => 273,  963 => 271,  953 => 266,  943 => 260,  932 => 254,  928 => 253,  924 => 252,  921 => 251,  919 => 250,  916 => 249,  908 => 245,  906 => 241,  904 => 240,  901 => 239,  877 => 232,  871 => 230,  868 => 229,  862 => 227,  859 => 226,  856 => 225,  851 => 223,  848 => 222,  840 => 216,  837 => 215,  835 => 214,  832 => 213,  824 => 209,  821 => 208,  819 => 207,  816 => 206,  808 => 202,  805 => 201,  800 => 199,  789 => 194,  787 => 193,  773 => 187,  771 => 186,  757 => 180,  755 => 179,  744 => 174,  742 => 173,  739 => 172,  731 => 168,  728 => 167,  726 => 166,  723 => 165,  715 => 161,  712 => 160,  710 => 159,  708 => 158,  698 => 152,  685 => 150,  679 => 148,  674 => 146,  671 => 145,  663 => 139,  661 => 138,  659 => 136,  658 => 135,  653 => 134,  644 => 131,  642 => 130,  639 => 129,  630 => 123,  626 => 122,  622 => 121,  618 => 120,  613 => 119,  607 => 117,  604 => 116,  602 => 115,  599 => 114,  583 => 110,  578 => 108,  560 => 103,  557 => 102,  528 => 96,  519 => 92,  514 => 91,  493 => 89,  491 => 88,  479 => 82,  473 => 80,  467 => 78,  465 => 77,  457 => 75,  454 => 74,  436 => 69,  427 => 64,  419 => 62,  414 => 61,  405 => 59,  385 => 50,  382 => 49,  377 => 47,  365 => 42,  362 => 41,  351 => 36,  343 => 33,  321 => 24,  319 => 23,  314 => 22,  309 => 20,  290 => 15,  258 => 331,  250 => 327,  239 => 322,  231 => 313,  229 => 307,  221 => 298,  211 => 271,  203 => 264,  198 => 259,  177 => 221,  122 => 129,  161 => 98,  409 => 264,  397 => 255,  367 => 43,  347 => 217,  307 => 186,  266 => 154,  247 => 141,  174 => 219,  167 => 206,  132 => 157,  124 => 144,  1129 => 306,  1124 => 304,  1117 => 300,  1113 => 299,  1109 => 298,  1105 => 297,  1101 => 315,  1097 => 295,  1091 => 294,  1085 => 291,  1079 => 290,  1075 => 310,  1071 => 288,  1065 => 287,  1059 => 284,  1055 => 283,  1051 => 282,  1045 => 307,  1040 => 279,  1036 => 278,  1030 => 277,  1023 => 299,  1017 => 272,  1009 => 267,  1003 => 266,  998 => 288,  992 => 263,  987 => 261,  981 => 280,  976 => 258,  972 => 257,  966 => 272,  961 => 254,  955 => 267,  950 => 265,  946 => 250,  940 => 259,  935 => 247,  931 => 246,  925 => 245,  920 => 243,  914 => 242,  907 => 238,  903 => 237,  899 => 236,  895 => 235,  889 => 234,  884 => 232,  880 => 233,  874 => 231,  869 => 228,  865 => 228,  861 => 226,  857 => 225,  853 => 224,  847 => 223,  842 => 221,  838 => 220,  834 => 219,  830 => 218,  826 => 217,  820 => 216,  815 => 214,  811 => 213,  807 => 212,  803 => 200,  797 => 210,  792 => 195,  788 => 207,  784 => 192,  780 => 205,  776 => 188,  772 => 203,  768 => 185,  764 => 201,  760 => 181,  756 => 199,  752 => 178,  746 => 197,  741 => 195,  737 => 194,  733 => 193,  729 => 192,  725 => 191,  721 => 190,  717 => 189,  713 => 188,  709 => 187,  705 => 157,  699 => 185,  694 => 183,  690 => 151,  686 => 181,  682 => 180,  676 => 147,  670 => 176,  666 => 175,  660 => 137,  655 => 172,  651 => 171,  647 => 132,  643 => 169,  637 => 168,  632 => 166,  628 => 165,  624 => 164,  620 => 163,  616 => 162,  612 => 161,  606 => 160,  601 => 158,  597 => 157,  593 => 156,  589 => 155,  585 => 154,  581 => 109,  576 => 151,  572 => 150,  568 => 149,  562 => 104,  555 => 144,  549 => 143,  544 => 141,  540 => 98,  536 => 139,  532 => 138,  526 => 137,  521 => 93,  517 => 134,  511 => 90,  506 => 131,  502 => 130,  498 => 129,  494 => 128,  488 => 87,  483 => 125,  477 => 124,  472 => 122,  468 => 121,  464 => 120,  460 => 76,  456 => 118,  452 => 117,  448 => 72,  444 => 115,  438 => 70,  433 => 68,  429 => 111,  425 => 110,  421 => 109,  415 => 108,  410 => 60,  402 => 104,  396 => 103,  391 => 52,  387 => 100,  381 => 99,  376 => 97,  372 => 96,  368 => 95,  364 => 94,  360 => 93,  354 => 37,  349 => 90,  339 => 212,  328 => 83,  324 => 82,  320 => 81,  284 => 72,  280 => 71,  276 => 8,  253 => 64,  218 => 296,  214 => 54,  202 => 51,  191 => 48,  180 => 222,  133 => 33,  100 => 30,  93 => 28,  25 => 2,  19 => 1,  95 => 45,  139 => 171,  194 => 16,  111 => 35,  149 => 184,  92 => 57,  196 => 50,  153 => 47,  148 => 51,  141 => 47,  123 => 39,  114 => 113,  112 => 108,  107 => 102,  102 => 87,  73 => 15,  249 => 63,  103 => 55,  326 => 168,  296 => 75,  292 => 74,  285 => 13,  261 => 66,  252 => 328,  240 => 124,  233 => 59,  186 => 14,  154 => 191,  143 => 105,  128 => 110,  125 => 49,  108 => 25,  77 => 32,  342 => 168,  337 => 165,  316 => 80,  312 => 21,  297 => 152,  293 => 16,  281 => 141,  274 => 137,  270 => 6,  257 => 65,  246 => 325,  236 => 320,  227 => 58,  219 => 111,  216 => 287,  195 => 258,  193 => 249,  190 => 248,  188 => 239,  185 => 238,  147 => 178,  142 => 172,  120 => 57,  32 => 5,  99 => 86,  87 => 47,  83 => 28,  70 => 14,  62 => 3,  43 => 6,  89 => 56,  75 => 21,  72 => 20,  38 => 8,  82 => 41,  67 => 13,  18 => 1,  476 => 81,  469 => 162,  466 => 161,  446 => 71,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 105,  403 => 58,  400 => 57,  390 => 120,  384 => 117,  380 => 48,  373 => 113,  370 => 112,  359 => 226,  355 => 103,  348 => 35,  345 => 34,  340 => 32,  332 => 27,  327 => 26,  317 => 161,  304 => 77,  294 => 176,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 4,  262 => 3,  248 => 326,  243 => 58,  241 => 61,  238 => 56,  232 => 52,  226 => 306,  220 => 48,  212 => 109,  206 => 265,  204 => 39,  200 => 262,  184 => 37,  166 => 93,  163 => 75,  155 => 73,  152 => 185,  140 => 38,  118 => 37,  115 => 41,  106 => 38,  96 => 35,  91 => 27,  84 => 46,  81 => 18,  79 => 40,  76 => 20,  71 => 41,  69 => 19,  66 => 39,  56 => 16,  54 => 10,  49 => 10,  41 => 7,  34 => 5,  29 => 3,  264 => 93,  254 => 329,  244 => 324,  228 => 76,  224 => 299,  222 => 56,  213 => 286,  210 => 53,  208 => 270,  201 => 142,  199 => 154,  187 => 60,  182 => 236,  179 => 86,  175 => 34,  172 => 213,  169 => 212,  164 => 205,  162 => 199,  159 => 198,  156 => 49,  151 => 72,  146 => 78,  144 => 177,  137 => 165,  135 => 44,  129 => 156,  127 => 145,  119 => 128,  116 => 27,  110 => 74,  104 => 101,  101 => 36,  97 => 68,  88 => 45,  64 => 12,  60 => 14,  36 => 5,  23 => 2,  21 => 2,  85 => 19,  78 => 21,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 207,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 78,  303 => 111,  300 => 76,  291 => 98,  288 => 14,  282 => 71,  279 => 93,  277 => 162,  271 => 89,  267 => 5,  263 => 133,  260 => 20,  256 => 330,  245 => 62,  242 => 323,  237 => 60,  234 => 314,  225 => 103,  223 => 123,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 81,  176 => 44,  173 => 107,  171 => 53,  168 => 42,  160 => 90,  157 => 192,  150 => 41,  145 => 36,  138 => 52,  134 => 164,  130 => 56,  126 => 61,  121 => 76,  117 => 114,  113 => 68,  109 => 107,  105 => 32,  98 => 45,  94 => 67,  90 => 21,  86 => 20,  80 => 71,  74 => 31,  55 => 10,  50 => 18,  44 => 7,  39 => 6,  33 => 4,  26 => 2,  68 => 19,  61 => 15,  58 => 12,  48 => 8,  46 => 9,  42 => 7,  35 => 5,  31 => 5,  28 => 4,  170 => 83,  165 => 53,  65 => 13,  63 => 13,  59 => 2,  57 => 14,  53 => 10,  51 => 8,  47 => 18,  45 => 8,  40 => 10,  37 => 5,  30 => 4,  27 => 3,);
    }
}
