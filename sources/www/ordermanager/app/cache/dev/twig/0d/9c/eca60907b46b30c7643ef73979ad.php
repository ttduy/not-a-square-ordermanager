<?php

/* LeepAdminBundle:DistributionChannel:list.html.twig */
class __TwigTemplate_0d9ceca60907b46b30c7643ef73979ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/sweet-alert.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 11
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:DistributionChannel:list.html.twig", "1736422095")->display(array_merge($context, array("grid" => $this->getContext($context, "grid"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:DistributionChannel:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:DistributionChannel:list.html.twig */
class __TwigTemplate_0d9ceca60907b46b30c7643ef73979ad_1736422095 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
            'gridDrawCallback' => array($this, 'block_gridDrawCallback'),
            'filterForm' => array($this, 'block_filterForm'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_gridDrawCallback($context, array $blocks = array())
    {
        // line 14
        echo "            jQuery(\".simpletip\").tooltip({
                bodyHandler: function() {
                    return jQuery(this).find('.hide-content').html();
                },
                left: -300,
                top: 20,
                showURL: false
            });
            jQuery('.dataTablePopupButton').colorbox({
                iframe: true,
                width:  \"75%\",
                height: \"75%\",
                onClosed:function(){
                    funcDynamicTableReload_";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo "();
                }
            });
        ";
    }

    // line 32
    public function block_filterForm($context, array $blocks = array())
    {
        // line 33
        echo "            <style type=\"text/css\">
            button.disabled {
                opacity: 0.5;
            }
            </style>

            ";
        // line 39
        if ($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filter", array(), "array", true, true)) {
            // line 40
            echo "                ";
            $this->env->loadTemplate("LeepAdminBundle:DistributionChannel:list.html.twig", "683695386")->display(array_merge($context, array("form" => $this->getAttribute($this->getContext($context, "grid"), "filter", array(), "array"))));
            // line 107
            echo "            ";
        }
        // line 108
        echo "        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:DistributionChannel:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 108,  145 => 107,  142 => 40,  140 => 39,  132 => 33,  129 => 32,  121 => 27,  106 => 14,  103 => 13,  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:DistributionChannel:list.html.twig */
class __TwigTemplate_0d9ceca60907b46b30c7643ef73979ad_683695386 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Filter.html.twig");

        $this->blocks = array(
            'buttons' => array($this, 'block_buttons'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Filter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 41
    public function block_buttons($context, array $blocks = array())
    {
        // line 42
        echo "                        ";
        $this->displayParentBlock("buttons", $context, $blocks);
        echo "
                        <div style=\"float: right\">
                            <button class=\"radius2\" onclick=\"return exportDC()\">&nbsp;&nbsp;Export&nbsp;&nbsp;</button>
                            <button class=\"radius2\" onclick=\"return ratingDC()\">&nbsp;&nbsp;Rating&nbsp;&nbsp;</button>
                            <button class=\"radius2 disabled\" id=\"btn-web-login-mass-email\" onClick=\"return massWebLoginEmail()\" disabled>&nbsp;&nbsp;Web Login Email&nbsp;&nbsp;</button>
                        </div>
                        <script type=\"text/javascript\">
                            function exportDC() {
                                var url = '";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "distributionChannel", array(), "array"), "exportDC", array(), "array"), "html", null, true);
        echo "';
                                window.open(url,'_blank');
                                return false;
                            }

                            function ratingDC() {
                                var url = '";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "rating", array(), "array"), "ratingDC", array(), "array"), "html", null, true);
        echo "';
                                window.open(url,'_blank');
                                return false;
                            }

                            function massWebLoginEmail() {
                                selectedId = getSelectedId();
                                if (selectedId != []) {
                                    var targetUrl = '";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "distributionChannel", array(), "array"), "webLoginMassEmail", array(), "array"), "html", null, true);
        echo "?selected=' + getSelectedId().join(',');
                                    jQuery.colorbox({
                                        href: targetUrl,
                                        iframe: true,
                                        width:  \"90%\",
                                        height: \"90%\",
                                        onClosed:function(){
                                            funcDynamicTableReload_CrudGrid();
                                            setTimeout(function(){ onCheckBoxChange(); }, 500);
                                        }
                                    });
                                }

                                return false;
                            }

                            function getSelectedId() {
                                var selectedId = [];
                                var checkboxes = jQuery(\"#";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " .stdtableCheckBox\");
                                checkboxes.each(function(i, v) {
                                    if (jQuery(this).is(':checked')) {
                                        var id = jQuery(this).attr('id').substring(4);
                                        selectedId.push(id);
                                    }
                                });

                                return selectedId;
                            }

                            function onCheckBoxChange() {
                                if (getSelectedId().length <= 0) {
                                    jQuery(\"#btn-web-login-mass-email\").addClass('disabled').prop(\"disabled\", true);
                                } else {
                                    jQuery(\"#btn-web-login-mass-email\").removeClass('disabled').prop(\"disabled\", false);
                                }
                            }

                            jQuery(document).on(\"change\", \"input[type='checkbox']\", function () {
                                onCheckBoxChange();
                            });
                        </script>
                    ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:DistributionChannel:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 82,  228 => 64,  217 => 56,  208 => 50,  196 => 42,  193 => 41,  148 => 108,  145 => 107,  142 => 40,  140 => 39,  132 => 33,  129 => 32,  121 => 27,  106 => 14,  103 => 13,  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
