<?php

/* DesignStarlightBundle:Component:Message.html.twig */
class __TwigTemplate_7e8f0f6d7d99bb34bf75984ac7aa999f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((!twig_test_empty($this->getContext($context, "message")))) {
            // line 2
            if (($this->getContext($context, "type") == "info")) {
                // line 3
                echo "    ";
                $context["class"] = "msginfo";
            } elseif (($this->getContext($context, "type") == "error")) {
                // line 5
                echo "    ";
                $context["class"] = "msgerror";
            }
            // line 7
            echo "
<div class=\"notification ";
            // line 8
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter($this->getContext($context, "class"), "msgsuccess")) : ("msgsuccess")), "html", null, true);
            echo "\">
    <a class=\"close\"></a>
    <p> 
        ";
            // line 11
            echo $this->getContext($context, "message");
            echo "
    </p>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Component:Message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 11,  32 => 8,  29 => 7,  25 => 5,  21 => 3,  19 => 2,  17 => 1,);
    }
}
