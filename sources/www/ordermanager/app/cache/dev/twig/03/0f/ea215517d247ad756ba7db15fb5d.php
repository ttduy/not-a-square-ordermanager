<?php

/* LeepAdminBundle:FormTypes:app_push_result.html.twig */
class __TwigTemplate_030fea215517d247ad756ba7db15fb5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_push_result_widget' => array($this, 'block_app_push_result_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_push_result_widget', $context, $blocks);
    }

    public function block_app_push_result_widget($context, array $blocks = array())
    {
        // line 2
        echo "        <tr>
            <td width=\"10%\" style=\"border: none;\">Status</td>
            <td width=\"23%\" style=\"border: none;\">";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "status"), 'widget');
        echo "</td>
            <td width=\"10%\" style=\"border: none;\">Date</td>
            <td width=\"23%\" style=\"border: none;\">";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'widget');
        echo "</td>
            <td width=\"10%\" style=\"border: none;\">Timestamp</td>
            <td width=\"23%\" style=\"border: none;\">";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "timestamp"), 'widget');
        echo "</td>
        </tr>
        <tr>
            <td width=\"10%\" style=\"border: none;\">PUSH</td>
            <td width=\"23%\" style=\"border: none;\">";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "sentOn"), 'widget');
        echo "</td>
            <td width=\"10%\" style=\"border: none;\">Server response</td>
            <td width=\"56%\" colspan=\"3\" style=\"border: none;\">";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "responseCode"), 'widget');
        echo "</td>
        </tr>
        <tr>
            <td width=\"10%\" style=\"border: none;\">Raw response</td>
            <td width=\"90%\" colspan=\"5\" style=\"border: none;\">";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "responseRaw"), 'widget');
        echo "</td>
        </tr>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:app_push_result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  93 => 26,  25 => 2,  19 => 1,  95 => 45,  139 => 104,  194 => 16,  111 => 96,  149 => 46,  92 => 80,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 31,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 26,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 14,  154 => 42,  143 => 105,  128 => 110,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 4,  99 => 28,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 8,  82 => 18,  67 => 17,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 38,  118 => 14,  115 => 13,  106 => 50,  96 => 27,  91 => 2,  84 => 40,  81 => 22,  79 => 20,  76 => 20,  71 => 18,  69 => 32,  66 => 13,  56 => 12,  54 => 10,  49 => 12,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 63,  169 => 32,  164 => 107,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 40,  144 => 44,  137 => 37,  135 => 18,  129 => 46,  127 => 38,  119 => 48,  116 => 33,  110 => 30,  104 => 30,  101 => 28,  97 => 26,  88 => 21,  64 => 99,  60 => 13,  36 => 6,  23 => 2,  21 => 1,  85 => 24,  78 => 21,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 51,  150 => 41,  145 => 22,  138 => 36,  134 => 102,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 22,  90 => 21,  86 => 20,  80 => 71,  74 => 16,  55 => 15,  50 => 14,  44 => 9,  39 => 10,  33 => 6,  26 => 2,  68 => 19,  61 => 16,  58 => 12,  48 => 8,  46 => 10,  42 => 7,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 13,  59 => 18,  57 => 18,  53 => 14,  51 => 10,  47 => 9,  45 => 12,  40 => 6,  37 => 5,  30 => 3,  27 => 3,);
    }
}
