<?php

/* LeepAdminBundle:FormTypes:container_collection.html.twig */
class __TwigTemplate_4cd2492f6a2ad6b3c8900c6e3856eca2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container_collection_row' => array($this, 'block_container_collection_row'),
            '__internal_de267e16634544bb4b71f952cc6a6b87de325c80' => array($this, 'block___internal_de267e16634544bb4b71f952cc6a6b87de325c80'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('container_collection_row', $context, $blocks);
    }

    public function block_container_collection_row($context, array $blocks = array())
    {
        // line 2
        echo "    <div id=\"container_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\" index=\"0\">
        ";
        // line 3
        $context["index"] = 0;
        // line 4
        echo "        ";
        $context["add"] = 1;
        // line 5
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 6
            echo "            <p id=\"container_row_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_";
            echo twig_escape_filter($this->env, $this->getContext($context, "index"), "html", null, true);
            echo "\">
                <span class=\"\" style=\"margin-left:-1px\">
                    ";
            // line 8
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "row"), 'widget');
            echo "
                    ";
            // line 9
            if ($this->getAttribute($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "vars", array(), "any", false, true), "disable-add", array(), "array", true, true)) {
                // line 10
                echo "                        ";
                $context["add"] = 0;
                // line 11
                echo "                    ";
            }
            // line 12
            echo "                    ";
            if ((!$this->getAttribute($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "vars", array(), "any", false, true), "disable-insert", array(), "array", true, true))) {
                // line 13
                echo "                        <a href=\"javascript:tableCollectionInsertRow('";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
                echo "', '";
                echo twig_escape_filter($this->env, $this->getContext($context, "index"), "html", null, true);
                echo "')\" style=\"margin-left: 20px\">Insert</a>
                    ";
            }
            // line 15
            echo "                    ";
            if ((!$this->getAttribute($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "vars", array(), "any", false, true), "disable-delete", array(), "array", true, true))) {
                // line 16
                echo "                        <a href=\"javascript:tableCollectionRemoveRow('";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
                echo "', '";
                echo twig_escape_filter($this->env, $this->getContext($context, "index"), "html", null, true);
                echo "')\" style=\"margin-left: 20px\">Remove</a>
                    ";
            }
            // line 18
            echo "                </span>
            </p>
            ";
            // line 20
            $context["index"] = ($this->getContext($context, "index") + 1);
            // line 21
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 22
        echo "
        <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                var container = jQuery('#container_";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "');
                container.attr('index', ";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "index"), "html", null, true);
        echo ");
            });
        </script>
    </div>
    ";
        // line 30
        if (($this->getContext($context, "add") == 1)) {
            // line 31
            echo "        <p>
            <span class=\"field\" style=\"margin-left:-1px\">
                <a href=\"javascript:tableCollectionAddRow('";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "')\">Add</a>
            </span>
        </p>
    ";
        }
        // line 37
        echo "    <div id=\"prototype_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\" data-prototype=\"";
        ob_start();
        echo twig_escape_filter($this->env, (string) $this->renderBlock("__internal_de267e16634544bb4b71f952cc6a6b87de325c80", $context, $blocks));
        // line 46
        echo "        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        echo "\">
    </div>
";
    }

    // line 37
    public function block___internal_de267e16634544bb4b71f952cc6a6b87de325c80($context, array $blocks = array())
    {
        // line 38
        echo "        <p id=\"container_row_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "___name__\">
            <span class=\"field\" style=\"margin-left: -1px\">
                ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "prototype"), 'widget');
        echo "
                <a href=\"javascript:tableCollectionInsertRow('";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "', '__name__')\" style=\"margin-left: 20px\">Insert</a>
                <a href=\"javascript:tableCollectionRemoveRow('";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "', '__name__')\" style=\"margin-left: 20px\">Remove</a>
            </span>
        </p>
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:container_collection.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  19 => 1,  95 => 45,  139 => 104,  194 => 16,  111 => 96,  149 => 46,  92 => 80,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 31,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 26,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 14,  154 => 42,  143 => 105,  128 => 110,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 4,  99 => 25,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 8,  82 => 18,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 38,  118 => 14,  115 => 13,  106 => 50,  96 => 27,  91 => 2,  84 => 40,  81 => 160,  79 => 20,  76 => 141,  71 => 15,  69 => 32,  66 => 13,  56 => 12,  54 => 10,  49 => 17,  41 => 6,  34 => 7,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 63,  169 => 32,  164 => 107,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 40,  144 => 44,  137 => 37,  135 => 18,  129 => 46,  127 => 38,  119 => 48,  116 => 33,  110 => 30,  104 => 44,  101 => 28,  97 => 26,  88 => 21,  64 => 99,  60 => 12,  36 => 6,  23 => 2,  21 => 1,  85 => 24,  78 => 22,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 51,  150 => 41,  145 => 22,  138 => 36,  134 => 102,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 22,  90 => 21,  86 => 20,  80 => 71,  74 => 16,  55 => 15,  50 => 24,  44 => 43,  39 => 10,  33 => 4,  26 => 2,  68 => 19,  61 => 16,  58 => 12,  48 => 8,  46 => 10,  42 => 9,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 13,  59 => 18,  57 => 11,  53 => 14,  51 => 13,  47 => 12,  45 => 7,  40 => 6,  37 => 5,  30 => 3,  27 => 3,);
    }
}
