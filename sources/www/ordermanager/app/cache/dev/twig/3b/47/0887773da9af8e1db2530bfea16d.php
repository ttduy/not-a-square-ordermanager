<?php

/* LeepAdminBundle:FormTypes:app_scan_form_editor.html.twig */
class __TwigTemplate_3b470887773da9af8e1db2530bfea16d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_scan_form_editor_row' => array($this, 'block_app_scan_form_editor_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_scan_form_editor_row', $context, $blocks);
    }

    public function block_app_scan_form_editor_row($context, array $blocks = array())
    {
        // line 2
        echo "    <p id=\"field_row_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\">
        <span class=\"field\" style=\"margin-left: -1px; \">
            <img id=\"formImg\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "formImage"), "vars"), "value"), "html", null, true);
        echo "\"></img><br/>
            <button class=\"button\" id=\"btnAddNewField\">Add new field</button>
            ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "zones"), 'widget');
        echo "
        </span>
    </p>

    <p>
        <span class=\"field\" style=\"margin-left: -1px; \">
            Name: <input id=\"zoneName\" class=\"zoneField\" style=\"min-width: 150px; max-width: 150px\"/>
            Type: <select id=\"zoneType\" class=\"zoneField\"  style=\"min-width: 90px; max-width: 90px\">
                <option value=\"TB\">Text box</option>
                <option value=\"CB\">Checkbox</option>
                <option value=\"TA\">Text area</option>
            </select>

            <a href=\"#\" id=\"zoneDelete\">Delete</a>
        </span>
    </p>

    <script type=\"text/javascript\">
        var zones = [];
        var selectedZone = null;

        function zoneChange() {
            if (selectedZone != null) {
                var zoneName = jQuery('#zoneName').val();
                var zoneType = jQuery('#zoneType').val();
                selectedZone.attr('data-zone-title', zoneName);
                selectedZone.attr('data-zone-type', zoneType);

                var title = \"<b style='color: red'>\" + zoneName + \":\" + zoneType + \"</b>\";
                selectedZone.find('a').html(title);
            }
        }

        function zoneDelete() {
            if (selectedZone != null) {
                selectedZone.css('display', 'none');                
                jQuery('#zoneName').val('');
                jQuery('#zoneType').val('');
            }
        }

        function selectZone(zoneId) {
            var zone = jQuery(\"#\" + zoneId);
            for (var i = 0; i < zones.length; i++) {  
                zones[i].css('border', '2px solid gray');
            }

            zone.css('border', '2px solid red');
            selectedZone = zone;

            jQuery('#zoneName').val(zone.attr('data-zone-title'));
            jQuery('#zoneType').val(zone.attr('data-zone-type'));
        }

        function createZone(parentId, zoneId, zoneTitle, zoneType, x, y, width, height) {
            var parentElement = jQuery(parentId);

            var absX = parentElement.offset().left + x;
            var absY = parentElement.offset().top + y;

            var title = \"<b style='color: red'>\" + zoneTitle + \":\" + zoneType + \"</b>\";
            var zoneButton = jQuery('<a>').html(title);
            zoneButton.attr('href', '#');
            zoneButton.click(function(event) {
                event.preventDefault();
                if (zoneType != 'FORM_ID') {
                    selectZone('zone_' + zoneId);
                }
            });


            var zone = jQuery('<div>');
            zone.attr('id', 'zone_' + zoneId);
            zone.attr('class', 'ui-widget-content');
            zone.append(zoneButton);
            zone.css('display', 'block');
            zone.css('background', 'none');
            zone.css('position', 'absolute');
            zone.css('width', width);
            zone.css('height', height);
            zone.css('border', '2px solid gray');
            zone.offset({ left: absX, top: absY });

            zone.attr('data-zone-id', zoneId);
            zone.attr('data-zone-title', zoneTitle);
            zone.attr('data-zone-type', zoneType);

            zone.draggable();
            zone.resizable();

            jQuery('body').append(zone);

            zones.push(zone);
            return zone;
        }

        var totalField = ";
        // line 102
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getContext($context, "zones")), "html", null, true);
        echo ";
        jQuery(document).ready(function() {
            ";
        // line 104
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "zones"));
        foreach ($context['_seq'] as $context["i"] => $context["zone"]) {
            // line 105
            echo "                createZone('#formImg', '";
            echo twig_escape_filter($this->env, $this->getContext($context, "i"), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "title", array(), "array"), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "idFieldType", array(), "array"), "html", null, true);
            echo "', ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "x", array(), "array"), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "y", array(), "array"), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "width", array(), "array"), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "zone"), "height", array(), "array"), "html", null, true);
            echo ");
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['zone'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 107
        echo "            

            jQuery('.zoneField').change(function() {
                zoneChange();
            });

            jQuery('#btnAddNewField').click(function(event) {
                event.preventDefault();
                var zone = createZone('#formImg', totalField, '[Edit me]', 'TB', 50, 50, 150, 50);                
                selectZone('zone_' + totalField);
                totalField++;
            });

            jQuery('#zoneDelete').click(function(event) {
                event.preventDefault();

                zoneDelete();
            });

            jQuery('.submit').click(function(event) {
                var imgElement = jQuery('#formImg');                

                var zoneValues = [];
                for (var i = 0; i < zones.length; i++) {
                    if (zones[i].css('display') == 'block') {
                        zoneValues.push({
                            x: zones[i].offset().left - imgElement.offset().left,
                            y: zones[i].offset().top - imgElement.offset().top,
                            width: zones[i].width(),
                            height: zones[i].height(),
                            title: zones[i].attr('data-zone-title'), 
                            type: zones[i].attr('data-zone-type')
                        });
                    }
                }
                jQuery('#";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "zones"), "vars"), "id"), "html", null, true);
        echo "').val(JSON.stringify(zoneValues));
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:app_scan_form_editor.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  139 => 104,  194 => 16,  111 => 96,  149 => 46,  92 => 80,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 38,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 14,  154 => 12,  143 => 105,  128 => 110,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 5,  99 => 27,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 6,  82 => 23,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 30,  96 => 27,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 32,  66 => 13,  56 => 12,  54 => 15,  49 => 17,  41 => 6,  34 => 10,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 63,  169 => 32,  164 => 107,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 44,  137 => 39,  135 => 18,  129 => 60,  127 => 38,  119 => 48,  116 => 56,  110 => 31,  104 => 44,  101 => 28,  97 => 26,  88 => 25,  64 => 99,  60 => 26,  36 => 12,  23 => 2,  21 => 1,  85 => 24,  78 => 22,  52 => 49,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 51,  150 => 69,  145 => 22,  138 => 36,  134 => 102,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 24,  80 => 71,  74 => 39,  55 => 15,  50 => 24,  44 => 43,  39 => 10,  33 => 4,  26 => 2,  68 => 19,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 13,  35 => 6,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 57,  59 => 16,  57 => 10,  53 => 18,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 8,  30 => 4,  27 => 3,);
    }
}
