<?php

/* DesignStarlightBundle:Form:Filter.html.twig */
class __TwigTemplate_0cf4c4c335a61e7198eeba37ac300196 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'messagesBox' => array($this, 'block_messagesBox'),
            'content' => array($this, 'block_content'),
            'buttons' => array($this, 'block_buttons'),
            'extraJavaScript' => array($this, 'block_extraJavaScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), array(0 => "DesignStarlightBundle:Form:BasicTheme.html.twig"));
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('messagesBox', $context, $blocks);
        // line 5
        echo "
";
        // line 6
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "title", array(), "array", true, true)) {
            // line 7
            echo "<div class=\"contenttitle\">
    <h2 class=\"form\"><span>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "title", array(), "array"), "html", null, true);
            echo "</span></h2>
</div><!--contenttitle-->
";
        }
        // line 11
        echo "
<script type=\"text/javascript\">
    function toggleFilterForm_";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "() {
        var contentDiv = jQuery('#";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_ContentDiv');
        if(contentDiv.is(':visible')) {
            contentDiv.slideUp('fast');
        } else {
            contentDiv.slideDown('fast');
        }

        var toggeBtn = jQuery('#";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_toggerButtonDiv');
        var btnDiv = jQuery('#";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submitButtonDiv');
        var clearDiv = jQuery('#";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_clearButtonDiv');
        if(btnDiv.is(':visible')) {
            btnDiv.css('display', 'none');
        } else {
            btnDiv.css('display', '');
        }    

        if(clearDiv.is(':visible')) {
            clearDiv.css('display', 'none');
        } else {
            clearDiv.css('display', '');
        }        

        return false;
    }
</script>

<form id=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo " style=\"margin-top: -1px\">
    <input type=\"hidden\" id=\"__clear_all\" name=\"__clear_all\" value=\"0\" />
    ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 43
            echo "        ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 44
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
        ";
            }
            // line 46
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 47
        echo "
    <div id=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_ContentDiv\" ";
        if (($this->getAttribute($this->getContext($context, "form"), "visiblity", array(), "array") == "hide")) {
            echo "style=\"display: none\"";
        }
        echo ">
        ";
        // line 49
        $this->displayBlock('content', $context, $blocks);
        // line 79
        echo "    </div>
    <table class=\"display_two_column stdform stdformfilter\" style='margin-top: -1px'>        
        <tr>
            <td colspan=\"4\">
                ";
        // line 83
        $this->displayBlock('buttons', $context, $blocks);
        // line 88
        echo "            </td>
        </tr>
    </table>

    <script type=\"text/javascript\">
        ";
        // line 93
        $this->displayBlock('extraJavaScript', $context, $blocks);
        // line 110
        echo "    </script>
</form>";
    }

    // line 3
    public function block_messagesBox($context, array $blocks = array())
    {
    }

    // line 49
    public function block_content($context, array $blocks = array())
    {
        // line 50
        echo "        <table class=\"display_two_column stdform stdformfilter\">        
            ";
        // line 51
        $context["count"] = 0;
        // line 52
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 53
            echo "                ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 54
                echo "                    ";
                if ((($this->getContext($context, "count") % 2) == 0)) {
                    // line 55
                    echo "                        <tr>
                    ";
                }
                // line 57
                echo "                        
                    <td>";
                // line 58
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'label');
                echo "</td>
                    <td>
                        ";
                // line 60
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                        <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                            ";
                // line 62
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'errors');
                echo "
                        </label>
                    </td>
                    
                    ";
                // line 66
                if ((($this->getContext($context, "count") % 2) == 1)) {
                    // line 67
                    echo "                        </tr>
                    ";
                }
                // line 69
                echo "
                    ";
                // line 70
                $context["count"] = ($this->getContext($context, "count") + 1);
                // line 71
                echo "                ";
            }
            // line 72
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo "   

            ";
        // line 74
        if ((($this->getContext($context, "count") % 2) == 1)) {
            // line 75
            echo "                <td></td><td></td></tr>
            ";
        }
        // line 76
        echo "        
        </table>
        ";
    }

    // line 83
    public function block_buttons($context, array $blocks = array())
    {
        // line 84
        echo "                <button id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_toggerButtonDiv\" class=\"radius2\" onclick=\"javascript:toggleFilterForm_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "(); return false;\">&nbsp;&nbsp;Toggle filter&nbsp;&nbsp;</button>            
                <button id=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submitButtonDiv\" ";
        if (($this->getAttribute($this->getContext($context, "form"), "visiblity", array(), "array") == "hide")) {
            echo "style=\"display: none\"";
        }
        echo " class=\"submit radius2\">&nbsp;&nbsp;";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "submitLabel", array(), "array"), "html", null, true);
        echo "&nbsp;&nbsp;</button>
                <button id=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_clearButtonDiv\" ";
        if (($this->getAttribute($this->getContext($context, "form"), "visiblity", array(), "array") == "hide")) {
            echo "style=\"display: none\"";
        }
        echo " class=\"submit radius2\">&nbsp;&nbsp;Clear filter&nbsp;&nbsp;</button>            
                ";
    }

    // line 93
    public function block_extraJavaScript($context, array $blocks = array())
    {
        // line 94
        echo "            jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_clearButtonDiv').click(function() {
                jQuery('#__clear_all').val(1);
                return true;
            });

            jQuery(document).ready(function() {
                jQuery(\"#";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\").keypress(function (e) {
                    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                        jQuery('#";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submitButtonDiv').click();
                        return false;
                    } else {
                        return true;
                    }
                });
            });
        ";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Form:Filter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 93,  254 => 86,  244 => 85,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 71,  208 => 70,  201 => 67,  199 => 66,  187 => 60,  182 => 58,  179 => 57,  175 => 55,  172 => 54,  169 => 53,  164 => 52,  162 => 51,  159 => 50,  156 => 49,  151 => 3,  146 => 110,  144 => 93,  137 => 88,  135 => 83,  129 => 79,  127 => 49,  119 => 48,  116 => 47,  110 => 46,  104 => 44,  101 => 43,  97 => 42,  88 => 40,  64 => 22,  60 => 21,  36 => 8,  23 => 2,  21 => 1,  85 => 16,  78 => 14,  52 => 8,  24 => 3,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 114,  315 => 140,  313 => 126,  310 => 125,  308 => 114,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 102,  279 => 93,  277 => 100,  271 => 89,  267 => 94,  263 => 86,  260 => 85,  256 => 70,  245 => 67,  242 => 66,  237 => 84,  234 => 83,  225 => 103,  223 => 85,  207 => 71,  205 => 69,  197 => 59,  192 => 62,  189 => 55,  183 => 144,  181 => 110,  178 => 109,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 47,  157 => 46,  150 => 42,  145 => 40,  138 => 36,  134 => 35,  130 => 34,  126 => 33,  121 => 31,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 12,  55 => 9,  50 => 14,  44 => 49,  39 => 45,  33 => 7,  26 => 3,  68 => 23,  61 => 7,  58 => 8,  48 => 51,  46 => 13,  42 => 11,  35 => 6,  31 => 6,  28 => 5,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 10,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 5,  40 => 9,  37 => 7,  30 => 4,  27 => 3,);
    }
}
