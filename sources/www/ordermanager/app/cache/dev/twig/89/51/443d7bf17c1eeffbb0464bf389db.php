<?php

/* DesignStarlightBundle:Component:SideMenu.html.twig */
class __TwigTemplate_8951443d7bf17c1eeffbb0464bf389db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "menu"), "content", array(), "array"));
        foreach ($context['_seq'] as $context["cMenuId"] => $context["cMenu"]) {
            // line 3
            echo "        <li";
            if (($this->getContext($context, "cMenuId") == $this->getAttribute($this->getContext($context, "menu"), "selected", array(), "array"))) {
                echo " class=\"current\"";
            }
            echo ">
            <a href=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenu"), "link", array(), "array"), "html", null, true);
            echo "\" ";
            if ($this->getAttribute((isset($context["cMenu"]) ? $context["cMenu"] : null), "class", array(), "array", true, true)) {
                echo " class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenu"), "class", array(), "array"), "html", null, true);
                echo " ";
                if ($this->getAttribute((isset($context["cMenu"]) ? $context["cMenu"] : null), "childs", array(), "array", true, true)) {
                    echo "menudrop";
                }
                echo "\"";
            }
            echo ">
                <span>";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenu"), "title", array(), "array"), "html", null, true);
            echo "</span>
            </a>
            ";
            // line 7
            if ($this->getAttribute((isset($context["cMenu"]) ? $context["cMenu"] : null), "childs", array(), "array", true, true)) {
                // line 8
                echo "                <ul>
                    ";
                // line 9
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "cMenu"), "childs", array(), "array"));
                foreach ($context['_seq'] as $context["childMenuId"] => $context["childMenu"]) {
                    // line 10
                    echo "                        <li><a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "childMenu"), "link", array(), "array"), "html", null, true);
                    echo "\" ";
                    if (($this->getAttribute((isset($context["childMenu"]) ? $context["childMenu"] : null), "newTab", array(), "array", true, true) && ($this->getAttribute($this->getContext($context, "childMenu"), "newTab", array(), "array") == true))) {
                        echo "target=\"_blank\"";
                    }
                    echo "><span>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "childMenu"), "title", array(), "array"), "html", null, true);
                    echo "</span></a></li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['childMenuId'], $context['childMenu'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 12
                echo "                </ul>
            ";
            }
            // line 14
            echo "        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['cMenuId'], $context['cMenu'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 16
        echo "</ul>

";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Component:SideMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 16,  78 => 14,  52 => 8,  24 => 3,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 114,  315 => 140,  313 => 126,  310 => 125,  308 => 114,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 94,  279 => 93,  277 => 92,  271 => 89,  267 => 88,  263 => 86,  260 => 85,  256 => 70,  245 => 67,  242 => 66,  237 => 65,  234 => 64,  225 => 103,  223 => 85,  207 => 71,  205 => 64,  197 => 59,  192 => 56,  189 => 55,  183 => 144,  181 => 110,  178 => 109,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 47,  157 => 46,  150 => 42,  145 => 40,  138 => 36,  134 => 35,  130 => 34,  126 => 33,  121 => 31,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 12,  55 => 9,  50 => 7,  44 => 49,  39 => 45,  33 => 6,  26 => 1,  68 => 11,  61 => 7,  58 => 8,  48 => 51,  46 => 12,  42 => 46,  35 => 6,  31 => 4,  28 => 3,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 10,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 5,  40 => 9,  37 => 7,  30 => 4,  27 => 3,);
    }
}
