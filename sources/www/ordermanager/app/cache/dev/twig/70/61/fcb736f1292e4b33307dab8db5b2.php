<?php

/* LeepAdminBundle:Lead:lead_category_status.html.twig */
class __TwigTemplate_7061fcb736f1292e4b33307dab8db5b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:PopupPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:PopupPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "\t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"content\">
        ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "title"));
        foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
            // line 10
            echo "        \t<h1><span style=\"position: relative; margin-left: 4px; top: -4px; width:200px; color:#ff0000\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "title"), "type", array(), "array"), "html", null, true);
            echo " </span></h1>
        \t<br/>
      \t\t<h4> ";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "title"), "description", array(), "array"), "html", null, true);
            echo " </h4>
      \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 14
        echo "        <br/>

        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable stdform\" style=\"margin-top: 0px\">
          <thead>  
            <tr style=\"heigth:45px\">
                <th class=\"head1\" style=\"width: 15%\">Lead name</th>
                <th class=\"head1\" style=\"width: 8%\">Responsible</th>
                  ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "header"));
        foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
            // line 22
            echo "                    <th class=\"head1\" style=\"width: 10%\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "header"), "name", array(), "array"), "html", null, true);
            echo "</th>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 24
        echo "                  <th class=\"head1\" style=\"width: 10%\"\">Select DC</th>
                  <th class=\"head1\" style=\"width: 10%\"\">TOTAL Orders</th>
                  <th class=\"head1\" style=\"width: 10%\"\">TOTAL Revenue</th>
                  <th class=\"head1\" style=\"width: 5%\"\">Last order</th>
                  <th class=\"head1\" style=\"width: 5%\"\">Rating</th>
            </tr>
          </thead>
          
          <tbody>
          ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "body"));
        foreach ($context['_seq'] as $context["_key"] => $context["body"]) {
            // line 34
            echo "            <tr>
                <td align=\"center\">";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "body"), "leadName", array(), "array"), "html", null, true);
            echo "</td>

                <td style=\"padding: 0px\">
                  <select style=\"height: 100%; width: 100%\">
                  ";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "user"));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 40
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "username", array(), "array"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "username", array(), "array"), "html", null, true);
                echo "</option>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 42
            echo "                  </select>
                </td>

                ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "body"), "leadStatus", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["_body_"]) {
                // line 46
                echo "                ";
                if (($this->getContext($context, "_body_") < 0)) {
                    // line 47
                    echo "                  <td align=\"center\"><span style=\"color: #ff0000\"><b>";
                    echo twig_escape_filter($this->env, $this->getContext($context, "_body_"), "html", null, true);
                    echo "d</b></span></td>
                  ";
                } else {
                    // line 49
                    echo "                    <td style=\"background-color: #9ae8c1\" align=\"center\"><b>";
                    echo twig_escape_filter($this->env, $this->getContext($context, "_body_"), "html", null, true);
                    echo "d<b></td>
                ";
                }
                // line 51
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_body_'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 52
            echo "
                <td style=\"padding: 0px\">
                  <select style=\"height: 100%; width: 100%\">
                  ";
            // line 55
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dc"));
            foreach ($context['_seq'] as $context["_key"] => $context["dc"]) {
                // line 56
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dc"), "name", array(), "array"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dc"), "name", array(), "array"), "html", null, true);
                echo "</option>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dc'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 58
            echo "                  </select>
                </td>

                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['body'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 67
        echo "          </tbody>
        </table>
        
    </div>

<br></br>
<button id='Form_Save' 
    style=\"border:1px solid #333;background: #333;color:#fff;cursor: pointer;padding: 7px 15px; font-weight: bold; font-size: 12px; font-family: sans-serif\">Add new
</button>

";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:lead_category_status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 67,  173 => 58,  162 => 56,  158 => 55,  153 => 52,  147 => 51,  141 => 49,  135 => 47,  132 => 46,  128 => 45,  123 => 42,  112 => 40,  108 => 39,  101 => 35,  98 => 34,  94 => 33,  83 => 24,  74 => 22,  70 => 21,  61 => 14,  53 => 12,  47 => 10,  43 => 9,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}
