<?php

/* LeepAdminBundle:FormTypes:img_template_generator.html.twig */
class __TwigTemplate_b5f2f08d861d945e165567186b463e6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'img_template_generator_widget' => array($this, 'block_img_template_generator_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('img_template_generator_widget', $context, $blocks);
    }

    public function block_img_template_generator_widget($context, array $blocks = array())
    {
        // line 2
        echo "    <input class=\"img_template_generator_input_result\" type=\"text\" name=\"img_template_generator_input_result\" style=\"width: 500px\">
    </br>
    </br>
    Width:&nbsp;&nbsp;<input class=\"img_template_generator_input_width\" type=\"text\" name=\"img_template_generator_input_width\" style=\"width: 80px\">
    &nbsp;&nbsp;Height:&nbsp;&nbsp;<input class=\"img_template_generator_input_height\" type=\"text\" name=\"img_template_generator_input_height\" style=\"width: 80px\">
    </br>
    </br>
    <input class=\"img_template_generator_input_img\" type=\"file\" name=\"img_template_generator_input_img\" style=\"display: none\">
    <input class=\"img_template_generator_input_img_url\" type=\"text\" name=\"img_template_generator_input_img_url\" style=\"display: none\">
    <button class=\"radius2\" onclick=\"onImgTemplateGeneratorUpload(); return false;\">Upload</button>

    <script type=\"text/javascript\">
    function onImgTemplateGeneratorUpload() {
        jQuery(\".img_template_generator_input_img\").click();
    }

    function generateImgTemplate() {
        var url = jQuery('.img_template_generator_input_img_url').first().val().trim();
        if (url) {
            var width = jQuery('.img_template_generator_input_width').first().val().trim();
            var height = jQuery('.img_template_generator_input_height').first().val().trim();

            var src = \"src='\" + url + \"'\";
            var style = \"style='\";
            if (width) {
                if (parseFloat(width) + \"\" == width) {
                    width = width + \"px\";
                }

                style += \"width:\" + width + \";\";
                width = \"width='\" + width + \"'\";
            }

            if (height) {
                if (parseFloat(height) + \"\" == height) {
                    height = height + \"px\";
                }

                style += \" height:\" + height + \";\";
                height = \"height='\" + height + \"'\";
            }

            style += \"'\";

            jQuery('.img_template_generator_input_result').first().val(\"<img \" + src + \" \" + width + \" \" + height + \" \" + style + \">\");
        }
    }

    jQuery(document).ready(function() {
        jQuery('.img_template_generator_input_img').change(function(event) {
            jQuery('.img_template_generator_input_result').first().val(\"Loading... please wait\");
            files = event.target.files;
            var data = new FormData();
            data.append('file', files[0]);

            jQuery.ajax({
                url: '";
        // line 58
        echo twig_escape_filter($this->env, $this->getContext($context, "uploadUrl"), "html", null, true);
        echo "',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR) {
                    if(typeof data.error === 'undefined') {
                        jQuery(\".img_template_generator_input_img_url\").first().val(data.filelink);
                        generateImgTemplate();
                    }
                    else {
                        // Handle errors here
                        console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // Handle errors here
                    console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        });

        jQuery('.img_template_generator_input_img_url').change(generateImgTemplate);
        jQuery('.img_template_generator_input_width').change(generateImgTemplate);
        jQuery('.img_template_generator_input_height').change(generateImgTemplate);
    });
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:img_template_generator.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  82 => 58,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 92,  327 => 90,  317 => 87,  304 => 79,  294 => 75,  289 => 73,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 60,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 42,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 31,  163 => 30,  155 => 27,  152 => 26,  140 => 21,  118 => 14,  115 => 13,  106 => 7,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 142,  76 => 141,  71 => 126,  69 => 112,  66 => 111,  56 => 64,  54 => 11,  49 => 48,  41 => 25,  34 => 13,  29 => 3,  264 => 93,  254 => 61,  244 => 85,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 71,  208 => 70,  201 => 67,  199 => 66,  187 => 60,  182 => 58,  179 => 57,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 50,  156 => 49,  151 => 3,  146 => 110,  144 => 93,  137 => 88,  135 => 18,  129 => 79,  127 => 49,  119 => 48,  116 => 47,  110 => 46,  104 => 44,  101 => 5,  97 => 42,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 16,  78 => 14,  52 => 8,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 81,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 94,  263 => 86,  260 => 85,  256 => 70,  245 => 59,  242 => 66,  237 => 84,  234 => 83,  225 => 103,  223 => 49,  207 => 71,  205 => 69,  197 => 59,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 28,  150 => 42,  145 => 22,  138 => 36,  134 => 35,  130 => 34,  126 => 16,  121 => 15,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 19,  55 => 9,  50 => 10,  44 => 26,  39 => 21,  33 => 4,  26 => 3,  68 => 23,  61 => 98,  58 => 12,  48 => 51,  46 => 47,  42 => 11,  35 => 6,  31 => 12,  28 => 5,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 65,  57 => 30,  53 => 28,  51 => 55,  47 => 13,  45 => 7,  40 => 9,  37 => 5,  30 => 4,  27 => 3,);
    }
}
