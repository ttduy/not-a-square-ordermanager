<?php

/* LeepAdminBundle:Customer:attachment.html.twig */
class __TwigTemplate_b5a5ace582d6c85a850d820f53010afd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <link rel=\"stylesheet\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/ui-themes/base/ui.all.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" title=\"no title\" charset=\"utf-8\">
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/plugins/elfinder.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" title=\"no title\" charset=\"utf-8\">
    <script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/elfinder/jquery-1.4.1.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/elfinder/jquery-ui-1.7.2.custom.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/elfinder/elfinder.full.js"), "html", null, true);
        echo "\"></script>
</head>
<body>
    <div id=\"finder\"></div>

    <script type=\"text/javascript\" charset=\"utf-8\">
        jQuery('#finder').elfinder({
            ";
        // line 14
        if ($this->getContext($context, "allowCloneDir")) {
            // line 15
            echo "                getdirurl: '";
            echo twig_escape_filter($this->env, $this->getContext($context, "processGetAttachmentUrl"), "html", null, true);
            echo "',
                clonedirurl: '";
            // line 16
            echo twig_escape_filter($this->env, $this->getContext($context, "cloneDirUrl"), "html", null, true);
            echo "',
            ";
        }
        // line 18
        echo "            url: '";
        echo twig_escape_filter($this->env, $this->getContext($context, "processAttachmentUrl"), "html", null, true);
        echo "',
            lang: 'en',
            width: 580,
            height:260,
            toolbar: [
                ['reload', 'select'],[";
        // line 23
        if (($this->getContext($context, "mode") == "full")) {
            echo "'mkdir', ";
        }
        echo "'mkfile', 'upload', 'extract'],['icons', 'list'],";
        if ($this->getContext($context, "allowCloneDir")) {
            echo " ['clonedir', 'getdirurl'] ";
        }
        // line 24
        echo "            ],
            contextmenu : {
               cwd : ['reload', 'delim', 'mkfile', 'upload', 'delim', 'paste', 'delim', 'info', 'list'],
               file : ['select', 'open', 'delim', 'copy', 'cut', 'rm', 'delim', 'duplicate', 'rename'],
               group : ['copy', 'cut', 'rm', 'delim', 'archive', 'extract', 'delim', 'info']
            },
            places: \"\"
        });
    </script>
</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:attachment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 24,  68 => 23,  59 => 18,  54 => 16,  49 => 15,  47 => 14,  37 => 7,  33 => 6,  29 => 5,  25 => 4,  21 => 3,  17 => 1,);
    }
}
