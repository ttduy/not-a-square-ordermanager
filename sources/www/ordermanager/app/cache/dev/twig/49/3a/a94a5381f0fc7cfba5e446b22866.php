<?php

/* LeepAdminBundle:Dashboard:list.html.twig */
class __TwigTemplate_493aa94a5381f0fc7cfba5e446b22866 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
            'PendingOrder' => array($this, 'block_PendingOrder'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"widgetgrid\">
        ";
        // line 9
        if (twig_test_empty($this->getContext($context, "widgets"))) {
            // line 10
            echo "            <div class=\"widgetbox\" style=\"width: 100%\">
                <div class=\"title\"><h2 class=\"tabbed\"><span>Welcome</span></h2></div>
                <div class=\"widgetcontent\">
                    Welcome back to order management system!
                </div>
            </div>
        ";
        }
        // line 17
        echo "
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "widgets"));
        foreach ($context['_seq'] as $context["_key"] => $context["widget"]) {
            // line 19
            echo "            <div class=\"widgetbox\" style=\"width: 100%\">
                <div class=\"title\"><h2 class=\"tabbed\"><span>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "name", array(), "array"), "html", null, true);
            echo "</span></h2></div>
                <div class=\"widgetcontent padding0\">
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\">
                        <thead>
                            <tr>
                                <th class=\"head0\" style=\"width: 20%\">Must be status</th>
                                <th class=\"head0\" style=\"width: 20%\">Must not be status</th>
                                <th class=\"head0\" style=\"width: 20%\">Must be product</th>
                                <th class=\"head0\" style=\"width: 20%\">Must be category</th>
                                <th class=\"head0\" style=\"width: 15%\">Last update</th>
                                <th class=\"head0\" style=\"width: 5%\">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "mustBeStatus", array(), "array"), "html", null, true);
            echo "</td>
                                <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "mustNotBeStatus", array(), "array"), "html", null, true);
            echo "</td>
                                <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "mustBeProduct", array(), "array"), "html", null, true);
            echo "</td>
                                <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "mustBeCategory", array(), "array"), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "lastUpdate", array(), "array"), "html", null, true);
            echo "
                                    <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "updateLink", array(), "array"), "html", null, true);
            echo "\"><div class=\"button-checked\" alt=\"Update now\" title=\"Update now\"></div></a>
                                </td>
                                <td>
                                    <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "widget"), "customerLink", array(), "array"), "html", null, true);
            echo "\">
                                        <div class=\"button-detail\" alt=\"See customers\" title=\"See customers\"></div>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\">
                        <colgroup>
                            <col class=\"con0\" width=\"20%\">
                            ";
            // line 54
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "widget"), "calculates", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["calculate"]) {
                // line 55
                echo "                                <col class=\"con0\">
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['calculate'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 57
            echo "                        </colgroup>
                        <thead>
                            <tr>
                                <th class=\"head0\">Period</th>
                                ";
            // line 61
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "widget"), "calculates", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["calculate"]) {
                // line 62
                echo "                                    <th class=\"head0\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "calculate"), "html", null, true);
                echo "</th>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['calculate'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 64
            echo "                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 67
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "widget"), "timePeriods", array(), "array"));
            foreach ($context['_seq'] as $context["idPeriod"] => $context["period"]) {
                // line 68
                echo "                                <tr>
                                    <td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getContext($context, "period"), "html", null, true);
                echo "</td>
                                    ";
                // line 70
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "widget"), "calculates", array(), "array"));
                foreach ($context['_seq'] as $context["idCalculate"] => $context["calculate"]) {
                    // line 71
                    echo "                                        <td>
                                            ";
                    // line 72
                    $context["idResult"] = (($this->getContext($context, "idPeriod") . "_") . $this->getContext($context, "idCalculate"));
                    // line 73
                    echo "                                            ";
                    if ($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "result", array(), "array", false, true), $this->getContext($context, "idResult"), array(), "array", true, true)) {
                        // line 74
                        echo "                                                ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "widget"), "result", array(), "array"), $this->getContext($context, "idResult"), array(), "array"), "html", null, true);
                        echo "
                                            ";
                    }
                    // line 76
                    echo "                                        </td>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['idCalculate'], $context['calculate'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 78
                echo "                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['idPeriod'], $context['period'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 80
            echo "                        </tbody>
                    </table>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['widget'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 85
        echo "
        ";
        // line 86
        $this->displayBlock('PendingOrder', $context, $blocks);
        // line 123
        echo "    </div>
";
    }

    // line 86
    public function block_PendingOrder($context, array $blocks = array())
    {
        // line 87
        echo "            <div class=\"widgetbox\" style=\"width: 100%\">
                <div class=\"title\"><h2 class=\"tabbed\"><span>Pending Order Information</span></h2></div>
                <div class=\"widgetcontent padding0\">
                    <table class=\"stdtable\">
                        <thead>
                            <tr>
                                <th class=\"head0\" style=\"width:20%\">Distribution Channel</th>
                                <th class=\"head0\" style=\"width:20%\">Customer</th>
                                <th class=\"head0\" style=\"width:20%\">Category</th>
                                <th class=\"head0\" style=\"width:20%\">Number Products</th>
                                <th class=\"head0\" style=\"width:20%\">Products</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 101
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pendingOrdersInfo"));
        foreach ($context['_seq'] as $context["_key"] => $context["pendingOrder"]) {
            // line 102
            echo "                                <tr>
                                    <td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "pendingOrder"), "dc", array(), "array"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "pendingOrder"), "customer", array(), "array"), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 106
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "pendingOrder"), "categories", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["categoryName"]) {
                // line 107
                echo "                                            <div>";
                echo twig_escape_filter($this->env, $this->getContext($context, "categoryName"), "html", null, true);
                echo "</div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoryName'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 109
            echo "                                    </td>
                                    <td>";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "pendingOrder"), "numberProducts", array(), "array"), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 112
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "pendingOrder"), "products", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["productName"]) {
                // line 113
                echo "                                            <div>";
                echo twig_escape_filter($this->env, $this->getContext($context, "productName"), "html", null, true);
                echo "</div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productName'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 115
            echo "                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pendingOrder'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 118
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Dashboard:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 118,  290 => 115,  281 => 113,  277 => 112,  272 => 110,  269 => 109,  260 => 107,  256 => 106,  251 => 104,  247 => 103,  244 => 102,  240 => 101,  224 => 87,  221 => 86,  216 => 123,  214 => 86,  211 => 85,  201 => 80,  194 => 78,  187 => 76,  181 => 74,  178 => 73,  176 => 72,  173 => 71,  169 => 70,  165 => 69,  162 => 68,  158 => 67,  153 => 64,  144 => 62,  140 => 61,  134 => 57,  127 => 55,  123 => 54,  110 => 44,  104 => 41,  100 => 40,  95 => 38,  91 => 37,  87 => 36,  83 => 35,  65 => 20,  62 => 19,  58 => 18,  55 => 17,  46 => 10,  44 => 9,  41 => 8,  38 => 7,  31 => 4,  28 => 3,);
    }
}
