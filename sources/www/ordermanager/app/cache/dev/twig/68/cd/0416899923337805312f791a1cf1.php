<?php

/* DesignStarlightBundle:Form:BasicTheme.html.twig */
class __TwigTemplate_68cd0416899923337805312f791a1cf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_row' => array($this, 'block_field_row'),
            'field_errors' => array($this, 'block_field_errors'),
            'section_row' => array($this, 'block_section_row'),
            'form_label' => array($this, 'block_form_label'),
            'label_widget' => array($this, 'block_label_widget'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'choice_row' => array($this, 'block_choice_row'),
            'searchable_box_widget' => array($this, 'block_searchable_box_widget'),
            'searchable_cache_box_widget' => array($this, 'block_searchable_cache_box_widget'),
            'datepicker_widget' => array($this, 'block_datepicker_widget'),
            'datetimepicker_widget' => array($this, 'block_datetimepicker_widget'),
            'time_period_widget' => array($this, 'block_time_period_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('field_row', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('field_errors', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('section_row', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('form_label', $context, $blocks);
        // line 47
        echo "
";
        // line 48
        $this->displayBlock('label_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 64
        echo "
";
        // line 65
        $this->displayBlock('choice_row', $context, $blocks);
        // line 98
        echo "
";
        // line 99
        $this->displayBlock('searchable_box_widget', $context, $blocks);
        // line 111
        echo "
";
        // line 112
        $this->displayBlock('searchable_cache_box_widget', $context, $blocks);
        // line 126
        echo "
";
        // line 127
        $this->displayBlock('datepicker_widget', $context, $blocks);
        // line 141
        echo "
";
        // line 142
        $this->displayBlock('datetimepicker_widget', $context, $blocks);
        // line 160
        echo "
";
        // line 161
        $this->displayBlock('time_period_widget', $context, $blocks);
    }

    // line 1
    public function block_field_row($context, array $blocks = array())
    {
        // line 2
        echo "    <p id=\"field_row_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\">
        ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
        echo "
        <span class=\"field\">
            ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
            <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
            </label>
        </span>
    </p>
";
    }

    // line 13
    public function block_field_errors($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        if (array_key_exists("errors", $context)) {
            // line 15
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 16
                echo "            ";
                echo $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators");
                echo "<br/>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 18
            echo "    ";
        }
    }

    // line 21
    public function block_section_row($context, array $blocks = array())
    {
        echo "    
    <br/><h2>";
        // line 22
        echo twig_escape_filter($this->env, $this->getContext($context, "label"), "html", null, true);
        echo "</h2><br/>
    <p></p>
";
    }

    // line 26
    public function block_form_label($context, array $blocks = array())
    {
        // line 27
        ob_start();
        // line 28
        echo "    ";
        if ((!$this->getContext($context, "compound"))) {
            // line 29
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id")));
            // line 30
            echo "    ";
        }
        // line 31
        echo "    ";
        if ($this->getContext($context, "required")) {
            // line 32
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class"), "")) : ("")) . " required"))));
            // line 33
            echo "    ";
        }
        // line 34
        echo "    ";
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 35
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
            // line 36
            echo "    ";
        }
        // line 37
        echo "    <label";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo ">
        ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "
        ";
        // line 39
        if ($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "helpText", array(), "array", true, true)) {
            // line 40
            echo "            <small style=\"display: inline-block\">";
            echo $this->getAttribute($this->getContext($context, "label_attr"), "helpText", array(), "array");
            echo "</small>
        ";
        }
        // line 42
        echo "    </label>
    
    
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 48
    public function block_label_widget($context, array $blocks = array())
    {
        // line 49
        echo "    ";
        if ((!twig_test_empty($this->getContext($context, "value")))) {
            // line 50
            echo "        ";
            echo $this->getContext($context, "value");
            echo "&nbsp;
    ";
        } else {
            // line 52
            echo "        &nbsp;
    ";
        }
    }

    // line 56
    public function block_checkbox_widget($context, array $blocks = array())
    {
        // line 57
        ob_start();
        // line 58
        echo "        <input type=\"checkbox\" ";
        // line 59
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            // line 60
            echo "               value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\"";
        }
        if ($this->getContext($context, "checked")) {
            // line 61
            echo "               checked=\"checked\"";
        }
        echo " />
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 65
    public function block_choice_row($context, array $blocks = array())
    {
        // line 66
        echo "    ";
        if ($this->getContext($context, "expanded")) {
            // line 67
            echo "        <p id=\"field_row_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "\">
            ";
            // line 68
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
            echo "
            <span class=\"field\">
                <span ";
            // line 70
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                    ";
            // line 71
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 72
                echo "                        <span>
                            <span style=\"height: 20px\">";
                // line 73
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'widget');
                echo "</span>
                            <span style=\"height: 20px; margin-left: 5px\">
                                ";
                // line 75
                echo $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "vars"), "label");
                echo "                        
                            </span>
                        </span>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 79
            echo "
                    <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                        ";
            // line 81
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
                    </label>
                </span>
            </span>
        </p>
    ";
        } else {
            // line 87
            echo "        <p id=\"field_row_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "\">
            ";
            // line 88
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
            echo "
            <span class=\"field\">
                ";
            // line 90
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
            echo "
                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
            // line 92
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
                </label>
            </span>
        </p>
    ";
        }
        // line 96
        echo "    
";
    }

    // line 99
    public function block_searchable_box_widget($context, array $blocks = array())
    {
        // line 100
        echo "    ";
        $this->displayBlock("choice_widget", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        jQuery(document).ready(function() {
            jQuery('#";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "').select2({
                width: '";
        // line 104
        echo twig_escape_filter($this->env, $this->getContext($context, "width"), "html", null, true);
        echo "',
                allowClear: true,
                dropdownAutoWidth: true
            });
        });
    </script>
";
    }

    // line 112
    public function block_searchable_cache_box_widget($context, array $blocks = array())
    {
        // line 113
        echo "    ";
        $this->displayBlock("field_widget", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        jQuery(document).ready(function() {
            jQuery('#";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "').select2({
                width: '";
        // line 117
        echo twig_escape_filter($this->env, $this->getContext($context, "width"), "html", null, true);
        echo "',
                allowClear: true,
                dropdownAutoWidth: true,
                data: { results: searchableCacheBoxData[\"";
        // line 120
        echo twig_escape_filter($this->env, $this->getContext($context, "dataKey"), "html", null, true);
        echo "\"]},
                formatSelection: searchableCacheBoxResultFormat
            });
        });
    </script>
";
    }

    // line 127
    public function block_datepicker_widget($context, array $blocks = array())
    {
        // line 128
        echo "    ";
        ob_start();
        // line 129
        echo "        ";
        $this->displayBlock("date_widget", $context, $blocks);
        echo " 
        
        <script>
            jQuery(\"#";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\").datepicker({
                showOn: \"both\",
                buttonImage: \"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/images/calendar_small.png"), "html", null, true);
        echo "\",
                buttonImageOnly: true,
                dateFormat: \"";
        // line 136
        echo twig_escape_filter($this->env, $this->getContext($context, "pattern"), "html", null, true);
        echo "\"
            });
        </script>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 142
    public function block_datetimepicker_widget($context, array $blocks = array())
    {
        // line 143
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "hour"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "minute"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "second"), 'widget');
        echo "
    <a style=\"display: inline-block\" href=\"javascript:setCurrentDate('";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "')\">Set current time</a>
    <script type=\"text/javascript\">
    function setCurrentDate(id) {
        var currentDate=new Date();
        var month = currentDate.getMonth() + 1;
        if (month < 10) month = '0' + month;
        var date = currentDate.getDate();
        if (date < 10) date = '0' + date;

        jQuery('#' + id + '_date').val(date + '/' + month + '/' + currentDate.getFullYear());
        jQuery('#' + id + '_hour').val(currentDate.getHours());
        jQuery('#' + id + '_minute').val(currentDate.getMinutes());
        jQuery('#' + id + '_second').val(currentDate.getSeconds());
    }
    </script>
";
    }

    // line 161
    public function block_time_period_widget($context, array $blocks = array())
    {
        // line 162
        echo "    From&nbsp;";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "startHour"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "startMinute"), 'widget');
        echo "&nbsp;
    to&nbsp;";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "endHour"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "endMinute"), 'widget');
        echo "&nbsp;
";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Form:BasicTheme.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 92,  327 => 90,  317 => 87,  304 => 79,  294 => 75,  289 => 73,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 60,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 42,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 31,  163 => 30,  155 => 27,  152 => 26,  140 => 21,  118 => 14,  115 => 13,  106 => 7,  96 => 3,  91 => 2,  84 => 161,  81 => 160,  79 => 142,  76 => 141,  71 => 126,  69 => 112,  66 => 111,  56 => 64,  54 => 56,  49 => 48,  41 => 25,  34 => 13,  29 => 1,  264 => 93,  254 => 61,  244 => 85,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 71,  208 => 70,  201 => 67,  199 => 66,  187 => 60,  182 => 58,  179 => 57,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 50,  156 => 49,  151 => 3,  146 => 110,  144 => 93,  137 => 88,  135 => 18,  129 => 79,  127 => 49,  119 => 48,  116 => 47,  110 => 46,  104 => 44,  101 => 5,  97 => 42,  88 => 1,  64 => 99,  60 => 21,  36 => 20,  23 => 2,  21 => 1,  85 => 16,  78 => 14,  52 => 8,  24 => 3,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 81,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 94,  263 => 86,  260 => 85,  256 => 70,  245 => 59,  242 => 66,  237 => 84,  234 => 83,  225 => 103,  223 => 49,  207 => 71,  205 => 69,  197 => 59,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 28,  150 => 42,  145 => 22,  138 => 36,  134 => 35,  130 => 34,  126 => 16,  121 => 15,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 127,  55 => 9,  50 => 14,  44 => 26,  39 => 21,  33 => 7,  26 => 3,  68 => 23,  61 => 98,  58 => 8,  48 => 51,  46 => 47,  42 => 11,  35 => 6,  31 => 12,  28 => 5,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 65,  57 => 30,  53 => 28,  51 => 55,  47 => 13,  45 => 5,  40 => 9,  37 => 7,  30 => 4,  27 => 3,);
    }
}
