<?php

/* LeepAdminBundle:Bill:app_billing_order_selector.html.twig */
class __TwigTemplate_765a96c6969d6a189e2b2bb590321d98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_billing_order_selector_row' => array($this, 'block_app_billing_order_selector_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_billing_order_selector_row', $context, $blocks);
    }

    public function block_app_billing_order_selector_row($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idTarget"), 'row');
        echo "
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "haveInvoiceAddress"), 'row');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressStreet"), 'row');
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressPostCode"), 'row');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressCity"), 'row');
        echo "
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressIdCountry"), 'row');
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressTelephone"), 'row');
        echo "
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressFax"), 'row');
        echo "
    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "invoiceAddressUid"), 'row');
        echo "
    ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "selectedEntries"), 'row');
        echo "

    ";
        // line 13
        $this->env->loadTemplate("LeepAdminBundle:Bill:app_billing_order_selector.html.twig", "1684608695")->display(array_merge($context, array("grid" => $this->getContext($context, "grid"))));
        // line 35
        echo "
    <script type=\"text/javascript\">
        jQuery(document).ready(function() {
            //jQuery('#field_row_form_orderSelector_idTarget > span').append('<a href=\"javascript:loadBillingData()\">Refresh</a>');
            jQuery('#";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTarget"), "vars"), "id"), "html", null, true);
        echo "').change(function() {
                jQuery('#SaveButton').css('display', 'none');
                loadBillingData();
            });
            onChangeCurrency('#form_currencyRate_idCurrency', true);
        });
        function loadBillingData() {
            // Load target billing data
            var targetUrl = '";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "getTargetBillingData"), "html", null, true);
        echo "?idType=";
        echo twig_escape_filter($this->env, $this->getContext($context, "idType"), "html", null, true);
        echo "&id=' + jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTarget"), "vars"), "id"), "html", null, true);
        echo "').val();
            jQuery.ajax({
                url: targetUrl,
                type: \"POST\",
                context: document.body
            }).done(function(data) {
                if (data == 'FAIL') {
                    alert(\"ERROR!\");
                    return;
                }
                var billingData = JSON.parse(data);

                // Update isWithTax
                var isTaxField = jQuery('#form_tax_isWithTax');
                if (billingData.hasOwnProperty('isWithTax')) {
                    if (billingData['isWithTax']) {
                        if (!isTaxField.attr('checked')) {
                            isTaxField.attr('checked',true);
                            onChangeIsTax('#form_tax_isWithTax');
                        }
                    }
                    else {
                        if (isTaxField.attr('checked'))  {
                            isTaxField.attr('checked',false);
                            onChangeIsTax('#form_tax_isWithTax');
                        }
                    }
                }
                /// udpate invoide address
                if (billingData.length == 0) {
                    jQuery('#form_orderSelector_invoiceAddressStreet').val('');
                    jQuery('#form_orderSelector_invoiceAddressPostCode').val('');
                    jQuery('#form_orderSelector_invoiceAddressCity').val('');
                    jQuery('#form_orderSelector_invoiceAddressIdCountry').val('');
                    jQuery('#form_orderSelector_invoiceAddressTelephone').val('');
                    jQuery('#form_orderSelector_invoiceAddressFax').val(1);
                    jQuery('#form_orderSelector_invoiceAddressUid').val('');
                    jQuery('#form_orderSelector_textAddress').val('');
                }
                if (billingData.hasOwnProperty('isInvoiceAddress')) {
                    jQuery('#form_orderSelector_invoiceAddressStreet').val(billingData['addressStreet']);
                    jQuery('#form_orderSelector_invoiceAddressPostCode').val(billingData['addressPostCode']);
                    jQuery('#form_orderSelector_invoiceAddressCity').val(billingData['addressCity']);
                    jQuery('#form_orderSelector_invoiceAddressIdCountry').val(billingData['addressCountry']);
                    jQuery('#form_orderSelector_invoiceAddressTelephone').val(billingData['addressTelephone']);
                    jQuery('#form_orderSelector_invoiceAddressFax').val(billingData['addressFax']);
                    jQuery('#form_orderSelector_invoiceAddressUid').val(billingData['addressUid']);
                    jQuery('#form_orderSelector_haveInvoiceAddress').val(billingData['haveInvoiceAddress']);
                }
                else {
                    jQuery('#form_orderSelector_invoiceAddressStreet').val('');
                    jQuery('#form_orderSelector_invoiceAddressPostCode').val('');
                    jQuery('#form_orderSelector_invoiceAddressCity').val('');
                    jQuery('#form_orderSelector_invoiceAddressIdCountry').val(1);
                    jQuery('#form_orderSelector_invoiceAddressTelephone').val('');
                    jQuery('#form_orderSelector_invoiceAddressFax').val('');
                    jQuery('#form_orderSelector_invoiceAddressUid').val('');
                    jQuery('#form_orderSelector_haveInvoiceAddress').val(billingData['haveInvoiceAddress']);
                }
                // Update Commission Payment With tax
                var commissionPaymentWithTaxField = jQuery('#form_commissionPaymentWithTax');
                if (billingData.hasOwnProperty('commissionPaymentWithTax')) {
                    if (billingData['commissionPaymentWithTax'] != null) {
                        commissionPaymentWithTaxField.val(billingData['commissionPaymentWithTax']);
                    } else {
                        commissionPaymentWithTaxField.val(0);
                    }
                }

                // Update idNoTaxText
                var idNoTaxTextField = jQuery('#form_noTaxText');
                if (billingData.hasOwnProperty('noTaxText')) {
                    if (idNoTaxTextField) {
                        idNoTaxTextField.val(billingData['noTaxText']);
                    }
                } else {
                        if (idNoTaxTextField) {
                            idNoTaxTextField.val(0);
                    }
                }

                // update idCurrency
                var idCurrencyField = jQuery('#form_currencyRate_idCurrency');
                if (billingData.hasOwnProperty('idCurrency')) {
                    if (idCurrencyField) {
                        idCurrencyField.val(billingData['idCurrency']);
                        onChangeCurrency('#form_currencyRate_idCurrency', true);
                    } else {
                        if (idCurrencyField) {
                            idCurrencyField.val(0);
                            onChangeCurrency('#form_currencyRate_idCurrency', true);
                        }
                    }
                }

                // Update margin payment mode
                var idMarginPaymentMode = jQuery('#form_idMarginPaymentMode');
                if (billingData.hasOwnProperty('idMarginPaymentMode')) {
                    if (idMarginPaymentMode) {
                        idMarginPaymentMode.val(billingData['idMarginPaymentMode']);
                    }
                }
            }).error(function(jqXHR, textStatus, errorThrown ) {
                alert(\"ERROR!\" + textStatus);
            });

            // Reload table
            var customerTable = jQuery('#";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo "');
            var db = customerTable.dataTable();
            db.fnDraw(false);
        };
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Bill:app_billing_order_selector.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 12,  143 => 67,  128 => 55,  125 => 54,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 130,  246 => 122,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 7,  99 => 28,  87 => 22,  83 => 21,  70 => 46,  62 => 26,  43 => 12,  89 => 39,  75 => 30,  72 => 17,  38 => 6,  82 => 34,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 11,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 112,  66 => 13,  56 => 12,  54 => 15,  49 => 8,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 69,  137 => 66,  135 => 18,  129 => 60,  127 => 49,  119 => 48,  116 => 56,  110 => 46,  104 => 44,  101 => 43,  97 => 67,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 47,  78 => 14,  52 => 12,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 85,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 13,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 15,  117 => 48,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 59,  80 => 33,  74 => 39,  55 => 24,  50 => 14,  44 => 17,  39 => 7,  33 => 4,  26 => 2,  68 => 35,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 24,  35 => 11,  31 => 4,  28 => 4,  170 => 83,  165 => 51,  65 => 12,  63 => 18,  59 => 25,  57 => 10,  53 => 9,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}


/* LeepAdminBundle:Bill:app_billing_order_selector.html.twig */
class __TwigTemplate_765a96c6969d6a189e2b2bb590321d98_1684608695 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
            'fnServerParams' => array($this, 'block_fnServerParams'),
            'gridDrawCallback' => array($this, 'block_gridDrawCallback'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_fnServerParams($context, array $blocks = array())
    {
        // line 15
        echo "            var idTarget = jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTarget"), "vars"), "id"), "html", null, true);
        echo "').val();
            aoData.push( { \"name\": \"idTarget\", \"value\": idTarget } );
        ";
    }

    // line 19
    public function block_gridDrawCallback($context, array $blocks = array())
    {
        // line 20
        echo "            var selectedEntries = jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "selectedEntries"), "vars"), "id"), "html", null, true);
        echo "').val();
            var arr = selectedEntries.split(',')
            var i;
            for (i in arr) {
                if (!isNaN(arr[i])) {
                    jQuery('#row_' + arr[i]).parents('span').addClass('checked');
                    jQuery('#row_' + arr[i]).parents('tr').addClass('selected');
                    jQuery('#row_' + arr[i]).attr('checked', 'checked');
                }
            }

            jQuery('#SaveButton').css('display', 'inline-block');
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Bill:app_billing_order_selector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 12,  143 => 67,  128 => 55,  125 => 54,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 7,  99 => 28,  87 => 22,  83 => 21,  70 => 46,  62 => 26,  43 => 12,  89 => 39,  75 => 30,  72 => 17,  38 => 6,  82 => 34,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 11,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 112,  66 => 13,  56 => 12,  54 => 15,  49 => 8,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 69,  137 => 66,  135 => 18,  129 => 60,  127 => 49,  119 => 48,  116 => 56,  110 => 46,  104 => 44,  101 => 43,  97 => 67,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 47,  78 => 14,  52 => 12,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 13,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 15,  117 => 48,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 59,  80 => 33,  74 => 39,  55 => 24,  50 => 14,  44 => 17,  39 => 7,  33 => 4,  26 => 2,  68 => 35,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 24,  35 => 11,  31 => 4,  28 => 4,  170 => 83,  165 => 51,  65 => 12,  63 => 18,  59 => 25,  57 => 10,  53 => 9,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}
