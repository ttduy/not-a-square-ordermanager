<?php

/* DesignStarlightBundle:Form:Basic.html.twig */
class __TwigTemplate_ec558d4abaddf43269649844a015a092 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'formContent' => array($this, 'block_formContent'),
            'formContentNormal' => array($this, 'block_formContentNormal'),
            'formContentHidden' => array($this, 'block_formContentHidden'),
            'buttons' => array($this, 'block_buttons'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), array(0 => "DesignStarlightBundle:Form:BasicTheme.html.twig"));
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 6
            echo "    ";
            $this->env->loadTemplate("DesignStarlightBundle:Component:Message.html.twig")->display(array_merge($context, array("message" => $this->getContext($context, "message"), "type" => "info")));
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "errors", array(), "array"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 9
            echo "    ";
            $this->env->loadTemplate("DesignStarlightBundle:Component:Message.html.twig")->display(array_merge($context, array("message" => $this->getContext($context, "message"), "type" => "error")));
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 11
        echo "

";
        // line 13
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "title", array(), "array")))) {
            // line 14
            echo "<div class=\"contenttitle\">
    <h2 class=\"form\"><span>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "title", array(), "array"), "html", null, true);
            echo "</span></h2>
</div><!--contenttitle-->
";
        }
        // line 18
        echo "
<form id=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" class=\"stdform stdform2\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo " style=\"margin-top: -1px\">
    ";
        // line 20
        $this->displayBlock('formContent', $context, $blocks);
        // line 37
        echo "
    <p class=\"stdformbutton\">
        ";
        // line 39
        $this->displayBlock('buttons', $context, $blocks);
        // line 42
        echo "    </p>
</form>
";
    }

    // line 20
    public function block_formContent($context, array $blocks = array())
    {
        // line 21
        echo "        ";
        $this->displayBlock('formContentNormal', $context, $blocks);
        // line 28
        echo "
        ";
        // line 29
        $this->displayBlock('formContentHidden', $context, $blocks);
        // line 36
        echo "    ";
    }

    // line 21
    public function block_formContentNormal($context, array $blocks = array())
    {
        // line 22
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 23
            echo "                ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 24
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'row');
                echo "
                ";
            }
            // line 26
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 27
        echo "        ";
    }

    // line 29
    public function block_formContentHidden($context, array $blocks = array())
    {
        // line 30
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 31
            echo "                ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 32
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                ";
            }
            // line 34
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 35
        echo "        ";
    }

    // line 39
    public function block_buttons($context, array $blocks = array())
    {
        // line 40
        echo "        <button class=\"submit radius2\">&nbsp;&nbsp;";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "submitLabel", array(), "array"), "html", null, true);
        echo "&nbsp;&nbsp;</button>
        ";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Form:Basic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 40,  196 => 39,  192 => 35,  186 => 34,  180 => 32,  177 => 31,  172 => 30,  169 => 29,  165 => 27,  159 => 26,  153 => 24,  150 => 23,  145 => 22,  142 => 21,  138 => 36,  136 => 29,  133 => 28,  130 => 21,  127 => 20,  121 => 42,  119 => 39,  115 => 37,  113 => 20,  105 => 19,  102 => 18,  96 => 15,  93 => 14,  91 => 13,  87 => 11,  72 => 9,  55 => 8,  40 => 6,  23 => 5,  21 => 1,  107 => 60,  78 => 34,  62 => 21,  54 => 16,  48 => 13,  43 => 11,  37 => 8,  32 => 5,  29 => 3,  26 => 2,);
    }
}
