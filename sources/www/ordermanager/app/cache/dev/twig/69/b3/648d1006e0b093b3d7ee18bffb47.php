<?php

/* DesignStarlightBundle:Component:SearchableCacheBoxData.html.twig */
class __TwigTemplate_69b3648d1006e0b093b3d7ee18bffb47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    var searchableCacheBoxData = JSON.parse('";
        // line 2
        echo ((array_key_exists("cacheBoxData", $context)) ? (_twig_default_filter($this->getContext($context, "cacheBoxData"), "[]")) : ("[]"));
        echo "');
</script>
";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Component:SearchableCacheBoxData.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 114,  315 => 140,  313 => 126,  310 => 125,  308 => 114,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 94,  279 => 93,  277 => 92,  271 => 89,  267 => 88,  263 => 86,  260 => 85,  256 => 70,  245 => 67,  242 => 66,  237 => 65,  234 => 64,  225 => 103,  223 => 85,  207 => 71,  205 => 64,  197 => 59,  192 => 56,  189 => 55,  183 => 144,  181 => 110,  178 => 109,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 47,  157 => 46,  150 => 42,  145 => 40,  138 => 36,  134 => 35,  130 => 34,  126 => 33,  121 => 31,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 14,  55 => 7,  50 => 147,  44 => 49,  39 => 45,  33 => 6,  26 => 1,  68 => 11,  61 => 7,  58 => 8,  48 => 51,  46 => 12,  42 => 46,  35 => 6,  31 => 4,  28 => 3,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 9,  37 => 7,  30 => 4,  27 => 3,);
    }
}
