<?php

/* LeepAdminBundle:Customer:list.html.twig */
class __TwigTemplate_6bc719669f0938f9c9f0e9507ea6107c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/sweet-alert.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 11
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:Customer:list.html.twig", "87903301")->display(array_merge($context, array("grid" => $this->getContext($context, "grid"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:Customer:list.html.twig */
class __TwigTemplate_6bc719669f0938f9c9f0e9507ea6107c_87903301 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
            'extraScriptOnReady' => array($this, 'block_extraScriptOnReady'),
            'gridDrawCallback' => array($this, 'block_gridDrawCallback'),
            'filterForm' => array($this, 'block_filterForm'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_extraScriptOnReady($context, array $blocks = array())
    {
        // line 14
        echo "            dynamicTables_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo ".fnSort( [ [0,'desc'] ] );
        ";
    }

    // line 17
    public function block_gridDrawCallback($context, array $blocks = array())
    {
        // line 18
        echo "            jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " tr').each(function() {
                var colorCode = jQuery(this).find('.customer_color').attr('color-code');
                if (colorCode != 'undefined') {
                    jQuery(this).css('background-color', colorCode);
                }
            });

            jQuery(\".simpletip\").tooltip({
            
                bodyHandler: function() {
                    return jQuery(this).find('.hide-content').html();
                },
                left: -100,
                top: 20,
                showURL: false
            });
        ";
    }

    // line 36
    public function block_filterForm($context, array $blocks = array())
    {
        // line 37
        echo "            ";
        if ($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filter", array(), "array", true, true)) {
            // line 38
            echo "                ";
            $this->env->loadTemplate("LeepAdminBundle:Customer:list.html.twig", "29309388")->display(array_merge($context, array("form" => $this->getAttribute($this->getContext($context, "grid"), "filter", array(), "array"))));
            // line 127
            echo "            ";
        }
        // line 128
        echo "        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 128,  148 => 127,  145 => 38,  142 => 37,  139 => 36,  117 => 18,  114 => 17,  107 => 14,  104 => 13,  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:Customer:list.html.twig */
class __TwigTemplate_6bc719669f0938f9c9f0e9507ea6107c_29309388 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Filter.html.twig");

        $this->blocks = array(
            'buttons' => array($this, 'block_buttons'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Filter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 39
    public function block_buttons($context, array $blocks = array())
    {
        // line 40
        echo "                        ";
        $this->displayParentBlock("buttons", $context, $blocks);
        echo "
                        <div style=\"float: right\">
                            ";
        // line 42
        if ($this->getContext($context, "customerModifyPermission")) {
            // line 43
            echo "                                <button class=\"radius2\" onclick=\"return exportCustomerList()\">&nbsp;&nbsp;Export&nbsp;&nbsp;</button>
                                <button class=\"radius2\" onclick=\"return customerStatusBulkEdit()\">&nbsp;&nbsp;Bulk-edit&nbsp;&nbsp;</button>
                                <button class=\"radius2\" onclick=\"return customerSettingBulkEdit()\">&nbsp;&nbsp;Bulk-edit-setting&nbsp;&nbsp;</button>
                                <button class=\"radius2\" onclick=\"return customerCheckDuplication()\">&nbsp;&nbsp;Check duplication&nbsp;&nbsp;</button>
                            ";
        }
        // line 48
        echo "                        </div>
                        <script type=\"text/javascript\">
                            function customerCheckDuplication() {
                                var checkboxes = jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " .stdtableCheckBox');
                                var selectedId = '';
                                checkboxes.each(function(i, v) {
                                    if (jQuery(this).is(':checked')) {
                                        var id = jQuery(this).attr('id').substring(4);
                                        selectedId += id + ',';
                                    }
                                });
                                var targetUrl = '";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "customer", array(), "array"), "checkDuplication", array(), "array"), "html", null, true);
        echo "?selectedId=' + selectedId;
                                jQuery.colorbox({
                                    href: targetUrl,
                                    iframe: true,
                                    width:  \"90%\",
                                    height: \"90%\",
                                    onClosed:function(){
                                        funcDynamicTableReload_";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo "();
                                    }
                                });

                                return false;
                            }

                            function customerSettingBulkEdit() {
                                var checkboxes = jQuery('#";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " .stdtableCheckBox');
                                var selectedId = '';
                                checkboxes.each(function(i, v) {
                                    if (jQuery(this).is(':checked')) {
                                        var id = jQuery(this).attr('id').substring(4);
                                        selectedId += id + ',';
                                    }
                                });
                                var targetUrl = '";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "customer", array(), "array"), "bulkEditSetting", array(), "array"), "html", null, true);
        echo "?selectedId=' + selectedId;
                                jQuery.colorbox({
                                    href: targetUrl,
                                    iframe: true,
                                    width:  \"90%\",
                                    height: \"90%\",
                                    onClosed:function(){
                                        funcDynamicTableReload_";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo "();
                                    }
                                });

                                return false;
                            }

                            function customerStatusBulkEdit() {
                                var checkboxes = jQuery('#";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " .stdtableCheckBox');
                                var selectedId = '';
                                checkboxes.each(function(i, v) {
                                    if (jQuery(this).is(':checked')) {
                                        var id = jQuery(this).attr('id').substring(4);
                                        selectedId += id + ',';
                                    }
                                });
                                var targetUrl = '";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "customer", array(), "array"), "bulkEditStatus", array(), "array"), "html", null, true);
        echo "?selectedId=' + selectedId;
                                jQuery.colorbox({
                                    href: targetUrl,
                                    iframe: true,
                                    width:  \"90%\",
                                    height: \"90%\",
                                    onClosed:function(){
                                        funcDynamicTableReload_";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo "();
                                    }
                                });

                                return false;
                            }

                            function exportCustomerList() {
                                var url = '";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "customer", array(), "array"), "exportCustomers", array(), "array"), "html", null, true);
        echo "';
                                window.open(url, '_blank');
                                return false;
                            }
                        </script>
                    ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  315 => 120,  304 => 112,  294 => 105,  283 => 97,  272 => 89,  262 => 82,  251 => 74,  240 => 66,  230 => 59,  219 => 51,  214 => 48,  207 => 43,  205 => 42,  199 => 40,  196 => 39,  151 => 128,  148 => 127,  145 => 38,  142 => 37,  139 => 36,  117 => 18,  114 => 17,  107 => 14,  104 => 13,  56 => 12,  53 => 11,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
