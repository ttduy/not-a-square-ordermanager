<?php

/* LeepAdminBundle:Plate:app_plate_box_selector.html.twig */
class __TwigTemplate_215be560fb56b10e94d12d186c6ddea8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_plate_box_selector_row' => array($this, 'block_app_plate_box_selector_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_plate_box_selector_row', $context, $blocks);
    }

    public function block_app_plate_box_selector_row($context, array $blocks = array())
    {
        // line 2
        echo "    <p>
        <table id=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_wellTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\">
            <colgroup>
                ";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 13));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 6
            echo "                    <col class=\"con0\" />
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "            </colgroup>
            <thead>
                <tr>
                    <th class=\"head1\" colspan=\"13\" style=\"text-align: left\">PLATE: ";
        // line 11
        echo twig_escape_filter($this->env, $this->getContext($context, "plateName"), "html", null, true);
        echo "</th>
                </tr>
                <tr>
                    <th class=\"head1\" width=\"4%\"></th>
                    ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 16
            echo "                        <th class=\"head1\" width=\"8%\" onclick=\"javascript:clickAppPlateBoxSelectorColumn('";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "', ";
            echo twig_escape_filter($this->env, $this->getContext($context, "i"), "html", null, true);
            echo ")\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "i"), "html", null, true);
            echo "</th>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 18
        echo "                </tr>
            </thead>

            <tbody>
                ";
        // line 22
        $context["m"] = array(0 => "A", 1 => "B", 2 => "C", 3 => "D", 4 => "E", 5 => "F", 6 => "G", 7 => "H");
        // line 23
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 24
            echo "                    <tr>
                        <td class=\"head1\" style=\"background-color: #ddd; text-align: center; font-weight: bold\" width=\"4%\">";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "m"), $this->getContext($context, "r"), array(), "array"), "html", null, true);
            echo "</td>
                        ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, 11));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 27
                echo "                            <td col-id=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "c"), "html", null, true);
                echo "\" height=\"40px\" style=\"text-align: center; font-size: 11px\" width=\"8%\">
                                ";
                // line 28
                echo (($this->getAttribute($this->getAttribute((isset($context["wellData"]) ? $context["wellData"] : null), $this->getContext($context, "r"), array(), "array", false, true), $this->getContext($context, "c"), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["wellData"]) ? $context["wellData"] : null), $this->getContext($context, "r"), array(), "array", false, true), $this->getContext($context, "c"), array(), "array"), "")) : (""));
                echo "
                            </td>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 31
            echo "                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 33
        echo "            </tbody>
        </table>
        ";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 36
            echo "            ";
            $context["columnSelectedName"] = ("columnSelect_" . $this->getContext($context, "i"));
            // line 37
            echo "            ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "getChild", array(0 => $this->getContext($context, "columnSelectedName")), "method"), 'widget');
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 39
        echo "
        <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                var i;
                for (i = 0; i < 12; i++) {
                    var columnSelect = jQuery('#";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_columnSelect_' + i).val();
                    if (columnSelect == 1) {
                        clickAppPlateBoxSelectorColumn('";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "', i);
                    }
                }
            });
        </script>
    </p>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Plate:app_plate_box_selector.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  149 => 46,  92 => 26,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 38,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 12,  143 => 67,  128 => 37,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 5,  99 => 27,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 6,  82 => 23,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 30,  96 => 27,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 13,  66 => 13,  56 => 12,  54 => 15,  49 => 12,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 44,  137 => 39,  135 => 18,  129 => 60,  127 => 38,  119 => 48,  116 => 56,  110 => 31,  104 => 44,  101 => 28,  97 => 26,  88 => 25,  64 => 99,  60 => 21,  36 => 6,  23 => 2,  21 => 1,  85 => 24,  78 => 22,  52 => 12,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 48,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 24,  80 => 23,  74 => 39,  55 => 15,  50 => 14,  44 => 17,  39 => 7,  33 => 4,  26 => 2,  68 => 19,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 24,  35 => 11,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 18,  59 => 16,  57 => 10,  53 => 13,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}
