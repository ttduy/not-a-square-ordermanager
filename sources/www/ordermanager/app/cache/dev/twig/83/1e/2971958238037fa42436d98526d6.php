<?php

/* LeepAdminBundle:FormTypes:app_text_tool.html.twig */
class __TwigTemplate_831e2971958238037fa42436d98526d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_text_tool_widget' => array($this, 'block_app_text_tool_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_text_tool_widget', $context, $blocks);
    }

    public function block_app_text_tool_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "sortOrder"), 'widget');
        echo "
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), 'widget');
        echo "
    <br/>

    <!-- IMAGE -->
    <span id=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolImageAcrossPage\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolImageAcrossPage", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Image:</text>";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageAcrossPage"), "image"), 'widget');
        echo "</span>
    </span>
    <span id=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolImageHeading\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolImageHeading", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Image:</text>";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "image"), 'widget');
        echo " </span>
        <span><text>Overall height:</text>";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "overallHeight"), 'widget');
        echo " </span>
        <span><text>Text Block:</text>";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "textBlock"), 'widget');
        echo " </span>
        <span><text>Box color:</text>";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "boxColor"), 'widget');
        echo " </span>
        <span><text>Alignment:</text>";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "alignment"), 'widget');
        echo " </span>
        <span><text>Font color:</text>";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "fontColor"), 'widget');
        echo " </span>
        <span><text>Font size:</text>";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Top:</text>";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "marginTop"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Left:</text>";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "marginLeft"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Right:</text>";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageHeading"), "marginRight"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolImageTitlePageLogo\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolImageTitlePageLogo", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Image:</text>";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageTitlePageLogo"), "image"), 'widget');
        echo " </span>
        <span><text>Box color:</text>";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageTitlePageLogo"), "boxColor"), 'widget');
        echo " </span>
        <span><text>Logo:</text>";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageTitlePageLogo"), "logo"), 'widget');
        echo " </span>
        <span><text>Override logo path:</text>";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageTitlePageLogo"), "overrideLogo"), 'widget');
        echo " </span><br/>
        <span><text>Alignment:</text>";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolImageTitlePageLogo"), "alignment"), 'widget');
        echo " </span>
    </span>

    <!-- TEXT -->
    <span id=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextBarcode\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextBarcode", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Text Block:</text>";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "textBlock"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Alignment:</text>";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "alignment"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font:</text>";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "effects"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Code type:</text>";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextBarcode"), "idCodeType"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextPlain\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextPlain", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Text Block:</text>";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "textBlock"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Alignment:</text>";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "alignment"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font:</text>";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "effects"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextPlain"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextHtml\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextHtml", array(), "array"), "html", null, true);
        echo "\">
        <span><text>HTML:</text>";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextHtml"), "textBlock"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextList\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextList", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Text Block:</text>";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "textBlock"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font:</text>";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "effects"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Bullet color:</text>";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "bulletColor"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextList"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextColorTextbox\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextColorTextbox", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Text Block:</text>";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "textBlock"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Alignment:</text>";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "alignment"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font:</text>";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "effects"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Box size:</text>";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "boxSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Box color:</text>";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "boxColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Box background color:</text>";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "boxBackgroundColor"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextColorTextbox"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolTextFloatingPlain\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolTextFloatingPlain", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Text Block:</text>";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "textBlock"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Alignment:</text>";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "alignment"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\">";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "alignmentVertical"), 'widget');
        echo "</span>
        <span style=\"width: 240px; display: inline-block\"><text>Font:</text>";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "effects"), 'widget');
        echo " </span> <br/>
        <span style=\"width: 240px; display: inline-block\"><text>Pos X:</text>";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "posX"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Pos Y:</text>";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "posY"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Width:</text>";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "textWidth"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Height:</text>";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "textHeight"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Background color:</text>";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "backgroundColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Border color:</text>";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "borderColor"), 'widget');
        echo " </span><br/>
        <span style=\"width: 240px; display: inline-block\"><text>Type:</text>";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "idType"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Visibility:</text>";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "visibility"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Rotation:</text>";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolTextFloatingPlain"), "rotation"), 'widget');
        echo " </span>
    </span>

    <!-- STRUCTURE -->
    <span id=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureSpacer\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureSpacer", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Height:</text>";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureSpacer"), "height"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureSpacer"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureHorizontalLine\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureHorizontalLine", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 220px; display: inline-block\"><text>Font Color:</text>";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureHorizontalLine"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Line Height:</text>";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureHorizontalLine"), "lineHeight"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Top:</text>";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureHorizontalLine"), "marginTop"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Bottom:</text>";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureHorizontalLine"), "marginBottom"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureHorizontalLine"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureChangeLayout\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureChangeLayout", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Layout:</text>";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeLayout"), "idLayout"), 'widget');
        echo " </span>
        <span style=\"width: 400px; display: inline-block\"><text>Custom layout:</text>";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeLayout"), "customLayout"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureBreak\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureBreak", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Break:</text>";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBreak"), "idBreak"), 'widget');
        echo " </span>
        <span style=\"width: 400px; display: inline-block\"><text>Conditional:</text>";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBreak"), "conditional"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBreak"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructurePageLayout\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructurePageLayout", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 220px; display: inline-block\"><text>Margin Top:</text>";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageLayout"), "marginTop"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Bottom:</text>";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageLayout"), "marginBottom"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Left:</text>";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageLayout"), "marginLeft"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Margin Right:</text>";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageLayout"), "marginRight"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructurePageFooter\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructurePageFooter", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 220px; display: inline-block\"><text>Line color:</text>";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "lineColor"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Text \"Page\":</text>";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "textPage"), 'widget');
        echo " </span>
        <span style=\"width: 220px; display: inline-block\"><text>Text \"Of:</text>";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "textOf"), 'widget');
        echo " </span><br/>
        <span style=\"display: inline-block\"><text>Logo:</text>";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "logo"), 'widget');
        echo " </span> <br/>
        <span><text>Override logo path:</text>";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "overrideLogo"), 'widget');
        echo " </span><br/>
        <span style=\"display: inline-block\"><text>Order number:</text>";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "orderNumber"), 'widget');
        echo " </span>
        <span style=\"display: inline-block\"><text>Is hide footer?:</text>";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "isBreak"), 'widget');
        echo " </span>
        <span style=\"display: inline-block\"><text>Correct page group number?:</text>";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooter"), "isCorrectPageGroup"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructurePageFooterSimplified\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructurePageFooterSimplified", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Config:</text>";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructurePageFooterSimplified"), "config"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureChangeFormat\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureChangeFormat", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Format:</text>";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeFormat"), "idFormat"), 'widget');
        echo " </span><br/>
        <span style=\"width: 400px; display: inline-block\"><text>Orientation:</text>";
        // line 129
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeFormat"), "idOrientation"), 'widget');
        echo " </span><br/>
        <span style=\"width: 400px; display: inline-block\"><text>Width:</text>";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeFormat"), "width"), 'widget');
        echo " </span><br/>
        <span style=\"width: 400px; display: inline-block\"><text>Height:</text>";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureChangeFormat"), "height"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureCover\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureCover", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 700px; display: inline-block\"><text>Cover image:</text>";
        // line 134
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureCover"), "coverImage"), 'widget');
        echo " </span>
        <span style=\"width: 700px; display: inline-block\"><text>Num pages:</text>";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureCover"), "numPage"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureBodBarcode\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureBodBarcode", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Pos X:</text>";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBodBarcode"), "posX"), 'widget');
        echo " </span>
        <span style=\"width: 400px; display: inline-block\"><text>Pos Y:</text>";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBodBarcode"), "posY"), 'widget');
        echo " </span><br/>
        <span style=\"width: 700px; display: inline-block\"><text>Style:</text>";
        // line 140
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBodBarcode"), "style"), 'widget');
        echo " </span><br/>
        <span style=\"width: 400px; display: inline-block\"><text>Type:</text>";
        // line 141
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureBodBarcode"), "idBarcodeType"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureWebCover\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureWebCover", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 700px; display: inline-block\"><text>Cover image:</text>";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureWebCover"), "coverImage"), 'widget');
        echo " </span>
    </span>

    <!-- WIDGET -->
    <span id=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetScaleTool\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetScaleTool", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 300px; display: inline-block\"><text>Left label:</text>";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "leftLabel"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Middle label:</text>";
        // line 150
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "middleLabel"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Right label:</text>";
        // line 151
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "rightLabel"), 'widget');
        echo " </span>
        <br/>
        <span style=\"width: 240px; display: inline-block\"><text>Font color:</text>";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "fontColor"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Font size:</text>";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "fontSize"), 'widget');
        echo " </span>
        <span style=\"width: 240px; display: inline-block\"><text>Arrow size:</text>";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "arrowSize"), 'widget');
        echo " </span> <br/>
        <span><text>Scale definition:</text>";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "scaleDefinition"), 'widget');
        echo " </span>
        <span><text>Selected Value:</text>";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "selectedValue"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetScaleTool"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetSportTable\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetSportTable", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Daily Sport Kcal:</text>";
        // line 161
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "dailySportKcal"), 'widget');
        echo " </span>
        <span><text>Table definition:</text>";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "tableDefinition"), 'widget');
        echo " </span>
        <span><text>Activity definition:</text>";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "activityDefinition"), 'widget');
        echo " </span>
        <span><text>Session definition:</text>";
        // line 164
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "sessionDefinition"), 'widget');
        echo " </span>
        <span><text>Style:</text>";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "style"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetSportTable"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetTable\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetTable", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Header:</text>";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTable"), "header"), 'widget');
        echo " </span>
        <span><text>Content:</text>";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTable"), "content"), 'widget');
        echo " </span>
        <span><text>Risks (Additional):</text>";
        // line 171
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTable"), "risks"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 172
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTable"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetDrugList\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetDrugList", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Drug information:</text>";
        // line 175
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDrugList"), "drugsInformation"), 'widget');
        echo " </span>
        <span><text>Parameters:</text>";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDrugList"), "parameters"), 'widget');
        echo " </span>
    </span>
    <!-- Widget table -->
    <span id=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetDailySportTable\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetDailySportTable", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 300px; display: inline-block\"><text>From week:</text>";
        // line 180
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDailySportTable"), "fromWeek"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>To week:</text>";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDailySportTable"), "toWeek"), 'widget');
        echo " </span>
        <span><text>Definition:</text>";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDailySportTable"), "definition"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 183
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetDailySportTable"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetImageWithLabel\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetImageWithLabel", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 700px; display: inline-block\"><text>Image:</text>";
        // line 186
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "image"), 'widget');
        echo " </span> <br/>
        <span style=\"width: 700px; display: inline-block\"><text>Override image:</text>";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "overrideImage"), 'widget');
        echo " </span> <br/>
        <span style=\"width: 300px; display: inline-block\"><text>Height:</text>";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "height"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Rotation:</text>";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "rotation"), 'widget');
        echo " </span>
        <span><text>Labels:</text>";
        // line 190
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "labels"), 'widget');
        echo " </span>
        <span><text>Link:</text>";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "link"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Pos X:</text>";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "posX"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Pos Y:</text>";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "posY"), 'widget');
        echo " </span>
        <span><text>Alignment:</text>";
        // line 194
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "alignment"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 195
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetImageWithLabel"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetFoodTable\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetFoodTable", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 500px; display: inline-block\"><text>Food definition:</text>";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "foodDefinition"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Ingredients display:</text>";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "ingredientDisplay"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Risks:</text>";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "risks"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Other variables:</text>";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "otherVariables"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Labels:</text>";
        // line 202
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "label"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Styles:</text>";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "style"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Overshoot factor:</text>";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "overshootFactor"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Debug:</text>";
        // line 205
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "isDebug"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Warning:</text>";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "warning"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Scales:</text>";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "scales"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Bar calculation:</text>";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable"), "idBarCalculation"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetFoodTable2\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetFoodTable2", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 500px; display: inline-block\"><text>Food definition:</text>";
        // line 211
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable2"), "foodDefinition"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Parameters:</text>";
        // line 212
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable2"), "parameters"), 'widget');
        echo " </span><br/>
        <span style=\"width: 500px; display: inline-block\"><text>Ingredients display:</text>";
        // line 213
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable2"), "ingredientDisplay"), 'widget');
        echo "</span>
        <span style=\"width: 500px; display: inline-block\"><text>Warning:</text>";
        // line 214
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetFoodTable2"), "warning"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetBarChart\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetBarChart", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 300px; display: inline-block\"><text>Selected value:</text>";
        // line 217
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetBarChart"), "selectedValue"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Chart height:</text>";
        // line 218
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetBarChart"), "chartHeight"), 'widget');
        echo " </span>
        <span style=\"width: 500px; display: inline-block\"><text>Title:</text>";
        // line 219
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetBarChart"), "chartTitle"), 'widget');
        echo " </span>
        <span><text>Chart definition:</text>";
        // line 220
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetBarChart"), "chartDefinition"), 'widget');
        echo " </span>
        <span><text>Bar definition:</text>";
        // line 221
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetBarChart"), "barDefinition"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetRecipe\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetRecipe", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Calory amount:</text>";
        // line 224
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipe"), "caloryAmount"), 'widget');
        echo " </span>
        <span style=\"width: 400px; display: inline-block\"><text>Calory amount 2:</text>";
        // line 225
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipe"), "caloryAmount2"), 'widget');
        echo " </span> <br/>
        <span><text>Recipe ID:</text>";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipe"), "recipeId"), 'widget');
        echo " </span>
        <span><text>Risks:</text>";
        // line 227
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipe"), "risks"), 'widget');
        echo " </span>
        <span><text>Style:</text>";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipe"), "style"), 'widget');
        echo "</span>
    </span>
    <span id=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetTableNew\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetTableNew", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Content:</text>";
        // line 231
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTableNew"), "content"), 'widget');
        echo " </span>
        <span><text>Visibility:</text>";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetTableNew"), "visibility"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolWidgetRecipeSimplified\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolWidgetRecipeSimplified", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 400px; display: inline-block\"><text>Calory amount:</text>";
        // line 235
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipeSimplified"), "caloryAmount"), 'widget');
        echo " </span>
        <span><text>Recipe ID:</text>";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipeSimplified"), "recipeId"), 'widget');
        echo " </span>
        <span><text>Risks:</text>";
        // line 237
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipeSimplified"), "risks"), 'widget');
        echo " </span>
        <span><text>Style:</text>";
        // line 238
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolWidgetRecipeSimplified"), "style"), 'widget');
        echo "</span>
    </span>

    <!-- UTILS -->
    <span id=\"";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilGrabTextBlock\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilGrabTextBlock", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 300px; display: inline-block\"><text>Text blocks:</text>";
        // line 243
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilGrabTextBlock"), "textBlocks"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilExternalPdf\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilExternalPdf", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 600px; display: inline-block\"><text>PDF:</text>";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilExternalPdf"), "pdf"), 'widget');
        echo " </span>
        <span ><text>Override Pdf:</text>";
        // line 247
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilExternalPdf"), "overridePdf"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilThirdPartyExternalPdf\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilThirdPartyExternalPdf", array(), "array"), "html", null, true);
        echo "\">
        <span ><text>Pdf:</text>";
        // line 250
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilThirdPartyExternalPdf"), "pdf"), 'widget');
        echo " </span>
        <span ><text>Party:</text>";
        // line 251
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilThirdPartyExternalPdf"), "idParty"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilBackgroundImage\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilBackgroundImage", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Image:</text>";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilBackgroundImage"), "image"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilMoveCursor\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilMoveCursor", array(), "array"), "html", null, true);
        echo "\">
        <span style=\"width: 300px; display: inline-block\"><text>X:</text>";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilMoveCursor"), "x"), 'widget');
        echo " </span>
        <span style=\"width: 300px; display: inline-block\"><text>Y:</text>";
        // line 258
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilMoveCursor"), "y"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 260
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilTOC\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilTOC", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Style:</text>";
        // line 261
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilTOC"), "style"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 263
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilTOCEntry\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilTOCEntry", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Code:</text>";
        // line 264
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilTOCEntry"), "code"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolUtilAnnotator\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolUtilAnnotator", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Annotator:</text>";
        // line 267
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolUtilAnnotator"), "annotator"), 'widget');
        echo " </span>
    </span>


    <!-- PAGE -->
    <span id=\"";
        // line 272
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolPageIntroPage\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolPageIntroPage", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Config:</text>";
        // line 273
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolPageIntroPage"), "style"), 'widget');
        echo " </span>
    </span>

    <!-- CHAPTER -->
    <span id=\"";
        // line 277
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolChapterToc\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolChapterToc", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Chapters:</text>";
        // line 278
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolChapterToc"), "chapters"), 'widget');
        echo " </span>
        <span><text>Options:</text>";
        // line 279
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolChapterToc"), "options"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 281
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolChapterPage\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolChapterPage", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Chapter ID:</text>";
        // line 282
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolChapterPage"), "chapterId"), 'widget');
        echo " </span>
        <span><text>Chapter color:</text>";
        // line 283
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolChapterPage"), "chapterColor"), 'widget');
        echo " </span>
        <span><text>Options:</text>";
        // line 284
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolChapterPage"), "options"), 'widget');
        echo " </span>
    </span>

    <span id=\"";
        // line 287
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureNewCoverNovogenia\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureNewCoverNovogenia", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Background:</text>";
        // line 288
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureNewCoverNovogenia"), "background"), 'widget');
        echo "</span>
        <span><text>Rings (Step by step):</text>";
        // line 289
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureNewCoverNovogenia"), "rings"), 'widget');
        echo "</span>
        <span><text>Number of pages:</text>";
        // line 290
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureNewCoverNovogenia"), "numPage"), 'widget');
        echo "&nbsp;&nbsp;<a target=\"_blank\" href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureNewCoverNovogenia"), "vars"), "imageHelperPath"), "html", null, true);
        echo "\">Pick image</a></span>
        <span style=\"display: inline-block\"><text>Is BOD Cover?:</text>";
        // line 291
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureNewCoverNovogenia"), "isBodCover"), 'widget');
        echo " </span>
    </span>

    <span id=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "_textTool_toolStructureStandardCover\" class=\"text_block\" style=\"background-color: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "bgColor"), "toolStructureStandardCover", array(), "array"), "html", null, true);
        echo "\">
        <span><text>Background:</text>";
        // line 295
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "bgColor"), 'widget');
        echo " </span>
        <span><text>Line:</text>";
        // line 296
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "lineBox"), 'widget');
        echo " </span>
        <span><text>Logo boxs:</text>";
        // line 297
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "logoBox"), 'widget');
        echo " </span>
        <span><text>Icon:</text>";
        // line 298
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "iconBox"), 'widget');
        echo " </span>
        <span><text>Image content:</text>";
        // line 299
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "imageContent"), 'widget');
        echo " </span>
        <span><text>Number of pages:</text>";
        // line 300
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "toolStructureStandardCover"), "numPage"), 'widget');
        echo " </span>
    </span>

    <script type=\"text/javascript\">
        jQuery('#";
        // line 304
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "').change(function() {
            var toolId = jQuery(this).val();
            onChangeTextTool('#";
        // line 306
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idTextToolType"), "vars"), "id"), "html", null, true);
        echo "', toolId);
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:app_text_tool.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1129 => 306,  1124 => 304,  1117 => 300,  1113 => 299,  1109 => 298,  1105 => 297,  1101 => 296,  1097 => 295,  1091 => 294,  1085 => 291,  1079 => 290,  1075 => 289,  1071 => 288,  1065 => 287,  1059 => 284,  1055 => 283,  1051 => 282,  1045 => 281,  1040 => 279,  1036 => 278,  1030 => 277,  1023 => 273,  1017 => 272,  1009 => 267,  1003 => 266,  998 => 264,  992 => 263,  987 => 261,  981 => 260,  976 => 258,  972 => 257,  966 => 256,  961 => 254,  955 => 253,  950 => 251,  946 => 250,  940 => 249,  935 => 247,  931 => 246,  925 => 245,  920 => 243,  914 => 242,  907 => 238,  903 => 237,  899 => 236,  895 => 235,  889 => 234,  884 => 232,  880 => 231,  874 => 230,  869 => 228,  865 => 227,  861 => 226,  857 => 225,  853 => 224,  847 => 223,  842 => 221,  838 => 220,  834 => 219,  830 => 218,  826 => 217,  820 => 216,  815 => 214,  811 => 213,  807 => 212,  803 => 211,  797 => 210,  792 => 208,  788 => 207,  784 => 206,  780 => 205,  776 => 204,  772 => 203,  768 => 202,  764 => 201,  760 => 200,  756 => 199,  752 => 198,  746 => 197,  741 => 195,  737 => 194,  733 => 193,  729 => 192,  725 => 191,  721 => 190,  717 => 189,  713 => 188,  709 => 187,  705 => 186,  699 => 185,  694 => 183,  690 => 182,  686 => 181,  682 => 180,  676 => 179,  670 => 176,  666 => 175,  660 => 174,  655 => 172,  651 => 171,  647 => 170,  643 => 169,  637 => 168,  632 => 166,  628 => 165,  624 => 164,  620 => 163,  616 => 162,  612 => 161,  606 => 160,  601 => 158,  597 => 157,  593 => 156,  589 => 155,  585 => 154,  581 => 153,  576 => 151,  572 => 150,  568 => 149,  562 => 148,  555 => 144,  549 => 143,  544 => 141,  540 => 140,  536 => 139,  532 => 138,  526 => 137,  521 => 135,  517 => 134,  511 => 133,  506 => 131,  502 => 130,  498 => 129,  494 => 128,  488 => 127,  483 => 125,  477 => 124,  472 => 122,  468 => 121,  464 => 120,  460 => 119,  456 => 118,  452 => 117,  448 => 116,  444 => 115,  438 => 114,  433 => 112,  429 => 111,  425 => 110,  421 => 109,  415 => 108,  410 => 106,  402 => 104,  396 => 103,  391 => 101,  387 => 100,  381 => 99,  376 => 97,  372 => 96,  368 => 95,  364 => 94,  360 => 93,  354 => 92,  349 => 90,  339 => 88,  328 => 83,  324 => 82,  320 => 81,  284 => 72,  280 => 71,  276 => 70,  253 => 64,  218 => 55,  214 => 54,  202 => 51,  191 => 48,  180 => 45,  133 => 33,  100 => 23,  93 => 26,  25 => 2,  19 => 1,  95 => 45,  139 => 104,  194 => 16,  111 => 96,  149 => 37,  92 => 80,  196 => 50,  153 => 47,  148 => 44,  141 => 35,  123 => 31,  114 => 31,  112 => 26,  107 => 36,  102 => 34,  73 => 16,  249 => 63,  103 => 26,  326 => 168,  296 => 75,  292 => 74,  285 => 152,  261 => 66,  252 => 127,  240 => 124,  233 => 59,  186 => 14,  154 => 39,  143 => 105,  128 => 110,  125 => 36,  108 => 25,  77 => 17,  342 => 168,  337 => 165,  316 => 80,  312 => 79,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 69,  257 => 65,  246 => 14,  236 => 116,  227 => 58,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 47,  147 => 68,  142 => 68,  120 => 57,  32 => 4,  99 => 28,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 20,  75 => 21,  72 => 18,  38 => 8,  82 => 18,  67 => 17,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 105,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 89,  340 => 96,  332 => 84,  327 => 90,  317 => 161,  304 => 77,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 67,  262 => 65,  248 => 126,  243 => 58,  241 => 61,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 52,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 38,  118 => 14,  115 => 13,  106 => 50,  96 => 27,  91 => 2,  84 => 40,  81 => 18,  79 => 20,  76 => 20,  71 => 18,  69 => 15,  66 => 13,  56 => 12,  54 => 10,  49 => 12,  41 => 6,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 56,  213 => 72,  210 => 53,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 43,  169 => 32,  164 => 41,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 40,  144 => 44,  137 => 34,  135 => 18,  129 => 32,  127 => 38,  119 => 48,  116 => 27,  110 => 30,  104 => 24,  101 => 28,  97 => 26,  88 => 21,  64 => 99,  60 => 13,  36 => 7,  23 => 2,  21 => 1,  85 => 19,  78 => 21,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 78,  303 => 111,  300 => 76,  291 => 98,  288 => 73,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 62,  242 => 120,  237 => 60,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 44,  173 => 54,  171 => 53,  168 => 42,  160 => 40,  157 => 51,  150 => 41,  145 => 36,  138 => 36,  134 => 102,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 22,  90 => 21,  86 => 20,  80 => 71,  74 => 16,  55 => 15,  50 => 14,  44 => 9,  39 => 10,  33 => 6,  26 => 2,  68 => 19,  61 => 13,  58 => 12,  48 => 8,  46 => 10,  42 => 8,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 14,  63 => 13,  59 => 18,  57 => 12,  53 => 11,  51 => 10,  47 => 10,  45 => 12,  40 => 6,  37 => 5,  30 => 3,  27 => 3,);
    }
}
