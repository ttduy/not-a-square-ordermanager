<?php

/* LeepAdminBundle:Customer:form.html.twig */
class __TwigTemplate_44f5106843ffc37bb31366f81d9c423a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        if ((array_key_exists("isAddedInvoiced", $context) && $this->getContext($context, "isAddedInvoiced"))) {
            // line 9
            echo "        <div class=\"notification msgalert\">
            <p>Already invoiced! Changes won't be updated anymore</p>
        </div>
        <br/>
    ";
        }
        // line 14
        echo "
    ";
        // line 15
        $this->env->loadTemplate("LeepAdminBundle:Customer:form.html.twig", "200201819")->display(array_merge($context, array("form" => $this->getContext($context, "form"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 15,  50 => 14,  43 => 9,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:Customer:form.html.twig */
class __TwigTemplate_44f5106843ffc37bb31366f81d9c423a_200201819 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Basic.html.twig");

        $this->blocks = array(
            'formContentNormal' => array($this, 'block_formContentNormal'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Basic.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 16
    public function block_formContentNormal($context, array $blocks = array())
    {
        // line 17
        echo "            <style type=\"text/css\">
                .hidden {
                    display: none;
                }
            </style>
            <div>
                <p id=\"quick-access-links\" style=\"border: 0px; visibility: hidden;\">
                    <span id=\"span-go-acquisiteur1\"><a id=\"go-acquisiteur1\" href=\"javascript: goAcquisiteur(posAcquisiteur = 1)\">Go to Acquisiteur1</a></span>
                    <span id=\"span-go-acquisiteur2\" style=\"padding-left: 30px;\"><a id=\"go-acquisiteur2\" href=\"javascript: goAcquisiteur(posAcquisiteur = 2)\">Go to Acquisiteur2</a></span>
                    <span id=\"span-go-acquisiteur3\" style=\"padding-left: 30px;\"><a id=\"go-acquisiteur3\" href=\"javascript: goAcquisiteur(posAcquisiteur = 3)\">Go to Acquisiteur3</a></span>
                    <span id=\"span-go-partner\" style=\"padding-left: 30px;\"><a id=\"go-partner\" href=\"javascript: goPartner()\">Go to Partner</a></span><span style=\"padding-left: 30px;\"><a id=\"go-distribution-channel\" href=\"javascript: goDistributionChannel()\">Go to Distribution Channel</a></span>
                </p>
                <br>
                <output id=\"err-mess\" class=\"error-message\"></output>
            </div>
            <script type=\"text/javascript\">
                var customerId;
                var distributionChannelId;
                var partnerId;
                var getPartnerSuccess;
                var acquisiteurId1, acquisiteurId2, acquisiteurId3;
                jQuery(document).ready(function(){

                    if (\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getContext($context, "isCreateHandler"), "html", null, true);
        echo "\" == '0') {
                        jQuery(\"#quick-access-links\").css({
                            \"visibility\":\"visible\"
                        });
                        customerId = \"";
        // line 44
        echo twig_escape_filter($this->env, $this->getContext($context, "appRequestId"), "html", null, true);
        echo "\";
                        distributionChannelId = String(jQuery(\"#";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idDistributionChannel"), "vars"), "id"), "html", null, true);
        echo "\").val());
                        var quickInfo_js = \"";
        // line 46
        echo twig_escape_filter($this->env, $this->getContext($context, "customerQuickAccesskInformation"), "html", null, true);
        echo "\";
                        var quickInfo = JSON.parse(quickInfo_js);
                        acquisiteurId1 = quickInfo[0];
                        acquisiteurId2 = quickInfo[1];
                        acquisiteurId3 = quickInfo[2];
                        partnerId = String(quickInfo[3]);
                        var countHidden = 0;
                        if (!acquisiteurId1) {
                            jQuery(\"#span-go-acquisiteur1\").remove();
                            countHidden += 1;
                        }
                        if (!acquisiteurId2) {
                            jQuery(\"#span-go-acquisiteur2\").remove();
                            countHidden += 1;
                        }
                        else{
                            if (countHidden == 1) {
                                jQuery(\"#go-acquisiteur2\").css({
                                    \"margin-left\":\"-30px\",
                                });
                            }
                        }
                        if (!acquisiteurId3) {
                            jQuery(\"#span-go-acquisiteur3\").remove();
                            countHidden += 1;
                        }
                        else{
                            if (countHidden == 2) {
                                jQuery(\"#go-acquisiteur3\").css({
                                    \"margin-left\":\"-30px\",
                                });
                            }
                        }
                        if (!partnerId) {
                            jQuery(\"#span-go-partner\").remove();
                            countHidden += 1;
                        }
                        else{
                            if (countHidden == 3) {
                                jQuery(\"#go-partner\").css({
                                    \"margin-left\":\"-30px\",
                                });
                            }
                        }
                        if(!distributionChannelId){
                            jQuery(\"#go-distribution-channel\").remove();
                        }
                        else{
                            if (countHidden == 4) {
                                jQuery(\"#go-distribution-channel\").css({
                                    \"margin-left\":\"-30px\"
                                });
                            }
                        }
                    }
                    var delayDownload = jQuery('#";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "');
                    if (delayDownload.is(':checked')) {
                        jQuery('#";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').show();
                    }
                    else {
                        jQuery('#";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').hide();
                    }

                    jQuery('#";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').change(function(){
                        if (this.checked) {
                            jQuery('#";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').show();
                        }
                        else {
                            jQuery('#";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').hide();
                        }
                    });
                });

                // check if similar customer info available, get the existing one
                function checkCustomerInfo() {
                    var checkExistingCustomerSpan = jQuery('#checkExistingCustomerSpan');
                    checkExistingCustomerSpan.html('Checking..');

                    // collect data
                    var customerInfo = {};

                    customerInfo.customerNumber = jQuery('#";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "customerNumber"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.idCustomer = '";
        // line 128
        echo twig_escape_filter($this->env, $this->getContext($context, "appRequestId"), "html", null, true);
        echo "';
                    customerInfo.externalBarcode = jQuery('#";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "externalBarcode"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.idDistributionChannel = jQuery('#";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "idDistributionChannel"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.firstName = jQuery('#";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "firstName"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.surName = jQuery('#";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "surName"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.dateOfBirth = jQuery('#";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "dateOfBirth"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.postCode = jQuery('#";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "postCode"), "vars"), "id"), "html", null, true);
        echo "').val();
                    customerInfo.detailInformation = jQuery('#";
        // line 135
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "detailInformation"), "vars"), "id"), "html", null, true);
        echo "').val();

                    customerInfo.isCustomerBeContacted = jQuery('#";
        // line 137
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "isCustomerBeContacted"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;
                    customerInfo.isNutrimeOffered = jQuery('#";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "isNutrimeOffered"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;
                    customerInfo.isNotContacted = jQuery('#";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "isNotContacted"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;
                    customerInfo.isDestroySample = jQuery('#";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "isDestroySample"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;
                    customerInfo.sampleCanBeUsedForScience = jQuery('#";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "sampleCanBeUsedForScience"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;
                    customerInfo.isFutureResearchParticipant = jQuery('#";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view"), "isFutureResearchParticipant"), "vars"), "id"), "html", null, true);
        echo "').is(':checked') ? 1 : 0;


                    // send to server
                    var targetUrl = '";
        // line 146
        echo twig_escape_filter($this->env, $this->getContext($context, "urlCheckCustomerInfo"), "html", null, true);
        echo "';

                    jQuery.ajax({
                        url: targetUrl,
                        type: \"POST\",
                        context: document.body,
                        data: customerInfo
                    }).done(function(data) {
                        data = JSON.parse(data);
                        checkExistingCustomerSpan.html(data.msg + \"<br/>\");

                        var key;
                        for (key in data.customerInfos) {
                            var url = \"";
        // line 159
        echo twig_escape_filter($this->env, $this->getContext($context, "urlViewCustomerInfo"), "html", null, true);
        echo "?id=\" + key;
                            var link = jQuery('<a style=\"margin-right: 5px\" target=\"_blank\" href=\"' + url + '\">' + data.customerInfos[key] + '</a>');
                            checkExistingCustomerSpan.append(link);
                        }
                    }).error(function(jqXHR, textStatus, errorThrown ) {
                        checkExistingCustomerSpan.html(\"ERROR! CAN'T LOAD DATA\");
                    });
                }


                function openEmail(url) {
                    jQuery.colorbox({
                        href: url,
                        iframe: true,
                        width:  \"80%\",
                        height: \"80%\"
                    });
                }

                function statusRowDelete(rowId) {
                    jQuery('#' + rowId).remove();
                }

                var newIndex = 1;

                function statusRowAddNew(appendAfter) {
                    var collectionHolder = jQuery('#statusRowCollection');
                    var prototype = jQuery('#statusRowDataPrototype').attr('data-prototype');

                    newIndex++;
                    var newForm = prototype.replace(/__name__/g, newIndex);

                    var rowId = 'statusRecordRow_' + newIndex;

                    var newWidget = jQuery('<p id=\"' + rowId + '\"><span class=\"field\" style=\"margin-left: -1px\"></span></p>');
                    var spanWidget = newWidget.find('span');
                    if (appendAfter != '') {
                        var a = collectionHolder.find(appendAfter);
                        newWidget.insertAfter(a);
                    }
                    else {
                        collectionHolder.append(newWidget);
                    }
                    spanWidget.append(newForm);
                    spanWidget.append('<span style=\"width: 50px; display: inline-block\"></span><a href=\"javascript:statusRowDelete(\\'' + rowId + '\\')\">Remove</a>');
                    spanWidget.append('&nbsp;&nbsp;<a href=\"javascript:statusRowAddNew(\\'' + '#' + rowId + '\\')\">Insert</a>');
                    spanWidget.append('&nbsp;&nbsp;<a href=\"javascript:statusRowEmail(\\'' + '#' + rowId + '\\')\">Email</a>');
                }

                function statusRowEmail(id) {
                    var status = jQuery(id).find('select').val();
                    jQuery.colorbox({
                        href: '";
        // line 211
        echo $this->getContext($context, "emailSendingUrl");
        echo "&status=' + status,
                        iframe: true,
                        width:  \"75%\",
                        height: \"75%\"
                    });
                }

                function showError(errorMessage){
                    var delay = 500;
                    setTimeout(function(){
                        document.getElementById(\"err-mess\").value = errorMessage;
                        document.getElementById(\"err-mess\").style.visibility = \"visible\";
                    }, delay);
                }

                function goPartner(){
                    document.getElementById(\"err-mess\").style.visibility = \"hidden\";
                    // submit to server to get acquisiteur id correspond acquisiteur in form
                    if (partnerId) {
                        var partnerEditUrl = \"";
        // line 230
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "partner", array(), "array"), "edit", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + String(partnerId);
                        window.open(partnerEditUrl, '_blank');
                    }
                    else{
                        var errorMessage = \"Error! Partner is empty\";
                        showError(errorMessage);
                    }
                }

                function goDistributionChannel(){
                    document.getElementById(\"err-mess\").style.visibility = \"hidden\";
                    if (distributionChannelId) {
                        var url = \"";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "distributionChannel", array(), "array"), "editLink", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + distributionChannelId;
                        window.open(url, '_blank');
                    }
                    else{
                        var errorMessage = \"Error! Distribution Channel is empty\";
                        showError(errorMessage);
                    }
                }

                function jumpAcquisiteur(customerId, position) {
                    var acquisiteurInfo_json = JSON.stringify([customerId, position]);
                    // call to php function and get cosrrepond acquisiteurId
                    var targetUrl = \"";
        // line 254
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "customer", array(), "array"), "getAcquisiteurIdUrl", array(), "array"), "html", null, true);
        echo "\" + \"?param=\" + acquisiteurInfo_json;
                    jQuery.ajax({
                        url: targetUrl,
                        type: 'POST'
                    }).done(function(acquisiteurId){
                        var acquisiteurId_str = String(acquisiteurId);
                        if (acquisiteurId_str) {
                            var acquisiteurEditUrl = \"";
        // line 261
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "acquisiteur", array(), "array"), "edit", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + acquisiteurId_str;
                            window.open(acquisiteurEditUrl, '_blank');
                        }
                        else{
                            var errorMessage = \"Error! Acquisiteur\" + String(posAcquisiteur) + \" is empty\";
                            showError(errorMessage);
                        }
                    }).error(function(){
                        var errorMessage = \"Error! Can't not open Acquisiteur\";
                        showError(errorMessage);
                    });
                }

                function showAcquisiteurError(posAcquisiteur){
                    var errorMessage = \"Error! Acquisiteur\" + String(posAcquisiteur) + \" is empty\";
                    showError(errorMessage);
                }

                function goAcquisiteur(posAcquisiteur){
                    document.getElementById(\"err-mess\").style.visibility = \"hidden\";
                    switch (posAcquisiteur){
                        case 1:
                            {
                                if (acquisiteurid1) {
                                var acquisiteurEditUrl = \"";
        // line 285
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "acquisiteur", array(), "array"), "edit", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + acquisiteurId1;
                                window.open(acquisiteurEditUrl, '_blank');
                                }
                                else{
                                    showAcquisiteurError(posAcquisiteur);
                                }
                            }
                            break;
                        case 2:
                            {
                                if (acquisiteurid2) {
                                    var acquisiteurEditUrl = \"";
        // line 296
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "acquisiteur", array(), "array"), "edit", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + acquisiteurId2;
                                    window.open(acquisiteurEditUrl, '_blank');
                                }
                                else{
                                    showAcquisiteurError(posAcquisiteur);
                                }
                            }
                            break;
                        case 3:
                            {
                                if (acquisiteurid1) {
                                    var acquisiteurEditUrl = \"";
        // line 307
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "acquisiteur", array(), "array"), "edit", array(), "array"), "html", null, true);
        echo "\" + \"?id=\" + acquisiteurId2;
                                    window.open(acquisiteurEditUrl, '_blank');
                                }
                                else{
                                    showAcquisiteurError(posAcquisiteur);
                                }
                            }
                            break;
                        default:
                            {
                                errorMessage = \"Error! Can't not open acquisiteur\"
                                show(errorMessage);
                            }
                    }
                }

            </script>

            ";
        // line 325
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionAttachment"), 'row');
        echo "
            ";
        // line 326
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "attachment"), 'row');
        echo "

            ";
        // line 328
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "view", array(), "array", false, true), "oldBarcode", array(), "any", true, true)) {
            // line 329
            echo "                <br/><h2>Copy history</h2><br/>
                <p></p>
                ";
            // line 331
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "copyHistory"), 'row');
            echo "
                <br/>
            ";
        }
        // line 334
        echo "
            ";
        // line 335
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionCustomerInfo"), 'row');
        echo "
            <div style=\"margin-top: -1px\">
                <div class=\"one_half\">
                    ";
        // line 338
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "customerNumber"), 'row');
        echo "
                    ";
        // line 339
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "externalBarcode"), 'row');
        echo "
                    ";
        // line 340
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idDistributionChannel"), 'row');
        echo "
                    ";
        // line 341
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isCustomerBeContacted"), 'row');
        echo "
                    ";
        // line 342
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isNutrimeOffered"), 'row');
        echo "
                    ";
        // line 343
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isNotContacted"), 'row');
        echo "
                    ";
        // line 344
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDestroySample"), 'row');
        echo "
                    ";
        // line 345
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sampleCanBeUsedForScience"), 'row');
        echo "
                    ";
        // line 346
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isFutureResearchParticipant"), 'row');
        echo "
                    ";
        // line 347
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "detailInformation"), 'row');
        echo "
                </div>
                <div class=\"one_half last\">
                    ";
        // line 350
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "firstName"), 'row');
        echo "
                    ";
        // line 351
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "surName"), 'row');
        echo "
                    ";
        // line 352
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "previousOrderNumber"), 'row');
        echo "
                    ";
        // line 353
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "dateOfBirth"), 'row');
        echo "
                    ";
        // line 354
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "postCode"), 'row');
        echo "
                    ";
        // line 355
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "genderId"), 'row');
        echo "
                    ";
        // line 356
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "title"), 'row');
        echo "
                    ";
        // line 357
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "webLoginGoesTo"), 'row');
        echo "

                    ";
        // line 359
        if (((!$this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "view", array(), "array", false, true), "id", array(), "any", true, true)) || (null === $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "id")))) {
            // line 360
            echo "                    ";
        } else {
            // line 361
            echo "                        <p>
                            <span class=\"field\" style=\"margin-left: -1px\">
                                <button class=\"radius2\" type=\"button\" onclick=\"checkCustomerInfo();\">&nbsp;&nbsp;Check Existing Customer&nbsp;&nbsp;</button>
                                <br/><br/>
                                <span id=\"checkExistingCustomerSpan\">

                                </span>
                            </span>
                        </p>
                        ";
            // line 370
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "otherOrders"), 'row');
            echo "
                    ";
        }
        // line 372
        echo "
                </div>
                <br clear=\"all\" />
            </div>

            <br/>
            <div id=\"main-order-tabs\">
                <ul>
                    <li><a href=\"#section-order\">Order</a></li>
                    <li><a href=\"#section-address\">Address</a></li>
                    <li><a href=\"#section-invoice-address\">Invoice Address</a></li>
                    <li><a href=\"#section-settings\">Setting</a></li>
                    <li><a href=\"#section-invoice-settings\">Invoice Setting</a></li>
                    <li><a href=\"#section-status\">Status</a></li>
                    <li><a href=\"#section-product\">Product</a></li>
                    <li><a href=\"#section-questions\">Questions</a></li>

                    ";
        // line 389
        if (array_key_exists("subOrderInfo", $context)) {
            // line 390
            echo "                        <li><a href=\"#section-sub-order\">Sub Order</a></li>
                    ";
        }
        // line 392
        echo "                </ul>

                <div id=\"section-order\">
                    ";
        // line 395
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionOrder"), 'row');
        echo "
                    ";
        // line 396
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "orderNumber"), 'row');
        echo "
                    ";
        // line 397
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "trackingCode"), 'row');
        echo "
                    ";
        // line 398
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "dnaSampleOrderNumberVario"), 'row');
        echo "
                    ";
        // line 399
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "orderInfoText"), 'row');
        echo "
                    ";
        // line 400
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "domain"), 'row');
        echo "
                    ";
        // line 401
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "dateOrdered"), 'row');
        echo "
                    ";
        // line 402
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "orderAge"), 'row');
        echo "
                    ";
        // line 403
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "laboratoryDetails"), 'row');
        echo "
                    ";
        // line 404
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "canCustomersDownloadReports"), 'row');
        echo "
                    ";
        // line 405
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDisplayInCustomerDashboard"), 'row');
        echo "
                    ";
        // line 406
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDelayDownloadReport"), 'row');
        echo "
                    ";
        // line 407
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), 'row');
        echo "
                </div>

                <div id=\"section-address\">
                    ";
        // line 411
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionAddress"), 'row');
        echo "
                    ";
        // line 412
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "street"), 'row');
        echo "
                    ";
        // line 413
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "street2"), 'row');
        echo "
                    ";
        // line 414
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "city"), 'row');
        echo "
                    ";
        // line 415
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "countryId"), 'row');
        echo "
                    ";
        // line 416
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "email"), 'row');
        echo "
                    ";
        // line 417
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "telephone"), 'row');
        echo "
                    ";
        // line 418
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "fax"), 'row');
        echo "
                    ";
        // line 419
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "notes"), 'row');
        echo "
                </div>

                <div  id=\"section-invoice-address\">
                    ";
        // line 423
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionInvoice"), 'row');
        echo "
                    ";
        // line 424
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressIsUsed"), 'row');
        echo "
                    ";
        // line 425
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressCompanyName"), 'row');
        echo "
                    ";
        // line 426
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressClientName"), 'row');
        echo "
                    ";
        // line 427
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressStreet"), 'row');
        echo "
                    ";
        // line 428
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressPostCode"), 'row');
        echo "
                    ";
        // line 429
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressCity"), 'row');
        echo "
                    ";
        // line 430
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressIdCountry"), 'row');
        echo "
                    ";
        // line 431
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressTelephone"), 'row');
        echo "
                    ";
        // line 432
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressFax"), 'row');
        echo "
                    ";
        // line 433
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceAddressUid"), 'row');
        echo "
                </div>

                <div id=\"section-settings\">
                    ";
        // line 437
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionSetting"), 'row');
        echo "
                    ";
        // line 438
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "languageId"), 'row');
        echo "

                    ";
        // line 440
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionReportDetails"), 'row');
        echo "
                    ";
        // line 441
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "companyInfo"), 'row');
        echo "
                    ";
        // line 442
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "laboratoryInfo"), 'row');
        echo "
                    ";
        // line 443
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "contactInfo"), 'row');
        echo "
                    ";
        // line 444
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "contactUs"), 'row');
        echo "
                    ";
        // line 445
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "letterExtraText"), 'row');
        echo "
                    ";
        // line 446
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "clinicianInfoText"), 'row');
        echo "
                    ";
        // line 447
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text7"), 'row');
        echo "
                    ";
        // line 448
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text8"), 'row');
        echo "
                    ";
        // line 449
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text9"), 'row');
        echo "
                    ";
        // line 450
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text10"), 'row');
        echo "
                    ";
        // line 451
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text11"), 'row');
        echo "

                    ";
        // line 453
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionGsSetting"), 'row');
        echo "
                    ";
        // line 454
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "doctorsReporting"), 'row');
        echo "
                    ";
        // line 455
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "automaticReporting"), 'row');
        echo "
                    ";
        // line 456
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "noReporting"), 'row');
        echo "
                    ";
        // line 457
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoicingAndPaymentInfo"), 'row');
        echo "
                    ";
        // line 458
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idDncReportType"), 'row');
        echo "

                    ";
        // line 460
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionRebrander"), 'row');
        echo "
                    ";
        // line 461
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "rebrandingNick"), 'row');
        echo "
                    ";
        // line 462
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "headerFileName"), 'row');
        echo "
                    ";
        // line 463
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "footerFileName"), 'row');
        echo "
                    ";
        // line 464
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "titleLogoFileName"), 'row');
        echo "
                    ";
        // line 465
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "boxLogoFileName"), 'row');
        echo "
                </div>

                <div id=\"section-invoice-settings\">
                    ";
        // line 469
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionDelivery"), 'row');
        echo "
                    ";
        // line 470
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "priceCategoryId"), 'row');
        echo "
                    ";
        // line 471
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportDeliveryEmail"), 'row');
        echo "
                    ";
        // line 472
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceDeliveryEmail"), 'row');
        echo "
                    ";
        // line 473
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceGoesToId"), 'row');
        echo "
                    ";
        // line 474
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportGoesToId"), 'row');
        echo "
                    ";
        // line 475
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportDeliveryId"), 'row');
        echo "
                    ";
        // line 476
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "ftpServer"), 'row');
        echo "
                    ";
        // line 477
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idPreferredPayment"), 'row');
        echo "
                    ";
        // line 478
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idNutrimeGoesTo"), 'row');
        echo "

                    ";
        // line 480
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionReportDeliveryOptions"), 'row');
        echo "
                    ";
        // line 481
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryDownloadAccess"), 'row');
        echo "
                    ";
        // line 482
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryFtpServer"), 'row');
        echo "
                    ";
        // line 483
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryPrintedBooklet"), 'row');
        echo "
                    ";
        // line 484
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryDontChargeBooklet"), 'row');
        echo "

                    ";
        // line 486
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionReportSetting"), 'row');
        echo "
                    ";
        // line 487
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idRecallGoesTo"), 'row');
        echo "

                    ";
        // line 489
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionPricing"), 'row');
        echo "
                    ";
        // line 490
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idMarginGoesTo"), 'row');
        echo "
                    ";
        // line 491
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "marginOverrideDc"), 'row');
        echo "
                    ";
        // line 492
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "marginOverridePartner"), 'row');
        echo "
                    ";
        // line 493
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "priceOverride"), 'row');
        echo "
                    ";
        // line 494
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur1"), 'row');
        echo "
                    ";
        // line 495
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate1"), 'row');
        echo "
                    ";
        // line 496
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur2"), 'row');
        echo "
                    ";
        // line 497
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate2"), 'row');
        echo "
                    ";
        // line 498
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur3"), 'row');
        echo "
                    ";
        // line 499
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate3"), 'row');
        echo "
                </div>

                <div id=\"section-status\">
                    ";
        // line 503
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionStatus"), 'row');
        echo "
                    <div id=\"statusRowCollection\">
                        ";
        // line 505
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "statusList"));
        foreach ($context['_seq'] as $context["id"] => $context["statusRow"]) {
            // line 506
            echo "                            <p id=\"statusRecordRow_";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "\">
                                <span class=\"field\" style=\"margin-left:-1px\">
                                    ";
            // line 508
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "statusRow"), 'widget');
            echo "
                                    <span style=\"width: 50px; display: inline-block\"></span><a href=\"javascript:statusRowDelete('statusRecordRow_";
            // line 509
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "')\">Remove</a>
                                    &nbsp;<a href=\"javascript:statusRowAddNew('#statusRecordRow_";
            // line 510
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "')\">Insert</a>
                                    &nbsp;<a href=\"javascript:statusRowEmail('#statusRecordRow_";
            // line 511
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "')\">Email</a>
                                </span>
                                <script type=\"text/javascript\">
                                    var newIndex = newIndex > ";
            // line 514
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo " ? newIndex : ";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo ";
                                </script>
                            </p>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['statusRow'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 518
        echo "                    </div>
                    <p>
                        <span class=\"field\" style=\"margin-left: -1px\">
                            <a id=\"statusRowAddNewLink\" href=\"javascript:statusRowAddNew('')\">Add new record</a>
                        </span>
                    </p>

                    <div id=\"statusRowDataPrototype\" data-prototype=\"";
        // line 525
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "statusList"), "vars"), "prototype"), 'widget'));
        echo "\" style=\"display: none\">
                    </div>
                </div>

                <div id=\"section-product\">
                    ";
        // line 530
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sectionOrderInformation"), 'row');
        echo "
                    ";
        // line 531
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "orderInfo"), 'row');
        echo "
                </div>

                <div id=\"section-questions\">
                    ";
        // line 535
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "questions"), 'row');
        echo "
                </div>

                ";
        // line 538
        if (array_key_exists("subOrderInfo", $context)) {
            // line 539
            echo "                <div id=\"section-sub-order\">
                    ";
            // line 540
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "subOrderInfo"));
            foreach ($context['_seq'] as $context["_key"] => $context["productGroup"]) {
                // line 541
                echo "                        ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "productGroup"), "groupName", array(), "array"), "html", null, true);
                echo "
                        ";
                // line 542
                if ($this->getAttribute($this->getContext($context, "productGroup"), "barcode", array(), "array")) {
                    // line 543
                    echo "                            [";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "productGroup"), "barcode", array(), "array"), "html", null, true);
                    echo "]
                        ";
                }
                // line 545
                echo "                        <br/>
                        ";
                // line 546
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "productGroup"), "products", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["productName"]) {
                    // line 547
                    echo "                            - ";
                    echo twig_escape_filter($this->env, $this->getContext($context, "productName"), "html", null, true);
                    echo " <br/>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productName'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 549
                echo "                        <br/>
                        <br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productGroup'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 552
            echo "                </div>
                ";
        }
        // line 554
        echo "            </div>

            <script type=\"text/javascript\">
                function onChangeReportDeliveryId() {
                    var reportDeliveryId = jQuery('#";
        // line 558
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportDeliveryId"), "vars"), "id"), "html", null, true);
        echo "').val();
                    var ftpServerRow = jQuery('#field_row_";
        // line 559
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "ftpServer"), "vars"), "id"), "html", null, true);
        echo "');

                    if (reportDeliveryId == ";
        // line 561
        echo twig_escape_filter($this->env, constant("Leep\\AdminBundle\\Business\\DistributionChannel\\Constant::REPORT_DELIVERY_FTP_SERVER"), "html", null, true);
        echo ") {
                        ftpServerRow.css('display', '');
                    }
                    else {
                        ftpServerRow.css('display', 'none');
                    }
                }


                function onChangeDistributionChannel() {
                    var id = jQuery('#";
        // line 571
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idDistributionChannel"), "vars"), "id"), "html", null, true);
        echo "').val();
                    var targetUrl = \"";
        // line 572
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "distributionChannel"), "ajaxRead"), "html", null, true);
        echo "?id=\" + id;
                    jQuery.ajax({
                        url: targetUrl,
                        context: document.body
                    }).done(function(data) {
                        if (data == \"FAIL\") {
                            return;
                        }
                        var dc = JSON.parse(data);

                        jQuery('#";
        // line 582
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "webLoginGoesTo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['webLoginGoesTo']);
                        jQuery('#";
        // line 583
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "companyInfo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['companyinfo']);
                        jQuery('#";
        // line 584
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "laboratoryInfo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['laboratoryinfo']);
                        jQuery('#";
        // line 585
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "contactInfo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['contactinfo']);
                        jQuery('#";
        // line 586
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "contactUs"), "vars"), "id"), "html", null, true);
        echo "').val(dc['contactus']);
                        jQuery('#";
        // line 587
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "letterExtraText"), "vars"), "id"), "html", null, true);
        echo "').val(dc['letterextratext']);
                        jQuery('#";
        // line 588
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "clinicianInfoText"), "vars"), "id"), "html", null, true);
        echo "').val(dc['clinicianinfotext']);
                        jQuery('#";
        // line 589
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text7"), "vars"), "id"), "html", null, true);
        echo "').val(dc['text7']);
                        jQuery('#";
        // line 590
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text8"), "vars"), "id"), "html", null, true);
        echo "').val(dc['text8']);
                        jQuery('#";
        // line 591
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text9"), "vars"), "id"), "html", null, true);
        echo "').val(dc['text9']);
                        jQuery('#";
        // line 592
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text10"), "vars"), "id"), "html", null, true);
        echo "').val(dc['text10']);
                        jQuery('#";
        // line 593
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "text11"), "vars"), "id"), "html", null, true);
        echo "').val(dc['text11']);

                        jQuery('#";
        // line 595
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "doctorsReporting"), "vars"), "id"), "html", null, true);
        echo "').val(dc['doctorsreporting']);
                        jQuery('#";
        // line 596
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "automaticReporting"), "vars"), "id"), "html", null, true);
        echo "').val(dc['automaticreporting']);
                        jQuery('#";
        // line 597
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "noReporting"), "vars"), "id"), "html", null, true);
        echo "').val(dc['noreporting']);
                        jQuery('#";
        // line 598
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoicingAndPaymentInfo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['invoicingandpaymentinfo']);
                        jQuery('#";
        // line 599
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idDncReportType"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idDncReportType']); // exist

                        jQuery('#";
        // line 601
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "priceCategoryId"), "vars"), "id"), "html", null, true);
        echo "').val(dc['pricecategoryid']);
                        jQuery('#";
        // line 602
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportDeliveryEmail"), "vars"), "id"), "html", null, true);
        echo "').val(dc['reportdeliveryemail']);
                        jQuery('#";
        // line 603
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceDeliveryEmail"), "vars"), "id"), "html", null, true);
        echo "').val(dc['invoicedeliveryemail']);
                        jQuery('#";
        // line 604
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "invoiceGoesToId"), "vars"), "id"), "html", null, true);
        echo "').val(dc['invoicegoestoid']);
                        jQuery('#";
        // line 605
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportGoesToId"), "vars"), "id"), "html", null, true);
        echo "').val(dc['reportgoestoid']);
                        jQuery('#";
        // line 606
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportDeliveryId"), "vars"), "id"), "html", null, true);
        echo "').val(dc['reportdeliveryid']);
                        jQuery('#";
        // line 607
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "ftpServer"), "server"), "vars"), "id"), "html", null, true);
        echo "').val(dc['ftpservername']);
                        jQuery('#";
        // line 608
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "ftpServer"), "username"), "vars"), "id"), "html", null, true);
        echo "').val(dc['ftpusername']);
                        jQuery('#";
        // line 609
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "ftpServer"), "password"), "vars"), "id"), "html", null, true);
        echo "').val(dc['ftppassword']);
                        jQuery('#";
        // line 610
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idPreferredPayment"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idPreferredPayment']);
                        jQuery('#";
        // line 611
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idMarginGoesTo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idMarginGoesTo']);
                        jQuery('#";
        // line 612
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idNutrimeGoesTo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idNutrimeGoesTo']);

                        jQuery('#";
        // line 614
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryDownloadAccess"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isReportDeliveryDownloadAccess']);
                        jQuery('#";
        // line 615
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryFtpServer"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isReportDeliveryFtpServer']);
                        jQuery('#";
        // line 616
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryPrintedBooklet"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isReportDeliveryPrintedBooklet']);
                        jQuery('#";
        // line 617
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isReportDeliveryDontChargeBooklet"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isReportDeliveryDontChargeBooklet']);
                        jQuery('#";
        // line 618
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "canCustomersDownloadReports"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['canCustomersDownloadReports']);

                        jQuery('#";
        // line 620
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur1"), "vars"), "id"), "html", null, true);
        echo "').val(dc['acquisiteurid1']);
                        jQuery('#";
        // line 621
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate1"), "vars"), "id"), "html", null, true);
        echo "').val(dc['commission1']);
                        jQuery('#";
        // line 622
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur2"), "vars"), "id"), "html", null, true);
        echo "').val(dc['acquisiteurid2']);
                        jQuery('#";
        // line 623
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate2"), "vars"), "id"), "html", null, true);
        echo "').val(dc['commission2']);
                        jQuery('#";
        // line 624
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idAcquisiteur3"), "vars"), "id"), "html", null, true);
        echo "').val(dc['acquisiteurid3']);
                        jQuery('#";
        // line 625
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "acquisiteurCommissionRate3"), "vars"), "id"), "html", null, true);
        echo "').val(dc['commission3']);

                        jQuery('#";
        // line 627
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "rebrandingNick"), "vars"), "id"), "html", null, true);
        echo "').val(dc['rebrandingnick']);
                        jQuery('#";
        // line 628
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "headerFileName"), "vars"), "id"), "html", null, true);
        echo "').val(dc['headerfilename']);
                        jQuery('#";
        // line 629
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "footerFileName"), "vars"), "id"), "html", null, true);
        echo "').val(dc['footerfilename']);
                        jQuery('#";
        // line 630
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "titleLogoFileName"), "vars"), "id"), "html", null, true);
        echo "').val(dc['titlelogofilename']);
                        jQuery('#";
        // line 631
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "boxLogoFileName"), "vars"), "id"), "html", null, true);
        echo "').val(dc['boxlogofilename']);

                        jQuery('#";
        // line 633
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isCustomerBeContacted"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isCustomerBeContacted']);
                        jQuery('#";
        // line 634
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isNutrimeOffered"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isNutrimeOffered']);
                        jQuery('#";
        // line 635
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isNotContacted"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isNotContacted']);
                        jQuery('#";
        // line 636
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDestroySample"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isDestroySample']);
                        jQuery('#";
        // line 637
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "sampleCanBeUsedForScience"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['sampleCanBeUsedForScience']);
                        jQuery('#";
        // line 638
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isFutureResearchParticipant"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isFutureResearchParticipant']);

                        jQuery('#";
        // line 640
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idVisibleGroupProduct"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idVisibleGroupProduct']);


                        jQuery('#";
        // line 643
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idRecallGoesTo"), "vars"), "id"), "html", null, true);
        echo "').val(dc['idRecallGoesTo']);

                        jQuery('#";
        // line 645
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').prop('checked', dc['isDelayDownloadReport']);
                        jQuery('#";
        // line 646
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').val(dc['numberDateDelayDownloadReport']);

                        var delayDownload = jQuery('#";
        // line 648
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "isDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "');
                        if (delayDownload.is(':checked')) {
                            jQuery('#";
        // line 650
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').show();
                        }
                        else {
                            jQuery('#";
        // line 653
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "numberDateDelayDownloadReport"), "vars"), "id"), "html", null, true);
        echo "').hide();
                        }

                        // update Product section
                        updateCategoryProductVisibility();

                        onChangeReportDeliveryId();

                        if (dc['isFixedReportGoesTo']) {
                            var labelNode = jQuery('#";
        // line 662
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportGoesToId"), "vars"), "id"), "html", null, true);
        echo "').parent().find('label');
                            labelNode.append(jQuery(\"<br/><b style='color: red'>WARNING! For the select DC, report must always go to \" + dc['reportGoesToName'] + \"</b>\"));
                        }
                        else {
                            var labelNode = jQuery('#";
        // line 666
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reportGoesToId"), "vars"), "id"), "html", null, true);
        echo "').parent().find('label');
                            labelNode.html('');
                        }
                    });
                }

                jQuery(document).ready(function() {
                    onChangeReportDeliveryId();
                    jQuery('#main-order-tabs').tabs();
                });
            </script>

            <style>
                .content {
                    line-height: normal;
                }
                .stdform2 label {
                    padding: 5px 20px 5px 20px;
                }
            </style>
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Customer:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1476 => 666,  1469 => 662,  1457 => 653,  1451 => 650,  1446 => 648,  1441 => 646,  1437 => 645,  1432 => 643,  1426 => 640,  1421 => 638,  1417 => 637,  1413 => 636,  1409 => 635,  1405 => 634,  1401 => 633,  1396 => 631,  1392 => 630,  1388 => 629,  1384 => 628,  1380 => 627,  1375 => 625,  1371 => 624,  1367 => 623,  1363 => 622,  1359 => 621,  1355 => 620,  1350 => 618,  1346 => 617,  1342 => 616,  1338 => 615,  1334 => 614,  1329 => 612,  1325 => 611,  1321 => 610,  1317 => 609,  1313 => 608,  1309 => 607,  1305 => 606,  1301 => 605,  1297 => 604,  1293 => 603,  1289 => 602,  1285 => 601,  1280 => 599,  1276 => 598,  1272 => 597,  1268 => 596,  1264 => 595,  1259 => 593,  1255 => 592,  1251 => 591,  1247 => 590,  1243 => 589,  1239 => 588,  1235 => 587,  1231 => 586,  1227 => 585,  1223 => 584,  1219 => 583,  1215 => 582,  1202 => 572,  1198 => 571,  1185 => 561,  1180 => 559,  1176 => 558,  1170 => 554,  1166 => 552,  1158 => 549,  1149 => 547,  1145 => 546,  1142 => 545,  1136 => 543,  1134 => 542,  1129 => 541,  1125 => 540,  1122 => 539,  1120 => 538,  1114 => 535,  1107 => 531,  1103 => 530,  1095 => 525,  1086 => 518,  1074 => 514,  1068 => 511,  1064 => 510,  1060 => 509,  1056 => 508,  1050 => 506,  1046 => 505,  1041 => 503,  1034 => 499,  1030 => 498,  1026 => 497,  1022 => 496,  1018 => 495,  1014 => 494,  1010 => 493,  1006 => 492,  1002 => 491,  998 => 490,  994 => 489,  989 => 487,  985 => 486,  980 => 484,  976 => 483,  972 => 482,  968 => 481,  964 => 480,  959 => 478,  955 => 477,  951 => 476,  947 => 475,  943 => 474,  939 => 473,  935 => 472,  931 => 471,  927 => 470,  923 => 469,  916 => 465,  912 => 464,  908 => 463,  904 => 462,  900 => 461,  896 => 460,  891 => 458,  887 => 457,  883 => 456,  879 => 455,  875 => 454,  871 => 453,  866 => 451,  862 => 450,  858 => 449,  854 => 448,  850 => 447,  846 => 446,  842 => 445,  838 => 444,  834 => 443,  830 => 442,  826 => 441,  822 => 440,  817 => 438,  813 => 437,  806 => 433,  802 => 432,  798 => 431,  794 => 430,  790 => 429,  786 => 428,  782 => 427,  778 => 426,  774 => 425,  770 => 424,  766 => 423,  759 => 419,  755 => 418,  751 => 417,  747 => 416,  743 => 415,  739 => 414,  735 => 413,  731 => 412,  727 => 411,  720 => 407,  716 => 406,  712 => 405,  708 => 404,  704 => 403,  700 => 402,  696 => 401,  692 => 400,  688 => 399,  684 => 398,  680 => 397,  676 => 396,  672 => 395,  667 => 392,  663 => 390,  661 => 389,  642 => 372,  637 => 370,  626 => 361,  623 => 360,  621 => 359,  616 => 357,  612 => 356,  608 => 355,  604 => 354,  600 => 353,  596 => 352,  592 => 351,  588 => 350,  582 => 347,  578 => 346,  574 => 345,  570 => 344,  566 => 343,  562 => 342,  558 => 341,  554 => 340,  550 => 339,  546 => 338,  540 => 335,  537 => 334,  531 => 331,  527 => 329,  525 => 328,  520 => 326,  516 => 325,  495 => 307,  481 => 296,  467 => 285,  440 => 261,  430 => 254,  415 => 242,  400 => 230,  378 => 211,  323 => 159,  307 => 146,  300 => 142,  296 => 141,  292 => 140,  288 => 139,  284 => 138,  280 => 137,  275 => 135,  271 => 134,  267 => 133,  263 => 132,  259 => 131,  255 => 130,  251 => 129,  247 => 128,  243 => 127,  227 => 114,  221 => 111,  216 => 109,  210 => 106,  204 => 103,  199 => 101,  141 => 46,  137 => 45,  133 => 44,  126 => 40,  101 => 17,  98 => 16,  53 => 15,  50 => 14,  43 => 9,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}
