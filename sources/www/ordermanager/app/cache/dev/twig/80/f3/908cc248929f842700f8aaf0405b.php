<?php

/* LeepAdminBundle:Login:show.html.twig */
class __TwigTemplate_80f3908cc248929f842700f8aaf0405b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle::Base.html.twig");

        $this->blocks = array(
            'BODY' => array($this, 'block_BODY'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle::Base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_BODY($context, array $blocks = array())
    {
        // line 4
        echo "<body class=\"login\">
    <div class=\"loginbox radius3\">
        <div class=\"loginboxinner radius3\">
        <div class=\"loginheader\">
            <h1 class=\"bebas\">Sign In</h1>
            <div class=\"logo\"><!-- <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/blocktemplatestarlight"), "html", null, true);
        echo "/images/starlight_admin_template_logo.png\" alt=\"\" />--></div>
        </div>

        <div class=\"loginform\">
            ";
        // line 13
        if ((!twig_test_empty($this->getContext($context, "form_error")))) {
            // line 14
            echo "                <div class=\"loginerror\" style=\"display: block\"><p>Login failed</p></div>
            ";
        }
        // line 16
        echo "
            <form id=\"login\" action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "form_url"), "html", null, true);
        echo "\" method=\"post\">
                <p>
                    <label for=\"username\" class=\"bebas\">Username</label>
                    <input type=\"text\" id=\"username\" name=\"username\" class=\"radius2\" />
                </p>
                <p>
                    <label for=\"password\" class=\"bebas\">Password</label>
                    <input type=\"password\" id=\"password\" name=\"password\" class=\"radius2\" />
                </p>
                <p>
                    <button class=\"radius3 bebas\">Sign in</button>
                </p>
                <!--<p><a href=\"\" class=\"whitelink small\">Can't access your account?</a></p>-->
            </form>
        </div>
        </div>
    </div>
</body>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Login:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 17,  49 => 16,  45 => 14,  43 => 13,  36 => 9,  29 => 4,  26 => 3,);
    }
}
