<?php

/* LeepAdminBundle:FormTypes:app_gene_select.html.twig */
class __TwigTemplate_1b2f51d53e74016605adc93ef18e15f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_gene_select_row' => array($this, 'block_app_gene_select_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_gene_select_row', $context, $blocks);
    }

    public function block_app_gene_select_row($context, array $blocks = array())
    {
        // line 3
        echo "    <p id=\"field_row_form_orderInfo_categories\">
        <label for=\"form_orderInfo_categories\">Categories</label>
        <span class=\"field\">
            <span>
                ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "categories"), 'widget');
        echo "
            </span>
            <span style=\"vertical-align: top; margin-left: 20px; display: inline-block\" id=\"orderInfoSelectedCategories\">
            </span>
            <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">  
                ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "categories"), 'errors');
        echo "                              
            </label>
        </span>
    </p>
    <p id=\"field_row_form_orderInfo_products\">
        <label for=\"form_orderInfo_products\">
            Products
        </label>
        <span class=\"field\">
            <span>
                ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "products"), 'widget');
        echo "
            </span>
            <span style=\"vertical-align: top; margin-left: 20px; display: inline-block\" id=\"orderInfoSelectedProducts\">
            </span>
            <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">       
                ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "products"), 'errors');
        echo "                                                       
            </label>
        </span>
    </p>
    <p>
        <label>&nbsp;</label>
        <span class=\"field\">
            <span>
                <a href='#' class=\"showAll\">SHOW ALL</a> / <a href='#' class=\"showLess\">SHOW LESS</a>
            </span>
        </span> 
    </p>

    <p>
        <label>&nbsp;</label>
        <span class=\"field\">
            <span>
                <a href=\"javascript:viewProductGenes()\">View product genes</a>";
        // line 44
        if (($this->getContext($context, "id") > 0)) {
            echo " / <a class=\"popupButton\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "viewAllGeneUrl"), "html", null, true);
            echo "\">View all genes captured</a>";
        }
        // line 45
        echo "            </span>
        </span>
    </p>

    ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "specialProducts"), 'row');
        echo "

    <br/>
    <div class=\"widgetbox\" style=\"width: 100%\">
        <div class=\"title\"><h2 class=\"tabbed\"><span>Questions (OLD)</span></h2></div>
        <div class=\"widgetcontent\" style=\"display: none\">        
            ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "productQuestions"));
        foreach ($context['_seq'] as $context["productId"] => $context["questions"]) {
            // line 56
            echo "                <div id=\"product_div_";
            echo twig_escape_filter($this->env, $this->getContext($context, "productId"), "html", null, true);
            echo "\" style=\"display: none\">
                    ";
            // line 57
            $context["ctrlId"] = ("section_product_" . $this->getContext($context, "productId"));
            // line 58
            echo "                    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'row');
            echo "
                    ";
            // line 59
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "questions"));
            foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
                // line 60
                echo "                        ";
                $context["ctrlId"] = ((("product_question_" . $this->getContext($context, "productId")) . "_") . $this->getAttribute($this->getContext($context, "question"), "id"));
                // line 61
                echo "                        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'row');
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 63
            echo "                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['productId'], $context['questions'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 65
        echo "        </div>
    </div>
    <br/>

    <script type=\"text/javascript\">    


    var categoryProductLink = JSON.parse('";
        // line 72
        echo $this->getContext($context, "categoryProductLinkJson");
        echo "');
    var specialProductLink = JSON.parse('";
        // line 73
        echo $this->getContext($context, "specialProductLinkJson");
        echo "');
    var specialProductCategoryLink = JSON.parse('";
        // line 74
        echo $this->getContext($context, "specialProductCategoryLinkJson");
        echo "');
    var visibleGroupProductCategoryLink = JSON.parse('";
        // line 75
        echo $this->getContext($context, "visibleGroupProductCategoryLinkJson");
        echo "');
    var visibleGroupProductProductLink = JSON.parse('";
        // line 76
        echo $this->getContext($context, "visibleGroupProductProductLinkJson");
        echo "');
    var productVisibility = false;

    function viewProductGenes() {
        var targetUrl = \"";
        // line 80
        echo twig_escape_filter($this->env, $this->getContext($context, "viewProductGeneUrl"), "html", null, true);
        echo "&products=\";
        jQuery('#";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) {
            var idProduct = jQuery(this).val();
            targetUrl += \",\" + idProduct;
        });

        jQuery.colorbox({
            href: targetUrl,
            iframe: true,
            width:  \"90%\",
            height: \"90%\"
        });
                                
        return false;
    }

    function onChangeCategories() {
        updateCategoryProduct();
    }

    function onChangeProducts() {
        updateCategoryProduct();
    }

    jQuery(document).ready(function(){
        // when click on 'show all'
        jQuery('.showAll').each(function(index, value) {
            jQuery(this).click(function(){
                productVisibility = true;
                updateCategoryProductVisibility();
                return false;
            });
        });
        jQuery('.showLess').each(function(index, value) {
            jQuery(this).click(function(){
                productVisibility = false;
                updateCategoryProductVisibility();
                return false;
            });
        });

        // Save original list
        var selectedSpecialProducts = {};
        jQuery('#";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "specialProducts"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) {
            var specialProductId = jQuery(this).val();
            selectedSpecialProducts[specialProductId] = 1;
        });

        updateCategoryProduct();
        updateCategoryProductVisibility();

        jQuery('#";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "specialProducts"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            var specialProductId = jQuery(this).val();
            if (selectedSpecialProducts[specialProductId] == 1) {                
                jQuery(this).attr('selected', 'selected');
            }
        });
        window.scrollTo(0,0);
    });

    function updateCategoryProductVisibility() {
        jQuery('#";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categories"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            jQuery(this).css('display', '');
        });

        jQuery('#";
        // line 145
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            jQuery(this).css('display', '');
        });

        var idVisibleGroupProduct = jQuery('#form_idVisibleGroupProduct').val();
        if (idVisibleGroupProduct && (productVisibility == false)) {
            var dcVisibleCategories = visibleGroupProductCategoryLink[idVisibleGroupProduct];
            var dcVisibleProducts = visibleGroupProductProductLink[idVisibleGroupProduct];
            
            jQuery('#";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categories"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                var idCategory = jQuery(this).val();
                if ((!dcVisibleCategories || (typeof dcVisibleCategories[idCategory] == 'undefined')) && 
                    (jQuery(this).is(':selected') == false)) {
                    jQuery(this).css('display', 'none');
                }
            });

            jQuery('#";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                var idProduct = jQuery(this).val();
                if ((!dcVisibleProducts || (typeof dcVisibleProducts[idProduct] == 'undefined')) &&
                    (jQuery(this).is(':selected') == false)) {
                    jQuery(this).css('display', 'none');
                }
            });
        }
    }

    function updateCategoryProduct() {
        var productSelected = {};

        // Compute product selected
        jQuery('#";
        // line 176
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categories"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) { 
            var categoryId = jQuery(this).val();
            var products = categoryProductLink[categoryId];
            for (id in products) {
                var productId = products[id];
                productSelected[productId] = 1;
            }
        });                    

        // Update product box
        jQuery('#";
        // line 186
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            var productId = jQuery(this).val();
            if (productSelected[productId] == 1) {                
                jQuery(this).attr('selected', 'selected');

                var html = jQuery(this).html();
                if (!endsWith(html, ' (*)')) {
                    jQuery(this).html(html + ' (*)');
                }
            }
            else {
                var html = jQuery(this).html();
                if (endsWith(html, ' (*)')) {
                    jQuery(this).removeAttr('selected');
                    html = html.substr(0, html.length - 4);
                    jQuery(this).html(html);
                }
            }
        });        

        // Update special products
        jQuery('#";
        // line 207
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "specialProducts"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            jQuery(this).remove();
        });

        var idDistributionChannel = jQuery('#form_distributionChannelId').val();
        jQuery('#";
        // line 212
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) { 
            var productId = jQuery(this).val();
            if (idDistributionChannel != 0) {
                var specialProducts = specialProductLink[idDistributionChannel];
                if (typeof specialProducts != 'undefined') {
                    var specialProductBox = jQuery('#";
        // line 217
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "specialProducts"), "vars"), "id"), "html", null, true);
        echo "');
                    if (typeof specialProducts[productId] != 'undefined') {
                        var specialProduct = specialProducts[productId];
                        specialProductBox.append('<option value='+ specialProduct['id'] + '>' + specialProduct['name'] + '</option>');
                    }
                }
            }
        });

        jQuery('#";
        // line 226
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categories"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) {
            var categoryId = jQuery(this).val();
            if (idDistributionChannel != 0) {
                var specialProducts = specialProductCategoryLink[idDistributionChannel];
                if (typeof specialProducts != 'undefined') {
                    var specialProductBox = jQuery('#";
        // line 231
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "specialProducts"), "vars"), "id"), "html", null, true);
        echo "');
                    if (typeof specialProducts[categoryId] != 'undefined') {
                        var specialProduct = specialProducts[categoryId];
                        specialProductBox.append('<option value='+ specialProduct['id'] + '>' + specialProduct['name'] + '</option>');
                    }
                }
            }
        });

        // Update question
        jQuery('#";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
            var productId = jQuery(this).val();
            if (jQuery(this).attr('selected')) {
                jQuery('#product_div_' + productId).css('display', '');
            }
            else {
                jQuery('#product_div_' + productId).css('display', 'none');
            }
        });

        // Update categories selected        
        var orderInfoSelectedCategories = jQuery('#orderInfoSelectedCategories');
        orderInfoSelectedCategories.html('');
        var content = '';
        jQuery('#";
        // line 255
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categories"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) { 
            content += jQuery(this).html() + \"<br/>\";
        });
        orderInfoSelectedCategories.html(content);

        // Update products selected        
        var orderInfoSelectedProducts = jQuery('#orderInfoSelectedProducts');
        orderInfoSelectedProducts.html('');
        content = '';
        jQuery('#";
        // line 264
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "products"), "vars"), "id"), "html", null, true);
        echo " :selected').each(function(index, value) { 
            content += jQuery(this).html() + \"<br/>\";
        });
        orderInfoSelectedProducts.html(content);

    }
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:app_gene_select.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  409 => 264,  397 => 255,  367 => 231,  347 => 217,  307 => 186,  266 => 154,  247 => 141,  174 => 80,  167 => 76,  132 => 79,  124 => 71,  1129 => 306,  1124 => 304,  1117 => 300,  1113 => 299,  1109 => 298,  1105 => 297,  1101 => 296,  1097 => 295,  1091 => 294,  1085 => 291,  1079 => 290,  1075 => 289,  1071 => 288,  1065 => 287,  1059 => 284,  1055 => 283,  1051 => 282,  1045 => 281,  1040 => 279,  1036 => 278,  1030 => 277,  1023 => 273,  1017 => 272,  1009 => 267,  1003 => 266,  998 => 264,  992 => 263,  987 => 261,  981 => 260,  976 => 258,  972 => 257,  966 => 256,  961 => 254,  955 => 253,  950 => 251,  946 => 250,  940 => 249,  935 => 247,  931 => 246,  925 => 245,  920 => 243,  914 => 242,  907 => 238,  903 => 237,  899 => 236,  895 => 235,  889 => 234,  884 => 232,  880 => 231,  874 => 230,  869 => 228,  865 => 227,  861 => 226,  857 => 225,  853 => 224,  847 => 223,  842 => 221,  838 => 220,  834 => 219,  830 => 218,  826 => 217,  820 => 216,  815 => 214,  811 => 213,  807 => 212,  803 => 211,  797 => 210,  792 => 208,  788 => 207,  784 => 206,  780 => 205,  776 => 204,  772 => 203,  768 => 202,  764 => 201,  760 => 200,  756 => 199,  752 => 198,  746 => 197,  741 => 195,  737 => 194,  733 => 193,  729 => 192,  725 => 191,  721 => 190,  717 => 189,  713 => 188,  709 => 187,  705 => 186,  699 => 185,  694 => 183,  690 => 182,  686 => 181,  682 => 180,  676 => 179,  670 => 176,  666 => 175,  660 => 174,  655 => 172,  651 => 171,  647 => 170,  643 => 169,  637 => 168,  632 => 166,  628 => 165,  624 => 164,  620 => 163,  616 => 162,  612 => 161,  606 => 160,  601 => 158,  597 => 157,  593 => 156,  589 => 155,  585 => 154,  581 => 153,  576 => 151,  572 => 150,  568 => 149,  562 => 148,  555 => 144,  549 => 143,  544 => 141,  540 => 140,  536 => 139,  532 => 138,  526 => 137,  521 => 135,  517 => 134,  511 => 133,  506 => 131,  502 => 130,  498 => 129,  494 => 128,  488 => 127,  483 => 125,  477 => 124,  472 => 122,  468 => 121,  464 => 120,  460 => 119,  456 => 118,  452 => 117,  448 => 116,  444 => 115,  438 => 114,  433 => 112,  429 => 111,  425 => 110,  421 => 109,  415 => 108,  410 => 106,  402 => 104,  396 => 103,  391 => 101,  387 => 100,  381 => 99,  376 => 97,  372 => 96,  368 => 95,  364 => 94,  360 => 93,  354 => 92,  349 => 90,  339 => 212,  328 => 83,  324 => 82,  320 => 81,  284 => 72,  280 => 71,  276 => 70,  253 => 64,  218 => 55,  214 => 54,  202 => 51,  191 => 48,  180 => 45,  133 => 33,  100 => 23,  93 => 26,  25 => 2,  19 => 1,  95 => 45,  139 => 104,  194 => 16,  111 => 96,  149 => 37,  92 => 80,  196 => 50,  153 => 47,  148 => 44,  141 => 35,  123 => 60,  114 => 58,  112 => 57,  107 => 56,  102 => 69,  73 => 16,  249 => 63,  103 => 55,  326 => 168,  296 => 75,  292 => 74,  285 => 152,  261 => 66,  252 => 127,  240 => 124,  233 => 59,  186 => 14,  154 => 86,  143 => 105,  128 => 110,  125 => 36,  108 => 25,  77 => 17,  342 => 168,  337 => 165,  316 => 80,  312 => 79,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 69,  257 => 65,  246 => 14,  236 => 116,  227 => 58,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 47,  147 => 68,  142 => 65,  120 => 57,  32 => 5,  99 => 28,  87 => 22,  83 => 21,  70 => 15,  62 => 27,  43 => 6,  89 => 20,  75 => 21,  72 => 18,  38 => 8,  82 => 44,  67 => 17,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 105,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 241,  373 => 113,  370 => 112,  359 => 226,  355 => 103,  348 => 100,  345 => 89,  340 => 96,  332 => 84,  327 => 90,  317 => 161,  304 => 77,  294 => 176,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 67,  262 => 65,  248 => 126,  243 => 58,  241 => 61,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 52,  204 => 39,  200 => 38,  184 => 37,  166 => 93,  163 => 75,  155 => 73,  152 => 82,  140 => 38,  118 => 70,  115 => 13,  106 => 67,  96 => 27,  91 => 2,  84 => 40,  81 => 18,  79 => 20,  76 => 20,  71 => 41,  69 => 15,  66 => 39,  56 => 10,  54 => 22,  49 => 12,  41 => 12,  34 => 6,  29 => 3,  264 => 93,  254 => 145,  244 => 125,  228 => 76,  224 => 75,  222 => 56,  213 => 72,  210 => 53,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 43,  169 => 32,  164 => 41,  162 => 51,  159 => 74,  156 => 49,  151 => 72,  146 => 78,  144 => 44,  137 => 34,  135 => 63,  129 => 32,  127 => 38,  119 => 59,  116 => 27,  110 => 74,  104 => 24,  101 => 28,  97 => 26,  88 => 45,  64 => 99,  60 => 13,  36 => 5,  23 => 2,  21 => 2,  85 => 19,  78 => 21,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 207,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 78,  303 => 111,  300 => 76,  291 => 98,  288 => 73,  282 => 71,  279 => 93,  277 => 162,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 62,  242 => 120,  237 => 60,  234 => 131,  225 => 103,  223 => 123,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 81,  176 => 44,  173 => 54,  171 => 53,  168 => 42,  160 => 90,  157 => 51,  150 => 41,  145 => 36,  138 => 36,  134 => 74,  130 => 56,  126 => 61,  121 => 76,  117 => 33,  113 => 68,  109 => 54,  105 => 27,  98 => 45,  94 => 49,  90 => 21,  86 => 20,  80 => 71,  74 => 16,  55 => 15,  50 => 14,  44 => 12,  39 => 5,  33 => 7,  26 => 2,  68 => 19,  61 => 12,  58 => 12,  48 => 8,  46 => 10,  42 => 7,  35 => 4,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 13,  63 => 13,  59 => 18,  57 => 12,  53 => 11,  51 => 8,  47 => 7,  45 => 12,  40 => 6,  37 => 5,  30 => 3,  27 => 3,);
    }
}
