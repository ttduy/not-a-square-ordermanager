<?php

/* LeepAdminBundle:Invoice:app_partner_customers_picker.html.twig */
class __TwigTemplate_bbb4aabbfb4bf439d1d12ed6c9fb617f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_partner_customers_picker_row' => array($this, 'block_app_partner_customers_picker_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_partner_customers_picker_row', $context, $blocks);
    }

    public function block_app_partner_customers_picker_row($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idPartner"), 'row');
        echo "
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "customerDataSessionKey"), 'row');
        echo "
    
    <p>
        <label>Customers</label>
        <span class=\"field\">
            <button class=\"radius2\" id=\"addCustomersBtn\">Add customers</button>
            <button class=\"radius2\" id=\"removeCustomersBtn\">Remove selected customers</button>
        </span>
    </p>

    ";
        // line 13
        $this->env->loadTemplate("LeepAdminBundle:Invoice:app_partner_customers_picker.html.twig", "2090367765")->display(array_merge($context, array("grid" => $this->getContext($context, "customerGridViewer"))));
        // line 43
        echo "
    <script type=\"text/javascript\">
        jQuery(document).ready(function() {
            jQuery('#addCustomersBtn').click(function(e) {
                e.preventDefault();

                var idPartner = jQuery('#";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idPartner"), "vars"), "id"), "html", null, true);
        echo "').val();
                if (idPartner == 0) {
                    alert(\"please select a partner first\");
                    return false;
                }

                jQuery.colorbox({
                    iframe: true,
                    href:   \"";
        // line 57
        echo $this->getContext($context, "customersPickerUrl");
        echo "&idPartner=\" + idPartner,
                    width:  \"75%\",
                    height: \"75%\",
                    onClosed: function() {
                        var customerTable = jQuery('#CustomerGridViewer');
                        var db = customerTable.dataTable();
                        db.fnDraw(false);
                    }
                });
            });

            jQuery('#removeCustomersBtn').click(function(e) {
                e.preventDefault();

                var checkboxes = jQuery('#";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "customerGridViewer"), "id", array(), "array"), "html", null, true);
        echo " .stdtableCheckBox');
                var selectedId = '';
                checkboxes.each(function(i, v) {
                    if (jQuery(this).is(':checked')) {
                        var id = jQuery(this).attr('id').substring(4);
                        selectedId += id + ',';
                    }
                });

                var targetUrl = '";
        // line 80
        echo $this->getContext($context, "removeCustomersUrl");
        echo "';
                jQuery.ajax({
                    url: targetUrl,
                    type: \"POST\",
                    data: { selectedId: selectedId },
                    context: document.body
                }).done(function(data) {
                    var customerTable = jQuery('#CustomerGridViewer');
                    var db = customerTable.dataTable();
                    db.fnDraw(false);
                }).error(function(jqXHR, textStatus, errorThrown ) {
                    alert(textStatus + \" \" + errorThrown);
                });                 
            });

            var currentIdPartner = 0;
            jQuery('#";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idPartner"), "vars"), "id"), "html", null, true);
        echo "')
            .focus(function() {
                currentIdPartner = jQuery(this).val();
            })
            .change(function() {
                var customerTable = jQuery('#CustomerGridViewer');
                var db = customerTable.dataTable();
                if (db.fnGetData().length > 0) {
                    if (!confirm(\"All current selected customers will be erased. Are you want to change partner?\")) {                        
                        jQuery(this).val(currentIdPartner);
                        return false;
                    }
                    
                    // Call AJAX
                    var targetUrl = '";
        // line 110
        echo $this->getContext($context, "removeCustomersUrl");
        echo "';
                    jQuery.ajax({
                        url: targetUrl,
                        type: \"POST\",
                        data: { isClearAll: true },
                        context: document.body
                    }).done(function(data) {
                        var customerTable = jQuery('#CustomerGridViewer');
                        var db = customerTable.dataTable();
                        db.fnDraw(false);
                    }).error(function(jqXHR, textStatus, errorThrown ) {
                        alert(textStatus + \" \" + errorThrown);
                    });
                }
            });
        });        
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Invoice:app_partner_customers_picker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  111 => 96,  149 => 46,  92 => 80,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 38,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 12,  143 => 67,  128 => 110,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 5,  99 => 27,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 6,  82 => 23,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 30,  96 => 27,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 13,  66 => 13,  56 => 12,  54 => 15,  49 => 12,  41 => 6,  34 => 10,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 63,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 44,  137 => 39,  135 => 18,  129 => 60,  127 => 38,  119 => 48,  116 => 56,  110 => 31,  104 => 44,  101 => 28,  97 => 26,  88 => 25,  64 => 99,  60 => 21,  36 => 12,  23 => 2,  21 => 1,  85 => 24,  78 => 22,  52 => 49,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 51,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 24,  80 => 71,  74 => 39,  55 => 15,  50 => 24,  44 => 43,  39 => 7,  33 => 4,  26 => 2,  68 => 19,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 13,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 57,  59 => 16,  57 => 10,  53 => 13,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}


/* LeepAdminBundle:Invoice:app_partner_customers_picker.html.twig */
class __TwigTemplate_bbb4aabbfb4bf439d1d12ed6c9fb617f_2090367765 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LeepAdminBundle:Invoice:customers_viewer_grid.html.twig");

        $this->blocks = array(
            'gridDrawCallback' => array($this, 'block_gridDrawCallback'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeepAdminBundle:Invoice:customers_viewer_grid.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_gridDrawCallback($context, array $blocks = array())
    {
        // line 15
        echo "            jQuery('#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " .invoiceCustomerGridField').change(function() {
                var targetUrl = '";
        // line 16
        echo $this->getContext($context, "updateCustomerDataUrl");
        echo "';
                var idCustomer = jQuery(this).attr('idCustomer');
                var field = jQuery(this).attr('field');;
                var value = jQuery(this).val();

                jQuery.ajax({
                    url: targetUrl,
                    type: \"POST\",
                    data: { idCustomer: idCustomer, field: field, value: value },
                    context: document.body
                }).done(function(data) {
                    
                }).error(function(jqXHR, textStatus, errorThrown ) {
                    
                });
            });
            
            jQuery(\".simpletip\").tooltip({ 
                bodyHandler: function() {   
                    return jQuery(this).find('.hide-content').html();
                }, 
                left: -100, 
                top: 20,
                showURL: false 
            }); 
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Invoice:app_partner_customers_picker.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 16,  111 => 96,  149 => 46,  92 => 80,  196 => 78,  153 => 47,  148 => 44,  141 => 42,  123 => 37,  114 => 31,  112 => 38,  107 => 36,  102 => 34,  73 => 14,  249 => 15,  103 => 10,  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 14,  154 => 12,  143 => 67,  128 => 110,  125 => 36,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 19,  246 => 14,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 5,  99 => 27,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 25,  75 => 21,  72 => 18,  38 => 6,  82 => 23,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 30,  96 => 27,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 13,  66 => 13,  56 => 12,  54 => 15,  49 => 12,  41 => 6,  34 => 10,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 63,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 44,  137 => 39,  135 => 18,  129 => 60,  127 => 38,  119 => 48,  116 => 56,  110 => 31,  104 => 44,  101 => 28,  97 => 26,  88 => 25,  64 => 99,  60 => 21,  36 => 12,  23 => 2,  21 => 1,  85 => 24,  78 => 22,  52 => 49,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 51,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 24,  80 => 71,  74 => 39,  55 => 15,  50 => 24,  44 => 43,  39 => 7,  33 => 4,  26 => 2,  68 => 19,  61 => 11,  58 => 12,  48 => 11,  46 => 47,  42 => 13,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 12,  63 => 57,  59 => 16,  57 => 10,  53 => 13,  51 => 30,  47 => 8,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}
