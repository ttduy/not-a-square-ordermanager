<?php

/* LeepAdminBundle:FormTypes:app_multi_material_used.html.twig */
class __TwigTemplate_82d80dc00d1f2d7524a4d5290c090017 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_multi_material_used_widget' => array($this, 'block_app_multi_material_used_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_multi_material_used_widget', $context, $blocks);
    }

    public function block_app_multi_material_used_widget($context, array $blocks = array())
    {
        // line 2
        echo "    <script type=\"text/javascript\">
    // STOCK USED UP
    function widgetStockUsedRowDelete(rowId) {
        jQuery('#' + rowId).remove();
    }
    var newIndexStockUsed = 1;
    function widgetStockUsedRowAddNew(appendAfter, id) {
        var collectionHolder = jQuery('#widgetStockUsedRowCollection_'+ id);
        var prototype = jQuery('#widgetStockUsedRowDataPrototype_'+ id).attr('data-prototype');

        newIndexStockUsed++;
        var newForm = prototype.replace(/__name__/g, newIndexStockUsed);

        var rowId = 'widgetStockUsedRow_' + newIndexStockUsed+ \"_\"+ id;

        var newWidget = jQuery('<tr id=\"' + rowId + '\"></tr>');
        var spanWidget = newWidget;

        if (appendAfter != '') {
            if (appendAfter == 'TOP') {
                collectionHolder.prepend(newWidget);
            }
            else {
                var a = collectionHolder.find(appendAfter);
                newWidget.insertAfter(a);
            }
        }
        else {
            collectionHolder.append(newWidget);
        }
        spanWidget.append(newForm);
        spanWidget.append('<td><a href=\"javascript:widgetStockUsedRowDelete(\\'' + rowId + '\\')\">Remove</a>&nbsp;&nbsp;<a href=\"javascript:widgetStockUsedRowAddNew(\\'' + '#' + rowId + '\\', \\'' + id + '\\')\">Insert</a></td>');
    }
    </script>
    <!-- MULTI PURCHASE -->
    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "id"), 'widget');
        echo "
    <br/>
    <h3 style=\"margin-top: 10px\"> ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "section"), 'widget');
        echo " </h3>
    <br/>
    <table id=\"widgetStockUsedRowCollection_";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
        echo "\" class=\"stdtable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" >
        <colgroup>
            <col class=\"con0\" width=\"10%\" />
            <col class=\"con0\" width=\"20%\" />
            <col class=\"con1\" width=\"20%\" />
            <col class=\"con0\" width=\"15%\" />
            <col class=\"con1\" width=\"20%\" />
            <col class=\"con0\" width=\"15%\" />
        </colgroup>
        <thead>
            <tr>
                <td class=\"head0\">In Use</td>
                <td class=\"head0\">Quantity in units</td>
                <td class=\"head0\">Used for</td>
                <td class=\"head0\">Date taken</td>
                <td class=\"head0\">Taken by</td>
                <td class=\"head0\"></td>
            </tr>
            <tr>
                <td colspan=\"6\" style=\"padding: 20px; font-weight: normal\">
                    <a id=\"widgetStockUsedRowPrependNewLink\" href=\"javascript:widgetStockUsedRowAddNew('TOP', ";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
        echo ")\">Add new record</a>
                </td>
            </tr>
        </thead>
        <tbody>
        ";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "usedList"));
        foreach ($context['_seq'] as $context["id"] => $context["widgetStockUsedRow"]) {
            // line 67
            echo "            <tr id=\"widgetStockUsedRow_";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
            echo "\">
                ";
            // line 68
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "widgetStockUsedRow"), 'widget');
            echo "
                <td>
                    <a href=\"javascript:widgetStockUsedRowDelete('widgetStockUsedRow_";
            // line 70
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
            echo "')\">Remove</a>&nbsp;
                    <a href=\"javascript:widgetStockUsedRowAddNew('#widgetStockUsedRow_";
            // line 71
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
            echo "', ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
            echo ")\">Insert</a>
                </td>
                <script type=\"text/javascript\">
                    newIndexStockUsed = newIndexStockUsed > ";
            // line 74
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo " ? newIndexStockUsed : ";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo ";
                </script>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['widgetStockUsedRow'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 78
        echo "        </tbody>
    </table>
    <p>
        <span class=\"field\" style=\"margin-left: -1px\">
            <a id=\"widgetStockUsedRowAddNewLink\" href=\"javascript:widgetStockUsedRowAddNew('', ";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
        echo ")\">Add new record</a>
        </span>
    </p>

    <div style=\"display: none\">
        <input type=\"radio\" name=\"inUseMaterial\" value=\"0\">
    </div>

    <div id=\"widgetStockUsedRowDataPrototype_";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "id"), "vars"), "value"), "html", null, true);
        echo "\" data-prototype=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "usedList"), "vars"), "prototype"), 'widget'));
        echo "\" style=\"display: none\">
    </div>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:FormTypes:app_multi_material_used.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  124 => 71,  1129 => 306,  1124 => 304,  1117 => 300,  1113 => 299,  1109 => 298,  1105 => 297,  1101 => 296,  1097 => 295,  1091 => 294,  1085 => 291,  1079 => 290,  1075 => 289,  1071 => 288,  1065 => 287,  1059 => 284,  1055 => 283,  1051 => 282,  1045 => 281,  1040 => 279,  1036 => 278,  1030 => 277,  1023 => 273,  1017 => 272,  1009 => 267,  1003 => 266,  998 => 264,  992 => 263,  987 => 261,  981 => 260,  976 => 258,  972 => 257,  966 => 256,  961 => 254,  955 => 253,  950 => 251,  946 => 250,  940 => 249,  935 => 247,  931 => 246,  925 => 245,  920 => 243,  914 => 242,  907 => 238,  903 => 237,  899 => 236,  895 => 235,  889 => 234,  884 => 232,  880 => 231,  874 => 230,  869 => 228,  865 => 227,  861 => 226,  857 => 225,  853 => 224,  847 => 223,  842 => 221,  838 => 220,  834 => 219,  830 => 218,  826 => 217,  820 => 216,  815 => 214,  811 => 213,  807 => 212,  803 => 211,  797 => 210,  792 => 208,  788 => 207,  784 => 206,  780 => 205,  776 => 204,  772 => 203,  768 => 202,  764 => 201,  760 => 200,  756 => 199,  752 => 198,  746 => 197,  741 => 195,  737 => 194,  733 => 193,  729 => 192,  725 => 191,  721 => 190,  717 => 189,  713 => 188,  709 => 187,  705 => 186,  699 => 185,  694 => 183,  690 => 182,  686 => 181,  682 => 180,  676 => 179,  670 => 176,  666 => 175,  660 => 174,  655 => 172,  651 => 171,  647 => 170,  643 => 169,  637 => 168,  632 => 166,  628 => 165,  624 => 164,  620 => 163,  616 => 162,  612 => 161,  606 => 160,  601 => 158,  597 => 157,  593 => 156,  589 => 155,  585 => 154,  581 => 153,  576 => 151,  572 => 150,  568 => 149,  562 => 148,  555 => 144,  549 => 143,  544 => 141,  540 => 140,  536 => 139,  532 => 138,  526 => 137,  521 => 135,  517 => 134,  511 => 133,  506 => 131,  502 => 130,  498 => 129,  494 => 128,  488 => 127,  483 => 125,  477 => 124,  472 => 122,  468 => 121,  464 => 120,  460 => 119,  456 => 118,  452 => 117,  448 => 116,  444 => 115,  438 => 114,  433 => 112,  429 => 111,  425 => 110,  421 => 109,  415 => 108,  410 => 106,  402 => 104,  396 => 103,  391 => 101,  387 => 100,  381 => 99,  376 => 97,  372 => 96,  368 => 95,  364 => 94,  360 => 93,  354 => 92,  349 => 90,  339 => 88,  328 => 83,  324 => 82,  320 => 81,  284 => 72,  280 => 71,  276 => 70,  253 => 64,  218 => 55,  214 => 54,  202 => 51,  191 => 48,  180 => 45,  133 => 33,  100 => 23,  93 => 26,  25 => 2,  19 => 1,  95 => 45,  139 => 104,  194 => 16,  111 => 96,  149 => 37,  92 => 80,  196 => 50,  153 => 47,  148 => 44,  141 => 35,  123 => 31,  114 => 31,  112 => 26,  107 => 36,  102 => 66,  73 => 16,  249 => 63,  103 => 26,  326 => 168,  296 => 75,  292 => 74,  285 => 152,  261 => 66,  252 => 127,  240 => 124,  233 => 59,  186 => 14,  154 => 39,  143 => 105,  128 => 110,  125 => 36,  108 => 25,  77 => 17,  342 => 168,  337 => 165,  316 => 80,  312 => 79,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 69,  257 => 65,  246 => 14,  236 => 116,  227 => 58,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 47,  147 => 68,  142 => 68,  120 => 57,  32 => 5,  99 => 28,  87 => 22,  83 => 21,  70 => 20,  62 => 15,  43 => 8,  89 => 20,  75 => 21,  72 => 18,  38 => 8,  82 => 18,  67 => 17,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 105,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 89,  340 => 96,  332 => 84,  327 => 90,  317 => 161,  304 => 77,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 67,  262 => 65,  248 => 126,  243 => 58,  241 => 61,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 52,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 90,  155 => 27,  152 => 82,  140 => 38,  118 => 70,  115 => 13,  106 => 67,  96 => 27,  91 => 2,  84 => 40,  81 => 18,  79 => 20,  76 => 20,  71 => 41,  69 => 15,  66 => 39,  56 => 12,  54 => 10,  49 => 12,  41 => 6,  34 => 6,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 56,  213 => 72,  210 => 53,  208 => 70,  201 => 142,  199 => 154,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 43,  169 => 32,  164 => 41,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 78,  144 => 44,  137 => 34,  135 => 18,  129 => 32,  127 => 38,  119 => 48,  116 => 27,  110 => 30,  104 => 24,  101 => 28,  97 => 26,  88 => 21,  64 => 99,  60 => 13,  36 => 5,  23 => 2,  21 => 1,  85 => 19,  78 => 21,  52 => 9,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 78,  303 => 111,  300 => 76,  291 => 98,  288 => 73,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 20,  256 => 70,  245 => 62,  242 => 120,  237 => 60,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 15,  183 => 144,  181 => 66,  178 => 35,  176 => 44,  173 => 54,  171 => 53,  168 => 42,  160 => 40,  157 => 51,  150 => 41,  145 => 36,  138 => 36,  134 => 74,  130 => 56,  126 => 16,  121 => 35,  117 => 33,  113 => 68,  109 => 54,  105 => 27,  98 => 45,  94 => 61,  90 => 21,  86 => 20,  80 => 71,  74 => 16,  55 => 15,  50 => 14,  44 => 12,  39 => 9,  33 => 4,  26 => 2,  68 => 19,  61 => 37,  58 => 12,  48 => 8,  46 => 10,  42 => 7,  35 => 5,  31 => 4,  28 => 4,  170 => 83,  165 => 53,  65 => 14,  63 => 13,  59 => 18,  57 => 12,  53 => 11,  51 => 16,  47 => 10,  45 => 12,  40 => 6,  37 => 5,  30 => 3,  27 => 3,);
    }
}
