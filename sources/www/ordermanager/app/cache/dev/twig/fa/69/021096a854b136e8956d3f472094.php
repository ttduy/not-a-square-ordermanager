<?php

/* LeepAdminBundle:Lead:form.html.twig */
class __TwigTemplate_fa69021096a854b136e8956d3f472094 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:Lead:form.html.twig", "700438345")->display(array_merge($context, array("form" => $this->getContext($context, "form"))));
        // line 5
        echo "
    <script type=\"text/javascript\">
        
        var myData = JSON.parse(";
        // line 8
        echo twig_jsonencode_filter($this->getContext($context, "myData"));
        echo ");

        function onChangeReminderType() {
            var idReminderIn = jQuery('#";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idReminderIn"), "vars"), "id"), "html", null, true);
        echo "').val();
            if (idReminderIn == 1) { // Specific date
                jQuery('#field_row_";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reminderDate"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'block');
            } 
            else {
                jQuery('#field_row_";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "reminderDate"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
            }
        }

         jQuery(document).ready(function() {
            jQuery('#";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "idReminderIn"), "vars"), "id"), "html", null, true);
        echo "').change(function() {
                onChangeReminderType();
            })
            onChangeReminderType();

            jQuery(\"a\").click(function(){
                setTimeout(function(){
                    setLastStatus();
                }, 300)
            });
        });

        function onChangeCategoryId(){
            var leadCategoryId = jQuery('#";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "category"), "vars"), "id"), "html", null, true);
        echo "').val();
            var validStatus = myData.data[leadCategoryId];
            var children = jQuery('#container_form_statusList').children('p');

            if (children) {
                for (var i = 0; i < children.length; i++) {
                    var child = jQuery(children[i]).children('span')[0];
                    var selection = jQuery(child).children('select')[0];
                    var options = jQuery(selection).children('option');
                    for (var j = 0; j < options.length; j++) {
                        var option = options[j];
                        if (jQuery(option).val() in validStatus) {
                            jQuery(option).show();
                        } else {
                            jQuery(option).hide();
                        }
                    
                    }

                    // hard code set
                    jQuery(selection).val(0);
                }
            }
        }

        function setLastStatus(){
            var leadCategoryId = jQuery('#";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "category"), "vars"), "id"), "html", null, true);
        echo "').val();
            var validStatus = myData.data[leadCategoryId];
            var children = jQuery('#container_form_statusList').children('p').length - 1;

            var lastSelect = jQuery('#form_statusList_' + children + '_status');
            var options = jQuery(lastSelect).children('option');
            for (var j = 0; j < options.length; j++){
                var option = options[j];
                if(jQuery(option).val() in validStatus){
                    jQuery(option).show();
                }
                else {
                    jQuery(option).hide();
                }
            }
        }

    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 60,  78 => 34,  62 => 21,  54 => 16,  48 => 13,  43 => 11,  37 => 8,  32 => 5,  29 => 3,  26 => 2,);
    }
}


/* LeepAdminBundle:Lead:form.html.twig */
class __TwigTemplate_fa69021096a854b136e8956d3f472094_700438345 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Basic.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Basic.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 60,  78 => 34,  62 => 21,  54 => 16,  48 => 13,  43 => 11,  37 => 8,  32 => 5,  29 => 3,  26 => 2,);
    }
}
