<?php

/* DesignStarlightBundle:Layout:NormalPage.html.twig */
class __TwigTemplate_293c137e23c4df98c851b4af0938f8e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle::Base.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'MainContentTop' => array($this, 'block_MainContentTop'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle::Base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"maincontent noright\">
        <div class=\"maincontentinner\">
            ";
        // line 6
        $this->displayBlock('MainContentTop', $context, $blocks);
        // line 8
        echo "            <ul class=\"maintabmenu\">
                ";
        // line 9
        $this->env->loadTemplate("DesignStarlightBundle:Component:MainTabs.html.twig")->display(array_merge($context, array("tabs" => $this->getContext($context, "mainTabs"))));
        // line 10
        echo "            </ul>
            <div class=\"content\">
                ";
        // line 12
        $this->displayBlock('MainContentInner', $context, $blocks);
        // line 14
        echo "            </div>
        </div>
        <div class=\"footer\">
            <p></p>
        </div><!--footer-->
    </div>
";
    }

    // line 6
    public function block_MainContentTop($context, array $blocks = array())
    {
        // line 7
        echo "            ";
    }

    // line 12
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 13
        echo "                ";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 13,  61 => 7,  58 => 6,  48 => 14,  46 => 12,  42 => 10,  35 => 6,  31 => 4,  28 => 3,  170 => 17,  165 => 16,  65 => 12,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 9,  37 => 8,  30 => 4,  27 => 3,);
    }
}
