<?php

/* LeepAdminBundle:NutriMe:app_nutri_me_parsed.html.twig */
class __TwigTemplate_b29f4b2217a3ab6911ee262a96ccf0cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_nutri_me_parsed_row' => array($this, 'block_app_nutri_me_parsed_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_nutri_me_parsed_row', $context, $blocks);
    }

    public function block_app_nutri_me_parsed_row($context, array $blocks = array())
    {
        // line 2
        echo "<br/>
<h2>Code parsed</h2>
<br/>

";
        // line 6
        if (array_key_exists("error", $context)) {
            // line 7
            echo "    Error in code: ";
            echo twig_escape_filter($this->env, $this->getContext($context, "error"), "html", null, true);
            echo " <br/>
";
        } else {
            // line 9
            echo "    <div class=\"one_half\">
        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
            <thead>
                ";
            // line 12
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "info"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 13
                echo "                    <tr>
                        <th class=\"head0\" style=\"width: 35%; text-align: right\">";
                // line 14
                echo $this->getAttribute($this->getContext($context, "r"), "label", array(), "array");
                echo "</th>
                        <th style=\"width: 65%; text-align: left; font-weight: normal\">";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "value", array(), "array"), "html", null, true);
                echo "</th>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 18
            echo "            </thead>
        </table>

        ";
            // line 21
            if ((!array_key_exists("isMade", $context))) {
                // line 22
                echo "        <br/>
        <button id=\"startProductionBtn\" class=\"submit radius2\">Start production</button>
        <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#startProductionBtn').click(function() {
                    window.location = '";
                // line 27
                echo $this->getContext($context, "startProductionLink");
                echo "';
                    return false;
                });
            });
        </script>
        ";
            }
            // line 33
            echo "    </div>

    <div class=\"one_half last\">
        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
            <thead>
                <tr>
                    <th class=\"head0\" style=\"width: 35%; text-align: right\">Type</th>
                    <th style=\"width: 65%; text-align: left; font-weight: normal\">Amount</th>
                </tr>
                ";
            // line 42
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "drugs"));
            foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
                // line 43
                echo "                    <tr>
                        <th class=\"head0\" style=\"width: 35%; text-align: right\">";
                // line 44
                echo $this->getAttribute($this->getContext($context, "d"), "type", array(), "array");
                echo "</th>
                        <th style=\"width: 65%; text-align: left; font-weight: normal\">";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "d"), "amount", array(), "array"), "html", null, true);
                echo "</th>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 48
            echo "            </thead>
        </table>
    </div>

    <br clear=\"all\" /><br />
";
        }
        // line 54
        echo "
";
        // line 55
        if (array_key_exists("isMade", $context)) {
            // line 56
            echo "
    <h2>History</h2>
    <br/>
    <div class=\"one_half\">
        <div class=\"contenttitle radiusbottom0\">
                    <h2 class=\"form\">
                        <span>Summary</span>
                    </h2>
                </div>
        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
            <thead>
                ";
            // line 67
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "history"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 68
                echo "                    <tr>
                        <th class=\"head0\" style=\"width: 35%; text-align: right\">";
                // line 69
                echo $this->getAttribute($this->getContext($context, "r"), "label", array(), "array");
                echo "</th>
                        <th style=\"width: 65%; text-align: left; font-weight: normal\">";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "value", array(), "array"), "html", null, true);
                echo "</th>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 73
            echo "            </thead>
        </table>

    </div>

    <div class=\"one_half last\">
                <div class=\"contenttitle radiusbottom0\">
                    <h2 class=\"form\">
                        <span>SUPPLEMENT TYPE</span>
                    </h2>
                </div>
                <div>
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
                        <thead>
                            <tr>
                                <th class=\"head0\" style=\"width: 40%\">Supplement Type</th>
                                <th class=\"head0\" style=\"width: 30%\">Letter</th>
                                <th class=\"head0\" style=\"width: 30%\">Lot in use</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 94
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "currentLots"), "supplementType", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 95
                echo "                                <tr>
                                    <td>";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "name", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 97
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "letter", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 98
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "lotInUse", array(), "array"), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 101
            echo "                        </tbody>
                    </table>
                </div>

                <br/>
                <div class=\"contenttitle radiusbottom0\">
                    <h2 class=\"form\">
                        <span>STOCK IN-USE</span>
                    </h2>
                </div>
                <div>
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
                        <thead>
                            <tr>
                                <th class=\"head0\" style=\"width: 25%\">Name</th>
                                <th class=\"head0\" style=\"width: 15%\">Letter</th>
                                <th class=\"head0\" style=\"width: 15%\">Shipment</th>
                                <th class=\"head0\" style=\"width: 45%\">w/w (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 122
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "currentLots"), "testedAndInUsePellet", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 123
                echo "                                <tr>
                                    <td>";
                // line 124
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "name", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "name", array(), "array"), "")) : ("")), "html", null, true);
                echo "</td>
                                    <td>";
                // line 125
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "letter", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "letter", array(), "array"), "")) : ("")), "html", null, true);
                echo "</td>
                                    <td>";
                // line 126
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "shipment", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "shipment", array(), "array"), "")) : ("")), "html", null, true);
                echo "</td>
                                    <td>";
                // line 127
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "activeIngredient", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "activeIngredient", array(), "array"), "")) : ("")), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 130
            echo "                        </tbody>
                    </table>
                </div>

                <br/>
                <div class=\"contenttitle radiusbottom0\">
                    <h2 class=\"form\">
                        <span>MATERIAL USED UP - IN USE</span>
                    </h2>
                </div>
                <div>
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable\" style=\"margin-top: 0px\">
                        <thead>
                            <tr>
                                <th class=\"head0\" style=\"width: 35%\">Material</th>
                                <th class=\"head0\" style=\"width: 10%\">Quantity in units</th>
                                <th class=\"head0\" style=\"width: 25%\">Used for</th>
                                <th class=\"head0\" style=\"width: 10%\">Date taken</th>
                                <th class=\"head0\" style=\"width: 25%\">Taken by</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 152
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "currentLots"), "selectedUsedUpMaterial", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 153
                echo "                                <tr>
                                    <td>";
                // line 154
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "materialName", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 155
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "quantity", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 156
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "usedFor", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 157
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "dateTaken", array(), "array"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 158
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "r"), "takenBy", array(), "array"), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 161
            echo "                        </tbody>
                    </table>
                </div>
    </div>

    <br clear=\"all\" /><br />
";
        }
        // line 168
        echo "
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:NutriMe:app_nutri_me_parsed.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  326 => 168,  296 => 155,  292 => 154,  285 => 152,  261 => 130,  252 => 127,  240 => 124,  233 => 122,  186 => 94,  154 => 70,  143 => 67,  128 => 55,  125 => 54,  108 => 45,  77 => 27,  342 => 168,  337 => 165,  316 => 157,  312 => 156,  297 => 152,  293 => 151,  281 => 141,  274 => 137,  270 => 135,  257 => 130,  246 => 122,  236 => 116,  227 => 113,  219 => 111,  216 => 110,  195 => 94,  193 => 96,  190 => 95,  188 => 91,  185 => 90,  147 => 68,  142 => 68,  120 => 57,  32 => 7,  99 => 28,  87 => 22,  83 => 21,  70 => 22,  62 => 26,  43 => 12,  89 => 39,  75 => 30,  72 => 17,  38 => 9,  82 => 34,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 162,  327 => 90,  317 => 161,  304 => 157,  294 => 75,  289 => 153,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 126,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 109,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 82,  163 => 73,  155 => 27,  152 => 26,  140 => 67,  118 => 14,  115 => 13,  106 => 7,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 112,  66 => 27,  56 => 64,  54 => 15,  49 => 9,  41 => 9,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 125,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 101,  208 => 70,  201 => 98,  199 => 66,  187 => 60,  182 => 58,  179 => 86,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 80,  156 => 49,  151 => 3,  146 => 110,  144 => 69,  137 => 66,  135 => 18,  129 => 60,  127 => 49,  119 => 48,  116 => 56,  110 => 46,  104 => 44,  101 => 43,  97 => 42,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 46,  78 => 14,  52 => 11,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 160,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 158,  303 => 111,  300 => 156,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 134,  263 => 133,  260 => 85,  256 => 70,  245 => 59,  242 => 120,  237 => 123,  234 => 83,  225 => 103,  223 => 112,  207 => 71,  205 => 69,  197 => 97,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 28,  150 => 69,  145 => 22,  138 => 36,  134 => 35,  130 => 56,  126 => 16,  121 => 15,  117 => 48,  113 => 55,  109 => 54,  105 => 27,  98 => 45,  94 => 26,  90 => 21,  86 => 33,  80 => 33,  74 => 19,  55 => 24,  50 => 14,  44 => 17,  39 => 7,  33 => 4,  26 => 3,  68 => 21,  61 => 98,  58 => 12,  48 => 10,  46 => 47,  42 => 7,  35 => 6,  31 => 12,  28 => 5,  170 => 83,  165 => 51,  65 => 12,  63 => 18,  59 => 25,  57 => 13,  53 => 10,  51 => 55,  47 => 13,  45 => 7,  40 => 9,  37 => 5,  30 => 6,  27 => 3,);
    }
}
