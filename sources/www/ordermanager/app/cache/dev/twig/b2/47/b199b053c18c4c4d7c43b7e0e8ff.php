<?php

/* DesignStarlightBundle::Base.html.twig */
class __TwigTemplate_b247b199b053c18c4c4d7c43b7e0e8ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'HeaderScript' => array($this, 'block_HeaderScript'),
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'BODY' => array($this, 'block_BODY'),
            'ContentHeader' => array($this, 'block_ContentHeader'),
            'quickAccessButton' => array($this, 'block_quickAccessButton'),
            'UserPanel' => array($this, 'block_UserPanel'),
            'MainContent' => array($this, 'block_MainContent'),
            'Menu' => array($this, 'block_Menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
    <title>";
        // line 6
        echo twig_escape_filter($this->env, ((array_key_exists("pageTitle", $context)) ? (_twig_default_filter($this->getContext($context, "pageTitle"), "")) : ("")), "html", null, true);
        echo "</title>
    ";
        // line 7
        $this->displayBlock('HeaderScript', $context, $blocks);
        // line 45
        echo "    
    ";
        // line 46
        $this->displayBlock('CustomHeaderScript', $context, $blocks);
        // line 49
        echo "</head>

";
        // line 51
        $this->displayBlock('BODY', $context, $blocks);
        // line 147
        echo "</html>
";
    }

    // line 7
    public function block_HeaderScript($context, array $blocks = array())
    {
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/style.darkblue.css"), "html", null, true);
        echo "\" type=\"text/css\">
        <!--[if IE 9]>
            <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie9.css"), "html", null, true);
        echo "\"/>
        <![endif]-->
        <!--[if IE 8]>
            <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie8.css"), "html", null, true);
        echo "\"/>
        <![endif]-->
        <!--[if IE 7]>
            <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/css/ie7.css"), "html", null, true);
        echo "\"/>
        <![endif]-->

        <script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery-1.7.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery-ui-1.8.16.custom.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/custom/general.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/custom.js"), "html", null, true);
        echo "\"></script>
        <!--[if lt IE 9]>
            <script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
        <![endif]-->
        <!--[if lte IE 8]><script language=\"javascript\" type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
        <link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
        <script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/source/select2-3.4.8/select2.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/js/searchable_cache_box.js"), "html", null, true);
        echo "\"></script>

        <link rel=\"stylesheet\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/sweet-alert.css"), "html", null, true);
        echo "\" type=\"text/css\">
        <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/plugin/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>

        <!-- ///// -->

        <script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery.dataTables.js"), "html", null, true);
        echo "\"></script>

        <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/js/plugins/jquery.colorbox-min.js"), "html", null, true);
        echo "\"></script>

    ";
    }

    // line 46
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 47
        echo "
    ";
    }

    // line 51
    public function block_BODY($context, array $blocks = array())
    {
        // line 52
        echo "<body class=\"loggedin\">
    ";
        // line 53
        $this->env->loadTemplate("DesignStarlightBundle:Component:SearchableCacheBoxData.html.twig")->display($context);
        // line 54
        echo "    <!-- START OF HEADER -->
    ";
        // line 55
        $this->displayBlock('ContentHeader', $context, $blocks);
        // line 109
        echo "
    ";
        // line 110
        $this->displayBlock('MainContent', $context, $blocks);
        // line 144
        echo "
</body>
";
    }

    // line 55
    public function block_ContentHeader($context, array $blocks = array())
    {
        // line 56
        echo "    <div class=\"header radius3\">
        <div class=\"headerinner\">
            <div style=\"height: 37px; color: white; font-size: 25px; font-style: bold;\">
                <b style=\"position: absolute\">&nbsp;&nbsp;&nbsp;";
        // line 59
        echo twig_escape_filter($this->env, ((array_key_exists("pageTitle", $context)) ? (_twig_default_filter($this->getContext($context, "pageTitle"), "")) : ("")), "html", null, true);
        echo "</b>
                <span>
                    <div id=\"frontheader\">
                        <nav class=\"quick-access\" style=\"float: left;\">
                            <ul>
                                ";
        // line 64
        $this->displayBlock('quickAccessButton', $context, $blocks);
        // line 71
        echo "                            </ul>
                        </nav>
                    </div>
                </span>
            </div>
            <div class=\"headright\">
                <div class=\"headercolumn\">&nbsp;</div>

                <!-- FIX ME -->
                <div id=\"notiPanel\" class=\"headercolumn\" style=\"display: none\">
                    <div class=\"notiwrapper\">
                    </div><!--notiwrapper-->
                </div><!--headercolumn-->

                ";
        // line 85
        $this->displayBlock('UserPanel', $context, $blocks);
        // line 103
        echo "            </div><!--headright-->
        </div><!--headerinner-->
    </div><!--header-->

    <!-- END OF HEADER -->
    ";
    }

    // line 64
    public function block_quickAccessButton($context, array $blocks = array())
    {
        // line 65
        echo "                                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "quickAccess"));
        foreach ($context['_seq'] as $context["_key"] => $context["shortcut"]) {
            // line 66
            echo "                                        <li>
                                            <a href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "shortcut"), "link", array(), "array"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "shortcut"), "nameShortcut", array(), "array"), "html", null, true);
            echo "</a>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shortcut'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 70
        echo "                                ";
    }

    // line 85
    public function block_UserPanel($context, array $blocks = array())
    {
        // line 86
        echo "                    <div id=\"userPanel\" class=\"headercolumn\">
                        <a href=\"\" class=\"userinfo radius2\">
                            <img src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designstarlight/images/avatar.png"), "html", null, true);
        echo "\" alt=\"\" class=\"radius2\" />
                            <span><strong>";
        // line 89
        echo twig_escape_filter($this->env, ((array_key_exists("username", $context)) ? (_twig_default_filter($this->getContext($context, "username"), "")) : ("")), "html", null, true);
        echo "</strong></span>
                        </a>
                        <div class=\"userdrop\">
                            ";
        // line 92
        if ($this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 93
            echo "                                <ul>
                                    <li><a href=\"";
            // line 94
            echo twig_escape_filter($this->env, $this->getContext($context, "exitSwitchUserUrl"), "html", null, true);
            echo "\">Back to admin</a></li>
                                </ul>
                            ";
        }
        // line 97
        echo "                            <ul>
                                <li><a href=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->getContext($context, "logoutUrl"), "html", null, true);
        echo "\">Logout</a></li>
                            </ul>
                        </div><!--userdrop-->
                    </div><!--headercolumn-->
                ";
    }

    // line 110
    public function block_MainContent($context, array $blocks = array())
    {
        // line 111
        echo "    <!-- START OF MAIN CONTENT -->
    <div class=\"mainwrapper\">
        <div class=\"mainwrapperinner\">
            ";
        // line 114
        $this->displayBlock('Menu', $context, $blocks);
        // line 125
        echo "
            ";
        // line 126
        $this->displayBlock('content', $context, $blocks);
        // line 140
        echo "       </div><!--mainwrapperinner-->
    </div><!--mainwrapper-->
  <!-- END OF MAIN CONTENT -->
  ";
    }

    // line 114
    public function block_Menu($context, array $blocks = array())
    {
        // line 115
        echo "                <div class=\"mainleft\">
                    <div class=\"mainleftinner\">
                        <div id=\"togglemenuleft_intop\"><a></a></div>
                        <div class=\"leftmenu\">
                            ";
        // line 119
        $this->env->loadTemplate("DesignStarlightBundle:Component:SideMenu.html.twig")->display(array_merge($context, array("menu" => $this->getContext($context, "menu"))));
        // line 120
        echo "                        </div><!--leftmenu-->
                        <div id=\"togglemenuleft\"><a></a></div>
                    </div><!--mainleftinner-->
                </div><!--mainleft-->
            ";
    }

    // line 126
    public function block_content($context, array $blocks = array())
    {
        // line 127
        echo "            <div class=\"maincontent noright\">
                <div class=\"maincontentinner\">
                    <ul class=\"maintabmenu\">
                        <li class=\"current\"><a href=\"#\">&nbsp;</a></li>
                    </ul><!--maintabmenu-->
                    <div class=\"content\">
                    </div><!--content-->
                </div><!--maincontentinner-->
                <div class=\"footer\">
                    <p></p>
                </div><!--footer-->
            </div><!--maincontent-->
            ";
    }

    public function getTemplateName()
    {
        return "DesignStarlightBundle::Base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 114,  315 => 140,  313 => 126,  310 => 125,  308 => 114,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 94,  279 => 93,  277 => 92,  271 => 89,  267 => 88,  263 => 86,  260 => 85,  256 => 70,  245 => 67,  242 => 66,  237 => 65,  234 => 64,  225 => 103,  223 => 85,  207 => 71,  205 => 64,  197 => 59,  192 => 56,  189 => 55,  183 => 144,  181 => 110,  178 => 109,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 47,  157 => 46,  150 => 42,  145 => 40,  138 => 36,  134 => 35,  130 => 34,  126 => 33,  121 => 31,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  80 => 17,  74 => 14,  55 => 7,  50 => 147,  44 => 49,  39 => 45,  33 => 6,  26 => 1,  68 => 11,  61 => 7,  58 => 8,  48 => 51,  46 => 12,  42 => 46,  35 => 6,  31 => 4,  28 => 3,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 9,  37 => 7,  30 => 4,  27 => 3,);
    }
}
