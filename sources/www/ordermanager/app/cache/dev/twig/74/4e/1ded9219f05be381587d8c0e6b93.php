<?php

/* LeepAdminBundle:DistributionChannel:distribution_channel_statistics.html.twig */
class __TwigTemplate_744e1ded9219f05be381587d8c0e6b93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:PopupPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:PopupPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "  <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "   <div class=\"content\">
      ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dcName"));
        foreach ($context['_seq'] as $context["_key"] => $context["dcName"]) {
            // line 10
            echo "          <h1><span style=\"position: relative; margin-left: 4px; top: -4px; width:200px\"> Customer in distribution channel 
                <span style=\"color: green\">\"";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo "\"</span></span></h1>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dcName'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 13
        echo "
      ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "countIn"));
        foreach ($context['_seq'] as $context["_key"] => $context["countIn"]) {
            // line 15
            echo "        <h4>Number customer: <span style=\"color: orange\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "countIn"), "countIn", array(), "array"), "html", null, true);
            echo "</span></h4>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['countIn'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 16
        echo " 
      <h4>Number customer contain gene <span style=\"color: blue\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ": </span><span style=\"color:orange\">";
        echo twig_escape_filter($this->env, $this->getContext($context, "geneCountIn"), "html", null, true);
        echo "</span></h4>

      <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable stdform\" style=\"margin-top: 0px\">
          <thead>  
            <tr style=\"heigth:45px\">
                <th class=\"head1\" style=\"width: 5%\"></th>
                <th class=\"head1\" style=\"width: 10%\">Number customer (in customer have ";
        // line 23
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ")</th>
                <th class=\"head1\" style=\"width: 10%\">Percentage (on customer have ";
        // line 24
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ")</th>
                ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dcName"));
        foreach ($context['_seq'] as $context["_key"] => $context["dcName"]) {
            // line 26
            echo "                <th class=\"head1\" style=\"width: 10%\">Number customer (in all customer of ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo ")</th>
                <th class=\"head1\" style=\"width: 10%\">Percentage (on all customer of ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo ")</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dcName'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 29
        echo "            </tr>    
          </thead>
          
          <tbody>
          ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "geneResult"));
        foreach ($context['_seq'] as $context["_key"] => $context["geneResult"]) {
            // line 34
            echo "            <tr>
                <td align=\"center\">";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "geneResult"), "name", array(), "array"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "geneResult"), "total", array(), "array"), "html", null, true);
            echo "</td>  
                <td align=\"center\">";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "geneResult"), "percentage", array(), "array"), "html", null, true);
            echo "</td>
                ";
            // line 38
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "outGeneResult"));
            foreach ($context['_seq'] as $context["_key"] => $context["outGeneResult"]) {
                // line 39
                echo "                    ";
                if (($this->getAttribute($this->getContext($context, "outGeneResult"), "name", array(), "array") == $this->getAttribute($this->getContext($context, "geneResult"), "name", array(), "array"))) {
                    // line 40
                    echo "                        <td align=\"center\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "outGeneResult"), "total", array(), "array"), "html", null, true);
                    echo "</td>
                        <td align=\"center\">";
                    // line 41
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "outGeneResult"), "percentage", array(), "array"), "html", null, true);
                    echo "</td>
                    ";
                }
                // line 43
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['outGeneResult'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 44
            echo "                
            </tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geneResult'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 47
        echo "
          </tbody>
        </table>

    <br></br>
    <br></br>


    ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dcName"));
        foreach ($context['_seq'] as $context["_key"] => $context["dcName"]) {
            // line 56
            echo "      <h1><span style=\"position: relative; margin-left: 4px; top: -4px; width:200px\"> 
          Customer <span style=\"color: red\">not</span> in distribution channel <span style=\"color: green\">\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo "\"</span></span></h1>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dcName'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 59
        echo "      <h4>Number customer: <span style=\"color: orange\"> ";
        echo twig_escape_filter($this->env, $this->getContext($context, "countNotIn"), "html", null, true);
        echo " </span></h4>
      <h4>Number customer contain gene <span style=\"color: blue\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ": </span><span style=\"color:orange\">";
        echo twig_escape_filter($this->env, $this->getContext($context, "restTotalGene"), "html", null, true);
        echo "</span></h4>

      <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"stdtable stdform\" style=\"margin-top: 0px\">
          <thead>  
            <tr style=\"heigth:45px\">
                <th class=\"head1\" style=\"width: 5%\"></th>
                <th class=\"head1\" style=\"width: 10%\">Number customer (in customer have ";
        // line 66
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ")</th>
                <th class=\"head1\" style=\"width: 10%\">Percentage (on customer have ";
        // line 67
        echo twig_escape_filter($this->env, $this->getContext($context, "geneName"), "html", null, true);
        echo ")</th>
                ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dcName"));
        foreach ($context['_seq'] as $context["_key"] => $context["dcName"]) {
            // line 69
            echo "                  <th class=\"head1\" style=\"width: 10%\">Number customer (in all customers without of ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo ")</th>
                  <th class=\"head1\" style=\"width: 10%\">Percentage (on all customers without of ";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "dcName"), "dcName", array(), "array"), "html", null, true);
            echo ")</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dcName'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 72
        echo "            </tr>    
          </thead>
          
          <tbody>
          ";
        // line 76
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "notDCGeneResult"));
        foreach ($context['_seq'] as $context["_key"] => $context["notDCGeneResult"]) {
            // line 77
            echo "            <tr>
                <td align=\"center\">";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "notDCGeneResult"), "name", array(), "array"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "notDCGeneResult"), "total", array(), "array"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "notDCGeneResult"), "percentage", array(), "array"), "html", null, true);
            echo "</td>

                ";
            // line 82
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "notDCNotGeneResult"));
            foreach ($context['_seq'] as $context["_key"] => $context["notDCNotGeneResult"]) {
                // line 83
                echo "                    ";
                if (($this->getAttribute($this->getContext($context, "notDCNotGeneResult"), "name", array(), "array") == $this->getAttribute($this->getContext($context, "notDCGeneResult"), "name", array(), "array"))) {
                    // line 84
                    echo "                        <td align=\"center\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "notDCNotGeneResult"), "total", array(), "array"), "html", null, true);
                    echo "</td>
                        <td align=\"center\">";
                    // line 85
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "notDCNotGeneResult"), "percentage", array(), "array"), "html", null, true);
                    echo "</td>
                    ";
                }
                // line 87
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['notDCNotGeneResult'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 88
            echo "
            </tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['notDCGeneResult'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 91
        echo "
          </tbody>

        </table>

    <br>
    <form>
      <button id='backUrl' style=\"padding: 7px 16px; border: 1px solid #333; background: #333; color: #fff; cursor: pointer; font-weight: bold; font-size: 12px; font-family: sans-serif; margin: 0\" onClick=\"redirectPage()\">
        Back
      </button>
    </form>
   </div>

   <script type=\"text/javascript\">
      function redirectPage(){
          var targetUrl = '";
        // line 106
        echo twig_escape_filter($this->env, $this->getContext($context, "backUrl"), "html", null, true);
        echo "?id=";
        echo twig_escape_filter($this->env, $this->getContext($context, "dcId"), "html", null, true);
        echo "'
          window.open(targetUrl);
      }  
   </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:DistributionChannel:distribution_channel_statistics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 106,  292 => 91,  284 => 88,  278 => 87,  273 => 85,  268 => 84,  265 => 83,  261 => 82,  256 => 80,  252 => 79,  248 => 78,  245 => 77,  241 => 76,  235 => 72,  227 => 70,  222 => 69,  218 => 68,  214 => 67,  210 => 66,  199 => 60,  194 => 59,  186 => 57,  183 => 56,  179 => 55,  169 => 47,  161 => 44,  155 => 43,  150 => 41,  145 => 40,  142 => 39,  138 => 38,  134 => 37,  130 => 36,  126 => 35,  123 => 34,  119 => 33,  113 => 29,  105 => 27,  100 => 26,  96 => 25,  92 => 24,  88 => 23,  77 => 17,  74 => 16,  65 => 15,  61 => 14,  58 => 13,  50 => 11,  47 => 10,  43 => 9,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}
