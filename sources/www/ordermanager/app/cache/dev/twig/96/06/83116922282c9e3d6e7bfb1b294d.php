<?php

/* LeepAdminBundle:TodoTask:dashboard.html.twig */
class __TwigTemplate_960683116922282c9e3d6e7bfb1b294d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"contenttitle radiusbottom0\">
        <h2 class=\"table\"><span>Filter&nbsp;</span></h2>
    </div>
    ";
        // line 11
        $this->env->loadTemplate("LeepAdminBundle:TodoTask:dashboard.html.twig", "1896039937")->display(array_merge($context, array("form" => $this->getContext($context, "filter"))));
        // line 13
        echo "
    <br/>
    ";
        // line 15
        $this->env->loadTemplate("LeepAdminBundle:TodoTask:dashboard.html.twig", "515729430")->display(array_merge($context, array("grid" => $this->getContext($context, "gridTask"), "sDom" => "rt")));
        // line 28
        echo "
    <br/>
    ";
        // line 30
        $this->env->loadTemplate("LeepAdminBundle:TodoTask:dashboard.html.twig", "1219105506")->display(array_merge($context, array("grid" => $this->getContext($context, "gridContinualImprovement"), "sDom" => "rt")));
        // line 32
        echo "
    <br/>
    ";
        // line 34
        $this->env->loadTemplate("LeepAdminBundle:TodoTask:dashboard.html.twig", "1637967102")->display(array_merge($context, array("grid" => $this->getContext($context, "gridGoal"), "sDom" => "rt")));
        // line 36
        echo "    
    <script type=\"text/javascript\">
        function callTaskAjax(targetUrl) {
            jQuery.ajax({
                url: targetUrl,
                type: \"POST\",
                context: document.body
            }).done(function(data) {
                funcDynamicTableReload_GridTask();
                funcDynamicTableReload_GridGoal();                
            }).error(function(jqXHR, textStatus, errorThrown ) {
                alert(textStatus + \" \" + errorThrown);
            });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:TodoTask:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 36,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:TodoTask:dashboard.html.twig */
class __TwigTemplate_960683116922282c9e3d6e7bfb1b294d_1896039937 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Filter.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Filter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:TodoTask:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 36,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:TodoTask:dashboard.html.twig */
class __TwigTemplate_960683116922282c9e3d6e7bfb1b294d_515729430 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
            'gridDrawCallback' => array($this, 'block_gridDrawCallback'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 16
    public function block_gridDrawCallback($context, array $blocks = array())
    {
        echo "            
            jQuery('#";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
        echo " tr').each(function() {
                var colorCode = jQuery(this).find('.task_color').attr('color-code');
                if (colorCode != 'undefined') {
                    jQuery(this).css('background-color', colorCode);
                }
            });

            funcDynamicTableReload_GridGoal();     
            funcDynamicTableReload_GridContinualImprovement();            
        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:TodoTask:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 17,  165 => 16,  65 => 36,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:TodoTask:dashboard.html.twig */
class __TwigTemplate_960683116922282c9e3d6e7bfb1b294d_1219105506 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:TodoTask:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 17,  165 => 16,  65 => 36,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:TodoTask:dashboard.html.twig */
class __TwigTemplate_960683116922282c9e3d6e7bfb1b294d_1637967102 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:TodoTask:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 17,  165 => 16,  65 => 36,  63 => 34,  59 => 32,  57 => 30,  53 => 28,  51 => 15,  47 => 13,  45 => 11,  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}
