<?php

/* LeepAdminBundle:Lead:list.html.twig */
class __TwigTemplate_da91bbdc923f45d5681303fdfb95f248 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/jquery.tooltip.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 9
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:Lead:list.html.twig", "969064864")->display(array_merge($context, array("grid" => $this->getContext($context, "grid"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 10,  45 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:Lead:list.html.twig */
class __TwigTemplate_da91bbdc923f45d5681303fdfb95f248_969064864 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
            'filterForm' => array($this, 'block_filterForm'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_filterForm($context, array $blocks = array())
    {
        // line 12
        echo "            ";
        if ($this->getAttribute((isset($context["grid"]) ? $context["grid"] : null), "filter", array(), "array", true, true)) {
            // line 13
            echo "                ";
            $this->env->loadTemplate("LeepAdminBundle:Lead:list.html.twig", "1063094722")->display(array_merge($context, array("form" => $this->getAttribute($this->getContext($context, "grid"), "filter", array(), "array"))));
            // line 24
            echo "
                 <script type=\"text/javascript\">
                    function bulkConvertLeadToDC() {
                        var selectedId = '';
                        var checkboxes = jQuery('#";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
            echo " .stdtableCheckBox');
                        checkboxes.each(function(i, v) {
                            if (jQuery(this).is(':checked')) {
                                var id = jQuery(this).attr('id').substring(4);
                                selectedId += id + ',';
                            }
                        });

                        if (selectedId) {
                            var targetUrl = '";
            // line 37
            echo twig_escape_filter($this->env, $this->getContext($context, "bulkConvertLeadToDC"), "html", null, true);
            echo "?id=' + selectedId;
                            jQuery.colorbox({
                                href: targetUrl,
                                iframe: true,
                                width:  \"90%\",
                                height: \"90%\",
                                onClosed:function(){
                                    funcDynamicTableReload_";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "grid"), "id", array(), "array"), "html", null, true);
            echo "();
                                }
                            });
                        } else {
                            swal(\"Missing Something!\", \"Plz select at least 1 Lead!\");
                        }


                        return false;
                    }

                    function exportLeadList() {
                        var url = '";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "links"), "lead", array(), "array"), "exportLead", array(), "array"), "html", null, true);
            echo "';
                        window.open(url,'_blank');
                        return false;
                    }
                </script>
            ";
        }
        // line 62
        echo "        ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 62,  146 => 56,  131 => 44,  121 => 37,  109 => 28,  103 => 24,  100 => 13,  97 => 12,  94 => 11,  48 => 10,  45 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:Lead:list.html.twig */
class __TwigTemplate_da91bbdc923f45d5681303fdfb95f248_1063094722 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Filter.html.twig");

        $this->blocks = array(
            'buttons' => array($this, 'block_buttons'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Filter.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_buttons($context, array $blocks = array())
    {
        // line 15
        echo "                        ";
        $this->displayParentBlock("buttons", $context, $blocks);
        echo "
                        <div style=\"float: right\">
                            <button type=\"button\" class=\"radius2\" onclick=\"exportLeadList()\">&nbsp;&nbsp;Export&nbsp;&nbsp;</button>
                            <button class=\"radius2\" onclick=\"return bulkConvertLeadToDC()\">
                                &nbsp;&nbsp;Bulk Convert Lead to DC&nbsp;&nbsp;
                            </button>
                        </div>
                    ";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Lead:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 15,  200 => 14,  155 => 62,  146 => 56,  131 => 44,  121 => 37,  109 => 28,  103 => 24,  100 => 13,  97 => 12,  94 => 11,  48 => 10,  45 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
