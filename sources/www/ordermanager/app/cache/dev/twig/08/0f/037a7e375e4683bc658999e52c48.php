<?php

/* LeepAdminBundle:Form:app_form_item.html.twig */
class __TwigTemplate_080f037a7e375e4683bc658999e52c48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_form_item_widget' => array($this, 'block_app_form_item_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_form_item_widget', $context, $blocks);
    }

    public function block_app_form_item_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "id"), 'widget');
        echo " 
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "sortOrder"), 'widget');
        echo " 
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idType"), 'widget');
        echo "

    <!-- STRUCTURE -->
    <span id=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "_formItem_structureInstruction\" class=\"text_block\" style=\"background-color: #FFFFFF\">
        <span><text>Text:</text>";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structureInstruction"), "text"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "_formItem_structureTextBox\" class=\"text_block\" style=\"background-color: #FFFFFF\">
        <span><text>Text:</text>";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structureTextBox"), "text"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "_formItem_structureDate\" class=\"text_block\" style=\"background-color: #FFFFFF\">
    </span>
    <span id=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "_formItem_structurePerson\" class=\"text_block\" style=\"background-color: #FFFFFF\">
        <span><text>Text:</text>";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structurePerson"), "text"), 'widget');
        echo " </span>
        <span><text>Exclude Me:</text>";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structurePerson"), "excludeMe"), 'widget');
        echo " </span>
    </span>
    <span id=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "_formItem_structureAuthorizedPersonnel\" class=\"text_block\" style=\"background-color: #FFFFFF\">
        <span><text>Text:</text>";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structureAuthorizedPersonnel"), "text"), 'widget');
        echo " </span>
        <span><text>Exclude Me:</text>";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structureAuthorizedPersonnel"), "excludeMe"), 'widget');
        echo " </span><br/>
        <span><text>Authorized Personnel:</text>";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "structureAuthorizedPersonnel"), "authorizedPersonnel"), 'widget');
        echo " </span>
    </span>

    <script type=\"text/javascript\">            
        jQuery('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "').change(function() {
            var toolId = jQuery(this).val();
            onChangeFormItem('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idType"), "vars"), "id"), "html", null, true);
        echo "', toolId);
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:Form:app_form_item.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  99 => 28,  87 => 22,  83 => 21,  70 => 17,  62 => 15,  43 => 8,  89 => 47,  75 => 19,  72 => 17,  38 => 6,  82 => 58,  67 => 14,  18 => 1,  476 => 163,  469 => 162,  466 => 161,  446 => 144,  435 => 143,  432 => 142,  423 => 136,  418 => 134,  413 => 132,  406 => 129,  403 => 128,  400 => 127,  390 => 120,  384 => 117,  380 => 116,  373 => 113,  370 => 112,  359 => 104,  355 => 103,  348 => 100,  345 => 99,  340 => 96,  332 => 92,  327 => 90,  317 => 87,  304 => 79,  294 => 75,  289 => 73,  286 => 72,  278 => 70,  273 => 68,  268 => 67,  265 => 66,  262 => 65,  248 => 60,  243 => 58,  241 => 57,  238 => 56,  232 => 52,  226 => 50,  220 => 48,  212 => 42,  206 => 40,  204 => 39,  200 => 38,  184 => 37,  166 => 31,  163 => 30,  155 => 27,  152 => 26,  140 => 21,  118 => 14,  115 => 13,  106 => 7,  96 => 3,  91 => 2,  84 => 26,  81 => 160,  79 => 20,  76 => 141,  71 => 126,  69 => 112,  66 => 16,  56 => 64,  54 => 11,  49 => 9,  41 => 25,  34 => 5,  29 => 3,  264 => 93,  254 => 61,  244 => 85,  228 => 76,  224 => 75,  222 => 74,  213 => 72,  210 => 71,  208 => 70,  201 => 67,  199 => 66,  187 => 60,  182 => 58,  179 => 57,  175 => 34,  172 => 33,  169 => 32,  164 => 52,  162 => 51,  159 => 50,  156 => 49,  151 => 3,  146 => 110,  144 => 93,  137 => 88,  135 => 18,  129 => 79,  127 => 49,  119 => 48,  116 => 47,  110 => 46,  104 => 44,  101 => 5,  97 => 42,  88 => 1,  64 => 99,  60 => 21,  36 => 5,  23 => 2,  21 => 1,  85 => 46,  78 => 14,  52 => 11,  24 => 2,  20 => 2,  17 => 1,  344 => 127,  341 => 126,  333 => 120,  331 => 119,  325 => 115,  322 => 88,  315 => 140,  313 => 126,  310 => 125,  308 => 81,  303 => 111,  300 => 110,  291 => 98,  288 => 97,  282 => 71,  279 => 93,  277 => 100,  271 => 89,  267 => 94,  263 => 86,  260 => 85,  256 => 70,  245 => 59,  242 => 66,  237 => 84,  234 => 83,  225 => 103,  223 => 49,  207 => 71,  205 => 69,  197 => 59,  192 => 62,  189 => 55,  183 => 144,  181 => 36,  178 => 35,  176 => 55,  173 => 54,  171 => 53,  168 => 52,  160 => 29,  157 => 28,  150 => 42,  145 => 22,  138 => 36,  134 => 35,  130 => 34,  126 => 16,  121 => 15,  117 => 30,  113 => 29,  109 => 28,  105 => 27,  98 => 23,  94 => 26,  90 => 21,  86 => 20,  80 => 17,  74 => 19,  55 => 25,  50 => 10,  44 => 17,  39 => 7,  33 => 4,  26 => 3,  68 => 23,  61 => 98,  58 => 12,  48 => 10,  46 => 47,  42 => 7,  35 => 6,  31 => 12,  28 => 5,  170 => 17,  165 => 51,  65 => 12,  63 => 9,  59 => 65,  57 => 13,  53 => 10,  51 => 55,  47 => 13,  45 => 7,  40 => 9,  37 => 5,  30 => 4,  27 => 3,);
    }
}
