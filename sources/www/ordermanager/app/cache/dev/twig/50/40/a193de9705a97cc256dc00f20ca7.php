<?php

/* LeepAdminBundle:CRUD:form.html.twig */
class __TwigTemplate_5040a193de9705a97cc256dc00f20ca7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
";
    }

    // line 7
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:CRUD:form.html.twig", "846987446")->display(array_merge($context, array("form" => $this->getContext($context, "form"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:CRUD:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:CRUD:form.html.twig */
class __TwigTemplate_5040a193de9705a97cc256dc00f20ca7_846987446 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Form:Basic.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Form:Basic.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:CRUD:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  30 => 4,  27 => 3,);
    }
}
