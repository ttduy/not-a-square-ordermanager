<?php

/* LeepAdminBundle:CRUD:list.html.twig */
class __TwigTemplate_415665680b6b9c8252f251f9dba3e672 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Layout:NormalPage.html.twig");

        $this->blocks = array(
            'CustomHeaderScript' => array($this, 'block_CustomHeaderScript'),
            'MainContentInner' => array($this, 'block_MainContentInner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Layout:NormalPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_CustomHeaderScript($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/app_style.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/css/sweet-alert.css"), "html", null, true);
        echo "\" type=\"text/css\">
    <script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leepadmin/sweet-alert.min.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 9
    public function block_MainContentInner($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->env->loadTemplate("LeepAdminBundle:CRUD:list.html.twig", "977937477")->display(array_merge($context, array("grid" => $this->getContext($context, "grid"))));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:CRUD:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 10,  45 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}


/* LeepAdminBundle:CRUD:list.html.twig */
class __TwigTemplate_415665680b6b9c8252f251f9dba3e672_977937477 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DesignStarlightBundle:Grid:BasicNoButton.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "DesignStarlightBundle:Grid:BasicNoButton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "LeepAdminBundle:CRUD:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 10,  45 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
