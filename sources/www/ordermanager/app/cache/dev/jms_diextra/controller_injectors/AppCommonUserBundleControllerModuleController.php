<?php

namespace App\Common\UserBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class ModuleController__JMSInjector
{
    public static function inject($container) {
        $instance = new \App\Common\UserBundle\Controller\ModuleController();
        return $instance;
    }
}
