<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRoutes = array(
        'leep_admin_cryosave_api_create_sample' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\CryosaveApiController::createSampleAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/cryosave_api/create_sample',    ),  ),),
        'leep_admin' => array (  0 =>   array (    0 => 'module',    1 => 'feature',    2 => 'action',  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\ModuleController::handleAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]+',      3 => 'action',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]+',      3 => 'feature',    ),    2 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]+',      3 => 'module',    ),  ),),
        'leep_admin_default_controller' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),),
        'leep_admin_login_show' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\LoginController::showAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/Login/Show',    ),  ),),
        'leep_admin_feedback_form' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\FeedbackController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/feedback_form',    ),  ),),
        'leep_admin_feedback_form_submit' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\FeedbackController::submitAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/feedback_form/submit',    ),  ),),
        'leep_admin_public_access_report' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::accessReportAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/public/accessReport',    ),  ),),
        'leep_admin_public_your_report' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::yourReportAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/public/yourReport',    ),  ),),
        'leep_admin_public_disable_access_link' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::disableAccessLinkAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/public/disableAccessLink',    ),  ),),
        'leep_admin_public_download_report' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::downloadReportAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/public/downloadReport',    ),  ),),
        'leep_admin_public_download_shipment' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::downloadShipmentAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/public/downloadShipment',    ),  ),),
        'Security_LoginCheck' => array (  0 =>   array (  ),  1 =>   array (  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/Login/Check',    ),  ),),
        'Security_Logout' => array (  0 =>   array (  ),  1 =>   array (  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/Login/Logout',    ),  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }
}
