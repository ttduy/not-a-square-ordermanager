<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // leep_admin_cryosave_api_create_sample
        if ($pathinfo === '/cryosave_api/create_sample') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\CryosaveApiController::createSampleAction',  '_route' => 'leep_admin_cryosave_api_create_sample',);
        }

        // leep_admin
        if (preg_match('#^/(?<module>[^/]+)/(?<feature>[^/]+)/(?<action>[^/]+)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leep\\AdminBundle\\Controller\\ModuleController::handleAction',)), array('_route' => 'leep_admin'));
        }

        // leep_admin_default_controller
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'leep_admin_default_controller');
            }

            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'leep_admin_default_controller',);
        }

        // leep_admin_login_show
        if ($pathinfo === '/Login/Show') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\LoginController::showAction',  '_route' => 'leep_admin_login_show',);
        }

        // leep_admin_feedback_form
        if ($pathinfo === '/feedback_form') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\FeedbackController::indexAction',  '_route' => 'leep_admin_feedback_form',);
        }

        // leep_admin_feedback_form_submit
        if ($pathinfo === '/feedback_form/submit') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\FeedbackController::submitAction',  '_route' => 'leep_admin_feedback_form_submit',);
        }

        // leep_admin_public_access_report
        if ($pathinfo === '/public/accessReport') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::accessReportAction',  '_route' => 'leep_admin_public_access_report',);
        }

        // leep_admin_public_your_report
        if ($pathinfo === '/public/yourReport') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::yourReportAction',  '_route' => 'leep_admin_public_your_report',);
        }

        // leep_admin_public_disable_access_link
        if ($pathinfo === '/public/disableAccessLink') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::disableAccessLinkAction',  '_route' => 'leep_admin_public_disable_access_link',);
        }

        // leep_admin_public_download_report
        if ($pathinfo === '/public/downloadReport') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::downloadReportAction',  '_route' => 'leep_admin_public_download_report',);
        }

        // leep_admin_public_download_shipment
        if ($pathinfo === '/public/downloadShipment') {
            return array (  '_controller' => 'Leep\\AdminBundle\\Controller\\PublicController::downloadShipmentAction',  '_route' => 'leep_admin_public_download_shipment',);
        }

        // Security_LoginCheck
        if ($pathinfo === '/Login/Check') {
            return array('_route' => 'Security_LoginCheck');
        }

        // Security_Logout
        if ($pathinfo === '/Login/Logout') {
            return array('_route' => 'Security_Logout');
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
