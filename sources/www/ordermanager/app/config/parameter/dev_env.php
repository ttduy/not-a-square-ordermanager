<?php
$container->setParameter('database_host',     getenv('DB_PORT_3306_TCP_ADDR'));
$container->setParameter('database_port',     getenv('DB_PORT_3306_TCP_PORT'));
$container->setParameter('database_password', getenv('DB_ENV_MYSQL_ROOT_PASSWORD'));
