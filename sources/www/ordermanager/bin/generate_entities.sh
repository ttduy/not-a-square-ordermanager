CURRENT_DIR=`dirname $0`

. $CURRENT_DIR/config.sh


rm $CURRENT_DIR/../src/$DATABASE_DIR/Resources/config/doctrine/metadata/orm/*
rm $CURRENT_DIR/../src/$DATABASE_DIR/Entity/*
php $CURRENT_DIR/../app/console doctrine:mapping:convert xml $CURRENT_DIR/../src/$DATABASE_DIR/Resources/config/doctrine/metadata/orm --from-database --force
php $CURRENT_DIR/../app/console doctrine:mapping:import $DATABASE_BUNDLE annotation
cp $CURRENT_DIR/../src/$DATABASE_DIR/EntityView/* $CURRENT_DIR/../src/$DATABASE_DIR/Entity
php $CURRENT_DIR/../app/console doctrine:generate:entities $DATABASE_BUNDLE
