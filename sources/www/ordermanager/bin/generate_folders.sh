source bin/config.sh

mkdir app/cache 2>/dev/null
mkdir app/cache/file_cache 2>/dev/null
mkdir app/cache/text_block 2>/dev/null
mkdir app/logs 2>/dev/null
mkdir app/spool 2>/dev/null
mkdir files 2>/dev/null
mkdir files 2>/dev/null
mkdir files 2>/dev/null
mkdir files/fonts 2>/dev/null
mkdir files/invoices 2>/dev/null
mkdir files/recipe 2>/dev/null
mkdir web/attachments 2>/dev/null
mkdir web/attachments/external_pdf 2>/dev/null
mkdir web/attachments/logo 2>/dev/null
mkdir web/attachments/report_resource 2>/dev/null
mkdir web/attachments/customers 2>/dev/null
mkdir web/attachments/distribution_channels 2>/dev/null
mkdir web/attachments/distribution_channel_marketing_status 2>/dev/null
mkdir web/report 2>/dev/null
mkdir web/upload 2>/dev/null
mkdir web/upload/distribution_channel_type_flag 2>/dev/null

chmod 774 app/cache 2>/dev/null
chmod 774 app/cache/file_cache 2>/dev/null
chmod 774 app/cache/text_block 2>/dev/null
chmod 774 app/logs 2>/dev/null
chmod 774 app/spool 2>/dev/null
chmod 774 files 2>/dev/null
chmod 774 files/fonts 2>/dev/null
chmod 774 files/invoices 2>/dev/null
chmod 774 files/recipe 2>/dev/null
chmod 774 web/attachments 2>/dev/null
chmod 774 web/attachments/external_pdf 2>/dev/null
chmod 774 web/attachments/logo 2>/dev/null
chmod 774 web/attachments/report_resource 2>/dev/null
chmod 774 web/attachments/customers 2>/dev/null
chmod 774 web/attachments/distribution_channels 2>/dev/null
chmod 774 web/attachments/distribution_channel_marketing_status 2>/dev/null
chmod 774 web/report 2>/dev/null
chmod 774 web/upload 2>/dev/null
chmod 774 web/upload/distribution_channel_type_flag 2>/dev/null
