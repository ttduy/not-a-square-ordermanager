call bin\config.bat
del %APP_DIR%\src\%DB_DIR%\Resources\config\doctrine\metadata\orm\* /Q
del %APP_DIR%\src\%DB_DIR%\Entity\* /Q
php app/console doctrine:mapping:convert xml ./src/%DB_DIR2%/Resources/config/doctrine/metadata/orm --em=app_main --from-database --force 
php app/console doctrine:mapping:import %DB_BUNDLE% --em=app_main annotation
xcopy %APP_DIR%\src\%DB_DIR%\EntityView\* %APP_DIR%\src\%DB_DIR%\Entity /Y
php app/console doctrine:generate:entities %DB_BUNDLE%