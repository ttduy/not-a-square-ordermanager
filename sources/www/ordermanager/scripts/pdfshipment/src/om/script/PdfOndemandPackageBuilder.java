package om.script;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import org.apache.pdfbox.exceptions.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.edit.*;
import org.apache.pdfbox.pdmodel.common.*;
import org.apache.pdfbox.pdmodel.font.*;
import org.apache.pdfbox.pdmodel.graphics.color.*;
import org.apache.pdfbox.pdmodel.graphics.xobject.*;
import org.apache.pdfbox.util.*;
import org.apache.pdfbox.pdfwriter.*;
import org.apache.pdfbox.pdfparser.*;
import org.apache.pdfbox.cos.*;
import org.apache.pdfbox.io.*;

import java.awt.color.*;
import java.awt.image.*;
import java.awt.RenderingHints;
import java.awt.print.PageFormat;

import java.util.zip.*;
import java.text.*;

import org.apache.velocity.app.*;
import org.apache.velocity.*;
import org.apache.velocity.runtime.*;

public class PdfOndemandPackageBuilder {
    protected ColorSpace rgbIccColorSpace = null;
    protected ColorSpace cmykIccColorSpace = null;
    protected PDICCBased cmykPDIccColorSpace = null;

    protected Properties properties = null;
    protected List<String> cleanupFiles = new ArrayList<String>();

    protected void setProperties(Properties properties) throws Exception {
        this.properties = properties;
    }

    private String reformatBookId(String bookId) {
        while (bookId.length() < 8) bookId = "0" + bookId;
        return bookId;
    }

    public void buildPackage(Properties shipmentProperties, String shipmentId) throws Exception {
        // Prepare zip
        String customerOrderNumber = shipmentProperties.getProperty("customerOrderNumber");
        String mainBookId = shipmentProperties.getProperty("bookId1");
        String mainFormatterBookId = this.reformatBookId(mainBookId);
        String mainFilename = mainFormatterBookId + properties.getProperty("customerNumber");
        String zipFile = "files/output/" + customerOrderNumber + "_" + mainFormatterBookId + ".zip";
        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zos = new ZipOutputStream(fos);

        // Process books
        List<String> itemNumberList = new ArrayList<String>();
        List<String> pageNumberList = new ArrayList<String>();
        int numBooks = Integer.parseInt(shipmentProperties.getProperty("numBooks"));
        for (int i = 1; i <= numBooks; i++) {
            // Load basic information
            String bookId = shipmentProperties.getProperty("bookId" + i);
            String formattedBookId = this.reformatBookId(bookId);
            String customerNumber = properties.getProperty("customerNumber");
            String fileNumber = formattedBookId + customerNumber;

            // Load book
            String bookFile = shipmentProperties.getProperty("book" + i);
            String bookItemNumber = buildItemBarcode(formattedBookId, customerNumber, "02");
            PDDocument document = PDDocument.load(bookFile);

            // Load cover
            String coverFile = shipmentProperties.getProperty("cover" + i);
            String coverItemNumber = buildItemBarcode(formattedBookId, customerNumber, "12");
            PDDocument coverDocument = PDDocument.load(coverFile);

            // Convert to CMYK
            this.validateDocument(document);
            this.loadResources(document);
            PDDocument convertedDocument = this.convertDocumentToCMYK(document);
            PDDocument coverConvertedDocument = this.convertDocumentToCMYK(coverDocument);

            // Build cover and content
            PDDocument coverPdf = new PDDocument();
            PDDocument contentPdf = new PDDocument();

            List coverPages = coverConvertedDocument.getDocumentCatalog().getAllPages();
            List pages = convertedDocument.getDocumentCatalog().getAllPages();

            coverPdf.addPage((PDPage) coverPages.get(1));
            for (int j = 0; j < pages.size()-2; j++) {
                PDPage page = (PDPage) pages.get(j);
                if (isLandspace(page)) {
                    page.setRotation(-90);
                }
                contentPdf.addPage(page);
            }
            contentPdf.addPage((PDPage) coverPages.get(0));

            String coverPdfFilename = fileNumber + "_Cover.pdf";
            String contentPdfFilename = fileNumber + "_Bookblock.pdf";
            coverPdf.save("files/" + coverPdfFilename);
            contentPdf.save("files/" + contentPdfFilename);

            // Build XML Metadata
            itemNumberList.add(coverItemNumber);
            int numPage = contentPdf.getNumberOfPages();
            pageNumberList.add(numPage + "");

            // Done
            coverPdf.close();
            contentPdf.close();
            convertedDocument.close();
            coverConvertedDocument.close();
            coverDocument.close();
            document.close();

            // Package to ZIP
            addFileToZip(zos, "files/" + coverPdfFilename, coverPdfFilename);
            addFileToZip(zos, "files/" + contentPdfFilename, contentPdfFilename);

            this.cleanupFiles.add("files/" + coverPdfFilename);
            this.cleanupFiles.add("files/" + contentPdfFilename);
        }
            
        // Build XML meta data
        String xmlMetadataFilename = mainFilename + "_IndividualOrder.xml";
        buildXmlMetadata("files/" + xmlMetadataFilename, shipmentId, mainBookId, itemNumberList, pageNumberList);
        addFileToZip(zos, "files/" + xmlMetadataFilename, xmlMetadataFilename);
        this.cleanupFiles.add("files/" + xmlMetadataFilename);

        // Delivery notes
        String deliveryNoteFile = shipmentProperties.getProperty("deliveryNote");
        addFileToZip(zos, deliveryNoteFile, mainFormatterBookId + ".pdf");

        // DONE
        zos.close();
        fos.close();

        // Clean up
        for (String filename : this.cleanupFiles) {
            System.out.println("Cleanup : " + filename);
            deleteFile(filename);
        }
    }

    protected boolean isLandspace(PDPage page) {
        PDRectangle box = page.findMediaBox();
        return box.getWidth() > box.getHeight();
    }

    protected void addFileToZip(ZipOutputStream zos, String filename, String zipEntryName) throws Exception {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filename);
        }
        catch (Exception e) {
            return;
        }

        ZipEntry entry = new ZipEntry(zipEntryName);
        zos.putNextEntry(entry);
               
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }
        zos.closeEntry();
        if (fis != null) {
            fis.close();
        }
    }

    protected void deleteFile(String filename) throws Exception {
        File file = new File(filename);
        file.delete();
    }
    
    protected String buildItemBarcode(String formattedBookId, String customerNumber, String code) throws Exception {
        String barcode = formattedBookId + customerNumber + code;
        int sum = 0;
        for (int i = 0; i < barcode.length(); i++) {
            int digit = Integer.parseInt(barcode.charAt(i) + "");
            int mul = (i%2 == 0) ? 1 : 3;
            sum += mul * digit;
        }

        int checkSum = (10 - (sum % 10)) % 10;

        return formattedBookId + customerNumber + code + checkSum;
    }

    protected void buildXmlMetadata(String xmlMetadataFilename, String shipmentId, String mainBookId, List<String> itemNumberList, List<String> pageNumberList) throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, properties.get("vmResourceDir"));
        ve.init();

        Template template = ve.getTemplate("IndividualOrder.xml.vm");
        Template itemTemplate = ve.getTemplate("Item.xml.vm");
        VelocityContext context = new VelocityContext();

        ///////////////////////////////
        // x. Set content
        Properties sp = new Properties();
        Date today = new Date();
        String shipmentPropertiesFilename = properties.get("omShipmentInfoFilesPath") + "/" + shipmentId + ".properties";
        sp.load(new InputStreamReader(
            new FileInputStream(shipmentPropertiesFilename),
            "UTF-8"
        ));
        
        context.put("fromCompany", sp.getProperty("fromCompany"));
        context.put("fromCompanyNumber", sp.getProperty("fromCompanyNumber"));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        context.put("sentDate", dateFormat.format(today));
        context.put("sentTime", timeFormat.format(today));

        context.put("fromPerson", sp.getProperty("fromPerson"));
        context.put("fromEmail", sp.getProperty("fromEmail"));

        String formattedBookId = this.reformatBookId(mainBookId);
        String customerNumber = properties.getProperty("customerNumber");
        
        String itemXML = "";
        int j = 0;
        for (String itemNumber : itemNumberList) {
            VelocityContext itemContext = new VelocityContext();
            itemContext.put("itemNumber", itemNumber);
            itemContext.put("itemTitle", sp.getProperty("itemTitle"));

            itemContext.put("contributorRole", sp.getProperty("contributorRole"));
            itemContext.put("contributorName", sp.getProperty("contributorName"));

            itemContext.put("itemHeight", 297);
            itemContext.put("itemWidth", 210);

            int numberOfPages = Integer.parseInt(pageNumberList.get(j));
            itemContext.put("itemNumPages", numberOfPages);
            itemContext.put("itemColouredPages", numberOfPages);
            String str = "1";
            for (int i = 2; i <= numberOfPages; i++) {
                str += "," + i;
            }
            itemContext.put("itemColouredPagesPosition", str);

            itemContext.put("itemPaper", sp.getProperty("itemPaper"));
            itemContext.put("itemBinding", sp.getProperty("itemBinding"));
            itemContext.put("itemBack", sp.getProperty("itemBack"));
            itemContext.put("itemFinish", sp.getProperty("itemFinish"));
            itemContext.put("itemJacket", sp.getProperty("itemJacket"));

            StringWriter itemWriter = new StringWriter();
            itemTemplate.merge(itemContext,itemWriter);
            itemXML += itemWriter.toString();

            j++;
        }
        context.put("itemXML", itemXML);
        
        context.put("itemNumberList", itemNumberList);
        context.put("orderNumber", formattedBookId);
        context.put("orderCustomerOrderNumber", sp.getProperty("customerOrderNumber"));
        context.put("orderCopies", sp.getProperty("orderCopies"));
        context.put("orderAddressLine1", sp.getProperty("orderAddressLine1"));
        context.put("orderAddressLine2", sp.getProperty("orderAddressLine2"));
        context.put("orderAddressLine3", sp.getProperty("orderAddressLine3"));
        context.put("orderStreet", sp.getProperty("orderStreet"));
        context.put("orderZip", sp.getProperty("orderZip"));
        context.put("orderCity", sp.getProperty("orderCity"));
        context.put("orderCountry", sp.getProperty("orderCountry"));
        context.put("orderInvoiceFile", formattedBookId + ".pdf");

        context.put("orderCustomsValue", sp.getProperty("orderCustomsValue"));
        context.put("orderShipping", sp.getProperty("orderShipping"));

        ///////////////////////////////
        // x. Render
        StringWriter writer = new StringWriter();
        template.merge(context,writer);
        PrintWriter printWriter = new PrintWriter(xmlMetadataFilename, "UTF-8");
        printWriter.write(writer.toString());
        printWriter.close();
    }

    protected void validateDocument(PDDocument document) throws Exception {
        List pages = document.getDocumentCatalog().getAllPages();
        int n = pages.size();
        if (n < 2) {
            throw new Exception("Number of pages should be >= 2");
        }

        if (this.properties == null) {
            throw new Exception("Properties must be set");
        }
    }

    protected void loadResources(PDDocument document) throws Exception {
        rgbIccColorSpace = new ICC_ColorSpace(ICC_Profile.getInstance(properties.getProperty("rgbIccFile")));
        cmykIccColorSpace = new ICC_ColorSpace(ICC_Profile.getInstance(properties.getProperty("cmykIccFile")));

        Map<String,PDColorSpace> colorSpaces = new HashMap<String, PDColorSpace>();
        COSArray iccArr = new COSArray();
        iccArr.add(COSName.getPDFName(PDICCBased.NAME));
        InputStream in = new FileInputStream(properties.getProperty("cmykIccFile"));
        PDStream pdStream = new PDStream(document, in);
        iccArr.add(pdStream);
        cmykPDIccColorSpace = new PDICCBased(iccArr);
        cmykPDIccColorSpace.setNumberOfComponents(4);
    }

    protected PDDocument convertDocumentToCMYK(PDDocument document) throws Exception {
        PDDocument convertedDocument = new PDDocument();

        List pages = document.getDocumentCatalog().getAllPages();
        for (int i = 0; i < pages.size(); i++) {
            PDPage page = (PDPage) pages.get(i);
            page = convertPageToCMYK(convertedDocument, page);
            convertedDocument.addPage(page);
        }

        return convertedDocument;
    }  

    protected PDPage convertPageToCMYK(PDDocument document, PDPage page) throws Exception {        
        ///////////////////////////////////////////
        // x. CONVERT RESOURCES
        PDResources resources = page.getResources();

        // Set CMYK ICC Color Space
        Map<String,PDColorSpace> colorSpaces = new HashMap<String, PDColorSpace>();
        colorSpaces.put("ICCBased", cmykPDIccColorSpace);
        resources.setColorSpaces(colorSpaces);
        
        // Convert Images
        Map<String,PDXObject> xobjects = resources.getXObjects();
        if (xobjects != null) {
            for (String key : xobjects.keySet()) {
                Object obj = xobjects.get(key);
                if (obj instanceof PDXObjectImage) {
                    PDXObjectImage xobject = (PDXObjectImage) obj;
                    xobject.setColorSpace(cmykPDIccColorSpace);
                    xobjects.put(key, xobject);
                }
            }
        }
        resources.setXObjects(xobjects);

        page.setResources(resources);

        ////////////////////////////////////
        // x. Convert text
        PDFStreamParser parser = new PDFStreamParser(page.getContents().getStream());
        parser.parse();            
        List pageTokens = parser.getTokens();
        List editedPageTokens = new ArrayList();

        for (int i = 0; i < pageTokens.size(); i++) {
            Object token = pageTokens.get(i);
            
            if( token instanceof PDFOperator) {
                PDFOperator tokenOperator = (PDFOperator) token;
                String operator = tokenOperator.getOperation();

                if (operator.equals("RG")) {
                    replaceRBGToCMYK(pageTokens, editedPageTokens, i,
                        PDFOperator.getOperator("CS"),
                        PDFOperator.getOperator("SC")
                    );
                }
                else if (operator.equals("rg")) {
                    replaceRBGToCMYK(pageTokens, editedPageTokens, i,
                        PDFOperator.getOperator("cs"),
                        PDFOperator.getOperator("sc")
                    );
                }
                else if (operator.equals("G")) {
                    replaceGrayToCMYK(pageTokens, editedPageTokens, i, 
                        PDFOperator.getOperator("CS"),
                        PDFOperator.getOperator("SC")
                    );
                }
                else if (operator.equals("g")) {
                    replaceGrayToCMYK(pageTokens, editedPageTokens, i, 
                        PDFOperator.getOperator("cs"),
                        PDFOperator.getOperator("sc")
                    );
                }
                else if (operator.equals("K")) {
                    replaceCMYKToCMYK(pageTokens, editedPageTokens, i, 
                        PDFOperator.getOperator("CS"),
                        PDFOperator.getOperator("SC")
                    );

                }
                else if (operator.equals("k")) {
                    replaceCMYKToCMYK(pageTokens, editedPageTokens, i, 
                        PDFOperator.getOperator("cs"),
                        PDFOperator.getOperator("sc")
                    );
                }
                else {
                    editedPageTokens.add(token);    
                }
            }
            else {
                editedPageTokens.add(token);
            }
        }

        PDStream updatedPageContents = new PDStream(document);
        ContentStreamWriter contentWriter = new ContentStreamWriter(updatedPageContents.createOutputStream());
        contentWriter.writeTokens(editedPageTokens);
        page.setContents(updatedPageContents);

        return page;
    }

    private void replaceRBGToCMYK(List pageTokens, List editedPageTokens, int currentPosition, PDFOperator csOperator, PDFOperator scOperator) throws Exception {
        // Current R,G,B values
        float red = ((COSNumber)pageTokens.get(currentPosition - 3)).floatValue();
        float green = ((COSNumber)pageTokens.get(currentPosition - 2)).floatValue();
        float blue = ((COSNumber)pageTokens.get(currentPosition - 1)).floatValue();        

        float[] cmyk = convertRGBToCMYK(red, green, blue);

        editedPageTokens.remove(editedPageTokens.size() -1);
        editedPageTokens.remove(editedPageTokens.size() -1);
        editedPageTokens.remove(editedPageTokens.size() -1);
               
        editedPageTokens.add(COSName.ICCBASED);
        editedPageTokens.add(csOperator);

        // Add the new CMYK color
        editedPageTokens.add(new COSFloat( cmyk[0]));
        editedPageTokens.add(new COSFloat( cmyk[1]));
        editedPageTokens.add(new COSFloat( cmyk[2]));
        editedPageTokens.add(new COSFloat( cmyk[3]));
        editedPageTokens.add(scOperator);
    }

    private void replaceGrayToCMYK(List pageTokens, List editedPageTokens, int currentPosition, PDFOperator csOperator, PDFOperator scOperator) {
        // Current gray values
        float gray = ((COSNumber)pageTokens.get(currentPosition - 1)).floatValue();        

        // Remove gray value
        editedPageTokens.remove(editedPageTokens.size() -1);

        editedPageTokens.add(COSName.ICCBASED);
        editedPageTokens.add(csOperator);

        float[] cmyk = convertRGBToCMYK(gray, gray, gray);
        // Add the new CMYK color
        editedPageTokens.add(new COSFloat( cmyk[0]));
        editedPageTokens.add(new COSFloat( cmyk[1]));
        editedPageTokens.add(new COSFloat( cmyk[2]));
        editedPageTokens.add(new COSFloat( cmyk[3]));
        editedPageTokens.add(scOperator);
    }


    private void replaceCMYKToCMYK(List pageTokens, List editedPageTokens, int currentPosition, PDFOperator csOperator, PDFOperator scOperator) throws Exception {
        // Current CMYK values
        float c = ((COSNumber)pageTokens.get(currentPosition - 4)).floatValue();
        float m = ((COSNumber)pageTokens.get(currentPosition - 3)).floatValue();
        float y = ((COSNumber)pageTokens.get(currentPosition - 2)).floatValue();
        float k = ((COSNumber)pageTokens.get(currentPosition - 1)).floatValue();        

        editedPageTokens.remove(editedPageTokens.size() -1);
        editedPageTokens.remove(editedPageTokens.size() -1);
        editedPageTokens.remove(editedPageTokens.size() -1);
        editedPageTokens.remove(editedPageTokens.size() -1);
               
        editedPageTokens.add(COSName.ICCBASED);
        editedPageTokens.add(csOperator);

        // Add the new CMYK color
        editedPageTokens.add(new COSFloat( c));
        editedPageTokens.add(new COSFloat( m));
        editedPageTokens.add(new COSFloat( y));
        editedPageTokens.add(new COSFloat( k));
        editedPageTokens.add(scOperator);
    }

    private float[] convertRGBToCMYK( float red, float green, float blue )
    {
        /*float[] ciexyz = rbgICCColorSpace.toCIEXYZ(new float[] { red, green, blue });
        float[] cmyk = cmykIccColorSpace.fromCIEXYZ(ciexyz);
        return cmyk;*/
        float[] cmyk = cmykIccColorSpace.fromRGB(new float[] { red, green, blue });
        
        return cmyk;
    }
}