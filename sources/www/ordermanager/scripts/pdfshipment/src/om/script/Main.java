package om.script;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import org.apache.pdfbox.exceptions.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.edit.*;
import org.apache.pdfbox.pdmodel.common.*;
import org.apache.pdfbox.pdmodel.font.*;
import org.apache.pdfbox.pdmodel.graphics.color.*;
import org.apache.pdfbox.pdmodel.graphics.xobject.*;
import org.apache.pdfbox.util.*;
import org.apache.pdfbox.pdfwriter.*;
import org.apache.pdfbox.pdfparser.*;
import org.apache.pdfbox.cos.*;
import org.apache.pdfbox.io.*;

import java.awt.color.*;
import java.awt.image.*;
import java.awt.RenderingHints;

import java.util.zip.*;

public class Main {
    public static void main(String[] args) {
        PrintWriter errorWriter = null;

        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("resources/config.properties"));

            String shipmentId = args[0];
            errorWriter = new PrintWriter("files/output/" + shipmentId + ".txt");

            String customerNumber = args[1];
            properties.setProperty("customerNumber", customerNumber);

            String shipmentPropertiesFilename = properties.get("omShipmentInfoFilesPath") + "/" + shipmentId + ".properties";
            Properties sp = new Properties();
            sp.load(new InputStreamReader(
                new FileInputStream(shipmentPropertiesFilename),
                "UTF-8"
            ));

            String filename = sp.getProperty("pdfFilePath");
            String orderNumber = sp.getProperty("customerOrderNumber");
            PdfOndemandPackageBuilder builder = new PdfOndemandPackageBuilder();
            builder.setProperties(properties);
            builder.buildPackage(sp, shipmentId);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            errorWriter.write(sw.toString());
        } finally {
            errorWriter.close();
        }
    }
}