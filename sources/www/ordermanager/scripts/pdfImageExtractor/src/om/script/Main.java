package om.script;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import om.script.model.*;

public class Main {
    public static void main(String[] args) throws Exception
    {
        if (args.length < 3) {
            System.out.println("DM");
        }

        String inputPdfFile = args[0];
        String outputFolder = args[1];
        
        List<Zone> zones = new ArrayList<Zone>();
        String[] tokens = args[2].split(";");
        for (String token: tokens) {
            String[] zoneTokens = token.split(",");
            if (zoneTokens.length == 6) {
                Zone zone = new Zone();
                zone.pageNumber = Integer.parseInt(zoneTokens[0]);
                zone.x = Integer.parseInt(zoneTokens[1]);
                zone.y = Integer.parseInt(zoneTokens[2]);
                zone.width = Integer.parseInt(zoneTokens[3]);
                zone.height = Integer.parseInt(zoneTokens[4]);
                zone.code = zoneTokens[5].trim();
                zones.add(zone);
            }
        }

        ImageExtractor extractor = new ImageExtractor();
        extractor.extractImages(inputPdfFile, outputFolder, zones);
    }
}