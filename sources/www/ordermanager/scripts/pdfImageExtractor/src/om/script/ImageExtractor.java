package om.script;

import java.io.*;
import java.util.*;
import java.awt.image.*;
import javax.imageio.*;

import org.apache.pdfbox.cos.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.encryption.*;
import org.apache.pdfbox.pdmodel.graphics.xobject.*;
import org.apache.pdfbox.io.*;

import om.script.model.*;

public class ImageExtractor {
    public void extractImages(String inputPdfFile, String outputFolder, List<Zone> zones) throws Exception {
        PDDocument document = PDDocument.loadNonSeq(new File(inputPdfFile), null);
        List pages = document.getDocumentCatalog().getAllPages();
        Iterator iter = pages.iterator();
        int pageIndex = 0;
        while (iter.hasNext()) {
            PDPage page = (PDPage) iter.next();
            pageIndex++;
            float pageWidth = page.getMediaBox().getWidth();
            float pageHeight = page.getMediaBox().getHeight();

            PDResources resources = page.getResources();
            if (resources == null) continue;
            Map<String, PDXObject> xobjects = resources.getXObjects();
            if (xobjects == null) continue;
            Iterator<String> xobjectIter = xobjects.keySet().iterator();

            while(xobjectIter.hasNext()) {
                String key = xobjectIter.next();
                PDXObject xobject = xobjects.get(key);
                if (xobject instanceof PDXObjectImage) {
                    PDXObjectImage image = (PDXObjectImage) xobject;                    
                    BufferedImage imageBuff = image.getRGBImage();
                    float imageWidth = imageBuff.getWidth();
                    float imageHeight = imageBuff.getHeight();

                    for (Zone zone : zones) {
                        if (zone.pageNumber == pageIndex) {
                            float[] scaleRatio = computeScaleRatio(pageWidth, pageHeight, imageWidth, imageHeight);
                            int x = (int) (scaleRatio[0] * zone.x);
                            int y = (int) (scaleRatio[1] * zone.y);
                            int width = (int) (scaleRatio[0] * zone.width);
                            int height = (int) (scaleRatio[1] * zone.height);

                            if (x > imageWidth || y > imageHeight) continue;
                            if (x + width + 5 > imageWidth) { width = (int)imageWidth - x - 10; }
                            if (y + height + 5 > imageHeight) { height = (int)imageHeight - x - 10; }
                            try {
                                BufferedImage extractedImage = imageBuff.getSubimage(x, y, width, height);
                                File fileOutput = new File(outputFolder + "/" + zone.code + "." + image.getSuffix());
                                ImageIO.write(extractedImage, image.getSuffix(), fileOutput);
                            }
                            catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    }
                    break;
                }
            }
        }
        document.close();
    }

    public float[] computeScaleRatio(float pageWidth, float pageHeight, float imageWidth, float imageHeight) {
        float pageRatio = pageWidth / pageHeight;
        float imageRatio = imageWidth / imageHeight;

        float fitWidth = 0;
        float fitHeight = 0;
        if (pageRatio > imageRatio) {
            fitWidth = pageHeight * imageRatio;
            fitHeight = pageHeight;
        }
        else {
            fitWidth = pageWidth;
            fitHeight = pageWidth / imageRatio;
        }

        return new float[] { imageWidth / fitWidth, imageHeight / fitHeight };
    }
}