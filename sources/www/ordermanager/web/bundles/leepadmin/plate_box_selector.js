function clickAppPlateBoxSelectorColumn(idWellTable, columnNumber) {
    var wellTable = jQuery('#' + idWellTable + "_wellTable");
    var columnsSelected = jQuery('#' + idWellTable + "_columnSelect_" + columnNumber);

    var val = columnsSelected.val();

    wellTable.find("td[col-id='" + columnNumber + "']").each(function() {
        if (val == '') {
            jQuery(this).css('background-color', '#fffaaa');
            columnsSelected.val('1');
        }
        else {
            jQuery(this).css('background-color', '');
            columnsSelected.val('');
        }
    })
}