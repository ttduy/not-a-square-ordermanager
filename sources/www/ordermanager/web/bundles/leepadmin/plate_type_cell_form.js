(function ($) {
    "use strict";
    
    var PlateTypeCellForm = function (options) {
        this.init('plateTypeCellForm', options, PlateTypeCellForm.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(PlateTypeCellForm, $.fn.editabletypes.abstractinput);

    $.extend(PlateTypeCellForm.prototype, {
        render: function() {
            this.$input = this.$tpl.find('input');
            this.$select = this.$tpl.find('select');

            var wellIndex = this.$input.filter('[name="plateWellIndex"]');
            wellIndex.val("ABCD");

            var geneSelect = this.$select.filter('[name="idGene"]');

            var geneListJSON = $('#geneList').val();
            var geneList = {};
            if (geneListJSON) {
                geneList = JSON.parse(geneListJSON);
            }
            $.each(geneList, function(key, obj) {
                geneSelect
                 .append($("<option></option>")
                 .attr("value",obj.id)
                 .text(obj.name)); 
            });
        },

        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }

            if (value.plateWellIndex == '') {
                $(element).empty();
                return;
            }

            if (value.idGene == '') {
                $(element).empty();
                return;
            }

            jQuery(this.options.saveField).val(value.plateWellIndex + "!!" + value.idGene + "!!" + value.color);

            var geneRefJSON = $('#geneRef').val();
            var geneRef = {};
            if (geneRefJSON) {
                geneRef = JSON.parse(geneRefJSON);
            }

            if (value.idGene) {
                var geneName = geneRef[value.idGene];
                $(element).html('<text style="color: ' + value.color + '">' + value.plateWellIndex + "<br/>" + geneName + '</text>');
            } else {
                $(element).html('<text style="color: ' + value.color + '">' + value.plateWellIndex + '</text>');
            }

        },
        

        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
    
        value2str: function(value) {
            var strVal = value.plateWellIndex + "!!" + value.idGene + "!!" + value.color;
            return strVal;
        }, 
       
        str2value: function(str) {
            if (typeof str == "string") {
                var arr = str.split("!!");
            
                return {
                    plateWellIndex: arr[0],
                    idGene: arr[1],
                    color: arr[2]
                };
            }
            return str;
        },                
       
        value2input: function(value) {
            if(!value) {
                return;
            }
            this.$input.filter('[name="plateWellIndex"]').val(value.plateWellIndex);
            this.$select.filter('[name="idGene"]').find('option').filter('[value="' + value.idGene + '"]').prop('selected', true);
            this.$input.filter('[name="color"]').val(value.color);
        },       

        input2value: function() { 
            return {
                plateWellIndex: this.$input.filter('[name="plateWellIndex"]').val(), 
                color: this.$input.filter('[name="color"]').val(), 
                idGene: this.$select.filter('[name="idGene"]').find(':selected').val(), 
            };
        },        

        activate: function() {
            this.$input.filter('[name="plateWellIndex"]').focus();
            this.$input.filter('[name="color"]').focus();
            this.$select.filter('[name="idGene"]').focus();
        },  
       
        autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
        }
    });

    PlateTypeCellForm.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address"><label><span>Plate Well Index: </span><input type="text" name="plateWellIndex" style="max-width: 190px"></label></div>'+
             '<div class="editable-address"><label><span>Gene: </span><select name="idGene" style="max-width: 190px"></select></label></div>' + 
             '<div class="editable-address"><label><span>Color: </span><input type="text" name="color" style="max-width: 190px"></label></div>',
        inputclass: '',
        saveField: ''
    });

    $.fn.editabletypes.plateTypeCellForm = PlateTypeCellForm;

}(window.jQuery));