(function ($) {
    "use strict";
    
    var PlateCellForm = function (options) {
        this.init('plateCellForm', options, PlateCellForm.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(PlateCellForm, $.fn.editabletypes.abstractinput);

    $.extend(PlateCellForm.prototype, {
        render: function() {
            this.$input = this.$tpl.find('input');

            this.$input.filter('[name="orderNumber"]').val("ABCD");

            this.$input.filter('[name="date"]').datepicker({
                dateFormat: "dd/mm/yy",
                onSelect: function ()
                {
                    this.focus();
                }
            });
        },

        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }

            if (value.orderNumber == '') {
                $(element).empty();
                return;
            }

            jQuery(this.options.saveField).val(value.orderNumber + "##" + value.date);
            if (value.date != '') {
                var arr = value.date.split('/');

                var today = new Date();
                var date = new Date(arr[2], parseInt(arr[1]) - 1, arr[0], 0, 0, 0, 0);


                var timediff = today.getTime() - date.getTime();
                var days = Math.floor(timediff / 86400000);
                var dayStr = days + " day(s)";
                if (days == 0) {
                    dayStr = 'Today';
                }

                $(element).html(value.orderNumber + "<br/>" + dayStr); 
            }
            else {
                $(element).html(value.orderNumber);
            }
        },
        

        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
    
        value2str: function(value) {
            var strVal = value.orderNumber + "##" + value.date;
            return strVal;
        }, 
       
        str2value: function(str) {
            if (typeof str == "string") {
                var arr = str.split("##");
            
                return {
                    orderNumber: arr[0],
                    date: arr[1]
                };
            }
            return str;
        },                
       
        value2input: function(value) {
            if(!value) {
                return;
            }
            this.$input.filter('[name="orderNumber"]').val(value.orderNumber);
            this.$input.filter('[name="date"]').val(value.date);
        },       

        input2value: function() { 
            return {
                orderNumber: this.$input.filter('[name="orderNumber"]').val(), 
                date: this.$input.filter('[name="date"]').val(), 
            };
        },        

        activate: function() {
            this.$input.filter('[name="orderNumber"]').focus();
        },  
       
        autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
        }
    });

    PlateCellForm.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address"><label><span>Order Number: </span><input type="text" name="orderNumber" style="max-width: 190px"></label></div>'+
             '<div class="editable-address"><label><span>Date: </span><input type="text" name="date" style="max-width: 190px"></label></div>',             
        inputclass: '',
        saveField: ''
    });

    $.fn.editabletypes.plateCellForm = PlateCellForm;

}(window.jQuery));