jQuery(document).ready(function(){
    jQuery('.popupButton').colorbox({
        iframe: true,
        width:  "75%",
        height: "75%"
    });
});


function formSubmit(formId) {
    var element = document.getElementById(formId);
    if (element) {
        element.submit();
    }
}

function dataTableActionWithSelected(tableId, url) {
    var selected = '';
    jQuery('#' + tableId + ' input[type=checkbox]').each(function(){
        if (jQuery(this).is(':checked')) {
            selected += jQuery(this).attr('id').substring(4) + ",";
        }
    });

    url += '?selected=' + selected;
    document.location = url;
}

function showAjaxUpdate(id) {
    jQuery('#' + id + '_textbox').css('display', 'inline-block');
    jQuery('#' + id + '_field').css('display', 'none');
    jQuery('#' + id + '_link').css('display', 'none');
}

function ajaxUpdate(id, targetUrl) {
    jQuery('#' + id + '_textbox').css('display', 'none');
    jQuery('#' + id + '_field').css('display', 'inline-block');
    jQuery('#' + id + '_link').css('display', 'inline-block');

    var val = jQuery('#' + id).val();

    var jqxhr = jQuery.ajax({
        url:  targetUrl,
        type: 'GET',
        data: { fieldVal: val },
    }).done(function( msg ) {
        jQuery('#' + id + '_field').html(val);
    }).fail(function(jqXHR, textStatus) {
        alert("Failed: " + textStatus );
    });
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


/** TABLE COLLECTION **/
function tableCollectionAddRow(collectionId) {
    var container = jQuery('#container_' + collectionId);
    var index = parseInt(container.attr('index'));

    var prototype = jQuery('#prototype_' + collectionId).attr('data-prototype');
    prototype = prototype.replace(/__name__/g, index);
    var newRow = jQuery(prototype);
    container.attr('index', index+1);
    container.append(newRow);

    tableCollectionReindex(collectionId);
    tableCollectionAfterUpdateRow(collectionId);
}

function tableCollectionRemoveRow(collectionId, rowIndex) {
    var row = jQuery('#container_row_' + collectionId + '_' + rowIndex);
    row.remove();

    tableCollectionReindex(collectionId);
    tableCollectionAfterUpdateRow(collectionId);
}

function tableCollectionInsertRow(collectionId, rowIndex) {
    var container = jQuery('#container_' + collectionId);
    var index = parseInt(container.attr('index'));

    var prototype = jQuery('#prototype_' + collectionId).attr('data-prototype');
    prototype = prototype.replace(/__name__/g, index);
    var newRow = jQuery(prototype);
    container.attr('index', index+1);

    var targetRow = jQuery('#container_row_' + collectionId + '_' + rowIndex);
    var targetRow = container.find(targetRow);
    newRow.insertBefore(targetRow);

    tableCollectionReindex(collectionId);
    tableCollectionAfterUpdateRow(collectionId);
}

function tableCollectionReindex(collectionId) {

}

function tableCollectionAfterUpdateRow() {
}