import boto3
import botocore
import os.path

print("Create AWS S3 Client using Boto3.\n")
client = boto3.client(
	's3',
	aws_access_key_id="AKIAIVPJXDW5X5CQWNGA",
	aws_secret_access_key="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"
)

with open("backup_files.txt") as backup_files:
	files = backup_files.readlines()
	print("Download backup orders.\n")

	for backup_file in files:
		backup_file = backup_file.strip()
		if backup_file == "":
			continue

		print("----------------------------------------\n")
		if os.path.isfile("Backups/" + backup_file):
			print("Backup file: " + backup_file + " already existed. Skip!")
			continue

		print("Downloading: " + backup_file + ".\n This may take a while so please be patients!\n")
		try:
			client.download_file("order-backup", backup_file, "Backups/" + backup_file)
		except botocore.exceptions.ClientError as e:
			error = ""
			if e.response['Error']['Code'] == 'EntityAlreadyExists':
				error = "User already exists"
			else:
				error = "Unexpected error: %s" % e
			print("Error: %s" % e)

			with open("Logs/error.log", "a+") as error_log:
				error_log.write("(%s) Error: %s\n" % (backup_file, e))

print("Finished")
