/*
SQLyog Ultimate v9.02 
MySQL - 5.5.31-0ubuntu0.12.04.2 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `laboratory_result` (
	`Id` int (10),
	`Result` varchar (54),
	`ResultId` int (18)
); 
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('1','A','16');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('2','A-',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('3','AC','20');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('4','AG','17');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('5','AT','23');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('6','C','21');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('7','C-',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('8','CA','20');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('9','CG','41');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('10','CT','38');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('11','DEL','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('12','DEL.C','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('13','G','18');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('14','G-','77');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('15','GA','17');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('16','GC','41');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('17','GT','29');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('18','INS.C','76');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('19','NEG','84');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('20','POS','82');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('21','T','24');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('22','T-',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('23','TA','23');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('24','TC','38');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('25','TG','29');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('26','NA',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('27','G/G','18');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('28','-/-','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('29','A/A','16');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('30','A/C','20');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('31','A/G','17');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('32','A/T','23');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('33','C/C','21');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('34','C/G','41');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('35','C/T','38');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('36','G/T','29');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('37','T/T','24');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('38','GG','18');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('39','AA','16');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('40','CC','21');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('41','G/-','77');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('42','GDEL',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('43','G/del',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('44','del/del','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('45','0',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('46','DELG',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('47','Ins/Ins','76');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('48','Tdel',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('49','Adel',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('50','A.DEL',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('51','4b/4b','76');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('52','4a/4a','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('53','4a/4b','77');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('54','4b/4a','77');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('55','4a','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('56','4b','76');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('57','4A','78');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('58','4B','76');
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('59','DELT',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('60','SHORT',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('61','s',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('62','S',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('63','LONG',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('64','l',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('65','L',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('66','sl',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('67','ls',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('68','SHORT/LONG',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('69','LONG/SHORT',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('70','S/L',NULL);
insert into `laboratory_result` (`Id`, `Result`, `ResultId`) values('71','L/S',NULL);
