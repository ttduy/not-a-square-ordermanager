<?php

namespace App\Common\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AppCommonUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
