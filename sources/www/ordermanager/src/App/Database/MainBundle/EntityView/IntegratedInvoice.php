<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\IntegratedInvoice
 *
 * @ORM\Table(name="integrated_invoice")
 * @ORM\Entity
 */
class IntegratedInvoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     */
    public $id;

    /**
     * @var string $invoiceType
     *
     * @ORM\Column(name="invoice_type", type="string", length=255, nullable=true)
     */
    public $invoiceType;

    /**
     * @var string $invoiceName
     *
     * @ORM\Column(name="invoice_name", type="string", length=255, nullable=true)
     */
    public $invoiceName;

    /**
     * @var string $invoiceNumber
     *
     * @ORM\Column(name="invoice_number", type="string", length=255, nullable=true)
     */
    public $invoiceNumber;


    /**
     * @var \DateTime $invoiceDate
     *
     * @ORM\Column(name="invoice_date", type="date", nullable=true)
     */
    public $invoiceDate;

    /**
     * @var integer $invoiceStatus
     *
     * @ORM\Column(name="invoice_status", type="integer", nullable=true)
     */
    public $invoiceStatus;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    public $statusDate;

    /**
     * @var float $invoiceAmount
     *
     * @ORM\Column(name="invoice_amount", type="decimal", nullable=true)
     */
    public $invoiceAmount;
    /**
     * @var float $isWithTax
     *
     * @ORM\Column(name="is_with_tax", type="decimal", nullable=true)
     */
    public $isWithTax;
    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    public $taxValue;
}