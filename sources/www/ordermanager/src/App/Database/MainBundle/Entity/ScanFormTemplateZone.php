<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormTemplateZone
 *
 * @ORM\Table(name="scan_form_template_zone")
 * @ORM\Entity
 */
class ScanFormTemplateZone
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormTemplate
     *
     * @ORM\Column(name="id_scan_form_template", type="integer", nullable=true)
     */
    private $idScanFormTemplate;

    /**
     * @var integer $pageNumber
     *
     * @ORM\Column(name="page_number", type="integer", nullable=true)
     */
    private $pageNumber;

    /**
     * @var string $zonesDefinition
     *
     * @ORM\Column(name="zones_definition", type="text", nullable=true)
     */
    private $zonesDefinition;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormTemplate
     *
     * @param integer $idScanFormTemplate
     * @return ScanFormTemplateZone
     */
    public function setIdScanFormTemplate($idScanFormTemplate)
    {
        $this->idScanFormTemplate = $idScanFormTemplate;
    
        return $this;
    }

    /**
     * Get idScanFormTemplate
     *
     * @return integer 
     */
    public function getIdScanFormTemplate()
    {
        return $this->idScanFormTemplate;
    }

    /**
     * Set pageNumber
     *
     * @param integer $pageNumber
     * @return ScanFormTemplateZone
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
    
        return $this;
    }

    /**
     * Get pageNumber
     *
     * @return integer 
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * Set zonesDefinition
     *
     * @param string $zonesDefinition
     * @return ScanFormTemplateZone
     */
    public function setZonesDefinition($zonesDefinition)
    {
        $this->zonesDefinition = $zonesDefinition;
    
        return $this;
    }

    /**
     * Get zonesDefinition
     *
     * @return string 
     */
    public function getZonesDefinition()
    {
        return $this->zonesDefinition;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return ScanFormTemplateZone
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}