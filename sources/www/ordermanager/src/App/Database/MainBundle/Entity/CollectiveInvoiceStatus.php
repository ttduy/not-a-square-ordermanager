<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectiveInvoiceStatus
 *
 * @ORM\Table(name="collective_invoice_status")
 * @ORM\Entity
 */
class CollectiveInvoiceStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $collectiveinvoiceid
     *
     * @ORM\Column(name="CollectiveInvoiceId", type="integer", nullable=false)
     */
    private $collectiveinvoiceid;

    /**
     * @var \DateTime $statusdate
     *
     * @ORM\Column(name="StatusDate", type="date", nullable=true)
     */
    private $statusdate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=true)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectiveinvoiceid
     *
     * @param integer $collectiveinvoiceid
     * @return CollectiveInvoiceStatus
     */
    public function setCollectiveinvoiceid($collectiveinvoiceid)
    {
        $this->collectiveinvoiceid = $collectiveinvoiceid;
    
        return $this;
    }

    /**
     * Get collectiveinvoiceid
     *
     * @return integer 
     */
    public function getCollectiveinvoiceid()
    {
        return $this->collectiveinvoiceid;
    }

    /**
     * Set statusdate
     *
     * @param \DateTime $statusdate
     * @return CollectiveInvoiceStatus
     */
    public function setStatusdate($statusdate)
    {
        $this->statusdate = $statusdate;
    
        return $this;
    }

    /**
     * Get statusdate
     *
     * @return \DateTime 
     */
    public function getStatusdate()
    {
        return $this->statusdate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CollectiveInvoiceStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CollectiveInvoiceStatus
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return CollectiveInvoiceStatus
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}