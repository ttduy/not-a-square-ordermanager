<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormStep
 *
 * @ORM\Table(name="form_step")
 * @ORM\Entity
 */
class FormStep
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFormVersion
     *
     * @ORM\Column(name="id_form_version", type="integer", nullable=true)
     */
    private $idFormVersion;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormVersion
     *
     * @param integer $idFormVersion
     * @return FormStep
     */
    public function setIdFormVersion($idFormVersion)
    {
        $this->idFormVersion = $idFormVersion;
    
        return $this;
    }

    /**
     * Get idFormVersion
     *
     * @return integer 
     */
    public function getIdFormVersion()
    {
        return $this->idFormVersion;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FormStep
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return FormStep
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}