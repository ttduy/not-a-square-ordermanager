<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerInfoGene
 *
 * @ORM\Table(name="customer_info_gene")
 * @ORM\Entity
 */
class CustomerInfoGene
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idGene
     *
     * @ORM\Column(name="id_gene", type="integer", nullable=true)
     */
    private $idGene;

    /**
     * @var string $result
     *
     * @ORM\Column(name="result", type="string", length=50, nullable=true)
     */
    private $result;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return CustomerInfoGene
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idGene
     *
     * @param integer $idGene
     * @return CustomerInfoGene
     */
    public function setIdGene($idGene)
    {
        $this->idGene = $idGene;
    
        return $this;
    }

    /**
     * Get idGene
     *
     * @return integer 
     */
    public function getIdGene()
    {
        return $this->idGene;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return CustomerInfoGene
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }
}