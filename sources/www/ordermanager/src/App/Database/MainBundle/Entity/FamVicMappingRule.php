<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FamVicMappingRule
 *
 * @ORM\Table(name="fam_vic_mapping_rule")
 * @ORM\Entity
 */
class FamVicMappingRule
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idResultGroup
     *
     * @ORM\Column(name="id_result_group", type="integer", nullable=true)
     */
    private $idResultGroup;

    /**
     * @var boolean $resultVic
     *
     * @ORM\Column(name="result_vic", type="boolean", nullable=true)
     */
    private $resultVic;

    /**
     * @var boolean $resultFam
     *
     * @ORM\Column(name="result_fam", type="boolean", nullable=true)
     */
    private $resultFam;

    /**
     * @var string $result
     *
     * @ORM\Column(name="result", type="string", length=255, nullable=true)
     */
    private $result;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idResultGroup
     *
     * @param integer $idResultGroup
     * @return FamVicMappingRule
     */
    public function setIdResultGroup($idResultGroup)
    {
        $this->idResultGroup = $idResultGroup;
    
        return $this;
    }

    /**
     * Get idResultGroup
     *
     * @return integer 
     */
    public function getIdResultGroup()
    {
        return $this->idResultGroup;
    }

    /**
     * Set resultVic
     *
     * @param boolean $resultVic
     * @return FamVicMappingRule
     */
    public function setResultVic($resultVic)
    {
        $this->resultVic = $resultVic;
    
        return $this;
    }

    /**
     * Get resultVic
     *
     * @return boolean 
     */
    public function getResultVic()
    {
        return $this->resultVic;
    }

    /**
     * Set resultFam
     *
     * @param boolean $resultFam
     * @return FamVicMappingRule
     */
    public function setResultFam($resultFam)
    {
        $this->resultFam = $resultFam;
    
        return $this;
    }

    /**
     * Get resultFam
     *
     * @return boolean 
     */
    public function getResultFam()
    {
        return $this->resultFam;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return FamVicMappingRule
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }
}