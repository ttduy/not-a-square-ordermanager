<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Pellet
 *
 * @ORM\Table(name="pellet")
 * @ORM\Entity
 */
class Pellet
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $expiryDateWarningThreshold
     *
     * @ORM\Column(name="expiry_date_warning_threshold", type="integer", nullable=true)
     */
    private $expiryDateWarningThreshold;

    /**
     * @var integer $itemWarningThreshold
     *
     * @ORM\Column(name="item_warning_threshold", type="integer", nullable=true)
     */
    private $itemWarningThreshold;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $letter
     *
     * @ORM\Column(name="letter", type="string", length=10, nullable=true)
     */
    private $letter;

    /**
     * @var string $substanceName
     *
     * @ORM\Column(name="substance_name", type="string", length=255, nullable=true)
     */
    private $substanceName;

    /**
     * @var string $activeIngredientName
     *
     * @ORM\Column(name="active_ingredient_name", type="string", length=255, nullable=true)
     */
    private $activeIngredientName;

    /**
     * @var float $weightPerCup
     *
     * @ORM\Column(name="weight_per_cup", type="float", nullable=true)
     */
    private $weightPerCup;

    /**
     * @var integer $weightWarningThreshold
     *
     * @ORM\Column(name="weight_warning_threshold", type="integer", nullable=true)
     */
    private $weightWarningThreshold;

    /**
     * @var string $supplierDeclaration
     *
     * @ORM\Column(name="supplier_declaration", type="text", nullable=true)
     */
    private $supplierDeclaration;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float $unitsPerPrice
     *
     * @ORM\Column(name="units_per_price", type="float", nullable=true)
     */
    private $unitsPerPrice;

    /**
     * @var string $unitDescription
     *
     * @ORM\Column(name="unit_description", type="string", length=255, nullable=true)
     */
    private $unitDescription;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expiryDateWarningThreshold
     *
     * @param integer $expiryDateWarningThreshold
     * @return Pellet
     */
    public function setExpiryDateWarningThreshold($expiryDateWarningThreshold)
    {
        $this->expiryDateWarningThreshold = $expiryDateWarningThreshold;
    
        return $this;
    }

    /**
     * Get expiryDateWarningThreshold
     *
     * @return integer 
     */
    public function getExpiryDateWarningThreshold()
    {
        return $this->expiryDateWarningThreshold;
    }

    /**
     * Set itemWarningThreshold
     *
     * @param integer $itemWarningThreshold
     * @return Pellet
     */
    public function setItemWarningThreshold($itemWarningThreshold)
    {
        $this->itemWarningThreshold = $itemWarningThreshold;
    
        return $this;
    }

    /**
     * Get itemWarningThreshold
     *
     * @return integer 
     */
    public function getItemWarningThreshold()
    {
        return $this->itemWarningThreshold;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Pellet
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Pellet
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set letter
     *
     * @param string $letter
     * @return Pellet
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;
    
        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set substanceName
     *
     * @param string $substanceName
     * @return Pellet
     */
    public function setSubstanceName($substanceName)
    {
        $this->substanceName = $substanceName;
    
        return $this;
    }

    /**
     * Get substanceName
     *
     * @return string 
     */
    public function getSubstanceName()
    {
        return $this->substanceName;
    }

    /**
     * Set activeIngredientName
     *
     * @param string $activeIngredientName
     * @return Pellet
     */
    public function setActiveIngredientName($activeIngredientName)
    {
        $this->activeIngredientName = $activeIngredientName;
    
        return $this;
    }

    /**
     * Get activeIngredientName
     *
     * @return string 
     */
    public function getActiveIngredientName()
    {
        return $this->activeIngredientName;
    }

    /**
     * Set weightPerCup
     *
     * @param float $weightPerCup
     * @return Pellet
     */
    public function setWeightPerCup($weightPerCup)
    {
        $this->weightPerCup = $weightPerCup;
    
        return $this;
    }

    /**
     * Get weightPerCup
     *
     * @return float 
     */
    public function getWeightPerCup()
    {
        return $this->weightPerCup;
    }

    /**
     * Set weightWarningThreshold
     *
     * @param integer $weightWarningThreshold
     * @return Pellet
     */
    public function setWeightWarningThreshold($weightWarningThreshold)
    {
        $this->weightWarningThreshold = $weightWarningThreshold;
    
        return $this;
    }

    /**
     * Get weightWarningThreshold
     *
     * @return integer 
     */
    public function getWeightWarningThreshold()
    {
        return $this->weightWarningThreshold;
    }

    /**
     * Set supplierDeclaration
     *
     * @param string $supplierDeclaration
     * @return Pellet
     */
    public function setSupplierDeclaration($supplierDeclaration)
    {
        $this->supplierDeclaration = $supplierDeclaration;
    
        return $this;
    }

    /**
     * Get supplierDeclaration
     *
     * @return string 
     */
    public function getSupplierDeclaration()
    {
        return $this->supplierDeclaration;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Pellet
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set unitsPerPrice
     *
     * @param float $unitsPerPrice
     * @return Pellet
     */
    public function setUnitsPerPrice($unitsPerPrice)
    {
        $this->unitsPerPrice = $unitsPerPrice;
    
        return $this;
    }

    /**
     * Get unitsPerPrice
     *
     * @return float 
     */
    public function getUnitsPerPrice()
    {
        return $this->unitsPerPrice;
    }

    /**
     * Set unitDescription
     *
     * @param string $unitDescription
     * @return Pellet
     */
    public function setUnitDescription($unitDescription)
    {
        $this->unitDescription = $unitDescription;
    
        return $this;
    }

    /**
     * Get unitDescription
     *
     * @return string 
     */
    public function getUnitDescription()
    {
        return $this->unitDescription;
    }
}