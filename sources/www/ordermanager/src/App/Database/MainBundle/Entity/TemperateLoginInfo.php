<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TemperateLoginInfo
 *
 * @ORM\Table(name="temperate_login_info")
 * @ORM\Entity
 */
class TemperateLoginInfo
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $randCode
     *
     * @ORM\Column(name="rand_code", type="string", length=40, nullable=true)
     */
    private $randCode;

    /**
     * @var integer $entityId
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    private $entityId;

    /**
     * @var \DateTime $clickTime
     *
     * @ORM\Column(name="click_time", type="datetime", nullable=false)
     */
    private $clickTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set randCode
     *
     * @param string $randCode
     * @return TemperateLoginInfo
     */
    public function setRandCode($randCode)
    {
        $this->randCode = $randCode;
    
        return $this;
    }

    /**
     * Get randCode
     *
     * @return string 
     */
    public function getRandCode()
    {
        return $this->randCode;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     * @return TemperateLoginInfo
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    
        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set clickTime
     *
     * @param \DateTime $clickTime
     * @return TemperateLoginInfo
     */
    public function setClickTime($clickTime)
    {
        $this->clickTime = $clickTime;
    
        return $this;
    }

    /**
     * Get clickTime
     *
     * @return \DateTime 
     */
    public function getClickTime()
    {
        return $this->clickTime;
    }
}