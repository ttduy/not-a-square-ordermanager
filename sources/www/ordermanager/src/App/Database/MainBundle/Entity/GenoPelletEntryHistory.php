<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPelletEntryHistory
 *
 * @ORM\Table(name="geno_pellet_entry_history")
 * @ORM\Entity
 */
class GenoPelletEntryHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGenoPelletEntry
     *
     * @ORM\Column(name="id_geno_pellet_entry", type="integer", nullable=true)
     */
    private $idGenoPelletEntry;

    /**
     * @var string $whatChange
     *
     * @ORM\Column(name="what_change", type="string", length=255, nullable=true)
     */
    private $whatChange;

    /**
     * @var string $fromValue
     *
     * @ORM\Column(name="from_value", type="string", length=255, nullable=true)
     */
    private $fromValue;

    /**
     * @var string $toValue
     *
     * @ORM\Column(name="to_value", type="string", length=255, nullable=true)
     */
    private $toValue;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer $user
     *
     * @ORM\Column(name="user", type="integer", nullable=true)
     */
    private $user;

    /**
     * @var integer $idGenoPellet
     *
     * @ORM\Column(name="id_geno_pellet", type="integer", nullable=true)
     */
    private $idGenoPellet;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGenoPelletEntry
     *
     * @param integer $idGenoPelletEntry
     * @return GenoPelletEntryHistory
     */
    public function setIdGenoPelletEntry($idGenoPelletEntry)
    {
        $this->idGenoPelletEntry = $idGenoPelletEntry;
    
        return $this;
    }

    /**
     * Get idGenoPelletEntry
     *
     * @return integer 
     */
    public function getIdGenoPelletEntry()
    {
        return $this->idGenoPelletEntry;
    }

    /**
     * Set whatChange
     *
     * @param string $whatChange
     * @return GenoPelletEntryHistory
     */
    public function setWhatChange($whatChange)
    {
        $this->whatChange = $whatChange;
    
        return $this;
    }

    /**
     * Get whatChange
     *
     * @return string 
     */
    public function getWhatChange()
    {
        return $this->whatChange;
    }

    /**
     * Set fromValue
     *
     * @param string $fromValue
     * @return GenoPelletEntryHistory
     */
    public function setFromValue($fromValue)
    {
        $this->fromValue = $fromValue;
    
        return $this;
    }

    /**
     * Get fromValue
     *
     * @return string 
     */
    public function getFromValue()
    {
        return $this->fromValue;
    }

    /**
     * Set toValue
     *
     * @param string $toValue
     * @return GenoPelletEntryHistory
     */
    public function setToValue($toValue)
    {
        $this->toValue = $toValue;
    
        return $this;
    }

    /**
     * Get toValue
     *
     * @return string 
     */
    public function getToValue()
    {
        return $this->toValue;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return GenoPelletEntryHistory
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return GenoPelletEntryHistory
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set idGenoPellet
     *
     * @param integer $idGenoPellet
     * @return GenoPelletEntryHistory
     */
    public function setIdGenoPellet($idGenoPellet)
    {
        $this->idGenoPellet = $idGenoPellet;
    
        return $this;
    }

    /**
     * Get idGenoPellet
     *
     * @return integer 
     */
    public function getIdGenoPellet()
    {
        return $this->idGenoPellet;
    }
}