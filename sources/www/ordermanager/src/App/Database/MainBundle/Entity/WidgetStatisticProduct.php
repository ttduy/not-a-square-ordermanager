<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WidgetStatisticProduct
 *
 * @ORM\Table(name="widget_statistic_product")
 * @ORM\Entity
 */
class WidgetStatisticProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWidgetStatistic
     *
     * @ORM\Column(name="id_widget_statistic", type="integer", nullable=true)
     */
    private $idWidgetStatistic;

    /**
     * @var integer $idProduct
     *
     * @ORM\Column(name="id_product", type="integer", nullable=true)
     */
    private $idProduct;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWidgetStatistic
     *
     * @param integer $idWidgetStatistic
     * @return WidgetStatisticProduct
     */
    public function setIdWidgetStatistic($idWidgetStatistic)
    {
        $this->idWidgetStatistic = $idWidgetStatistic;
    
        return $this;
    }

    /**
     * Get idWidgetStatistic
     *
     * @return integer 
     */
    public function getIdWidgetStatistic()
    {
        return $this->idWidgetStatistic;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     * @return WidgetStatisticProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    
        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }
}