<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TblHelp
 *
 * @ORM\Table(name="tbl_help")
 * @ORM\Entity
 */
class TblHelp
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $helptext
     *
     * @ORM\Column(name="HelpText", type="text", nullable=true)
     */
    private $helptext;

    /**
     * @var string $helptitle
     *
     * @ORM\Column(name="HelpTitle", type="string", length=255, nullable=true)
     */
    private $helptitle;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set helptext
     *
     * @param string $helptext
     * @return TblHelp
     */
    public function setHelptext($helptext)
    {
        $this->helptext = $helptext;
    
        return $this;
    }

    /**
     * Get helptext
     *
     * @return string 
     */
    public function getHelptext()
    {
        return $this->helptext;
    }

    /**
     * Set helptitle
     *
     * @param string $helptitle
     * @return TblHelp
     */
    public function setHelptitle($helptitle)
    {
        $this->helptitle = $helptitle;
    
        return $this;
    }

    /**
     * Get helptitle
     *
     * @return string 
     */
    public function getHelptitle()
    {
        return $this->helptitle;
    }
}