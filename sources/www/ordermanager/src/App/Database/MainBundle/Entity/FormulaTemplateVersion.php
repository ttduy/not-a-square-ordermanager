<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormulaTemplateVersion
 *
 * @ORM\Table(name="formula_template_version")
 * @ORM\Entity
 */
class FormulaTemplateVersion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFormulaTemplate
     *
     * @ORM\Column(name="id_formula_template", type="integer", nullable=true)
     */
    private $idFormulaTemplate;

    /**
     * @var integer $number
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $formula
     *
     * @ORM\Column(name="formula", type="text", nullable=true)
     */
    private $formula;

    /**
     * @var string $changesNote
     *
     * @ORM\Column(name="changes_note", type="text", nullable=true)
     */
    private $changesNote;

    /**
     * @var string $versionAlias
     *
     * @ORM\Column(name="version_alias", type="string", length=255, nullable=true)
     */
    private $versionAlias;

    /**
     * @var string $testInput
     *
     * @ORM\Column(name="test_input", type="text", nullable=true)
     */
    private $testInput;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormulaTemplate
     *
     * @param integer $idFormulaTemplate
     * @return FormulaTemplateVersion
     */
    public function setIdFormulaTemplate($idFormulaTemplate)
    {
        $this->idFormulaTemplate = $idFormulaTemplate;
    
        return $this;
    }

    /**
     * Get idFormulaTemplate
     *
     * @return integer 
     */
    public function getIdFormulaTemplate()
    {
        return $this->idFormulaTemplate;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return FormulaTemplateVersion
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return FormulaTemplateVersion
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set formula
     *
     * @param string $formula
     * @return FormulaTemplateVersion
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
    
        return $this;
    }

    /**
     * Get formula
     *
     * @return string 
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set changesNote
     *
     * @param string $changesNote
     * @return FormulaTemplateVersion
     */
    public function setChangesNote($changesNote)
    {
        $this->changesNote = $changesNote;
    
        return $this;
    }

    /**
     * Get changesNote
     *
     * @return string 
     */
    public function getChangesNote()
    {
        return $this->changesNote;
    }

    /**
     * Set versionAlias
     *
     * @param string $versionAlias
     * @return FormulaTemplateVersion
     */
    public function setVersionAlias($versionAlias)
    {
        $this->versionAlias = $versionAlias;
    
        return $this;
    }

    /**
     * Get versionAlias
     *
     * @return string 
     */
    public function getVersionAlias()
    {
        return $this->versionAlias;
    }

    /**
     * Set testInput
     *
     * @param string $testInput
     * @return FormulaTemplateVersion
     */
    public function setTestInput($testInput)
    {
        $this->testInput = $testInput;
    
        return $this;
    }

    /**
     * Get testInput
     *
     * @return string 
     */
    public function getTestInput()
    {
        return $this->testInput;
    }
}