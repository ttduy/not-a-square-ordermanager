<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerDashboardStatus
 *
 * @ORM\Table(name="customer_dashboard_status")
 * @ORM\Entity
 */
class CustomerDashboardStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idStatus
     *
     * @ORM\Column(name="id_status", type="integer", nullable=true)
     */
    private $idStatus;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idStatus
     *
     * @param integer $idStatus
     * @return CustomerDashboardStatus
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;
    
        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer 
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return CustomerDashboardStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}