<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaInvoice
 *
 * @ORM\Table(name="mwa_invoice")
 * @ORM\Entity
 */
class MwaInvoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $invoiceNumber
     *
     * @ORM\Column(name="invoice_number", type="string", length=255, nullable=true)
     */
    private $invoiceNumber;

    /**
     * @var \DateTime $invoiceDate
     *
     * @ORM\Column(name="invoice_date", type="date", nullable=true)
     */
    private $invoiceDate;

    /**
     * @var boolean $isWithTax
     *
     * @ORM\Column(name="is_with_tax", type="boolean", nullable=true)
     */
    private $isWithTax;

    /**
     * @var integer $idCompany
     *
     * @ORM\Column(name="id_company", type="integer", nullable=true)
     */
    private $idCompany;

    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    private $taxValue;

    /**
     * @var float $postage
     *
     * @ORM\Column(name="postage", type="decimal", nullable=true)
     */
    private $postage;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var string $uid
     *
     * @ORM\Column(name="uid", type="string", length=255, nullable=true)
     */
    private $uid;

    /**
     * @var string $purchaseOrder
     *
     * @ORM\Column(name="purchase_order", type="string", length=255, nullable=true)
     */
    private $purchaseOrder;

    /**
     * @var string $supplierNumber
     *
     * @ORM\Column(name="supplier_number", type="string", length=255, nullable=true)
     */
    private $supplierNumber;

    /**
     * @var string $product
     *
     * @ORM\Column(name="product", type="string", length=255, nullable=true)
     */
    private $product;

    /**
     * @var float $productPrice
     *
     * @ORM\Column(name="product_price", type="decimal", nullable=true)
     */
    private $productPrice;

    /**
     * @var integer $idGroup
     *
     * @ORM\Column(name="id_group", type="integer", nullable=true)
     */
    private $idGroup;

    /**
     * @var boolean $isShowSummarySentence
     *
     * @ORM\Column(name="is_show_summary_sentence", type="boolean", nullable=true)
     */
    private $isShowSummarySentence;

    /**
     * @var float $invoiceAmount
     *
     * @ORM\Column(name="invoice_amount", type="decimal", nullable=true)
     */
    private $invoiceAmount;

    /**
     * @var string $invoiceFile
     *
     * @ORM\Column(name="invoice_file", type="string", length=255, nullable=true)
     */
    private $invoiceFile;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var float $exchangeRate
     *
     * @ORM\Column(name="exchange_rate", type="decimal", nullable=true)
     */
    private $exchangeRate;

    /**
     * @var string $textAfterDescription
     *
     * @ORM\Column(name="text_after_description", type="text", nullable=true)
     */
    private $textAfterDescription;

    /**
     * @var string $textFooter
     *
     * @ORM\Column(name="text_footer", type="text", nullable=true)
     */
    private $textFooter;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MwaInvoice
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     * @return MwaInvoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string 
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     * @return MwaInvoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;
    
        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime 
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Set isWithTax
     *
     * @param boolean $isWithTax
     * @return MwaInvoice
     */
    public function setIsWithTax($isWithTax)
    {
        $this->isWithTax = $isWithTax;
    
        return $this;
    }

    /**
     * Get isWithTax
     *
     * @return boolean 
     */
    public function getIsWithTax()
    {
        return $this->isWithTax;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return MwaInvoice
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;
    
        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set taxValue
     *
     * @param float $taxValue
     * @return MwaInvoice
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    
        return $this;
    }

    /**
     * Get taxValue
     *
     * @return float 
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }

    /**
     * Set postage
     *
     * @param float $postage
     * @return MwaInvoice
     */
    public function setPostage($postage)
    {
        $this->postage = $postage;
    
        return $this;
    }

    /**
     * Get postage
     *
     * @return float 
     */
    public function getPostage()
    {
        return $this->postage;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MwaInvoice
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return MwaInvoice
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return MwaInvoice
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    
        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set purchaseOrder
     *
     * @param string $purchaseOrder
     * @return MwaInvoice
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    
        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return string 
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * Set supplierNumber
     *
     * @param string $supplierNumber
     * @return MwaInvoice
     */
    public function setSupplierNumber($supplierNumber)
    {
        $this->supplierNumber = $supplierNumber;
    
        return $this;
    }

    /**
     * Get supplierNumber
     *
     * @return string 
     */
    public function getSupplierNumber()
    {
        return $this->supplierNumber;
    }

    /**
     * Set product
     *
     * @param string $product
     * @return MwaInvoice
     */
    public function setProduct($product)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set productPrice
     *
     * @param float $productPrice
     * @return MwaInvoice
     */
    public function setProductPrice($productPrice)
    {
        $this->productPrice = $productPrice;
    
        return $this;
    }

    /**
     * Get productPrice
     *
     * @return float 
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     * @return MwaInvoice
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;
    
        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer 
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set isShowSummarySentence
     *
     * @param boolean $isShowSummarySentence
     * @return MwaInvoice
     */
    public function setIsShowSummarySentence($isShowSummarySentence)
    {
        $this->isShowSummarySentence = $isShowSummarySentence;
    
        return $this;
    }

    /**
     * Get isShowSummarySentence
     *
     * @return boolean 
     */
    public function getIsShowSummarySentence()
    {
        return $this->isShowSummarySentence;
    }

    /**
     * Set invoiceAmount
     *
     * @param float $invoiceAmount
     * @return MwaInvoice
     */
    public function setInvoiceAmount($invoiceAmount)
    {
        $this->invoiceAmount = $invoiceAmount;
    
        return $this;
    }

    /**
     * Get invoiceAmount
     *
     * @return float 
     */
    public function getInvoiceAmount()
    {
        return $this->invoiceAmount;
    }

    /**
     * Set invoiceFile
     *
     * @param string $invoiceFile
     * @return MwaInvoice
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;
    
        return $this;
    }

    /**
     * Get invoiceFile
     *
     * @return string 
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return MwaInvoice
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     * @return MwaInvoice
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    
        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set textAfterDescription
     *
     * @param string $textAfterDescription
     * @return MwaInvoice
     */
    public function setTextAfterDescription($textAfterDescription)
    {
        $this->textAfterDescription = $textAfterDescription;
    
        return $this;
    }

    /**
     * Get textAfterDescription
     *
     * @return string 
     */
    public function getTextAfterDescription()
    {
        return $this->textAfterDescription;
    }

    /**
     * Set textFooter
     *
     * @param string $textFooter
     * @return MwaInvoice
     */
    public function setTextFooter($textFooter)
    {
        $this->textFooter = $textFooter;
    
        return $this;
    }

    /**
     * Get textFooter
     *
     * @return string 
     */
    public function getTextFooter()
    {
        return $this->textFooter;
    }
}