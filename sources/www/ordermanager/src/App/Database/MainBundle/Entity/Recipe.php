<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Recipe
 *
 * @ORM\Table(name="recipe")
 * @ORM\Entity
 */
class Recipe
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $recipeId
     *
     * @ORM\Column(name="recipe_id", type="string", length=255, nullable=true)
     */
    private $recipeId;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $ingredient
     *
     * @ORM\Column(name="ingredient", type="text", nullable=true)
     */
    private $ingredient;

    /**
     * @var string $instruction
     *
     * @ORM\Column(name="instruction", type="text", nullable=true)
     */
    private $instruction;

    /**
     * @var string $imageFile
     *
     * @ORM\Column(name="image_file", type="string", length=255, nullable=true)
     */
    private $imageFile;

    /**
     * @var float $preparationTime
     *
     * @ORM\Column(name="preparation_time", type="decimal", nullable=true)
     */
    private $preparationTime;

    /**
     * @var float $carb
     *
     * @ORM\Column(name="carb", type="decimal", nullable=true)
     */
    private $carb;

    /**
     * @var float $prot
     *
     * @ORM\Column(name="prot", type="decimal", nullable=true)
     */
    private $prot;

    /**
     * @var float $fat
     *
     * @ORM\Column(name="fat", type="decimal", nullable=true)
     */
    private $fat;

    /**
     * @var boolean $isVegan
     *
     * @ORM\Column(name="is_vegan", type="boolean", nullable=true)
     */
    private $isVegan;

    /**
     * @var string $foodIngredients
     *
     * @ORM\Column(name="food_ingredients", type="text", nullable=true)
     */
    private $foodIngredients;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipeId
     *
     * @param string $recipeId
     * @return Recipe
     */
    public function setRecipeId($recipeId)
    {
        $this->recipeId = $recipeId;
    
        return $this;
    }

    /**
     * Get recipeId
     *
     * @return string 
     */
    public function getRecipeId()
    {
        return $this->recipeId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Recipe
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ingredient
     *
     * @param string $ingredient
     * @return Recipe
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
    
        return $this;
    }

    /**
     * Get ingredient
     *
     * @return string 
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * Set instruction
     *
     * @param string $instruction
     * @return Recipe
     */
    public function setInstruction($instruction)
    {
        $this->instruction = $instruction;
    
        return $this;
    }

    /**
     * Get instruction
     *
     * @return string 
     */
    public function getInstruction()
    {
        return $this->instruction;
    }

    /**
     * Set imageFile
     *
     * @param string $imageFile
     * @return Recipe
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
    
        return $this;
    }

    /**
     * Get imageFile
     *
     * @return string 
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set preparationTime
     *
     * @param float $preparationTime
     * @return Recipe
     */
    public function setPreparationTime($preparationTime)
    {
        $this->preparationTime = $preparationTime;
    
        return $this;
    }

    /**
     * Get preparationTime
     *
     * @return float 
     */
    public function getPreparationTime()
    {
        return $this->preparationTime;
    }

    /**
     * Set carb
     *
     * @param float $carb
     * @return Recipe
     */
    public function setCarb($carb)
    {
        $this->carb = $carb;
    
        return $this;
    }

    /**
     * Get carb
     *
     * @return float 
     */
    public function getCarb()
    {
        return $this->carb;
    }

    /**
     * Set prot
     *
     * @param float $prot
     * @return Recipe
     */
    public function setProt($prot)
    {
        $this->prot = $prot;
    
        return $this;
    }

    /**
     * Get prot
     *
     * @return float 
     */
    public function getProt()
    {
        return $this->prot;
    }

    /**
     * Set fat
     *
     * @param float $fat
     * @return Recipe
     */
    public function setFat($fat)
    {
        $this->fat = $fat;
    
        return $this;
    }

    /**
     * Get fat
     *
     * @return float 
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * Set isVegan
     *
     * @param boolean $isVegan
     * @return Recipe
     */
    public function setIsVegan($isVegan)
    {
        $this->isVegan = $isVegan;
    
        return $this;
    }

    /**
     * Get isVegan
     *
     * @return boolean 
     */
    public function getIsVegan()
    {
        return $this->isVegan;
    }

    /**
     * Set foodIngredients
     *
     * @param string $foodIngredients
     * @return Recipe
     */
    public function setFoodIngredients($foodIngredients)
    {
        $this->foodIngredients = $foodIngredients;
    
        return $this;
    }

    /**
     * Get foodIngredients
     *
     * @return string 
     */
    public function getFoodIngredients()
    {
        return $this->foodIngredients;
    }
}