<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Results
 *
 * @ORM\Table(name="results")
 * @ORM\Entity
 */
class Results
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $groupid
     *
     * @ORM\Column(name="GroupId", type="integer", nullable=true)
     */
    private $groupid;

    /**
     * @var string $result
     *
     * @ORM\Column(name="Result", type="string", length=50, nullable=true)
     */
    private $result;

    /**
     * @var string $callresult
     *
     * @ORM\Column(name="CallResult", type="string", length=255, nullable=true)
     */
    private $callresult;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupid
     *
     * @param integer $groupid
     * @return Results
     */
    public function setGroupid($groupid)
    {
        $this->groupid = $groupid;
    
        return $this;
    }

    /**
     * Get groupid
     *
     * @return integer 
     */
    public function getGroupid()
    {
        return $this->groupid;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return Results
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set callresult
     *
     * @param string $callresult
     * @return Results
     */
    public function setCallresult($callresult)
    {
        $this->callresult = $callresult;
    
        return $this;
    }

    /**
     * Get callresult
     *
     * @return string 
     */
    public function getCallresult()
    {
        return $this->callresult;
    }
}