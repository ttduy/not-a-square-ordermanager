<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\NutriMeStatus
 *
 * @ORM\Table(name="nutri_me_status")
 * @ORM\Entity
 */
class NutriMeStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idNutriMe
     *
     * @ORM\Column(name="id_nutri_me", type="integer", nullable=true)
     */
    private $idNutriMe;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNutriMe
     *
     * @param integer $idNutriMe
     * @return NutriMeStatus
     */
    public function setIdNutriMe($idNutriMe)
    {
        $this->idNutriMe = $idNutriMe;
    
        return $this;
    }

    /**
     * Get idNutriMe
     *
     * @return integer 
     */
    public function getIdNutriMe()
    {
        return $this->idNutriMe;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return NutriMeStatus
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return NutriMeStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return NutriMeStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}