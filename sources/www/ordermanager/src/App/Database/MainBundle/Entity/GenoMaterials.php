<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoMaterials
 *
 * @ORM\Table(name="geno_materials")
 * @ORM\Entity
 */
class GenoMaterials
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $shortName
     *
     * @ORM\Column(name="short_name", type="string", length=45, nullable=true)
     */
    private $shortName;

    /**
     * @var string $fullName
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var float $targetLoad
     *
     * @ORM\Column(name="target_load", type="float", nullable=true)
     */
    private $targetLoad;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;

    /**
     * @var string $storageConditions
     *
     * @ORM\Column(name="storage_conditions", type="text", nullable=true)
     */
    private $storageConditions;

    /**
     * @var integer $type
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer $unitsInStock
     *
     * @ORM\Column(name="units_in_stock", type="integer", nullable=true)
     */
    private $unitsInStock;

    /**
     * @var string $unitDescription
     *
     * @ORM\Column(name="unit_description", type="string", length=255, nullable=true)
     */
    private $unitDescription;

    /**
     * @var boolean $isCriticalSubstance
     *
     * @ORM\Column(name="is_critical_substance", type="boolean", nullable=true)
     */
    private $isCriticalSubstance;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime $modifiedDate
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=true)
     */
    private $modifiedDate;

    /**
     * @var integer $modifiedBy
     *
     * @ORM\Column(name="modified_by", type="integer", nullable=true)
     */
    private $modifiedBy;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var string $fileAttachment
     *
     * @ORM\Column(name="file_attachment", type="text", nullable=true)
     */
    private $fileAttachment;

    /**
     * @var boolean $enableCollectStorageSampleWarning
     *
     * @ORM\Column(name="enable_collect_storage_sample_warning", type="boolean", nullable=true)
     */
    private $enableCollectStorageSampleWarning;

    /**
     * @var string $substanceFullName
     *
     * @ORM\Column(name="substance_full_name", type="string", length=255, nullable=true)
     */
    private $substanceFullName;

    /**
     * @var string $substanceTextBlock
     *
     * @ORM\Column(name="substance_text_block", type="string", length=255, nullable=true)
     */
    private $substanceTextBlock;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return GenoMaterials
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    
        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return GenoMaterials
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set targetLoad
     *
     * @param float $targetLoad
     * @return GenoMaterials
     */
    public function setTargetLoad($targetLoad)
    {
        $this->targetLoad = $targetLoad;
    
        return $this;
    }

    /**
     * Get targetLoad
     *
     * @return float 
     */
    public function getTargetLoad()
    {
        return $this->targetLoad;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return GenoMaterials
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }

    /**
     * Set storageConditions
     *
     * @param string $storageConditions
     * @return GenoMaterials
     */
    public function setStorageConditions($storageConditions)
    {
        $this->storageConditions = $storageConditions;
    
        return $this;
    }

    /**
     * Get storageConditions
     *
     * @return string 
     */
    public function getStorageConditions()
    {
        return $this->storageConditions;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return GenoMaterials
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set unitsInStock
     *
     * @param integer $unitsInStock
     * @return GenoMaterials
     */
    public function setUnitsInStock($unitsInStock)
    {
        $this->unitsInStock = $unitsInStock;
    
        return $this;
    }

    /**
     * Get unitsInStock
     *
     * @return integer 
     */
    public function getUnitsInStock()
    {
        return $this->unitsInStock;
    }

    /**
     * Set unitDescription
     *
     * @param string $unitDescription
     * @return GenoMaterials
     */
    public function setUnitDescription($unitDescription)
    {
        $this->unitDescription = $unitDescription;
    
        return $this;
    }

    /**
     * Get unitDescription
     *
     * @return string 
     */
    public function getUnitDescription()
    {
        return $this->unitDescription;
    }

    /**
     * Set isCriticalSubstance
     *
     * @param boolean $isCriticalSubstance
     * @return GenoMaterials
     */
    public function setIsCriticalSubstance($isCriticalSubstance)
    {
        $this->isCriticalSubstance = $isCriticalSubstance;
    
        return $this;
    }

    /**
     * Get isCriticalSubstance
     *
     * @return boolean 
     */
    public function getIsCriticalSubstance()
    {
        return $this->isCriticalSubstance;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenoMaterials
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return GenoMaterials
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return GenoMaterials
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return GenoMaterials
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set fileAttachment
     *
     * @param string $fileAttachment
     * @return GenoMaterials
     */
    public function setFileAttachment($fileAttachment)
    {
        $this->fileAttachment = $fileAttachment;
    
        return $this;
    }

    /**
     * Get fileAttachment
     *
     * @return string 
     */
    public function getFileAttachment()
    {
        return $this->fileAttachment;
    }

    /**
     * Set enableCollectStorageSampleWarning
     *
     * @param boolean $enableCollectStorageSampleWarning
     * @return GenoMaterials
     */
    public function setEnableCollectStorageSampleWarning($enableCollectStorageSampleWarning)
    {
        $this->enableCollectStorageSampleWarning = $enableCollectStorageSampleWarning;
    
        return $this;
    }

    /**
     * Get enableCollectStorageSampleWarning
     *
     * @return boolean 
     */
    public function getEnableCollectStorageSampleWarning()
    {
        return $this->enableCollectStorageSampleWarning;
    }

    /**
     * Set substanceFullName
     *
     * @param string $substanceFullName
     * @return GenoMaterials
     */
    public function setSubstanceFullName($substanceFullName)
    {
        $this->substanceFullName = $substanceFullName;
    
        return $this;
    }

    /**
     * Get substanceFullName
     *
     * @return string 
     */
    public function getSubstanceFullName()
    {
        return $this->substanceFullName;
    }

    /**
     * Set substanceTextBlock
     *
     * @param string $substanceTextBlock
     * @return GenoMaterials
     */
    public function setSubstanceTextBlock($substanceTextBlock)
    {
        $this->substanceTextBlock = $substanceTextBlock;
    
        return $this;
    }

    /**
     * Get substanceTextBlock
     *
     * @return string 
     */
    public function getSubstanceTextBlock()
    {
        return $this->substanceTextBlock;
    }
}