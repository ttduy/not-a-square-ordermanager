<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Worker
 *
 * @ORM\Table(name="worker")
 * @ORM\Entity
 */
class Worker
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $apiKey
     *
     * @ORM\Column(name="api_key", type="text", nullable=true)
     */
    private $apiKey;

    /**
     * @var \DateTime $lastCheckin
     *
     * @ORM\Column(name="last_checkin", type="datetime", nullable=true)
     */
    private $lastCheckin;

    /**
     * @var \DateTime $baseUrl
     *
     * @ORM\Column(name="base_url", type="datetime", nullable=true)
     */
    private $baseUrl;

    /**
     * @var integer $totalProcess
     *
     * @ORM\Column(name="total_process", type="integer", nullable=true)
     */
    private $totalProcess;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Worker
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Worker
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Worker
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return Worker
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    
        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set lastCheckin
     *
     * @param \DateTime $lastCheckin
     * @return Worker
     */
    public function setLastCheckin($lastCheckin)
    {
        $this->lastCheckin = $lastCheckin;
    
        return $this;
    }

    /**
     * Get lastCheckin
     *
     * @return \DateTime 
     */
    public function getLastCheckin()
    {
        return $this->lastCheckin;
    }

    /**
     * Set baseUrl
     *
     * @param \DateTime $baseUrl
     * @return Worker
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    
        return $this;
    }

    /**
     * Get baseUrl
     *
     * @return \DateTime 
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Set totalProcess
     *
     * @param integer $totalProcess
     * @return Worker
     */
    public function setTotalProcess($totalProcess)
    {
        $this->totalProcess = $totalProcess;
    
        return $this;
    }

    /**
     * Get totalProcess
     *
     * @return integer 
     */
    public function getTotalProcess()
    {
        return $this->totalProcess;
    }
}