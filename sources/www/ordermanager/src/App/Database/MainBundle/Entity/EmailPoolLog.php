<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolLog
 *
 * @ORM\Table(name="email_pool_log")
 * @ORM\Entity
 */
class EmailPoolLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailPool
     *
     * @ORM\Column(name="id_email_pool", type="integer", nullable=true)
     */
    private $idEmailPool;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $receiver
     *
     * @ORM\Column(name="receiver", type="string", length=500, nullable=true)
     */
    private $receiver;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailPool
     *
     * @param integer $idEmailPool
     * @return EmailPoolLog
     */
    public function setIdEmailPool($idEmailPool)
    {
        $this->idEmailPool = $idEmailPool;
    
        return $this;
    }

    /**
     * Get idEmailPool
     *
     * @return integer 
     */
    public function getIdEmailPool()
    {
        return $this->idEmailPool;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return EmailPoolLog
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set receiver
     *
     * @param string $receiver
     * @return EmailPoolLog
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * Get receiver
     *
     * @return string 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return EmailPoolLog
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailPoolLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}