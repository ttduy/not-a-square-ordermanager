<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CostTrackerSubmitDataHistoryError
 *
 * @ORM\Table(name="cost_tracker_submit_data_history_error")
 * @ORM\Entity
 */
class CostTrackerSubmitDataHistoryError
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCostTrackerSubmitDataHistory
     *
     * @ORM\Column(name="id_cost_tracker_submit_data_history", type="integer", nullable=true)
     */
    private $idCostTrackerSubmitDataHistory;

    /**
     * @var integer $idErrorType
     *
     * @ORM\Column(name="id_error_type", type="integer", nullable=true)
     */
    private $idErrorType;

    /**
     * @var string $errorData
     *
     * @ORM\Column(name="error_data", type="text", nullable=true)
     */
    private $errorData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCostTrackerSubmitDataHistory
     *
     * @param integer $idCostTrackerSubmitDataHistory
     * @return CostTrackerSubmitDataHistoryError
     */
    public function setIdCostTrackerSubmitDataHistory($idCostTrackerSubmitDataHistory)
    {
        $this->idCostTrackerSubmitDataHistory = $idCostTrackerSubmitDataHistory;
    
        return $this;
    }

    /**
     * Get idCostTrackerSubmitDataHistory
     *
     * @return integer 
     */
    public function getIdCostTrackerSubmitDataHistory()
    {
        return $this->idCostTrackerSubmitDataHistory;
    }

    /**
     * Set idErrorType
     *
     * @param integer $idErrorType
     * @return CostTrackerSubmitDataHistoryError
     */
    public function setIdErrorType($idErrorType)
    {
        $this->idErrorType = $idErrorType;
    
        return $this;
    }

    /**
     * Get idErrorType
     *
     * @return integer 
     */
    public function getIdErrorType()
    {
        return $this->idErrorType;
    }

    /**
     * Set errorData
     *
     * @param string $errorData
     * @return CostTrackerSubmitDataHistoryError
     */
    public function setErrorData($errorData)
    {
        $this->errorData = $errorData;
    
        return $this;
    }

    /**
     * Get errorData
     *
     * @return string 
     */
    public function getErrorData()
    {
        return $this->errorData;
    }
}