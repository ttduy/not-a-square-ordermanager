<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductCategoriesDnaPlus
 *
 * @ORM\Table(name="product_categories_dna_plus")
 * @ORM\Entity
 */
class ProductCategoriesDnaPlus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $categoryname
     *
     * @ORM\Column(name="CategoryName", type="string", length=255, nullable=true)
     */
    private $categoryname;

    /**
     * @var float $endcustomerprice
     *
     * @ORM\Column(name="EndCustomerPrice", type="float", nullable=true)
     */
    private $endcustomerprice;

    /**
     * @var float $materialcostsexclreports
     *
     * @ORM\Column(name="MaterialCostsExclReports", type="float", nullable=true)
     */
    private $materialcostsexclreports;

    /**
     * @var float $productioncosts
     *
     * @ORM\Column(name="ProductionCosts", type="float", nullable=true)
     */
    private $productioncosts;

    /**
     * @var float $doctorsmargin
     *
     * @ORM\Column(name="DoctorsMargin", type="float", nullable=true)
     */
    private $doctorsmargin;

    /**
     * @var float $distributionchannelmargin
     *
     * @ORM\Column(name="DistributionChannelMargin", type="float", nullable=true)
     */
    private $distributionchannelmargin;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var string $codename
     *
     * @ORM\Column(name="codename", type="string", length=255, nullable=true)
     */
    private $codename;

    /**
     * @var integer $productTypeAustria
     *
     * @ORM\Column(name="product_type_austria", type="integer", nullable=true)
     */
    private $productTypeAustria;

    /**
     * @var integer $productTypeGermany
     *
     * @ORM\Column(name="product_type_germany", type="integer", nullable=true)
     */
    private $productTypeGermany;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryname
     *
     * @param string $categoryname
     * @return ProductCategoriesDnaPlus
     */
    public function setCategoryname($categoryname)
    {
        $this->categoryname = $categoryname;
    
        return $this;
    }

    /**
     * Get categoryname
     *
     * @return string 
     */
    public function getCategoryname()
    {
        return $this->categoryname;
    }

    /**
     * Set endcustomerprice
     *
     * @param float $endcustomerprice
     * @return ProductCategoriesDnaPlus
     */
    public function setEndcustomerprice($endcustomerprice)
    {
        $this->endcustomerprice = $endcustomerprice;
    
        return $this;
    }

    /**
     * Get endcustomerprice
     *
     * @return float 
     */
    public function getEndcustomerprice()
    {
        return $this->endcustomerprice;
    }

    /**
     * Set materialcostsexclreports
     *
     * @param float $materialcostsexclreports
     * @return ProductCategoriesDnaPlus
     */
    public function setMaterialcostsexclreports($materialcostsexclreports)
    {
        $this->materialcostsexclreports = $materialcostsexclreports;
    
        return $this;
    }

    /**
     * Get materialcostsexclreports
     *
     * @return float 
     */
    public function getMaterialcostsexclreports()
    {
        return $this->materialcostsexclreports;
    }

    /**
     * Set productioncosts
     *
     * @param float $productioncosts
     * @return ProductCategoriesDnaPlus
     */
    public function setProductioncosts($productioncosts)
    {
        $this->productioncosts = $productioncosts;
    
        return $this;
    }

    /**
     * Get productioncosts
     *
     * @return float 
     */
    public function getProductioncosts()
    {
        return $this->productioncosts;
    }

    /**
     * Set doctorsmargin
     *
     * @param float $doctorsmargin
     * @return ProductCategoriesDnaPlus
     */
    public function setDoctorsmargin($doctorsmargin)
    {
        $this->doctorsmargin = $doctorsmargin;
    
        return $this;
    }

    /**
     * Get doctorsmargin
     *
     * @return float 
     */
    public function getDoctorsmargin()
    {
        return $this->doctorsmargin;
    }

    /**
     * Set distributionchannelmargin
     *
     * @param float $distributionchannelmargin
     * @return ProductCategoriesDnaPlus
     */
    public function setDistributionchannelmargin($distributionchannelmargin)
    {
        $this->distributionchannelmargin = $distributionchannelmargin;
    
        return $this;
    }

    /**
     * Get distributionchannelmargin
     *
     * @return float 
     */
    public function getDistributionchannelmargin()
    {
        return $this->distributionchannelmargin;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return ProductCategoriesDnaPlus
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set codename
     *
     * @param string $codename
     * @return ProductCategoriesDnaPlus
     */
    public function setCodename($codename)
    {
        $this->codename = $codename;
    
        return $this;
    }

    /**
     * Get codename
     *
     * @return string 
     */
    public function getCodename()
    {
        return $this->codename;
    }

    /**
     * Set productTypeAustria
     *
     * @param integer $productTypeAustria
     * @return ProductCategoriesDnaPlus
     */
    public function setProductTypeAustria($productTypeAustria)
    {
        $this->productTypeAustria = $productTypeAustria;
    
        return $this;
    }

    /**
     * Get productTypeAustria
     *
     * @return integer 
     */
    public function getProductTypeAustria()
    {
        return $this->productTypeAustria;
    }

    /**
     * Set productTypeGermany
     *
     * @param integer $productTypeGermany
     * @return ProductCategoriesDnaPlus
     */
    public function setProductTypeGermany($productTypeGermany)
    {
        $this->productTypeGermany = $productTypeGermany;
    
        return $this;
    }

    /**
     * Get productTypeGermany
     *
     * @return integer 
     */
    public function getProductTypeGermany()
    {
        return $this->productTypeGermany;
    }
}