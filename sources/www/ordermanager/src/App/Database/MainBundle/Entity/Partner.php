<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Partner
 *
 * @ORM\Table(name="partner")
 * @ORM\Entity
 */
class Partner
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="Domain", type="text", nullable=true)
     */
    private $domain;

    /**
     * @var string $partner
     *
     * @ORM\Column(name="Partner", type="string", length=255, nullable=true)
     */
    private $partner;

    /**
     * @var string $type
     *
     * @ORM\Column(name="Type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var boolean $male
     *
     * @ORM\Column(name="Male", type="boolean", nullable=true)
     */
    private $male;

    /**
     * @var boolean $female
     *
     * @ORM\Column(name="Female", type="boolean", nullable=true)
     */
    private $female;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="Surname", type="string", length=50, nullable=true)
     */
    private $surname;

    /**
     * @var string $institution
     *
     * @ORM\Column(name="Institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $countryid
     *
     * @ORM\Column(name="CountryId", type="integer", nullable=true)
     */
    private $countryid;

    /**
     * @var string $contactemail
     *
     * @ORM\Column(name="ContactEmail", type="string", length=255, nullable=true)
     */
    private $contactemail;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $statusdeliveryemail
     *
     * @ORM\Column(name="StatusDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $statusdeliveryemail;

    /**
     * @var string $invoicedeliveryemail
     *
     * @ORM\Column(name="InvoiceDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $invoicedeliveryemail;

    /**
     * @var string $accountname
     *
     * @ORM\Column(name="AccountName", type="string", length=36, nullable=true)
     */
    private $accountname;

    /**
     * @var string $accountnumber
     *
     * @ORM\Column(name="AccountNumber", type="string", length=50, nullable=true)
     */
    private $accountnumber;

    /**
     * @var string $bankcode
     *
     * @ORM\Column(name="BankCode", type="string", length=50, nullable=true)
     */
    private $bankcode;

    /**
     * @var string $bic
     *
     * @ORM\Column(name="BIC", type="string", length=50, nullable=true)
     */
    private $bic;

    /**
     * @var string $iban
     *
     * @ORM\Column(name="IBAN", type="string", length=36, nullable=true)
     */
    private $iban;

    /**
     * @var string $othernotes
     *
     * @ORM\Column(name="OtherNotes", type="text", nullable=true)
     */
    private $othernotes;

    /**
     * @var integer $acquisiteurid1
     *
     * @ORM\Column(name="AcquisiteurId1", type="integer", nullable=true)
     */
    private $acquisiteurid1;

    /**
     * @var float $commission1
     *
     * @ORM\Column(name="Commission1", type="float", nullable=true)
     */
    private $commission1;

    /**
     * @var integer $acquisiteurid2
     *
     * @ORM\Column(name="AcquisiteurId2", type="integer", nullable=true)
     */
    private $acquisiteurid2;

    /**
     * @var float $commission2
     *
     * @ORM\Column(name="Commission2", type="float", nullable=true)
     */
    private $commission2;

    /**
     * @var integer $acquisiteurid3
     *
     * @ORM\Column(name="AcquisiteurId3", type="integer", nullable=true)
     */
    private $acquisiteurid3;

    /**
     * @var float $commission3
     *
     * @ORM\Column(name="Commission3", type="float", nullable=true)
     */
    private $commission3;

    /**
     * @var string $webusers
     *
     * @ORM\Column(name="WebUsers", type="string", length=30, nullable=true)
     */
    private $webusers;

    /**
     * @var string $uidnumber
     *
     * @ORM\Column(name="UidNumber", type="string", length=255, nullable=true)
     */
    private $uidnumber;

    /**
     * @var integer $webuserid
     *
     * @ORM\Column(name="WebUserId", type="integer", nullable=true)
     */
    private $webuserid;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var boolean $invoiceAddressIsUsed
     *
     * @ORM\Column(name="invoice_address_is_used", type="boolean", nullable=true)
     */
    private $invoiceAddressIsUsed;

    /**
     * @var string $invoiceAddressStreet
     *
     * @ORM\Column(name="invoice_address_street", type="string", length=255, nullable=true)
     */
    private $invoiceAddressStreet;

    /**
     * @var string $invoiceAddressPostCode
     *
     * @ORM\Column(name="invoice_address_post_code", type="string", length=255, nullable=true)
     */
    private $invoiceAddressPostCode;

    /**
     * @var string $invoiceAddressCity
     *
     * @ORM\Column(name="invoice_address_city", type="string", length=255, nullable=true)
     */
    private $invoiceAddressCity;

    /**
     * @var integer $invoiceAddressIdCountry
     *
     * @ORM\Column(name="invoice_address_id_country", type="integer", nullable=true)
     */
    private $invoiceAddressIdCountry;

    /**
     * @var string $invoiceAddressTelephone
     *
     * @ORM\Column(name="invoice_address_telephone", type="string", length=255, nullable=true)
     */
    private $invoiceAddressTelephone;

    /**
     * @var string $invoiceAddressFax
     *
     * @ORM\Column(name="invoice_address_fax", type="string", length=255, nullable=true)
     */
    private $invoiceAddressFax;

    /**
     * @var float $accountBalance
     *
     * @ORM\Column(name="account_balance", type="decimal", nullable=true)
     */
    private $accountBalance;

    /**
     * @var string $invoiceClientName
     *
     * @ORM\Column(name="invoice_client_name", type="string", length=255, nullable=true)
     */
    private $invoiceClientName;

    /**
     * @var string $invoiceCompanyName
     *
     * @ORM\Column(name="invoice_company_name", type="string", length=255, nullable=true)
     */
    private $invoiceCompanyName;

    /**
     * @var string $contactPersonName
     *
     * @ORM\Column(name="contact_person_name", type="string", length=255, nullable=true)
     */
    private $contactPersonName;

    /**
     * @var string $contactPersonEmail
     *
     * @ORM\Column(name="contact_person_email", type="string", length=255, nullable=true)
     */
    private $contactPersonEmail;

    /**
     * @var string $contactPersonTelephoneNumber
     *
     * @ORM\Column(name="contact_person_telephone_number", type="string", length=255, nullable=true)
     */
    private $contactPersonTelephoneNumber;

    /**
     * @var boolean $isStopShipping
     *
     * @ORM\Column(name="is_stop_shipping", type="boolean", nullable=true)
     */
    private $isStopShipping;

    /**
     * @var integer $numberFeedback
     *
     * @ORM\Column(name="number_feedback", type="integer", nullable=true)
     */
    private $numberFeedback;

    /**
     * @var integer $idPreferredPayment
     *
     * @ORM\Column(name="id_preferred_payment", type="integer", nullable=true)
     */
    private $idPreferredPayment;

    /**
     * @var integer $idInvoicesGoesTo
     *
     * @ORM\Column(name="id_invoices_goes_to", type="integer", nullable=true)
     */
    private $idInvoicesGoesTo;

    /**
     * @var integer $idMarginGoesTo
     *
     * @ORM\Column(name="id_margin_goes_to", type="integer", nullable=true)
     */
    private $idMarginGoesTo;

    /**
     * @var integer $idNutrimeGoesTo
     *
     * @ORM\Column(name="id_nutrime_goes_to", type="integer", nullable=true)
     */
    private $idNutrimeGoesTo;

    /**
     * @var boolean $isInvoiceWithTax
     *
     * @ORM\Column(name="is_invoice_with_tax", type="boolean", nullable=true)
     */
    private $isInvoiceWithTax;

    /**
     * @var boolean $isPaymentWithTax
     *
     * @ORM\Column(name="is_payment_with_tax", type="boolean", nullable=true)
     */
    private $isPaymentWithTax;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var integer $idMarginPaymentMode
     *
     * @ORM\Column(name="id_margin_payment_mode", type="integer", nullable=true)
     */
    private $idMarginPaymentMode;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur1
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur1", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur1;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur2
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur2", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur2;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur3
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur3", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur3;

    /**
     * @var integer $paymentNoTaxText
     *
     * @ORM\Column(name="payment_no_tax_text", type="integer", nullable=true)
     */
    private $paymentNoTaxText;

    /**
     * @var float $yearlyDecayPercentage1
     *
     * @ORM\Column(name="yearly_decay_percentage1", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage1;

    /**
     * @var float $yearlyDecayPercentage2
     *
     * @ORM\Column(name="yearly_decay_percentage2", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage2;

    /**
     * @var float $yearlyDecayPercentage3
     *
     * @ORM\Column(name="yearly_decay_percentage3", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage3;

    /**
     * @var string $webLoginUsername
     *
     * @ORM\Column(name="web_login_username", type="string", length=255, nullable=true)
     */
    private $webLoginUsername;

    /**
     * @var string $webLoginPassword
     *
     * @ORM\Column(name="web_login_password", type="string", length=255, nullable=true)
     */
    private $webLoginPassword;

    /**
     * @var integer $webLoginStatus
     *
     * @ORM\Column(name="web_login_status", type="integer", nullable=true)
     */
    private $webLoginStatus;

    /**
     * @var boolean $isCustomerBeContacted
     *
     * @ORM\Column(name="is_customer_be_contacted", type="boolean", nullable=true)
     */
    private $isCustomerBeContacted;

    /**
     * @var boolean $isNutrimeOffered
     *
     * @ORM\Column(name="is_nutrime_offered", type="boolean", nullable=true)
     */
    private $isNutrimeOffered;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var boolean $isDestroySample
     *
     * @ORM\Column(name="is_destroy_sample", type="boolean", nullable=true)
     */
    private $isDestroySample;

    /**
     * @var boolean $sampleCanBeUsedForScience
     *
     * @ORM\Column(name="sample_can_be_used_for_science", type="boolean", nullable=true)
     */
    private $sampleCanBeUsedForScience;

    /**
     * @var boolean $isFutureResearchParticipant
     *
     * @ORM\Column(name="is_future_research_participant", type="boolean", nullable=true)
     */
    private $isFutureResearchParticipant;

    /**
     * @var integer $invoiceDeliveryNote
     *
     * @ORM\Column(name="invoice_delivery_note", type="integer", nullable=true)
     */
    private $invoiceDeliveryNote;

    /**
     * @var integer $webIdLanguage
     *
     * @ORM\Column(name="web_id_language", type="integer", nullable=true)
     */
    private $webIdLanguage;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var boolean $canCreateLoginAndSendStatusMessageToUser
     *
     * @ORM\Column(name="can_create_login_and_send_status_message_to_user", type="boolean", nullable=true)
     */
    private $canCreateLoginAndSendStatusMessageToUser;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var boolean $canViewShop
     *
     * @ORM\Column(name="can_view_shop", type="boolean", nullable=true)
     */
    private $canViewShop;

    /**
     * @var boolean $canCustomersDownloadReports
     *
     * @ORM\Column(name="can_customers_download_reports", type="boolean", nullable=true)
     */
    private $canCustomersDownloadReports;

    /**
     * @var boolean $isGenerateFoodTableExcelVersion
     *
     * @ORM\Column(name="is_generate_food_table_excel_version", type="boolean", nullable=true)
     */
    private $isGenerateFoodTableExcelVersion;

    /**
     * @var boolean $isPrintedReportSentToPartner
     *
     * @ORM\Column(name="is_printed_report_sent_to_partner", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToPartner;

    /**
     * @var boolean $isPrintedReportSentToDc
     *
     * @ORM\Column(name="is_printed_report_sent_to_dc", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToDc;

    /**
     * @var boolean $isPrintedReportSentToCustomer
     *
     * @ORM\Column(name="is_printed_report_sent_to_customer", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToCustomer;

    /**
     * @var boolean $isDigitalReportGoesToCustomer
     *
     * @ORM\Column(name="is_digital_report_goes_to_customer", type="boolean", nullable=true)
     */
    private $isDigitalReportGoesToCustomer;

    /**
     * @var integer $idRecallGoesTo
     *
     * @ORM\Column(name="id_recall_goes_to", type="integer", nullable=true)
     */
    private $idRecallGoesTo;

    /**
     * @var boolean $isCanViewYourOrder
     *
     * @ORM\Column(name="is_can_view_your_order", type="boolean", nullable=true)
     */
    private $isCanViewYourOrder;

    /**
     * @var boolean $isCanViewCustomerProtection
     *
     * @ORM\Column(name="is_can_view_customer_protection", type="boolean", nullable=true)
     */
    private $isCanViewCustomerProtection;

    /**
     * @var boolean $isCanViewRegisterNewCustomer
     *
     * @ORM\Column(name="is_can_view_register_new_customer", type="boolean", nullable=true)
     */
    private $isCanViewRegisterNewCustomer;

    /**
     * @var boolean $isCanViewPendingOrder
     *
     * @ORM\Column(name="is_can_view_pending_order", type="boolean", nullable=true)
     */
    private $isCanViewPendingOrder;

    /**
     * @var boolean $isCanViewSetting
     *
     * @ORM\Column(name="is_can_view_setting", type="boolean", nullable=true)
     */
    private $isCanViewSetting;

    /**
     * @var string $idCode
     *
     * @ORM\Column(name="id_code", type="string", length=5, nullable=true)
     */
    private $idCode;

    /**
     * @var integer $accountingCode
     *
     * @ORM\Column(name="accounting_code", type="integer", nullable=true)
     */
    private $accountingCode;

    /**
     * @var boolean $isWebLoginOptOut
     *
     * @ORM\Column(name="is_web_login_opt_out", type="boolean", nullable=true)
     */
    private $isWebLoginOptOut;

    /**
     * @var integer $webLoginGoesTo
     *
     * @ORM\Column(name="web_login_goes_to", type="integer", nullable=true)
     */
    private $webLoginGoesTo;

    /**
     * @var integer $invoiceNoTaxText
     *
     * @ORM\Column(name="invoice_no_tax_text", type="integer", nullable=true)
     */
    private $invoiceNoTaxText;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var string $commissionPaymentWithTax
     *
     * @ORM\Column(name="commission_payment_with_tax", type="string", length=45, nullable=true)
     */
    private $commissionPaymentWithTax;

    /**
     * @var boolean $isDelayDownloadReport
     *
     * @ORM\Column(name="is_delay_download_report", type="boolean", nullable=true)
     */
    private $isDelayDownloadReport;

    /**
     * @var integer $numberDateDelayDownloadReport
     *
     * @ORM\Column(name="number_date_delay_download_report", type="integer", nullable=true)
     */
    private $numberDateDelayDownloadReport;

    /**
     * @var boolean $isNotAccessReport
     *
     * @ORM\Column(name="is_not_access_report", type="boolean", nullable=true)
     */
    private $isNotAccessReport;

    /**
     * @var boolean $isNotSendEmail
     *
     * @ORM\Column(name="is_not_send_email", type="boolean", nullable=true)
     */
    private $isNotSendEmail;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Partner
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set partner
     *
     * @param string $partner
     * @return Partner
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    
        return $this;
    }

    /**
     * Get partner
     *
     * @return string 
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Partner
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return Partner
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set male
     *
     * @param boolean $male
     * @return Partner
     */
    public function setMale($male)
    {
        $this->male = $male;
    
        return $this;
    }

    /**
     * Get male
     *
     * @return boolean 
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * Set female
     *
     * @param boolean $female
     * @return Partner
     */
    public function setFemale($female)
    {
        $this->female = $female;
    
        return $this;
    }

    /**
     * Get female
     *
     * @return boolean 
     */
    public function getFemale()
    {
        return $this->female;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Partner
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Partner
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Partner
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return Partner
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    
        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Partner
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Partner
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Partner
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Partner
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;
    
        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set contactemail
     *
     * @param string $contactemail
     * @return Partner
     */
    public function setContactemail($contactemail)
    {
        $this->contactemail = $contactemail;
    
        return $this;
    }

    /**
     * Get contactemail
     *
     * @return string 
     */
    public function getContactemail()
    {
        return $this->contactemail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Partner
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Partner
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set statusdeliveryemail
     *
     * @param string $statusdeliveryemail
     * @return Partner
     */
    public function setStatusdeliveryemail($statusdeliveryemail)
    {
        $this->statusdeliveryemail = $statusdeliveryemail;
    
        return $this;
    }

    /**
     * Get statusdeliveryemail
     *
     * @return string 
     */
    public function getStatusdeliveryemail()
    {
        return $this->statusdeliveryemail;
    }

    /**
     * Set invoicedeliveryemail
     *
     * @param string $invoicedeliveryemail
     * @return Partner
     */
    public function setInvoicedeliveryemail($invoicedeliveryemail)
    {
        $this->invoicedeliveryemail = $invoicedeliveryemail;
    
        return $this;
    }

    /**
     * Get invoicedeliveryemail
     *
     * @return string 
     */
    public function getInvoicedeliveryemail()
    {
        return $this->invoicedeliveryemail;
    }

    /**
     * Set accountname
     *
     * @param string $accountname
     * @return Partner
     */
    public function setAccountname($accountname)
    {
        $this->accountname = $accountname;
    
        return $this;
    }

    /**
     * Get accountname
     *
     * @return string 
     */
    public function getAccountname()
    {
        return $this->accountname;
    }

    /**
     * Set accountnumber
     *
     * @param string $accountnumber
     * @return Partner
     */
    public function setAccountnumber($accountnumber)
    {
        $this->accountnumber = $accountnumber;
    
        return $this;
    }

    /**
     * Get accountnumber
     *
     * @return string 
     */
    public function getAccountnumber()
    {
        return $this->accountnumber;
    }

    /**
     * Set bankcode
     *
     * @param string $bankcode
     * @return Partner
     */
    public function setBankcode($bankcode)
    {
        $this->bankcode = $bankcode;
    
        return $this;
    }

    /**
     * Get bankcode
     *
     * @return string 
     */
    public function getBankcode()
    {
        return $this->bankcode;
    }

    /**
     * Set bic
     *
     * @param string $bic
     * @return Partner
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    
        return $this;
    }

    /**
     * Get bic
     *
     * @return string 
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return Partner
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    
        return $this;
    }

    /**
     * Get iban
     *
     * @return string 
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set othernotes
     *
     * @param string $othernotes
     * @return Partner
     */
    public function setOthernotes($othernotes)
    {
        $this->othernotes = $othernotes;
    
        return $this;
    }

    /**
     * Get othernotes
     *
     * @return string 
     */
    public function getOthernotes()
    {
        return $this->othernotes;
    }

    /**
     * Set acquisiteurid1
     *
     * @param integer $acquisiteurid1
     * @return Partner
     */
    public function setAcquisiteurid1($acquisiteurid1)
    {
        $this->acquisiteurid1 = $acquisiteurid1;
    
        return $this;
    }

    /**
     * Get acquisiteurid1
     *
     * @return integer 
     */
    public function getAcquisiteurid1()
    {
        return $this->acquisiteurid1;
    }

    /**
     * Set commission1
     *
     * @param float $commission1
     * @return Partner
     */
    public function setCommission1($commission1)
    {
        $this->commission1 = $commission1;
    
        return $this;
    }

    /**
     * Get commission1
     *
     * @return float 
     */
    public function getCommission1()
    {
        return $this->commission1;
    }

    /**
     * Set acquisiteurid2
     *
     * @param integer $acquisiteurid2
     * @return Partner
     */
    public function setAcquisiteurid2($acquisiteurid2)
    {
        $this->acquisiteurid2 = $acquisiteurid2;
    
        return $this;
    }

    /**
     * Get acquisiteurid2
     *
     * @return integer 
     */
    public function getAcquisiteurid2()
    {
        return $this->acquisiteurid2;
    }

    /**
     * Set commission2
     *
     * @param float $commission2
     * @return Partner
     */
    public function setCommission2($commission2)
    {
        $this->commission2 = $commission2;
    
        return $this;
    }

    /**
     * Get commission2
     *
     * @return float 
     */
    public function getCommission2()
    {
        return $this->commission2;
    }

    /**
     * Set acquisiteurid3
     *
     * @param integer $acquisiteurid3
     * @return Partner
     */
    public function setAcquisiteurid3($acquisiteurid3)
    {
        $this->acquisiteurid3 = $acquisiteurid3;
    
        return $this;
    }

    /**
     * Get acquisiteurid3
     *
     * @return integer 
     */
    public function getAcquisiteurid3()
    {
        return $this->acquisiteurid3;
    }

    /**
     * Set commission3
     *
     * @param float $commission3
     * @return Partner
     */
    public function setCommission3($commission3)
    {
        $this->commission3 = $commission3;
    
        return $this;
    }

    /**
     * Get commission3
     *
     * @return float 
     */
    public function getCommission3()
    {
        return $this->commission3;
    }

    /**
     * Set webusers
     *
     * @param string $webusers
     * @return Partner
     */
    public function setWebusers($webusers)
    {
        $this->webusers = $webusers;
    
        return $this;
    }

    /**
     * Get webusers
     *
     * @return string 
     */
    public function getWebusers()
    {
        return $this->webusers;
    }

    /**
     * Set uidnumber
     *
     * @param string $uidnumber
     * @return Partner
     */
    public function setUidnumber($uidnumber)
    {
        $this->uidnumber = $uidnumber;
    
        return $this;
    }

    /**
     * Get uidnumber
     *
     * @return string 
     */
    public function getUidnumber()
    {
        return $this->uidnumber;
    }

    /**
     * Set webuserid
     *
     * @param integer $webuserid
     * @return Partner
     */
    public function setWebuserid($webuserid)
    {
        $this->webuserid = $webuserid;
    
        return $this;
    }

    /**
     * Get webuserid
     *
     * @return integer 
     */
    public function getWebuserid()
    {
        return $this->webuserid;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Partner
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return Partner
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }

    /**
     * Set invoiceAddressIsUsed
     *
     * @param boolean $invoiceAddressIsUsed
     * @return Partner
     */
    public function setInvoiceAddressIsUsed($invoiceAddressIsUsed)
    {
        $this->invoiceAddressIsUsed = $invoiceAddressIsUsed;
    
        return $this;
    }

    /**
     * Get invoiceAddressIsUsed
     *
     * @return boolean 
     */
    public function getInvoiceAddressIsUsed()
    {
        return $this->invoiceAddressIsUsed;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     * @return Partner
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;
    
        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string 
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressPostCode
     *
     * @param string $invoiceAddressPostCode
     * @return Partner
     */
    public function setInvoiceAddressPostCode($invoiceAddressPostCode)
    {
        $this->invoiceAddressPostCode = $invoiceAddressPostCode;
    
        return $this;
    }

    /**
     * Get invoiceAddressPostCode
     *
     * @return string 
     */
    public function getInvoiceAddressPostCode()
    {
        return $this->invoiceAddressPostCode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     * @return Partner
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;
    
        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string 
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressIdCountry
     *
     * @param integer $invoiceAddressIdCountry
     * @return Partner
     */
    public function setInvoiceAddressIdCountry($invoiceAddressIdCountry)
    {
        $this->invoiceAddressIdCountry = $invoiceAddressIdCountry;
    
        return $this;
    }

    /**
     * Get invoiceAddressIdCountry
     *
     * @return integer 
     */
    public function getInvoiceAddressIdCountry()
    {
        return $this->invoiceAddressIdCountry;
    }

    /**
     * Set invoiceAddressTelephone
     *
     * @param string $invoiceAddressTelephone
     * @return Partner
     */
    public function setInvoiceAddressTelephone($invoiceAddressTelephone)
    {
        $this->invoiceAddressTelephone = $invoiceAddressTelephone;
    
        return $this;
    }

    /**
     * Get invoiceAddressTelephone
     *
     * @return string 
     */
    public function getInvoiceAddressTelephone()
    {
        return $this->invoiceAddressTelephone;
    }

    /**
     * Set invoiceAddressFax
     *
     * @param string $invoiceAddressFax
     * @return Partner
     */
    public function setInvoiceAddressFax($invoiceAddressFax)
    {
        $this->invoiceAddressFax = $invoiceAddressFax;
    
        return $this;
    }

    /**
     * Get invoiceAddressFax
     *
     * @return string 
     */
    public function getInvoiceAddressFax()
    {
        return $this->invoiceAddressFax;
    }

    /**
     * Set accountBalance
     *
     * @param float $accountBalance
     * @return Partner
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;
    
        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return float 
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set invoiceClientName
     *
     * @param string $invoiceClientName
     * @return Partner
     */
    public function setInvoiceClientName($invoiceClientName)
    {
        $this->invoiceClientName = $invoiceClientName;
    
        return $this;
    }

    /**
     * Get invoiceClientName
     *
     * @return string 
     */
    public function getInvoiceClientName()
    {
        return $this->invoiceClientName;
    }

    /**
     * Set invoiceCompanyName
     *
     * @param string $invoiceCompanyName
     * @return Partner
     */
    public function setInvoiceCompanyName($invoiceCompanyName)
    {
        $this->invoiceCompanyName = $invoiceCompanyName;
    
        return $this;
    }

    /**
     * Get invoiceCompanyName
     *
     * @return string 
     */
    public function getInvoiceCompanyName()
    {
        return $this->invoiceCompanyName;
    }

    /**
     * Set contactPersonName
     *
     * @param string $contactPersonName
     * @return Partner
     */
    public function setContactPersonName($contactPersonName)
    {
        $this->contactPersonName = $contactPersonName;
    
        return $this;
    }

    /**
     * Get contactPersonName
     *
     * @return string 
     */
    public function getContactPersonName()
    {
        return $this->contactPersonName;
    }

    /**
     * Set contactPersonEmail
     *
     * @param string $contactPersonEmail
     * @return Partner
     */
    public function setContactPersonEmail($contactPersonEmail)
    {
        $this->contactPersonEmail = $contactPersonEmail;
    
        return $this;
    }

    /**
     * Get contactPersonEmail
     *
     * @return string 
     */
    public function getContactPersonEmail()
    {
        return $this->contactPersonEmail;
    }

    /**
     * Set contactPersonTelephoneNumber
     *
     * @param string $contactPersonTelephoneNumber
     * @return Partner
     */
    public function setContactPersonTelephoneNumber($contactPersonTelephoneNumber)
    {
        $this->contactPersonTelephoneNumber = $contactPersonTelephoneNumber;
    
        return $this;
    }

    /**
     * Get contactPersonTelephoneNumber
     *
     * @return string 
     */
    public function getContactPersonTelephoneNumber()
    {
        return $this->contactPersonTelephoneNumber;
    }

    /**
     * Set isStopShipping
     *
     * @param boolean $isStopShipping
     * @return Partner
     */
    public function setIsStopShipping($isStopShipping)
    {
        $this->isStopShipping = $isStopShipping;
    
        return $this;
    }

    /**
     * Get isStopShipping
     *
     * @return boolean 
     */
    public function getIsStopShipping()
    {
        return $this->isStopShipping;
    }

    /**
     * Set numberFeedback
     *
     * @param integer $numberFeedback
     * @return Partner
     */
    public function setNumberFeedback($numberFeedback)
    {
        $this->numberFeedback = $numberFeedback;
    
        return $this;
    }

    /**
     * Get numberFeedback
     *
     * @return integer 
     */
    public function getNumberFeedback()
    {
        return $this->numberFeedback;
    }

    /**
     * Set idPreferredPayment
     *
     * @param integer $idPreferredPayment
     * @return Partner
     */
    public function setIdPreferredPayment($idPreferredPayment)
    {
        $this->idPreferredPayment = $idPreferredPayment;
    
        return $this;
    }

    /**
     * Get idPreferredPayment
     *
     * @return integer 
     */
    public function getIdPreferredPayment()
    {
        return $this->idPreferredPayment;
    }

    /**
     * Set idInvoicesGoesTo
     *
     * @param integer $idInvoicesGoesTo
     * @return Partner
     */
    public function setIdInvoicesGoesTo($idInvoicesGoesTo)
    {
        $this->idInvoicesGoesTo = $idInvoicesGoesTo;
    
        return $this;
    }

    /**
     * Get idInvoicesGoesTo
     *
     * @return integer 
     */
    public function getIdInvoicesGoesTo()
    {
        return $this->idInvoicesGoesTo;
    }

    /**
     * Set idMarginGoesTo
     *
     * @param integer $idMarginGoesTo
     * @return Partner
     */
    public function setIdMarginGoesTo($idMarginGoesTo)
    {
        $this->idMarginGoesTo = $idMarginGoesTo;
    
        return $this;
    }

    /**
     * Get idMarginGoesTo
     *
     * @return integer 
     */
    public function getIdMarginGoesTo()
    {
        return $this->idMarginGoesTo;
    }

    /**
     * Set idNutrimeGoesTo
     *
     * @param integer $idNutrimeGoesTo
     * @return Partner
     */
    public function setIdNutrimeGoesTo($idNutrimeGoesTo)
    {
        $this->idNutrimeGoesTo = $idNutrimeGoesTo;
    
        return $this;
    }

    /**
     * Get idNutrimeGoesTo
     *
     * @return integer 
     */
    public function getIdNutrimeGoesTo()
    {
        return $this->idNutrimeGoesTo;
    }

    /**
     * Set isInvoiceWithTax
     *
     * @param boolean $isInvoiceWithTax
     * @return Partner
     */
    public function setIsInvoiceWithTax($isInvoiceWithTax)
    {
        $this->isInvoiceWithTax = $isInvoiceWithTax;
    
        return $this;
    }

    /**
     * Get isInvoiceWithTax
     *
     * @return boolean 
     */
    public function getIsInvoiceWithTax()
    {
        return $this->isInvoiceWithTax;
    }

    /**
     * Set isPaymentWithTax
     *
     * @param boolean $isPaymentWithTax
     * @return Partner
     */
    public function setIsPaymentWithTax($isPaymentWithTax)
    {
        $this->isPaymentWithTax = $isPaymentWithTax;
    
        return $this;
    }

    /**
     * Get isPaymentWithTax
     *
     * @return boolean 
     */
    public function getIsPaymentWithTax()
    {
        return $this->isPaymentWithTax;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Partner
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set idMarginPaymentMode
     *
     * @param integer $idMarginPaymentMode
     * @return Partner
     */
    public function setIdMarginPaymentMode($idMarginPaymentMode)
    {
        $this->idMarginPaymentMode = $idMarginPaymentMode;
    
        return $this;
    }

    /**
     * Get idMarginPaymentMode
     *
     * @return integer 
     */
    public function getIdMarginPaymentMode()
    {
        return $this->idMarginPaymentMode;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur1
     *
     * @param boolean $isTimelyDeclinedAcquisiteur1
     * @return Partner
     */
    public function setIsTimelyDeclinedAcquisiteur1($isTimelyDeclinedAcquisiteur1)
    {
        $this->isTimelyDeclinedAcquisiteur1 = $isTimelyDeclinedAcquisiteur1;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur1
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur1()
    {
        return $this->isTimelyDeclinedAcquisiteur1;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur2
     *
     * @param boolean $isTimelyDeclinedAcquisiteur2
     * @return Partner
     */
    public function setIsTimelyDeclinedAcquisiteur2($isTimelyDeclinedAcquisiteur2)
    {
        $this->isTimelyDeclinedAcquisiteur2 = $isTimelyDeclinedAcquisiteur2;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur2
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur2()
    {
        return $this->isTimelyDeclinedAcquisiteur2;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur3
     *
     * @param boolean $isTimelyDeclinedAcquisiteur3
     * @return Partner
     */
    public function setIsTimelyDeclinedAcquisiteur3($isTimelyDeclinedAcquisiteur3)
    {
        $this->isTimelyDeclinedAcquisiteur3 = $isTimelyDeclinedAcquisiteur3;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur3
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur3()
    {
        return $this->isTimelyDeclinedAcquisiteur3;
    }

    /**
     * Set paymentNoTaxText
     *
     * @param integer $paymentNoTaxText
     * @return Partner
     */
    public function setPaymentNoTaxText($paymentNoTaxText)
    {
        $this->paymentNoTaxText = $paymentNoTaxText;
    
        return $this;
    }

    /**
     * Get paymentNoTaxText
     *
     * @return integer 
     */
    public function getPaymentNoTaxText()
    {
        return $this->paymentNoTaxText;
    }

    /**
     * Set yearlyDecayPercentage1
     *
     * @param float $yearlyDecayPercentage1
     * @return Partner
     */
    public function setYearlyDecayPercentage1($yearlyDecayPercentage1)
    {
        $this->yearlyDecayPercentage1 = $yearlyDecayPercentage1;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage1
     *
     * @return float 
     */
    public function getYearlyDecayPercentage1()
    {
        return $this->yearlyDecayPercentage1;
    }

    /**
     * Set yearlyDecayPercentage2
     *
     * @param float $yearlyDecayPercentage2
     * @return Partner
     */
    public function setYearlyDecayPercentage2($yearlyDecayPercentage2)
    {
        $this->yearlyDecayPercentage2 = $yearlyDecayPercentage2;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage2
     *
     * @return float 
     */
    public function getYearlyDecayPercentage2()
    {
        return $this->yearlyDecayPercentage2;
    }

    /**
     * Set yearlyDecayPercentage3
     *
     * @param float $yearlyDecayPercentage3
     * @return Partner
     */
    public function setYearlyDecayPercentage3($yearlyDecayPercentage3)
    {
        $this->yearlyDecayPercentage3 = $yearlyDecayPercentage3;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage3
     *
     * @return float 
     */
    public function getYearlyDecayPercentage3()
    {
        return $this->yearlyDecayPercentage3;
    }

    /**
     * Set webLoginUsername
     *
     * @param string $webLoginUsername
     * @return Partner
     */
    public function setWebLoginUsername($webLoginUsername)
    {
        $this->webLoginUsername = $webLoginUsername;
    
        return $this;
    }

    /**
     * Get webLoginUsername
     *
     * @return string 
     */
    public function getWebLoginUsername()
    {
        return $this->webLoginUsername;
    }

    /**
     * Set webLoginPassword
     *
     * @param string $webLoginPassword
     * @return Partner
     */
    public function setWebLoginPassword($webLoginPassword)
    {
        $this->webLoginPassword = $webLoginPassword;
    
        return $this;
    }

    /**
     * Get webLoginPassword
     *
     * @return string 
     */
    public function getWebLoginPassword()
    {
        return $this->webLoginPassword;
    }

    /**
     * Set webLoginStatus
     *
     * @param integer $webLoginStatus
     * @return Partner
     */
    public function setWebLoginStatus($webLoginStatus)
    {
        $this->webLoginStatus = $webLoginStatus;
    
        return $this;
    }

    /**
     * Get webLoginStatus
     *
     * @return integer 
     */
    public function getWebLoginStatus()
    {
        return $this->webLoginStatus;
    }

    /**
     * Set isCustomerBeContacted
     *
     * @param boolean $isCustomerBeContacted
     * @return Partner
     */
    public function setIsCustomerBeContacted($isCustomerBeContacted)
    {
        $this->isCustomerBeContacted = $isCustomerBeContacted;
    
        return $this;
    }

    /**
     * Get isCustomerBeContacted
     *
     * @return boolean 
     */
    public function getIsCustomerBeContacted()
    {
        return $this->isCustomerBeContacted;
    }

    /**
     * Set isNutrimeOffered
     *
     * @param boolean $isNutrimeOffered
     * @return Partner
     */
    public function setIsNutrimeOffered($isNutrimeOffered)
    {
        $this->isNutrimeOffered = $isNutrimeOffered;
    
        return $this;
    }

    /**
     * Get isNutrimeOffered
     *
     * @return boolean 
     */
    public function getIsNutrimeOffered()
    {
        return $this->isNutrimeOffered;
    }

    /**
     * Set isNotContacted
     *
     * @param boolean $isNotContacted
     * @return Partner
     */
    public function setIsNotContacted($isNotContacted)
    {
        $this->isNotContacted = $isNotContacted;
    
        return $this;
    }

    /**
     * Get isNotContacted
     *
     * @return boolean 
     */
    public function getIsNotContacted()
    {
        return $this->isNotContacted;
    }

    /**
     * Set isDestroySample
     *
     * @param boolean $isDestroySample
     * @return Partner
     */
    public function setIsDestroySample($isDestroySample)
    {
        $this->isDestroySample = $isDestroySample;
    
        return $this;
    }

    /**
     * Get isDestroySample
     *
     * @return boolean 
     */
    public function getIsDestroySample()
    {
        return $this->isDestroySample;
    }

    /**
     * Set sampleCanBeUsedForScience
     *
     * @param boolean $sampleCanBeUsedForScience
     * @return Partner
     */
    public function setSampleCanBeUsedForScience($sampleCanBeUsedForScience)
    {
        $this->sampleCanBeUsedForScience = $sampleCanBeUsedForScience;
    
        return $this;
    }

    /**
     * Get sampleCanBeUsedForScience
     *
     * @return boolean 
     */
    public function getSampleCanBeUsedForScience()
    {
        return $this->sampleCanBeUsedForScience;
    }

    /**
     * Set isFutureResearchParticipant
     *
     * @param boolean $isFutureResearchParticipant
     * @return Partner
     */
    public function setIsFutureResearchParticipant($isFutureResearchParticipant)
    {
        $this->isFutureResearchParticipant = $isFutureResearchParticipant;
    
        return $this;
    }

    /**
     * Get isFutureResearchParticipant
     *
     * @return boolean 
     */
    public function getIsFutureResearchParticipant()
    {
        return $this->isFutureResearchParticipant;
    }

    /**
     * Set invoiceDeliveryNote
     *
     * @param integer $invoiceDeliveryNote
     * @return Partner
     */
    public function setInvoiceDeliveryNote($invoiceDeliveryNote)
    {
        $this->invoiceDeliveryNote = $invoiceDeliveryNote;
    
        return $this;
    }

    /**
     * Get invoiceDeliveryNote
     *
     * @return integer 
     */
    public function getInvoiceDeliveryNote()
    {
        return $this->invoiceDeliveryNote;
    }

    /**
     * Set webIdLanguage
     *
     * @param integer $webIdLanguage
     * @return Partner
     */
    public function setWebIdLanguage($webIdLanguage)
    {
        $this->webIdLanguage = $webIdLanguage;
    
        return $this;
    }

    /**
     * Get webIdLanguage
     *
     * @return integer 
     */
    public function getWebIdLanguage()
    {
        return $this->webIdLanguage;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return Partner
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set canCreateLoginAndSendStatusMessageToUser
     *
     * @param boolean $canCreateLoginAndSendStatusMessageToUser
     * @return Partner
     */
    public function setCanCreateLoginAndSendStatusMessageToUser($canCreateLoginAndSendStatusMessageToUser)
    {
        $this->canCreateLoginAndSendStatusMessageToUser = $canCreateLoginAndSendStatusMessageToUser;
    
        return $this;
    }

    /**
     * Get canCreateLoginAndSendStatusMessageToUser
     *
     * @return boolean 
     */
    public function getCanCreateLoginAndSendStatusMessageToUser()
    {
        return $this->canCreateLoginAndSendStatusMessageToUser;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return Partner
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set canViewShop
     *
     * @param boolean $canViewShop
     * @return Partner
     */
    public function setCanViewShop($canViewShop)
    {
        $this->canViewShop = $canViewShop;
    
        return $this;
    }

    /**
     * Get canViewShop
     *
     * @return boolean 
     */
    public function getCanViewShop()
    {
        return $this->canViewShop;
    }

    /**
     * Set canCustomersDownloadReports
     *
     * @param boolean $canCustomersDownloadReports
     * @return Partner
     */
    public function setCanCustomersDownloadReports($canCustomersDownloadReports)
    {
        $this->canCustomersDownloadReports = $canCustomersDownloadReports;
    
        return $this;
    }

    /**
     * Get canCustomersDownloadReports
     *
     * @return boolean 
     */
    public function getCanCustomersDownloadReports()
    {
        return $this->canCustomersDownloadReports;
    }

    /**
     * Set isGenerateFoodTableExcelVersion
     *
     * @param boolean $isGenerateFoodTableExcelVersion
     * @return Partner
     */
    public function setIsGenerateFoodTableExcelVersion($isGenerateFoodTableExcelVersion)
    {
        $this->isGenerateFoodTableExcelVersion = $isGenerateFoodTableExcelVersion;
    
        return $this;
    }

    /**
     * Get isGenerateFoodTableExcelVersion
     *
     * @return boolean 
     */
    public function getIsGenerateFoodTableExcelVersion()
    {
        return $this->isGenerateFoodTableExcelVersion;
    }

    /**
     * Set isPrintedReportSentToPartner
     *
     * @param boolean $isPrintedReportSentToPartner
     * @return Partner
     */
    public function setIsPrintedReportSentToPartner($isPrintedReportSentToPartner)
    {
        $this->isPrintedReportSentToPartner = $isPrintedReportSentToPartner;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToPartner
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToPartner()
    {
        return $this->isPrintedReportSentToPartner;
    }

    /**
     * Set isPrintedReportSentToDc
     *
     * @param boolean $isPrintedReportSentToDc
     * @return Partner
     */
    public function setIsPrintedReportSentToDc($isPrintedReportSentToDc)
    {
        $this->isPrintedReportSentToDc = $isPrintedReportSentToDc;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToDc
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToDc()
    {
        return $this->isPrintedReportSentToDc;
    }

    /**
     * Set isPrintedReportSentToCustomer
     *
     * @param boolean $isPrintedReportSentToCustomer
     * @return Partner
     */
    public function setIsPrintedReportSentToCustomer($isPrintedReportSentToCustomer)
    {
        $this->isPrintedReportSentToCustomer = $isPrintedReportSentToCustomer;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToCustomer
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToCustomer()
    {
        return $this->isPrintedReportSentToCustomer;
    }

    /**
     * Set isDigitalReportGoesToCustomer
     *
     * @param boolean $isDigitalReportGoesToCustomer
     * @return Partner
     */
    public function setIsDigitalReportGoesToCustomer($isDigitalReportGoesToCustomer)
    {
        $this->isDigitalReportGoesToCustomer = $isDigitalReportGoesToCustomer;
    
        return $this;
    }

    /**
     * Get isDigitalReportGoesToCustomer
     *
     * @return boolean 
     */
    public function getIsDigitalReportGoesToCustomer()
    {
        return $this->isDigitalReportGoesToCustomer;
    }

    /**
     * Set idRecallGoesTo
     *
     * @param integer $idRecallGoesTo
     * @return Partner
     */
    public function setIdRecallGoesTo($idRecallGoesTo)
    {
        $this->idRecallGoesTo = $idRecallGoesTo;
    
        return $this;
    }

    /**
     * Get idRecallGoesTo
     *
     * @return integer 
     */
    public function getIdRecallGoesTo()
    {
        return $this->idRecallGoesTo;
    }

    /**
     * Set isCanViewYourOrder
     *
     * @param boolean $isCanViewYourOrder
     * @return Partner
     */
    public function setIsCanViewYourOrder($isCanViewYourOrder)
    {
        $this->isCanViewYourOrder = $isCanViewYourOrder;
    
        return $this;
    }

    /**
     * Get isCanViewYourOrder
     *
     * @return boolean 
     */
    public function getIsCanViewYourOrder()
    {
        return $this->isCanViewYourOrder;
    }

    /**
     * Set isCanViewCustomerProtection
     *
     * @param boolean $isCanViewCustomerProtection
     * @return Partner
     */
    public function setIsCanViewCustomerProtection($isCanViewCustomerProtection)
    {
        $this->isCanViewCustomerProtection = $isCanViewCustomerProtection;
    
        return $this;
    }

    /**
     * Get isCanViewCustomerProtection
     *
     * @return boolean 
     */
    public function getIsCanViewCustomerProtection()
    {
        return $this->isCanViewCustomerProtection;
    }

    /**
     * Set isCanViewRegisterNewCustomer
     *
     * @param boolean $isCanViewRegisterNewCustomer
     * @return Partner
     */
    public function setIsCanViewRegisterNewCustomer($isCanViewRegisterNewCustomer)
    {
        $this->isCanViewRegisterNewCustomer = $isCanViewRegisterNewCustomer;
    
        return $this;
    }

    /**
     * Get isCanViewRegisterNewCustomer
     *
     * @return boolean 
     */
    public function getIsCanViewRegisterNewCustomer()
    {
        return $this->isCanViewRegisterNewCustomer;
    }

    /**
     * Set isCanViewPendingOrder
     *
     * @param boolean $isCanViewPendingOrder
     * @return Partner
     */
    public function setIsCanViewPendingOrder($isCanViewPendingOrder)
    {
        $this->isCanViewPendingOrder = $isCanViewPendingOrder;
    
        return $this;
    }

    /**
     * Get isCanViewPendingOrder
     *
     * @return boolean 
     */
    public function getIsCanViewPendingOrder()
    {
        return $this->isCanViewPendingOrder;
    }

    /**
     * Set isCanViewSetting
     *
     * @param boolean $isCanViewSetting
     * @return Partner
     */
    public function setIsCanViewSetting($isCanViewSetting)
    {
        $this->isCanViewSetting = $isCanViewSetting;
    
        return $this;
    }

    /**
     * Get isCanViewSetting
     *
     * @return boolean 
     */
    public function getIsCanViewSetting()
    {
        return $this->isCanViewSetting;
    }

    /**
     * Set idCode
     *
     * @param string $idCode
     * @return Partner
     */
    public function setIdCode($idCode)
    {
        $this->idCode = $idCode;
    
        return $this;
    }

    /**
     * Get idCode
     *
     * @return string 
     */
    public function getIdCode()
    {
        return $this->idCode;
    }

    /**
     * Set accountingCode
     *
     * @param integer $accountingCode
     * @return Partner
     */
    public function setAccountingCode($accountingCode)
    {
        $this->accountingCode = $accountingCode;
    
        return $this;
    }

    /**
     * Get accountingCode
     *
     * @return integer 
     */
    public function getAccountingCode()
    {
        return $this->accountingCode;
    }

    /**
     * Set isWebLoginOptOut
     *
     * @param boolean $isWebLoginOptOut
     * @return Partner
     */
    public function setIsWebLoginOptOut($isWebLoginOptOut)
    {
        $this->isWebLoginOptOut = $isWebLoginOptOut;
    
        return $this;
    }

    /**
     * Get isWebLoginOptOut
     *
     * @return boolean 
     */
    public function getIsWebLoginOptOut()
    {
        return $this->isWebLoginOptOut;
    }

    /**
     * Set webLoginGoesTo
     *
     * @param integer $webLoginGoesTo
     * @return Partner
     */
    public function setWebLoginGoesTo($webLoginGoesTo)
    {
        $this->webLoginGoesTo = $webLoginGoesTo;
    
        return $this;
    }

    /**
     * Get webLoginGoesTo
     *
     * @return integer 
     */
    public function getWebLoginGoesTo()
    {
        return $this->webLoginGoesTo;
    }

    /**
     * Set invoiceNoTaxText
     *
     * @param integer $invoiceNoTaxText
     * @return Partner
     */
    public function setInvoiceNoTaxText($invoiceNoTaxText)
    {
        $this->invoiceNoTaxText = $invoiceNoTaxText;
    
        return $this;
    }

    /**
     * Get invoiceNoTaxText
     *
     * @return integer 
     */
    public function getInvoiceNoTaxText()
    {
        return $this->invoiceNoTaxText;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return Partner
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set commissionPaymentWithTax
     *
     * @param string $commissionPaymentWithTax
     * @return Partner
     */
    public function setCommissionPaymentWithTax($commissionPaymentWithTax)
    {
        $this->commissionPaymentWithTax = $commissionPaymentWithTax;
    
        return $this;
    }

    /**
     * Get commissionPaymentWithTax
     *
     * @return string 
     */
    public function getCommissionPaymentWithTax()
    {
        return $this->commissionPaymentWithTax;
    }

    /**
     * Set isDelayDownloadReport
     *
     * @param boolean $isDelayDownloadReport
     * @return Partner
     */
    public function setIsDelayDownloadReport($isDelayDownloadReport)
    {
        $this->isDelayDownloadReport = $isDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get isDelayDownloadReport
     *
     * @return boolean 
     */
    public function getIsDelayDownloadReport()
    {
        return $this->isDelayDownloadReport;
    }

    /**
     * Set numberDateDelayDownloadReport
     *
     * @param integer $numberDateDelayDownloadReport
     * @return Partner
     */
    public function setNumberDateDelayDownloadReport($numberDateDelayDownloadReport)
    {
        $this->numberDateDelayDownloadReport = $numberDateDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get numberDateDelayDownloadReport
     *
     * @return integer 
     */
    public function getNumberDateDelayDownloadReport()
    {
        return $this->numberDateDelayDownloadReport;
    }

    /**
     * Set isNotAccessReport
     *
     * @param boolean $isNotAccessReport
     * @return Partner
     */
    public function setIsNotAccessReport($isNotAccessReport)
    {
        $this->isNotAccessReport = $isNotAccessReport;
    
        return $this;
    }

    /**
     * Get isNotAccessReport
     *
     * @return boolean 
     */
    public function getIsNotAccessReport()
    {
        return $this->isNotAccessReport;
    }

    /**
     * Set isNotSendEmail
     *
     * @param boolean $isNotSendEmail
     * @return Partner
     */
    public function setIsNotSendEmail($isNotSendEmail)
    {
        $this->isNotSendEmail = $isNotSendEmail;
    
        return $this;
    }

    /**
     * Get isNotSendEmail
     *
     * @return boolean 
     */
    public function getIsNotSendEmail()
    {
        return $this->isNotSendEmail;
    }
}