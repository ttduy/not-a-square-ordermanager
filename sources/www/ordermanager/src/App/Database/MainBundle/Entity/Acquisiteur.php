<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Acquisiteur
 *
 * @ORM\Table(name="acquisiteur")
 * @ORM\Entity
 */
class Acquisiteur
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $uidNumber
     *
     * @ORM\Column(name="uid_number", type="string", length=50, nullable=true)
     */
    private $uidNumber;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="Domain", type="text", nullable=true)
     */
    private $domain;

    /**
     * @var string $acquisiteur
     *
     * @ORM\Column(name="Acquisiteur", type="string", length=255, nullable=true)
     */
    private $acquisiteur;

    /**
     * @var string $type
     *
     * @ORM\Column(name="Type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var boolean $male
     *
     * @ORM\Column(name="Male", type="boolean", nullable=true)
     */
    private $male;

    /**
     * @var boolean $female
     *
     * @ORM\Column(name="Female", type="boolean", nullable=true)
     */
    private $female;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="SurName", type="text", nullable=true)
     */
    private $surname;

    /**
     * @var string $institution
     *
     * @ORM\Column(name="Institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $countryid
     *
     * @ORM\Column(name="CountryId", type="integer", nullable=true)
     */
    private $countryid;

    /**
     * @var string $contactemail
     *
     * @ORM\Column(name="ContactEmail", type="string", length=255, nullable=true)
     */
    private $contactemail;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $statusdeliveryemail
     *
     * @ORM\Column(name="StatusDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $statusdeliveryemail;

    /**
     * @var string $invoicedeliveryemail
     *
     * @ORM\Column(name="InvoiceDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $invoicedeliveryemail;

    /**
     * @var string $accountname
     *
     * @ORM\Column(name="AccountName", type="string", length=36, nullable=true)
     */
    private $accountname;

    /**
     * @var string $accountnumber
     *
     * @ORM\Column(name="AccountNumber", type="string", length=50, nullable=true)
     */
    private $accountnumber;

    /**
     * @var string $bankcode
     *
     * @ORM\Column(name="BankCode", type="string", length=50, nullable=true)
     */
    private $bankcode;

    /**
     * @var string $bic
     *
     * @ORM\Column(name="BIC", type="string", length=50, nullable=true)
     */
    private $bic;

    /**
     * @var string $iban
     *
     * @ORM\Column(name="IBAN", type="string", length=36, nullable=true)
     */
    private $iban;

    /**
     * @var string $othernotes
     *
     * @ORM\Column(name="OtherNotes", type="text", nullable=true)
     */
    private $othernotes;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var integer $idMarginPaymentMode
     *
     * @ORM\Column(name="id_margin_payment_mode", type="integer", nullable=true)
     */
    private $idMarginPaymentMode;

    /**
     * @var integer $paymentNoTaxText
     *
     * @ORM\Column(name="payment_no_tax_text", type="integer", nullable=true)
     */
    private $paymentNoTaxText;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var integer $accountingCode
     *
     * @ORM\Column(name="accounting_code", type="integer", nullable=true)
     */
    private $accountingCode;

    /**
     * @var string $commissionPaymentWithTax
     *
     * @ORM\Column(name="commission_payment_with_tax", type="string", length=45, nullable=true)
     */
    private $commissionPaymentWithTax;

    /**
     * @var boolean $isPaymentWithTax
     *
     * @ORM\Column(name="is_payment_with_tax", type="boolean", nullable=true)
     */
    private $isPaymentWithTax;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uidNumber
     *
     * @param string $uidNumber
     * @return Acquisiteur
     */
    public function setUidNumber($uidNumber)
    {
        $this->uidNumber = $uidNumber;
    
        return $this;
    }

    /**
     * Get uidNumber
     *
     * @return string 
     */
    public function getUidNumber()
    {
        return $this->uidNumber;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Acquisiteur
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set acquisiteur
     *
     * @param string $acquisiteur
     * @return Acquisiteur
     */
    public function setAcquisiteur($acquisiteur)
    {
        $this->acquisiteur = $acquisiteur;
    
        return $this;
    }

    /**
     * Get acquisiteur
     *
     * @return string 
     */
    public function getAcquisiteur()
    {
        return $this->acquisiteur;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Acquisiteur
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return Acquisiteur
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set male
     *
     * @param boolean $male
     * @return Acquisiteur
     */
    public function setMale($male)
    {
        $this->male = $male;
    
        return $this;
    }

    /**
     * Get male
     *
     * @return boolean 
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * Set female
     *
     * @param boolean $female
     * @return Acquisiteur
     */
    public function setFemale($female)
    {
        $this->female = $female;
    
        return $this;
    }

    /**
     * Get female
     *
     * @return boolean 
     */
    public function getFemale()
    {
        return $this->female;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Acquisiteur
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Acquisiteur
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Acquisiteur
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return Acquisiteur
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    
        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Acquisiteur
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Acquisiteur
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Acquisiteur
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Acquisiteur
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;
    
        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set contactemail
     *
     * @param string $contactemail
     * @return Acquisiteur
     */
    public function setContactemail($contactemail)
    {
        $this->contactemail = $contactemail;
    
        return $this;
    }

    /**
     * Get contactemail
     *
     * @return string 
     */
    public function getContactemail()
    {
        return $this->contactemail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Acquisiteur
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Acquisiteur
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set statusdeliveryemail
     *
     * @param string $statusdeliveryemail
     * @return Acquisiteur
     */
    public function setStatusdeliveryemail($statusdeliveryemail)
    {
        $this->statusdeliveryemail = $statusdeliveryemail;
    
        return $this;
    }

    /**
     * Get statusdeliveryemail
     *
     * @return string 
     */
    public function getStatusdeliveryemail()
    {
        return $this->statusdeliveryemail;
    }

    /**
     * Set invoicedeliveryemail
     *
     * @param string $invoicedeliveryemail
     * @return Acquisiteur
     */
    public function setInvoicedeliveryemail($invoicedeliveryemail)
    {
        $this->invoicedeliveryemail = $invoicedeliveryemail;
    
        return $this;
    }

    /**
     * Get invoicedeliveryemail
     *
     * @return string 
     */
    public function getInvoicedeliveryemail()
    {
        return $this->invoicedeliveryemail;
    }

    /**
     * Set accountname
     *
     * @param string $accountname
     * @return Acquisiteur
     */
    public function setAccountname($accountname)
    {
        $this->accountname = $accountname;
    
        return $this;
    }

    /**
     * Get accountname
     *
     * @return string 
     */
    public function getAccountname()
    {
        return $this->accountname;
    }

    /**
     * Set accountnumber
     *
     * @param string $accountnumber
     * @return Acquisiteur
     */
    public function setAccountnumber($accountnumber)
    {
        $this->accountnumber = $accountnumber;
    
        return $this;
    }

    /**
     * Get accountnumber
     *
     * @return string 
     */
    public function getAccountnumber()
    {
        return $this->accountnumber;
    }

    /**
     * Set bankcode
     *
     * @param string $bankcode
     * @return Acquisiteur
     */
    public function setBankcode($bankcode)
    {
        $this->bankcode = $bankcode;
    
        return $this;
    }

    /**
     * Get bankcode
     *
     * @return string 
     */
    public function getBankcode()
    {
        return $this->bankcode;
    }

    /**
     * Set bic
     *
     * @param string $bic
     * @return Acquisiteur
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    
        return $this;
    }

    /**
     * Get bic
     *
     * @return string 
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return Acquisiteur
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    
        return $this;
    }

    /**
     * Get iban
     *
     * @return string 
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set othernotes
     *
     * @param string $othernotes
     * @return Acquisiteur
     */
    public function setOthernotes($othernotes)
    {
        $this->othernotes = $othernotes;
    
        return $this;
    }

    /**
     * Get othernotes
     *
     * @return string 
     */
    public function getOthernotes()
    {
        return $this->othernotes;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Acquisiteur
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Acquisiteur
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set idMarginPaymentMode
     *
     * @param integer $idMarginPaymentMode
     * @return Acquisiteur
     */
    public function setIdMarginPaymentMode($idMarginPaymentMode)
    {
        $this->idMarginPaymentMode = $idMarginPaymentMode;
    
        return $this;
    }

    /**
     * Get idMarginPaymentMode
     *
     * @return integer 
     */
    public function getIdMarginPaymentMode()
    {
        return $this->idMarginPaymentMode;
    }

    /**
     * Set paymentNoTaxText
     *
     * @param integer $paymentNoTaxText
     * @return Acquisiteur
     */
    public function setPaymentNoTaxText($paymentNoTaxText)
    {
        $this->paymentNoTaxText = $paymentNoTaxText;
    
        return $this;
    }

    /**
     * Get paymentNoTaxText
     *
     * @return integer 
     */
    public function getPaymentNoTaxText()
    {
        return $this->paymentNoTaxText;
    }

    /**
     * Set isNotContacted
     *
     * @param boolean $isNotContacted
     * @return Acquisiteur
     */
    public function setIsNotContacted($isNotContacted)
    {
        $this->isNotContacted = $isNotContacted;
    
        return $this;
    }

    /**
     * Get isNotContacted
     *
     * @return boolean 
     */
    public function getIsNotContacted()
    {
        return $this->isNotContacted;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return Acquisiteur
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set accountingCode
     *
     * @param integer $accountingCode
     * @return Acquisiteur
     */
    public function setAccountingCode($accountingCode)
    {
        $this->accountingCode = $accountingCode;
    
        return $this;
    }

    /**
     * Get accountingCode
     *
     * @return integer 
     */
    public function getAccountingCode()
    {
        return $this->accountingCode;
    }

    /**
     * Set commissionPaymentWithTax
     *
     * @param string $commissionPaymentWithTax
     * @return Acquisiteur
     */
    public function setCommissionPaymentWithTax($commissionPaymentWithTax)
    {
        $this->commissionPaymentWithTax = $commissionPaymentWithTax;
    
        return $this;
    }

    /**
     * Get commissionPaymentWithTax
     *
     * @return string 
     */
    public function getCommissionPaymentWithTax()
    {
        return $this->commissionPaymentWithTax;
    }

    /**
     * Set isPaymentWithTax
     *
     * @param boolean $isPaymentWithTax
     * @return Acquisiteur
     */
    public function setIsPaymentWithTax($isPaymentWithTax)
    {
        $this->isPaymentWithTax = $isPaymentWithTax;
    
        return $this;
    }

    /**
     * Get isPaymentWithTax
     *
     * @return boolean 
     */
    public function getIsPaymentWithTax()
    {
        return $this->isPaymentWithTax;
    }
}