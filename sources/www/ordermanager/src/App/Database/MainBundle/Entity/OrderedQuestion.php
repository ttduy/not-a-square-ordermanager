<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderedQuestion
 *
 * @ORM\Table(name="ordered_question")
 * @ORM\Entity
 */
class OrderedQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idFormQuestion
     *
     * @ORM\Column(name="id_form_question", type="integer", nullable=true)
     */
    private $idFormQuestion;

    /**
     * @var string $answer
     *
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return OrderedQuestion
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idFormQuestion
     *
     * @param integer $idFormQuestion
     * @return OrderedQuestion
     */
    public function setIdFormQuestion($idFormQuestion)
    {
        $this->idFormQuestion = $idFormQuestion;
    
        return $this;
    }

    /**
     * Get idFormQuestion
     *
     * @return integer 
     */
    public function getIdFormQuestion()
    {
        return $this->idFormQuestion;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return OrderedQuestion
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}