<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ApplicationFont
 *
 * @ORM\Table(name="application_font")
 * @ORM\Entity
 */
class ApplicationFont
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $fontFile
     *
     * @ORM\Column(name="font_file", type="string", length=255, nullable=true)
     */
    private $fontFile;

    /**
     * @var string $tcpdfCode
     *
     * @ORM\Column(name="tcpdf_code", type="string", length=255, nullable=true)
     */
    private $tcpdfCode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ApplicationFont
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fontFile
     *
     * @param string $fontFile
     * @return ApplicationFont
     */
    public function setFontFile($fontFile)
    {
        $this->fontFile = $fontFile;
    
        return $this;
    }

    /**
     * Get fontFile
     *
     * @return string 
     */
    public function getFontFile()
    {
        return $this->fontFile;
    }

    /**
     * Set tcpdfCode
     *
     * @param string $tcpdfCode
     * @return ApplicationFont
     */
    public function setTcpdfCode($tcpdfCode)
    {
        $this->tcpdfCode = $tcpdfCode;
    
        return $this;
    }

    /**
     * Get tcpdfCode
     *
     * @return string 
     */
    public function getTcpdfCode()
    {
        return $this->tcpdfCode;
    }
}