<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\InvoiceSample
 *
 * @ORM\Table(name="invoice_sample")
 * @ORM\Entity
 */
class InvoiceSample
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idInvoice
     *
     * @ORM\Column(name="id_invoice", type="integer", nullable=true)
     */
    private $idInvoice;

    /**
     * @var integer $idMwaSample
     *
     * @ORM\Column(name="id_mwa_sample", type="integer", nullable=true)
     */
    private $idMwaSample;

    /**
     * @var \DateTime $priceDate
     *
     * @ORM\Column(name="price_date", type="date", nullable=true)
     */
    private $priceDate;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", nullable=true)
     */
    private $price;

    /**
     * @var float $overridePrice
     *
     * @ORM\Column(name="override_price", type="decimal", nullable=true)
     */
    private $overridePrice;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInvoice
     *
     * @param integer $idInvoice
     * @return InvoiceSample
     */
    public function setIdInvoice($idInvoice)
    {
        $this->idInvoice = $idInvoice;
    
        return $this;
    }

    /**
     * Get idInvoice
     *
     * @return integer 
     */
    public function getIdInvoice()
    {
        return $this->idInvoice;
    }

    /**
     * Set idMwaSample
     *
     * @param integer $idMwaSample
     * @return InvoiceSample
     */
    public function setIdMwaSample($idMwaSample)
    {
        $this->idMwaSample = $idMwaSample;
    
        return $this;
    }

    /**
     * Get idMwaSample
     *
     * @return integer 
     */
    public function getIdMwaSample()
    {
        return $this->idMwaSample;
    }

    /**
     * Set priceDate
     *
     * @param \DateTime $priceDate
     * @return InvoiceSample
     */
    public function setPriceDate($priceDate)
    {
        $this->priceDate = $priceDate;
    
        return $this;
    }

    /**
     * Get priceDate
     *
     * @return \DateTime 
     */
    public function getPriceDate()
    {
        return $this->priceDate;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return InvoiceSample
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set overridePrice
     *
     * @param float $overridePrice
     * @return InvoiceSample
     */
    public function setOverridePrice($overridePrice)
    {
        $this->overridePrice = $overridePrice;
    
        return $this;
    }

    /**
     * Get overridePrice
     *
     * @return float 
     */
    public function getOverridePrice()
    {
        return $this->overridePrice;
    }
}