<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormTemplateExpectedTokens
 *
 * @ORM\Table(name="scan_form_template_expected_tokens")
 * @ORM\Entity
 */
class ScanFormTemplateExpectedTokens
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormTemplate
     *
     * @ORM\Column(name="id_scan_form_template", type="integer", nullable=true)
     */
    private $idScanFormTemplate;

    /**
     * @var integer $idScanFormTemplateZone
     *
     * @ORM\Column(name="id_scan_form_template_zone", type="integer", nullable=true)
     */
    private $idScanFormTemplateZone;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=500, nullable=true)
     */
    private $image;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormTemplate
     *
     * @param integer $idScanFormTemplate
     * @return ScanFormTemplateExpectedTokens
     */
    public function setIdScanFormTemplate($idScanFormTemplate)
    {
        $this->idScanFormTemplate = $idScanFormTemplate;
    
        return $this;
    }

    /**
     * Get idScanFormTemplate
     *
     * @return integer 
     */
    public function getIdScanFormTemplate()
    {
        return $this->idScanFormTemplate;
    }

    /**
     * Set idScanFormTemplateZone
     *
     * @param integer $idScanFormTemplateZone
     * @return ScanFormTemplateExpectedTokens
     */
    public function setIdScanFormTemplateZone($idScanFormTemplateZone)
    {
        $this->idScanFormTemplateZone = $idScanFormTemplateZone;
    
        return $this;
    }

    /**
     * Get idScanFormTemplateZone
     *
     * @return integer 
     */
    public function getIdScanFormTemplateZone()
    {
        return $this->idScanFormTemplateZone;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ScanFormTemplateExpectedTokens
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}