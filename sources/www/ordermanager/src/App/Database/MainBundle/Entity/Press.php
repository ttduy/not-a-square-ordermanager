<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Press
 *
 * @ORM\Table(name="press")
 * @ORM\Entity
 */
class Press
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var integer $mailingList
     *
     * @ORM\Column(name="mailing_list", type="integer", nullable=true)
     */
    private $mailingList;

    /**
     * @var integer $type
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer $idContact
     *
     * @ORM\Column(name="id_contact", type="integer", nullable=true)
     */
    private $idContact;

    /**
     * @var boolean $isPress
     *
     * @ORM\Column(name="is_press", type="boolean", nullable=true)
     */
    private $isPress;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="domain", type="string", length=255, nullable=true)
     */
    private $domain;

    /**
     * @var string $mainFocus
     *
     * @ORM\Column(name="main_focus", type="string", length=255, nullable=true)
     */
    private $mainFocus;

    /**
     * @var boolean $isScientific
     *
     * @ORM\Column(name="is_scientific", type="boolean", nullable=true)
     */
    private $isScientific;

    /**
     * @var string $source
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var integer $idGender
     *
     * @ORM\Column(name="id_gender", type="integer", nullable=true)
     */
    private $idGender;

    /**
     * @var boolean $isUnisex
     *
     * @ORM\Column(name="is_unisex", type="boolean", nullable=true)
     */
    private $isUnisex;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $surName
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @var string $company
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string $publishingCompany
     *
     * @ORM\Column(name="publishing_company", type="string", length=255, nullable=true)
     */
    private $publishingCompany;

    /**
     * @var string $street
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postalCode
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string $region
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string $idCountry
     *
     * @ORM\Column(name="id_country", type="string", length=255, nullable=true)
     */
    private $idCountry;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $officePhone
     *
     * @ORM\Column(name="office_phone", type="string", length=255, nullable=true)
     */
    private $officePhone;

    /**
     * @var string $mobilePhone
     *
     * @ORM\Column(name="mobile_phone", type="string", length=255, nullable=true)
     */
    private $mobilePhone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var boolean $isOptedOut
     *
     * @ORM\Column(name="is_opted_out", type="boolean", nullable=true)
     */
    private $isOptedOut;

    /**
     * @var boolean $canBeContactedDirectly
     *
     * @ORM\Column(name="can_be_contacted_directly", type="boolean", nullable=true)
     */
    private $canBeContactedDirectly;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return Press
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set mailingList
     *
     * @param integer $mailingList
     * @return Press
     */
    public function setMailingList($mailingList)
    {
        $this->mailingList = $mailingList;
    
        return $this;
    }

    /**
     * Get mailingList
     *
     * @return integer 
     */
    public function getMailingList()
    {
        return $this->mailingList;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Press
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set idContact
     *
     * @param integer $idContact
     * @return Press
     */
    public function setIdContact($idContact)
    {
        $this->idContact = $idContact;
    
        return $this;
    }

    /**
     * Get idContact
     *
     * @return integer 
     */
    public function getIdContact()
    {
        return $this->idContact;
    }

    /**
     * Set isPress
     *
     * @param boolean $isPress
     * @return Press
     */
    public function setIsPress($isPress)
    {
        $this->isPress = $isPress;
    
        return $this;
    }

    /**
     * Get isPress
     *
     * @return boolean 
     */
    public function getIsPress()
    {
        return $this->isPress;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Press
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set mainFocus
     *
     * @param string $mainFocus
     * @return Press
     */
    public function setMainFocus($mainFocus)
    {
        $this->mainFocus = $mainFocus;
    
        return $this;
    }

    /**
     * Get mainFocus
     *
     * @return string 
     */
    public function getMainFocus()
    {
        return $this->mainFocus;
    }

    /**
     * Set isScientific
     *
     * @param boolean $isScientific
     * @return Press
     */
    public function setIsScientific($isScientific)
    {
        $this->isScientific = $isScientific;
    
        return $this;
    }

    /**
     * Get isScientific
     *
     * @return boolean 
     */
    public function getIsScientific()
    {
        return $this->isScientific;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Press
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set idGender
     *
     * @param integer $idGender
     * @return Press
     */
    public function setIdGender($idGender)
    {
        $this->idGender = $idGender;
    
        return $this;
    }

    /**
     * Get idGender
     *
     * @return integer 
     */
    public function getIdGender()
    {
        return $this->idGender;
    }

    /**
     * Set isUnisex
     *
     * @param boolean $isUnisex
     * @return Press
     */
    public function setIsUnisex($isUnisex)
    {
        $this->isUnisex = $isUnisex;
    
        return $this;
    }

    /**
     * Get isUnisex
     *
     * @return boolean 
     */
    public function getIsUnisex()
    {
        return $this->isUnisex;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Press
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Press
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surName
     *
     * @param string $surName
     * @return Press
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;
    
        return $this;
    }

    /**
     * Get surName
     *
     * @return string 
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Press
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set publishingCompany
     *
     * @param string $publishingCompany
     * @return Press
     */
    public function setPublishingCompany($publishingCompany)
    {
        $this->publishingCompany = $publishingCompany;
    
        return $this;
    }

    /**
     * Get publishingCompany
     *
     * @return string 
     */
    public function getPublishingCompany()
    {
        return $this->publishingCompany;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Press
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Press
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    
        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Press
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Press
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set idCountry
     *
     * @param string $idCountry
     * @return Press
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;
    
        return $this;
    }

    /**
     * Get idCountry
     *
     * @return string 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Press
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set officePhone
     *
     * @param string $officePhone
     * @return Press
     */
    public function setOfficePhone($officePhone)
    {
        $this->officePhone = $officePhone;
    
        return $this;
    }

    /**
     * Get officePhone
     *
     * @return string 
     */
    public function getOfficePhone()
    {
        return $this->officePhone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     * @return Press
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
    
        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string 
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Press
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Press
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set isOptedOut
     *
     * @param boolean $isOptedOut
     * @return Press
     */
    public function setIsOptedOut($isOptedOut)
    {
        $this->isOptedOut = $isOptedOut;
    
        return $this;
    }

    /**
     * Get isOptedOut
     *
     * @return boolean 
     */
    public function getIsOptedOut()
    {
        return $this->isOptedOut;
    }

    /**
     * Set canBeContactedDirectly
     *
     * @param boolean $canBeContactedDirectly
     * @return Press
     */
    public function setCanBeContactedDirectly($canBeContactedDirectly)
    {
        $this->canBeContactedDirectly = $canBeContactedDirectly;
    
        return $this;
    }

    /**
     * Get canBeContactedDirectly
     *
     * @return boolean 
     */
    public function getCanBeContactedDirectly()
    {
        return $this->canBeContactedDirectly;
    }
}