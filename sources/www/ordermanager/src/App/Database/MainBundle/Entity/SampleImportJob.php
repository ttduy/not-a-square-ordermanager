<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SampleImportJob
 *
 * @ORM\Table(name="sample_import_job")
 * @ORM\Entity
 */
class SampleImportJob
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idTask
     *
     * @ORM\Column(name="id_task", type="integer", nullable=true)
     */
    private $idTask;

    /**
     * @var string $sampleId
     *
     * @ORM\Column(name="sample_id", type="string", length=50, nullable=true)
     */
    private $sampleId;

    /**
     * @var integer $idNextAction
     *
     * @ORM\Column(name="id_next_action", type="integer", nullable=true)
     */
    private $idNextAction;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $lastUpdate
     *
     * @ORM\Column(name="last_update", type="text", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \DateTime $lastUpdateTimestamp
     *
     * @ORM\Column(name="last_update_timestamp", type="datetime", nullable=true)
     */
    private $lastUpdateTimestamp;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var integer $isSelected
     *
     * @ORM\Column(name="is_selected", type="integer", nullable=true)
     */
    private $isSelected;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTask
     *
     * @param integer $idTask
     * @return SampleImportJob
     */
    public function setIdTask($idTask)
    {
        $this->idTask = $idTask;
    
        return $this;
    }

    /**
     * Get idTask
     *
     * @return integer 
     */
    public function getIdTask()
    {
        return $this->idTask;
    }

    /**
     * Set sampleId
     *
     * @param string $sampleId
     * @return SampleImportJob
     */
    public function setSampleId($sampleId)
    {
        $this->sampleId = $sampleId;
    
        return $this;
    }

    /**
     * Get sampleId
     *
     * @return string 
     */
    public function getSampleId()
    {
        return $this->sampleId;
    }

    /**
     * Set idNextAction
     *
     * @param integer $idNextAction
     * @return SampleImportJob
     */
    public function setIdNextAction($idNextAction)
    {
        $this->idNextAction = $idNextAction;
    
        return $this;
    }

    /**
     * Get idNextAction
     *
     * @return integer 
     */
    public function getIdNextAction()
    {
        return $this->idNextAction;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SampleImportJob
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set lastUpdate
     *
     * @param string $lastUpdate
     * @return SampleImportJob
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    
        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return string 
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateTimestamp
     *
     * @param \DateTime $lastUpdateTimestamp
     * @return SampleImportJob
     */
    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;
    
        return $this;
    }

    /**
     * Get lastUpdateTimestamp
     *
     * @return \DateTime 
     */
    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return SampleImportJob
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set isSelected
     *
     * @param integer $isSelected
     * @return SampleImportJob
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;
    
        return $this;
    }

    /**
     * Get isSelected
     *
     * @return integer 
     */
    public function getIsSelected()
    {
        return $this->isSelected;
    }
}