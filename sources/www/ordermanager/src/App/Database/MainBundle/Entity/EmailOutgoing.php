<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailOutgoing
 *
 * @ORM\Table(name="email_outgoing")
 * @ORM\Entity
 */
class EmailOutgoing
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $idQueue
     *
     * @ORM\Column(name="id_queue", type="integer", nullable=true)
     */
    private $idQueue;

    /**
     * @var integer $idMassEmail
     *
     * @ORM\Column(name="id_mass_email", type="integer", nullable=true)
     */
    private $idMassEmail;

    /**
     * @var integer $idEmailMethod
     *
     * @ORM\Column(name="id_email_method", type="integer", nullable=true)
     */
    private $idEmailMethod;

    /**
     * @var string $recipients
     *
     * @ORM\Column(name="recipients", type="text", nullable=true)
     */
    private $recipients;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string $body
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var boolean $isSendNow
     *
     * @ORM\Column(name="is_send_now", type="boolean", nullable=true)
     */
    private $isSendNow;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $lastUpdated
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     */
    private $lastUpdated;

    /**
     * @var string $lastMessage
     *
     * @ORM\Column(name="last_message", type="text", nullable=true)
     */
    private $lastMessage;

    /**
     * @var integer $idMassSender
     *
     * @ORM\Column(name="id_mass_sender", type="integer", nullable=true)
     */
    private $idMassSender;

    /**
     * @var integer $recipientsType
     *
     * @ORM\Column(name="recipients_type", type="integer", nullable=true)
     */
    private $recipientsType;

    /**
     * @var integer $recipientsId
     *
     * @ORM\Column(name="recipients_id", type="integer", nullable=true)
     */
    private $recipientsId;

    /**
     * @var string $cc
     *
     * @ORM\Column(name="cc", type="text", nullable=true)
     */
    private $cc;

    /**
     * @var boolean $isAutoRetry
     *
     * @ORM\Column(name="is_auto_retry", type="boolean", nullable=true)
     */
    private $isAutoRetry;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return EmailOutgoing
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set idQueue
     *
     * @param integer $idQueue
     * @return EmailOutgoing
     */
    public function setIdQueue($idQueue)
    {
        $this->idQueue = $idQueue;
    
        return $this;
    }

    /**
     * Get idQueue
     *
     * @return integer 
     */
    public function getIdQueue()
    {
        return $this->idQueue;
    }

    /**
     * Set idMassEmail
     *
     * @param integer $idMassEmail
     * @return EmailOutgoing
     */
    public function setIdMassEmail($idMassEmail)
    {
        $this->idMassEmail = $idMassEmail;
    
        return $this;
    }

    /**
     * Get idMassEmail
     *
     * @return integer 
     */
    public function getIdMassEmail()
    {
        return $this->idMassEmail;
    }

    /**
     * Set idEmailMethod
     *
     * @param integer $idEmailMethod
     * @return EmailOutgoing
     */
    public function setIdEmailMethod($idEmailMethod)
    {
        $this->idEmailMethod = $idEmailMethod;
    
        return $this;
    }

    /**
     * Get idEmailMethod
     *
     * @return integer 
     */
    public function getIdEmailMethod()
    {
        return $this->idEmailMethod;
    }

    /**
     * Set recipients
     *
     * @param string $recipients
     * @return EmailOutgoing
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    
        return $this;
    }

    /**
     * Get recipients
     *
     * @return string 
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return EmailOutgoing
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return EmailOutgoing
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set isSendNow
     *
     * @param boolean $isSendNow
     * @return EmailOutgoing
     */
    public function setIsSendNow($isSendNow)
    {
        $this->isSendNow = $isSendNow;
    
        return $this;
    }

    /**
     * Get isSendNow
     *
     * @return boolean 
     */
    public function getIsSendNow()
    {
        return $this->isSendNow;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return EmailOutgoing
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     * @return EmailOutgoing
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    
        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime 
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * Set lastMessage
     *
     * @param string $lastMessage
     * @return EmailOutgoing
     */
    public function setLastMessage($lastMessage)
    {
        $this->lastMessage = $lastMessage;
    
        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return string 
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set idMassSender
     *
     * @param integer $idMassSender
     * @return EmailOutgoing
     */
    public function setIdMassSender($idMassSender)
    {
        $this->idMassSender = $idMassSender;
    
        return $this;
    }

    /**
     * Get idMassSender
     *
     * @return integer 
     */
    public function getIdMassSender()
    {
        return $this->idMassSender;
    }

    /**
     * Set recipientsType
     *
     * @param integer $recipientsType
     * @return EmailOutgoing
     */
    public function setRecipientsType($recipientsType)
    {
        $this->recipientsType = $recipientsType;
    
        return $this;
    }

    /**
     * Get recipientsType
     *
     * @return integer 
     */
    public function getRecipientsType()
    {
        return $this->recipientsType;
    }

    /**
     * Set recipientsId
     *
     * @param integer $recipientsId
     * @return EmailOutgoing
     */
    public function setRecipientsId($recipientsId)
    {
        $this->recipientsId = $recipientsId;
    
        return $this;
    }

    /**
     * Get recipientsId
     *
     * @return integer 
     */
    public function getRecipientsId()
    {
        return $this->recipientsId;
    }

    /**
     * Set cc
     *
     * @param string $cc
     * @return EmailOutgoing
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    
        return $this;
    }

    /**
     * Get cc
     *
     * @return string 
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set isAutoRetry
     *
     * @param boolean $isAutoRetry
     * @return EmailOutgoing
     */
    public function setIsAutoRetry($isAutoRetry)
    {
        $this->isAutoRetry = $isAutoRetry;
    
        return $this;
    }

    /**
     * Get isAutoRetry
     *
     * @return boolean 
     */
    public function getIsAutoRetry()
    {
        return $this->isAutoRetry;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return EmailOutgoing
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return EmailOutgoing
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
}