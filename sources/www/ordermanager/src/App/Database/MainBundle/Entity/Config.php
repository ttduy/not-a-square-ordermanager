<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity
 */
class Config
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $dateFormat
     *
     * @ORM\Column(name="date_format", type="string", length=255, nullable=true)
     */
    private $dateFormat;

    /**
     * @var string $customerImportUrl
     *
     * @ORM\Column(name="customer_import_url", type="text", nullable=true)
     */
    private $customerImportUrl;

    /**
     * @var string $customerImportUsername
     *
     * @ORM\Column(name="customer_import_username", type="string", length=255, nullable=true)
     */
    private $customerImportUsername;

    /**
     * @var string $customerImportPassword
     *
     * @ORM\Column(name="customer_import_password", type="string", length=255, nullable=true)
     */
    private $customerImportPassword;

    /**
     * @var integer $mwaInitialStatus
     *
     * @ORM\Column(name="mwa_initial_status", type="integer", nullable=true)
     */
    private $mwaInitialStatus;

    /**
     * @var string $mwaPushUrl
     *
     * @ORM\Column(name="mwa_push_url", type="text", nullable=true)
     */
    private $mwaPushUrl;

    /**
     * @var integer $mwaMessagePerSession
     *
     * @ORM\Column(name="mwa_message_per_session", type="integer", nullable=true)
     */
    private $mwaMessagePerSession;

    /**
     * @var float $defaultTax
     *
     * @ORM\Column(name="default_tax", type="decimal", nullable=true)
     */
    private $defaultTax;

    /**
     * @var string $mwaReceivedStatusArr
     *
     * @ORM\Column(name="mwa_received_status_arr", type="text", nullable=true)
     */
    private $mwaReceivedStatusArr;

    /**
     * @var string $mwaInprogressStatusArr
     *
     * @ORM\Column(name="mwa_inprogress_status_arr", type="text", nullable=true)
     */
    private $mwaInprogressStatusArr;

    /**
     * @var string $mwaCompletedStatusArr
     *
     * @ORM\Column(name="mwa_completed_status_arr", type="text", nullable=true)
     */
    private $mwaCompletedStatusArr;

    /**
     * @var string $materialExpirationNotificationEmail
     *
     * @ORM\Column(name="material_expiration_notification_email", type="text", nullable=true)
     */
    private $materialExpirationNotificationEmail;

    /**
     * @var string $mwaInvoiceYourUid
     *
     * @ORM\Column(name="mwa_invoice_your_uid", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceYourUid;

    /**
     * @var string $mwaInvoicePurchaseOrder
     *
     * @ORM\Column(name="mwa_invoice_purchase_order", type="string", length=255, nullable=true)
     */
    private $mwaInvoicePurchaseOrder;

    /**
     * @var string $mwaInvoiceSupplierNumber
     *
     * @ORM\Column(name="mwa_invoice_supplier_number", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceSupplierNumber;

    /**
     * @var string $mwaInvoiceProduct
     *
     * @ORM\Column(name="mwa_invoice_product", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceProduct;

    /**
     * @var float $amwayDefaultRevenue
     *
     * @ORM\Column(name="amway_default_revenue", type="decimal", nullable=false)
     */
    private $amwayDefaultRevenue;

    /**
     * @var float $amwayRussiaRevenue
     *
     * @ORM\Column(name="amway_russia_revenue", type="decimal", nullable=false)
     */
    private $amwayRussiaRevenue;

    /**
     * @var integer $retentionDay
     *
     * @ORM\Column(name="retention_day", type="integer", nullable=true)
     */
    private $retentionDay;

    /**
     * @var integer $idFormulaTemplateMwaSample
     *
     * @ORM\Column(name="id_formula_template_mwa_sample", type="integer", nullable=true)
     */
    private $idFormulaTemplateMwaSample;

    /**
     * @var string $lytechUrl
     *
     * @ORM\Column(name="lytech_url", type="string", length=255, nullable=true)
     */
    private $lytechUrl;

    /**
     * @var string $lytechUsername
     *
     * @ORM\Column(name="lytech_username", type="string", length=255, nullable=true)
     */
    private $lytechUsername;

    /**
     * @var string $lytechPassword
     *
     * @ORM\Column(name="lytech_password", type="string", length=255, nullable=true)
     */
    private $lytechPassword;

    /**
     * @var string $sampleImportJobMinute
     *
     * @ORM\Column(name="sample_import_job_minute", type="string", length=255, nullable=true)
     */
    private $sampleImportJobMinute;

    /**
     * @var string $strategyplanningcolorcode
     *
     * @ORM\Column(name="strategyPlanningColorCode", type="text", nullable=true)
     */
    private $strategyplanningcolorcode;

    /**
     * @var string $feedbackWelcomeMessage
     *
     * @ORM\Column(name="feedback_welcome_message", type="text", nullable=true)
     */
    private $feedbackWelcomeMessage;

    /**
     * @var string $customerColorCode
     *
     * @ORM\Column(name="customer_color_code", type="text", nullable=true)
     */
    private $customerColorCode;

    /**
     * @var string $bodfromcompany
     *
     * @ORM\Column(name="bodFromCompany", type="string", length=255, nullable=true)
     */
    private $bodfromcompany;

    /**
     * @var string $bodfromperson
     *
     * @ORM\Column(name="bodFromPerson", type="string", length=255, nullable=true)
     */
    private $bodfromperson;

    /**
     * @var string $bodfromemail
     *
     * @ORM\Column(name="bodFromEmail", type="string", length=255, nullable=true)
     */
    private $bodfromemail;

    /**
     * @var string $boditemtitle
     *
     * @ORM\Column(name="bodItemTitle", type="string", length=255, nullable=true)
     */
    private $boditemtitle;

    /**
     * @var string $bodcontributorrole
     *
     * @ORM\Column(name="bodContributorRole", type="string", length=255, nullable=true)
     */
    private $bodcontributorrole;

    /**
     * @var string $bodcontributorname
     *
     * @ORM\Column(name="bodContributorName", type="string", length=255, nullable=true)
     */
    private $bodcontributorname;

    /**
     * @var string $boditempaper
     *
     * @ORM\Column(name="bodItemPaper", type="string", length=255, nullable=true)
     */
    private $boditempaper;

    /**
     * @var string $boditembinding
     *
     * @ORM\Column(name="bodItemBinding", type="string", length=255, nullable=true)
     */
    private $boditembinding;

    /**
     * @var string $boditemback
     *
     * @ORM\Column(name="bodItemBack", type="string", length=255, nullable=true)
     */
    private $boditemback;

    /**
     * @var string $boditemfinish
     *
     * @ORM\Column(name="bodItemFinish", type="string", length=255, nullable=true)
     */
    private $boditemfinish;

    /**
     * @var string $boditemjacket
     *
     * @ORM\Column(name="bodItemJacket", type="string", length=255, nullable=true)
     */
    private $boditemjacket;

    /**
     * @var string $bodordercustomsvalue
     *
     * @ORM\Column(name="bodOrderCustomsValue", type="string", length=255, nullable=true)
     */
    private $bodordercustomsvalue;

    /**
     * @var string $bodordershipping
     *
     * @ORM\Column(name="bodOrderShipping", type="string", length=255, nullable=true)
     */
    private $bodordershipping;

    /**
     * @var integer $bodiddeliverynotereport
     *
     * @ORM\Column(name="bodIdDeliveryNoteReport", type="integer", nullable=true)
     */
    private $bodiddeliverynotereport;

    /**
     * @var integer $idDefaultShipmentEmail
     *
     * @ORM\Column(name="id_default_shipment_email", type="integer", nullable=true)
     */
    private $idDefaultShipmentEmail;

    /**
     * @var integer $idCryosaveOrderBaby111
     *
     * @ORM\Column(name="id_cryosave_order_baby111", type="integer", nullable=true)
     */
    private $idCryosaveOrderBaby111;

    /**
     * @var integer $idCryosaveOrderMilkome
     *
     * @ORM\Column(name="id_cryosave_order_milkome", type="integer", nullable=true)
     */
    private $idCryosaveOrderMilkome;

    /**
     * @var integer $idCryosaveOrderBloodom
     *
     * @ORM\Column(name="id_cryosave_order_bloodom", type="integer", nullable=true)
     */
    private $idCryosaveOrderBloodom;

    /**
     * @var integer $idNovoLaboratory
     *
     * @ORM\Column(name="id_novo_laboratory", type="integer", nullable=true)
     */
    private $idNovoLaboratory;

    /**
     * @var string $cryosaveFtpServer
     *
     * @ORM\Column(name="cryosave_ftp_server", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpServer;

    /**
     * @var string $cryosaveFtpUsername
     *
     * @ORM\Column(name="cryosave_ftp_username", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpUsername;

    /**
     * @var string $cryosaveFtpPassword
     *
     * @ORM\Column(name="cryosave_ftp_password", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpPassword;

    /**
     * @var string $bodFtpServer
     *
     * @ORM\Column(name="bod_ftp_server", type="string", length=255, nullable=true)
     */
    private $bodFtpServer;

    /**
     * @var string $bodFtpUsername
     *
     * @ORM\Column(name="bod_ftp_username", type="string", length=255, nullable=true)
     */
    private $bodFtpUsername;

    /**
     * @var string $bodFtpPassword
     *
     * @ORM\Column(name="bod_ftp_password", type="string", length=255, nullable=true)
     */
    private $bodFtpPassword;

    /**
     * @var integer $idBillingReportInvoiceCustomer
     *
     * @ORM\Column(name="id_billing_report_invoice_customer", type="integer", nullable=true)
     */
    private $idBillingReportInvoiceCustomer;

    /**
     * @var integer $idBillingReportInvoiceDistributionChannel
     *
     * @ORM\Column(name="id_billing_report_invoice_distribution_channel", type="integer", nullable=true)
     */
    private $idBillingReportInvoiceDistributionChannel;

    /**
     * @var integer $idBillingReportInvoicePartner
     *
     * @ORM\Column(name="id_billing_report_invoice_partner", type="integer", nullable=true)
     */
    private $idBillingReportInvoicePartner;

    /**
     * @var integer $idBillingReportPaymentDistributionChannel
     *
     * @ORM\Column(name="id_billing_report_payment_distribution_channel", type="integer", nullable=true)
     */
    private $idBillingReportPaymentDistributionChannel;

    /**
     * @var integer $idBillingReportPaymentPartner
     *
     * @ORM\Column(name="id_billing_report_payment_partner", type="integer", nullable=true)
     */
    private $idBillingReportPaymentPartner;

    /**
     * @var integer $idBillingReportPaymentAcquisiteur
     *
     * @ORM\Column(name="id_billing_report_payment_acquisiteur", type="integer", nullable=true)
     */
    private $idBillingReportPaymentAcquisiteur;

    /**
     * @var integer $idBillingReportReminderNotice
     *
     * @ORM\Column(name="id_billing_report_reminder_notice", type="integer", nullable=true)
     */
    private $idBillingReportReminderNotice;

    /**
     * @var \DateTime $billingBaselineDate
     *
     * @ORM\Column(name="billing_baseline_date", type="date", nullable=true)
     */
    private $billingBaselineDate;

    /**
     * @var string $nutriMeProducts
     *
     * @ORM\Column(name="nutri_me_products", type="text", nullable=true)
     */
    private $nutriMeProducts;

    /**
     * @var integer $idBillingReportPaymentDistributionChannelInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_distribution_channel_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentDistributionChannelInvoice;

    /**
     * @var integer $idBillingReportPaymentPartnerInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_partner_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentPartnerInvoice;

    /**
     * @var integer $idBillingReportPaymentAcquisiteurInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_acquisiteur_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentAcquisiteurInvoice;

    /**
     * @var string $billingText
     *
     * @ORM\Column(name="billing_text", type="text", nullable=true)
     */
    private $billingText;

    /**
     * @var integer $reportAvailableDefaultXDays
     *
     * @ORM\Column(name="report_available_default_x_days", type="integer", nullable=true)
     */
    private $reportAvailableDefaultXDays;

    /**
     * @var integer $defaultWebLoginEmailPartner
     *
     * @ORM\Column(name="default_web_login_email_partner", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailPartner;

    /**
     * @var integer $defaultWebLoginEmailDistributionChannel
     *
     * @ORM\Column(name="default_web_login_email_distribution_channel", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailDistributionChannel;

    /**
     * @var integer $defaultWebLoginEmailCustomerInfo
     *
     * @ORM\Column(name="default_web_login_email_customer_info", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailCustomerInfo;

    /**
     * @var integer $customerLeadDefaultStatus
     *
     * @ORM\Column(name="customer_lead_default_status", type="integer", nullable=true)
     */
    private $customerLeadDefaultStatus;

    /**
     * @var string $customerDashboardLoginUrl
     *
     * @ORM\Column(name="customer_dashboard_login_url", type="string", length=255, nullable=true)
     */
    private $customerDashboardLoginUrl;

    /**
     * @var string $shopOrderNutrimeBasedOnYourGeneUrl
     *
     * @ORM\Column(name="shop_order_nutrime_based_on_your_gene_url", type="string", length=255, nullable=true)
     */
    private $shopOrderNutrimeBasedOnYourGeneUrl;

    /**
     * @var string $shopOrderPrintedBookletUrl
     *
     * @ORM\Column(name="shop_order_printed_booklet_url", type="string", length=255, nullable=true)
     */
    private $shopOrderPrintedBookletUrl;

    /**
     * @var string $massMailerHost
     *
     * @ORM\Column(name="mass_mailer_host", type="string", length=255, nullable=true)
     */
    private $massMailerHost;

    /**
     * @var integer $massMailerPort
     *
     * @ORM\Column(name="mass_mailer_port", type="integer", nullable=true)
     */
    private $massMailerPort;

    /**
     * @var string $massMailerEncryption
     *
     * @ORM\Column(name="mass_mailer_encryption", type="string", length=10, nullable=true)
     */
    private $massMailerEncryption;

    /**
     * @var string $massMailerUsername
     *
     * @ORM\Column(name="mass_mailer_username", type="string", length=255, nullable=true)
     */
    private $massMailerUsername;

    /**
     * @var string $massMailerPassword
     *
     * @ORM\Column(name="mass_mailer_password", type="string", length=255, nullable=true)
     */
    private $massMailerPassword;

    /**
     * @var string $massMailerSenderName
     *
     * @ORM\Column(name="mass_mailer_sender_name", type="string", length=255, nullable=true)
     */
    private $massMailerSenderName;

    /**
     * @var string $massMailerSenderEmail
     *
     * @ORM\Column(name="mass_mailer_sender_email", type="string", length=255, nullable=true)
     */
    private $massMailerSenderEmail;

    /**
     * @var string $massMailerRate
     *
     * @ORM\Column(name="mass_mailer_rate", type="string", length=255, nullable=true)
     */
    private $massMailerRate;

    /**
     * @var string $mandrillApiKey
     *
     * @ORM\Column(name="mandrill_api_key", type="string", length=255, nullable=true)
     */
    private $mandrillApiKey;

    /**
     * @var string $awsAccessKeyId
     *
     * @ORM\Column(name="aws_access_key_id", type="string", length=255, nullable=true)
     */
    private $awsAccessKeyId;

    /**
     * @var string $awsSecretAccessKey
     *
     * @ORM\Column(name="aws_secret_access_key", type="string", length=255, nullable=true)
     */
    private $awsSecretAccessKey;

    /**
     * @var string $awsRegion
     *
     * @ORM\Column(name="aws_region", type="string", length=255, nullable=true)
     */
    private $awsRegion;

    /**
     * @var string $customerDashboardIdTextBlockGroup
     *
     * @ORM\Column(name="customer_dashboard_id_text_block_group", type="text", nullable=true)
     */
    private $customerDashboardIdTextBlockGroup;

    /**
     * @var string $customerDashboardLanguages
     *
     * @ORM\Column(name="customer_dashboard_languages", type="text", nullable=true)
     */
    private $customerDashboardLanguages;

    /**
     * @var integer $customerDashboardLoginEmailTemplate
     *
     * @ORM\Column(name="customer_dashboard_login_email_template", type="integer", nullable=true)
     */
    private $customerDashboardLoginEmailTemplate;

    /**
     * @var string $unlockPassword
     *
     * @ORM\Column(name="unlock_password", type="string", length=255, nullable=true)
     */
    private $unlockPassword;

    /**
     * @var string $dcMarketingStatusReminderLimitPassedDays
     *
     * @ORM\Column(name="dc_marketing_status_reminder_limit_passed_days", type="string", length=255, nullable=true)
     */
    private $dcMarketingStatusReminderLimitPassedDays;

    /**
     * @var string $defaultRatingDistributionChannel
     *
     * @ORM\Column(name="default_rating_distribution_channel", type="text", nullable=true)
     */
    private $defaultRatingDistributionChannel;

    /**
     * @var string $defaultRatingPartner
     *
     * @ORM\Column(name="default_rating_partner", type="text", nullable=true)
     */
    private $defaultRatingPartner;

    /**
     * @var string $defaultRatingAcquisiteur
     *
     * @ORM\Column(name="default_rating_acquisiteur", type="text", nullable=true)
     */
    private $defaultRatingAcquisiteur;

    /**
     * @var integer $orderBackupDateDuration
     *
     * @ORM\Column(name="order_backup_date_duration", type="integer", nullable=true)
     */
    private $orderBackupDateDuration;

    /**
     * @var string $untranslatedPattern
     *
     * @ORM\Column(name="untranslated_pattern", type="text", nullable=true)
     */
    private $untranslatedPattern;

    /**
     * @var string $cmsContactMessageNotificationEmailList
     *
     * @ORM\Column(name="cms_contact_message_notification_email_list", type="string", length=255, nullable=true)
     */
    private $cmsContactMessageNotificationEmailList;

    /**
     * @var string $emailWebLoginFilterConfig
     *
     * @ORM\Column(name="email_web_login_filter_config", type="text", nullable=true)
     */
    private $emailWebLoginFilterConfig;

    /**
     * @var integer $genoBatchPrinterReportTemplateId
     *
     * @ORM\Column(name="geno_batch_printer_report_template_id", type="integer", nullable=true)
     */
    private $genoBatchPrinterReportTemplateId;

    /**
     * @var integer $offsetCustomerPricing
     *
     * @ORM\Column(name="offset_customer_pricing", type="integer", nullable=true)
     */
    private $offsetCustomerPricing;

    /**
     * @var string $configCustomerPricing
     *
     * @ORM\Column(name="config_customer_pricing", type="text", nullable=true)
     */
    private $configCustomerPricing;

    /**
     * @var string $lastEmailWebloginOrderedStatus
     *
     * @ORM\Column(name="last_email_weblogin_ordered_status", type="text", nullable=true)
     */
    private $lastEmailWebloginOrderedStatus;

    /**
     * @var integer $emailMethodCmsOrder
     *
     * @ORM\Column(name="email_method_cms_order", type="integer", nullable=true)
     */
    private $emailMethodCmsOrder;

    /**
     * @var integer $emailMethodCustomerDashboard
     *
     * @ORM\Column(name="email_method_customer_dashboard", type="integer", nullable=true)
     */
    private $emailMethodCustomerDashboard;

    /**
     * @var string $salesAnalysisFilterConfig
     *
     * @ORM\Column(name="sales_analysis_filter_config", type="text", nullable=true)
     */
    private $salesAnalysisFilterConfig;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateFormat
     *
     * @param string $dateFormat
     * @return Config
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;
    
        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return string 
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set customerImportUrl
     *
     * @param string $customerImportUrl
     * @return Config
     */
    public function setCustomerImportUrl($customerImportUrl)
    {
        $this->customerImportUrl = $customerImportUrl;
    
        return $this;
    }

    /**
     * Get customerImportUrl
     *
     * @return string 
     */
    public function getCustomerImportUrl()
    {
        return $this->customerImportUrl;
    }

    /**
     * Set customerImportUsername
     *
     * @param string $customerImportUsername
     * @return Config
     */
    public function setCustomerImportUsername($customerImportUsername)
    {
        $this->customerImportUsername = $customerImportUsername;
    
        return $this;
    }

    /**
     * Get customerImportUsername
     *
     * @return string 
     */
    public function getCustomerImportUsername()
    {
        return $this->customerImportUsername;
    }

    /**
     * Set customerImportPassword
     *
     * @param string $customerImportPassword
     * @return Config
     */
    public function setCustomerImportPassword($customerImportPassword)
    {
        $this->customerImportPassword = $customerImportPassword;
    
        return $this;
    }

    /**
     * Get customerImportPassword
     *
     * @return string 
     */
    public function getCustomerImportPassword()
    {
        return $this->customerImportPassword;
    }

    /**
     * Set mwaInitialStatus
     *
     * @param integer $mwaInitialStatus
     * @return Config
     */
    public function setMwaInitialStatus($mwaInitialStatus)
    {
        $this->mwaInitialStatus = $mwaInitialStatus;
    
        return $this;
    }

    /**
     * Get mwaInitialStatus
     *
     * @return integer 
     */
    public function getMwaInitialStatus()
    {
        return $this->mwaInitialStatus;
    }

    /**
     * Set mwaPushUrl
     *
     * @param string $mwaPushUrl
     * @return Config
     */
    public function setMwaPushUrl($mwaPushUrl)
    {
        $this->mwaPushUrl = $mwaPushUrl;
    
        return $this;
    }

    /**
     * Get mwaPushUrl
     *
     * @return string 
     */
    public function getMwaPushUrl()
    {
        return $this->mwaPushUrl;
    }

    /**
     * Set mwaMessagePerSession
     *
     * @param integer $mwaMessagePerSession
     * @return Config
     */
    public function setMwaMessagePerSession($mwaMessagePerSession)
    {
        $this->mwaMessagePerSession = $mwaMessagePerSession;
    
        return $this;
    }

    /**
     * Get mwaMessagePerSession
     *
     * @return integer 
     */
    public function getMwaMessagePerSession()
    {
        return $this->mwaMessagePerSession;
    }

    /**
     * Set defaultTax
     *
     * @param float $defaultTax
     * @return Config
     */
    public function setDefaultTax($defaultTax)
    {
        $this->defaultTax = $defaultTax;
    
        return $this;
    }

    /**
     * Get defaultTax
     *
     * @return float 
     */
    public function getDefaultTax()
    {
        return $this->defaultTax;
    }

    /**
     * Set mwaReceivedStatusArr
     *
     * @param string $mwaReceivedStatusArr
     * @return Config
     */
    public function setMwaReceivedStatusArr($mwaReceivedStatusArr)
    {
        $this->mwaReceivedStatusArr = $mwaReceivedStatusArr;
    
        return $this;
    }

    /**
     * Get mwaReceivedStatusArr
     *
     * @return string 
     */
    public function getMwaReceivedStatusArr()
    {
        return $this->mwaReceivedStatusArr;
    }

    /**
     * Set mwaInprogressStatusArr
     *
     * @param string $mwaInprogressStatusArr
     * @return Config
     */
    public function setMwaInprogressStatusArr($mwaInprogressStatusArr)
    {
        $this->mwaInprogressStatusArr = $mwaInprogressStatusArr;
    
        return $this;
    }

    /**
     * Get mwaInprogressStatusArr
     *
     * @return string 
     */
    public function getMwaInprogressStatusArr()
    {
        return $this->mwaInprogressStatusArr;
    }

    /**
     * Set mwaCompletedStatusArr
     *
     * @param string $mwaCompletedStatusArr
     * @return Config
     */
    public function setMwaCompletedStatusArr($mwaCompletedStatusArr)
    {
        $this->mwaCompletedStatusArr = $mwaCompletedStatusArr;
    
        return $this;
    }

    /**
     * Get mwaCompletedStatusArr
     *
     * @return string 
     */
    public function getMwaCompletedStatusArr()
    {
        return $this->mwaCompletedStatusArr;
    }

    /**
     * Set materialExpirationNotificationEmail
     *
     * @param string $materialExpirationNotificationEmail
     * @return Config
     */
    public function setMaterialExpirationNotificationEmail($materialExpirationNotificationEmail)
    {
        $this->materialExpirationNotificationEmail = $materialExpirationNotificationEmail;
    
        return $this;
    }

    /**
     * Get materialExpirationNotificationEmail
     *
     * @return string 
     */
    public function getMaterialExpirationNotificationEmail()
    {
        return $this->materialExpirationNotificationEmail;
    }

    /**
     * Set mwaInvoiceYourUid
     *
     * @param string $mwaInvoiceYourUid
     * @return Config
     */
    public function setMwaInvoiceYourUid($mwaInvoiceYourUid)
    {
        $this->mwaInvoiceYourUid = $mwaInvoiceYourUid;
    
        return $this;
    }

    /**
     * Get mwaInvoiceYourUid
     *
     * @return string 
     */
    public function getMwaInvoiceYourUid()
    {
        return $this->mwaInvoiceYourUid;
    }

    /**
     * Set mwaInvoicePurchaseOrder
     *
     * @param string $mwaInvoicePurchaseOrder
     * @return Config
     */
    public function setMwaInvoicePurchaseOrder($mwaInvoicePurchaseOrder)
    {
        $this->mwaInvoicePurchaseOrder = $mwaInvoicePurchaseOrder;
    
        return $this;
    }

    /**
     * Get mwaInvoicePurchaseOrder
     *
     * @return string 
     */
    public function getMwaInvoicePurchaseOrder()
    {
        return $this->mwaInvoicePurchaseOrder;
    }

    /**
     * Set mwaInvoiceSupplierNumber
     *
     * @param string $mwaInvoiceSupplierNumber
     * @return Config
     */
    public function setMwaInvoiceSupplierNumber($mwaInvoiceSupplierNumber)
    {
        $this->mwaInvoiceSupplierNumber = $mwaInvoiceSupplierNumber;
    
        return $this;
    }

    /**
     * Get mwaInvoiceSupplierNumber
     *
     * @return string 
     */
    public function getMwaInvoiceSupplierNumber()
    {
        return $this->mwaInvoiceSupplierNumber;
    }

    /**
     * Set mwaInvoiceProduct
     *
     * @param string $mwaInvoiceProduct
     * @return Config
     */
    public function setMwaInvoiceProduct($mwaInvoiceProduct)
    {
        $this->mwaInvoiceProduct = $mwaInvoiceProduct;
    
        return $this;
    }

    /**
     * Get mwaInvoiceProduct
     *
     * @return string 
     */
    public function getMwaInvoiceProduct()
    {
        return $this->mwaInvoiceProduct;
    }

    /**
     * Set amwayDefaultRevenue
     *
     * @param float $amwayDefaultRevenue
     * @return Config
     */
    public function setAmwayDefaultRevenue($amwayDefaultRevenue)
    {
        $this->amwayDefaultRevenue = $amwayDefaultRevenue;
    
        return $this;
    }

    /**
     * Get amwayDefaultRevenue
     *
     * @return float 
     */
    public function getAmwayDefaultRevenue()
    {
        return $this->amwayDefaultRevenue;
    }

    /**
     * Set amwayRussiaRevenue
     *
     * @param float $amwayRussiaRevenue
     * @return Config
     */
    public function setAmwayRussiaRevenue($amwayRussiaRevenue)
    {
        $this->amwayRussiaRevenue = $amwayRussiaRevenue;
    
        return $this;
    }

    /**
     * Get amwayRussiaRevenue
     *
     * @return float 
     */
    public function getAmwayRussiaRevenue()
    {
        return $this->amwayRussiaRevenue;
    }

    /**
     * Set retentionDay
     *
     * @param integer $retentionDay
     * @return Config
     */
    public function setRetentionDay($retentionDay)
    {
        $this->retentionDay = $retentionDay;
    
        return $this;
    }

    /**
     * Get retentionDay
     *
     * @return integer 
     */
    public function getRetentionDay()
    {
        return $this->retentionDay;
    }

    /**
     * Set idFormulaTemplateMwaSample
     *
     * @param integer $idFormulaTemplateMwaSample
     * @return Config
     */
    public function setIdFormulaTemplateMwaSample($idFormulaTemplateMwaSample)
    {
        $this->idFormulaTemplateMwaSample = $idFormulaTemplateMwaSample;
    
        return $this;
    }

    /**
     * Get idFormulaTemplateMwaSample
     *
     * @return integer 
     */
    public function getIdFormulaTemplateMwaSample()
    {
        return $this->idFormulaTemplateMwaSample;
    }

    /**
     * Set lytechUrl
     *
     * @param string $lytechUrl
     * @return Config
     */
    public function setLytechUrl($lytechUrl)
    {
        $this->lytechUrl = $lytechUrl;
    
        return $this;
    }

    /**
     * Get lytechUrl
     *
     * @return string 
     */
    public function getLytechUrl()
    {
        return $this->lytechUrl;
    }

    /**
     * Set lytechUsername
     *
     * @param string $lytechUsername
     * @return Config
     */
    public function setLytechUsername($lytechUsername)
    {
        $this->lytechUsername = $lytechUsername;
    
        return $this;
    }

    /**
     * Get lytechUsername
     *
     * @return string 
     */
    public function getLytechUsername()
    {
        return $this->lytechUsername;
    }

    /**
     * Set lytechPassword
     *
     * @param string $lytechPassword
     * @return Config
     */
    public function setLytechPassword($lytechPassword)
    {
        $this->lytechPassword = $lytechPassword;
    
        return $this;
    }

    /**
     * Get lytechPassword
     *
     * @return string 
     */
    public function getLytechPassword()
    {
        return $this->lytechPassword;
    }

    /**
     * Set sampleImportJobMinute
     *
     * @param string $sampleImportJobMinute
     * @return Config
     */
    public function setSampleImportJobMinute($sampleImportJobMinute)
    {
        $this->sampleImportJobMinute = $sampleImportJobMinute;
    
        return $this;
    }

    /**
     * Get sampleImportJobMinute
     *
     * @return string 
     */
    public function getSampleImportJobMinute()
    {
        return $this->sampleImportJobMinute;
    }

    /**
     * Set strategyplanningcolorcode
     *
     * @param string $strategyplanningcolorcode
     * @return Config
     */
    public function setStrategyplanningcolorcode($strategyplanningcolorcode)
    {
        $this->strategyplanningcolorcode = $strategyplanningcolorcode;
    
        return $this;
    }

    /**
     * Get strategyplanningcolorcode
     *
     * @return string 
     */
    public function getStrategyplanningcolorcode()
    {
        return $this->strategyplanningcolorcode;
    }

    /**
     * Set feedbackWelcomeMessage
     *
     * @param string $feedbackWelcomeMessage
     * @return Config
     */
    public function setFeedbackWelcomeMessage($feedbackWelcomeMessage)
    {
        $this->feedbackWelcomeMessage = $feedbackWelcomeMessage;
    
        return $this;
    }

    /**
     * Get feedbackWelcomeMessage
     *
     * @return string 
     */
    public function getFeedbackWelcomeMessage()
    {
        return $this->feedbackWelcomeMessage;
    }

    /**
     * Set customerColorCode
     *
     * @param string $customerColorCode
     * @return Config
     */
    public function setCustomerColorCode($customerColorCode)
    {
        $this->customerColorCode = $customerColorCode;
    
        return $this;
    }

    /**
     * Get customerColorCode
     *
     * @return string 
     */
    public function getCustomerColorCode()
    {
        return $this->customerColorCode;
    }

    /**
     * Set bodfromcompany
     *
     * @param string $bodfromcompany
     * @return Config
     */
    public function setBodfromcompany($bodfromcompany)
    {
        $this->bodfromcompany = $bodfromcompany;
    
        return $this;
    }

    /**
     * Get bodfromcompany
     *
     * @return string 
     */
    public function getBodfromcompany()
    {
        return $this->bodfromcompany;
    }

    /**
     * Set bodfromperson
     *
     * @param string $bodfromperson
     * @return Config
     */
    public function setBodfromperson($bodfromperson)
    {
        $this->bodfromperson = $bodfromperson;
    
        return $this;
    }

    /**
     * Get bodfromperson
     *
     * @return string 
     */
    public function getBodfromperson()
    {
        return $this->bodfromperson;
    }

    /**
     * Set bodfromemail
     *
     * @param string $bodfromemail
     * @return Config
     */
    public function setBodfromemail($bodfromemail)
    {
        $this->bodfromemail = $bodfromemail;
    
        return $this;
    }

    /**
     * Get bodfromemail
     *
     * @return string 
     */
    public function getBodfromemail()
    {
        return $this->bodfromemail;
    }

    /**
     * Set boditemtitle
     *
     * @param string $boditemtitle
     * @return Config
     */
    public function setBoditemtitle($boditemtitle)
    {
        $this->boditemtitle = $boditemtitle;
    
        return $this;
    }

    /**
     * Get boditemtitle
     *
     * @return string 
     */
    public function getBoditemtitle()
    {
        return $this->boditemtitle;
    }

    /**
     * Set bodcontributorrole
     *
     * @param string $bodcontributorrole
     * @return Config
     */
    public function setBodcontributorrole($bodcontributorrole)
    {
        $this->bodcontributorrole = $bodcontributorrole;
    
        return $this;
    }

    /**
     * Get bodcontributorrole
     *
     * @return string 
     */
    public function getBodcontributorrole()
    {
        return $this->bodcontributorrole;
    }

    /**
     * Set bodcontributorname
     *
     * @param string $bodcontributorname
     * @return Config
     */
    public function setBodcontributorname($bodcontributorname)
    {
        $this->bodcontributorname = $bodcontributorname;
    
        return $this;
    }

    /**
     * Get bodcontributorname
     *
     * @return string 
     */
    public function getBodcontributorname()
    {
        return $this->bodcontributorname;
    }

    /**
     * Set boditempaper
     *
     * @param string $boditempaper
     * @return Config
     */
    public function setBoditempaper($boditempaper)
    {
        $this->boditempaper = $boditempaper;
    
        return $this;
    }

    /**
     * Get boditempaper
     *
     * @return string 
     */
    public function getBoditempaper()
    {
        return $this->boditempaper;
    }

    /**
     * Set boditembinding
     *
     * @param string $boditembinding
     * @return Config
     */
    public function setBoditembinding($boditembinding)
    {
        $this->boditembinding = $boditembinding;
    
        return $this;
    }

    /**
     * Get boditembinding
     *
     * @return string 
     */
    public function getBoditembinding()
    {
        return $this->boditembinding;
    }

    /**
     * Set boditemback
     *
     * @param string $boditemback
     * @return Config
     */
    public function setBoditemback($boditemback)
    {
        $this->boditemback = $boditemback;
    
        return $this;
    }

    /**
     * Get boditemback
     *
     * @return string 
     */
    public function getBoditemback()
    {
        return $this->boditemback;
    }

    /**
     * Set boditemfinish
     *
     * @param string $boditemfinish
     * @return Config
     */
    public function setBoditemfinish($boditemfinish)
    {
        $this->boditemfinish = $boditemfinish;
    
        return $this;
    }

    /**
     * Get boditemfinish
     *
     * @return string 
     */
    public function getBoditemfinish()
    {
        return $this->boditemfinish;
    }

    /**
     * Set boditemjacket
     *
     * @param string $boditemjacket
     * @return Config
     */
    public function setBoditemjacket($boditemjacket)
    {
        $this->boditemjacket = $boditemjacket;
    
        return $this;
    }

    /**
     * Get boditemjacket
     *
     * @return string 
     */
    public function getBoditemjacket()
    {
        return $this->boditemjacket;
    }

    /**
     * Set bodordercustomsvalue
     *
     * @param string $bodordercustomsvalue
     * @return Config
     */
    public function setBodordercustomsvalue($bodordercustomsvalue)
    {
        $this->bodordercustomsvalue = $bodordercustomsvalue;
    
        return $this;
    }

    /**
     * Get bodordercustomsvalue
     *
     * @return string 
     */
    public function getBodordercustomsvalue()
    {
        return $this->bodordercustomsvalue;
    }

    /**
     * Set bodordershipping
     *
     * @param string $bodordershipping
     * @return Config
     */
    public function setBodordershipping($bodordershipping)
    {
        $this->bodordershipping = $bodordershipping;
    
        return $this;
    }

    /**
     * Get bodordershipping
     *
     * @return string 
     */
    public function getBodordershipping()
    {
        return $this->bodordershipping;
    }

    /**
     * Set bodiddeliverynotereport
     *
     * @param integer $bodiddeliverynotereport
     * @return Config
     */
    public function setBodiddeliverynotereport($bodiddeliverynotereport)
    {
        $this->bodiddeliverynotereport = $bodiddeliverynotereport;
    
        return $this;
    }

    /**
     * Get bodiddeliverynotereport
     *
     * @return integer 
     */
    public function getBodiddeliverynotereport()
    {
        return $this->bodiddeliverynotereport;
    }

    /**
     * Set idDefaultShipmentEmail
     *
     * @param integer $idDefaultShipmentEmail
     * @return Config
     */
    public function setIdDefaultShipmentEmail($idDefaultShipmentEmail)
    {
        $this->idDefaultShipmentEmail = $idDefaultShipmentEmail;
    
        return $this;
    }

    /**
     * Get idDefaultShipmentEmail
     *
     * @return integer 
     */
    public function getIdDefaultShipmentEmail()
    {
        return $this->idDefaultShipmentEmail;
    }

    /**
     * Set idCryosaveOrderBaby111
     *
     * @param integer $idCryosaveOrderBaby111
     * @return Config
     */
    public function setIdCryosaveOrderBaby111($idCryosaveOrderBaby111)
    {
        $this->idCryosaveOrderBaby111 = $idCryosaveOrderBaby111;
    
        return $this;
    }

    /**
     * Get idCryosaveOrderBaby111
     *
     * @return integer 
     */
    public function getIdCryosaveOrderBaby111()
    {
        return $this->idCryosaveOrderBaby111;
    }

    /**
     * Set idCryosaveOrderMilkome
     *
     * @param integer $idCryosaveOrderMilkome
     * @return Config
     */
    public function setIdCryosaveOrderMilkome($idCryosaveOrderMilkome)
    {
        $this->idCryosaveOrderMilkome = $idCryosaveOrderMilkome;
    
        return $this;
    }

    /**
     * Get idCryosaveOrderMilkome
     *
     * @return integer 
     */
    public function getIdCryosaveOrderMilkome()
    {
        return $this->idCryosaveOrderMilkome;
    }

    /**
     * Set idCryosaveOrderBloodom
     *
     * @param integer $idCryosaveOrderBloodom
     * @return Config
     */
    public function setIdCryosaveOrderBloodom($idCryosaveOrderBloodom)
    {
        $this->idCryosaveOrderBloodom = $idCryosaveOrderBloodom;
    
        return $this;
    }

    /**
     * Get idCryosaveOrderBloodom
     *
     * @return integer 
     */
    public function getIdCryosaveOrderBloodom()
    {
        return $this->idCryosaveOrderBloodom;
    }

    /**
     * Set idNovoLaboratory
     *
     * @param integer $idNovoLaboratory
     * @return Config
     */
    public function setIdNovoLaboratory($idNovoLaboratory)
    {
        $this->idNovoLaboratory = $idNovoLaboratory;
    
        return $this;
    }

    /**
     * Get idNovoLaboratory
     *
     * @return integer 
     */
    public function getIdNovoLaboratory()
    {
        return $this->idNovoLaboratory;
    }

    /**
     * Set cryosaveFtpServer
     *
     * @param string $cryosaveFtpServer
     * @return Config
     */
    public function setCryosaveFtpServer($cryosaveFtpServer)
    {
        $this->cryosaveFtpServer = $cryosaveFtpServer;
    
        return $this;
    }

    /**
     * Get cryosaveFtpServer
     *
     * @return string 
     */
    public function getCryosaveFtpServer()
    {
        return $this->cryosaveFtpServer;
    }

    /**
     * Set cryosaveFtpUsername
     *
     * @param string $cryosaveFtpUsername
     * @return Config
     */
    public function setCryosaveFtpUsername($cryosaveFtpUsername)
    {
        $this->cryosaveFtpUsername = $cryosaveFtpUsername;
    
        return $this;
    }

    /**
     * Get cryosaveFtpUsername
     *
     * @return string 
     */
    public function getCryosaveFtpUsername()
    {
        return $this->cryosaveFtpUsername;
    }

    /**
     * Set cryosaveFtpPassword
     *
     * @param string $cryosaveFtpPassword
     * @return Config
     */
    public function setCryosaveFtpPassword($cryosaveFtpPassword)
    {
        $this->cryosaveFtpPassword = $cryosaveFtpPassword;
    
        return $this;
    }

    /**
     * Get cryosaveFtpPassword
     *
     * @return string 
     */
    public function getCryosaveFtpPassword()
    {
        return $this->cryosaveFtpPassword;
    }

    /**
     * Set bodFtpServer
     *
     * @param string $bodFtpServer
     * @return Config
     */
    public function setBodFtpServer($bodFtpServer)
    {
        $this->bodFtpServer = $bodFtpServer;
    
        return $this;
    }

    /**
     * Get bodFtpServer
     *
     * @return string 
     */
    public function getBodFtpServer()
    {
        return $this->bodFtpServer;
    }

    /**
     * Set bodFtpUsername
     *
     * @param string $bodFtpUsername
     * @return Config
     */
    public function setBodFtpUsername($bodFtpUsername)
    {
        $this->bodFtpUsername = $bodFtpUsername;
    
        return $this;
    }

    /**
     * Get bodFtpUsername
     *
     * @return string 
     */
    public function getBodFtpUsername()
    {
        return $this->bodFtpUsername;
    }

    /**
     * Set bodFtpPassword
     *
     * @param string $bodFtpPassword
     * @return Config
     */
    public function setBodFtpPassword($bodFtpPassword)
    {
        $this->bodFtpPassword = $bodFtpPassword;
    
        return $this;
    }

    /**
     * Get bodFtpPassword
     *
     * @return string 
     */
    public function getBodFtpPassword()
    {
        return $this->bodFtpPassword;
    }

    /**
     * Set idBillingReportInvoiceCustomer
     *
     * @param integer $idBillingReportInvoiceCustomer
     * @return Config
     */
    public function setIdBillingReportInvoiceCustomer($idBillingReportInvoiceCustomer)
    {
        $this->idBillingReportInvoiceCustomer = $idBillingReportInvoiceCustomer;
    
        return $this;
    }

    /**
     * Get idBillingReportInvoiceCustomer
     *
     * @return integer 
     */
    public function getIdBillingReportInvoiceCustomer()
    {
        return $this->idBillingReportInvoiceCustomer;
    }

    /**
     * Set idBillingReportInvoiceDistributionChannel
     *
     * @param integer $idBillingReportInvoiceDistributionChannel
     * @return Config
     */
    public function setIdBillingReportInvoiceDistributionChannel($idBillingReportInvoiceDistributionChannel)
    {
        $this->idBillingReportInvoiceDistributionChannel = $idBillingReportInvoiceDistributionChannel;
    
        return $this;
    }

    /**
     * Get idBillingReportInvoiceDistributionChannel
     *
     * @return integer 
     */
    public function getIdBillingReportInvoiceDistributionChannel()
    {
        return $this->idBillingReportInvoiceDistributionChannel;
    }

    /**
     * Set idBillingReportInvoicePartner
     *
     * @param integer $idBillingReportInvoicePartner
     * @return Config
     */
    public function setIdBillingReportInvoicePartner($idBillingReportInvoicePartner)
    {
        $this->idBillingReportInvoicePartner = $idBillingReportInvoicePartner;
    
        return $this;
    }

    /**
     * Get idBillingReportInvoicePartner
     *
     * @return integer 
     */
    public function getIdBillingReportInvoicePartner()
    {
        return $this->idBillingReportInvoicePartner;
    }

    /**
     * Set idBillingReportPaymentDistributionChannel
     *
     * @param integer $idBillingReportPaymentDistributionChannel
     * @return Config
     */
    public function setIdBillingReportPaymentDistributionChannel($idBillingReportPaymentDistributionChannel)
    {
        $this->idBillingReportPaymentDistributionChannel = $idBillingReportPaymentDistributionChannel;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentDistributionChannel
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentDistributionChannel()
    {
        return $this->idBillingReportPaymentDistributionChannel;
    }

    /**
     * Set idBillingReportPaymentPartner
     *
     * @param integer $idBillingReportPaymentPartner
     * @return Config
     */
    public function setIdBillingReportPaymentPartner($idBillingReportPaymentPartner)
    {
        $this->idBillingReportPaymentPartner = $idBillingReportPaymentPartner;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentPartner
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentPartner()
    {
        return $this->idBillingReportPaymentPartner;
    }

    /**
     * Set idBillingReportPaymentAcquisiteur
     *
     * @param integer $idBillingReportPaymentAcquisiteur
     * @return Config
     */
    public function setIdBillingReportPaymentAcquisiteur($idBillingReportPaymentAcquisiteur)
    {
        $this->idBillingReportPaymentAcquisiteur = $idBillingReportPaymentAcquisiteur;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentAcquisiteur
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentAcquisiteur()
    {
        return $this->idBillingReportPaymentAcquisiteur;
    }

    /**
     * Set idBillingReportReminderNotice
     *
     * @param integer $idBillingReportReminderNotice
     * @return Config
     */
    public function setIdBillingReportReminderNotice($idBillingReportReminderNotice)
    {
        $this->idBillingReportReminderNotice = $idBillingReportReminderNotice;
    
        return $this;
    }

    /**
     * Get idBillingReportReminderNotice
     *
     * @return integer 
     */
    public function getIdBillingReportReminderNotice()
    {
        return $this->idBillingReportReminderNotice;
    }

    /**
     * Set billingBaselineDate
     *
     * @param \DateTime $billingBaselineDate
     * @return Config
     */
    public function setBillingBaselineDate($billingBaselineDate)
    {
        $this->billingBaselineDate = $billingBaselineDate;
    
        return $this;
    }

    /**
     * Get billingBaselineDate
     *
     * @return \DateTime 
     */
    public function getBillingBaselineDate()
    {
        return $this->billingBaselineDate;
    }

    /**
     * Set nutriMeProducts
     *
     * @param string $nutriMeProducts
     * @return Config
     */
    public function setNutriMeProducts($nutriMeProducts)
    {
        $this->nutriMeProducts = $nutriMeProducts;
    
        return $this;
    }

    /**
     * Get nutriMeProducts
     *
     * @return string 
     */
    public function getNutriMeProducts()
    {
        return $this->nutriMeProducts;
    }

    /**
     * Set idBillingReportPaymentDistributionChannelInvoice
     *
     * @param integer $idBillingReportPaymentDistributionChannelInvoice
     * @return Config
     */
    public function setIdBillingReportPaymentDistributionChannelInvoice($idBillingReportPaymentDistributionChannelInvoice)
    {
        $this->idBillingReportPaymentDistributionChannelInvoice = $idBillingReportPaymentDistributionChannelInvoice;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentDistributionChannelInvoice
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentDistributionChannelInvoice()
    {
        return $this->idBillingReportPaymentDistributionChannelInvoice;
    }

    /**
     * Set idBillingReportPaymentPartnerInvoice
     *
     * @param integer $idBillingReportPaymentPartnerInvoice
     * @return Config
     */
    public function setIdBillingReportPaymentPartnerInvoice($idBillingReportPaymentPartnerInvoice)
    {
        $this->idBillingReportPaymentPartnerInvoice = $idBillingReportPaymentPartnerInvoice;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentPartnerInvoice
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentPartnerInvoice()
    {
        return $this->idBillingReportPaymentPartnerInvoice;
    }

    /**
     * Set idBillingReportPaymentAcquisiteurInvoice
     *
     * @param integer $idBillingReportPaymentAcquisiteurInvoice
     * @return Config
     */
    public function setIdBillingReportPaymentAcquisiteurInvoice($idBillingReportPaymentAcquisiteurInvoice)
    {
        $this->idBillingReportPaymentAcquisiteurInvoice = $idBillingReportPaymentAcquisiteurInvoice;
    
        return $this;
    }

    /**
     * Get idBillingReportPaymentAcquisiteurInvoice
     *
     * @return integer 
     */
    public function getIdBillingReportPaymentAcquisiteurInvoice()
    {
        return $this->idBillingReportPaymentAcquisiteurInvoice;
    }

    /**
     * Set billingText
     *
     * @param string $billingText
     * @return Config
     */
    public function setBillingText($billingText)
    {
        $this->billingText = $billingText;
    
        return $this;
    }

    /**
     * Get billingText
     *
     * @return string 
     */
    public function getBillingText()
    {
        return $this->billingText;
    }

    /**
     * Set reportAvailableDefaultXDays
     *
     * @param integer $reportAvailableDefaultXDays
     * @return Config
     */
    public function setReportAvailableDefaultXDays($reportAvailableDefaultXDays)
    {
        $this->reportAvailableDefaultXDays = $reportAvailableDefaultXDays;
    
        return $this;
    }

    /**
     * Get reportAvailableDefaultXDays
     *
     * @return integer 
     */
    public function getReportAvailableDefaultXDays()
    {
        return $this->reportAvailableDefaultXDays;
    }

    /**
     * Set defaultWebLoginEmailPartner
     *
     * @param integer $defaultWebLoginEmailPartner
     * @return Config
     */
    public function setDefaultWebLoginEmailPartner($defaultWebLoginEmailPartner)
    {
        $this->defaultWebLoginEmailPartner = $defaultWebLoginEmailPartner;
    
        return $this;
    }

    /**
     * Get defaultWebLoginEmailPartner
     *
     * @return integer 
     */
    public function getDefaultWebLoginEmailPartner()
    {
        return $this->defaultWebLoginEmailPartner;
    }

    /**
     * Set defaultWebLoginEmailDistributionChannel
     *
     * @param integer $defaultWebLoginEmailDistributionChannel
     * @return Config
     */
    public function setDefaultWebLoginEmailDistributionChannel($defaultWebLoginEmailDistributionChannel)
    {
        $this->defaultWebLoginEmailDistributionChannel = $defaultWebLoginEmailDistributionChannel;
    
        return $this;
    }

    /**
     * Get defaultWebLoginEmailDistributionChannel
     *
     * @return integer 
     */
    public function getDefaultWebLoginEmailDistributionChannel()
    {
        return $this->defaultWebLoginEmailDistributionChannel;
    }

    /**
     * Set defaultWebLoginEmailCustomerInfo
     *
     * @param integer $defaultWebLoginEmailCustomerInfo
     * @return Config
     */
    public function setDefaultWebLoginEmailCustomerInfo($defaultWebLoginEmailCustomerInfo)
    {
        $this->defaultWebLoginEmailCustomerInfo = $defaultWebLoginEmailCustomerInfo;
    
        return $this;
    }

    /**
     * Get defaultWebLoginEmailCustomerInfo
     *
     * @return integer 
     */
    public function getDefaultWebLoginEmailCustomerInfo()
    {
        return $this->defaultWebLoginEmailCustomerInfo;
    }

    /**
     * Set customerLeadDefaultStatus
     *
     * @param integer $customerLeadDefaultStatus
     * @return Config
     */
    public function setCustomerLeadDefaultStatus($customerLeadDefaultStatus)
    {
        $this->customerLeadDefaultStatus = $customerLeadDefaultStatus;
    
        return $this;
    }

    /**
     * Get customerLeadDefaultStatus
     *
     * @return integer 
     */
    public function getCustomerLeadDefaultStatus()
    {
        return $this->customerLeadDefaultStatus;
    }

    /**
     * Set customerDashboardLoginUrl
     *
     * @param string $customerDashboardLoginUrl
     * @return Config
     */
    public function setCustomerDashboardLoginUrl($customerDashboardLoginUrl)
    {
        $this->customerDashboardLoginUrl = $customerDashboardLoginUrl;
    
        return $this;
    }

    /**
     * Get customerDashboardLoginUrl
     *
     * @return string 
     */
    public function getCustomerDashboardLoginUrl()
    {
        return $this->customerDashboardLoginUrl;
    }

    /**
     * Set shopOrderNutrimeBasedOnYourGeneUrl
     *
     * @param string $shopOrderNutrimeBasedOnYourGeneUrl
     * @return Config
     */
    public function setShopOrderNutrimeBasedOnYourGeneUrl($shopOrderNutrimeBasedOnYourGeneUrl)
    {
        $this->shopOrderNutrimeBasedOnYourGeneUrl = $shopOrderNutrimeBasedOnYourGeneUrl;
    
        return $this;
    }

    /**
     * Get shopOrderNutrimeBasedOnYourGeneUrl
     *
     * @return string 
     */
    public function getShopOrderNutrimeBasedOnYourGeneUrl()
    {
        return $this->shopOrderNutrimeBasedOnYourGeneUrl;
    }

    /**
     * Set shopOrderPrintedBookletUrl
     *
     * @param string $shopOrderPrintedBookletUrl
     * @return Config
     */
    public function setShopOrderPrintedBookletUrl($shopOrderPrintedBookletUrl)
    {
        $this->shopOrderPrintedBookletUrl = $shopOrderPrintedBookletUrl;
    
        return $this;
    }

    /**
     * Get shopOrderPrintedBookletUrl
     *
     * @return string 
     */
    public function getShopOrderPrintedBookletUrl()
    {
        return $this->shopOrderPrintedBookletUrl;
    }

    /**
     * Set massMailerHost
     *
     * @param string $massMailerHost
     * @return Config
     */
    public function setMassMailerHost($massMailerHost)
    {
        $this->massMailerHost = $massMailerHost;
    
        return $this;
    }

    /**
     * Get massMailerHost
     *
     * @return string 
     */
    public function getMassMailerHost()
    {
        return $this->massMailerHost;
    }

    /**
     * Set massMailerPort
     *
     * @param integer $massMailerPort
     * @return Config
     */
    public function setMassMailerPort($massMailerPort)
    {
        $this->massMailerPort = $massMailerPort;
    
        return $this;
    }

    /**
     * Get massMailerPort
     *
     * @return integer 
     */
    public function getMassMailerPort()
    {
        return $this->massMailerPort;
    }

    /**
     * Set massMailerEncryption
     *
     * @param string $massMailerEncryption
     * @return Config
     */
    public function setMassMailerEncryption($massMailerEncryption)
    {
        $this->massMailerEncryption = $massMailerEncryption;
    
        return $this;
    }

    /**
     * Get massMailerEncryption
     *
     * @return string 
     */
    public function getMassMailerEncryption()
    {
        return $this->massMailerEncryption;
    }

    /**
     * Set massMailerUsername
     *
     * @param string $massMailerUsername
     * @return Config
     */
    public function setMassMailerUsername($massMailerUsername)
    {
        $this->massMailerUsername = $massMailerUsername;
    
        return $this;
    }

    /**
     * Get massMailerUsername
     *
     * @return string 
     */
    public function getMassMailerUsername()
    {
        return $this->massMailerUsername;
    }

    /**
     * Set massMailerPassword
     *
     * @param string $massMailerPassword
     * @return Config
     */
    public function setMassMailerPassword($massMailerPassword)
    {
        $this->massMailerPassword = $massMailerPassword;
    
        return $this;
    }

    /**
     * Get massMailerPassword
     *
     * @return string 
     */
    public function getMassMailerPassword()
    {
        return $this->massMailerPassword;
    }

    /**
     * Set massMailerSenderName
     *
     * @param string $massMailerSenderName
     * @return Config
     */
    public function setMassMailerSenderName($massMailerSenderName)
    {
        $this->massMailerSenderName = $massMailerSenderName;
    
        return $this;
    }

    /**
     * Get massMailerSenderName
     *
     * @return string 
     */
    public function getMassMailerSenderName()
    {
        return $this->massMailerSenderName;
    }

    /**
     * Set massMailerSenderEmail
     *
     * @param string $massMailerSenderEmail
     * @return Config
     */
    public function setMassMailerSenderEmail($massMailerSenderEmail)
    {
        $this->massMailerSenderEmail = $massMailerSenderEmail;
    
        return $this;
    }

    /**
     * Get massMailerSenderEmail
     *
     * @return string 
     */
    public function getMassMailerSenderEmail()
    {
        return $this->massMailerSenderEmail;
    }

    /**
     * Set massMailerRate
     *
     * @param string $massMailerRate
     * @return Config
     */
    public function setMassMailerRate($massMailerRate)
    {
        $this->massMailerRate = $massMailerRate;
    
        return $this;
    }

    /**
     * Get massMailerRate
     *
     * @return string 
     */
    public function getMassMailerRate()
    {
        return $this->massMailerRate;
    }

    /**
     * Set mandrillApiKey
     *
     * @param string $mandrillApiKey
     * @return Config
     */
    public function setMandrillApiKey($mandrillApiKey)
    {
        $this->mandrillApiKey = $mandrillApiKey;
    
        return $this;
    }

    /**
     * Get mandrillApiKey
     *
     * @return string 
     */
    public function getMandrillApiKey()
    {
        return $this->mandrillApiKey;
    }

    /**
     * Set awsAccessKeyId
     *
     * @param string $awsAccessKeyId
     * @return Config
     */
    public function setAwsAccessKeyId($awsAccessKeyId)
    {
        $this->awsAccessKeyId = $awsAccessKeyId;
    
        return $this;
    }

    /**
     * Get awsAccessKeyId
     *
     * @return string 
     */
    public function getAwsAccessKeyId()
    {
        return $this->awsAccessKeyId;
    }

    /**
     * Set awsSecretAccessKey
     *
     * @param string $awsSecretAccessKey
     * @return Config
     */
    public function setAwsSecretAccessKey($awsSecretAccessKey)
    {
        $this->awsSecretAccessKey = $awsSecretAccessKey;
    
        return $this;
    }

    /**
     * Get awsSecretAccessKey
     *
     * @return string 
     */
    public function getAwsSecretAccessKey()
    {
        return $this->awsSecretAccessKey;
    }

    /**
     * Set awsRegion
     *
     * @param string $awsRegion
     * @return Config
     */
    public function setAwsRegion($awsRegion)
    {
        $this->awsRegion = $awsRegion;
    
        return $this;
    }

    /**
     * Get awsRegion
     *
     * @return string 
     */
    public function getAwsRegion()
    {
        return $this->awsRegion;
    }

    /**
     * Set customerDashboardIdTextBlockGroup
     *
     * @param string $customerDashboardIdTextBlockGroup
     * @return Config
     */
    public function setCustomerDashboardIdTextBlockGroup($customerDashboardIdTextBlockGroup)
    {
        $this->customerDashboardIdTextBlockGroup = $customerDashboardIdTextBlockGroup;
    
        return $this;
    }

    /**
     * Get customerDashboardIdTextBlockGroup
     *
     * @return string 
     */
    public function getCustomerDashboardIdTextBlockGroup()
    {
        return $this->customerDashboardIdTextBlockGroup;
    }

    /**
     * Set customerDashboardLanguages
     *
     * @param string $customerDashboardLanguages
     * @return Config
     */
    public function setCustomerDashboardLanguages($customerDashboardLanguages)
    {
        $this->customerDashboardLanguages = $customerDashboardLanguages;
    
        return $this;
    }

    /**
     * Get customerDashboardLanguages
     *
     * @return string 
     */
    public function getCustomerDashboardLanguages()
    {
        return $this->customerDashboardLanguages;
    }

    /**
     * Set customerDashboardLoginEmailTemplate
     *
     * @param integer $customerDashboardLoginEmailTemplate
     * @return Config
     */
    public function setCustomerDashboardLoginEmailTemplate($customerDashboardLoginEmailTemplate)
    {
        $this->customerDashboardLoginEmailTemplate = $customerDashboardLoginEmailTemplate;
    
        return $this;
    }

    /**
     * Get customerDashboardLoginEmailTemplate
     *
     * @return integer 
     */
    public function getCustomerDashboardLoginEmailTemplate()
    {
        return $this->customerDashboardLoginEmailTemplate;
    }

    /**
     * Set unlockPassword
     *
     * @param string $unlockPassword
     * @return Config
     */
    public function setUnlockPassword($unlockPassword)
    {
        $this->unlockPassword = $unlockPassword;
    
        return $this;
    }

    /**
     * Get unlockPassword
     *
     * @return string 
     */
    public function getUnlockPassword()
    {
        return $this->unlockPassword;
    }

    /**
     * Set dcMarketingStatusReminderLimitPassedDays
     *
     * @param string $dcMarketingStatusReminderLimitPassedDays
     * @return Config
     */
    public function setDcMarketingStatusReminderLimitPassedDays($dcMarketingStatusReminderLimitPassedDays)
    {
        $this->dcMarketingStatusReminderLimitPassedDays = $dcMarketingStatusReminderLimitPassedDays;
    
        return $this;
    }

    /**
     * Get dcMarketingStatusReminderLimitPassedDays
     *
     * @return string 
     */
    public function getDcMarketingStatusReminderLimitPassedDays()
    {
        return $this->dcMarketingStatusReminderLimitPassedDays;
    }

    /**
     * Set defaultRatingDistributionChannel
     *
     * @param string $defaultRatingDistributionChannel
     * @return Config
     */
    public function setDefaultRatingDistributionChannel($defaultRatingDistributionChannel)
    {
        $this->defaultRatingDistributionChannel = $defaultRatingDistributionChannel;
    
        return $this;
    }

    /**
     * Get defaultRatingDistributionChannel
     *
     * @return string 
     */
    public function getDefaultRatingDistributionChannel()
    {
        return $this->defaultRatingDistributionChannel;
    }

    /**
     * Set defaultRatingPartner
     *
     * @param string $defaultRatingPartner
     * @return Config
     */
    public function setDefaultRatingPartner($defaultRatingPartner)
    {
        $this->defaultRatingPartner = $defaultRatingPartner;
    
        return $this;
    }

    /**
     * Get defaultRatingPartner
     *
     * @return string 
     */
    public function getDefaultRatingPartner()
    {
        return $this->defaultRatingPartner;
    }

    /**
     * Set defaultRatingAcquisiteur
     *
     * @param string $defaultRatingAcquisiteur
     * @return Config
     */
    public function setDefaultRatingAcquisiteur($defaultRatingAcquisiteur)
    {
        $this->defaultRatingAcquisiteur = $defaultRatingAcquisiteur;
    
        return $this;
    }

    /**
     * Get defaultRatingAcquisiteur
     *
     * @return string 
     */
    public function getDefaultRatingAcquisiteur()
    {
        return $this->defaultRatingAcquisiteur;
    }

    /**
     * Set orderBackupDateDuration
     *
     * @param integer $orderBackupDateDuration
     * @return Config
     */
    public function setOrderBackupDateDuration($orderBackupDateDuration)
    {
        $this->orderBackupDateDuration = $orderBackupDateDuration;
    
        return $this;
    }

    /**
     * Get orderBackupDateDuration
     *
     * @return integer 
     */
    public function getOrderBackupDateDuration()
    {
        return $this->orderBackupDateDuration;
    }

    /**
     * Set untranslatedPattern
     *
     * @param string $untranslatedPattern
     * @return Config
     */
    public function setUntranslatedPattern($untranslatedPattern)
    {
        $this->untranslatedPattern = $untranslatedPattern;
    
        return $this;
    }

    /**
     * Get untranslatedPattern
     *
     * @return string 
     */
    public function getUntranslatedPattern()
    {
        return $this->untranslatedPattern;
    }

    /**
     * Set cmsContactMessageNotificationEmailList
     *
     * @param string $cmsContactMessageNotificationEmailList
     * @return Config
     */
    public function setCmsContactMessageNotificationEmailList($cmsContactMessageNotificationEmailList)
    {
        $this->cmsContactMessageNotificationEmailList = $cmsContactMessageNotificationEmailList;
    
        return $this;
    }

    /**
     * Get cmsContactMessageNotificationEmailList
     *
     * @return string 
     */
    public function getCmsContactMessageNotificationEmailList()
    {
        return $this->cmsContactMessageNotificationEmailList;
    }

    /**
     * Set emailWebLoginFilterConfig
     *
     * @param string $emailWebLoginFilterConfig
     * @return Config
     */
    public function setEmailWebLoginFilterConfig($emailWebLoginFilterConfig)
    {
        $this->emailWebLoginFilterConfig = $emailWebLoginFilterConfig;
    
        return $this;
    }

    /**
     * Get emailWebLoginFilterConfig
     *
     * @return string 
     */
    public function getEmailWebLoginFilterConfig()
    {
        return $this->emailWebLoginFilterConfig;
    }

    /**
     * Set genoBatchPrinterReportTemplateId
     *
     * @param integer $genoBatchPrinterReportTemplateId
     * @return Config
     */
    public function setGenoBatchPrinterReportTemplateId($genoBatchPrinterReportTemplateId)
    {
        $this->genoBatchPrinterReportTemplateId = $genoBatchPrinterReportTemplateId;
    
        return $this;
    }

    /**
     * Get genoBatchPrinterReportTemplateId
     *
     * @return integer 
     */
    public function getGenoBatchPrinterReportTemplateId()
    {
        return $this->genoBatchPrinterReportTemplateId;
    }

    /**
     * Set offsetCustomerPricing
     *
     * @param integer $offsetCustomerPricing
     * @return Config
     */
    public function setOffsetCustomerPricing($offsetCustomerPricing)
    {
        $this->offsetCustomerPricing = $offsetCustomerPricing;
    
        return $this;
    }

    /**
     * Get offsetCustomerPricing
     *
     * @return integer 
     */
    public function getOffsetCustomerPricing()
    {
        return $this->offsetCustomerPricing;
    }

    /**
     * Set configCustomerPricing
     *
     * @param string $configCustomerPricing
     * @return Config
     */
    public function setConfigCustomerPricing($configCustomerPricing)
    {
        $this->configCustomerPricing = $configCustomerPricing;
    
        return $this;
    }

    /**
     * Get configCustomerPricing
     *
     * @return string 
     */
    public function getConfigCustomerPricing()
    {
        return $this->configCustomerPricing;
    }

    /**
     * Set lastEmailWebloginOrderedStatus
     *
     * @param string $lastEmailWebloginOrderedStatus
     * @return Config
     */
    public function setLastEmailWebloginOrderedStatus($lastEmailWebloginOrderedStatus)
    {
        $this->lastEmailWebloginOrderedStatus = $lastEmailWebloginOrderedStatus;
    
        return $this;
    }

    /**
     * Get lastEmailWebloginOrderedStatus
     *
     * @return string 
     */
    public function getLastEmailWebloginOrderedStatus()
    {
        return $this->lastEmailWebloginOrderedStatus;
    }

    /**
     * Set emailMethodCmsOrder
     *
     * @param integer $emailMethodCmsOrder
     * @return Config
     */
    public function setEmailMethodCmsOrder($emailMethodCmsOrder)
    {
        $this->emailMethodCmsOrder = $emailMethodCmsOrder;
    
        return $this;
    }

    /**
     * Get emailMethodCmsOrder
     *
     * @return integer 
     */
    public function getEmailMethodCmsOrder()
    {
        return $this->emailMethodCmsOrder;
    }

    /**
     * Set emailMethodCustomerDashboard
     *
     * @param integer $emailMethodCustomerDashboard
     * @return Config
     */
    public function setEmailMethodCustomerDashboard($emailMethodCustomerDashboard)
    {
        $this->emailMethodCustomerDashboard = $emailMethodCustomerDashboard;
    
        return $this;
    }

    /**
     * Get emailMethodCustomerDashboard
     *
     * @return integer 
     */
    public function getEmailMethodCustomerDashboard()
    {
        return $this->emailMethodCustomerDashboard;
    }

    /**
     * Set salesAnalysisFilterConfig
     *
     * @param string $salesAnalysisFilterConfig
     * @return Config
     */
    public function setSalesAnalysisFilterConfig($salesAnalysisFilterConfig)
    {
        $this->salesAnalysisFilterConfig = $salesAnalysisFilterConfig;
    
        return $this;
    }

    /**
     * Get salesAnalysisFilterConfig
     *
     * @return string 
     */
    public function getSalesAnalysisFilterConfig()
    {
        return $this->salesAnalysisFilterConfig;
    }
}