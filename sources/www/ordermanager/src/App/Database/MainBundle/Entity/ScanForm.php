<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanForm
 *
 * @ORM\Table(name="scan_form")
 * @ORM\Entity
 */
class ScanForm
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $formImgFile
     *
     * @ORM\Column(name="form_img_file", type="string", length=255, nullable=true)
     */
    private $formImgFile;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=11, nullable=true)
     */
    private $code;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScanForm
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formImgFile
     *
     * @param string $formImgFile
     * @return ScanForm
     */
    public function setFormImgFile($formImgFile)
    {
        $this->formImgFile = $formImgFile;
    
        return $this;
    }

    /**
     * Get formImgFile
     *
     * @return string 
     */
    public function getFormImgFile()
    {
        return $this->formImgFile;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ScanForm
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
}