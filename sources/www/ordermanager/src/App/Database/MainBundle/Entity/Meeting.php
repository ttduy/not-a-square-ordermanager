<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Meeting
 *
 * @ORM\Table(name="meeting")
 * @ORM\Entity
 */
class Meeting
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var \DateTime $meetingDatetime
     *
     * @ORM\Column(name="meeting_datetime", type="datetime", nullable=true)
     */
    private $meetingDatetime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Meeting
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set meetingDatetime
     *
     * @param \DateTime $meetingDatetime
     * @return Meeting
     */
    public function setMeetingDatetime($meetingDatetime)
    {
        $this->meetingDatetime = $meetingDatetime;
    
        return $this;
    }

    /**
     * Get meetingDatetime
     *
     * @return \DateTime 
     */
    public function getMeetingDatetime()
    {
        return $this->meetingDatetime;
    }
}