<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TextToolType
 *
 * @ORM\Table(name="text_tool_type")
 * @ORM\Entity
 */
class TextToolType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=765, nullable=true)
     */
    private $name;

    /**
     * @var string $controlMapping
     *
     * @ORM\Column(name="control_mapping", type="string", length=765, nullable=true)
     */
    private $controlMapping;

    /**
     * @var string $bgColor
     *
     * @ORM\Column(name="bg_color", type="string", length=765, nullable=true)
     */
    private $bgColor;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TextToolType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set controlMapping
     *
     * @param string $controlMapping
     * @return TextToolType
     */
    public function setControlMapping($controlMapping)
    {
        $this->controlMapping = $controlMapping;
    
        return $this;
    }

    /**
     * Get controlMapping
     *
     * @return string 
     */
    public function getControlMapping()
    {
        return $this->controlMapping;
    }

    /**
     * Set bgColor
     *
     * @param string $bgColor
     * @return TextToolType
     */
    public function setBgColor($bgColor)
    {
        $this->bgColor = $bgColor;
    
        return $this;
    }

    /**
     * Get bgColor
     *
     * @return string 
     */
    public function getBgColor()
    {
        return $this->bgColor;
    }
}