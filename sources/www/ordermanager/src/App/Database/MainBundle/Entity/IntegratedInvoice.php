<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\IntegratedInvoice
 *
 * @ORM\Table(name="integrated_invoice")
 * @ORM\Entity
 */
class IntegratedInvoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     */
    public $id;

    /**
     * @var string $invoiceType
     *
     * @ORM\Column(name="invoice_type", type="string", length=255, nullable=true)
     */
    public $invoiceType;

    /**
     * @var string $invoiceName
     *
     * @ORM\Column(name="invoice_name", type="string", length=255, nullable=true)
     */
    public $invoiceName;

    /**
     * @var string $invoiceNumber
     *
     * @ORM\Column(name="invoice_number", type="string", length=255, nullable=true)
     */
    public $invoiceNumber;


    /**
     * @var \DateTime $invoiceDate
     *
     * @ORM\Column(name="invoice_date", type="date", nullable=true)
     */
    public $invoiceDate;

    /**
     * @var integer $invoiceStatus
     *
     * @ORM\Column(name="invoice_status", type="integer", nullable=true)
     */
    public $invoiceStatus;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    public $statusDate;

    /**
     * @var float $invoiceAmount
     *
     * @ORM\Column(name="invoice_amount", type="decimal", nullable=true)
     */
    public $invoiceAmount;
    /**
     * @var float $isWithTax
     *
     * @ORM\Column(name="is_with_tax", type="decimal", nullable=true)
     */
    public $isWithTax;
    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    public $taxValue;

    /**
     * Set id
     *
     * @param integer $id
     * @return IntegratedInvoice
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoiceType
     *
     * @param string $invoiceType
     * @return IntegratedInvoice
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    
        return $this;
    }

    /**
     * Get invoiceType
     *
     * @return string 
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * Set invoiceName
     *
     * @param string $invoiceName
     * @return IntegratedInvoice
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;
    
        return $this;
    }

    /**
     * Get invoiceName
     *
     * @return string 
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     * @return IntegratedInvoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string 
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     * @return IntegratedInvoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;
    
        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime 
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Set invoiceStatus
     *
     * @param integer $invoiceStatus
     * @return IntegratedInvoice
     */
    public function setInvoiceStatus($invoiceStatus)
    {
        $this->invoiceStatus = $invoiceStatus;
    
        return $this;
    }

    /**
     * Get invoiceStatus
     *
     * @return integer 
     */
    public function getInvoiceStatus()
    {
        return $this->invoiceStatus;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return IntegratedInvoice
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set invoiceAmount
     *
     * @param float $invoiceAmount
     * @return IntegratedInvoice
     */
    public function setInvoiceAmount($invoiceAmount)
    {
        $this->invoiceAmount = $invoiceAmount;
    
        return $this;
    }

    /**
     * Get invoiceAmount
     *
     * @return float 
     */
    public function getInvoiceAmount()
    {
        return $this->invoiceAmount;
    }

    /**
     * Set isWithTax
     *
     * @param float $isWithTax
     * @return IntegratedInvoice
     */
    public function setIsWithTax($isWithTax)
    {
        $this->isWithTax = $isWithTax;
    
        return $this;
    }

    /**
     * Get isWithTax
     *
     * @return float 
     */
    public function getIsWithTax()
    {
        return $this->isWithTax;
    }

    /**
     * Set taxValue
     *
     * @param float $taxValue
     * @return IntegratedInvoice
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    
        return $this;
    }

    /**
     * Get taxValue
     *
     * @return float 
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }
}