<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Table1
 *
 * @ORM\Table(name="table1")
 * @ORM\Entity
 */
class Table1
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="Domain", type="text", nullable=true)
     */
    private $domain;

    /**
     * @var string $distributionchannel
     *
     * @ORM\Column(name="DistributionChannel", type="string", length=255, nullable=true)
     */
    private $distributionchannel;

    /**
     * @var string $uid
     *
     * @ORM\Column(name="Uid", type="string", length=255, nullable=true)
     */
    private $uid;

    /**
     * @var string $type
     *
     * @ORM\Column(name="Type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer $pricecategory
     *
     * @ORM\Column(name="PriceCategory", type="integer", nullable=true)
     */
    private $pricecategory;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var boolean $male
     *
     * @ORM\Column(name="Male", type="boolean", nullable=true)
     */
    private $male;

    /**
     * @var integer $female
     *
     * @ORM\Column(name="Female", type="integer", nullable=true)
     */
    private $female;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="Surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string $institution
     *
     * @ORM\Column(name="Institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string $country
     *
     * @ORM\Column(name="Country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string $contactemail
     *
     * @ORM\Column(name="ContactEmail", type="string", length=255, nullable=true)
     */
    private $contactemail;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $statusdeliveryemail
     *
     * @ORM\Column(name="StatusDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $statusdeliveryemail;

    /**
     * @var string $reportdeliveryemail
     *
     * @ORM\Column(name="ReportDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $reportdeliveryemail;

    /**
     * @var string $invoicedeliveryemail
     *
     * @ORM\Column(name="InvoiceDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $invoicedeliveryemail;

    /**
     * @var string $invoicegoesto
     *
     * @ORM\Column(name="InvoiceGoesTo", type="string", length=255, nullable=true)
     */
    private $invoicegoesto;

    /**
     * @var integer $isinvoicetopartnerwhichone
     *
     * @ORM\Column(name="IsInvoiceToPartnerWhichOne", type="integer", nullable=true)
     */
    private $isinvoicetopartnerwhichone;

    /**
     * @var string $reportgoesto
     *
     * @ORM\Column(name="ReportGoesTo", type="string", length=255, nullable=true)
     */
    private $reportgoesto;

    /**
     * @var string $reportdelivery
     *
     * @ORM\Column(name="ReportDelivery", type="string", length=255, nullable=true)
     */
    private $reportdelivery;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Table1
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set distributionchannel
     *
     * @param string $distributionchannel
     * @return Table1
     */
    public function setDistributionchannel($distributionchannel)
    {
        $this->distributionchannel = $distributionchannel;
    
        return $this;
    }

    /**
     * Get distributionchannel
     *
     * @return string 
     */
    public function getDistributionchannel()
    {
        return $this->distributionchannel;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return Table1
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    
        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Table1
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pricecategory
     *
     * @param integer $pricecategory
     * @return Table1
     */
    public function setPricecategory($pricecategory)
    {
        $this->pricecategory = $pricecategory;
    
        return $this;
    }

    /**
     * Get pricecategory
     *
     * @return integer 
     */
    public function getPricecategory()
    {
        return $this->pricecategory;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return Table1
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set male
     *
     * @param boolean $male
     * @return Table1
     */
    public function setMale($male)
    {
        $this->male = $male;
    
        return $this;
    }

    /**
     * Get male
     *
     * @return boolean 
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * Set female
     *
     * @param integer $female
     * @return Table1
     */
    public function setFemale($female)
    {
        $this->female = $female;
    
        return $this;
    }

    /**
     * Get female
     *
     * @return integer 
     */
    public function getFemale()
    {
        return $this->female;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Table1
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Table1
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Table1
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return Table1
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    
        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Table1
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Table1
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Table1
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Table1
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set contactemail
     *
     * @param string $contactemail
     * @return Table1
     */
    public function setContactemail($contactemail)
    {
        $this->contactemail = $contactemail;
    
        return $this;
    }

    /**
     * Get contactemail
     *
     * @return string 
     */
    public function getContactemail()
    {
        return $this->contactemail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Table1
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Table1
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set statusdeliveryemail
     *
     * @param string $statusdeliveryemail
     * @return Table1
     */
    public function setStatusdeliveryemail($statusdeliveryemail)
    {
        $this->statusdeliveryemail = $statusdeliveryemail;
    
        return $this;
    }

    /**
     * Get statusdeliveryemail
     *
     * @return string 
     */
    public function getStatusdeliveryemail()
    {
        return $this->statusdeliveryemail;
    }

    /**
     * Set reportdeliveryemail
     *
     * @param string $reportdeliveryemail
     * @return Table1
     */
    public function setReportdeliveryemail($reportdeliveryemail)
    {
        $this->reportdeliveryemail = $reportdeliveryemail;
    
        return $this;
    }

    /**
     * Get reportdeliveryemail
     *
     * @return string 
     */
    public function getReportdeliveryemail()
    {
        return $this->reportdeliveryemail;
    }

    /**
     * Set invoicedeliveryemail
     *
     * @param string $invoicedeliveryemail
     * @return Table1
     */
    public function setInvoicedeliveryemail($invoicedeliveryemail)
    {
        $this->invoicedeliveryemail = $invoicedeliveryemail;
    
        return $this;
    }

    /**
     * Get invoicedeliveryemail
     *
     * @return string 
     */
    public function getInvoicedeliveryemail()
    {
        return $this->invoicedeliveryemail;
    }

    /**
     * Set invoicegoesto
     *
     * @param string $invoicegoesto
     * @return Table1
     */
    public function setInvoicegoesto($invoicegoesto)
    {
        $this->invoicegoesto = $invoicegoesto;
    
        return $this;
    }

    /**
     * Get invoicegoesto
     *
     * @return string 
     */
    public function getInvoicegoesto()
    {
        return $this->invoicegoesto;
    }

    /**
     * Set isinvoicetopartnerwhichone
     *
     * @param integer $isinvoicetopartnerwhichone
     * @return Table1
     */
    public function setIsinvoicetopartnerwhichone($isinvoicetopartnerwhichone)
    {
        $this->isinvoicetopartnerwhichone = $isinvoicetopartnerwhichone;
    
        return $this;
    }

    /**
     * Get isinvoicetopartnerwhichone
     *
     * @return integer 
     */
    public function getIsinvoicetopartnerwhichone()
    {
        return $this->isinvoicetopartnerwhichone;
    }

    /**
     * Set reportgoesto
     *
     * @param string $reportgoesto
     * @return Table1
     */
    public function setReportgoesto($reportgoesto)
    {
        $this->reportgoesto = $reportgoesto;
    
        return $this;
    }

    /**
     * Get reportgoesto
     *
     * @return string 
     */
    public function getReportgoesto()
    {
        return $this->reportgoesto;
    }

    /**
     * Set reportdelivery
     *
     * @param string $reportdelivery
     * @return Table1
     */
    public function setReportdelivery($reportdelivery)
    {
        $this->reportdelivery = $reportdelivery;
    
        return $this;
    }

    /**
     * Get reportdelivery
     *
     * @return string 
     */
    public function getReportdelivery()
    {
        return $this->reportdelivery;
    }
}