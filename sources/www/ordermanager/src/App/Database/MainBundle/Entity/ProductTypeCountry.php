<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductTypeCountry
 *
 * @ORM\Table(name="product_type_country")
 * @ORM\Entity
 */
class ProductTypeCountry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idProductType
     *
     * @ORM\Column(name="id_product_type", type="integer", nullable=true)
     */
    private $idProductType;

    /**
     * @var integer $idCountry
     *
     * @ORM\Column(name="id_country", type="integer", nullable=true)
     */
    private $idCountry;

    /**
     * @var string $countryCode
     *
     * @ORM\Column(name="country_code", type="string", length=45, nullable=true)
     */
    private $countryCode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProductType
     *
     * @param integer $idProductType
     * @return ProductTypeCountry
     */
    public function setIdProductType($idProductType)
    {
        $this->idProductType = $idProductType;
    
        return $this;
    }

    /**
     * Get idProductType
     *
     * @return integer 
     */
    public function getIdProductType()
    {
        return $this->idProductType;
    }

    /**
     * Set idCountry
     *
     * @param integer $idCountry
     * @return ProductTypeCountry
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;
    
        return $this;
    }

    /**
     * Get idCountry
     *
     * @return integer 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return ProductTypeCountry
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    
        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
}