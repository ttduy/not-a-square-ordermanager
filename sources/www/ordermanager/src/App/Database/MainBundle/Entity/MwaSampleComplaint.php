<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaSampleComplaint
 *
 * @ORM\Table(name="mwa_sample_complaint")
 * @ORM\Entity
 */
class MwaSampleComplaint
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idMwaSample
     *
     * @ORM\Column(name="id_mwa_sample", type="integer", nullable=true)
     */
    private $idMwaSample;

    /**
     * @var \DateTime $problemTimestamp
     *
     * @ORM\Column(name="problem_timestamp", type="datetime", nullable=false)
     */
    private $problemTimestamp;

    /**
     * @var integer $problemIdWebUser
     *
     * @ORM\Column(name="problem_id_web_user", type="integer", nullable=true)
     */
    private $problemIdWebUser;

    /**
     * @var string $problemComplaintant
     *
     * @ORM\Column(name="problem_complaintant", type="string", length=255, nullable=true)
     */
    private $problemComplaintant;

    /**
     * @var string $problemDescription
     *
     * @ORM\Column(name="problem_description", type="text", nullable=true)
     */
    private $problemDescription;

    /**
     * @var \DateTime $solutionTimestamp
     *
     * @ORM\Column(name="solution_timestamp", type="datetime", nullable=false)
     */
    private $solutionTimestamp;

    /**
     * @var integer $solutionIdWebUser
     *
     * @ORM\Column(name="solution_id_web_user", type="integer", nullable=true)
     */
    private $solutionIdWebUser;

    /**
     * @var string $solutionDescription
     *
     * @ORM\Column(name="solution_description", type="text", nullable=true)
     */
    private $solutionDescription;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMwaSample
     *
     * @param integer $idMwaSample
     * @return MwaSampleComplaint
     */
    public function setIdMwaSample($idMwaSample)
    {
        $this->idMwaSample = $idMwaSample;
    
        return $this;
    }

    /**
     * Get idMwaSample
     *
     * @return integer 
     */
    public function getIdMwaSample()
    {
        return $this->idMwaSample;
    }

    /**
     * Set problemTimestamp
     *
     * @param \DateTime $problemTimestamp
     * @return MwaSampleComplaint
     */
    public function setProblemTimestamp($problemTimestamp)
    {
        $this->problemTimestamp = $problemTimestamp;
    
        return $this;
    }

    /**
     * Get problemTimestamp
     *
     * @return \DateTime 
     */
    public function getProblemTimestamp()
    {
        return $this->problemTimestamp;
    }

    /**
     * Set problemIdWebUser
     *
     * @param integer $problemIdWebUser
     * @return MwaSampleComplaint
     */
    public function setProblemIdWebUser($problemIdWebUser)
    {
        $this->problemIdWebUser = $problemIdWebUser;
    
        return $this;
    }

    /**
     * Get problemIdWebUser
     *
     * @return integer 
     */
    public function getProblemIdWebUser()
    {
        return $this->problemIdWebUser;
    }

    /**
     * Set problemComplaintant
     *
     * @param string $problemComplaintant
     * @return MwaSampleComplaint
     */
    public function setProblemComplaintant($problemComplaintant)
    {
        $this->problemComplaintant = $problemComplaintant;
    
        return $this;
    }

    /**
     * Get problemComplaintant
     *
     * @return string 
     */
    public function getProblemComplaintant()
    {
        return $this->problemComplaintant;
    }

    /**
     * Set problemDescription
     *
     * @param string $problemDescription
     * @return MwaSampleComplaint
     */
    public function setProblemDescription($problemDescription)
    {
        $this->problemDescription = $problemDescription;
    
        return $this;
    }

    /**
     * Get problemDescription
     *
     * @return string 
     */
    public function getProblemDescription()
    {
        return $this->problemDescription;
    }

    /**
     * Set solutionTimestamp
     *
     * @param \DateTime $solutionTimestamp
     * @return MwaSampleComplaint
     */
    public function setSolutionTimestamp($solutionTimestamp)
    {
        $this->solutionTimestamp = $solutionTimestamp;
    
        return $this;
    }

    /**
     * Get solutionTimestamp
     *
     * @return \DateTime 
     */
    public function getSolutionTimestamp()
    {
        return $this->solutionTimestamp;
    }

    /**
     * Set solutionIdWebUser
     *
     * @param integer $solutionIdWebUser
     * @return MwaSampleComplaint
     */
    public function setSolutionIdWebUser($solutionIdWebUser)
    {
        $this->solutionIdWebUser = $solutionIdWebUser;
    
        return $this;
    }

    /**
     * Get solutionIdWebUser
     *
     * @return integer 
     */
    public function getSolutionIdWebUser()
    {
        return $this->solutionIdWebUser;
    }

    /**
     * Set solutionDescription
     *
     * @param string $solutionDescription
     * @return MwaSampleComplaint
     */
    public function setSolutionDescription($solutionDescription)
    {
        $this->solutionDescription = $solutionDescription;
    
        return $this;
    }

    /**
     * Get solutionDescription
     *
     * @return string 
     */
    public function getSolutionDescription()
    {
        return $this->solutionDescription;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MwaSampleComplaint
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return MwaSampleComplaint
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}