<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PelletEntry
 *
 * @ORM\Table(name="pellet_entry")
 * @ORM\Entity
 */
class PelletEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPellet
     *
     * @ORM\Column(name="id_pellet", type="integer", nullable=true)
     */
    private $idPellet;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="date", nullable=true)
     */
    private $created;

    /**
     * @var string $createdBy
     *
     * @ORM\Column(name="created_by", type="string", length=255, nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime $received
     *
     * @ORM\Column(name="received", type="date", nullable=true)
     */
    private $received;

    /**
     * @var boolean $receivedStatus
     *
     * @ORM\Column(name="received_status", type="boolean", nullable=true)
     */
    private $receivedStatus;

    /**
     * @var string $receivedBy
     *
     * @ORM\Column(name="received_by", type="string", length=255, nullable=true)
     */
    private $receivedBy;

    /**
     * @var \DateTime $expiryDate
     *
     * @ORM\Column(name="expiry_date", type="date", nullable=true)
     */
    private $expiryDate;

    /**
     * @var boolean $expiryDateWarningDisabled
     *
     * @ORM\Column(name="expiry_date_warning_disabled", type="boolean", nullable=true)
     */
    private $expiryDateWarningDisabled;

    /**
     * @var integer $cups
     *
     * @ORM\Column(name="cups", type="integer", nullable=true)
     */
    private $cups;

    /**
     * @var integer $section
     *
     * @ORM\Column(name="section", type="integer", nullable=true)
     */
    private $section;

    /**
     * @var string $note
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var boolean $isSelected
     *
     * @ORM\Column(name="is_selected", type="boolean", nullable=true)
     */
    private $isSelected;

    /**
     * @var \DateTime $dateTaken
     *
     * @ORM\Column(name="date_taken", type="date", nullable=true)
     */
    private $dateTaken;

    /**
     * @var string $usedFor
     *
     * @ORM\Column(name="used_for", type="string", length=255, nullable=true)
     */
    private $usedFor;

    /**
     * @var string $takenBy
     *
     * @ORM\Column(name="taken_by", type="string", length=255, nullable=true)
     */
    private $takenBy;

    /**
     * @var float $ordered
     *
     * @ORM\Column(name="ordered", type="float", nullable=true)
     */
    private $ordered;

    /**
     * @var string $shipment
     *
     * @ORM\Column(name="shipment", type="string", length=30, nullable=true)
     */
    private $shipment;

    /**
     * @var integer $idSuperEntry
     *
     * @ORM\Column(name="id_super_entry", type="integer", nullable=true)
     */
    private $idSuperEntry;

    /**
     * @var string $exterialLot
     *
     * @ORM\Column(name="exterial_lot", type="string", length=255, nullable=true)
     */
    private $exterialLot;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPellet
     *
     * @param integer $idPellet
     * @return PelletEntry
     */
    public function setIdPellet($idPellet)
    {
        $this->idPellet = $idPellet;
    
        return $this;
    }

    /**
     * Get idPellet
     *
     * @return integer 
     */
    public function getIdPellet()
    {
        return $this->idPellet;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PelletEntry
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return PelletEntry
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set received
     *
     * @param \DateTime $received
     * @return PelletEntry
     */
    public function setReceived($received)
    {
        $this->received = $received;
    
        return $this;
    }

    /**
     * Get received
     *
     * @return \DateTime 
     */
    public function getReceived()
    {
        return $this->received;
    }

    /**
     * Set receivedStatus
     *
     * @param boolean $receivedStatus
     * @return PelletEntry
     */
    public function setReceivedStatus($receivedStatus)
    {
        $this->receivedStatus = $receivedStatus;
    
        return $this;
    }

    /**
     * Get receivedStatus
     *
     * @return boolean 
     */
    public function getReceivedStatus()
    {
        return $this->receivedStatus;
    }

    /**
     * Set receivedBy
     *
     * @param string $receivedBy
     * @return PelletEntry
     */
    public function setReceivedBy($receivedBy)
    {
        $this->receivedBy = $receivedBy;
    
        return $this;
    }

    /**
     * Get receivedBy
     *
     * @return string 
     */
    public function getReceivedBy()
    {
        return $this->receivedBy;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return PelletEntry
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set expiryDateWarningDisabled
     *
     * @param boolean $expiryDateWarningDisabled
     * @return PelletEntry
     */
    public function setExpiryDateWarningDisabled($expiryDateWarningDisabled)
    {
        $this->expiryDateWarningDisabled = $expiryDateWarningDisabled;
    
        return $this;
    }

    /**
     * Get expiryDateWarningDisabled
     *
     * @return boolean 
     */
    public function getExpiryDateWarningDisabled()
    {
        return $this->expiryDateWarningDisabled;
    }

    /**
     * Set cups
     *
     * @param integer $cups
     * @return PelletEntry
     */
    public function setCups($cups)
    {
        $this->cups = $cups;
    
        return $this;
    }

    /**
     * Get cups
     *
     * @return integer 
     */
    public function getCups()
    {
        return $this->cups;
    }

    /**
     * Set section
     *
     * @param integer $section
     * @return PelletEntry
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return integer 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return PelletEntry
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set isSelected
     *
     * @param boolean $isSelected
     * @return PelletEntry
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;
    
        return $this;
    }

    /**
     * Get isSelected
     *
     * @return boolean 
     */
    public function getIsSelected()
    {
        return $this->isSelected;
    }

    /**
     * Set dateTaken
     *
     * @param \DateTime $dateTaken
     * @return PelletEntry
     */
    public function setDateTaken($dateTaken)
    {
        $this->dateTaken = $dateTaken;
    
        return $this;
    }

    /**
     * Get dateTaken
     *
     * @return \DateTime 
     */
    public function getDateTaken()
    {
        return $this->dateTaken;
    }

    /**
     * Set usedFor
     *
     * @param string $usedFor
     * @return PelletEntry
     */
    public function setUsedFor($usedFor)
    {
        $this->usedFor = $usedFor;
    
        return $this;
    }

    /**
     * Get usedFor
     *
     * @return string 
     */
    public function getUsedFor()
    {
        return $this->usedFor;
    }

    /**
     * Set takenBy
     *
     * @param string $takenBy
     * @return PelletEntry
     */
    public function setTakenBy($takenBy)
    {
        $this->takenBy = $takenBy;
    
        return $this;
    }

    /**
     * Get takenBy
     *
     * @return string 
     */
    public function getTakenBy()
    {
        return $this->takenBy;
    }

    /**
     * Set ordered
     *
     * @param float $ordered
     * @return PelletEntry
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;
    
        return $this;
    }

    /**
     * Get ordered
     *
     * @return float 
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * Set shipment
     *
     * @param string $shipment
     * @return PelletEntry
     */
    public function setShipment($shipment)
    {
        $this->shipment = $shipment;
    
        return $this;
    }

    /**
     * Get shipment
     *
     * @return string 
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set idSuperEntry
     *
     * @param integer $idSuperEntry
     * @return PelletEntry
     */
    public function setIdSuperEntry($idSuperEntry)
    {
        $this->idSuperEntry = $idSuperEntry;
    
        return $this;
    }

    /**
     * Get idSuperEntry
     *
     * @return integer 
     */
    public function getIdSuperEntry()
    {
        return $this->idSuperEntry;
    }

    /**
     * Set exterialLot
     *
     * @param string $exterialLot
     * @return PelletEntry
     */
    public function setExterialLot($exterialLot)
    {
        $this->exterialLot = $exterialLot;
    
        return $this;
    }

    /**
     * Get exterialLot
     *
     * @return string 
     */
    public function getExterialLot()
    {
        return $this->exterialLot;
    }
}