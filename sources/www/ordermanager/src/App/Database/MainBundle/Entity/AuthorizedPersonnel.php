<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\AuthorizedPersonnel
 *
 * @ORM\Table(name="authorized_personnel")
 * @ORM\Entity
 */
class AuthorizedPersonnel
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $webUsers
     *
     * @ORM\Column(name="web_users", type="text", nullable=true)
     */
    private $webUsers;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AuthorizedPersonnel
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set webUsers
     *
     * @param string $webUsers
     * @return AuthorizedPersonnel
     */
    public function setWebUsers($webUsers)
    {
        $this->webUsers = $webUsers;
    
        return $this;
    }

    /**
     * Get webUsers
     *
     * @return string 
     */
    public function getWebUsers()
    {
        return $this->webUsers;
    }
}