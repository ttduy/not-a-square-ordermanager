<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MeetingEntry
 *
 * @ORM\Table(name="meeting_entry")
 * @ORM\Entity
 */
class MeetingEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idMeeting
     *
     * @ORM\Column(name="id_meeting", type="integer", nullable=true)
     */
    private $idMeeting;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $idTodoTask
     *
     * @ORM\Column(name="id_todo_task", type="integer", nullable=true)
     */
    private $idTodoTask;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMeeting
     *
     * @param integer $idMeeting
     * @return MeetingEntry
     */
    public function setIdMeeting($idMeeting)
    {
        $this->idMeeting = $idMeeting;
    
        return $this;
    }

    /**
     * Get idMeeting
     *
     * @return integer 
     */
    public function getIdMeeting()
    {
        return $this->idMeeting;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MeetingEntry
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MeetingEntry
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idTodoTask
     *
     * @param integer $idTodoTask
     * @return MeetingEntry
     */
    public function setIdTodoTask($idTodoTask)
    {
        $this->idTodoTask = $idTodoTask;
    
        return $this;
    }

    /**
     * Get idTodoTask
     *
     * @return integer 
     */
    public function getIdTodoTask()
    {
        return $this->idTodoTask;
    }
}