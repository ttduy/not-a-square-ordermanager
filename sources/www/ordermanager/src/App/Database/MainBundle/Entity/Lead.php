<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Lead
 *
 * @ORM\Table(name="lead")
 * @ORM\Entity
 */
class Lead
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $uidNumber
     *
     * @ORM\Column(name="uid_number", type="string", length=255, nullable=true)
     */
    private $uidNumber;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var integer $idGender
     *
     * @ORM\Column(name="id_gender", type="integer", nullable=true)
     */
    private $idGender;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $idAcquisiteur
     *
     * @ORM\Column(name="id_acquisiteur", type="integer", nullable=true)
     */
    private $idAcquisiteur;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $surName
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @var string $institution
     *
     * @ORM\Column(name="institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string $street
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $idCountry
     *
     * @ORM\Column(name="id_country", type="integer", nullable=true)
     */
    private $idCountry;

    /**
     * @var string $contactEmail
     *
     * @ORM\Column(name="contact_email", type="string", length=255, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="date", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var integer $idReminderIn
     *
     * @ORM\Column(name="id_reminder_in", type="integer", nullable=true)
     */
    private $idReminderIn;

    /**
     * @var \DateTime $reminderDate
     *
     * @ORM\Column(name="reminder_date", type="date", nullable=true)
     */
    private $reminderDate;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var string $source
     *
     * @ORM\Column(name="source", type="text", nullable=true)
     */
    private $source;

    /**
     * @var string $statusList
     *
     * @ORM\Column(name="status_list", type="text", nullable=true)
     */
    private $statusList;

    /**
     * @var string $company
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string $inputCustomerName
     *
     * @ORM\Column(name="input_customer_name", type="string", length=255, nullable=true)
     */
    private $inputCustomerName;

    /**
     * @var string $inputCustomerEmail
     *
     * @ORM\Column(name="input_customer_email", type="string", length=255, nullable=true)
     */
    private $inputCustomerEmail;

    /**
     * @var string $inputCustomerCompany
     *
     * @ORM\Column(name="input_customer_company", type="string", length=255, nullable=true)
     */
    private $inputCustomerCompany;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idCategory
     *
     * @ORM\Column(name="id_category", type="integer", nullable=true)
     */
    private $idCategory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Lead
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uidNumber
     *
     * @param string $uidNumber
     * @return Lead
     */
    public function setUidNumber($uidNumber)
    {
        $this->uidNumber = $uidNumber;
    
        return $this;
    }

    /**
     * Get uidNumber
     *
     * @return string 
     */
    public function getUidNumber()
    {
        return $this->uidNumber;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return Lead
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set idGender
     *
     * @param integer $idGender
     * @return Lead
     */
    public function setIdGender($idGender)
    {
        $this->idGender = $idGender;
    
        return $this;
    }

    /**
     * Get idGender
     *
     * @return integer 
     */
    public function getIdGender()
    {
        return $this->idGender;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return Lead
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idAcquisiteur
     *
     * @param integer $idAcquisiteur
     * @return Lead
     */
    public function setIdAcquisiteur($idAcquisiteur)
    {
        $this->idAcquisiteur = $idAcquisiteur;
    
        return $this;
    }

    /**
     * Get idAcquisiteur
     *
     * @return integer 
     */
    public function getIdAcquisiteur()
    {
        return $this->idAcquisiteur;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Lead
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Lead
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surName
     *
     * @param string $surName
     * @return Lead
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;
    
        return $this;
    }

    /**
     * Get surName
     *
     * @return string 
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return Lead
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    
        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Lead
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Lead
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Lead
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set idCountry
     *
     * @param integer $idCountry
     * @return Lead
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;
    
        return $this;
    }

    /**
     * Get idCountry
     *
     * @return integer 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Lead
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    
        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string 
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Lead
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return Lead
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return Lead
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set idReminderIn
     *
     * @param integer $idReminderIn
     * @return Lead
     */
    public function setIdReminderIn($idReminderIn)
    {
        $this->idReminderIn = $idReminderIn;
    
        return $this;
    }

    /**
     * Get idReminderIn
     *
     * @return integer 
     */
    public function getIdReminderIn()
    {
        return $this->idReminderIn;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     * @return Lead
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;
    
        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime 
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Lead
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Lead
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set statusList
     *
     * @param string $statusList
     * @return Lead
     */
    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    
        return $this;
    }

    /**
     * Get statusList
     *
     * @return string 
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Lead
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set inputCustomerName
     *
     * @param string $inputCustomerName
     * @return Lead
     */
    public function setInputCustomerName($inputCustomerName)
    {
        $this->inputCustomerName = $inputCustomerName;
    
        return $this;
    }

    /**
     * Get inputCustomerName
     *
     * @return string 
     */
    public function getInputCustomerName()
    {
        return $this->inputCustomerName;
    }

    /**
     * Set inputCustomerEmail
     *
     * @param string $inputCustomerEmail
     * @return Lead
     */
    public function setInputCustomerEmail($inputCustomerEmail)
    {
        $this->inputCustomerEmail = $inputCustomerEmail;
    
        return $this;
    }

    /**
     * Get inputCustomerEmail
     *
     * @return string 
     */
    public function getInputCustomerEmail()
    {
        return $this->inputCustomerEmail;
    }

    /**
     * Set inputCustomerCompany
     *
     * @param string $inputCustomerCompany
     * @return Lead
     */
    public function setInputCustomerCompany($inputCustomerCompany)
    {
        $this->inputCustomerCompany = $inputCustomerCompany;
    
        return $this;
    }

    /**
     * Get inputCustomerCompany
     *
     * @return string 
     */
    public function getInputCustomerCompany()
    {
        return $this->inputCustomerCompany;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return Lead
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     * @return Lead
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    
        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer 
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }
}