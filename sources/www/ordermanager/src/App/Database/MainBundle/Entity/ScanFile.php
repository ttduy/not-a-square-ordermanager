<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFile
 *
 * @ORM\Table(name="scan_file")
 * @ORM\Entity
 */
class ScanFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $filename
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $errorMessage
     *
     * @ORM\Column(name="error_message", type="text", nullable=true)
     */
    private $errorMessage;

    /**
     * @var integer $idScanFormTemplate
     *
     * @ORM\Column(name="id_scan_form_template", type="integer", nullable=true)
     */
    private $idScanFormTemplate;

    /**
     * @var integer $idOrder
     *
     * @ORM\Column(name="id_order", type="integer", nullable=true)
     */
    private $idOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return ScanFile
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return ScanFile
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ScanFile
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     * @return ScanFile
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    
        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string 
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set idScanFormTemplate
     *
     * @param integer $idScanFormTemplate
     * @return ScanFile
     */
    public function setIdScanFormTemplate($idScanFormTemplate)
    {
        $this->idScanFormTemplate = $idScanFormTemplate;
    
        return $this;
    }

    /**
     * Get idScanFormTemplate
     *
     * @return integer 
     */
    public function getIdScanFormTemplate()
    {
        return $this->idScanFormTemplate;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     * @return ScanFile
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    
        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer 
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }
}