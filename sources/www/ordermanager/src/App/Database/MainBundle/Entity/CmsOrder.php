<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CmsOrder
 *
 * @ORM\Table(name="cms_order")
 * @ORM\Entity
 */
class CmsOrder
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $orderCode
     *
     * @ORM\Column(name="order_code", type="string", length=255, nullable=true)
     */
    private $orderCode;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var integer $idSource
     *
     * @ORM\Column(name="id_source", type="integer", nullable=true)
     */
    private $idSource;

    /**
     * @var string $currencyCode
     *
     * @ORM\Column(name="currency_code", type="string", length=50, nullable=true)
     */
    private $currencyCode;

    /**
     * @var string $languageCode
     *
     * @ORM\Column(name="language_code", type="string", length=50, nullable=true)
     */
    private $languageCode;

    /**
     * @var float $taxAmount
     *
     * @ORM\Column(name="tax_amount", type="float", nullable=true)
     */
    private $taxAmount;

    /**
     * @var string $userIp
     *
     * @ORM\Column(name="user_ip", type="string", length=50, nullable=true)
     */
    private $userIp;

    /**
     * @var string $fullName
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string $telephoneNumber
     *
     * @ORM\Column(name="telephone_number", type="string", length=255, nullable=true)
     */
    private $telephoneNumber;

    /**
     * @var string $previousOrderNumber
     *
     * @ORM\Column(name="previous_order_number", type="string", length=255, nullable=true)
     */
    private $previousOrderNumber;

    /**
     * @var string $emailAddress
     *
     * @ORM\Column(name="email_address", type="string", length=255, nullable=true)
     */
    private $emailAddress;

    /**
     * @var string $invoiceFirstName
     *
     * @ORM\Column(name="invoice_first_name", type="string", length=255, nullable=true)
     */
    private $invoiceFirstName;

    /**
     * @var string $invoiceSurName
     *
     * @ORM\Column(name="invoice_sur_name", type="string", length=255, nullable=true)
     */
    private $invoiceSurName;

    /**
     * @var string $invoiceStreet
     *
     * @ORM\Column(name="invoice_street", type="string", length=255, nullable=true)
     */
    private $invoiceStreet;

    /**
     * @var string $invoiceCity
     *
     * @ORM\Column(name="invoice_city", type="string", length=255, nullable=true)
     */
    private $invoiceCity;

    /**
     * @var string $invoicePostCode
     *
     * @ORM\Column(name="invoice_post_code", type="string", length=255, nullable=true)
     */
    private $invoicePostCode;

    /**
     * @var string $invoiceCountryCode
     *
     * @ORM\Column(name="invoice_country_code", type="string", length=255, nullable=true)
     */
    private $invoiceCountryCode;

    /**
     * @var boolean $shippingIsSameInvoice
     *
     * @ORM\Column(name="shipping_is_same_invoice", type="boolean", nullable=true)
     */
    private $shippingIsSameInvoice;

    /**
     * @var string $shippingStreet
     *
     * @ORM\Column(name="shipping_street", type="string", length=255, nullable=true)
     */
    private $shippingStreet;

    /**
     * @var string $shippingCity
     *
     * @ORM\Column(name="shipping_city", type="string", length=255, nullable=true)
     */
    private $shippingCity;

    /**
     * @var string $shippingPostCode
     *
     * @ORM\Column(name="shipping_post_code", type="string", length=255, nullable=true)
     */
    private $shippingPostCode;

    /**
     * @var string $shippingCountryCode
     *
     * @ORM\Column(name="shipping_country_code", type="string", length=255, nullable=true)
     */
    private $shippingCountryCode;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var float $totalWithoutTax
     *
     * @ORM\Column(name="total_without_tax", type="float", nullable=true)
     */
    private $totalWithoutTax;

    /**
     * @var float $totalWithTax
     *
     * @ORM\Column(name="total_with_tax", type="float", nullable=true)
     */
    private $totalWithTax;

    /**
     * @var float $totalPayment
     *
     * @ORM\Column(name="total_payment", type="float", nullable=true)
     */
    private $totalPayment;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="datetime", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var string $birthday
     *
     * @ORM\Column(name="birthday", type="string", length=255, nullable=true)
     */
    private $birthday;

    /**
     * @var string $shippingFirstName
     *
     * @ORM\Column(name="shipping_first_name", type="string", length=255, nullable=true)
     */
    private $shippingFirstName;

    /**
     * @var string $shippingSurName
     *
     * @ORM\Column(name="shipping_sur_name", type="string", length=255, nullable=true)
     */
    private $shippingSurName;

    /**
     * @var string $couponCode
     *
     * @ORM\Column(name="coupon_code", type="string", length=255, nullable=true)
     */
    private $couponCode;

    /**
     * @var float $subtotalAmount
     *
     * @ORM\Column(name="subtotal_amount", type="float", nullable=true)
     */
    private $subtotalAmount;

    /**
     * @var float $discountAmount
     *
     * @ORM\Column(name="discount_amount", type="float", nullable=true)
     */
    private $discountAmount;

    /**
     * @var float $totalAmount
     *
     * @ORM\Column(name="total_amount", type="float", nullable=true)
     */
    private $totalAmount;

    /**
     * @var float $grandTotalAmount
     *
     * @ORM\Column(name="grand_total_amount", type="float", nullable=true)
     */
    private $grandTotalAmount;

    /**
     * @var string $websiteName
     *
     * @ORM\Column(name="website_name", type="string", length=255, nullable=true)
     */
    private $websiteName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderCode
     *
     * @param string $orderCode
     * @return CmsOrder
     */
    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;
    
        return $this;
    }

    /**
     * Get orderCode
     *
     * @return string 
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return CmsOrder
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set idSource
     *
     * @param integer $idSource
     * @return CmsOrder
     */
    public function setIdSource($idSource)
    {
        $this->idSource = $idSource;
    
        return $this;
    }

    /**
     * Get idSource
     *
     * @return integer 
     */
    public function getIdSource()
    {
        return $this->idSource;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     * @return CmsOrder
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    
        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string 
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set languageCode
     *
     * @param string $languageCode
     * @return CmsOrder
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;
    
        return $this;
    }

    /**
     * Get languageCode
     *
     * @return string 
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * Set taxAmount
     *
     * @param float $taxAmount
     * @return CmsOrder
     */
    public function setTaxAmount($taxAmount)
    {
        $this->taxAmount = $taxAmount;
    
        return $this;
    }

    /**
     * Get taxAmount
     *
     * @return float 
     */
    public function getTaxAmount()
    {
        return $this->taxAmount;
    }

    /**
     * Set userIp
     *
     * @param string $userIp
     * @return CmsOrder
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
    
        return $this;
    }

    /**
     * Get userIp
     *
     * @return string 
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return CmsOrder
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set telephoneNumber
     *
     * @param string $telephoneNumber
     * @return CmsOrder
     */
    public function setTelephoneNumber($telephoneNumber)
    {
        $this->telephoneNumber = $telephoneNumber;
    
        return $this;
    }

    /**
     * Get telephoneNumber
     *
     * @return string 
     */
    public function getTelephoneNumber()
    {
        return $this->telephoneNumber;
    }

    /**
     * Set previousOrderNumber
     *
     * @param string $previousOrderNumber
     * @return CmsOrder
     */
    public function setPreviousOrderNumber($previousOrderNumber)
    {
        $this->previousOrderNumber = $previousOrderNumber;
    
        return $this;
    }

    /**
     * Get previousOrderNumber
     *
     * @return string 
     */
    public function getPreviousOrderNumber()
    {
        return $this->previousOrderNumber;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return CmsOrder
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    
        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set invoiceFirstName
     *
     * @param string $invoiceFirstName
     * @return CmsOrder
     */
    public function setInvoiceFirstName($invoiceFirstName)
    {
        $this->invoiceFirstName = $invoiceFirstName;
    
        return $this;
    }

    /**
     * Get invoiceFirstName
     *
     * @return string 
     */
    public function getInvoiceFirstName()
    {
        return $this->invoiceFirstName;
    }

    /**
     * Set invoiceSurName
     *
     * @param string $invoiceSurName
     * @return CmsOrder
     */
    public function setInvoiceSurName($invoiceSurName)
    {
        $this->invoiceSurName = $invoiceSurName;
    
        return $this;
    }

    /**
     * Get invoiceSurName
     *
     * @return string 
     */
    public function getInvoiceSurName()
    {
        return $this->invoiceSurName;
    }

    /**
     * Set invoiceStreet
     *
     * @param string $invoiceStreet
     * @return CmsOrder
     */
    public function setInvoiceStreet($invoiceStreet)
    {
        $this->invoiceStreet = $invoiceStreet;
    
        return $this;
    }

    /**
     * Get invoiceStreet
     *
     * @return string 
     */
    public function getInvoiceStreet()
    {
        return $this->invoiceStreet;
    }

    /**
     * Set invoiceCity
     *
     * @param string $invoiceCity
     * @return CmsOrder
     */
    public function setInvoiceCity($invoiceCity)
    {
        $this->invoiceCity = $invoiceCity;
    
        return $this;
    }

    /**
     * Get invoiceCity
     *
     * @return string 
     */
    public function getInvoiceCity()
    {
        return $this->invoiceCity;
    }

    /**
     * Set invoicePostCode
     *
     * @param string $invoicePostCode
     * @return CmsOrder
     */
    public function setInvoicePostCode($invoicePostCode)
    {
        $this->invoicePostCode = $invoicePostCode;
    
        return $this;
    }

    /**
     * Get invoicePostCode
     *
     * @return string 
     */
    public function getInvoicePostCode()
    {
        return $this->invoicePostCode;
    }

    /**
     * Set invoiceCountryCode
     *
     * @param string $invoiceCountryCode
     * @return CmsOrder
     */
    public function setInvoiceCountryCode($invoiceCountryCode)
    {
        $this->invoiceCountryCode = $invoiceCountryCode;
    
        return $this;
    }

    /**
     * Get invoiceCountryCode
     *
     * @return string 
     */
    public function getInvoiceCountryCode()
    {
        return $this->invoiceCountryCode;
    }

    /**
     * Set shippingIsSameInvoice
     *
     * @param boolean $shippingIsSameInvoice
     * @return CmsOrder
     */
    public function setShippingIsSameInvoice($shippingIsSameInvoice)
    {
        $this->shippingIsSameInvoice = $shippingIsSameInvoice;
    
        return $this;
    }

    /**
     * Get shippingIsSameInvoice
     *
     * @return boolean 
     */
    public function getShippingIsSameInvoice()
    {
        return $this->shippingIsSameInvoice;
    }

    /**
     * Set shippingStreet
     *
     * @param string $shippingStreet
     * @return CmsOrder
     */
    public function setShippingStreet($shippingStreet)
    {
        $this->shippingStreet = $shippingStreet;
    
        return $this;
    }

    /**
     * Get shippingStreet
     *
     * @return string 
     */
    public function getShippingStreet()
    {
        return $this->shippingStreet;
    }

    /**
     * Set shippingCity
     *
     * @param string $shippingCity
     * @return CmsOrder
     */
    public function setShippingCity($shippingCity)
    {
        $this->shippingCity = $shippingCity;
    
        return $this;
    }

    /**
     * Get shippingCity
     *
     * @return string 
     */
    public function getShippingCity()
    {
        return $this->shippingCity;
    }

    /**
     * Set shippingPostCode
     *
     * @param string $shippingPostCode
     * @return CmsOrder
     */
    public function setShippingPostCode($shippingPostCode)
    {
        $this->shippingPostCode = $shippingPostCode;
    
        return $this;
    }

    /**
     * Get shippingPostCode
     *
     * @return string 
     */
    public function getShippingPostCode()
    {
        return $this->shippingPostCode;
    }

    /**
     * Set shippingCountryCode
     *
     * @param string $shippingCountryCode
     * @return CmsOrder
     */
    public function setShippingCountryCode($shippingCountryCode)
    {
        $this->shippingCountryCode = $shippingCountryCode;
    
        return $this;
    }

    /**
     * Get shippingCountryCode
     *
     * @return string 
     */
    public function getShippingCountryCode()
    {
        return $this->shippingCountryCode;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CmsOrder
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set totalWithoutTax
     *
     * @param float $totalWithoutTax
     * @return CmsOrder
     */
    public function setTotalWithoutTax($totalWithoutTax)
    {
        $this->totalWithoutTax = $totalWithoutTax;
    
        return $this;
    }

    /**
     * Get totalWithoutTax
     *
     * @return float 
     */
    public function getTotalWithoutTax()
    {
        return $this->totalWithoutTax;
    }

    /**
     * Set totalWithTax
     *
     * @param float $totalWithTax
     * @return CmsOrder
     */
    public function setTotalWithTax($totalWithTax)
    {
        $this->totalWithTax = $totalWithTax;
    
        return $this;
    }

    /**
     * Get totalWithTax
     *
     * @return float 
     */
    public function getTotalWithTax()
    {
        return $this->totalWithTax;
    }

    /**
     * Set totalPayment
     *
     * @param float $totalPayment
     * @return CmsOrder
     */
    public function setTotalPayment($totalPayment)
    {
        $this->totalPayment = $totalPayment;
    
        return $this;
    }

    /**
     * Get totalPayment
     *
     * @return float 
     */
    public function getTotalPayment()
    {
        return $this->totalPayment;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return CmsOrder
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return CmsOrder
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set birthday
     *
     * @param string $birthday
     * @return CmsOrder
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return string 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set shippingFirstName
     *
     * @param string $shippingFirstName
     * @return CmsOrder
     */
    public function setShippingFirstName($shippingFirstName)
    {
        $this->shippingFirstName = $shippingFirstName;
    
        return $this;
    }

    /**
     * Get shippingFirstName
     *
     * @return string 
     */
    public function getShippingFirstName()
    {
        return $this->shippingFirstName;
    }

    /**
     * Set shippingSurName
     *
     * @param string $shippingSurName
     * @return CmsOrder
     */
    public function setShippingSurName($shippingSurName)
    {
        $this->shippingSurName = $shippingSurName;
    
        return $this;
    }

    /**
     * Get shippingSurName
     *
     * @return string 
     */
    public function getShippingSurName()
    {
        return $this->shippingSurName;
    }

    /**
     * Set couponCode
     *
     * @param string $couponCode
     * @return CmsOrder
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    
        return $this;
    }

    /**
     * Get couponCode
     *
     * @return string 
     */
    public function getCouponCode()
    {
        return $this->couponCode;
    }

    /**
     * Set subtotalAmount
     *
     * @param float $subtotalAmount
     * @return CmsOrder
     */
    public function setSubtotalAmount($subtotalAmount)
    {
        $this->subtotalAmount = $subtotalAmount;
    
        return $this;
    }

    /**
     * Get subtotalAmount
     *
     * @return float 
     */
    public function getSubtotalAmount()
    {
        return $this->subtotalAmount;
    }

    /**
     * Set discountAmount
     *
     * @param float $discountAmount
     * @return CmsOrder
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;
    
        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float 
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     * @return CmsOrder
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    
        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float 
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set grandTotalAmount
     *
     * @param float $grandTotalAmount
     * @return CmsOrder
     */
    public function setGrandTotalAmount($grandTotalAmount)
    {
        $this->grandTotalAmount = $grandTotalAmount;
    
        return $this;
    }

    /**
     * Get grandTotalAmount
     *
     * @return float 
     */
    public function getGrandTotalAmount()
    {
        return $this->grandTotalAmount;
    }

    /**
     * Set websiteName
     *
     * @param string $websiteName
     * @return CmsOrder
     */
    public function setWebsiteName($websiteName)
    {
        $this->websiteName = $websiteName;
    
        return $this;
    }

    /**
     * Get websiteName
     *
     * @return string 
     */
    public function getWebsiteName()
    {
        return $this->websiteName;
    }
}