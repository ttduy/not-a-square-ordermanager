<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormQuestion
 *
 * @ORM\Table(name="form_question")
 * @ORM\Entity
 */
class FormQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var string $question
     *
     * @ORM\Column(name="question", type="string", length=255, nullable=true)
     */
    private $question;

    /**
     * @var string $questionData
     *
     * @ORM\Column(name="question_data", type="text", nullable=true)
     */
    private $questionData;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var string $reportKey
     *
     * @ORM\Column(name="report_key", type="string", length=255, nullable=true)
     */
    private $reportKey;

    /**
     * @var boolean $isHideCustomerDashboard
     *
     * @ORM\Column(name="is_hide_customer_dashboard", type="boolean", nullable=true)
     */
    private $isHideCustomerDashboard;

    /**
     * @var string $questionI18n
     *
     * @ORM\Column(name="question_i18n", type="string", length=255, nullable=true)
     */
    private $questionI18n;

    /**
     * @var string $questionDataI18n
     *
     * @ORM\Column(name="question_data_i18n", type="text", nullable=true)
     */
    private $questionDataI18n;

    /**
     * @var boolean $isRequired
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=true)
     */
    private $isRequired;

    /**
     * @var string $hideFromPartners
     *
     * @ORM\Column(name="hide_from_partners", type="text", nullable=true)
     */
    private $hideFromPartners;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return FormQuestion
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return FormQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set questionData
     *
     * @param string $questionData
     * @return FormQuestion
     */
    public function setQuestionData($questionData)
    {
        $this->questionData = $questionData;
    
        return $this;
    }

    /**
     * Get questionData
     *
     * @return string 
     */
    public function getQuestionData()
    {
        return $this->questionData;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return FormQuestion
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set reportKey
     *
     * @param string $reportKey
     * @return FormQuestion
     */
    public function setReportKey($reportKey)
    {
        $this->reportKey = $reportKey;
    
        return $this;
    }

    /**
     * Get reportKey
     *
     * @return string 
     */
    public function getReportKey()
    {
        return $this->reportKey;
    }

    /**
     * Set isHideCustomerDashboard
     *
     * @param boolean $isHideCustomerDashboard
     * @return FormQuestion
     */
    public function setIsHideCustomerDashboard($isHideCustomerDashboard)
    {
        $this->isHideCustomerDashboard = $isHideCustomerDashboard;
    
        return $this;
    }

    /**
     * Get isHideCustomerDashboard
     *
     * @return boolean 
     */
    public function getIsHideCustomerDashboard()
    {
        return $this->isHideCustomerDashboard;
    }

    /**
     * Set questionI18n
     *
     * @param string $questionI18n
     * @return FormQuestion
     */
    public function setQuestionI18n($questionI18n)
    {
        $this->questionI18n = $questionI18n;
    
        return $this;
    }

    /**
     * Get questionI18n
     *
     * @return string 
     */
    public function getQuestionI18n()
    {
        return $this->questionI18n;
    }

    /**
     * Set questionDataI18n
     *
     * @param string $questionDataI18n
     * @return FormQuestion
     */
    public function setQuestionDataI18n($questionDataI18n)
    {
        $this->questionDataI18n = $questionDataI18n;
    
        return $this;
    }

    /**
     * Get questionDataI18n
     *
     * @return string 
     */
    public function getQuestionDataI18n()
    {
        return $this->questionDataI18n;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     * @return FormQuestion
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;
    
        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean 
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set hideFromPartners
     *
     * @param string $hideFromPartners
     * @return FormQuestion
     */
    public function setHideFromPartners($hideFromPartners)
    {
        $this->hideFromPartners = $hideFromPartners;
    
        return $this;
    }

    /**
     * Get hideFromPartners
     *
     * @return string 
     */
    public function getHideFromPartners()
    {
        return $this->hideFromPartners;
    }
}