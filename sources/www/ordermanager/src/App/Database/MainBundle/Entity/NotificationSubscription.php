<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\NotificationSubscription
 *
 * @ORM\Table(name="notification_subscription")
 * @ORM\Entity
 */
class NotificationSubscription
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idUser
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var string $idTopics
     *
     * @ORM\Column(name="id_topics", type="text", nullable=true)
     */
    private $idTopics;

    /**
     * @var boolean $subscribeAll
     *
     * @ORM\Column(name="subscribe_all", type="boolean", nullable=true)
     */
    private $subscribeAll;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return NotificationSubscription
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idTopics
     *
     * @param string $idTopics
     * @return NotificationSubscription
     */
    public function setIdTopics($idTopics)
    {
        $this->idTopics = $idTopics;
    
        return $this;
    }

    /**
     * Get idTopics
     *
     * @return string 
     */
    public function getIdTopics()
    {
        return $this->idTopics;
    }

    /**
     * Set subscribeAll
     *
     * @param boolean $subscribeAll
     * @return NotificationSubscription
     */
    public function setSubscribeAll($subscribeAll)
    {
        $this->subscribeAll = $subscribeAll;
    
        return $this;
    }

    /**
     * Get subscribeAll
     *
     * @return boolean 
     */
    public function getSubscribeAll()
    {
        return $this->subscribeAll;
    }
}