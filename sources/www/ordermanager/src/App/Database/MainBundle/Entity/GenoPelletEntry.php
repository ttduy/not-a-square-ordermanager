<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPelletEntry
 *
 * @ORM\Table(name="geno_pellet_entry")
 * @ORM\Entity
 */
class GenoPelletEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPellet
     *
     * @ORM\Column(name="id_pellet", type="integer", nullable=true)
     */
    private $idPellet;

    /**
     * @var \DateTime $orderedDate
     *
     * @ORM\Column(name="ordered_date", type="date", nullable=true)
     */
    private $orderedDate;

    /**
     * @var string $orderedBy
     *
     * @ORM\Column(name="ordered_by", type="string", length=255, nullable=true)
     */
    private $orderedBy;

    /**
     * @var \DateTime $receivedDate
     *
     * @ORM\Column(name="received_date", type="date", nullable=true)
     */
    private $receivedDate;

    /**
     * @var boolean $received
     *
     * @ORM\Column(name="received", type="boolean", nullable=true)
     */
    private $received;

    /**
     * @var string $receivedBy
     *
     * @ORM\Column(name="received_by", type="string", length=255, nullable=true)
     */
    private $receivedBy;

    /**
     * @var \DateTime $expiryDate
     *
     * @ORM\Column(name="expiry_date", type="date", nullable=true)
     */
    private $expiryDate;

    /**
     * @var boolean $expiryDateWarningDisabled
     *
     * @ORM\Column(name="expiry_date_warning_disabled", type="boolean", nullable=true)
     */
    private $expiryDateWarningDisabled;

    /**
     * @var float $loading
     *
     * @ORM\Column(name="loading", type="float", nullable=true)
     */
    private $loading;

    /**
     * @var integer $section
     *
     * @ORM\Column(name="section", type="integer", nullable=true)
     */
    private $section;

    /**
     * @var string $note
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var boolean $isSelected
     *
     * @ORM\Column(name="is_selected", type="boolean", nullable=true)
     */
    private $isSelected;

    /**
     * @var \DateTime $dateTaken
     *
     * @ORM\Column(name="date_taken", type="date", nullable=true)
     */
    private $dateTaken;

    /**
     * @var string $usedFor
     *
     * @ORM\Column(name="used_for", type="string", length=255, nullable=true)
     */
    private $usedFor;

    /**
     * @var string $takenBy
     *
     * @ORM\Column(name="taken_by", type="string", length=255, nullable=true)
     */
    private $takenBy;

    /**
     * @var float $ordered
     *
     * @ORM\Column(name="ordered", type="float", nullable=true)
     */
    private $ordered;

    /**
     * @var string $shipment
     *
     * @ORM\Column(name="shipment", type="string", length=30, nullable=true)
     */
    private $shipment;

    /**
     * @var integer $idSuperEntry
     *
     * @ORM\Column(name="id_super_entry", type="integer", nullable=true)
     */
    private $idSuperEntry;

    /**
     * @var string $exterialLot
     *
     * @ORM\Column(name="exterial_lot", type="string", length=255, nullable=true)
     */
    private $exterialLot;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=50, nullable=true)
     */
    private $attachmentKey;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPellet
     *
     * @param integer $idPellet
     * @return GenoPelletEntry
     */
    public function setIdPellet($idPellet)
    {
        $this->idPellet = $idPellet;
    
        return $this;
    }

    /**
     * Get idPellet
     *
     * @return integer 
     */
    public function getIdPellet()
    {
        return $this->idPellet;
    }

    /**
     * Set orderedDate
     *
     * @param \DateTime $orderedDate
     * @return GenoPelletEntry
     */
    public function setOrderedDate($orderedDate)
    {
        $this->orderedDate = $orderedDate;
    
        return $this;
    }

    /**
     * Get orderedDate
     *
     * @return \DateTime 
     */
    public function getOrderedDate()
    {
        return $this->orderedDate;
    }

    /**
     * Set orderedBy
     *
     * @param string $orderedBy
     * @return GenoPelletEntry
     */
    public function setOrderedBy($orderedBy)
    {
        $this->orderedBy = $orderedBy;
    
        return $this;
    }

    /**
     * Get orderedBy
     *
     * @return string 
     */
    public function getOrderedBy()
    {
        return $this->orderedBy;
    }

    /**
     * Set receivedDate
     *
     * @param \DateTime $receivedDate
     * @return GenoPelletEntry
     */
    public function setReceivedDate($receivedDate)
    {
        $this->receivedDate = $receivedDate;
    
        return $this;
    }

    /**
     * Get receivedDate
     *
     * @return \DateTime 
     */
    public function getReceivedDate()
    {
        return $this->receivedDate;
    }

    /**
     * Set received
     *
     * @param boolean $received
     * @return GenoPelletEntry
     */
    public function setReceived($received)
    {
        $this->received = $received;
    
        return $this;
    }

    /**
     * Get received
     *
     * @return boolean 
     */
    public function getReceived()
    {
        return $this->received;
    }

    /**
     * Set receivedBy
     *
     * @param string $receivedBy
     * @return GenoPelletEntry
     */
    public function setReceivedBy($receivedBy)
    {
        $this->receivedBy = $receivedBy;
    
        return $this;
    }

    /**
     * Get receivedBy
     *
     * @return string 
     */
    public function getReceivedBy()
    {
        return $this->receivedBy;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return GenoPelletEntry
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set expiryDateWarningDisabled
     *
     * @param boolean $expiryDateWarningDisabled
     * @return GenoPelletEntry
     */
    public function setExpiryDateWarningDisabled($expiryDateWarningDisabled)
    {
        $this->expiryDateWarningDisabled = $expiryDateWarningDisabled;
    
        return $this;
    }

    /**
     * Get expiryDateWarningDisabled
     *
     * @return boolean 
     */
    public function getExpiryDateWarningDisabled()
    {
        return $this->expiryDateWarningDisabled;
    }

    /**
     * Set loading
     *
     * @param float $loading
     * @return GenoPelletEntry
     */
    public function setLoading($loading)
    {
        $this->loading = $loading;
    
        return $this;
    }

    /**
     * Get loading
     *
     * @return float 
     */
    public function getLoading()
    {
        return $this->loading;
    }

    /**
     * Set section
     *
     * @param integer $section
     * @return GenoPelletEntry
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return integer 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return GenoPelletEntry
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set isSelected
     *
     * @param boolean $isSelected
     * @return GenoPelletEntry
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;
    
        return $this;
    }

    /**
     * Get isSelected
     *
     * @return boolean 
     */
    public function getIsSelected()
    {
        return $this->isSelected;
    }

    /**
     * Set dateTaken
     *
     * @param \DateTime $dateTaken
     * @return GenoPelletEntry
     */
    public function setDateTaken($dateTaken)
    {
        $this->dateTaken = $dateTaken;
    
        return $this;
    }

    /**
     * Get dateTaken
     *
     * @return \DateTime 
     */
    public function getDateTaken()
    {
        return $this->dateTaken;
    }

    /**
     * Set usedFor
     *
     * @param string $usedFor
     * @return GenoPelletEntry
     */
    public function setUsedFor($usedFor)
    {
        $this->usedFor = $usedFor;
    
        return $this;
    }

    /**
     * Get usedFor
     *
     * @return string 
     */
    public function getUsedFor()
    {
        return $this->usedFor;
    }

    /**
     * Set takenBy
     *
     * @param string $takenBy
     * @return GenoPelletEntry
     */
    public function setTakenBy($takenBy)
    {
        $this->takenBy = $takenBy;
    
        return $this;
    }

    /**
     * Get takenBy
     *
     * @return string 
     */
    public function getTakenBy()
    {
        return $this->takenBy;
    }

    /**
     * Set ordered
     *
     * @param float $ordered
     * @return GenoPelletEntry
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;
    
        return $this;
    }

    /**
     * Get ordered
     *
     * @return float 
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * Set shipment
     *
     * @param string $shipment
     * @return GenoPelletEntry
     */
    public function setShipment($shipment)
    {
        $this->shipment = $shipment;
    
        return $this;
    }

    /**
     * Get shipment
     *
     * @return string 
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set idSuperEntry
     *
     * @param integer $idSuperEntry
     * @return GenoPelletEntry
     */
    public function setIdSuperEntry($idSuperEntry)
    {
        $this->idSuperEntry = $idSuperEntry;
    
        return $this;
    }

    /**
     * Get idSuperEntry
     *
     * @return integer 
     */
    public function getIdSuperEntry()
    {
        return $this->idSuperEntry;
    }

    /**
     * Set exterialLot
     *
     * @param string $exterialLot
     * @return GenoPelletEntry
     */
    public function setExterialLot($exterialLot)
    {
        $this->exterialLot = $exterialLot;
    
        return $this;
    }

    /**
     * Get exterialLot
     *
     * @return string 
     */
    public function getExterialLot()
    {
        return $this->exterialLot;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return GenoPelletEntry
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }
}