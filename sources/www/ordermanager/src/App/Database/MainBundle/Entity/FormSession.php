<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormSession
 *
 * @ORM\Table(name="form_session")
 * @ORM\Entity
 */
class FormSession
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFormVersion
     *
     * @ORM\Column(name="id_form_version", type="integer", nullable=true)
     */
    private $idFormVersion;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $idActiveFormStep
     *
     * @ORM\Column(name="id_active_form_step", type="integer", nullable=true)
     */
    private $idActiveFormStep;

    /**
     * @var string $currentLots
     *
     * @ORM\Column(name="current_lots", type="text", nullable=true)
     */
    private $currentLots;

    /**
     * @var \DateTime $currentLotsTimestamp
     *
     * @ORM\Column(name="current_lots_timestamp", type="datetime", nullable=true)
     */
    private $currentLotsTimestamp;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $additionalInformation
     *
     * @ORM\Column(name="additional_information", type="text", nullable=true)
     */
    private $additionalInformation;

    /**
     * @var string $note
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormVersion
     *
     * @param integer $idFormVersion
     * @return FormSession
     */
    public function setIdFormVersion($idFormVersion)
    {
        $this->idFormVersion = $idFormVersion;
    
        return $this;
    }

    /**
     * Get idFormVersion
     *
     * @return integer 
     */
    public function getIdFormVersion()
    {
        return $this->idFormVersion;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return FormSession
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return FormSession
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set idActiveFormStep
     *
     * @param integer $idActiveFormStep
     * @return FormSession
     */
    public function setIdActiveFormStep($idActiveFormStep)
    {
        $this->idActiveFormStep = $idActiveFormStep;
    
        return $this;
    }

    /**
     * Get idActiveFormStep
     *
     * @return integer 
     */
    public function getIdActiveFormStep()
    {
        return $this->idActiveFormStep;
    }

    /**
     * Set currentLots
     *
     * @param string $currentLots
     * @return FormSession
     */
    public function setCurrentLots($currentLots)
    {
        $this->currentLots = $currentLots;
    
        return $this;
    }

    /**
     * Get currentLots
     *
     * @return string 
     */
    public function getCurrentLots()
    {
        return $this->currentLots;
    }

    /**
     * Set currentLotsTimestamp
     *
     * @param \DateTime $currentLotsTimestamp
     * @return FormSession
     */
    public function setCurrentLotsTimestamp($currentLotsTimestamp)
    {
        $this->currentLotsTimestamp = $currentLotsTimestamp;
    
        return $this;
    }

    /**
     * Get currentLotsTimestamp
     *
     * @return \DateTime 
     */
    public function getCurrentLotsTimestamp()
    {
        return $this->currentLotsTimestamp;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return FormSession
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set additionalInformation
     *
     * @param string $additionalInformation
     * @return FormSession
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    
        return $this;
    }

    /**
     * Get additionalInformation
     *
     * @return string 
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return FormSession
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }
}