<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\InvoicePdf
 *
 * @ORM\Table(name="invoice_pdf")
 * @ORM\Entity
 */
class InvoicePdf
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idInvoice
     *
     * @ORM\Column(name="id_invoice", type="integer", nullable=true)
     */
    private $idInvoice;

    /**
     * @var \DateTime $generateDate
     *
     * @ORM\Column(name="generate_date", type="datetime", nullable=true)
     */
    private $generateDate;

    /**
     * @var string $invoiceFile
     *
     * @ORM\Column(name="invoice_file", type="string", length=500, nullable=true)
     */
    private $invoiceFile;

    /**
     * @var float $totalAmount
     *
     * @ORM\Column(name="total_amount", type="decimal", nullable=true)
     */
    private $totalAmount;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInvoice
     *
     * @param integer $idInvoice
     * @return InvoicePdf
     */
    public function setIdInvoice($idInvoice)
    {
        $this->idInvoice = $idInvoice;
    
        return $this;
    }

    /**
     * Get idInvoice
     *
     * @return integer 
     */
    public function getIdInvoice()
    {
        return $this->idInvoice;
    }

    /**
     * Set generateDate
     *
     * @param \DateTime $generateDate
     * @return InvoicePdf
     */
    public function setGenerateDate($generateDate)
    {
        $this->generateDate = $generateDate;
    
        return $this;
    }

    /**
     * Get generateDate
     *
     * @return \DateTime 
     */
    public function getGenerateDate()
    {
        return $this->generateDate;
    }

    /**
     * Set invoiceFile
     *
     * @param string $invoiceFile
     * @return InvoicePdf
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;
    
        return $this;
    }

    /**
     * Get invoiceFile
     *
     * @return string 
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     * @return InvoicePdf
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    
        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float 
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }
}