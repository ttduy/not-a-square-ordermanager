<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\LeadStatus
 *
 * @ORM\Table(name="lead_status")
 * @ORM\Entity
 */
class LeadStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLead
     *
     * @ORM\Column(name="id_lead", type="integer", nullable=true)
     */
    private $idLead;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var integer $dateDiff
     *
     * @ORM\Column(name="date_diff", type="integer", nullable=true)
     */
    private $dateDiff;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLead
     *
     * @param integer $idLead
     * @return LeadStatus
     */
    public function setIdLead($idLead)
    {
        $this->idLead = $idLead;
    
        return $this;
    }

    /**
     * Get idLead
     *
     * @return integer 
     */
    public function getIdLead()
    {
        return $this->idLead;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return LeadStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return LeadStatus
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return LeadStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set dateDiff
     *
     * @param integer $dateDiff
     * @return LeadStatus
     */
    public function setDateDiff($dateDiff)
    {
        $this->dateDiff = $dateDiff;
    
        return $this;
    }

    /**
     * Get dateDiff
     *
     * @return integer 
     */
    public function getDateDiff()
    {
        return $this->dateDiff;
    }
}