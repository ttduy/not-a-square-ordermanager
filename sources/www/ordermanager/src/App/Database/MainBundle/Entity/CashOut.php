<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CashOut
 *
 * @ORM\Table(name="cash_out")
 * @ORM\Entity
 */
class CashOut
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCostTrackerSector
     *
     * @ORM\Column(name="id_cost_tracker_sector", type="integer", nullable=true)
     */
    private $idCostTrackerSector;

    /**
     * @var string $designation
     *
     * @ORM\Column(name="designation", type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @var \DateTime $recordedDate
     *
     * @ORM\Column(name="recorded_date", type="date", nullable=false)
     */
    private $recordedDate;

    /**
     * @var integer $recordedMonth
     *
     * @ORM\Column(name="recorded_month", type="integer", nullable=false)
     */
    private $recordedMonth;

    /**
     * @var integer $recordedYear
     *
     * @ORM\Column(name="recorded_year", type="integer", nullable=false)
     */
    private $recordedYear;

    /**
     * @var \DateTime $postingDate
     *
     * @ORM\Column(name="posting_date", type="date", nullable=true)
     */
    private $postingDate;

    /**
     * @var integer $postingMonth
     *
     * @ORM\Column(name="posting_month", type="integer", nullable=true)
     */
    private $postingMonth;

    /**
     * @var integer $postingYear
     *
     * @ORM\Column(name="posting_year", type="integer", nullable=true)
     */
    private $postingYear;

    /**
     * @var float $cashAmount
     *
     * @ORM\Column(name="cash_amount", type="decimal", nullable=false)
     */
    private $cashAmount;

    /**
     * @var string $currency
     *
     * @ORM\Column(name="currency", type="string", length=10, nullable=true)
     */
    private $currency;

    /**
     * @var string $partnerName
     *
     * @ORM\Column(name="partner_name", type="string", length=255, nullable=true)
     */
    private $partnerName;

    /**
     * @var string $reference
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var string $saleTarget
     *
     * @ORM\Column(name="sale_target", type="string", length=255, nullable=true)
     */
    private $saleTarget;

    /**
     * @var string $partner
     *
     * @ORM\Column(name="partner", type="string", length=255, nullable=true)
     */
    private $partner;

    /**
     * @var string $transactionContent
     *
     * @ORM\Column(name="transaction_content", type="text", nullable=true)
     */
    private $transactionContent;

    /**
     * @var string $transactionId
     *
     * @ORM\Column(name="transaction_id", type="string", length=100, nullable=true)
     */
    private $transactionId;

    /**
     * @var string $companyName
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string $companyId
     *
     * @ORM\Column(name="company_id", type="string", length=100, nullable=true)
     */
    private $companyId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCostTrackerSector
     *
     * @param integer $idCostTrackerSector
     * @return CashOut
     */
    public function setIdCostTrackerSector($idCostTrackerSector)
    {
        $this->idCostTrackerSector = $idCostTrackerSector;
    
        return $this;
    }

    /**
     * Get idCostTrackerSector
     *
     * @return integer 
     */
    public function getIdCostTrackerSector()
    {
        return $this->idCostTrackerSector;
    }

    /**
     * Set designation
     *
     * @param string $designation
     * @return CashOut
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string 
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set recordedDate
     *
     * @param \DateTime $recordedDate
     * @return CashOut
     */
    public function setRecordedDate($recordedDate)
    {
        $this->recordedDate = $recordedDate;
    
        return $this;
    }

    /**
     * Get recordedDate
     *
     * @return \DateTime 
     */
    public function getRecordedDate()
    {
        return $this->recordedDate;
    }

    /**
     * Set recordedMonth
     *
     * @param integer $recordedMonth
     * @return CashOut
     */
    public function setRecordedMonth($recordedMonth)
    {
        $this->recordedMonth = $recordedMonth;
    
        return $this;
    }

    /**
     * Get recordedMonth
     *
     * @return integer 
     */
    public function getRecordedMonth()
    {
        return $this->recordedMonth;
    }

    /**
     * Set recordedYear
     *
     * @param integer $recordedYear
     * @return CashOut
     */
    public function setRecordedYear($recordedYear)
    {
        $this->recordedYear = $recordedYear;
    
        return $this;
    }

    /**
     * Get recordedYear
     *
     * @return integer 
     */
    public function getRecordedYear()
    {
        return $this->recordedYear;
    }

    /**
     * Set postingDate
     *
     * @param \DateTime $postingDate
     * @return CashOut
     */
    public function setPostingDate($postingDate)
    {
        $this->postingDate = $postingDate;
    
        return $this;
    }

    /**
     * Get postingDate
     *
     * @return \DateTime 
     */
    public function getPostingDate()
    {
        return $this->postingDate;
    }

    /**
     * Set postingMonth
     *
     * @param integer $postingMonth
     * @return CashOut
     */
    public function setPostingMonth($postingMonth)
    {
        $this->postingMonth = $postingMonth;
    
        return $this;
    }

    /**
     * Get postingMonth
     *
     * @return integer 
     */
    public function getPostingMonth()
    {
        return $this->postingMonth;
    }

    /**
     * Set postingYear
     *
     * @param integer $postingYear
     * @return CashOut
     */
    public function setPostingYear($postingYear)
    {
        $this->postingYear = $postingYear;
    
        return $this;
    }

    /**
     * Get postingYear
     *
     * @return integer 
     */
    public function getPostingYear()
    {
        return $this->postingYear;
    }

    /**
     * Set cashAmount
     *
     * @param float $cashAmount
     * @return CashOut
     */
    public function setCashAmount($cashAmount)
    {
        $this->cashAmount = $cashAmount;
    
        return $this;
    }

    /**
     * Get cashAmount
     *
     * @return float 
     */
    public function getCashAmount()
    {
        return $this->cashAmount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return CashOut
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set partnerName
     *
     * @param string $partnerName
     * @return CashOut
     */
    public function setPartnerName($partnerName)
    {
        $this->partnerName = $partnerName;
    
        return $this;
    }

    /**
     * Get partnerName
     *
     * @return string 
     */
    public function getPartnerName()
    {
        return $this->partnerName;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return CashOut
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set saleTarget
     *
     * @param string $saleTarget
     * @return CashOut
     */
    public function setSaleTarget($saleTarget)
    {
        $this->saleTarget = $saleTarget;
    
        return $this;
    }

    /**
     * Get saleTarget
     *
     * @return string 
     */
    public function getSaleTarget()
    {
        return $this->saleTarget;
    }

    /**
     * Set partner
     *
     * @param string $partner
     * @return CashOut
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    
        return $this;
    }

    /**
     * Get partner
     *
     * @return string 
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set transactionContent
     *
     * @param string $transactionContent
     * @return CashOut
     */
    public function setTransactionContent($transactionContent)
    {
        $this->transactionContent = $transactionContent;
    
        return $this;
    }

    /**
     * Get transactionContent
     *
     * @return string 
     */
    public function getTransactionContent()
    {
        return $this->transactionContent;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     * @return CashOut
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    
        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string 
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return CashOut
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    
        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyId
     *
     * @param string $companyId
     * @return CashOut
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    
        return $this;
    }

    /**
     * Get companyId
     *
     * @return string 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }
}