<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WorkReportPriority
 *
 * @ORM\Table(name="work_report_priority")
 * @ORM\Entity
 */
class WorkReportPriority
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWorker
     *
     * @ORM\Column(name="id_worker", type="integer", nullable=true)
     */
    private $idWorker;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $idPriority
     *
     * @ORM\Column(name="id_priority", type="integer", nullable=true)
     */
    private $idPriority;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWorker
     *
     * @param integer $idWorker
     * @return WorkReportPriority
     */
    public function setIdWorker($idWorker)
    {
        $this->idWorker = $idWorker;
    
        return $this;
    }

    /**
     * Get idWorker
     *
     * @return integer 
     */
    public function getIdWorker()
    {
        return $this->idWorker;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return WorkReportPriority
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set idPriority
     *
     * @param integer $idPriority
     * @return WorkReportPriority
     */
    public function setIdPriority($idPriority)
    {
        $this->idPriority = $idPriority;
    
        return $this;
    }

    /**
     * Get idPriority
     *
     * @return integer 
     */
    public function getIdPriority()
    {
        return $this->idPriority;
    }
}