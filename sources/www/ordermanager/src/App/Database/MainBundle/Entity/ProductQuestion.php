<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductQuestion
 *
 * @ORM\Table(name="product_question")
 * @ORM\Entity
 */
class ProductQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $productid
     *
     * @ORM\Column(name="ProductId", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var string $question
     *
     * @ORM\Column(name="Question", type="string", length=255, nullable=false)
     */
    private $question;

    /**
     * @var integer $controltypeid
     *
     * @ORM\Column(name="ControlTypeId", type="integer", nullable=false)
     */
    private $controltypeid;

    /**
     * @var string $controlconfig
     *
     * @ORM\Column(name="ControlConfig", type="text", nullable=true)
     */
    private $controlconfig;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=false)
     */
    private $sortorder;

    /**
     * @var string $reportkey
     *
     * @ORM\Column(name="ReportKey", type="string", length=45, nullable=true)
     */
    private $reportkey;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductQuestion
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    
        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return ProductQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set controltypeid
     *
     * @param integer $controltypeid
     * @return ProductQuestion
     */
    public function setControltypeid($controltypeid)
    {
        $this->controltypeid = $controltypeid;
    
        return $this;
    }

    /**
     * Get controltypeid
     *
     * @return integer 
     */
    public function getControltypeid()
    {
        return $this->controltypeid;
    }

    /**
     * Set controlconfig
     *
     * @param string $controlconfig
     * @return ProductQuestion
     */
    public function setControlconfig($controlconfig)
    {
        $this->controlconfig = $controlconfig;
    
        return $this;
    }

    /**
     * Get controlconfig
     *
     * @return string 
     */
    public function getControlconfig()
    {
        return $this->controlconfig;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return ProductQuestion
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * Set reportkey
     *
     * @param string $reportkey
     * @return ProductQuestion
     */
    public function setReportkey($reportkey)
    {
        $this->reportkey = $reportkey;
    
        return $this;
    }

    /**
     * Get reportkey
     *
     * @return string 
     */
    public function getReportkey()
    {
        return $this->reportkey;
    }
}