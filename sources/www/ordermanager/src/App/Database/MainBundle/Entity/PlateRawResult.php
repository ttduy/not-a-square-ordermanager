<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PlateRawResult
 *
 * @ORM\Table(name="plate_raw_result")
 * @ORM\Entity
 */
class PlateRawResult
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPlate
     *
     * @ORM\Column(name="id_plate", type="integer", nullable=true)
     */
    private $idPlate;

    /**
     * @var string $excelName
     *
     * @ORM\Column(name="excel_name", type="string", length=255, nullable=true)
     */
    private $excelName;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var string $rawData
     *
     * @ORM\Column(name="raw_data", type="text", nullable=true)
     */
    private $rawData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPlate
     *
     * @param integer $idPlate
     * @return PlateRawResult
     */
    public function setIdPlate($idPlate)
    {
        $this->idPlate = $idPlate;
    
        return $this;
    }

    /**
     * Get idPlate
     *
     * @return integer 
     */
    public function getIdPlate()
    {
        return $this->idPlate;
    }

    /**
     * Set excelName
     *
     * @param string $excelName
     * @return PlateRawResult
     */
    public function setExcelName($excelName)
    {
        $this->excelName = $excelName;
    
        return $this;
    }

    /**
     * Get excelName
     *
     * @return string 
     */
    public function getExcelName()
    {
        return $this->excelName;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return PlateRawResult
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set rawData
     *
     * @param string $rawData
     * @return PlateRawResult
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
    
        return $this;
    }

    /**
     * Get rawData
     *
     * @return string 
     */
    public function getRawData()
    {
        return $this->rawData;
    }
}