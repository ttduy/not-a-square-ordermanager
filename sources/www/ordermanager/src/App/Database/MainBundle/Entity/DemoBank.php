<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DemoBank
 *
 * @ORM\Table(name="demo_bank")
 * @ORM\Entity
 */
class DemoBank
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGene
     *
     * @ORM\Column(name="id_gene", type="integer", nullable=true)
     */
    private $idGene;

    /**
     * @var integer $idFamVicMappingRule
     *
     * @ORM\Column(name="id_fam_vic_mapping_rule", type="integer", nullable=true)
     */
    private $idFamVicMappingRule;

    /**
     * @var float $fam
     *
     * @ORM\Column(name="fam", type="float", nullable=true)
     */
    private $fam;

    /**
     * @var float $vic
     *
     * @ORM\Column(name="vic", type="float", nullable=true)
     */
    private $vic;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGene
     *
     * @param integer $idGene
     * @return DemoBank
     */
    public function setIdGene($idGene)
    {
        $this->idGene = $idGene;
    
        return $this;
    }

    /**
     * Get idGene
     *
     * @return integer 
     */
    public function getIdGene()
    {
        return $this->idGene;
    }

    /**
     * Set idFamVicMappingRule
     *
     * @param integer $idFamVicMappingRule
     * @return DemoBank
     */
    public function setIdFamVicMappingRule($idFamVicMappingRule)
    {
        $this->idFamVicMappingRule = $idFamVicMappingRule;
    
        return $this;
    }

    /**
     * Get idFamVicMappingRule
     *
     * @return integer 
     */
    public function getIdFamVicMappingRule()
    {
        return $this->idFamVicMappingRule;
    }

    /**
     * Set fam
     *
     * @param float $fam
     * @return DemoBank
     */
    public function setFam($fam)
    {
        $this->fam = $fam;
    
        return $this;
    }

    /**
     * Get fam
     *
     * @return float 
     */
    public function getFam()
    {
        return $this->fam;
    }

    /**
     * Set vic
     *
     * @param float $vic
     * @return DemoBank
     */
    public function setVic($vic)
    {
        $this->vic = $vic;
    
        return $this;
    }

    /**
     * Get vic
     *
     * @return float 
     */
    public function getVic()
    {
        return $this->vic;
    }
}