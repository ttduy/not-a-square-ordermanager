<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\VisibleGroupProductProduct
 *
 * @ORM\Table(name="visible_group_product_product")
 * @ORM\Entity
 */
class VisibleGroupProductProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $idVisibleGroupProduct
     *
     * @ORM\Column(name="id_visible_group_product", type="string", length=255, nullable=true)
     */
    private $idVisibleGroupProduct;

    /**
     * @var integer $idProduct
     *
     * @ORM\Column(name="id_product", type="integer", nullable=true)
     */
    private $idProduct;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idVisibleGroupProduct
     *
     * @param string $idVisibleGroupProduct
     * @return VisibleGroupProductProduct
     */
    public function setIdVisibleGroupProduct($idVisibleGroupProduct)
    {
        $this->idVisibleGroupProduct = $idVisibleGroupProduct;
    
        return $this;
    }

    /**
     * Get idVisibleGroupProduct
     *
     * @return string 
     */
    public function getIdVisibleGroupProduct()
    {
        return $this->idVisibleGroupProduct;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     * @return VisibleGroupProductProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    
        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }
}