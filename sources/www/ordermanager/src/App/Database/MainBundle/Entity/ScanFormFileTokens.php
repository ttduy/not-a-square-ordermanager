<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormFileTokens
 *
 * @ORM\Table(name="scan_form_file_tokens")
 * @ORM\Entity
 */
class ScanFormFileTokens
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormFile
     *
     * @ORM\Column(name="id_scan_form_file", type="integer", nullable=true)
     */
    private $idScanFormFile;

    /**
     * @var string $tokenKey
     *
     * @ORM\Column(name="token_key", type="string", length=255, nullable=true)
     */
    private $tokenKey;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var boolean $isCreationRequired
     *
     * @ORM\Column(name="is_creation_required", type="boolean", nullable=true)
     */
    private $isCreationRequired;

    /**
     * @var string $tokenImage
     *
     * @ORM\Column(name="token_image", type="string", length=500, nullable=true)
     */
    private $tokenImage;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormFile
     *
     * @param integer $idScanFormFile
     * @return ScanFormFileTokens
     */
    public function setIdScanFormFile($idScanFormFile)
    {
        $this->idScanFormFile = $idScanFormFile;
    
        return $this;
    }

    /**
     * Get idScanFormFile
     *
     * @return integer 
     */
    public function getIdScanFormFile()
    {
        return $this->idScanFormFile;
    }

    /**
     * Set tokenKey
     *
     * @param string $tokenKey
     * @return ScanFormFileTokens
     */
    public function setTokenKey($tokenKey)
    {
        $this->tokenKey = $tokenKey;
    
        return $this;
    }

    /**
     * Get tokenKey
     *
     * @return string 
     */
    public function getTokenKey()
    {
        return $this->tokenKey;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return ScanFormFileTokens
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set isCreationRequired
     *
     * @param boolean $isCreationRequired
     * @return ScanFormFileTokens
     */
    public function setIsCreationRequired($isCreationRequired)
    {
        $this->isCreationRequired = $isCreationRequired;
    
        return $this;
    }

    /**
     * Get isCreationRequired
     *
     * @return boolean 
     */
    public function getIsCreationRequired()
    {
        return $this->isCreationRequired;
    }

    /**
     * Set tokenImage
     *
     * @param string $tokenImage
     * @return ScanFormFileTokens
     */
    public function setTokenImage($tokenImage)
    {
        $this->tokenImage = $tokenImage;
    
        return $this;
    }

    /**
     * Get tokenImage
     *
     * @return string 
     */
    public function getTokenImage()
    {
        return $this->tokenImage;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return ScanFormFileTokens
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }
}