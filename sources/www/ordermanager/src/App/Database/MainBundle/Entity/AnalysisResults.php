<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\AnalysisResults
 *
 * @ORM\Table(name="analysis_results")
 * @ORM\Entity
 */
class AnalysisResults
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=true)
     */
    private $customerid;

    /**
     * @var integer $geneid
     *
     * @ORM\Column(name="GeneId", type="integer", nullable=true)
     */
    private $geneid;

    /**
     * @var integer $resultid
     *
     * @ORM\Column(name="ResultId", type="integer", nullable=true)
     */
    private $resultid;

    /**
     * @var string $result
     *
     * @ORM\Column(name="Result", type="string", length=255, nullable=true)
     */
    private $result;

    /**
     * @var integer $finalresultid
     *
     * @ORM\Column(name="FinalResultId", type="integer", nullable=true)
     */
    private $finalresultid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return AnalysisResults
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set geneid
     *
     * @param integer $geneid
     * @return AnalysisResults
     */
    public function setGeneid($geneid)
    {
        $this->geneid = $geneid;
    
        return $this;
    }

    /**
     * Get geneid
     *
     * @return integer 
     */
    public function getGeneid()
    {
        return $this->geneid;
    }

    /**
     * Set resultid
     *
     * @param integer $resultid
     * @return AnalysisResults
     */
    public function setResultid($resultid)
    {
        $this->resultid = $resultid;
    
        return $this;
    }

    /**
     * Get resultid
     *
     * @return integer 
     */
    public function getResultid()
    {
        return $this->resultid;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return AnalysisResults
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set finalresultid
     *
     * @param integer $finalresultid
     * @return AnalysisResults
     */
    public function setFinalresultid($finalresultid)
    {
        $this->finalresultid = $finalresultid;
    
        return $this;
    }

    /**
     * Get finalresultid
     *
     * @return integer 
     */
    public function getFinalresultid()
    {
        return $this->finalresultid;
    }
}