<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WebUserPermission
 *
 * @ORM\Table(name="web_user_permission")
 * @ORM\Entity
 */
class WebUserPermission
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var string $permissionKey
     *
     * @ORM\Column(name="permission_key", type="string", length=255, nullable=true)
     */
    private $permissionKey;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return WebUserPermission
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set permissionKey
     *
     * @param string $permissionKey
     * @return WebUserPermission
     */
    public function setPermissionKey($permissionKey)
    {
        $this->permissionKey = $permissionKey;
    
        return $this;
    }

    /**
     * Get permissionKey
     *
     * @return string 
     */
    public function getPermissionKey()
    {
        return $this->permissionKey;
    }
}