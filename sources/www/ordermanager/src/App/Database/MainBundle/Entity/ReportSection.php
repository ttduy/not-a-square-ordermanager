<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportSection
 *
 * @ORM\Table(name="report_section")
 * @ORM\Entity
 */
class ReportSection
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var boolean $isHide
     *
     * @ORM\Column(name="is_hide", type="boolean", nullable=true)
     */
    private $isHide;

    /**
     * @var integer $idReportSectionLink
     *
     * @ORM\Column(name="id_report_section_link", type="integer", nullable=true)
     */
    private $idReportSectionLink;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var string $alias
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @var integer $idReportLink
     *
     * @ORM\Column(name="id_report_link", type="integer", nullable=true)
     */
    private $idReportLink;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return ReportSection
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ReportSection
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return ReportSection
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isHide
     *
     * @param boolean $isHide
     * @return ReportSection
     */
    public function setIsHide($isHide)
    {
        $this->isHide = $isHide;
    
        return $this;
    }

    /**
     * Get isHide
     *
     * @return boolean 
     */
    public function getIsHide()
    {
        return $this->isHide;
    }

    /**
     * Set idReportSectionLink
     *
     * @param integer $idReportSectionLink
     * @return ReportSection
     */
    public function setIdReportSectionLink($idReportSectionLink)
    {
        $this->idReportSectionLink = $idReportSectionLink;
    
        return $this;
    }

    /**
     * Get idReportSectionLink
     *
     * @return integer 
     */
    public function getIdReportSectionLink()
    {
        return $this->idReportSectionLink;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return ReportSection
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ReportSection
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set idReportLink
     *
     * @param integer $idReportLink
     * @return ReportSection
     */
    public function setIdReportLink($idReportLink)
    {
        $this->idReportLink = $idReportLink;
    
        return $this;
    }

    /**
     * Get idReportLink
     *
     * @return integer 
     */
    public function getIdReportLink()
    {
        return $this->idReportLink;
    }
}