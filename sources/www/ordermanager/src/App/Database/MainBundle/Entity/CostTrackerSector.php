<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CostTrackerSector
 *
 * @ORM\Table(name="cost_tracker_sector")
 * @ORM\Entity
 */
class CostTrackerSector
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $txt
     *
     * @ORM\Column(name="txt", type="string", length=255, nullable=true)
     */
    private $txt;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var boolean $isForCostTracker
     *
     * @ORM\Column(name="is_for_cost_tracker", type="boolean", nullable=true)
     */
    private $isForCostTracker;

    /**
     * @var boolean $isForCashManager
     *
     * @ORM\Column(name="is_for_cash_manager", type="boolean", nullable=true)
     */
    private $isForCashManager;

    /**
     * @var boolean $isForBookKeeper
     *
     * @ORM\Column(name="is_for_book_keeper", type="boolean", nullable=true)
     */
    private $isForBookKeeper;

    /**
     * @var boolean $isInTotal
     *
     * @ORM\Column(name="is_in_total", type="boolean", nullable=true)
     */
    private $isInTotal;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set txt
     *
     * @param string $txt
     * @return CostTrackerSector
     */
    public function setTxt($txt)
    {
        $this->txt = $txt;
    
        return $this;
    }

    /**
     * Get txt
     *
     * @return string 
     */
    public function getTxt()
    {
        return $this->txt;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return CostTrackerSector
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isForCostTracker
     *
     * @param boolean $isForCostTracker
     * @return CostTrackerSector
     */
    public function setIsForCostTracker($isForCostTracker)
    {
        $this->isForCostTracker = $isForCostTracker;
    
        return $this;
    }

    /**
     * Get isForCostTracker
     *
     * @return boolean 
     */
    public function getIsForCostTracker()
    {
        return $this->isForCostTracker;
    }

    /**
     * Set isForCashManager
     *
     * @param boolean $isForCashManager
     * @return CostTrackerSector
     */
    public function setIsForCashManager($isForCashManager)
    {
        $this->isForCashManager = $isForCashManager;
    
        return $this;
    }

    /**
     * Get isForCashManager
     *
     * @return boolean 
     */
    public function getIsForCashManager()
    {
        return $this->isForCashManager;
    }

    /**
     * Set isForBookKeeper
     *
     * @param boolean $isForBookKeeper
     * @return CostTrackerSector
     */
    public function setIsForBookKeeper($isForBookKeeper)
    {
        $this->isForBookKeeper = $isForBookKeeper;
    
        return $this;
    }

    /**
     * Get isForBookKeeper
     *
     * @return boolean 
     */
    public function getIsForBookKeeper()
    {
        return $this->isForBookKeeper;
    }

    /**
     * Set isInTotal
     *
     * @param boolean $isInTotal
     * @return CostTrackerSector
     */
    public function setIsInTotal($isInTotal)
    {
        $this->isInTotal = $isInTotal;
    
        return $this;
    }

    /**
     * Get isInTotal
     *
     * @return boolean 
     */
    public function getIsInTotal()
    {
        return $this->isInTotal;
    }
}