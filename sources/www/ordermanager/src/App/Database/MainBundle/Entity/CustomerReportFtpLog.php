<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerReportFtpLog
 *
 * @ORM\Table(name="customer_report_ftp_log")
 * @ORM\Entity
 */
class CustomerReportFtpLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $uploadTime
     *
     * @ORM\Column(name="upload_time", type="datetime", nullable=true)
     */
    private $uploadTime;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var string $reportName
     *
     * @ORM\Column(name="report_name", type="string", length=255, nullable=true)
     */
    private $reportName;

    /**
     * @var string $fileName
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     */
    private $fileName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uploadTime
     *
     * @param \DateTime $uploadTime
     * @return CustomerReportFtpLog
     */
    public function setUploadTime($uploadTime)
    {
        $this->uploadTime = $uploadTime;
    
        return $this;
    }

    /**
     * Get uploadTime
     *
     * @return \DateTime 
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return CustomerReportFtpLog
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set reportName
     *
     * @param string $reportName
     * @return CustomerReportFtpLog
     */
    public function setReportName($reportName)
    {
        $this->reportName = $reportName;
    
        return $this;
    }

    /**
     * Get reportName
     *
     * @return string 
     */
    public function getReportName()
    {
        return $this->reportName;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return CustomerReportFtpLog
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    
        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }
}