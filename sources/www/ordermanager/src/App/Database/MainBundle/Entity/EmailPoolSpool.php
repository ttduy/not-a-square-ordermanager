<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolSpool
 *
 * @ORM\Table(name="email_pool_spool")
 * @ORM\Entity
 */
class EmailPoolSpool
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailPool
     *
     * @ORM\Column(name="id_email_pool", type="integer", nullable=true)
     */
    private $idEmailPool;

    /**
     * @var integer $idEmailPoolReceiver
     *
     * @ORM\Column(name="id_email_pool_receiver", type="integer", nullable=true)
     */
    private $idEmailPoolReceiver;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailPool
     *
     * @param integer $idEmailPool
     * @return EmailPoolSpool
     */
    public function setIdEmailPool($idEmailPool)
    {
        $this->idEmailPool = $idEmailPool;
    
        return $this;
    }

    /**
     * Get idEmailPool
     *
     * @return integer 
     */
    public function getIdEmailPool()
    {
        return $this->idEmailPool;
    }

    /**
     * Set idEmailPoolReceiver
     *
     * @param integer $idEmailPoolReceiver
     * @return EmailPoolSpool
     */
    public function setIdEmailPoolReceiver($idEmailPoolReceiver)
    {
        $this->idEmailPoolReceiver = $idEmailPoolReceiver;
    
        return $this;
    }

    /**
     * Get idEmailPoolReceiver
     *
     * @return integer 
     */
    public function getIdEmailPoolReceiver()
    {
        return $this->idEmailPoolReceiver;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailPoolSpool
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailPoolSpool
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailPoolSpool
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}