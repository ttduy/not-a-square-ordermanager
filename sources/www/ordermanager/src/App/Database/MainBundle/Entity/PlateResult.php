<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PlateResult
 *
 * @ORM\Table(name="plate_result")
 * @ORM\Entity
 */
class PlateResult
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPlateRawResult
     *
     * @ORM\Column(name="id_plate_raw_result", type="integer", nullable=true)
     */
    private $idPlateRawResult;

    /**
     * @var integer $wellIndex
     *
     * @ORM\Column(name="well_index", type="integer", nullable=true)
     */
    private $wellIndex;

    /**
     * @var integer $assignment
     *
     * @ORM\Column(name="assignment", type="integer", nullable=true)
     */
    private $assignment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPlateRawResult
     *
     * @param integer $idPlateRawResult
     * @return PlateResult
     */
    public function setIdPlateRawResult($idPlateRawResult)
    {
        $this->idPlateRawResult = $idPlateRawResult;
    
        return $this;
    }

    /**
     * Get idPlateRawResult
     *
     * @return integer 
     */
    public function getIdPlateRawResult()
    {
        return $this->idPlateRawResult;
    }

    /**
     * Set wellIndex
     *
     * @param integer $wellIndex
     * @return PlateResult
     */
    public function setWellIndex($wellIndex)
    {
        $this->wellIndex = $wellIndex;
    
        return $this;
    }

    /**
     * Get wellIndex
     *
     * @return integer 
     */
    public function getWellIndex()
    {
        return $this->wellIndex;
    }

    /**
     * Set assignment
     *
     * @param integer $assignment
     * @return PlateResult
     */
    public function setAssignment($assignment)
    {
        $this->assignment = $assignment;
    
        return $this;
    }

    /**
     * Get assignment
     *
     * @return integer 
     */
    public function getAssignment()
    {
        return $this->assignment;
    }
}