<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CmsOrderPayment
 *
 * @ORM\Table(name="cms_order_payment")
 * @ORM\Entity
 */
class CmsOrderPayment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCmsOrder
     *
     * @ORM\Column(name="id_cms_order", type="integer", nullable=true)
     */
    private $idCmsOrder;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var integer $idMethod
     *
     * @ORM\Column(name="id_method", type="integer", nullable=true)
     */
    private $idMethod;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCmsOrder
     *
     * @param integer $idCmsOrder
     * @return CmsOrderPayment
     */
    public function setIdCmsOrder($idCmsOrder)
    {
        $this->idCmsOrder = $idCmsOrder;
    
        return $this;
    }

    /**
     * Get idCmsOrder
     *
     * @return integer 
     */
    public function getIdCmsOrder()
    {
        return $this->idCmsOrder;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return CmsOrderPayment
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set idMethod
     *
     * @param integer $idMethod
     * @return CmsOrderPayment
     */
    public function setIdMethod($idMethod)
    {
        $this->idMethod = $idMethod;
    
        return $this;
    }

    /**
     * Get idMethod
     *
     * @return integer 
     */
    public function getIdMethod()
    {
        return $this->idMethod;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return CmsOrderPayment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CmsOrderPayment
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return CmsOrderPayment
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }
}