<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PyrosGeneLink
 *
 * @ORM\Table(name="pyros_gene_link")
 * @ORM\Entity
 */
class PyrosGeneLink
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $pyrosid
     *
     * @ORM\Column(name="PyrosId", type="integer", nullable=true)
     */
    private $pyrosid;

    /**
     * @var integer $geneid
     *
     * @ORM\Column(name="GeneId", type="integer", nullable=true)
     */
    private $geneid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pyrosid
     *
     * @param integer $pyrosid
     * @return PyrosGeneLink
     */
    public function setPyrosid($pyrosid)
    {
        $this->pyrosid = $pyrosid;
    
        return $this;
    }

    /**
     * Get pyrosid
     *
     * @return integer 
     */
    public function getPyrosid()
    {
        return $this->pyrosid;
    }

    /**
     * Set geneid
     *
     * @param integer $geneid
     * @return PyrosGeneLink
     */
    public function setGeneid($geneid)
    {
        $this->geneid = $geneid;
    
        return $this;
    }

    /**
     * Get geneid
     *
     * @return integer 
     */
    public function getGeneid()
    {
        return $this->geneid;
    }
}