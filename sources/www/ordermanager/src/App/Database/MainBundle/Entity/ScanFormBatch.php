<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormBatch
 *
 * @ORM\Table(name="scan_form_batch")
 * @ORM\Entity
 */
class ScanFormBatch
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime $batchDate
     *
     * @ORM\Column(name="batch_date", type="date", nullable=true)
     */
    private $batchDate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var integer $verifyStatus
     *
     * @ORM\Column(name="verify_status", type="integer", nullable=true)
     */
    private $verifyStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScanFormBatch
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set batchDate
     *
     * @param \DateTime $batchDate
     * @return ScanFormBatch
     */
    public function setBatchDate($batchDate)
    {
        $this->batchDate = $batchDate;
    
        return $this;
    }

    /**
     * Get batchDate
     *
     * @return \DateTime 
     */
    public function getBatchDate()
    {
        return $this->batchDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ScanFormBatch
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return ScanFormBatch
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set verifyStatus
     *
     * @param integer $verifyStatus
     * @return ScanFormBatch
     */
    public function setVerifyStatus($verifyStatus)
    {
        $this->verifyStatus = $verifyStatus;
    
        return $this;
    }

    /**
     * Get verifyStatus
     *
     * @return integer 
     */
    public function getVerifyStatus()
    {
        return $this->verifyStatus;
    }
}