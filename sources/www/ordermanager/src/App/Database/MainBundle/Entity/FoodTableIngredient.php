<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FoodTableIngredient
 *
 * @ORM\Table(name="food_table_ingredient")
 * @ORM\Entity
 */
class FoodTableIngredient
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $variableName
     *
     * @ORM\Column(name="variable_name", type="string", length=255, nullable=true)
     */
    private $variableName;

    /**
     * @var integer $riskAmountMax
     *
     * @ORM\Column(name="risk_amount_max", type="integer", nullable=true)
     */
    private $riskAmountMax;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FoodTableIngredient
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set variableName
     *
     * @param string $variableName
     * @return FoodTableIngredient
     */
    public function setVariableName($variableName)
    {
        $this->variableName = $variableName;
    
        return $this;
    }

    /**
     * Get variableName
     *
     * @return string 
     */
    public function getVariableName()
    {
        return $this->variableName;
    }

    /**
     * Set riskAmountMax
     *
     * @param integer $riskAmountMax
     * @return FoodTableIngredient
     */
    public function setRiskAmountMax($riskAmountMax)
    {
        $this->riskAmountMax = $riskAmountMax;
    
        return $this;
    }

    /**
     * Get riskAmountMax
     *
     * @return integer 
     */
    public function getRiskAmountMax()
    {
        return $this->riskAmountMax;
    }
}