<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolReceiverView
 *
 * @ORM\Table(name="email_pool_receiver_view")
 * @ORM\Entity
 */
class EmailPoolReceiverView
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailPoolReceiver
     *
     * @ORM\Column(name="id_email_pool_receiver", type="integer", nullable=true)
     */
    private $idEmailPoolReceiver;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailPoolReceiver
     *
     * @param integer $idEmailPoolReceiver
     * @return EmailPoolReceiverView
     */
    public function setIdEmailPoolReceiver($idEmailPoolReceiver)
    {
        $this->idEmailPoolReceiver = $idEmailPoolReceiver;
    
        return $this;
    }

    /**
     * Get idEmailPoolReceiver
     *
     * @return integer 
     */
    public function getIdEmailPoolReceiver()
    {
        return $this->idEmailPoolReceiver;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailPoolReceiverView
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}