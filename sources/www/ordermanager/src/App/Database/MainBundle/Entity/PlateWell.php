<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PlateWell
 *
 * @ORM\Table(name="plate_well")
 * @ORM\Entity
 */
class PlateWell
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPlate
     *
     * @ORM\Column(name="id_plate", type="integer", nullable=true)
     */
    private $idPlate;

    /**
     * @var integer $row
     *
     * @ORM\Column(name="row", type="integer", nullable=true)
     */
    private $row;

    /**
     * @var integer $col
     *
     * @ORM\Column(name="col", type="integer", nullable=true)
     */
    private $col;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;

    /**
     * @var integer $idOrder
     *
     * @ORM\Column(name="id_order", type="integer", nullable=true)
     */
    private $idOrder;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPlate
     *
     * @param integer $idPlate
     * @return PlateWell
     */
    public function setIdPlate($idPlate)
    {
        $this->idPlate = $idPlate;
    
        return $this;
    }

    /**
     * Get idPlate
     *
     * @return integer 
     */
    public function getIdPlate()
    {
        return $this->idPlate;
    }

    /**
     * Set row
     *
     * @param integer $row
     * @return PlateWell
     */
    public function setRow($row)
    {
        $this->row = $row;
    
        return $this;
    }

    /**
     * Get row
     *
     * @return integer 
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set col
     *
     * @param integer $col
     * @return PlateWell
     */
    public function setCol($col)
    {
        $this->col = $col;
    
        return $this;
    }

    /**
     * Get col
     *
     * @return integer 
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return PlateWell
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     * @return PlateWell
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    
        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer 
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return PlateWell
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}