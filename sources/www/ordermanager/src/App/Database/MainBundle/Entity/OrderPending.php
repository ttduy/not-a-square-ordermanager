<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderPending
 *
 * @ORM\Table(name="order_pending")
 * @ORM\Entity
 */
class OrderPending
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $contactIdGender
     *
     * @ORM\Column(name="contact_id_gender", type="integer", nullable=true)
     */
    private $contactIdGender;

    /**
     * @var \DateTime $contactDateOfBirth
     *
     * @ORM\Column(name="contact_date_of_birth", type="date", nullable=true)
     */
    private $contactDateOfBirth;

    /**
     * @var string $contactTitle
     *
     * @ORM\Column(name="contact_title", type="string", length=255, nullable=true)
     */
    private $contactTitle;

    /**
     * @var string $contactFirstName
     *
     * @ORM\Column(name="contact_first_name", type="string", length=255, nullable=true)
     */
    private $contactFirstName;

    /**
     * @var string $contactSurName
     *
     * @ORM\Column(name="contact_sur_name", type="string", length=255, nullable=true)
     */
    private $contactSurName;

    /**
     * @var string $orderInfo
     *
     * @ORM\Column(name="order_info", type="text", nullable=true)
     */
    private $orderInfo;

    /**
     * @var string $addressStreet
     *
     * @ORM\Column(name="address_street", type="string", length=255, nullable=true)
     */
    private $addressStreet;

    /**
     * @var string $addressStreet2
     *
     * @ORM\Column(name="address_street2", type="string", length=255, nullable=true)
     */
    private $addressStreet2;

    /**
     * @var string $addressPostCode
     *
     * @ORM\Column(name="address_post_code", type="string", length=255, nullable=true)
     */
    private $addressPostCode;

    /**
     * @var string $addressCity
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     */
    private $addressCity;

    /**
     * @var integer $addressIdCountry
     *
     * @ORM\Column(name="address_id_country", type="integer", nullable=true)
     */
    private $addressIdCountry;

    /**
     * @var string $addressEmail
     *
     * @ORM\Column(name="address_email", type="string", length=255, nullable=true)
     */
    private $addressEmail;

    /**
     * @var string $addressTelephone
     *
     * @ORM\Column(name="address_telephone", type="string", length=255, nullable=true)
     */
    private $addressTelephone;

    /**
     * @var string $addressFax
     *
     * @ORM\Column(name="address_fax", type="string", length=255, nullable=true)
     */
    private $addressFax;

    /**
     * @var string $addressNote
     *
     * @ORM\Column(name="address_note", type="text", nullable=true)
     */
    private $addressNote;

    /**
     * @var integer $settingIdLanguage
     *
     * @ORM\Column(name="setting_id_language", type="integer", nullable=true)
     */
    private $settingIdLanguage;

    /**
     * @var integer $settingReportGoesTo
     *
     * @ORM\Column(name="setting_report_goes_to", type="integer", nullable=true)
     */
    private $settingReportGoesTo;

    /**
     * @var integer $settingReportDelivery
     *
     * @ORM\Column(name="setting_report_delivery", type="integer", nullable=true)
     */
    private $settingReportDelivery;

    /**
     * @var integer $settingNutrimeGoesTo
     *
     * @ORM\Column(name="setting_nutrime_goes_to", type="integer", nullable=true)
     */
    private $settingNutrimeGoesTo;

    /**
     * @var string $orderedCategories
     *
     * @ORM\Column(name="ordered_categories", type="text", nullable=true)
     */
    private $orderedCategories;

    /**
     * @var string $orderedProducts
     *
     * @ORM\Column(name="ordered_products", type="text", nullable=true)
     */
    private $orderedProducts;

    /**
     * @var string $orderedSpecialProducts
     *
     * @ORM\Column(name="ordered_special_products", type="text", nullable=true)
     */
    private $orderedSpecialProducts;

    /**
     * @var string $orderedQuestions
     *
     * @ORM\Column(name="ordered_questions", type="text", nullable=true)
     */
    private $orderedQuestions;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idCreator
     *
     * @ORM\Column(name="id_creator", type="integer", nullable=true)
     */
    private $idCreator;

    /**
     * @var integer $creatorType
     *
     * @ORM\Column(name="creator_type", type="integer", nullable=true)
     */
    private $creatorType;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var boolean $isReportDeliveryDownloadAccess
     *
     * @ORM\Column(name="is_report_delivery_download_access", type="boolean", nullable=true)
     */
    private $isReportDeliveryDownloadAccess;

    /**
     * @var boolean $isReportDeliveryPrintedBooklet
     *
     * @ORM\Column(name="is_report_delivery_printed_booklet", type="boolean", nullable=true)
     */
    private $isReportDeliveryPrintedBooklet;

    /**
     * @var integer $idDncReportType
     *
     * @ORM\Column(name="id_dnc_report_type", type="integer", nullable=true)
     */
    private $idDncReportType;

    /**
     * @var string $previousOrderNumber
     *
     * @ORM\Column(name="previous_order_number", type="string", length=255, nullable=true)
     */
    private $previousOrderNumber;

    /**
     * @var string $orderExternalBarcode
     *
     * @ORM\Column(name="order_external_barcode", type="string", length=255, nullable=true)
     */
    private $orderExternalBarcode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactIdGender
     *
     * @param integer $contactIdGender
     * @return OrderPending
     */
    public function setContactIdGender($contactIdGender)
    {
        $this->contactIdGender = $contactIdGender;
    
        return $this;
    }

    /**
     * Get contactIdGender
     *
     * @return integer 
     */
    public function getContactIdGender()
    {
        return $this->contactIdGender;
    }

    /**
     * Set contactDateOfBirth
     *
     * @param \DateTime $contactDateOfBirth
     * @return OrderPending
     */
    public function setContactDateOfBirth($contactDateOfBirth)
    {
        $this->contactDateOfBirth = $contactDateOfBirth;
    
        return $this;
    }

    /**
     * Get contactDateOfBirth
     *
     * @return \DateTime 
     */
    public function getContactDateOfBirth()
    {
        return $this->contactDateOfBirth;
    }

    /**
     * Set contactTitle
     *
     * @param string $contactTitle
     * @return OrderPending
     */
    public function setContactTitle($contactTitle)
    {
        $this->contactTitle = $contactTitle;
    
        return $this;
    }

    /**
     * Get contactTitle
     *
     * @return string 
     */
    public function getContactTitle()
    {
        return $this->contactTitle;
    }

    /**
     * Set contactFirstName
     *
     * @param string $contactFirstName
     * @return OrderPending
     */
    public function setContactFirstName($contactFirstName)
    {
        $this->contactFirstName = $contactFirstName;
    
        return $this;
    }

    /**
     * Get contactFirstName
     *
     * @return string 
     */
    public function getContactFirstName()
    {
        return $this->contactFirstName;
    }

    /**
     * Set contactSurName
     *
     * @param string $contactSurName
     * @return OrderPending
     */
    public function setContactSurName($contactSurName)
    {
        $this->contactSurName = $contactSurName;
    
        return $this;
    }

    /**
     * Get contactSurName
     *
     * @return string 
     */
    public function getContactSurName()
    {
        return $this->contactSurName;
    }

    /**
     * Set orderInfo
     *
     * @param string $orderInfo
     * @return OrderPending
     */
    public function setOrderInfo($orderInfo)
    {
        $this->orderInfo = $orderInfo;
    
        return $this;
    }

    /**
     * Get orderInfo
     *
     * @return string 
     */
    public function getOrderInfo()
    {
        return $this->orderInfo;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     * @return OrderPending
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    
        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string 
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressStreet2
     *
     * @param string $addressStreet2
     * @return OrderPending
     */
    public function setAddressStreet2($addressStreet2)
    {
        $this->addressStreet2 = $addressStreet2;
    
        return $this;
    }

    /**
     * Get addressStreet2
     *
     * @return string 
     */
    public function getAddressStreet2()
    {
        return $this->addressStreet2;
    }

    /**
     * Set addressPostCode
     *
     * @param string $addressPostCode
     * @return OrderPending
     */
    public function setAddressPostCode($addressPostCode)
    {
        $this->addressPostCode = $addressPostCode;
    
        return $this;
    }

    /**
     * Get addressPostCode
     *
     * @return string 
     */
    public function getAddressPostCode()
    {
        return $this->addressPostCode;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return OrderPending
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    
        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string 
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressIdCountry
     *
     * @param integer $addressIdCountry
     * @return OrderPending
     */
    public function setAddressIdCountry($addressIdCountry)
    {
        $this->addressIdCountry = $addressIdCountry;
    
        return $this;
    }

    /**
     * Get addressIdCountry
     *
     * @return integer 
     */
    public function getAddressIdCountry()
    {
        return $this->addressIdCountry;
    }

    /**
     * Set addressEmail
     *
     * @param string $addressEmail
     * @return OrderPending
     */
    public function setAddressEmail($addressEmail)
    {
        $this->addressEmail = $addressEmail;
    
        return $this;
    }

    /**
     * Get addressEmail
     *
     * @return string 
     */
    public function getAddressEmail()
    {
        return $this->addressEmail;
    }

    /**
     * Set addressTelephone
     *
     * @param string $addressTelephone
     * @return OrderPending
     */
    public function setAddressTelephone($addressTelephone)
    {
        $this->addressTelephone = $addressTelephone;
    
        return $this;
    }

    /**
     * Get addressTelephone
     *
     * @return string 
     */
    public function getAddressTelephone()
    {
        return $this->addressTelephone;
    }

    /**
     * Set addressFax
     *
     * @param string $addressFax
     * @return OrderPending
     */
    public function setAddressFax($addressFax)
    {
        $this->addressFax = $addressFax;
    
        return $this;
    }

    /**
     * Get addressFax
     *
     * @return string 
     */
    public function getAddressFax()
    {
        return $this->addressFax;
    }

    /**
     * Set addressNote
     *
     * @param string $addressNote
     * @return OrderPending
     */
    public function setAddressNote($addressNote)
    {
        $this->addressNote = $addressNote;
    
        return $this;
    }

    /**
     * Get addressNote
     *
     * @return string 
     */
    public function getAddressNote()
    {
        return $this->addressNote;
    }

    /**
     * Set settingIdLanguage
     *
     * @param integer $settingIdLanguage
     * @return OrderPending
     */
    public function setSettingIdLanguage($settingIdLanguage)
    {
        $this->settingIdLanguage = $settingIdLanguage;
    
        return $this;
    }

    /**
     * Get settingIdLanguage
     *
     * @return integer 
     */
    public function getSettingIdLanguage()
    {
        return $this->settingIdLanguage;
    }

    /**
     * Set settingReportGoesTo
     *
     * @param integer $settingReportGoesTo
     * @return OrderPending
     */
    public function setSettingReportGoesTo($settingReportGoesTo)
    {
        $this->settingReportGoesTo = $settingReportGoesTo;
    
        return $this;
    }

    /**
     * Get settingReportGoesTo
     *
     * @return integer 
     */
    public function getSettingReportGoesTo()
    {
        return $this->settingReportGoesTo;
    }

    /**
     * Set settingReportDelivery
     *
     * @param integer $settingReportDelivery
     * @return OrderPending
     */
    public function setSettingReportDelivery($settingReportDelivery)
    {
        $this->settingReportDelivery = $settingReportDelivery;
    
        return $this;
    }

    /**
     * Get settingReportDelivery
     *
     * @return integer 
     */
    public function getSettingReportDelivery()
    {
        return $this->settingReportDelivery;
    }

    /**
     * Set settingNutrimeGoesTo
     *
     * @param integer $settingNutrimeGoesTo
     * @return OrderPending
     */
    public function setSettingNutrimeGoesTo($settingNutrimeGoesTo)
    {
        $this->settingNutrimeGoesTo = $settingNutrimeGoesTo;
    
        return $this;
    }

    /**
     * Get settingNutrimeGoesTo
     *
     * @return integer 
     */
    public function getSettingNutrimeGoesTo()
    {
        return $this->settingNutrimeGoesTo;
    }

    /**
     * Set orderedCategories
     *
     * @param string $orderedCategories
     * @return OrderPending
     */
    public function setOrderedCategories($orderedCategories)
    {
        $this->orderedCategories = $orderedCategories;
    
        return $this;
    }

    /**
     * Get orderedCategories
     *
     * @return string 
     */
    public function getOrderedCategories()
    {
        return $this->orderedCategories;
    }

    /**
     * Set orderedProducts
     *
     * @param string $orderedProducts
     * @return OrderPending
     */
    public function setOrderedProducts($orderedProducts)
    {
        $this->orderedProducts = $orderedProducts;
    
        return $this;
    }

    /**
     * Get orderedProducts
     *
     * @return string 
     */
    public function getOrderedProducts()
    {
        return $this->orderedProducts;
    }

    /**
     * Set orderedSpecialProducts
     *
     * @param string $orderedSpecialProducts
     * @return OrderPending
     */
    public function setOrderedSpecialProducts($orderedSpecialProducts)
    {
        $this->orderedSpecialProducts = $orderedSpecialProducts;
    
        return $this;
    }

    /**
     * Get orderedSpecialProducts
     *
     * @return string 
     */
    public function getOrderedSpecialProducts()
    {
        return $this->orderedSpecialProducts;
    }

    /**
     * Set orderedQuestions
     *
     * @param string $orderedQuestions
     * @return OrderPending
     */
    public function setOrderedQuestions($orderedQuestions)
    {
        $this->orderedQuestions = $orderedQuestions;
    
        return $this;
    }

    /**
     * Get orderedQuestions
     *
     * @return string 
     */
    public function getOrderedQuestions()
    {
        return $this->orderedQuestions;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return OrderPending
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idCreator
     *
     * @param integer $idCreator
     * @return OrderPending
     */
    public function setIdCreator($idCreator)
    {
        $this->idCreator = $idCreator;
    
        return $this;
    }

    /**
     * Get idCreator
     *
     * @return integer 
     */
    public function getIdCreator()
    {
        return $this->idCreator;
    }

    /**
     * Set creatorType
     *
     * @param integer $creatorType
     * @return OrderPending
     */
    public function setCreatorType($creatorType)
    {
        $this->creatorType = $creatorType;
    
        return $this;
    }

    /**
     * Get creatorType
     *
     * @return integer 
     */
    public function getCreatorType()
    {
        return $this->creatorType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OrderPending
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isReportDeliveryDownloadAccess
     *
     * @param boolean $isReportDeliveryDownloadAccess
     * @return OrderPending
     */
    public function setIsReportDeliveryDownloadAccess($isReportDeliveryDownloadAccess)
    {
        $this->isReportDeliveryDownloadAccess = $isReportDeliveryDownloadAccess;
    
        return $this;
    }

    /**
     * Get isReportDeliveryDownloadAccess
     *
     * @return boolean 
     */
    public function getIsReportDeliveryDownloadAccess()
    {
        return $this->isReportDeliveryDownloadAccess;
    }

    /**
     * Set isReportDeliveryPrintedBooklet
     *
     * @param boolean $isReportDeliveryPrintedBooklet
     * @return OrderPending
     */
    public function setIsReportDeliveryPrintedBooklet($isReportDeliveryPrintedBooklet)
    {
        $this->isReportDeliveryPrintedBooklet = $isReportDeliveryPrintedBooklet;
    
        return $this;
    }

    /**
     * Get isReportDeliveryPrintedBooklet
     *
     * @return boolean 
     */
    public function getIsReportDeliveryPrintedBooklet()
    {
        return $this->isReportDeliveryPrintedBooklet;
    }

    /**
     * Set idDncReportType
     *
     * @param integer $idDncReportType
     * @return OrderPending
     */
    public function setIdDncReportType($idDncReportType)
    {
        $this->idDncReportType = $idDncReportType;
    
        return $this;
    }

    /**
     * Get idDncReportType
     *
     * @return integer 
     */
    public function getIdDncReportType()
    {
        return $this->idDncReportType;
    }

    /**
     * Set previousOrderNumber
     *
     * @param string $previousOrderNumber
     * @return OrderPending
     */
    public function setPreviousOrderNumber($previousOrderNumber)
    {
        $this->previousOrderNumber = $previousOrderNumber;
    
        return $this;
    }

    /**
     * Get previousOrderNumber
     *
     * @return string 
     */
    public function getPreviousOrderNumber()
    {
        return $this->previousOrderNumber;
    }

    /**
     * Set orderExternalBarcode
     *
     * @param string $orderExternalBarcode
     * @return OrderPending
     */
    public function setOrderExternalBarcode($orderExternalBarcode)
    {
        $this->orderExternalBarcode = $orderExternalBarcode;
    
        return $this;
    }

    /**
     * Get orderExternalBarcode
     *
     * @return string 
     */
    public function getOrderExternalBarcode()
    {
        return $this->orderExternalBarcode;
    }
}