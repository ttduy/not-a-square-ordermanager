<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerQuestion
 *
 * @ORM\Table(name="customer_question")
 * @ORM\Entity
 */
class CustomerQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=true)
     */
    private $customerid;

    /**
     * @var integer $questionid
     *
     * @ORM\Column(name="QuestionId", type="integer", nullable=false)
     */
    private $questionid;

    /**
     * @var string $answer
     *
     * @ORM\Column(name="Answer", type="text", nullable=true)
     */
    private $answer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return CustomerQuestion
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set questionid
     *
     * @param integer $questionid
     * @return CustomerQuestion
     */
    public function setQuestionid($questionid)
    {
        $this->questionid = $questionid;
    
        return $this;
    }

    /**
     * Get questionid
     *
     * @return integer 
     */
    public function getQuestionid()
    {
        return $this->questionid;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return CustomerQuestion
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}