<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GeneManagerResultHistory
 *
 * @ORM\Table(name="gene_manager_result_history")
 * @ORM\Entity
 */
class GeneManagerResultHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $result
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    private $result;

    /**
     * @var string $previewData
     *
     * @ORM\Column(name="preview_data", type="text", nullable=true)
     */
    private $previewData;

    /**
     * @var integer $numFormatError
     *
     * @ORM\Column(name="num_format_error", type="integer", nullable=true)
     */
    private $numFormatError;

    /**
     * @var integer $numDataError
     *
     * @ORM\Column(name="num_data_error", type="integer", nullable=true)
     */
    private $numDataError;

    /**
     * @var integer $numConflictError
     *
     * @ORM\Column(name="num_conflict_error", type="integer", nullable=true)
     */
    private $numConflictError;

    /**
     * @var integer $numSuccess
     *
     * @ORM\Column(name="num_success", type="integer", nullable=true)
     */
    private $numSuccess;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $numWarning
     *
     * @ORM\Column(name="num_warning", type="integer", nullable=true)
     */
    private $numWarning;

    /**
     * @var integer $idWebUsers
     *
     * @ORM\Column(name="id_web_users", type="integer", nullable=true)
     */
    private $idWebUsers;

    /**
     * @var string $additionalInformation
     *
     * @ORM\Column(name="additional_information", type="text", nullable=true)
     */
    private $additionalInformation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return GeneManagerResultHistory
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return GeneManagerResultHistory
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set previewData
     *
     * @param string $previewData
     * @return GeneManagerResultHistory
     */
    public function setPreviewData($previewData)
    {
        $this->previewData = $previewData;
    
        return $this;
    }

    /**
     * Get previewData
     *
     * @return string 
     */
    public function getPreviewData()
    {
        return $this->previewData;
    }

    /**
     * Set numFormatError
     *
     * @param integer $numFormatError
     * @return GeneManagerResultHistory
     */
    public function setNumFormatError($numFormatError)
    {
        $this->numFormatError = $numFormatError;
    
        return $this;
    }

    /**
     * Get numFormatError
     *
     * @return integer 
     */
    public function getNumFormatError()
    {
        return $this->numFormatError;
    }

    /**
     * Set numDataError
     *
     * @param integer $numDataError
     * @return GeneManagerResultHistory
     */
    public function setNumDataError($numDataError)
    {
        $this->numDataError = $numDataError;
    
        return $this;
    }

    /**
     * Get numDataError
     *
     * @return integer 
     */
    public function getNumDataError()
    {
        return $this->numDataError;
    }

    /**
     * Set numConflictError
     *
     * @param integer $numConflictError
     * @return GeneManagerResultHistory
     */
    public function setNumConflictError($numConflictError)
    {
        $this->numConflictError = $numConflictError;
    
        return $this;
    }

    /**
     * Get numConflictError
     *
     * @return integer 
     */
    public function getNumConflictError()
    {
        return $this->numConflictError;
    }

    /**
     * Set numSuccess
     *
     * @param integer $numSuccess
     * @return GeneManagerResultHistory
     */
    public function setNumSuccess($numSuccess)
    {
        $this->numSuccess = $numSuccess;
    
        return $this;
    }

    /**
     * Get numSuccess
     *
     * @return integer 
     */
    public function getNumSuccess()
    {
        return $this->numSuccess;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return GeneManagerResultHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set numWarning
     *
     * @param integer $numWarning
     * @return GeneManagerResultHistory
     */
    public function setNumWarning($numWarning)
    {
        $this->numWarning = $numWarning;
    
        return $this;
    }

    /**
     * Get numWarning
     *
     * @return integer 
     */
    public function getNumWarning()
    {
        return $this->numWarning;
    }

    /**
     * Set idWebUsers
     *
     * @param integer $idWebUsers
     * @return GeneManagerResultHistory
     */
    public function setIdWebUsers($idWebUsers)
    {
        $this->idWebUsers = $idWebUsers;
    
        return $this;
    }

    /**
     * Get idWebUsers
     *
     * @return integer 
     */
    public function getIdWebUsers()
    {
        return $this->idWebUsers;
    }

    /**
     * Set additionalInformation
     *
     * @param string $additionalInformation
     * @return GeneManagerResultHistory
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    
        return $this;
    }

    /**
     * Get additionalInformation
     *
     * @return string 
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }
}