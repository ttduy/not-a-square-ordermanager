<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TextBlock
 *
 * @ORM\Table(name="text_block")
 * @ORM\Entity
 */
class TextBlock
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGroup
     *
     * @ORM\Column(name="id_group", type="integer", nullable=true)
     */
    private $idGroup;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $maxLength
     *
     * @ORM\Column(name="max_length", type="integer", nullable=true)
     */
    private $maxLength;

    /**
     * @var boolean $isUppercase
     *
     * @ORM\Column(name="is_uppercase", type="boolean", nullable=true)
     */
    private $isUppercase;

    /**
     * @var integer $numLineBreak
     *
     * @ORM\Column(name="num_line_break", type="integer", nullable=true)
     */
    private $numLineBreak;

    /**
     * @var string $keyWords
     *
     * @ORM\Column(name="key_words", type="string", length=255, nullable=true)
     */
    private $keyWords;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     * @return TextBlock
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;
    
        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer 
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TextBlock
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set maxLength
     *
     * @param integer $maxLength
     * @return TextBlock
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    
        return $this;
    }

    /**
     * Get maxLength
     *
     * @return integer 
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * Set isUppercase
     *
     * @param boolean $isUppercase
     * @return TextBlock
     */
    public function setIsUppercase($isUppercase)
    {
        $this->isUppercase = $isUppercase;
    
        return $this;
    }

    /**
     * Get isUppercase
     *
     * @return boolean 
     */
    public function getIsUppercase()
    {
        return $this->isUppercase;
    }

    /**
     * Set numLineBreak
     *
     * @param integer $numLineBreak
     * @return TextBlock
     */
    public function setNumLineBreak($numLineBreak)
    {
        $this->numLineBreak = $numLineBreak;
    
        return $this;
    }

    /**
     * Get numLineBreak
     *
     * @return integer 
     */
    public function getNumLineBreak()
    {
        return $this->numLineBreak;
    }

    /**
     * Set keyWords
     *
     * @param string $keyWords
     * @return TextBlock
     */
    public function setKeyWords($keyWords)
    {
        $this->keyWords = $keyWords;
    
        return $this;
    }

    /**
     * Get keyWords
     *
     * @return string 
     */
    public function getKeyWords()
    {
        return $this->keyWords;
    }
}