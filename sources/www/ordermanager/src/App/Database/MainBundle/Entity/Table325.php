<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Table325
 *
 * @ORM\Table(name="TABLE 325")
 * @ORM\Entity
 */
class Table325
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $variableName
     *
     * @ORM\Column(name="variable_name", type="string", length=37, nullable=true)
     */
    private $variableName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variableName
     *
     * @param string $variableName
     * @return Table325
     */
    public function setVariableName($variableName)
    {
        $this->variableName = $variableName;
    
        return $this;
    }

    /**
     * Get variableName
     *
     * @return string 
     */
    public function getVariableName()
    {
        return $this->variableName;
    }
}