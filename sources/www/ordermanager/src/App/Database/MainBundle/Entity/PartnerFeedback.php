<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PartnerFeedback
 *
 * @ORM\Table(name="partner_feedback")
 * @ORM\Entity
 */
class PartnerFeedback
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var \DateTime $fbTimestamp
     *
     * @ORM\Column(name="fb_timestamp", type="datetime", nullable=false)
     */
    private $fbTimestamp;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var string $feedbacker
     *
     * @ORM\Column(name="feedbacker", type="string", length=255, nullable=true)
     */
    private $feedbacker;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return PartnerFeedback
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set fbTimestamp
     *
     * @param \DateTime $fbTimestamp
     * @return PartnerFeedback
     */
    public function setFbTimestamp($fbTimestamp)
    {
        $this->fbTimestamp = $fbTimestamp;
    
        return $this;
    }

    /**
     * Get fbTimestamp
     *
     * @return \DateTime 
     */
    public function getFbTimestamp()
    {
        return $this->fbTimestamp;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return PartnerFeedback
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set feedbacker
     *
     * @param string $feedbacker
     * @return PartnerFeedback
     */
    public function setFeedbacker($feedbacker)
    {
        $this->feedbacker = $feedbacker;
    
        return $this;
    }

    /**
     * Get feedbacker
     *
     * @return string 
     */
    public function getFeedbacker()
    {
        return $this->feedbacker;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PartnerFeedback
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PartnerFeedback
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}