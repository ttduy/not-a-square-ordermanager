<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PelletEntryWeight
 *
 * @ORM\Table(name="pellet_entry_weight")
 * @ORM\Entity
 */
class PelletEntryWeight
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPelletEntry
     *
     * @ORM\Column(name="id_pellet_entry", type="integer", nullable=true)
     */
    private $idPelletEntry;

    /**
     * @var string $txt
     *
     * @ORM\Column(name="txt", type="string", length=255, nullable=true)
     */
    private $txt;

    /**
     * @var float $weight
     *
     * @ORM\Column(name="weight", type="float", nullable=true)
     */
    private $weight;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPelletEntry
     *
     * @param integer $idPelletEntry
     * @return PelletEntryWeight
     */
    public function setIdPelletEntry($idPelletEntry)
    {
        $this->idPelletEntry = $idPelletEntry;
    
        return $this;
    }

    /**
     * Get idPelletEntry
     *
     * @return integer 
     */
    public function getIdPelletEntry()
    {
        return $this->idPelletEntry;
    }

    /**
     * Set txt
     *
     * @param string $txt
     * @return PelletEntryWeight
     */
    public function setTxt($txt)
    {
        $this->txt = $txt;
    
        return $this;
    }

    /**
     * Get txt
     *
     * @return string 
     */
    public function getTxt()
    {
        return $this->txt;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return PelletEntryWeight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PelletEntryWeight
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}