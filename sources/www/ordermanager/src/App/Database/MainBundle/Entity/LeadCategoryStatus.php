<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\LeadCategoryStatus
 *
 * @ORM\Table(name="lead_category_status")
 * @ORM\Entity
 */
class LeadCategoryStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLeadCategory
     *
     * @ORM\Column(name="id_lead_category", type="integer", nullable=true)
     */
    private $idLeadCategory;

    /**
     * @var integer $idLeadStatus
     *
     * @ORM\Column(name="id_lead_status", type="integer", nullable=true)
     */
    private $idLeadStatus;

    /**
     * @var string $leadStatusValue
     *
     * @ORM\Column(name="lead_status_value", type="string", length=255, nullable=true)
     */
    private $leadStatusValue;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLeadCategory
     *
     * @param integer $idLeadCategory
     * @return LeadCategoryStatus
     */
    public function setIdLeadCategory($idLeadCategory)
    {
        $this->idLeadCategory = $idLeadCategory;
    
        return $this;
    }

    /**
     * Get idLeadCategory
     *
     * @return integer 
     */
    public function getIdLeadCategory()
    {
        return $this->idLeadCategory;
    }

    /**
     * Set idLeadStatus
     *
     * @param integer $idLeadStatus
     * @return LeadCategoryStatus
     */
    public function setIdLeadStatus($idLeadStatus)
    {
        $this->idLeadStatus = $idLeadStatus;
    
        return $this;
    }

    /**
     * Get idLeadStatus
     *
     * @return integer 
     */
    public function getIdLeadStatus()
    {
        return $this->idLeadStatus;
    }

    /**
     * Set leadStatusValue
     *
     * @param string $leadStatusValue
     * @return LeadCategoryStatus
     */
    public function setLeadStatusValue($leadStatusValue)
    {
        $this->leadStatusValue = $leadStatusValue;
    
        return $this;
    }

    /**
     * Get leadStatusValue
     *
     * @return string 
     */
    public function getLeadStatusValue()
    {
        return $this->leadStatusValue;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return LeadCategoryStatus
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }
}