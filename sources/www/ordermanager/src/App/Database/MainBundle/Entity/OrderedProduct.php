<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderedProduct
 *
 * @ORM\Table(name="ordered_product")
 * @ORM\Entity
 */
class OrderedProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=true)
     */
    private $customerid;

    /**
     * @var integer $productid
     *
     * @ORM\Column(name="ProductId", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var integer $specialproductid
     *
     * @ORM\Column(name="SpecialProductId", type="integer", nullable=true)
     */
    private $specialproductid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return OrderedProduct
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return OrderedProduct
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    
        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set specialproductid
     *
     * @param integer $specialproductid
     * @return OrderedProduct
     */
    public function setSpecialproductid($specialproductid)
    {
        $this->specialproductid = $specialproductid;
    
        return $this;
    }

    /**
     * Get specialproductid
     *
     * @return integer 
     */
    public function getSpecialproductid()
    {
        return $this->specialproductid;
    }
}