<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TranslatorUserReport
 *
 * @ORM\Table(name="translator_user_report")
 * @ORM\Entity
 */
class TranslatorUserReport
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idTranslatorUser
     *
     * @ORM\Column(name="id_translator_user", type="integer", nullable=true)
     */
    private $idTranslatorUser;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTranslatorUser
     *
     * @param integer $idTranslatorUser
     * @return TranslatorUserReport
     */
    public function setIdTranslatorUser($idTranslatorUser)
    {
        $this->idTranslatorUser = $idTranslatorUser;
    
        return $this;
    }

    /**
     * Get idTranslatorUser
     *
     * @return integer 
     */
    public function getIdTranslatorUser()
    {
        return $this->idTranslatorUser;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return TranslatorUserReport
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }
}