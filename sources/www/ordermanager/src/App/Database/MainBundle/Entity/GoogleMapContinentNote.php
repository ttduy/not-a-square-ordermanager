<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GoogleMapContinentNote
 *
 * @ORM\Table(name="google_map_continent_note")
 * @ORM\Entity
 */
class GoogleMapContinentNote
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $continent
     *
     * @ORM\Column(name="continent", type="string", length=255, nullable=true)
     */
    private $continent;

    /**
     * @var boolean $isAvailable
     *
     * @ORM\Column(name="is_available", type="boolean", nullable=true)
     */
    private $isAvailable;

    /**
     * @var string $note
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var string $note2
     *
     * @ORM\Column(name="note2", type="text", nullable=true)
     */
    private $note2;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set continent
     *
     * @param string $continent
     * @return GoogleMapContinentNote
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;
    
        return $this;
    }

    /**
     * Get continent
     *
     * @return string 
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * Set isAvailable
     *
     * @param boolean $isAvailable
     * @return GoogleMapContinentNote
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;
    
        return $this;
    }

    /**
     * Get isAvailable
     *
     * @return boolean 
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return GoogleMapContinentNote
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note2
     *
     * @param string $note2
     * @return GoogleMapContinentNote
     */
    public function setNote2($note2)
    {
        $this->note2 = $note2;
    
        return $this;
    }

    /**
     * Get note2
     *
     * @return string 
     */
    public function getNote2()
    {
        return $this->note2;
    }
}