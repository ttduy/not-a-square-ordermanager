<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DrugIngredient
 *
 * @ORM\Table(name="drug_ingredient")
 * @ORM\Entity
 */
class DrugIngredient
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $variableName
     *
     * @ORM\Column(name="variable_name", type="string", length=255, nullable=true)
     */
    private $variableName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variableName
     *
     * @param string $variableName
     * @return DrugIngredient
     */
    public function setVariableName($variableName)
    {
        $this->variableName = $variableName;
    
        return $this;
    }

    /**
     * Get variableName
     *
     * @return string 
     */
    public function getVariableName()
    {
        return $this->variableName;
    }
}