<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerFeedback
 *
 * @ORM\Table(name="customer_feedback")
 * @ORM\Entity
 */
class CustomerFeedback
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var \DateTime $fbTimestamp
     *
     * @ORM\Column(name="fb_timestamp", type="datetime", nullable=false)
     */
    private $fbTimestamp;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var string $feedbacker
     *
     * @ORM\Column(name="feedbacker", type="string", length=255, nullable=true)
     */
    private $feedbacker;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return CustomerFeedback
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set fbTimestamp
     *
     * @param \DateTime $fbTimestamp
     * @return CustomerFeedback
     */
    public function setFbTimestamp($fbTimestamp)
    {
        $this->fbTimestamp = $fbTimestamp;
    
        return $this;
    }

    /**
     * Get fbTimestamp
     *
     * @return \DateTime 
     */
    public function getFbTimestamp()
    {
        return $this->fbTimestamp;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return CustomerFeedback
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set feedbacker
     *
     * @param string $feedbacker
     * @return CustomerFeedback
     */
    public function setFeedbacker($feedbacker)
    {
        $this->feedbacker = $feedbacker;
    
        return $this;
    }

    /**
     * Get feedbacker
     *
     * @return string 
     */
    public function getFeedbacker()
    {
        return $this->feedbacker;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CustomerFeedback
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return CustomerFeedback
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}