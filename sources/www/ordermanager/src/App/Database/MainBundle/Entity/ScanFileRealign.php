<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFileRealign
 *
 * @ORM\Table(name="scan_file_realign")
 * @ORM\Entity
 */
class ScanFileRealign
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFile
     *
     * @ORM\Column(name="id_scan_file", type="integer", nullable=true)
     */
    private $idScanFile;

    /**
     * @var integer $pageNumber
     *
     * @ORM\Column(name="page_number", type="integer", nullable=true)
     */
    private $pageNumber;

    /**
     * @var integer $x
     *
     * @ORM\Column(name="x", type="integer", nullable=true)
     */
    private $x;

    /**
     * @var integer $y
     *
     * @ORM\Column(name="y", type="integer", nullable=true)
     */
    private $y;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFile
     *
     * @param integer $idScanFile
     * @return ScanFileRealign
     */
    public function setIdScanFile($idScanFile)
    {
        $this->idScanFile = $idScanFile;
    
        return $this;
    }

    /**
     * Get idScanFile
     *
     * @return integer 
     */
    public function getIdScanFile()
    {
        return $this->idScanFile;
    }

    /**
     * Set pageNumber
     *
     * @param integer $pageNumber
     * @return ScanFileRealign
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
    
        return $this;
    }

    /**
     * Get pageNumber
     *
     * @return integer 
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return ScanFileRealign
     */
    public function setX($x)
    {
        $this->x = $x;
    
        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return ScanFileRealign
     */
    public function setY($y)
    {
        $this->y = $y;
    
        return $this;
    }

    /**
     * Get y
     *
     * @return integer 
     */
    public function getY()
    {
        return $this->y;
    }
}