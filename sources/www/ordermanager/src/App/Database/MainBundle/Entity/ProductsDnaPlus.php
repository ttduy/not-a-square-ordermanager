<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductsDnaPlus
 *
 * @ORM\Table(name="products_dna_plus")
 * @ORM\Entity
 */
class ProductsDnaPlus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var float $standardpriceexclvat
     *
     * @ORM\Column(name="StandardPriceExclVat", type="float", nullable=true)
     */
    private $standardpriceexclvat;

    /**
     * @var float $materialcosts
     *
     * @ORM\Column(name="MaterialCosts", type="float", nullable=true)
     */
    private $materialcosts;

    /**
     * @var float $productioncostsnoreports
     *
     * @ORM\Column(name="ProductionCostsNoReports", type="float", nullable=true)
     */
    private $productioncostsnoreports;

    /**
     * @var string $doctorsmargin
     *
     * @ORM\Column(name="DoctorsMargin", type="string", length=255, nullable=true)
     */
    private $doctorsmargin;

    /**
     * @var string $distributionchannelmargin
     *
     * @ORM\Column(name="DistributionChannelMargin", type="string", length=255, nullable=true)
     */
    private $distributionchannelmargin;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var string $codename
     *
     * @ORM\Column(name="codename", type="string", length=255, nullable=true)
     */
    private $codename;

    /**
     * @var boolean $isNutriMe
     *
     * @ORM\Column(name="is_nutri_me", type="boolean", nullable=true)
     */
    private $isNutriMe;

    /**
     * @var integer $idProductGroup
     *
     * @ORM\Column(name="id_product_group", type="integer", nullable=true)
     */
    private $idProductGroup;

    /**
     * @var integer $productTypeAustria
     *
     * @ORM\Column(name="product_type_austria", type="integer", nullable=true)
     */
    private $productTypeAustria;

    /**
     * @var integer $productTypeGermany
     *
     * @ORM\Column(name="product_type_germany", type="integer", nullable=true)
     */
    private $productTypeGermany;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductsDnaPlus
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set standardpriceexclvat
     *
     * @param float $standardpriceexclvat
     * @return ProductsDnaPlus
     */
    public function setStandardpriceexclvat($standardpriceexclvat)
    {
        $this->standardpriceexclvat = $standardpriceexclvat;
    
        return $this;
    }

    /**
     * Get standardpriceexclvat
     *
     * @return float 
     */
    public function getStandardpriceexclvat()
    {
        return $this->standardpriceexclvat;
    }

    /**
     * Set materialcosts
     *
     * @param float $materialcosts
     * @return ProductsDnaPlus
     */
    public function setMaterialcosts($materialcosts)
    {
        $this->materialcosts = $materialcosts;
    
        return $this;
    }

    /**
     * Get materialcosts
     *
     * @return float 
     */
    public function getMaterialcosts()
    {
        return $this->materialcosts;
    }

    /**
     * Set productioncostsnoreports
     *
     * @param float $productioncostsnoreports
     * @return ProductsDnaPlus
     */
    public function setProductioncostsnoreports($productioncostsnoreports)
    {
        $this->productioncostsnoreports = $productioncostsnoreports;
    
        return $this;
    }

    /**
     * Get productioncostsnoreports
     *
     * @return float 
     */
    public function getProductioncostsnoreports()
    {
        return $this->productioncostsnoreports;
    }

    /**
     * Set doctorsmargin
     *
     * @param string $doctorsmargin
     * @return ProductsDnaPlus
     */
    public function setDoctorsmargin($doctorsmargin)
    {
        $this->doctorsmargin = $doctorsmargin;
    
        return $this;
    }

    /**
     * Get doctorsmargin
     *
     * @return string 
     */
    public function getDoctorsmargin()
    {
        return $this->doctorsmargin;
    }

    /**
     * Set distributionchannelmargin
     *
     * @param string $distributionchannelmargin
     * @return ProductsDnaPlus
     */
    public function setDistributionchannelmargin($distributionchannelmargin)
    {
        $this->distributionchannelmargin = $distributionchannelmargin;
    
        return $this;
    }

    /**
     * Get distributionchannelmargin
     *
     * @return string 
     */
    public function getDistributionchannelmargin()
    {
        return $this->distributionchannelmargin;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return ProductsDnaPlus
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set codename
     *
     * @param string $codename
     * @return ProductsDnaPlus
     */
    public function setCodename($codename)
    {
        $this->codename = $codename;
    
        return $this;
    }

    /**
     * Get codename
     *
     * @return string 
     */
    public function getCodename()
    {
        return $this->codename;
    }

    /**
     * Set isNutriMe
     *
     * @param boolean $isNutriMe
     * @return ProductsDnaPlus
     */
    public function setIsNutriMe($isNutriMe)
    {
        $this->isNutriMe = $isNutriMe;
    
        return $this;
    }

    /**
     * Get isNutriMe
     *
     * @return boolean 
     */
    public function getIsNutriMe()
    {
        return $this->isNutriMe;
    }

    /**
     * Set idProductGroup
     *
     * @param integer $idProductGroup
     * @return ProductsDnaPlus
     */
    public function setIdProductGroup($idProductGroup)
    {
        $this->idProductGroup = $idProductGroup;
    
        return $this;
    }

    /**
     * Get idProductGroup
     *
     * @return integer 
     */
    public function getIdProductGroup()
    {
        return $this->idProductGroup;
    }

    /**
     * Set productTypeAustria
     *
     * @param integer $productTypeAustria
     * @return ProductsDnaPlus
     */
    public function setProductTypeAustria($productTypeAustria)
    {
        $this->productTypeAustria = $productTypeAustria;
    
        return $this;
    }

    /**
     * Get productTypeAustria
     *
     * @return integer 
     */
    public function getProductTypeAustria()
    {
        return $this->productTypeAustria;
    }

    /**
     * Set productTypeGermany
     *
     * @param integer $productTypeGermany
     * @return ProductsDnaPlus
     */
    public function setProductTypeGermany($productTypeGermany)
    {
        $this->productTypeGermany = $productTypeGermany;
    
        return $this;
    }

    /**
     * Get productTypeGermany
     *
     * @return integer 
     */
    public function getProductTypeGermany()
    {
        return $this->productTypeGermany;
    }
}