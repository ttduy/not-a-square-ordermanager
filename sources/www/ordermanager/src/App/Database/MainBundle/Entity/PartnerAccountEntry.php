<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PartnerAccountEntry
 *
 * @ORM\Table(name="partner_account_entry")
 * @ORM\Entity
 */
class PartnerAccountEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var \DateTime $entryDate
     *
     * @ORM\Column(name="entry_date", type="date", nullable=true)
     */
    private $entryDate;

    /**
     * @var integer $idDirection
     *
     * @ORM\Column(name="id_direction", type="integer", nullable=true)
     */
    private $idDirection;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @var string $entryNote
     *
     * @ORM\Column(name="entry_note", type="text", nullable=true)
     */
    private $entryNote;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return PartnerAccountEntry
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     * @return PartnerAccountEntry
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;
    
        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime 
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Set idDirection
     *
     * @param integer $idDirection
     * @return PartnerAccountEntry
     */
    public function setIdDirection($idDirection)
    {
        $this->idDirection = $idDirection;
    
        return $this;
    }

    /**
     * Get idDirection
     *
     * @return integer 
     */
    public function getIdDirection()
    {
        return $this->idDirection;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return PartnerAccountEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set entryNote
     *
     * @param string $entryNote
     * @return PartnerAccountEntry
     */
    public function setEntryNote($entryNote)
    {
        $this->entryNote = $entryNote;
    
        return $this;
    }

    /**
     * Get entryNote
     *
     * @return string 
     */
    public function getEntryNote()
    {
        return $this->entryNote;
    }
}