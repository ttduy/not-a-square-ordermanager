<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoStorageCell
 *
 * @ORM\Table(name="geno_storage_cell")
 * @ORM\Entity
 */
class GenoStorageCell
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var integer $storageId
     *
     * @ORM\Column(name="storage_id", type="integer", nullable=true)
     */
    private $storageId;

    /**
     * @var integer $loading
     *
     * @ORM\Column(name="loading", type="integer", nullable=true)
     */
    private $loading;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return GenoStorageCell
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set storageId
     *
     * @param integer $storageId
     * @return GenoStorageCell
     */
    public function setStorageId($storageId)
    {
        $this->storageId = $storageId;
    
        return $this;
    }

    /**
     * Get storageId
     *
     * @return integer 
     */
    public function getStorageId()
    {
        return $this->storageId;
    }

    /**
     * Set loading
     *
     * @param integer $loading
     * @return GenoStorageCell
     */
    public function setLoading($loading)
    {
        $this->loading = $loading;
    
        return $this;
    }

    /**
     * Get loading
     *
     * @return integer 
     */
    public function getLoading()
    {
        return $this->loading;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return GenoStorageCell
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GenoStorageCell
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}