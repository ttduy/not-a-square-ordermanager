<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillingInvoicePartnerEntry
 *
 * @ORM\Table(name="billing_invoice_partner_entry")
 * @ORM\Entity
 */
class BillingInvoicePartnerEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBillingInvoicePartner
     *
     * @ORM\Column(name="id_billing_invoice_partner", type="integer", nullable=true)
     */
    private $idBillingInvoicePartner;

    /**
     * @var string $entry
     *
     * @ORM\Column(name="entry", type="string", length=255, nullable=true)
     */
    private $entry;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @var string $entryId
     *
     * @ORM\Column(name="entry_id", type="string", length=50, nullable=true)
     */
    private $entryId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBillingInvoicePartner
     *
     * @param integer $idBillingInvoicePartner
     * @return BillingInvoicePartnerEntry
     */
    public function setIdBillingInvoicePartner($idBillingInvoicePartner)
    {
        $this->idBillingInvoicePartner = $idBillingInvoicePartner;
    
        return $this;
    }

    /**
     * Get idBillingInvoicePartner
     *
     * @return integer 
     */
    public function getIdBillingInvoicePartner()
    {
        return $this->idBillingInvoicePartner;
    }

    /**
     * Set entry
     *
     * @param string $entry
     * @return BillingInvoicePartnerEntry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    
        return $this;
    }

    /**
     * Get entry
     *
     * @return string 
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return BillingInvoicePartnerEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set entryId
     *
     * @param string $entryId
     * @return BillingInvoicePartnerEntry
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    
        return $this;
    }

    /**
     * Get entryId
     *
     * @return string 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }
}