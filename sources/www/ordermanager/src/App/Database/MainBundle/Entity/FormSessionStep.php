<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormSessionStep
 *
 * @ORM\Table(name="form_session_step")
 * @ORM\Entity
 */
class FormSessionStep
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFormSession
     *
     * @ORM\Column(name="id_form_session", type="integer", nullable=true)
     */
    private $idFormSession;

    /**
     * @var integer $idFormStep
     *
     * @ORM\Column(name="id_form_step", type="integer", nullable=true)
     */
    private $idFormStep;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormSession
     *
     * @param integer $idFormSession
     * @return FormSessionStep
     */
    public function setIdFormSession($idFormSession)
    {
        $this->idFormSession = $idFormSession;
    
        return $this;
    }

    /**
     * Get idFormSession
     *
     * @return integer 
     */
    public function getIdFormSession()
    {
        return $this->idFormSession;
    }

    /**
     * Set idFormStep
     *
     * @param integer $idFormStep
     * @return FormSessionStep
     */
    public function setIdFormStep($idFormStep)
    {
        $this->idFormStep = $idFormStep;
    
        return $this;
    }

    /**
     * Get idFormStep
     *
     * @return integer 
     */
    public function getIdFormStep()
    {
        return $this->idFormStep;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return FormSessionStep
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return FormSessionStep
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return FormSessionStep
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }
}