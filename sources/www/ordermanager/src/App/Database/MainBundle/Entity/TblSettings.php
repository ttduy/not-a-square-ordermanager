<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TblSettings
 *
 * @ORM\Table(name="tbl_settings")
 * @ORM\Entity
 */
class TblSettings
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $setvar
     *
     * @ORM\Column(name="SetVar", type="string", length=255, nullable=true)
     */
    private $setvar;

    /**
     * @var string $valvar
     *
     * @ORM\Column(name="ValVar", type="string", length=255, nullable=true)
     */
    private $valvar;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set setvar
     *
     * @param string $setvar
     * @return TblSettings
     */
    public function setSetvar($setvar)
    {
        $this->setvar = $setvar;
    
        return $this;
    }

    /**
     * Get setvar
     *
     * @return string 
     */
    public function getSetvar()
    {
        return $this->setvar;
    }

    /**
     * Set valvar
     *
     * @param string $valvar
     * @return TblSettings
     */
    public function setValvar($valvar)
    {
        $this->valvar = $valvar;
    
        return $this;
    }

    /**
     * Get valvar
     *
     * @return string 
     */
    public function getValvar()
    {
        return $this->valvar;
    }
}