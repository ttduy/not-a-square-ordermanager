<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaNextStatus
 *
 * @ORM\Table(name="mwa_next_status")
 * @ORM\Entity
 */
class MwaNextStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idMwaStatus
     *
     * @ORM\Column(name="id_mwa_status", type="integer", nullable=true)
     */
    private $idMwaStatus;

    /**
     * @var integer $idMwaNextStatus
     *
     * @ORM\Column(name="id_mwa_next_status", type="integer", nullable=true)
     */
    private $idMwaNextStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMwaStatus
     *
     * @param integer $idMwaStatus
     * @return MwaNextStatus
     */
    public function setIdMwaStatus($idMwaStatus)
    {
        $this->idMwaStatus = $idMwaStatus;
    
        return $this;
    }

    /**
     * Get idMwaStatus
     *
     * @return integer 
     */
    public function getIdMwaStatus()
    {
        return $this->idMwaStatus;
    }

    /**
     * Set idMwaNextStatus
     *
     * @param integer $idMwaNextStatus
     * @return MwaNextStatus
     */
    public function setIdMwaNextStatus($idMwaNextStatus)
    {
        $this->idMwaNextStatus = $idMwaNextStatus;
    
        return $this;
    }

    /**
     * Get idMwaNextStatus
     *
     * @return integer 
     */
    public function getIdMwaNextStatus()
    {
        return $this->idMwaNextStatus;
    }
}