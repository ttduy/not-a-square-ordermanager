<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FoodTableSmileyConversion
 *
 * @ORM\Table(name="food_table_smiley_conversion")
 * @ORM\Entity
 */
class FoodTableSmileyConversion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $fromValue
     *
     * @ORM\Column(name="from_value", type="integer", nullable=true)
     */
    private $fromValue;

    /**
     * @var integer $toValue
     *
     * @ORM\Column(name="to_value", type="integer", nullable=true)
     */
    private $toValue;

    /**
     * @var integer $normalizedValue
     *
     * @ORM\Column(name="normalized_value", type="integer", nullable=true)
     */
    private $normalizedValue;

    /**
     * @var string $text
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromValue
     *
     * @param integer $fromValue
     * @return FoodTableSmileyConversion
     */
    public function setFromValue($fromValue)
    {
        $this->fromValue = $fromValue;
    
        return $this;
    }

    /**
     * Get fromValue
     *
     * @return integer 
     */
    public function getFromValue()
    {
        return $this->fromValue;
    }

    /**
     * Set toValue
     *
     * @param integer $toValue
     * @return FoodTableSmileyConversion
     */
    public function setToValue($toValue)
    {
        $this->toValue = $toValue;
    
        return $this;
    }

    /**
     * Get toValue
     *
     * @return integer 
     */
    public function getToValue()
    {
        return $this->toValue;
    }

    /**
     * Set normalizedValue
     *
     * @param integer $normalizedValue
     * @return FoodTableSmileyConversion
     */
    public function setNormalizedValue($normalizedValue)
    {
        $this->normalizedValue = $normalizedValue;
    
        return $this;
    }

    /**
     * Get normalizedValue
     *
     * @return integer 
     */
    public function getNormalizedValue()
    {
        return $this->normalizedValue;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return FoodTableSmileyConversion
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }
}