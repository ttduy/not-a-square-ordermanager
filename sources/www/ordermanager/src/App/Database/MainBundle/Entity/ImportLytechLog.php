<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ImportLytechLog
 *
 * @ORM\Table(name="import_lytech_log")
 * @ORM\Entity
 */
class ImportLytechLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $userId
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \DateTime $importTime
     *
     * @ORM\Column(name="import_time", type="datetime", nullable=true)
     */
    private $importTime;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string $errorMessage
     *
     * @ORM\Column(name="error_message", type="string", length=100, nullable=true)
     */
    private $errorMessage;

    /**
     * @var string $newSamples
     *
     * @ORM\Column(name="new_samples", type="text", nullable=true)
     */
    private $newSamples;

    /**
     * @var string $changedSamples
     *
     * @ORM\Column(name="changed_samples", type="text", nullable=true)
     */
    private $changedSamples;

    /**
     * @var string $errorSamples
     *
     * @ORM\Column(name="error_samples", type="text", nullable=true)
     */
    private $errorSamples;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return ImportLytechLog
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set importTime
     *
     * @param \DateTime $importTime
     * @return ImportLytechLog
     */
    public function setImportTime($importTime)
    {
        $this->importTime = $importTime;
    
        return $this;
    }

    /**
     * Get importTime
     *
     * @return \DateTime 
     */
    public function getImportTime()
    {
        return $this->importTime;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return ImportLytechLog
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return ImportLytechLog
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ImportLytechLog
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     * @return ImportLytechLog
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    
        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string 
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set newSamples
     *
     * @param string $newSamples
     * @return ImportLytechLog
     */
    public function setNewSamples($newSamples)
    {
        $this->newSamples = $newSamples;
    
        return $this;
    }

    /**
     * Get newSamples
     *
     * @return string 
     */
    public function getNewSamples()
    {
        return $this->newSamples;
    }

    /**
     * Set changedSamples
     *
     * @param string $changedSamples
     * @return ImportLytechLog
     */
    public function setChangedSamples($changedSamples)
    {
        $this->changedSamples = $changedSamples;
    
        return $this;
    }

    /**
     * Get changedSamples
     *
     * @return string 
     */
    public function getChangedSamples()
    {
        return $this->changedSamples;
    }

    /**
     * Set errorSamples
     *
     * @param string $errorSamples
     * @return ImportLytechLog
     */
    public function setErrorSamples($errorSamples)
    {
        $this->errorSamples = $errorSamples;
    
        return $this;
    }

    /**
     * Get errorSamples
     *
     * @return string 
     */
    public function getErrorSamples()
    {
        return $this->errorSamples;
    }
}