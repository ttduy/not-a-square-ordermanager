<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectivePayment
 *
 * @ORM\Table(name="collective_payment")
 * @ORM\Entity
 */
class CollectivePayment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $paymentnumber
     *
     * @ORM\Column(name="PaymentNumber", type="string", length=255, nullable=true)
     */
    private $paymentnumber;

    /**
     * @var \DateTime $paymentdate
     *
     * @ORM\Column(name="PaymentDate", type="date", nullable=true)
     */
    private $paymentdate;

    /**
     * @var integer $paymentfromcompanyid
     *
     * @ORM\Column(name="PaymentFromCompanyId", type="integer", nullable=true)
     */
    private $paymentfromcompanyid;

    /**
     * @var boolean $iswithtax
     *
     * @ORM\Column(name="IsWithTax", type="boolean", nullable=true)
     */
    private $iswithtax;

    /**
     * @var integer $destinationid
     *
     * @ORM\Column(name="DestinationId", type="integer", nullable=true)
     */
    private $destinationid;

    /**
     * @var integer $partnerid
     *
     * @ORM\Column(name="PartnerId", type="integer", nullable=true)
     */
    private $partnerid;

    /**
     * @var integer $distributionchannelid
     *
     * @ORM\Column(name="DistributionChannelId", type="integer", nullable=true)
     */
    private $distributionchannelid;

    /**
     * @var integer $paymentstatus
     *
     * @ORM\Column(name="PaymentStatus", type="integer", nullable=true)
     */
    private $paymentstatus;

    /**
     * @var integer $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="integer", nullable=true)
     */
    private $isdeleted;

    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    private $taxValue;

    /**
     * @var float $overrideAmount
     *
     * @ORM\Column(name="override_amount", type="decimal", nullable=true)
     */
    private $overrideAmount;

    /**
     * @var string $paymentFile
     *
     * @ORM\Column(name="payment_file", type="string", length=255, nullable=true)
     */
    private $paymentFile;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CollectivePayment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set paymentnumber
     *
     * @param string $paymentnumber
     * @return CollectivePayment
     */
    public function setPaymentnumber($paymentnumber)
    {
        $this->paymentnumber = $paymentnumber;
    
        return $this;
    }

    /**
     * Get paymentnumber
     *
     * @return string 
     */
    public function getPaymentnumber()
    {
        return $this->paymentnumber;
    }

    /**
     * Set paymentdate
     *
     * @param \DateTime $paymentdate
     * @return CollectivePayment
     */
    public function setPaymentdate($paymentdate)
    {
        $this->paymentdate = $paymentdate;
    
        return $this;
    }

    /**
     * Get paymentdate
     *
     * @return \DateTime 
     */
    public function getPaymentdate()
    {
        return $this->paymentdate;
    }

    /**
     * Set paymentfromcompanyid
     *
     * @param integer $paymentfromcompanyid
     * @return CollectivePayment
     */
    public function setPaymentfromcompanyid($paymentfromcompanyid)
    {
        $this->paymentfromcompanyid = $paymentfromcompanyid;
    
        return $this;
    }

    /**
     * Get paymentfromcompanyid
     *
     * @return integer 
     */
    public function getPaymentfromcompanyid()
    {
        return $this->paymentfromcompanyid;
    }

    /**
     * Set iswithtax
     *
     * @param boolean $iswithtax
     * @return CollectivePayment
     */
    public function setIswithtax($iswithtax)
    {
        $this->iswithtax = $iswithtax;
    
        return $this;
    }

    /**
     * Get iswithtax
     *
     * @return boolean 
     */
    public function getIswithtax()
    {
        return $this->iswithtax;
    }

    /**
     * Set destinationid
     *
     * @param integer $destinationid
     * @return CollectivePayment
     */
    public function setDestinationid($destinationid)
    {
        $this->destinationid = $destinationid;
    
        return $this;
    }

    /**
     * Get destinationid
     *
     * @return integer 
     */
    public function getDestinationid()
    {
        return $this->destinationid;
    }

    /**
     * Set partnerid
     *
     * @param integer $partnerid
     * @return CollectivePayment
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;
    
        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer 
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set distributionchannelid
     *
     * @param integer $distributionchannelid
     * @return CollectivePayment
     */
    public function setDistributionchannelid($distributionchannelid)
    {
        $this->distributionchannelid = $distributionchannelid;
    
        return $this;
    }

    /**
     * Get distributionchannelid
     *
     * @return integer 
     */
    public function getDistributionchannelid()
    {
        return $this->distributionchannelid;
    }

    /**
     * Set paymentstatus
     *
     * @param integer $paymentstatus
     * @return CollectivePayment
     */
    public function setPaymentstatus($paymentstatus)
    {
        $this->paymentstatus = $paymentstatus;
    
        return $this;
    }

    /**
     * Get paymentstatus
     *
     * @return integer 
     */
    public function getPaymentstatus()
    {
        return $this->paymentstatus;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return CollectivePayment
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set taxValue
     *
     * @param float $taxValue
     * @return CollectivePayment
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    
        return $this;
    }

    /**
     * Get taxValue
     *
     * @return float 
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }

    /**
     * Set overrideAmount
     *
     * @param float $overrideAmount
     * @return CollectivePayment
     */
    public function setOverrideAmount($overrideAmount)
    {
        $this->overrideAmount = $overrideAmount;
    
        return $this;
    }

    /**
     * Get overrideAmount
     *
     * @return float 
     */
    public function getOverrideAmount()
    {
        return $this->overrideAmount;
    }

    /**
     * Set paymentFile
     *
     * @param string $paymentFile
     * @return CollectivePayment
     */
    public function setPaymentFile($paymentFile)
    {
        $this->paymentFile = $paymentFile;
    
        return $this;
    }

    /**
     * Get paymentFile
     *
     * @return string 
     */
    public function getPaymentFile()
    {
        return $this->paymentFile;
    }
}