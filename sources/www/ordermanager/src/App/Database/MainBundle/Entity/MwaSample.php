<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaSample
 *
 * @ORM\Table(name="mwa_sample")
 * @ORM\Entity
 */
class MwaSample
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $sampleId
     *
     * @ORM\Column(name="sample_id", type="string", length=255, nullable=true)
     */
    private $sampleId;

    /**
     * @var string $sampleGroup
     *
     * @ORM\Column(name="sample_group", type="string", length=255, nullable=true)
     */
    private $sampleGroup;

    /**
     * @var integer $age
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string $resultPattern
     *
     * @ORM\Column(name="result_pattern", type="string", length=255, nullable=true)
     */
    private $resultPattern;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var boolean $isDeleted
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var boolean $isSendNow
     *
     * @ORM\Column(name="is_send_now", type="boolean", nullable=true)
     */
    private $isSendNow;

    /**
     * @var integer $sendingStatus
     *
     * @ORM\Column(name="sending_status", type="integer", nullable=true)
     */
    private $sendingStatus;

    /**
     * @var integer $pushStatus
     *
     * @ORM\Column(name="push_status", type="integer", nullable=true)
     */
    private $pushStatus;

    /**
     * @var \DateTime $pushDate
     *
     * @ORM\Column(name="push_date", type="date", nullable=true)
     */
    private $pushDate;

    /**
     * @var string $lastError
     *
     * @ORM\Column(name="last_error", type="text", nullable=true)
     */
    private $lastError;

    /**
     * @var \DateTime $pushDatetime
     *
     * @ORM\Column(name="push_datetime", type="datetime", nullable=true)
     */
    private $pushDatetime;

    /**
     * @var integer $idMwaInvoice
     *
     * @ORM\Column(name="id_mwa_invoice", type="integer", nullable=true)
     */
    private $idMwaInvoice;

    /**
     * @var float $priceOverride
     *
     * @ORM\Column(name="price_override", type="decimal", nullable=true)
     */
    private $priceOverride;

    /**
     * @var \DateTime $registeredDate
     *
     * @ORM\Column(name="registered_date", type="date", nullable=true)
     */
    private $registeredDate;

    /**
     * @var \DateTime $finishedDate
     *
     * @ORM\Column(name="finished_date", type="date", nullable=true)
     */
    private $finishedDate;

    /**
     * @var boolean $isOverrideSending
     *
     * @ORM\Column(name="is_override_sending", type="boolean", nullable=true)
     */
    private $isOverrideSending;

    /**
     * @var integer $numFailed
     *
     * @ORM\Column(name="num_failed", type="integer", nullable=true)
     */
    private $numFailed;

    /**
     * @var string $infoText
     *
     * @ORM\Column(name="info_text", type="text", nullable=true)
     */
    private $infoText;

    /**
     * @var integer $idGroup
     *
     * @ORM\Column(name="id_group", type="integer", nullable=true)
     */
    private $idGroup;

    /**
     * @var integer $isLocal
     *
     * @ORM\Column(name="is_local", type="integer", nullable=true)
     */
    private $isLocal;

    /**
     * @var integer $isError
     *
     * @ORM\Column(name="is_error", type="integer", nullable=true)
     */
    private $isError;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var string $gew1
     *
     * @ORM\Column(name="gew_1", type="string", length=25, nullable=true)
     */
    private $gew1;

    /**
     * @var string $gew2
     *
     * @ORM\Column(name="gew_2", type="string", length=25, nullable=true)
     */
    private $gew2;

    /**
     * @var string $gew3
     *
     * @ORM\Column(name="gew_3", type="string", length=25, nullable=true)
     */
    private $gew3;

    /**
     * @var string $gew4
     *
     * @ORM\Column(name="gew_4", type="string", length=25, nullable=true)
     */
    private $gew4;

    /**
     * @var string $gew5
     *
     * @ORM\Column(name="gew_5", type="string", length=25, nullable=true)
     */
    private $gew5;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleId
     *
     * @param string $sampleId
     * @return MwaSample
     */
    public function setSampleId($sampleId)
    {
        $this->sampleId = $sampleId;
    
        return $this;
    }

    /**
     * Get sampleId
     *
     * @return string 
     */
    public function getSampleId()
    {
        return $this->sampleId;
    }

    /**
     * Set sampleGroup
     *
     * @param string $sampleGroup
     * @return MwaSample
     */
    public function setSampleGroup($sampleGroup)
    {
        $this->sampleGroup = $sampleGroup;
    
        return $this;
    }

    /**
     * Get sampleGroup
     *
     * @return string 
     */
    public function getSampleGroup()
    {
        return $this->sampleGroup;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return MwaSample
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set resultPattern
     *
     * @param string $resultPattern
     * @return MwaSample
     */
    public function setResultPattern($resultPattern)
    {
        $this->resultPattern = $resultPattern;
    
        return $this;
    }

    /**
     * Get resultPattern
     *
     * @return string 
     */
    public function getResultPattern()
    {
        return $this->resultPattern;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return MwaSample
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return MwaSample
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isSendNow
     *
     * @param boolean $isSendNow
     * @return MwaSample
     */
    public function setIsSendNow($isSendNow)
    {
        $this->isSendNow = $isSendNow;
    
        return $this;
    }

    /**
     * Get isSendNow
     *
     * @return boolean 
     */
    public function getIsSendNow()
    {
        return $this->isSendNow;
    }

    /**
     * Set sendingStatus
     *
     * @param integer $sendingStatus
     * @return MwaSample
     */
    public function setSendingStatus($sendingStatus)
    {
        $this->sendingStatus = $sendingStatus;
    
        return $this;
    }

    /**
     * Get sendingStatus
     *
     * @return integer 
     */
    public function getSendingStatus()
    {
        return $this->sendingStatus;
    }

    /**
     * Set pushStatus
     *
     * @param integer $pushStatus
     * @return MwaSample
     */
    public function setPushStatus($pushStatus)
    {
        $this->pushStatus = $pushStatus;
    
        return $this;
    }

    /**
     * Get pushStatus
     *
     * @return integer 
     */
    public function getPushStatus()
    {
        return $this->pushStatus;
    }

    /**
     * Set pushDate
     *
     * @param \DateTime $pushDate
     * @return MwaSample
     */
    public function setPushDate($pushDate)
    {
        $this->pushDate = $pushDate;
    
        return $this;
    }

    /**
     * Get pushDate
     *
     * @return \DateTime 
     */
    public function getPushDate()
    {
        return $this->pushDate;
    }

    /**
     * Set lastError
     *
     * @param string $lastError
     * @return MwaSample
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    
        return $this;
    }

    /**
     * Get lastError
     *
     * @return string 
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Set pushDatetime
     *
     * @param \DateTime $pushDatetime
     * @return MwaSample
     */
    public function setPushDatetime($pushDatetime)
    {
        $this->pushDatetime = $pushDatetime;
    
        return $this;
    }

    /**
     * Get pushDatetime
     *
     * @return \DateTime 
     */
    public function getPushDatetime()
    {
        return $this->pushDatetime;
    }

    /**
     * Set idMwaInvoice
     *
     * @param integer $idMwaInvoice
     * @return MwaSample
     */
    public function setIdMwaInvoice($idMwaInvoice)
    {
        $this->idMwaInvoice = $idMwaInvoice;
    
        return $this;
    }

    /**
     * Get idMwaInvoice
     *
     * @return integer 
     */
    public function getIdMwaInvoice()
    {
        return $this->idMwaInvoice;
    }

    /**
     * Set priceOverride
     *
     * @param float $priceOverride
     * @return MwaSample
     */
    public function setPriceOverride($priceOverride)
    {
        $this->priceOverride = $priceOverride;
    
        return $this;
    }

    /**
     * Get priceOverride
     *
     * @return float 
     */
    public function getPriceOverride()
    {
        return $this->priceOverride;
    }

    /**
     * Set registeredDate
     *
     * @param \DateTime $registeredDate
     * @return MwaSample
     */
    public function setRegisteredDate($registeredDate)
    {
        $this->registeredDate = $registeredDate;
    
        return $this;
    }

    /**
     * Get registeredDate
     *
     * @return \DateTime 
     */
    public function getRegisteredDate()
    {
        return $this->registeredDate;
    }

    /**
     * Set finishedDate
     *
     * @param \DateTime $finishedDate
     * @return MwaSample
     */
    public function setFinishedDate($finishedDate)
    {
        $this->finishedDate = $finishedDate;
    
        return $this;
    }

    /**
     * Get finishedDate
     *
     * @return \DateTime 
     */
    public function getFinishedDate()
    {
        return $this->finishedDate;
    }

    /**
     * Set isOverrideSending
     *
     * @param boolean $isOverrideSending
     * @return MwaSample
     */
    public function setIsOverrideSending($isOverrideSending)
    {
        $this->isOverrideSending = $isOverrideSending;
    
        return $this;
    }

    /**
     * Get isOverrideSending
     *
     * @return boolean 
     */
    public function getIsOverrideSending()
    {
        return $this->isOverrideSending;
    }

    /**
     * Set numFailed
     *
     * @param integer $numFailed
     * @return MwaSample
     */
    public function setNumFailed($numFailed)
    {
        $this->numFailed = $numFailed;
    
        return $this;
    }

    /**
     * Get numFailed
     *
     * @return integer 
     */
    public function getNumFailed()
    {
        return $this->numFailed;
    }

    /**
     * Set infoText
     *
     * @param string $infoText
     * @return MwaSample
     */
    public function setInfoText($infoText)
    {
        $this->infoText = $infoText;
    
        return $this;
    }

    /**
     * Get infoText
     *
     * @return string 
     */
    public function getInfoText()
    {
        return $this->infoText;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     * @return MwaSample
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;
    
        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer 
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set isLocal
     *
     * @param integer $isLocal
     * @return MwaSample
     */
    public function setIsLocal($isLocal)
    {
        $this->isLocal = $isLocal;
    
        return $this;
    }

    /**
     * Get isLocal
     *
     * @return integer 
     */
    public function getIsLocal()
    {
        return $this->isLocal;
    }

    /**
     * Set isError
     *
     * @param integer $isError
     * @return MwaSample
     */
    public function setIsError($isError)
    {
        $this->isError = $isError;
    
        return $this;
    }

    /**
     * Get isError
     *
     * @return integer 
     */
    public function getIsError()
    {
        return $this->isError;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return MwaSample
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }

    /**
     * Set gew1
     *
     * @param string $gew1
     * @return MwaSample
     */
    public function setGew1($gew1)
    {
        $this->gew1 = $gew1;
    
        return $this;
    }

    /**
     * Get gew1
     *
     * @return string 
     */
    public function getGew1()
    {
        return $this->gew1;
    }

    /**
     * Set gew2
     *
     * @param string $gew2
     * @return MwaSample
     */
    public function setGew2($gew2)
    {
        $this->gew2 = $gew2;
    
        return $this;
    }

    /**
     * Get gew2
     *
     * @return string 
     */
    public function getGew2()
    {
        return $this->gew2;
    }

    /**
     * Set gew3
     *
     * @param string $gew3
     * @return MwaSample
     */
    public function setGew3($gew3)
    {
        $this->gew3 = $gew3;
    
        return $this;
    }

    /**
     * Get gew3
     *
     * @return string 
     */
    public function getGew3()
    {
        return $this->gew3;
    }

    /**
     * Set gew4
     *
     * @param string $gew4
     * @return MwaSample
     */
    public function setGew4($gew4)
    {
        $this->gew4 = $gew4;
    
        return $this;
    }

    /**
     * Get gew4
     *
     * @return string 
     */
    public function getGew4()
    {
        return $this->gew4;
    }

    /**
     * Set gew5
     *
     * @param string $gew5
     * @return MwaSample
     */
    public function setGew5($gew5)
    {
        $this->gew5 = $gew5;
    
        return $this;
    }

    /**
     * Get gew5
     *
     * @return string 
     */
    public function getGew5()
    {
        return $this->gew5;
    }
}