<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerInfo
 *
 * @ORM\Table(name="customer_info")
 * @ORM\Entity
 */
class CustomerInfo
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $customerNumber
     *
     * @ORM\Column(name="customer_number", type="string", length=255, nullable=true)
     */
    private $customerNumber;

    /**
     * @var string $externalBarcode
     *
     * @ORM\Column(name="external_barcode", type="string", length=255, nullable=true)
     */
    private $externalBarcode;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=false)
     */
    private $idDistributionChannel;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $surName
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @var \DateTime $dateofbirth
     *
     * @ORM\Column(name="DateOfBirth", type="date", nullable=true)
     */
    private $dateofbirth;

    /**
     * @var string $postCode
     *
     * @ORM\Column(name="post_code", type="string", length=255, nullable=true)
     */
    private $postCode;

    /**
     * @var boolean $isDestroySample
     *
     * @ORM\Column(name="is_destroy_sample", type="boolean", nullable=false)
     */
    private $isDestroySample;

    /**
     * @var boolean $isFutureResearchParticipant
     *
     * @ORM\Column(name="is_future_research_participant", type="boolean", nullable=false)
     */
    private $isFutureResearchParticipant;

    /**
     * @var string $detailInformation
     *
     * @ORM\Column(name="detail_information", type="text", nullable=true)
     */
    private $detailInformation;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $webLoginUsername
     *
     * @ORM\Column(name="web_login_username", type="string", length=255, nullable=true)
     */
    private $webLoginUsername;

    /**
     * @var string $webLoginPassword
     *
     * @ORM\Column(name="web_login_password", type="string", length=255, nullable=true)
     */
    private $webLoginPassword;

    /**
     * @var integer $webLoginStatus
     *
     * @ORM\Column(name="web_login_status", type="integer", nullable=true)
     */
    private $webLoginStatus;

    /**
     * @var boolean $isCustomerBeContacted
     *
     * @ORM\Column(name="is_customer_be_contacted", type="boolean", nullable=true)
     */
    private $isCustomerBeContacted;

    /**
     * @var boolean $isNutrimeOffered
     *
     * @ORM\Column(name="is_nutrime_offered", type="boolean", nullable=true)
     */
    private $isNutrimeOffered;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var \DateTime $reportAvailableDate
     *
     * @ORM\Column(name="report_available_date", type="date", nullable=true)
     */
    private $reportAvailableDate;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var integer $numberFeedback
     *
     * @ORM\Column(name="number_feedback", type="integer", nullable=true)
     */
    private $numberFeedback;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer $webIdLanguage
     *
     * @ORM\Column(name="web_id_language", type="integer", nullable=true)
     */
    private $webIdLanguage;

    /**
     * @var boolean $sampleCanBeUsedForScience
     *
     * @ORM\Column(name="sample_can_be_used_for_science", type="boolean", nullable=true)
     */
    private $sampleCanBeUsedForScience;

    /**
     * @var boolean $isLocked
     *
     * @ORM\Column(name="is_locked", type="boolean", nullable=true)
     */
    private $isLocked;

    /**
     * @var \DateTime $lockDate
     *
     * @ORM\Column(name="lock_date", type="date", nullable=true)
     */
    private $lockDate;

    /**
     * @var boolean $isWebLoginOptOut
     *
     * @ORM\Column(name="is_web_login_opt_out", type="boolean", nullable=true)
     */
    private $isWebLoginOptOut;

    /**
     * @var boolean $isCanViewYourOrder
     *
     * @ORM\Column(name="is_can_view_your_order", type="boolean", nullable=true)
     */
    private $isCanViewYourOrder;

    /**
     * @var boolean $isCanViewCustomerProtection
     *
     * @ORM\Column(name="is_can_view_customer_protection", type="boolean", nullable=true)
     */
    private $isCanViewCustomerProtection;

    /**
     * @var boolean $isCanViewRegisterNewCustomer
     *
     * @ORM\Column(name="is_can_view_register_new_customer", type="boolean", nullable=true)
     */
    private $isCanViewRegisterNewCustomer;

    /**
     * @var boolean $isCanViewPendingOrder
     *
     * @ORM\Column(name="is_can_view_pending_order", type="boolean", nullable=true)
     */
    private $isCanViewPendingOrder;

    /**
     * @var boolean $isCanViewSetting
     *
     * @ORM\Column(name="is_can_view_setting", type="boolean", nullable=true)
     */
    private $isCanViewSetting;

    /**
     * @var boolean $canViewShop
     *
     * @ORM\Column(name="can_view_shop", type="boolean", nullable=true)
     */
    private $canViewShop;

    /**
     * @var integer $webLoginGoesTo
     *
     * @ORM\Column(name="web_login_goes_to", type="integer", nullable=true)
     */
    private $webLoginGoesTo;

    /**
     * @var integer $accountingCode
     *
     * @ORM\Column(name="accounting_code", type="integer", nullable=true)
     */
    private $accountingCode;


}
