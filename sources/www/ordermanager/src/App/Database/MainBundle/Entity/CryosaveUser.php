<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CryosaveUser
 *
 * @ORM\Table(name="cryosave_user")
 * @ORM\Entity
 */
class CryosaveUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLaboratory
     *
     * @ORM\Column(name="id_laboratory", type="integer", nullable=true)
     */
    private $idLaboratory;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLaboratory
     *
     * @param integer $idLaboratory
     * @return CryosaveUser
     */
    public function setIdLaboratory($idLaboratory)
    {
        $this->idLaboratory = $idLaboratory;
    
        return $this;
    }

    /**
     * Get idLaboratory
     *
     * @return integer 
     */
    public function getIdLaboratory()
    {
        return $this->idLaboratory;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return CryosaveUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return CryosaveUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
}