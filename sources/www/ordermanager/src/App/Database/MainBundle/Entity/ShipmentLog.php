<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ShipmentLog
 *
 * @ORM\Table(name="shipment_log")
 * @ORM\Entity
 */
class ShipmentLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idBodShipment
     *
     * @ORM\Column(name="id_bod_shipment", type="integer", nullable=true)
     */
    private $idBodShipment;

    /**
     * @var string $fileName
     *
     * @ORM\Column(name="file_name", type="text", nullable=true)
     */
    private $fileName;

    /**
     * @var \DateTime $logTime
     *
     * @ORM\Column(name="log_time", type="datetime", nullable=true)
     */
    private $logTime;

    /**
     * @var string $logText
     *
     * @ORM\Column(name="log_text", type="text", nullable=true)
     */
    private $logText;

    /**
     * @var integer $idReportQueue
     *
     * @ORM\Column(name="id_report_queue", type="integer", nullable=true)
     */
    private $idReportQueue;

    /**
     * @var string $reportQueueDescription
     *
     * @ORM\Column(name="report_queue_description", type="text", nullable=true)
     */
    private $reportQueueDescription;

    /**
     * @var string $reportQueueInput
     *
     * @ORM\Column(name="report_queue_input", type="text", nullable=true)
     */
    private $reportQueueInput;

    /**
     * @var integer $ftpUploadStatus
     *
     * @ORM\Column(name="ftp_upload_status", type="integer", nullable=true)
     */
    private $ftpUploadStatus;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return ShipmentLog
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idBodShipment
     *
     * @param integer $idBodShipment
     * @return ShipmentLog
     */
    public function setIdBodShipment($idBodShipment)
    {
        $this->idBodShipment = $idBodShipment;
    
        return $this;
    }

    /**
     * Get idBodShipment
     *
     * @return integer 
     */
    public function getIdBodShipment()
    {
        return $this->idBodShipment;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return ShipmentLog
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    
        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set logTime
     *
     * @param \DateTime $logTime
     * @return ShipmentLog
     */
    public function setLogTime($logTime)
    {
        $this->logTime = $logTime;
    
        return $this;
    }

    /**
     * Get logTime
     *
     * @return \DateTime 
     */
    public function getLogTime()
    {
        return $this->logTime;
    }

    /**
     * Set logText
     *
     * @param string $logText
     * @return ShipmentLog
     */
    public function setLogText($logText)
    {
        $this->logText = $logText;
    
        return $this;
    }

    /**
     * Get logText
     *
     * @return string 
     */
    public function getLogText()
    {
        return $this->logText;
    }

    /**
     * Set idReportQueue
     *
     * @param integer $idReportQueue
     * @return ShipmentLog
     */
    public function setIdReportQueue($idReportQueue)
    {
        $this->idReportQueue = $idReportQueue;
    
        return $this;
    }

    /**
     * Get idReportQueue
     *
     * @return integer 
     */
    public function getIdReportQueue()
    {
        return $this->idReportQueue;
    }

    /**
     * Set reportQueueDescription
     *
     * @param string $reportQueueDescription
     * @return ShipmentLog
     */
    public function setReportQueueDescription($reportQueueDescription)
    {
        $this->reportQueueDescription = $reportQueueDescription;
    
        return $this;
    }

    /**
     * Get reportQueueDescription
     *
     * @return string 
     */
    public function getReportQueueDescription()
    {
        return $this->reportQueueDescription;
    }

    /**
     * Set reportQueueInput
     *
     * @param string $reportQueueInput
     * @return ShipmentLog
     */
    public function setReportQueueInput($reportQueueInput)
    {
        $this->reportQueueInput = $reportQueueInput;
    
        return $this;
    }

    /**
     * Get reportQueueInput
     *
     * @return string 
     */
    public function getReportQueueInput()
    {
        return $this->reportQueueInput;
    }

    /**
     * Set ftpUploadStatus
     *
     * @param integer $ftpUploadStatus
     * @return ShipmentLog
     */
    public function setFtpUploadStatus($ftpUploadStatus)
    {
        $this->ftpUploadStatus = $ftpUploadStatus;
    
        return $this;
    }

    /**
     * Get ftpUploadStatus
     *
     * @return integer 
     */
    public function getFtpUploadStatus()
    {
        return $this->ftpUploadStatus;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return ShipmentLog
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
}