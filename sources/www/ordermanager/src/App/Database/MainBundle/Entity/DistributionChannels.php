<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannels
 *
 * @ORM\Table(name="distribution_channels")
 * @ORM\Entity
 */
class DistributionChannels
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="Domain", type="text", nullable=true)
     */
    private $domain;

    /**
     * @var string $distributionchannel
     *
     * @ORM\Column(name="DistributionChannel", type="string", length=255, nullable=true)
     */
    private $distributionchannel;

    /**
     * @var string $uidnumber
     *
     * @ORM\Column(name="UidNumber", type="string", length=18, nullable=true)
     */
    private $uidnumber;

    /**
     * @var integer $typeid
     *
     * @ORM\Column(name="TypeId", type="integer", nullable=true)
     */
    private $typeid;

    /**
     * @var string $type
     *
     * @ORM\Column(name="Type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer $pricecategoryid
     *
     * @ORM\Column(name="PriceCategoryId", type="integer", nullable=true)
     */
    private $pricecategoryid;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var boolean $male
     *
     * @ORM\Column(name="Male", type="boolean", nullable=true)
     */
    private $male;

    /**
     * @var boolean $female
     *
     * @ORM\Column(name="Female", type="boolean", nullable=true)
     */
    private $female;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="Surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string $institution
     *
     * @ORM\Column(name="Institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $countryid
     *
     * @ORM\Column(name="CountryId", type="integer", nullable=true)
     */
    private $countryid;

    /**
     * @var string $contactemail
     *
     * @ORM\Column(name="ContactEmail", type="string", length=255, nullable=true)
     */
    private $contactemail;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $statusdeliveryemail
     *
     * @ORM\Column(name="StatusDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $statusdeliveryemail;

    /**
     * @var string $reportdeliveryemail
     *
     * @ORM\Column(name="ReportDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $reportdeliveryemail;

    /**
     * @var string $invoicedeliveryemail
     *
     * @ORM\Column(name="InvoiceDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $invoicedeliveryemail;

    /**
     * @var integer $invoicegoestoid
     *
     * @ORM\Column(name="InvoiceGoesToId", type="integer", nullable=true)
     */
    private $invoicegoestoid;

    /**
     * @var integer $invoicegoestopartnerid
     *
     * @ORM\Column(name="InvoiceGoesToPartnerId", type="integer", nullable=true)
     */
    private $invoicegoestopartnerid;

    /**
     * @var integer $reportgoestoid
     *
     * @ORM\Column(name="ReportGoesToId", type="integer", nullable=true)
     */
    private $reportgoestoid;

    /**
     * @var integer $reportdeliveryid
     *
     * @ORM\Column(name="ReportDeliveryId", type="integer", nullable=true)
     */
    private $reportdeliveryid;

    /**
     * @var string $ftpservername
     *
     * @ORM\Column(name="FtpServerName", type="string", length=255, nullable=true)
     */
    private $ftpservername;

    /**
     * @var string $ftpusername
     *
     * @ORM\Column(name="FtpUserName", type="string", length=255, nullable=true)
     */
    private $ftpusername;

    /**
     * @var string $ftppassword
     *
     * @ORM\Column(name="FtpPassword", type="string", length=36, nullable=true)
     */
    private $ftppassword;

    /**
     * @var string $accountname
     *
     * @ORM\Column(name="AccountName", type="string", length=36, nullable=true)
     */
    private $accountname;

    /**
     * @var string $accountnumber
     *
     * @ORM\Column(name="AccountNumber", type="string", length=50, nullable=true)
     */
    private $accountnumber;

    /**
     * @var string $bankcode
     *
     * @ORM\Column(name="BankCode", type="string", length=50, nullable=true)
     */
    private $bankcode;

    /**
     * @var string $bic
     *
     * @ORM\Column(name="BIC", type="string", length=50, nullable=true)
     */
    private $bic;

    /**
     * @var string $iban
     *
     * @ORM\Column(name="IBAN", type="string", length=36, nullable=true)
     */
    private $iban;

    /**
     * @var string $othernotes
     *
     * @ORM\Column(name="OtherNotes", type="text", nullable=true)
     */
    private $othernotes;

    /**
     * @var integer $acquisiteurid1
     *
     * @ORM\Column(name="AcquisiteurId1", type="integer", nullable=true)
     */
    private $acquisiteurid1;

    /**
     * @var float $commission1
     *
     * @ORM\Column(name="Commission1", type="float", nullable=true)
     */
    private $commission1;

    /**
     * @var integer $acquisiteurid2
     *
     * @ORM\Column(name="AcquisiteurId2", type="integer", nullable=true)
     */
    private $acquisiteurid2;

    /**
     * @var float $commission2
     *
     * @ORM\Column(name="Commission2", type="float", nullable=true)
     */
    private $commission2;

    /**
     * @var integer $acquisiteurid3
     *
     * @ORM\Column(name="AcquisiteurId3", type="integer", nullable=true)
     */
    private $acquisiteurid3;

    /**
     * @var float $commission3
     *
     * @ORM\Column(name="Commission3", type="float", nullable=true)
     */
    private $commission3;

    /**
     * @var integer $webuserid
     *
     * @ORM\Column(name="WebUserId", type="integer", nullable=true)
     */
    private $webuserid;

    /**
     * @var string $webusers
     *
     * @ORM\Column(name="WebUsers", type="string", length=30, nullable=true)
     */
    private $webusers;

    /**
     * @var integer $partnerid
     *
     * @ORM\Column(name="PartnerId", type="integer", nullable=true)
     */
    private $partnerid;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var string $rebrandingnick
     *
     * @ORM\Column(name="RebrandingNick", type="string", length=255, nullable=true)
     */
    private $rebrandingnick;

    /**
     * @var string $headerfilename
     *
     * @ORM\Column(name="HeaderFileName", type="string", length=255, nullable=true)
     */
    private $headerfilename;

    /**
     * @var string $footerfilename
     *
     * @ORM\Column(name="FooterFileName", type="string", length=255, nullable=true)
     */
    private $footerfilename;

    /**
     * @var string $titlelogofilename
     *
     * @ORM\Column(name="TitleLogoFileName", type="string", length=255, nullable=true)
     */
    private $titlelogofilename;

    /**
     * @var string $boxlogofilename
     *
     * @ORM\Column(name="BoxLogoFileName", type="string", length=255, nullable=true)
     */
    private $boxlogofilename;

    /**
     * @var string $companyinfo
     *
     * @ORM\Column(name="CompanyInfo", type="text", nullable=true)
     */
    private $companyinfo;

    /**
     * @var string $laboratoryinfo
     *
     * @ORM\Column(name="LaboratoryInfo", type="text", nullable=true)
     */
    private $laboratoryinfo;

    /**
     * @var string $contactinfo
     *
     * @ORM\Column(name="ContactInfo", type="text", nullable=true)
     */
    private $contactinfo;

    /**
     * @var string $contactus
     *
     * @ORM\Column(name="ContactUs", type="text", nullable=true)
     */
    private $contactus;

    /**
     * @var string $letterextratext
     *
     * @ORM\Column(name="LetterExtraText", type="text", nullable=true)
     */
    private $letterextratext;

    /**
     * @var string $clinicianinfotext
     *
     * @ORM\Column(name="ClinicianInfoText", type="text", nullable=true)
     */
    private $clinicianinfotext;

    /**
     * @var string $text7
     *
     * @ORM\Column(name="Text7", type="text", nullable=true)
     */
    private $text7;

    /**
     * @var string $text8
     *
     * @ORM\Column(name="Text8", type="text", nullable=true)
     */
    private $text8;

    /**
     * @var string $text9
     *
     * @ORM\Column(name="Text9", type="text", nullable=true)
     */
    private $text9;

    /**
     * @var string $text10
     *
     * @ORM\Column(name="Text10", type="text", nullable=true)
     */
    private $text10;

    /**
     * @var string $text11
     *
     * @ORM\Column(name="Text11", type="text", nullable=true)
     */
    private $text11;

    /**
     * @var string $doctorsreporting
     *
     * @ORM\Column(name="DoctorsReporting", type="string", length=255, nullable=true)
     */
    private $doctorsreporting;

    /**
     * @var string $automaticreporting
     *
     * @ORM\Column(name="AutomaticReporting", type="string", length=255, nullable=true)
     */
    private $automaticreporting;

    /**
     * @var string $noreporting
     *
     * @ORM\Column(name="NoReporting", type="string", length=255, nullable=true)
     */
    private $noreporting;

    /**
     * @var string $invoicingandpaymentinfo
     *
     * @ORM\Column(name="InvoicingAndPaymentInfo", type="text", nullable=true)
     */
    private $invoicingandpaymentinfo;

    /**
     * @var integer $cardtypeid
     *
     * @ORM\Column(name="CardTypeId", type="integer", nullable=true)
     */
    private $cardtypeid;

    /**
     * @var string $cardnumber
     *
     * @ORM\Column(name="CardNumber", type="string", length=255, nullable=true)
     */
    private $cardnumber;

    /**
     * @var \DateTime $cardexpirydate
     *
     * @ORM\Column(name="CardExpiryDate", type="date", nullable=true)
     */
    private $cardexpirydate;

    /**
     * @var string $cardcvccode
     *
     * @ORM\Column(name="CardCvcCode", type="string", length=255, nullable=true)
     */
    private $cardcvccode;

    /**
     * @var string $cardfirstname
     *
     * @ORM\Column(name="CardFirstName", type="string", length=255, nullable=true)
     */
    private $cardfirstname;

    /**
     * @var string $cardsurname
     *
     * @ORM\Column(name="CardSurName", type="string", length=255, nullable=true)
     */
    private $cardsurname;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var boolean $invoiceAddressIsUsed
     *
     * @ORM\Column(name="invoice_address_is_used", type="boolean", nullable=true)
     */
    private $invoiceAddressIsUsed;

    /**
     * @var string $invoiceAddressStreet
     *
     * @ORM\Column(name="invoice_address_street", type="string", length=255, nullable=true)
     */
    private $invoiceAddressStreet;

    /**
     * @var string $invoiceAddressPostCode
     *
     * @ORM\Column(name="invoice_address_post_code", type="string", length=255, nullable=true)
     */
    private $invoiceAddressPostCode;

    /**
     * @var string $invoiceAddressCity
     *
     * @ORM\Column(name="invoice_address_city", type="string", length=255, nullable=true)
     */
    private $invoiceAddressCity;

    /**
     * @var integer $invoiceAddressIdCountry
     *
     * @ORM\Column(name="invoice_address_id_country", type="integer", nullable=true)
     */
    private $invoiceAddressIdCountry;

    /**
     * @var string $invoiceAddressTelephone
     *
     * @ORM\Column(name="invoice_address_telephone", type="string", length=255, nullable=true)
     */
    private $invoiceAddressTelephone;

    /**
     * @var string $invoiceAddressFax
     *
     * @ORM\Column(name="invoice_address_fax", type="string", length=255, nullable=true)
     */
    private $invoiceAddressFax;

    /**
     * @var string $reportColor
     *
     * @ORM\Column(name="report_color", type="string", length=255, nullable=true)
     */
    private $reportColor;

    /**
     * @var string $reportLogo
     *
     * @ORM\Column(name="report_logo", type="string", length=255, nullable=true)
     */
    private $reportLogo;

    /**
     * @var integer $isRecheckGeocoding
     *
     * @ORM\Column(name="is_recheck_geocoding", type="integer", nullable=true)
     */
    private $isRecheckGeocoding;

    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var string $geocodingStatusCode
     *
     * @ORM\Column(name="geocoding_status_code", type="string", length=255, nullable=true)
     */
    private $geocodingStatusCode;

    /**
     * @var float $accountBalance
     *
     * @ORM\Column(name="account_balance", type="decimal", nullable=true)
     */
    private $accountBalance;

    /**
     * @var string $invoiceClientName
     *
     * @ORM\Column(name="invoice_client_name", type="string", length=255, nullable=true)
     */
    private $invoiceClientName;

    /**
     * @var string $invoiceCompanyName
     *
     * @ORM\Column(name="invoice_company_name", type="string", length=255, nullable=true)
     */
    private $invoiceCompanyName;

    /**
     * @var string $contactPersonName
     *
     * @ORM\Column(name="contact_person_name", type="string", length=255, nullable=true)
     */
    private $contactPersonName;

    /**
     * @var string $contactPersonEmail
     *
     * @ORM\Column(name="contact_person_email", type="string", length=255, nullable=true)
     */
    private $contactPersonEmail;

    /**
     * @var string $contactPersonTelephoneNumber
     *
     * @ORM\Column(name="contact_person_telephone_number", type="string", length=255, nullable=true)
     */
    private $contactPersonTelephoneNumber;

    /**
     * @var boolean $isStopShipping
     *
     * @ORM\Column(name="is_stop_shipping", type="boolean", nullable=true)
     */
    private $isStopShipping;

    /**
     * @var integer $idPreferredPayment
     *
     * @ORM\Column(name="id_preferred_payment", type="integer", nullable=true)
     */
    private $idPreferredPayment;

    /**
     * @var integer $numberFeedback
     *
     * @ORM\Column(name="number_feedback", type="integer", nullable=true)
     */
    private $numberFeedback;

    /**
     * @var integer $idDncReportType
     *
     * @ORM\Column(name="id_dnc_report_type", type="integer", nullable=true)
     */
    private $idDncReportType;

    /**
     * @var integer $idReportNameType
     *
     * @ORM\Column(name="id_report_name_type", type="integer", nullable=true)
     */
    private $idReportNameType;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var integer $idMarginGoesTo
     *
     * @ORM\Column(name="id_margin_goes_to", type="integer", nullable=true)
     */
    private $idMarginGoesTo;

    /**
     * @var integer $idNutrimeGoesTo
     *
     * @ORM\Column(name="id_nutrime_goes_to", type="integer", nullable=true)
     */
    private $idNutrimeGoesTo;

    /**
     * @var boolean $isFixedReportGoesTo
     *
     * @ORM\Column(name="is_fixed_report_goes_to", type="boolean", nullable=true)
     */
    private $isFixedReportGoesTo;

    /**
     * @var boolean $isInvoiceWithTax
     *
     * @ORM\Column(name="is_invoice_with_tax", type="boolean", nullable=true)
     */
    private $isInvoiceWithTax;

    /**
     * @var boolean $isPaymentWithTax
     *
     * @ORM\Column(name="is_payment_with_tax", type="boolean", nullable=true)
     */
    private $isPaymentWithTax;

    /**
     * @var integer $idCompleteStatus
     *
     * @ORM\Column(name="id_complete_status", type="integer", nullable=true)
     */
    private $idCompleteStatus;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var integer $idMarginPaymentMode
     *
     * @ORM\Column(name="id_margin_payment_mode", type="integer", nullable=true)
     */
    private $idMarginPaymentMode;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur1
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur1", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur1;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur2
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur2", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur2;

    /**
     * @var boolean $isTimelyDeclinedAcquisiteur3
     *
     * @ORM\Column(name="is_timely_declined_acquisiteur3", type="boolean", nullable=true)
     */
    private $isTimelyDeclinedAcquisiteur3;

    /**
     * @var integer $paymentNoTaxText
     *
     * @ORM\Column(name="payment_no_tax_text", type="integer", nullable=true)
     */
    private $paymentNoTaxText;

    /**
     * @var float $yearlyDecayPercentage1
     *
     * @ORM\Column(name="yearly_decay_percentage1", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage1;

    /**
     * @var float $yearlyDecayPercentage2
     *
     * @ORM\Column(name="yearly_decay_percentage2", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage2;

    /**
     * @var float $yearlyDecayPercentage3
     *
     * @ORM\Column(name="yearly_decay_percentage3", type="decimal", nullable=true)
     */
    private $yearlyDecayPercentage3;

    /**
     * @var string $supplementInfo
     *
     * @ORM\Column(name="supplement_info", type="text", nullable=true)
     */
    private $supplementInfo;

    /**
     * @var string $webLoginUsername
     *
     * @ORM\Column(name="web_login_username", type="string", length=255, nullable=true)
     */
    private $webLoginUsername;

    /**
     * @var string $webLoginPassword
     *
     * @ORM\Column(name="web_login_password", type="string", length=255, nullable=true)
     */
    private $webLoginPassword;

    /**
     * @var integer $webLoginStatus
     *
     * @ORM\Column(name="web_login_status", type="integer", nullable=true)
     */
    private $webLoginStatus;

    /**
     * @var boolean $isCustomerBeContacted
     *
     * @ORM\Column(name="is_customer_be_contacted", type="boolean", nullable=true)
     */
    private $isCustomerBeContacted;

    /**
     * @var boolean $isNutrimeOffered
     *
     * @ORM\Column(name="is_nutrime_offered", type="boolean", nullable=true)
     */
    private $isNutrimeOffered;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var boolean $isDestroySample
     *
     * @ORM\Column(name="is_destroy_sample", type="boolean", nullable=true)
     */
    private $isDestroySample;

    /**
     * @var boolean $sampleCanBeUsedForScience
     *
     * @ORM\Column(name="sample_can_be_used_for_science", type="boolean", nullable=true)
     */
    private $sampleCanBeUsedForScience;

    /**
     * @var boolean $isFutureResearchParticipant
     *
     * @ORM\Column(name="is_future_research_participant", type="boolean", nullable=true)
     */
    private $isFutureResearchParticipant;

    /**
     * @var integer $idVisibleGroupProduct
     *
     * @ORM\Column(name="id_visible_group_product", type="integer", nullable=true)
     */
    private $idVisibleGroupProduct;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var integer $idVisibleGroupReport
     *
     * @ORM\Column(name="id_visible_group_report", type="integer", nullable=true)
     */
    private $idVisibleGroupReport;

    /**
     * @var integer $invoiceDeliveryNote
     *
     * @ORM\Column(name="invoice_delivery_note", type="integer", nullable=true)
     */
    private $invoiceDeliveryNote;

    /**
     * @var integer $webIdLanguage
     *
     * @ORM\Column(name="web_id_language", type="integer", nullable=true)
     */
    private $webIdLanguage;

    /**
     * @var boolean $isShowBodCoverPage
     *
     * @ORM\Column(name="is_show_bod_cover_page", type="boolean", nullable=true)
     */
    private $isShowBodCoverPage;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var boolean $canCreateLoginAndSendStatusMessageToUser
     *
     * @ORM\Column(name="can_create_login_and_send_status_message_to_user", type="boolean", nullable=true)
     */
    private $canCreateLoginAndSendStatusMessageToUser;

    /**
     * @var boolean $canViewShop
     *
     * @ORM\Column(name="can_view_shop", type="boolean", nullable=true)
     */
    private $canViewShop;

    /**
     * @var boolean $isGenerateFoodTableExcelVersion
     *
     * @ORM\Column(name="is_generate_food_table_excel_version", type="boolean", nullable=true)
     */
    private $isGenerateFoodTableExcelVersion;

    /**
     * @var \DateTime $lastOrderDate
     *
     * @ORM\Column(name="last_order_date", type="date", nullable=true)
     */
    private $lastOrderDate;

    /**
     * @var integer $destroyInNumDays
     *
     * @ORM\Column(name="destroy_in_num_days", type="integer", nullable=true)
     */
    private $destroyInNumDays;

    /**
     * @var integer $idRecipeBookDelivery
     *
     * @ORM\Column(name="id_recipe_book_delivery", type="integer", nullable=true)
     */
    private $idRecipeBookDelivery;

    /**
     * @var integer $idRecipeBookGoesTo
     *
     * @ORM\Column(name="id_recipe_book_goes_to", type="integer", nullable=true)
     */
    private $idRecipeBookGoesTo;

    /**
     * @var boolean $canCustomersDownloadReports
     *
     * @ORM\Column(name="can_customers_download_reports", type="boolean", nullable=true)
     */
    private $canCustomersDownloadReports;

    /**
     * @var boolean $isGenerateXmlVersion
     *
     * @ORM\Column(name="is_generate_xml_version", type="boolean", nullable=true)
     */
    private $isGenerateXmlVersion;

    /**
     * @var boolean $isDeliveryViaSftp
     *
     * @ORM\Column(name="is_delivery_via_sftp", type="boolean", nullable=true)
     */
    private $isDeliveryViaSftp;

    /**
     * @var string $sftpHost
     *
     * @ORM\Column(name="sftp_host", type="string", length=255, nullable=true)
     */
    private $sftpHost;

    /**
     * @var string $sftpUsername
     *
     * @ORM\Column(name="sftp_username", type="string", length=255, nullable=true)
     */
    private $sftpUsername;

    /**
     * @var string $sftpPassword
     *
     * @ORM\Column(name="sftp_password", type="string", length=255, nullable=true)
     */
    private $sftpPassword;

    /**
     * @var boolean $isCanViewYourOrder
     *
     * @ORM\Column(name="is_can_view_your_order", type="boolean", nullable=true)
     */
    private $isCanViewYourOrder;

    /**
     * @var boolean $isCanViewCustomerProtection
     *
     * @ORM\Column(name="is_can_view_customer_protection", type="boolean", nullable=true)
     */
    private $isCanViewCustomerProtection;

    /**
     * @var boolean $isCanViewRegisterNewCustomer
     *
     * @ORM\Column(name="is_can_view_register_new_customer", type="boolean", nullable=true)
     */
    private $isCanViewRegisterNewCustomer;

    /**
     * @var boolean $isCanViewPendingOrder
     *
     * @ORM\Column(name="is_can_view_pending_order", type="boolean", nullable=true)
     */
    private $isCanViewPendingOrder;

    /**
     * @var boolean $isCanViewSetting
     *
     * @ORM\Column(name="is_can_view_setting", type="boolean", nullable=true)
     */
    private $isCanViewSetting;

    /**
     * @var boolean $isPrintedReportSentToPartner
     *
     * @ORM\Column(name="is_printed_report_sent_to_partner", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToPartner;

    /**
     * @var boolean $isPrintedReportSentToDc
     *
     * @ORM\Column(name="is_printed_report_sent_to_dc", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToDc;

    /**
     * @var boolean $isPrintedReportSentToCustomer
     *
     * @ORM\Column(name="is_printed_report_sent_to_customer", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToCustomer;

    /**
     * @var boolean $isDigitalReportGoesToCustomer
     *
     * @ORM\Column(name="is_digital_report_goes_to_customer", type="boolean", nullable=true)
     */
    private $isDigitalReportGoesToCustomer;

    /**
     * @var integer $idRecallGoesTo
     *
     * @ORM\Column(name="id_recall_goes_to", type="integer", nullable=true)
     */
    private $idRecallGoesTo;

    /**
     * @var string $sftpFolder
     *
     * @ORM\Column(name="sftp_folder", type="string", length=255, nullable=true)
     */
    private $sftpFolder;

    /**
     * @var boolean $isReportDeliveryDownloadAccess
     *
     * @ORM\Column(name="is_report_delivery_download_access", type="boolean", nullable=true)
     */
    private $isReportDeliveryDownloadAccess;

    /**
     * @var boolean $isReportDeliveryFtpServer
     *
     * @ORM\Column(name="is_report_delivery_ftp_server", type="boolean", nullable=true)
     */
    private $isReportDeliveryFtpServer;

    /**
     * @var boolean $isReportDeliveryPrintedBooklet
     *
     * @ORM\Column(name="is_report_delivery_printed_booklet", type="boolean", nullable=true)
     */
    private $isReportDeliveryPrintedBooklet;

    /**
     * @var boolean $isReportDeliveryDontChargeBooklet
     *
     * @ORM\Column(name="is_report_delivery_dont_charge_booklet", type="boolean", nullable=true)
     */
    private $isReportDeliveryDontChargeBooklet;

    /**
     * @var boolean $onlyShowDistributionChannelAddress
     *
     * @ORM\Column(name="only_show_distribution_channel_address", type="boolean", nullable=true)
     */
    private $onlyShowDistributionChannelAddress;

    /**
     * @var string $idCode
     *
     * @ORM\Column(name="id_code", type="string", length=5, nullable=true)
     */
    private $idCode;

    /**
     * @var integer $accountingCode
     *
     * @ORM\Column(name="accounting_code", type="integer", nullable=true)
     */
    private $accountingCode;

    /**
     * @var boolean $isWebLoginOptOut
     *
     * @ORM\Column(name="is_web_login_opt_out", type="boolean", nullable=true)
     */
    private $isWebLoginOptOut;

    /**
     * @var integer $webLoginGoesTo
     *
     * @ORM\Column(name="web_login_goes_to", type="integer", nullable=true)
     */
    private $webLoginGoesTo;

    /**
     * @var integer $invoiceNoTaxText
     *
     * @ORM\Column(name="invoice_no_tax_text", type="integer", nullable=true)
     */
    private $invoiceNoTaxText;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var boolean $expressShipment
     *
     * @ORM\Column(name="express_shipment", type="boolean", nullable=true)
     */
    private $expressShipment;

    /**
     * @var string $commissionPaymentWithTax
     *
     * @ORM\Column(name="commission_payment_with_tax", type="string", length=45, nullable=true)
     */
    private $commissionPaymentWithTax;

    /**
     * @var string $customBodAccountNumber
     *
     * @ORM\Column(name="custom_bod_account_number", type="string", length=45, nullable=true)
     */
    private $customBodAccountNumber;

    /**
     * @var string $customBodBarcodeNumber
     *
     * @ORM\Column(name="custom_bod_barcode_number", type="string", length=45, nullable=true)
     */
    private $customBodBarcodeNumber;

    /**
     * @var boolean $canDownloadViaSftp
     *
     * @ORM\Column(name="can_download_via_sftp", type="boolean", nullable=true)
     */
    private $canDownloadViaSftp;

    /**
     * @var string $sftpDownloadHost
     *
     * @ORM\Column(name="sftp_download_host", type="string", length=255, nullable=true)
     */
    private $sftpDownloadHost;

    /**
     * @var string $sftpDownloadUsername
     *
     * @ORM\Column(name="sftp_download_username", type="string", length=255, nullable=true)
     */
    private $sftpDownloadUsername;

    /**
     * @var string $sftpDownloadPassword
     *
     * @ORM\Column(name="sftp_download_password", type="string", length=255, nullable=true)
     */
    private $sftpDownloadPassword;

    /**
     * @var string $sftpDownloadFolder
     *
     * @ORM\Column(name="sftp_download_folder", type="string", length=255, nullable=true)
     */
    private $sftpDownloadFolder;

    /**
     * @var boolean $isDelayDownloadReport
     *
     * @ORM\Column(name="is_delay_download_report", type="boolean", nullable=true)
     */
    private $isDelayDownloadReport;

    /**
     * @var integer $numberDateDelayDownloadReport
     *
     * @ORM\Column(name="number_date_delay_download_report", type="integer", nullable=true)
     */
    private $numberDateDelayDownloadReport;

    /**
     * @var boolean $isNotAccessReport
     *
     * @ORM\Column(name="is_not_access_report", type="boolean", nullable=true)
     */
    private $isNotAccessReport;

    /**
     * @var boolean $isNotSendEmail
     *
     * @ORM\Column(name="is_not_send_email", type="boolean", nullable=true)
     */
    private $isNotSendEmail;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return DistributionChannels
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set distributionchannel
     *
     * @param string $distributionchannel
     * @return DistributionChannels
     */
    public function setDistributionchannel($distributionchannel)
    {
        $this->distributionchannel = $distributionchannel;
    
        return $this;
    }

    /**
     * Get distributionchannel
     *
     * @return string 
     */
    public function getDistributionchannel()
    {
        return $this->distributionchannel;
    }

    /**
     * Set uidnumber
     *
     * @param string $uidnumber
     * @return DistributionChannels
     */
    public function setUidnumber($uidnumber)
    {
        $this->uidnumber = $uidnumber;
    
        return $this;
    }

    /**
     * Get uidnumber
     *
     * @return string 
     */
    public function getUidnumber()
    {
        return $this->uidnumber;
    }

    /**
     * Set typeid
     *
     * @param integer $typeid
     * @return DistributionChannels
     */
    public function setTypeid($typeid)
    {
        $this->typeid = $typeid;
    
        return $this;
    }

    /**
     * Get typeid
     *
     * @return integer 
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return DistributionChannels
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pricecategoryid
     *
     * @param integer $pricecategoryid
     * @return DistributionChannels
     */
    public function setPricecategoryid($pricecategoryid)
    {
        $this->pricecategoryid = $pricecategoryid;
    
        return $this;
    }

    /**
     * Get pricecategoryid
     *
     * @return integer 
     */
    public function getPricecategoryid()
    {
        return $this->pricecategoryid;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return DistributionChannels
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set male
     *
     * @param boolean $male
     * @return DistributionChannels
     */
    public function setMale($male)
    {
        $this->male = $male;
    
        return $this;
    }

    /**
     * Get male
     *
     * @return boolean 
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * Set female
     *
     * @param boolean $female
     * @return DistributionChannels
     */
    public function setFemale($female)
    {
        $this->female = $female;
    
        return $this;
    }

    /**
     * Get female
     *
     * @return boolean 
     */
    public function getFemale()
    {
        return $this->female;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return DistributionChannels
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return DistributionChannels
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return DistributionChannels
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return DistributionChannels
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    
        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return DistributionChannels
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return DistributionChannels
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return DistributionChannels
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return DistributionChannels
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;
    
        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set contactemail
     *
     * @param string $contactemail
     * @return DistributionChannels
     */
    public function setContactemail($contactemail)
    {
        $this->contactemail = $contactemail;
    
        return $this;
    }

    /**
     * Get contactemail
     *
     * @return string 
     */
    public function getContactemail()
    {
        return $this->contactemail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return DistributionChannels
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return DistributionChannels
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set statusdeliveryemail
     *
     * @param string $statusdeliveryemail
     * @return DistributionChannels
     */
    public function setStatusdeliveryemail($statusdeliveryemail)
    {
        $this->statusdeliveryemail = $statusdeliveryemail;
    
        return $this;
    }

    /**
     * Get statusdeliveryemail
     *
     * @return string 
     */
    public function getStatusdeliveryemail()
    {
        return $this->statusdeliveryemail;
    }

    /**
     * Set reportdeliveryemail
     *
     * @param string $reportdeliveryemail
     * @return DistributionChannels
     */
    public function setReportdeliveryemail($reportdeliveryemail)
    {
        $this->reportdeliveryemail = $reportdeliveryemail;
    
        return $this;
    }

    /**
     * Get reportdeliveryemail
     *
     * @return string 
     */
    public function getReportdeliveryemail()
    {
        return $this->reportdeliveryemail;
    }

    /**
     * Set invoicedeliveryemail
     *
     * @param string $invoicedeliveryemail
     * @return DistributionChannels
     */
    public function setInvoicedeliveryemail($invoicedeliveryemail)
    {
        $this->invoicedeliveryemail = $invoicedeliveryemail;
    
        return $this;
    }

    /**
     * Get invoicedeliveryemail
     *
     * @return string 
     */
    public function getInvoicedeliveryemail()
    {
        return $this->invoicedeliveryemail;
    }

    /**
     * Set invoicegoestoid
     *
     * @param integer $invoicegoestoid
     * @return DistributionChannels
     */
    public function setInvoicegoestoid($invoicegoestoid)
    {
        $this->invoicegoestoid = $invoicegoestoid;
    
        return $this;
    }

    /**
     * Get invoicegoestoid
     *
     * @return integer 
     */
    public function getInvoicegoestoid()
    {
        return $this->invoicegoestoid;
    }

    /**
     * Set invoicegoestopartnerid
     *
     * @param integer $invoicegoestopartnerid
     * @return DistributionChannels
     */
    public function setInvoicegoestopartnerid($invoicegoestopartnerid)
    {
        $this->invoicegoestopartnerid = $invoicegoestopartnerid;
    
        return $this;
    }

    /**
     * Get invoicegoestopartnerid
     *
     * @return integer 
     */
    public function getInvoicegoestopartnerid()
    {
        return $this->invoicegoestopartnerid;
    }

    /**
     * Set reportgoestoid
     *
     * @param integer $reportgoestoid
     * @return DistributionChannels
     */
    public function setReportgoestoid($reportgoestoid)
    {
        $this->reportgoestoid = $reportgoestoid;
    
        return $this;
    }

    /**
     * Get reportgoestoid
     *
     * @return integer 
     */
    public function getReportgoestoid()
    {
        return $this->reportgoestoid;
    }

    /**
     * Set reportdeliveryid
     *
     * @param integer $reportdeliveryid
     * @return DistributionChannels
     */
    public function setReportdeliveryid($reportdeliveryid)
    {
        $this->reportdeliveryid = $reportdeliveryid;
    
        return $this;
    }

    /**
     * Get reportdeliveryid
     *
     * @return integer 
     */
    public function getReportdeliveryid()
    {
        return $this->reportdeliveryid;
    }

    /**
     * Set ftpservername
     *
     * @param string $ftpservername
     * @return DistributionChannels
     */
    public function setFtpservername($ftpservername)
    {
        $this->ftpservername = $ftpservername;
    
        return $this;
    }

    /**
     * Get ftpservername
     *
     * @return string 
     */
    public function getFtpservername()
    {
        return $this->ftpservername;
    }

    /**
     * Set ftpusername
     *
     * @param string $ftpusername
     * @return DistributionChannels
     */
    public function setFtpusername($ftpusername)
    {
        $this->ftpusername = $ftpusername;
    
        return $this;
    }

    /**
     * Get ftpusername
     *
     * @return string 
     */
    public function getFtpusername()
    {
        return $this->ftpusername;
    }

    /**
     * Set ftppassword
     *
     * @param string $ftppassword
     * @return DistributionChannels
     */
    public function setFtppassword($ftppassword)
    {
        $this->ftppassword = $ftppassword;
    
        return $this;
    }

    /**
     * Get ftppassword
     *
     * @return string 
     */
    public function getFtppassword()
    {
        return $this->ftppassword;
    }

    /**
     * Set accountname
     *
     * @param string $accountname
     * @return DistributionChannels
     */
    public function setAccountname($accountname)
    {
        $this->accountname = $accountname;
    
        return $this;
    }

    /**
     * Get accountname
     *
     * @return string 
     */
    public function getAccountname()
    {
        return $this->accountname;
    }

    /**
     * Set accountnumber
     *
     * @param string $accountnumber
     * @return DistributionChannels
     */
    public function setAccountnumber($accountnumber)
    {
        $this->accountnumber = $accountnumber;
    
        return $this;
    }

    /**
     * Get accountnumber
     *
     * @return string 
     */
    public function getAccountnumber()
    {
        return $this->accountnumber;
    }

    /**
     * Set bankcode
     *
     * @param string $bankcode
     * @return DistributionChannels
     */
    public function setBankcode($bankcode)
    {
        $this->bankcode = $bankcode;
    
        return $this;
    }

    /**
     * Get bankcode
     *
     * @return string 
     */
    public function getBankcode()
    {
        return $this->bankcode;
    }

    /**
     * Set bic
     *
     * @param string $bic
     * @return DistributionChannels
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    
        return $this;
    }

    /**
     * Get bic
     *
     * @return string 
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return DistributionChannels
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    
        return $this;
    }

    /**
     * Get iban
     *
     * @return string 
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set othernotes
     *
     * @param string $othernotes
     * @return DistributionChannels
     */
    public function setOthernotes($othernotes)
    {
        $this->othernotes = $othernotes;
    
        return $this;
    }

    /**
     * Get othernotes
     *
     * @return string 
     */
    public function getOthernotes()
    {
        return $this->othernotes;
    }

    /**
     * Set acquisiteurid1
     *
     * @param integer $acquisiteurid1
     * @return DistributionChannels
     */
    public function setAcquisiteurid1($acquisiteurid1)
    {
        $this->acquisiteurid1 = $acquisiteurid1;
    
        return $this;
    }

    /**
     * Get acquisiteurid1
     *
     * @return integer 
     */
    public function getAcquisiteurid1()
    {
        return $this->acquisiteurid1;
    }

    /**
     * Set commission1
     *
     * @param float $commission1
     * @return DistributionChannels
     */
    public function setCommission1($commission1)
    {
        $this->commission1 = $commission1;
    
        return $this;
    }

    /**
     * Get commission1
     *
     * @return float 
     */
    public function getCommission1()
    {
        return $this->commission1;
    }

    /**
     * Set acquisiteurid2
     *
     * @param integer $acquisiteurid2
     * @return DistributionChannels
     */
    public function setAcquisiteurid2($acquisiteurid2)
    {
        $this->acquisiteurid2 = $acquisiteurid2;
    
        return $this;
    }

    /**
     * Get acquisiteurid2
     *
     * @return integer 
     */
    public function getAcquisiteurid2()
    {
        return $this->acquisiteurid2;
    }

    /**
     * Set commission2
     *
     * @param float $commission2
     * @return DistributionChannels
     */
    public function setCommission2($commission2)
    {
        $this->commission2 = $commission2;
    
        return $this;
    }

    /**
     * Get commission2
     *
     * @return float 
     */
    public function getCommission2()
    {
        return $this->commission2;
    }

    /**
     * Set acquisiteurid3
     *
     * @param integer $acquisiteurid3
     * @return DistributionChannels
     */
    public function setAcquisiteurid3($acquisiteurid3)
    {
        $this->acquisiteurid3 = $acquisiteurid3;
    
        return $this;
    }

    /**
     * Get acquisiteurid3
     *
     * @return integer 
     */
    public function getAcquisiteurid3()
    {
        return $this->acquisiteurid3;
    }

    /**
     * Set commission3
     *
     * @param float $commission3
     * @return DistributionChannels
     */
    public function setCommission3($commission3)
    {
        $this->commission3 = $commission3;
    
        return $this;
    }

    /**
     * Get commission3
     *
     * @return float 
     */
    public function getCommission3()
    {
        return $this->commission3;
    }

    /**
     * Set webuserid
     *
     * @param integer $webuserid
     * @return DistributionChannels
     */
    public function setWebuserid($webuserid)
    {
        $this->webuserid = $webuserid;
    
        return $this;
    }

    /**
     * Get webuserid
     *
     * @return integer 
     */
    public function getWebuserid()
    {
        return $this->webuserid;
    }

    /**
     * Set webusers
     *
     * @param string $webusers
     * @return DistributionChannels
     */
    public function setWebusers($webusers)
    {
        $this->webusers = $webusers;
    
        return $this;
    }

    /**
     * Get webusers
     *
     * @return string 
     */
    public function getWebusers()
    {
        return $this->webusers;
    }

    /**
     * Set partnerid
     *
     * @param integer $partnerid
     * @return DistributionChannels
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;
    
        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer 
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return DistributionChannels
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set rebrandingnick
     *
     * @param string $rebrandingnick
     * @return DistributionChannels
     */
    public function setRebrandingnick($rebrandingnick)
    {
        $this->rebrandingnick = $rebrandingnick;
    
        return $this;
    }

    /**
     * Get rebrandingnick
     *
     * @return string 
     */
    public function getRebrandingnick()
    {
        return $this->rebrandingnick;
    }

    /**
     * Set headerfilename
     *
     * @param string $headerfilename
     * @return DistributionChannels
     */
    public function setHeaderfilename($headerfilename)
    {
        $this->headerfilename = $headerfilename;
    
        return $this;
    }

    /**
     * Get headerfilename
     *
     * @return string 
     */
    public function getHeaderfilename()
    {
        return $this->headerfilename;
    }

    /**
     * Set footerfilename
     *
     * @param string $footerfilename
     * @return DistributionChannels
     */
    public function setFooterfilename($footerfilename)
    {
        $this->footerfilename = $footerfilename;
    
        return $this;
    }

    /**
     * Get footerfilename
     *
     * @return string 
     */
    public function getFooterfilename()
    {
        return $this->footerfilename;
    }

    /**
     * Set titlelogofilename
     *
     * @param string $titlelogofilename
     * @return DistributionChannels
     */
    public function setTitlelogofilename($titlelogofilename)
    {
        $this->titlelogofilename = $titlelogofilename;
    
        return $this;
    }

    /**
     * Get titlelogofilename
     *
     * @return string 
     */
    public function getTitlelogofilename()
    {
        return $this->titlelogofilename;
    }

    /**
     * Set boxlogofilename
     *
     * @param string $boxlogofilename
     * @return DistributionChannels
     */
    public function setBoxlogofilename($boxlogofilename)
    {
        $this->boxlogofilename = $boxlogofilename;
    
        return $this;
    }

    /**
     * Get boxlogofilename
     *
     * @return string 
     */
    public function getBoxlogofilename()
    {
        return $this->boxlogofilename;
    }

    /**
     * Set companyinfo
     *
     * @param string $companyinfo
     * @return DistributionChannels
     */
    public function setCompanyinfo($companyinfo)
    {
        $this->companyinfo = $companyinfo;
    
        return $this;
    }

    /**
     * Get companyinfo
     *
     * @return string 
     */
    public function getCompanyinfo()
    {
        return $this->companyinfo;
    }

    /**
     * Set laboratoryinfo
     *
     * @param string $laboratoryinfo
     * @return DistributionChannels
     */
    public function setLaboratoryinfo($laboratoryinfo)
    {
        $this->laboratoryinfo = $laboratoryinfo;
    
        return $this;
    }

    /**
     * Get laboratoryinfo
     *
     * @return string 
     */
    public function getLaboratoryinfo()
    {
        return $this->laboratoryinfo;
    }

    /**
     * Set contactinfo
     *
     * @param string $contactinfo
     * @return DistributionChannels
     */
    public function setContactinfo($contactinfo)
    {
        $this->contactinfo = $contactinfo;
    
        return $this;
    }

    /**
     * Get contactinfo
     *
     * @return string 
     */
    public function getContactinfo()
    {
        return $this->contactinfo;
    }

    /**
     * Set contactus
     *
     * @param string $contactus
     * @return DistributionChannels
     */
    public function setContactus($contactus)
    {
        $this->contactus = $contactus;
    
        return $this;
    }

    /**
     * Get contactus
     *
     * @return string 
     */
    public function getContactus()
    {
        return $this->contactus;
    }

    /**
     * Set letterextratext
     *
     * @param string $letterextratext
     * @return DistributionChannels
     */
    public function setLetterextratext($letterextratext)
    {
        $this->letterextratext = $letterextratext;
    
        return $this;
    }

    /**
     * Get letterextratext
     *
     * @return string 
     */
    public function getLetterextratext()
    {
        return $this->letterextratext;
    }

    /**
     * Set clinicianinfotext
     *
     * @param string $clinicianinfotext
     * @return DistributionChannels
     */
    public function setClinicianinfotext($clinicianinfotext)
    {
        $this->clinicianinfotext = $clinicianinfotext;
    
        return $this;
    }

    /**
     * Get clinicianinfotext
     *
     * @return string 
     */
    public function getClinicianinfotext()
    {
        return $this->clinicianinfotext;
    }

    /**
     * Set text7
     *
     * @param string $text7
     * @return DistributionChannels
     */
    public function setText7($text7)
    {
        $this->text7 = $text7;
    
        return $this;
    }

    /**
     * Get text7
     *
     * @return string 
     */
    public function getText7()
    {
        return $this->text7;
    }

    /**
     * Set text8
     *
     * @param string $text8
     * @return DistributionChannels
     */
    public function setText8($text8)
    {
        $this->text8 = $text8;
    
        return $this;
    }

    /**
     * Get text8
     *
     * @return string 
     */
    public function getText8()
    {
        return $this->text8;
    }

    /**
     * Set text9
     *
     * @param string $text9
     * @return DistributionChannels
     */
    public function setText9($text9)
    {
        $this->text9 = $text9;
    
        return $this;
    }

    /**
     * Get text9
     *
     * @return string 
     */
    public function getText9()
    {
        return $this->text9;
    }

    /**
     * Set text10
     *
     * @param string $text10
     * @return DistributionChannels
     */
    public function setText10($text10)
    {
        $this->text10 = $text10;
    
        return $this;
    }

    /**
     * Get text10
     *
     * @return string 
     */
    public function getText10()
    {
        return $this->text10;
    }

    /**
     * Set text11
     *
     * @param string $text11
     * @return DistributionChannels
     */
    public function setText11($text11)
    {
        $this->text11 = $text11;
    
        return $this;
    }

    /**
     * Get text11
     *
     * @return string 
     */
    public function getText11()
    {
        return $this->text11;
    }

    /**
     * Set doctorsreporting
     *
     * @param string $doctorsreporting
     * @return DistributionChannels
     */
    public function setDoctorsreporting($doctorsreporting)
    {
        $this->doctorsreporting = $doctorsreporting;
    
        return $this;
    }

    /**
     * Get doctorsreporting
     *
     * @return string 
     */
    public function getDoctorsreporting()
    {
        return $this->doctorsreporting;
    }

    /**
     * Set automaticreporting
     *
     * @param string $automaticreporting
     * @return DistributionChannels
     */
    public function setAutomaticreporting($automaticreporting)
    {
        $this->automaticreporting = $automaticreporting;
    
        return $this;
    }

    /**
     * Get automaticreporting
     *
     * @return string 
     */
    public function getAutomaticreporting()
    {
        return $this->automaticreporting;
    }

    /**
     * Set noreporting
     *
     * @param string $noreporting
     * @return DistributionChannels
     */
    public function setNoreporting($noreporting)
    {
        $this->noreporting = $noreporting;
    
        return $this;
    }

    /**
     * Get noreporting
     *
     * @return string 
     */
    public function getNoreporting()
    {
        return $this->noreporting;
    }

    /**
     * Set invoicingandpaymentinfo
     *
     * @param string $invoicingandpaymentinfo
     * @return DistributionChannels
     */
    public function setInvoicingandpaymentinfo($invoicingandpaymentinfo)
    {
        $this->invoicingandpaymentinfo = $invoicingandpaymentinfo;
    
        return $this;
    }

    /**
     * Get invoicingandpaymentinfo
     *
     * @return string 
     */
    public function getInvoicingandpaymentinfo()
    {
        return $this->invoicingandpaymentinfo;
    }

    /**
     * Set cardtypeid
     *
     * @param integer $cardtypeid
     * @return DistributionChannels
     */
    public function setCardtypeid($cardtypeid)
    {
        $this->cardtypeid = $cardtypeid;
    
        return $this;
    }

    /**
     * Get cardtypeid
     *
     * @return integer 
     */
    public function getCardtypeid()
    {
        return $this->cardtypeid;
    }

    /**
     * Set cardnumber
     *
     * @param string $cardnumber
     * @return DistributionChannels
     */
    public function setCardnumber($cardnumber)
    {
        $this->cardnumber = $cardnumber;
    
        return $this;
    }

    /**
     * Get cardnumber
     *
     * @return string 
     */
    public function getCardnumber()
    {
        return $this->cardnumber;
    }

    /**
     * Set cardexpirydate
     *
     * @param \DateTime $cardexpirydate
     * @return DistributionChannels
     */
    public function setCardexpirydate($cardexpirydate)
    {
        $this->cardexpirydate = $cardexpirydate;
    
        return $this;
    }

    /**
     * Get cardexpirydate
     *
     * @return \DateTime 
     */
    public function getCardexpirydate()
    {
        return $this->cardexpirydate;
    }

    /**
     * Set cardcvccode
     *
     * @param string $cardcvccode
     * @return DistributionChannels
     */
    public function setCardcvccode($cardcvccode)
    {
        $this->cardcvccode = $cardcvccode;
    
        return $this;
    }

    /**
     * Get cardcvccode
     *
     * @return string 
     */
    public function getCardcvccode()
    {
        return $this->cardcvccode;
    }

    /**
     * Set cardfirstname
     *
     * @param string $cardfirstname
     * @return DistributionChannels
     */
    public function setCardfirstname($cardfirstname)
    {
        $this->cardfirstname = $cardfirstname;
    
        return $this;
    }

    /**
     * Get cardfirstname
     *
     * @return string 
     */
    public function getCardfirstname()
    {
        return $this->cardfirstname;
    }

    /**
     * Set cardsurname
     *
     * @param string $cardsurname
     * @return DistributionChannels
     */
    public function setCardsurname($cardsurname)
    {
        $this->cardsurname = $cardsurname;
    
        return $this;
    }

    /**
     * Get cardsurname
     *
     * @return string 
     */
    public function getCardsurname()
    {
        return $this->cardsurname;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return DistributionChannels
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }

    /**
     * Set invoiceAddressIsUsed
     *
     * @param boolean $invoiceAddressIsUsed
     * @return DistributionChannels
     */
    public function setInvoiceAddressIsUsed($invoiceAddressIsUsed)
    {
        $this->invoiceAddressIsUsed = $invoiceAddressIsUsed;
    
        return $this;
    }

    /**
     * Get invoiceAddressIsUsed
     *
     * @return boolean 
     */
    public function getInvoiceAddressIsUsed()
    {
        return $this->invoiceAddressIsUsed;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     * @return DistributionChannels
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;
    
        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string 
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressPostCode
     *
     * @param string $invoiceAddressPostCode
     * @return DistributionChannels
     */
    public function setInvoiceAddressPostCode($invoiceAddressPostCode)
    {
        $this->invoiceAddressPostCode = $invoiceAddressPostCode;
    
        return $this;
    }

    /**
     * Get invoiceAddressPostCode
     *
     * @return string 
     */
    public function getInvoiceAddressPostCode()
    {
        return $this->invoiceAddressPostCode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     * @return DistributionChannels
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;
    
        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string 
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressIdCountry
     *
     * @param integer $invoiceAddressIdCountry
     * @return DistributionChannels
     */
    public function setInvoiceAddressIdCountry($invoiceAddressIdCountry)
    {
        $this->invoiceAddressIdCountry = $invoiceAddressIdCountry;
    
        return $this;
    }

    /**
     * Get invoiceAddressIdCountry
     *
     * @return integer 
     */
    public function getInvoiceAddressIdCountry()
    {
        return $this->invoiceAddressIdCountry;
    }

    /**
     * Set invoiceAddressTelephone
     *
     * @param string $invoiceAddressTelephone
     * @return DistributionChannels
     */
    public function setInvoiceAddressTelephone($invoiceAddressTelephone)
    {
        $this->invoiceAddressTelephone = $invoiceAddressTelephone;
    
        return $this;
    }

    /**
     * Get invoiceAddressTelephone
     *
     * @return string 
     */
    public function getInvoiceAddressTelephone()
    {
        return $this->invoiceAddressTelephone;
    }

    /**
     * Set invoiceAddressFax
     *
     * @param string $invoiceAddressFax
     * @return DistributionChannels
     */
    public function setInvoiceAddressFax($invoiceAddressFax)
    {
        $this->invoiceAddressFax = $invoiceAddressFax;
    
        return $this;
    }

    /**
     * Get invoiceAddressFax
     *
     * @return string 
     */
    public function getInvoiceAddressFax()
    {
        return $this->invoiceAddressFax;
    }

    /**
     * Set reportColor
     *
     * @param string $reportColor
     * @return DistributionChannels
     */
    public function setReportColor($reportColor)
    {
        $this->reportColor = $reportColor;
    
        return $this;
    }

    /**
     * Get reportColor
     *
     * @return string 
     */
    public function getReportColor()
    {
        return $this->reportColor;
    }

    /**
     * Set reportLogo
     *
     * @param string $reportLogo
     * @return DistributionChannels
     */
    public function setReportLogo($reportLogo)
    {
        $this->reportLogo = $reportLogo;
    
        return $this;
    }

    /**
     * Get reportLogo
     *
     * @return string 
     */
    public function getReportLogo()
    {
        return $this->reportLogo;
    }

    /**
     * Set isRecheckGeocoding
     *
     * @param integer $isRecheckGeocoding
     * @return DistributionChannels
     */
    public function setIsRecheckGeocoding($isRecheckGeocoding)
    {
        $this->isRecheckGeocoding = $isRecheckGeocoding;
    
        return $this;
    }

    /**
     * Get isRecheckGeocoding
     *
     * @return integer 
     */
    public function getIsRecheckGeocoding()
    {
        return $this->isRecheckGeocoding;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return DistributionChannels
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return DistributionChannels
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set geocodingStatusCode
     *
     * @param string $geocodingStatusCode
     * @return DistributionChannels
     */
    public function setGeocodingStatusCode($geocodingStatusCode)
    {
        $this->geocodingStatusCode = $geocodingStatusCode;
    
        return $this;
    }

    /**
     * Get geocodingStatusCode
     *
     * @return string 
     */
    public function getGeocodingStatusCode()
    {
        return $this->geocodingStatusCode;
    }

    /**
     * Set accountBalance
     *
     * @param float $accountBalance
     * @return DistributionChannels
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;
    
        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return float 
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set invoiceClientName
     *
     * @param string $invoiceClientName
     * @return DistributionChannels
     */
    public function setInvoiceClientName($invoiceClientName)
    {
        $this->invoiceClientName = $invoiceClientName;
    
        return $this;
    }

    /**
     * Get invoiceClientName
     *
     * @return string 
     */
    public function getInvoiceClientName()
    {
        return $this->invoiceClientName;
    }

    /**
     * Set invoiceCompanyName
     *
     * @param string $invoiceCompanyName
     * @return DistributionChannels
     */
    public function setInvoiceCompanyName($invoiceCompanyName)
    {
        $this->invoiceCompanyName = $invoiceCompanyName;
    
        return $this;
    }

    /**
     * Get invoiceCompanyName
     *
     * @return string 
     */
    public function getInvoiceCompanyName()
    {
        return $this->invoiceCompanyName;
    }

    /**
     * Set contactPersonName
     *
     * @param string $contactPersonName
     * @return DistributionChannels
     */
    public function setContactPersonName($contactPersonName)
    {
        $this->contactPersonName = $contactPersonName;
    
        return $this;
    }

    /**
     * Get contactPersonName
     *
     * @return string 
     */
    public function getContactPersonName()
    {
        return $this->contactPersonName;
    }

    /**
     * Set contactPersonEmail
     *
     * @param string $contactPersonEmail
     * @return DistributionChannels
     */
    public function setContactPersonEmail($contactPersonEmail)
    {
        $this->contactPersonEmail = $contactPersonEmail;
    
        return $this;
    }

    /**
     * Get contactPersonEmail
     *
     * @return string 
     */
    public function getContactPersonEmail()
    {
        return $this->contactPersonEmail;
    }

    /**
     * Set contactPersonTelephoneNumber
     *
     * @param string $contactPersonTelephoneNumber
     * @return DistributionChannels
     */
    public function setContactPersonTelephoneNumber($contactPersonTelephoneNumber)
    {
        $this->contactPersonTelephoneNumber = $contactPersonTelephoneNumber;
    
        return $this;
    }

    /**
     * Get contactPersonTelephoneNumber
     *
     * @return string 
     */
    public function getContactPersonTelephoneNumber()
    {
        return $this->contactPersonTelephoneNumber;
    }

    /**
     * Set isStopShipping
     *
     * @param boolean $isStopShipping
     * @return DistributionChannels
     */
    public function setIsStopShipping($isStopShipping)
    {
        $this->isStopShipping = $isStopShipping;
    
        return $this;
    }

    /**
     * Get isStopShipping
     *
     * @return boolean 
     */
    public function getIsStopShipping()
    {
        return $this->isStopShipping;
    }

    /**
     * Set idPreferredPayment
     *
     * @param integer $idPreferredPayment
     * @return DistributionChannels
     */
    public function setIdPreferredPayment($idPreferredPayment)
    {
        $this->idPreferredPayment = $idPreferredPayment;
    
        return $this;
    }

    /**
     * Get idPreferredPayment
     *
     * @return integer 
     */
    public function getIdPreferredPayment()
    {
        return $this->idPreferredPayment;
    }

    /**
     * Set numberFeedback
     *
     * @param integer $numberFeedback
     * @return DistributionChannels
     */
    public function setNumberFeedback($numberFeedback)
    {
        $this->numberFeedback = $numberFeedback;
    
        return $this;
    }

    /**
     * Get numberFeedback
     *
     * @return integer 
     */
    public function getNumberFeedback()
    {
        return $this->numberFeedback;
    }

    /**
     * Set idDncReportType
     *
     * @param integer $idDncReportType
     * @return DistributionChannels
     */
    public function setIdDncReportType($idDncReportType)
    {
        $this->idDncReportType = $idDncReportType;
    
        return $this;
    }

    /**
     * Get idDncReportType
     *
     * @return integer 
     */
    public function getIdDncReportType()
    {
        return $this->idDncReportType;
    }

    /**
     * Set idReportNameType
     *
     * @param integer $idReportNameType
     * @return DistributionChannels
     */
    public function setIdReportNameType($idReportNameType)
    {
        $this->idReportNameType = $idReportNameType;
    
        return $this;
    }

    /**
     * Get idReportNameType
     *
     * @return integer 
     */
    public function getIdReportNameType()
    {
        return $this->idReportNameType;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return DistributionChannels
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set idMarginGoesTo
     *
     * @param integer $idMarginGoesTo
     * @return DistributionChannels
     */
    public function setIdMarginGoesTo($idMarginGoesTo)
    {
        $this->idMarginGoesTo = $idMarginGoesTo;
    
        return $this;
    }

    /**
     * Get idMarginGoesTo
     *
     * @return integer 
     */
    public function getIdMarginGoesTo()
    {
        return $this->idMarginGoesTo;
    }

    /**
     * Set idNutrimeGoesTo
     *
     * @param integer $idNutrimeGoesTo
     * @return DistributionChannels
     */
    public function setIdNutrimeGoesTo($idNutrimeGoesTo)
    {
        $this->idNutrimeGoesTo = $idNutrimeGoesTo;
    
        return $this;
    }

    /**
     * Get idNutrimeGoesTo
     *
     * @return integer 
     */
    public function getIdNutrimeGoesTo()
    {
        return $this->idNutrimeGoesTo;
    }

    /**
     * Set isFixedReportGoesTo
     *
     * @param boolean $isFixedReportGoesTo
     * @return DistributionChannels
     */
    public function setIsFixedReportGoesTo($isFixedReportGoesTo)
    {
        $this->isFixedReportGoesTo = $isFixedReportGoesTo;
    
        return $this;
    }

    /**
     * Get isFixedReportGoesTo
     *
     * @return boolean 
     */
    public function getIsFixedReportGoesTo()
    {
        return $this->isFixedReportGoesTo;
    }

    /**
     * Set isInvoiceWithTax
     *
     * @param boolean $isInvoiceWithTax
     * @return DistributionChannels
     */
    public function setIsInvoiceWithTax($isInvoiceWithTax)
    {
        $this->isInvoiceWithTax = $isInvoiceWithTax;
    
        return $this;
    }

    /**
     * Get isInvoiceWithTax
     *
     * @return boolean 
     */
    public function getIsInvoiceWithTax()
    {
        return $this->isInvoiceWithTax;
    }

    /**
     * Set isPaymentWithTax
     *
     * @param boolean $isPaymentWithTax
     * @return DistributionChannels
     */
    public function setIsPaymentWithTax($isPaymentWithTax)
    {
        $this->isPaymentWithTax = $isPaymentWithTax;
    
        return $this;
    }

    /**
     * Get isPaymentWithTax
     *
     * @return boolean 
     */
    public function getIsPaymentWithTax()
    {
        return $this->isPaymentWithTax;
    }

    /**
     * Set idCompleteStatus
     *
     * @param integer $idCompleteStatus
     * @return DistributionChannels
     */
    public function setIdCompleteStatus($idCompleteStatus)
    {
        $this->idCompleteStatus = $idCompleteStatus;
    
        return $this;
    }

    /**
     * Get idCompleteStatus
     *
     * @return integer 
     */
    public function getIdCompleteStatus()
    {
        return $this->idCompleteStatus;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return DistributionChannels
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set idMarginPaymentMode
     *
     * @param integer $idMarginPaymentMode
     * @return DistributionChannels
     */
    public function setIdMarginPaymentMode($idMarginPaymentMode)
    {
        $this->idMarginPaymentMode = $idMarginPaymentMode;
    
        return $this;
    }

    /**
     * Get idMarginPaymentMode
     *
     * @return integer 
     */
    public function getIdMarginPaymentMode()
    {
        return $this->idMarginPaymentMode;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur1
     *
     * @param boolean $isTimelyDeclinedAcquisiteur1
     * @return DistributionChannels
     */
    public function setIsTimelyDeclinedAcquisiteur1($isTimelyDeclinedAcquisiteur1)
    {
        $this->isTimelyDeclinedAcquisiteur1 = $isTimelyDeclinedAcquisiteur1;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur1
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur1()
    {
        return $this->isTimelyDeclinedAcquisiteur1;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur2
     *
     * @param boolean $isTimelyDeclinedAcquisiteur2
     * @return DistributionChannels
     */
    public function setIsTimelyDeclinedAcquisiteur2($isTimelyDeclinedAcquisiteur2)
    {
        $this->isTimelyDeclinedAcquisiteur2 = $isTimelyDeclinedAcquisiteur2;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur2
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur2()
    {
        return $this->isTimelyDeclinedAcquisiteur2;
    }

    /**
     * Set isTimelyDeclinedAcquisiteur3
     *
     * @param boolean $isTimelyDeclinedAcquisiteur3
     * @return DistributionChannels
     */
    public function setIsTimelyDeclinedAcquisiteur3($isTimelyDeclinedAcquisiteur3)
    {
        $this->isTimelyDeclinedAcquisiteur3 = $isTimelyDeclinedAcquisiteur3;
    
        return $this;
    }

    /**
     * Get isTimelyDeclinedAcquisiteur3
     *
     * @return boolean 
     */
    public function getIsTimelyDeclinedAcquisiteur3()
    {
        return $this->isTimelyDeclinedAcquisiteur3;
    }

    /**
     * Set paymentNoTaxText
     *
     * @param integer $paymentNoTaxText
     * @return DistributionChannels
     */
    public function setPaymentNoTaxText($paymentNoTaxText)
    {
        $this->paymentNoTaxText = $paymentNoTaxText;
    
        return $this;
    }

    /**
     * Get paymentNoTaxText
     *
     * @return integer 
     */
    public function getPaymentNoTaxText()
    {
        return $this->paymentNoTaxText;
    }

    /**
     * Set yearlyDecayPercentage1
     *
     * @param float $yearlyDecayPercentage1
     * @return DistributionChannels
     */
    public function setYearlyDecayPercentage1($yearlyDecayPercentage1)
    {
        $this->yearlyDecayPercentage1 = $yearlyDecayPercentage1;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage1
     *
     * @return float 
     */
    public function getYearlyDecayPercentage1()
    {
        return $this->yearlyDecayPercentage1;
    }

    /**
     * Set yearlyDecayPercentage2
     *
     * @param float $yearlyDecayPercentage2
     * @return DistributionChannels
     */
    public function setYearlyDecayPercentage2($yearlyDecayPercentage2)
    {
        $this->yearlyDecayPercentage2 = $yearlyDecayPercentage2;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage2
     *
     * @return float 
     */
    public function getYearlyDecayPercentage2()
    {
        return $this->yearlyDecayPercentage2;
    }

    /**
     * Set yearlyDecayPercentage3
     *
     * @param float $yearlyDecayPercentage3
     * @return DistributionChannels
     */
    public function setYearlyDecayPercentage3($yearlyDecayPercentage3)
    {
        $this->yearlyDecayPercentage3 = $yearlyDecayPercentage3;
    
        return $this;
    }

    /**
     * Get yearlyDecayPercentage3
     *
     * @return float 
     */
    public function getYearlyDecayPercentage3()
    {
        return $this->yearlyDecayPercentage3;
    }

    /**
     * Set supplementInfo
     *
     * @param string $supplementInfo
     * @return DistributionChannels
     */
    public function setSupplementInfo($supplementInfo)
    {
        $this->supplementInfo = $supplementInfo;
    
        return $this;
    }

    /**
     * Get supplementInfo
     *
     * @return string 
     */
    public function getSupplementInfo()
    {
        return $this->supplementInfo;
    }

    /**
     * Set webLoginUsername
     *
     * @param string $webLoginUsername
     * @return DistributionChannels
     */
    public function setWebLoginUsername($webLoginUsername)
    {
        $this->webLoginUsername = $webLoginUsername;
    
        return $this;
    }

    /**
     * Get webLoginUsername
     *
     * @return string 
     */
    public function getWebLoginUsername()
    {
        return $this->webLoginUsername;
    }

    /**
     * Set webLoginPassword
     *
     * @param string $webLoginPassword
     * @return DistributionChannels
     */
    public function setWebLoginPassword($webLoginPassword)
    {
        $this->webLoginPassword = $webLoginPassword;
    
        return $this;
    }

    /**
     * Get webLoginPassword
     *
     * @return string 
     */
    public function getWebLoginPassword()
    {
        return $this->webLoginPassword;
    }

    /**
     * Set webLoginStatus
     *
     * @param integer $webLoginStatus
     * @return DistributionChannels
     */
    public function setWebLoginStatus($webLoginStatus)
    {
        $this->webLoginStatus = $webLoginStatus;
    
        return $this;
    }

    /**
     * Get webLoginStatus
     *
     * @return integer 
     */
    public function getWebLoginStatus()
    {
        return $this->webLoginStatus;
    }

    /**
     * Set isCustomerBeContacted
     *
     * @param boolean $isCustomerBeContacted
     * @return DistributionChannels
     */
    public function setIsCustomerBeContacted($isCustomerBeContacted)
    {
        $this->isCustomerBeContacted = $isCustomerBeContacted;
    
        return $this;
    }

    /**
     * Get isCustomerBeContacted
     *
     * @return boolean 
     */
    public function getIsCustomerBeContacted()
    {
        return $this->isCustomerBeContacted;
    }

    /**
     * Set isNutrimeOffered
     *
     * @param boolean $isNutrimeOffered
     * @return DistributionChannels
     */
    public function setIsNutrimeOffered($isNutrimeOffered)
    {
        $this->isNutrimeOffered = $isNutrimeOffered;
    
        return $this;
    }

    /**
     * Get isNutrimeOffered
     *
     * @return boolean 
     */
    public function getIsNutrimeOffered()
    {
        return $this->isNutrimeOffered;
    }

    /**
     * Set isNotContacted
     *
     * @param boolean $isNotContacted
     * @return DistributionChannels
     */
    public function setIsNotContacted($isNotContacted)
    {
        $this->isNotContacted = $isNotContacted;
    
        return $this;
    }

    /**
     * Get isNotContacted
     *
     * @return boolean 
     */
    public function getIsNotContacted()
    {
        return $this->isNotContacted;
    }

    /**
     * Set isDestroySample
     *
     * @param boolean $isDestroySample
     * @return DistributionChannels
     */
    public function setIsDestroySample($isDestroySample)
    {
        $this->isDestroySample = $isDestroySample;
    
        return $this;
    }

    /**
     * Get isDestroySample
     *
     * @return boolean 
     */
    public function getIsDestroySample()
    {
        return $this->isDestroySample;
    }

    /**
     * Set sampleCanBeUsedForScience
     *
     * @param boolean $sampleCanBeUsedForScience
     * @return DistributionChannels
     */
    public function setSampleCanBeUsedForScience($sampleCanBeUsedForScience)
    {
        $this->sampleCanBeUsedForScience = $sampleCanBeUsedForScience;
    
        return $this;
    }

    /**
     * Get sampleCanBeUsedForScience
     *
     * @return boolean 
     */
    public function getSampleCanBeUsedForScience()
    {
        return $this->sampleCanBeUsedForScience;
    }

    /**
     * Set isFutureResearchParticipant
     *
     * @param boolean $isFutureResearchParticipant
     * @return DistributionChannels
     */
    public function setIsFutureResearchParticipant($isFutureResearchParticipant)
    {
        $this->isFutureResearchParticipant = $isFutureResearchParticipant;
    
        return $this;
    }

    /**
     * Get isFutureResearchParticipant
     *
     * @return boolean 
     */
    public function getIsFutureResearchParticipant()
    {
        return $this->isFutureResearchParticipant;
    }

    /**
     * Set idVisibleGroupProduct
     *
     * @param integer $idVisibleGroupProduct
     * @return DistributionChannels
     */
    public function setIdVisibleGroupProduct($idVisibleGroupProduct)
    {
        $this->idVisibleGroupProduct = $idVisibleGroupProduct;
    
        return $this;
    }

    /**
     * Get idVisibleGroupProduct
     *
     * @return integer 
     */
    public function getIdVisibleGroupProduct()
    {
        return $this->idVisibleGroupProduct;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return DistributionChannels
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set idVisibleGroupReport
     *
     * @param integer $idVisibleGroupReport
     * @return DistributionChannels
     */
    public function setIdVisibleGroupReport($idVisibleGroupReport)
    {
        $this->idVisibleGroupReport = $idVisibleGroupReport;
    
        return $this;
    }

    /**
     * Get idVisibleGroupReport
     *
     * @return integer 
     */
    public function getIdVisibleGroupReport()
    {
        return $this->idVisibleGroupReport;
    }

    /**
     * Set invoiceDeliveryNote
     *
     * @param integer $invoiceDeliveryNote
     * @return DistributionChannels
     */
    public function setInvoiceDeliveryNote($invoiceDeliveryNote)
    {
        $this->invoiceDeliveryNote = $invoiceDeliveryNote;
    
        return $this;
    }

    /**
     * Get invoiceDeliveryNote
     *
     * @return integer 
     */
    public function getInvoiceDeliveryNote()
    {
        return $this->invoiceDeliveryNote;
    }

    /**
     * Set webIdLanguage
     *
     * @param integer $webIdLanguage
     * @return DistributionChannels
     */
    public function setWebIdLanguage($webIdLanguage)
    {
        $this->webIdLanguage = $webIdLanguage;
    
        return $this;
    }

    /**
     * Get webIdLanguage
     *
     * @return integer 
     */
    public function getWebIdLanguage()
    {
        return $this->webIdLanguage;
    }

    /**
     * Set isShowBodCoverPage
     *
     * @param boolean $isShowBodCoverPage
     * @return DistributionChannels
     */
    public function setIsShowBodCoverPage($isShowBodCoverPage)
    {
        $this->isShowBodCoverPage = $isShowBodCoverPage;
    
        return $this;
    }

    /**
     * Get isShowBodCoverPage
     *
     * @return boolean 
     */
    public function getIsShowBodCoverPage()
    {
        return $this->isShowBodCoverPage;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return DistributionChannels
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set canCreateLoginAndSendStatusMessageToUser
     *
     * @param boolean $canCreateLoginAndSendStatusMessageToUser
     * @return DistributionChannels
     */
    public function setCanCreateLoginAndSendStatusMessageToUser($canCreateLoginAndSendStatusMessageToUser)
    {
        $this->canCreateLoginAndSendStatusMessageToUser = $canCreateLoginAndSendStatusMessageToUser;
    
        return $this;
    }

    /**
     * Get canCreateLoginAndSendStatusMessageToUser
     *
     * @return boolean 
     */
    public function getCanCreateLoginAndSendStatusMessageToUser()
    {
        return $this->canCreateLoginAndSendStatusMessageToUser;
    }

    /**
     * Set canViewShop
     *
     * @param boolean $canViewShop
     * @return DistributionChannels
     */
    public function setCanViewShop($canViewShop)
    {
        $this->canViewShop = $canViewShop;
    
        return $this;
    }

    /**
     * Get canViewShop
     *
     * @return boolean 
     */
    public function getCanViewShop()
    {
        return $this->canViewShop;
    }

    /**
     * Set isGenerateFoodTableExcelVersion
     *
     * @param boolean $isGenerateFoodTableExcelVersion
     * @return DistributionChannels
     */
    public function setIsGenerateFoodTableExcelVersion($isGenerateFoodTableExcelVersion)
    {
        $this->isGenerateFoodTableExcelVersion = $isGenerateFoodTableExcelVersion;
    
        return $this;
    }

    /**
     * Get isGenerateFoodTableExcelVersion
     *
     * @return boolean 
     */
    public function getIsGenerateFoodTableExcelVersion()
    {
        return $this->isGenerateFoodTableExcelVersion;
    }

    /**
     * Set lastOrderDate
     *
     * @param \DateTime $lastOrderDate
     * @return DistributionChannels
     */
    public function setLastOrderDate($lastOrderDate)
    {
        $this->lastOrderDate = $lastOrderDate;
    
        return $this;
    }

    /**
     * Get lastOrderDate
     *
     * @return \DateTime 
     */
    public function getLastOrderDate()
    {
        return $this->lastOrderDate;
    }

    /**
     * Set destroyInNumDays
     *
     * @param integer $destroyInNumDays
     * @return DistributionChannels
     */
    public function setDestroyInNumDays($destroyInNumDays)
    {
        $this->destroyInNumDays = $destroyInNumDays;
    
        return $this;
    }

    /**
     * Get destroyInNumDays
     *
     * @return integer 
     */
    public function getDestroyInNumDays()
    {
        return $this->destroyInNumDays;
    }

    /**
     * Set idRecipeBookDelivery
     *
     * @param integer $idRecipeBookDelivery
     * @return DistributionChannels
     */
    public function setIdRecipeBookDelivery($idRecipeBookDelivery)
    {
        $this->idRecipeBookDelivery = $idRecipeBookDelivery;
    
        return $this;
    }

    /**
     * Get idRecipeBookDelivery
     *
     * @return integer 
     */
    public function getIdRecipeBookDelivery()
    {
        return $this->idRecipeBookDelivery;
    }

    /**
     * Set idRecipeBookGoesTo
     *
     * @param integer $idRecipeBookGoesTo
     * @return DistributionChannels
     */
    public function setIdRecipeBookGoesTo($idRecipeBookGoesTo)
    {
        $this->idRecipeBookGoesTo = $idRecipeBookGoesTo;
    
        return $this;
    }

    /**
     * Get idRecipeBookGoesTo
     *
     * @return integer 
     */
    public function getIdRecipeBookGoesTo()
    {
        return $this->idRecipeBookGoesTo;
    }

    /**
     * Set canCustomersDownloadReports
     *
     * @param boolean $canCustomersDownloadReports
     * @return DistributionChannels
     */
    public function setCanCustomersDownloadReports($canCustomersDownloadReports)
    {
        $this->canCustomersDownloadReports = $canCustomersDownloadReports;
    
        return $this;
    }

    /**
     * Get canCustomersDownloadReports
     *
     * @return boolean 
     */
    public function getCanCustomersDownloadReports()
    {
        return $this->canCustomersDownloadReports;
    }

    /**
     * Set isGenerateXmlVersion
     *
     * @param boolean $isGenerateXmlVersion
     * @return DistributionChannels
     */
    public function setIsGenerateXmlVersion($isGenerateXmlVersion)
    {
        $this->isGenerateXmlVersion = $isGenerateXmlVersion;
    
        return $this;
    }

    /**
     * Get isGenerateXmlVersion
     *
     * @return boolean 
     */
    public function getIsGenerateXmlVersion()
    {
        return $this->isGenerateXmlVersion;
    }

    /**
     * Set isDeliveryViaSftp
     *
     * @param boolean $isDeliveryViaSftp
     * @return DistributionChannels
     */
    public function setIsDeliveryViaSftp($isDeliveryViaSftp)
    {
        $this->isDeliveryViaSftp = $isDeliveryViaSftp;
    
        return $this;
    }

    /**
     * Get isDeliveryViaSftp
     *
     * @return boolean 
     */
    public function getIsDeliveryViaSftp()
    {
        return $this->isDeliveryViaSftp;
    }

    /**
     * Set sftpHost
     *
     * @param string $sftpHost
     * @return DistributionChannels
     */
    public function setSftpHost($sftpHost)
    {
        $this->sftpHost = $sftpHost;
    
        return $this;
    }

    /**
     * Get sftpHost
     *
     * @return string 
     */
    public function getSftpHost()
    {
        return $this->sftpHost;
    }

    /**
     * Set sftpUsername
     *
     * @param string $sftpUsername
     * @return DistributionChannels
     */
    public function setSftpUsername($sftpUsername)
    {
        $this->sftpUsername = $sftpUsername;
    
        return $this;
    }

    /**
     * Get sftpUsername
     *
     * @return string 
     */
    public function getSftpUsername()
    {
        return $this->sftpUsername;
    }

    /**
     * Set sftpPassword
     *
     * @param string $sftpPassword
     * @return DistributionChannels
     */
    public function setSftpPassword($sftpPassword)
    {
        $this->sftpPassword = $sftpPassword;
    
        return $this;
    }

    /**
     * Get sftpPassword
     *
     * @return string 
     */
    public function getSftpPassword()
    {
        return $this->sftpPassword;
    }

    /**
     * Set isCanViewYourOrder
     *
     * @param boolean $isCanViewYourOrder
     * @return DistributionChannels
     */
    public function setIsCanViewYourOrder($isCanViewYourOrder)
    {
        $this->isCanViewYourOrder = $isCanViewYourOrder;
    
        return $this;
    }

    /**
     * Get isCanViewYourOrder
     *
     * @return boolean 
     */
    public function getIsCanViewYourOrder()
    {
        return $this->isCanViewYourOrder;
    }

    /**
     * Set isCanViewCustomerProtection
     *
     * @param boolean $isCanViewCustomerProtection
     * @return DistributionChannels
     */
    public function setIsCanViewCustomerProtection($isCanViewCustomerProtection)
    {
        $this->isCanViewCustomerProtection = $isCanViewCustomerProtection;
    
        return $this;
    }

    /**
     * Get isCanViewCustomerProtection
     *
     * @return boolean 
     */
    public function getIsCanViewCustomerProtection()
    {
        return $this->isCanViewCustomerProtection;
    }

    /**
     * Set isCanViewRegisterNewCustomer
     *
     * @param boolean $isCanViewRegisterNewCustomer
     * @return DistributionChannels
     */
    public function setIsCanViewRegisterNewCustomer($isCanViewRegisterNewCustomer)
    {
        $this->isCanViewRegisterNewCustomer = $isCanViewRegisterNewCustomer;
    
        return $this;
    }

    /**
     * Get isCanViewRegisterNewCustomer
     *
     * @return boolean 
     */
    public function getIsCanViewRegisterNewCustomer()
    {
        return $this->isCanViewRegisterNewCustomer;
    }

    /**
     * Set isCanViewPendingOrder
     *
     * @param boolean $isCanViewPendingOrder
     * @return DistributionChannels
     */
    public function setIsCanViewPendingOrder($isCanViewPendingOrder)
    {
        $this->isCanViewPendingOrder = $isCanViewPendingOrder;
    
        return $this;
    }

    /**
     * Get isCanViewPendingOrder
     *
     * @return boolean 
     */
    public function getIsCanViewPendingOrder()
    {
        return $this->isCanViewPendingOrder;
    }

    /**
     * Set isCanViewSetting
     *
     * @param boolean $isCanViewSetting
     * @return DistributionChannels
     */
    public function setIsCanViewSetting($isCanViewSetting)
    {
        $this->isCanViewSetting = $isCanViewSetting;
    
        return $this;
    }

    /**
     * Get isCanViewSetting
     *
     * @return boolean 
     */
    public function getIsCanViewSetting()
    {
        return $this->isCanViewSetting;
    }

    /**
     * Set isPrintedReportSentToPartner
     *
     * @param boolean $isPrintedReportSentToPartner
     * @return DistributionChannels
     */
    public function setIsPrintedReportSentToPartner($isPrintedReportSentToPartner)
    {
        $this->isPrintedReportSentToPartner = $isPrintedReportSentToPartner;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToPartner
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToPartner()
    {
        return $this->isPrintedReportSentToPartner;
    }

    /**
     * Set isPrintedReportSentToDc
     *
     * @param boolean $isPrintedReportSentToDc
     * @return DistributionChannels
     */
    public function setIsPrintedReportSentToDc($isPrintedReportSentToDc)
    {
        $this->isPrintedReportSentToDc = $isPrintedReportSentToDc;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToDc
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToDc()
    {
        return $this->isPrintedReportSentToDc;
    }

    /**
     * Set isPrintedReportSentToCustomer
     *
     * @param boolean $isPrintedReportSentToCustomer
     * @return DistributionChannels
     */
    public function setIsPrintedReportSentToCustomer($isPrintedReportSentToCustomer)
    {
        $this->isPrintedReportSentToCustomer = $isPrintedReportSentToCustomer;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToCustomer
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToCustomer()
    {
        return $this->isPrintedReportSentToCustomer;
    }

    /**
     * Set isDigitalReportGoesToCustomer
     *
     * @param boolean $isDigitalReportGoesToCustomer
     * @return DistributionChannels
     */
    public function setIsDigitalReportGoesToCustomer($isDigitalReportGoesToCustomer)
    {
        $this->isDigitalReportGoesToCustomer = $isDigitalReportGoesToCustomer;
    
        return $this;
    }

    /**
     * Get isDigitalReportGoesToCustomer
     *
     * @return boolean 
     */
    public function getIsDigitalReportGoesToCustomer()
    {
        return $this->isDigitalReportGoesToCustomer;
    }

    /**
     * Set idRecallGoesTo
     *
     * @param integer $idRecallGoesTo
     * @return DistributionChannels
     */
    public function setIdRecallGoesTo($idRecallGoesTo)
    {
        $this->idRecallGoesTo = $idRecallGoesTo;
    
        return $this;
    }

    /**
     * Get idRecallGoesTo
     *
     * @return integer 
     */
    public function getIdRecallGoesTo()
    {
        return $this->idRecallGoesTo;
    }

    /**
     * Set sftpFolder
     *
     * @param string $sftpFolder
     * @return DistributionChannels
     */
    public function setSftpFolder($sftpFolder)
    {
        $this->sftpFolder = $sftpFolder;
    
        return $this;
    }

    /**
     * Get sftpFolder
     *
     * @return string 
     */
    public function getSftpFolder()
    {
        return $this->sftpFolder;
    }

    /**
     * Set isReportDeliveryDownloadAccess
     *
     * @param boolean $isReportDeliveryDownloadAccess
     * @return DistributionChannels
     */
    public function setIsReportDeliveryDownloadAccess($isReportDeliveryDownloadAccess)
    {
        $this->isReportDeliveryDownloadAccess = $isReportDeliveryDownloadAccess;
    
        return $this;
    }

    /**
     * Get isReportDeliveryDownloadAccess
     *
     * @return boolean 
     */
    public function getIsReportDeliveryDownloadAccess()
    {
        return $this->isReportDeliveryDownloadAccess;
    }

    /**
     * Set isReportDeliveryFtpServer
     *
     * @param boolean $isReportDeliveryFtpServer
     * @return DistributionChannels
     */
    public function setIsReportDeliveryFtpServer($isReportDeliveryFtpServer)
    {
        $this->isReportDeliveryFtpServer = $isReportDeliveryFtpServer;
    
        return $this;
    }

    /**
     * Get isReportDeliveryFtpServer
     *
     * @return boolean 
     */
    public function getIsReportDeliveryFtpServer()
    {
        return $this->isReportDeliveryFtpServer;
    }

    /**
     * Set isReportDeliveryPrintedBooklet
     *
     * @param boolean $isReportDeliveryPrintedBooklet
     * @return DistributionChannels
     */
    public function setIsReportDeliveryPrintedBooklet($isReportDeliveryPrintedBooklet)
    {
        $this->isReportDeliveryPrintedBooklet = $isReportDeliveryPrintedBooklet;
    
        return $this;
    }

    /**
     * Get isReportDeliveryPrintedBooklet
     *
     * @return boolean 
     */
    public function getIsReportDeliveryPrintedBooklet()
    {
        return $this->isReportDeliveryPrintedBooklet;
    }

    /**
     * Set isReportDeliveryDontChargeBooklet
     *
     * @param boolean $isReportDeliveryDontChargeBooklet
     * @return DistributionChannels
     */
    public function setIsReportDeliveryDontChargeBooklet($isReportDeliveryDontChargeBooklet)
    {
        $this->isReportDeliveryDontChargeBooklet = $isReportDeliveryDontChargeBooklet;
    
        return $this;
    }

    /**
     * Get isReportDeliveryDontChargeBooklet
     *
     * @return boolean 
     */
    public function getIsReportDeliveryDontChargeBooklet()
    {
        return $this->isReportDeliveryDontChargeBooklet;
    }

    /**
     * Set onlyShowDistributionChannelAddress
     *
     * @param boolean $onlyShowDistributionChannelAddress
     * @return DistributionChannels
     */
    public function setOnlyShowDistributionChannelAddress($onlyShowDistributionChannelAddress)
    {
        $this->onlyShowDistributionChannelAddress = $onlyShowDistributionChannelAddress;
    
        return $this;
    }

    /**
     * Get onlyShowDistributionChannelAddress
     *
     * @return boolean 
     */
    public function getOnlyShowDistributionChannelAddress()
    {
        return $this->onlyShowDistributionChannelAddress;
    }

    /**
     * Set idCode
     *
     * @param string $idCode
     * @return DistributionChannels
     */
    public function setIdCode($idCode)
    {
        $this->idCode = $idCode;
    
        return $this;
    }

    /**
     * Get idCode
     *
     * @return string 
     */
    public function getIdCode()
    {
        return $this->idCode;
    }

    /**
     * Set accountingCode
     *
     * @param integer $accountingCode
     * @return DistributionChannels
     */
    public function setAccountingCode($accountingCode)
    {
        $this->accountingCode = $accountingCode;
    
        return $this;
    }

    /**
     * Get accountingCode
     *
     * @return integer 
     */
    public function getAccountingCode()
    {
        return $this->accountingCode;
    }

    /**
     * Set isWebLoginOptOut
     *
     * @param boolean $isWebLoginOptOut
     * @return DistributionChannels
     */
    public function setIsWebLoginOptOut($isWebLoginOptOut)
    {
        $this->isWebLoginOptOut = $isWebLoginOptOut;
    
        return $this;
    }

    /**
     * Get isWebLoginOptOut
     *
     * @return boolean 
     */
    public function getIsWebLoginOptOut()
    {
        return $this->isWebLoginOptOut;
    }

    /**
     * Set webLoginGoesTo
     *
     * @param integer $webLoginGoesTo
     * @return DistributionChannels
     */
    public function setWebLoginGoesTo($webLoginGoesTo)
    {
        $this->webLoginGoesTo = $webLoginGoesTo;
    
        return $this;
    }

    /**
     * Get webLoginGoesTo
     *
     * @return integer 
     */
    public function getWebLoginGoesTo()
    {
        return $this->webLoginGoesTo;
    }

    /**
     * Set invoiceNoTaxText
     *
     * @param integer $invoiceNoTaxText
     * @return DistributionChannels
     */
    public function setInvoiceNoTaxText($invoiceNoTaxText)
    {
        $this->invoiceNoTaxText = $invoiceNoTaxText;
    
        return $this;
    }

    /**
     * Get invoiceNoTaxText
     *
     * @return integer 
     */
    public function getInvoiceNoTaxText()
    {
        return $this->invoiceNoTaxText;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return DistributionChannels
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set expressShipment
     *
     * @param boolean $expressShipment
     * @return DistributionChannels
     */
    public function setExpressShipment($expressShipment)
    {
        $this->expressShipment = $expressShipment;
    
        return $this;
    }

    /**
     * Get expressShipment
     *
     * @return boolean 
     */
    public function getExpressShipment()
    {
        return $this->expressShipment;
    }

    /**
     * Set commissionPaymentWithTax
     *
     * @param string $commissionPaymentWithTax
     * @return DistributionChannels
     */
    public function setCommissionPaymentWithTax($commissionPaymentWithTax)
    {
        $this->commissionPaymentWithTax = $commissionPaymentWithTax;
    
        return $this;
    }

    /**
     * Get commissionPaymentWithTax
     *
     * @return string 
     */
    public function getCommissionPaymentWithTax()
    {
        return $this->commissionPaymentWithTax;
    }

    /**
     * Set customBodAccountNumber
     *
     * @param string $customBodAccountNumber
     * @return DistributionChannels
     */
    public function setCustomBodAccountNumber($customBodAccountNumber)
    {
        $this->customBodAccountNumber = $customBodAccountNumber;
    
        return $this;
    }

    /**
     * Get customBodAccountNumber
     *
     * @return string 
     */
    public function getCustomBodAccountNumber()
    {
        return $this->customBodAccountNumber;
    }

    /**
     * Set customBodBarcodeNumber
     *
     * @param string $customBodBarcodeNumber
     * @return DistributionChannels
     */
    public function setCustomBodBarcodeNumber($customBodBarcodeNumber)
    {
        $this->customBodBarcodeNumber = $customBodBarcodeNumber;
    
        return $this;
    }

    /**
     * Get customBodBarcodeNumber
     *
     * @return string 
     */
    public function getCustomBodBarcodeNumber()
    {
        return $this->customBodBarcodeNumber;
    }

    /**
     * Set canDownloadViaSftp
     *
     * @param boolean $canDownloadViaSftp
     * @return DistributionChannels
     */
    public function setCanDownloadViaSftp($canDownloadViaSftp)
    {
        $this->canDownloadViaSftp = $canDownloadViaSftp;
    
        return $this;
    }

    /**
     * Get canDownloadViaSftp
     *
     * @return boolean 
     */
    public function getCanDownloadViaSftp()
    {
        return $this->canDownloadViaSftp;
    }

    /**
     * Set sftpDownloadHost
     *
     * @param string $sftpDownloadHost
     * @return DistributionChannels
     */
    public function setSftpDownloadHost($sftpDownloadHost)
    {
        $this->sftpDownloadHost = $sftpDownloadHost;
    
        return $this;
    }

    /**
     * Get sftpDownloadHost
     *
     * @return string 
     */
    public function getSftpDownloadHost()
    {
        return $this->sftpDownloadHost;
    }

    /**
     * Set sftpDownloadUsername
     *
     * @param string $sftpDownloadUsername
     * @return DistributionChannels
     */
    public function setSftpDownloadUsername($sftpDownloadUsername)
    {
        $this->sftpDownloadUsername = $sftpDownloadUsername;
    
        return $this;
    }

    /**
     * Get sftpDownloadUsername
     *
     * @return string 
     */
    public function getSftpDownloadUsername()
    {
        return $this->sftpDownloadUsername;
    }

    /**
     * Set sftpDownloadPassword
     *
     * @param string $sftpDownloadPassword
     * @return DistributionChannels
     */
    public function setSftpDownloadPassword($sftpDownloadPassword)
    {
        $this->sftpDownloadPassword = $sftpDownloadPassword;
    
        return $this;
    }

    /**
     * Get sftpDownloadPassword
     *
     * @return string 
     */
    public function getSftpDownloadPassword()
    {
        return $this->sftpDownloadPassword;
    }

    /**
     * Set sftpDownloadFolder
     *
     * @param string $sftpDownloadFolder
     * @return DistributionChannels
     */
    public function setSftpDownloadFolder($sftpDownloadFolder)
    {
        $this->sftpDownloadFolder = $sftpDownloadFolder;
    
        return $this;
    }

    /**
     * Get sftpDownloadFolder
     *
     * @return string 
     */
    public function getSftpDownloadFolder()
    {
        return $this->sftpDownloadFolder;
    }

    /**
     * Set isDelayDownloadReport
     *
     * @param boolean $isDelayDownloadReport
     * @return DistributionChannels
     */
    public function setIsDelayDownloadReport($isDelayDownloadReport)
    {
        $this->isDelayDownloadReport = $isDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get isDelayDownloadReport
     *
     * @return boolean 
     */
    public function getIsDelayDownloadReport()
    {
        return $this->isDelayDownloadReport;
    }

    /**
     * Set numberDateDelayDownloadReport
     *
     * @param integer $numberDateDelayDownloadReport
     * @return DistributionChannels
     */
    public function setNumberDateDelayDownloadReport($numberDateDelayDownloadReport)
    {
        $this->numberDateDelayDownloadReport = $numberDateDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get numberDateDelayDownloadReport
     *
     * @return integer 
     */
    public function getNumberDateDelayDownloadReport()
    {
        return $this->numberDateDelayDownloadReport;
    }

    /**
     * Set isNotAccessReport
     *
     * @param boolean $isNotAccessReport
     * @return DistributionChannels
     */
    public function setIsNotAccessReport($isNotAccessReport)
    {
        $this->isNotAccessReport = $isNotAccessReport;
    
        return $this;
    }

    /**
     * Get isNotAccessReport
     *
     * @return boolean 
     */
    public function getIsNotAccessReport()
    {
        return $this->isNotAccessReport;
    }

    /**
     * Set isNotSendEmail
     *
     * @param boolean $isNotSendEmail
     * @return DistributionChannels
     */
    public function setIsNotSendEmail($isNotSendEmail)
    {
        $this->isNotSendEmail = $isNotSendEmail;
    
        return $this;
    }

    /**
     * Get isNotSendEmail
     *
     * @return boolean 
     */
    public function getIsNotSendEmail()
    {
        return $this->isNotSendEmail;
    }
}