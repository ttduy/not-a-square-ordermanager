<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CashFlowSubmitDataHistory
 *
 * @ORM\Table(name="cash_flow_submit_data_history")
 * @ORM\Entity
 */
class CashFlowSubmitDataHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string $previewData
     *
     * @ORM\Column(name="preview_data", type="text", nullable=true)
     */
    private $previewData;

    /**
     * @var integer $numFormatError
     *
     * @ORM\Column(name="num_format_error", type="integer", nullable=true)
     */
    private $numFormatError;

    /**
     * @var integer $numDataError
     *
     * @ORM\Column(name="num_data_error", type="integer", nullable=true)
     */
    private $numDataError;

    /**
     * @var integer $numSuccess
     *
     * @ORM\Column(name="num_success", type="integer", nullable=true)
     */
    private $numSuccess;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return CashFlowSubmitDataHistory
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return CashFlowSubmitDataHistory
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set previewData
     *
     * @param string $previewData
     * @return CashFlowSubmitDataHistory
     */
    public function setPreviewData($previewData)
    {
        $this->previewData = $previewData;
    
        return $this;
    }

    /**
     * Get previewData
     *
     * @return string 
     */
    public function getPreviewData()
    {
        return $this->previewData;
    }

    /**
     * Set numFormatError
     *
     * @param integer $numFormatError
     * @return CashFlowSubmitDataHistory
     */
    public function setNumFormatError($numFormatError)
    {
        $this->numFormatError = $numFormatError;
    
        return $this;
    }

    /**
     * Get numFormatError
     *
     * @return integer 
     */
    public function getNumFormatError()
    {
        return $this->numFormatError;
    }

    /**
     * Set numDataError
     *
     * @param integer $numDataError
     * @return CashFlowSubmitDataHistory
     */
    public function setNumDataError($numDataError)
    {
        $this->numDataError = $numDataError;
    
        return $this;
    }

    /**
     * Get numDataError
     *
     * @return integer 
     */
    public function getNumDataError()
    {
        return $this->numDataError;
    }

    /**
     * Set numSuccess
     *
     * @param integer $numSuccess
     * @return CashFlowSubmitDataHistory
     */
    public function setNumSuccess($numSuccess)
    {
        $this->numSuccess = $numSuccess;
    
        return $this;
    }

    /**
     * Get numSuccess
     *
     * @return integer 
     */
    public function getNumSuccess()
    {
        return $this->numSuccess;
    }
}