<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SubOrder
 *
 * @ORM\Table(name="sub_order")
 * @ORM\Entity
 */
class SubOrder
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idOrder
     *
     * @ORM\Column(name="id_order", type="integer", nullable=true)
     */
    private $idOrder;

    /**
     * @var integer $idProductGroup
     *
     * @ORM\Column(name="id_product_group", type="integer", nullable=true)
     */
    private $idProductGroup;

    /**
     * @var string $barcode
     *
     * @ORM\Column(name="barcode", type="string", length=255, nullable=true)
     */
    private $barcode;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="date", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var string $status
     *
     * @ORM\Column(name="status", type="text", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     * @return SubOrder
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    
        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer 
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set idProductGroup
     *
     * @param integer $idProductGroup
     * @return SubOrder
     */
    public function setIdProductGroup($idProductGroup)
    {
        $this->idProductGroup = $idProductGroup;
    
        return $this;
    }

    /**
     * Get idProductGroup
     *
     * @return integer 
     */
    public function getIdProductGroup()
    {
        return $this->idProductGroup;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     * @return SubOrder
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    
        return $this;
    }

    /**
     * Get barcode
     *
     * @return string 
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return SubOrder
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return SubOrder
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return SubOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}