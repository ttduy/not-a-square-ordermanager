<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderedOverridePrice
 *
 * @ORM\Table(name="ordered_override_price")
 * @ORM\Entity
 */
class OrderedOverridePrice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var string $idEntry
     *
     * @ORM\Column(name="id_entry", type="string", length=50, nullable=true)
     */
    private $idEntry;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", nullable=true)
     */
    private $price;

    /**
     * @var float $dcMargin
     *
     * @ORM\Column(name="dc_margin", type="decimal", nullable=true)
     */
    private $dcMargin;

    /**
     * @var float $partMargin
     *
     * @ORM\Column(name="part_margin", type="decimal", nullable=true)
     */
    private $partMargin;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return OrderedOverridePrice
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idEntry
     *
     * @param string $idEntry
     * @return OrderedOverridePrice
     */
    public function setIdEntry($idEntry)
    {
        $this->idEntry = $idEntry;
    
        return $this;
    }

    /**
     * Get idEntry
     *
     * @return string 
     */
    public function getIdEntry()
    {
        return $this->idEntry;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return OrderedOverridePrice
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set dcMargin
     *
     * @param float $dcMargin
     * @return OrderedOverridePrice
     */
    public function setDcMargin($dcMargin)
    {
        $this->dcMargin = $dcMargin;
    
        return $this;
    }

    /**
     * Get dcMargin
     *
     * @return float 
     */
    public function getDcMargin()
    {
        return $this->dcMargin;
    }

    /**
     * Set partMargin
     *
     * @param float $partMargin
     * @return OrderedOverridePrice
     */
    public function setPartMargin($partMargin)
    {
        $this->partMargin = $partMargin;
    
        return $this;
    }

    /**
     * Get partMargin
     *
     * @return float 
     */
    public function getPartMargin()
    {
        return $this->partMargin;
    }
}