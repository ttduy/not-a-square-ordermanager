<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ExtraPriceReportDelivery
 *
 * @ORM\Table(name="extra_price_report_delivery")
 * @ORM\Entity
 */
class ExtraPriceReportDelivery
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCountry
     *
     * @ORM\Column(name="id_country", type="integer", nullable=true)
     */
    private $idCountry;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", nullable=true)
     */
    private $price;

    /**
     * @var float $nutriMePrice
     *
     * @ORM\Column(name="nutri_me_price", type="decimal", nullable=true)
     */
    private $nutriMePrice;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCountry
     *
     * @param integer $idCountry
     * @return ExtraPriceReportDelivery
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;
    
        return $this;
    }

    /**
     * Get idCountry
     *
     * @return integer 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return ExtraPriceReportDelivery
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set nutriMePrice
     *
     * @param float $nutriMePrice
     * @return ExtraPriceReportDelivery
     */
    public function setNutriMePrice($nutriMePrice)
    {
        $this->nutriMePrice = $nutriMePrice;
    
        return $this;
    }

    /**
     * Get nutriMePrice
     *
     * @return float 
     */
    public function getNutriMePrice()
    {
        return $this->nutriMePrice;
    }
}