<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SessionExtra
 *
 * @ORM\Table(name="session_extra")
 * @ORM\Entity
 */
class SessionExtra
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $sessionKey
     *
     * @ORM\Column(name="session_key", type="string", length=255, nullable=true)
     */
    private $sessionKey;

    /**
     * @var string $sessionValue
     *
     * @ORM\Column(name="session_value", type="text", nullable=true)
     */
    private $sessionValue;

    /**
     * @var \DateTime $sessionTime
     *
     * @ORM\Column(name="session_time", type="datetime", nullable=true)
     */
    private $sessionTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionKey
     *
     * @param string $sessionKey
     * @return SessionExtra
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;
    
        return $this;
    }

    /**
     * Get sessionKey
     *
     * @return string 
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * Set sessionValue
     *
     * @param string $sessionValue
     * @return SessionExtra
     */
    public function setSessionValue($sessionValue)
    {
        $this->sessionValue = $sessionValue;
    
        return $this;
    }

    /**
     * Get sessionValue
     *
     * @return string 
     */
    public function getSessionValue()
    {
        return $this->sessionValue;
    }

    /**
     * Set sessionTime
     *
     * @param \DateTime $sessionTime
     * @return SessionExtra
     */
    public function setSessionTime($sessionTime)
    {
        $this->sessionTime = $sessionTime;
    
        return $this;
    }

    /**
     * Get sessionTime
     *
     * @return \DateTime 
     */
    public function getSessionTime()
    {
        return $this->sessionTime;
    }
}