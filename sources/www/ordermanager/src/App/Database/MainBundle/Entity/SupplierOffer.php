<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SupplierOffer
 *
 * @ORM\Table(name="supplier_offer")
 * @ORM\Entity
 */
class SupplierOffer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string $fileKey
     *
     * @ORM\Column(name="file_key", type="string", length=255, nullable=true)
     */
    private $fileKey;

    /**
     * @var string $file
     *
     * @ORM\Column(name="file", type="text", nullable=true)
     */
    private $file;

    /**
     * @var string $supplier
     *
     * @ORM\Column(name="supplier", type="string", length=255, nullable=true)
     */
    private $supplier;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SupplierOffer
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fileKey
     *
     * @param string $fileKey
     * @return SupplierOffer
     */
    public function setFileKey($fileKey)
    {
        $this->fileKey = $fileKey;
    
        return $this;
    }

    /**
     * Get fileKey
     *
     * @return string 
     */
    public function getFileKey()
    {
        return $this->fileKey;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return SupplierOffer
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set supplier
     *
     * @param string $supplier
     * @return SupplierOffer
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    
        return $this;
    }

    /**
     * Get supplier
     *
     * @return string 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }
}