<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GlacierFile
 *
 * @ORM\Table(name="glacier_file")
 * @ORM\Entity
 */
class GlacierFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGlacierVault
     *
     * @ORM\Column(name="id_glacier_vault", type="integer", nullable=true)
     */
    private $idGlacierVault;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="text", nullable=true)
     */
    private $code;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime $archiveDatetime
     *
     * @ORM\Column(name="archive_datetime", type="datetime", nullable=true)
     */
    private $archiveDatetime;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $statusDatetime
     *
     * @ORM\Column(name="status_datetime", type="datetime", nullable=true)
     */
    private $statusDatetime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGlacierVault
     *
     * @param integer $idGlacierVault
     * @return GlacierFile
     */
    public function setIdGlacierVault($idGlacierVault)
    {
        $this->idGlacierVault = $idGlacierVault;
    
        return $this;
    }

    /**
     * Get idGlacierVault
     *
     * @return integer 
     */
    public function getIdGlacierVault()
    {
        return $this->idGlacierVault;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return GlacierFile
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GlacierFile
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set archiveDatetime
     *
     * @param \DateTime $archiveDatetime
     * @return GlacierFile
     */
    public function setArchiveDatetime($archiveDatetime)
    {
        $this->archiveDatetime = $archiveDatetime;
    
        return $this;
    }

    /**
     * Get archiveDatetime
     *
     * @return \DateTime 
     */
    public function getArchiveDatetime()
    {
        return $this->archiveDatetime;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return GlacierFile
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusDatetime
     *
     * @param \DateTime $statusDatetime
     * @return GlacierFile
     */
    public function setStatusDatetime($statusDatetime)
    {
        $this->statusDatetime = $statusDatetime;
    
        return $this;
    }

    /**
     * Get statusDatetime
     *
     * @return \DateTime 
     */
    public function getStatusDatetime()
    {
        return $this->statusDatetime;
    }
}