<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolAttachment
 *
 * @ORM\Table(name="email_pool_attachment")
 * @ORM\Entity
 */
class EmailPoolAttachment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailPool
     *
     * @ORM\Column(name="id_email_pool", type="integer", nullable=true)
     */
    private $idEmailPool;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $originalName
     *
     * @ORM\Column(name="original_name", type="string", length=255, nullable=true)
     */
    private $originalName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailPool
     *
     * @param integer $idEmailPool
     * @return EmailPoolAttachment
     */
    public function setIdEmailPool($idEmailPool)
    {
        $this->idEmailPool = $idEmailPool;
    
        return $this;
    }

    /**
     * Get idEmailPool
     *
     * @return integer 
     */
    public function getIdEmailPool()
    {
        return $this->idEmailPool;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EmailPoolAttachment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     * @return EmailPoolAttachment
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    
        return $this;
    }

    /**
     * Get originalName
     *
     * @return string 
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }
}