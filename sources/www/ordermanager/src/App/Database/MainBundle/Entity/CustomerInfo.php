<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerInfo
 *
 * @ORM\Table(name="customer_info")
 * @ORM\Entity
 */
class CustomerInfo
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $customerNumber
     *
     * @ORM\Column(name="customer_number", type="string", length=255, nullable=true)
     */
    private $customerNumber;

    /**
     * @var string $externalBarcode
     *
     * @ORM\Column(name="external_barcode", type="string", length=255, nullable=true)
     */
    private $externalBarcode;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=false)
     */
    private $idDistributionChannel;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $surName
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @var \DateTime $dateofbirth
     *
     * @ORM\Column(name="DateOfBirth", type="date", nullable=true)
     */
    private $dateofbirth;

    /**
     * @var string $postCode
     *
     * @ORM\Column(name="post_code", type="string", length=255, nullable=true)
     */
    private $postCode;

    /**
     * @var boolean $isDestroySample
     *
     * @ORM\Column(name="is_destroy_sample", type="boolean", nullable=false)
     */
    private $isDestroySample;

    /**
     * @var boolean $isFutureResearchParticipant
     *
     * @ORM\Column(name="is_future_research_participant", type="boolean", nullable=false)
     */
    private $isFutureResearchParticipant;

    /**
     * @var string $detailInformation
     *
     * @ORM\Column(name="detail_information", type="text", nullable=true)
     */
    private $detailInformation;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $webLoginUsername
     *
     * @ORM\Column(name="web_login_username", type="string", length=255, nullable=true)
     */
    private $webLoginUsername;

    /**
     * @var string $webLoginPassword
     *
     * @ORM\Column(name="web_login_password", type="string", length=255, nullable=true)
     */
    private $webLoginPassword;

    /**
     * @var integer $webLoginStatus
     *
     * @ORM\Column(name="web_login_status", type="integer", nullable=true)
     */
    private $webLoginStatus;

    /**
     * @var boolean $isCustomerBeContacted
     *
     * @ORM\Column(name="is_customer_be_contacted", type="boolean", nullable=true)
     */
    private $isCustomerBeContacted;

    /**
     * @var boolean $isNutrimeOffered
     *
     * @ORM\Column(name="is_nutrime_offered", type="boolean", nullable=true)
     */
    private $isNutrimeOffered;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var \DateTime $reportAvailableDate
     *
     * @ORM\Column(name="report_available_date", type="date", nullable=true)
     */
    private $reportAvailableDate;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var integer $numberFeedback
     *
     * @ORM\Column(name="number_feedback", type="integer", nullable=true)
     */
    private $numberFeedback;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer $webIdLanguage
     *
     * @ORM\Column(name="web_id_language", type="integer", nullable=true)
     */
    private $webIdLanguage;

    /**
     * @var boolean $sampleCanBeUsedForScience
     *
     * @ORM\Column(name="sample_can_be_used_for_science", type="boolean", nullable=true)
     */
    private $sampleCanBeUsedForScience;

    /**
     * @var boolean $isLocked
     *
     * @ORM\Column(name="is_locked", type="boolean", nullable=true)
     */
    private $isLocked;

    /**
     * @var \DateTime $lockDate
     *
     * @ORM\Column(name="lock_date", type="date", nullable=true)
     */
    private $lockDate;

    /**
     * @var boolean $isWebLoginOptOut
     *
     * @ORM\Column(name="is_web_login_opt_out", type="boolean", nullable=true)
     */
    private $isWebLoginOptOut;

    /**
     * @var boolean $isCanViewYourOrder
     *
     * @ORM\Column(name="is_can_view_your_order", type="boolean", nullable=true)
     */
    private $isCanViewYourOrder;

    /**
     * @var boolean $isCanViewCustomerProtection
     *
     * @ORM\Column(name="is_can_view_customer_protection", type="boolean", nullable=true)
     */
    private $isCanViewCustomerProtection;

    /**
     * @var boolean $isCanViewRegisterNewCustomer
     *
     * @ORM\Column(name="is_can_view_register_new_customer", type="boolean", nullable=true)
     */
    private $isCanViewRegisterNewCustomer;

    /**
     * @var boolean $isCanViewPendingOrder
     *
     * @ORM\Column(name="is_can_view_pending_order", type="boolean", nullable=true)
     */
    private $isCanViewPendingOrder;

    /**
     * @var boolean $isCanViewSetting
     *
     * @ORM\Column(name="is_can_view_setting", type="boolean", nullable=true)
     */
    private $isCanViewSetting;

    /**
     * @var boolean $canViewShop
     *
     * @ORM\Column(name="can_view_shop", type="boolean", nullable=true)
     */
    private $canViewShop;

    /**
     * @var integer $webLoginGoesTo
     *
     * @ORM\Column(name="web_login_goes_to", type="integer", nullable=true)
     */
    private $webLoginGoesTo;

    /**
     * @var integer $accountingCode
     *
     * @ORM\Column(name="accounting_code", type="integer", nullable=true)
     */
    private $accountingCode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerNumber
     *
     * @param string $customerNumber
     * @return CustomerInfo
     */
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;
    
        return $this;
    }

    /**
     * Get customerNumber
     *
     * @return string 
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * Set externalBarcode
     *
     * @param string $externalBarcode
     * @return CustomerInfo
     */
    public function setExternalBarcode($externalBarcode)
    {
        $this->externalBarcode = $externalBarcode;
    
        return $this;
    }

    /**
     * Get externalBarcode
     *
     * @return string 
     */
    public function getExternalBarcode()
    {
        return $this->externalBarcode;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return CustomerInfo
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return CustomerInfo
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surName
     *
     * @param string $surName
     * @return CustomerInfo
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;
    
        return $this;
    }

    /**
     * Get surName
     *
     * @return string 
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set dateofbirth
     *
     * @param \DateTime $dateofbirth
     * @return CustomerInfo
     */
    public function setDateofbirth($dateofbirth)
    {
        $this->dateofbirth = $dateofbirth;
    
        return $this;
    }

    /**
     * Get dateofbirth
     *
     * @return \DateTime 
     */
    public function getDateofbirth()
    {
        return $this->dateofbirth;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return CustomerInfo
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    
        return $this;
    }

    /**
     * Get postCode
     *
     * @return string 
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set isDestroySample
     *
     * @param boolean $isDestroySample
     * @return CustomerInfo
     */
    public function setIsDestroySample($isDestroySample)
    {
        $this->isDestroySample = $isDestroySample;
    
        return $this;
    }

    /**
     * Get isDestroySample
     *
     * @return boolean 
     */
    public function getIsDestroySample()
    {
        return $this->isDestroySample;
    }

    /**
     * Set isFutureResearchParticipant
     *
     * @param boolean $isFutureResearchParticipant
     * @return CustomerInfo
     */
    public function setIsFutureResearchParticipant($isFutureResearchParticipant)
    {
        $this->isFutureResearchParticipant = $isFutureResearchParticipant;
    
        return $this;
    }

    /**
     * Get isFutureResearchParticipant
     *
     * @return boolean 
     */
    public function getIsFutureResearchParticipant()
    {
        return $this->isFutureResearchParticipant;
    }

    /**
     * Set detailInformation
     *
     * @param string $detailInformation
     * @return CustomerInfo
     */
    public function setDetailInformation($detailInformation)
    {
        $this->detailInformation = $detailInformation;
    
        return $this;
    }

    /**
     * Get detailInformation
     *
     * @return string 
     */
    public function getDetailInformation()
    {
        return $this->detailInformation;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return CustomerInfo
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CustomerInfo
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set webLoginUsername
     *
     * @param string $webLoginUsername
     * @return CustomerInfo
     */
    public function setWebLoginUsername($webLoginUsername)
    {
        $this->webLoginUsername = $webLoginUsername;
    
        return $this;
    }

    /**
     * Get webLoginUsername
     *
     * @return string 
     */
    public function getWebLoginUsername()
    {
        return $this->webLoginUsername;
    }

    /**
     * Set webLoginPassword
     *
     * @param string $webLoginPassword
     * @return CustomerInfo
     */
    public function setWebLoginPassword($webLoginPassword)
    {
        $this->webLoginPassword = $webLoginPassword;
    
        return $this;
    }

    /**
     * Get webLoginPassword
     *
     * @return string 
     */
    public function getWebLoginPassword()
    {
        return $this->webLoginPassword;
    }

    /**
     * Set webLoginStatus
     *
     * @param integer $webLoginStatus
     * @return CustomerInfo
     */
    public function setWebLoginStatus($webLoginStatus)
    {
        $this->webLoginStatus = $webLoginStatus;
    
        return $this;
    }

    /**
     * Get webLoginStatus
     *
     * @return integer 
     */
    public function getWebLoginStatus()
    {
        return $this->webLoginStatus;
    }

    /**
     * Set isCustomerBeContacted
     *
     * @param boolean $isCustomerBeContacted
     * @return CustomerInfo
     */
    public function setIsCustomerBeContacted($isCustomerBeContacted)
    {
        $this->isCustomerBeContacted = $isCustomerBeContacted;
    
        return $this;
    }

    /**
     * Get isCustomerBeContacted
     *
     * @return boolean 
     */
    public function getIsCustomerBeContacted()
    {
        return $this->isCustomerBeContacted;
    }

    /**
     * Set isNutrimeOffered
     *
     * @param boolean $isNutrimeOffered
     * @return CustomerInfo
     */
    public function setIsNutrimeOffered($isNutrimeOffered)
    {
        $this->isNutrimeOffered = $isNutrimeOffered;
    
        return $this;
    }

    /**
     * Get isNutrimeOffered
     *
     * @return boolean 
     */
    public function getIsNutrimeOffered()
    {
        return $this->isNutrimeOffered;
    }

    /**
     * Set isNotContacted
     *
     * @param boolean $isNotContacted
     * @return CustomerInfo
     */
    public function setIsNotContacted($isNotContacted)
    {
        $this->isNotContacted = $isNotContacted;
    
        return $this;
    }

    /**
     * Get isNotContacted
     *
     * @return boolean 
     */
    public function getIsNotContacted()
    {
        return $this->isNotContacted;
    }

    /**
     * Set reportAvailableDate
     *
     * @param \DateTime $reportAvailableDate
     * @return CustomerInfo
     */
    public function setReportAvailableDate($reportAvailableDate)
    {
        $this->reportAvailableDate = $reportAvailableDate;
    
        return $this;
    }

    /**
     * Get reportAvailableDate
     *
     * @return \DateTime 
     */
    public function getReportAvailableDate()
    {
        return $this->reportAvailableDate;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return CustomerInfo
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }

    /**
     * Set numberFeedback
     *
     * @param integer $numberFeedback
     * @return CustomerInfo
     */
    public function setNumberFeedback($numberFeedback)
    {
        $this->numberFeedback = $numberFeedback;
    
        return $this;
    }

    /**
     * Get numberFeedback
     *
     * @return integer 
     */
    public function getNumberFeedback()
    {
        return $this->numberFeedback;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CustomerInfo
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set webIdLanguage
     *
     * @param integer $webIdLanguage
     * @return CustomerInfo
     */
    public function setWebIdLanguage($webIdLanguage)
    {
        $this->webIdLanguage = $webIdLanguage;
    
        return $this;
    }

    /**
     * Get webIdLanguage
     *
     * @return integer 
     */
    public function getWebIdLanguage()
    {
        return $this->webIdLanguage;
    }

    /**
     * Set sampleCanBeUsedForScience
     *
     * @param boolean $sampleCanBeUsedForScience
     * @return CustomerInfo
     */
    public function setSampleCanBeUsedForScience($sampleCanBeUsedForScience)
    {
        $this->sampleCanBeUsedForScience = $sampleCanBeUsedForScience;
    
        return $this;
    }

    /**
     * Get sampleCanBeUsedForScience
     *
     * @return boolean 
     */
    public function getSampleCanBeUsedForScience()
    {
        return $this->sampleCanBeUsedForScience;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return CustomerInfo
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
    
        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set lockDate
     *
     * @param \DateTime $lockDate
     * @return CustomerInfo
     */
    public function setLockDate($lockDate)
    {
        $this->lockDate = $lockDate;
    
        return $this;
    }

    /**
     * Get lockDate
     *
     * @return \DateTime 
     */
    public function getLockDate()
    {
        return $this->lockDate;
    }

    /**
     * Set isWebLoginOptOut
     *
     * @param boolean $isWebLoginOptOut
     * @return CustomerInfo
     */
    public function setIsWebLoginOptOut($isWebLoginOptOut)
    {
        $this->isWebLoginOptOut = $isWebLoginOptOut;
    
        return $this;
    }

    /**
     * Get isWebLoginOptOut
     *
     * @return boolean 
     */
    public function getIsWebLoginOptOut()
    {
        return $this->isWebLoginOptOut;
    }

    /**
     * Set isCanViewYourOrder
     *
     * @param boolean $isCanViewYourOrder
     * @return CustomerInfo
     */
    public function setIsCanViewYourOrder($isCanViewYourOrder)
    {
        $this->isCanViewYourOrder = $isCanViewYourOrder;
    
        return $this;
    }

    /**
     * Get isCanViewYourOrder
     *
     * @return boolean 
     */
    public function getIsCanViewYourOrder()
    {
        return $this->isCanViewYourOrder;
    }

    /**
     * Set isCanViewCustomerProtection
     *
     * @param boolean $isCanViewCustomerProtection
     * @return CustomerInfo
     */
    public function setIsCanViewCustomerProtection($isCanViewCustomerProtection)
    {
        $this->isCanViewCustomerProtection = $isCanViewCustomerProtection;
    
        return $this;
    }

    /**
     * Get isCanViewCustomerProtection
     *
     * @return boolean 
     */
    public function getIsCanViewCustomerProtection()
    {
        return $this->isCanViewCustomerProtection;
    }

    /**
     * Set isCanViewRegisterNewCustomer
     *
     * @param boolean $isCanViewRegisterNewCustomer
     * @return CustomerInfo
     */
    public function setIsCanViewRegisterNewCustomer($isCanViewRegisterNewCustomer)
    {
        $this->isCanViewRegisterNewCustomer = $isCanViewRegisterNewCustomer;
    
        return $this;
    }

    /**
     * Get isCanViewRegisterNewCustomer
     *
     * @return boolean 
     */
    public function getIsCanViewRegisterNewCustomer()
    {
        return $this->isCanViewRegisterNewCustomer;
    }

    /**
     * Set isCanViewPendingOrder
     *
     * @param boolean $isCanViewPendingOrder
     * @return CustomerInfo
     */
    public function setIsCanViewPendingOrder($isCanViewPendingOrder)
    {
        $this->isCanViewPendingOrder = $isCanViewPendingOrder;
    
        return $this;
    }

    /**
     * Get isCanViewPendingOrder
     *
     * @return boolean 
     */
    public function getIsCanViewPendingOrder()
    {
        return $this->isCanViewPendingOrder;
    }

    /**
     * Set isCanViewSetting
     *
     * @param boolean $isCanViewSetting
     * @return CustomerInfo
     */
    public function setIsCanViewSetting($isCanViewSetting)
    {
        $this->isCanViewSetting = $isCanViewSetting;
    
        return $this;
    }

    /**
     * Get isCanViewSetting
     *
     * @return boolean 
     */
    public function getIsCanViewSetting()
    {
        return $this->isCanViewSetting;
    }

    /**
     * Set canViewShop
     *
     * @param boolean $canViewShop
     * @return CustomerInfo
     */
    public function setCanViewShop($canViewShop)
    {
        $this->canViewShop = $canViewShop;
    
        return $this;
    }

    /**
     * Get canViewShop
     *
     * @return boolean 
     */
    public function getCanViewShop()
    {
        return $this->canViewShop;
    }

    /**
     * Set webLoginGoesTo
     *
     * @param integer $webLoginGoesTo
     * @return CustomerInfo
     */
    public function setWebLoginGoesTo($webLoginGoesTo)
    {
        $this->webLoginGoesTo = $webLoginGoesTo;
    
        return $this;
    }

    /**
     * Get webLoginGoesTo
     *
     * @return integer 
     */
    public function getWebLoginGoesTo()
    {
        return $this->webLoginGoesTo;
    }

    /**
     * Set accountingCode
     *
     * @param integer $accountingCode
     * @return CustomerInfo
     */
    public function setAccountingCode($accountingCode)
    {
        $this->accountingCode = $accountingCode;
    
        return $this;
    }

    /**
     * Get accountingCode
     *
     * @return integer 
     */
    public function getAccountingCode()
    {
        return $this->accountingCode;
    }
}