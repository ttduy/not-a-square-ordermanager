<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolReceiver
 *
 * @ORM\Table(name="email_pool_receiver")
 * @ORM\Entity
 */
class EmailPoolReceiver
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailPool
     *
     * @ORM\Column(name="id_email_pool", type="integer", nullable=true)
     */
    private $idEmailPool;

    /**
     * @var string $idMessage
     *
     * @ORM\Column(name="id_message", type="string", length=255, nullable=true)
     */
    private $idMessage;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailPool
     *
     * @param integer $idEmailPool
     * @return EmailPoolReceiver
     */
    public function setIdEmailPool($idEmailPool)
    {
        $this->idEmailPool = $idEmailPool;
    
        return $this;
    }

    /**
     * Get idEmailPool
     *
     * @return integer 
     */
    public function getIdEmailPool()
    {
        return $this->idEmailPool;
    }

    /**
     * Set idMessage
     *
     * @param string $idMessage
     * @return EmailPoolReceiver
     */
    public function setIdMessage($idMessage)
    {
        $this->idMessage = $idMessage;
    
        return $this;
    }

    /**
     * Get idMessage
     *
     * @return string 
     */
    public function getIdMessage()
    {
        return $this->idMessage;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EmailPoolReceiver
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EmailPoolReceiver
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return EmailPoolReceiver
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return EmailPoolReceiver
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}