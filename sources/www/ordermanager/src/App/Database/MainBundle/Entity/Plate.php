<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Plate
 *
 * @ORM\Table(name="plate")
 * @ORM\Entity
 */
class Plate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $idPlateType
     *
     * @ORM\Column(name="id_plate_type", type="integer", nullable=true)
     */
    private $idPlateType;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $idCurrentPlateRawResult
     *
     * @ORM\Column(name="id_current_plate_raw_result", type="integer", nullable=true)
     */
    private $idCurrentPlateRawResult;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plate
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idPlateType
     *
     * @param integer $idPlateType
     * @return Plate
     */
    public function setIdPlateType($idPlateType)
    {
        $this->idPlateType = $idPlateType;
    
        return $this;
    }

    /**
     * Get idPlateType
     *
     * @return integer 
     */
    public function getIdPlateType()
    {
        return $this->idPlateType;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return Plate
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Plate
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set idCurrentPlateRawResult
     *
     * @param integer $idCurrentPlateRawResult
     * @return Plate
     */
    public function setIdCurrentPlateRawResult($idCurrentPlateRawResult)
    {
        $this->idCurrentPlateRawResult = $idCurrentPlateRawResult;
    
        return $this;
    }

    /**
     * Get idCurrentPlateRawResult
     *
     * @return integer 
     */
    public function getIdCurrentPlateRawResult()
    {
        return $this->idCurrentPlateRawResult;
    }
}