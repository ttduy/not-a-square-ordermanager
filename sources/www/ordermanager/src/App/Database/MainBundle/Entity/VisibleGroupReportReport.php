<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\VisibleGroupReportReport
 *
 * @ORM\Table(name="visible_group_report_report")
 * @ORM\Entity
 */
class VisibleGroupReportReport
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $idVisibleGroupReport
     *
     * @ORM\Column(name="id_visible_group_report", type="string", length=255, nullable=true)
     */
    private $idVisibleGroupReport;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idVisibleGroupReport
     *
     * @param string $idVisibleGroupReport
     * @return VisibleGroupReportReport
     */
    public function setIdVisibleGroupReport($idVisibleGroupReport)
    {
        $this->idVisibleGroupReport = $idVisibleGroupReport;
    
        return $this;
    }

    /**
     * Get idVisibleGroupReport
     *
     * @return string 
     */
    public function getIdVisibleGroupReport()
    {
        return $this->idVisibleGroupReport;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return VisibleGroupReportReport
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }
}