<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity
 */
class Invoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $number
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime $invoiceDate
     *
     * @ORM\Column(name="invoice_date", type="date", nullable=true)
     */
    private $invoiceDate;

    /**
     * @var integer $idFromCompany
     *
     * @ORM\Column(name="id_from_company", type="integer", nullable=true)
     */
    private $idFromCompany;

    /**
     * @var integer $idInvoiceType
     *
     * @ORM\Column(name="id_invoice_type", type="integer", nullable=true)
     */
    private $idInvoiceType;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idMwaGroup
     *
     * @ORM\Column(name="id_mwa_group", type="integer", nullable=true)
     */
    private $idMwaGroup;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var float $postage
     *
     * @ORM\Column(name="postage", type="decimal", nullable=true)
     */
    private $postage;

    /**
     * @var boolean $isTax
     *
     * @ORM\Column(name="is_tax", type="boolean", nullable=true)
     */
    private $isTax;

    /**
     * @var float $taxPercentage
     *
     * @ORM\Column(name="tax_percentage", type="decimal", nullable=true)
     */
    private $taxPercentage;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var float $exchangeRate
     *
     * @ORM\Column(name="exchange_rate", type="decimal", nullable=true)
     */
    private $exchangeRate;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="date", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var string $invoiceFile
     *
     * @ORM\Column(name="invoice_file", type="string", length=500, nullable=true)
     */
    private $invoiceFile;

    /**
     * @var float $subTotalAmount
     *
     * @ORM\Column(name="sub_total_amount", type="decimal", nullable=true)
     */
    private $subTotalAmount;

    /**
     * @var float $taxAmount
     *
     * @ORM\Column(name="tax_amount", type="decimal", nullable=true)
     */
    private $taxAmount;

    /**
     * @var float $totalAmount
     *
     * @ORM\Column(name="total_amount", type="decimal", nullable=true)
     */
    private $totalAmount;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Invoice
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     * @return Invoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;
    
        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime 
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Set idFromCompany
     *
     * @param integer $idFromCompany
     * @return Invoice
     */
    public function setIdFromCompany($idFromCompany)
    {
        $this->idFromCompany = $idFromCompany;
    
        return $this;
    }

    /**
     * Get idFromCompany
     *
     * @return integer 
     */
    public function getIdFromCompany()
    {
        return $this->idFromCompany;
    }

    /**
     * Set idInvoiceType
     *
     * @param integer $idInvoiceType
     * @return Invoice
     */
    public function setIdInvoiceType($idInvoiceType)
    {
        $this->idInvoiceType = $idInvoiceType;
    
        return $this;
    }

    /**
     * Get idInvoiceType
     *
     * @return integer 
     */
    public function getIdInvoiceType()
    {
        return $this->idInvoiceType;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return Invoice
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return Invoice
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idMwaGroup
     *
     * @param integer $idMwaGroup
     * @return Invoice
     */
    public function setIdMwaGroup($idMwaGroup)
    {
        $this->idMwaGroup = $idMwaGroup;
    
        return $this;
    }

    /**
     * Get idMwaGroup
     *
     * @return integer 
     */
    public function getIdMwaGroup()
    {
        return $this->idMwaGroup;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return Invoice
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set postage
     *
     * @param float $postage
     * @return Invoice
     */
    public function setPostage($postage)
    {
        $this->postage = $postage;
    
        return $this;
    }

    /**
     * Get postage
     *
     * @return float 
     */
    public function getPostage()
    {
        return $this->postage;
    }

    /**
     * Set isTax
     *
     * @param boolean $isTax
     * @return Invoice
     */
    public function setIsTax($isTax)
    {
        $this->isTax = $isTax;
    
        return $this;
    }

    /**
     * Get isTax
     *
     * @return boolean 
     */
    public function getIsTax()
    {
        return $this->isTax;
    }

    /**
     * Set taxPercentage
     *
     * @param float $taxPercentage
     * @return Invoice
     */
    public function setTaxPercentage($taxPercentage)
    {
        $this->taxPercentage = $taxPercentage;
    
        return $this;
    }

    /**
     * Get taxPercentage
     *
     * @return float 
     */
    public function getTaxPercentage()
    {
        return $this->taxPercentage;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return Invoice
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     * @return Invoice
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    
        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return Invoice
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return Invoice
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set invoiceFile
     *
     * @param string $invoiceFile
     * @return Invoice
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;
    
        return $this;
    }

    /**
     * Get invoiceFile
     *
     * @return string 
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set subTotalAmount
     *
     * @param float $subTotalAmount
     * @return Invoice
     */
    public function setSubTotalAmount($subTotalAmount)
    {
        $this->subTotalAmount = $subTotalAmount;
    
        return $this;
    }

    /**
     * Get subTotalAmount
     *
     * @return float 
     */
    public function getSubTotalAmount()
    {
        return $this->subTotalAmount;
    }

    /**
     * Set taxAmount
     *
     * @param float $taxAmount
     * @return Invoice
     */
    public function setTaxAmount($taxAmount)
    {
        $this->taxAmount = $taxAmount;
    
        return $this;
    }

    /**
     * Get taxAmount
     *
     * @return float 
     */
    public function getTaxAmount()
    {
        return $this->taxAmount;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     * @return Invoice
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    
        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float 
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }
}