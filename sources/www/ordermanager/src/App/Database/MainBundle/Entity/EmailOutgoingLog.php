<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailOutgoingLog
 *
 * @ORM\Table(name="email_outgoing_log")
 * @ORM\Entity
 */
class EmailOutgoingLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailOutgoing
     *
     * @ORM\Column(name="id_email_outgoing", type="integer", nullable=true)
     */
    private $idEmailOutgoing;

    /**
     * @var \DateTime $logTimestamp
     *
     * @ORM\Column(name="log_timestamp", type="datetime", nullable=true)
     */
    private $logTimestamp;

    /**
     * @var string $logMessage
     *
     * @ORM\Column(name="log_message", type="text", nullable=true)
     */
    private $logMessage;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailOutgoing
     *
     * @param integer $idEmailOutgoing
     * @return EmailOutgoingLog
     */
    public function setIdEmailOutgoing($idEmailOutgoing)
    {
        $this->idEmailOutgoing = $idEmailOutgoing;
    
        return $this;
    }

    /**
     * Get idEmailOutgoing
     *
     * @return integer 
     */
    public function getIdEmailOutgoing()
    {
        return $this->idEmailOutgoing;
    }

    /**
     * Set logTimestamp
     *
     * @param \DateTime $logTimestamp
     * @return EmailOutgoingLog
     */
    public function setLogTimestamp($logTimestamp)
    {
        $this->logTimestamp = $logTimestamp;
    
        return $this;
    }

    /**
     * Get logTimestamp
     *
     * @return \DateTime 
     */
    public function getLogTimestamp()
    {
        return $this->logTimestamp;
    }

    /**
     * Set logMessage
     *
     * @param string $logMessage
     * @return EmailOutgoingLog
     */
    public function setLogMessage($logMessage)
    {
        $this->logMessage = $logMessage;
    
        return $this;
    }

    /**
     * Get logMessage
     *
     * @return string 
     */
    public function getLogMessage()
    {
        return $this->logMessage;
    }
}