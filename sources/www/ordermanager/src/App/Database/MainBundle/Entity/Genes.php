<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Genes
 *
 * @ORM\Table(name="genes")
 * @ORM\Entity
 */
class Genes
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $genename
     *
     * @ORM\Column(name="GeneName", type="string", length=255, nullable=true)
     */
    private $genename;

    /**
     * @var string $scientificname
     *
     * @ORM\Column(name="ScientificName", type="string", length=255, nullable=true)
     */
    private $scientificname;

    /**
     * @var string $rsnumber
     *
     * @ORM\Column(name="RsNumber", type="string", length=255, nullable=true)
     */
    private $rsnumber;

    /**
     * @var integer $groupid
     *
     * @ORM\Column(name="GroupId", type="integer", nullable=true)
     */
    private $groupid;

    /**
     * @var string $analysisresult
     *
     * @ORM\Column(name="AnalysisResult", type="string", length=255, nullable=true)
     */
    private $analysisresult;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var boolean $relevant
     *
     * @ORM\Column(name="Relevant", type="boolean", nullable=false)
     */
    private $relevant;

    /**
     * @var boolean $recheck
     *
     * @ORM\Column(name="Recheck", type="boolean", nullable=false)
     */
    private $recheck;

    /**
     * @var string $wtbase
     *
     * @ORM\Column(name="WTBase", type="string", length=255, nullable=true)
     */
    private $wtbase;

    /**
     * @var string $mutbase
     *
     * @ORM\Column(name="MutBase", type="string", length=255, nullable=true)
     */
    private $mutbase;

    /**
     * @var string $oddsratiowt
     *
     * @ORM\Column(name="OddsRatioWt", type="string", length=255, nullable=true)
     */
    private $oddsratiowt;

    /**
     * @var string $oddsratiohet
     *
     * @ORM\Column(name="OddsRatioHet", type="string", length=255, nullable=true)
     */
    private $oddsratiohet;

    /**
     * @var string $oddsratiomut
     *
     * @ORM\Column(name="OddsRatioMut", type="string", length=255, nullable=true)
     */
    private $oddsratiomut;

    /**
     * @var string $reference1
     *
     * @ORM\Column(name="Reference1", type="text", nullable=true)
     */
    private $reference1;

    /**
     * @var string $reference2
     *
     * @ORM\Column(name="Reference2", type="text", nullable=true)
     */
    private $reference2;

    /**
     * @var string $reference3
     *
     * @ORM\Column(name="Reference3", type="text", nullable=true)
     */
    private $reference3;

    /**
     * @var string $valwt
     *
     * @ORM\Column(name="ValWT", type="string", length=255, nullable=true)
     */
    private $valwt;

    /**
     * @var string $valhet
     *
     * @ORM\Column(name="ValHet", type="string", length=255, nullable=true)
     */
    private $valhet;

    /**
     * @var string $valmut
     *
     * @ORM\Column(name="ValMut", type="string", length=255, nullable=true)
     */
    private $valmut;

    /**
     * @var string $seqwh
     *
     * @ORM\Column(name="Seqwh", type="string", length=255, nullable=true)
     */
    private $seqwh;

    /**
     * @var string $disease
     *
     * @ORM\Column(name="Disease", type="string", length=255, nullable=true)
     */
    private $disease;

    /**
     * @var string $amplicon1
     *
     * @ORM\Column(name="Amplicon1", type="string", length=255, nullable=true)
     */
    private $amplicon1;

    /**
     * @var string $primerfwd1
     *
     * @ORM\Column(name="PrimerFwd1", type="string", length=255, nullable=true)
     */
    private $primerfwd1;

    /**
     * @var string $primerfwdseq1
     *
     * @ORM\Column(name="PrimerFwdSeq1", type="string", length=255, nullable=true)
     */
    private $primerfwdseq1;

    /**
     * @var string $primerrev1
     *
     * @ORM\Column(name="PrimerRev1", type="string", length=255, nullable=true)
     */
    private $primerrev1;

    /**
     * @var string $primerrevseq1
     *
     * @ORM\Column(name="PrimerRevSeq1", type="string", length=255, nullable=true)
     */
    private $primerrevseq1;

    /**
     * @var string $amplicon2
     *
     * @ORM\Column(name="Amplicon2", type="string", length=255, nullable=true)
     */
    private $amplicon2;

    /**
     * @var string $primerfwd2
     *
     * @ORM\Column(name="PrimerFwd2", type="string", length=255, nullable=true)
     */
    private $primerfwd2;

    /**
     * @var string $primerfwdseq2
     *
     * @ORM\Column(name="PrimerFwdSeq2", type="string", length=255, nullable=true)
     */
    private $primerfwdseq2;

    /**
     * @var string $primerrev2
     *
     * @ORM\Column(name="PrimerRev2", type="string", length=255, nullable=true)
     */
    private $primerrev2;

    /**
     * @var string $primerrevseq2
     *
     * @ORM\Column(name="PrimerRevSeq2", type="string", length=255, nullable=true)
     */
    private $primerrevseq2;

    /**
     * @var string $amplicon3
     *
     * @ORM\Column(name="Amplicon3", type="string", length=255, nullable=true)
     */
    private $amplicon3;

    /**
     * @var string $primerfwd3
     *
     * @ORM\Column(name="PrimerFwd3", type="string", length=255, nullable=true)
     */
    private $primerfwd3;

    /**
     * @var string $primerfwdseq3
     *
     * @ORM\Column(name="PrimerFwdSeq3", type="string", length=255, nullable=true)
     */
    private $primerfwdseq3;

    /**
     * @var string $primerrev3
     *
     * @ORM\Column(name="PrimerRev3", type="string", length=255, nullable=true)
     */
    private $primerrev3;

    /**
     * @var string $primerrevseq3
     *
     * @ORM\Column(name="PrimerRevSeq3", type="string", length=255, nullable=true)
     */
    private $primerrevseq3;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set genename
     *
     * @param string $genename
     * @return Genes
     */
    public function setGenename($genename)
    {
        $this->genename = $genename;
    
        return $this;
    }

    /**
     * Get genename
     *
     * @return string 
     */
    public function getGenename()
    {
        return $this->genename;
    }

    /**
     * Set scientificname
     *
     * @param string $scientificname
     * @return Genes
     */
    public function setScientificname($scientificname)
    {
        $this->scientificname = $scientificname;
    
        return $this;
    }

    /**
     * Get scientificname
     *
     * @return string 
     */
    public function getScientificname()
    {
        return $this->scientificname;
    }

    /**
     * Set rsnumber
     *
     * @param string $rsnumber
     * @return Genes
     */
    public function setRsnumber($rsnumber)
    {
        $this->rsnumber = $rsnumber;
    
        return $this;
    }

    /**
     * Get rsnumber
     *
     * @return string 
     */
    public function getRsnumber()
    {
        return $this->rsnumber;
    }

    /**
     * Set groupid
     *
     * @param integer $groupid
     * @return Genes
     */
    public function setGroupid($groupid)
    {
        $this->groupid = $groupid;
    
        return $this;
    }

    /**
     * Get groupid
     *
     * @return integer 
     */
    public function getGroupid()
    {
        return $this->groupid;
    }

    /**
     * Set analysisresult
     *
     * @param string $analysisresult
     * @return Genes
     */
    public function setAnalysisresult($analysisresult)
    {
        $this->analysisresult = $analysisresult;
    
        return $this;
    }

    /**
     * Get analysisresult
     *
     * @return string 
     */
    public function getAnalysisresult()
    {
        return $this->analysisresult;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Genes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set relevant
     *
     * @param boolean $relevant
     * @return Genes
     */
    public function setRelevant($relevant)
    {
        $this->relevant = $relevant;
    
        return $this;
    }

    /**
     * Get relevant
     *
     * @return boolean 
     */
    public function getRelevant()
    {
        return $this->relevant;
    }

    /**
     * Set recheck
     *
     * @param boolean $recheck
     * @return Genes
     */
    public function setRecheck($recheck)
    {
        $this->recheck = $recheck;
    
        return $this;
    }

    /**
     * Get recheck
     *
     * @return boolean 
     */
    public function getRecheck()
    {
        return $this->recheck;
    }

    /**
     * Set wtbase
     *
     * @param string $wtbase
     * @return Genes
     */
    public function setWtbase($wtbase)
    {
        $this->wtbase = $wtbase;
    
        return $this;
    }

    /**
     * Get wtbase
     *
     * @return string 
     */
    public function getWtbase()
    {
        return $this->wtbase;
    }

    /**
     * Set mutbase
     *
     * @param string $mutbase
     * @return Genes
     */
    public function setMutbase($mutbase)
    {
        $this->mutbase = $mutbase;
    
        return $this;
    }

    /**
     * Get mutbase
     *
     * @return string 
     */
    public function getMutbase()
    {
        return $this->mutbase;
    }

    /**
     * Set oddsratiowt
     *
     * @param string $oddsratiowt
     * @return Genes
     */
    public function setOddsratiowt($oddsratiowt)
    {
        $this->oddsratiowt = $oddsratiowt;
    
        return $this;
    }

    /**
     * Get oddsratiowt
     *
     * @return string 
     */
    public function getOddsratiowt()
    {
        return $this->oddsratiowt;
    }

    /**
     * Set oddsratiohet
     *
     * @param string $oddsratiohet
     * @return Genes
     */
    public function setOddsratiohet($oddsratiohet)
    {
        $this->oddsratiohet = $oddsratiohet;
    
        return $this;
    }

    /**
     * Get oddsratiohet
     *
     * @return string 
     */
    public function getOddsratiohet()
    {
        return $this->oddsratiohet;
    }

    /**
     * Set oddsratiomut
     *
     * @param string $oddsratiomut
     * @return Genes
     */
    public function setOddsratiomut($oddsratiomut)
    {
        $this->oddsratiomut = $oddsratiomut;
    
        return $this;
    }

    /**
     * Get oddsratiomut
     *
     * @return string 
     */
    public function getOddsratiomut()
    {
        return $this->oddsratiomut;
    }

    /**
     * Set reference1
     *
     * @param string $reference1
     * @return Genes
     */
    public function setReference1($reference1)
    {
        $this->reference1 = $reference1;
    
        return $this;
    }

    /**
     * Get reference1
     *
     * @return string 
     */
    public function getReference1()
    {
        return $this->reference1;
    }

    /**
     * Set reference2
     *
     * @param string $reference2
     * @return Genes
     */
    public function setReference2($reference2)
    {
        $this->reference2 = $reference2;
    
        return $this;
    }

    /**
     * Get reference2
     *
     * @return string 
     */
    public function getReference2()
    {
        return $this->reference2;
    }

    /**
     * Set reference3
     *
     * @param string $reference3
     * @return Genes
     */
    public function setReference3($reference3)
    {
        $this->reference3 = $reference3;
    
        return $this;
    }

    /**
     * Get reference3
     *
     * @return string 
     */
    public function getReference3()
    {
        return $this->reference3;
    }

    /**
     * Set valwt
     *
     * @param string $valwt
     * @return Genes
     */
    public function setValwt($valwt)
    {
        $this->valwt = $valwt;
    
        return $this;
    }

    /**
     * Get valwt
     *
     * @return string 
     */
    public function getValwt()
    {
        return $this->valwt;
    }

    /**
     * Set valhet
     *
     * @param string $valhet
     * @return Genes
     */
    public function setValhet($valhet)
    {
        $this->valhet = $valhet;
    
        return $this;
    }

    /**
     * Get valhet
     *
     * @return string 
     */
    public function getValhet()
    {
        return $this->valhet;
    }

    /**
     * Set valmut
     *
     * @param string $valmut
     * @return Genes
     */
    public function setValmut($valmut)
    {
        $this->valmut = $valmut;
    
        return $this;
    }

    /**
     * Get valmut
     *
     * @return string 
     */
    public function getValmut()
    {
        return $this->valmut;
    }

    /**
     * Set seqwh
     *
     * @param string $seqwh
     * @return Genes
     */
    public function setSeqwh($seqwh)
    {
        $this->seqwh = $seqwh;
    
        return $this;
    }

    /**
     * Get seqwh
     *
     * @return string 
     */
    public function getSeqwh()
    {
        return $this->seqwh;
    }

    /**
     * Set disease
     *
     * @param string $disease
     * @return Genes
     */
    public function setDisease($disease)
    {
        $this->disease = $disease;
    
        return $this;
    }

    /**
     * Get disease
     *
     * @return string 
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * Set amplicon1
     *
     * @param string $amplicon1
     * @return Genes
     */
    public function setAmplicon1($amplicon1)
    {
        $this->amplicon1 = $amplicon1;
    
        return $this;
    }

    /**
     * Get amplicon1
     *
     * @return string 
     */
    public function getAmplicon1()
    {
        return $this->amplicon1;
    }

    /**
     * Set primerfwd1
     *
     * @param string $primerfwd1
     * @return Genes
     */
    public function setPrimerfwd1($primerfwd1)
    {
        $this->primerfwd1 = $primerfwd1;
    
        return $this;
    }

    /**
     * Get primerfwd1
     *
     * @return string 
     */
    public function getPrimerfwd1()
    {
        return $this->primerfwd1;
    }

    /**
     * Set primerfwdseq1
     *
     * @param string $primerfwdseq1
     * @return Genes
     */
    public function setPrimerfwdseq1($primerfwdseq1)
    {
        $this->primerfwdseq1 = $primerfwdseq1;
    
        return $this;
    }

    /**
     * Get primerfwdseq1
     *
     * @return string 
     */
    public function getPrimerfwdseq1()
    {
        return $this->primerfwdseq1;
    }

    /**
     * Set primerrev1
     *
     * @param string $primerrev1
     * @return Genes
     */
    public function setPrimerrev1($primerrev1)
    {
        $this->primerrev1 = $primerrev1;
    
        return $this;
    }

    /**
     * Get primerrev1
     *
     * @return string 
     */
    public function getPrimerrev1()
    {
        return $this->primerrev1;
    }

    /**
     * Set primerrevseq1
     *
     * @param string $primerrevseq1
     * @return Genes
     */
    public function setPrimerrevseq1($primerrevseq1)
    {
        $this->primerrevseq1 = $primerrevseq1;
    
        return $this;
    }

    /**
     * Get primerrevseq1
     *
     * @return string 
     */
    public function getPrimerrevseq1()
    {
        return $this->primerrevseq1;
    }

    /**
     * Set amplicon2
     *
     * @param string $amplicon2
     * @return Genes
     */
    public function setAmplicon2($amplicon2)
    {
        $this->amplicon2 = $amplicon2;
    
        return $this;
    }

    /**
     * Get amplicon2
     *
     * @return string 
     */
    public function getAmplicon2()
    {
        return $this->amplicon2;
    }

    /**
     * Set primerfwd2
     *
     * @param string $primerfwd2
     * @return Genes
     */
    public function setPrimerfwd2($primerfwd2)
    {
        $this->primerfwd2 = $primerfwd2;
    
        return $this;
    }

    /**
     * Get primerfwd2
     *
     * @return string 
     */
    public function getPrimerfwd2()
    {
        return $this->primerfwd2;
    }

    /**
     * Set primerfwdseq2
     *
     * @param string $primerfwdseq2
     * @return Genes
     */
    public function setPrimerfwdseq2($primerfwdseq2)
    {
        $this->primerfwdseq2 = $primerfwdseq2;
    
        return $this;
    }

    /**
     * Get primerfwdseq2
     *
     * @return string 
     */
    public function getPrimerfwdseq2()
    {
        return $this->primerfwdseq2;
    }

    /**
     * Set primerrev2
     *
     * @param string $primerrev2
     * @return Genes
     */
    public function setPrimerrev2($primerrev2)
    {
        $this->primerrev2 = $primerrev2;
    
        return $this;
    }

    /**
     * Get primerrev2
     *
     * @return string 
     */
    public function getPrimerrev2()
    {
        return $this->primerrev2;
    }

    /**
     * Set primerrevseq2
     *
     * @param string $primerrevseq2
     * @return Genes
     */
    public function setPrimerrevseq2($primerrevseq2)
    {
        $this->primerrevseq2 = $primerrevseq2;
    
        return $this;
    }

    /**
     * Get primerrevseq2
     *
     * @return string 
     */
    public function getPrimerrevseq2()
    {
        return $this->primerrevseq2;
    }

    /**
     * Set amplicon3
     *
     * @param string $amplicon3
     * @return Genes
     */
    public function setAmplicon3($amplicon3)
    {
        $this->amplicon3 = $amplicon3;
    
        return $this;
    }

    /**
     * Get amplicon3
     *
     * @return string 
     */
    public function getAmplicon3()
    {
        return $this->amplicon3;
    }

    /**
     * Set primerfwd3
     *
     * @param string $primerfwd3
     * @return Genes
     */
    public function setPrimerfwd3($primerfwd3)
    {
        $this->primerfwd3 = $primerfwd3;
    
        return $this;
    }

    /**
     * Get primerfwd3
     *
     * @return string 
     */
    public function getPrimerfwd3()
    {
        return $this->primerfwd3;
    }

    /**
     * Set primerfwdseq3
     *
     * @param string $primerfwdseq3
     * @return Genes
     */
    public function setPrimerfwdseq3($primerfwdseq3)
    {
        $this->primerfwdseq3 = $primerfwdseq3;
    
        return $this;
    }

    /**
     * Get primerfwdseq3
     *
     * @return string 
     */
    public function getPrimerfwdseq3()
    {
        return $this->primerfwdseq3;
    }

    /**
     * Set primerrev3
     *
     * @param string $primerrev3
     * @return Genes
     */
    public function setPrimerrev3($primerrev3)
    {
        $this->primerrev3 = $primerrev3;
    
        return $this;
    }

    /**
     * Get primerrev3
     *
     * @return string 
     */
    public function getPrimerrev3()
    {
        return $this->primerrev3;
    }

    /**
     * Set primerrevseq3
     *
     * @param string $primerrevseq3
     * @return Genes
     */
    public function setPrimerrevseq3($primerrevseq3)
    {
        $this->primerrevseq3 = $primerrevseq3;
    
        return $this;
    }

    /**
     * Get primerrevseq3
     *
     * @return string 
     */
    public function getPrimerrevseq3()
    {
        return $this->primerrevseq3;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Genes
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }
}