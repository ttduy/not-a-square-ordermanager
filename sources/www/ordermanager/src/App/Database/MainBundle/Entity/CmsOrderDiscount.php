<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CmsOrderDiscount
 *
 * @ORM\Table(name="cms_order_discount")
 * @ORM\Entity
 */
class CmsOrderDiscount
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCmsOrder
     *
     * @ORM\Column(name="id_cms_order", type="integer", nullable=true)
     */
    private $idCmsOrder;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $discountCode
     *
     * @ORM\Column(name="discount_code", type="string", length=255, nullable=true)
     */
    private $discountCode;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCmsOrder
     *
     * @param integer $idCmsOrder
     * @return CmsOrderDiscount
     */
    public function setIdCmsOrder($idCmsOrder)
    {
        $this->idCmsOrder = $idCmsOrder;
    
        return $this;
    }

    /**
     * Get idCmsOrder
     *
     * @return integer 
     */
    public function getIdCmsOrder()
    {
        return $this->idCmsOrder;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CmsOrderDiscount
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set discountCode
     *
     * @param string $discountCode
     * @return CmsOrderDiscount
     */
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;
    
        return $this;
    }

    /**
     * Get discountCode
     *
     * @return string 
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return CmsOrderDiscount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }
}