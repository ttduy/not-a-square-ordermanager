<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FinanceCostPerMonth
 *
 * @ORM\Table(name="finance_cost_per_month")
 * @ORM\Entity
 */
class FinanceCostPerMonth
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var integer $month
     *
     * @ORM\Column(name="month", type="integer", nullable=true)
     */
    private $month;

    /**
     * @var float $cost
     *
     * @ORM\Column(name="cost", type="decimal", nullable=true)
     */
    private $cost;

    /**
     * @var float $cash
     *
     * @ORM\Column(name="cash", type="decimal", nullable=true)
     */
    private $cash;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return FinanceCostPerMonth
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return FinanceCostPerMonth
     */
    public function setMonth($month)
    {
        $this->month = $month;
    
        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return FinanceCostPerMonth
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set cash
     *
     * @param float $cash
     * @return FinanceCostPerMonth
     */
    public function setCash($cash)
    {
        $this->cash = $cash;
    
        return $this;
    }

    /**
     * Get cash
     *
     * @return float 
     */
    public function getCash()
    {
        return $this->cash;
    }
}