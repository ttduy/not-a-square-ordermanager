<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailTemplate
 *
 * @ORM\Table(name="email_template")
 * @ORM\Entity
 */
class EmailTemplate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="Subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string $content
     *
     * @ORM\Column(name="Content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var boolean $tocustomer
     *
     * @ORM\Column(name="ToCustomer", type="boolean", nullable=true)
     */
    private $tocustomer;

    /**
     * @var boolean $todistributionchannel
     *
     * @ORM\Column(name="ToDistributionChannel", type="boolean", nullable=true)
     */
    private $todistributionchannel;

    /**
     * @var boolean $topartner
     *
     * @ORM\Column(name="ToPartner", type="boolean", nullable=true)
     */
    private $topartner;

    /**
     * @var string $toothers
     *
     * @ORM\Column(name="ToOthers", type="text", nullable=true)
     */
    private $toothers;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=true)
     */
    private $isdeleted;

    /**
     * @var string $contentWithParagraph
     *
     * @ORM\Column(name="content_with_paragraph", type="text", nullable=true)
     */
    private $contentWithParagraph;

    /**
     * @var boolean $isResaveContent
     *
     * @ORM\Column(name="is_resave_content", type="boolean", nullable=true)
     */
    private $isResaveContent;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EmailTemplate
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailTemplate
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set tocustomer
     *
     * @param boolean $tocustomer
     * @return EmailTemplate
     */
    public function setTocustomer($tocustomer)
    {
        $this->tocustomer = $tocustomer;
    
        return $this;
    }

    /**
     * Get tocustomer
     *
     * @return boolean 
     */
    public function getTocustomer()
    {
        return $this->tocustomer;
    }

    /**
     * Set todistributionchannel
     *
     * @param boolean $todistributionchannel
     * @return EmailTemplate
     */
    public function setTodistributionchannel($todistributionchannel)
    {
        $this->todistributionchannel = $todistributionchannel;
    
        return $this;
    }

    /**
     * Get todistributionchannel
     *
     * @return boolean 
     */
    public function getTodistributionchannel()
    {
        return $this->todistributionchannel;
    }

    /**
     * Set topartner
     *
     * @param boolean $topartner
     * @return EmailTemplate
     */
    public function setTopartner($topartner)
    {
        $this->topartner = $topartner;
    
        return $this;
    }

    /**
     * Get topartner
     *
     * @return boolean 
     */
    public function getTopartner()
    {
        return $this->topartner;
    }

    /**
     * Set toothers
     *
     * @param string $toothers
     * @return EmailTemplate
     */
    public function setToothers($toothers)
    {
        $this->toothers = $toothers;
    
        return $this;
    }

    /**
     * Get toothers
     *
     * @return string 
     */
    public function getToothers()
    {
        return $this->toothers;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return EmailTemplate
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set contentWithParagraph
     *
     * @param string $contentWithParagraph
     * @return EmailTemplate
     */
    public function setContentWithParagraph($contentWithParagraph)
    {
        $this->contentWithParagraph = $contentWithParagraph;
    
        return $this;
    }

    /**
     * Get contentWithParagraph
     *
     * @return string 
     */
    public function getContentWithParagraph()
    {
        return $this->contentWithParagraph;
    }

    /**
     * Set isResaveContent
     *
     * @param boolean $isResaveContent
     * @return EmailTemplate
     */
    public function setIsResaveContent($isResaveContent)
    {
        $this->isResaveContent = $isResaveContent;
    
        return $this;
    }

    /**
     * Get isResaveContent
     *
     * @return boolean 
     */
    public function getIsResaveContent()
    {
        return $this->isResaveContent;
    }
}