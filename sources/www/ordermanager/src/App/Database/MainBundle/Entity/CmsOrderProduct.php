<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CmsOrderProduct
 *
 * @ORM\Table(name="cms_order_product")
 * @ORM\Entity
 */
class CmsOrderProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCmsOrder
     *
     * @ORM\Column(name="id_cms_order", type="integer", nullable=true)
     */
    private $idCmsOrder;

    /**
     * @var string $productCode
     *
     * @ORM\Column(name="product_code", type="string", length=255, nullable=true)
     */
    private $productCode;

    /**
     * @var string $extraData
     *
     * @ORM\Column(name="extra_data", type="text", nullable=true)
     */
    private $extraData;

    /**
     * @var float $priceWithoutTax
     *
     * @ORM\Column(name="price_without_tax", type="float", nullable=true)
     */
    private $priceWithoutTax;

    /**
     * @var float $priceWithTax
     *
     * @ORM\Column(name="price_with_tax", type="float", nullable=true)
     */
    private $priceWithTax;

    /**
     * @var string $productKey
     *
     * @ORM\Column(name="product_key", type="string", length=255, nullable=true)
     */
    private $productKey;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCmsOrder
     *
     * @param integer $idCmsOrder
     * @return CmsOrderProduct
     */
    public function setIdCmsOrder($idCmsOrder)
    {
        $this->idCmsOrder = $idCmsOrder;
    
        return $this;
    }

    /**
     * Get idCmsOrder
     *
     * @return integer 
     */
    public function getIdCmsOrder()
    {
        return $this->idCmsOrder;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     * @return CmsOrderProduct
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
    
        return $this;
    }

    /**
     * Get productCode
     *
     * @return string 
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set extraData
     *
     * @param string $extraData
     * @return CmsOrderProduct
     */
    public function setExtraData($extraData)
    {
        $this->extraData = $extraData;
    
        return $this;
    }

    /**
     * Get extraData
     *
     * @return string 
     */
    public function getExtraData()
    {
        return $this->extraData;
    }

    /**
     * Set priceWithoutTax
     *
     * @param float $priceWithoutTax
     * @return CmsOrderProduct
     */
    public function setPriceWithoutTax($priceWithoutTax)
    {
        $this->priceWithoutTax = $priceWithoutTax;
    
        return $this;
    }

    /**
     * Get priceWithoutTax
     *
     * @return float 
     */
    public function getPriceWithoutTax()
    {
        return $this->priceWithoutTax;
    }

    /**
     * Set priceWithTax
     *
     * @param float $priceWithTax
     * @return CmsOrderProduct
     */
    public function setPriceWithTax($priceWithTax)
    {
        $this->priceWithTax = $priceWithTax;
    
        return $this;
    }

    /**
     * Get priceWithTax
     *
     * @return float 
     */
    public function getPriceWithTax()
    {
        return $this->priceWithTax;
    }

    /**
     * Set productKey
     *
     * @param string $productKey
     * @return CmsOrderProduct
     */
    public function setProductKey($productKey)
    {
        $this->productKey = $productKey;
    
        return $this;
    }

    /**
     * Get productKey
     *
     * @return string 
     */
    public function getProductKey()
    {
        return $this->productKey;
    }
}