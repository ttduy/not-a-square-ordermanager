<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TodoTask
 *
 * @ORM\Table(name="todo_task")
 * @ORM\Entity
 */
class TodoTask
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var integer $idCategory
     *
     * @ORM\Column(name="id_category", type="integer", nullable=true)
     */
    private $idCategory;

    /**
     * @var integer $idGoal
     *
     * @ORM\Column(name="id_goal", type="integer", nullable=true)
     */
    private $idGoal;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $color
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;

    /**
     * @var integer $idContinualImprovement
     *
     * @ORM\Column(name="id_continual_improvement", type="integer", nullable=true)
     */
    private $idContinualImprovement;

    /**
     * @var integer $idMeeting
     *
     * @ORM\Column(name="id_meeting", type="integer", nullable=true)
     */
    private $idMeeting;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return TodoTask
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     * @return TodoTask
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    
        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer 
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * Set idGoal
     *
     * @param integer $idGoal
     * @return TodoTask
     */
    public function setIdGoal($idGoal)
    {
        $this->idGoal = $idGoal;
    
        return $this;
    }

    /**
     * Get idGoal
     *
     * @return integer 
     */
    public function getIdGoal()
    {
        return $this->idGoal;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return TodoTask
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TodoTask
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TodoTask
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TodoTask
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return TodoTask
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set idContinualImprovement
     *
     * @param integer $idContinualImprovement
     * @return TodoTask
     */
    public function setIdContinualImprovement($idContinualImprovement)
    {
        $this->idContinualImprovement = $idContinualImprovement;
    
        return $this;
    }

    /**
     * Get idContinualImprovement
     *
     * @return integer 
     */
    public function getIdContinualImprovement()
    {
        return $this->idContinualImprovement;
    }

    /**
     * Set idMeeting
     *
     * @param integer $idMeeting
     * @return TodoTask
     */
    public function setIdMeeting($idMeeting)
    {
        $this->idMeeting = $idMeeting;
    
        return $this;
    }

    /**
     * Get idMeeting
     *
     * @return integer 
     */
    public function getIdMeeting()
    {
        return $this->idMeeting;
    }
}