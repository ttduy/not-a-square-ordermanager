<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Bill
 *
 * @ORM\Table(name="bill")
 * @ORM\Entity
 */
class Bill
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $number
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime $billDate
     *
     * @ORM\Column(name="bill_date", type="date", nullable=true)
     */
    private $billDate;

    /**
     * @var integer $idTarget
     *
     * @ORM\Column(name="id_target", type="integer", nullable=true)
     */
    private $idTarget;

    /**
     * @var integer $idCompany
     *
     * @ORM\Column(name="id_company", type="integer", nullable=true)
     */
    private $idCompany;

    /**
     * @var boolean $isWithTax
     *
     * @ORM\Column(name="is_with_tax", type="boolean", nullable=true)
     */
    private $isWithTax;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var float $billAmount
     *
     * @ORM\Column(name="bill_amount", type="decimal", nullable=true)
     */
    private $billAmount;

    /**
     * @var string $billFilename
     *
     * @ORM\Column(name="bill_filename", type="string", length=500, nullable=true)
     */
    private $billFilename;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var float $exchangeRate
     *
     * @ORM\Column(name="exchange_rate", type="decimal", nullable=true)
     */
    private $exchangeRate;

    /**
     * @var string $textAfterDescription
     *
     * @ORM\Column(name="text_after_description", type="text", nullable=true)
     */
    private $textAfterDescription;

    /**
     * @var string $textFooter
     *
     * @ORM\Column(name="text_footer", type="text", nullable=true)
     */
    private $textFooter;

    /**
     * @var integer $idPreferredPayment
     *
     * @ORM\Column(name="id_preferred_payment", type="integer", nullable=true)
     */
    private $idPreferredPayment;

    /**
     * @var integer $idActiveBillFile
     *
     * @ORM\Column(name="id_active_bill_file", type="integer", nullable=true)
     */
    private $idActiveBillFile;

    /**
     * @var integer $numberReminderCount
     *
     * @ORM\Column(name="number_reminder_count", type="integer", nullable=true)
     */
    private $numberReminderCount;

    /**
     * @var string $extraTextAfterDescription
     *
     * @ORM\Column(name="extra_text_after_description", type="text", nullable=true)
     */
    private $extraTextAfterDescription;

    /**
     * @var string $extraTextFooter
     *
     * @ORM\Column(name="extra_text_footer", type="text", nullable=true)
     */
    private $extraTextFooter;

    /**
     * @var integer $idNoTaxText
     *
     * @ORM\Column(name="id_no_tax_text", type="integer", nullable=true)
     */
    private $idNoTaxText;

    /**
     * @var integer $idMarginPaymentMode
     *
     * @ORM\Column(name="id_margin_payment_mode", type="integer", nullable=true)
     */
    private $idMarginPaymentMode;

    /**
     * @var integer $deliverynote
     *
     * @ORM\Column(name="deliveryNote", type="integer", nullable=true)
     */
    private $deliverynote;

    /**
     * @var string $deliveryemail
     *
     * @ORM\Column(name="deliveryEmail", type="string", length=255, nullable=true)
     */
    private $deliveryemail;

    /**
     * @var string $invoiceAddressStreet
     *
     * @ORM\Column(name="invoice_address_street", type="string", length=255, nullable=true)
     */
    private $invoiceAddressStreet;

    /**
     * @var string $invoiceAddressPostCode
     *
     * @ORM\Column(name="invoice_address_post_code", type="string", length=255, nullable=true)
     */
    private $invoiceAddressPostCode;

    /**
     * @var string $invoiceAddressCity
     *
     * @ORM\Column(name="invoice_address_city", type="string", length=255, nullable=true)
     */
    private $invoiceAddressCity;

    /**
     * @var integer $invoiceAddressIdCountry
     *
     * @ORM\Column(name="invoice_address_id_country", type="integer", nullable=true)
     */
    private $invoiceAddressIdCountry;

    /**
     * @var string $invoiceAddressTelephone
     *
     * @ORM\Column(name="invoice_address_telephone", type="string", length=255, nullable=true)
     */
    private $invoiceAddressTelephone;

    /**
     * @var string $invoiceAddressFax
     *
     * @ORM\Column(name="invoice_address_fax", type="string", length=255, nullable=true)
     */
    private $invoiceAddressFax;

    /**
     * @var string $invoiceAddressUid
     *
     * @ORM\Column(name="invoice_address_uid", type="string", length=255, nullable=true)
     */
    private $invoiceAddressUid;

    /**
     * @var float $commissionPaymentWithTax
     *
     * @ORM\Column(name="commission_payment_with_tax", type="decimal", nullable=true)
     */
    private $commissionPaymentWithTax;

    /**
     * @var boolean $isCancel
     *
     * @ORM\Column(name="is_cancel", type="boolean", nullable=true)
     */
    private $isCancel;

    /**
     * @var boolean $isInvoiceAddress
     *
     * @ORM\Column(name="is_invoice_address", type="boolean", nullable=true)
     */
    private $isInvoiceAddress;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return Bill
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Bill
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Bill
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set billDate
     *
     * @param \DateTime $billDate
     * @return Bill
     */
    public function setBillDate($billDate)
    {
        $this->billDate = $billDate;
    
        return $this;
    }

    /**
     * Get billDate
     *
     * @return \DateTime 
     */
    public function getBillDate()
    {
        return $this->billDate;
    }

    /**
     * Set idTarget
     *
     * @param integer $idTarget
     * @return Bill
     */
    public function setIdTarget($idTarget)
    {
        $this->idTarget = $idTarget;
    
        return $this;
    }

    /**
     * Get idTarget
     *
     * @return integer 
     */
    public function getIdTarget()
    {
        return $this->idTarget;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return Bill
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;
    
        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set isWithTax
     *
     * @param boolean $isWithTax
     * @return Bill
     */
    public function setIsWithTax($isWithTax)
    {
        $this->isWithTax = $isWithTax;
    
        return $this;
    }

    /**
     * Get isWithTax
     *
     * @return boolean 
     */
    public function getIsWithTax()
    {
        return $this->isWithTax;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Bill
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return Bill
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set billAmount
     *
     * @param float $billAmount
     * @return Bill
     */
    public function setBillAmount($billAmount)
    {
        $this->billAmount = $billAmount;
    
        return $this;
    }

    /**
     * Get billAmount
     *
     * @return float 
     */
    public function getBillAmount()
    {
        return $this->billAmount;
    }

    /**
     * Set billFilename
     *
     * @param string $billFilename
     * @return Bill
     */
    public function setBillFilename($billFilename)
    {
        $this->billFilename = $billFilename;
    
        return $this;
    }

    /**
     * Get billFilename
     *
     * @return string 
     */
    public function getBillFilename()
    {
        return $this->billFilename;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return Bill
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     * @return Bill
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    
        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set textAfterDescription
     *
     * @param string $textAfterDescription
     * @return Bill
     */
    public function setTextAfterDescription($textAfterDescription)
    {
        $this->textAfterDescription = $textAfterDescription;
    
        return $this;
    }

    /**
     * Get textAfterDescription
     *
     * @return string 
     */
    public function getTextAfterDescription()
    {
        return $this->textAfterDescription;
    }

    /**
     * Set textFooter
     *
     * @param string $textFooter
     * @return Bill
     */
    public function setTextFooter($textFooter)
    {
        $this->textFooter = $textFooter;
    
        return $this;
    }

    /**
     * Get textFooter
     *
     * @return string 
     */
    public function getTextFooter()
    {
        return $this->textFooter;
    }

    /**
     * Set idPreferredPayment
     *
     * @param integer $idPreferredPayment
     * @return Bill
     */
    public function setIdPreferredPayment($idPreferredPayment)
    {
        $this->idPreferredPayment = $idPreferredPayment;
    
        return $this;
    }

    /**
     * Get idPreferredPayment
     *
     * @return integer 
     */
    public function getIdPreferredPayment()
    {
        return $this->idPreferredPayment;
    }

    /**
     * Set idActiveBillFile
     *
     * @param integer $idActiveBillFile
     * @return Bill
     */
    public function setIdActiveBillFile($idActiveBillFile)
    {
        $this->idActiveBillFile = $idActiveBillFile;
    
        return $this;
    }

    /**
     * Get idActiveBillFile
     *
     * @return integer 
     */
    public function getIdActiveBillFile()
    {
        return $this->idActiveBillFile;
    }

    /**
     * Set numberReminderCount
     *
     * @param integer $numberReminderCount
     * @return Bill
     */
    public function setNumberReminderCount($numberReminderCount)
    {
        $this->numberReminderCount = $numberReminderCount;
    
        return $this;
    }

    /**
     * Get numberReminderCount
     *
     * @return integer 
     */
    public function getNumberReminderCount()
    {
        return $this->numberReminderCount;
    }

    /**
     * Set extraTextAfterDescription
     *
     * @param string $extraTextAfterDescription
     * @return Bill
     */
    public function setExtraTextAfterDescription($extraTextAfterDescription)
    {
        $this->extraTextAfterDescription = $extraTextAfterDescription;
    
        return $this;
    }

    /**
     * Get extraTextAfterDescription
     *
     * @return string 
     */
    public function getExtraTextAfterDescription()
    {
        return $this->extraTextAfterDescription;
    }

    /**
     * Set extraTextFooter
     *
     * @param string $extraTextFooter
     * @return Bill
     */
    public function setExtraTextFooter($extraTextFooter)
    {
        $this->extraTextFooter = $extraTextFooter;
    
        return $this;
    }

    /**
     * Get extraTextFooter
     *
     * @return string 
     */
    public function getExtraTextFooter()
    {
        return $this->extraTextFooter;
    }

    /**
     * Set idNoTaxText
     *
     * @param integer $idNoTaxText
     * @return Bill
     */
    public function setIdNoTaxText($idNoTaxText)
    {
        $this->idNoTaxText = $idNoTaxText;
    
        return $this;
    }

    /**
     * Get idNoTaxText
     *
     * @return integer 
     */
    public function getIdNoTaxText()
    {
        return $this->idNoTaxText;
    }

    /**
     * Set idMarginPaymentMode
     *
     * @param integer $idMarginPaymentMode
     * @return Bill
     */
    public function setIdMarginPaymentMode($idMarginPaymentMode)
    {
        $this->idMarginPaymentMode = $idMarginPaymentMode;
    
        return $this;
    }

    /**
     * Get idMarginPaymentMode
     *
     * @return integer 
     */
    public function getIdMarginPaymentMode()
    {
        return $this->idMarginPaymentMode;
    }

    /**
     * Set deliverynote
     *
     * @param integer $deliverynote
     * @return Bill
     */
    public function setDeliverynote($deliverynote)
    {
        $this->deliverynote = $deliverynote;
    
        return $this;
    }

    /**
     * Get deliverynote
     *
     * @return integer 
     */
    public function getDeliverynote()
    {
        return $this->deliverynote;
    }

    /**
     * Set deliveryemail
     *
     * @param string $deliveryemail
     * @return Bill
     */
    public function setDeliveryemail($deliveryemail)
    {
        $this->deliveryemail = $deliveryemail;
    
        return $this;
    }

    /**
     * Get deliveryemail
     *
     * @return string 
     */
    public function getDeliveryemail()
    {
        return $this->deliveryemail;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     * @return Bill
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;
    
        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string 
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressPostCode
     *
     * @param string $invoiceAddressPostCode
     * @return Bill
     */
    public function setInvoiceAddressPostCode($invoiceAddressPostCode)
    {
        $this->invoiceAddressPostCode = $invoiceAddressPostCode;
    
        return $this;
    }

    /**
     * Get invoiceAddressPostCode
     *
     * @return string 
     */
    public function getInvoiceAddressPostCode()
    {
        return $this->invoiceAddressPostCode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     * @return Bill
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;
    
        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string 
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressIdCountry
     *
     * @param integer $invoiceAddressIdCountry
     * @return Bill
     */
    public function setInvoiceAddressIdCountry($invoiceAddressIdCountry)
    {
        $this->invoiceAddressIdCountry = $invoiceAddressIdCountry;
    
        return $this;
    }

    /**
     * Get invoiceAddressIdCountry
     *
     * @return integer 
     */
    public function getInvoiceAddressIdCountry()
    {
        return $this->invoiceAddressIdCountry;
    }

    /**
     * Set invoiceAddressTelephone
     *
     * @param string $invoiceAddressTelephone
     * @return Bill
     */
    public function setInvoiceAddressTelephone($invoiceAddressTelephone)
    {
        $this->invoiceAddressTelephone = $invoiceAddressTelephone;
    
        return $this;
    }

    /**
     * Get invoiceAddressTelephone
     *
     * @return string 
     */
    public function getInvoiceAddressTelephone()
    {
        return $this->invoiceAddressTelephone;
    }

    /**
     * Set invoiceAddressFax
     *
     * @param string $invoiceAddressFax
     * @return Bill
     */
    public function setInvoiceAddressFax($invoiceAddressFax)
    {
        $this->invoiceAddressFax = $invoiceAddressFax;
    
        return $this;
    }

    /**
     * Get invoiceAddressFax
     *
     * @return string 
     */
    public function getInvoiceAddressFax()
    {
        return $this->invoiceAddressFax;
    }

    /**
     * Set invoiceAddressUid
     *
     * @param string $invoiceAddressUid
     * @return Bill
     */
    public function setInvoiceAddressUid($invoiceAddressUid)
    {
        $this->invoiceAddressUid = $invoiceAddressUid;
    
        return $this;
    }

    /**
     * Get invoiceAddressUid
     *
     * @return string 
     */
    public function getInvoiceAddressUid()
    {
        return $this->invoiceAddressUid;
    }

    /**
     * Set commissionPaymentWithTax
     *
     * @param float $commissionPaymentWithTax
     * @return Bill
     */
    public function setCommissionPaymentWithTax($commissionPaymentWithTax)
    {
        $this->commissionPaymentWithTax = $commissionPaymentWithTax;
    
        return $this;
    }

    /**
     * Get commissionPaymentWithTax
     *
     * @return float 
     */
    public function getCommissionPaymentWithTax()
    {
        return $this->commissionPaymentWithTax;
    }

    /**
     * Set isCancel
     *
     * @param boolean $isCancel
     * @return Bill
     */
    public function setIsCancel($isCancel)
    {
        $this->isCancel = $isCancel;
    
        return $this;
    }

    /**
     * Get isCancel
     *
     * @return boolean 
     */
    public function getIsCancel()
    {
        return $this->isCancel;
    }

    /**
     * Set isInvoiceAddress
     *
     * @param boolean $isInvoiceAddress
     * @return Bill
     */
    public function setIsInvoiceAddress($isInvoiceAddress)
    {
        $this->isInvoiceAddress = $isInvoiceAddress;
    
        return $this;
    }

    /**
     * Get isInvoiceAddress
     *
     * @return boolean 
     */
    public function getIsInvoiceAddress()
    {
        return $this->isInvoiceAddress;
    }
}