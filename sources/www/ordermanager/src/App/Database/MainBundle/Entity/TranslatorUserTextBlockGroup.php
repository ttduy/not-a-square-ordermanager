<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TranslatorUserTextBlockGroup
 *
 * @ORM\Table(name="translator_user_text_block_group")
 * @ORM\Entity
 */
class TranslatorUserTextBlockGroup
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idTranslatorUser
     *
     * @ORM\Column(name="id_translator_user", type="integer", nullable=true)
     */
    private $idTranslatorUser;

    /**
     * @var integer $idTextBlockGroup
     *
     * @ORM\Column(name="id_text_block_group", type="integer", nullable=true)
     */
    private $idTextBlockGroup;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTranslatorUser
     *
     * @param integer $idTranslatorUser
     * @return TranslatorUserTextBlockGroup
     */
    public function setIdTranslatorUser($idTranslatorUser)
    {
        $this->idTranslatorUser = $idTranslatorUser;
    
        return $this;
    }

    /**
     * Get idTranslatorUser
     *
     * @return integer 
     */
    public function getIdTranslatorUser()
    {
        return $this->idTranslatorUser;
    }

    /**
     * Set idTextBlockGroup
     *
     * @param integer $idTextBlockGroup
     * @return TranslatorUserTextBlockGroup
     */
    public function setIdTextBlockGroup($idTextBlockGroup)
    {
        $this->idTextBlockGroup = $idTextBlockGroup;
    
        return $this;
    }

    /**
     * Get idTextBlockGroup
     *
     * @return integer 
     */
    public function getIdTextBlockGroup()
    {
        return $this->idTextBlockGroup;
    }
}