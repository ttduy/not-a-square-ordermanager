<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerDashboardRebrand
 *
 * @ORM\Table(name="customer_dashboard_rebrand")
 * @ORM\Entity
 */
class CustomerDashboardRebrand
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var string $logoUrl
     *
     * @ORM\Column(name="logo_url", type="text", nullable=true)
     */
    private $logoUrl;

    /**
     * @var string $logoUpload
     *
     * @ORM\Column(name="logo_upload", type="text", nullable=true)
     */
    private $logoUpload;

    /**
     * @var string $themeColor1
     *
     * @ORM\Column(name="theme_color_1", type="string", length=255, nullable=true)
     */
    private $themeColor1;

    /**
     * @var string $themeColor2
     *
     * @ORM\Column(name="theme_color_2", type="string", length=255, nullable=true)
     */
    private $themeColor2;

    /**
     * @var string $textColorNormal
     *
     * @ORM\Column(name="text_color_normal", type="string", length=255, nullable=true)
     */
    private $textColorNormal;

    /**
     * @var string $textColorBold
     *
     * @ORM\Column(name="text_color_bold", type="string", length=255, nullable=true)
     */
    private $textColorBold;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return CustomerDashboardRebrand
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return CustomerDashboardRebrand
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return CustomerDashboardRebrand
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    
        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set logoUpload
     *
     * @param string $logoUpload
     * @return CustomerDashboardRebrand
     */
    public function setLogoUpload($logoUpload)
    {
        $this->logoUpload = $logoUpload;
    
        return $this;
    }

    /**
     * Get logoUpload
     *
     * @return string 
     */
    public function getLogoUpload()
    {
        return $this->logoUpload;
    }

    /**
     * Set themeColor1
     *
     * @param string $themeColor1
     * @return CustomerDashboardRebrand
     */
    public function setThemeColor1($themeColor1)
    {
        $this->themeColor1 = $themeColor1;
    
        return $this;
    }

    /**
     * Get themeColor1
     *
     * @return string 
     */
    public function getThemeColor1()
    {
        return $this->themeColor1;
    }

    /**
     * Set themeColor2
     *
     * @param string $themeColor2
     * @return CustomerDashboardRebrand
     */
    public function setThemeColor2($themeColor2)
    {
        $this->themeColor2 = $themeColor2;
    
        return $this;
    }

    /**
     * Get themeColor2
     *
     * @return string 
     */
    public function getThemeColor2()
    {
        return $this->themeColor2;
    }

    /**
     * Set textColorNormal
     *
     * @param string $textColorNormal
     * @return CustomerDashboardRebrand
     */
    public function setTextColorNormal($textColorNormal)
    {
        $this->textColorNormal = $textColorNormal;
    
        return $this;
    }

    /**
     * Get textColorNormal
     *
     * @return string 
     */
    public function getTextColorNormal()
    {
        return $this->textColorNormal;
    }

    /**
     * Set textColorBold
     *
     * @param string $textColorBold
     * @return CustomerDashboardRebrand
     */
    public function setTextColorBold($textColorBold)
    {
        $this->textColorBold = $textColorBold;
    
        return $this;
    }

    /**
     * Get textColorBold
     *
     * @return string 
     */
    public function getTextColorBold()
    {
        return $this->textColorBold;
    }
}