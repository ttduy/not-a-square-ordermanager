<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectiveInvoice
 *
 * @ORM\Table(name="collective_invoice")
 * @ORM\Entity
 */
class CollectiveInvoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $invoicenumber
     *
     * @ORM\Column(name="InvoiceNumber", type="string", length=255, nullable=true)
     */
    private $invoicenumber;

    /**
     * @var \DateTime $invoicedate
     *
     * @ORM\Column(name="InvoiceDate", type="date", nullable=true)
     */
    private $invoicedate;

    /**
     * @var integer $invoicefromcompanyid
     *
     * @ORM\Column(name="InvoiceFromCompanyId", type="integer", nullable=true)
     */
    private $invoicefromcompanyid;

    /**
     * @var boolean $iswithtax
     *
     * @ORM\Column(name="IsWithTax", type="boolean", nullable=true)
     */
    private $iswithtax;

    /**
     * @var integer $destinationid
     *
     * @ORM\Column(name="DestinationId", type="integer", nullable=true)
     */
    private $destinationid;

    /**
     * @var integer $partnerid
     *
     * @ORM\Column(name="PartnerId", type="integer", nullable=true)
     */
    private $partnerid;

    /**
     * @var integer $distributionchannelid
     *
     * @ORM\Column(name="DistributionChannelId", type="integer", nullable=true)
     */
    private $distributionchannelid;

    /**
     * @var float $postage
     *
     * @ORM\Column(name="Postage", type="decimal", nullable=true)
     */
    private $postage;

    /**
     * @var integer $invoicestatus
     *
     * @ORM\Column(name="InvoiceStatus", type="integer", nullable=true)
     */
    private $invoicestatus;

    /**
     * @var integer $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="integer", nullable=true)
     */
    private $isdeleted;

    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    private $taxValue;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var integer $idShow
     *
     * @ORM\Column(name="id_show", type="integer", nullable=true)
     */
    private $idShow;

    /**
     * @var float $invoiceAmount
     *
     * @ORM\Column(name="invoice_amount", type="decimal", nullable=true)
     */
    private $invoiceAmount;

    /**
     * @var string $invoiceFile
     *
     * @ORM\Column(name="invoice_file", type="string", length=255, nullable=true)
     */
    private $invoiceFile;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var float $exchangeRate
     *
     * @ORM\Column(name="exchange_rate", type="decimal", nullable=true)
     */
    private $exchangeRate;

    /**
     * @var string $textAfterDescription
     *
     * @ORM\Column(name="text_after_description", type="text", nullable=true)
     */
    private $textAfterDescription;

    /**
     * @var string $textFooter
     *
     * @ORM\Column(name="text_footer", type="text", nullable=true)
     */
    private $textFooter;

    /**
     * @var integer $idPreferredPayment
     *
     * @ORM\Column(name="id_preferred_payment", type="integer", nullable=true)
     */
    private $idPreferredPayment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CollectiveInvoice
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set invoicenumber
     *
     * @param string $invoicenumber
     * @return CollectiveInvoice
     */
    public function setInvoicenumber($invoicenumber)
    {
        $this->invoicenumber = $invoicenumber;
    
        return $this;
    }

    /**
     * Get invoicenumber
     *
     * @return string 
     */
    public function getInvoicenumber()
    {
        return $this->invoicenumber;
    }

    /**
     * Set invoicedate
     *
     * @param \DateTime $invoicedate
     * @return CollectiveInvoice
     */
    public function setInvoicedate($invoicedate)
    {
        $this->invoicedate = $invoicedate;
    
        return $this;
    }

    /**
     * Get invoicedate
     *
     * @return \DateTime 
     */
    public function getInvoicedate()
    {
        return $this->invoicedate;
    }

    /**
     * Set invoicefromcompanyid
     *
     * @param integer $invoicefromcompanyid
     * @return CollectiveInvoice
     */
    public function setInvoicefromcompanyid($invoicefromcompanyid)
    {
        $this->invoicefromcompanyid = $invoicefromcompanyid;
    
        return $this;
    }

    /**
     * Get invoicefromcompanyid
     *
     * @return integer 
     */
    public function getInvoicefromcompanyid()
    {
        return $this->invoicefromcompanyid;
    }

    /**
     * Set iswithtax
     *
     * @param boolean $iswithtax
     * @return CollectiveInvoice
     */
    public function setIswithtax($iswithtax)
    {
        $this->iswithtax = $iswithtax;
    
        return $this;
    }

    /**
     * Get iswithtax
     *
     * @return boolean 
     */
    public function getIswithtax()
    {
        return $this->iswithtax;
    }

    /**
     * Set destinationid
     *
     * @param integer $destinationid
     * @return CollectiveInvoice
     */
    public function setDestinationid($destinationid)
    {
        $this->destinationid = $destinationid;
    
        return $this;
    }

    /**
     * Get destinationid
     *
     * @return integer 
     */
    public function getDestinationid()
    {
        return $this->destinationid;
    }

    /**
     * Set partnerid
     *
     * @param integer $partnerid
     * @return CollectiveInvoice
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;
    
        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer 
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set distributionchannelid
     *
     * @param integer $distributionchannelid
     * @return CollectiveInvoice
     */
    public function setDistributionchannelid($distributionchannelid)
    {
        $this->distributionchannelid = $distributionchannelid;
    
        return $this;
    }

    /**
     * Get distributionchannelid
     *
     * @return integer 
     */
    public function getDistributionchannelid()
    {
        return $this->distributionchannelid;
    }

    /**
     * Set postage
     *
     * @param float $postage
     * @return CollectiveInvoice
     */
    public function setPostage($postage)
    {
        $this->postage = $postage;
    
        return $this;
    }

    /**
     * Get postage
     *
     * @return float 
     */
    public function getPostage()
    {
        return $this->postage;
    }

    /**
     * Set invoicestatus
     *
     * @param integer $invoicestatus
     * @return CollectiveInvoice
     */
    public function setInvoicestatus($invoicestatus)
    {
        $this->invoicestatus = $invoicestatus;
    
        return $this;
    }

    /**
     * Get invoicestatus
     *
     * @return integer 
     */
    public function getInvoicestatus()
    {
        return $this->invoicestatus;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return CollectiveInvoice
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set taxValue
     *
     * @param float $taxValue
     * @return CollectiveInvoice
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    
        return $this;
    }

    /**
     * Get taxValue
     *
     * @return float 
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return CollectiveInvoice
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set idShow
     *
     * @param integer $idShow
     * @return CollectiveInvoice
     */
    public function setIdShow($idShow)
    {
        $this->idShow = $idShow;
    
        return $this;
    }

    /**
     * Get idShow
     *
     * @return integer 
     */
    public function getIdShow()
    {
        return $this->idShow;
    }

    /**
     * Set invoiceAmount
     *
     * @param float $invoiceAmount
     * @return CollectiveInvoice
     */
    public function setInvoiceAmount($invoiceAmount)
    {
        $this->invoiceAmount = $invoiceAmount;
    
        return $this;
    }

    /**
     * Get invoiceAmount
     *
     * @return float 
     */
    public function getInvoiceAmount()
    {
        return $this->invoiceAmount;
    }

    /**
     * Set invoiceFile
     *
     * @param string $invoiceFile
     * @return CollectiveInvoice
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;
    
        return $this;
    }

    /**
     * Get invoiceFile
     *
     * @return string 
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return CollectiveInvoice
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     * @return CollectiveInvoice
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    
        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set textAfterDescription
     *
     * @param string $textAfterDescription
     * @return CollectiveInvoice
     */
    public function setTextAfterDescription($textAfterDescription)
    {
        $this->textAfterDescription = $textAfterDescription;
    
        return $this;
    }

    /**
     * Get textAfterDescription
     *
     * @return string 
     */
    public function getTextAfterDescription()
    {
        return $this->textAfterDescription;
    }

    /**
     * Set textFooter
     *
     * @param string $textFooter
     * @return CollectiveInvoice
     */
    public function setTextFooter($textFooter)
    {
        $this->textFooter = $textFooter;
    
        return $this;
    }

    /**
     * Get textFooter
     *
     * @return string 
     */
    public function getTextFooter()
    {
        return $this->textFooter;
    }

    /**
     * Set idPreferredPayment
     *
     * @param integer $idPreferredPayment
     * @return CollectiveInvoice
     */
    public function setIdPreferredPayment($idPreferredPayment)
    {
        $this->idPreferredPayment = $idPreferredPayment;
    
        return $this;
    }

    /**
     * Get idPreferredPayment
     *
     * @return integer 
     */
    public function getIdPreferredPayment()
    {
        return $this->idPreferredPayment;
    }
}