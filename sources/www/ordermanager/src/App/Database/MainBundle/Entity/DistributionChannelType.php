<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannelType
 *
 * @ORM\Table(name="distribution_channel_type")
 * @ORM\Entity
 */
class DistributionChannelType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $flagIcon
     *
     * @ORM\Column(name="flag_icon", type="string", length=255, nullable=true)
     */
    private $flagIcon;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DistributionChannelType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set flagIcon
     *
     * @param string $flagIcon
     * @return DistributionChannelType
     */
    public function setFlagIcon($flagIcon)
    {
        $this->flagIcon = $flagIcon;
    
        return $this;
    }

    /**
     * Get flagIcon
     *
     * @return string 
     */
    public function getFlagIcon()
    {
        return $this->flagIcon;
    }
}