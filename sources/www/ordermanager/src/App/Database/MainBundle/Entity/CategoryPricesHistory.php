<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CategoryPricesHistory
 *
 * @ORM\Table(name="category_prices_history")
 * @ORM\Entity
 */
class CategoryPricesHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $categoryid
     *
     * @ORM\Column(name="CategoryId", type="integer", nullable=true)
     */
    private $categoryid;

    /**
     * @var integer $pricecatid
     *
     * @ORM\Column(name="PriceCatId", type="integer", nullable=true)
     */
    private $pricecatid;

    /**
     * @var float $price
     *
     * @ORM\Column(name="Price", type="decimal", nullable=true)
     */
    private $price;

    /**
     * @var float $dcmargin
     *
     * @ORM\Column(name="DCMargin", type="decimal", nullable=true)
     */
    private $dcmargin;

    /**
     * @var float $partmargin
     *
     * @ORM\Column(name="PartMargin", type="decimal", nullable=true)
     */
    private $partmargin;

    /**
     * @var \DateTime $priceTimestamp
     *
     * @ORM\Column(name="price_timestamp", type="datetime", nullable=true)
     */
    private $priceTimestamp;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     * @return CategoryPricesHistory
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;
    
        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set pricecatid
     *
     * @param integer $pricecatid
     * @return CategoryPricesHistory
     */
    public function setPricecatid($pricecatid)
    {
        $this->pricecatid = $pricecatid;
    
        return $this;
    }

    /**
     * Get pricecatid
     *
     * @return integer 
     */
    public function getPricecatid()
    {
        return $this->pricecatid;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return CategoryPricesHistory
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set dcmargin
     *
     * @param float $dcmargin
     * @return CategoryPricesHistory
     */
    public function setDcmargin($dcmargin)
    {
        $this->dcmargin = $dcmargin;
    
        return $this;
    }

    /**
     * Get dcmargin
     *
     * @return float 
     */
    public function getDcmargin()
    {
        return $this->dcmargin;
    }

    /**
     * Set partmargin
     *
     * @param float $partmargin
     * @return CategoryPricesHistory
     */
    public function setPartmargin($partmargin)
    {
        $this->partmargin = $partmargin;
    
        return $this;
    }

    /**
     * Get partmargin
     *
     * @return float 
     */
    public function getPartmargin()
    {
        return $this->partmargin;
    }

    /**
     * Set priceTimestamp
     *
     * @param \DateTime $priceTimestamp
     * @return CategoryPricesHistory
     */
    public function setPriceTimestamp($priceTimestamp)
    {
        $this->priceTimestamp = $priceTimestamp;
    
        return $this;
    }

    /**
     * Get priceTimestamp
     *
     * @return \DateTime 
     */
    public function getPriceTimestamp()
    {
        return $this->priceTimestamp;
    }
}