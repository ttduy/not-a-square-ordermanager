<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerActivity
 *
 * @ORM\Table(name="customer_activity")
 * @ORM\Entity
 */
class CustomerActivity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=false)
     */
    private $customerid;

    /**
     * @var \DateTime $activitydate
     *
     * @ORM\Column(name="ActivityDate", type="date", nullable=true)
     */
    private $activitydate;

    /**
     * @var integer $activitytimeid
     *
     * @ORM\Column(name="ActivityTimeId", type="integer", nullable=true)
     */
    private $activitytimeid;

    /**
     * @var integer $activitytypeid
     *
     * @ORM\Column(name="ActivityTypeId", type="integer", nullable=true)
     */
    private $activitytypeid;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=false)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return CustomerActivity
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set activitydate
     *
     * @param \DateTime $activitydate
     * @return CustomerActivity
     */
    public function setActivitydate($activitydate)
    {
        $this->activitydate = $activitydate;
    
        return $this;
    }

    /**
     * Get activitydate
     *
     * @return \DateTime 
     */
    public function getActivitydate()
    {
        return $this->activitydate;
    }

    /**
     * Set activitytimeid
     *
     * @param integer $activitytimeid
     * @return CustomerActivity
     */
    public function setActivitytimeid($activitytimeid)
    {
        $this->activitytimeid = $activitytimeid;
    
        return $this;
    }

    /**
     * Get activitytimeid
     *
     * @return integer 
     */
    public function getActivitytimeid()
    {
        return $this->activitytimeid;
    }

    /**
     * Set activitytypeid
     *
     * @param integer $activitytypeid
     * @return CustomerActivity
     */
    public function setActivitytypeid($activitytypeid)
    {
        $this->activitytypeid = $activitytypeid;
    
        return $this;
    }

    /**
     * Get activitytypeid
     *
     * @return integer 
     */
    public function getActivitytypeid()
    {
        return $this->activitytypeid;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CustomerActivity
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return CustomerActivity
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}