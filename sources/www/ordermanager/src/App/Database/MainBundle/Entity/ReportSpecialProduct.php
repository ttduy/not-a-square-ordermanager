<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportSpecialProduct
 *
 * @ORM\Table(name="report_special_product")
 * @ORM\Entity
 */
class ReportSpecialProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $idSpecialProduct
     *
     * @ORM\Column(name="id_special_product", type="integer", nullable=true)
     */
    private $idSpecialProduct;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return ReportSpecialProduct
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set idSpecialProduct
     *
     * @param integer $idSpecialProduct
     * @return ReportSpecialProduct
     */
    public function setIdSpecialProduct($idSpecialProduct)
    {
        $this->idSpecialProduct = $idSpecialProduct;
    
        return $this;
    }

    /**
     * Get idSpecialProduct
     *
     * @return integer 
     */
    public function getIdSpecialProduct()
    {
        return $this->idSpecialProduct;
    }
}