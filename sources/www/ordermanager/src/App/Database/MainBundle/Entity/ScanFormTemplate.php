<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormTemplate
 *
 * @ORM\Table(name="scan_form_template")
 * @ORM\Entity
 */
class ScanFormTemplate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $templateFile
     *
     * @ORM\Column(name="template_file", type="string", length=255, nullable=true)
     */
    private $templateFile;

    /**
     * @var integer $idFormulaTemplateCreate
     *
     * @ORM\Column(name="id_formula_template_create", type="integer", nullable=true)
     */
    private $idFormulaTemplateCreate;

    /**
     * @var integer $idFormulaTemplateUpdate
     *
     * @ORM\Column(name="id_formula_template_update", type="integer", nullable=true)
     */
    private $idFormulaTemplateUpdate;

    /**
     * @var integer $isParseExpectedZone
     *
     * @ORM\Column(name="is_parse_expected_zone", type="integer", nullable=true)
     */
    private $isParseExpectedZone;

    /**
     * @var float $pageWidth
     *
     * @ORM\Column(name="page_width", type="float", nullable=true)
     */
    private $pageWidth;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScanFormTemplate
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set templateFile
     *
     * @param string $templateFile
     * @return ScanFormTemplate
     */
    public function setTemplateFile($templateFile)
    {
        $this->templateFile = $templateFile;
    
        return $this;
    }

    /**
     * Get templateFile
     *
     * @return string 
     */
    public function getTemplateFile()
    {
        return $this->templateFile;
    }

    /**
     * Set idFormulaTemplateCreate
     *
     * @param integer $idFormulaTemplateCreate
     * @return ScanFormTemplate
     */
    public function setIdFormulaTemplateCreate($idFormulaTemplateCreate)
    {
        $this->idFormulaTemplateCreate = $idFormulaTemplateCreate;
    
        return $this;
    }

    /**
     * Get idFormulaTemplateCreate
     *
     * @return integer 
     */
    public function getIdFormulaTemplateCreate()
    {
        return $this->idFormulaTemplateCreate;
    }

    /**
     * Set idFormulaTemplateUpdate
     *
     * @param integer $idFormulaTemplateUpdate
     * @return ScanFormTemplate
     */
    public function setIdFormulaTemplateUpdate($idFormulaTemplateUpdate)
    {
        $this->idFormulaTemplateUpdate = $idFormulaTemplateUpdate;
    
        return $this;
    }

    /**
     * Get idFormulaTemplateUpdate
     *
     * @return integer 
     */
    public function getIdFormulaTemplateUpdate()
    {
        return $this->idFormulaTemplateUpdate;
    }

    /**
     * Set isParseExpectedZone
     *
     * @param integer $isParseExpectedZone
     * @return ScanFormTemplate
     */
    public function setIsParseExpectedZone($isParseExpectedZone)
    {
        $this->isParseExpectedZone = $isParseExpectedZone;
    
        return $this;
    }

    /**
     * Get isParseExpectedZone
     *
     * @return integer 
     */
    public function getIsParseExpectedZone()
    {
        return $this->isParseExpectedZone;
    }

    /**
     * Set pageWidth
     *
     * @param float $pageWidth
     * @return ScanFormTemplate
     */
    public function setPageWidth($pageWidth)
    {
        $this->pageWidth = $pageWidth;
    
        return $this;
    }

    /**
     * Get pageWidth
     *
     * @return float 
     */
    public function getPageWidth()
    {
        return $this->pageWidth;
    }
}