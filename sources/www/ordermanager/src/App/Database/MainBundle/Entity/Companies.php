<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Companies
 *
 * @ORM\Table(name="companies")
 * @ORM\Entity
 */
class Companies
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $companyname
     *
     * @ORM\Column(name="CompanyName", type="string", length=255, nullable=true)
     */
    private $companyname;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $countryid
     *
     * @ORM\Column(name="CountryId", type="integer", nullable=true)
     */
    private $countryid;

    /**
     * @var string $vat
     *
     * @ORM\Column(name="VAT", type="string", length=255, nullable=true)
     */
    private $vat;

    /**
     * @var string $email
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $website
     *
     * @ORM\Column(name="Website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string $bank
     *
     * @ORM\Column(name="Bank", type="string", length=255, nullable=true)
     */
    private $bank;

    /**
     * @var string $bankaccount
     *
     * @ORM\Column(name="BankAccount", type="string", length=255, nullable=true)
     */
    private $bankaccount;

    /**
     * @var string $bankcode
     *
     * @ORM\Column(name="BankCode", type="string", length=255, nullable=true)
     */
    private $bankcode;

    /**
     * @var string $bic
     *
     * @ORM\Column(name="BIC", type="string", length=255, nullable=true)
     */
    private $bic;

    /**
     * @var string $iban
     *
     * @ORM\Column(name="IBAN", type="string", length=255, nullable=true)
     */
    private $iban;

    /**
     * @var string $logo
     *
     * @ORM\Column(name="Logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string $traderegister
     *
     * @ORM\Column(name="TradeRegister", type="string", length=255, nullable=true)
     */
    private $traderegister;

    /**
     * @var string $jurisdiction
     *
     * @ORM\Column(name="Jurisdiction", type="string", length=255, nullable=true)
     */
    private $jurisdiction;

    /**
     * @var string $manager
     *
     * @ORM\Column(name="Manager", type="string", length=255, nullable=true)
     */
    private $manager;

    /**
     * @var string $legalform
     *
     * @ORM\Column(name="LegalForm", type="string", length=255, nullable=true)
     */
    private $legalform;

    /**
     * @var string $taxnumber
     *
     * @ORM\Column(name="TaxNumber", type="string", length=255, nullable=true)
     */
    private $taxnumber;

    /**
     * @var string $uidnumber
     *
     * @ORM\Column(name="UidNumber", type="string", length=255, nullable=true)
     */
    private $uidnumber;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var string $billVariables
     *
     * @ORM\Column(name="bill_variables", type="text", nullable=true)
     */
    private $billVariables;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyname
     *
     * @param string $companyname
     * @return Companies
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;
    
        return $this;
    }

    /**
     * Get companyname
     *
     * @return string 
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Companies
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Companies
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Companies
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Companies
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;
    
        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set vat
     *
     * @param string $vat
     * @return Companies
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    
        return $this;
    }

    /**
     * Get vat
     *
     * @return string 
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Companies
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Companies
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Companies
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    
        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set bank
     *
     * @param string $bank
     * @return Companies
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    
        return $this;
    }

    /**
     * Get bank
     *
     * @return string 
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set bankaccount
     *
     * @param string $bankaccount
     * @return Companies
     */
    public function setBankaccount($bankaccount)
    {
        $this->bankaccount = $bankaccount;
    
        return $this;
    }

    /**
     * Get bankaccount
     *
     * @return string 
     */
    public function getBankaccount()
    {
        return $this->bankaccount;
    }

    /**
     * Set bankcode
     *
     * @param string $bankcode
     * @return Companies
     */
    public function setBankcode($bankcode)
    {
        $this->bankcode = $bankcode;
    
        return $this;
    }

    /**
     * Get bankcode
     *
     * @return string 
     */
    public function getBankcode()
    {
        return $this->bankcode;
    }

    /**
     * Set bic
     *
     * @param string $bic
     * @return Companies
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    
        return $this;
    }

    /**
     * Get bic
     *
     * @return string 
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return Companies
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    
        return $this;
    }

    /**
     * Get iban
     *
     * @return string 
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Companies
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set traderegister
     *
     * @param string $traderegister
     * @return Companies
     */
    public function setTraderegister($traderegister)
    {
        $this->traderegister = $traderegister;
    
        return $this;
    }

    /**
     * Get traderegister
     *
     * @return string 
     */
    public function getTraderegister()
    {
        return $this->traderegister;
    }

    /**
     * Set jurisdiction
     *
     * @param string $jurisdiction
     * @return Companies
     */
    public function setJurisdiction($jurisdiction)
    {
        $this->jurisdiction = $jurisdiction;
    
        return $this;
    }

    /**
     * Get jurisdiction
     *
     * @return string 
     */
    public function getJurisdiction()
    {
        return $this->jurisdiction;
    }

    /**
     * Set manager
     *
     * @param string $manager
     * @return Companies
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    
        return $this;
    }

    /**
     * Get manager
     *
     * @return string 
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set legalform
     *
     * @param string $legalform
     * @return Companies
     */
    public function setLegalform($legalform)
    {
        $this->legalform = $legalform;
    
        return $this;
    }

    /**
     * Get legalform
     *
     * @return string 
     */
    public function getLegalform()
    {
        return $this->legalform;
    }

    /**
     * Set taxnumber
     *
     * @param string $taxnumber
     * @return Companies
     */
    public function setTaxnumber($taxnumber)
    {
        $this->taxnumber = $taxnumber;
    
        return $this;
    }

    /**
     * Get taxnumber
     *
     * @return string 
     */
    public function getTaxnumber()
    {
        return $this->taxnumber;
    }

    /**
     * Set uidnumber
     *
     * @param string $uidnumber
     * @return Companies
     */
    public function setUidnumber($uidnumber)
    {
        $this->uidnumber = $uidnumber;
    
        return $this;
    }

    /**
     * Get uidnumber
     *
     * @return string 
     */
    public function getUidnumber()
    {
        return $this->uidnumber;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Companies
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set billVariables
     *
     * @param string $billVariables
     * @return Companies
     */
    public function setBillVariables($billVariables)
    {
        $this->billVariables = $billVariables;
    
        return $this;
    }

    /**
     * Get billVariables
     *
     * @return string 
     */
    public function getBillVariables()
    {
        return $this->billVariables;
    }
}