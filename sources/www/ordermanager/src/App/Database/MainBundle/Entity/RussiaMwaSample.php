<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\RussiaMwaSample
 *
 * @ORM\Table(name="russia_mwa_sample")
 * @ORM\Entity
 */
class RussiaMwaSample
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $sampleId
     *
     * @ORM\Column(name="sample_id", type="string", length=255, nullable=true)
     */
    private $sampleId;

    /**
     * @var string $sampleGroup
     *
     * @ORM\Column(name="sample_group", type="string", length=255, nullable=true)
     */
    private $sampleGroup;

    /**
     * @var integer $age
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string $resultPattern
     *
     * @ORM\Column(name="result_pattern", type="string", length=255, nullable=true)
     */
    private $resultPattern;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var boolean $isDeleted
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var boolean $isSendNow
     *
     * @ORM\Column(name="is_send_now", type="boolean", nullable=true)
     */
    private $isSendNow;

    /**
     * @var integer $sendingStatus
     *
     * @ORM\Column(name="sending_status", type="integer", nullable=true)
     */
    private $sendingStatus;

    /**
     * @var integer $pushStatus
     *
     * @ORM\Column(name="push_status", type="integer", nullable=true)
     */
    private $pushStatus;

    /**
     * @var \DateTime $pushDate
     *
     * @ORM\Column(name="push_date", type="date", nullable=true)
     */
    private $pushDate;

    /**
     * @var string $lastError
     *
     * @ORM\Column(name="last_error", type="text", nullable=true)
     */
    private $lastError;

    /**
     * @var \DateTime $pushDatetime
     *
     * @ORM\Column(name="push_datetime", type="datetime", nullable=true)
     */
    private $pushDatetime;

    /**
     * @var integer $idMwaInvoice
     *
     * @ORM\Column(name="id_mwa_invoice", type="integer", nullable=true)
     */
    private $idMwaInvoice;

    /**
     * @var float $priceOverride
     *
     * @ORM\Column(name="price_override", type="decimal", nullable=true)
     */
    private $priceOverride;

    /**
     * @var boolean $isOverrideSending
     *
     * @ORM\Column(name="is_override_sending", type="boolean", nullable=true)
     */
    private $isOverrideSending;

    /**
     * @var integer $numFailed
     *
     * @ORM\Column(name="num_failed", type="integer", nullable=true)
     */
    private $numFailed;

    /**
     * @var \DateTime $registeredDate
     *
     * @ORM\Column(name="registered_date", type="date", nullable=true)
     */
    private $registeredDate;

    /**
     * @var \DateTime $finishedDate
     *
     * @ORM\Column(name="finished_date", type="date", nullable=true)
     */
    private $finishedDate;

    /**
     * @var string $infoText
     *
     * @ORM\Column(name="info_text", type="text", nullable=true)
     */
    private $infoText;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleId
     *
     * @param string $sampleId
     * @return RussiaMwaSample
     */
    public function setSampleId($sampleId)
    {
        $this->sampleId = $sampleId;
    
        return $this;
    }

    /**
     * Get sampleId
     *
     * @return string 
     */
    public function getSampleId()
    {
        return $this->sampleId;
    }

    /**
     * Set sampleGroup
     *
     * @param string $sampleGroup
     * @return RussiaMwaSample
     */
    public function setSampleGroup($sampleGroup)
    {
        $this->sampleGroup = $sampleGroup;
    
        return $this;
    }

    /**
     * Get sampleGroup
     *
     * @return string 
     */
    public function getSampleGroup()
    {
        return $this->sampleGroup;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return RussiaMwaSample
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set resultPattern
     *
     * @param string $resultPattern
     * @return RussiaMwaSample
     */
    public function setResultPattern($resultPattern)
    {
        $this->resultPattern = $resultPattern;
    
        return $this;
    }

    /**
     * Get resultPattern
     *
     * @return string 
     */
    public function getResultPattern()
    {
        return $this->resultPattern;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return RussiaMwaSample
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return RussiaMwaSample
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isSendNow
     *
     * @param boolean $isSendNow
     * @return RussiaMwaSample
     */
    public function setIsSendNow($isSendNow)
    {
        $this->isSendNow = $isSendNow;
    
        return $this;
    }

    /**
     * Get isSendNow
     *
     * @return boolean 
     */
    public function getIsSendNow()
    {
        return $this->isSendNow;
    }

    /**
     * Set sendingStatus
     *
     * @param integer $sendingStatus
     * @return RussiaMwaSample
     */
    public function setSendingStatus($sendingStatus)
    {
        $this->sendingStatus = $sendingStatus;
    
        return $this;
    }

    /**
     * Get sendingStatus
     *
     * @return integer 
     */
    public function getSendingStatus()
    {
        return $this->sendingStatus;
    }

    /**
     * Set pushStatus
     *
     * @param integer $pushStatus
     * @return RussiaMwaSample
     */
    public function setPushStatus($pushStatus)
    {
        $this->pushStatus = $pushStatus;
    
        return $this;
    }

    /**
     * Get pushStatus
     *
     * @return integer 
     */
    public function getPushStatus()
    {
        return $this->pushStatus;
    }

    /**
     * Set pushDate
     *
     * @param \DateTime $pushDate
     * @return RussiaMwaSample
     */
    public function setPushDate($pushDate)
    {
        $this->pushDate = $pushDate;
    
        return $this;
    }

    /**
     * Get pushDate
     *
     * @return \DateTime 
     */
    public function getPushDate()
    {
        return $this->pushDate;
    }

    /**
     * Set lastError
     *
     * @param string $lastError
     * @return RussiaMwaSample
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    
        return $this;
    }

    /**
     * Get lastError
     *
     * @return string 
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Set pushDatetime
     *
     * @param \DateTime $pushDatetime
     * @return RussiaMwaSample
     */
    public function setPushDatetime($pushDatetime)
    {
        $this->pushDatetime = $pushDatetime;
    
        return $this;
    }

    /**
     * Get pushDatetime
     *
     * @return \DateTime 
     */
    public function getPushDatetime()
    {
        return $this->pushDatetime;
    }

    /**
     * Set idMwaInvoice
     *
     * @param integer $idMwaInvoice
     * @return RussiaMwaSample
     */
    public function setIdMwaInvoice($idMwaInvoice)
    {
        $this->idMwaInvoice = $idMwaInvoice;
    
        return $this;
    }

    /**
     * Get idMwaInvoice
     *
     * @return integer 
     */
    public function getIdMwaInvoice()
    {
        return $this->idMwaInvoice;
    }

    /**
     * Set priceOverride
     *
     * @param float $priceOverride
     * @return RussiaMwaSample
     */
    public function setPriceOverride($priceOverride)
    {
        $this->priceOverride = $priceOverride;
    
        return $this;
    }

    /**
     * Get priceOverride
     *
     * @return float 
     */
    public function getPriceOverride()
    {
        return $this->priceOverride;
    }

    /**
     * Set isOverrideSending
     *
     * @param boolean $isOverrideSending
     * @return RussiaMwaSample
     */
    public function setIsOverrideSending($isOverrideSending)
    {
        $this->isOverrideSending = $isOverrideSending;
    
        return $this;
    }

    /**
     * Get isOverrideSending
     *
     * @return boolean 
     */
    public function getIsOverrideSending()
    {
        return $this->isOverrideSending;
    }

    /**
     * Set numFailed
     *
     * @param integer $numFailed
     * @return RussiaMwaSample
     */
    public function setNumFailed($numFailed)
    {
        $this->numFailed = $numFailed;
    
        return $this;
    }

    /**
     * Get numFailed
     *
     * @return integer 
     */
    public function getNumFailed()
    {
        return $this->numFailed;
    }

    /**
     * Set registeredDate
     *
     * @param \DateTime $registeredDate
     * @return RussiaMwaSample
     */
    public function setRegisteredDate($registeredDate)
    {
        $this->registeredDate = $registeredDate;
    
        return $this;
    }

    /**
     * Get registeredDate
     *
     * @return \DateTime 
     */
    public function getRegisteredDate()
    {
        return $this->registeredDate;
    }

    /**
     * Set finishedDate
     *
     * @param \DateTime $finishedDate
     * @return RussiaMwaSample
     */
    public function setFinishedDate($finishedDate)
    {
        $this->finishedDate = $finishedDate;
    
        return $this;
    }

    /**
     * Get finishedDate
     *
     * @return \DateTime 
     */
    public function getFinishedDate()
    {
        return $this->finishedDate;
    }

    /**
     * Set infoText
     *
     * @param string $infoText
     * @return RussiaMwaSample
     */
    public function setInfoText($infoText)
    {
        $this->infoText = $infoText;
    
        return $this;
    }

    /**
     * Get infoText
     *
     * @return string 
     */
    public function getInfoText()
    {
        return $this->infoText;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return RussiaMwaSample
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }
}