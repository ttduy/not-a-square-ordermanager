<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormField
 *
 * @ORM\Table(name="scan_form_field")
 * @ORM\Entity
 */
class ScanFormField
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanForm
     *
     * @ORM\Column(name="id_scan_form", type="integer", nullable=true)
     */
    private $idScanForm;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $idFieldType
     *
     * @ORM\Column(name="id_field_type", type="string", length=255, nullable=true)
     */
    private $idFieldType;

    /**
     * @var integer $zoneX
     *
     * @ORM\Column(name="zone_x", type="integer", nullable=true)
     */
    private $zoneX;

    /**
     * @var integer $zoneY
     *
     * @ORM\Column(name="zone_y", type="integer", nullable=true)
     */
    private $zoneY;

    /**
     * @var integer $zoneWidth
     *
     * @ORM\Column(name="zone_width", type="integer", nullable=true)
     */
    private $zoneWidth;

    /**
     * @var integer $zoneHeight
     *
     * @ORM\Column(name="zone_height", type="integer", nullable=true)
     */
    private $zoneHeight;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanForm
     *
     * @param integer $idScanForm
     * @return ScanFormField
     */
    public function setIdScanForm($idScanForm)
    {
        $this->idScanForm = $idScanForm;
    
        return $this;
    }

    /**
     * Get idScanForm
     *
     * @return integer 
     */
    public function getIdScanForm()
    {
        return $this->idScanForm;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScanFormField
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idFieldType
     *
     * @param string $idFieldType
     * @return ScanFormField
     */
    public function setIdFieldType($idFieldType)
    {
        $this->idFieldType = $idFieldType;
    
        return $this;
    }

    /**
     * Get idFieldType
     *
     * @return string 
     */
    public function getIdFieldType()
    {
        return $this->idFieldType;
    }

    /**
     * Set zoneX
     *
     * @param integer $zoneX
     * @return ScanFormField
     */
    public function setZoneX($zoneX)
    {
        $this->zoneX = $zoneX;
    
        return $this;
    }

    /**
     * Get zoneX
     *
     * @return integer 
     */
    public function getZoneX()
    {
        return $this->zoneX;
    }

    /**
     * Set zoneY
     *
     * @param integer $zoneY
     * @return ScanFormField
     */
    public function setZoneY($zoneY)
    {
        $this->zoneY = $zoneY;
    
        return $this;
    }

    /**
     * Get zoneY
     *
     * @return integer 
     */
    public function getZoneY()
    {
        return $this->zoneY;
    }

    /**
     * Set zoneWidth
     *
     * @param integer $zoneWidth
     * @return ScanFormField
     */
    public function setZoneWidth($zoneWidth)
    {
        $this->zoneWidth = $zoneWidth;
    
        return $this;
    }

    /**
     * Get zoneWidth
     *
     * @return integer 
     */
    public function getZoneWidth()
    {
        return $this->zoneWidth;
    }

    /**
     * Set zoneHeight
     *
     * @param integer $zoneHeight
     * @return ScanFormField
     */
    public function setZoneHeight($zoneHeight)
    {
        $this->zoneHeight = $zoneHeight;
    
        return $this;
    }

    /**
     * Get zoneHeight
     *
     * @return integer 
     */
    public function getZoneHeight()
    {
        return $this->zoneHeight;
    }
}