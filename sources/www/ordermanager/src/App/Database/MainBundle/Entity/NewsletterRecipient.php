<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\NewsletterRecipient
 *
 * @ORM\Table(name="newsletter_recipient")
 * @ORM\Entity
 */
class NewsletterRecipient
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $idCountry
     *
     * @ORM\Column(name="id_country", type="integer", nullable=true)
     */
    private $idCountry;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $surName
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var \DateTime $lastOrder
     *
     * @ORM\Column(name="last_order", type="datetime", nullable=true)
     */
    private $lastOrder;

    /**
     * @var boolean $isNotContacted
     *
     * @ORM\Column(name="is_not_contacted", type="boolean", nullable=true)
     */
    private $isNotContacted;

    /**
     * @var boolean $isCustomerBeContacted
     *
     * @ORM\Column(name="is_customer_be_contacted", type="boolean", nullable=true)
     */
    private $isCustomerBeContacted;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return NewsletterRecipient
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return NewsletterRecipient
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idCountry
     *
     * @param integer $idCountry
     * @return NewsletterRecipient
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;
    
        return $this;
    }

    /**
     * Get idCountry
     *
     * @return integer 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return NewsletterRecipient
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return NewsletterRecipient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surName
     *
     * @param string $surName
     * @return NewsletterRecipient
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;
    
        return $this;
    }

    /**
     * Get surName
     *
     * @return string 
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return NewsletterRecipient
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastOrder
     *
     * @param \DateTime $lastOrder
     * @return NewsletterRecipient
     */
    public function setLastOrder($lastOrder)
    {
        $this->lastOrder = $lastOrder;
    
        return $this;
    }

    /**
     * Get lastOrder
     *
     * @return \DateTime 
     */
    public function getLastOrder()
    {
        return $this->lastOrder;
    }

    /**
     * Set isNotContacted
     *
     * @param boolean $isNotContacted
     * @return NewsletterRecipient
     */
    public function setIsNotContacted($isNotContacted)
    {
        $this->isNotContacted = $isNotContacted;
    
        return $this;
    }

    /**
     * Get isNotContacted
     *
     * @return boolean 
     */
    public function getIsNotContacted()
    {
        return $this->isNotContacted;
    }

    /**
     * Set isCustomerBeContacted
     *
     * @param boolean $isCustomerBeContacted
     * @return NewsletterRecipient
     */
    public function setIsCustomerBeContacted($isCustomerBeContacted)
    {
        $this->isCustomerBeContacted = $isCustomerBeContacted;
    
        return $this;
    }

    /**
     * Get isCustomerBeContacted
     *
     * @return boolean 
     */
    public function getIsCustomerBeContacted()
    {
        return $this->isCustomerBeContacted;
    }
}