<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BackgroundJob
 *
 * @ORM\Table(name="background_job")
 * @ORM\Entity
 */
class BackgroundJob
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var integer $idJobType
     *
     * @ORM\Column(name="id_job_type", type="integer", nullable=true)
     */
    private $idJobType;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string $reportEmail
     *
     * @ORM\Column(name="report_email", type="text", nullable=true)
     */
    private $reportEmail;

    /**
     * @var boolean $isProcessing
     *
     * @ORM\Column(name="is_processing", type="boolean", nullable=true)
     */
    private $isProcessing;

    /**
     * @var boolean $isDone
     *
     * @ORM\Column(name="is_done", type="boolean", nullable=true)
     */
    private $isDone;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return BackgroundJob
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set idJobType
     *
     * @param integer $idJobType
     * @return BackgroundJob
     */
    public function setIdJobType($idJobType)
    {
        $this->idJobType = $idJobType;
    
        return $this;
    }

    /**
     * Get idJobType
     *
     * @return integer 
     */
    public function getIdJobType()
    {
        return $this->idJobType;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return BackgroundJob
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set reportEmail
     *
     * @param string $reportEmail
     * @return BackgroundJob
     */
    public function setReportEmail($reportEmail)
    {
        $this->reportEmail = $reportEmail;
    
        return $this;
    }

    /**
     * Get reportEmail
     *
     * @return string 
     */
    public function getReportEmail()
    {
        return $this->reportEmail;
    }

    /**
     * Set isProcessing
     *
     * @param boolean $isProcessing
     * @return BackgroundJob
     */
    public function setIsProcessing($isProcessing)
    {
        $this->isProcessing = $isProcessing;
    
        return $this;
    }

    /**
     * Get isProcessing
     *
     * @return boolean 
     */
    public function getIsProcessing()
    {
        return $this->isProcessing;
    }

    /**
     * Set isDone
     *
     * @param boolean $isDone
     * @return BackgroundJob
     */
    public function setIsDone($isDone)
    {
        $this->isDone = $isDone;
    
        return $this;
    }

    /**
     * Get isDone
     *
     * @return boolean 
     */
    public function getIsDone()
    {
        return $this->isDone;
    }
}