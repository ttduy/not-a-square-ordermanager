<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Prices
 *
 * @ORM\Table(name="prices")
 * @ORM\Entity
 */
class Prices
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $productid
     *
     * @ORM\Column(name="ProductId", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var integer $pricecatid
     *
     * @ORM\Column(name="PriceCatId", type="integer", nullable=true)
     */
    private $pricecatid;

    /**
     * @var float $price
     *
     * @ORM\Column(name="Price", type="decimal", nullable=true)
     */
    private $price;

    /**
     * @var float $dcmargin
     *
     * @ORM\Column(name="DCMargin", type="decimal", nullable=true)
     */
    private $dcmargin;

    /**
     * @var float $partmargin
     *
     * @ORM\Column(name="PartMargin", type="decimal", nullable=true)
     */
    private $partmargin;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return Prices
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    
        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set pricecatid
     *
     * @param integer $pricecatid
     * @return Prices
     */
    public function setPricecatid($pricecatid)
    {
        $this->pricecatid = $pricecatid;
    
        return $this;
    }

    /**
     * Get pricecatid
     *
     * @return integer 
     */
    public function getPricecatid()
    {
        return $this->pricecatid;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Prices
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set dcmargin
     *
     * @param float $dcmargin
     * @return Prices
     */
    public function setDcmargin($dcmargin)
    {
        $this->dcmargin = $dcmargin;
    
        return $this;
    }

    /**
     * Get dcmargin
     *
     * @return float 
     */
    public function getDcmargin()
    {
        return $this->dcmargin;
    }

    /**
     * Set partmargin
     *
     * @param float $partmargin
     * @return Prices
     */
    public function setPartmargin($partmargin)
    {
        $this->partmargin = $partmargin;
    
        return $this;
    }

    /**
     * Get partmargin
     *
     * @return float 
     */
    public function getPartmargin()
    {
        return $this->partmargin;
    }
}