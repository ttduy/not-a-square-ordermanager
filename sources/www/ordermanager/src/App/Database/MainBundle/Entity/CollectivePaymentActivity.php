<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectivePaymentActivity
 *
 * @ORM\Table(name="collective_payment_activity")
 * @ORM\Entity
 */
class CollectivePaymentActivity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $collectivepaymentid
     *
     * @ORM\Column(name="CollectivePaymentId", type="integer", nullable=false)
     */
    private $collectivepaymentid;

    /**
     * @var \DateTime $activitydate
     *
     * @ORM\Column(name="ActivityDate", type="date", nullable=true)
     */
    private $activitydate;

    /**
     * @var integer $activitytimeid
     *
     * @ORM\Column(name="ActivityTimeId", type="integer", nullable=true)
     */
    private $activitytimeid;

    /**
     * @var integer $activitytypeid
     *
     * @ORM\Column(name="ActivityTypeId", type="integer", nullable=true)
     */
    private $activitytypeid;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=false)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectivepaymentid
     *
     * @param integer $collectivepaymentid
     * @return CollectivePaymentActivity
     */
    public function setCollectivepaymentid($collectivepaymentid)
    {
        $this->collectivepaymentid = $collectivepaymentid;
    
        return $this;
    }

    /**
     * Get collectivepaymentid
     *
     * @return integer 
     */
    public function getCollectivepaymentid()
    {
        return $this->collectivepaymentid;
    }

    /**
     * Set activitydate
     *
     * @param \DateTime $activitydate
     * @return CollectivePaymentActivity
     */
    public function setActivitydate($activitydate)
    {
        $this->activitydate = $activitydate;
    
        return $this;
    }

    /**
     * Get activitydate
     *
     * @return \DateTime 
     */
    public function getActivitydate()
    {
        return $this->activitydate;
    }

    /**
     * Set activitytimeid
     *
     * @param integer $activitytimeid
     * @return CollectivePaymentActivity
     */
    public function setActivitytimeid($activitytimeid)
    {
        $this->activitytimeid = $activitytimeid;
    
        return $this;
    }

    /**
     * Get activitytimeid
     *
     * @return integer 
     */
    public function getActivitytimeid()
    {
        return $this->activitytimeid;
    }

    /**
     * Set activitytypeid
     *
     * @param integer $activitytypeid
     * @return CollectivePaymentActivity
     */
    public function setActivitytypeid($activitytypeid)
    {
        $this->activitytypeid = $activitytypeid;
    
        return $this;
    }

    /**
     * Get activitytypeid
     *
     * @return integer 
     */
    public function getActivitytypeid()
    {
        return $this->activitytypeid;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CollectivePaymentActivity
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return CollectivePaymentActivity
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}