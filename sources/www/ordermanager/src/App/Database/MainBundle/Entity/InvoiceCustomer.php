<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\InvoiceCustomer
 *
 * @ORM\Table(name="invoice_customer")
 * @ORM\Entity
 */
class InvoiceCustomer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idInvoice
     *
     * @ORM\Column(name="id_invoice", type="integer", nullable=true)
     */
    private $idInvoice;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var \DateTime $priceDate
     *
     * @ORM\Column(name="price_date", type="date", nullable=true)
     */
    private $priceDate;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float $overridePrice
     *
     * @ORM\Column(name="override_price", type="float", nullable=true)
     */
    private $overridePrice;

    /**
     * @var string $priceDetail
     *
     * @ORM\Column(name="price_detail", type="text", nullable=true)
     */
    private $priceDetail;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInvoice
     *
     * @param integer $idInvoice
     * @return InvoiceCustomer
     */
    public function setIdInvoice($idInvoice)
    {
        $this->idInvoice = $idInvoice;
    
        return $this;
    }

    /**
     * Get idInvoice
     *
     * @return integer 
     */
    public function getIdInvoice()
    {
        return $this->idInvoice;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return InvoiceCustomer
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set priceDate
     *
     * @param \DateTime $priceDate
     * @return InvoiceCustomer
     */
    public function setPriceDate($priceDate)
    {
        $this->priceDate = $priceDate;
    
        return $this;
    }

    /**
     * Get priceDate
     *
     * @return \DateTime 
     */
    public function getPriceDate()
    {
        return $this->priceDate;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return InvoiceCustomer
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set overridePrice
     *
     * @param float $overridePrice
     * @return InvoiceCustomer
     */
    public function setOverridePrice($overridePrice)
    {
        $this->overridePrice = $overridePrice;
    
        return $this;
    }

    /**
     * Get overridePrice
     *
     * @return float 
     */
    public function getOverridePrice()
    {
        return $this->overridePrice;
    }

    /**
     * Set priceDetail
     *
     * @param string $priceDetail
     * @return InvoiceCustomer
     */
    public function setPriceDetail($priceDetail)
    {
        $this->priceDetail = $priceDetail;
    
        return $this;
    }

    /**
     * Get priceDetail
     *
     * @return string 
     */
    public function getPriceDetail()
    {
        return $this->priceDetail;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return InvoiceCustomer
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }
}