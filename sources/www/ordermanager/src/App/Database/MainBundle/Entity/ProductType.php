<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductType
 *
 * @ORM\Table(name="product_type")
 * @ORM\Entity
 */
class ProductType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $invoicingCode
     *
     * @ORM\Column(name="invoicing_code", type="string", length=4, nullable=true)
     */
    private $invoicingCode;

    /**
     * @var float $taxGermany
     *
     * @ORM\Column(name="tax_germany", type="decimal", nullable=true)
     */
    private $taxGermany;

    /**
     * @var float $taxAustria
     *
     * @ORM\Column(name="tax_austria", type="decimal", nullable=true)
     */
    private $taxAustria;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set invoicingCode
     *
     * @param string $invoicingCode
     * @return ProductType
     */
    public function setInvoicingCode($invoicingCode)
    {
        $this->invoicingCode = $invoicingCode;
    
        return $this;
    }

    /**
     * Get invoicingCode
     *
     * @return string 
     */
    public function getInvoicingCode()
    {
        return $this->invoicingCode;
    }

    /**
     * Set taxGermany
     *
     * @param float $taxGermany
     * @return ProductType
     */
    public function setTaxGermany($taxGermany)
    {
        $this->taxGermany = $taxGermany;
    
        return $this;
    }

    /**
     * Get taxGermany
     *
     * @return float 
     */
    public function getTaxGermany()
    {
        return $this->taxGermany;
    }

    /**
     * Set taxAustria
     *
     * @param float $taxAustria
     * @return ProductType
     */
    public function setTaxAustria($taxAustria)
    {
        $this->taxAustria = $taxAustria;
    
        return $this;
    }

    /**
     * Get taxAustria
     *
     * @return float 
     */
    public function getTaxAustria()
    {
        return $this->taxAustria;
    }
}