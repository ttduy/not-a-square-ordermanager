<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PlateRawResultDetail
 *
 * @ORM\Table(name="plate_raw_result_detail")
 * @ORM\Entity
 */
class PlateRawResultDetail
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPlateRawResult
     *
     * @ORM\Column(name="id_plate_raw_result", type="integer", nullable=true)
     */
    private $idPlateRawResult;

    /**
     * @var integer $wellIndex
     *
     * @ORM\Column(name="well_index", type="integer", nullable=true)
     */
    private $wellIndex;

    /**
     * @var integer $cycle
     *
     * @ORM\Column(name="cycle", type="integer", nullable=true)
     */
    private $cycle;

    /**
     * @var float $fam
     *
     * @ORM\Column(name="fam", type="float", nullable=true)
     */
    private $fam;

    /**
     * @var float $rox
     *
     * @ORM\Column(name="rox", type="float", nullable=true)
     */
    private $rox;

    /**
     * @var float $vic
     *
     * @ORM\Column(name="vic", type="float", nullable=true)
     */
    private $vic;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPlateRawResult
     *
     * @param integer $idPlateRawResult
     * @return PlateRawResultDetail
     */
    public function setIdPlateRawResult($idPlateRawResult)
    {
        $this->idPlateRawResult = $idPlateRawResult;
    
        return $this;
    }

    /**
     * Get idPlateRawResult
     *
     * @return integer 
     */
    public function getIdPlateRawResult()
    {
        return $this->idPlateRawResult;
    }

    /**
     * Set wellIndex
     *
     * @param integer $wellIndex
     * @return PlateRawResultDetail
     */
    public function setWellIndex($wellIndex)
    {
        $this->wellIndex = $wellIndex;
    
        return $this;
    }

    /**
     * Get wellIndex
     *
     * @return integer 
     */
    public function getWellIndex()
    {
        return $this->wellIndex;
    }

    /**
     * Set cycle
     *
     * @param integer $cycle
     * @return PlateRawResultDetail
     */
    public function setCycle($cycle)
    {
        $this->cycle = $cycle;
    
        return $this;
    }

    /**
     * Get cycle
     *
     * @return integer 
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * Set fam
     *
     * @param float $fam
     * @return PlateRawResultDetail
     */
    public function setFam($fam)
    {
        $this->fam = $fam;
    
        return $this;
    }

    /**
     * Get fam
     *
     * @return float 
     */
    public function getFam()
    {
        return $this->fam;
    }

    /**
     * Set rox
     *
     * @param float $rox
     * @return PlateRawResultDetail
     */
    public function setRox($rox)
    {
        $this->rox = $rox;
    
        return $this;
    }

    /**
     * Get rox
     *
     * @return float 
     */
    public function getRox()
    {
        return $this->rox;
    }

    /**
     * Set vic
     *
     * @param float $vic
     * @return PlateRawResultDetail
     */
    public function setVic($vic)
    {
        $this->vic = $vic;
    
        return $this;
    }

    /**
     * Get vic
     *
     * @return float 
     */
    public function getVic()
    {
        return $this->vic;
    }
}