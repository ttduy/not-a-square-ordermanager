<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OfflineJob
 *
 * @ORM\Table(name="offline_job")
 * @ORM\Entity
 */
class OfflineJob
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idHandler
     *
     * @ORM\Column(name="id_handler", type="integer", nullable=true)
     */
    private $idHandler;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var \DateTime $inputTime
     *
     * @ORM\Column(name="input_time", type="datetime", nullable=true)
     */
    private $inputTime;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $parameters
     *
     * @ORM\Column(name="parameters", type="text", nullable=true)
     */
    private $parameters;

    /**
     * @var string $outputResult
     *
     * @ORM\Column(name="output_result", type="text", nullable=true)
     */
    private $outputResult;

    /**
     * @var float $percentage
     *
     * @ORM\Column(name="percentage", type="decimal", nullable=true)
     */
    private $percentage;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $isSaved
     *
     * @ORM\Column(name="is_saved", type="integer", nullable=true)
     */
    private $isSaved;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idHandler
     *
     * @param integer $idHandler
     * @return OfflineJob
     */
    public function setIdHandler($idHandler)
    {
        $this->idHandler = $idHandler;
    
        return $this;
    }

    /**
     * Get idHandler
     *
     * @return integer 
     */
    public function getIdHandler()
    {
        return $this->idHandler;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return OfflineJob
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set inputTime
     *
     * @param \DateTime $inputTime
     * @return OfflineJob
     */
    public function setInputTime($inputTime)
    {
        $this->inputTime = $inputTime;
    
        return $this;
    }

    /**
     * Get inputTime
     *
     * @return \DateTime 
     */
    public function getInputTime()
    {
        return $this->inputTime;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OfflineJob
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OfflineJob
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set parameters
     *
     * @param string $parameters
     * @return OfflineJob
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    
        return $this;
    }

    /**
     * Get parameters
     *
     * @return string 
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set outputResult
     *
     * @param string $outputResult
     * @return OfflineJob
     */
    public function setOutputResult($outputResult)
    {
        $this->outputResult = $outputResult;
    
        return $this;
    }

    /**
     * Get outputResult
     *
     * @return string 
     */
    public function getOutputResult()
    {
        return $this->outputResult;
    }

    /**
     * Set percentage
     *
     * @param float $percentage
     * @return OfflineJob
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    
        return $this;
    }

    /**
     * Get percentage
     *
     * @return float 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return OfflineJob
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return OfflineJob
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set isSaved
     *
     * @param integer $isSaved
     * @return OfflineJob
     */
    public function setIsSaved($isSaved)
    {
        $this->isSaved = $isSaved;
    
        return $this;
    }

    /**
     * Get isSaved
     *
     * @return integer 
     */
    public function getIsSaved()
    {
        return $this->isSaved;
    }
}