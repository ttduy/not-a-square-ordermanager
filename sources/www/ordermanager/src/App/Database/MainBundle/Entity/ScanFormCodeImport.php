<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormCodeImport
 *
 * @ORM\Table(name="scan_form_code_import")
 * @ORM\Entity
 */
class ScanFormCodeImport
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="text", nullable=true)
     */
    private $code;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ScanFormCodeImport
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return ScanFormCodeImport
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ScanFormCodeImport
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}