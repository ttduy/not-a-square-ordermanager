<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectiveInvoiceActivity
 *
 * @ORM\Table(name="collective_invoice_activity")
 * @ORM\Entity
 */
class CollectiveInvoiceActivity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $collectiveinvoiceid
     *
     * @ORM\Column(name="CollectiveInvoiceId", type="integer", nullable=false)
     */
    private $collectiveinvoiceid;

    /**
     * @var \DateTime $activitydate
     *
     * @ORM\Column(name="ActivityDate", type="date", nullable=true)
     */
    private $activitydate;

    /**
     * @var integer $activitytimeid
     *
     * @ORM\Column(name="ActivityTimeId", type="integer", nullable=true)
     */
    private $activitytimeid;

    /**
     * @var integer $activitytypeid
     *
     * @ORM\Column(name="ActivityTypeId", type="integer", nullable=true)
     */
    private $activitytypeid;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=false)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectiveinvoiceid
     *
     * @param integer $collectiveinvoiceid
     * @return CollectiveInvoiceActivity
     */
    public function setCollectiveinvoiceid($collectiveinvoiceid)
    {
        $this->collectiveinvoiceid = $collectiveinvoiceid;
    
        return $this;
    }

    /**
     * Get collectiveinvoiceid
     *
     * @return integer 
     */
    public function getCollectiveinvoiceid()
    {
        return $this->collectiveinvoiceid;
    }

    /**
     * Set activitydate
     *
     * @param \DateTime $activitydate
     * @return CollectiveInvoiceActivity
     */
    public function setActivitydate($activitydate)
    {
        $this->activitydate = $activitydate;
    
        return $this;
    }

    /**
     * Get activitydate
     *
     * @return \DateTime 
     */
    public function getActivitydate()
    {
        return $this->activitydate;
    }

    /**
     * Set activitytimeid
     *
     * @param integer $activitytimeid
     * @return CollectiveInvoiceActivity
     */
    public function setActivitytimeid($activitytimeid)
    {
        $this->activitytimeid = $activitytimeid;
    
        return $this;
    }

    /**
     * Get activitytimeid
     *
     * @return integer 
     */
    public function getActivitytimeid()
    {
        return $this->activitytimeid;
    }

    /**
     * Set activitytypeid
     *
     * @param integer $activitytypeid
     * @return CollectiveInvoiceActivity
     */
    public function setActivitytypeid($activitytypeid)
    {
        $this->activitytypeid = $activitytypeid;
    
        return $this;
    }

    /**
     * Get activitytypeid
     *
     * @return integer 
     */
    public function getActivitytypeid()
    {
        return $this->activitytypeid;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CollectiveInvoiceActivity
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return CollectiveInvoiceActivity
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}