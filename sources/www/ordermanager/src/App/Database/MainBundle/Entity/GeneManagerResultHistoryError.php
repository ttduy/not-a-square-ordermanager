<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GeneManagerResultHistoryError
 *
 * @ORM\Table(name="gene_manager_result_history_error")
 * @ORM\Entity
 */
class GeneManagerResultHistoryError
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGeneManagerResultHistory
     *
     * @ORM\Column(name="id_gene_manager_result_history", type="integer", nullable=true)
     */
    private $idGeneManagerResultHistory;

    /**
     * @var integer $idErrorType
     *
     * @ORM\Column(name="id_error_type", type="integer", nullable=true)
     */
    private $idErrorType;

    /**
     * @var string $errorData
     *
     * @ORM\Column(name="error_data", type="text", nullable=true)
     */
    private $errorData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGeneManagerResultHistory
     *
     * @param integer $idGeneManagerResultHistory
     * @return GeneManagerResultHistoryError
     */
    public function setIdGeneManagerResultHistory($idGeneManagerResultHistory)
    {
        $this->idGeneManagerResultHistory = $idGeneManagerResultHistory;
    
        return $this;
    }

    /**
     * Get idGeneManagerResultHistory
     *
     * @return integer 
     */
    public function getIdGeneManagerResultHistory()
    {
        return $this->idGeneManagerResultHistory;
    }

    /**
     * Set idErrorType
     *
     * @param integer $idErrorType
     * @return GeneManagerResultHistoryError
     */
    public function setIdErrorType($idErrorType)
    {
        $this->idErrorType = $idErrorType;
    
        return $this;
    }

    /**
     * Get idErrorType
     *
     * @return integer 
     */
    public function getIdErrorType()
    {
        return $this->idErrorType;
    }

    /**
     * Set errorData
     *
     * @param string $errorData
     * @return GeneManagerResultHistoryError
     */
    public function setErrorData($errorData)
    {
        $this->errorData = $errorData;
    
        return $this;
    }

    /**
     * Get errorData
     *
     * @return string 
     */
    public function getErrorData()
    {
        return $this->errorData;
    }
}