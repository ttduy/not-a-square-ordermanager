<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SupplementType
 *
 * @ORM\Table(name="supplement_type")
 * @ORM\Entity
 */
class SupplementType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $letter
     *
     * @ORM\Column(name="letter", type="string", length=50, nullable=true)
     */
    private $letter;

    /**
     * @var string $lotInUse
     *
     * @ORM\Column(name="lot_in_use", type="string", length=50, nullable=true)
     */
    private $lotInUse;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SupplementType
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set letter
     *
     * @param string $letter
     * @return SupplementType
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;
    
        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set lotInUse
     *
     * @param string $lotInUse
     * @return SupplementType
     */
    public function setLotInUse($lotInUse)
    {
        $this->lotInUse = $lotInUse;
    
        return $this;
    }

    /**
     * Get lotInUse
     *
     * @return string 
     */
    public function getLotInUse()
    {
        return $this->lotInUse;
    }
}