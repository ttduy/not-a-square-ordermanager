<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormFileActualTokens
 *
 * @ORM\Table(name="scan_form_file_actual_tokens")
 * @ORM\Entity
 */
class ScanFormFileActualTokens
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormFile
     *
     * @ORM\Column(name="id_scan_form_file", type="integer", nullable=true)
     */
    private $idScanFormFile;

    /**
     * @var integer $idPart
     *
     * @ORM\Column(name="id_part", type="integer", nullable=true)
     */
    private $idPart;

    /**
     * @var integer $idScanFormTemplate
     *
     * @ORM\Column(name="id_scan_form_template", type="integer", nullable=true)
     */
    private $idScanFormTemplate;

    /**
     * @var integer $idScanFormTemplateZone
     *
     * @ORM\Column(name="id_scan_form_template_zone", type="integer", nullable=true)
     */
    private $idScanFormTemplateZone;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=500, nullable=true)
     */
    private $image;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormFile
     *
     * @param integer $idScanFormFile
     * @return ScanFormFileActualTokens
     */
    public function setIdScanFormFile($idScanFormFile)
    {
        $this->idScanFormFile = $idScanFormFile;
    
        return $this;
    }

    /**
     * Get idScanFormFile
     *
     * @return integer 
     */
    public function getIdScanFormFile()
    {
        return $this->idScanFormFile;
    }

    /**
     * Set idPart
     *
     * @param integer $idPart
     * @return ScanFormFileActualTokens
     */
    public function setIdPart($idPart)
    {
        $this->idPart = $idPart;
    
        return $this;
    }

    /**
     * Get idPart
     *
     * @return integer 
     */
    public function getIdPart()
    {
        return $this->idPart;
    }

    /**
     * Set idScanFormTemplate
     *
     * @param integer $idScanFormTemplate
     * @return ScanFormFileActualTokens
     */
    public function setIdScanFormTemplate($idScanFormTemplate)
    {
        $this->idScanFormTemplate = $idScanFormTemplate;
    
        return $this;
    }

    /**
     * Get idScanFormTemplate
     *
     * @return integer 
     */
    public function getIdScanFormTemplate()
    {
        return $this->idScanFormTemplate;
    }

    /**
     * Set idScanFormTemplateZone
     *
     * @param integer $idScanFormTemplateZone
     * @return ScanFormFileActualTokens
     */
    public function setIdScanFormTemplateZone($idScanFormTemplateZone)
    {
        $this->idScanFormTemplateZone = $idScanFormTemplateZone;
    
        return $this;
    }

    /**
     * Get idScanFormTemplateZone
     *
     * @return integer 
     */
    public function getIdScanFormTemplateZone()
    {
        return $this->idScanFormTemplateZone;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ScanFormFileActualTokens
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}