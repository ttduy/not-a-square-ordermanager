<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportLanguageFont
 *
 * @ORM\Table(name="report_language_font")
 * @ORM\Entity
 */
class ReportLanguageFont
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var integer $idRegular
     *
     * @ORM\Column(name="id_regular", type="integer", nullable=true)
     */
    private $idRegular;

    /**
     * @var integer $idRegularItalic
     *
     * @ORM\Column(name="id_regular_italic", type="integer", nullable=true)
     */
    private $idRegularItalic;

    /**
     * @var integer $idBold
     *
     * @ORM\Column(name="id_bold", type="integer", nullable=true)
     */
    private $idBold;

    /**
     * @var integer $idLight
     *
     * @ORM\Column(name="id_light", type="integer", nullable=true)
     */
    private $idLight;

    /**
     * @var integer $idLightItalic
     *
     * @ORM\Column(name="id_light_italic", type="integer", nullable=true)
     */
    private $idLightItalic;

    /**
     * @var integer $idMedium
     *
     * @ORM\Column(name="id_medium", type="integer", nullable=true)
     */
    private $idMedium;

    /**
     * @var integer $idThin
     *
     * @ORM\Column(name="id_thin", type="integer", nullable=true)
     */
    private $idThin;

    /**
     * @var integer $idSpecial
     *
     * @ORM\Column(name="id_special", type="integer", nullable=true)
     */
    private $idSpecial;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return ReportLanguageFont
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set idRegular
     *
     * @param integer $idRegular
     * @return ReportLanguageFont
     */
    public function setIdRegular($idRegular)
    {
        $this->idRegular = $idRegular;
    
        return $this;
    }

    /**
     * Get idRegular
     *
     * @return integer 
     */
    public function getIdRegular()
    {
        return $this->idRegular;
    }

    /**
     * Set idRegularItalic
     *
     * @param integer $idRegularItalic
     * @return ReportLanguageFont
     */
    public function setIdRegularItalic($idRegularItalic)
    {
        $this->idRegularItalic = $idRegularItalic;
    
        return $this;
    }

    /**
     * Get idRegularItalic
     *
     * @return integer 
     */
    public function getIdRegularItalic()
    {
        return $this->idRegularItalic;
    }

    /**
     * Set idBold
     *
     * @param integer $idBold
     * @return ReportLanguageFont
     */
    public function setIdBold($idBold)
    {
        $this->idBold = $idBold;
    
        return $this;
    }

    /**
     * Get idBold
     *
     * @return integer 
     */
    public function getIdBold()
    {
        return $this->idBold;
    }

    /**
     * Set idLight
     *
     * @param integer $idLight
     * @return ReportLanguageFont
     */
    public function setIdLight($idLight)
    {
        $this->idLight = $idLight;
    
        return $this;
    }

    /**
     * Get idLight
     *
     * @return integer 
     */
    public function getIdLight()
    {
        return $this->idLight;
    }

    /**
     * Set idLightItalic
     *
     * @param integer $idLightItalic
     * @return ReportLanguageFont
     */
    public function setIdLightItalic($idLightItalic)
    {
        $this->idLightItalic = $idLightItalic;
    
        return $this;
    }

    /**
     * Get idLightItalic
     *
     * @return integer 
     */
    public function getIdLightItalic()
    {
        return $this->idLightItalic;
    }

    /**
     * Set idMedium
     *
     * @param integer $idMedium
     * @return ReportLanguageFont
     */
    public function setIdMedium($idMedium)
    {
        $this->idMedium = $idMedium;
    
        return $this;
    }

    /**
     * Get idMedium
     *
     * @return integer 
     */
    public function getIdMedium()
    {
        return $this->idMedium;
    }

    /**
     * Set idThin
     *
     * @param integer $idThin
     * @return ReportLanguageFont
     */
    public function setIdThin($idThin)
    {
        $this->idThin = $idThin;
    
        return $this;
    }

    /**
     * Get idThin
     *
     * @return integer 
     */
    public function getIdThin()
    {
        return $this->idThin;
    }

    /**
     * Set idSpecial
     *
     * @param integer $idSpecial
     * @return ReportLanguageFont
     */
    public function setIdSpecial($idSpecial)
    {
        $this->idSpecial = $idSpecial;
    
        return $this;
    }

    /**
     * Get idSpecial
     *
     * @return integer 
     */
    public function getIdSpecial()
    {
        return $this->idSpecial;
    }
}