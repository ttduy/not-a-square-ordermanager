<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillManualSystemNote
 *
 * @ORM\Table(name="bill_manual_system_note")
 * @ORM\Entity
 */
class BillManualSystemNote
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBill
     *
     * @ORM\Column(name="id_bill", type="integer", nullable=true)
     */
    private $idBill;

    /**
     * @var integer $idTarget
     *
     * @ORM\Column(name="id_target", type="integer", nullable=true)
     */
    private $idTarget;

    /**
     * @var string $cost
     *
     * @ORM\Column(name="cost", type="string", length=255, nullable=true)
     */
    private $cost;

    /**
     * @var string $text
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string $note
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBill
     *
     * @param integer $idBill
     * @return BillManualSystemNote
     */
    public function setIdBill($idBill)
    {
        $this->idBill = $idBill;
    
        return $this;
    }

    /**
     * Get idBill
     *
     * @return integer 
     */
    public function getIdBill()
    {
        return $this->idBill;
    }

    /**
     * Set idTarget
     *
     * @param integer $idTarget
     * @return BillManualSystemNote
     */
    public function setIdTarget($idTarget)
    {
        $this->idTarget = $idTarget;
    
        return $this;
    }

    /**
     * Get idTarget
     *
     * @return integer 
     */
    public function getIdTarget()
    {
        return $this->idTarget;
    }

    /**
     * Set cost
     *
     * @param string $cost
     * @return BillManualSystemNote
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return string 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return BillManualSystemNote
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return BillManualSystemNote
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return BillManualSystemNote
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return BillManualSystemNote
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }
}