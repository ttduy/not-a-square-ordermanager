<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\AcquisiteurRatingHistory
 *
 * @ORM\Table(name="acquisiteur_rating_history")
 * @ORM\Entity
 */
class AcquisiteurRatingHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idRating
     *
     * @ORM\Column(name="id_rating", type="integer", nullable=true)
     */
    private $idRating;

    /**
     * @var integer $idAcquisiteur
     *
     * @ORM\Column(name="id_acquisiteur", type="integer", nullable=true)
     */
    private $idAcquisiteur;

    /**
     * @var integer $oldRating
     *
     * @ORM\Column(name="old_rating", type="integer", nullable=true)
     */
    private $oldRating;

    /**
     * @var integer $newRating
     *
     * @ORM\Column(name="new_rating", type="integer", nullable=true)
     */
    private $newRating;

    /**
     * @var integer $totalOfOrders
     *
     * @ORM\Column(name="total_of_orders", type="integer", nullable=true)
     */
    private $totalOfOrders;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="date", nullable=true)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idRating
     *
     * @param integer $idRating
     * @return AcquisiteurRatingHistory
     */
    public function setIdRating($idRating)
    {
        $this->idRating = $idRating;
    
        return $this;
    }

    /**
     * Get idRating
     *
     * @return integer 
     */
    public function getIdRating()
    {
        return $this->idRating;
    }

    /**
     * Set idAcquisiteur
     *
     * @param integer $idAcquisiteur
     * @return AcquisiteurRatingHistory
     */
    public function setIdAcquisiteur($idAcquisiteur)
    {
        $this->idAcquisiteur = $idAcquisiteur;
    
        return $this;
    }

    /**
     * Get idAcquisiteur
     *
     * @return integer 
     */
    public function getIdAcquisiteur()
    {
        return $this->idAcquisiteur;
    }

    /**
     * Set oldRating
     *
     * @param integer $oldRating
     * @return AcquisiteurRatingHistory
     */
    public function setOldRating($oldRating)
    {
        $this->oldRating = $oldRating;
    
        return $this;
    }

    /**
     * Get oldRating
     *
     * @return integer 
     */
    public function getOldRating()
    {
        return $this->oldRating;
    }

    /**
     * Set newRating
     *
     * @param integer $newRating
     * @return AcquisiteurRatingHistory
     */
    public function setNewRating($newRating)
    {
        $this->newRating = $newRating;
    
        return $this;
    }

    /**
     * Get newRating
     *
     * @return integer 
     */
    public function getNewRating()
    {
        return $this->newRating;
    }

    /**
     * Set totalOfOrders
     *
     * @param integer $totalOfOrders
     * @return AcquisiteurRatingHistory
     */
    public function setTotalOfOrders($totalOfOrders)
    {
        $this->totalOfOrders = $totalOfOrders;
    
        return $this;
    }

    /**
     * Get totalOfOrders
     *
     * @return integer 
     */
    public function getTotalOfOrders()
    {
        return $this->totalOfOrders;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AcquisiteurRatingHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}