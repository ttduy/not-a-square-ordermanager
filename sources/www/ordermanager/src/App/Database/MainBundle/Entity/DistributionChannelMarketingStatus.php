<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannelMarketingStatus
 *
 * @ORM\Table(name="distribution_channel_marketing_status")
 * @ORM\Entity
 */
class DistributionChannelMarketingStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idMarketingStatus
     *
     * @ORM\Column(name="id_marketing_status", type="integer", nullable=true)
     */
    private $idMarketingStatus;

    /**
     * @var integer $idUser
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime $reminderDate
     *
     * @ORM\Column(name="reminder_date", type="date", nullable=true)
     */
    private $reminderDate;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return DistributionChannelMarketingStatus
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idMarketingStatus
     *
     * @param integer $idMarketingStatus
     * @return DistributionChannelMarketingStatus
     */
    public function setIdMarketingStatus($idMarketingStatus)
    {
        $this->idMarketingStatus = $idMarketingStatus;
    
        return $this;
    }

    /**
     * Get idMarketingStatus
     *
     * @return integer 
     */
    public function getIdMarketingStatus()
    {
        return $this->idMarketingStatus;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return DistributionChannelMarketingStatus
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return DistributionChannelMarketingStatus
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return DistributionChannelMarketingStatus
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     * @return DistributionChannelMarketingStatus
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;
    
        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime 
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return DistributionChannelMarketingStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}