<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TodoContinualImprovement
 *
 * @ORM\Table(name="todo_continual_improvement")
 * @ORM\Entity
 */
class TodoContinualImprovement
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idUser
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $milestones
     *
     * @ORM\Column(name="milestones", type="text", nullable=true)
     */
    private $milestones;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $idFinishIn
     *
     * @ORM\Column(name="id_finish_in", type="integer", nullable=true)
     */
    private $idFinishIn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return TodoContinualImprovement
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return TodoContinualImprovement
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TodoContinualImprovement
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TodoContinualImprovement
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set milestones
     *
     * @param string $milestones
     * @return TodoContinualImprovement
     */
    public function setMilestones($milestones)
    {
        $this->milestones = $milestones;
    
        return $this;
    }

    /**
     * Get milestones
     *
     * @return string 
     */
    public function getMilestones()
    {
        return $this->milestones;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TodoContinualImprovement
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idFinishIn
     *
     * @param integer $idFinishIn
     * @return TodoContinualImprovement
     */
    public function setIdFinishIn($idFinishIn)
    {
        $this->idFinishIn = $idFinishIn;
    
        return $this;
    }

    /**
     * Get idFinishIn
     *
     * @return integer 
     */
    public function getIdFinishIn()
    {
        return $this->idFinishIn;
    }
}