<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WebUsers
 *
 * @ORM\Table(name="web_users")
 * @ORM\Entity
 */
class WebUsers
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="Username", type="string", length=1024, nullable=true)
     */
    private $username;

    /**
     * @var string $password
     *
     * @ORM\Column(name="Password", type="string", length=1024, nullable=true)
     */
    private $password;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var integer $idrole
     *
     * @ORM\Column(name="IdRole", type="integer", nullable=true)
     */
    private $idrole;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return WebUsers
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return WebUsers
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return WebUsers
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set idrole
     *
     * @param integer $idrole
     * @return WebUsers
     */
    public function setIdrole($idrole)
    {
        $this->idrole = $idrole;
    
        return $this;
    }

    /**
     * Get idrole
     *
     * @return integer 
     */
    public function getIdrole()
    {
        return $this->idrole;
    }
}