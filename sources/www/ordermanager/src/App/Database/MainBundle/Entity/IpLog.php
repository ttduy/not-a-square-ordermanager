<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\IpLog
 *
 * @ORM\Table(name="ip_log")
 * @ORM\Entity
 */
class IpLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $ipAddress
     *
     * @ORM\Column(name="ip_address", type="string", length=255, nullable=true)
     */
    private $ipAddress;

    /**
     * @var \DateTime $logTime
     *
     * @ORM\Column(name="log_time", type="datetime", nullable=true)
     */
    private $logTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return IpLog
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    
        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set logTime
     *
     * @param \DateTime $logTime
     * @return IpLog
     */
    public function setLogTime($logTime)
    {
        $this->logTime = $logTime;
    
        return $this;
    }

    /**
     * Get logTime
     *
     * @return \DateTime 
     */
    public function getLogTime()
    {
        return $this->logTime;
    }
}