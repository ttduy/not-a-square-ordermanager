<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderBackup
 *
 * @ORM\Table(name="order_backup")
 * @ORM\Entity
 */
class OrderBackup
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var string $file
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $md5Hash
     *
     * @ORM\Column(name="md5_hash", type="string", length=255, nullable=true)
     */
    private $md5Hash;

    /**
     * @var \DateTime $backupDate
     *
     * @ORM\Column(name="backup_date", type="datetime", nullable=true)
     */
    private $backupDate;

    /**
     * @var string $backupFiles
     *
     * @ORM\Column(name="backup_files", type="text", nullable=true)
     */
    private $backupFiles;

    /**
     * @var integer $numberOfOrders
     *
     * @ORM\Column(name="number_of_orders", type="integer", nullable=true)
     */
    private $numberOfOrders;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OrderBackup
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrderBackup
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return OrderBackup
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return OrderBackup
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return OrderBackup
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OrderBackup
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set md5Hash
     *
     * @param string $md5Hash
     * @return OrderBackup
     */
    public function setMd5Hash($md5Hash)
    {
        $this->md5Hash = $md5Hash;
    
        return $this;
    }

    /**
     * Get md5Hash
     *
     * @return string 
     */
    public function getMd5Hash()
    {
        return $this->md5Hash;
    }

    /**
     * Set backupDate
     *
     * @param \DateTime $backupDate
     * @return OrderBackup
     */
    public function setBackupDate($backupDate)
    {
        $this->backupDate = $backupDate;
    
        return $this;
    }

    /**
     * Get backupDate
     *
     * @return \DateTime 
     */
    public function getBackupDate()
    {
        return $this->backupDate;
    }

    /**
     * Set backupFiles
     *
     * @param string $backupFiles
     * @return OrderBackup
     */
    public function setBackupFiles($backupFiles)
    {
        $this->backupFiles = $backupFiles;
    
        return $this;
    }

    /**
     * Get backupFiles
     *
     * @return string 
     */
    public function getBackupFiles()
    {
        return $this->backupFiles;
    }

    /**
     * Set numberOfOrders
     *
     * @param integer $numberOfOrders
     * @return OrderBackup
     */
    public function setNumberOfOrders($numberOfOrders)
    {
        $this->numberOfOrders = $numberOfOrders;
    
        return $this;
    }

    /**
     * Get numberOfOrders
     *
     * @return integer 
     */
    public function getNumberOfOrders()
    {
        return $this->numberOfOrders;
    }
}