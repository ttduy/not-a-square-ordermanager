<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CostTrackerSectorPerMonth
 *
 * @ORM\Table(name="cost_tracker_sector_per_month")
 * @ORM\Entity
 */
class CostTrackerSectorPerMonth
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $costTrackerSectorId
     *
     * @ORM\Column(name="cost_tracker_sector_id", type="integer", nullable=false)
     */
    private $costTrackerSectorId;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var integer $month
     *
     * @ORM\Column(name="month", type="integer", nullable=true)
     */
    private $month;

    /**
     * @var float $cost
     *
     * @ORM\Column(name="cost", type="decimal", nullable=true)
     */
    private $cost;

    /**
     * @var float $costPrognosis
     *
     * @ORM\Column(name="cost_prognosis", type="decimal", nullable=true)
     */
    private $costPrognosis;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set costTrackerSectorId
     *
     * @param integer $costTrackerSectorId
     * @return CostTrackerSectorPerMonth
     */
    public function setCostTrackerSectorId($costTrackerSectorId)
    {
        $this->costTrackerSectorId = $costTrackerSectorId;
    
        return $this;
    }

    /**
     * Get costTrackerSectorId
     *
     * @return integer 
     */
    public function getCostTrackerSectorId()
    {
        return $this->costTrackerSectorId;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return CostTrackerSectorPerMonth
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return CostTrackerSectorPerMonth
     */
    public function setMonth($month)
    {
        $this->month = $month;
    
        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return CostTrackerSectorPerMonth
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set costPrognosis
     *
     * @param float $costPrognosis
     * @return CostTrackerSectorPerMonth
     */
    public function setCostPrognosis($costPrognosis)
    {
        $this->costPrognosis = $costPrognosis;
    
        return $this;
    }

    /**
     * Get costPrognosis
     *
     * @return float 
     */
    public function getCostPrognosis()
    {
        return $this->costPrognosis;
    }
}