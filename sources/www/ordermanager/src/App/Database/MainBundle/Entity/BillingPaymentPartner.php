<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillingPaymentPartner
 *
 * @ORM\Table(name="billing_payment_partner")
 * @ORM\Entity
 */
class BillingPaymentPartner
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;

    /**
     * @var \DateTime $orderDate
     *
     * @ORM\Column(name="order_date", type="date", nullable=true)
     */
    private $orderDate;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var float $totalAmount
     *
     * @ORM\Column(name="total_amount", type="decimal", nullable=true)
     */
    private $totalAmount;

    /**
     * @var float $overrideAmount
     *
     * @ORM\Column(name="override_amount", type="decimal", nullable=true)
     */
    private $overrideAmount;

    /**
     * @var integer $idBill
     *
     * @ORM\Column(name="id_bill", type="integer", nullable=true)
     */
    private $idBill;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return BillingPaymentPartner
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return BillingPaymentPartner
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return BillingPaymentPartner
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     * @return BillingPaymentPartner
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
    
        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime 
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BillingPaymentPartner
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     * @return BillingPaymentPartner
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    
        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float 
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set overrideAmount
     *
     * @param float $overrideAmount
     * @return BillingPaymentPartner
     */
    public function setOverrideAmount($overrideAmount)
    {
        $this->overrideAmount = $overrideAmount;
    
        return $this;
    }

    /**
     * Get overrideAmount
     *
     * @return float 
     */
    public function getOverrideAmount()
    {
        return $this->overrideAmount;
    }

    /**
     * Set idBill
     *
     * @param integer $idBill
     * @return BillingPaymentPartner
     */
    public function setIdBill($idBill)
    {
        $this->idBill = $idBill;
    
        return $this;
    }

    /**
     * Get idBill
     *
     * @return integer 
     */
    public function getIdBill()
    {
        return $this->idBill;
    }
}