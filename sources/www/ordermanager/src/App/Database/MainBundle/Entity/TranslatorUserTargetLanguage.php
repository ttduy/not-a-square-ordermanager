<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TranslatorUserTargetLanguage
 *
 * @ORM\Table(name="translator_user_target_language")
 * @ORM\Entity
 */
class TranslatorUserTargetLanguage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idTranslatorUser
     *
     * @ORM\Column(name="id_translator_user", type="integer", nullable=true)
     */
    private $idTranslatorUser;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTranslatorUser
     *
     * @param integer $idTranslatorUser
     * @return TranslatorUserTargetLanguage
     */
    public function setIdTranslatorUser($idTranslatorUser)
    {
        $this->idTranslatorUser = $idTranslatorUser;
    
        return $this;
    }

    /**
     * Get idTranslatorUser
     *
     * @return integer 
     */
    public function getIdTranslatorUser()
    {
        return $this->idTranslatorUser;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return TranslatorUserTargetLanguage
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }
}