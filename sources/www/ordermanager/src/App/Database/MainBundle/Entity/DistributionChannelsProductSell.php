<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannelsProductSell
 *
 * @ORM\Table(name="distribution_channels_product_sell")
 * @ORM\Entity
 */
class DistributionChannelsProductSell
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var integer $idProduct
     *
     * @ORM\Column(name="id_product", type="integer", nullable=true)
     */
    private $idProduct;

    /**
     * @var integer $total
     *
     * @ORM\Column(name="total", type="integer", nullable=true)
     */
    private $total;

    /**
     * @var integer $isTopSell
     *
     * @ORM\Column(name="is_top_sell", type="integer", nullable=true)
     */
    private $isTopSell;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return DistributionChannelsProductSell
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     * @return DistributionChannelsProductSell
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    
        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set total
     *
     * @param integer $total
     * @return DistributionChannelsProductSell
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return integer 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set isTopSell
     *
     * @param integer $isTopSell
     * @return DistributionChannelsProductSell
     */
    public function setIsTopSell($isTopSell)
    {
        $this->isTopSell = $isTopSell;
    
        return $this;
    }

    /**
     * Get isTopSell
     *
     * @return integer 
     */
    public function getIsTopSell()
    {
        return $this->isTopSell;
    }
}