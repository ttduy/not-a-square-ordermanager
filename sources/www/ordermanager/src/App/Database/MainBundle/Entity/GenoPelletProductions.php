<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPelletProductions
 *
 * @ORM\Table(name="geno_pellet_productions")
 * @ORM\Entity
 */
class GenoPelletProductions
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var integer $startBy
     *
     * @ORM\Column(name="start_by", type="integer", nullable=true)
     */
    private $startBy;

    /**
     * @var string $formulaName
     *
     * @ORM\Column(name="formula_name", type="string", length=255, nullable=true)
     */
    private $formulaName;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return GenoPelletProductions
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return GenoPelletProductions
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set startBy
     *
     * @param integer $startBy
     * @return GenoPelletProductions
     */
    public function setStartBy($startBy)
    {
        $this->startBy = $startBy;
    
        return $this;
    }

    /**
     * Get startBy
     *
     * @return integer 
     */
    public function getStartBy()
    {
        return $this->startBy;
    }

    /**
     * Set formulaName
     *
     * @param string $formulaName
     * @return GenoPelletProductions
     */
    public function setFormulaName($formulaName)
    {
        $this->formulaName = $formulaName;
    
        return $this;
    }

    /**
     * Get formulaName
     *
     * @return string 
     */
    public function getFormulaName()
    {
        return $this->formulaName;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return GenoPelletProductions
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}