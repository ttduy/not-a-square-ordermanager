<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormVersion
 *
 * @ORM\Table(name="form_version")
 * @ORM\Entity
 */
class FormVersion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idForm
     *
     * @ORM\Column(name="id_form", type="integer", nullable=true)
     */
    private $idForm;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $version
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idForm
     *
     * @param integer $idForm
     * @return FormVersion
     */
    public function setIdForm($idForm)
    {
        $this->idForm = $idForm;
    
        return $this;
    }

    /**
     * Get idForm
     *
     * @return integer 
     */
    public function getIdForm()
    {
        return $this->idForm;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return FormVersion
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return FormVersion
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }
}