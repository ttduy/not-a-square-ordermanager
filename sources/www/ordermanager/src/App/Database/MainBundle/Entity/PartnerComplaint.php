<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PartnerComplaint
 *
 * @ORM\Table(name="partner_complaint")
 * @ORM\Entity
 */
class PartnerComplaint
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var \DateTime $problemTimestamp
     *
     * @ORM\Column(name="problem_timestamp", type="datetime", nullable=false)
     */
    private $problemTimestamp;

    /**
     * @var integer $problemIdWebUser
     *
     * @ORM\Column(name="problem_id_web_user", type="integer", nullable=true)
     */
    private $problemIdWebUser;

    /**
     * @var string $problemComplaintant
     *
     * @ORM\Column(name="problem_complaintant", type="string", length=255, nullable=true)
     */
    private $problemComplaintant;

    /**
     * @var string $problemDescription
     *
     * @ORM\Column(name="problem_description", type="text", nullable=true)
     */
    private $problemDescription;

    /**
     * @var \DateTime $solutionTimestamp
     *
     * @ORM\Column(name="solution_timestamp", type="datetime", nullable=false)
     */
    private $solutionTimestamp;

    /**
     * @var integer $solutionIdWebUser
     *
     * @ORM\Column(name="solution_id_web_user", type="integer", nullable=true)
     */
    private $solutionIdWebUser;

    /**
     * @var string $solutionDescription
     *
     * @ORM\Column(name="solution_description", type="text", nullable=true)
     */
    private $solutionDescription;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var integer $idTopic
     *
     * @ORM\Column(name="id_topic", type="integer", nullable=true)
     */
    private $idTopic;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return PartnerComplaint
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set problemTimestamp
     *
     * @param \DateTime $problemTimestamp
     * @return PartnerComplaint
     */
    public function setProblemTimestamp($problemTimestamp)
    {
        $this->problemTimestamp = $problemTimestamp;
    
        return $this;
    }

    /**
     * Get problemTimestamp
     *
     * @return \DateTime 
     */
    public function getProblemTimestamp()
    {
        return $this->problemTimestamp;
    }

    /**
     * Set problemIdWebUser
     *
     * @param integer $problemIdWebUser
     * @return PartnerComplaint
     */
    public function setProblemIdWebUser($problemIdWebUser)
    {
        $this->problemIdWebUser = $problemIdWebUser;
    
        return $this;
    }

    /**
     * Get problemIdWebUser
     *
     * @return integer 
     */
    public function getProblemIdWebUser()
    {
        return $this->problemIdWebUser;
    }

    /**
     * Set problemComplaintant
     *
     * @param string $problemComplaintant
     * @return PartnerComplaint
     */
    public function setProblemComplaintant($problemComplaintant)
    {
        $this->problemComplaintant = $problemComplaintant;
    
        return $this;
    }

    /**
     * Get problemComplaintant
     *
     * @return string 
     */
    public function getProblemComplaintant()
    {
        return $this->problemComplaintant;
    }

    /**
     * Set problemDescription
     *
     * @param string $problemDescription
     * @return PartnerComplaint
     */
    public function setProblemDescription($problemDescription)
    {
        $this->problemDescription = $problemDescription;
    
        return $this;
    }

    /**
     * Get problemDescription
     *
     * @return string 
     */
    public function getProblemDescription()
    {
        return $this->problemDescription;
    }

    /**
     * Set solutionTimestamp
     *
     * @param \DateTime $solutionTimestamp
     * @return PartnerComplaint
     */
    public function setSolutionTimestamp($solutionTimestamp)
    {
        $this->solutionTimestamp = $solutionTimestamp;
    
        return $this;
    }

    /**
     * Get solutionTimestamp
     *
     * @return \DateTime 
     */
    public function getSolutionTimestamp()
    {
        return $this->solutionTimestamp;
    }

    /**
     * Set solutionIdWebUser
     *
     * @param integer $solutionIdWebUser
     * @return PartnerComplaint
     */
    public function setSolutionIdWebUser($solutionIdWebUser)
    {
        $this->solutionIdWebUser = $solutionIdWebUser;
    
        return $this;
    }

    /**
     * Get solutionIdWebUser
     *
     * @return integer 
     */
    public function getSolutionIdWebUser()
    {
        return $this->solutionIdWebUser;
    }

    /**
     * Set solutionDescription
     *
     * @param string $solutionDescription
     * @return PartnerComplaint
     */
    public function setSolutionDescription($solutionDescription)
    {
        $this->solutionDescription = $solutionDescription;
    
        return $this;
    }

    /**
     * Get solutionDescription
     *
     * @return string 
     */
    public function getSolutionDescription()
    {
        return $this->solutionDescription;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PartnerComplaint
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PartnerComplaint
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set idTopic
     *
     * @param integer $idTopic
     * @return PartnerComplaint
     */
    public function setIdTopic($idTopic)
    {
        $this->idTopic = $idTopic;
    
        return $this;
    }

    /**
     * Get idTopic
     *
     * @return integer 
     */
    public function getIdTopic()
    {
        return $this->idTopic;
    }
}