<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannelAccountEntry
 *
 * @ORM\Table(name="distribution_channel_account_entry")
 * @ORM\Entity
 */
class DistributionChannelAccountEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDistributionChannel
     *
     * @ORM\Column(name="id_distribution_channel", type="integer", nullable=true)
     */
    private $idDistributionChannel;

    /**
     * @var \DateTime $entryDate
     *
     * @ORM\Column(name="entry_date", type="date", nullable=true)
     */
    private $entryDate;

    /**
     * @var integer $idDirection
     *
     * @ORM\Column(name="id_direction", type="integer", nullable=true)
     */
    private $idDirection;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @var string $entryNote
     *
     * @ORM\Column(name="entry_note", type="text", nullable=true)
     */
    private $entryNote;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDistributionChannel
     *
     * @param integer $idDistributionChannel
     * @return DistributionChannelAccountEntry
     */
    public function setIdDistributionChannel($idDistributionChannel)
    {
        $this->idDistributionChannel = $idDistributionChannel;
    
        return $this;
    }

    /**
     * Get idDistributionChannel
     *
     * @return integer 
     */
    public function getIdDistributionChannel()
    {
        return $this->idDistributionChannel;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     * @return DistributionChannelAccountEntry
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;
    
        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime 
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Set idDirection
     *
     * @param integer $idDirection
     * @return DistributionChannelAccountEntry
     */
    public function setIdDirection($idDirection)
    {
        $this->idDirection = $idDirection;
    
        return $this;
    }

    /**
     * Get idDirection
     *
     * @return integer 
     */
    public function getIdDirection()
    {
        return $this->idDirection;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return DistributionChannelAccountEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set entryNote
     *
     * @param string $entryNote
     * @return DistributionChannelAccountEntry
     */
    public function setEntryNote($entryNote)
    {
        $this->entryNote = $entryNote;
    
        return $this;
    }

    /**
     * Get entryNote
     *
     * @return string 
     */
    public function getEntryNote()
    {
        return $this->entryNote;
    }
}