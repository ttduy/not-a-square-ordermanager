<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BodShipmentTracking
 *
 * @ORM\Table(name="bod_shipment_tracking")
 * @ORM\Entity
 */
class BodShipmentTracking
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;

    /**
     * @var string $customerNumber
     *
     * @ORM\Column(name="customer_number", type="string", length=255, nullable=true)
     */
    private $customerNumber;

    /**
     * @var string $shipmentDatetime
     *
     * @ORM\Column(name="shipment_datetime", type="string", length=255, nullable=true)
     */
    private $shipmentDatetime;

    /**
     * @var string $trackingNo
     *
     * @ORM\Column(name="tracking_no", type="string", length=255, nullable=true)
     */
    private $trackingNo;

    /**
     * @var string $trackingLink
     *
     * @ORM\Column(name="tracking_link", type="text", nullable=true)
     */
    private $trackingLink;

    /**
     * @var string $xml
     *
     * @ORM\Column(name="xml", type="text", nullable=true)
     */
    private $xml;

    /**
     * @var string $filename
     *
     * @ORM\Column(name="filename", type="text", nullable=true)
     */
    private $filename;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return BodShipmentTracking
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return BodShipmentTracking
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set customerNumber
     *
     * @param string $customerNumber
     * @return BodShipmentTracking
     */
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;
    
        return $this;
    }

    /**
     * Get customerNumber
     *
     * @return string 
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * Set shipmentDatetime
     *
     * @param string $shipmentDatetime
     * @return BodShipmentTracking
     */
    public function setShipmentDatetime($shipmentDatetime)
    {
        $this->shipmentDatetime = $shipmentDatetime;
    
        return $this;
    }

    /**
     * Get shipmentDatetime
     *
     * @return string 
     */
    public function getShipmentDatetime()
    {
        return $this->shipmentDatetime;
    }

    /**
     * Set trackingNo
     *
     * @param string $trackingNo
     * @return BodShipmentTracking
     */
    public function setTrackingNo($trackingNo)
    {
        $this->trackingNo = $trackingNo;
    
        return $this;
    }

    /**
     * Get trackingNo
     *
     * @return string 
     */
    public function getTrackingNo()
    {
        return $this->trackingNo;
    }

    /**
     * Set trackingLink
     *
     * @param string $trackingLink
     * @return BodShipmentTracking
     */
    public function setTrackingLink($trackingLink)
    {
        $this->trackingLink = $trackingLink;
    
        return $this;
    }

    /**
     * Get trackingLink
     *
     * @return string 
     */
    public function getTrackingLink()
    {
        return $this->trackingLink;
    }

    /**
     * Set xml
     *
     * @param string $xml
     * @return BodShipmentTracking
     */
    public function setXml($xml)
    {
        $this->xml = $xml;
    
        return $this;
    }

    /**
     * Get xml
     *
     * @return string 
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return BodShipmentTracking
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }
}