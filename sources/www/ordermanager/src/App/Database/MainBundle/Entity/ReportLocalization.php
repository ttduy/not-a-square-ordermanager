<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportLocalization
 *
 * @ORM\Table(name="report_localization")
 * @ORM\Entity
 */
class ReportLocalization
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var string $localization
     *
     * @ORM\Column(name="localization", type="text", nullable=true)
     */
    private $localization;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return ReportLocalization
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set localization
     *
     * @param string $localization
     * @return ReportLocalization
     */
    public function setLocalization($localization)
    {
        $this->localization = $localization;
    
        return $this;
    }

    /**
     * Get localization
     *
     * @return string 
     */
    public function getLocalization()
    {
        return $this->localization;
    }
}