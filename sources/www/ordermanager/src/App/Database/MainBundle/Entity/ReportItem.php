<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportItem
 *
 * @ORM\Table(name="report_item")
 * @ORM\Entity
 */
class ReportItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $idReportSection
     *
     * @ORM\Column(name="id_report_section", type="integer", nullable=true)
     */
    private $idReportSection;

    /**
     * @var integer $idTextToolType
     *
     * @ORM\Column(name="id_text_tool_type", type="integer", nullable=true)
     */
    private $idTextToolType;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return ReportItem
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set idReportSection
     *
     * @param integer $idReportSection
     * @return ReportItem
     */
    public function setIdReportSection($idReportSection)
    {
        $this->idReportSection = $idReportSection;
    
        return $this;
    }

    /**
     * Get idReportSection
     *
     * @return integer 
     */
    public function getIdReportSection()
    {
        return $this->idReportSection;
    }

    /**
     * Set idTextToolType
     *
     * @param integer $idTextToolType
     * @return ReportItem
     */
    public function setIdTextToolType($idTextToolType)
    {
        $this->idTextToolType = $idTextToolType;
    
        return $this;
    }

    /**
     * Get idTextToolType
     *
     * @return integer 
     */
    public function getIdTextToolType()
    {
        return $this->idTextToolType;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return ReportItem
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return ReportItem
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}