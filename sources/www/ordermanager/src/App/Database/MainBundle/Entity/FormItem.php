<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FormItem
 *
 * @ORM\Table(name="form_item")
 * @ORM\Entity
 */
class FormItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFormStep
     *
     * @ORM\Column(name="id_form_step", type="integer", nullable=true)
     */
    private $idFormStep;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormStep
     *
     * @param integer $idFormStep
     * @return FormItem
     */
    public function setIdFormStep($idFormStep)
    {
        $this->idFormStep = $idFormStep;
    
        return $this;
    }

    /**
     * Get idFormStep
     *
     * @return integer 
     */
    public function getIdFormStep()
    {
        return $this->idFormStep;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return FormItem
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return FormItem
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return FormItem
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}