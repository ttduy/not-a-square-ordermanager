<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailMassSender
 *
 * @ORM\Table(name="email_mass_sender")
 * @ORM\Entity
 */
class EmailMassSender
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var integer $numOpen
     *
     * @ORM\Column(name="num_open", type="integer", nullable=true)
     */
    private $numOpen;

    /**
     * @var integer $numSendNow
     *
     * @ORM\Column(name="num_send_now", type="integer", nullable=true)
     */
    private $numSendNow;

    /**
     * @var integer $numSending
     *
     * @ORM\Column(name="num_sending", type="integer", nullable=true)
     */
    private $numSending;

    /**
     * @var integer $numSent
     *
     * @ORM\Column(name="num_sent", type="integer", nullable=true)
     */
    private $numSent;

    /**
     * @var integer $numError
     *
     * @ORM\Column(name="num_error", type="integer", nullable=true)
     */
    private $numError;

    /**
     * @var integer $numRead
     *
     * @ORM\Column(name="num_read", type="integer", nullable=true)
     */
    private $numRead;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return EmailMassSender
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return EmailMassSender
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set numOpen
     *
     * @param integer $numOpen
     * @return EmailMassSender
     */
    public function setNumOpen($numOpen)
    {
        $this->numOpen = $numOpen;
    
        return $this;
    }

    /**
     * Get numOpen
     *
     * @return integer 
     */
    public function getNumOpen()
    {
        return $this->numOpen;
    }

    /**
     * Set numSendNow
     *
     * @param integer $numSendNow
     * @return EmailMassSender
     */
    public function setNumSendNow($numSendNow)
    {
        $this->numSendNow = $numSendNow;
    
        return $this;
    }

    /**
     * Get numSendNow
     *
     * @return integer 
     */
    public function getNumSendNow()
    {
        return $this->numSendNow;
    }

    /**
     * Set numSending
     *
     * @param integer $numSending
     * @return EmailMassSender
     */
    public function setNumSending($numSending)
    {
        $this->numSending = $numSending;
    
        return $this;
    }

    /**
     * Get numSending
     *
     * @return integer 
     */
    public function getNumSending()
    {
        return $this->numSending;
    }

    /**
     * Set numSent
     *
     * @param integer $numSent
     * @return EmailMassSender
     */
    public function setNumSent($numSent)
    {
        $this->numSent = $numSent;
    
        return $this;
    }

    /**
     * Get numSent
     *
     * @return integer 
     */
    public function getNumSent()
    {
        return $this->numSent;
    }

    /**
     * Set numError
     *
     * @param integer $numError
     * @return EmailMassSender
     */
    public function setNumError($numError)
    {
        $this->numError = $numError;
    
        return $this;
    }

    /**
     * Get numError
     *
     * @return integer 
     */
    public function getNumError()
    {
        return $this->numError;
    }

    /**
     * Set numRead
     *
     * @param integer $numRead
     * @return EmailMassSender
     */
    public function setNumRead($numRead)
    {
        $this->numRead = $numRead;
    
        return $this;
    }

    /**
     * Get numRead
     *
     * @return integer 
     */
    public function getNumRead()
    {
        return $this->numRead;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return EmailMassSender
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}