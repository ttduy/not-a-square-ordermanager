<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CryosaveSample
 *
 * @ORM\Table(name="cryosave_sample")
 * @ORM\Entity
 */
class CryosaveSample
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idLaboratory
     *
     * @ORM\Column(name="id_laboratory", type="integer", nullable=true)
     */
    private $idLaboratory;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string $sampleCode
     *
     * @ORM\Column(name="sample_code", type="string", length=50, nullable=true)
     */
    private $sampleCode;

    /**
     * @var integer $idSex
     *
     * @ORM\Column(name="id_sex", type="integer", nullable=true)
     */
    private $idSex;

    /**
     * @var \DateTime $birthday
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var integer $idEthnicity
     *
     * @ORM\Column(name="id_ethnicity", type="integer", nullable=true)
     */
    private $idEthnicity;

    /**
     * @var integer $idOrder
     *
     * @ORM\Column(name="id_order", type="integer", nullable=true)
     */
    private $idOrder;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var integer $idSampleOrigin
     *
     * @ORM\Column(name="id_sample_origin", type="integer", nullable=true)
     */
    private $idSampleOrigin;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="date", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var string $reportFilename
     *
     * @ORM\Column(name="report_filename", type="string", length=255, nullable=true)
     */
    private $reportFilename;

    /**
     * @var integer $isUploaded
     *
     * @ORM\Column(name="is_uploaded", type="integer", nullable=true)
     */
    private $isUploaded;

    /**
     * @var string $statusList
     *
     * @ORM\Column(name="status_list", type="text", nullable=true)
     */
    private $statusList;

    /**
     * @var \DateTime $dateOfSample
     *
     * @ORM\Column(name="date_of_sample", type="datetime", nullable=true)
     */
    private $dateOfSample;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLaboratory
     *
     * @param integer $idLaboratory
     * @return CryosaveSample
     */
    public function setIdLaboratory($idLaboratory)
    {
        $this->idLaboratory = $idLaboratory;
    
        return $this;
    }

    /**
     * Get idLaboratory
     *
     * @return integer 
     */
    public function getIdLaboratory()
    {
        return $this->idLaboratory;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return CryosaveSample
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set sampleCode
     *
     * @param string $sampleCode
     * @return CryosaveSample
     */
    public function setSampleCode($sampleCode)
    {
        $this->sampleCode = $sampleCode;
    
        return $this;
    }

    /**
     * Get sampleCode
     *
     * @return string 
     */
    public function getSampleCode()
    {
        return $this->sampleCode;
    }

    /**
     * Set idSex
     *
     * @param integer $idSex
     * @return CryosaveSample
     */
    public function setIdSex($idSex)
    {
        $this->idSex = $idSex;
    
        return $this;
    }

    /**
     * Get idSex
     *
     * @return integer 
     */
    public function getIdSex()
    {
        return $this->idSex;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return CryosaveSample
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set idEthnicity
     *
     * @param integer $idEthnicity
     * @return CryosaveSample
     */
    public function setIdEthnicity($idEthnicity)
    {
        $this->idEthnicity = $idEthnicity;
    
        return $this;
    }

    /**
     * Get idEthnicity
     *
     * @return integer 
     */
    public function getIdEthnicity()
    {
        return $this->idEthnicity;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     * @return CryosaveSample
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    
        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer 
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return CryosaveSample
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set idSampleOrigin
     *
     * @param integer $idSampleOrigin
     * @return CryosaveSample
     */
    public function setIdSampleOrigin($idSampleOrigin)
    {
        $this->idSampleOrigin = $idSampleOrigin;
    
        return $this;
    }

    /**
     * Get idSampleOrigin
     *
     * @return integer 
     */
    public function getIdSampleOrigin()
    {
        return $this->idSampleOrigin;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return CryosaveSample
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return CryosaveSample
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set reportFilename
     *
     * @param string $reportFilename
     * @return CryosaveSample
     */
    public function setReportFilename($reportFilename)
    {
        $this->reportFilename = $reportFilename;
    
        return $this;
    }

    /**
     * Get reportFilename
     *
     * @return string 
     */
    public function getReportFilename()
    {
        return $this->reportFilename;
    }

    /**
     * Set isUploaded
     *
     * @param integer $isUploaded
     * @return CryosaveSample
     */
    public function setIsUploaded($isUploaded)
    {
        $this->isUploaded = $isUploaded;
    
        return $this;
    }

    /**
     * Get isUploaded
     *
     * @return integer 
     */
    public function getIsUploaded()
    {
        return $this->isUploaded;
    }

    /**
     * Set statusList
     *
     * @param string $statusList
     * @return CryosaveSample
     */
    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    
        return $this;
    }

    /**
     * Get statusList
     *
     * @return string 
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * Set dateOfSample
     *
     * @param \DateTime $dateOfSample
     * @return CryosaveSample
     */
    public function setDateOfSample($dateOfSample)
    {
        $this->dateOfSample = $dateOfSample;
    
        return $this;
    }

    /**
     * Get dateOfSample
     *
     * @return \DateTime 
     */
    public function getDateOfSample()
    {
        return $this->dateOfSample;
    }
}