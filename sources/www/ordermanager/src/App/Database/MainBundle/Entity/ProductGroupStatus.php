<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProductGroupStatus
 *
 * @ORM\Table(name="product_group_status")
 * @ORM\Entity
 */
class ProductGroupStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $idProductGroup
     *
     * @ORM\Column(name="id_product_group", type="string", length=255, nullable=true)
     */
    private $idProductGroup;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProductGroup
     *
     * @param string $idProductGroup
     * @return ProductGroupStatus
     */
    public function setIdProductGroup($idProductGroup)
    {
        $this->idProductGroup = $idProductGroup;
    
        return $this;
    }

    /**
     * Get idProductGroup
     *
     * @return string 
     */
    public function getIdProductGroup()
    {
        return $this->idProductGroup;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ProductGroupStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}