<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PriceCategories
 *
 * @ORM\Table(name="price_categories")
 * @ORM\Entity
 */
class PriceCategories
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $pricecategory
     *
     * @ORM\Column(name="PriceCategory", type="string", length=255, nullable=true)
     */
    private $pricecategory;

    /**
     * @var integer $listorder
     *
     * @ORM\Column(name="ListOrder", type="integer", nullable=true)
     */
    private $listorder;

    /**
     * @var string $description
     *
     * @ORM\Column(name="Description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=true)
     */
    private $isdeleted;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricecategory
     *
     * @param string $pricecategory
     * @return PriceCategories
     */
    public function setPricecategory($pricecategory)
    {
        $this->pricecategory = $pricecategory;
    
        return $this;
    }

    /**
     * Get pricecategory
     *
     * @return string 
     */
    public function getPricecategory()
    {
        return $this->pricecategory;
    }

    /**
     * Set listorder
     *
     * @param integer $listorder
     * @return PriceCategories
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;
    
        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer 
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PriceCategories
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return PriceCategories
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }
}