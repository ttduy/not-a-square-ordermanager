<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TextBlockLanguage
 *
 * @ORM\Table(name="text_block_language")
 * @ORM\Entity
 */
class TextBlockLanguage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idTextBlock
     *
     * @ORM\Column(name="id_text_block", type="integer", nullable=true)
     */
    private $idTextBlock;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var string $body
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var boolean $isCached
     *
     * @ORM\Column(name="is_cached", type="boolean", nullable=true)
     */
    private $isCached;

    /**
     * @var boolean $isTranslated
     *
     * @ORM\Column(name="is_translated", type="boolean", nullable=true)
     */
    private $isTranslated;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTextBlock
     *
     * @param integer $idTextBlock
     * @return TextBlockLanguage
     */
    public function setIdTextBlock($idTextBlock)
    {
        $this->idTextBlock = $idTextBlock;
    
        return $this;
    }

    /**
     * Get idTextBlock
     *
     * @return integer 
     */
    public function getIdTextBlock()
    {
        return $this->idTextBlock;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return TextBlockLanguage
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return TextBlockLanguage
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set isCached
     *
     * @param boolean $isCached
     * @return TextBlockLanguage
     */
    public function setIsCached($isCached)
    {
        $this->isCached = $isCached;
    
        return $this;
    }

    /**
     * Get isCached
     *
     * @return boolean 
     */
    public function getIsCached()
    {
        return $this->isCached;
    }

    /**
     * Set isTranslated
     *
     * @param boolean $isTranslated
     * @return TextBlockLanguage
     */
    public function setIsTranslated($isTranslated)
    {
        $this->isTranslated = $isTranslated;
    
        return $this;
    }

    /**
     * Get isTranslated
     *
     * @return boolean 
     */
    public function getIsTranslated()
    {
        return $this->isTranslated;
    }
}