<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerReport
 *
 * @ORM\Table(name="customer_report")
 * @ORM\Entity
 */
class CustomerReport
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idLanguage
     *
     * @ORM\Column(name="id_language", type="integer", nullable=true)
     */
    private $idLanguage;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $idFormulaTemplate
     *
     * @ORM\Column(name="id_formula_template", type="integer", nullable=true)
     */
    private $idFormulaTemplate;

    /**
     * @var string $customerData
     *
     * @ORM\Column(name="customer_data", type="text", nullable=true)
     */
    private $customerData;

    /**
     * @var string $deductedData
     *
     * @ORM\Column(name="deducted_data", type="text", nullable=true)
     */
    private $deductedData;

    /**
     * @var string $reportFinalData
     *
     * @ORM\Column(name="report_final_data", type="text", nullable=true)
     */
    private $reportFinalData;

    /**
     * @var string $filename
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var \DateTime $inputTime
     *
     * @ORM\Column(name="input_time", type="datetime", nullable=true)
     */
    private $inputTime;

    /**
     * @var boolean $isVisibleToCustomer
     *
     * @ORM\Column(name="is_visible_to_customer", type="boolean", nullable=true)
     */
    private $isVisibleToCustomer;

    /**
     * @var string $lowResolutionFilename
     *
     * @ORM\Column(name="low_resolution_filename", type="string", length=255, nullable=true)
     */
    private $lowResolutionFilename;

    /**
     * @var string $webCoverFilename
     *
     * @ORM\Column(name="web_cover_filename", type="string", length=255, nullable=true)
     */
    private $webCoverFilename;

    /**
     * @var string $webCoverLowResolutionFilename
     *
     * @ORM\Column(name="web_cover_low_resolution_filename", type="string", length=255, nullable=true)
     */
    private $webCoverLowResolutionFilename;

    /**
     * @var string $bodCoverFilename
     *
     * @ORM\Column(name="bod_cover_filename", type="string", length=255, nullable=true)
     */
    private $bodCoverFilename;

    /**
     * @var string $bodCoverLowResolutionFilename
     *
     * @ORM\Column(name="bod_cover_low_resolution_filename", type="string", length=255, nullable=true)
     */
    private $bodCoverLowResolutionFilename;

    /**
     * @var string $foodTableExcelFilename
     *
     * @ORM\Column(name="food_table_excel_filename", type="string", length=255, nullable=true)
     */
    private $foodTableExcelFilename;

    /**
     * @var string $xmlFilename
     *
     * @ORM\Column(name="xml_filename", type="string", length=255, nullable=true)
     */
    private $xmlFilename;

    /**
     * @var string $foodTableExcelFile2Name
     *
     * @ORM\Column(name="food_table_excel_file_2_name", type="string", length=255, nullable=true)
     */
    private $foodTableExcelFile2Name;

    /**
     * @var boolean $isBackupToS3
     *
     * @ORM\Column(name="is_backup_to_s3", type="boolean", nullable=true)
     */
    private $isBackupToS3;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CustomerReport
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return CustomerReport
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idLanguage
     *
     * @param integer $idLanguage
     * @return CustomerReport
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;
    
        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return integer 
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return CustomerReport
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set idFormulaTemplate
     *
     * @param integer $idFormulaTemplate
     * @return CustomerReport
     */
    public function setIdFormulaTemplate($idFormulaTemplate)
    {
        $this->idFormulaTemplate = $idFormulaTemplate;
    
        return $this;
    }

    /**
     * Get idFormulaTemplate
     *
     * @return integer 
     */
    public function getIdFormulaTemplate()
    {
        return $this->idFormulaTemplate;
    }

    /**
     * Set customerData
     *
     * @param string $customerData
     * @return CustomerReport
     */
    public function setCustomerData($customerData)
    {
        $this->customerData = $customerData;
    
        return $this;
    }

    /**
     * Get customerData
     *
     * @return string 
     */
    public function getCustomerData()
    {
        return $this->customerData;
    }

    /**
     * Set deductedData
     *
     * @param string $deductedData
     * @return CustomerReport
     */
    public function setDeductedData($deductedData)
    {
        $this->deductedData = $deductedData;
    
        return $this;
    }

    /**
     * Get deductedData
     *
     * @return string 
     */
    public function getDeductedData()
    {
        return $this->deductedData;
    }

    /**
     * Set reportFinalData
     *
     * @param string $reportFinalData
     * @return CustomerReport
     */
    public function setReportFinalData($reportFinalData)
    {
        $this->reportFinalData = $reportFinalData;
    
        return $this;
    }

    /**
     * Get reportFinalData
     *
     * @return string 
     */
    public function getReportFinalData()
    {
        return $this->reportFinalData;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return CustomerReport
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set inputTime
     *
     * @param \DateTime $inputTime
     * @return CustomerReport
     */
    public function setInputTime($inputTime)
    {
        $this->inputTime = $inputTime;
    
        return $this;
    }

    /**
     * Get inputTime
     *
     * @return \DateTime 
     */
    public function getInputTime()
    {
        return $this->inputTime;
    }

    /**
     * Set isVisibleToCustomer
     *
     * @param boolean $isVisibleToCustomer
     * @return CustomerReport
     */
    public function setIsVisibleToCustomer($isVisibleToCustomer)
    {
        $this->isVisibleToCustomer = $isVisibleToCustomer;
    
        return $this;
    }

    /**
     * Get isVisibleToCustomer
     *
     * @return boolean 
     */
    public function getIsVisibleToCustomer()
    {
        return $this->isVisibleToCustomer;
    }

    /**
     * Set lowResolutionFilename
     *
     * @param string $lowResolutionFilename
     * @return CustomerReport
     */
    public function setLowResolutionFilename($lowResolutionFilename)
    {
        $this->lowResolutionFilename = $lowResolutionFilename;
    
        return $this;
    }

    /**
     * Get lowResolutionFilename
     *
     * @return string 
     */
    public function getLowResolutionFilename()
    {
        return $this->lowResolutionFilename;
    }

    /**
     * Set webCoverFilename
     *
     * @param string $webCoverFilename
     * @return CustomerReport
     */
    public function setWebCoverFilename($webCoverFilename)
    {
        $this->webCoverFilename = $webCoverFilename;
    
        return $this;
    }

    /**
     * Get webCoverFilename
     *
     * @return string 
     */
    public function getWebCoverFilename()
    {
        return $this->webCoverFilename;
    }

    /**
     * Set webCoverLowResolutionFilename
     *
     * @param string $webCoverLowResolutionFilename
     * @return CustomerReport
     */
    public function setWebCoverLowResolutionFilename($webCoverLowResolutionFilename)
    {
        $this->webCoverLowResolutionFilename = $webCoverLowResolutionFilename;
    
        return $this;
    }

    /**
     * Get webCoverLowResolutionFilename
     *
     * @return string 
     */
    public function getWebCoverLowResolutionFilename()
    {
        return $this->webCoverLowResolutionFilename;
    }

    /**
     * Set bodCoverFilename
     *
     * @param string $bodCoverFilename
     * @return CustomerReport
     */
    public function setBodCoverFilename($bodCoverFilename)
    {
        $this->bodCoverFilename = $bodCoverFilename;
    
        return $this;
    }

    /**
     * Get bodCoverFilename
     *
     * @return string 
     */
    public function getBodCoverFilename()
    {
        return $this->bodCoverFilename;
    }

    /**
     * Set bodCoverLowResolutionFilename
     *
     * @param string $bodCoverLowResolutionFilename
     * @return CustomerReport
     */
    public function setBodCoverLowResolutionFilename($bodCoverLowResolutionFilename)
    {
        $this->bodCoverLowResolutionFilename = $bodCoverLowResolutionFilename;
    
        return $this;
    }

    /**
     * Get bodCoverLowResolutionFilename
     *
     * @return string 
     */
    public function getBodCoverLowResolutionFilename()
    {
        return $this->bodCoverLowResolutionFilename;
    }

    /**
     * Set foodTableExcelFilename
     *
     * @param string $foodTableExcelFilename
     * @return CustomerReport
     */
    public function setFoodTableExcelFilename($foodTableExcelFilename)
    {
        $this->foodTableExcelFilename = $foodTableExcelFilename;
    
        return $this;
    }

    /**
     * Get foodTableExcelFilename
     *
     * @return string 
     */
    public function getFoodTableExcelFilename()
    {
        return $this->foodTableExcelFilename;
    }

    /**
     * Set xmlFilename
     *
     * @param string $xmlFilename
     * @return CustomerReport
     */
    public function setXmlFilename($xmlFilename)
    {
        $this->xmlFilename = $xmlFilename;
    
        return $this;
    }

    /**
     * Get xmlFilename
     *
     * @return string 
     */
    public function getXmlFilename()
    {
        return $this->xmlFilename;
    }

    /**
     * Set foodTableExcelFile2Name
     *
     * @param string $foodTableExcelFile2Name
     * @return CustomerReport
     */
    public function setFoodTableExcelFile2Name($foodTableExcelFile2Name)
    {
        $this->foodTableExcelFile2Name = $foodTableExcelFile2Name;
    
        return $this;
    }

    /**
     * Get foodTableExcelFile2Name
     *
     * @return string 
     */
    public function getFoodTableExcelFile2Name()
    {
        return $this->foodTableExcelFile2Name;
    }

    /**
     * Set isBackupToS3
     *
     * @param boolean $isBackupToS3
     * @return CustomerReport
     */
    public function setIsBackupToS3($isBackupToS3)
    {
        $this->isBackupToS3 = $isBackupToS3;
    
        return $this;
    }

    /**
     * Get isBackupToS3
     *
     * @return boolean 
     */
    public function getIsBackupToS3()
    {
        return $this->isBackupToS3;
    }
}