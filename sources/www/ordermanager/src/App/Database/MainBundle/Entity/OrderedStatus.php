<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderedStatus
 *
 * @ORM\Table(name="ordered_status")
 * @ORM\Entity
 */
class OrderedStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=true)
     */
    private $customerid;

    /**
     * @var \DateTime $statusdate
     *
     * @ORM\Column(name="StatusDate", type="datetime", nullable=true)
     */
    private $statusdate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var boolean $iscompleted
     *
     * @ORM\Column(name="IsCompleted", type="boolean", nullable=true)
     */
    private $iscompleted;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=true)
     */
    private $sortorder;

    /**
     * @var boolean $isNotFilterable
     *
     * @ORM\Column(name="is_not_filterable", type="boolean", nullable=true)
     */
    private $isNotFilterable;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return OrderedStatus
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set statusdate
     *
     * @param \DateTime $statusdate
     * @return OrderedStatus
     */
    public function setStatusdate($statusdate)
    {
        $this->statusdate = $statusdate;
    
        return $this;
    }

    /**
     * Get statusdate
     *
     * @return \DateTime 
     */
    public function getStatusdate()
    {
        return $this->statusdate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OrderedStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set iscompleted
     *
     * @param boolean $iscompleted
     * @return OrderedStatus
     */
    public function setIscompleted($iscompleted)
    {
        $this->iscompleted = $iscompleted;
    
        return $this;
    }

    /**
     * Get iscompleted
     *
     * @return boolean 
     */
    public function getIscompleted()
    {
        return $this->iscompleted;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return OrderedStatus
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * Set isNotFilterable
     *
     * @param boolean $isNotFilterable
     * @return OrderedStatus
     */
    public function setIsNotFilterable($isNotFilterable)
    {
        $this->isNotFilterable = $isNotFilterable;
    
        return $this;
    }

    /**
     * Get isNotFilterable
     *
     * @return boolean 
     */
    public function getIsNotFilterable()
    {
        return $this->isNotFilterable;
    }
}