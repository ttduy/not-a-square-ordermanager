<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Drug
 *
 * @ORM\Table(name="drug")
 * @ORM\Entity
 */
class Drug
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $drugName
     *
     * @ORM\Column(name="drug_name", type="string", length=255, nullable=true)
     */
    private $drugName;

    /**
     * @var string $cardioDrugName
     *
     * @ORM\Column(name="cardio_drug_name", type="string", length=255, nullable=true)
     */
    private $cardioDrugName;

    /**
     * @var string $pubchemId
     *
     * @ORM\Column(name="pubchem_id", type="string", length=255, nullable=true)
     */
    private $pubchemId;

    /**
     * @var string $drugBankId
     *
     * @ORM\Column(name="drug_bank_id", type="string", length=255, nullable=true)
     */
    private $drugBankId;

    /**
     * @var string $mainPathwayLink
     *
     * @ORM\Column(name="main_pathway_link", type="string", length=255, nullable=true)
     */
    private $mainPathwayLink;

    /**
     * @var string $level1
     *
     * @ORM\Column(name="level_1", type="string", length=255, nullable=true)
     */
    private $level1;

    /**
     * @var string $level2
     *
     * @ORM\Column(name="level_2", type="string", length=255, nullable=true)
     */
    private $level2;

    /**
     * @var string $level3
     *
     * @ORM\Column(name="level_3", type="string", length=255, nullable=true)
     */
    private $level3;

    /**
     * @var string $level4
     *
     * @ORM\Column(name="level_4", type="string", length=255, nullable=true)
     */
    private $level4;

    /**
     * @var boolean $cypInteraction
     *
     * @ORM\Column(name="cyp_interaction", type="boolean", nullable=true)
     */
    private $cypInteraction;

    /**
     * @var float $cypBreakDown
     *
     * @ORM\Column(name="cyp_break_down", type="float", nullable=true)
     */
    private $cypBreakDown;

    /**
     * @var float $halfLifeH
     *
     * @ORM\Column(name="half_life_h", type="float", nullable=true)
     */
    private $halfLifeH;

    /**
     * @var string $referencesCombined
     *
     * @ORM\Column(name="references_combined", type="string", length=255, nullable=true)
     */
    private $referencesCombined;

    /**
     * @var string $ingredients
     *
     * @ORM\Column(name="ingredients", type="text", nullable=true)
     */
    private $ingredients;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set drugName
     *
     * @param string $drugName
     * @return Drug
     */
    public function setDrugName($drugName)
    {
        $this->drugName = $drugName;
    
        return $this;
    }

    /**
     * Get drugName
     *
     * @return string 
     */
    public function getDrugName()
    {
        return $this->drugName;
    }

    /**
     * Set cardioDrugName
     *
     * @param string $cardioDrugName
     * @return Drug
     */
    public function setCardioDrugName($cardioDrugName)
    {
        $this->cardioDrugName = $cardioDrugName;
    
        return $this;
    }

    /**
     * Get cardioDrugName
     *
     * @return string 
     */
    public function getCardioDrugName()
    {
        return $this->cardioDrugName;
    }

    /**
     * Set pubchemId
     *
     * @param string $pubchemId
     * @return Drug
     */
    public function setPubchemId($pubchemId)
    {
        $this->pubchemId = $pubchemId;
    
        return $this;
    }

    /**
     * Get pubchemId
     *
     * @return string 
     */
    public function getPubchemId()
    {
        return $this->pubchemId;
    }

    /**
     * Set drugBankId
     *
     * @param string $drugBankId
     * @return Drug
     */
    public function setDrugBankId($drugBankId)
    {
        $this->drugBankId = $drugBankId;
    
        return $this;
    }

    /**
     * Get drugBankId
     *
     * @return string 
     */
    public function getDrugBankId()
    {
        return $this->drugBankId;
    }

    /**
     * Set mainPathwayLink
     *
     * @param string $mainPathwayLink
     * @return Drug
     */
    public function setMainPathwayLink($mainPathwayLink)
    {
        $this->mainPathwayLink = $mainPathwayLink;
    
        return $this;
    }

    /**
     * Get mainPathwayLink
     *
     * @return string 
     */
    public function getMainPathwayLink()
    {
        return $this->mainPathwayLink;
    }

    /**
     * Set level1
     *
     * @param string $level1
     * @return Drug
     */
    public function setLevel1($level1)
    {
        $this->level1 = $level1;
    
        return $this;
    }

    /**
     * Get level1
     *
     * @return string 
     */
    public function getLevel1()
    {
        return $this->level1;
    }

    /**
     * Set level2
     *
     * @param string $level2
     * @return Drug
     */
    public function setLevel2($level2)
    {
        $this->level2 = $level2;
    
        return $this;
    }

    /**
     * Get level2
     *
     * @return string 
     */
    public function getLevel2()
    {
        return $this->level2;
    }

    /**
     * Set level3
     *
     * @param string $level3
     * @return Drug
     */
    public function setLevel3($level3)
    {
        $this->level3 = $level3;
    
        return $this;
    }

    /**
     * Get level3
     *
     * @return string 
     */
    public function getLevel3()
    {
        return $this->level3;
    }

    /**
     * Set level4
     *
     * @param string $level4
     * @return Drug
     */
    public function setLevel4($level4)
    {
        $this->level4 = $level4;
    
        return $this;
    }

    /**
     * Get level4
     *
     * @return string 
     */
    public function getLevel4()
    {
        return $this->level4;
    }

    /**
     * Set cypInteraction
     *
     * @param boolean $cypInteraction
     * @return Drug
     */
    public function setCypInteraction($cypInteraction)
    {
        $this->cypInteraction = $cypInteraction;
    
        return $this;
    }

    /**
     * Get cypInteraction
     *
     * @return boolean 
     */
    public function getCypInteraction()
    {
        return $this->cypInteraction;
    }

    /**
     * Set cypBreakDown
     *
     * @param float $cypBreakDown
     * @return Drug
     */
    public function setCypBreakDown($cypBreakDown)
    {
        $this->cypBreakDown = $cypBreakDown;
    
        return $this;
    }

    /**
     * Get cypBreakDown
     *
     * @return float 
     */
    public function getCypBreakDown()
    {
        return $this->cypBreakDown;
    }

    /**
     * Set halfLifeH
     *
     * @param float $halfLifeH
     * @return Drug
     */
    public function setHalfLifeH($halfLifeH)
    {
        $this->halfLifeH = $halfLifeH;
    
        return $this;
    }

    /**
     * Get halfLifeH
     *
     * @return float 
     */
    public function getHalfLifeH()
    {
        return $this->halfLifeH;
    }

    /**
     * Set referencesCombined
     *
     * @param string $referencesCombined
     * @return Drug
     */
    public function setReferencesCombined($referencesCombined)
    {
        $this->referencesCombined = $referencesCombined;
    
        return $this;
    }

    /**
     * Get referencesCombined
     *
     * @return string 
     */
    public function getReferencesCombined()
    {
        return $this->referencesCombined;
    }

    /**
     * Set ingredients
     *
     * @param string $ingredients
     * @return Drug
     */
    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;
    
        return $this;
    }

    /**
     * Get ingredients
     *
     * @return string 
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}