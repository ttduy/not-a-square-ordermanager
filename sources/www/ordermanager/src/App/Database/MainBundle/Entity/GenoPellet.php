<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPellet
 *
 * @ORM\Table(name="geno_pellet")
 * @ORM\Entity
 */
class GenoPellet
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $expiryDateWarningThreshold
     *
     * @ORM\Column(name="expiry_date_warning_threshold", type="integer", nullable=true)
     */
    private $expiryDateWarningThreshold;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $letter
     *
     * @ORM\Column(name="letter", type="string", length=10, nullable=true)
     */
    private $letter;

    /**
     * @var string $substanceName
     *
     * @ORM\Column(name="substance_name", type="string", length=255, nullable=true)
     */
    private $substanceName;

    /**
     * @var string $activeIngredientName
     *
     * @ORM\Column(name="active_ingredient_name", type="string", length=255, nullable=true)
     */
    private $activeIngredientName;

    /**
     * @var integer $sortOrderMixing
     *
     * @ORM\Column(name="sort_order_mixing", type="integer", nullable=true)
     */
    private $sortOrderMixing;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;

    /**
     * @var string $supplierDeclaration
     *
     * @ORM\Column(name="supplier_declaration", type="text", nullable=true)
     */
    private $supplierDeclaration;

    /**
     * @var float $pricePerGram
     *
     * @ORM\Column(name="price_per_gram", type="float", nullable=true)
     */
    private $pricePerGram;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var integer $createdBy
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime $modifiedDate
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=true)
     */
    private $modifiedDate;

    /**
     * @var integer $modifiedBy
     *
     * @ORM\Column(name="modified_by", type="integer", nullable=true)
     */
    private $modifiedBy;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var string $fileAttachment
     *
     * @ORM\Column(name="file_attachment", type="text", nullable=true)
     */
    private $fileAttachment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expiryDateWarningThreshold
     *
     * @param integer $expiryDateWarningThreshold
     * @return GenoPellet
     */
    public function setExpiryDateWarningThreshold($expiryDateWarningThreshold)
    {
        $this->expiryDateWarningThreshold = $expiryDateWarningThreshold;
    
        return $this;
    }

    /**
     * Get expiryDateWarningThreshold
     *
     * @return integer 
     */
    public function getExpiryDateWarningThreshold()
    {
        return $this->expiryDateWarningThreshold;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return GenoPellet
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GenoPellet
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set letter
     *
     * @param string $letter
     * @return GenoPellet
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;
    
        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set substanceName
     *
     * @param string $substanceName
     * @return GenoPellet
     */
    public function setSubstanceName($substanceName)
    {
        $this->substanceName = $substanceName;
    
        return $this;
    }

    /**
     * Get substanceName
     *
     * @return string 
     */
    public function getSubstanceName()
    {
        return $this->substanceName;
    }

    /**
     * Set activeIngredientName
     *
     * @param string $activeIngredientName
     * @return GenoPellet
     */
    public function setActiveIngredientName($activeIngredientName)
    {
        $this->activeIngredientName = $activeIngredientName;
    
        return $this;
    }

    /**
     * Get activeIngredientName
     *
     * @return string 
     */
    public function getActiveIngredientName()
    {
        return $this->activeIngredientName;
    }

    /**
     * Set sortOrderMixing
     *
     * @param integer $sortOrderMixing
     * @return GenoPellet
     */
    public function setSortOrderMixing($sortOrderMixing)
    {
        $this->sortOrderMixing = $sortOrderMixing;
    
        return $this;
    }

    /**
     * Get sortOrderMixing
     *
     * @return integer 
     */
    public function getSortOrderMixing()
    {
        return $this->sortOrderMixing;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return GenoPellet
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }

    /**
     * Set supplierDeclaration
     *
     * @param string $supplierDeclaration
     * @return GenoPellet
     */
    public function setSupplierDeclaration($supplierDeclaration)
    {
        $this->supplierDeclaration = $supplierDeclaration;
    
        return $this;
    }

    /**
     * Get supplierDeclaration
     *
     * @return string 
     */
    public function getSupplierDeclaration()
    {
        return $this->supplierDeclaration;
    }

    /**
     * Set pricePerGram
     *
     * @param float $pricePerGram
     * @return GenoPellet
     */
    public function setPricePerGram($pricePerGram)
    {
        $this->pricePerGram = $pricePerGram;
    
        return $this;
    }

    /**
     * Get pricePerGram
     *
     * @return float 
     */
    public function getPricePerGram()
    {
        return $this->pricePerGram;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenoPellet
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return GenoPellet
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return GenoPellet
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return GenoPellet
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return GenoPellet
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set fileAttachment
     *
     * @param string $fileAttachment
     * @return GenoPellet
     */
    public function setFileAttachment($fileAttachment)
    {
        $this->fileAttachment = $fileAttachment;
    
        return $this;
    }

    /**
     * Get fileAttachment
     *
     * @return string 
     */
    public function getFileAttachment()
    {
        return $this->fileAttachment;
    }
}