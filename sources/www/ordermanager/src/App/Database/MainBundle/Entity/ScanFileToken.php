<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFileToken
 *
 * @ORM\Table(name="scan_file_token")
 * @ORM\Entity
 */
class ScanFileToken
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFile
     *
     * @ORM\Column(name="id_scan_file", type="integer", nullable=true)
     */
    private $idScanFile;

    /**
     * @var string $tokenKey
     *
     * @ORM\Column(name="token_key", type="string", length=255, nullable=true)
     */
    private $tokenKey;

    /**
     * @var boolean $isCreationRequired
     *
     * @ORM\Column(name="is_creation_required", type="boolean", nullable=true)
     */
    private $isCreationRequired;

    /**
     * @var integer $idTokenType
     *
     * @ORM\Column(name="id_token_type", type="integer", nullable=true)
     */
    private $idTokenType;

    /**
     * @var string $tokenImage
     *
     * @ORM\Column(name="token_image", type="string", length=255, nullable=true)
     */
    private $tokenImage;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string $params
     *
     * @ORM\Column(name="params", type="text", nullable=true)
     */
    private $params;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFile
     *
     * @param integer $idScanFile
     * @return ScanFileToken
     */
    public function setIdScanFile($idScanFile)
    {
        $this->idScanFile = $idScanFile;
    
        return $this;
    }

    /**
     * Get idScanFile
     *
     * @return integer 
     */
    public function getIdScanFile()
    {
        return $this->idScanFile;
    }

    /**
     * Set tokenKey
     *
     * @param string $tokenKey
     * @return ScanFileToken
     */
    public function setTokenKey($tokenKey)
    {
        $this->tokenKey = $tokenKey;
    
        return $this;
    }

    /**
     * Get tokenKey
     *
     * @return string 
     */
    public function getTokenKey()
    {
        return $this->tokenKey;
    }

    /**
     * Set isCreationRequired
     *
     * @param boolean $isCreationRequired
     * @return ScanFileToken
     */
    public function setIsCreationRequired($isCreationRequired)
    {
        $this->isCreationRequired = $isCreationRequired;
    
        return $this;
    }

    /**
     * Get isCreationRequired
     *
     * @return boolean 
     */
    public function getIsCreationRequired()
    {
        return $this->isCreationRequired;
    }

    /**
     * Set idTokenType
     *
     * @param integer $idTokenType
     * @return ScanFileToken
     */
    public function setIdTokenType($idTokenType)
    {
        $this->idTokenType = $idTokenType;
    
        return $this;
    }

    /**
     * Get idTokenType
     *
     * @return integer 
     */
    public function getIdTokenType()
    {
        return $this->idTokenType;
    }

    /**
     * Set tokenImage
     *
     * @param string $tokenImage
     * @return ScanFileToken
     */
    public function setTokenImage($tokenImage)
    {
        $this->tokenImage = $tokenImage;
    
        return $this;
    }

    /**
     * Get tokenImage
     *
     * @return string 
     */
    public function getTokenImage()
    {
        return $this->tokenImage;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return ScanFileToken
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return ScanFileToken
     */
    public function setParams($params)
    {
        $this->params = $params;
    
        return $this;
    }

    /**
     * Get params
     *
     * @return string 
     */
    public function getParams()
    {
        return $this->params;
    }
}