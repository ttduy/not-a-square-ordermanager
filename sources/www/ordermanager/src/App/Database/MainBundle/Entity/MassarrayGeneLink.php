<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MassarrayGeneLink
 *
 * @ORM\Table(name="massarray_gene_link")
 * @ORM\Entity
 */
class MassarrayGeneLink
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $massarrayid
     *
     * @ORM\Column(name="MassarrayId", type="integer", nullable=true)
     */
    private $massarrayid;

    /**
     * @var integer $geneid
     *
     * @ORM\Column(name="GeneId", type="integer", nullable=true)
     */
    private $geneid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set massarrayid
     *
     * @param integer $massarrayid
     * @return MassarrayGeneLink
     */
    public function setMassarrayid($massarrayid)
    {
        $this->massarrayid = $massarrayid;
    
        return $this;
    }

    /**
     * Get massarrayid
     *
     * @return integer 
     */
    public function getMassarrayid()
    {
        return $this->massarrayid;
    }

    /**
     * Set geneid
     *
     * @param integer $geneid
     * @return MassarrayGeneLink
     */
    public function setGeneid($geneid)
    {
        $this->geneid = $geneid;
    
        return $this;
    }

    /**
     * Get geneid
     *
     * @return integer 
     */
    public function getGeneid()
    {
        return $this->geneid;
    }
}