<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SyncRequest
 *
 * @ORM\Table(name="sync_request")
 * @ORM\Entity
 */
class SyncRequest
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idUser
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var \DateTime $lastUpdated
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     */
    private $lastUpdated;

    /**
     * @var boolean $isSyncTextBlock
     *
     * @ORM\Column(name="is_sync_text_block", type="boolean", nullable=true)
     */
    private $isSyncTextBlock;

    /**
     * @var boolean $isSyncResources
     *
     * @ORM\Column(name="is_sync_resources", type="boolean", nullable=true)
     */
    private $isSyncResources;

    /**
     * @var integer $isSyncTextBlockStatus
     *
     * @ORM\Column(name="is_sync_text_block_status", type="integer", nullable=true)
     */
    private $isSyncTextBlockStatus;

    /**
     * @var integer $isSyncResourcesStatus
     *
     * @ORM\Column(name="is_sync_resources_status", type="integer", nullable=true)
     */
    private $isSyncResourcesStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return SyncRequest
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return SyncRequest
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     * @return SyncRequest
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    
        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime 
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * Set isSyncTextBlock
     *
     * @param boolean $isSyncTextBlock
     * @return SyncRequest
     */
    public function setIsSyncTextBlock($isSyncTextBlock)
    {
        $this->isSyncTextBlock = $isSyncTextBlock;
    
        return $this;
    }

    /**
     * Get isSyncTextBlock
     *
     * @return boolean 
     */
    public function getIsSyncTextBlock()
    {
        return $this->isSyncTextBlock;
    }

    /**
     * Set isSyncResources
     *
     * @param boolean $isSyncResources
     * @return SyncRequest
     */
    public function setIsSyncResources($isSyncResources)
    {
        $this->isSyncResources = $isSyncResources;
    
        return $this;
    }

    /**
     * Get isSyncResources
     *
     * @return boolean 
     */
    public function getIsSyncResources()
    {
        return $this->isSyncResources;
    }

    /**
     * Set isSyncTextBlockStatus
     *
     * @param integer $isSyncTextBlockStatus
     * @return SyncRequest
     */
    public function setIsSyncTextBlockStatus($isSyncTextBlockStatus)
    {
        $this->isSyncTextBlockStatus = $isSyncTextBlockStatus;
    
        return $this;
    }

    /**
     * Get isSyncTextBlockStatus
     *
     * @return integer 
     */
    public function getIsSyncTextBlockStatus()
    {
        return $this->isSyncTextBlockStatus;
    }

    /**
     * Set isSyncResourcesStatus
     *
     * @param integer $isSyncResourcesStatus
     * @return SyncRequest
     */
    public function setIsSyncResourcesStatus($isSyncResourcesStatus)
    {
        $this->isSyncResourcesStatus = $isSyncResourcesStatus;
    
        return $this;
    }

    /**
     * Get isSyncResourcesStatus
     *
     * @return integer 
     */
    public function getIsSyncResourcesStatus()
    {
        return $this->isSyncResourcesStatus;
    }
}