<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormRecord
 *
 * @ORM\Table(name="scan_form_record")
 * @ORM\Entity
 */
class ScanFormRecord
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormBatch
     *
     * @ORM\Column(name="id_scan_form_batch", type="integer", nullable=true)
     */
    private $idScanFormBatch;

    /**
     * @var \DateTime $inputTimestamp
     *
     * @ORM\Column(name="input_timestamp", type="datetime", nullable=true)
     */
    private $inputTimestamp;

    /**
     * @var string $imageFilename
     *
     * @ORM\Column(name="image_filename", type="string", length=255, nullable=true)
     */
    private $imageFilename;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $idScanFormRecognized
     *
     * @ORM\Column(name="id_scan_form_recognized", type="integer", nullable=true)
     */
    private $idScanFormRecognized;

    /**
     * @var string $formData
     *
     * @ORM\Column(name="form_data", type="text", nullable=true)
     */
    private $formData;

    /**
     * @var integer $verifyFormStatus
     *
     * @ORM\Column(name="verify_form_status", type="integer", nullable=true)
     */
    private $verifyFormStatus;

    /**
     * @var integer $verifyTextboxStatus
     *
     * @ORM\Column(name="verify_textbox_status", type="integer", nullable=true)
     */
    private $verifyTextboxStatus;

    /**
     * @var integer $verifyTextareaStatus
     *
     * @ORM\Column(name="verify_textarea_status", type="integer", nullable=true)
     */
    private $verifyTextareaStatus;

    /**
     * @var integer $verifyCheckboxStatus
     *
     * @ORM\Column(name="verify_checkbox_status", type="integer", nullable=true)
     */
    private $verifyCheckboxStatus;

    /**
     * @var integer $verifyProcessedStatus
     *
     * @ORM\Column(name="verify_processed_status", type="integer", nullable=true)
     */
    private $verifyProcessedStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormBatch
     *
     * @param integer $idScanFormBatch
     * @return ScanFormRecord
     */
    public function setIdScanFormBatch($idScanFormBatch)
    {
        $this->idScanFormBatch = $idScanFormBatch;
    
        return $this;
    }

    /**
     * Get idScanFormBatch
     *
     * @return integer 
     */
    public function getIdScanFormBatch()
    {
        return $this->idScanFormBatch;
    }

    /**
     * Set inputTimestamp
     *
     * @param \DateTime $inputTimestamp
     * @return ScanFormRecord
     */
    public function setInputTimestamp($inputTimestamp)
    {
        $this->inputTimestamp = $inputTimestamp;
    
        return $this;
    }

    /**
     * Get inputTimestamp
     *
     * @return \DateTime 
     */
    public function getInputTimestamp()
    {
        return $this->inputTimestamp;
    }

    /**
     * Set imageFilename
     *
     * @param string $imageFilename
     * @return ScanFormRecord
     */
    public function setImageFilename($imageFilename)
    {
        $this->imageFilename = $imageFilename;
    
        return $this;
    }

    /**
     * Get imageFilename
     *
     * @return string 
     */
    public function getImageFilename()
    {
        return $this->imageFilename;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ScanFormRecord
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idScanFormRecognized
     *
     * @param integer $idScanFormRecognized
     * @return ScanFormRecord
     */
    public function setIdScanFormRecognized($idScanFormRecognized)
    {
        $this->idScanFormRecognized = $idScanFormRecognized;
    
        return $this;
    }

    /**
     * Get idScanFormRecognized
     *
     * @return integer 
     */
    public function getIdScanFormRecognized()
    {
        return $this->idScanFormRecognized;
    }

    /**
     * Set formData
     *
     * @param string $formData
     * @return ScanFormRecord
     */
    public function setFormData($formData)
    {
        $this->formData = $formData;
    
        return $this;
    }

    /**
     * Get formData
     *
     * @return string 
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * Set verifyFormStatus
     *
     * @param integer $verifyFormStatus
     * @return ScanFormRecord
     */
    public function setVerifyFormStatus($verifyFormStatus)
    {
        $this->verifyFormStatus = $verifyFormStatus;
    
        return $this;
    }

    /**
     * Get verifyFormStatus
     *
     * @return integer 
     */
    public function getVerifyFormStatus()
    {
        return $this->verifyFormStatus;
    }

    /**
     * Set verifyTextboxStatus
     *
     * @param integer $verifyTextboxStatus
     * @return ScanFormRecord
     */
    public function setVerifyTextboxStatus($verifyTextboxStatus)
    {
        $this->verifyTextboxStatus = $verifyTextboxStatus;
    
        return $this;
    }

    /**
     * Get verifyTextboxStatus
     *
     * @return integer 
     */
    public function getVerifyTextboxStatus()
    {
        return $this->verifyTextboxStatus;
    }

    /**
     * Set verifyTextareaStatus
     *
     * @param integer $verifyTextareaStatus
     * @return ScanFormRecord
     */
    public function setVerifyTextareaStatus($verifyTextareaStatus)
    {
        $this->verifyTextareaStatus = $verifyTextareaStatus;
    
        return $this;
    }

    /**
     * Get verifyTextareaStatus
     *
     * @return integer 
     */
    public function getVerifyTextareaStatus()
    {
        return $this->verifyTextareaStatus;
    }

    /**
     * Set verifyCheckboxStatus
     *
     * @param integer $verifyCheckboxStatus
     * @return ScanFormRecord
     */
    public function setVerifyCheckboxStatus($verifyCheckboxStatus)
    {
        $this->verifyCheckboxStatus = $verifyCheckboxStatus;
    
        return $this;
    }

    /**
     * Get verifyCheckboxStatus
     *
     * @return integer 
     */
    public function getVerifyCheckboxStatus()
    {
        return $this->verifyCheckboxStatus;
    }

    /**
     * Set verifyProcessedStatus
     *
     * @param integer $verifyProcessedStatus
     * @return ScanFormRecord
     */
    public function setVerifyProcessedStatus($verifyProcessedStatus)
    {
        $this->verifyProcessedStatus = $verifyProcessedStatus;
    
        return $this;
    }

    /**
     * Get verifyProcessedStatus
     *
     * @return integer 
     */
    public function getVerifyProcessedStatus()
    {
        return $this->verifyProcessedStatus;
    }
}