<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MatType
 *
 * @ORM\Table(name="mat_type")
 * @ORM\Entity
 */
class MatType
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $mattype
     *
     * @ORM\Column(name="MatType", type="string", length=255, nullable=true)
     */
    private $mattype;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mattype
     *
     * @param string $mattype
     * @return MatType
     */
    public function setMattype($mattype)
    {
        $this->mattype = $mattype;
    
        return $this;
    }

    /**
     * Get mattype
     *
     * @return string 
     */
    public function getMattype()
    {
        return $this->mattype;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return MatType
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return MatType
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }
}