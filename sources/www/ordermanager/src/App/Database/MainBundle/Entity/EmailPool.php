<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPool
 *
 * @ORM\Table(name="email_pool")
 * @ORM\Entity
 */
class EmailPool
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $numSuccess
     *
     * @ORM\Column(name="num_success", type="integer", nullable=true)
     */
    private $numSuccess;

    /**
     * @var integer $numFailed
     *
     * @ORM\Column(name="num_failed", type="integer", nullable=true)
     */
    private $numFailed;

    /**
     * @var integer $numPending
     *
     * @ORM\Column(name="num_pending", type="integer", nullable=true)
     */
    private $numPending;

    /**
     * @var integer $numView
     *
     * @ORM\Column(name="num_view", type="integer", nullable=true)
     */
    private $numView;

    /**
     * @var integer $numIgnore
     *
     * @ORM\Column(name="num_ignore", type="integer", nullable=true)
     */
    private $numIgnore;

    /**
     * @var integer $numSending
     *
     * @ORM\Column(name="num_sending", type="integer", nullable=true)
     */
    private $numSending;

    /**
     * @var integer $numSent
     *
     * @ORM\Column(name="num_sent", type="integer", nullable=true)
     */
    private $numSent;

    /**
     * @var integer $numQueued
     *
     * @ORM\Column(name="num_queued", type="integer", nullable=true)
     */
    private $numQueued;

    /**
     * @var integer $numScheduled
     *
     * @ORM\Column(name="num_scheduled", type="integer", nullable=true)
     */
    private $numScheduled;

    /**
     * @var integer $numRejected
     *
     * @ORM\Column(name="num_rejected", type="integer", nullable=true)
     */
    private $numRejected;

    /**
     * @var integer $numInvalid
     *
     * @ORM\Column(name="num_invalid", type="integer", nullable=true)
     */
    private $numInvalid;

    /**
     * @var integer $numIgnored
     *
     * @ORM\Column(name="num_ignored", type="integer", nullable=true)
     */
    private $numIgnored;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailPool
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailPool
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailPool
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return EmailPool
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set numSuccess
     *
     * @param integer $numSuccess
     * @return EmailPool
     */
    public function setNumSuccess($numSuccess)
    {
        $this->numSuccess = $numSuccess;
    
        return $this;
    }

    /**
     * Get numSuccess
     *
     * @return integer 
     */
    public function getNumSuccess()
    {
        return $this->numSuccess;
    }

    /**
     * Set numFailed
     *
     * @param integer $numFailed
     * @return EmailPool
     */
    public function setNumFailed($numFailed)
    {
        $this->numFailed = $numFailed;
    
        return $this;
    }

    /**
     * Get numFailed
     *
     * @return integer 
     */
    public function getNumFailed()
    {
        return $this->numFailed;
    }

    /**
     * Set numPending
     *
     * @param integer $numPending
     * @return EmailPool
     */
    public function setNumPending($numPending)
    {
        $this->numPending = $numPending;
    
        return $this;
    }

    /**
     * Get numPending
     *
     * @return integer 
     */
    public function getNumPending()
    {
        return $this->numPending;
    }

    /**
     * Set numView
     *
     * @param integer $numView
     * @return EmailPool
     */
    public function setNumView($numView)
    {
        $this->numView = $numView;
    
        return $this;
    }

    /**
     * Get numView
     *
     * @return integer 
     */
    public function getNumView()
    {
        return $this->numView;
    }

    /**
     * Set numIgnore
     *
     * @param integer $numIgnore
     * @return EmailPool
     */
    public function setNumIgnore($numIgnore)
    {
        $this->numIgnore = $numIgnore;
    
        return $this;
    }

    /**
     * Get numIgnore
     *
     * @return integer 
     */
    public function getNumIgnore()
    {
        return $this->numIgnore;
    }

    /**
     * Set numSending
     *
     * @param integer $numSending
     * @return EmailPool
     */
    public function setNumSending($numSending)
    {
        $this->numSending = $numSending;
    
        return $this;
    }

    /**
     * Get numSending
     *
     * @return integer 
     */
    public function getNumSending()
    {
        return $this->numSending;
    }

    /**
     * Set numSent
     *
     * @param integer $numSent
     * @return EmailPool
     */
    public function setNumSent($numSent)
    {
        $this->numSent = $numSent;
    
        return $this;
    }

    /**
     * Get numSent
     *
     * @return integer 
     */
    public function getNumSent()
    {
        return $this->numSent;
    }

    /**
     * Set numQueued
     *
     * @param integer $numQueued
     * @return EmailPool
     */
    public function setNumQueued($numQueued)
    {
        $this->numQueued = $numQueued;
    
        return $this;
    }

    /**
     * Get numQueued
     *
     * @return integer 
     */
    public function getNumQueued()
    {
        return $this->numQueued;
    }

    /**
     * Set numScheduled
     *
     * @param integer $numScheduled
     * @return EmailPool
     */
    public function setNumScheduled($numScheduled)
    {
        $this->numScheduled = $numScheduled;
    
        return $this;
    }

    /**
     * Get numScheduled
     *
     * @return integer 
     */
    public function getNumScheduled()
    {
        return $this->numScheduled;
    }

    /**
     * Set numRejected
     *
     * @param integer $numRejected
     * @return EmailPool
     */
    public function setNumRejected($numRejected)
    {
        $this->numRejected = $numRejected;
    
        return $this;
    }

    /**
     * Get numRejected
     *
     * @return integer 
     */
    public function getNumRejected()
    {
        return $this->numRejected;
    }

    /**
     * Set numInvalid
     *
     * @param integer $numInvalid
     * @return EmailPool
     */
    public function setNumInvalid($numInvalid)
    {
        $this->numInvalid = $numInvalid;
    
        return $this;
    }

    /**
     * Get numInvalid
     *
     * @return integer 
     */
    public function getNumInvalid()
    {
        return $this->numInvalid;
    }

    /**
     * Set numIgnored
     *
     * @param integer $numIgnored
     * @return EmailPool
     */
    public function setNumIgnored($numIgnored)
    {
        $this->numIgnored = $numIgnored;
    
        return $this;
    }

    /**
     * Get numIgnored
     *
     * @return integer 
     */
    public function getNumIgnored()
    {
        return $this->numIgnored;
    }
}