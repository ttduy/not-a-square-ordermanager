<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportTextBlock
 *
 * @ORM\Table(name="report_text_block")
 * @ORM\Entity
 */
class ReportTextBlock
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $idTextBlock
     *
     * @ORM\Column(name="id_text_block", type="integer", nullable=true)
     */
    private $idTextBlock;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return ReportTextBlock
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set idTextBlock
     *
     * @param integer $idTextBlock
     * @return ReportTextBlock
     */
    public function setIdTextBlock($idTextBlock)
    {
        $this->idTextBlock = $idTextBlock;
    
        return $this;
    }

    /**
     * Get idTextBlock
     *
     * @return integer 
     */
    public function getIdTextBlock()
    {
        return $this->idTextBlock;
    }
}