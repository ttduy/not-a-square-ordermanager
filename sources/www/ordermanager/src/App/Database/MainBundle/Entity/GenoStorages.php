<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoStorages
 *
 * @ORM\Table(name="geno_storages")
 * @ORM\Entity
 */
class GenoStorages
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $loading
     *
     * @ORM\Column(name="loading", type="integer", nullable=true)
     */
    private $loading;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GenoStorages
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return GenoStorages
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set loading
     *
     * @param integer $loading
     * @return GenoStorages
     */
    public function setLoading($loading)
    {
        $this->loading = $loading;
    
        return $this;
    }

    /**
     * Get loading
     *
     * @return integer 
     */
    public function getLoading()
    {
        return $this->loading;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return GenoStorages
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenoStorages
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
}