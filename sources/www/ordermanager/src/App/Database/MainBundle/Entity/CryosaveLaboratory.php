<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CryosaveLaboratory
 *
 * @ORM\Table(name="cryosave_laboratory")
 * @ORM\Entity
 */
class CryosaveLaboratory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var integer $idSampleOrigin
     *
     * @ORM\Column(name="id_sample_origin", type="integer", nullable=true)
     */
    private $idSampleOrigin;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CryosaveLaboratory
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return CryosaveLaboratory
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set idSampleOrigin
     *
     * @param integer $idSampleOrigin
     * @return CryosaveLaboratory
     */
    public function setIdSampleOrigin($idSampleOrigin)
    {
        $this->idSampleOrigin = $idSampleOrigin;
    
        return $this;
    }

    /**
     * Get idSampleOrigin
     *
     * @return integer 
     */
    public function getIdSampleOrigin()
    {
        return $this->idSampleOrigin;
    }
}