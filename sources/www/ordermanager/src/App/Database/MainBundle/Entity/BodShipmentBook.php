<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BodShipmentBook
 *
 * @ORM\Table(name="bod_shipment_book")
 * @ORM\Entity
 */
class BodShipmentBook
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBodShipment
     *
     * @ORM\Column(name="id_bod_shipment", type="integer", nullable=true)
     */
    private $idBodShipment;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var string $reportFile
     *
     * @ORM\Column(name="report_file", type="string", length=255, nullable=true)
     */
    private $reportFile;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBodShipment
     *
     * @param integer $idBodShipment
     * @return BodShipmentBook
     */
    public function setIdBodShipment($idBodShipment)
    {
        $this->idBodShipment = $idBodShipment;
    
        return $this;
    }

    /**
     * Get idBodShipment
     *
     * @return integer 
     */
    public function getIdBodShipment()
    {
        return $this->idBodShipment;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return BodShipmentBook
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set reportFile
     *
     * @param string $reportFile
     * @return BodShipmentBook
     */
    public function setReportFile($reportFile)
    {
        $this->reportFile = $reportFile;
    
        return $this;
    }

    /**
     * Get reportFile
     *
     * @return string 
     */
    public function getReportFile()
    {
        return $this->reportFile;
    }
}