<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WidgetStatisticResult
 *
 * @ORM\Table(name="widget_statistic_result")
 * @ORM\Entity
 */
class WidgetStatisticResult
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWidgetStatistic
     *
     * @ORM\Column(name="id_widget_statistic", type="integer", nullable=true)
     */
    private $idWidgetStatistic;

    /**
     * @var integer $idTimePeriod
     *
     * @ORM\Column(name="id_time_period", type="integer", nullable=true)
     */
    private $idTimePeriod;

    /**
     * @var integer $idCalculate
     *
     * @ORM\Column(name="id_calculate", type="integer", nullable=true)
     */
    private $idCalculate;

    /**
     * @var string $result
     *
     * @ORM\Column(name="result", type="string", length=255, nullable=true)
     */
    private $result;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWidgetStatistic
     *
     * @param integer $idWidgetStatistic
     * @return WidgetStatisticResult
     */
    public function setIdWidgetStatistic($idWidgetStatistic)
    {
        $this->idWidgetStatistic = $idWidgetStatistic;
    
        return $this;
    }

    /**
     * Get idWidgetStatistic
     *
     * @return integer 
     */
    public function getIdWidgetStatistic()
    {
        return $this->idWidgetStatistic;
    }

    /**
     * Set idTimePeriod
     *
     * @param integer $idTimePeriod
     * @return WidgetStatisticResult
     */
    public function setIdTimePeriod($idTimePeriod)
    {
        $this->idTimePeriod = $idTimePeriod;
    
        return $this;
    }

    /**
     * Get idTimePeriod
     *
     * @return integer 
     */
    public function getIdTimePeriod()
    {
        return $this->idTimePeriod;
    }

    /**
     * Set idCalculate
     *
     * @param integer $idCalculate
     * @return WidgetStatisticResult
     */
    public function setIdCalculate($idCalculate)
    {
        $this->idCalculate = $idCalculate;
    
        return $this;
    }

    /**
     * Get idCalculate
     *
     * @return integer 
     */
    public function getIdCalculate()
    {
        return $this->idCalculate;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return WidgetStatisticResult
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }
}