<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\InvoiceExtraText
 *
 * @ORM\Table(name="invoice_extra_text")
 * @ORM\Entity
 */
class InvoiceExtraText
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idInvoice
     *
     * @ORM\Column(name="id_invoice", type="integer", nullable=true)
     */
    private $idInvoice;

    /**
     * @var integer $idPosition
     *
     * @ORM\Column(name="id_position", type="integer", nullable=true)
     */
    private $idPosition;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInvoice
     *
     * @param integer $idInvoice
     * @return InvoiceExtraText
     */
    public function setIdInvoice($idInvoice)
    {
        $this->idInvoice = $idInvoice;
    
        return $this;
    }

    /**
     * Get idInvoice
     *
     * @return integer 
     */
    public function getIdInvoice()
    {
        return $this->idInvoice;
    }

    /**
     * Set idPosition
     *
     * @param integer $idPosition
     * @return InvoiceExtraText
     */
    public function setIdPosition($idPosition)
    {
        $this->idPosition = $idPosition;
    
        return $this;
    }

    /**
     * Get idPosition
     *
     * @return integer 
     */
    public function getIdPosition()
    {
        return $this->idPosition;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return InvoiceExtraText
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return InvoiceExtraText
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}