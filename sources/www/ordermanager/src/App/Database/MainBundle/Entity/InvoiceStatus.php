<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\InvoiceStatus
 *
 * @ORM\Table(name="invoice_status")
 * @ORM\Entity
 */
class InvoiceStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $customerid
     *
     * @ORM\Column(name="CustomerId", type="integer", nullable=true)
     */
    private $customerid;

    /**
     * @var \DateTime $statusdate
     *
     * @ORM\Column(name="StatusDate", type="date", nullable=true)
     */
    private $statusdate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=true)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return InvoiceStatus
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;
    
        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set statusdate
     *
     * @param \DateTime $statusdate
     * @return InvoiceStatus
     */
    public function setStatusdate($statusdate)
    {
        $this->statusdate = $statusdate;
    
        return $this;
    }

    /**
     * Get statusdate
     *
     * @return \DateTime 
     */
    public function getStatusdate()
    {
        return $this->statusdate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return InvoiceStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return InvoiceStatus
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return InvoiceStatus
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}