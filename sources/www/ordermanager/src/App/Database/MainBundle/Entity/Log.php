<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Log
 *
 * @ORM\Table(name="log")
 * @ORM\Entity
 */
class Log
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=false)
     */
    private $idWebUser;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime $logTime
     *
     * @ORM\Column(name="log_time", type="datetime", nullable=false)
     */
    private $logTime;

    /**
     * @var string $logData
     *
     * @ORM\Column(name="log_data", type="text", nullable=false)
     */
    private $logData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return Log
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Log
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set logTime
     *
     * @param \DateTime $logTime
     * @return Log
     */
    public function setLogTime($logTime)
    {
        $this->logTime = $logTime;
    
        return $this;
    }

    /**
     * Get logTime
     *
     * @return \DateTime 
     */
    public function getLogTime()
    {
        return $this->logTime;
    }

    /**
     * Set logData
     *
     * @param string $logData
     * @return Log
     */
    public function setLogData($logData)
    {
        $this->logData = $logData;
    
        return $this;
    }

    /**
     * Get logData
     *
     * @return string 
     */
    public function getLogData()
    {
        return $this->logData;
    }
}