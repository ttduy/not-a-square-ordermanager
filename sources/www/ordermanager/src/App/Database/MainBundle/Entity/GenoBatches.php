<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoBatches
 *
 * @ORM\Table(name="geno_batches")
 * @ORM\Entity
 */
class GenoBatches
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $idGenoMaterial
     *
     * @ORM\Column(name="id_geno_material", type="integer", nullable=true)
     */
    private $idGenoMaterial;

    /**
     * @var integer $location
     *
     * @ORM\Column(name="location", type="integer", nullable=true)
     */
    private $location;

    /**
     * @var integer $amountInStock
     *
     * @ORM\Column(name="amount_in_stock", type="integer", nullable=true)
     */
    private $amountInStock;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var integer $createdBy
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime $modifiedDate
     *
     * @ORM\Column(name="modified_date", type="datetime", nullable=true)
     */
    private $modifiedDate;

    /**
     * @var integer $modifiedBy
     *
     * @ORM\Column(name="modified_by", type="integer", nullable=true)
     */
    private $modifiedBy;

    /**
     * @var integer $orderedBy
     *
     * @ORM\Column(name="ordered_by", type="integer", nullable=true)
     */
    private $orderedBy;

    /**
     * @var integer $orderedPrice
     *
     * @ORM\Column(name="ordered_price", type="integer", nullable=true)
     */
    private $orderedPrice;

    /**
     * @var integer $unitPrice
     *
     * @ORM\Column(name="unit_price", type="integer", nullable=true)
     */
    private $unitPrice;

    /**
     * @var integer $unitQuantity
     *
     * @ORM\Column(name="unit_quantity", type="integer", nullable=true)
     */
    private $unitQuantity;

    /**
     * @var \DateTime $orderedDate
     *
     * @ORM\Column(name="ordered_date", type="datetime", nullable=true)
     */
    private $orderedDate;

    /**
     * @var string $supplierName
     *
     * @ORM\Column(name="supplier_name", type="string", length=255, nullable=true)
     */
    private $supplierName;

    /**
     * @var string $fillers
     *
     * @ORM\Column(name="fillers", type="string", length=255, nullable=true)
     */
    private $fillers;

    /**
     * @var integer $recievedBy
     *
     * @ORM\Column(name="recieved_by", type="integer", nullable=true)
     */
    private $recievedBy;

    /**
     * @var \DateTime $recievedDate
     *
     * @ORM\Column(name="recieved_date", type="datetime", nullable=true)
     */
    private $recievedDate;

    /**
     * @var \DateTime $expiryDate
     *
     * @ORM\Column(name="expiry_date", type="datetime", nullable=true)
     */
    private $expiryDate;

    /**
     * @var float $loading
     *
     * @ORM\Column(name="loading", type="float", nullable=true)
     */
    private $loading;

    /**
     * @var string $externalLot
     *
     * @ORM\Column(name="external_lot", type="string", length=255, nullable=true)
     */
    private $externalLot;

    /**
     * @var boolean $isShipmentOk
     *
     * @ORM\Column(name="is_shipment_ok", type="boolean", nullable=true)
     */
    private $isShipmentOk;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var string $fileAttachment
     *
     * @ORM\Column(name="file_attachment", type="text", nullable=true)
     */
    private $fileAttachment;

    /**
     * @var float $pricePerGram
     *
     * @ORM\Column(name="price_per_gram", type="float", nullable=true)
     */
    private $pricePerGram;

    /**
     * @var boolean $isStorageSampleCollected
     *
     * @ORM\Column(name="is_storage_sample_collected", type="boolean", nullable=true)
     */
    private $isStorageSampleCollected;

    /**
     * @var float $orderQuantity
     *
     * @ORM\Column(name="order_quantity", type="float", nullable=true)
     */
    private $orderQuantity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return GenoBatches
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idGenoMaterial
     *
     * @param integer $idGenoMaterial
     * @return GenoBatches
     */
    public function setIdGenoMaterial($idGenoMaterial)
    {
        $this->idGenoMaterial = $idGenoMaterial;
    
        return $this;
    }

    /**
     * Get idGenoMaterial
     *
     * @return integer 
     */
    public function getIdGenoMaterial()
    {
        return $this->idGenoMaterial;
    }

    /**
     * Set location
     *
     * @param integer $location
     * @return GenoBatches
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return integer 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set amountInStock
     *
     * @param integer $amountInStock
     * @return GenoBatches
     */
    public function setAmountInStock($amountInStock)
    {
        $this->amountInStock = $amountInStock;
    
        return $this;
    }

    /**
     * Get amountInStock
     *
     * @return integer 
     */
    public function getAmountInStock()
    {
        return $this->amountInStock;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenoBatches
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return GenoBatches
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return GenoBatches
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return GenoBatches
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set orderedBy
     *
     * @param integer $orderedBy
     * @return GenoBatches
     */
    public function setOrderedBy($orderedBy)
    {
        $this->orderedBy = $orderedBy;
    
        return $this;
    }

    /**
     * Get orderedBy
     *
     * @return integer 
     */
    public function getOrderedBy()
    {
        return $this->orderedBy;
    }

    /**
     * Set orderedPrice
     *
     * @param integer $orderedPrice
     * @return GenoBatches
     */
    public function setOrderedPrice($orderedPrice)
    {
        $this->orderedPrice = $orderedPrice;
    
        return $this;
    }

    /**
     * Get orderedPrice
     *
     * @return integer 
     */
    public function getOrderedPrice()
    {
        return $this->orderedPrice;
    }

    /**
     * Set unitPrice
     *
     * @param integer $unitPrice
     * @return GenoBatches
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    
        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return integer 
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set unitQuantity
     *
     * @param integer $unitQuantity
     * @return GenoBatches
     */
    public function setUnitQuantity($unitQuantity)
    {
        $this->unitQuantity = $unitQuantity;
    
        return $this;
    }

    /**
     * Get unitQuantity
     *
     * @return integer 
     */
    public function getUnitQuantity()
    {
        return $this->unitQuantity;
    }

    /**
     * Set orderedDate
     *
     * @param \DateTime $orderedDate
     * @return GenoBatches
     */
    public function setOrderedDate($orderedDate)
    {
        $this->orderedDate = $orderedDate;
    
        return $this;
    }

    /**
     * Get orderedDate
     *
     * @return \DateTime 
     */
    public function getOrderedDate()
    {
        return $this->orderedDate;
    }

    /**
     * Set supplierName
     *
     * @param string $supplierName
     * @return GenoBatches
     */
    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;
    
        return $this;
    }

    /**
     * Get supplierName
     *
     * @return string 
     */
    public function getSupplierName()
    {
        return $this->supplierName;
    }

    /**
     * Set fillers
     *
     * @param string $fillers
     * @return GenoBatches
     */
    public function setFillers($fillers)
    {
        $this->fillers = $fillers;
    
        return $this;
    }

    /**
     * Get fillers
     *
     * @return string 
     */
    public function getFillers()
    {
        return $this->fillers;
    }

    /**
     * Set recievedBy
     *
     * @param integer $recievedBy
     * @return GenoBatches
     */
    public function setRecievedBy($recievedBy)
    {
        $this->recievedBy = $recievedBy;
    
        return $this;
    }

    /**
     * Get recievedBy
     *
     * @return integer 
     */
    public function getRecievedBy()
    {
        return $this->recievedBy;
    }

    /**
     * Set recievedDate
     *
     * @param \DateTime $recievedDate
     * @return GenoBatches
     */
    public function setRecievedDate($recievedDate)
    {
        $this->recievedDate = $recievedDate;
    
        return $this;
    }

    /**
     * Get recievedDate
     *
     * @return \DateTime 
     */
    public function getRecievedDate()
    {
        return $this->recievedDate;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return GenoBatches
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set loading
     *
     * @param float $loading
     * @return GenoBatches
     */
    public function setLoading($loading)
    {
        $this->loading = $loading;
    
        return $this;
    }

    /**
     * Get loading
     *
     * @return float 
     */
    public function getLoading()
    {
        return $this->loading;
    }

    /**
     * Set externalLot
     *
     * @param string $externalLot
     * @return GenoBatches
     */
    public function setExternalLot($externalLot)
    {
        $this->externalLot = $externalLot;
    
        return $this;
    }

    /**
     * Get externalLot
     *
     * @return string 
     */
    public function getExternalLot()
    {
        return $this->externalLot;
    }

    /**
     * Set isShipmentOk
     *
     * @param boolean $isShipmentOk
     * @return GenoBatches
     */
    public function setIsShipmentOk($isShipmentOk)
    {
        $this->isShipmentOk = $isShipmentOk;
    
        return $this;
    }

    /**
     * Get isShipmentOk
     *
     * @return boolean 
     */
    public function getIsShipmentOk()
    {
        return $this->isShipmentOk;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return GenoBatches
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return GenoBatches
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set fileAttachment
     *
     * @param string $fileAttachment
     * @return GenoBatches
     */
    public function setFileAttachment($fileAttachment)
    {
        $this->fileAttachment = $fileAttachment;
    
        return $this;
    }

    /**
     * Get fileAttachment
     *
     * @return string 
     */
    public function getFileAttachment()
    {
        return $this->fileAttachment;
    }

    /**
     * Set pricePerGram
     *
     * @param float $pricePerGram
     * @return GenoBatches
     */
    public function setPricePerGram($pricePerGram)
    {
        $this->pricePerGram = $pricePerGram;
    
        return $this;
    }

    /**
     * Get pricePerGram
     *
     * @return float 
     */
    public function getPricePerGram()
    {
        return $this->pricePerGram;
    }

    /**
     * Set isStorageSampleCollected
     *
     * @param boolean $isStorageSampleCollected
     * @return GenoBatches
     */
    public function setIsStorageSampleCollected($isStorageSampleCollected)
    {
        $this->isStorageSampleCollected = $isStorageSampleCollected;
    
        return $this;
    }

    /**
     * Get isStorageSampleCollected
     *
     * @return boolean 
     */
    public function getIsStorageSampleCollected()
    {
        return $this->isStorageSampleCollected;
    }

    /**
     * Set orderQuantity
     *
     * @param float $orderQuantity
     * @return GenoBatches
     */
    public function setOrderQuantity($orderQuantity)
    {
        $this->orderQuantity = $orderQuantity;
    
        return $this;
    }

    /**
     * Get orderQuantity
     *
     * @return float 
     */
    public function getOrderQuantity()
    {
        return $this->orderQuantity;
    }
}