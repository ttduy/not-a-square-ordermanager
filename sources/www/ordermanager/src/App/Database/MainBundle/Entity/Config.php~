<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity
 */
class Config
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $dateFormat
     *
     * @ORM\Column(name="date_format", type="string", length=255, nullable=true)
     */
    private $dateFormat;

    /**
     * @var string $customerImportUrl
     *
     * @ORM\Column(name="customer_import_url", type="text", nullable=true)
     */
    private $customerImportUrl;

    /**
     * @var string $customerImportUsername
     *
     * @ORM\Column(name="customer_import_username", type="string", length=255, nullable=true)
     */
    private $customerImportUsername;

    /**
     * @var string $customerImportPassword
     *
     * @ORM\Column(name="customer_import_password", type="string", length=255, nullable=true)
     */
    private $customerImportPassword;

    /**
     * @var integer $mwaInitialStatus
     *
     * @ORM\Column(name="mwa_initial_status", type="integer", nullable=true)
     */
    private $mwaInitialStatus;

    /**
     * @var string $mwaPushUrl
     *
     * @ORM\Column(name="mwa_push_url", type="text", nullable=true)
     */
    private $mwaPushUrl;

    /**
     * @var integer $mwaMessagePerSession
     *
     * @ORM\Column(name="mwa_message_per_session", type="integer", nullable=true)
     */
    private $mwaMessagePerSession;

    /**
     * @var float $defaultTax
     *
     * @ORM\Column(name="default_tax", type="decimal", nullable=true)
     */
    private $defaultTax;

    /**
     * @var string $mwaReceivedStatusArr
     *
     * @ORM\Column(name="mwa_received_status_arr", type="text", nullable=true)
     */
    private $mwaReceivedStatusArr;

    /**
     * @var string $mwaInprogressStatusArr
     *
     * @ORM\Column(name="mwa_inprogress_status_arr", type="text", nullable=true)
     */
    private $mwaInprogressStatusArr;

    /**
     * @var string $mwaCompletedStatusArr
     *
     * @ORM\Column(name="mwa_completed_status_arr", type="text", nullable=true)
     */
    private $mwaCompletedStatusArr;

    /**
     * @var string $materialExpirationNotificationEmail
     *
     * @ORM\Column(name="material_expiration_notification_email", type="text", nullable=true)
     */
    private $materialExpirationNotificationEmail;

    /**
     * @var string $mwaInvoiceYourUid
     *
     * @ORM\Column(name="mwa_invoice_your_uid", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceYourUid;

    /**
     * @var string $mwaInvoicePurchaseOrder
     *
     * @ORM\Column(name="mwa_invoice_purchase_order", type="string", length=255, nullable=true)
     */
    private $mwaInvoicePurchaseOrder;

    /**
     * @var string $mwaInvoiceSupplierNumber
     *
     * @ORM\Column(name="mwa_invoice_supplier_number", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceSupplierNumber;

    /**
     * @var string $mwaInvoiceProduct
     *
     * @ORM\Column(name="mwa_invoice_product", type="string", length=255, nullable=true)
     */
    private $mwaInvoiceProduct;

    /**
     * @var float $amwayDefaultRevenue
     *
     * @ORM\Column(name="amway_default_revenue", type="decimal", nullable=false)
     */
    private $amwayDefaultRevenue;

    /**
     * @var float $amwayRussiaRevenue
     *
     * @ORM\Column(name="amway_russia_revenue", type="decimal", nullable=false)
     */
    private $amwayRussiaRevenue;

    /**
     * @var integer $retentionDay
     *
     * @ORM\Column(name="retention_day", type="integer", nullable=true)
     */
    private $retentionDay;

    /**
     * @var integer $idFormulaTemplateMwaSample
     *
     * @ORM\Column(name="id_formula_template_mwa_sample", type="integer", nullable=true)
     */
    private $idFormulaTemplateMwaSample;

    /**
     * @var string $lytechUrl
     *
     * @ORM\Column(name="lytech_url", type="string", length=255, nullable=true)
     */
    private $lytechUrl;

    /**
     * @var string $lytechUsername
     *
     * @ORM\Column(name="lytech_username", type="string", length=255, nullable=true)
     */
    private $lytechUsername;

    /**
     * @var string $lytechPassword
     *
     * @ORM\Column(name="lytech_password", type="string", length=255, nullable=true)
     */
    private $lytechPassword;

    /**
     * @var string $sampleImportJobMinute
     *
     * @ORM\Column(name="sample_import_job_minute", type="string", length=255, nullable=true)
     */
    private $sampleImportJobMinute;

    /**
     * @var string $strategyplanningcolorcode
     *
     * @ORM\Column(name="strategyPlanningColorCode", type="text", nullable=true)
     */
    private $strategyplanningcolorcode;

    /**
     * @var string $feedbackWelcomeMessage
     *
     * @ORM\Column(name="feedback_welcome_message", type="text", nullable=true)
     */
    private $feedbackWelcomeMessage;

    /**
     * @var string $customerColorCode
     *
     * @ORM\Column(name="customer_color_code", type="text", nullable=true)
     */
    private $customerColorCode;

    /**
     * @var string $bodfromcompany
     *
     * @ORM\Column(name="bodFromCompany", type="string", length=255, nullable=true)
     */
    private $bodfromcompany;

    /**
     * @var string $bodfromperson
     *
     * @ORM\Column(name="bodFromPerson", type="string", length=255, nullable=true)
     */
    private $bodfromperson;

    /**
     * @var string $bodfromemail
     *
     * @ORM\Column(name="bodFromEmail", type="string", length=255, nullable=true)
     */
    private $bodfromemail;

    /**
     * @var string $boditemtitle
     *
     * @ORM\Column(name="bodItemTitle", type="string", length=255, nullable=true)
     */
    private $boditemtitle;

    /**
     * @var string $bodcontributorrole
     *
     * @ORM\Column(name="bodContributorRole", type="string", length=255, nullable=true)
     */
    private $bodcontributorrole;

    /**
     * @var string $bodcontributorname
     *
     * @ORM\Column(name="bodContributorName", type="string", length=255, nullable=true)
     */
    private $bodcontributorname;

    /**
     * @var string $boditempaper
     *
     * @ORM\Column(name="bodItemPaper", type="string", length=255, nullable=true)
     */
    private $boditempaper;

    /**
     * @var string $boditembinding
     *
     * @ORM\Column(name="bodItemBinding", type="string", length=255, nullable=true)
     */
    private $boditembinding;

    /**
     * @var string $boditemback
     *
     * @ORM\Column(name="bodItemBack", type="string", length=255, nullable=true)
     */
    private $boditemback;

    /**
     * @var string $boditemfinish
     *
     * @ORM\Column(name="bodItemFinish", type="string", length=255, nullable=true)
     */
    private $boditemfinish;

    /**
     * @var string $boditemjacket
     *
     * @ORM\Column(name="bodItemJacket", type="string", length=255, nullable=true)
     */
    private $boditemjacket;

    /**
     * @var string $bodordercustomsvalue
     *
     * @ORM\Column(name="bodOrderCustomsValue", type="string", length=255, nullable=true)
     */
    private $bodordercustomsvalue;

    /**
     * @var string $bodordershipping
     *
     * @ORM\Column(name="bodOrderShipping", type="string", length=255, nullable=true)
     */
    private $bodordershipping;

    /**
     * @var integer $bodiddeliverynotereport
     *
     * @ORM\Column(name="bodIdDeliveryNoteReport", type="integer", nullable=true)
     */
    private $bodiddeliverynotereport;

    /**
     * @var integer $idDefaultShipmentEmail
     *
     * @ORM\Column(name="id_default_shipment_email", type="integer", nullable=true)
     */
    private $idDefaultShipmentEmail;

    /**
     * @var integer $idCryosaveOrderBaby111
     *
     * @ORM\Column(name="id_cryosave_order_baby111", type="integer", nullable=true)
     */
    private $idCryosaveOrderBaby111;

    /**
     * @var integer $idCryosaveOrderMilkome
     *
     * @ORM\Column(name="id_cryosave_order_milkome", type="integer", nullable=true)
     */
    private $idCryosaveOrderMilkome;

    /**
     * @var integer $idCryosaveOrderBloodom
     *
     * @ORM\Column(name="id_cryosave_order_bloodom", type="integer", nullable=true)
     */
    private $idCryosaveOrderBloodom;

    /**
     * @var integer $idNovoLaboratory
     *
     * @ORM\Column(name="id_novo_laboratory", type="integer", nullable=true)
     */
    private $idNovoLaboratory;

    /**
     * @var string $cryosaveFtpServer
     *
     * @ORM\Column(name="cryosave_ftp_server", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpServer;

    /**
     * @var string $cryosaveFtpUsername
     *
     * @ORM\Column(name="cryosave_ftp_username", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpUsername;

    /**
     * @var string $cryosaveFtpPassword
     *
     * @ORM\Column(name="cryosave_ftp_password", type="string", length=255, nullable=true)
     */
    private $cryosaveFtpPassword;

    /**
     * @var string $bodFtpServer
     *
     * @ORM\Column(name="bod_ftp_server", type="string", length=255, nullable=true)
     */
    private $bodFtpServer;

    /**
     * @var string $bodFtpUsername
     *
     * @ORM\Column(name="bod_ftp_username", type="string", length=255, nullable=true)
     */
    private $bodFtpUsername;

    /**
     * @var string $bodFtpPassword
     *
     * @ORM\Column(name="bod_ftp_password", type="string", length=255, nullable=true)
     */
    private $bodFtpPassword;

    /**
     * @var integer $idBillingReportInvoiceCustomer
     *
     * @ORM\Column(name="id_billing_report_invoice_customer", type="integer", nullable=true)
     */
    private $idBillingReportInvoiceCustomer;

    /**
     * @var integer $idBillingReportInvoiceDistributionChannel
     *
     * @ORM\Column(name="id_billing_report_invoice_distribution_channel", type="integer", nullable=true)
     */
    private $idBillingReportInvoiceDistributionChannel;

    /**
     * @var integer $idBillingReportInvoicePartner
     *
     * @ORM\Column(name="id_billing_report_invoice_partner", type="integer", nullable=true)
     */
    private $idBillingReportInvoicePartner;

    /**
     * @var integer $idBillingReportPaymentDistributionChannel
     *
     * @ORM\Column(name="id_billing_report_payment_distribution_channel", type="integer", nullable=true)
     */
    private $idBillingReportPaymentDistributionChannel;

    /**
     * @var integer $idBillingReportPaymentPartner
     *
     * @ORM\Column(name="id_billing_report_payment_partner", type="integer", nullable=true)
     */
    private $idBillingReportPaymentPartner;

    /**
     * @var integer $idBillingReportPaymentAcquisiteur
     *
     * @ORM\Column(name="id_billing_report_payment_acquisiteur", type="integer", nullable=true)
     */
    private $idBillingReportPaymentAcquisiteur;

    /**
     * @var integer $idBillingReportReminderNotice
     *
     * @ORM\Column(name="id_billing_report_reminder_notice", type="integer", nullable=true)
     */
    private $idBillingReportReminderNotice;

    /**
     * @var \DateTime $billingBaselineDate
     *
     * @ORM\Column(name="billing_baseline_date", type="date", nullable=true)
     */
    private $billingBaselineDate;

    /**
     * @var string $nutriMeProducts
     *
     * @ORM\Column(name="nutri_me_products", type="text", nullable=true)
     */
    private $nutriMeProducts;

    /**
     * @var integer $idBillingReportPaymentDistributionChannelInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_distribution_channel_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentDistributionChannelInvoice;

    /**
     * @var integer $idBillingReportPaymentPartnerInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_partner_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentPartnerInvoice;

    /**
     * @var integer $idBillingReportPaymentAcquisiteurInvoice
     *
     * @ORM\Column(name="id_billing_report_payment_acquisiteur_invoice", type="integer", nullable=true)
     */
    private $idBillingReportPaymentAcquisiteurInvoice;

    /**
     * @var string $billingText
     *
     * @ORM\Column(name="billing_text", type="text", nullable=true)
     */
    private $billingText;

    /**
     * @var integer $reportAvailableDefaultXDays
     *
     * @ORM\Column(name="report_available_default_x_days", type="integer", nullable=true)
     */
    private $reportAvailableDefaultXDays;

    /**
     * @var integer $defaultWebLoginEmailPartner
     *
     * @ORM\Column(name="default_web_login_email_partner", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailPartner;

    /**
     * @var integer $defaultWebLoginEmailDistributionChannel
     *
     * @ORM\Column(name="default_web_login_email_distribution_channel", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailDistributionChannel;

    /**
     * @var integer $defaultWebLoginEmailCustomerInfo
     *
     * @ORM\Column(name="default_web_login_email_customer_info", type="integer", nullable=true)
     */
    private $defaultWebLoginEmailCustomerInfo;

    /**
     * @var integer $customerLeadDefaultStatus
     *
     * @ORM\Column(name="customer_lead_default_status", type="integer", nullable=true)
     */
    private $customerLeadDefaultStatus;

    /**
     * @var string $customerDashboardLoginUrl
     *
     * @ORM\Column(name="customer_dashboard_login_url", type="string", length=255, nullable=true)
     */
    private $customerDashboardLoginUrl;

    /**
     * @var string $shopOrderNutrimeBasedOnYourGeneUrl
     *
     * @ORM\Column(name="shop_order_nutrime_based_on_your_gene_url", type="string", length=255, nullable=true)
     */
    private $shopOrderNutrimeBasedOnYourGeneUrl;

    /**
     * @var string $shopOrderPrintedBookletUrl
     *
     * @ORM\Column(name="shop_order_printed_booklet_url", type="string", length=255, nullable=true)
     */
    private $shopOrderPrintedBookletUrl;

    /**
     * @var string $massMailerHost
     *
     * @ORM\Column(name="mass_mailer_host", type="string", length=255, nullable=true)
     */
    private $massMailerHost;

    /**
     * @var integer $massMailerPort
     *
     * @ORM\Column(name="mass_mailer_port", type="integer", nullable=true)
     */
    private $massMailerPort;

    /**
     * @var string $massMailerEncryption
     *
     * @ORM\Column(name="mass_mailer_encryption", type="string", length=10, nullable=true)
     */
    private $massMailerEncryption;

    /**
     * @var string $massMailerUsername
     *
     * @ORM\Column(name="mass_mailer_username", type="string", length=255, nullable=true)
     */
    private $massMailerUsername;

    /**
     * @var string $massMailerPassword
     *
     * @ORM\Column(name="mass_mailer_password", type="string", length=255, nullable=true)
     */
    private $massMailerPassword;

    /**
     * @var string $massMailerSenderName
     *
     * @ORM\Column(name="mass_mailer_sender_name", type="string", length=255, nullable=true)
     */
    private $massMailerSenderName;

    /**
     * @var string $massMailerSenderEmail
     *
     * @ORM\Column(name="mass_mailer_sender_email", type="string", length=255, nullable=true)
     */
    private $massMailerSenderEmail;

    /**
     * @var string $massMailerRate
     *
     * @ORM\Column(name="mass_mailer_rate", type="string", length=255, nullable=true)
     */
    private $massMailerRate;

    /**
     * @var string $mandrillApiKey
     *
     * @ORM\Column(name="mandrill_api_key", type="string", length=255, nullable=true)
     */
    private $mandrillApiKey;

    /**
     * @var string $awsAccessKeyId
     *
     * @ORM\Column(name="aws_access_key_id", type="string", length=255, nullable=true)
     */
    private $awsAccessKeyId;

    /**
     * @var string $awsSecretAccessKey
     *
     * @ORM\Column(name="aws_secret_access_key", type="string", length=255, nullable=true)
     */
    private $awsSecretAccessKey;

    /**
     * @var string $awsRegion
     *
     * @ORM\Column(name="aws_region", type="string", length=255, nullable=true)
     */
    private $awsRegion;

    /**
     * @var string $customerDashboardIdTextBlockGroup
     *
     * @ORM\Column(name="customer_dashboard_id_text_block_group", type="text", nullable=true)
     */
    private $customerDashboardIdTextBlockGroup;

    /**
     * @var string $customerDashboardLanguages
     *
     * @ORM\Column(name="customer_dashboard_languages", type="text", nullable=true)
     */
    private $customerDashboardLanguages;

    /**
     * @var integer $customerDashboardLoginEmailTemplate
     *
     * @ORM\Column(name="customer_dashboard_login_email_template", type="integer", nullable=true)
     */
    private $customerDashboardLoginEmailTemplate;

    /**
     * @var string $unlockPassword
     *
     * @ORM\Column(name="unlock_password", type="string", length=255, nullable=true)
     */
    private $unlockPassword;

    /**
     * @var string $dcMarketingStatusReminderLimitPassedDays
     *
     * @ORM\Column(name="dc_marketing_status_reminder_limit_passed_days", type="string", length=255, nullable=true)
     */
    private $dcMarketingStatusReminderLimitPassedDays;

    /**
     * @var string $defaultRatingDistributionChannel
     *
     * @ORM\Column(name="default_rating_distribution_channel", type="text", nullable=true)
     */
    private $defaultRatingDistributionChannel;

    /**
     * @var string $defaultRatingPartner
     *
     * @ORM\Column(name="default_rating_partner", type="text", nullable=true)
     */
    private $defaultRatingPartner;

    /**
     * @var string $defaultRatingAcquisiteur
     *
     * @ORM\Column(name="default_rating_acquisiteur", type="text", nullable=true)
     */
    private $defaultRatingAcquisiteur;

    /**
     * @var integer $orderBackupDateDuration
     *
     * @ORM\Column(name="order_backup_date_duration", type="integer", nullable=true)
     */
    private $orderBackupDateDuration;

    /**
     * @var string $untranslatedPattern
     *
     * @ORM\Column(name="untranslated_pattern", type="text", nullable=true)
     */
    private $untranslatedPattern;

    /**
     * @var string $cmsContactMessageNotificationEmailList
     *
     * @ORM\Column(name="cms_contact_message_notification_email_list", type="string", length=255, nullable=true)
     */
    private $cmsContactMessageNotificationEmailList;

    /**
     * @var string $emailWebLoginFilterConfig
     *
     * @ORM\Column(name="email_web_login_filter_config", type="text", nullable=true)
     */
    private $emailWebLoginFilterConfig;

    /**
     * @var integer $genoBatchPrinterReportTemplateId
     *
     * @ORM\Column(name="geno_batch_printer_report_template_id", type="integer", nullable=true)
     */
    private $genoBatchPrinterReportTemplateId;

    /**
     * @var integer $offsetCustomerPricing
     *
     * @ORM\Column(name="offset_customer_pricing", type="integer", nullable=true)
     */
    private $offsetCustomerPricing;

    /**
     * @var string $configCustomerPricing
     *
     * @ORM\Column(name="config_customer_pricing", type="text", nullable=true)
     */
    private $configCustomerPricing;

    /**
     * @var string $lastEmailWebloginOrderedStatus
     *
     * @ORM\Column(name="last_email_weblogin_ordered_status", type="text", nullable=true)
     */
    private $lastEmailWebloginOrderedStatus;

    /**
     * @var integer $emailMethodCmsOrder
     *
     * @ORM\Column(name="email_method_cms_order", type="integer", nullable=true)
     */
    private $emailMethodCmsOrder;

    /**
     * @var integer $emailMethodCustomerDashboard
     *
     * @ORM\Column(name="email_method_customer_dashboard", type="integer", nullable=true)
     */
    private $emailMethodCustomerDashboard;

    /**
     * @var string $salesAnalysisFilterConfig
     *
     * @ORM\Column(name="sales_analysis_filter_config", type="text", nullable=true)
     */
    private $salesAnalysisFilterConfig;


}
