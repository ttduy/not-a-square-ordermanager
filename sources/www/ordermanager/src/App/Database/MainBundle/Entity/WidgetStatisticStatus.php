<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WidgetStatisticStatus
 *
 * @ORM\Table(name="widget_statistic_status")
 * @ORM\Entity
 */
class WidgetStatisticStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWidgetStatistic
     *
     * @ORM\Column(name="id_widget_statistic", type="integer", nullable=true)
     */
    private $idWidgetStatistic;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWidgetStatistic
     *
     * @param integer $idWidgetStatistic
     * @return WidgetStatisticStatus
     */
    public function setIdWidgetStatistic($idWidgetStatistic)
    {
        $this->idWidgetStatistic = $idWidgetStatistic;
    
        return $this;
    }

    /**
     * Get idWidgetStatistic
     *
     * @return integer 
     */
    public function getIdWidgetStatistic()
    {
        return $this->idWidgetStatistic;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return WidgetStatisticStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}