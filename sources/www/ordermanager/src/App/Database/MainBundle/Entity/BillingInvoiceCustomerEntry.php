<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillingInvoiceCustomerEntry
 *
 * @ORM\Table(name="billing_invoice_customer_entry")
 * @ORM\Entity
 */
class BillingInvoiceCustomerEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBillingInvoiceCustomer
     *
     * @ORM\Column(name="id_billing_invoice_customer", type="integer", nullable=true)
     */
    private $idBillingInvoiceCustomer;

    /**
     * @var string $entry
     *
     * @ORM\Column(name="entry", type="string", length=255, nullable=true)
     */
    private $entry;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @var string $entryId
     *
     * @ORM\Column(name="entry_id", type="string", length=50, nullable=true)
     */
    private $entryId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBillingInvoiceCustomer
     *
     * @param integer $idBillingInvoiceCustomer
     * @return BillingInvoiceCustomerEntry
     */
    public function setIdBillingInvoiceCustomer($idBillingInvoiceCustomer)
    {
        $this->idBillingInvoiceCustomer = $idBillingInvoiceCustomer;
    
        return $this;
    }

    /**
     * Get idBillingInvoiceCustomer
     *
     * @return integer 
     */
    public function getIdBillingInvoiceCustomer()
    {
        return $this->idBillingInvoiceCustomer;
    }

    /**
     * Set entry
     *
     * @param string $entry
     * @return BillingInvoiceCustomerEntry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    
        return $this;
    }

    /**
     * Get entry
     *
     * @return string 
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return BillingInvoiceCustomerEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set entryId
     *
     * @param string $entryId
     * @return BillingInvoiceCustomerEntry
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    
        return $this;
    }

    /**
     * Get entryId
     *
     * @return string 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }
}