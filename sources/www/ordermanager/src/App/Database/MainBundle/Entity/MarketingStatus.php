<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MarketingStatus
 *
 * @ORM\Table(name="marketing_status")
 * @ORM\Entity
 */
class MarketingStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $txt
     *
     * @ORM\Column(name="txt", type="string", length=255, nullable=true)
     */
    private $txt;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set txt
     *
     * @param string $txt
     * @return MarketingStatus
     */
    public function setTxt($txt)
    {
        $this->txt = $txt;
    
        return $this;
    }

    /**
     * Get txt
     *
     * @return string 
     */
    public function getTxt()
    {
        return $this->txt;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return MarketingStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}