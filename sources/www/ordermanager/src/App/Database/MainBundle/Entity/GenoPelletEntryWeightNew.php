<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPelletEntryWeightNew
 *
 * @ORM\Table(name="geno_pellet_entry_weight_new")
 * @ORM\Entity
 */
class GenoPelletEntryWeightNew
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPelletEntry
     *
     * @ORM\Column(name="id_pellet_entry", type="integer", nullable=true)
     */
    private $idPelletEntry;

    /**
     * @var integer $activeIngredientsSubstance
     *
     * @ORM\Column(name="active_ingredients_substance", type="integer", nullable=true)
     */
    private $activeIngredientsSubstance;

    /**
     * @var integer $activeSubstance
     *
     * @ORM\Column(name="active_substance", type="integer", nullable=true)
     */
    private $activeSubstance;

    /**
     * @var float $activeIngredientsSubstanceWeight
     *
     * @ORM\Column(name="active_ingredients_substance_weight", type="float", nullable=true)
     */
    private $activeIngredientsSubstanceWeight;

    /**
     * @var float $activeSubstanceWeight
     *
     * @ORM\Column(name="active_substance_weight", type="float", nullable=true)
     */
    private $activeSubstanceWeight;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPelletEntry
     *
     * @param integer $idPelletEntry
     * @return GenoPelletEntryWeightNew
     */
    public function setIdPelletEntry($idPelletEntry)
    {
        $this->idPelletEntry = $idPelletEntry;
    
        return $this;
    }

    /**
     * Get idPelletEntry
     *
     * @return integer 
     */
    public function getIdPelletEntry()
    {
        return $this->idPelletEntry;
    }

    /**
     * Set activeIngredientsSubstance
     *
     * @param integer $activeIngredientsSubstance
     * @return GenoPelletEntryWeightNew
     */
    public function setActiveIngredientsSubstance($activeIngredientsSubstance)
    {
        $this->activeIngredientsSubstance = $activeIngredientsSubstance;
    
        return $this;
    }

    /**
     * Get activeIngredientsSubstance
     *
     * @return integer 
     */
    public function getActiveIngredientsSubstance()
    {
        return $this->activeIngredientsSubstance;
    }

    /**
     * Set activeSubstance
     *
     * @param integer $activeSubstance
     * @return GenoPelletEntryWeightNew
     */
    public function setActiveSubstance($activeSubstance)
    {
        $this->activeSubstance = $activeSubstance;
    
        return $this;
    }

    /**
     * Get activeSubstance
     *
     * @return integer 
     */
    public function getActiveSubstance()
    {
        return $this->activeSubstance;
    }

    /**
     * Set activeIngredientsSubstanceWeight
     *
     * @param float $activeIngredientsSubstanceWeight
     * @return GenoPelletEntryWeightNew
     */
    public function setActiveIngredientsSubstanceWeight($activeIngredientsSubstanceWeight)
    {
        $this->activeIngredientsSubstanceWeight = $activeIngredientsSubstanceWeight;
    
        return $this;
    }

    /**
     * Get activeIngredientsSubstanceWeight
     *
     * @return float 
     */
    public function getActiveIngredientsSubstanceWeight()
    {
        return $this->activeIngredientsSubstanceWeight;
    }

    /**
     * Set activeSubstanceWeight
     *
     * @param float $activeSubstanceWeight
     * @return GenoPelletEntryWeightNew
     */
    public function setActiveSubstanceWeight($activeSubstanceWeight)
    {
        $this->activeSubstanceWeight = $activeSubstanceWeight;
    
        return $this;
    }

    /**
     * Get activeSubstanceWeight
     *
     * @return float 
     */
    public function getActiveSubstanceWeight()
    {
        return $this->activeSubstanceWeight;
    }
}