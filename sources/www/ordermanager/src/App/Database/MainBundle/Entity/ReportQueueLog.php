<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportQueueLog
 *
 * @ORM\Table(name="report_queue_log")
 * @ORM\Entity
 */
class ReportQueueLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idReportQueue
     *
     * @ORM\Column(name="id_report_queue", type="integer", nullable=true)
     */
    private $idReportQueue;

    /**
     * @var \DateTime $logTime
     *
     * @ORM\Column(name="log_time", type="datetime", nullable=true)
     */
    private $logTime;

    /**
     * @var string $logText
     *
     * @ORM\Column(name="log_text", type="text", nullable=true)
     */
    private $logText;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idReportQueue
     *
     * @param integer $idReportQueue
     * @return ReportQueueLog
     */
    public function setIdReportQueue($idReportQueue)
    {
        $this->idReportQueue = $idReportQueue;
    
        return $this;
    }

    /**
     * Get idReportQueue
     *
     * @return integer 
     */
    public function getIdReportQueue()
    {
        return $this->idReportQueue;
    }

    /**
     * Set logTime
     *
     * @param \DateTime $logTime
     * @return ReportQueueLog
     */
    public function setLogTime($logTime)
    {
        $this->logTime = $logTime;
    
        return $this;
    }

    /**
     * Get logTime
     *
     * @return \DateTime 
     */
    public function getLogTime()
    {
        return $this->logTime;
    }

    /**
     * Set logText
     *
     * @param string $logText
     * @return ReportQueueLog
     */
    public function setLogText($logText)
    {
        $this->logText = $logText;
    
        return $this;
    }

    /**
     * Get logText
     *
     * @return string 
     */
    public function getLogText()
    {
        return $this->logText;
    }
}