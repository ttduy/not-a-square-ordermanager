<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ReportQueue
 *
 * @ORM\Table(name="report_queue")
 * @ORM\Entity
 */
class ReportQueue
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var integer $idWorker
     *
     * @ORM\Column(name="id_worker", type="integer", nullable=true)
     */
    private $idWorker;

    /**
     * @var \DateTime $inputTime
     *
     * @ORM\Column(name="input_time", type="datetime", nullable=true)
     */
    private $inputTime;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $input
     *
     * @ORM\Column(name="input", type="text", nullable=true)
     */
    private $input;

    /**
     * @var string $output
     *
     * @ORM\Column(name="output", type="text", nullable=true)
     */
    private $output;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var float $progress
     *
     * @ORM\Column(name="progress", type="decimal", nullable=true)
     */
    private $progress;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idReport
     *
     * @ORM\Column(name="id_report", type="integer", nullable=true)
     */
    private $idReport;

    /**
     * @var integer $isSaved
     *
     * @ORM\Column(name="is_saved", type="integer", nullable=true)
     */
    private $isSaved;

    /**
     * @var integer $idJobType
     *
     * @ORM\Column(name="id_job_type", type="integer", nullable=true)
     */
    private $idJobType;

    /**
     * @var integer $processingTime
     *
     * @ORM\Column(name="processing_time", type="integer", nullable=true)
     */
    private $processingTime;

    /**
     * @var \DateTime $lastUpdated
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     */
    private $lastUpdated;

    /**
     * @var integer $idPriority
     *
     * @ORM\Column(name="id_priority", type="integer", nullable=true)
     */
    private $idPriority;

    /**
     * @var \DateTime $startTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime $endTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var integer $ftpUploadStatus
     *
     * @ORM\Column(name="ftp_upload_status", type="integer", nullable=true)
     */
    private $ftpUploadStatus;

    /**
     * @var boolean $isWarning
     *
     * @ORM\Column(name="is_warning", type="boolean", nullable=true)
     */
    private $isWarning;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return ReportQueue
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set idWorker
     *
     * @param integer $idWorker
     * @return ReportQueue
     */
    public function setIdWorker($idWorker)
    {
        $this->idWorker = $idWorker;
    
        return $this;
    }

    /**
     * Get idWorker
     *
     * @return integer 
     */
    public function getIdWorker()
    {
        return $this->idWorker;
    }

    /**
     * Set inputTime
     *
     * @param \DateTime $inputTime
     * @return ReportQueue
     */
    public function setInputTime($inputTime)
    {
        $this->inputTime = $inputTime;
    
        return $this;
    }

    /**
     * Get inputTime
     *
     * @return \DateTime 
     */
    public function getInputTime()
    {
        return $this->inputTime;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ReportQueue
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set input
     *
     * @param string $input
     * @return ReportQueue
     */
    public function setInput($input)
    {
        $this->input = $input;
    
        return $this;
    }

    /**
     * Get input
     *
     * @return string 
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Set output
     *
     * @param string $output
     * @return ReportQueue
     */
    public function setOutput($output)
    {
        $this->output = $output;
    
        return $this;
    }

    /**
     * Get output
     *
     * @return string 
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ReportQueue
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set progress
     *
     * @param float $progress
     * @return ReportQueue
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;
    
        return $this;
    }

    /**
     * Get progress
     *
     * @return float 
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return ReportQueue
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idReport
     *
     * @param integer $idReport
     * @return ReportQueue
     */
    public function setIdReport($idReport)
    {
        $this->idReport = $idReport;
    
        return $this;
    }

    /**
     * Get idReport
     *
     * @return integer 
     */
    public function getIdReport()
    {
        return $this->idReport;
    }

    /**
     * Set isSaved
     *
     * @param integer $isSaved
     * @return ReportQueue
     */
    public function setIsSaved($isSaved)
    {
        $this->isSaved = $isSaved;
    
        return $this;
    }

    /**
     * Get isSaved
     *
     * @return integer 
     */
    public function getIsSaved()
    {
        return $this->isSaved;
    }

    /**
     * Set idJobType
     *
     * @param integer $idJobType
     * @return ReportQueue
     */
    public function setIdJobType($idJobType)
    {
        $this->idJobType = $idJobType;
    
        return $this;
    }

    /**
     * Get idJobType
     *
     * @return integer 
     */
    public function getIdJobType()
    {
        return $this->idJobType;
    }

    /**
     * Set processingTime
     *
     * @param integer $processingTime
     * @return ReportQueue
     */
    public function setProcessingTime($processingTime)
    {
        $this->processingTime = $processingTime;
    
        return $this;
    }

    /**
     * Get processingTime
     *
     * @return integer 
     */
    public function getProcessingTime()
    {
        return $this->processingTime;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     * @return ReportQueue
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    
        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime 
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * Set idPriority
     *
     * @param integer $idPriority
     * @return ReportQueue
     */
    public function setIdPriority($idPriority)
    {
        $this->idPriority = $idPriority;
    
        return $this;
    }

    /**
     * Get idPriority
     *
     * @return integer 
     */
    public function getIdPriority()
    {
        return $this->idPriority;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return ReportQueue
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    
        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * @return ReportQueue
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    
        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set ftpUploadStatus
     *
     * @param integer $ftpUploadStatus
     * @return ReportQueue
     */
    public function setFtpUploadStatus($ftpUploadStatus)
    {
        $this->ftpUploadStatus = $ftpUploadStatus;
    
        return $this;
    }

    /**
     * Get ftpUploadStatus
     *
     * @return integer 
     */
    public function getFtpUploadStatus()
    {
        return $this->ftpUploadStatus;
    }

    /**
     * Set isWarning
     *
     * @param boolean $isWarning
     * @return ReportQueue
     */
    public function setIsWarning($isWarning)
    {
        $this->isWarning = $isWarning;
    
        return $this;
    }

    /**
     * Get isWarning
     *
     * @return boolean 
     */
    public function getIsWarning()
    {
        return $this->isWarning;
    }
}