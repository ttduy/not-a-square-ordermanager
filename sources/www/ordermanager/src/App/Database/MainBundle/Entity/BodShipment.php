<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BodShipment
 *
 * @ORM\Table(name="bod_shipment")
 * @ORM\Entity
 */
class BodShipment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idCustomerReport
     *
     * @ORM\Column(name="id_customer_report", type="integer", nullable=true)
     */
    private $idCustomerReport;

    /**
     * @var string $customerReportName
     *
     * @ORM\Column(name="customer_report_name", type="string", length=510, nullable=true)
     */
    private $customerReportName;

    /**
     * @var string $bodCustomerOrderNumber
     *
     * @ORM\Column(name="bod_customer_order_number", type="string", length=255, nullable=true)
     */
    private $bodCustomerOrderNumber;

    /**
     * @var string $bodFromCompany
     *
     * @ORM\Column(name="bod_from_company", type="string", length=255, nullable=true)
     */
    private $bodFromCompany;

    /**
     * @var string $bodFromPerson
     *
     * @ORM\Column(name="bod_from_person", type="string", length=255, nullable=true)
     */
    private $bodFromPerson;

    /**
     * @var string $bodFromEmail
     *
     * @ORM\Column(name="bod_from_email", type="string", length=255, nullable=true)
     */
    private $bodFromEmail;

    /**
     * @var string $bodItemTitle
     *
     * @ORM\Column(name="bod_item_title", type="string", length=255, nullable=true)
     */
    private $bodItemTitle;

    /**
     * @var string $bodContributorRole
     *
     * @ORM\Column(name="bod_contributor_role", type="string", length=255, nullable=true)
     */
    private $bodContributorRole;

    /**
     * @var string $bodContributorName
     *
     * @ORM\Column(name="bod_contributor_name", type="string", length=255, nullable=true)
     */
    private $bodContributorName;

    /**
     * @var string $bodItemPaper
     *
     * @ORM\Column(name="bod_item_paper", type="string", length=255, nullable=true)
     */
    private $bodItemPaper;

    /**
     * @var string $bodItemBinding
     *
     * @ORM\Column(name="bod_item_binding", type="string", length=255, nullable=true)
     */
    private $bodItemBinding;

    /**
     * @var string $bodItemBack
     *
     * @ORM\Column(name="bod_item_back", type="string", length=255, nullable=true)
     */
    private $bodItemBack;

    /**
     * @var string $bodItemFinish
     *
     * @ORM\Column(name="bod_item_finish", type="string", length=255, nullable=true)
     */
    private $bodItemFinish;

    /**
     * @var string $bodItemJacket
     *
     * @ORM\Column(name="bod_item_jacket", type="string", length=255, nullable=true)
     */
    private $bodItemJacket;

    /**
     * @var string $bodOrderNumber
     *
     * @ORM\Column(name="bod_order_number", type="string", length=255, nullable=true)
     */
    private $bodOrderNumber;

    /**
     * @var string $bodOrderCopies
     *
     * @ORM\Column(name="bod_order_copies", type="string", length=255, nullable=true)
     */
    private $bodOrderCopies;

    /**
     * @var string $bodOrderAddressLine1
     *
     * @ORM\Column(name="bod_order_address_line1", type="string", length=255, nullable=true)
     */
    private $bodOrderAddressLine1;

    /**
     * @var string $bodOrderStreet
     *
     * @ORM\Column(name="bod_order_street", type="string", length=255, nullable=true)
     */
    private $bodOrderStreet;

    /**
     * @var string $bodOrderZip
     *
     * @ORM\Column(name="bod_order_zip", type="string", length=255, nullable=true)
     */
    private $bodOrderZip;

    /**
     * @var string $bodOrderCity
     *
     * @ORM\Column(name="bod_order_city", type="string", length=255, nullable=true)
     */
    private $bodOrderCity;

    /**
     * @var string $bodOrderCountry
     *
     * @ORM\Column(name="bod_order_country", type="string", length=255, nullable=true)
     */
    private $bodOrderCountry;

    /**
     * @var string $bodOrderCustomsValue
     *
     * @ORM\Column(name="bod_order_customs_value", type="string", length=255, nullable=true)
     */
    private $bodOrderCustomsValue;

    /**
     * @var string $bodOrderShipping
     *
     * @ORM\Column(name="bod_order_shipping", type="string", length=255, nullable=true)
     */
    private $bodOrderShipping;

    /**
     * @var string $packageFile
     *
     * @ORM\Column(name="package_file", type="string", length=255, nullable=true)
     */
    private $packageFile;

    /**
     * @var string $reportFile
     *
     * @ORM\Column(name="report_file", type="string", length=255, nullable=true)
     */
    private $reportFile;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $bodiddeliverynotereport
     *
     * @ORM\Column(name="bodIdDeliveryNoteReport", type="integer", nullable=true)
     */
    private $bodiddeliverynotereport;

    /**
     * @var integer $bodDeliveryNoteIdReport
     *
     * @ORM\Column(name="bod_delivery_note_id_report", type="integer", nullable=true)
     */
    private $bodDeliveryNoteIdReport;

    /**
     * @var integer $bodDeliveryNoteIdLanguage
     *
     * @ORM\Column(name="bod_delivery_note_id_language", type="integer", nullable=true)
     */
    private $bodDeliveryNoteIdLanguage;

    /**
     * @var string $bodOrderAddressLine2
     *
     * @ORM\Column(name="bod_order_address_line2", type="string", length=255, nullable=true)
     */
    private $bodOrderAddressLine2;

    /**
     * @var string $bodOrderAddressLine3
     *
     * @ORM\Column(name="bod_order_address_line3", type="string", length=255, nullable=true)
     */
    private $bodOrderAddressLine3;

    /**
     * @var string $highResolutionFile
     *
     * @ORM\Column(name="high_resolution_file", type="string", length=255, nullable=true)
     */
    private $highResolutionFile;

    /**
     * @var string $lowResolutionFile
     *
     * @ORM\Column(name="low_resolution_file", type="string", length=255, nullable=true)
     */
    private $lowResolutionFile;

    /**
     * @var string $customBodAccountNumber
     *
     * @ORM\Column(name="custom_bod_account_number", type="string", length=45, nullable=true)
     */
    private $customBodAccountNumber;

    /**
     * @var string $customBodBarcodeNumber
     *
     * @ORM\Column(name="custom_bod_barcode_number", type="string", length=45, nullable=true)
     */
    private $customBodBarcodeNumber;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return BodShipment
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return BodShipment
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idCustomerReport
     *
     * @param integer $idCustomerReport
     * @return BodShipment
     */
    public function setIdCustomerReport($idCustomerReport)
    {
        $this->idCustomerReport = $idCustomerReport;
    
        return $this;
    }

    /**
     * Get idCustomerReport
     *
     * @return integer 
     */
    public function getIdCustomerReport()
    {
        return $this->idCustomerReport;
    }

    /**
     * Set customerReportName
     *
     * @param string $customerReportName
     * @return BodShipment
     */
    public function setCustomerReportName($customerReportName)
    {
        $this->customerReportName = $customerReportName;
    
        return $this;
    }

    /**
     * Get customerReportName
     *
     * @return string 
     */
    public function getCustomerReportName()
    {
        return $this->customerReportName;
    }

    /**
     * Set bodCustomerOrderNumber
     *
     * @param string $bodCustomerOrderNumber
     * @return BodShipment
     */
    public function setBodCustomerOrderNumber($bodCustomerOrderNumber)
    {
        $this->bodCustomerOrderNumber = $bodCustomerOrderNumber;
    
        return $this;
    }

    /**
     * Get bodCustomerOrderNumber
     *
     * @return string 
     */
    public function getBodCustomerOrderNumber()
    {
        return $this->bodCustomerOrderNumber;
    }

    /**
     * Set bodFromCompany
     *
     * @param string $bodFromCompany
     * @return BodShipment
     */
    public function setBodFromCompany($bodFromCompany)
    {
        $this->bodFromCompany = $bodFromCompany;
    
        return $this;
    }

    /**
     * Get bodFromCompany
     *
     * @return string 
     */
    public function getBodFromCompany()
    {
        return $this->bodFromCompany;
    }

    /**
     * Set bodFromPerson
     *
     * @param string $bodFromPerson
     * @return BodShipment
     */
    public function setBodFromPerson($bodFromPerson)
    {
        $this->bodFromPerson = $bodFromPerson;
    
        return $this;
    }

    /**
     * Get bodFromPerson
     *
     * @return string 
     */
    public function getBodFromPerson()
    {
        return $this->bodFromPerson;
    }

    /**
     * Set bodFromEmail
     *
     * @param string $bodFromEmail
     * @return BodShipment
     */
    public function setBodFromEmail($bodFromEmail)
    {
        $this->bodFromEmail = $bodFromEmail;
    
        return $this;
    }

    /**
     * Get bodFromEmail
     *
     * @return string 
     */
    public function getBodFromEmail()
    {
        return $this->bodFromEmail;
    }

    /**
     * Set bodItemTitle
     *
     * @param string $bodItemTitle
     * @return BodShipment
     */
    public function setBodItemTitle($bodItemTitle)
    {
        $this->bodItemTitle = $bodItemTitle;
    
        return $this;
    }

    /**
     * Get bodItemTitle
     *
     * @return string 
     */
    public function getBodItemTitle()
    {
        return $this->bodItemTitle;
    }

    /**
     * Set bodContributorRole
     *
     * @param string $bodContributorRole
     * @return BodShipment
     */
    public function setBodContributorRole($bodContributorRole)
    {
        $this->bodContributorRole = $bodContributorRole;
    
        return $this;
    }

    /**
     * Get bodContributorRole
     *
     * @return string 
     */
    public function getBodContributorRole()
    {
        return $this->bodContributorRole;
    }

    /**
     * Set bodContributorName
     *
     * @param string $bodContributorName
     * @return BodShipment
     */
    public function setBodContributorName($bodContributorName)
    {
        $this->bodContributorName = $bodContributorName;
    
        return $this;
    }

    /**
     * Get bodContributorName
     *
     * @return string 
     */
    public function getBodContributorName()
    {
        return $this->bodContributorName;
    }

    /**
     * Set bodItemPaper
     *
     * @param string $bodItemPaper
     * @return BodShipment
     */
    public function setBodItemPaper($bodItemPaper)
    {
        $this->bodItemPaper = $bodItemPaper;
    
        return $this;
    }

    /**
     * Get bodItemPaper
     *
     * @return string 
     */
    public function getBodItemPaper()
    {
        return $this->bodItemPaper;
    }

    /**
     * Set bodItemBinding
     *
     * @param string $bodItemBinding
     * @return BodShipment
     */
    public function setBodItemBinding($bodItemBinding)
    {
        $this->bodItemBinding = $bodItemBinding;
    
        return $this;
    }

    /**
     * Get bodItemBinding
     *
     * @return string 
     */
    public function getBodItemBinding()
    {
        return $this->bodItemBinding;
    }

    /**
     * Set bodItemBack
     *
     * @param string $bodItemBack
     * @return BodShipment
     */
    public function setBodItemBack($bodItemBack)
    {
        $this->bodItemBack = $bodItemBack;
    
        return $this;
    }

    /**
     * Get bodItemBack
     *
     * @return string 
     */
    public function getBodItemBack()
    {
        return $this->bodItemBack;
    }

    /**
     * Set bodItemFinish
     *
     * @param string $bodItemFinish
     * @return BodShipment
     */
    public function setBodItemFinish($bodItemFinish)
    {
        $this->bodItemFinish = $bodItemFinish;
    
        return $this;
    }

    /**
     * Get bodItemFinish
     *
     * @return string 
     */
    public function getBodItemFinish()
    {
        return $this->bodItemFinish;
    }

    /**
     * Set bodItemJacket
     *
     * @param string $bodItemJacket
     * @return BodShipment
     */
    public function setBodItemJacket($bodItemJacket)
    {
        $this->bodItemJacket = $bodItemJacket;
    
        return $this;
    }

    /**
     * Get bodItemJacket
     *
     * @return string 
     */
    public function getBodItemJacket()
    {
        return $this->bodItemJacket;
    }

    /**
     * Set bodOrderNumber
     *
     * @param string $bodOrderNumber
     * @return BodShipment
     */
    public function setBodOrderNumber($bodOrderNumber)
    {
        $this->bodOrderNumber = $bodOrderNumber;
    
        return $this;
    }

    /**
     * Get bodOrderNumber
     *
     * @return string 
     */
    public function getBodOrderNumber()
    {
        return $this->bodOrderNumber;
    }

    /**
     * Set bodOrderCopies
     *
     * @param string $bodOrderCopies
     * @return BodShipment
     */
    public function setBodOrderCopies($bodOrderCopies)
    {
        $this->bodOrderCopies = $bodOrderCopies;
    
        return $this;
    }

    /**
     * Get bodOrderCopies
     *
     * @return string 
     */
    public function getBodOrderCopies()
    {
        return $this->bodOrderCopies;
    }

    /**
     * Set bodOrderAddressLine1
     *
     * @param string $bodOrderAddressLine1
     * @return BodShipment
     */
    public function setBodOrderAddressLine1($bodOrderAddressLine1)
    {
        $this->bodOrderAddressLine1 = $bodOrderAddressLine1;
    
        return $this;
    }

    /**
     * Get bodOrderAddressLine1
     *
     * @return string 
     */
    public function getBodOrderAddressLine1()
    {
        return $this->bodOrderAddressLine1;
    }

    /**
     * Set bodOrderStreet
     *
     * @param string $bodOrderStreet
     * @return BodShipment
     */
    public function setBodOrderStreet($bodOrderStreet)
    {
        $this->bodOrderStreet = $bodOrderStreet;
    
        return $this;
    }

    /**
     * Get bodOrderStreet
     *
     * @return string 
     */
    public function getBodOrderStreet()
    {
        return $this->bodOrderStreet;
    }

    /**
     * Set bodOrderZip
     *
     * @param string $bodOrderZip
     * @return BodShipment
     */
    public function setBodOrderZip($bodOrderZip)
    {
        $this->bodOrderZip = $bodOrderZip;
    
        return $this;
    }

    /**
     * Get bodOrderZip
     *
     * @return string 
     */
    public function getBodOrderZip()
    {
        return $this->bodOrderZip;
    }

    /**
     * Set bodOrderCity
     *
     * @param string $bodOrderCity
     * @return BodShipment
     */
    public function setBodOrderCity($bodOrderCity)
    {
        $this->bodOrderCity = $bodOrderCity;
    
        return $this;
    }

    /**
     * Get bodOrderCity
     *
     * @return string 
     */
    public function getBodOrderCity()
    {
        return $this->bodOrderCity;
    }

    /**
     * Set bodOrderCountry
     *
     * @param string $bodOrderCountry
     * @return BodShipment
     */
    public function setBodOrderCountry($bodOrderCountry)
    {
        $this->bodOrderCountry = $bodOrderCountry;
    
        return $this;
    }

    /**
     * Get bodOrderCountry
     *
     * @return string 
     */
    public function getBodOrderCountry()
    {
        return $this->bodOrderCountry;
    }

    /**
     * Set bodOrderCustomsValue
     *
     * @param string $bodOrderCustomsValue
     * @return BodShipment
     */
    public function setBodOrderCustomsValue($bodOrderCustomsValue)
    {
        $this->bodOrderCustomsValue = $bodOrderCustomsValue;
    
        return $this;
    }

    /**
     * Get bodOrderCustomsValue
     *
     * @return string 
     */
    public function getBodOrderCustomsValue()
    {
        return $this->bodOrderCustomsValue;
    }

    /**
     * Set bodOrderShipping
     *
     * @param string $bodOrderShipping
     * @return BodShipment
     */
    public function setBodOrderShipping($bodOrderShipping)
    {
        $this->bodOrderShipping = $bodOrderShipping;
    
        return $this;
    }

    /**
     * Get bodOrderShipping
     *
     * @return string 
     */
    public function getBodOrderShipping()
    {
        return $this->bodOrderShipping;
    }

    /**
     * Set packageFile
     *
     * @param string $packageFile
     * @return BodShipment
     */
    public function setPackageFile($packageFile)
    {
        $this->packageFile = $packageFile;
    
        return $this;
    }

    /**
     * Get packageFile
     *
     * @return string 
     */
    public function getPackageFile()
    {
        return $this->packageFile;
    }

    /**
     * Set reportFile
     *
     * @param string $reportFile
     * @return BodShipment
     */
    public function setReportFile($reportFile)
    {
        $this->reportFile = $reportFile;
    
        return $this;
    }

    /**
     * Get reportFile
     *
     * @return string 
     */
    public function getReportFile()
    {
        return $this->reportFile;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return BodShipment
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set bodiddeliverynotereport
     *
     * @param integer $bodiddeliverynotereport
     * @return BodShipment
     */
    public function setBodiddeliverynotereport($bodiddeliverynotereport)
    {
        $this->bodiddeliverynotereport = $bodiddeliverynotereport;
    
        return $this;
    }

    /**
     * Get bodiddeliverynotereport
     *
     * @return integer 
     */
    public function getBodiddeliverynotereport()
    {
        return $this->bodiddeliverynotereport;
    }

    /**
     * Set bodDeliveryNoteIdReport
     *
     * @param integer $bodDeliveryNoteIdReport
     * @return BodShipment
     */
    public function setBodDeliveryNoteIdReport($bodDeliveryNoteIdReport)
    {
        $this->bodDeliveryNoteIdReport = $bodDeliveryNoteIdReport;
    
        return $this;
    }

    /**
     * Get bodDeliveryNoteIdReport
     *
     * @return integer 
     */
    public function getBodDeliveryNoteIdReport()
    {
        return $this->bodDeliveryNoteIdReport;
    }

    /**
     * Set bodDeliveryNoteIdLanguage
     *
     * @param integer $bodDeliveryNoteIdLanguage
     * @return BodShipment
     */
    public function setBodDeliveryNoteIdLanguage($bodDeliveryNoteIdLanguage)
    {
        $this->bodDeliveryNoteIdLanguage = $bodDeliveryNoteIdLanguage;
    
        return $this;
    }

    /**
     * Get bodDeliveryNoteIdLanguage
     *
     * @return integer 
     */
    public function getBodDeliveryNoteIdLanguage()
    {
        return $this->bodDeliveryNoteIdLanguage;
    }

    /**
     * Set bodOrderAddressLine2
     *
     * @param string $bodOrderAddressLine2
     * @return BodShipment
     */
    public function setBodOrderAddressLine2($bodOrderAddressLine2)
    {
        $this->bodOrderAddressLine2 = $bodOrderAddressLine2;
    
        return $this;
    }

    /**
     * Get bodOrderAddressLine2
     *
     * @return string 
     */
    public function getBodOrderAddressLine2()
    {
        return $this->bodOrderAddressLine2;
    }

    /**
     * Set bodOrderAddressLine3
     *
     * @param string $bodOrderAddressLine3
     * @return BodShipment
     */
    public function setBodOrderAddressLine3($bodOrderAddressLine3)
    {
        $this->bodOrderAddressLine3 = $bodOrderAddressLine3;
    
        return $this;
    }

    /**
     * Get bodOrderAddressLine3
     *
     * @return string 
     */
    public function getBodOrderAddressLine3()
    {
        return $this->bodOrderAddressLine3;
    }

    /**
     * Set highResolutionFile
     *
     * @param string $highResolutionFile
     * @return BodShipment
     */
    public function setHighResolutionFile($highResolutionFile)
    {
        $this->highResolutionFile = $highResolutionFile;
    
        return $this;
    }

    /**
     * Get highResolutionFile
     *
     * @return string 
     */
    public function getHighResolutionFile()
    {
        return $this->highResolutionFile;
    }

    /**
     * Set lowResolutionFile
     *
     * @param string $lowResolutionFile
     * @return BodShipment
     */
    public function setLowResolutionFile($lowResolutionFile)
    {
        $this->lowResolutionFile = $lowResolutionFile;
    
        return $this;
    }

    /**
     * Get lowResolutionFile
     *
     * @return string 
     */
    public function getLowResolutionFile()
    {
        return $this->lowResolutionFile;
    }

    /**
     * Set customBodAccountNumber
     *
     * @param string $customBodAccountNumber
     * @return BodShipment
     */
    public function setCustomBodAccountNumber($customBodAccountNumber)
    {
        $this->customBodAccountNumber = $customBodAccountNumber;
    
        return $this;
    }

    /**
     * Get customBodAccountNumber
     *
     * @return string 
     */
    public function getCustomBodAccountNumber()
    {
        return $this->customBodAccountNumber;
    }

    /**
     * Set customBodBarcodeNumber
     *
     * @param string $customBodBarcodeNumber
     * @return BodShipment
     */
    public function setCustomBodBarcodeNumber($customBodBarcodeNumber)
    {
        $this->customBodBarcodeNumber = $customBodBarcodeNumber;
    
        return $this;
    }

    /**
     * Get customBodBarcodeNumber
     *
     * @return string 
     */
    public function getCustomBodBarcodeNumber()
    {
        return $this->customBodBarcodeNumber;
    }
}