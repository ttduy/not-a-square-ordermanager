<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Form
 *
 * @ORM\Table(name="form")
 * @ORM\Entity
 */
class Form
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $formType
     *
     * @ORM\Column(name="form_type", type="string", length=255, nullable=true)
     */
    private $formType;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $formNumber
     *
     * @ORM\Column(name="form_number", type="string", length=255, nullable=true)
     */
    private $formNumber;

    /**
     * @var integer $idActiveFormVersion
     *
     * @ORM\Column(name="id_active_form_version", type="integer", nullable=true)
     */
    private $idActiveFormVersion;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var boolean $alertOldVersion
     *
     * @ORM\Column(name="alert_old_version", type="boolean", nullable=true)
     */
    private $alertOldVersion;

    /**
     * @var string $colorCode
     *
     * @ORM\Column(name="color_code", type="string", length=255, nullable=true)
     */
    private $colorCode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Form
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formType
     *
     * @param string $formType
     * @return Form
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;
    
        return $this;
    }

    /**
     * Get formType
     *
     * @return string 
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Form
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set formNumber
     *
     * @param string $formNumber
     * @return Form
     */
    public function setFormNumber($formNumber)
    {
        $this->formNumber = $formNumber;
    
        return $this;
    }

    /**
     * Get formNumber
     *
     * @return string 
     */
    public function getFormNumber()
    {
        return $this->formNumber;
    }

    /**
     * Set idActiveFormVersion
     *
     * @param integer $idActiveFormVersion
     * @return Form
     */
    public function setIdActiveFormVersion($idActiveFormVersion)
    {
        $this->idActiveFormVersion = $idActiveFormVersion;
    
        return $this;
    }

    /**
     * Get idActiveFormVersion
     *
     * @return integer 
     */
    public function getIdActiveFormVersion()
    {
        return $this->idActiveFormVersion;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Form
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set alertOldVersion
     *
     * @param boolean $alertOldVersion
     * @return Form
     */
    public function setAlertOldVersion($alertOldVersion)
    {
        $this->alertOldVersion = $alertOldVersion;
    
        return $this;
    }

    /**
     * Get alertOldVersion
     *
     * @return boolean 
     */
    public function getAlertOldVersion()
    {
        return $this->alertOldVersion;
    }

    /**
     * Set colorCode
     *
     * @param string $colorCode
     * @return Form
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
    
        return $this;
    }

    /**
     * Get colorCode
     *
     * @return string 
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }
}