<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmployeeFeedbackResponse
 *
 * @ORM\Table(name="employee_feedback_response")
 * @ORM\Entity
 */
class EmployeeFeedbackResponse
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmployeeFeedbackQuestion
     *
     * @ORM\Column(name="id_employee_feedback_question", type="integer", nullable=true)
     */
    private $idEmployeeFeedbackQuestion;

    /**
     * @var string $idUser
     *
     * @ORM\Column(name="id_user", type="string", length=255, nullable=true)
     */
    private $idUser;

    /**
     * @var string $answer
     *
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @var \DateTime $responseTime
     *
     * @ORM\Column(name="response_time", type="datetime", nullable=true)
     */
    private $responseTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmployeeFeedbackQuestion
     *
     * @param integer $idEmployeeFeedbackQuestion
     * @return EmployeeFeedbackResponse
     */
    public function setIdEmployeeFeedbackQuestion($idEmployeeFeedbackQuestion)
    {
        $this->idEmployeeFeedbackQuestion = $idEmployeeFeedbackQuestion;
    
        return $this;
    }

    /**
     * Get idEmployeeFeedbackQuestion
     *
     * @return integer 
     */
    public function getIdEmployeeFeedbackQuestion()
    {
        return $this->idEmployeeFeedbackQuestion;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     * @return EmployeeFeedbackResponse
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return string 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return EmployeeFeedbackResponse
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set responseTime
     *
     * @param \DateTime $responseTime
     * @return EmployeeFeedbackResponse
     */
    public function setResponseTime($responseTime)
    {
        $this->responseTime = $responseTime;
    
        return $this;
    }

    /**
     * Get responseTime
     *
     * @return \DateTime 
     */
    public function getResponseTime()
    {
        return $this->responseTime;
    }
}