<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CustomerCopyHistory
 *
 * @ORM\Table(name="customer_copy_history")
 * @ORM\Entity
 */
class CustomerCopyHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idOriginalCustomer
     *
     * @ORM\Column(name="id_original_customer", type="integer", nullable=true)
     */
    private $idOriginalCustomer;

    /**
     * @var \DateTime $copyDatetime
     *
     * @ORM\Column(name="copy_datetime", type="datetime", nullable=true)
     */
    private $copyDatetime;

    /**
     * @var integer $idFromCustomer
     *
     * @ORM\Column(name="id_from_customer", type="integer", nullable=true)
     */
    private $idFromCustomer;

    /**
     * @var integer $idToCustomer
     *
     * @ORM\Column(name="id_to_customer", type="integer", nullable=true)
     */
    private $idToCustomer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOriginalCustomer
     *
     * @param integer $idOriginalCustomer
     * @return CustomerCopyHistory
     */
    public function setIdOriginalCustomer($idOriginalCustomer)
    {
        $this->idOriginalCustomer = $idOriginalCustomer;
    
        return $this;
    }

    /**
     * Get idOriginalCustomer
     *
     * @return integer 
     */
    public function getIdOriginalCustomer()
    {
        return $this->idOriginalCustomer;
    }

    /**
     * Set copyDatetime
     *
     * @param \DateTime $copyDatetime
     * @return CustomerCopyHistory
     */
    public function setCopyDatetime($copyDatetime)
    {
        $this->copyDatetime = $copyDatetime;
    
        return $this;
    }

    /**
     * Get copyDatetime
     *
     * @return \DateTime 
     */
    public function getCopyDatetime()
    {
        return $this->copyDatetime;
    }

    /**
     * Set idFromCustomer
     *
     * @param integer $idFromCustomer
     * @return CustomerCopyHistory
     */
    public function setIdFromCustomer($idFromCustomer)
    {
        $this->idFromCustomer = $idFromCustomer;
    
        return $this;
    }

    /**
     * Get idFromCustomer
     *
     * @return integer 
     */
    public function getIdFromCustomer()
    {
        return $this->idFromCustomer;
    }

    /**
     * Set idToCustomer
     *
     * @param integer $idToCustomer
     * @return CustomerCopyHistory
     */
    public function setIdToCustomer($idToCustomer)
    {
        $this->idToCustomer = $idToCustomer;
    
        return $this;
    }

    /**
     * Get idToCustomer
     *
     * @return integer 
     */
    public function getIdToCustomer()
    {
        return $this->idToCustomer;
    }
}