<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ProdGeneLink
 *
 * @ORM\Table(name="prod_gene_link")
 * @ORM\Entity
 */
class ProdGeneLink
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $productid
     *
     * @ORM\Column(name="ProductId", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var integer $geneid
     *
     * @ORM\Column(name="GeneId", type="integer", nullable=true)
     */
    private $geneid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProdGeneLink
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    
        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set geneid
     *
     * @param integer $geneid
     * @return ProdGeneLink
     */
    public function setGeneid($geneid)
    {
        $this->geneid = $geneid;
    
        return $this;
    }

    /**
     * Get geneid
     *
     * @return integer 
     */
    public function getGeneid()
    {
        return $this->geneid;
    }
}