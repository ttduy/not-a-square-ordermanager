<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoStorageCellLogs
 *
 * @ORM\Table(name="geno_storage_cell_logs")
 * @ORM\Entity
 */
class GenoStorageCellLogs
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $cellId
     *
     * @ORM\Column(name="cell_id", type="integer", nullable=true)
     */
    private $cellId;

    /**
     * @var integer $amount
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var string $helper
     *
     * @ORM\Column(name="helper", type="string", length=255, nullable=true)
     */
    private $helper;

    /**
     * @var integer $user
     *
     * @ORM\Column(name="user", type="integer", nullable=true)
     */
    private $user;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cellId
     *
     * @param integer $cellId
     * @return GenoStorageCellLogs
     */
    public function setCellId($cellId)
    {
        $this->cellId = $cellId;
    
        return $this;
    }

    /**
     * Get cellId
     *
     * @return integer 
     */
    public function getCellId()
    {
        return $this->cellId;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return GenoStorageCellLogs
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set helper
     *
     * @param string $helper
     * @return GenoStorageCellLogs
     */
    public function setHelper($helper)
    {
        $this->helper = $helper;
    
        return $this;
    }

    /**
     * Get helper
     *
     * @return string 
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return GenoStorageCellLogs
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return GenoStorageCellLogs
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}