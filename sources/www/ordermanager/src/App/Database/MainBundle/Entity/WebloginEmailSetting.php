<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WebloginEmailSetting
 *
 * @ORM\Table(name="weblogin_email_setting")
 * @ORM\Entity
 */
class WebloginEmailSetting
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idStatus
     *
     * @ORM\Column(name="id_status", type="integer", nullable=true)
     */
    private $idStatus;

    /**
     * @var integer $idEmailTemplate
     *
     * @ORM\Column(name="id_email_template", type="integer", nullable=true)
     */
    private $idEmailTemplate;

    /**
     * @var integer $idEmailMethod
     *
     * @ORM\Column(name="id_email_method", type="integer", nullable=true)
     */
    private $idEmailMethod;

    /**
     * @var boolean $isSendNow
     *
     * @ORM\Column(name="is_send_now", type="boolean", nullable=true)
     */
    private $isSendNow;

    /**
     * @var boolean $isAutoRetry
     *
     * @ORM\Column(name="is_auto_retry", type="boolean", nullable=true)
     */
    private $isAutoRetry;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idStatus
     *
     * @param integer $idStatus
     * @return WebloginEmailSetting
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;
    
        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer 
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set idEmailTemplate
     *
     * @param integer $idEmailTemplate
     * @return WebloginEmailSetting
     */
    public function setIdEmailTemplate($idEmailTemplate)
    {
        $this->idEmailTemplate = $idEmailTemplate;
    
        return $this;
    }

    /**
     * Get idEmailTemplate
     *
     * @return integer 
     */
    public function getIdEmailTemplate()
    {
        return $this->idEmailTemplate;
    }

    /**
     * Set idEmailMethod
     *
     * @param integer $idEmailMethod
     * @return WebloginEmailSetting
     */
    public function setIdEmailMethod($idEmailMethod)
    {
        $this->idEmailMethod = $idEmailMethod;
    
        return $this;
    }

    /**
     * Get idEmailMethod
     *
     * @return integer 
     */
    public function getIdEmailMethod()
    {
        return $this->idEmailMethod;
    }

    /**
     * Set isSendNow
     *
     * @param boolean $isSendNow
     * @return WebloginEmailSetting
     */
    public function setIsSendNow($isSendNow)
    {
        $this->isSendNow = $isSendNow;
    
        return $this;
    }

    /**
     * Get isSendNow
     *
     * @return boolean 
     */
    public function getIsSendNow()
    {
        return $this->isSendNow;
    }

    /**
     * Set isAutoRetry
     *
     * @param boolean $isAutoRetry
     * @return WebloginEmailSetting
     */
    public function setIsAutoRetry($isAutoRetry)
    {
        $this->isAutoRetry = $isAutoRetry;
    
        return $this;
    }

    /**
     * Get isAutoRetry
     *
     * @return boolean 
     */
    public function getIsAutoRetry()
    {
        return $this->isAutoRetry;
    }
}