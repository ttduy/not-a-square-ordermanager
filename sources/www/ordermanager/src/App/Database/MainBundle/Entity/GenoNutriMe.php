<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoNutriMe
 *
 * @ORM\Table(name="geno_nutri_me")
 * @ORM\Entity
 */
class GenoNutriMe
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $barcode
     *
     * @ORM\Column(name="barcode", type="text", nullable=true)
     */
    private $barcode;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var string $orderNumber
     *
     * @ORM\Column(name="order_number", type="string", length=255, nullable=true)
     */
    private $orderNumber;

    /**
     * @var string $customerName
     *
     * @ORM\Column(name="customer_name", type="string", length=255, nullable=true)
     */
    private $customerName;

    /**
     * @var integer $supplementDaysOrder
     *
     * @ORM\Column(name="supplement_days_order", type="integer", nullable=true)
     */
    private $supplementDaysOrder;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var \DateTime $currentStatusDate
     *
     * @ORM\Column(name="current_status_date", type="date", nullable=true)
     */
    private $currentStatusDate;

    /**
     * @var float $actualUnit
     *
     * @ORM\Column(name="actual_unit", type="decimal", nullable=true)
     */
    private $actualUnit;

    /**
     * @var \DateTime $processedOn
     *
     * @ORM\Column(name="processed_on", type="datetime", nullable=true)
     */
    private $processedOn;

    /**
     * @var integer $processedBy
     *
     * @ORM\Column(name="processed_by", type="integer", nullable=true)
     */
    private $processedBy;

    /**
     * @var string $statusList
     *
     * @ORM\Column(name="status_list", type="text", nullable=true)
     */
    private $statusList;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var string $currentLots
     *
     * @ORM\Column(name="current_lots", type="text", nullable=true)
     */
    private $currentLots;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     * @return GenoNutriMe
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    
        return $this;
    }

    /**
     * Get barcode
     *
     * @return string 
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return GenoNutriMe
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return GenoNutriMe
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    
        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     * @return GenoNutriMe
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    
        return $this;
    }

    /**
     * Get customerName
     *
     * @return string 
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set supplementDaysOrder
     *
     * @param integer $supplementDaysOrder
     * @return GenoNutriMe
     */
    public function setSupplementDaysOrder($supplementDaysOrder)
    {
        $this->supplementDaysOrder = $supplementDaysOrder;
    
        return $this;
    }

    /**
     * Get supplementDaysOrder
     *
     * @return integer 
     */
    public function getSupplementDaysOrder()
    {
        return $this->supplementDaysOrder;
    }

    /**
     * Set currentStatus
     *
     * @param integer $currentStatus
     * @return GenoNutriMe
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return integer 
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currentStatusDate
     *
     * @param \DateTime $currentStatusDate
     * @return GenoNutriMe
     */
    public function setCurrentStatusDate($currentStatusDate)
    {
        $this->currentStatusDate = $currentStatusDate;
    
        return $this;
    }

    /**
     * Get currentStatusDate
     *
     * @return \DateTime 
     */
    public function getCurrentStatusDate()
    {
        return $this->currentStatusDate;
    }

    /**
     * Set actualUnit
     *
     * @param float $actualUnit
     * @return GenoNutriMe
     */
    public function setActualUnit($actualUnit)
    {
        $this->actualUnit = $actualUnit;
    
        return $this;
    }

    /**
     * Get actualUnit
     *
     * @return float 
     */
    public function getActualUnit()
    {
        return $this->actualUnit;
    }

    /**
     * Set processedOn
     *
     * @param \DateTime $processedOn
     * @return GenoNutriMe
     */
    public function setProcessedOn($processedOn)
    {
        $this->processedOn = $processedOn;
    
        return $this;
    }

    /**
     * Get processedOn
     *
     * @return \DateTime 
     */
    public function getProcessedOn()
    {
        return $this->processedOn;
    }

    /**
     * Set processedBy
     *
     * @param integer $processedBy
     * @return GenoNutriMe
     */
    public function setProcessedBy($processedBy)
    {
        $this->processedBy = $processedBy;
    
        return $this;
    }

    /**
     * Get processedBy
     *
     * @return integer 
     */
    public function getProcessedBy()
    {
        return $this->processedBy;
    }

    /**
     * Set statusList
     *
     * @param string $statusList
     * @return GenoNutriMe
     */
    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    
        return $this;
    }

    /**
     * Get statusList
     *
     * @return string 
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return GenoNutriMe
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set currentLots
     *
     * @param string $currentLots
     * @return GenoNutriMe
     */
    public function setCurrentLots($currentLots)
    {
        $this->currentLots = $currentLots;
    
        return $this;
    }

    /**
     * Get currentLots
     *
     * @return string 
     */
    public function getCurrentLots()
    {
        return $this->currentLots;
    }
}