<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PartnerRatingHistory
 *
 * @ORM\Table(name="partner_rating_history")
 * @ORM\Entity
 */
class PartnerRatingHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idRating
     *
     * @ORM\Column(name="id_rating", type="integer", nullable=true)
     */
    private $idRating;

    /**
     * @var integer $idPartner
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=true)
     */
    private $idPartner;

    /**
     * @var integer $oldRating
     *
     * @ORM\Column(name="old_rating", type="integer", nullable=true)
     */
    private $oldRating;

    /**
     * @var integer $newRating
     *
     * @ORM\Column(name="new_rating", type="integer", nullable=true)
     */
    private $newRating;

    /**
     * @var integer $totalOfOrders
     *
     * @ORM\Column(name="total_of_orders", type="integer", nullable=true)
     */
    private $totalOfOrders;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="date", nullable=true)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idRating
     *
     * @param integer $idRating
     * @return PartnerRatingHistory
     */
    public function setIdRating($idRating)
    {
        $this->idRating = $idRating;
    
        return $this;
    }

    /**
     * Get idRating
     *
     * @return integer 
     */
    public function getIdRating()
    {
        return $this->idRating;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     * @return PartnerRatingHistory
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;
    
        return $this;
    }

    /**
     * Get idPartner
     *
     * @return integer 
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set oldRating
     *
     * @param integer $oldRating
     * @return PartnerRatingHistory
     */
    public function setOldRating($oldRating)
    {
        $this->oldRating = $oldRating;
    
        return $this;
    }

    /**
     * Get oldRating
     *
     * @return integer 
     */
    public function getOldRating()
    {
        return $this->oldRating;
    }

    /**
     * Set newRating
     *
     * @param integer $newRating
     * @return PartnerRatingHistory
     */
    public function setNewRating($newRating)
    {
        $this->newRating = $newRating;
    
        return $this;
    }

    /**
     * Get newRating
     *
     * @return integer 
     */
    public function getNewRating()
    {
        return $this->newRating;
    }

    /**
     * Set totalOfOrders
     *
     * @param integer $totalOfOrders
     * @return PartnerRatingHistory
     */
    public function setTotalOfOrders($totalOfOrders)
    {
        $this->totalOfOrders = $totalOfOrders;
    
        return $this;
    }

    /**
     * Get totalOfOrders
     *
     * @return integer 
     */
    public function getTotalOfOrders()
    {
        return $this->totalOfOrders;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PartnerRatingHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}