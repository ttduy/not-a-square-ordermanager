<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CryosaveSampleStatus
 *
 * @ORM\Table(name="cryosave_sample_status")
 * @ORM\Entity
 */
class CryosaveSampleStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCryosaveSample
     *
     * @ORM\Column(name="id_cryosave_sample", type="integer", nullable=false)
     */
    private $idCryosaveSample;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCryosaveSample
     *
     * @param integer $idCryosaveSample
     * @return CryosaveSampleStatus
     */
    public function setIdCryosaveSample($idCryosaveSample)
    {
        $this->idCryosaveSample = $idCryosaveSample;
    
        return $this;
    }

    /**
     * Get idCryosaveSample
     *
     * @return integer 
     */
    public function getIdCryosaveSample()
    {
        return $this->idCryosaveSample;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return CryosaveSampleStatus
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CryosaveSampleStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CryosaveSampleStatus
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return CryosaveSampleStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}