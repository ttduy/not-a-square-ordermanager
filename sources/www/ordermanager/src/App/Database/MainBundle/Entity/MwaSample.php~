<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaSample
 *
 * @ORM\Table(name="mwa_sample")
 * @ORM\Entity
 */
class MwaSample
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $sampleId
     *
     * @ORM\Column(name="sample_id", type="string", length=255, nullable=true)
     */
    private $sampleId;

    /**
     * @var string $sampleGroup
     *
     * @ORM\Column(name="sample_group", type="string", length=255, nullable=true)
     */
    private $sampleGroup;

    /**
     * @var integer $age
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string $resultPattern
     *
     * @ORM\Column(name="result_pattern", type="string", length=255, nullable=true)
     */
    private $resultPattern;

    /**
     * @var integer $currentStatus
     *
     * @ORM\Column(name="current_status", type="integer", nullable=true)
     */
    private $currentStatus;

    /**
     * @var boolean $isDeleted
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var boolean $isSendNow
     *
     * @ORM\Column(name="is_send_now", type="boolean", nullable=true)
     */
    private $isSendNow;

    /**
     * @var integer $sendingStatus
     *
     * @ORM\Column(name="sending_status", type="integer", nullable=true)
     */
    private $sendingStatus;

    /**
     * @var integer $pushStatus
     *
     * @ORM\Column(name="push_status", type="integer", nullable=true)
     */
    private $pushStatus;

    /**
     * @var \DateTime $pushDate
     *
     * @ORM\Column(name="push_date", type="date", nullable=true)
     */
    private $pushDate;

    /**
     * @var string $lastError
     *
     * @ORM\Column(name="last_error", type="text", nullable=true)
     */
    private $lastError;

    /**
     * @var \DateTime $pushDatetime
     *
     * @ORM\Column(name="push_datetime", type="datetime", nullable=true)
     */
    private $pushDatetime;

    /**
     * @var integer $idMwaInvoice
     *
     * @ORM\Column(name="id_mwa_invoice", type="integer", nullable=true)
     */
    private $idMwaInvoice;

    /**
     * @var float $priceOverride
     *
     * @ORM\Column(name="price_override", type="decimal", nullable=true)
     */
    private $priceOverride;

    /**
     * @var \DateTime $registeredDate
     *
     * @ORM\Column(name="registered_date", type="date", nullable=true)
     */
    private $registeredDate;

    /**
     * @var \DateTime $finishedDate
     *
     * @ORM\Column(name="finished_date", type="date", nullable=true)
     */
    private $finishedDate;

    /**
     * @var boolean $isOverrideSending
     *
     * @ORM\Column(name="is_override_sending", type="boolean", nullable=true)
     */
    private $isOverrideSending;

    /**
     * @var integer $numFailed
     *
     * @ORM\Column(name="num_failed", type="integer", nullable=true)
     */
    private $numFailed;

    /**
     * @var string $infoText
     *
     * @ORM\Column(name="info_text", type="text", nullable=true)
     */
    private $infoText;

    /**
     * @var integer $idGroup
     *
     * @ORM\Column(name="id_group", type="integer", nullable=true)
     */
    private $idGroup;

    /**
     * @var integer $isLocal
     *
     * @ORM\Column(name="is_local", type="integer", nullable=true)
     */
    private $isLocal;

    /**
     * @var integer $isError
     *
     * @ORM\Column(name="is_error", type="integer", nullable=true)
     */
    private $isError;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var string $gew1
     *
     * @ORM\Column(name="gew_1", type="string", length=25, nullable=true)
     */
    private $gew1;

    /**
     * @var string $gew2
     *
     * @ORM\Column(name="gew_2", type="string", length=25, nullable=true)
     */
    private $gew2;

    /**
     * @var string $gew3
     *
     * @ORM\Column(name="gew_3", type="string", length=25, nullable=true)
     */
    private $gew3;

    /**
     * @var string $gew4
     *
     * @ORM\Column(name="gew_4", type="string", length=25, nullable=true)
     */
    private $gew4;

    /**
     * @var string $gew5
     *
     * @ORM\Column(name="gew_5", type="string", length=25, nullable=true)
     */
    private $gew5;


}
