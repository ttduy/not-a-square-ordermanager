<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FoodTableFoodItem
 *
 * @ORM\Table(name="food_table_food_item")
 * @ORM\Entity
 */
class FoodTableFoodItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCategory
     *
     * @ORM\Column(name="id_category", type="integer", nullable=true)
     */
    private $idCategory;

    /**
     * @var string $foodId
     *
     * @ORM\Column(name="food_id", type="string", length=255, nullable=true)
     */
    private $foodId;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var float $gPortion
     *
     * @ORM\Column(name="g_portion", type="decimal", nullable=true)
     */
    private $gPortion;

    /**
     * @var float $kcal
     *
     * @ORM\Column(name="kcal", type="decimal", nullable=true)
     */
    private $kcal;

    /**
     * @var float $protG
     *
     * @ORM\Column(name="prot_g", type="decimal", nullable=true)
     */
    private $protG;

    /**
     * @var float $carbG
     *
     * @ORM\Column(name="carb_g", type="decimal", nullable=true)
     */
    private $carbG;

    /**
     * @var float $fatG
     *
     * @ORM\Column(name="fat_g", type="decimal", nullable=true)
     */
    private $fatG;

    /**
     * @var string $ingredientData
     *
     * @ORM\Column(name="ingredient_data", type="text", nullable=true)
     */
    private $ingredientData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     * @return FoodTableFoodItem
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    
        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer 
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * Set foodId
     *
     * @param string $foodId
     * @return FoodTableFoodItem
     */
    public function setFoodId($foodId)
    {
        $this->foodId = $foodId;
    
        return $this;
    }

    /**
     * Get foodId
     *
     * @return string 
     */
    public function getFoodId()
    {
        return $this->foodId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FoodTableFoodItem
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gPortion
     *
     * @param float $gPortion
     * @return FoodTableFoodItem
     */
    public function setGPortion($gPortion)
    {
        $this->gPortion = $gPortion;
    
        return $this;
    }

    /**
     * Get gPortion
     *
     * @return float 
     */
    public function getGPortion()
    {
        return $this->gPortion;
    }

    /**
     * Set kcal
     *
     * @param float $kcal
     * @return FoodTableFoodItem
     */
    public function setKcal($kcal)
    {
        $this->kcal = $kcal;
    
        return $this;
    }

    /**
     * Get kcal
     *
     * @return float 
     */
    public function getKcal()
    {
        return $this->kcal;
    }

    /**
     * Set protG
     *
     * @param float $protG
     * @return FoodTableFoodItem
     */
    public function setProtG($protG)
    {
        $this->protG = $protG;
    
        return $this;
    }

    /**
     * Get protG
     *
     * @return float 
     */
    public function getProtG()
    {
        return $this->protG;
    }

    /**
     * Set carbG
     *
     * @param float $carbG
     * @return FoodTableFoodItem
     */
    public function setCarbG($carbG)
    {
        $this->carbG = $carbG;
    
        return $this;
    }

    /**
     * Get carbG
     *
     * @return float 
     */
    public function getCarbG()
    {
        return $this->carbG;
    }

    /**
     * Set fatG
     *
     * @param float $fatG
     * @return FoodTableFoodItem
     */
    public function setFatG($fatG)
    {
        $this->fatG = $fatG;
    
        return $this;
    }

    /**
     * Get fatG
     *
     * @return float 
     */
    public function getFatG()
    {
        return $this->fatG;
    }

    /**
     * Set ingredientData
     *
     * @param string $ingredientData
     * @return FoodTableFoodItem
     */
    public function setIngredientData($ingredientData)
    {
        $this->ingredientData = $ingredientData;
    
        return $this;
    }

    /**
     * Get ingredientData
     *
     * @return string 
     */
    public function getIngredientData()
    {
        return $this->ingredientData;
    }
}