<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CatProdLink
 *
 * @ORM\Table(name="cat_prod_link")
 * @ORM\Entity
 */
class CatProdLink
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $categoryid
     *
     * @ORM\Column(name="CategoryId", type="integer", nullable=true)
     */
    private $categoryid;

    /**
     * @var integer $productid
     *
     * @ORM\Column(name="ProductId", type="integer", nullable=true)
     */
    private $productid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     * @return CatProdLink
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;
    
        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return CatProdLink
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    
        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }
}