<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FeedbackResponse
 *
 * @ORM\Table(name="feedback_response")
 * @ORM\Entity
 */
class FeedbackResponse
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idFeedbackQuestion
     *
     * @ORM\Column(name="id_feedback_question", type="integer", nullable=true)
     */
    private $idFeedbackQuestion;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $answer
     *
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @var \DateTime $responseTime
     *
     * @ORM\Column(name="response_time", type="datetime", nullable=true)
     */
    private $responseTime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFeedbackQuestion
     *
     * @param integer $idFeedbackQuestion
     * @return FeedbackResponse
     */
    public function setIdFeedbackQuestion($idFeedbackQuestion)
    {
        $this->idFeedbackQuestion = $idFeedbackQuestion;
    
        return $this;
    }

    /**
     * Get idFeedbackQuestion
     *
     * @return integer 
     */
    public function getIdFeedbackQuestion()
    {
        return $this->idFeedbackQuestion;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FeedbackResponse
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return FeedbackResponse
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set responseTime
     *
     * @param \DateTime $responseTime
     * @return FeedbackResponse
     */
    public function setResponseTime($responseTime)
    {
        $this->responseTime = $responseTime;
    
        return $this;
    }

    /**
     * Get responseTime
     *
     * @return \DateTime 
     */
    public function getResponseTime()
    {
        return $this->responseTime;
    }
}