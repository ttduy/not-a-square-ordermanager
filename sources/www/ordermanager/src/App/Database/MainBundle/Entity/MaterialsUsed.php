<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MaterialsUsed
 *
 * @ORM\Table(name="materials_used")
 * @ORM\Entity
 */
class MaterialsUsed
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $materialid
     *
     * @ORM\Column(name="MaterialId", type="integer", nullable=true)
     */
    private $materialid;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(name="Quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float $value
     *
     * @ORM\Column(name="Value", type="decimal", nullable=true)
     */
    private $value;

    /**
     * @var string $usedfor
     *
     * @ORM\Column(name="UsedFor", type="string", length=255, nullable=true)
     */
    private $usedfor;

    /**
     * @var \DateTime $datetaken
     *
     * @ORM\Column(name="DateTaken", type="date", nullable=true)
     */
    private $datetaken;

    /**
     * @var string $takenby
     *
     * @ORM\Column(name="TakenBy", type="string", length=255, nullable=true)
     */
    private $takenby;

    /**
     * @var boolean $isSelected
     *
     * @ORM\Column(name="is_selected", type="boolean", nullable=true)
     */
    private $isSelected;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialid
     *
     * @param integer $materialid
     * @return MaterialsUsed
     */
    public function setMaterialid($materialid)
    {
        $this->materialid = $materialid;
    
        return $this;
    }

    /**
     * Get materialid
     *
     * @return integer 
     */
    public function getMaterialid()
    {
        return $this->materialid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return MaterialsUsed
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return MaterialsUsed
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set usedfor
     *
     * @param string $usedfor
     * @return MaterialsUsed
     */
    public function setUsedfor($usedfor)
    {
        $this->usedfor = $usedfor;
    
        return $this;
    }

    /**
     * Get usedfor
     *
     * @return string 
     */
    public function getUsedfor()
    {
        return $this->usedfor;
    }

    /**
     * Set datetaken
     *
     * @param \DateTime $datetaken
     * @return MaterialsUsed
     */
    public function setDatetaken($datetaken)
    {
        $this->datetaken = $datetaken;
    
        return $this;
    }

    /**
     * Get datetaken
     *
     * @return \DateTime 
     */
    public function getDatetaken()
    {
        return $this->datetaken;
    }

    /**
     * Set takenby
     *
     * @param string $takenby
     * @return MaterialsUsed
     */
    public function setTakenby($takenby)
    {
        $this->takenby = $takenby;
    
        return $this;
    }

    /**
     * Get takenby
     *
     * @return string 
     */
    public function getTakenby()
    {
        return $this->takenby;
    }

    /**
     * Set isSelected
     *
     * @param boolean $isSelected
     * @return MaterialsUsed
     */
    public function setIsSelected($isSelected)
    {
        $this->isSelected = $isSelected;
    
        return $this;
    }

    /**
     * Get isSelected
     *
     * @return boolean 
     */
    public function getIsSelected()
    {
        return $this->isSelected;
    }
}