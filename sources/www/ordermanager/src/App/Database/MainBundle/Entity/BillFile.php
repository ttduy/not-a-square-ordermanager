<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillFile
 *
 * @ORM\Table(name="bill_file")
 * @ORM\Entity
 */
class BillFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBill
     *
     * @ORM\Column(name="id_bill", type="integer", nullable=true)
     */
    private $idBill;

    /**
     * @var \DateTime $generateDate
     *
     * @ORM\Column(name="generate_date", type="datetime", nullable=true)
     */
    private $generateDate;

    /**
     * @var string $billFilename
     *
     * @ORM\Column(name="bill_filename", type="string", length=255, nullable=true)
     */
    private $billFilename;

    /**
     * @var float $billAmount
     *
     * @ORM\Column(name="bill_amount", type="decimal", nullable=true)
     */
    private $billAmount;

    /**
     * @var boolean $isActive
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBill
     *
     * @param integer $idBill
     * @return BillFile
     */
    public function setIdBill($idBill)
    {
        $this->idBill = $idBill;
    
        return $this;
    }

    /**
     * Get idBill
     *
     * @return integer 
     */
    public function getIdBill()
    {
        return $this->idBill;
    }

    /**
     * Set generateDate
     *
     * @param \DateTime $generateDate
     * @return BillFile
     */
    public function setGenerateDate($generateDate)
    {
        $this->generateDate = $generateDate;
    
        return $this;
    }

    /**
     * Get generateDate
     *
     * @return \DateTime 
     */
    public function getGenerateDate()
    {
        return $this->generateDate;
    }

    /**
     * Set billFilename
     *
     * @param string $billFilename
     * @return BillFile
     */
    public function setBillFilename($billFilename)
    {
        $this->billFilename = $billFilename;
    
        return $this;
    }

    /**
     * Get billFilename
     *
     * @return string 
     */
    public function getBillFilename()
    {
        return $this->billFilename;
    }

    /**
     * Set billAmount
     *
     * @param float $billAmount
     * @return BillFile
     */
    public function setBillAmount($billAmount)
    {
        $this->billAmount = $billAmount;
    
        return $this;
    }

    /**
     * Get billAmount
     *
     * @return float 
     */
    public function getBillAmount()
    {
        return $this->billAmount;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return BillFile
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BillFile
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}