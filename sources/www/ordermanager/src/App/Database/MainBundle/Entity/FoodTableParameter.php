<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\FoodTableParameter
 *
 * @ORM\Table(name="food_table_parameter")
 * @ORM\Entity
 */
class FoodTableParameter
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $colorGreenRiskamount
     *
     * @ORM\Column(name="color_green_riskamount", type="integer", nullable=true)
     */
    private $colorGreenRiskamount;

    /**
     * @var integer $colorRedRiskamount
     *
     * @ORM\Column(name="color_red_riskamount", type="integer", nullable=true)
     */
    private $colorRedRiskamount;

    /**
     * @var integer $foodScale
     *
     * @ORM\Column(name="food_scale", type="integer", nullable=true)
     */
    private $foodScale;

    /**
     * @var integer $warnungId
     *
     * @ORM\Column(name="warnung_id", type="integer", nullable=true)
     */
    private $warnungId;

    /**
     * @var integer $laktoseId
     *
     * @ORM\Column(name="laktose_id", type="integer", nullable=true)
     */
    private $laktoseId;

    /**
     * @var integer $glutenId
     *
     * @ORM\Column(name="gluten_id", type="integer", nullable=true)
     */
    private $glutenId;

    /**
     * @var integer $gId
     *
     * @ORM\Column(name="g_id", type="integer", nullable=true)
     */
    private $gId;

    /**
     * @var integer $kcalId
     *
     * @ORM\Column(name="kcal_id", type="integer", nullable=true)
     */
    private $kcalId;

    /**
     * @var integer $eiwId
     *
     * @ORM\Column(name="eiw_id", type="integer", nullable=true)
     */
    private $eiwId;

    /**
     * @var integer $kohId
     *
     * @ORM\Column(name="koh_id", type="integer", nullable=true)
     */
    private $kohId;

    /**
     * @var integer $fettId
     *
     * @ORM\Column(name="fett_id", type="integer", nullable=true)
     */
    private $fettId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorGreenRiskamount
     *
     * @param integer $colorGreenRiskamount
     * @return FoodTableParameter
     */
    public function setColorGreenRiskamount($colorGreenRiskamount)
    {
        $this->colorGreenRiskamount = $colorGreenRiskamount;
    
        return $this;
    }

    /**
     * Get colorGreenRiskamount
     *
     * @return integer 
     */
    public function getColorGreenRiskamount()
    {
        return $this->colorGreenRiskamount;
    }

    /**
     * Set colorRedRiskamount
     *
     * @param integer $colorRedRiskamount
     * @return FoodTableParameter
     */
    public function setColorRedRiskamount($colorRedRiskamount)
    {
        $this->colorRedRiskamount = $colorRedRiskamount;
    
        return $this;
    }

    /**
     * Get colorRedRiskamount
     *
     * @return integer 
     */
    public function getColorRedRiskamount()
    {
        return $this->colorRedRiskamount;
    }

    /**
     * Set foodScale
     *
     * @param integer $foodScale
     * @return FoodTableParameter
     */
    public function setFoodScale($foodScale)
    {
        $this->foodScale = $foodScale;
    
        return $this;
    }

    /**
     * Get foodScale
     *
     * @return integer 
     */
    public function getFoodScale()
    {
        return $this->foodScale;
    }

    /**
     * Set warnungId
     *
     * @param integer $warnungId
     * @return FoodTableParameter
     */
    public function setWarnungId($warnungId)
    {
        $this->warnungId = $warnungId;
    
        return $this;
    }

    /**
     * Get warnungId
     *
     * @return integer 
     */
    public function getWarnungId()
    {
        return $this->warnungId;
    }

    /**
     * Set laktoseId
     *
     * @param integer $laktoseId
     * @return FoodTableParameter
     */
    public function setLaktoseId($laktoseId)
    {
        $this->laktoseId = $laktoseId;
    
        return $this;
    }

    /**
     * Get laktoseId
     *
     * @return integer 
     */
    public function getLaktoseId()
    {
        return $this->laktoseId;
    }

    /**
     * Set glutenId
     *
     * @param integer $glutenId
     * @return FoodTableParameter
     */
    public function setGlutenId($glutenId)
    {
        $this->glutenId = $glutenId;
    
        return $this;
    }

    /**
     * Get glutenId
     *
     * @return integer 
     */
    public function getGlutenId()
    {
        return $this->glutenId;
    }

    /**
     * Set gId
     *
     * @param integer $gId
     * @return FoodTableParameter
     */
    public function setGId($gId)
    {
        $this->gId = $gId;
    
        return $this;
    }

    /**
     * Get gId
     *
     * @return integer 
     */
    public function getGId()
    {
        return $this->gId;
    }

    /**
     * Set kcalId
     *
     * @param integer $kcalId
     * @return FoodTableParameter
     */
    public function setKcalId($kcalId)
    {
        $this->kcalId = $kcalId;
    
        return $this;
    }

    /**
     * Get kcalId
     *
     * @return integer 
     */
    public function getKcalId()
    {
        return $this->kcalId;
    }

    /**
     * Set eiwId
     *
     * @param integer $eiwId
     * @return FoodTableParameter
     */
    public function setEiwId($eiwId)
    {
        $this->eiwId = $eiwId;
    
        return $this;
    }

    /**
     * Get eiwId
     *
     * @return integer 
     */
    public function getEiwId()
    {
        return $this->eiwId;
    }

    /**
     * Set kohId
     *
     * @param integer $kohId
     * @return FoodTableParameter
     */
    public function setKohId($kohId)
    {
        $this->kohId = $kohId;
    
        return $this;
    }

    /**
     * Get kohId
     *
     * @return integer 
     */
    public function getKohId()
    {
        return $this->kohId;
    }

    /**
     * Set fettId
     *
     * @param integer $fettId
     * @return FoodTableParameter
     */
    public function setFettId($fettId)
    {
        $this->fettId = $fettId;
    
        return $this;
    }

    /**
     * Get fettId
     *
     * @return integer 
     */
    public function getFettId()
    {
        return $this->fettId;
    }
}