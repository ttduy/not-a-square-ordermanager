<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity
 */
class Customer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idOrderBackup
     *
     * @ORM\Column(name="id_order_backup", type="integer", nullable=true)
     */
    private $idOrderBackup;

    /**
     * @var integer $idCustomerInfo
     *
     * @ORM\Column(name="id_customer_info", type="integer", nullable=true)
     */
    private $idCustomerInfo;

    /**
     * @var string $ordernumber
     *
     * @ORM\Column(name="OrderNumber", type="string", length=255, nullable=true)
     */
    private $ordernumber;

    /**
     * @var integer $distributionchannelid
     *
     * @ORM\Column(name="DistributionChannelId", type="integer", nullable=true)
     */
    private $distributionchannelid;

    /**
     * @var string $domain
     *
     * @ORM\Column(name="Domain", type="string", length=255, nullable=true)
     */
    private $domain;

    /**
     * @var \DateTime $dateordered
     *
     * @ORM\Column(name="DateOrdered", type="date", nullable=true)
     */
    private $dateordered;

    /**
     * @var integer $orderage
     *
     * @ORM\Column(name="OrderAge", type="integer", nullable=true)
     */
    private $orderage;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $genderid
     *
     * @ORM\Column(name="GenderId", type="integer", nullable=true)
     */
    private $genderid;

    /**
     * @var \DateTime $dateofbirth
     *
     * @ORM\Column(name="DateOfBirth", type="date", nullable=true)
     */
    private $dateofbirth;

    /**
     * @var string $title
     *
     * @ORM\Column(name="Title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $firstname
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="SurName", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string $street
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string $street2
     *
     * @ORM\Column(name="Street2", type="string", length=255, nullable=true)
     */
    private $street2;

    /**
     * @var string $postcode
     *
     * @ORM\Column(name="PostCode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string $city
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var integer $countryid
     *
     * @ORM\Column(name="CountryId", type="integer", nullable=true)
     */
    private $countryid;

    /**
     * @var string $email
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $telephone
     *
     * @ORM\Column(name="Telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string $fax
     *
     * @ORM\Column(name="Fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $invoiceexist
     *
     * @ORM\Column(name="InvoiceExist", type="integer", nullable=true)
     */
    private $invoiceexist;

    /**
     * @var string $invoicenumber
     *
     * @ORM\Column(name="InvoiceNumber", type="string", length=255, nullable=true)
     */
    private $invoicenumber;

    /**
     * @var \DateTime $invoicedate
     *
     * @ORM\Column(name="InvoiceDate", type="date", nullable=true)
     */
    private $invoicedate;

    /**
     * @var integer $invoicefromcompanyid
     *
     * @ORM\Column(name="InvoiceFromCompanyId", type="integer", nullable=true)
     */
    private $invoicefromcompanyid;

    /**
     * @var boolean $iswithtax
     *
     * @ORM\Column(name="IsWithTax", type="boolean", nullable=true)
     */
    private $iswithtax;

    /**
     * @var integer $invoicestatus
     *
     * @ORM\Column(name="InvoiceStatus", type="integer", nullable=true)
     */
    private $invoicestatus;

    /**
     * @var float $postage
     *
     * @ORM\Column(name="Postage", type="decimal", nullable=true)
     */
    private $postage;

    /**
     * @var float $priceoverride
     *
     * @ORM\Column(name="PriceOverride", type="decimal", nullable=true)
     */
    private $priceoverride;

    /**
     * @var string $invoiceinfo
     *
     * @ORM\Column(name="InvoiceInfo", type="text", nullable=true)
     */
    private $invoiceinfo;

    /**
     * @var integer $isinvoicecustomer
     *
     * @ORM\Column(name="IsInvoiceCustomer", type="integer", nullable=true)
     */
    private $isinvoicecustomer;

    /**
     * @var integer $collectiveinvoiceid
     *
     * @ORM\Column(name="CollectiveInvoiceId", type="integer", nullable=true)
     */
    private $collectiveinvoiceid;

    /**
     * @var integer $collectivepaymentid
     *
     * @ORM\Column(name="CollectivePaymentId", type="integer", nullable=true)
     */
    private $collectivepaymentid;

    /**
     * @var string $attachmentkey
     *
     * @ORM\Column(name="AttachmentKey", type="string", length=50, nullable=true)
     */
    private $attachmentkey;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var integer $languageid
     *
     * @ORM\Column(name="LanguageId", type="integer", nullable=true)
     */
    private $languageid;

    /**
     * @var string $companyinfo
     *
     * @ORM\Column(name="CompanyInfo", type="text", nullable=true)
     */
    private $companyinfo;

    /**
     * @var string $laboratoryinfo
     *
     * @ORM\Column(name="LaboratoryInfo", type="text", nullable=true)
     */
    private $laboratoryinfo;

    /**
     * @var string $contactinfo
     *
     * @ORM\Column(name="ContactInfo", type="text", nullable=true)
     */
    private $contactinfo;

    /**
     * @var string $contactus
     *
     * @ORM\Column(name="ContactUs", type="text", nullable=true)
     */
    private $contactus;

    /**
     * @var string $letterextratext
     *
     * @ORM\Column(name="LetterExtraText", type="text", nullable=true)
     */
    private $letterextratext;

    /**
     * @var string $clinicianinfotext
     *
     * @ORM\Column(name="ClinicianInfoText", type="text", nullable=true)
     */
    private $clinicianinfotext;

    /**
     * @var string $text7
     *
     * @ORM\Column(name="Text7", type="text", nullable=true)
     */
    private $text7;

    /**
     * @var string $text8
     *
     * @ORM\Column(name="Text8", type="text", nullable=true)
     */
    private $text8;

    /**
     * @var string $text9
     *
     * @ORM\Column(name="Text9", type="text", nullable=true)
     */
    private $text9;

    /**
     * @var string $text10
     *
     * @ORM\Column(name="Text10", type="text", nullable=true)
     */
    private $text10;

    /**
     * @var string $text11
     *
     * @ORM\Column(name="Text11", type="text", nullable=true)
     */
    private $text11;

    /**
     * @var string $doctorsreporting
     *
     * @ORM\Column(name="DoctorsReporting", type="string", length=255, nullable=true)
     */
    private $doctorsreporting;

    /**
     * @var string $automaticreporting
     *
     * @ORM\Column(name="AutomaticReporting", type="string", length=255, nullable=true)
     */
    private $automaticreporting;

    /**
     * @var string $noreporting
     *
     * @ORM\Column(name="NoReporting", type="string", length=255, nullable=true)
     */
    private $noreporting;

    /**
     * @var string $invoicingandpaymentinfo
     *
     * @ORM\Column(name="InvoicingAndPaymentInfo", type="text", nullable=true)
     */
    private $invoicingandpaymentinfo;

    /**
     * @var integer $pricecategoryid
     *
     * @ORM\Column(name="PriceCategoryId", type="integer", nullable=true)
     */
    private $pricecategoryid;

    /**
     * @var string $reportdeliveryemail
     *
     * @ORM\Column(name="ReportDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $reportdeliveryemail;

    /**
     * @var string $invoicedeliveryemail
     *
     * @ORM\Column(name="InvoiceDeliveryEmail", type="string", length=255, nullable=true)
     */
    private $invoicedeliveryemail;

    /**
     * @var integer $invoicegoestoid
     *
     * @ORM\Column(name="InvoiceGoesToId", type="integer", nullable=true)
     */
    private $invoicegoestoid;

    /**
     * @var integer $reportgoestoid
     *
     * @ORM\Column(name="ReportGoesToId", type="integer", nullable=true)
     */
    private $reportgoestoid;

    /**
     * @var integer $reportdeliveryid
     *
     * @ORM\Column(name="ReportDeliveryId", type="integer", nullable=true)
     */
    private $reportdeliveryid;

    /**
     * @var string $ftpservername
     *
     * @ORM\Column(name="FtpServerName", type="string", length=255, nullable=true)
     */
    private $ftpservername;

    /**
     * @var string $ftpusername
     *
     * @ORM\Column(name="FtpUserName", type="string", length=255, nullable=true)
     */
    private $ftpusername;

    /**
     * @var string $ftppassword
     *
     * @ORM\Column(name="FtpPassword", type="string", length=255, nullable=true)
     */
    private $ftppassword;

    /**
     * @var string $rebrandingnick
     *
     * @ORM\Column(name="RebrandingNick", type="string", length=255, nullable=true)
     */
    private $rebrandingnick;

    /**
     * @var string $headerfilename
     *
     * @ORM\Column(name="HeaderFileName", type="string", length=255, nullable=true)
     */
    private $headerfilename;

    /**
     * @var string $footerfilename
     *
     * @ORM\Column(name="FooterFileName", type="string", length=255, nullable=true)
     */
    private $footerfilename;

    /**
     * @var string $titlelogofilename
     *
     * @ORM\Column(name="TitleLogoFileName", type="string", length=255, nullable=true)
     */
    private $titlelogofilename;

    /**
     * @var string $boxlogofilename
     *
     * @ORM\Column(name="BoxLogoFileName", type="string", length=255, nullable=true)
     */
    private $boxlogofilename;

    /**
     * @var boolean $isdoublechecked
     *
     * @ORM\Column(name="IsDoubleChecked", type="boolean", nullable=true)
     */
    private $isdoublechecked;

    /**
     * @var float $taxValue
     *
     * @ORM\Column(name="tax_value", type="decimal", nullable=true)
     */
    private $taxValue;

    /**
     * @var string $statusList
     *
     * @ORM\Column(name="status_list", type="text", nullable=true)
     */
    private $statusList;

    /**
     * @var \DateTime $statusDate
     *
     * @ORM\Column(name="status_date", type="date", nullable=true)
     */
    private $statusDate;

    /**
     * @var \DateTime $invoiceStatusDate
     *
     * @ORM\Column(name="invoice_status_date", type="date", nullable=true)
     */
    private $invoiceStatusDate;

    /**
     * @var string $orderInfoText
     *
     * @ORM\Column(name="order_info_text", type="text", nullable=true)
     */
    private $orderInfoText;

    /**
     * @var \DateTime $arrivalDate
     *
     * @ORM\Column(name="arrival_date", type="date", nullable=true)
     */
    private $arrivalDate;

    /**
     * @var float $revenueEstimation
     *
     * @ORM\Column(name="revenue_estimation", type="decimal", nullable=true)
     */
    private $revenueEstimation;

    /**
     * @var integer $complaintStatus
     *
     * @ORM\Column(name="complaint_status", type="integer", nullable=true)
     */
    private $complaintStatus;

    /**
     * @var float $invoiceAmount
     *
     * @ORM\Column(name="invoice_amount", type="decimal", nullable=true)
     */
    private $invoiceAmount;

    /**
     * @var string $invoiceFile
     *
     * @ORM\Column(name="invoice_file", type="string", length=255, nullable=true)
     */
    private $invoiceFile;

    /**
     * @var string $laboratoryDetails
     *
     * @ORM\Column(name="laboratory_details", type="text", nullable=true)
     */
    private $laboratoryDetails;

    /**
     * @var integer $idCurrency
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=true)
     */
    private $idCurrency;

    /**
     * @var float $exchangeRate
     *
     * @ORM\Column(name="exchange_rate", type="decimal", nullable=true)
     */
    private $exchangeRate;

    /**
     * @var integer $isRecheckGeocoding
     *
     * @ORM\Column(name="is_recheck_geocoding", type="integer", nullable=true)
     */
    private $isRecheckGeocoding;

    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var string $geocodingStatusCode
     *
     * @ORM\Column(name="geocoding_status_code", type="string", length=255, nullable=true)
     */
    private $geocodingStatusCode;

    /**
     * @var string $textAfterDescription
     *
     * @ORM\Column(name="text_after_description", type="text", nullable=true)
     */
    private $textAfterDescription;

    /**
     * @var string $textFooter
     *
     * @ORM\Column(name="text_footer", type="text", nullable=true)
     */
    private $textFooter;

    /**
     * @var integer $idPreferredPayment
     *
     * @ORM\Column(name="id_preferred_payment", type="integer", nullable=true)
     */
    private $idPreferredPayment;

    /**
     * @var integer $numberFeedback
     *
     * @ORM\Column(name="number_feedback", type="integer", nullable=true)
     */
    private $numberFeedback;

    /**
     * @var string $colorCode
     *
     * @ORM\Column(name="color_code", type="string", length=50, nullable=true)
     */
    private $colorCode;

    /**
     * @var integer $idDncReportType
     *
     * @ORM\Column(name="id_dnc_report_type", type="integer", nullable=true)
     */
    private $idDncReportType;

    /**
     * @var string $shipmentKey
     *
     * @ORM\Column(name="shipment_key", type="string", length=255, nullable=true)
     */
    private $shipmentKey;

    /**
     * @var \DateTime $shipmentKeyDatetime
     *
     * @ORM\Column(name="shipment_key_datetime", type="datetime", nullable=true)
     */
    private $shipmentKeyDatetime;

    /**
     * @var string $shipmentCode
     *
     * @ORM\Column(name="shipment_code", type="string", length=20, nullable=true)
     */
    private $shipmentCode;

    /**
     * @var integer $idMarginGoesTo
     *
     * @ORM\Column(name="id_margin_goes_to", type="integer", nullable=true)
     */
    private $idMarginGoesTo;

    /**
     * @var integer $idNutrimeGoesTo
     *
     * @ORM\Column(name="id_nutrime_goes_to", type="integer", nullable=true)
     */
    private $idNutrimeGoesTo;

    /**
     * @var integer $idCollectivePaymentPartner
     *
     * @ORM\Column(name="id_collective_payment_partner", type="integer", nullable=true)
     */
    private $idCollectivePaymentPartner;

    /**
     * @var integer $idCollectivePaymentDc
     *
     * @ORM\Column(name="id_collective_payment_dc", type="integer", nullable=true)
     */
    private $idCollectivePaymentDc;

    /**
     * @var string $marginOverrideDc
     *
     * @ORM\Column(name="margin_override_dc", type="string", length=50, nullable=true)
     */
    private $marginOverrideDc;

    /**
     * @var string $marginOverridePartner
     *
     * @ORM\Column(name="margin_override_partner", type="string", length=50, nullable=true)
     */
    private $marginOverridePartner;

    /**
     * @var boolean $isInvoiced
     *
     * @ORM\Column(name="is_invoiced", type="boolean", nullable=true)
     */
    private $isInvoiced;

    /**
     * @var float $paymentOverrideAcquisiteur1
     *
     * @ORM\Column(name="payment_override_acquisiteur1", type="decimal", nullable=true)
     */
    private $paymentOverrideAcquisiteur1;

    /**
     * @var float $paymentOverrideAcquisiteur2
     *
     * @ORM\Column(name="payment_override_acquisiteur2", type="decimal", nullable=true)
     */
    private $paymentOverrideAcquisiteur2;

    /**
     * @var float $paymentOverrideAcquisiteur3
     *
     * @ORM\Column(name="payment_override_acquisiteur3", type="decimal", nullable=true)
     */
    private $paymentOverrideAcquisiteur3;

    /**
     * @var boolean $isAddedInvoiced
     *
     * @ORM\Column(name="is_added_invoiced", type="boolean", nullable=true)
     */
    private $isAddedInvoiced;

    /**
     * @var float $extraPriceReportDeliveryOverride
     *
     * @ORM\Column(name="extra_price_report_delivery_override", type="decimal", nullable=true)
     */
    private $extraPriceReportDeliveryOverride;

    /**
     * @var integer $idOriginalCustomer
     *
     * @ORM\Column(name="id_original_customer", type="integer", nullable=true)
     */
    private $idOriginalCustomer;

    /**
     * @var string $productList
     *
     * @ORM\Column(name="product_list", type="text", nullable=true)
     */
    private $productList;

    /**
     * @var string $categoryList
     *
     * @ORM\Column(name="category_list", type="text", nullable=true)
     */
    private $categoryList;

    /**
     * @var string $dnaSampleOrderNumberVario
     *
     * @ORM\Column(name="dna_sample_order_number_vario", type="string", length=255, nullable=true)
     */
    private $dnaSampleOrderNumberVario;

    /**
     * @var boolean $isRecall
     *
     * @ORM\Column(name="is_recall", type="boolean", nullable=true)
     */
    private $isRecall;

    /**
     * @var boolean $invoiceAddressIsUsed
     *
     * @ORM\Column(name="invoice_address_is_used", type="boolean", nullable=true)
     */
    private $invoiceAddressIsUsed;

    /**
     * @var string $invoiceAddressStreet
     *
     * @ORM\Column(name="invoice_address_street", type="string", length=255, nullable=true)
     */
    private $invoiceAddressStreet;

    /**
     * @var string $invoiceAddressPostCode
     *
     * @ORM\Column(name="invoice_address_post_code", type="string", length=255, nullable=true)
     */
    private $invoiceAddressPostCode;

    /**
     * @var string $invoiceAddressCity
     *
     * @ORM\Column(name="invoice_address_city", type="string", length=255, nullable=true)
     */
    private $invoiceAddressCity;

    /**
     * @var integer $invoiceAddressIdCountry
     *
     * @ORM\Column(name="invoice_address_id_country", type="integer", nullable=true)
     */
    private $invoiceAddressIdCountry;

    /**
     * @var string $invoiceAddressTelephone
     *
     * @ORM\Column(name="invoice_address_telephone", type="string", length=255, nullable=true)
     */
    private $invoiceAddressTelephone;

    /**
     * @var string $invoiceAddressFax
     *
     * @ORM\Column(name="invoice_address_fax", type="string", length=255, nullable=true)
     */
    private $invoiceAddressFax;

    /**
     * @var string $invoiceAddressClientName
     *
     * @ORM\Column(name="invoice_address_client_name", type="string", length=255, nullable=true)
     */
    private $invoiceAddressClientName;

    /**
     * @var string $invoiceAddressCompanyName
     *
     * @ORM\Column(name="invoice_address_company_name", type="string", length=255, nullable=true)
     */
    private $invoiceAddressCompanyName;

    /**
     * @var float $extraPriceNutriMeDeliveryOverride
     *
     * @ORM\Column(name="extra_price_nutri_me_delivery_override", type="decimal", nullable=true)
     */
    private $extraPriceNutriMeDeliveryOverride;

    /**
     * @var integer $idAcquisiteur1
     *
     * @ORM\Column(name="id_acquisiteur1", type="integer", nullable=true)
     */
    private $idAcquisiteur1;

    /**
     * @var integer $acquisiteurCommissionRate1
     *
     * @ORM\Column(name="acquisiteur_commission_rate_1", type="integer", nullable=true)
     */
    private $acquisiteurCommissionRate1;

    /**
     * @var integer $idAcquisiteur2
     *
     * @ORM\Column(name="id_acquisiteur2", type="integer", nullable=true)
     */
    private $idAcquisiteur2;

    /**
     * @var integer $acquisiteurCommissionRate2
     *
     * @ORM\Column(name="acquisiteur_commission_rate_2", type="integer", nullable=true)
     */
    private $acquisiteurCommissionRate2;

    /**
     * @var integer $idAcquisiteur3
     *
     * @ORM\Column(name="id_acquisiteur3", type="integer", nullable=true)
     */
    private $idAcquisiteur3;

    /**
     * @var integer $acquisiteurCommissionRate3
     *
     * @ORM\Column(name="acquisiteur_commission_rate_3", type="integer", nullable=true)
     */
    private $acquisiteurCommissionRate3;

    /**
     * @var string $trackingCode
     *
     * @ORM\Column(name="tracking_code", type="string", length=255, nullable=true)
     */
    private $trackingCode;

    /**
     * @var boolean $isLocked
     *
     * @ORM\Column(name="is_locked", type="boolean", nullable=true)
     */
    private $isLocked;

    /**
     * @var \DateTime $lockDate
     *
     * @ORM\Column(name="lock_date", type="date", nullable=true)
     */
    private $lockDate;

    /**
     * @var boolean $isPrintedReportSentToPartner
     *
     * @ORM\Column(name="is_printed_report_sent_to_partner", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToPartner;

    /**
     * @var boolean $isPrintedReportSentToDc
     *
     * @ORM\Column(name="is_printed_report_sent_to_dc", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToDc;

    /**
     * @var boolean $isPrintedReportSentToCustomer
     *
     * @ORM\Column(name="is_printed_report_sent_to_customer", type="boolean", nullable=true)
     */
    private $isPrintedReportSentToCustomer;

    /**
     * @var boolean $isDigitalReportGoesToCustomer
     *
     * @ORM\Column(name="is_digital_report_goes_to_customer", type="boolean", nullable=true)
     */
    private $isDigitalReportGoesToCustomer;

    /**
     * @var integer $idRecallGoesTo
     *
     * @ORM\Column(name="id_recall_goes_to", type="integer", nullable=true)
     */
    private $idRecallGoesTo;

    /**
     * @var boolean $isReportDeliveryDownloadAccess
     *
     * @ORM\Column(name="is_report_delivery_download_access", type="boolean", nullable=true)
     */
    private $isReportDeliveryDownloadAccess;

    /**
     * @var boolean $isReportDeliveryFtpServer
     *
     * @ORM\Column(name="is_report_delivery_ftp_server", type="boolean", nullable=true)
     */
    private $isReportDeliveryFtpServer;

    /**
     * @var boolean $isReportDeliveryPrintedBooklet
     *
     * @ORM\Column(name="is_report_delivery_printed_booklet", type="boolean", nullable=true)
     */
    private $isReportDeliveryPrintedBooklet;

    /**
     * @var boolean $isReportDeliveryDontChargeBooklet
     *
     * @ORM\Column(name="is_report_delivery_dont_charge_booklet", type="boolean", nullable=true)
     */
    private $isReportDeliveryDontChargeBooklet;

    /**
     * @var string $previousOrderNumber
     *
     * @ORM\Column(name="previous_order_number", type="string", length=255, nullable=true)
     */
    private $previousOrderNumber;

    /**
     * @var boolean $canCreateLoginAndSendStatusMessageToUser
     *
     * @ORM\Column(name="can_create_login_and_send_status_message_to_user", type="boolean", nullable=true)
     */
    private $canCreateLoginAndSendStatusMessageToUser;

    /**
     * @var boolean $canCustomersDownloadReports
     *
     * @ORM\Column(name="can_customers_download_reports", type="boolean", nullable=true)
     */
    private $canCustomersDownloadReports;

    /**
     * @var boolean $isDisplayInCustomerDashboard
     *
     * @ORM\Column(name="is_display_in_customer_dashboard", type="boolean", nullable=true)
     */
    private $isDisplayInCustomerDashboard;

    /**
     * @var string $invoiceAddressUid
     *
     * @ORM\Column(name="invoice_address_uid", type="string", length=255, nullable=true)
     */
    private $invoiceAddressUid;

    /**
     * @var boolean $isDelayDownloadReport
     *
     * @ORM\Column(name="is_delay_download_report", type="boolean", nullable=true)
     */
    private $isDelayDownloadReport;

    /**
     * @var integer $numberDateDelayDownloadReport
     *
     * @ORM\Column(name="number_date_delay_download_report", type="integer", nullable=true)
     */
    private $numberDateDelayDownloadReport;

    /**
     * @var \DateTime $dateSetDelayDownloadReport
     *
     * @ORM\Column(name="date_set_delay_download_report", type="datetime", nullable=true)
     */
    private $dateSetDelayDownloadReport;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOrderBackup
     *
     * @param integer $idOrderBackup
     * @return Customer
     */
    public function setIdOrderBackup($idOrderBackup)
    {
        $this->idOrderBackup = $idOrderBackup;
    
        return $this;
    }

    /**
     * Get idOrderBackup
     *
     * @return integer 
     */
    public function getIdOrderBackup()
    {
        return $this->idOrderBackup;
    }

    /**
     * Set idCustomerInfo
     *
     * @param integer $idCustomerInfo
     * @return Customer
     */
    public function setIdCustomerInfo($idCustomerInfo)
    {
        $this->idCustomerInfo = $idCustomerInfo;
    
        return $this;
    }

    /**
     * Get idCustomerInfo
     *
     * @return integer 
     */
    public function getIdCustomerInfo()
    {
        return $this->idCustomerInfo;
    }

    /**
     * Set ordernumber
     *
     * @param string $ordernumber
     * @return Customer
     */
    public function setOrdernumber($ordernumber)
    {
        $this->ordernumber = $ordernumber;
    
        return $this;
    }

    /**
     * Get ordernumber
     *
     * @return string 
     */
    public function getOrdernumber()
    {
        return $this->ordernumber;
    }

    /**
     * Set distributionchannelid
     *
     * @param integer $distributionchannelid
     * @return Customer
     */
    public function setDistributionchannelid($distributionchannelid)
    {
        $this->distributionchannelid = $distributionchannelid;
    
        return $this;
    }

    /**
     * Get distributionchannelid
     *
     * @return integer 
     */
    public function getDistributionchannelid()
    {
        return $this->distributionchannelid;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Customer
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set dateordered
     *
     * @param \DateTime $dateordered
     * @return Customer
     */
    public function setDateordered($dateordered)
    {
        $this->dateordered = $dateordered;
    
        return $this;
    }

    /**
     * Get dateordered
     *
     * @return \DateTime 
     */
    public function getDateordered()
    {
        return $this->dateordered;
    }

    /**
     * Set orderage
     *
     * @param integer $orderage
     * @return Customer
     */
    public function setOrderage($orderage)
    {
        $this->orderage = $orderage;
    
        return $this;
    }

    /**
     * Get orderage
     *
     * @return integer 
     */
    public function getOrderage()
    {
        return $this->orderage;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Customer
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set genderid
     *
     * @param integer $genderid
     * @return Customer
     */
    public function setGenderid($genderid)
    {
        $this->genderid = $genderid;
    
        return $this;
    }

    /**
     * Get genderid
     *
     * @return integer 
     */
    public function getGenderid()
    {
        return $this->genderid;
    }

    /**
     * Set dateofbirth
     *
     * @param \DateTime $dateofbirth
     * @return Customer
     */
    public function setDateofbirth($dateofbirth)
    {
        $this->dateofbirth = $dateofbirth;
    
        return $this;
    }

    /**
     * Get dateofbirth
     *
     * @return \DateTime 
     */
    public function getDateofbirth()
    {
        return $this->dateofbirth;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Customer
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Customer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Customer
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Customer
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street2
     *
     * @param string $street2
     * @return Customer
     */
    public function setStreet2($street2)
    {
        $this->street2 = $street2;
    
        return $this;
    }

    /**
     * Get street2
     *
     * @return string 
     */
    public function getStreet2()
    {
        return $this->street2;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Customer
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Customer
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;
    
        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Customer
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Customer
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Customer
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set invoiceexist
     *
     * @param integer $invoiceexist
     * @return Customer
     */
    public function setInvoiceexist($invoiceexist)
    {
        $this->invoiceexist = $invoiceexist;
    
        return $this;
    }

    /**
     * Get invoiceexist
     *
     * @return integer 
     */
    public function getInvoiceexist()
    {
        return $this->invoiceexist;
    }

    /**
     * Set invoicenumber
     *
     * @param string $invoicenumber
     * @return Customer
     */
    public function setInvoicenumber($invoicenumber)
    {
        $this->invoicenumber = $invoicenumber;
    
        return $this;
    }

    /**
     * Get invoicenumber
     *
     * @return string 
     */
    public function getInvoicenumber()
    {
        return $this->invoicenumber;
    }

    /**
     * Set invoicedate
     *
     * @param \DateTime $invoicedate
     * @return Customer
     */
    public function setInvoicedate($invoicedate)
    {
        $this->invoicedate = $invoicedate;
    
        return $this;
    }

    /**
     * Get invoicedate
     *
     * @return \DateTime 
     */
    public function getInvoicedate()
    {
        return $this->invoicedate;
    }

    /**
     * Set invoicefromcompanyid
     *
     * @param integer $invoicefromcompanyid
     * @return Customer
     */
    public function setInvoicefromcompanyid($invoicefromcompanyid)
    {
        $this->invoicefromcompanyid = $invoicefromcompanyid;
    
        return $this;
    }

    /**
     * Get invoicefromcompanyid
     *
     * @return integer 
     */
    public function getInvoicefromcompanyid()
    {
        return $this->invoicefromcompanyid;
    }

    /**
     * Set iswithtax
     *
     * @param boolean $iswithtax
     * @return Customer
     */
    public function setIswithtax($iswithtax)
    {
        $this->iswithtax = $iswithtax;
    
        return $this;
    }

    /**
     * Get iswithtax
     *
     * @return boolean 
     */
    public function getIswithtax()
    {
        return $this->iswithtax;
    }

    /**
     * Set invoicestatus
     *
     * @param integer $invoicestatus
     * @return Customer
     */
    public function setInvoicestatus($invoicestatus)
    {
        $this->invoicestatus = $invoicestatus;
    
        return $this;
    }

    /**
     * Get invoicestatus
     *
     * @return integer 
     */
    public function getInvoicestatus()
    {
        return $this->invoicestatus;
    }

    /**
     * Set postage
     *
     * @param float $postage
     * @return Customer
     */
    public function setPostage($postage)
    {
        $this->postage = $postage;
    
        return $this;
    }

    /**
     * Get postage
     *
     * @return float 
     */
    public function getPostage()
    {
        return $this->postage;
    }

    /**
     * Set priceoverride
     *
     * @param float $priceoverride
     * @return Customer
     */
    public function setPriceoverride($priceoverride)
    {
        $this->priceoverride = $priceoverride;
    
        return $this;
    }

    /**
     * Get priceoverride
     *
     * @return float 
     */
    public function getPriceoverride()
    {
        return $this->priceoverride;
    }

    /**
     * Set invoiceinfo
     *
     * @param string $invoiceinfo
     * @return Customer
     */
    public function setInvoiceinfo($invoiceinfo)
    {
        $this->invoiceinfo = $invoiceinfo;
    
        return $this;
    }

    /**
     * Get invoiceinfo
     *
     * @return string 
     */
    public function getInvoiceinfo()
    {
        return $this->invoiceinfo;
    }

    /**
     * Set isinvoicecustomer
     *
     * @param integer $isinvoicecustomer
     * @return Customer
     */
    public function setIsinvoicecustomer($isinvoicecustomer)
    {
        $this->isinvoicecustomer = $isinvoicecustomer;
    
        return $this;
    }

    /**
     * Get isinvoicecustomer
     *
     * @return integer 
     */
    public function getIsinvoicecustomer()
    {
        return $this->isinvoicecustomer;
    }

    /**
     * Set collectiveinvoiceid
     *
     * @param integer $collectiveinvoiceid
     * @return Customer
     */
    public function setCollectiveinvoiceid($collectiveinvoiceid)
    {
        $this->collectiveinvoiceid = $collectiveinvoiceid;
    
        return $this;
    }

    /**
     * Get collectiveinvoiceid
     *
     * @return integer 
     */
    public function getCollectiveinvoiceid()
    {
        return $this->collectiveinvoiceid;
    }

    /**
     * Set collectivepaymentid
     *
     * @param integer $collectivepaymentid
     * @return Customer
     */
    public function setCollectivepaymentid($collectivepaymentid)
    {
        $this->collectivepaymentid = $collectivepaymentid;
    
        return $this;
    }

    /**
     * Get collectivepaymentid
     *
     * @return integer 
     */
    public function getCollectivepaymentid()
    {
        return $this->collectivepaymentid;
    }

    /**
     * Set attachmentkey
     *
     * @param string $attachmentkey
     * @return Customer
     */
    public function setAttachmentkey($attachmentkey)
    {
        $this->attachmentkey = $attachmentkey;
    
        return $this;
    }

    /**
     * Get attachmentkey
     *
     * @return string 
     */
    public function getAttachmentkey()
    {
        return $this->attachmentkey;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Customer
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return Customer
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;
    
        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set companyinfo
     *
     * @param string $companyinfo
     * @return Customer
     */
    public function setCompanyinfo($companyinfo)
    {
        $this->companyinfo = $companyinfo;
    
        return $this;
    }

    /**
     * Get companyinfo
     *
     * @return string 
     */
    public function getCompanyinfo()
    {
        return $this->companyinfo;
    }

    /**
     * Set laboratoryinfo
     *
     * @param string $laboratoryinfo
     * @return Customer
     */
    public function setLaboratoryinfo($laboratoryinfo)
    {
        $this->laboratoryinfo = $laboratoryinfo;
    
        return $this;
    }

    /**
     * Get laboratoryinfo
     *
     * @return string 
     */
    public function getLaboratoryinfo()
    {
        return $this->laboratoryinfo;
    }

    /**
     * Set contactinfo
     *
     * @param string $contactinfo
     * @return Customer
     */
    public function setContactinfo($contactinfo)
    {
        $this->contactinfo = $contactinfo;
    
        return $this;
    }

    /**
     * Get contactinfo
     *
     * @return string 
     */
    public function getContactinfo()
    {
        return $this->contactinfo;
    }

    /**
     * Set contactus
     *
     * @param string $contactus
     * @return Customer
     */
    public function setContactus($contactus)
    {
        $this->contactus = $contactus;
    
        return $this;
    }

    /**
     * Get contactus
     *
     * @return string 
     */
    public function getContactus()
    {
        return $this->contactus;
    }

    /**
     * Set letterextratext
     *
     * @param string $letterextratext
     * @return Customer
     */
    public function setLetterextratext($letterextratext)
    {
        $this->letterextratext = $letterextratext;
    
        return $this;
    }

    /**
     * Get letterextratext
     *
     * @return string 
     */
    public function getLetterextratext()
    {
        return $this->letterextratext;
    }

    /**
     * Set clinicianinfotext
     *
     * @param string $clinicianinfotext
     * @return Customer
     */
    public function setClinicianinfotext($clinicianinfotext)
    {
        $this->clinicianinfotext = $clinicianinfotext;
    
        return $this;
    }

    /**
     * Get clinicianinfotext
     *
     * @return string 
     */
    public function getClinicianinfotext()
    {
        return $this->clinicianinfotext;
    }

    /**
     * Set text7
     *
     * @param string $text7
     * @return Customer
     */
    public function setText7($text7)
    {
        $this->text7 = $text7;
    
        return $this;
    }

    /**
     * Get text7
     *
     * @return string 
     */
    public function getText7()
    {
        return $this->text7;
    }

    /**
     * Set text8
     *
     * @param string $text8
     * @return Customer
     */
    public function setText8($text8)
    {
        $this->text8 = $text8;
    
        return $this;
    }

    /**
     * Get text8
     *
     * @return string 
     */
    public function getText8()
    {
        return $this->text8;
    }

    /**
     * Set text9
     *
     * @param string $text9
     * @return Customer
     */
    public function setText9($text9)
    {
        $this->text9 = $text9;
    
        return $this;
    }

    /**
     * Get text9
     *
     * @return string 
     */
    public function getText9()
    {
        return $this->text9;
    }

    /**
     * Set text10
     *
     * @param string $text10
     * @return Customer
     */
    public function setText10($text10)
    {
        $this->text10 = $text10;
    
        return $this;
    }

    /**
     * Get text10
     *
     * @return string 
     */
    public function getText10()
    {
        return $this->text10;
    }

    /**
     * Set text11
     *
     * @param string $text11
     * @return Customer
     */
    public function setText11($text11)
    {
        $this->text11 = $text11;
    
        return $this;
    }

    /**
     * Get text11
     *
     * @return string 
     */
    public function getText11()
    {
        return $this->text11;
    }

    /**
     * Set doctorsreporting
     *
     * @param string $doctorsreporting
     * @return Customer
     */
    public function setDoctorsreporting($doctorsreporting)
    {
        $this->doctorsreporting = $doctorsreporting;
    
        return $this;
    }

    /**
     * Get doctorsreporting
     *
     * @return string 
     */
    public function getDoctorsreporting()
    {
        return $this->doctorsreporting;
    }

    /**
     * Set automaticreporting
     *
     * @param string $automaticreporting
     * @return Customer
     */
    public function setAutomaticreporting($automaticreporting)
    {
        $this->automaticreporting = $automaticreporting;
    
        return $this;
    }

    /**
     * Get automaticreporting
     *
     * @return string 
     */
    public function getAutomaticreporting()
    {
        return $this->automaticreporting;
    }

    /**
     * Set noreporting
     *
     * @param string $noreporting
     * @return Customer
     */
    public function setNoreporting($noreporting)
    {
        $this->noreporting = $noreporting;
    
        return $this;
    }

    /**
     * Get noreporting
     *
     * @return string 
     */
    public function getNoreporting()
    {
        return $this->noreporting;
    }

    /**
     * Set invoicingandpaymentinfo
     *
     * @param string $invoicingandpaymentinfo
     * @return Customer
     */
    public function setInvoicingandpaymentinfo($invoicingandpaymentinfo)
    {
        $this->invoicingandpaymentinfo = $invoicingandpaymentinfo;
    
        return $this;
    }

    /**
     * Get invoicingandpaymentinfo
     *
     * @return string 
     */
    public function getInvoicingandpaymentinfo()
    {
        return $this->invoicingandpaymentinfo;
    }

    /**
     * Set pricecategoryid
     *
     * @param integer $pricecategoryid
     * @return Customer
     */
    public function setPricecategoryid($pricecategoryid)
    {
        $this->pricecategoryid = $pricecategoryid;
    
        return $this;
    }

    /**
     * Get pricecategoryid
     *
     * @return integer 
     */
    public function getPricecategoryid()
    {
        return $this->pricecategoryid;
    }

    /**
     * Set reportdeliveryemail
     *
     * @param string $reportdeliveryemail
     * @return Customer
     */
    public function setReportdeliveryemail($reportdeliveryemail)
    {
        $this->reportdeliveryemail = $reportdeliveryemail;
    
        return $this;
    }

    /**
     * Get reportdeliveryemail
     *
     * @return string 
     */
    public function getReportdeliveryemail()
    {
        return $this->reportdeliveryemail;
    }

    /**
     * Set invoicedeliveryemail
     *
     * @param string $invoicedeliveryemail
     * @return Customer
     */
    public function setInvoicedeliveryemail($invoicedeliveryemail)
    {
        $this->invoicedeliveryemail = $invoicedeliveryemail;
    
        return $this;
    }

    /**
     * Get invoicedeliveryemail
     *
     * @return string 
     */
    public function getInvoicedeliveryemail()
    {
        return $this->invoicedeliveryemail;
    }

    /**
     * Set invoicegoestoid
     *
     * @param integer $invoicegoestoid
     * @return Customer
     */
    public function setInvoicegoestoid($invoicegoestoid)
    {
        $this->invoicegoestoid = $invoicegoestoid;
    
        return $this;
    }

    /**
     * Get invoicegoestoid
     *
     * @return integer 
     */
    public function getInvoicegoestoid()
    {
        return $this->invoicegoestoid;
    }

    /**
     * Set reportgoestoid
     *
     * @param integer $reportgoestoid
     * @return Customer
     */
    public function setReportgoestoid($reportgoestoid)
    {
        $this->reportgoestoid = $reportgoestoid;
    
        return $this;
    }

    /**
     * Get reportgoestoid
     *
     * @return integer 
     */
    public function getReportgoestoid()
    {
        return $this->reportgoestoid;
    }

    /**
     * Set reportdeliveryid
     *
     * @param integer $reportdeliveryid
     * @return Customer
     */
    public function setReportdeliveryid($reportdeliveryid)
    {
        $this->reportdeliveryid = $reportdeliveryid;
    
        return $this;
    }

    /**
     * Get reportdeliveryid
     *
     * @return integer 
     */
    public function getReportdeliveryid()
    {
        return $this->reportdeliveryid;
    }

    /**
     * Set ftpservername
     *
     * @param string $ftpservername
     * @return Customer
     */
    public function setFtpservername($ftpservername)
    {
        $this->ftpservername = $ftpservername;
    
        return $this;
    }

    /**
     * Get ftpservername
     *
     * @return string 
     */
    public function getFtpservername()
    {
        return $this->ftpservername;
    }

    /**
     * Set ftpusername
     *
     * @param string $ftpusername
     * @return Customer
     */
    public function setFtpusername($ftpusername)
    {
        $this->ftpusername = $ftpusername;
    
        return $this;
    }

    /**
     * Get ftpusername
     *
     * @return string 
     */
    public function getFtpusername()
    {
        return $this->ftpusername;
    }

    /**
     * Set ftppassword
     *
     * @param string $ftppassword
     * @return Customer
     */
    public function setFtppassword($ftppassword)
    {
        $this->ftppassword = $ftppassword;
    
        return $this;
    }

    /**
     * Get ftppassword
     *
     * @return string 
     */
    public function getFtppassword()
    {
        return $this->ftppassword;
    }

    /**
     * Set rebrandingnick
     *
     * @param string $rebrandingnick
     * @return Customer
     */
    public function setRebrandingnick($rebrandingnick)
    {
        $this->rebrandingnick = $rebrandingnick;
    
        return $this;
    }

    /**
     * Get rebrandingnick
     *
     * @return string 
     */
    public function getRebrandingnick()
    {
        return $this->rebrandingnick;
    }

    /**
     * Set headerfilename
     *
     * @param string $headerfilename
     * @return Customer
     */
    public function setHeaderfilename($headerfilename)
    {
        $this->headerfilename = $headerfilename;
    
        return $this;
    }

    /**
     * Get headerfilename
     *
     * @return string 
     */
    public function getHeaderfilename()
    {
        return $this->headerfilename;
    }

    /**
     * Set footerfilename
     *
     * @param string $footerfilename
     * @return Customer
     */
    public function setFooterfilename($footerfilename)
    {
        $this->footerfilename = $footerfilename;
    
        return $this;
    }

    /**
     * Get footerfilename
     *
     * @return string 
     */
    public function getFooterfilename()
    {
        return $this->footerfilename;
    }

    /**
     * Set titlelogofilename
     *
     * @param string $titlelogofilename
     * @return Customer
     */
    public function setTitlelogofilename($titlelogofilename)
    {
        $this->titlelogofilename = $titlelogofilename;
    
        return $this;
    }

    /**
     * Get titlelogofilename
     *
     * @return string 
     */
    public function getTitlelogofilename()
    {
        return $this->titlelogofilename;
    }

    /**
     * Set boxlogofilename
     *
     * @param string $boxlogofilename
     * @return Customer
     */
    public function setBoxlogofilename($boxlogofilename)
    {
        $this->boxlogofilename = $boxlogofilename;
    
        return $this;
    }

    /**
     * Get boxlogofilename
     *
     * @return string 
     */
    public function getBoxlogofilename()
    {
        return $this->boxlogofilename;
    }

    /**
     * Set isdoublechecked
     *
     * @param boolean $isdoublechecked
     * @return Customer
     */
    public function setIsdoublechecked($isdoublechecked)
    {
        $this->isdoublechecked = $isdoublechecked;
    
        return $this;
    }

    /**
     * Get isdoublechecked
     *
     * @return boolean 
     */
    public function getIsdoublechecked()
    {
        return $this->isdoublechecked;
    }

    /**
     * Set taxValue
     *
     * @param float $taxValue
     * @return Customer
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    
        return $this;
    }

    /**
     * Get taxValue
     *
     * @return float 
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }

    /**
     * Set statusList
     *
     * @param string $statusList
     * @return Customer
     */
    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    
        return $this;
    }

    /**
     * Get statusList
     *
     * @return string 
     */
    public function getStatusList()
    {
        return $this->statusList;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     * @return Customer
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    
        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime 
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set invoiceStatusDate
     *
     * @param \DateTime $invoiceStatusDate
     * @return Customer
     */
    public function setInvoiceStatusDate($invoiceStatusDate)
    {
        $this->invoiceStatusDate = $invoiceStatusDate;
    
        return $this;
    }

    /**
     * Get invoiceStatusDate
     *
     * @return \DateTime 
     */
    public function getInvoiceStatusDate()
    {
        return $this->invoiceStatusDate;
    }

    /**
     * Set orderInfoText
     *
     * @param string $orderInfoText
     * @return Customer
     */
    public function setOrderInfoText($orderInfoText)
    {
        $this->orderInfoText = $orderInfoText;
    
        return $this;
    }

    /**
     * Get orderInfoText
     *
     * @return string 
     */
    public function getOrderInfoText()
    {
        return $this->orderInfoText;
    }

    /**
     * Set arrivalDate
     *
     * @param \DateTime $arrivalDate
     * @return Customer
     */
    public function setArrivalDate($arrivalDate)
    {
        $this->arrivalDate = $arrivalDate;
    
        return $this;
    }

    /**
     * Get arrivalDate
     *
     * @return \DateTime 
     */
    public function getArrivalDate()
    {
        return $this->arrivalDate;
    }

    /**
     * Set revenueEstimation
     *
     * @param float $revenueEstimation
     * @return Customer
     */
    public function setRevenueEstimation($revenueEstimation)
    {
        $this->revenueEstimation = $revenueEstimation;
    
        return $this;
    }

    /**
     * Get revenueEstimation
     *
     * @return float 
     */
    public function getRevenueEstimation()
    {
        return $this->revenueEstimation;
    }

    /**
     * Set complaintStatus
     *
     * @param integer $complaintStatus
     * @return Customer
     */
    public function setComplaintStatus($complaintStatus)
    {
        $this->complaintStatus = $complaintStatus;
    
        return $this;
    }

    /**
     * Get complaintStatus
     *
     * @return integer 
     */
    public function getComplaintStatus()
    {
        return $this->complaintStatus;
    }

    /**
     * Set invoiceAmount
     *
     * @param float $invoiceAmount
     * @return Customer
     */
    public function setInvoiceAmount($invoiceAmount)
    {
        $this->invoiceAmount = $invoiceAmount;
    
        return $this;
    }

    /**
     * Get invoiceAmount
     *
     * @return float 
     */
    public function getInvoiceAmount()
    {
        return $this->invoiceAmount;
    }

    /**
     * Set invoiceFile
     *
     * @param string $invoiceFile
     * @return Customer
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;
    
        return $this;
    }

    /**
     * Get invoiceFile
     *
     * @return string 
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set laboratoryDetails
     *
     * @param string $laboratoryDetails
     * @return Customer
     */
    public function setLaboratoryDetails($laboratoryDetails)
    {
        $this->laboratoryDetails = $laboratoryDetails;
    
        return $this;
    }

    /**
     * Get laboratoryDetails
     *
     * @return string 
     */
    public function getLaboratoryDetails()
    {
        return $this->laboratoryDetails;
    }

    /**
     * Set idCurrency
     *
     * @param integer $idCurrency
     * @return Customer
     */
    public function setIdCurrency($idCurrency)
    {
        $this->idCurrency = $idCurrency;
    
        return $this;
    }

    /**
     * Get idCurrency
     *
     * @return integer 
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     * @return Customer
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    
        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set isRecheckGeocoding
     *
     * @param integer $isRecheckGeocoding
     * @return Customer
     */
    public function setIsRecheckGeocoding($isRecheckGeocoding)
    {
        $this->isRecheckGeocoding = $isRecheckGeocoding;
    
        return $this;
    }

    /**
     * Get isRecheckGeocoding
     *
     * @return integer 
     */
    public function getIsRecheckGeocoding()
    {
        return $this->isRecheckGeocoding;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Customer
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Customer
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set geocodingStatusCode
     *
     * @param string $geocodingStatusCode
     * @return Customer
     */
    public function setGeocodingStatusCode($geocodingStatusCode)
    {
        $this->geocodingStatusCode = $geocodingStatusCode;
    
        return $this;
    }

    /**
     * Get geocodingStatusCode
     *
     * @return string 
     */
    public function getGeocodingStatusCode()
    {
        return $this->geocodingStatusCode;
    }

    /**
     * Set textAfterDescription
     *
     * @param string $textAfterDescription
     * @return Customer
     */
    public function setTextAfterDescription($textAfterDescription)
    {
        $this->textAfterDescription = $textAfterDescription;
    
        return $this;
    }

    /**
     * Get textAfterDescription
     *
     * @return string 
     */
    public function getTextAfterDescription()
    {
        return $this->textAfterDescription;
    }

    /**
     * Set textFooter
     *
     * @param string $textFooter
     * @return Customer
     */
    public function setTextFooter($textFooter)
    {
        $this->textFooter = $textFooter;
    
        return $this;
    }

    /**
     * Get textFooter
     *
     * @return string 
     */
    public function getTextFooter()
    {
        return $this->textFooter;
    }

    /**
     * Set idPreferredPayment
     *
     * @param integer $idPreferredPayment
     * @return Customer
     */
    public function setIdPreferredPayment($idPreferredPayment)
    {
        $this->idPreferredPayment = $idPreferredPayment;
    
        return $this;
    }

    /**
     * Get idPreferredPayment
     *
     * @return integer 
     */
    public function getIdPreferredPayment()
    {
        return $this->idPreferredPayment;
    }

    /**
     * Set numberFeedback
     *
     * @param integer $numberFeedback
     * @return Customer
     */
    public function setNumberFeedback($numberFeedback)
    {
        $this->numberFeedback = $numberFeedback;
    
        return $this;
    }

    /**
     * Get numberFeedback
     *
     * @return integer 
     */
    public function getNumberFeedback()
    {
        return $this->numberFeedback;
    }

    /**
     * Set colorCode
     *
     * @param string $colorCode
     * @return Customer
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
    
        return $this;
    }

    /**
     * Get colorCode
     *
     * @return string 
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * Set idDncReportType
     *
     * @param integer $idDncReportType
     * @return Customer
     */
    public function setIdDncReportType($idDncReportType)
    {
        $this->idDncReportType = $idDncReportType;
    
        return $this;
    }

    /**
     * Get idDncReportType
     *
     * @return integer 
     */
    public function getIdDncReportType()
    {
        return $this->idDncReportType;
    }

    /**
     * Set shipmentKey
     *
     * @param string $shipmentKey
     * @return Customer
     */
    public function setShipmentKey($shipmentKey)
    {
        $this->shipmentKey = $shipmentKey;
    
        return $this;
    }

    /**
     * Get shipmentKey
     *
     * @return string 
     */
    public function getShipmentKey()
    {
        return $this->shipmentKey;
    }

    /**
     * Set shipmentKeyDatetime
     *
     * @param \DateTime $shipmentKeyDatetime
     * @return Customer
     */
    public function setShipmentKeyDatetime($shipmentKeyDatetime)
    {
        $this->shipmentKeyDatetime = $shipmentKeyDatetime;
    
        return $this;
    }

    /**
     * Get shipmentKeyDatetime
     *
     * @return \DateTime 
     */
    public function getShipmentKeyDatetime()
    {
        return $this->shipmentKeyDatetime;
    }

    /**
     * Set shipmentCode
     *
     * @param string $shipmentCode
     * @return Customer
     */
    public function setShipmentCode($shipmentCode)
    {
        $this->shipmentCode = $shipmentCode;
    
        return $this;
    }

    /**
     * Get shipmentCode
     *
     * @return string 
     */
    public function getShipmentCode()
    {
        return $this->shipmentCode;
    }

    /**
     * Set idMarginGoesTo
     *
     * @param integer $idMarginGoesTo
     * @return Customer
     */
    public function setIdMarginGoesTo($idMarginGoesTo)
    {
        $this->idMarginGoesTo = $idMarginGoesTo;
    
        return $this;
    }

    /**
     * Get idMarginGoesTo
     *
     * @return integer 
     */
    public function getIdMarginGoesTo()
    {
        return $this->idMarginGoesTo;
    }

    /**
     * Set idNutrimeGoesTo
     *
     * @param integer $idNutrimeGoesTo
     * @return Customer
     */
    public function setIdNutrimeGoesTo($idNutrimeGoesTo)
    {
        $this->idNutrimeGoesTo = $idNutrimeGoesTo;
    
        return $this;
    }

    /**
     * Get idNutrimeGoesTo
     *
     * @return integer 
     */
    public function getIdNutrimeGoesTo()
    {
        return $this->idNutrimeGoesTo;
    }

    /**
     * Set idCollectivePaymentPartner
     *
     * @param integer $idCollectivePaymentPartner
     * @return Customer
     */
    public function setIdCollectivePaymentPartner($idCollectivePaymentPartner)
    {
        $this->idCollectivePaymentPartner = $idCollectivePaymentPartner;
    
        return $this;
    }

    /**
     * Get idCollectivePaymentPartner
     *
     * @return integer 
     */
    public function getIdCollectivePaymentPartner()
    {
        return $this->idCollectivePaymentPartner;
    }

    /**
     * Set idCollectivePaymentDc
     *
     * @param integer $idCollectivePaymentDc
     * @return Customer
     */
    public function setIdCollectivePaymentDc($idCollectivePaymentDc)
    {
        $this->idCollectivePaymentDc = $idCollectivePaymentDc;
    
        return $this;
    }

    /**
     * Get idCollectivePaymentDc
     *
     * @return integer 
     */
    public function getIdCollectivePaymentDc()
    {
        return $this->idCollectivePaymentDc;
    }

    /**
     * Set marginOverrideDc
     *
     * @param string $marginOverrideDc
     * @return Customer
     */
    public function setMarginOverrideDc($marginOverrideDc)
    {
        $this->marginOverrideDc = $marginOverrideDc;
    
        return $this;
    }

    /**
     * Get marginOverrideDc
     *
     * @return string 
     */
    public function getMarginOverrideDc()
    {
        return $this->marginOverrideDc;
    }

    /**
     * Set marginOverridePartner
     *
     * @param string $marginOverridePartner
     * @return Customer
     */
    public function setMarginOverridePartner($marginOverridePartner)
    {
        $this->marginOverridePartner = $marginOverridePartner;
    
        return $this;
    }

    /**
     * Get marginOverridePartner
     *
     * @return string 
     */
    public function getMarginOverridePartner()
    {
        return $this->marginOverridePartner;
    }

    /**
     * Set isInvoiced
     *
     * @param boolean $isInvoiced
     * @return Customer
     */
    public function setIsInvoiced($isInvoiced)
    {
        $this->isInvoiced = $isInvoiced;
    
        return $this;
    }

    /**
     * Get isInvoiced
     *
     * @return boolean 
     */
    public function getIsInvoiced()
    {
        return $this->isInvoiced;
    }

    /**
     * Set paymentOverrideAcquisiteur1
     *
     * @param float $paymentOverrideAcquisiteur1
     * @return Customer
     */
    public function setPaymentOverrideAcquisiteur1($paymentOverrideAcquisiteur1)
    {
        $this->paymentOverrideAcquisiteur1 = $paymentOverrideAcquisiteur1;
    
        return $this;
    }

    /**
     * Get paymentOverrideAcquisiteur1
     *
     * @return float 
     */
    public function getPaymentOverrideAcquisiteur1()
    {
        return $this->paymentOverrideAcquisiteur1;
    }

    /**
     * Set paymentOverrideAcquisiteur2
     *
     * @param float $paymentOverrideAcquisiteur2
     * @return Customer
     */
    public function setPaymentOverrideAcquisiteur2($paymentOverrideAcquisiteur2)
    {
        $this->paymentOverrideAcquisiteur2 = $paymentOverrideAcquisiteur2;
    
        return $this;
    }

    /**
     * Get paymentOverrideAcquisiteur2
     *
     * @return float 
     */
    public function getPaymentOverrideAcquisiteur2()
    {
        return $this->paymentOverrideAcquisiteur2;
    }

    /**
     * Set paymentOverrideAcquisiteur3
     *
     * @param float $paymentOverrideAcquisiteur3
     * @return Customer
     */
    public function setPaymentOverrideAcquisiteur3($paymentOverrideAcquisiteur3)
    {
        $this->paymentOverrideAcquisiteur3 = $paymentOverrideAcquisiteur3;
    
        return $this;
    }

    /**
     * Get paymentOverrideAcquisiteur3
     *
     * @return float 
     */
    public function getPaymentOverrideAcquisiteur3()
    {
        return $this->paymentOverrideAcquisiteur3;
    }

    /**
     * Set isAddedInvoiced
     *
     * @param boolean $isAddedInvoiced
     * @return Customer
     */
    public function setIsAddedInvoiced($isAddedInvoiced)
    {
        $this->isAddedInvoiced = $isAddedInvoiced;
    
        return $this;
    }

    /**
     * Get isAddedInvoiced
     *
     * @return boolean 
     */
    public function getIsAddedInvoiced()
    {
        return $this->isAddedInvoiced;
    }

    /**
     * Set extraPriceReportDeliveryOverride
     *
     * @param float $extraPriceReportDeliveryOverride
     * @return Customer
     */
    public function setExtraPriceReportDeliveryOverride($extraPriceReportDeliveryOverride)
    {
        $this->extraPriceReportDeliveryOverride = $extraPriceReportDeliveryOverride;
    
        return $this;
    }

    /**
     * Get extraPriceReportDeliveryOverride
     *
     * @return float 
     */
    public function getExtraPriceReportDeliveryOverride()
    {
        return $this->extraPriceReportDeliveryOverride;
    }

    /**
     * Set idOriginalCustomer
     *
     * @param integer $idOriginalCustomer
     * @return Customer
     */
    public function setIdOriginalCustomer($idOriginalCustomer)
    {
        $this->idOriginalCustomer = $idOriginalCustomer;
    
        return $this;
    }

    /**
     * Get idOriginalCustomer
     *
     * @return integer 
     */
    public function getIdOriginalCustomer()
    {
        return $this->idOriginalCustomer;
    }

    /**
     * Set productList
     *
     * @param string $productList
     * @return Customer
     */
    public function setProductList($productList)
    {
        $this->productList = $productList;
    
        return $this;
    }

    /**
     * Get productList
     *
     * @return string 
     */
    public function getProductList()
    {
        return $this->productList;
    }

    /**
     * Set categoryList
     *
     * @param string $categoryList
     * @return Customer
     */
    public function setCategoryList($categoryList)
    {
        $this->categoryList = $categoryList;
    
        return $this;
    }

    /**
     * Get categoryList
     *
     * @return string 
     */
    public function getCategoryList()
    {
        return $this->categoryList;
    }

    /**
     * Set dnaSampleOrderNumberVario
     *
     * @param string $dnaSampleOrderNumberVario
     * @return Customer
     */
    public function setDnaSampleOrderNumberVario($dnaSampleOrderNumberVario)
    {
        $this->dnaSampleOrderNumberVario = $dnaSampleOrderNumberVario;
    
        return $this;
    }

    /**
     * Get dnaSampleOrderNumberVario
     *
     * @return string 
     */
    public function getDnaSampleOrderNumberVario()
    {
        return $this->dnaSampleOrderNumberVario;
    }

    /**
     * Set isRecall
     *
     * @param boolean $isRecall
     * @return Customer
     */
    public function setIsRecall($isRecall)
    {
        $this->isRecall = $isRecall;
    
        return $this;
    }

    /**
     * Get isRecall
     *
     * @return boolean 
     */
    public function getIsRecall()
    {
        return $this->isRecall;
    }

    /**
     * Set invoiceAddressIsUsed
     *
     * @param boolean $invoiceAddressIsUsed
     * @return Customer
     */
    public function setInvoiceAddressIsUsed($invoiceAddressIsUsed)
    {
        $this->invoiceAddressIsUsed = $invoiceAddressIsUsed;
    
        return $this;
    }

    /**
     * Get invoiceAddressIsUsed
     *
     * @return boolean 
     */
    public function getInvoiceAddressIsUsed()
    {
        return $this->invoiceAddressIsUsed;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     * @return Customer
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;
    
        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string 
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressPostCode
     *
     * @param string $invoiceAddressPostCode
     * @return Customer
     */
    public function setInvoiceAddressPostCode($invoiceAddressPostCode)
    {
        $this->invoiceAddressPostCode = $invoiceAddressPostCode;
    
        return $this;
    }

    /**
     * Get invoiceAddressPostCode
     *
     * @return string 
     */
    public function getInvoiceAddressPostCode()
    {
        return $this->invoiceAddressPostCode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     * @return Customer
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;
    
        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string 
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressIdCountry
     *
     * @param integer $invoiceAddressIdCountry
     * @return Customer
     */
    public function setInvoiceAddressIdCountry($invoiceAddressIdCountry)
    {
        $this->invoiceAddressIdCountry = $invoiceAddressIdCountry;
    
        return $this;
    }

    /**
     * Get invoiceAddressIdCountry
     *
     * @return integer 
     */
    public function getInvoiceAddressIdCountry()
    {
        return $this->invoiceAddressIdCountry;
    }

    /**
     * Set invoiceAddressTelephone
     *
     * @param string $invoiceAddressTelephone
     * @return Customer
     */
    public function setInvoiceAddressTelephone($invoiceAddressTelephone)
    {
        $this->invoiceAddressTelephone = $invoiceAddressTelephone;
    
        return $this;
    }

    /**
     * Get invoiceAddressTelephone
     *
     * @return string 
     */
    public function getInvoiceAddressTelephone()
    {
        return $this->invoiceAddressTelephone;
    }

    /**
     * Set invoiceAddressFax
     *
     * @param string $invoiceAddressFax
     * @return Customer
     */
    public function setInvoiceAddressFax($invoiceAddressFax)
    {
        $this->invoiceAddressFax = $invoiceAddressFax;
    
        return $this;
    }

    /**
     * Get invoiceAddressFax
     *
     * @return string 
     */
    public function getInvoiceAddressFax()
    {
        return $this->invoiceAddressFax;
    }

    /**
     * Set invoiceAddressClientName
     *
     * @param string $invoiceAddressClientName
     * @return Customer
     */
    public function setInvoiceAddressClientName($invoiceAddressClientName)
    {
        $this->invoiceAddressClientName = $invoiceAddressClientName;
    
        return $this;
    }

    /**
     * Get invoiceAddressClientName
     *
     * @return string 
     */
    public function getInvoiceAddressClientName()
    {
        return $this->invoiceAddressClientName;
    }

    /**
     * Set invoiceAddressCompanyName
     *
     * @param string $invoiceAddressCompanyName
     * @return Customer
     */
    public function setInvoiceAddressCompanyName($invoiceAddressCompanyName)
    {
        $this->invoiceAddressCompanyName = $invoiceAddressCompanyName;
    
        return $this;
    }

    /**
     * Get invoiceAddressCompanyName
     *
     * @return string 
     */
    public function getInvoiceAddressCompanyName()
    {
        return $this->invoiceAddressCompanyName;
    }

    /**
     * Set extraPriceNutriMeDeliveryOverride
     *
     * @param float $extraPriceNutriMeDeliveryOverride
     * @return Customer
     */
    public function setExtraPriceNutriMeDeliveryOverride($extraPriceNutriMeDeliveryOverride)
    {
        $this->extraPriceNutriMeDeliveryOverride = $extraPriceNutriMeDeliveryOverride;
    
        return $this;
    }

    /**
     * Get extraPriceNutriMeDeliveryOverride
     *
     * @return float 
     */
    public function getExtraPriceNutriMeDeliveryOverride()
    {
        return $this->extraPriceNutriMeDeliveryOverride;
    }

    /**
     * Set idAcquisiteur1
     *
     * @param integer $idAcquisiteur1
     * @return Customer
     */
    public function setIdAcquisiteur1($idAcquisiteur1)
    {
        $this->idAcquisiteur1 = $idAcquisiteur1;
    
        return $this;
    }

    /**
     * Get idAcquisiteur1
     *
     * @return integer 
     */
    public function getIdAcquisiteur1()
    {
        return $this->idAcquisiteur1;
    }

    /**
     * Set acquisiteurCommissionRate1
     *
     * @param integer $acquisiteurCommissionRate1
     * @return Customer
     */
    public function setAcquisiteurCommissionRate1($acquisiteurCommissionRate1)
    {
        $this->acquisiteurCommissionRate1 = $acquisiteurCommissionRate1;
    
        return $this;
    }

    /**
     * Get acquisiteurCommissionRate1
     *
     * @return integer 
     */
    public function getAcquisiteurCommissionRate1()
    {
        return $this->acquisiteurCommissionRate1;
    }

    /**
     * Set idAcquisiteur2
     *
     * @param integer $idAcquisiteur2
     * @return Customer
     */
    public function setIdAcquisiteur2($idAcquisiteur2)
    {
        $this->idAcquisiteur2 = $idAcquisiteur2;
    
        return $this;
    }

    /**
     * Get idAcquisiteur2
     *
     * @return integer 
     */
    public function getIdAcquisiteur2()
    {
        return $this->idAcquisiteur2;
    }

    /**
     * Set acquisiteurCommissionRate2
     *
     * @param integer $acquisiteurCommissionRate2
     * @return Customer
     */
    public function setAcquisiteurCommissionRate2($acquisiteurCommissionRate2)
    {
        $this->acquisiteurCommissionRate2 = $acquisiteurCommissionRate2;
    
        return $this;
    }

    /**
     * Get acquisiteurCommissionRate2
     *
     * @return integer 
     */
    public function getAcquisiteurCommissionRate2()
    {
        return $this->acquisiteurCommissionRate2;
    }

    /**
     * Set idAcquisiteur3
     *
     * @param integer $idAcquisiteur3
     * @return Customer
     */
    public function setIdAcquisiteur3($idAcquisiteur3)
    {
        $this->idAcquisiteur3 = $idAcquisiteur3;
    
        return $this;
    }

    /**
     * Get idAcquisiteur3
     *
     * @return integer 
     */
    public function getIdAcquisiteur3()
    {
        return $this->idAcquisiteur3;
    }

    /**
     * Set acquisiteurCommissionRate3
     *
     * @param integer $acquisiteurCommissionRate3
     * @return Customer
     */
    public function setAcquisiteurCommissionRate3($acquisiteurCommissionRate3)
    {
        $this->acquisiteurCommissionRate3 = $acquisiteurCommissionRate3;
    
        return $this;
    }

    /**
     * Get acquisiteurCommissionRate3
     *
     * @return integer 
     */
    public function getAcquisiteurCommissionRate3()
    {
        return $this->acquisiteurCommissionRate3;
    }

    /**
     * Set trackingCode
     *
     * @param string $trackingCode
     * @return Customer
     */
    public function setTrackingCode($trackingCode)
    {
        $this->trackingCode = $trackingCode;
    
        return $this;
    }

    /**
     * Get trackingCode
     *
     * @return string 
     */
    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return Customer
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
    
        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set lockDate
     *
     * @param \DateTime $lockDate
     * @return Customer
     */
    public function setLockDate($lockDate)
    {
        $this->lockDate = $lockDate;
    
        return $this;
    }

    /**
     * Get lockDate
     *
     * @return \DateTime 
     */
    public function getLockDate()
    {
        return $this->lockDate;
    }

    /**
     * Set isPrintedReportSentToPartner
     *
     * @param boolean $isPrintedReportSentToPartner
     * @return Customer
     */
    public function setIsPrintedReportSentToPartner($isPrintedReportSentToPartner)
    {
        $this->isPrintedReportSentToPartner = $isPrintedReportSentToPartner;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToPartner
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToPartner()
    {
        return $this->isPrintedReportSentToPartner;
    }

    /**
     * Set isPrintedReportSentToDc
     *
     * @param boolean $isPrintedReportSentToDc
     * @return Customer
     */
    public function setIsPrintedReportSentToDc($isPrintedReportSentToDc)
    {
        $this->isPrintedReportSentToDc = $isPrintedReportSentToDc;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToDc
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToDc()
    {
        return $this->isPrintedReportSentToDc;
    }

    /**
     * Set isPrintedReportSentToCustomer
     *
     * @param boolean $isPrintedReportSentToCustomer
     * @return Customer
     */
    public function setIsPrintedReportSentToCustomer($isPrintedReportSentToCustomer)
    {
        $this->isPrintedReportSentToCustomer = $isPrintedReportSentToCustomer;
    
        return $this;
    }

    /**
     * Get isPrintedReportSentToCustomer
     *
     * @return boolean 
     */
    public function getIsPrintedReportSentToCustomer()
    {
        return $this->isPrintedReportSentToCustomer;
    }

    /**
     * Set isDigitalReportGoesToCustomer
     *
     * @param boolean $isDigitalReportGoesToCustomer
     * @return Customer
     */
    public function setIsDigitalReportGoesToCustomer($isDigitalReportGoesToCustomer)
    {
        $this->isDigitalReportGoesToCustomer = $isDigitalReportGoesToCustomer;
    
        return $this;
    }

    /**
     * Get isDigitalReportGoesToCustomer
     *
     * @return boolean 
     */
    public function getIsDigitalReportGoesToCustomer()
    {
        return $this->isDigitalReportGoesToCustomer;
    }

    /**
     * Set idRecallGoesTo
     *
     * @param integer $idRecallGoesTo
     * @return Customer
     */
    public function setIdRecallGoesTo($idRecallGoesTo)
    {
        $this->idRecallGoesTo = $idRecallGoesTo;
    
        return $this;
    }

    /**
     * Get idRecallGoesTo
     *
     * @return integer 
     */
    public function getIdRecallGoesTo()
    {
        return $this->idRecallGoesTo;
    }

    /**
     * Set isReportDeliveryDownloadAccess
     *
     * @param boolean $isReportDeliveryDownloadAccess
     * @return Customer
     */
    public function setIsReportDeliveryDownloadAccess($isReportDeliveryDownloadAccess)
    {
        $this->isReportDeliveryDownloadAccess = $isReportDeliveryDownloadAccess;
    
        return $this;
    }

    /**
     * Get isReportDeliveryDownloadAccess
     *
     * @return boolean 
     */
    public function getIsReportDeliveryDownloadAccess()
    {
        return $this->isReportDeliveryDownloadAccess;
    }

    /**
     * Set isReportDeliveryFtpServer
     *
     * @param boolean $isReportDeliveryFtpServer
     * @return Customer
     */
    public function setIsReportDeliveryFtpServer($isReportDeliveryFtpServer)
    {
        $this->isReportDeliveryFtpServer = $isReportDeliveryFtpServer;
    
        return $this;
    }

    /**
     * Get isReportDeliveryFtpServer
     *
     * @return boolean 
     */
    public function getIsReportDeliveryFtpServer()
    {
        return $this->isReportDeliveryFtpServer;
    }

    /**
     * Set isReportDeliveryPrintedBooklet
     *
     * @param boolean $isReportDeliveryPrintedBooklet
     * @return Customer
     */
    public function setIsReportDeliveryPrintedBooklet($isReportDeliveryPrintedBooklet)
    {
        $this->isReportDeliveryPrintedBooklet = $isReportDeliveryPrintedBooklet;
    
        return $this;
    }

    /**
     * Get isReportDeliveryPrintedBooklet
     *
     * @return boolean 
     */
    public function getIsReportDeliveryPrintedBooklet()
    {
        return $this->isReportDeliveryPrintedBooklet;
    }

    /**
     * Set isReportDeliveryDontChargeBooklet
     *
     * @param boolean $isReportDeliveryDontChargeBooklet
     * @return Customer
     */
    public function setIsReportDeliveryDontChargeBooklet($isReportDeliveryDontChargeBooklet)
    {
        $this->isReportDeliveryDontChargeBooklet = $isReportDeliveryDontChargeBooklet;
    
        return $this;
    }

    /**
     * Get isReportDeliveryDontChargeBooklet
     *
     * @return boolean 
     */
    public function getIsReportDeliveryDontChargeBooklet()
    {
        return $this->isReportDeliveryDontChargeBooklet;
    }

    /**
     * Set previousOrderNumber
     *
     * @param string $previousOrderNumber
     * @return Customer
     */
    public function setPreviousOrderNumber($previousOrderNumber)
    {
        $this->previousOrderNumber = $previousOrderNumber;
    
        return $this;
    }

    /**
     * Get previousOrderNumber
     *
     * @return string 
     */
    public function getPreviousOrderNumber()
    {
        return $this->previousOrderNumber;
    }

    /**
     * Set canCreateLoginAndSendStatusMessageToUser
     *
     * @param boolean $canCreateLoginAndSendStatusMessageToUser
     * @return Customer
     */
    public function setCanCreateLoginAndSendStatusMessageToUser($canCreateLoginAndSendStatusMessageToUser)
    {
        $this->canCreateLoginAndSendStatusMessageToUser = $canCreateLoginAndSendStatusMessageToUser;
    
        return $this;
    }

    /**
     * Get canCreateLoginAndSendStatusMessageToUser
     *
     * @return boolean 
     */
    public function getCanCreateLoginAndSendStatusMessageToUser()
    {
        return $this->canCreateLoginAndSendStatusMessageToUser;
    }

    /**
     * Set canCustomersDownloadReports
     *
     * @param boolean $canCustomersDownloadReports
     * @return Customer
     */
    public function setCanCustomersDownloadReports($canCustomersDownloadReports)
    {
        $this->canCustomersDownloadReports = $canCustomersDownloadReports;
    
        return $this;
    }

    /**
     * Get canCustomersDownloadReports
     *
     * @return boolean 
     */
    public function getCanCustomersDownloadReports()
    {
        return $this->canCustomersDownloadReports;
    }

    /**
     * Set isDisplayInCustomerDashboard
     *
     * @param boolean $isDisplayInCustomerDashboard
     * @return Customer
     */
    public function setIsDisplayInCustomerDashboard($isDisplayInCustomerDashboard)
    {
        $this->isDisplayInCustomerDashboard = $isDisplayInCustomerDashboard;
    
        return $this;
    }

    /**
     * Get isDisplayInCustomerDashboard
     *
     * @return boolean 
     */
    public function getIsDisplayInCustomerDashboard()
    {
        return $this->isDisplayInCustomerDashboard;
    }

    /**
     * Set invoiceAddressUid
     *
     * @param string $invoiceAddressUid
     * @return Customer
     */
    public function setInvoiceAddressUid($invoiceAddressUid)
    {
        $this->invoiceAddressUid = $invoiceAddressUid;
    
        return $this;
    }

    /**
     * Get invoiceAddressUid
     *
     * @return string 
     */
    public function getInvoiceAddressUid()
    {
        return $this->invoiceAddressUid;
    }

    /**
     * Set isDelayDownloadReport
     *
     * @param boolean $isDelayDownloadReport
     * @return Customer
     */
    public function setIsDelayDownloadReport($isDelayDownloadReport)
    {
        $this->isDelayDownloadReport = $isDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get isDelayDownloadReport
     *
     * @return boolean 
     */
    public function getIsDelayDownloadReport()
    {
        return $this->isDelayDownloadReport;
    }

    /**
     * Set numberDateDelayDownloadReport
     *
     * @param integer $numberDateDelayDownloadReport
     * @return Customer
     */
    public function setNumberDateDelayDownloadReport($numberDateDelayDownloadReport)
    {
        $this->numberDateDelayDownloadReport = $numberDateDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get numberDateDelayDownloadReport
     *
     * @return integer 
     */
    public function getNumberDateDelayDownloadReport()
    {
        return $this->numberDateDelayDownloadReport;
    }

    /**
     * Set dateSetDelayDownloadReport
     *
     * @param \DateTime $dateSetDelayDownloadReport
     * @return Customer
     */
    public function setDateSetDelayDownloadReport($dateSetDelayDownloadReport)
    {
        $this->dateSetDelayDownloadReport = $dateSetDelayDownloadReport;
    
        return $this;
    }

    /**
     * Get dateSetDelayDownloadReport
     *
     * @return \DateTime 
     */
    public function getDateSetDelayDownloadReport()
    {
        return $this->dateSetDelayDownloadReport;
    }
}