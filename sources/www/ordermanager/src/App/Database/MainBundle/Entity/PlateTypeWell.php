<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PlateTypeWell
 *
 * @ORM\Table(name="plate_type_well")
 * @ORM\Entity
 */
class PlateTypeWell
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPlateType
     *
     * @ORM\Column(name="id_plate_type", type="integer", nullable=true)
     */
    private $idPlateType;

    /**
     * @var integer $idGene
     *
     * @ORM\Column(name="id_gene", type="integer", nullable=true)
     */
    private $idGene;

    /**
     * @var integer $row
     *
     * @ORM\Column(name="row", type="integer", nullable=true)
     */
    private $row;

    /**
     * @var integer $col
     *
     * @ORM\Column(name="col", type="integer", nullable=true)
     */
    private $col;

    /**
     * @var string $plateWellIndex
     *
     * @ORM\Column(name="plate_well_index", type="string", length=2, nullable=true)
     */
    private $plateWellIndex;

    /**
     * @var string $color
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPlateType
     *
     * @param integer $idPlateType
     * @return PlateTypeWell
     */
    public function setIdPlateType($idPlateType)
    {
        $this->idPlateType = $idPlateType;
    
        return $this;
    }

    /**
     * Get idPlateType
     *
     * @return integer 
     */
    public function getIdPlateType()
    {
        return $this->idPlateType;
    }

    /**
     * Set idGene
     *
     * @param integer $idGene
     * @return PlateTypeWell
     */
    public function setIdGene($idGene)
    {
        $this->idGene = $idGene;
    
        return $this;
    }

    /**
     * Get idGene
     *
     * @return integer 
     */
    public function getIdGene()
    {
        return $this->idGene;
    }

    /**
     * Set row
     *
     * @param integer $row
     * @return PlateTypeWell
     */
    public function setRow($row)
    {
        $this->row = $row;
    
        return $this;
    }

    /**
     * Get row
     *
     * @return integer 
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set col
     *
     * @param integer $col
     * @return PlateTypeWell
     */
    public function setCol($col)
    {
        $this->col = $col;
    
        return $this;
    }

    /**
     * Get col
     *
     * @return integer 
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set plateWellIndex
     *
     * @param string $plateWellIndex
     * @return PlateTypeWell
     */
    public function setPlateWellIndex($plateWellIndex)
    {
        $this->plateWellIndex = $plateWellIndex;
    
        return $this;
    }

    /**
     * Get plateWellIndex
     *
     * @return string 
     */
    public function getPlateWellIndex()
    {
        return $this->plateWellIndex;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return PlateTypeWell
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }
}