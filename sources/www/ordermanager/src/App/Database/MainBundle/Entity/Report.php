<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity
 */
class Report
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $testData
     *
     * @ORM\Column(name="test_data", type="text", nullable=true)
     */
    private $testData;

    /**
     * @var boolean $isDeleted
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var integer $idFormulaTemplate
     *
     * @ORM\Column(name="id_formula_template", type="integer", nullable=true)
     */
    private $idFormulaTemplate;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var boolean $isVisibleByDefault
     *
     * @ORM\Column(name="is_visible_by_default", type="boolean", nullable=true)
     */
    private $isVisibleByDefault;

    /**
     * @var string $reportCode
     *
     * @ORM\Column(name="report_code", type="string", length=255, nullable=true)
     */
    private $reportCode;

    /**
     * @var string $shortName
     *
     * @ORM\Column(name="short_name", type="string", length=255, nullable=true)
     */
    private $shortName;

    /**
     * @var boolean $isReleased
     *
     * @ORM\Column(name="is_released", type="boolean", nullable=true)
     */
    private $isReleased;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set testData
     *
     * @param string $testData
     * @return Report
     */
    public function setTestData($testData)
    {
        $this->testData = $testData;
    
        return $this;
    }

    /**
     * Get testData
     *
     * @return string 
     */
    public function getTestData()
    {
        return $this->testData;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Report
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set idFormulaTemplate
     *
     * @param integer $idFormulaTemplate
     * @return Report
     */
    public function setIdFormulaTemplate($idFormulaTemplate)
    {
        $this->idFormulaTemplate = $idFormulaTemplate;
    
        return $this;
    }

    /**
     * Get idFormulaTemplate
     *
     * @return integer 
     */
    public function getIdFormulaTemplate()
    {
        return $this->idFormulaTemplate;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Report
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isVisibleByDefault
     *
     * @param boolean $isVisibleByDefault
     * @return Report
     */
    public function setIsVisibleByDefault($isVisibleByDefault)
    {
        $this->isVisibleByDefault = $isVisibleByDefault;
    
        return $this;
    }

    /**
     * Get isVisibleByDefault
     *
     * @return boolean 
     */
    public function getIsVisibleByDefault()
    {
        return $this->isVisibleByDefault;
    }

    /**
     * Set reportCode
     *
     * @param string $reportCode
     * @return Report
     */
    public function setReportCode($reportCode)
    {
        $this->reportCode = $reportCode;
    
        return $this;
    }

    /**
     * Get reportCode
     *
     * @return string 
     */
    public function getReportCode()
    {
        return $this->reportCode;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Report
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    
        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set isReleased
     *
     * @param boolean $isReleased
     * @return Report
     */
    public function setIsReleased($isReleased)
    {
        $this->isReleased = $isReleased;
    
        return $this;
    }

    /**
     * Get isReleased
     *
     * @return boolean 
     */
    public function getIsReleased()
    {
        return $this->isReleased;
    }
}