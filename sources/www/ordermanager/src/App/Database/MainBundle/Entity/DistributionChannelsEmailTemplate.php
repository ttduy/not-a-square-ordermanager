<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\DistributionChannelsEmailTemplate
 *
 * @ORM\Table(name="distribution_channels_email_template")
 * @ORM\Entity
 */
class DistributionChannelsEmailTemplate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $distributionchannelid
     *
     * @ORM\Column(name="DistributionChannelId", type="integer", nullable=true)
     */
    private $distributionchannelid;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $emailtemplateid
     *
     * @ORM\Column(name="EmailTemplateId", type="integer", nullable=true)
     */
    private $emailtemplateid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set distributionchannelid
     *
     * @param integer $distributionchannelid
     * @return DistributionChannelsEmailTemplate
     */
    public function setDistributionchannelid($distributionchannelid)
    {
        $this->distributionchannelid = $distributionchannelid;
    
        return $this;
    }

    /**
     * Get distributionchannelid
     *
     * @return integer 
     */
    public function getDistributionchannelid()
    {
        return $this->distributionchannelid;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return DistributionChannelsEmailTemplate
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set emailtemplateid
     *
     * @param integer $emailtemplateid
     * @return DistributionChannelsEmailTemplate
     */
    public function setEmailtemplateid($emailtemplateid)
    {
        $this->emailtemplateid = $emailtemplateid;
    
        return $this;
    }

    /**
     * Get emailtemplateid
     *
     * @return integer 
     */
    public function getEmailtemplateid()
    {
        return $this->emailtemplateid;
    }
}