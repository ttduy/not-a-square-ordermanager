<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PelletUse
 *
 * @ORM\Table(name="pellet_use")
 * @ORM\Entity
 */
class PelletUse
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPellet
     *
     * @ORM\Column(name="id_pellet", type="integer", nullable=true)
     */
    private $idPellet;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="date", nullable=true)
     */
    private $timestamp;

    /**
     * @var integer $numCups
     *
     * @ORM\Column(name="num_cups", type="integer", nullable=true)
     */
    private $numCups;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPellet
     *
     * @param integer $idPellet
     * @return PelletUse
     */
    public function setIdPellet($idPellet)
    {
        $this->idPellet = $idPellet;
    
        return $this;
    }

    /**
     * Get idPellet
     *
     * @return integer 
     */
    public function getIdPellet()
    {
        return $this->idPellet;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return PelletUse
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set numCups
     *
     * @param integer $numCups
     * @return PelletUse
     */
    public function setNumCups($numCups)
    {
        $this->numCups = $numCups;
    
        return $this;
    }

    /**
     * Get numCups
     *
     * @return integer 
     */
    public function getNumCups()
    {
        return $this->numCups;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PelletUse
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}