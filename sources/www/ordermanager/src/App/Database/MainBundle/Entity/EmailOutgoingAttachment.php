<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailOutgoingAttachment
 *
 * @ORM\Table(name="email_outgoing_attachment")
 * @ORM\Entity
 */
class EmailOutgoingAttachment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmailOutgoing
     *
     * @ORM\Column(name="id_email_outgoing", type="integer", nullable=true)
     */
    private $idEmailOutgoing;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @var string $originalName
     *
     * @ORM\Column(name="original_name", type="text", nullable=true)
     */
    private $originalName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmailOutgoing
     *
     * @param integer $idEmailOutgoing
     * @return EmailOutgoingAttachment
     */
    public function setIdEmailOutgoing($idEmailOutgoing)
    {
        $this->idEmailOutgoing = $idEmailOutgoing;
    
        return $this;
    }

    /**
     * Get idEmailOutgoing
     *
     * @return integer 
     */
    public function getIdEmailOutgoing()
    {
        return $this->idEmailOutgoing;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EmailOutgoingAttachment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     * @return EmailOutgoingAttachment
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    
        return $this;
    }

    /**
     * Get originalName
     *
     * @return string 
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }
}