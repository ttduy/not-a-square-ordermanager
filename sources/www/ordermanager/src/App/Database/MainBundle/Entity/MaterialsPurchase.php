<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MaterialsPurchase
 *
 * @ORM\Table(name="materials_purchase")
 * @ORM\Entity
 */
class MaterialsPurchase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $materialid
     *
     * @ORM\Column(name="MaterialId", type="integer", nullable=true)
     */
    private $materialid;

    /**
     * @var \DateTime $dateordered
     *
     * @ORM\Column(name="DateOrdered", type="date", nullable=true)
     */
    private $dateordered;

    /**
     * @var boolean $orderedreceived
     *
     * @ORM\Column(name="OrderedReceived", type="boolean", nullable=true)
     */
    private $orderedreceived;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(name="Quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float $value
     *
     * @ORM\Column(name="Value", type="decimal", nullable=true)
     */
    private $value;

    /**
     * @var string $orderby
     *
     * @ORM\Column(name="OrderBy", type="string", length=255, nullable=true)
     */
    private $orderby;

    /**
     * @var string $lotNumber
     *
     * @ORM\Column(name="lot_number", type="string", length=255, nullable=true)
     */
    private $lotNumber;

    /**
     * @var \DateTime $expiryDate
     *
     * @ORM\Column(name="expiry_date", type="date", nullable=true)
     */
    private $expiryDate;

    /**
     * @var boolean $expiryDateWarningDisabled
     *
     * @ORM\Column(name="expiry_date_warning_disabled", type="boolean", nullable=false)
     */
    private $expiryDateWarningDisabled;

    /**
     * @var \DateTime $receivedDate
     *
     * @ORM\Column(name="received_date", type="date", nullable=true)
     */
    private $receivedDate;

    /**
     * @var string $receivedBy
     *
     * @ORM\Column(name="received_by", type="string", length=255, nullable=true)
     */
    private $receivedBy;

    /**
     * @var string $externalLot
     *
     * @ORM\Column(name="external_lot", type="string", length=255, nullable=true)
     */
    private $externalLot;

    /**
     * @var string $info
     *
     * @ORM\Column(name="info", type="text", nullable=true)
     */
    private $info;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialid
     *
     * @param integer $materialid
     * @return MaterialsPurchase
     */
    public function setMaterialid($materialid)
    {
        $this->materialid = $materialid;
    
        return $this;
    }

    /**
     * Get materialid
     *
     * @return integer 
     */
    public function getMaterialid()
    {
        return $this->materialid;
    }

    /**
     * Set dateordered
     *
     * @param \DateTime $dateordered
     * @return MaterialsPurchase
     */
    public function setDateordered($dateordered)
    {
        $this->dateordered = $dateordered;
    
        return $this;
    }

    /**
     * Get dateordered
     *
     * @return \DateTime 
     */
    public function getDateordered()
    {
        return $this->dateordered;
    }

    /**
     * Set orderedreceived
     *
     * @param boolean $orderedreceived
     * @return MaterialsPurchase
     */
    public function setOrderedreceived($orderedreceived)
    {
        $this->orderedreceived = $orderedreceived;
    
        return $this;
    }

    /**
     * Get orderedreceived
     *
     * @return boolean 
     */
    public function getOrderedreceived()
    {
        return $this->orderedreceived;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return MaterialsPurchase
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return MaterialsPurchase
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set orderby
     *
     * @param string $orderby
     * @return MaterialsPurchase
     */
    public function setOrderby($orderby)
    {
        $this->orderby = $orderby;
    
        return $this;
    }

    /**
     * Get orderby
     *
     * @return string 
     */
    public function getOrderby()
    {
        return $this->orderby;
    }

    /**
     * Set lotNumber
     *
     * @param string $lotNumber
     * @return MaterialsPurchase
     */
    public function setLotNumber($lotNumber)
    {
        $this->lotNumber = $lotNumber;
    
        return $this;
    }

    /**
     * Get lotNumber
     *
     * @return string 
     */
    public function getLotNumber()
    {
        return $this->lotNumber;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return MaterialsPurchase
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set expiryDateWarningDisabled
     *
     * @param boolean $expiryDateWarningDisabled
     * @return MaterialsPurchase
     */
    public function setExpiryDateWarningDisabled($expiryDateWarningDisabled)
    {
        $this->expiryDateWarningDisabled = $expiryDateWarningDisabled;
    
        return $this;
    }

    /**
     * Get expiryDateWarningDisabled
     *
     * @return boolean 
     */
    public function getExpiryDateWarningDisabled()
    {
        return $this->expiryDateWarningDisabled;
    }

    /**
     * Set receivedDate
     *
     * @param \DateTime $receivedDate
     * @return MaterialsPurchase
     */
    public function setReceivedDate($receivedDate)
    {
        $this->receivedDate = $receivedDate;
    
        return $this;
    }

    /**
     * Get receivedDate
     *
     * @return \DateTime 
     */
    public function getReceivedDate()
    {
        return $this->receivedDate;
    }

    /**
     * Set receivedBy
     *
     * @param string $receivedBy
     * @return MaterialsPurchase
     */
    public function setReceivedBy($receivedBy)
    {
        $this->receivedBy = $receivedBy;
    
        return $this;
    }

    /**
     * Get receivedBy
     *
     * @return string 
     */
    public function getReceivedBy()
    {
        return $this->receivedBy;
    }

    /**
     * Set externalLot
     *
     * @param string $externalLot
     * @return MaterialsPurchase
     */
    public function setExternalLot($externalLot)
    {
        $this->externalLot = $externalLot;
    
        return $this;
    }

    /**
     * Get externalLot
     *
     * @return string 
     */
    public function getExternalLot()
    {
        return $this->externalLot;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return MaterialsPurchase
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }
}