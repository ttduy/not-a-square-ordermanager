<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CashFlowSubmitDataHistoryError
 *
 * @ORM\Table(name="cash_flow_submit_data_history_error")
 * @ORM\Entity
 */
class CashFlowSubmitDataHistoryError
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCashFlowSubmitDataHistory
     *
     * @ORM\Column(name="id_cash_flow_submit_data_history", type="integer", nullable=true)
     */
    private $idCashFlowSubmitDataHistory;

    /**
     * @var integer $idErrorType
     *
     * @ORM\Column(name="id_error_type", type="integer", nullable=true)
     */
    private $idErrorType;

    /**
     * @var string $errorData
     *
     * @ORM\Column(name="error_data", type="text", nullable=true)
     */
    private $errorData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCashFlowSubmitDataHistory
     *
     * @param integer $idCashFlowSubmitDataHistory
     * @return CashFlowSubmitDataHistoryError
     */
    public function setIdCashFlowSubmitDataHistory($idCashFlowSubmitDataHistory)
    {
        $this->idCashFlowSubmitDataHistory = $idCashFlowSubmitDataHistory;
    
        return $this;
    }

    /**
     * Get idCashFlowSubmitDataHistory
     *
     * @return integer 
     */
    public function getIdCashFlowSubmitDataHistory()
    {
        return $this->idCashFlowSubmitDataHistory;
    }

    /**
     * Set idErrorType
     *
     * @param integer $idErrorType
     * @return CashFlowSubmitDataHistoryError
     */
    public function setIdErrorType($idErrorType)
    {
        $this->idErrorType = $idErrorType;
    
        return $this;
    }

    /**
     * Get idErrorType
     *
     * @return integer 
     */
    public function getIdErrorType()
    {
        return $this->idErrorType;
    }

    /**
     * Set errorData
     *
     * @param string $errorData
     * @return CashFlowSubmitDataHistoryError
     */
    public function setErrorData($errorData)
    {
        $this->errorData = $errorData;
    
        return $this;
    }

    /**
     * Get errorData
     *
     * @return string 
     */
    public function getErrorData()
    {
        return $this->errorData;
    }
}