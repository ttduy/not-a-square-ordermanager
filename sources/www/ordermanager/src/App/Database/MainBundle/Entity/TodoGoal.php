<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\TodoGoal
 *
 * @ORM\Table(name="todo_goal")
 * @ORM\Entity
 */
class TodoGoal
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $creationDate
     *
     * @ORM\Column(name="creation_date", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $milestones
     *
     * @ORM\Column(name="milestones", type="text", nullable=true)
     */
    private $milestones;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $idFinishIn
     *
     * @ORM\Column(name="id_finish_in", type="integer", nullable=true)
     */
    private $idFinishIn;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return TodoGoal
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TodoGoal
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TodoGoal
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set milestones
     *
     * @param string $milestones
     * @return TodoGoal
     */
    public function setMilestones($milestones)
    {
        $this->milestones = $milestones;
    
        return $this;
    }

    /**
     * Get milestones
     *
     * @return string 
     */
    public function getMilestones()
    {
        return $this->milestones;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TodoGoal
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idFinishIn
     *
     * @param integer $idFinishIn
     * @return TodoGoal
     */
    public function setIdFinishIn($idFinishIn)
    {
        $this->idFinishIn = $idFinishIn;
    
        return $this;
    }

    /**
     * Get idFinishIn
     *
     * @return integer 
     */
    public function getIdFinishIn()
    {
        return $this->idFinishIn;
    }
}