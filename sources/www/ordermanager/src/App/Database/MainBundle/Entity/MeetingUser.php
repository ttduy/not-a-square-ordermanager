<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MeetingUser
 *
 * @ORM\Table(name="meeting_user")
 * @ORM\Entity
 */
class MeetingUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idMeeting
     *
     * @ORM\Column(name="id_meeting", type="integer", nullable=true)
     */
    private $idMeeting;

    /**
     * @var integer $idUser
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMeeting
     *
     * @param integer $idMeeting
     * @return MeetingUser
     */
    public function setIdMeeting($idMeeting)
    {
        $this->idMeeting = $idMeeting;
    
        return $this;
    }

    /**
     * Get idMeeting
     *
     * @return integer 
     */
    public function getIdMeeting()
    {
        return $this->idMeeting;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return MeetingUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    
        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}