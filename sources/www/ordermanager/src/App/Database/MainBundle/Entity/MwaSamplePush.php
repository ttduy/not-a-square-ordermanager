<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\MwaSamplePush
 *
 * @ORM\Table(name="mwa_sample_push")
 * @ORM\Entity
 */
class MwaSamplePush
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idMwaSample
     *
     * @ORM\Column(name="id_mwa_sample", type="integer", nullable=true)
     */
    private $idMwaSample;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $pushDate
     *
     * @ORM\Column(name="push_date", type="date", nullable=true)
     */
    private $pushDate;

    /**
     * @var string $timestamp
     *
     * @ORM\Column(name="timestamp", type="string", length=255, nullable=true)
     */
    private $timestamp;

    /**
     * @var \DateTime $sentOn
     *
     * @ORM\Column(name="sent_on", type="datetime", nullable=true)
     */
    private $sentOn;

    /**
     * @var string $serverResponseCode
     *
     * @ORM\Column(name="server_response_code", type="text", nullable=true)
     */
    private $serverResponseCode;

    /**
     * @var string $serverResponseRaw
     *
     * @ORM\Column(name="server_response_raw", type="text", nullable=true)
     */
    private $serverResponseRaw;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var \DateTime $pushDatetime
     *
     * @ORM\Column(name="push_datetime", type="datetime", nullable=true)
     */
    private $pushDatetime;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMwaSample
     *
     * @param integer $idMwaSample
     * @return MwaSamplePush
     */
    public function setIdMwaSample($idMwaSample)
    {
        $this->idMwaSample = $idMwaSample;
    
        return $this;
    }

    /**
     * Get idMwaSample
     *
     * @return integer 
     */
    public function getIdMwaSample()
    {
        return $this->idMwaSample;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MwaSamplePush
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set pushDate
     *
     * @param \DateTime $pushDate
     * @return MwaSamplePush
     */
    public function setPushDate($pushDate)
    {
        $this->pushDate = $pushDate;
    
        return $this;
    }

    /**
     * Get pushDate
     *
     * @return \DateTime 
     */
    public function getPushDate()
    {
        return $this->pushDate;
    }

    /**
     * Set timestamp
     *
     * @param string $timestamp
     * @return MwaSamplePush
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return string 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set sentOn
     *
     * @param \DateTime $sentOn
     * @return MwaSamplePush
     */
    public function setSentOn($sentOn)
    {
        $this->sentOn = $sentOn;
    
        return $this;
    }

    /**
     * Get sentOn
     *
     * @return \DateTime 
     */
    public function getSentOn()
    {
        return $this->sentOn;
    }

    /**
     * Set serverResponseCode
     *
     * @param string $serverResponseCode
     * @return MwaSamplePush
     */
    public function setServerResponseCode($serverResponseCode)
    {
        $this->serverResponseCode = $serverResponseCode;
    
        return $this;
    }

    /**
     * Get serverResponseCode
     *
     * @return string 
     */
    public function getServerResponseCode()
    {
        return $this->serverResponseCode;
    }

    /**
     * Set serverResponseRaw
     *
     * @param string $serverResponseRaw
     * @return MwaSamplePush
     */
    public function setServerResponseRaw($serverResponseRaw)
    {
        $this->serverResponseRaw = $serverResponseRaw;
    
        return $this;
    }

    /**
     * Get serverResponseRaw
     *
     * @return string 
     */
    public function getServerResponseRaw()
    {
        return $this->serverResponseRaw;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return MwaSamplePush
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set pushDatetime
     *
     * @param \DateTime $pushDatetime
     * @return MwaSamplePush
     */
    public function setPushDatetime($pushDatetime)
    {
        $this->pushDatetime = $pushDatetime;
    
        return $this;
    }

    /**
     * Get pushDatetime
     *
     * @return \DateTime 
     */
    public function getPushDatetime()
    {
        return $this->pushDatetime;
    }
}