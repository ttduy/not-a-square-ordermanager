<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoBatchLogs
 *
 * @ORM\Table(name="geno_batch_logs")
 * @ORM\Entity
 */
class GenoBatchLogs
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $batchId
     *
     * @ORM\Column(name="batch_id", type="integer", nullable=true)
     */
    private $batchId;

    /**
     * @var integer $amount
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var string $helper
     *
     * @ORM\Column(name="helper", type="string", length=255, nullable=true)
     */
    private $helper;

    /**
     * @var integer $user
     *
     * @ORM\Column(name="user", type="integer", nullable=true)
     */
    private $user;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set batchId
     *
     * @param integer $batchId
     * @return GenoBatchLogs
     */
    public function setBatchId($batchId)
    {
        $this->batchId = $batchId;
    
        return $this;
    }

    /**
     * Get batchId
     *
     * @return integer 
     */
    public function getBatchId()
    {
        return $this->batchId;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return GenoBatchLogs
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set helper
     *
     * @param string $helper
     * @return GenoBatchLogs
     */
    public function setHelper($helper)
    {
        $this->helper = $helper;
    
        return $this;
    }

    /**
     * Get helper
     *
     * @return string 
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return GenoBatchLogs
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return GenoBatchLogs
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}