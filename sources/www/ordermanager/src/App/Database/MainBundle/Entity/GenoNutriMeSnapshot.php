<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoNutriMeSnapshot
 *
 * @ORM\Table(name="geno_nutri_me_snapshot")
 * @ORM\Entity
 */
class GenoNutriMeSnapshot
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $nutriMeId
     *
     * @ORM\Column(name="nutri_me_id", type="integer", nullable=true)
     */
    private $nutriMeId;

    /**
     * @var string $pelletType
     *
     * @ORM\Column(name="pellet_type", type="string", length=10, nullable=true)
     */
    private $pelletType;

    /**
     * @var string $pelletName
     *
     * @ORM\Column(name="pellet_name", type="string", length=255, nullable=true)
     */
    private $pelletName;

    /**
     * @var string $pelletShipment
     *
     * @ORM\Column(name="pellet_shipment", type="string", length=30, nullable=true)
     */
    private $pelletShipment;

    /**
     * @var string $pelletActiveIngredientName
     *
     * @ORM\Column(name="pellet_active_ingredient_name", type="string", length=255, nullable=true)
     */
    private $pelletActiveIngredientName;

    /**
     * @var integer $startTh
     *
     * @ORM\Column(name="start_th", type="integer", nullable=true)
     */
    private $startTh;

    /**
     * @var integer $webUserId
     *
     * @ORM\Column(name="web_user_id", type="integer", nullable=true)
     */
    private $webUserId;

    /**
     * @var float $typeAmount
     *
     * @ORM\Column(name="type_amount", type="float", nullable=true)
     */
    private $typeAmount;

    /**
     * @var \DateTime $createdDate
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nutriMeId
     *
     * @param integer $nutriMeId
     * @return GenoNutriMeSnapshot
     */
    public function setNutriMeId($nutriMeId)
    {
        $this->nutriMeId = $nutriMeId;
    
        return $this;
    }

    /**
     * Get nutriMeId
     *
     * @return integer 
     */
    public function getNutriMeId()
    {
        return $this->nutriMeId;
    }

    /**
     * Set pelletType
     *
     * @param string $pelletType
     * @return GenoNutriMeSnapshot
     */
    public function setPelletType($pelletType)
    {
        $this->pelletType = $pelletType;
    
        return $this;
    }

    /**
     * Get pelletType
     *
     * @return string 
     */
    public function getPelletType()
    {
        return $this->pelletType;
    }

    /**
     * Set pelletName
     *
     * @param string $pelletName
     * @return GenoNutriMeSnapshot
     */
    public function setPelletName($pelletName)
    {
        $this->pelletName = $pelletName;
    
        return $this;
    }

    /**
     * Get pelletName
     *
     * @return string 
     */
    public function getPelletName()
    {
        return $this->pelletName;
    }

    /**
     * Set pelletShipment
     *
     * @param string $pelletShipment
     * @return GenoNutriMeSnapshot
     */
    public function setPelletShipment($pelletShipment)
    {
        $this->pelletShipment = $pelletShipment;
    
        return $this;
    }

    /**
     * Get pelletShipment
     *
     * @return string 
     */
    public function getPelletShipment()
    {
        return $this->pelletShipment;
    }

    /**
     * Set pelletActiveIngredientName
     *
     * @param string $pelletActiveIngredientName
     * @return GenoNutriMeSnapshot
     */
    public function setPelletActiveIngredientName($pelletActiveIngredientName)
    {
        $this->pelletActiveIngredientName = $pelletActiveIngredientName;
    
        return $this;
    }

    /**
     * Get pelletActiveIngredientName
     *
     * @return string 
     */
    public function getPelletActiveIngredientName()
    {
        return $this->pelletActiveIngredientName;
    }

    /**
     * Set startTh
     *
     * @param integer $startTh
     * @return GenoNutriMeSnapshot
     */
    public function setStartTh($startTh)
    {
        $this->startTh = $startTh;
    
        return $this;
    }

    /**
     * Get startTh
     *
     * @return integer 
     */
    public function getStartTh()
    {
        return $this->startTh;
    }

    /**
     * Set webUserId
     *
     * @param integer $webUserId
     * @return GenoNutriMeSnapshot
     */
    public function setWebUserId($webUserId)
    {
        $this->webUserId = $webUserId;
    
        return $this;
    }

    /**
     * Get webUserId
     *
     * @return integer 
     */
    public function getWebUserId()
    {
        return $this->webUserId;
    }

    /**
     * Set typeAmount
     *
     * @param float $typeAmount
     * @return GenoNutriMeSnapshot
     */
    public function setTypeAmount($typeAmount)
    {
        $this->typeAmount = $typeAmount;
    
        return $this;
    }

    /**
     * Get typeAmount
     *
     * @return float 
     */
    public function getTypeAmount()
    {
        return $this->typeAmount;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return GenoNutriMeSnapshot
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
}