<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WidgetStatisticTimePeriod
 *
 * @ORM\Table(name="widget_statistic_time_period")
 * @ORM\Entity
 */
class WidgetStatisticTimePeriod
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWidgetStatistic
     *
     * @ORM\Column(name="id_widget_statistic", type="integer", nullable=true)
     */
    private $idWidgetStatistic;

    /**
     * @var integer $idTimePeriod
     *
     * @ORM\Column(name="id_time_period", type="integer", nullable=true)
     */
    private $idTimePeriod;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWidgetStatistic
     *
     * @param integer $idWidgetStatistic
     * @return WidgetStatisticTimePeriod
     */
    public function setIdWidgetStatistic($idWidgetStatistic)
    {
        $this->idWidgetStatistic = $idWidgetStatistic;
    
        return $this;
    }

    /**
     * Get idWidgetStatistic
     *
     * @return integer 
     */
    public function getIdWidgetStatistic()
    {
        return $this->idWidgetStatistic;
    }

    /**
     * Set idTimePeriod
     *
     * @param integer $idTimePeriod
     * @return WidgetStatisticTimePeriod
     */
    public function setIdTimePeriod($idTimePeriod)
    {
        $this->idTimePeriod = $idTimePeriod;
    
        return $this;
    }

    /**
     * Get idTimePeriod
     *
     * @return integer 
     */
    public function getIdTimePeriod()
    {
        return $this->idTimePeriod;
    }
}