<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormFileZoneTokens
 *
 * @ORM\Table(name="scan_form_file_zone_tokens")
 * @ORM\Entity
 */
class ScanFormFileZoneTokens
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idScanFormFile
     *
     * @ORM\Column(name="id_scan_form_file", type="integer", nullable=true)
     */
    private $idScanFormFile;

    /**
     * @var integer $idScanFormTemplateZone
     *
     * @ORM\Column(name="id_scan_form_template_zone", type="integer", nullable=true)
     */
    private $idScanFormTemplateZone;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=500, nullable=true)
     */
    private $image;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScanFormFile
     *
     * @param integer $idScanFormFile
     * @return ScanFormFileZoneTokens
     */
    public function setIdScanFormFile($idScanFormFile)
    {
        $this->idScanFormFile = $idScanFormFile;
    
        return $this;
    }

    /**
     * Get idScanFormFile
     *
     * @return integer 
     */
    public function getIdScanFormFile()
    {
        return $this->idScanFormFile;
    }

    /**
     * Set idScanFormTemplateZone
     *
     * @param integer $idScanFormTemplateZone
     * @return ScanFormFileZoneTokens
     */
    public function setIdScanFormTemplateZone($idScanFormTemplateZone)
    {
        $this->idScanFormTemplateZone = $idScanFormTemplateZone;
    
        return $this;
    }

    /**
     * Get idScanFormTemplateZone
     *
     * @return integer 
     */
    public function getIdScanFormTemplateZone()
    {
        return $this->idScanFormTemplateZone;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ScanFormFileZoneTokens
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return ScanFormFileZoneTokens
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }
}