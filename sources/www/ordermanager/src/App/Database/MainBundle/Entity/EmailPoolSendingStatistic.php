<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmailPoolSendingStatistic
 *
 * @ORM\Table(name="email_pool_sending_statistic")
 * @ORM\Entity
 */
class EmailPoolSendingStatistic
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $time
     *
     * @ORM\Column(name="time", type="datetime", nullable=true)
     */
    private $time;

    /**
     * @var integer $sent
     *
     * @ORM\Column(name="sent", type="integer", nullable=true)
     */
    private $sent;

    /**
     * @var integer $opens
     *
     * @ORM\Column(name="opens", type="integer", nullable=true)
     */
    private $opens;

    /**
     * @var integer $clicks
     *
     * @ORM\Column(name="clicks", type="integer", nullable=true)
     */
    private $clicks;

    /**
     * @var integer $hardBounces
     *
     * @ORM\Column(name="hard_bounces", type="integer", nullable=true)
     */
    private $hardBounces;

    /**
     * @var integer $softBounces
     *
     * @ORM\Column(name="soft_bounces", type="integer", nullable=true)
     */
    private $softBounces;

    /**
     * @var integer $rejects
     *
     * @ORM\Column(name="rejects", type="integer", nullable=true)
     */
    private $rejects;

    /**
     * @var integer $complaints
     *
     * @ORM\Column(name="complaints", type="integer", nullable=true)
     */
    private $complaints;

    /**
     * @var integer $unsubs
     *
     * @ORM\Column(name="unsubs", type="integer", nullable=true)
     */
    private $unsubs;

    /**
     * @var integer $uniqueOpens
     *
     * @ORM\Column(name="unique_opens", type="integer", nullable=true)
     */
    private $uniqueOpens;

    /**
     * @var integer $uniqueClicks
     *
     * @ORM\Column(name="unique_clicks", type="integer", nullable=true)
     */
    private $uniqueClicks;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return EmailPoolSendingStatistic
     */
    public function setTime($time)
    {
        $this->time = $time;
    
        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set sent
     *
     * @param integer $sent
     * @return EmailPoolSendingStatistic
     */
    public function setSent($sent)
    {
        $this->sent = $sent;
    
        return $this;
    }

    /**
     * Get sent
     *
     * @return integer 
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set opens
     *
     * @param integer $opens
     * @return EmailPoolSendingStatistic
     */
    public function setOpens($opens)
    {
        $this->opens = $opens;
    
        return $this;
    }

    /**
     * Get opens
     *
     * @return integer 
     */
    public function getOpens()
    {
        return $this->opens;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     * @return EmailPoolSendingStatistic
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
    
        return $this;
    }

    /**
     * Get clicks
     *
     * @return integer 
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set hardBounces
     *
     * @param integer $hardBounces
     * @return EmailPoolSendingStatistic
     */
    public function setHardBounces($hardBounces)
    {
        $this->hardBounces = $hardBounces;
    
        return $this;
    }

    /**
     * Get hardBounces
     *
     * @return integer 
     */
    public function getHardBounces()
    {
        return $this->hardBounces;
    }

    /**
     * Set softBounces
     *
     * @param integer $softBounces
     * @return EmailPoolSendingStatistic
     */
    public function setSoftBounces($softBounces)
    {
        $this->softBounces = $softBounces;
    
        return $this;
    }

    /**
     * Get softBounces
     *
     * @return integer 
     */
    public function getSoftBounces()
    {
        return $this->softBounces;
    }

    /**
     * Set rejects
     *
     * @param integer $rejects
     * @return EmailPoolSendingStatistic
     */
    public function setRejects($rejects)
    {
        $this->rejects = $rejects;
    
        return $this;
    }

    /**
     * Get rejects
     *
     * @return integer 
     */
    public function getRejects()
    {
        return $this->rejects;
    }

    /**
     * Set complaints
     *
     * @param integer $complaints
     * @return EmailPoolSendingStatistic
     */
    public function setComplaints($complaints)
    {
        $this->complaints = $complaints;
    
        return $this;
    }

    /**
     * Get complaints
     *
     * @return integer 
     */
    public function getComplaints()
    {
        return $this->complaints;
    }

    /**
     * Set unsubs
     *
     * @param integer $unsubs
     * @return EmailPoolSendingStatistic
     */
    public function setUnsubs($unsubs)
    {
        $this->unsubs = $unsubs;
    
        return $this;
    }

    /**
     * Get unsubs
     *
     * @return integer 
     */
    public function getUnsubs()
    {
        return $this->unsubs;
    }

    /**
     * Set uniqueOpens
     *
     * @param integer $uniqueOpens
     * @return EmailPoolSendingStatistic
     */
    public function setUniqueOpens($uniqueOpens)
    {
        $this->uniqueOpens = $uniqueOpens;
    
        return $this;
    }

    /**
     * Get uniqueOpens
     *
     * @return integer 
     */
    public function getUniqueOpens()
    {
        return $this->uniqueOpens;
    }

    /**
     * Set uniqueClicks
     *
     * @param integer $uniqueClicks
     * @return EmailPoolSendingStatistic
     */
    public function setUniqueClicks($uniqueClicks)
    {
        $this->uniqueClicks = $uniqueClicks;
    
        return $this;
    }

    /**
     * Get uniqueClicks
     *
     * @return integer 
     */
    public function getUniqueClicks()
    {
        return $this->uniqueClicks;
    }
}