<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\GenoPelletEntryAdditives
 *
 * @ORM\Table(name="geno_pellet_entry_additives")
 * @ORM\Entity
 */
class GenoPelletEntryAdditives
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idPelletEntry
     *
     * @ORM\Column(name="id_pellet_entry", type="integer", nullable=true)
     */
    private $idPelletEntry;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var float $weight
     *
     * @ORM\Column(name="weight", type="float", nullable=true)
     */
    private $weight;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPelletEntry
     *
     * @param integer $idPelletEntry
     * @return GenoPelletEntryAdditives
     */
    public function setIdPelletEntry($idPelletEntry)
    {
        $this->idPelletEntry = $idPelletEntry;
    
        return $this;
    }

    /**
     * Get idPelletEntry
     *
     * @return integer 
     */
    public function getIdPelletEntry()
    {
        return $this->idPelletEntry;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GenoPelletEntryAdditives
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return GenoPelletEntryAdditives
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}