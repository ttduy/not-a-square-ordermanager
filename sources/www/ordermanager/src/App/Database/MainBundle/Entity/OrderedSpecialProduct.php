<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\OrderedSpecialProduct
 *
 * @ORM\Table(name="ordered_special_product")
 * @ORM\Entity
 */
class OrderedSpecialProduct
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCustomer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=true)
     */
    private $idCustomer;

    /**
     * @var integer $idSpecialProduct
     *
     * @ORM\Column(name="id_special_product", type="integer", nullable=true)
     */
    private $idSpecialProduct;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     * @return OrderedSpecialProduct
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    
        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer 
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set idSpecialProduct
     *
     * @param integer $idSpecialProduct
     * @return OrderedSpecialProduct
     */
    public function setIdSpecialProduct($idSpecialProduct)
    {
        $this->idSpecialProduct = $idSpecialProduct;
    
        return $this;
    }

    /**
     * Get idSpecialProduct
     *
     * @return integer 
     */
    public function getIdSpecialProduct()
    {
        return $this->idSpecialProduct;
    }
}