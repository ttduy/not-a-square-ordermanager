<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\BillingPaymentAcquisiteurEntry
 *
 * @ORM\Table(name="billing_payment_acquisiteur_entry")
 * @ORM\Entity
 */
class BillingPaymentAcquisiteurEntry
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idBillingPaymentAcquisiteur
     *
     * @ORM\Column(name="id_billing_payment_acquisiteur", type="integer", nullable=true)
     */
    private $idBillingPaymentAcquisiteur;

    /**
     * @var string $entry
     *
     * @ORM\Column(name="entry", type="string", length=255, nullable=true)
     */
    private $entry;

    /**
     * @var float $amount
     *
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @var string $entryId
     *
     * @ORM\Column(name="entry_id", type="string", length=50, nullable=true)
     */
    private $entryId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBillingPaymentAcquisiteur
     *
     * @param integer $idBillingPaymentAcquisiteur
     * @return BillingPaymentAcquisiteurEntry
     */
    public function setIdBillingPaymentAcquisiteur($idBillingPaymentAcquisiteur)
    {
        $this->idBillingPaymentAcquisiteur = $idBillingPaymentAcquisiteur;
    
        return $this;
    }

    /**
     * Get idBillingPaymentAcquisiteur
     *
     * @return integer 
     */
    public function getIdBillingPaymentAcquisiteur()
    {
        return $this->idBillingPaymentAcquisiteur;
    }

    /**
     * Set entry
     *
     * @param string $entry
     * @return BillingPaymentAcquisiteurEntry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    
        return $this;
    }

    /**
     * Get entry
     *
     * @return string 
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return BillingPaymentAcquisiteurEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set entryId
     *
     * @param string $entryId
     * @return BillingPaymentAcquisiteurEntry
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    
        return $this;
    }

    /**
     * Get entryId
     *
     * @return string 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }
}