<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Materials
 *
 * @ORM\Table(name="materials")
 * @ORM\Entity
 */
class Materials
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $novodescription
     *
     * @ORM\Column(name="NovoDescription", type="string", length=255, nullable=true)
     */
    private $novodescription;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="Description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer $materialtypeid
     *
     * @ORM\Column(name="MaterialTypeId", type="integer", nullable=true)
     */
    private $materialtypeid;

    /**
     * @var string $supplier
     *
     * @ORM\Column(name="Supplier", type="string", length=255, nullable=true)
     */
    private $supplier;

    /**
     * @var float $price
     *
     * @ORM\Column(name="Price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float $unitsperprice
     *
     * @ORM\Column(name="UnitsPerPrice", type="float", nullable=true)
     */
    private $unitsperprice;

    /**
     * @var string $unitdescription
     *
     * @ORM\Column(name="UnitDescription", type="string", length=255, nullable=true)
     */
    private $unitdescription;

    /**
     * @var float $priceperunit
     *
     * @ORM\Column(name="PricePerUnit", type="float", nullable=true)
     */
    private $priceperunit;

    /**
     * @var string $articlenumber
     *
     * @ORM\Column(name="ArticleNumber", type="string", length=255, nullable=true)
     */
    private $articlenumber;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \DateTime $expiryDate
     *
     * @ORM\Column(name="expiry_date", type="date", nullable=true)
     */
    private $expiryDate;

    /**
     * @var integer $expiryDateWarningThreshold
     *
     * @ORM\Column(name="expiry_date_warning_threshold", type="integer", nullable=true)
     */
    private $expiryDateWarningThreshold;

    /**
     * @var integer $warningThreshold
     *
     * @ORM\Column(name="warning_threshold", type="integer", nullable=true)
     */
    private $warningThreshold;

    /**
     * @var string $extraNotes
     *
     * @ORM\Column(name="extra_notes", type="text", nullable=true)
     */
    private $extraNotes;

    /**
     * @var string $fileAttachment
     *
     * @ORM\Column(name="file_attachment", type="text", nullable=true)
     */
    private $fileAttachment;

    /**
     * @var string $attachmentKey
     *
     * @ORM\Column(name="attachment_key", type="string", length=255, nullable=true)
     */
    private $attachmentKey;

    /**
     * @var integer $stockStatus
     *
     * @ORM\Column(name="stock_status", type="integer", nullable=false)
     */
    private $stockStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set novodescription
     *
     * @param string $novodescription
     * @return Materials
     */
    public function setNovodescription($novodescription)
    {
        $this->novodescription = $novodescription;
    
        return $this;
    }

    /**
     * Get novodescription
     *
     * @return string 
     */
    public function getNovodescription()
    {
        return $this->novodescription;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Materials
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Materials
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set materialtypeid
     *
     * @param integer $materialtypeid
     * @return Materials
     */
    public function setMaterialtypeid($materialtypeid)
    {
        $this->materialtypeid = $materialtypeid;
    
        return $this;
    }

    /**
     * Get materialtypeid
     *
     * @return integer 
     */
    public function getMaterialtypeid()
    {
        return $this->materialtypeid;
    }

    /**
     * Set supplier
     *
     * @param string $supplier
     * @return Materials
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    
        return $this;
    }

    /**
     * Get supplier
     *
     * @return string 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Materials
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set unitsperprice
     *
     * @param float $unitsperprice
     * @return Materials
     */
    public function setUnitsperprice($unitsperprice)
    {
        $this->unitsperprice = $unitsperprice;
    
        return $this;
    }

    /**
     * Get unitsperprice
     *
     * @return float 
     */
    public function getUnitsperprice()
    {
        return $this->unitsperprice;
    }

    /**
     * Set unitdescription
     *
     * @param string $unitdescription
     * @return Materials
     */
    public function setUnitdescription($unitdescription)
    {
        $this->unitdescription = $unitdescription;
    
        return $this;
    }

    /**
     * Get unitdescription
     *
     * @return string 
     */
    public function getUnitdescription()
    {
        return $this->unitdescription;
    }

    /**
     * Set priceperunit
     *
     * @param float $priceperunit
     * @return Materials
     */
    public function setPriceperunit($priceperunit)
    {
        $this->priceperunit = $priceperunit;
    
        return $this;
    }

    /**
     * Get priceperunit
     *
     * @return float 
     */
    public function getPriceperunit()
    {
        return $this->priceperunit;
    }

    /**
     * Set articlenumber
     *
     * @param string $articlenumber
     * @return Materials
     */
    public function setArticlenumber($articlenumber)
    {
        $this->articlenumber = $articlenumber;
    
        return $this;
    }

    /**
     * Get articlenumber
     *
     * @return string 
     */
    public function getArticlenumber()
    {
        return $this->articlenumber;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Materials
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Materials
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return Materials
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set expiryDateWarningThreshold
     *
     * @param integer $expiryDateWarningThreshold
     * @return Materials
     */
    public function setExpiryDateWarningThreshold($expiryDateWarningThreshold)
    {
        $this->expiryDateWarningThreshold = $expiryDateWarningThreshold;
    
        return $this;
    }

    /**
     * Get expiryDateWarningThreshold
     *
     * @return integer 
     */
    public function getExpiryDateWarningThreshold()
    {
        return $this->expiryDateWarningThreshold;
    }

    /**
     * Set warningThreshold
     *
     * @param integer $warningThreshold
     * @return Materials
     */
    public function setWarningThreshold($warningThreshold)
    {
        $this->warningThreshold = $warningThreshold;
    
        return $this;
    }

    /**
     * Get warningThreshold
     *
     * @return integer 
     */
    public function getWarningThreshold()
    {
        return $this->warningThreshold;
    }

    /**
     * Set extraNotes
     *
     * @param string $extraNotes
     * @return Materials
     */
    public function setExtraNotes($extraNotes)
    {
        $this->extraNotes = $extraNotes;
    
        return $this;
    }

    /**
     * Get extraNotes
     *
     * @return string 
     */
    public function getExtraNotes()
    {
        return $this->extraNotes;
    }

    /**
     * Set fileAttachment
     *
     * @param string $fileAttachment
     * @return Materials
     */
    public function setFileAttachment($fileAttachment)
    {
        $this->fileAttachment = $fileAttachment;
    
        return $this;
    }

    /**
     * Get fileAttachment
     *
     * @return string 
     */
    public function getFileAttachment()
    {
        return $this->fileAttachment;
    }

    /**
     * Set attachmentKey
     *
     * @param string $attachmentKey
     * @return Materials
     */
    public function setAttachmentKey($attachmentKey)
    {
        $this->attachmentKey = $attachmentKey;
    
        return $this;
    }

    /**
     * Get attachmentKey
     *
     * @return string 
     */
    public function getAttachmentKey()
    {
        return $this->attachmentKey;
    }

    /**
     * Set stockStatus
     *
     * @param integer $stockStatus
     * @return Materials
     */
    public function setStockStatus($stockStatus)
    {
        $this->stockStatus = $stockStatus;
    
        return $this;
    }

    /**
     * Get stockStatus
     *
     * @return integer 
     */
    public function getStockStatus()
    {
        return $this->stockStatus;
    }
}