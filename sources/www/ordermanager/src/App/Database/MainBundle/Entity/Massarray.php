<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\Massarray
 *
 * @ORM\Table(name="massarray")
 * @ORM\Entity
 */
class Massarray
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="Name", type="string", length=1024, nullable=true)
     */
    private $name;

    /**
     * @var boolean $isdeleted
     *
     * @ORM\Column(name="IsDeleted", type="boolean", nullable=false)
     */
    private $isdeleted;

    /**
     * @var integer $exportedOrder
     *
     * @ORM\Column(name="exported_order", type="integer", nullable=true)
     */
    private $exportedOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Massarray
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isdeleted
     *
     * @param boolean $isdeleted
     * @return Massarray
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;
    
        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return boolean 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set exportedOrder
     *
     * @param integer $exportedOrder
     * @return Massarray
     */
    public function setExportedOrder($exportedOrder)
    {
        $this->exportedOrder = $exportedOrder;
    
        return $this;
    }

    /**
     * Get exportedOrder
     *
     * @return integer 
     */
    public function getExportedOrder()
    {
        return $this->exportedOrder;
    }
}