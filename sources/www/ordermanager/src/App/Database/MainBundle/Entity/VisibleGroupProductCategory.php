<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\VisibleGroupProductCategory
 *
 * @ORM\Table(name="visible_group_product_category")
 * @ORM\Entity
 */
class VisibleGroupProductCategory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $idVisibleGroupProduct
     *
     * @ORM\Column(name="id_visible_group_product", type="string", length=255, nullable=true)
     */
    private $idVisibleGroupProduct;

    /**
     * @var integer $idCategory
     *
     * @ORM\Column(name="id_category", type="integer", nullable=true)
     */
    private $idCategory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idVisibleGroupProduct
     *
     * @param string $idVisibleGroupProduct
     * @return VisibleGroupProductCategory
     */
    public function setIdVisibleGroupProduct($idVisibleGroupProduct)
    {
        $this->idVisibleGroupProduct = $idVisibleGroupProduct;
    
        return $this;
    }

    /**
     * Get idVisibleGroupProduct
     *
     * @return string 
     */
    public function getIdVisibleGroupProduct()
    {
        return $this->idVisibleGroupProduct;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     * @return VisibleGroupProductCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    
        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer 
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }
}