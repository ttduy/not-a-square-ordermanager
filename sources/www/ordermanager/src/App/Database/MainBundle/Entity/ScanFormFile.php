<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ScanFormFile
 *
 * @ORM\Table(name="scan_form_file")
 * @ORM\Entity
 */
class ScanFormFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $creationDatetime
     *
     * @ORM\Column(name="creation_datetime", type="datetime", nullable=true)
     */
    private $creationDatetime;

    /**
     * @var integer $idOrder
     *
     * @ORM\Column(name="id_order", type="integer", nullable=true)
     */
    private $idOrder;

    /**
     * @var integer $idScanFormTemplate
     *
     * @ORM\Column(name="id_scan_form_template", type="integer", nullable=true)
     */
    private $idScanFormTemplate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $errorMessage
     *
     * @ORM\Column(name="error_message", type="text", nullable=true)
     */
    private $errorMessage;

    /**
     * @var string $extractedRawData
     *
     * @ORM\Column(name="extracted_raw_data", type="text", nullable=true)
     */
    private $extractedRawData;

    /**
     * @var string $orderData
     *
     * @ORM\Column(name="order_data", type="text", nullable=true)
     */
    private $orderData;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDatetime
     *
     * @param \DateTime $creationDatetime
     * @return ScanFormFile
     */
    public function setCreationDatetime($creationDatetime)
    {
        $this->creationDatetime = $creationDatetime;
    
        return $this;
    }

    /**
     * Get creationDatetime
     *
     * @return \DateTime 
     */
    public function getCreationDatetime()
    {
        return $this->creationDatetime;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     * @return ScanFormFile
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    
        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer 
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set idScanFormTemplate
     *
     * @param integer $idScanFormTemplate
     * @return ScanFormFile
     */
    public function setIdScanFormTemplate($idScanFormTemplate)
    {
        $this->idScanFormTemplate = $idScanFormTemplate;
    
        return $this;
    }

    /**
     * Get idScanFormTemplate
     *
     * @return integer 
     */
    public function getIdScanFormTemplate()
    {
        return $this->idScanFormTemplate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ScanFormFile
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     * @return ScanFormFile
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    
        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string 
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set extractedRawData
     *
     * @param string $extractedRawData
     * @return ScanFormFile
     */
    public function setExtractedRawData($extractedRawData)
    {
        $this->extractedRawData = $extractedRawData;
    
        return $this;
    }

    /**
     * Get extractedRawData
     *
     * @return string 
     */
    public function getExtractedRawData()
    {
        return $this->extractedRawData;
    }

    /**
     * Set orderData
     *
     * @param string $orderData
     * @return ScanFormFile
     */
    public function setOrderData($orderData)
    {
        $this->orderData = $orderData;
    
        return $this;
    }

    /**
     * Get orderData
     *
     * @return string 
     */
    public function getOrderData()
    {
        return $this->orderData;
    }
}