<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\WidgetStatisticCalculate
 *
 * @ORM\Table(name="widget_statistic_calculate")
 * @ORM\Entity
 */
class WidgetStatisticCalculate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idWidgetStatistic
     *
     * @ORM\Column(name="id_widget_statistic", type="integer", nullable=true)
     */
    private $idWidgetStatistic;

    /**
     * @var integer $idCalculate
     *
     * @ORM\Column(name="id_calculate", type="integer", nullable=true)
     */
    private $idCalculate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWidgetStatistic
     *
     * @param integer $idWidgetStatistic
     * @return WidgetStatisticCalculate
     */
    public function setIdWidgetStatistic($idWidgetStatistic)
    {
        $this->idWidgetStatistic = $idWidgetStatistic;
    
        return $this;
    }

    /**
     * Get idWidgetStatistic
     *
     * @return integer 
     */
    public function getIdWidgetStatistic()
    {
        return $this->idWidgetStatistic;
    }

    /**
     * Set idCalculate
     *
     * @param integer $idCalculate
     * @return WidgetStatisticCalculate
     */
    public function setIdCalculate($idCalculate)
    {
        $this->idCalculate = $idCalculate;
    
        return $this;
    }

    /**
     * Get idCalculate
     *
     * @return integer 
     */
    public function getIdCalculate()
    {
        return $this->idCalculate;
    }
}