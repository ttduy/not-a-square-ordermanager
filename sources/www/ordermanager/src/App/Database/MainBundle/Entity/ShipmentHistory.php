<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\ShipmentHistory
 *
 * @ORM\Table(name="shipment_history")
 * @ORM\Entity
 */
class ShipmentHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=45, nullable=true)
     */
    private $address;

    /**
     * @var integer $createBy
     *
     * @ORM\Column(name="create_by", type="integer", nullable=true)
     */
    private $createBy;

    /**
     * @var string $status
     *
     * @ORM\Column(name="status", type="string", length=10, nullable=true)
     */
    private $status;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer $changeBy
     *
     * @ORM\Column(name="change_by", type="integer", nullable=true)
     */
    private $changeBy;

    /**
     * @var integer $idShipment
     *
     * @ORM\Column(name="id_shipment", type="integer", nullable=true)
     */
    private $idShipment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ShipmentHistory
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return ShipmentHistory
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createBy
     *
     * @param integer $createBy
     * @return ShipmentHistory
     */
    public function setCreateBy($createBy)
    {
        $this->createBy = $createBy;
    
        return $this;
    }

    /**
     * Get createBy
     *
     * @return integer 
     */
    public function getCreateBy()
    {
        return $this->createBy;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ShipmentHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return ShipmentHistory
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set changeBy
     *
     * @param integer $changeBy
     * @return ShipmentHistory
     */
    public function setChangeBy($changeBy)
    {
        $this->changeBy = $changeBy;
    
        return $this;
    }

    /**
     * Get changeBy
     *
     * @return integer 
     */
    public function getChangeBy()
    {
        return $this->changeBy;
    }

    /**
     * Set idShipment
     *
     * @param integer $idShipment
     * @return ShipmentHistory
     */
    public function setIdShipment($idShipment)
    {
        $this->idShipment = $idShipment;
    
        return $this;
    }

    /**
     * Get idShipment
     *
     * @return integer 
     */
    public function getIdShipment()
    {
        return $this->idShipment;
    }
}