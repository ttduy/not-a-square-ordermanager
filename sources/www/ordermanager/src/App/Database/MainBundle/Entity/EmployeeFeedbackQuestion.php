<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\EmployeeFeedbackQuestion
 *
 * @ORM\Table(name="employee_feedback_question")
 * @ORM\Entity
 */
class EmployeeFeedbackQuestion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $question
     *
     * @ORM\Column(name="question", type="text", nullable=true)
     */
    private $question;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @var string $summary
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;

    /**
     * @var integer $numResponses
     *
     * @ORM\Column(name="num_responses", type="integer", nullable=true)
     */
    private $numResponses;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return EmployeeFeedbackQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return EmployeeFeedbackQuestion
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return EmployeeFeedbackQuestion
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return EmployeeFeedbackQuestion
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    
        return $this;
    }

    /**
     * Get summary
     *
     * @return string 
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set numResponses
     *
     * @param integer $numResponses
     * @return EmployeeFeedbackQuestion
     */
    public function setNumResponses($numResponses)
    {
        $this->numResponses = $numResponses;
    
        return $this;
    }

    /**
     * Get numResponses
     *
     * @return integer 
     */
    public function getNumResponses()
    {
        return $this->numResponses;
    }
}