<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\PaymentNoTaxText
 *
 * @ORM\Table(name="payment_no_tax_text")
 * @ORM\Entity
 */
class PaymentNoTaxText
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $textGermany
     *
     * @ORM\Column(name="text_germany", type="text", nullable=true)
     */
    private $textGermany;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var string $textEnglish
     *
     * @ORM\Column(name="text_english", type="text", nullable=true)
     */
    private $textEnglish;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textGermany
     *
     * @param string $textGermany
     * @return PaymentNoTaxText
     */
    public function setTextGermany($textGermany)
    {
        $this->textGermany = $textGermany;
    
        return $this;
    }

    /**
     * Get textGermany
     *
     * @return string 
     */
    public function getTextGermany()
    {
        return $this->textGermany;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PaymentNoTaxText
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PaymentNoTaxText
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set textEnglish
     *
     * @param string $textEnglish
     * @return PaymentNoTaxText
     */
    public function setTextEnglish($textEnglish)
    {
        $this->textEnglish = $textEnglish;
    
        return $this;
    }

    /**
     * Get textEnglish
     *
     * @return string 
     */
    public function getTextEnglish()
    {
        return $this->textEnglish;
    }
}