<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SupplementTypeLog
 *
 * @ORM\Table(name="supplement_type_log")
 * @ORM\Entity
 */
class SupplementTypeLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idSupplementType
     *
     * @ORM\Column(name="id_supplement_type", type="integer", nullable=true)
     */
    private $idSupplementType;

    /**
     * @var \DateTime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var integer $idWebUser
     *
     * @ORM\Column(name="id_web_user", type="integer", nullable=true)
     */
    private $idWebUser;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string $letter
     *
     * @ORM\Column(name="letter", type="string", length=50, nullable=true)
     */
    private $letter;

    /**
     * @var string $lotInUse
     *
     * @ORM\Column(name="lot_in_use", type="string", length=255, nullable=true)
     */
    private $lotInUse;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSupplementType
     *
     * @param integer $idSupplementType
     * @return SupplementTypeLog
     */
    public function setIdSupplementType($idSupplementType)
    {
        $this->idSupplementType = $idSupplementType;
    
        return $this;
    }

    /**
     * Get idSupplementType
     *
     * @return integer 
     */
    public function getIdSupplementType()
    {
        return $this->idSupplementType;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return SupplementTypeLog
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set idWebUser
     *
     * @param integer $idWebUser
     * @return SupplementTypeLog
     */
    public function setIdWebUser($idWebUser)
    {
        $this->idWebUser = $idWebUser;
    
        return $this;
    }

    /**
     * Get idWebUser
     *
     * @return integer 
     */
    public function getIdWebUser()
    {
        return $this->idWebUser;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SupplementTypeLog
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set letter
     *
     * @param string $letter
     * @return SupplementTypeLog
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;
    
        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set lotInUse
     *
     * @param string $lotInUse
     * @return SupplementTypeLog
     */
    public function setLotInUse($lotInUse)
    {
        $this->lotInUse = $lotInUse;
    
        return $this;
    }

    /**
     * Get lotInUse
     *
     * @return string 
     */
    public function getLotInUse()
    {
        return $this->lotInUse;
    }
}