<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\CollectivePaymentStatus
 *
 * @ORM\Table(name="collective_payment_status")
 * @ORM\Entity
 */
class CollectivePaymentStatus
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $collectivepaymentid
     *
     * @ORM\Column(name="CollectivePaymentId", type="integer", nullable=false)
     */
    private $collectivepaymentid;

    /**
     * @var \DateTime $statusdate
     *
     * @ORM\Column(name="StatusDate", type="date", nullable=true)
     */
    private $statusdate;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="Status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $notes
     *
     * @ORM\Column(name="Notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var integer $sortorder
     *
     * @ORM\Column(name="SortOrder", type="integer", nullable=true)
     */
    private $sortorder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectivepaymentid
     *
     * @param integer $collectivepaymentid
     * @return CollectivePaymentStatus
     */
    public function setCollectivepaymentid($collectivepaymentid)
    {
        $this->collectivepaymentid = $collectivepaymentid;
    
        return $this;
    }

    /**
     * Get collectivepaymentid
     *
     * @return integer 
     */
    public function getCollectivepaymentid()
    {
        return $this->collectivepaymentid;
    }

    /**
     * Set statusdate
     *
     * @param \DateTime $statusdate
     * @return CollectivePaymentStatus
     */
    public function setStatusdate($statusdate)
    {
        $this->statusdate = $statusdate;
    
        return $this;
    }

    /**
     * Get statusdate
     *
     * @return \DateTime 
     */
    public function getStatusdate()
    {
        return $this->statusdate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CollectivePaymentStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CollectivePaymentStatus
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    
        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     * @return CollectivePaymentStatus
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    
        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer 
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }
}