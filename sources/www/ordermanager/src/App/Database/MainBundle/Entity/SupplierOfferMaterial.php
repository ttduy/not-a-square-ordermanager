<?php

namespace App\Database\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Database\MainBundle\Entity\SupplierOfferMaterial
 *
 * @ORM\Table(name="supplier_offer_material")
 * @ORM\Entity
 */
class SupplierOfferMaterial
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idSupplierOffer
     *
     * @ORM\Column(name="id_supplier_offer", type="integer", nullable=true)
     */
    private $idSupplierOffer;

    /**
     * @var integer $idMaterial
     *
     * @ORM\Column(name="id_material", type="integer", nullable=true)
     */
    private $idMaterial;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSupplierOffer
     *
     * @param integer $idSupplierOffer
     * @return SupplierOfferMaterial
     */
    public function setIdSupplierOffer($idSupplierOffer)
    {
        $this->idSupplierOffer = $idSupplierOffer;
    
        return $this;
    }

    /**
     * Get idSupplierOffer
     *
     * @return integer 
     */
    public function getIdSupplierOffer()
    {
        return $this->idSupplierOffer;
    }

    /**
     * Set idMaterial
     *
     * @param integer $idMaterial
     * @return SupplierOfferMaterial
     */
    public function setIdMaterial($idMaterial)
    {
        $this->idMaterial = $idMaterial;
    
        return $this;
    }

    /**
     * Get idMaterial
     *
     * @return integer 
     */
    public function getIdMaterial()
    {
        return $this->idMaterial;
    }
}