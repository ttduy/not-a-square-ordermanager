<?php
namespace App\AdminBundle\Module;

class User extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'User',
            'title'         => 'User'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Users',
            'create'        => 'Add new user'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'User',
            'reader'   => 'app_admin.user.business.grid_reader',
            'filter'   => 'app_admin.user.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'app_admin.user.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit user', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'app_admin.user.business.edit_handler'
        ));

        // EDIT PASSWORD
        $this->addFeature('edit_pwd', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'EditPwdForm',
                'handler'       => 'app_admin.user.business.edit_pwd_handler'
            )
        ));
        $this->setHookOptions('edit_pwd', 'mainTabs', array(            
            'extra'         => array(
                'edit_pwd' => array('title' => 'Edit password', 'link' => '#')
            ),
            'selected' => 'edit_pwd'
        ));
        $this->setTemplate('edit_pwd.edit', 'AppAdminBundle:CRUD:form.html.twig'); 
    }
}
