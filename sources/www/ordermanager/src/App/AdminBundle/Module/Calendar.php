<?php
namespace App\AdminBundle\Module;

class Calendar extends Base {   
    public function load() { 
        $this->addHook('header', array(
            'class'           => 'app_admin.hook.header',
            'point'           => 'hook_before',
            'features'        => '*'  
        ));
        $this->addHook('mainTabs', array(
            'class'           => 'app_admin.hook.main_tabs2',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'title' => 'Calendar'
            )
        ));   

        // CALENDAR
        $this->addFeature('calendar', array(
            'class'           => 'app_admin.feature.calendar'            
        ));        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Calendar',
            'title'         => 'Calendar'
        ));
        $this->setTemplate('calendar.list', 'AppAdminBundle:Calendar:list.html.twig');
    }  
}
