<?php
namespace App\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Base extends AbstractModule {   
    public function load() {    
        $this->addHook('header', array(
            'class'           => 'app_admin.hook.header',
            'point'           => 'hook_before',
            'features'        => '*'  
        ));
        $this->addHook('mainTabs', array(
            'class'           => 'app_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
    }
}
