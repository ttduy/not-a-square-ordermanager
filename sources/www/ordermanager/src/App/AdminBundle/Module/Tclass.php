<?php
namespace App\AdminBundle\Module;

class Tclass extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Class',
            'title'         => 'Class'
        ));

        $this->removeHook('mainTabs');

        $this->addHook('mainTabs2', array(
            'class'           => 'app_admin.hook.main_tabs_generic',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'content' => array(
                    'list'   => array('title' => 'Classes', 'link' => $this->getUrl('grid', 'list')),
                    'create' => array('title' => 'Add new class', 'link' => $this->getUrl('create', 'create')),
                    'schedule' => array('title' => 'Class schedule', 'link' => '#') 
                )
            )
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Room',
            'reader'   => 'app_admin.tclass.business.grid_reader'
        ));
        $this->setHookOptions('grid', 'mainTabs2', array(
            'selected' => 'list'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'app_admin.tclass.business.create_handler'
        ));        
        $this->setHookOptions('create', 'mainTabs2', array(
            'selected' => 'create'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs2', array(
            'content' => array(
                'list'   => array('title' => 'Classes', 'link' => $this->getUrl('grid', 'list')),
                'create' => array('title' => 'Add new class', 'link' => $this->getUrl('create', 'create')),
                'schedule' => array('title' => 'Class schedule', 'link' => '#') ,
                'edit' => array('title' => 'Edit class', 'link' => '#'),
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'app_admin.tclass.business.edit_handler'
        ));
    }
}
