<?php
namespace App\AdminBundle\Module;

class Dashboard extends Base {   
    public function load() { 
        $this->addHook('header', array(
            'class'           => 'app_admin.hook.header',
            'point'           => 'hook_before',
            'features'        => '*'  
        ));
        $this->addHook('mainTabs', array(
            'class'           => 'app_admin.hook.main_tabs2',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'title' => 'Dashboard'
            )
        ));   

        // DASHBOARD
        $this->addFeature('dashboard', array(
            'class'           => 'app_admin.feature.dashboard'            
        ));        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Dashboard',
            'title'         => 'Dashboard'
        ));
        $this->setTemplate('dashboard.list', 'AppAdminBundle:Dashboard:list.html.twig');
    }  
}
