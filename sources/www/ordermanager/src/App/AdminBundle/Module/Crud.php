<?php
namespace App\AdminBundle\Module;

class Crud extends Base { 
    public function load() {
        parent::load();

        // GRID
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object'
            )
        ));        
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'AppAdminBundle:CRUD:list.html.twig');

        // CREATE
        $this->addFeature('create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm'
            )
        ));
        $this->setHookOptions('create', 'mainTabs', array(
            'selected' => 'create'
        ));
        $this->setTemplate('create.create', 'AppAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm'
            )
        ));
        $this->setHookOptions('edit', 'mainTabs', array(            
            'selected' => 'edit'
        ));
        $this->setTemplate('edit.edit', 'AppAdminBundle:CRUD:form.html.twig');        
    }
}
