<?php
namespace App\AdminBundle\Module;

class Order extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Orders',
            'create'        => 'Add new order'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order',
            'reader'   => 'app_admin.order.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'app_admin.order.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit order', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'app_admin.order.business.edit_handler'
        ));
    }
}
