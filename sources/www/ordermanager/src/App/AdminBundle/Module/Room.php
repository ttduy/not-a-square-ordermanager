<?php
namespace App\AdminBundle\Module;

class Room extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Room',
            'title'         => 'Room'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Rooms',
            'create'        => 'Add new room'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Room',
            'reader'   => 'app_admin.room.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'app_admin.room.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit room', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'app_admin.room.business.edit_handler'
        ));
    }
}
