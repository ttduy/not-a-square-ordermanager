<?php
namespace App\AdminBundle\Module;

class Store extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Store',
            'title'         => 'Store'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Stores',
            'create'        => 'Add new store'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Store',
            'reader'   => 'app_admin.store.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'app_admin.store.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit store', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'app_admin.store.business.edit_handler'
        ));
    }
}
