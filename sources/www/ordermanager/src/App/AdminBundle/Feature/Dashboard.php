<?php
namespace App\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Dashboard extends AbstractFeature {
    public function getActions() { return array('create'); }
    public function getDefaultOptions() {
        return array();
    }

    public function handleList($controller, &$data, $options = array()) {
        
    }
}
