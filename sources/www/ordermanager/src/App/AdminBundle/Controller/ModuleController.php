<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleController extends Controller
{   
    public function handleAction($module = null, $feature = null, $action = null) {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('app_admin', $module);
        return $module->handle($this, $feature, $action);
    }
}
