<?php
namespace App\AdminBundle\Helper;

class ButtonListBuilder {    
    public $buttons = array();

    public function addButton($title, $link, $imageCode = '') {
        $this->buttons[] = '<a href="'.$link.'">'.$title.'</a>';
    }

    public function getHtml() {
        return implode('&nbsp;&nbsp;', $this->buttons);
    }
}