<?php
namespace App\AdminBundle\Business\Order;

use App\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:RepairOrder', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->idStore = $entity->getIdStore();
        $model->item = $entity->getItem();
        $model->description = $entity->getDescription();
        $model->customerName = $entity->getCustomerName();
        $model->customerPhoneNumber = $entity->getCustomerPhoneNumber();
        $model->status = $entity->getStatus();
        $model->idReceptionist = $entity->getIdReceptionist();
        $model->idRepairer = $entity->getIdRepairer();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idStore', 'choice', array(
            'label'    => 'Store',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Order_Store')
        ));
        $builder->add('customerName', 'text', array(
            'label'    => 'Customer Name',
            'required' => false
        ));
        $builder->add('customerPhoneNumber', 'text', array(
            'label'    => 'Customer Phone Number',
            'required' => false
        ));

        $builder->add('item', 'text', array(
            'label'    => 'Item',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false
        ));
        
        $builder->add('idReceptionist', 'choice', array(
            'label'    => 'Receptionist',
            'required' => false,
            'choices'  => $mapping->getMapping('AppAdmin_Order_User')
        ));
        $builder->add('idRepairer', 'choice', array(
            'label'    => 'Repairer assigned',
            'required' => false,
            'choices'  => $mapping->getMapping('AppAdmin_Order_User')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Order_Status')
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setItem($model->item);
        $this->entity->setDescription($model->description);
        $this->entity->setCustomerName($model->customerName);
        $this->entity->setCustomerPhoneNumber($model->customerPhoneNumber);
        $this->entity->setStatus($model->status);
        $this->entity->setIdStore($model->idStore);
        $this->entity->setIdReceptionist($model->idReceptionist);
        $this->entity->setIdRepairer($model->idRepairer);
        
        parent::onSuccess();
    }
}
