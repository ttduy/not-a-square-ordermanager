<?php
namespace App\AdminBundle\Business\Order;

use App\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('id', 'idStore', 'idReceptionist', 'idRepairer', 'item', 'customerName', 'customerPhoneNumber', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Order ID',     'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Store',        'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Receptionist', 'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Repairer',     'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Item',         'width' => '11%', 'sortable' => 'false'),
            array('title' => 'Customer Name','width' => '11%', 'sortable' => 'false'),
            array('title' => 'Phone Number', 'width' => '11%', 'sortable' => 'false'),
            array('title' => 'Status',       'width' => '11%', 'sortable' => 'false'),
            array('title' => 'Action',       'width' => '11%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_OrderGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:RepairOrder', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('app_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addButton('Edit', $mgr->getUrl('app_admin', 'order', 'edit', 'edit', array('id' => $row->getId())));

        return $builder->getHtml();
    }
}
