<?php 
namespace App\AdminBundle\Business\Order;

class Constant {    
    const ORDER_STATUS_PENDING = 1;
    const ORDER_STATUS_INPROGRESS = 2;
    const ORDER_STATUS_FINISHED = 3;

    public static function getOrderStatus() {
        return array(
            self::ORDER_STATUS_PENDING     => 'Pending',
            self::ORDER_STATUS_INPROGRESS  => 'In-Progress',
            self::ORDER_STATUS_FINISHED    => 'Finished',
        );
    }
}