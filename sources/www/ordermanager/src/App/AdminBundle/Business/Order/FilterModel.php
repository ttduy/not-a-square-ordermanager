<?php
namespace App\AdminBundle\Business\Order;

class FilterModel {
    public $item;
    public $customerName;
    public $customerPhoneNumber;
    public $status;
    public $idStore;
    public $idReceptionist;
    public $idRepairer;
}