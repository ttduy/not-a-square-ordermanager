<?php
namespace App\AdminBundle\Business\Order;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $item;
    public $description;
    public $customerName;
    public $customerPhoneNumber;
    public $status;
    public $idStore;
    public $idReceptionist;
    public $idRepairer;
}
