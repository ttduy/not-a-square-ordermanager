<?php
namespace App\AdminBundle\Business\Order;

use App\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->status = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('item', 'choice', array(
            'label'       => 'Item',
            'required'    => false,
        );
        $builder->add('customerName', 'choice', array(
            'label'       => 'Customer Name',
            'required'    => false,
        );
        $builder->add('customerPhoneNumber', 'choice', array(
            'label'       => 'Customer Phone Number',
            'required'    => false,
        );

        $builder->add('status', 'choice', array(
            'label'       => 'Status',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_Order_Status'),
            'empty_value' => false
        ));
        $builder->add('idStore', 'choice', array(
            'label'       => 'Store',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_Order_Store'),
            'empty_value' => false
        ));
        $builder->add('idReceptionist', 'choice', array(
            'label'       => 'Receptionist',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_Order_User'),
            'empty_value' => false
        ));
        $builder->add('idRepairer', 'choice', array(
            'label'       => 'Repairer',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_Order_User'),
            'empty_value' => false
        ));
        
        return $builder;
    }
}