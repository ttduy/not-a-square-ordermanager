<?php
namespace App\AdminBundle\Business\Order;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idStore', 'choice', array(
            'label'    => 'Store',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Order_Store')
        ));

        $builder->add('customerName', 'text', array(
            'label'    => 'Customer Name',
            'required' => false
        ));
        $builder->add('customerPhoneNumber', 'text', array(
            'label'    => 'Customer Phone Number',
            'required' => false
        ));

        $builder->add('item', 'text', array(
            'label'    => 'Item',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false
        ));
        
        $builder->add('idReceptionist', 'choice', array(
            'label'    => 'Receptionist',
            'required' => false,
            'choices'  => $mapping->getMapping('AppAdmin_Order_User')
        ));
        $builder->add('idRepairer', 'choice', array(
            'label'    => 'Repairer assigned',
            'required' => false,
            'choices'  => $mapping->getMapping('AppAdmin_Order_User')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Order_Status')
        ));
        
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $order = new Entity\RepairOrder();
        $order->setIdStore($model->idStore);
        $order->setCustomerName($model->customerName);
        $order->setCustomerPhoneNumber($model->customerPhoneNumber);
        $order->setItem($model->item);
        $order->setDescription($model->description);
        $order->setIdReceptionist($model->idReceptionist);
        $order->setIdRepairer($model->idRepairer);
        $order->setStatus($model->status);

        $em->persist($order);
        $em->flush();

        parent::onSuccess();
    }
}
