<?php
namespace App\AdminBundle\Business\Template;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        /*
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        */
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $em->persist($entity);
        $em->flush();

        parent::onSuccess();
    }
}
