<?php
namespace App\AdminBundle\Business\User;

use App\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('username', 'email', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Username', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Email',    'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Status',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_UserGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:User', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('app_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addButton('Edit', $mgr->getUrl('app_admin', 'user', 'edit', 'edit', array('id' => $row->getId())));
        $builder->addButton('Edit password', $mgr->getUrl('app_admin', 'user', 'edit_pwd', 'edit', array('id' => $row->getId())));

        return $builder->getHtml();
    }
}
