<?php
namespace App\AdminBundle\Business\Store;

use App\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'location', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',     'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Location', 'width' => '45%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_StoreGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Store', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('app_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addButton('Edit', $mgr->getUrl('app_admin', 'store', 'edit', 'edit', array('id' => $row->getId())));

        return $builder->getHtml();
    }
}
