<?php
namespace App\AdminBundle\Business\Base;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;

abstract class AppEditHandler extends BaseEditHandler {  
    public function getEntityManager() {
        return 'app_main';
    }
}
