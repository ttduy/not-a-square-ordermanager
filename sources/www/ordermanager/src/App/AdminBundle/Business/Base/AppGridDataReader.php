<?php
namespace App\AdminBundle\Business\Base;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;

abstract class AppGridDataReader extends DoctrineGridDataReader {
    public function getEntityManager() {
        return 'app_main';
    }

    public function getModuleManager() {
        return $this->container->get('easy_module.manager');
    }
}
