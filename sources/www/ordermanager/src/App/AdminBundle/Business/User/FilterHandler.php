<?php
namespace App\AdminBundle\Business\User;

use App\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->status = Constant::USER_STATUS_ACTIVE;
        $model->idRole = 0;
        $model->idStore = 0;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('status', 'choice', array(
            'label'       => 'Status',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_User_Status'),
            'empty_value' => false
        ));
        $builder->add('idRole', 'choice', array(
            'label'       => 'Role',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_User_Roles'),
            'empty_value' => false
        ));
        return $builder;
    }
}