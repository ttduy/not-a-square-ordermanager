<?php
namespace App\AdminBundle\Business\User;

use App\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditPwdHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:User', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditPwdModel();  

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('newPassword',            'password', array(
            'label' => 'New password'
        ));
        $builder->add('retypeNewPassword',      'password', array(
            'label' => 'Retype new password'
        ));
        return $builder;
    }

    public function onSuccess() {
        $userManager = $this->container->get('app_user_manager');
        $model = $this->getForm()->getData();

        $this->entity->setPassword($userManager->encodePassword($model->newPassword, $this->entity->getSalt()));
        
        parent::onSuccess();
    }
}
