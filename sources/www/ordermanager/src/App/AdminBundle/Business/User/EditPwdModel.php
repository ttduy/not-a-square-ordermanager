<?php
namespace App\AdminBundle\Business\User;

use Symfony\Component\Validator\ExecutionContext;
class EditPwdModel {
    public $newPassword;
    public $retypeNewPassword;


    public function isRetypePasswordValid(ExecutionContext $context) {
        if (!empty($this->newPassword) && !empty($this->retypeNewPassword)) {
            if (strcmp($this->newPassword, $this->retypeNewPassword) != 0) {
                $context->addViolationAtSubPath('retypeNewPassword', "Retype password doesn't match to the input password.");
            }
        }
    }
}
