<?php
namespace App\AdminBundle\Business\User;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $doctrine;

    public $username;
    public $password;
    public $retypePassword;
    public $email;
    public $status;
    public $idRole;
    public $idStore;

    public function isUniqueUsername(ExecutionContext $context) {
        if (!empty($this->username)) {
            $entityManager = $this->doctrine->getEntityManager();

            $queryBuilder = $entityManager->getRepository('AppDatabaseMainBundle:User')->createQueryBuilder('p');
            $queryBuilder->select('COUNT(p)');
            $queryBuilder->andWhere('p.username = :value')->setParameter('value', $this->username);
            
            $count = $queryBuilder->getQuery()->getSingleScalarResult();
            if ($count != 0) {
                $context->addViolationAtSubPath('username', "'%value%' is already existed'", array('%value%' => $this->username));
            }
        }
    }

    public function isRetypePasswordValid(ExecutionContext $context) {
        if (!empty($this->password) && !empty($this->retypePassword)) {
            if (strcmp($this->password, $this->retypePassword) != 0) {
                $context->addViolationAtSubPath('retypePassword', "Retype password doesn't match to the input password.");
            }
        }
    }
}
