<?php
namespace App\AdminBundle\Business\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use App\AdminBundle\Business;

class UserProvider implements UserProviderInterface
{
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function loadUserByUsername($username)
    {
        $q = $this->container->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:User')
            ->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->andWhere('u.status = '.Business\User\Constants::USER_STATUS_ACTIVE)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            throw new UsernameNotFoundException(sprintf('Unable to find an active admin AcmeUserBundle:User object identified by "%s".', $username), null, 0, $e);
        }

        $roleMap = Business\User\Constants::getUserRoleCode();
        $roleCode = $roleMap[$user->getIdRole()];

        $userSession = new UserSession();
        $userSession->setUser($user);
        $userSession->setRole($roleCode);

        return $userSession;
    }

    public function refreshUser(UserInterface $userSession)
    {
        if (!$userSession instanceof UserSession) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($userSession)));
        }

        return $this->loadUserByUsername($userSession->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'App\AdminBundle\Business\UserSession';
    }
}