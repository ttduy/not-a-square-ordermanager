<?php
namespace App\AdminBundle\Business\User;

class Constant {
    const USER_STATUS_ACTIVE   = 1;
    const USER_STATUS_INACTIVE = 2;

    const USER_ROLE_ADMIN       = 1;
    const USER_ROLE_TUTOR       = 2;
    const USER_ROLE_STUDENT     = 3;

    const USER_ROLE_CODE_ADMIN     = 'ROLE_ADMIN';
    const USER_ROLE_CODE_TUTOR     = 'ROLE_TUTOR';
    const USER_ROLE_CODE_STUDENT   = 'ROLE_STUDENT';

    public static function getUserStatus() {
        return array(
            self::USER_STATUS_ACTIVE   => 'Active',
            self::USER_STATUS_INACTIVE => 'In-Active',
        );
    }

    public static function getUserRoles() {
        return array(
            self::USER_ROLE_ADMIN      => 'Admin',
            self::USER_ROLE_TUTOR      => 'Tutor',
            self::USER_ROLE_STUDENT    => 'Student',        
        );
    }

    public static function getUserRoleCode() {
        return array(
            self::USER_ROLE_ADMIN      => self::USER_ROLE_CODE_ADMIN,
            self::USER_ROLE_TUTOR      => self::USER_ROLE_CODE_TUTOR,
            self::USER_ROLE_STUDENT    => self::USER_ROLE_CODE_STUDENT,   
        );
    }
}