<?php
namespace App\AdminBundle\Business\User;

class EditModel {
    public $item;
    public $description;
    public $customerName;
    public $customerPhoneNumber;
    public $status;
    public $idStore;
    public $idReceptionist;
    public $idRepairer;
}