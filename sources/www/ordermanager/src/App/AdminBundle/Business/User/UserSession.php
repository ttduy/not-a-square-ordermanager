<?php
namespace App\AdminBundle\Business\User;

use Symfony\Component\Security\Core\User\UserInterface;

class UserSession implements UserInterface {
    private $_user;
    private $_role;

    public function setRole($role) { $this->_role = $role; }
    public function setUser($user) { $this->_user = $user; }
    public function getUser() { return $this->_user; }

    function getPassword() {
        return $this->_user->getPassword();
    }

    function getSalt() {
        return $this->_user->getSalt();
    }

    function getUsername() {
        return $this->_user->getUsername();
    }

    function getRoles() {
        return array($this->_role);
    }

    function eraseCredentials() {

    }
    function equals(UserInterface $user) {
        return $this->getUsername() === $user->getUsername();
    }
}