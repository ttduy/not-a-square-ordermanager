<?php
namespace App\AdminBundle\Business\User;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->doctrine = $this->container->get('doctrine');
        $model->status = Constant::USER_STATUS_ACTIVE;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('password', 'password', array(
            'label'    => 'Password',
            'required' => true
        ));
        $builder->add('retypePassword', 'password', array(
            'label'    => 'Retype Password',
            'required' => true
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('status',   'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_User_Status')
        ));
        $builder->add('idRole',   'choice', array(
            'label'    => 'Role',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_User_Roles')
        ));
        $builder->add('idStore',   'choice', array(
            'label'    => 'Store assigned',
            'required' => false,
            'choices'  => $mapping->getMapping('AppAdmin_User_Store')
        ));
    }

    public function onSuccess() {
        $userManager = $this->container->get('app_user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $user = new Entity\User();
        $user->setUsername($model->username);
        $user->setName($model->name);
        $user->setSalt($userManager->generateSalt());
        $user->setPassword($userManager->encodePassword($model->password, $user->getSalt()));
        $user->setEmail($model->email);
        $user->setStatus($model->status);
        $user->setIdRole($model->idRole);
        $em->persist($user);
        $em->flush();

        parent::onSuccess();
    }
}
