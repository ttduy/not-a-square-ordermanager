<?php
namespace App\AdminBundle\Business\User;

use App\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:User', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name   = $entity->getName();
        $model->email  = $entity->getEmail();
        $model->status = $entity->getStatus();      
        $model->idRole = $entity->getIdRole();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name',            'text', array(
            'label' => 'Name',
            'required' => false
        ));    
        $builder->add('email',            'text', array(
            'label' => 'Email',
            'required' => false
        ));
        $builder->add('status',           'choice',  array(
            'label'       => 'Status',
            'choices'     => $mapping->getMapping('AppAdmin_User_Status')
        ));
        $builder->add('idRole',   'choice', array(
            'label'    => 'Role',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_User_Roles')
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $this->entity->setEmail($model->email);
        $this->entity->setStatus($model->status);
        $this->entity->setIdRole($model->idRole);
        $this->entity->setIdStore($model->idStore);
        
        parent::onSuccess();
    }
}
