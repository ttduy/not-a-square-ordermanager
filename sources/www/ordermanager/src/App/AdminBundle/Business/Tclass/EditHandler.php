<?php
namespace App\AdminBundle\Business\Tclass;

use App\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Tclass', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->idScheduleType = $entity->getIdScheduleType();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        
        $builder->add('name',            'text', array(
            'label' => 'Name',
            'required' => true
        ));
        $builder->add('idScheduleType', 'choice', array(
            'label'    => 'Schedule Type',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Class_ScheduleType')
        ));  
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        
        parent::onSuccess();
    }
}
