<?php 
namespace App\AdminBundle\Business\Tclass;

class Constant {
    const SCHEDULE_TYPE_WEEKLY = 1;

    public static function getScheduleTypes() {
        return array(
            self::SCHEDULE_TYPE_WEEKLY => 'Weekly-recurring'
        );
    }
}