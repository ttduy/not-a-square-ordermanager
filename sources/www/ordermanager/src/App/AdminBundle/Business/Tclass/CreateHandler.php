<?php
namespace App\AdminBundle\Business\Tclass;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->idScheduleType = Constant::SCHEDULE_TYPE_WEEKLY;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('idScheduleType', 'choice', array(
            'label'    => 'Schedule Type',
            'required' => true,
            'choices'  => $mapping->getMapping('AppAdmin_Class_ScheduleType')
        ));        
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $tclass = new Entity\Tclass();
        $tclass->setName($model->name);
        $tclass->setIdScheduleType($model->idScheduleType);

        $em->persist($tclass);
        $em->flush();

        parent::onSuccess();
    }
}
