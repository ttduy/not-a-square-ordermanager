<?php
namespace App\AdminBundle\Business\Tclass;

use App\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'idScheduleType', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Schedule Type', 'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_ClassGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Tclass', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('app_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addButton('Edit', $mgr->getUrl('app_admin', 'class', 'edit', 'edit', array('id' => $row->getId())));
        $builder->addButton('View schedules', 'javascript:alert("to be implemented")');

        return $builder->getHtml();
    }
}
