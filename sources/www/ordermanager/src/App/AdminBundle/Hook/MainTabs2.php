<?php
namespace App\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MainTabs2 extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['mainTabs'] = array(
            'content' => array(
                'main' => array('title' => $options['title'], 'link' => '#')
            ),
            'selected' => 'main'
        );
    }
}
