<?php
namespace App\AdminBundle\Hook;


use Easy\ModuleBundle\Module\AbstractHook;

class Header extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');

        $data['pageTitle'] = 'App Admin';
        $data['menu'] = array(
            'content' => array(
                'Dashboard' => array(
                    'link' => $manager->getUrl('app_admin', 'dashboard', 'dashboard', 'list'),
                    'class' => 'dashboard',
                    'title' => 'Dashboard'
                ),
                'Room' => array(
                    'link' => '#',
                    'class' => 'dashboard',
                    'title' => 'Room',
                    'childs' => array(
                        array(
                            'link' => $manager->getUrl('app_admin', 'room', 'grid', 'list'),
                            'title' => 'List all room'
                        ),
                        array(
                            'link' => $manager->getUrl('app_admin', 'room', 'create', 'create'),
                            'title' => 'Add new room'
                        )
                    )
                ),
                'Class' => array(
                    'link' => '#',
                    'class' => 'dashboard',
                    'title' => 'Class',
                    'childs' => array(
                        array(
                            'link' => $manager->getUrl('app_admin', 'class', 'grid', 'list'),
                            'title' => 'List all classes'
                        ),
                        array(
                            'link' => $manager->getUrl('app_admin', 'class', 'create', 'create'),
                            'title' => 'Add new class'
                        ),
                        array(
                            'link' => '#',
                            'title' => 'View class schedule'
                        )
                    )
                ),
                'User' => array(
                    'link' => '#',
                    'class' => 'users',
                    'title' => 'Users',
                    'childs' => array(
                        array(
                            'link' => $manager->getUrl('app_admin', 'user', 'grid', 'list'),
                            'title' => 'List all users'
                        ),
                        array(
                            'link' => $manager->getUrl('app_admin', 'user', 'create', 'create'),
                            'title' => 'Add new user'
                        )
                    )
                )
            ),
            'selected' => $options['menuSelected']
        );
    }
}
