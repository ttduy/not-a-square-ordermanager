<?php
namespace App\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['mainTabs'] = array(
            'content' => array(
                'list' => array('title' => $options['list'], 'link' => $this->getModule()->getUrl('grid', 'list')),
                'create'  => array('title' => $options['create'],  'link' => $this->getModule()->getUrl('create', 'create'))
            ),
            'selected' => $options['selected']
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
    }
}
