<?php
namespace App\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MainTabsGeneric extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['mainTabs'] = array(
            'content' => $options['content'],
            'selected' => $options['selected']
        );
    }
}
