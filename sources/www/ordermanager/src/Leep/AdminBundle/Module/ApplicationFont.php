<?php
namespace Leep\AdminBundle\Module;

class ApplicationFont extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Application Font'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Application fonts',
            'create'        => 'Add new application font'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Application Font',
            'reader'   => 'leep_admin.application_font.business.grid_reader',        
            'filter'   => 'leep_admin.application_font.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.application_font.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit application font', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.application_font.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.application_font.business.delete_handler'
        ));       
    }
}
