<?php
namespace Leep\AdminBundle\Module;

class BodShipmentTracking extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.bod_shipment_tracking.business.grid_reader',
                'filter'   => 'leep_admin.bod_shipment_tracking.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'BOD Shipment Tracking', 'link' => $this->getUrl('grid', 'list')),
                'sync' => array('title' => 'Sync', 'link' => $this->getUrl('sync', 'create')),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');  

        // SYNC
        $this->addFeature('sync', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.bod_shipment_tracking.business.sync_handler',
                'submit_label'  => 'Sync'
            )
        ));
        $this->setHookOptions('sync', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'BOD Shipment Tracking', 'link' => $this->getUrl('grid', 'list')),
                'sync' => array('title' => 'Sync', 'link' => $this->getUrl('sync', 'create')),
            ),
            'selected' => 'sync'
        ));
        $this->setTemplate('sync.create', 'LeepAdminBundle:CRUD:form.html.twig');        

    }
}
