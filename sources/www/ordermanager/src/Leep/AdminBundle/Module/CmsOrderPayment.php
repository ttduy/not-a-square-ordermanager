<?php
namespace Leep\AdminBundle\Module;

class CmsOrderPayment extends Crud {   
    public function load() { 
        parent::load();       

        $id = $this->container->get('request')->get('id', 0);

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.popup_page_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'list'            => 'Payment',
                'create'          => 'Add new payment'
            )
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Payment'
        ));


        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Payments',
            'reader'   => 'leep_admin.cms_order_payment.business.grid_reader'      
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:popup_list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_order_payment.business.create_handler'
        ));     
        $this->setHookOptions('create', 'mainTabs', array(
            'selected' => 'create'
        ));   
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:popup_form.html.twig');


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit payment', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_order_payment.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:popup_form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_order_payment.business.delete_handler'
        ));       
    }
}
