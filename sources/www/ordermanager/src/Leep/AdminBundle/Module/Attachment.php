<?php
namespace Leep\AdminBundle\Module;

class Attachment extends Base {
    public function load() {

        $this->addFeature('attachment', array(
            'class'           => 'leep_admin.feature.customer.attachment',
            'options'         => array(
            )
        ));
        $this->setTemplate('attachment.elfinder', 'LeepAdminBundle:Customer:attachment.html.twig');
        $this->setTemplate('attachment.show', 'LeepAdminBundle:Customer:show_attachment_page.html.twig');
    }

}
