<?php
namespace Leep\AdminBundle\Module;

//CRUD = Create Read Update Delete
class LeadType extends Crud {
	public function load(){
		parent::load();
		
		//ALL
		$this->setHookOptions('*', 'header', array(
			'menuSelected' 	=> 'Lead',
			'title'			=> 'Lead Category'
		));

		$this->setHookOptions('*', 'mainTabs', array(
			'list'			=> 'Lead Category',
			'create'		=> 'Add new Lead Category'
		));

		//LIST
		$this->setFeatureOptions('grid', array(
			'title'			=> 'Lead Category',
			'reader'		=> 'leep_admin.lead_type.business.grid_reader',
		));

		//CREATE
		$this->setFeatureOptions('create', array(
			'handler'		=> 'leep_admin.lead_type.business.create_handler'
		));


		$this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

		//EDIT
		$this->setHookOptions('edit','mainTabs', array(
			'extra'			=> array(
				'edit'		=> array('title' => 'Edit lead Category', 'link' => '#')
			),
			'selected'		=> 'edit'
		));

		$this->setFeatureOptions('edit', array(
			'handler'		=> 'leep_admin.lead_type.business.edit_handler'
		));

		$this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');

		//DELETE
		$this->setFeatureOptions('delete', array(
			'handler'		=> 'leep_admin.lead_type.business.delete_handler'
		));

		// FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.lead_type'
        ));
	}
}
