<?php
namespace Leep\AdminBundle\Module;

class CryosaveSample extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Cryosave',
            'title'         => 'Cryosave sample'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Cryosave samples',
            'create'        => 'Add new sample'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Cryosave sample',
            'reader'   => 'leep_admin.cryosave_sample.business.grid_reader',        
            'filter'   => 'leep_admin.cryosave_sample.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CryosaveSample:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cryosave_sample.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit sample', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cryosave_sample.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cryosave_sample.business.delete_handler'
        ));       
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.cryosave_sample',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
        // ATTACHMENT
        $this->addFeature('result_attachment', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.cryosave_sample.business.result_attachment_handler'
            )
        ));
        $this->setHookOptions('result_attachment', 'mainTabs', array(
            'extra'         => array(
                'result_attachment' => array('title' => 'Result upload', 'link' => '#')
            ),
            'selected' => 'result_attachment'
        ));
        $this->setTemplate('result_attachment.create', 'LeepAdminBundle:CryosaveSample:result_attachment.html.twig');

    }
}
