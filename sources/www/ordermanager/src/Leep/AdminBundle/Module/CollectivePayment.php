<?php
namespace Leep\AdminBundle\Module;

class CollectivePayment extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CollectivePayment',
            'title'         => 'CollectivePayment'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Collective Payment',
            'create'        => 'Add new collective payment'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Collective Payment',
            'reader'   => 'leep_admin.collective_payment.business.grid_reader',        
            'filter'   => 'leep_admin.collective_payment.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective payment', 'link' =>  $this->getUrl('payment_form', 'list'))
            ),
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.collective_payment.business.create_handler'
        ));        


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit collective payment', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.collective_payment.business.edit_handler'
        ));


        // EDIT ACTIVITY
        $this->addFeature('edit_activity', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.collective_payment.business.edit_activity_handler'
            )
        ));
        $this->setHookOptions('edit_activity', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_activity.edit', 'LeepAdminBundle:Customer:activity_form.html.twig');    
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.collective_payment.business.delete_handler'
        ));       
        


        // COLLECTIVE INVOICE FORM

        $this->addHook('payment_form', array(
            'class'           => 'leep_admin.hook.collective_payment_form',
            'point'           => 'hook_after',
            'features'        => 'payment_form payment_form_edit'
        ));
        $this->addFeature('payment_form', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Collective Payment',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.collective_payment_form.business.grid_reader',        
                'filter'   => 'leep_admin.collective_payment_form.business.filter_handler'                
            )
        ));        
        
        $this->setHookOptions('payment_form', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective payment', 'link' =>  $this->getUrl('payment_form', 'list'))
            ),
            'selected'   => 'create'
        ));
        $this->setTemplate('payment_form.list', 'LeepAdminBundle:CollectivePaymentForm:form_list.html.twig');


        // COLLECTIVE INVOICE FORM EDIT
        $this->addFeature('payment_form_edit', array(
            'class'           => 'leep_admin.feature.collective_payment',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Collective Payment',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.collective_payment_form.business.edit_grid_reader',        
                'filter'   => 'leep_admin.collective_payment_form.business.edit_filter_handler'                
            )
        ));        
        $this->setHookOptions('payment_form_edit', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective payment', 'link' =>  $this->getUrl('payment_form', 'list')),
                'edit'   => array('title' => 'Edit collective payment', 'link' => '#')
            ),
            'selected'   => 'edit'
        ));
        $this->setTemplate('payment_form_edit.list', 'LeepAdminBundle:CollectivePaymentForm:form_edit_list.html.twig');

        // COLLECTIVE INVOICE PDF
        $this->setHookOptions('payment_form_pdf', 'mainTabs', array(
            'selected'   => 'edit'
        ));
        $this->addFeature('payment_form_pdf', array(
            'class'           => 'leep_admin.feature.collective_payment_pdf',
            'options'         => array(              
            )
        ));        
        $this->setTemplate('payment_form_pdf.email', 'LeepAdminBundle:CollectivePaymentForm:email.html.twig');
        /*
        // COLLECTIVE INVOICE EMAIL
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.collective_invoice.business.email_handler',
                'submit_label' => 'Send'
            )
        ));
        $this->setHookOptions('email', 'mainTabs', array(
            'selected' => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:CollectiveInvoiceForm:email.html.twig');
        */
    }
}
