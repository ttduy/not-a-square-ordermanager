<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Api extends Base {   
    public function load() {
        parent::load();        

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => ''
        ));
        
        $this->addFeature('report_api', array(
            'class'           => 'leep_admin.feature.report_api',
            'options'         => array(
            )
        ));
    }
}
