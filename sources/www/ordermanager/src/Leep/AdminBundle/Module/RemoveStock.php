<?php
namespace Leep\AdminBundle\Module;

class RemoveStock extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => 'Remove stock'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Search',
            'create'        => 'Add new',
            'disable'       => ['create', 'list']
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.removeStock'
        ));

    }
}
