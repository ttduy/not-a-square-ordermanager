<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class AppUtils extends Base {   
    public function load() {
        parent::load();        

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => ''
        ));
        
        $this->addFeature('app_utils', array(
            'class'           => 'leep_admin.feature.app_utils',
            'options'         => array(
            )
        ));
    }
}
