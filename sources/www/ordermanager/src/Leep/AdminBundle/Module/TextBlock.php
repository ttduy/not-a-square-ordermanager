<?php
namespace Leep\AdminBundle\Module;

class TextBlock extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'TextBlock',
            'title'         => 'TextBlock'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Text Blocks',
            'create'        => 'Add new text block'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Text Block',
            'reader'   => 'leep_admin.text_block.business.grid_reader',
            'filter'   => 'leep_admin.text_block.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:TextBlock:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.text_block.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:TextBlock:create.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit text block', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.text_block.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:TextBlock:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.text_block.business.delete_handler'
        ));   
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.text_block',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
    }
}
