<?php
namespace Leep\AdminBundle\Module;

class Rating extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Rating',
            'title'         => 'Rating'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Rating',
            'create'        => 'Add new Rating'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.rating.business.create_handler'
        ));        

        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.rating.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'  
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Rating:form.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.rating',
            'options'         => array(
            )
        ));
    }
}
