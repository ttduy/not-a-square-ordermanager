<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Developer extends Base {   
    public function load() {
        parent::load();        

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'RussiaMWAProcessor',
            'title'         => 'Russia Mwa Dashboard'
        ));
        
        $this->addFeature('developer', array(
            'class'           => 'leep_admin.feature.developer',
            'options'         => array(
            )
        ));
    }
}
