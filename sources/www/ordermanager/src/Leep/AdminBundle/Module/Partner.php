<?php
namespace Leep\AdminBundle\Module;

class Partner extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Partner',
            'title'         => 'Partner'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Partners',
            'create'        => 'Add new partner'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Partner',
            'reader'   => 'leep_admin.partner.business.grid_reader',
            'filter'   => 'leep_admin.partner.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Partner:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.partner.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Partner:form.html.twig');
        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.partner.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit partner', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.partner.business.edit_handler'
        ));
        $this->addHook('edit_hook', array(
            'class'           => 'leep_admin.partner.business.edit_hook',
            'point'           => 'hook_after',
            'features'        => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Partner:form.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.partner.business.view_handler'
        ));
        $this->setTemplate('view.view', 'LeepAdminBundle:Partner:view.html.twig');

        // EMAIL
        $this->setHookOptions('email', 'mainTabs', array(
            'selected'      => 'partner_email'
        ));
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.partner.business.email_handler',
                'submit_label'  => 'Send'
            )
        ));
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.partner.business.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:Partner:email.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.partner.business.delete_handler'
        ));

        // EDIT COMPLAINTS
        $this->addFeature('edit_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.partner.business.edit_complaint_handler'
            )
        ));
        $this->setHookOptions('edit_complaint', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit complaint', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_complaint.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT FEEDBACKS
        $this->addFeature('edit_feedback', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.partner.business.edit_feedback_handler'
            )
        ));
        $this->setHookOptions('edit_feedback', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_feedback.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // FEATURES
        $this->addFeature('partner', array(
            'class'           => 'leep_admin.feature.partner',
            'options'         => array()
        ));

        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.partner',
            'options'         => array()
        ));
    }
}
