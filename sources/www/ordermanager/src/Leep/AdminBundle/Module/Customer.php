<?php
namespace Leep\AdminBundle\Module;

class Customer extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Orders',
            'create'        => 'Add new order',
            'extra'         => array(
                //'import'        => array('title' => 'Import customer', 'link' => $this->getUrl('customer_import', 'list')),
            )
        ));
        $this->addHook('customer_hook', array(
            'class'           => 'leep_admin.hook.customer',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order',
            'reader'   => 'leep_admin.customer.business.grid_reader',
            'filter'   => 'leep_admin.customer.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Customer:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.customer.business.create_handler',
            'toRedirect'        => true,
            'toCreateFeature'   => 'create'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Customer:form.html.twig');

        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.customer.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                //'import'        => array('title' => 'Import customer', 'link' => $this->getUrl('customer_import', 'list')),
                'edit' => array('title' => 'Edit customer', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.customer.business.edit_handler'
        ));

        $this->addHook('edit_hook_before', array(
            'class'           => 'leep_admin.customer.business.edit_hook_before',
            'point'           => 'hook_before',
            'features'        => 'edit'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:Customer:form.html.twig');

        $this->addHook('edit_hook', array(
            'class'           => 'leep_admin.customer.business.edit_hook',
            'point'           => 'hook_after',
            'features'        => 'edit'
        ));

        // EDIT STATUS
        $this->addFeature('edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.edit_status_handler'
            )
        ));
        $this->setHookOptions('edit_status', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_status.edit', 'LeepAdminBundle:Customer:form_update_status.html.twig');


        // LIST RESULT
        $this->addFeature('grid_result', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'       => 'leep_admin.customer.business.grid_result_reader',
                'filter'        => 'leep_admin.customer.business.filter_result_handler'
            )
        ));

        $this->setHookOptions('grid_result', 'mainTabs', array(
            'content'  => array(),
            'selected' => ''
        ));
        $this->setTemplate('grid_result.list', 'LeepAdminBundle:Customer:list_result.html.twig');


        // EDIT ACTIVITY
        $this->addFeature('edit_activity', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.edit_activity_handler'
            )
        ));
        $this->setHookOptions('edit_activity', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_activity.edit', 'LeepAdminBundle:Customer:activity_form.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.customer.business.view_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.customer.business.delete_handler'
        ));


        // DOUBLE CHECK
        $this->addFeature('double_check', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.double_check_handler',
                'submit_label'  => 'Check'
            )
        ));
        $this->setHookOptions('double_check', 'mainTabs', array(
            'extra'         => array(
                'double_check'  => array('title' => 'Double check customer', 'link' => $this->getUrl('double_check', 'create')),
                //'import'        => array('title' => 'Import customer', 'link' => $this->getUrl('customer_import', 'list')),
            ),
            'selected'      => 'double_check'
        ));
        $this->addHook('double_check_hook', array(
            'class'           => 'leep_admin.customer.business.double_check_hook',
            'point'           => 'hook_after',
            'features'        => 'double_check'
        ));
        $this->setTemplate('double_check.create', 'LeepAdminBundle:Customer:form_double_check.html.twig');

        // ATTACHMENT
        $this->addFeature('attachment', array(
            'class'         => 'leep_admin.feature.customer.attachment',
            'options'       => array(
            )
        ));
        $this->setHookOptions('attachment', 'mainTabs', array(
            'extra'         => array(
                'double_check'  => array('title' => 'Double check customer', 'link' => $this->getUrl('double_check', 'create')),
                //'import'        => array('title' => 'Import customer', 'link' => $this->getUrl('customer_import', 'list')),
                'attachment'    => array('title' => 'Attachment', 'link' => '#')
            ),
            'selected'      => 'attachment'
        ));
        $this->setTemplate('attachment.elfinder', 'LeepAdminBundle:Customer:attachment.html.twig');
        $this->setTemplate('attachment.show', 'LeepAdminBundle:Customer:show_attachment_page.html.twig');

        // IMPORT
        $this->addFeature('customer_import', array(
            'class'         => 'leep_admin.feature.customer_import',
            'options'       => array()
        ));
        $this->setHookOptions('customer_import', 'mainTabs', array(
            'extra'         => array(
                'double_check'  => array('title' => 'Double check customer', 'link' => $this->getUrl('double_check', 'create')),
                //'import'        => array('title' => 'Import customer', 'link' => $this->getUrl('customer_import', 'list')),
            ),
            'selected'      => 'import'
        ));

        $this->setTemplate('customer_import.list', 'LeepAdminBundle:Customer:import_list.html.twig');
        $this->setTemplate('customer_import.elfinder', 'LeepAdminBundle:Customer:attachment.html.twig');
        $this->setTemplate('customer_import.config', 'LeepAdminBundle:Customer:import_config_form.html.twig');

        // EXPORT
        $this->addFeature('customer_export', array(
            'class'         => 'leep_admin.feature.customer_export',
            'options'       => array()
        ));
        $this->setHookOptions('customer_export', 'mainTabs', array(
            'extra'         => array(),
            'selected'      => 'list'
        ));

        // CUSTOMER
        $this->addFeature('customer', array(
            'class'         => 'leep_admin.feature.customer',
            'options'       => array()
        ));
        $this->setHookOptions('customer', 'mainTabs', array(
            'extra'         => array(),
            'selected'      => 'list'
        ));
        $this->setTemplate('customer.showCapture', 'LeepAdminBundle:Customer:show_capture.html.twig');

        // EDIT COMPLAINTS
        $this->addFeature('edit_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.edit_complaint_handler'
            )
        ));
        $this->setHookOptions('edit_complaint', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit complaint', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_complaint.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT FEEDBACKS
        $this->addFeature('edit_feedback', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.edit_feedback_handler'
            )
        ));
        $this->setHookOptions('edit_feedback', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_feedback.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy customer', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.edit', 'LeepAdminBundle:CRUD:form.html.twig');


        // WEBLOGIN SHIPMENT
        $this->addFeature('weblogin_shipment', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.weblogin_shipment_handler',
                'submit_label'  => 'Submit'
            )
        ));
        $this->addHook('weblogin_shipment', array(
            'class'           => 'leep_admin.hook.customer_report_main_tabs',
            'point'           => 'hook_before',
            'features'        => 'weblogin_shipment',
            'options'         => array(
                'selected' => 'weblogin_shipment'
            )
        ));
        $this->addHook('weblogin_shipment_hook', array(
            'class'           => 'leep_admin.customer.business.weblogin_shipment_hook',
            'point'           => 'hook_before',
            'features'        => 'weblogin_shipment'
        ));
        $this->setTemplate('weblogin_shipment.create', 'LeepAdminBundle:Customer:form_weblogin_shipment.html.twig');

        // BULK EDIT SETTING
        $this->addFeature('bulk_edit_setting', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer.business.bulk_edit_setting_handler'
            )
        ));
        $this->setHookOptions('bulk_edit_setting', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Bulk Edit Setting', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->addHook('bulk_process_hook', array(
            'class'           => 'leep_admin.customer.business.bulk_edit_setting_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_setting'
        ));
        $this->setTemplate('bulk_edit_setting.edit', 'LeepAdminBundle:Customer:form_bulk_update_setting.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'     => 'leep_admin.feature.customer'
        ));
    }
}
