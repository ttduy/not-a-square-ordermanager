<?php
namespace Leep\AdminBundle\Module;

class NutriMe extends Crud {   
    public function load() { 
        parent::load();       

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'NutriMe',
            'title'         => 'NutriMe'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'NutriMe',
            'create'        => 'Add new NutriMe'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'NutriMe',
            'reader'   => 'leep_admin.nutri_me.business.grid_reader',
            'filter'   => 'leep_admin.nutri_me.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:NutriMe:list.html.twig');  

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.nutri_me.business.create_handler',
            'toEdit'        => true,
            'toEditFeature' => 'edit'
        ));        
        $this->setHookOptions('create', 'mainTabs', array(
            'selected'      => 'create'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit nutri me', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.nutri_me.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.nutri_me.business.delete_handler',
            'redirectFeature' => 'grid'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.nutri_me'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'      => 'list'
        ));

    }
}
