<?php
namespace Leep\AdminBundle\Module;

class ProductCategoryPrice extends Base {   
    public function load() { 
        parent::load();     

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Price',
            'title'         => 'Product Category Price'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array(
                'list'          => array('title' => 'Category price', 'link' => '#')
            ),
            'selected'          => 'list'
        ));

        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Category Price',
                'reader'   => 'leep_admin.product_category_price.business.grid_reader',
                'filter'   => 'leep_admin.product_category_price.business.filter_handler',
                'filter.visiblity'    => 'show'
            )
        ));        
        $this->setTemplate('grid.list', 'LeepAdminBundle:ProductCategoryPrice:list.html.twig');

        // AJAX UPDATE
        $this->addFeature('ajax_update', array(
            'class'      => 'leep_admin.feature.ajax_update',
            'options'    => array(
                'handlers' => array(
                    'price'       => 'leep_admin.product_category_price.business.ajax_update.price',
                    'dcmargin'    => 'leep_admin.product_category_price.business.ajax_update.price',
                    'partmargin'  => 'leep_admin.product_category_price.business.ajax_update.price',
                )
            )
        ));
    }
}
