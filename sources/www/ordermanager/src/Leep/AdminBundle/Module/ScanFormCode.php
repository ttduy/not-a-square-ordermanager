<?php
namespace Leep\AdminBundle\Module;

class ScanFormCode extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanForm',
            'title'         => 'Code'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.scan_form_code.business.grid_reader',
                'filter'   => 'leep_admin.scan_form_code.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Code', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');   
    }
}
