<?php
namespace Leep\AdminBundle\Module;

class EmailTemplate extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailTemplate',
            'title'         => 'EmailTemplate'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email Templates',
            'create'        => 'Add new email template'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email Template',
            'reader'   => 'leep_admin.email_template.business.grid_reader',
            'filter'   => 'leep_admin.email_template.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.email_template.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:EmailTemplates:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit email template', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:EmailTemplates:form.html.twig');

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.email_template.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.email_template.business.delete_handler'
        ));

        // GENERAL
        $this->addFeature('email_template', array(
            'class'           => 'leep_admin.feature.email_template',
            'options'         => array()
        ));
        $this->setHookOptions('email_template', 'mainTabs', array(
            'extra'         => array(),
            'selected'      => 'edit'
        ));


    }
}
