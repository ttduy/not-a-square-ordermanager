<?php
namespace Leep\AdminBundle\Module;

class CustomerDashboardStatus extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Status',
            'title'         => 'Customer Dashboard Status'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.customer_dashboard_status_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'selected'      => 'customer_dashboard_status'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.customer_dashboard_status.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CustomerDashboardStatus:form.html.twig');
    }
}
