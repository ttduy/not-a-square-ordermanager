<?php
namespace Leep\AdminBundle\Module;

class TodoTask extends Crud {   
    public function load() { 
        parent::load();       
        
        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.todo_task_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'StrategyPlanningTool',
            'title'         => 'Tasks'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Tasks',
            'create'        => 'Add new task'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.todo_task.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:TodoTask:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit task', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.todo_task.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:TodoTask:form.html.twig');
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.todo_task.business.delete_handler'
        ));      

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.todo_task',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
    }
}
