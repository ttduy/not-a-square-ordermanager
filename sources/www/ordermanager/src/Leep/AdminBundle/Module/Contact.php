<?php
namespace Leep\AdminBundle\Module;

class Contact extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => 'Contact'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Contact',
            'create'        => 'Add new',
            'disable'       => ['create']
        ));
        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Contact',
            'filter'   => 'leep_admin.contact.business.filter_handler',
            'reader'   => 'leep_admin.contact.business.grid_reader'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Contact:list.html.twig');


    }
}
