<?php
namespace Leep\AdminBundle\Module;

class CustomerReport extends Crud {   
    public function load() { 
        parent::load();     

        $id = $this->container->get('request')->get('id', 0);

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.customer_report_main_tabs',
            'point'           => 'hook_before',
            'features'        => 'grid create change_status ftp_shipment'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Report'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Reports',
            'reader'   => 'leep_admin.customer_report.business.grid_reader',        
            'filter'   => 'leep_admin.customer_report.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'report'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CustomerReport:list.html.twig');

        // GENERATE        
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.customer_report.business.create_handler'
        ));        
        $this->setHookOptions('create', 'mainTabs', array(
            'selected' => 'generate_report'
        ));
        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.customer_report.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:CustomerReport:create.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.customer_report',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'report'
        ));
        $this->setTemplate('feature.buildAllReport', 'LeepAdminBundle:CustomerReport:build_all_report.html.twig');

        // CHANGE STATUS
        $this->addFeature('change_status', array(
            'class'           => 'easy_crud.feature.create',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_report.business.change_status_create_handler',
                'submit_label'  => 'Submit'
            )
        ));
        $this->setHookOptions('change_status', 'mainTabs', array(
            'selected' => 'change_status'
        ));
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.customer_report.hook.change_status_hook',
            'point'           => 'hook_after',
            'features'        => 'change_status'  
        ));  
        $this->setTemplate('change_status.create', 'LeepAdminBundle:CustomerReport:form_change_status.html.twig');


        // GENERATE        
        $this->addFeature('ftp_shipment', array(
            'class'           => 'easy_crud.feature.create',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_report.business.ftp_shipment_handler',
                'submit_label'  => 'Submit'
            )
        ));
        $this->setHookOptions('ftp_shipment', 'mainTabs', array(
            'selected' => 'shipment_ftp'
        ));
        $this->addHook('ftp_shipment_hook', array(
            'class'           => 'leep_admin.customer_report.business.ftp_shipment_hook',
            'point'           => 'hook_after',
            'features'        => 'ftp_shipment'
        ));
        $this->setTemplate('ftp_shipment.create', 'LeepAdminBundle:CustomerReport:ftp_shipment.html.twig');        
    }
}
