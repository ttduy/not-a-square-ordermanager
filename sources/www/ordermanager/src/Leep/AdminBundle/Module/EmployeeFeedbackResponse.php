<?php
namespace Leep\AdminBundle\Module;

class EmployeeFeedbackResponse extends Crud {   
    public function load() { 
        parent::load();     

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmployeeFeedback',
            'title'         => 'Employee Feedback Response'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.employee_feedback_response',
            'point'           => 'hook_before',
            'features'        => 'grid'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Feedback responses',
            'reader'   => 'leep_admin.employee_feedback_response.business.grid_reader',        
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'employee_feedback_response'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:EmployeeFeedbackResponse:list.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.employee_feedback_response.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:popup_form_no_tab.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.employee_feedback_response.business.delete_handler'
        ));       
    }
}
