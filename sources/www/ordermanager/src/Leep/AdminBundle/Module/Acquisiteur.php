<?php
namespace Leep\AdminBundle\Module;

class Acquisiteur extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Acquisiteur',
            'title'         => 'Acquisiteur'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Acquisiteurs',
            'create'        => 'Add new acquisiteur'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Acquisiteur',
            'reader'   => 'leep_admin.acquisiteur.business.grid_reader',
            'filter'   => 'leep_admin.acquisiteur.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Acquisiteur:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.acquisiteur.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit acquisiteur', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.acquisiteur.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.acquisiteur.business.view_handler'
        ));      
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.acquisiteur.business.delete_handler'
        ));       
    }
}
