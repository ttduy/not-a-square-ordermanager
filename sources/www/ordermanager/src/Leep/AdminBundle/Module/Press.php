<?php
namespace Leep\AdminBundle\Module;

class Press extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Press',
            'title'         => 'Press'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Presses',
            'create'        => 'Add new Press'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Press',
            'reader'   => 'leep_admin.press.business.grid_reader',
            'filter'   => 'leep_admin.press.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.press.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit press', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.press.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.press.business.view_handler'
        ));      
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.press.business.delete_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:Press:form.html.twig');
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Press:form.html.twig');
    }
}
