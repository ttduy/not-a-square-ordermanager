<?php
namespace Leep\AdminBundle\Module;

class TranslatorUser extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'WebUser',
            'title'         => 'Translator User'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Translator Users',
            'create'        => 'Add new translator'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Translator User',
            'reader'   => 'leep_admin.translator_user.business.grid_reader',        
            'filter'   => 'leep_admin.translator_user.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.translator_user.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit translator', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.translator_user.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.translator_user.business.delete_handler'
        ));       
    }
}
