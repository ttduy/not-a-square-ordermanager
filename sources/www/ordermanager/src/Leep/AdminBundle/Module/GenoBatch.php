<?php
namespace Leep\AdminBundle\Module;

class GenoBatch extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoMaterial',
            'title'         => 'GenoMaterial'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Batches',
            'create'        => 'Add new Batch'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Geno Batches',
            'reader'   => 'leep_admin.geno_batch.business.grid_reader',
            'filter'   => 'leep_admin.geno_batch.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:GenoBatch:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_batch.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Batch', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.geno_batch.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_batch.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.geno_batch'
        ));

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'handler' =>    'leep_admin.geno_batch.business.copy_handler',
                'id' =>         'AppForm'
            )
        ));

        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy batch', 'link' => '#')
            ),
            'selected' => 'copy'
        ));

        $this->setTemplate('copy.create', 'LeepAdminBundle:CRUD:form.html.twig');
    }
}
