<?php
namespace Leep\AdminBundle\Module;

class Plate extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Plate',
            'title'         => 'Plate'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Plates',
            'create'        => 'Add new plate'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Plate',
            'reader'   => 'leep_admin.plate.business.grid_reader',
            'filter'   => 'leep_admin.plate.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'toEdit'        => true,
            'toEditFeature' => 'edit',
            'handler'       => 'leep_admin.plate.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:Plate:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit plate', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.plate.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Plate:form.html.twig');
   
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.plate.business.delete_handler'
        ));       


        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.create',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.plate.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy'  => array('title' => 'Copy plate', 'link' => '#'),
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.create', 'LeepAdminBundle:Plate:form_copy.html.twig');

        // FAM VIC MAPPING RULE
        $this->addFeature('fam_vic_mapping_rule', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.plate.business.fam_vic_mapping_rule_edit_handler',
                'submit_label'  => 'Save'
            )
        ));
        $this->setHookOptions('fam_vic_mapping_rule', 'mainTabs', array(
            'extra'         => array(
                'fam_vic_mapping_rule'  => array('title' => 'Edit Fam/Vic Conversion', 'link' => '#'),
            ),
            'selected'      => 'fam_vic_mapping_rule'
        ));
        $this->addHook('fam_vic_mapping_rule_hook', array(
            'class'           => 'leep_admin.plate.business.fam_vic_mapping_rule_edit_hook',
            'point'           => 'hook_after',
            'features'        => 'fam_vic_mapping_rule'  
        )); 
        $this->setTemplate('fam_vic_mapping_rule.edit', 'LeepAdminBundle:Plate:fam_vic_mapping_rule_form.html.twig');

        // Submit Result
        $this->addFeature('submit_raw_result', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.plate.business.submit_raw_result_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('submit_raw_result', 'mainTabs', array(
            'content'         => array(
                'create' => array('title' => 'Submit Raw Result', 'link' => '#')
            ), 
            'selected' => 'create'
        ));
        $this->addHook('submit_raw_result_hook', array(
            'class'           => 'leep_admin.plate.business.submit_raw_result_hook',
            'point'           => 'hook_after',
            'features'        => 'submit_raw_result'  
        ));
        $this->setTemplate('submit_raw_result.create', 'LeepAdminBundle:Plate:submit_raw_result.html.twig');

        // View history Plate Raw Result Submission
        $this->addFeature('view_history_raw_result_submission', array(
            'class'        => 'easy_crud.feature.grid',
            'options'      => array(
                'id'       => 'AppForm',                   
                'title'    => 'Raw Result Submission History',
                'reader'   => 'leep_admin.plate.business.submit_raw_result_history_grid',
                'filter'   => 'leep_admin.plate.business.submit_raw_result_history_filter_handler'
            )
        ));
        $this->setTemplate('view_history_raw_result_submission.list', 'LeepAdminBundle:Plate:plate_raw_result.html.twig');

        // Delete Plate Raw Result
        $this->addFeature('delete_plate_raw_result', array(
            'class'           => 'easy_crud.feature.delete',
            'options'         => array(
                'redirectFeature' => 'view_history_raw_result_submission',
                'handler'       => 'leep_admin.plate.business.delete_plate_raw_result_handler'
            )
        ));

        // Export Result
        $this->addFeature('export_result', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.plate.business.export_result_handler',
                'submit_label'  => 'Export'
            )
        ));
        $this->setHookOptions('export_result', 'mainTabs', array(
            'extra'         => array(
                'export' => array('title' => 'Export Result', 'link' => '#')
            ), 
            'selected' => 'export'
        ));
        $this->addHook('export_result_hook', array(
            'class'           => 'leep_admin.plate.business.export_result_hook',
            'point'           => 'hook_after',
            'features'        => 'export_result'  
        ));
        $this->setTemplate('export_result.edit', 'LeepAdminBundle:Plate:export_result.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.plate',
            'options'         => array(
            )
        ));
    }
}
