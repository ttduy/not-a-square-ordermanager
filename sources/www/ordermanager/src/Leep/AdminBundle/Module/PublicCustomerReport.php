<?php
namespace Leep\AdminBundle\Module;

class PublicCustomerReport extends Base {   
    public function load() { 
        parent::load();     

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Report'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.public_customer_report',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'report'
        ));
    }
}
