<?php
namespace Leep\AdminBundle\Module;

class Pyros extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Pyros',
            'title'         => 'Pyros'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Pyros',
            'create'        => 'Add new pyros'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Pyros',
            'reader'   => 'leep_admin.pyros.business.grid_reader',
            'filter'   => 'leep_admin.pyros.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.Pyros.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit pyros', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.pyros.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.pyros.business.delete_handler'
        ));       
    }
}
