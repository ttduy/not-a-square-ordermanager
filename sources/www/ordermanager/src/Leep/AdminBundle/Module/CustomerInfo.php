<?php
namespace Leep\AdminBundle\Module;

class CustomerInfo extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CustomerInfo',
            'title'         => 'Customer Info'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Customer Infos',
            'create'        => 'Add new customer info'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Customer Info',
            'reader'   => 'leep_admin.customer_info.business.grid_reader',        
            'filter'   => 'leep_admin.customer_info.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CustomerInfo:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.customer_info.business.create_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:CustomerInfo:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit customer info', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.customer_info.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:CustomerInfo:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.customer_info.business.delete_handler'
        ));       

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.customer_info.business.view_handler'
        ));
        $this->setTemplate('view.view', 'LeepAdminBundle:CustomerInfo:view.html.twig');

        // VIEW WEB LOGIN
        $this->setFeatureOptions('view_web_login', array(
            'handler'       => 'leep_admin.customer_info.business.view_web_login_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.customer_info'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'      => 'list'
        ));

        // EMAIL
        $this->setHookOptions('email', 'mainTabs', array(
            'selected'      => 'customer_info_email'
        ));
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_info.business.email_handler',
                'submit_label'  => 'Send'
            )
        ));
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.customer_info.business.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:CustomerInfo:email.html.twig');

        // GENE RESULT        
        $this->addFeature('gene_result_grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'       => 'leep_admin.customer_info.business.gene_result_grid_reader',
                'filter'        => 'leep_admin.customer_info.business.gene_result_filter_handler'
            )
        ));
        
        $this->setHookOptions('gene_result_grid', 'mainTabs', array(
            'content'  => array(),
            'selected' => ''
        ));
        $this->setTemplate('gene_result_grid.list', 'LeepAdminBundle:CustomerInfo:gene_result_list.html.twig');

        // EDIT COMPLAINTS
        $this->addFeature('edit_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_info.business.edit_complaint_handler'
            )
        ));
        $this->setHookOptions('edit_complaint', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit complaint', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_complaint.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT FEEDBACKS
        $this->addFeature('edit_feedback', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_info.business.edit_feedback_handler'
            )
        ));
        $this->setHookOptions('edit_feedback', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_feedback.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // UNLOCK
        $this->addFeature('unlock', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_info.business.unlock_handler'
            )
        ));
        $this->setTemplate('unlock.edit', 'LeepAdminBundle:CRUD:popup_form_no_tab.html.twig');

        // MERGE
        $this->addFeature('merge', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_info.business.merge_handler'
            )
        ));
        $this->setHookOptions('merge', 'mainTabs', array(          
            'extra'         => array(
                'merge' => array('title' => 'Merge orders', 'link' => '#')
            ), 
            'selected' => 'merge'
        ));
        $this->setTemplate('merge.edit', 'LeepAdminBundle:CustomerInfo:merge.html.twig');
    }
}
