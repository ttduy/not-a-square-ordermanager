<?php
namespace Leep\AdminBundle\Module;

class CustomerAnalysisTime extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Analysis Time'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // CREATE
        $this->addHook('create', array(
            'class'           => 'leep_admin.customer_analysis_time.business.analysis_time_hook',
            'point'           => 'hook_after',
            'features'        => 'create'  
        )); 
        $this->addFeature('create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',                   
                'handler'       => 'leep_admin.customer_analysis_time.business.analysis_time_handler'
            )
        ));
        $this->setHookOptions('create', 'mainTabs', array(          
            'content'  => array(
                'form' => array('title' => 'Analysis time', 'link' => '#'),
            ),
            'selected' => 'form'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:CustomerAnalysisTime:analysis_time_form.html.twig');   

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.customer_analysis_time.business.analysis_time_feature',
            'options'         => array(
            )
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content' => array(),
            'selected' => 'list'
        ));    
    }
}
