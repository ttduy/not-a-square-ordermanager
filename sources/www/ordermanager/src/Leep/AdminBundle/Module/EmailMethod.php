<?php
namespace Leep\AdminBundle\Module;

class EmailMethod extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailSystem',
            'title'         => 'EmailSystem'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email method',
            'create'        => 'Add new email method'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email Method',
            'reader'   => 'leep_admin.email_method.business.grid_reader',
            'filter'   => 'leep_admin.email_method.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.email_method.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit email method', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.email_method.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.email_method.business.delete_handler'
        ));
    }
}
