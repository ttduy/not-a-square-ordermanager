<?php
namespace Leep\AdminBundle\Module;

class ScanFormCodeImport extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanForm',
            'title'         => 'Scan form code import'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Code import',
            'create'        => 'Add new code import'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Code import',
            'reader'   => 'leep_admin.scan_form_code_import.business.grid_reader',        
            'filter'   => 'leep_admin.scan_form_code_import.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.scan_form_code_import.business.create_handler',
            'toEdit'        => true,
            'toEditFeature' => 'edit',
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:ScanFormCodeImport:create.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit code import', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.scan_form_code_import.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:ScanFormCodeImport:edit.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_form_code_import.business.delete_handler'
        ));       
    }
}
