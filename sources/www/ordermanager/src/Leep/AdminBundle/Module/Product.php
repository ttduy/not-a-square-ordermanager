<?php
namespace Leep\AdminBundle\Module;

class Product extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Product',
            'title'         => 'Product'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Products',
            'create'        => 'Add new product'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Product',
            'reader'   => 'leep_admin.product.business.grid_reader',
            'filter'   => 'leep_admin.product.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Product:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.product.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Product:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit product', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.product.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Product:form.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.product.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.product.business.delete_handler'
        ));

        // PRODUCT
        $this->setHookOptions('product', 'mainTabs', array(
            'extra'         => array(
            ),
            'selected'      => 'edit'
        ));
        $this->addFeature('product', array(
            'class'           => 'leep_admin.feature.product',
            'options'         => array(
            )
        ));

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.product.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy product', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.edit', 'LeepAdminBundle:CRUD:form.html.twig');
    }
}
