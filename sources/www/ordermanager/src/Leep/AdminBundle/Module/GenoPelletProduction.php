<?php
namespace Leep\AdminBundle\Module;

class GenoPelletProduction extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoPelletProduction',
            'title'         => 'Pellet Production'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Pellet Formulas',
            'create'        => 'Start new production'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_pellet_production.business.create_handler',
            'redirectNow'   => true,
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.geno_pellet_production'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_pellet_production.business.delete_handler'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Geno Productions',
            'reader'   => 'leep_admin.geno_pellet_production.business.grid_reader',
            'filter'   => 'leep_admin.geno_pellet_production.business.filter_handler'
        ));
    }
}
