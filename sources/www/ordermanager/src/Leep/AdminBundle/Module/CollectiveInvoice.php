<?php
namespace Leep\AdminBundle\Module;

class CollectiveInvoice extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CollectiveInvoice',
            'title'         => 'CollectiveInvoice'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Collective Invoice',
            'create'        => 'Add new collective invoice'
        ));
        


        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Collective Invoice',
            'reader'   => 'leep_admin.collective_invoice.business.grid_reader',        
            'filter'   => 'leep_admin.collective_invoice.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective invoice', 'link' =>  $this->getUrl('invoice_form', 'list'))
            ),
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.collective_invoice.business.create_handler'
        ));        


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit collective invoice', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.collective_invoice.business.edit_handler'
        ));

        // EDIT ACTIVITY
        $this->addFeature('edit_activity', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.collective_invoice.business.edit_activity_handler'
            )
        ));
        $this->setHookOptions('edit_activity', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_activity.edit', 'LeepAdminBundle:Customer:activity_form.html.twig');    

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.collective_invoice.business.delete_handler'
        ));       


        // COLLECTIVE INVOICE FORM
        $this->addHook('invoice_form', array(
            'class'           => 'leep_admin.hook.collective_invoice_form',
            'point'           => 'hook_after',
            'features'        => 'invoice_form invoice_form_edit'
        ));
        $this->addFeature('invoice_form', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Collective Invoice',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.collective_invoice_form.business.grid_reader',        
                'filter'   => 'leep_admin.collective_invoice_form.business.filter_handler'                
            )
        ));        
        
        $this->setHookOptions('invoice_form', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective invoice', 'link' =>  $this->getUrl('invoice_form', 'list'))
            ),
            'selected'   => 'create'
        ));
        $this->setTemplate('invoice_form.list', 'LeepAdminBundle:CollectiveInvoiceForm:form_list.html.twig');


        // COLLECTIVE INVOICE FORM EDIT
        $this->addFeature('invoice_form_edit', array(
            'class'           => 'leep_admin.feature.collective_invoice',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Collective Invoice',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.collective_invoice_form.business.edit_grid_reader',        
                'filter'   => 'leep_admin.collective_invoice_form.business.edit_filter_handler'                
            )
        ));        
        $this->setHookOptions('invoice_form_edit', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new collective invoice', 'link' =>  $this->getUrl('invoice_form', 'list')),
                'edit'   => array('title' => 'Edit collective invoice', 'link' => '#')
            ),
            'selected'   => 'edit'
        ));
        $this->setTemplate('invoice_form_edit.list', 'LeepAdminBundle:CollectiveInvoiceForm:form_edit_list.html.twig');

        // COLLECTIVE INVOICE PDF
        $this->setHookOptions('invoice_form_pdf', 'mainTabs', array(
            'selected'   => 'edit'
        ));
        $this->addFeature('invoice_form_pdf', array(
            'class'           => 'leep_admin.feature.collective_invoice_pdf',
            'options'         => array(              
            )
        ));        
        $this->setTemplate('invoice_form_pdf.email', 'LeepAdminBundle:CollectiveInvoiceForm:email.html.twig');

        // COLLECTIVE INVOICE EMAIL
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.email.hook.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'  
        ));  
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.collective_invoice.business.email_handler',
                'submit_label' => 'Send'
            )
        ));
        $this->setHookOptions('email', 'mainTabs', array(
            'selected' => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:CollectiveInvoiceForm:email.html.twig');

    }
}
