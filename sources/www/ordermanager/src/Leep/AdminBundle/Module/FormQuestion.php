<?php
namespace Leep\AdminBundle\Module;

class FormQuestion extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'FormQuestion'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Form questions',
            'create'        => 'Add new form question'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Form questions',
            'reader'   => 'leep_admin.form_question.business.grid_reader',
            'filter'   => 'leep_admin.form_question.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.form_question.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit form question', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.form_question.business.edit_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.form_question.business.delete_handler'
        ));       
    }
}
