<?php
namespace Leep\AdminBundle\Module;

class ScanForm extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanForm',
            'title'         => 'ScanForm'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Scan forms',
            'create'        => 'Add new scan form'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Scan form',
            'reader'   => 'leep_admin.scan_form.business.grid_reader',        
            'filter'   => 'leep_admin.scan_form.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.scan_form.business.create_handler',
            'toEdit'        => true,
            'toEditFeature' => 'edit',
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit scan form', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.scan_form.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:ScanForm:edit.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_form.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.scan_form',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
    }
}
