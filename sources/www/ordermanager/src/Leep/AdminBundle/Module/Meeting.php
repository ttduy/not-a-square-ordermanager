<?php
namespace Leep\AdminBundle\Module;

class Meeting extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Meeting',
            'title'         => 'Meeting log'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Meeting log',
            'create'        => 'Add new meeting'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Meeting log',
            'reader'   => 'leep_admin.meeting.business.grid_reader',        
            'filter'   => 'leep_admin.meeting.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Meeting:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'toEdit'        => true,
            'toEditFeature' => 'edit',
            'handler'       => 'leep_admin.meeting.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:Meeting:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit meeting', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.meeting.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Meeting:form.html.twig');
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.meeting.business.delete_handler'
        ));       

        // CREATE TASK
        $this->addFeature('create_task', array(
            'class'           => 'easy_crud.feature.create',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.meeting.business.create_task_handler',
                'submit_label'  => 'Create'
            )
        ));
        $this->setHookOptions('create_task', 'mainTabs', array(
            'extra'         => array(
                'create_task' => array('title' => 'Create task from meeting', 'link' => '#')
            ),
            'selected'      => 'create_task'
        ));
        $this->setTemplate('create_task.create', 'LeepAdminBundle:Meeting:create_task.html.twig');
    }
}
