<?php
namespace Leep\AdminBundle\Module;

class EmailMassSender extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailSystem',
            'title'         => 'EmailSystem'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email Mass Sender',
            'create'        => 'Add new mass email sender'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.email_mass_sender'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email Mass Sender',
            'reader'   => 'leep_admin.email_mass_sender.business.grid_reader',
            'filter'   => 'leep_admin.email_mass_sender.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.email_mass_sender.business.create_handler'
        ));

        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.email_mass_sender.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));

        $this->setHookOptions('create', 'mainTabs', array(
            'selected'      => 'create'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:EmailMassSender:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.email_mass_sender.business.delete_handler'
        ));

        // VIEW
        $this->addFeature('view', array(
            'class'           => 'easy_crud.feature.view',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.email_mass_sender.business.view_handler'
            )
        ));

        $this->setHookOptions('view', 'mainTabs', array(
            'selected' => 'view'
        ));

        $this->setTemplate('view.view', 'LeepAdminBundle:EmailMassSender:log_view.html.twig');
    }
}
