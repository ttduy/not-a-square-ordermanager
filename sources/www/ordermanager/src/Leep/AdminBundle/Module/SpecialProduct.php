<?php
namespace Leep\AdminBundle\Module;

class SpecialProduct extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'Special Product'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Special products',
            'create'        => 'Add new special products'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Special products',
            'reader'   => 'leep_admin.special_product.business.grid_reader',        
            'filter'   => 'leep_admin.special_product.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.special_product.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit special product', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.special_product.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.special_product.business.delete_handler'
        ));     
    }
}
