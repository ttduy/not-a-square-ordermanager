<?php
namespace Leep\AdminBundle\Module;

class FoodTableCategory extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FoodTable',
            'title'         => 'Food Table - Category'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Categories',
            'create'        => 'Add new category'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Food Table - Category',
            'reader'   => 'leep_admin.food_table_category.business.grid_reader',
            'filter'   => 'leep_admin.food_table_category.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.food_table_category.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit category', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.food_table_category.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.food_table_category.business.delete_handler'
        ));       
    }
}
