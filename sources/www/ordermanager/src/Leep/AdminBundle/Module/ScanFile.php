<?php
namespace Leep\AdminBundle\Module;

class ScanFile extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanFormTemplate',
            'title'         => 'Scan File'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.scan_file_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // UPLOAD
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.scan_file.business.upload_handler',
            'submit_label'   => 'Upload'
        ));        
        $this->setHookOptions('create', 'mainTabs', array(
            'extra'         => array(
            ),
            'selected'      => 'list'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Scan File',
            'filter'   => 'leep_admin.scan_file.business.filter_handler',
            'reader'   => 'leep_admin.scan_file.business.grid_reader'      
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                
            ),
            'selected'      => 'list'
        ));
        $this->addHook('grid_hook', array(
            'class'           => 'leep_admin.scan_file.business.grid_reader_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:ScanFile:list.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_file.business.delete_handler'
        ));       


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.scan_file',
            'options'         => array(
            )
        ));

        // DETECT TEMPLATE
        $this->addFeature('detect_template', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.detect_template_handler',
                'submit_label'      => 'Next',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('detect_template_hook', array(
            'class'           => 'leep_admin.scan_file.business.detect_template_hook',
            'point'           => 'hook_after',
            'features'        => 'detect_template'
        ));
        $this->setTemplate('detect_template.edit', 'LeepAdminBundle:ScanFile:detect_template.html.twig');

        // REALIGN TEMPLATE
        $this->addFeature('realign_template', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.realign_template_handler',
                'submit_label'      => 'Next',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('realign_template_hook', array(
            'class'           => 'leep_admin.scan_file.business.realign_template_hook',
            'point'           => 'hook_after',
            'features'        => 'realign_template'
        ));
        $this->setTemplate('realign_template.edit', 'LeepAdminBundle:ScanFile:realign_template.html.twig');

        // FILL BASIC DATA
        $this->addFeature('fill_basic_data', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.fill_basic_data_handler',
                'submit_label'      => 'Next',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('fill_basic_data_hook', array(
            'class'           => 'leep_admin.scan_file.business.fill_basic_data_hook',
            'point'           => 'hook_after',
            'features'        => 'fill_basic_data'
        ));
        $this->setTemplate('fill_basic_data.edit', 'LeepAdminBundle:ScanFile:fill_basic_data.html.twig');

        // GENERATE ORDER
        $this->addFeature('generate_order', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.generate_order_handler',
                'submit_label'      => 'Next file',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('generate_order_hook', array(
            'class'           => 'leep_admin.scan_file.business.generate_order_hook',
            'point'           => 'hook_after',
            'features'        => 'generate_order'
        ));
        $this->setTemplate('generate_order.edit', 'LeepAdminBundle:ScanFile:generate_order.html.twig');

        // FILL ALL DATA
        $this->addFeature('fill_all_data', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.fill_all_data_handler',
                'submit_label'      => 'Next',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('fill_all_data_hook', array(
            'class'           => 'leep_admin.scan_file.business.fill_all_data_hook',
            'point'           => 'hook_after',
            'features'        => 'fill_all_data'
        ));
        $this->setTemplate('fill_all_data.edit', 'LeepAdminBundle:ScanFile:fill_all_data.html.twig');

        // UPDATE ORDER
        $this->addFeature('update_order', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_file.business.update_order_handler',
                'submit_label'      => 'Close & Next file',
                'useFormRedirect'   => true
            )
        ));
        $this->addHook('update_order_hook', array(
            'class'           => 'leep_admin.scan_file.business.update_order_hook',
            'point'           => 'hook_after',
            'features'        => 'update_order'
        ));
        $this->setTemplate('update_order.edit', 'LeepAdminBundle:ScanFile:update_order.html.twig');

    }
}
