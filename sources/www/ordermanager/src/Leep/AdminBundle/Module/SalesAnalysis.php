<?php
namespace Leep\AdminBundle\Module;

class SalesAnalysis extends Base {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'SalesAnalysis',
            'title'         => 'Sales Analysis'
        ));

        $this->addHook('mainTabs', array(
            'class' =>              'leep_admin.hook.main_tabs_crud',
            'point' =>              'hook_before',
            'features' =>           '*'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'summary' => 'Summary',
            'empty-init-content' => true
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class' => 'leep_admin.feature.sales_analysis'
        ));

        // EDIT
        $this->addFeature('summary', array(
            'class' => 'easy_crud.feature.edit',
            'options' => [
                'id' => 'AppForm',
                'handler' => 'leep_admin.sales_analysis.business.summary_handler',
                'submit_label' => 'Filter'
            ]
        ));

        $this->setHookOptions('summary', 'mainTabs', array(
            'selected' => 'edit',
            'extra'         => [
                'edit' => ['title' => 'Summary', 'link' => '#']
            ]
        ));

        $this->setTemplate('summary.edit', 'LeepAdminBundle:SalesAnalysis:form.html.twig');
    }
}
