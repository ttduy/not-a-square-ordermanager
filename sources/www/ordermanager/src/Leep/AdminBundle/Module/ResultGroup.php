<?php
namespace Leep\AdminBundle\Module;

class ResultGroup extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GeneManager',
            'title'         => 'Result Group'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Result Groups',
            'create'        => 'Add new result group'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Result Group',
            'reader'   => 'leep_admin.result_group.business.grid_reader',
            'filter'   => 'leep_admin.result_group.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.result_group.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit result group', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.result_group.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.result_group.business.delete_handler'
        ));       
    }
}
