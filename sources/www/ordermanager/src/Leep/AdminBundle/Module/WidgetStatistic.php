<?php
namespace Leep\AdminBundle\Module;

class WidgetStatistic extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'WidgetStatistic',
            'title'         => 'WidgetStatistic'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Widgets',
            'create'        => 'Add new widget'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Widgets',
            'reader'   => 'leep_admin.widget_statistic.business.grid_reader',        
            'filter'   => 'leep_admin.widget_statistic.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.widget_statistic.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit widget', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.widget_statistic.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.widget_statistic.business.delete_handler'
        ));       
    }
}
