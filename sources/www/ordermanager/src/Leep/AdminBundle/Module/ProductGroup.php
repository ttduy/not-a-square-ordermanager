<?php
namespace Leep\AdminBundle\Module;

class ProductGroup extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Product',
            'title'         => 'Product Group'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Product Groups',
            'create'        => 'Add new Product Group'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Product Groups',
            'reader'   => 'leep_admin.product_group.business.grid_reader',        
            'filter'   => 'leep_admin.product_group.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.product_group.business.create_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit product group', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.product_group.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.product_group.business.delete_handler'
        ));

    }
}
