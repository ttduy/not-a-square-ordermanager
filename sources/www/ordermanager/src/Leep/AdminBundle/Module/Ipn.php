<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Ipn extends Base {   
    public function load() {
        parent::load();        

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => ''
        ));
        
        $this->addFeature('ipn', array(
            'class'           => 'leep_admin.feature.ipn',
            'options'         => array(
            )
        ));
    }
}
