<?php
namespace Leep\AdminBundle\Module;

class Recipe extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Recipe',
            'title'         => 'Recipe'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Recipe',
            'create'        => 'Add new recipe'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Recipe',
            'reader'   => 'leep_admin.recipe.business.grid_reader',
            'filter'   => 'leep_admin.recipe.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.recipe.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit recipe', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.recipe.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.recipe.business.delete_handler'
        ));       
    }
}
