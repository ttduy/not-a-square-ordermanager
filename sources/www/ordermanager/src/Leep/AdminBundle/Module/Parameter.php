<?php
namespace Leep\AdminBundle\Module;

class Parameter extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Parameter',
            'title'         => 'Parameter'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // EDIT
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',                   
                'handler'       => 'leep_admin.parameter.business.edit_handler'
            )
        ));
        $this->setHookOptions('edit', 'mainTabs', array(          
            'content'  => array(
                'edit' => array('title' => 'Edit parameter', 'link' => '#'),
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');        
    }
}
