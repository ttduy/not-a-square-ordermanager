<?php
namespace Leep\AdminBundle\Module;

class MwaValidCode extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'MWAProcessor',
            'title'         => 'Valid Code'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Valid Codes',
            'create'        => 'Add new valid code'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Valid Code',
            'reader'   => 'leep_admin.mwa_valid_code.business.grid_reader',
            'filter'   => 'leep_admin.mwa_valid_code.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:MwaValidCode:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.mwa_valid_code.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit valid code', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.mwa_valid_code.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.mwa_valid_code.business.delete_handler'
        ));       
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.mwa_valid_code',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
    }
}
