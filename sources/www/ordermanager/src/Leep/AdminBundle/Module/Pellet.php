<?php
namespace Leep\AdminBundle\Module;

class Pellet extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Pellet',
            'title'         => 'Pellet'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.pellet_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Pellets',
            'reader'   => 'leep_admin.pellet.business.grid_reader',
            'filter'   => 'leep_admin.pellet.business.filter_handler',
        ));
        $this->addHook('pellet_grid_hook', array(
            'class'           => 'leep_admin.pellet.business.pellet_grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Pellet:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.pellet.business.create_handler'
        ));
        $this->setHookOptions('create', 'mainTabs', array(
            'selected'      => 'create'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit pellet', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.pellet.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.pellet.business.delete_handler'
        ));

        // FEATURE - CONSIDER TO REMOVE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.pellet'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'      => 'list'
        ));

        // STOCK IN USE LIST
        $this->addFeature('stockInUseGrid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'   => 'leep_admin.pellet.business.stock_in_use_grid_reader',
                'filter'   => 'leep_admin.pellet.business.stock_in_use_filter_handler',
            )
        ));
        $this->setHookOptions('stockInUseGrid', 'mainTabs', array(
            'content'  => array(
                'list' => array('title' => 'Stock in use', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('stockInUseGrid.list', 'LeepAdminBundle:Pellet:stock_in_use_list.html.twig');
    }
}
