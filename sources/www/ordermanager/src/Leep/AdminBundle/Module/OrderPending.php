<?php
namespace Leep\AdminBundle\Module;

class OrderPending extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order Pending'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.order_pending_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order Pending',
            'reader'   => 'leep_admin.order_pending.business.grid_reader',
            'filter'   => 'leep_admin.order_pending.business.filter_handler'
        ));

        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:OrderPending:list.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.order_pending.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.order_pending.business.delete_handler'
        ));
    }
}
