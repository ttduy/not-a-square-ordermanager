<?php
namespace Leep\AdminBundle\Module;

class ProductPrice extends Base {   
    public function load() { 
        parent::load();     

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Price',
            'title'         => 'Product Price'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array(
                'list'          => array('title' => 'Product price', 'link' => '#')
            ),
            'selected'          => 'list'
        ));

        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Product Price',
                'reader'   => 'leep_admin.product_price.business.grid_reader',
                'filter'   => 'leep_admin.product_price.business.filter_handler',
                'filter.visiblity'    => 'show'
            )
        ));        
        $this->setTemplate('grid.list', 'LeepAdminBundle:ProductPrice:list.html.twig');

        // AJAX UPDATE
        $this->addFeature('ajax_update', array(
            'class'      => 'leep_admin.feature.ajax_update',
            'options'    => array(
                'handlers' => array(
                    'price'       => 'leep_admin.product_price.business.ajax_update.price',
                    'dcmargin'    => 'leep_admin.product_price.business.ajax_update.price',
                    'partmargin'  => 'leep_admin.product_price.business.ajax_update.price',
                )
            )
        ));

        // IMPORT PRICE
        $this->addFeature('import_price', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.product_price.business.import_price_handler'
            )
        ));
        $this->setHookOptions('import_price', 'mainTabs', array(
            'content'         => array(
                'edit' => array('title' => 'Import price', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('import_price.edit', 'LeepAdminBundle:ProductPrice:import_price.html.twig');   

    }
}
