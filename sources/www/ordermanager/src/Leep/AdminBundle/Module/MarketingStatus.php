<?php
namespace Leep\AdminBundle\Module;

class MarketingStatus extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'DC Info Status'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'DC Info Status',
            'create'        => 'Add new DC Info Status'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'DC Info Status',
            'reader'   => 'leep_admin.marketing_status.business.grid_reader',
            'filter'   => 'leep_admin.marketing_status.business.grid_filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.marketing_status.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit DC Info Status', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.marketing_status.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.marketing_status.business.delete_handler'
        ));       
    }
}
