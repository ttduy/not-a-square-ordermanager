<?php
namespace Leep\AdminBundle\Module;

class GenlifeSampleRegistration extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Genlife',
            'title'         => 'Genlife'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'empty-init-content' => true,
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Genlife sample registration',
            'reader'   => 'leep_admin.genlife_sample_registration.business.grid_reader',        
            'filter'   => 'leep_admin.genlife_sample_registration.business.filter_handler'
        ));
       

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'View genlife sample registration', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.genlife_sample_registration.business.edit_handler'
        ));
     
        $this->setTemplate('edit.edit', 'LeepAdminBundle:GenlifeSampleRegistration:edit_result.html.twig');


    }
}
