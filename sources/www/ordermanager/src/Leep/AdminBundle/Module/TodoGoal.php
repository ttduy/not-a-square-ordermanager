<?php
namespace Leep\AdminBundle\Module;

class TodoGoal extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'StrategyPlanningTool',
            'title'         => 'Goals'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Goals',
            'create'        => 'Add new goal'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Goals',
            'reader'   => 'leep_admin.todo_goal.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.todo_goal.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit goal', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.todo_goal.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.todo_goal.business.delete_handler'
        ));       
    }
}
