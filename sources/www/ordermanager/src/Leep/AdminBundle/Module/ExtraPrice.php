<?php
namespace Leep\AdminBundle\Module;

class ExtraPrice extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Price',
            'title'         => 'Extra price'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        $this->addHook('edit_price_report_delivery_hook', array(
            'class'           => 'leep_admin.extra_price.business.edit_price_report_delivery_hook',
            'point'           => 'hook_after',
            'features'        => 'edit_price_report_delivery'  
        ));  
        // EDIT
        $this->addFeature('edit_price_report_delivery', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',                   
                'handler'       => 'leep_admin.extra_price.business.edit_price_report_delivery_handler'
            )
        ));
        $this->setHookOptions('edit_price_report_delivery', 'mainTabs', array(          
            'content'  => array(
                'edit' => array('title' => 'Extra price', 'link' => '#'),
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_price_report_delivery.edit', 'LeepAdminBundle:ExtraPrice:edit_price_report_delivery.html.twig');        
    }
}
