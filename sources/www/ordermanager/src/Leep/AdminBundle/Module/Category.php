<?php
namespace Leep\AdminBundle\Module;

class Category extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Product',
            'title'         => 'Category'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Categories',
            'create'        => 'Add new category'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Category',
            'reader'   => 'leep_admin.category.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.category.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit category', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.category.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.category.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.category.business.delete_handler'
        ));        
    }
}
