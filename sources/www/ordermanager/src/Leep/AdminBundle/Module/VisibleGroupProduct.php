<?php
namespace Leep\AdminBundle\Module;

class VisibleGroupProduct extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'Visible Group Product'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Visible Group Product List',
            'create'        => 'Add new Visible Group Product'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Visible Group - Product',
            'reader'   => 'leep_admin.visible_group_product.business.grid_reader',        
            'filter'   => 'leep_admin.visible_group_product.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.visible_group_product.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Visible Group Product', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.visible_group_product.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.visible_group_product.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.visible_group_product.business.delete_handler'
        ));       
    }
}
