<?php
namespace Leep\AdminBundle\Module;

class GenoPelletFormula extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoPelletProduction',
            'title'         => 'Genop Pellet Formula'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Pellet Formulas',
            'create'        => 'Add new Pellet Formula'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Geno Pellet Formulas',
            'reader'   => 'leep_admin.geno_pellet_formula.business.grid_reader',
            'filter'   => 'leep_admin.geno_pellet_formula.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_pellet_formula.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Formula', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.geno_pellet_formula.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_pellet_formula.business.delete_handler'
        ));
    }
}
