<?php
namespace Leep\AdminBundle\Module;

class GenoPellet extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoPelletProduction',
            'title'         => 'Geno Pellet'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Pellet List',
            'create'        => 'Add new Pellet'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Pellet List',
            'reader'   => 'leep_admin.geno_pellet.business.grid_reader',
            'filter'   => 'leep_admin.geno_pellet.business.filter_handler',
        ));

        $this->addHook('grid_hook', array(
            'class'           => 'leep_admin.geno_pellet.business.grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));

        $this->setTemplate('grid.list', 'LeepAdminBundle:GenoPellet:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_pellet.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit pellet', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.geno_pellet.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_pellet.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.geno_pellet'
        ));

        // STOCK IN USE LIST
        $this->addFeature('stockInUseGrid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'   => 'leep_admin.geno_pellet.business.stock_in_use_grid_reader',
                'filter'   => 'leep_admin.geno_pellet.business.stock_in_use_filter_handler',
            )
        ));

        $this->setHookOptions('stockInUseGrid', 'mainTabs', array(
            'content'  => array(
                'list' => array('title' => 'Stock in use', 'link' => '#'),
            ),
            'selected' => 'list'
        ));

        $this->setTemplate('stockInUseGrid.list', 'LeepAdminBundle:GenoPellet:stock_in_use_list.html.twig');
    }
}
