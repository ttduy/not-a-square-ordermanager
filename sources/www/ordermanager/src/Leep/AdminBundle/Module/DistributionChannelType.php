<?php
namespace Leep\AdminBundle\Module;

class DistributionChannelType extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'Distribution Channel Types'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Distribution Channel Types',
            'create'        => 'Add new type'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Distribution Channel Types',
            'reader'   => 'leep_admin.distribution_channel_type.business.grid_reader'       
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.distribution_channel_type.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit type', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.distribution_channel_type.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.distribution_channel_type.business.delete_handler'
        ));       
    }
}
