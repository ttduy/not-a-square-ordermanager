<?php
namespace Leep\AdminBundle\Module;

class TextBlockGroup extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'TextBlock',
            'title'         => 'Text Block Group'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Groups',
            'create'        => 'Add new group'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Group',
            'reader'   => 'leep_admin.text_block_group.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.text_block_group.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit group', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.text_block_group.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.text_block_group.business.delete_handler'
        ));       
    }
}
