<?php
namespace Leep\AdminBundle\Module;

class CustomerProductFilter extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Product filter'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.customer_product_filter.business.grid_reader',
                'filter'   => 'leep_admin.customer_product_filter.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Product filter', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');   
    }
}
