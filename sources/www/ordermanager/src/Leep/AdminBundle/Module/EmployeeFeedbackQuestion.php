<?php
namespace Leep\AdminBundle\Module;

class EmployeeFeedbackQuestion extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmployeeFeedbackQuestion',
            'title'         => 'Employee Satisfaction Question'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Employee Satisfaction Questions',
            'create'        => 'Add new Employee Satisfaction Question'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Feedback questions',
            'reader'   => 'leep_admin.employee_feedback_question.business.grid_reader',        
            'filter'   => 'leep_admin.employee_feedback_question.business.filter_handler'
        ));
        $this->addHook('employee_feedback_question_grid_hook', array(
            'class'           => 'leep_admin.employee_feedback_question.business.grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'  
        )); 
        $this->setTemplate('grid.list', 'LeepAdminBundle:EmployeeFeedbackQuestion:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.employee_feedback_question.business.create_handler',
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Employee Satisfaction', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.employee_feedback_question.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.employee_feedback_question.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.employee_feedback_question',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
    }
}
