<?php
namespace Leep\AdminBundle\Module;

class Material extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Material',
            'title'         => 'Material'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Materials',
            'create'        => 'Add new material'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Material',
            'reader'   => 'leep_admin.material.business.grid_reader',
            'filter'   => 'leep_admin.material.business.filter_handler'
        ));
        $this->addHook('material_grid_hook', array(
            'class'           => 'leep_admin.material.business.material_grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Material:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.material.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit material', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.material.business.edit_handler'
        ));


        // EDIT STOCK
        $this->addFeature('edit_stock', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.material.business.edit_stock_handler'
            )
        ));
        $this->setHookOptions('edit_stock', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Manage stock', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_stock.edit', 'LeepAdminBundle:Material:form_material_stock.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.material.business.view_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.material.business.delete_handler'
        ));

        // CREATE MATERIAL PURCHASE
        $this->addFeature('create_material_purchase', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.material.business.create_material_purchase_handler'
            )
        ));
        $this->setHookOptions('create_material_purchase', 'mainTabs', array(
            'extra'         => array(
                'create_material_purchase' => array('title' => 'Add new material purchase', 'link' => '#')
            ),
            'selected' => 'create_material_purchase'
        ));
        $this->setTemplate('create_material_purchase.create', 'LeepAdminBundle:CRUD:form.html.twig');


        // CREATE MATERIAL USED
        $this->addFeature('create_material_used', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.material.business.create_material_used_handler'
            )
        ));
        $this->setHookOptions('create_material_used', 'mainTabs', array(
            'extra'         => array(
                'create_material_used' => array('title' => 'Add new material used', 'link' => '#')
            ),
            'selected' => 'create_material_used'
        ));
        $this->setTemplate('create_material_used.create', 'LeepAdminBundle:CRUD:form.html.twig');


        // MATERIAL FINANCE TRACKER
        $this->addFeature('material_finance_tracker', array(
            'class'           => 'leep_admin.feature.material_finance_tracker',
            'options'         => array(
            )
        ));
        $this->setTemplate('material_finance_tracker.materialType', 'LeepAdminBundle:Material:finance_tracker_material_type.html.twig');

        // MATERIAL FEATURE
        $this->addFeature('material', array(
            'class'           => 'leep_admin.feature.material',
            'options'         => array(
            )
        ));
    }
}
