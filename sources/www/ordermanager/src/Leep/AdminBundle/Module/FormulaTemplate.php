<?php
namespace Leep\AdminBundle\Module;

class FormulaTemplate extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FormulaTemplate',
            'title'         => 'FormulaTemplate'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Formula Templates',
            'create'        => 'Add new formula template'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Formula Template',
            'reader'   => 'leep_admin.formula_template.business.grid_reader',
            'filter'   => 'leep_admin.formula_template.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.formula_template.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:FormulaTemplate:create.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit formula template', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.formula_template.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:FormulaTemplate:form.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.formula_template',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.formula_template.business.delete_handler'
        ));       
    }
}
