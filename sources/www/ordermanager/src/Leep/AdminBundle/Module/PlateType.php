<?php
namespace Leep\AdminBundle\Module;

class PlateType extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Plate',
            'title'         => 'PlateType'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Plate Types',
            'create'        => 'Add new Plate Type'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'PlateType',
            'reader'   => 'leep_admin.plate_type.business.grid_reader',
            'filter'   => 'leep_admin.plate_type.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'toEdit'        => true,
            'toEditFeature' => 'edit',
            'handler'       => 'leep_admin.plate_type.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Plate Type', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.plate_type.business.edit_handler'
        ));
        $this->addHook('edit_hook', array(
            'class'           => 'leep_admin.plate_type.business.edit_hook',
            'point'           => 'hook_after',
            'features'        => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:PlateType:form.html.twig');
   
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.plate_type.business.delete_handler'
        ));       
    }
}
