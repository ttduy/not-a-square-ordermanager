<?php
namespace Leep\AdminBundle\Module;

class FoodTableFoodItem extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FoodTable',
            'title'         => 'Food Table - Food Item'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Food items',
            'create'        => 'Add new food item'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Food Table - Food item',
            'reader'   => 'leep_admin.food_table_food_item.business.grid_reader',
            'filter'   => 'leep_admin.food_table_food_item.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:FoodTableFoodItem:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.food_table_food_item.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit food item', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.food_table_food_item.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.food_table_food_item.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.food_table_food_item',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
    }
}
