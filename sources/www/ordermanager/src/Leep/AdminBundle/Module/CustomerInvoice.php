<?php
namespace Leep\AdminBundle\Module;

class CustomerInvoice extends Base {   
    public function load() { 
        parent::load();     

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Invoice'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array(
                'list'          => array('title' => 'Customers', 'link' => $this->getModuleManager()->getUrl('leep_admin', 'customer', 'grid', 'list')),
                'create'        => array('title' => 'Create new customer', 'link' => $this->getModuleManager()->getUrl('leep_admin', 'customer', 'create', 'create')),
                'edit'          => array('title' => 'Update invoice', 'link' => '#'),
            ),
            'selected'          => 'list'
        ));

        // EDIT INVOICE
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.customer_invoice.business.edit_handler'
            )
        ));
        $this->addHook('invoice_edit_hook', array(
            'class'           => 'leep_admin.hook.invoice_edit',
            'point'           => 'hook_after',
            'features'        => '*'  
        ));  
        $this->setHookOptions('edit', 'mainTabs', array(            
            'selected' => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Customer:invoice_form.html.twig');       

        // CUSTOMER INVOICE PDF
        $this->setHookOptions('customer_form_pdf', 'mainTabs', array(
            'selected'   => 'edit'
        ));
        $this->addFeature('customer_form_pdf', array(
            'class'           => 'leep_admin.feature.customer_invoice_pdf',
            'options'         => array(              
            )
        ));  
    }
}
