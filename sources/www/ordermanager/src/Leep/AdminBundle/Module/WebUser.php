<?php
namespace Leep\AdminBundle\Module;

class WebUser extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'WebUser',
            'title'         => 'WebUser'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Web Users',
            'create'        => 'Add new web user'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Web User',
            'reader'   => 'leep_admin.web_user.business.grid_reader',        
            'filter'   => 'leep_admin.web_user.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.web_user.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit web user', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.web_user.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.web_user.business.delete_handler'
        ));

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.create',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.web_user.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy Web User', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.create', 'LeepAdminBundle:CRUD:popup_form_no_tab.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.web_user',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('feature.optOut', 'LeepAdminBundle:WebUser:email_unsubscribe.html.twig');
    }
}
