<?php
namespace Leep\AdminBundle\Module;

class VisibleGroupReport extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'Visible Group Report'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Visible Group Report List',
            'create'        => 'Add new Visible Group Report'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Visible Group - Report',
            'reader'   => 'leep_admin.visible_group_report.business.grid_reader',        
            'filter'   => 'leep_admin.visible_group_report.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.visible_group_report.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Visible Group Report', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.visible_group_report.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.visible_group_report.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.visible_group_report.business.delete_handler'
        ));       
    }
}
