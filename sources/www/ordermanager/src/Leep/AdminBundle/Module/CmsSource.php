<?php
namespace Leep\AdminBundle\Module;

class CmsSource extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Source'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Source',
            'create'        => 'Add new source'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Source',
            'reader'   => 'leep_admin.cms_source.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_source.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit source', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_source.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_source.business.delete_handler'
        ));       
    }
}
