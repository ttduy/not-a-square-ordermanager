<?php
namespace Leep\AdminBundle\Module;

Class ShipmentManager extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ShipmentManager',
            'title'         => 'ShipmentManager'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'ShipmentManager List',
            'create'        => 'Add new Shipment'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'ShipmentManager List',
            'reader'   => 'leep_admin.shipment_manager.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.shipment_manager.business.create_handler'
        ));        
        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Shipment', 'link' => '#')
            ),
            'selected'      => 'edit',
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.shipment_manager.business.edit_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.shipment_manager',
            'options'         => array()
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.shipment_manager.business.delete_handler'
        )); 
        //$this->setTemplate('grid.list', 'LeepAdminBundle:ShipmentManager:list.html.twig');
    }
} 
?>