<?php
namespace Leep\AdminBundle\Module;

class Worker extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Worker'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Worker',
            'create'        => 'Add new worker'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Worker',
            'reader'   => 'leep_admin.worker.business.grid_reader'       
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.worker.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit worker', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.worker.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.worker.business.delete_handler'
        ));       
    }
}
