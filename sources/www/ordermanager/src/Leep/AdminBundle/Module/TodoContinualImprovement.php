<?php
namespace Leep\AdminBundle\Module;

class TodoContinualImprovement extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'StrategyPlanningTool',
            'title'         => 'Continual Improvement'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Continual improvements',
            'create'        => 'Add new continual improvement'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Continual improvement',
            'reader'   => 'leep_admin.todo_continual_improvement.business.grid_reader',
            'filter'   => 'leep_admin.todo_continual_improvement.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:TodoContinualImprovement:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.todo_continual_improvement.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit continual improvement', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.todo_continual_improvement.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.todo_continual_improvement.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.todo_continual_improvement',
            'options'         => array()
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
    }
}
