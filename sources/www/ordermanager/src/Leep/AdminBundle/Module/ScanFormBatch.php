<?php
namespace Leep\AdminBundle\Module;

class ScanFormBatch extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanFormData',
            'title'         => 'Scan Form Batch'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Scan form batch',
            'create'        => 'Add new batch'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Scan form batch',
            'reader'   => 'leep_admin.scan_form_batch.business.grid_reader',        
            'filter'   => 'leep_admin.scan_form_batch.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.scan_form_batch.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit batch', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.scan_form_batch.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_form_batch.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.scan_form_batch',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'extra'         => array(
                'verify' => array('title' => 'Verify', 'link' => '#')
            ),
            'selected' => 'verify'
        ));    
        $this->setTemplate('feature.verifyProcessed', 'LeepAdminBundle:ScanFormBatch:verify_processed.html.twig');
        $this->setTemplate('feature.verifyTextbox', 'LeepAdminBundle:ScanFormBatch:verify_textbox.html.twig');
        $this->setTemplate('feature.verifyCheckbox', 'LeepAdminBundle:ScanFormBatch:verify_checkbox.html.twig');
        $this->setTemplate('feature.verifyTextarea', 'LeepAdminBundle:ScanFormBatch:verify_textarea.html.twig');
        $this->setTemplate('feature.verifyForm', 'LeepAdminBundle:ScanFormBatch:verify_form.html.twig');
    }
}
