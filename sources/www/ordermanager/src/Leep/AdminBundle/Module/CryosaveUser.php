<?php
namespace Leep\AdminBundle\Module;

class CryosaveUser extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Cryosave',
            'title'         => 'Cryosave user'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Cryosave user',
            'create'        => 'Add new user'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Cryosave user',
            'reader'   => 'leep_admin.cryosave_user.business.grid_reader',         
            'filter'   => 'leep_admin.cryosave_user.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cryosave_user.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit user', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cryosave_user.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cryosave_user.business.delete_handler'
        ));     
    }
}
