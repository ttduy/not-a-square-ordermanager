<?php
namespace Leep\AdminBundle\Module;

class CostTrackerSector extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FinanceTracker',
            'title'         => 'Cost Tracker Sector'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Cost Tracker Sector',
            'create'        => 'Add new sector'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Cost Tracker Sector',
            'reader'   => 'leep_admin.cost_tracker_sector.business.grid_reader',
            'filter'   => 'leep_admin.cost_tracker_sector.business.filter_handler'      
        ));

        $this->addHook('grid_hook', array(
            'class'           => 'leep_admin.cost_tracker_sector.business.grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'  
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cost_tracker_sector.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit cost tracker sector', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cost_tracker_sector.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cost_tracker_sector.business.delete_handler'
        ));

        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.cost_tracker_sector',
            'options'         => array(
            )
        ));

        $this->setTemplate('grid.list', 'LeepAdminBundle:CostTrackerSector:list.html.twig');
    }
}
