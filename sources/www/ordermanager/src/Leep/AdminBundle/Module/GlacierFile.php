<?php
namespace Leep\AdminBundle\Module;

class GlacierFile extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Glacier',
            'title'         => 'Glacier'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Glacier files',
            'create'        => 'Add new file'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Glacier',
            'reader'   => 'leep_admin.glacier_file.business.grid_reader',         
            'filter'   => 'leep_admin.glacier_file.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.glacier_file.business.create_handler'
        ));          
    }
}
