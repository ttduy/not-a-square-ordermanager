<?php
namespace Leep\AdminBundle\Module;

class MaterialType extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Material',
            'title'         => 'Material Type'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Material Types',
            'create'        => 'Add new type'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Material Types',
            'reader'   => 'leep_admin.material_type.business.grid_reader',      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.material_type.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit material type', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.material_type.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.material_type.business.delete_handler'
        ));       
    }
}
