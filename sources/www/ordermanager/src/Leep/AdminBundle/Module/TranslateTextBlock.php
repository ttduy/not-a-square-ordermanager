<?php
namespace Leep\AdminBundle\Module;

class TranslateTextBlock extends Base {   
    public function load() { 
        parent::load();     

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Translator',
            'title'         => 'Text Blocks'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array(
                'list'          => array('title' => 'Text Blocks', 'link' => '#')
            ),
            'selected'          => 'list'
        ));

        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Text Blocks',
                'reader'   => 'leep_admin.translate_text_block.business.grid_reader',
                'filter'   => 'leep_admin.translate_text_block.business.filter_handler',
                'filter.visiblity'    => 'show'
            )
        ));        
        $this->setTemplate('grid.list', 'LeepAdminBundle:TranslateTextBlock:list.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.translate_text_block'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content' => array(
            ),
            'selected'          => ''
        ));
        $this->setTemplate('feature.dashboard', 'LeepAdminBundle:TranslateTextBlock:dashboard.html.twig');
        $this->setTemplate('feature.translate', 'LeepAdminBundle:TranslateTextBlock:translate.html.twig');

    }
}
