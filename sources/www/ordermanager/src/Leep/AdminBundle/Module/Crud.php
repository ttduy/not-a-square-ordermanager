<?php
namespace Leep\AdminBundle\Module;

class Crud extends Base { 
    public function load() {
        parent::load();

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs_crud',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // GRID
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object'
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');

        // CREATE
        $this->addFeature('create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm'
            )
        ));
        $this->setHookOptions('create', 'mainTabs', array(
            'selected' => 'create'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm'
            )
        ));
        $this->setHookOptions('edit', 'mainTabs', array(            
            'selected' => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');        

        // VIEW
        $this->addFeature('view', array(
            'class'           => 'easy_crud.feature.view',
            'options'         => array(
                'id'            => 'AppForm'
            )
        ));
        $this->setHookOptions('view', 'mainTabs', array(            
            'selected' => 'view'
        ));
        $this->setTemplate('view.view', 'LeepAdminBundle:CRUD:view.html.twig');        

        // DELETE
        $this->addFeature('delete', array(
            'class'           => 'easy_crud.feature.delete',
            'options'         => array()
        ));
        $this->setHookOptions('delete', 'mainTabs', array(            
            'selected' => 'list'
        ));
    }
}
