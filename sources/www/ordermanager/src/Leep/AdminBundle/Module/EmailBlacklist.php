<?php
namespace Leep\AdminBundle\Module;

class EmailBlacklist extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailSystem',
            'title'         => 'EmailSystem'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email blacklist',
            'create'        => 'Add new email to blacklist'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email Blacklist',
            'reader'   => 'leep_admin.email_blacklist.business.grid_reader',
            'filter'   => 'leep_admin.email_blacklist.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.email_blacklist.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit email blacklist', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.email_blacklist.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.email_blacklist.business.delete_handler'
        ));
    }
}
