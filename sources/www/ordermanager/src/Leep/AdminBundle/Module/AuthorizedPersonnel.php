<?php
namespace Leep\AdminBundle\Module;

class AuthorizedPersonnel extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Form',
            'title'         => 'Authorized Personnel'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Authorized Personnel',
            'create'        => 'Add new Authorized Personnel'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Authorized Personnel',
            'filter'   => 'leep_admin.authorized_personnel.business.filter_handler',
            'reader'   => 'leep_admin.authorized_personnel.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.authorized_personnel.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit currency', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.authorized_personnel.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.authorized_personnel.business.delete_handler'
        ));       
    }
}
