<?php
namespace Leep\AdminBundle\Module;

class FeedbackResponse extends Crud {   
    public function load() { 
        parent::load();     

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Feedback',
            'title'         => 'Feedback response'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.feedback_response',
            'point'           => 'hook_before',
            'features'        => 'grid'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Feedback responses',
            'reader'   => 'leep_admin.feedback_response.business.grid_reader',        
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'feedback_response'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:FeedbackResponse:list.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.feedback_response.business.delete_handler'
        ));       
    }
}
