<?php
namespace Leep\AdminBundle\Module;

class Massarray extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Massarray',
            'title'         => 'Massarray'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Massarrays',
            'create'        => 'Add new massarray'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Massarray',
            'reader'   => 'leep_admin.massarray.business.grid_reader',
            'filter'   => 'leep_admin.massarray.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.massarray.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit massarray', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.massarray.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.massarray.business.delete_handler'
        ));       
    }
}
