<?php
namespace Leep\AdminBundle\Module;

class GenoPelletEntry extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoPelletProduction',
            'title'         => 'Pellet Entry'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Pellet Entry',
            'create'        => 'Add new pellet entry'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Pellet Entry',
            'reader'   => 'leep_admin.geno_pellet_entry.business.grid_reader',
        ));
       

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_pellet_entry.business.create_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:GenoPelletEntry:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit plate', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.geno_pellet_entry.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:GenoPelletEntry:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_pellet_entry.business.delete_handler'
        ));

        // USE CUP
        $this->addFeature('usage', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.geno_pellet_entry.business.usage_handler',
                'submit_label'  => 'Process'
            )
        ));

        $this->setTemplate('usage.edit', 'LeepAdminBundle:CRUD:popup_form_no_tab.html.twig');
    }
}