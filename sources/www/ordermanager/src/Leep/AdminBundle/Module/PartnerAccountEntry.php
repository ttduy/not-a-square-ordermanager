<?php
namespace Leep\AdminBundle\Module;

class PartnerAccountEntry extends Crud {   
    public function load() { 
        parent::load();       

        $id = $this->container->get('request')->get('id', 0);

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.popup_page_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'list'            => 'Entries',
                'create'          => 'Add new entry'
            )
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Partner',
            'title'         => 'Account entry'
        ));


        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Entries',
            'filter'   => 'leep_admin.partner_account_entry.business.filter_handler',
            'reader'   => 'leep_admin.partner_account_entry.business.grid_reader'      
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:PartnerAccountEntry:list.html.twig');


        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.partner_account_entry.business.create_handler'
        ));     
        $this->setHookOptions('create', 'mainTabs', array(
            'selected' => 'create'
        ));   
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:popup_form.html.twig');


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit entry', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.partner_account_entry.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:popup_form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.partner_account_entry.business.delete_handler'
        ));       
    }
}
