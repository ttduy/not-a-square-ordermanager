<?php
namespace Leep\AdminBundle\Module;

class PriceCategory extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Price',
            'title'         => 'Price Category'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Price Categories',
            'create'        => 'Add new category'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Price Category',
            'reader'   => 'leep_admin.price_category.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.price_category.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit category', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.price_category.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.price_category.business.delete_handler'
        ));       


        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.edit',            
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.price_category.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy price category', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.edit', 'LeepAdminBundle:CRUD:form.html.twig');
    }
}
