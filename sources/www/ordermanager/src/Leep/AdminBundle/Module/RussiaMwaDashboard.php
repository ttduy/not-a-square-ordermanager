<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class RussiaMwaDashboard extends Base {   
    public function load() {
        parent::load();
        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'RussiaMWAProcessor',
            'title'         => 'Russia Mwa Dashboard'
        ));

        $this->addHook('main_tab', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'content' => array(
                    'list' => array('title' => 'Russia Mwa Dashboard', 'link' => '#')
                ),
                'selected' => 'list'
            )
        ));      

        $this->addFeature('mwa_dashboard', array(
            'class'           => 'leep_admin.feature.russia_mwa_dashboard',
            'options'         => array(
            )
        ));
        $this->setTemplate('mwa_dashboard.list', 'LeepAdminBundle:RussiaMwaDashboard:list.html.twig');
    }
}
