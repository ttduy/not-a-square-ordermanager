<?php
namespace Leep\AdminBundle\Module;

class CustomerColorFilter extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order color filter'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Order color filter',
            'create'        => 'Add new color filter'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order color filter',
            'reader'   => 'leep_admin.customer_color_filter.business.grid_reader'    
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.customer_color_filter.business.create_handler',
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit customer color filter', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.customer_color_filter.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.customer_color_filter.business.delete_handler'
        ));       
    }
}
