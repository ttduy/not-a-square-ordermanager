<?php
namespace Leep\AdminBundle\Module;

class FoodTableIngredient extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FoodTable',
            'title'         => 'Food Table - Ingredient'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Ingredients',
            'create'        => 'Add new ingredient'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Food Table - Ingredient',
            'reader'   => 'leep_admin.food_table_ingredient.business.grid_reader',
            'filter'   => 'leep_admin.food_table_ingredient.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.food_table_ingredient.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit ingredient', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.food_table_ingredient.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.food_table_ingredient.business.delete_handler'
        ));       
    }
}
