<?php
namespace Leep\AdminBundle\Module;

class EmailOutgoing extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailSystem',
            'title'         => 'EmailSystem'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email Outgoing',
            'create'        => 'Add new email outgoing'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email Outgoing',
            'reader'   => 'leep_admin.email_outgoing.business.grid_reader',
            'filter'   => 'leep_admin.email_outgoing.business.filter_handler'
        ));

        $this->setTemplate('grid.list', 'LeepAdminBundle:EmailOutgoing:gridview.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.email_outgoing.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:EmailOutgoing:form.html.twig');


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit email outgoing', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.email_outgoing.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:EmailOutgoing:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.email_outgoing.business.delete_handler'
        ));

        // BULK PROCESS
        $this->addFeature('bulk_process', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.email_outgoing.business.bulk_process_handler'
            )
        ));

        $this->addHook('bulk_process_hook', array(
            'class'           => 'leep_admin.email_outgoing.business.bulk_process_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_process'
        ));

        $this->setHookOptions('bulk_process', 'mainTabs', array(
            'selected' => 'edit'
        ));

        $this->setTemplate('bulk_process.edit', 'LeepAdminBundle:EmailOutgoing:bulk_process_form.html.twig');

        // VIEW
        $this->addFeature('view', array(
            'class'           => 'easy_crud.feature.view',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.email_outgoing.business.view_handler'
            )
        ));

        $this->setHookOptions('view', 'mainTabs', array(
            'selected' => 'view'
        ));

        $this->setTemplate('view.view', 'LeepAdminBundle:EmailOutgoing:log_view.html.twig');
    }
}
