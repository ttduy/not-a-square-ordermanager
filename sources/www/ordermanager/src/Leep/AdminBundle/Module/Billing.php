<?php
namespace Leep\AdminBundle\Module;

class Billing extends Base {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Billing',
            'title'         => 'Billing'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->addFeature('customer_pricing_grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'title'    => 'Customer pricing',
                'reader'   => 'leep_admin.billing.business.customer_pricing_grid_reader',
                'filter'   => 'leep_admin.billing.business.customer_pricing_filter_handler'
            )
        ));
        $this->setHookOptions('customer_pricing_grid', 'mainTabs', array(
            'content'  => array(
                'list' => array('title' => 'Customer pricing', 'link' => '#'),
            ),
            'selected' => 'list',
            'paginationDisplayLength' => 25
        ));
        $this->addHook('customerPricingHook', array(
            'class'           => 'leep_admin.billing.business.customer_pricing_hook',
            'point'           => 'hook_after',
            'features'        => 'customer_pricing_grid'
        ));

        $this->setTemplate('customer_pricing_grid.list', 'LeepAdminBundle:Billing:customer_pricing_list.html.twig');

        // EDIT PRICING
        $this->addHook('edit_pricing', array(
            'class'           => 'leep_admin.billing.business.edit_pricing_hook',
            'point'           => 'hook_after',
            'features'        => 'edit_pricing'
        ));
        $this->addFeature('edit_pricing', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.billing.business.edit_pricing_handler'
            )
        ));
        $this->setHookOptions('edit_pricing', 'mainTabs', array(
            'content'         => array(
                'edit' => array('title' => 'Edit pricing', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_pricing.edit', 'LeepAdminBundle:Billing:edit_pricing_form.html.twig');

        // BULK PROCESS
        $this->addFeature('bulk_process', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.billing.business.bulk_process_handler'
            )
        ));
        $this->addHook('bulk_process_hook', array(
            'class'           => 'leep_admin.billing.business.bulk_process_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_process'
        ));
        $this->setHookOptions('bulk_process', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Invoices', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('bulk_process.edit', 'LeepAdminBundle:Billing:bulk_process_form.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.billing',
            'options'         => array(
            )
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content' => array(),
            'selected' => 'list'
        ));
    }
}
