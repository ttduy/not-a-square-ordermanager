<?php
namespace Leep\AdminBundle\Module;

class SupplementType extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Material',
            'title'         => 'Supplement Types'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // set main tabs
        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array (
                'list'   => array('title' => 'Supplement Type', 'link' => $this->getUrl('feature', 'list')),
                'create' => array('title' => 'Add new supplement type', 'link' => $this->getUrl('create', 'create'))
            ),
            'selected' => ''
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Supplement type',
            'reader'   => 'leep_admin.supplement_type.business.grid_reader',
            'filter'   => 'leep_admin.supplement_type.business.filter_handler'  
        ));

        // CREATE
        $this->setHookOptions('create', 'mainTabs', array(
            'selected'      => 'create'
        ));

        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.supplement_type.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.supplement_type.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.supplement_type.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.supplement_type'
        ));

        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'      => 'list'
        ));

        // LIST OF MATERIAL USED UP - IN USE
        $this->addFeature('used_up_material_selected_grid', array(
            'class'        => 'easy_crud.feature.grid',
            'options'      => array(
                'id'       => 'AppForm',                   
                'title'    => 'Material Used Up - Selected',
                'reader'   => 'leep_admin.supplement_type.business.used_up_material_selected_grid_reader'
            )
        ));

        $this->setTemplate('feature.list', 'LeepAdminBundle:SupplementType:list.html.twig');
    }
}
