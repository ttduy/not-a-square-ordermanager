<?php
namespace Leep\AdminBundle\Module;

class OrderBackup extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Order Backup'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Order Backup',
            'create'        => 'Add new Order Backup'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order Backup',
            'filter'   => 'leep_admin.order_backup.business.filter_handler',
            'reader'   => 'leep_admin.order_backup.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.order_backup.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:OrderBackup:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Order Backup', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.order_backup.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.order_backup.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.order_backup',
            'options'         => array(
            )
        ));
    }
}
