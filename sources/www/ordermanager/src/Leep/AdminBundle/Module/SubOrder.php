<?php
namespace Leep\AdminBundle\Module;

class SubOrder extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'SubOrder',
            'title'         => 'Sub Order'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.sub_order_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Sub Order',
            'reader'   => 'leep_admin.sub_order.business.grid_reader',        
            'filter'   => 'leep_admin.sub_order.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                
            ),
            'selected'      => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:SubOrder:list.html.twig');   

        // EDIT STATUS
        $this->addFeature('edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.sub_order.business.edit_status_handler'
            )
        ));
        $this->setHookOptions('edit_status', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_status.edit', 'LeepAdminBundle:SubOrder:form_update_status.html.twig');   

        // EDIT BARCODE
        $this->addFeature('edit_barcode', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.sub_order.business.edit_barcode_handler'
            )
        ));
        $this->setHookOptions('edit_barcode', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_barcode.edit', 'LeepAdminBundle:SubOrder:form_update_barcode.html.twig');   

    }
}
