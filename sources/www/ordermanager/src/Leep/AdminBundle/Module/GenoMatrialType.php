 <?php
    namespace Leep\AdminBundle\Module;
     
    class GenoMaterialType extends Crud {
        public function load() {
            parent::load();
            // ALL
            $this->setHookOptions('*', 'header', array(
                'menuSelected' => 'GenoMaterialType',
                'title' => 'GenoMaterialType'
            ));
            $this->setHookOptions('*', 'mainTabs', array(
                'list' => 'Geno Material type',
                'create' => 'Add new Geno Material type'
            ));

            // LIST
            $this->setFeatureOptions('grid', array(
                'title' => 'Geno Material Type',
                'reader' => 'leep_admin.geno_material_type.business.grid_reader',
                'filter' => 'leep_admin.geno_material_type.business.filter_handler',
            ));

            // CREATE
            $this->setFeatureOptions('create', array(
                'handler' => 'leep_admin.geno_material_type.business.create_handler'
            ));

            // EDIT
            $this->setFeatureOptions('edit', array(
                'handler' => 'leep_admin.geno_material_type.business.edit_handler'
            ));

            // DELETE
            $this->setFeatureOptions('delete', array(
                'handler' => 'leep_admin.geno_material_type.business.delete_handler'
            ));
        }
    }
