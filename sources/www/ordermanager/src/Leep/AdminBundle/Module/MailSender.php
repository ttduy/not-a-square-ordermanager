<?php
namespace Leep\AdminBundle\Module;

class MailSender extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'MailSender',
            'title'         => 'Mail Sender'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.mail_sender_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.mail_sender'
        ));

        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.mail_sender.business.email_handler',
                'toRedirect'        => true,
                'toCreateFeature'   => 'email'
            )
        ));
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.mail_sender.business.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'
        ));
        $this->setHookOptions('email', 'mainTabs', array(
            'selected'      => 'create_email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:MailSender:email.html.twig');

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Email in Pool',
            'reader'   => 'leep_admin.mail_sender.business.grid_reader',
            'filter'   => 'leep_admin.mail_sender.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected'      => 'email_in_pool'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.mail_sender.business.view_handler'
        ));

        // GRID - EMAIL SENDING STATS
        $this->addFeature('email_sending_stats', array(
            'class'        => 'easy_crud.feature.grid',
            'options'      => array(
                'id'       => 'AppForm',                   
                'title'    => 'Email Sending Statistics',
                'reader'   => 'leep_admin.mail_sender.business.email_sending_statistics_grid_reader',
                'filter'   => 'leep_admin.mail_sender.business.email_sending_statistics_filter_handler'
            )
        ));
        $this->setHookOptions('email_sending_stats', 'mainTabs', array(
            'selected'      => 'email_sending_statistic'
        ));
        $this->addHook('email_sending_stats_hook', array(
            'class'           => 'leep_admin.mail_sender.business.email_sending_statistics_grid_reader_hook',
            'point'           => 'hook_after',
            'features'        => 'email_sending_stats'
        ));

        $this->setTemplate('email_sending_stats.list', 'LeepAdminBundle:MailSender:email_sending_statistic_list.html.twig');
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.mail_sender.business.delete_handler'
        ));       

        // BLACK LIST
        $this->addFeature('edit_blacklist', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.mail_sender.business.edit_blacklist'
            )
        ));
        $this->setHookOptions('edit_blacklist', 'mainTabs', array(
            'selected'      => 'edit_blacklist'
        ));
        $this->setTemplate('edit_blacklist.edit', 'LeepAdminBundle:MailSender:edit_blacklist.html.twig');
    }
}
