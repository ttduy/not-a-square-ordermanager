<?php
namespace Leep\AdminBundle\Module;

class Log extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Log',
            'title'         => 'Log'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.log.business.grid_reader',
                'filter'   => 'leep_admin.log.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Log', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');  

        // VIEW
        $this->addFeature('view', array(
            'class'           => 'easy_crud.feature.view',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.log.business.view_handler'
            )
        ));
        $this->setHookOptions('view', 'mainTabs', array(            
            'content'  => array(),
            'selected' => ''
        ));
        $this->setTemplate('view.view', 'LeepAdminBundle:CRUD:view.html.twig');        

    }
}
