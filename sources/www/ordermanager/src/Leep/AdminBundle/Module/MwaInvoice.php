<?php
namespace Leep\AdminBundle\Module;

class MwaInvoice extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'MWAProcessor',
            'title'         => 'MwaInvoice'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Mwa Invoice',
            'create'        => 'Add new mwa invoice'
        ));
        


        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Mwa Invoice',
            'reader'   => 'leep_admin.mwa_invoice.business.grid_reader',        
            'filter'   => 'leep_admin.mwa_invoice.business.filter_handler'
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new mwa invoice', 'link' =>  $this->getUrl('invoice_form', 'list'))
            ),
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.mwa_invoice.business.create_handler'
        ));        


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit mwa invoice', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.mwa_invoice.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.mwa_invoice.business.delete_handler'
        ));       

        // MWA INVOICE FORM
        $this->addHook('invoice_form', array(
            'class'           => 'leep_admin.hook.mwa_invoice_form',
            'point'           => 'hook_after',
            'features'        => 'invoice_form invoice_form_edit'
        ));
        $this->addFeature('invoice_form', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Mwa Invoice',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.mwa_invoice_form.business.grid_reader',        
                'filter'   => 'leep_admin.mwa_invoice_form.business.filter_handler'                
            )
        ));        
        
        $this->setHookOptions('invoice_form', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new mwa invoice', 'link' =>  $this->getUrl('invoice_form', 'list'))
            ),
            'selected'   => 'create'
        ));
        $this->setTemplate('invoice_form.list', 'LeepAdminBundle:MwaInvoiceForm:form_list.html.twig');


        // MWA INVOICE FORM EDIT
        $this->addFeature('invoice_form_edit', array(
            'class'           => 'leep_admin.feature.mwa_invoice',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Object',
                'title'    => 'Mwa Invoice',
                'filter.visiblity' => 'show',
                'reader'   => 'leep_admin.mwa_invoice_form.business.edit_grid_reader',        
                'filter'   => 'leep_admin.mwa_invoice_form.business.edit_filter_handler'                
            )
        ));        
        $this->setHookOptions('invoice_form_edit', 'mainTabs', array(
            'extra'         => array(
                'create' => array('title' => 'Add new mwa invoice', 'link' =>  $this->getUrl('invoice_form', 'list')),
                'edit'   => array('title' => 'Edit mwa invoice', 'link' => '#')
            ),
            'selected'   => 'edit'
        ));
        $this->setTemplate('invoice_form_edit.list', 'LeepAdminBundle:MwaInvoiceForm:form_edit_list.html.twig');
    }
}
