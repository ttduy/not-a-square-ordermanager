<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Base extends AbstractModule {   
    public function load() {    
        $this->addHook('header', array(
            'class'           => 'leep_admin.hook.header',
            'point'           => 'hook_before',
            'features'        => '*'  
        ));

        $this->addHook('security', array(
            'class'           => 'leep_admin.hook.security',
            'point'           => 'hook_before',
            'features'        => '*'  
        ));      

        $this->addHook('log', array(
            'class'           => 'leep_admin.hook.log',
            'point'           => 'hook_before',
            'features'        => '*'  
        )); 

        $this->addHook('footer', array(
            'class'           => 'leep_admin.hook.footer',
            'point'           => 'hook_after',
            'features'        => '*'
        ));
    }
}
