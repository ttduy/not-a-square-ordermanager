<?php
namespace Leep\AdminBundle\Module;

class Drug extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*','header',array(
            'menuSelected'  => 'Drug',
            'title'         => 'Drug'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Drug',
            'create'        => 'Add new Drug'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Drug',
            'reader'   => 'leep_admin.drug.business.grid_reader',
            'filter'   => 'leep_admin.drug.business.filter_handler'
        ));


        // CREATE
        $this->setFeatureOptions('create', array(
            'handler' => 'leep_admin.drug.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Drug', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.drug.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.drug.business.delete_handler'
        ));
    }
}
