<?php
namespace Leep\AdminBundle\Module;

class CmsOrderStatusValue extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Order Status'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Status',
            'create'        => 'Add new status'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Status',
            'reader'   => 'leep_admin.cms_order_status_value.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_order_status_value.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_order_status_value.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_order_status_value.business.delete_handler'
        ));       
    }
}
