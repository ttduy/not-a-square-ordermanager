<?php
namespace Leep\AdminBundle\Module;

class Invoice extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CollectiveInvoice',
            'title'         => 'Invoice'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Invoice',
            'create'        => 'Add new invoice'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Invoice',
            'reader'   => 'leep_admin.invoice.business.grid_reader',
            'filter'   => 'leep_admin.invoice.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.invoice.business.create_handler',
            'toEdit'        => true,
            'toEditFeature' => 'edit',
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit invoice', 'link' => '#')
            ),
            'selected'      => 'edit',
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.invoice.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Invoice:edit.html.twig');

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.invoice.business.view_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.invoice.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.invoice',
            'options'         => array()
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
    }
}
