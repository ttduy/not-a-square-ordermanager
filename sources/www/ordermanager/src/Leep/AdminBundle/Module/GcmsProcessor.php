<?php
namespace Leep\AdminBundle\Module;

class GcmsProcessor extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GCMSProcessor',
            'title'         => 'GcmsProcessor'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // EDIT
        $this->addFeature('create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',                   
                'handler'       => 'leep_admin.gcms_processor.business.create_handler'
            )
        ));
        $this->setHookOptions('create', 'mainTabs', array(          
            'content'  => array(
                'create' => array('title' => 'GCMS Processor', 'link' => '#'),
            ),
            'selected' => 'create'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:GcmsProcessor:form.html.twig');        
    }
}
