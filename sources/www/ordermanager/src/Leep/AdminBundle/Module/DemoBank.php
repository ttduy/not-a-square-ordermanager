<?php
namespace Leep\AdminBundle\Module;

class DemoBank extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Plate',
            'title'         => 'DemoBank'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'DemoBank',
            'create'        => 'Add new DemoBank'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'DemoBank',
            'filter'   => 'leep_admin.demo_bank.business.filter_handler',
            'reader'   => 'leep_admin.demo_bank.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.demo_bank.business.create_handler'
        ));
        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.demo_bank.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:DemoBank:form.html.twig');


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit DemoBank', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.demo_bank.business.edit_handler'
        ));
        $this->addHook('edit_hook', array(
            'class'           => 'leep_admin.demo_bank.business.edit_hook',
            'point'           => 'hook_after',
            'features'        => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:DemoBank:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.demo_bank.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.demo_bank',
            'options'         => array(
            )
        ));
    }
}
