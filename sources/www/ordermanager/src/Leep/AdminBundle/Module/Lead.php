<?php
namespace Leep\AdminBundle\Module;

class Lead extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Lead',
            'title'         => 'Lead'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Lead',
            'create'        => 'Add new lead'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Lead',
            'reader'   => 'leep_admin.lead.business.grid_reader',
            'filter'   => 'leep_admin.lead.business.filter_handler',
        ));
        $this->addHook('grid_hook', array(
            'class'           => 'leep_admin.lead.business.grid_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Lead:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.lead.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Lead:form.html.twig');

        //create hook
        $this->addHook('create_hook', array(
            'class'           => 'leep_admin.lead.business.create_hook',
            'point'           => 'hook_after',
            'features'        => 'create'
        ));

        //edit hook
        $this->addHook('edit_hook', array(
            'class'           => 'leep_admin.lead.business.edit_hook',
            'point'           => 'hook_after',
            'features'        => 'edit'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit lead', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.lead.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Lead:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.lead.business.delete_handler'
        ));

        // Bulk convert Lead to DC
        $this->addFeature('bulk_convert_lead_to_dc', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.lead.business.bulk_convert_lead_to_dc_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setTemplate('bulk_convert_lead_to_dc.edit', 'LeepAdminBundle:Lead:bulk_convert_lead_to_dc.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class' => 'leep_admin.feature.lead'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'  => ''
        ));
    }
}
