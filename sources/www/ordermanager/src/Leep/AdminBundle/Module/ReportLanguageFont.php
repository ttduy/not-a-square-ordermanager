<?php
namespace Leep\AdminBundle\Module;

class ReportLanguageFont extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Language fonts'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Language fonts',
            'reader'   => 'leep_admin.report_language_font.business.grid_reader'
        ));

        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Language fonts', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        
        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'content'  => array(
                'list' => array('title' => 'Language fonts', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit language fonts', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.report_language_font.business.edit_handler'
        ));
    }
}
