<?php
namespace Leep\AdminBundle\Module;

class ReportLocalization extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Report'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Localization',
            'reader'   => 'leep_admin.report_localization.business.grid_reader',
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Localization', 'link' => '#'),
            ),
            'selected' => 'list'
        ));


        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'content'  => array(
                'list' => array('title' => 'Localization', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit localization', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.report_localization.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.report_localization.business.delete_handler'
        ));       
    }
}
