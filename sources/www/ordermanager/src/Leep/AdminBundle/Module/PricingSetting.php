<?php
namespace Leep\AdminBundle\Module;

class PricingSetting extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Billing',
            'title'         => 'Auto pricing setting'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'asdas',
            'create'        => 'Add new',
            'disable'       => ['create', 'list']
        ));


        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => ''
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');
        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Auto pricing setting', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.pricing_setting.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');

    }
}
