<?php
namespace Leep\AdminBundle\Module;

class Language extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Language',
            'title'         => 'Language'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Languages',
            'create'        => 'Add new language'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Language',
            'reader'   => 'leep_admin.language.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.language.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit language', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.language.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.language.business.delete_handler'
        ));       
    }
}
