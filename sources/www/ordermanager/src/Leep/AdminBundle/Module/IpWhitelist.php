<?php
namespace Leep\AdminBundle\Module;

class IpWhitelist extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'IpLog',
            'title'         => 'Ip Whitelist'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'IP Whitelist',
            'create'        => 'Add IP whitelist'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'IP Whitelist',
            'reader'   => 'leep_admin.ip_whitelist.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.ip_whitelist.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit IP Whitelist', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.ip_whitelist.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.ip_whitelist.business.delete_handler'
        ));       
    }
}
