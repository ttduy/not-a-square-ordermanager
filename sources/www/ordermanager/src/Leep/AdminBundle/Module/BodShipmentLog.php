<?php
namespace Leep\AdminBundle\Module;

class BodShipmentLog extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'BOD shipment log'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'BOD shipment log',
            'create'        => 'Add new',
            'disable'       => ['create']
        ));
        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Bod shipment log',
            'filter'   => 'leep_admin.bod_shipment_log.business.filter_handler',
            'reader'   => 'leep_admin.bod_shipment_log.business.grid_reader'
        ));

    }
}
