<?php
namespace Leep\AdminBundle\Module;

class ScanFormTemplate extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanFormTemplate',
            'title'         => 'Scan Form Template'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Scan Form Templates',
            'create'        => 'Add new Scan Form Template'
        ));
        $this->addHook('scan_form_template', array(
            'class'           => 'leep_admin.scan_form_template.hook.scan_form_template',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Scan Form Template',
            'filter'   => 'leep_admin.scan_form_template.business.filter_handler',
            'reader'   => 'leep_admin.scan_form_template.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.scan_form_template.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Scan Form Template', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.scan_form_template.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:ScanFormTemplate:edit.html.twig');

        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_form_template.business.delete_handler'
        ));       


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.scan_form_template',
            'options'         => array(
            )
        ));
    }
}
