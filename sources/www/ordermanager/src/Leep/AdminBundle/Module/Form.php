<?php
namespace Leep\AdminBundle\Module;

class Form extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Form',
            'title'         => 'Form'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Forms',
            'create'        => 'Add new form'
        ));
        $this->addHook('form', array(
            'class'           => 'leep_admin.hook.form',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Form',
            'reader'   => 'leep_admin.form.business.grid_reader',
            'filter'   => 'leep_admin.form.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Form:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'toEdit'        => true,
            'toEditFeature' => 'edit_step',
            'handler'       => 'leep_admin.form.business.create_handler'
        ));

        // EDT SECTION
        $this->addFeature('edit_step', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.form.business.edit_step_handler'
            )
        ));
        $this->setHookOptions('edit_step', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit step', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setTemplate('edit_step.edit', 'LeepAdminBundle:Form:edit_step.html.twig');

        // EDT ITEM
        $this->addFeature('edit_item', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.form.business.edit_item_handler'
            )
        ));
        $this->setHookOptions('edit_item', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit item', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setTemplate('edit_item.edit', 'LeepAdminBundle:Form:edit_item.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.form.business.delete_handler'
        ));


        // LIST SESSION
        $this->addFeature('form_session_grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'   => 'leep_admin.form.business.form_session_grid_reader',
                'filter'   => 'leep_admin.form.business.form_session_filter_handler',
            )
        ));
        $this->setHookOptions('form_session_grid', 'mainTabs', array(
            'extra'  => array(
                'list' => array('title' => 'Form Session', 'link' => '#')
            ),
            'disabled' => array('create'),
            'selected' => 'list'
        ));
        $this->setTemplate('form_session_grid.list', 'LeepAdminBundle:CRUD:list.html.twig');


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.form'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected'      => 'form'
        ));

        // Edit Form Note
        $this->addFeature('edit_note', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.form.business.edit_note_handler'
            )
        ));
        $this->setHookOptions('edit_note', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit note', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_note.edit', 'LeepAdminBundle:Form:edit_note.html.twig');

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.form.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy Form', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EMAIL
        $this->addFeature('create_version_log', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.form.business.create_version_log_handler',
                'submit_label'  => 'Create'
            )
        ));
        $this->setTemplate('create_version_log.create', 'LeepAdminBundle:Form:create_version_log.html.twig');
    }
}
