<?php  
namespace Leep\AdminBundle\Module;

class SupplierOffer extends Crud {
    public function load() {
        parent::load();
        // REGISTER
        $this->setHookOptions('*', 'header', array(
            'menuSelected' => 'Material',
            'title'        => 'Supplier Offer'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'         => 'Supplier Offer',
            'create'       => 'Add new Supplier Offer'
        ));

        // CREATE 
        $this->setFeatureOptions('create', array(
            'handler'      => 'leep_admin.supplier_offer.business.create_handler'
        ));

        // LIST
        $this->setFeatureOptions('grid', 
            array('title' => 'Suppler Offer',
                'reader' => 'leep_admin.supplier_offer.business.grid_reader',
                'filter' => 'leep_admin.supplier_offer.business.filter_handler'
            )
        );

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit supplier offer', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit',
            array('handler'=>'leep_admin.supplier_offer.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', 
            array('handler'=> 'leep_admin.supplier_offer.business.delete_handler'
            )
        );
    }
}
