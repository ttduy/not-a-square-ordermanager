<?php
namespace Leep\AdminBundle\Module;

class IntegratedInvoice extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'BillingCockpit',
            'title'         => 'Invoices'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addHook('invoiceStatistic', array(
            'class'           => 'leep_admin.integrated_invoice.business.statistic_hook',
            'point'           => 'hook_before',
            'features'        => 'grid'
        ));
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.integrated_invoice.business.grid_reader',
                'filter'   => 'leep_admin.integrated_invoice.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Invoices', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:IntegratedInvoice:list.html.twig');  
        
        // EDIT STATUS
        $this->addFeature('edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.integrated_invoice.business.edit_status_handler'
            )
        ));
        $this->setHookOptions('edit_status', 'mainTabs', array(          
            'content'         => array(
                'list' => array('title' => 'Invoices', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_status.edit', 'LeepAdminBundle:CRUD:form.html.twig');       

        // BULK EDIT STATUS
        $this->addFeature('bulk_edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.integrated_invoice.business.bulk_edit_status_handler'
            )
        ));
        $this->addHook('bulk_edit_status_hook', array(
            'class'           => 'leep_admin.integrated_invoice.business.bulk_edit_status_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_status'  
        )); 
        $this->setHookOptions('bulk_edit_status', 'mainTabs', array(          
            'content'         => array(
                'list' => array('title' => 'Invoices', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('bulk_edit_status.edit', 'LeepAdminBundle:IntegratedInvoice:bulk_edit_status.html.twig');      
    }
}
