<?php
namespace Leep\AdminBundle\Module;

class FoodTableSmileyConversion extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FoodTable',
            'title'         => 'Food Table - Smiley Conversion'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Smiley conversions',
            'create'        => 'Add new conversion rule'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Food Table - Smiley conversion',
            'reader'   => 'leep_admin.food_table_smiley_conversion.business.grid_reader'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.food_table_smiley_conversion.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit conversion rule', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.food_table_smiley_conversion.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.food_table_smiley_conversion.business.delete_handler'
        ));       
    }
}
