<?php
namespace Leep\AdminBundle\Module;

class LeadStatusValue extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Lead',
            'title'         => 'Lead status'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Lead status',
            'create'        => 'Add new lead status'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Lead status',
            'reader'   => 'leep_admin.lead_status_value.business.grid_reader'       
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.lead_status_value.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit lead status', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.lead_status_value.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.lead_status_value.business.delete_handler'
        ));       
    }
}
