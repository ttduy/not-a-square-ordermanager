<?php
namespace Leep\AdminBundle\Module;

class Currency extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Currency',
            'title'         => 'Currency'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Currency',
            'create'        => 'Add new currency'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Currency',
            'filter'   => 'leep_admin.currency.business.filter_handler',
            'reader'   => 'leep_admin.currency.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.currency.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit currency', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.currency.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.currency.business.delete_handler'
        ));       


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.currency',
            'options'         => array(
            )
        ));
    }
}
