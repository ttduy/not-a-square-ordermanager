<?php
namespace Leep\AdminBundle\Module;

class Search extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => '',
            'title'         => 'Search'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Search',
            'create'        => 'Add new',
            'disable'       => ['create']
        ));
        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Search',
            'filter'   => 'leep_admin.search.business.filter_handler',
            'reader'   => 'leep_admin.search.business.grid_reader'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Search:list.html.twig');
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.search'
        ));

    }
}
