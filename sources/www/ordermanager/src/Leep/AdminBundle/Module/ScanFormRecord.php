<?php
namespace Leep\AdminBundle\Module;

class ScanFormRecord extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanFormData',
            'title'         => 'Scan form record'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.scan_form_record.business.grid_reader',
                'filter'   => 'leep_admin.scan_form_record.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Scan form record', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig');  

        // EDIT 
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.scan_form_record.business.edit_handler'
            )
        ));
        $this->setHookOptions('edit', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Scan form record', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:ScanFormRecord:edit.html.twig');       

    }
}
