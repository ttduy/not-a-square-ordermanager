<?php
namespace Leep\AdminBundle\Module;

class Notification extends Base { 
    public function load() {
        parent::load();
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Notification',
            'title'         => 'Notification'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.notification_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.notification.business.grid_reader',
                'filter'   => 'leep_admin.notification.business.filter_handler',
            )
        ));

        $this->setHookOptions('grid', 'mainTabs', array(    
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:CRUD:list.html.twig'); 
    }
}
