<?php
namespace Leep\AdminBundle\Module;

class GenoStorage extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GenoStorage',
            'title'         => 'GenoStorage'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Storages',
            'create'        => 'Add new Storage'
        ));

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.geno_storage'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Geno Storages',
            'reader'   => 'leep_admin.geno_storage.business.grid_reader',
            'filter'   => 'leep_admin.geno_storage.business.filter_handler'
        ));

        $this->addHook('create_hook', array(
            'class'     => 'leep_admin.geno_storage.business.create_hook',
            'point'     => 'hook_after',
            'features'  => 'grid'
        ));

        $this->setHookOptions('gird', 'maintabs', array(
            'selected' => 'grid'
        ));

        $this->setTemplate('grid.list', 'LeepAdminBundle:GenoStorage:gridview.html.twig');


        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.geno_storage.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Storage', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.geno_storage.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.geno_storage.business.delete_handler'
        ));
    }
}
