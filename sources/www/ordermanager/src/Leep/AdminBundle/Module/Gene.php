<?php
namespace Leep\AdminBundle\Module;

class Gene extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Product',
            'title'         => 'Gene'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Genes',
            'create'        => 'Add new gene'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Gene',
            'reader'   => 'leep_admin.gene.business.grid_reader',
            'filter'   => 'leep_admin.gene.business.filter_handler',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.gene.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit gene', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.gene.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.gene.business.view_handler'
        ));        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.gene.business.delete_handler'
        ));       
    }
}
