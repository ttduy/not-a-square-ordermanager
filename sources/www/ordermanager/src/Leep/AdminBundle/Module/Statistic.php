<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Statistic extends Base {   
    public function load() {
        parent::load();
        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Statistics',
            'title'         => 'Statistics'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // set main tabs
        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array (
                'summary'             => array('title' => 'Summary',              
                                                'link' => $this->getUrl('statistic', 'summary')),
            ),
            'selected' => ''
        ));

        // Statistic feature
        $this->addFeature('statistic', array(
            'class'           => 'leep_admin.feature.statistic',
            'options'         => array(
            )
        ));

        $this->setTemplate('statistic.summary', 'LeepAdminBundle:Statistic:summary.html.twig');
    }
}
