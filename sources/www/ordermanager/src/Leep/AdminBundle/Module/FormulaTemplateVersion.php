<?php
namespace Leep\AdminBundle\Module;

class FormulaTemplateVersion extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FormulaTemplate',
            'title'         => 'FormulaTemplate'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs_formula_template_version',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected'      => 'list'
        ));
        $this->setFeatureOptions('grid', array(
            'title'    => 'Formula template version',
            'reader'   => 'leep_admin.formula_template_version.business.grid_reader',      
            'filter'   => 'leep_admin.formula_template_version.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:FormulaTemplateVersion:list.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit formula template version', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.formula_template_version.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:FormulaTemplateVersion:edit.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.formula_template_version.business.delete_handler'
        ));       
    }
}
