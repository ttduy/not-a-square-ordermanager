<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Email extends Base {   
    public function load() {
        parent::load();
        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Email',
            'title'         => 'Email'
        ));

        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.email.hook.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'  
        ));  

        // FEATURE - EMAIL
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.email.business.create_handler',
                'submit_label' => 'Send'
            )
        ));
        $this->setHookOptions('email', 'mainTabs', array(
            'selected' => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:Email:email.html.twig');
    }
}
