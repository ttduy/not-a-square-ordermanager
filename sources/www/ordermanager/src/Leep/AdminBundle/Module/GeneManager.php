<?php
namespace Leep\AdminBundle\Module;

class GeneManager extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'GeneManager',
            'title'         => 'Gene Manager'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Gene Manager',
            'reader'   => 'leep_admin.gene_manager.business.grid_reader',        
            'filter'   => 'leep_admin.gene_manager.business.filter_handler'
        ));

        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Gene Manager', 'link' => '#'),
            ),
            'selected' => 'list'
        ));

        $this->setTemplate('grid.list', 'LeepAdminBundle:GeneManager:list.html.twig');

        // EDIT RESULT
        $this->addFeature('edit_result', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.gene_manager.business.edit_result_handler'
            )
        ));
        $this->setHookOptions('edit_result', 'mainTabs', array(
            'content'         => array(
                'edit' => array('title' => 'Edit result', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_result.edit', 'LeepAdminBundle:GeneManager:edit_result.html.twig');   

        // Submit RESULT
        $this->addFeature('submit_result', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.gene_manager.business.submit_result_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('submit_result', 'mainTabs', array(
            'content'         => array(
                'create' => array('title' => 'Submit result', 'link' => '#')
            ), 
            'selected' => 'create'
        ));
        $this->addHook('submit_result_hook', array(
            'class'           => 'leep_admin.gene_manager.business.submit_result_hook',
            'point'           => 'hook_after',
            'features'        => 'submit_result'  
        )); 
        $this->setTemplate('submit_result.create', 'LeepAdminBundle:GeneManager:submit_result.html.twig');   

        // LIST - RESULT HISTORY        
        $this->addFeature('grid_result_history', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'title'         => 'Result history',
                'reader'        => 'leep_admin.gene_manager.business.result_history_grid_reader',
                'filter'        => 'leep_admin.gene_manager.business.result_history_filter_handler'
            )
        ));        
        $this->setHookOptions('grid_result_history', 'mainTabs', array(          
            'content'  => array(),
            'selected' => ''
        ));        
        $this->setTemplate('grid_result_history.list', 'LeepAdminBundle:GeneManager:list_result_history.html.twig');       

        // DELETE - RESULT HISTORY        
        $this->addFeature('delete_result_history', array(
            'class'           => 'easy_crud.feature.delete',
            'options'         => array(
                'redirectFeature' => 'grid_result_history',
                'handler'       => 'leep_admin.gene_manager.business.result_history_delete_handler'
            )
        ));        
        $this->setHookOptions('delete_result_history', 'mainTabs', array(          
            'content'  => array(),
            'selected' => ''
        ));        

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.gene_manager'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content'       => array(),
            'selected'      => ''
        ));
    }
}
