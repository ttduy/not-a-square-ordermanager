<?php
namespace Leep\AdminBundle\Module;

class ReportQueue extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Report queue'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.report_queue.business.grid_reader',
                'filter'   => 'leep_admin.report_queue.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Report queue', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:ReportQueue:list.html.twig');   

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.report_queue'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content'       => array(),
            'selected'      => ''
        ));
    }
}
