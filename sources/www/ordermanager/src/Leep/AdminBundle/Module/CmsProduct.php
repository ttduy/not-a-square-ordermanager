<?php
namespace Leep\AdminBundle\Module;

class CmsProduct extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Product'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Product',
            'create'        => 'Add new product'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Product',
            'reader'   => 'leep_admin.cms_product.business.grid_reader',
            'filter'   => 'leep_admin.cms_product.business.filter_handler'  
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_product.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit product', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_product.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_product.business.delete_handler'
        ));       
    }
}
