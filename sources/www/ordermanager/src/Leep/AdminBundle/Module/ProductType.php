<?php
namespace Leep\AdminBundle\Module;

class ProductType extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Product',
            'title'         => 'Product Type'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Product Types',
            'create'        => 'Add new Product Type'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Product Types',
            'reader'   => 'leep_admin.product_type.business.grid_reader',
            'filter'   => 'leep_admin.product_type.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.product_type.business.create_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit product Type', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.product_type.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.product_type.business.delete_handler'
        ));

    }
}
