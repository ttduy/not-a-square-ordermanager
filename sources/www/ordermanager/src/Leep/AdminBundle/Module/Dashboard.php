<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Dashboard extends Base {
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Dashboard',
            'title'         => 'Dashboard'
        ));

        $this->addHook('main_tab', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*',
            'options'         => array(
                'content' => array(
                    'list' => array('title' => 'Dashboard', 'link' => '#')
                ),
                'selected' => 'list'
            )
        ));

        // FEATURE - EMAIL
        $this->addFeature('dashboard', array(
            'class'           => 'leep_admin.feature.dashboard',
            'options'         => array(
        )));
        $this->setTemplate('dashboard.list', 'LeepAdminBundle:Dashboard:list.html.twig');
        $this->setTemplate('dashboard.listPendingOrder', 'LeepAdminBundle:Dashboard:list.html.twig');
        $this->setTemplate('dashboard.seeCustomer', 'LeepAdminBundle:Dashboard:list.html.twig');
    }
}
