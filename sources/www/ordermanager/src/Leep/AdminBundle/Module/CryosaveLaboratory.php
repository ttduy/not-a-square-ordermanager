<?php
namespace Leep\AdminBundle\Module;

class CryosaveLaboratory extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Cryosave',
            'title'         => 'Cryosave laboratory'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Cryosave laboratory',
            'create'        => 'Add new laboratory'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Cryosave laboratory',
            'reader'   => 'leep_admin.cryosave_laboratory.business.grid_reader',    
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cryosave_laboratory.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit laboratory', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cryosave_laboratory.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cryosave_laboratory.business.delete_handler'
        ));     
    }
}
