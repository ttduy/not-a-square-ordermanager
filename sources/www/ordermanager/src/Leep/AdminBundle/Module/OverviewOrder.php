<?php
namespace Leep\AdminBundle\Module;

class OverviewOrder extends Crud {
    public function load() {
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Rating',
            'title'         => 'Rating'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Rating',
            'create'        => 'Add new Rating'
        ));

        // OVERVIEW ORDER PROCESS
        $this->addFeature('overview', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.overview_order.business.overview_handler'
            )
        ));

        $this->addHook('overview_hook', array(
            'class'           => 'leep_admin.overview_order.business.overview_hook',
            'point'           => 'hook_after',
            'features'        => 'overview'
        ));

        $this->setHookOptions('overview', 'mainTabs', array(
            'selected' => 'edit'
        ));

        $this->setTemplate('overview.edit', 'LeepAdminBundle:OverviewOrder:view.html.twig');
    }
}