<?php
namespace Leep\AdminBundle\Module;

class CmsOrder extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Order'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Order',
            'create'        => 'Add new order'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Order',
            'reader'   => 'leep_admin.cms_order.business.grid_reader',
            'filter'   => 'leep_admin.cms_order.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_order.business.create_handler'
        ));

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit order', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_order.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_order.business.delete_handler'
        ));


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.cms_order',
            'options'         => array(
            )
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content' => array(),
            'selected' => 'list'
        ));
    }
}
