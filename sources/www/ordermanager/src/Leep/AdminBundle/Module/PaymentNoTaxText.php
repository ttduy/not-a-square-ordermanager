<?php
namespace Leep\AdminBundle\Module;

class PaymentNoTaxText extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Billing',
            'title'         => 'Payment No Tax Text'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Payment No Tax Text',
            'create'        => 'Add new Payment No Tax Text'
        ));
        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Payment No Tax Text',
            'reader'   => 'leep_admin.payment_no_tax_text.business.grid_reader',
            'filter'   => 'leep_admin.payment_no_tax_text.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.payment_no_tax_text.business.create_handler'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit tax text', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));

        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.payment_no_tax_text.business.edit_handler'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.payment_no_tax_text.business.delete_handler'
        ));
    }
}
