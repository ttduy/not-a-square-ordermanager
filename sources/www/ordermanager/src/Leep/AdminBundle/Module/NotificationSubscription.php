<?php
namespace Leep\AdminBundle\Module;

class NotificationSubscription extends Crud {   
    public function load() { 
        parent::load();

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Notification',
            'title'         => 'Notification'
        ));
        
        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.notification_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.notification_subscription.business.create_handler',
            'submit_label'  => 'Save'
        ));

        $this->setHookOptions('create', 'mainTabs', array( 
            'selected' => 'subscribe'
        ));

        $this->setTemplate('create.create', 'LeepAdminBundle:NotificationSubscription:form.html.twig');
    }
}
