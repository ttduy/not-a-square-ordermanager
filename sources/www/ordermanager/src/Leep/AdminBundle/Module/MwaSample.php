<?php
namespace Leep\AdminBundle\Module;

class MwaSample extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'MWAProcessor',
            'title'         => 'MWA Sample'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Sample',
            'create'        => 'Add new sample'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Sample',
            'reader'   => 'leep_admin.mwa_sample.business.grid_reader',
            'filter'   => 'leep_admin.mwa_sample.business.filter_handler'
        ));        
        $this->addHook('gridPagination', array(
            'class'           => 'leep_admin.hook.grid_pagination',
            'point'           => 'hook_before',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:MwaSample:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.mwa_sample.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:MwaSample:create.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit sample', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.mwa_sample.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:MwaSample:edit.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.mwa_sample.business.delete_handler'
        ));       
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.mwa_sample',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));


        // EDIT COMPLAINTS
        $this->addFeature('edit_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.mwa_sample.business.edit_complaint_handler'
            )
        ));
        $this->setHookOptions('edit_complaint', 'mainTabs', array(          
            'extra'         => array(
                'edit' => array('title' => 'Edit complaint', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_complaint.edit', 'LeepAdminBundle:CRUD:form.html.twig');
    }
}
