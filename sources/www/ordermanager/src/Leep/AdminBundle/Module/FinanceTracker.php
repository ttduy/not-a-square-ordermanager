<?php
namespace Leep\AdminBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class FinanceTracker extends Base {   
    public function load() {
        parent::load();
        
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FinanceTracker',
            'title'         => 'Finance Tracker'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // set main tabs
        $this->setHookOptions('*', 'mainTabs', array(
            'content' => array (
                'summary'             => array('title' => 'Summary',              'link' => $this->getUrl('finance_tracker', 'summary')),
                'distributionChannel' => array('title' => 'Distribution Channel', 'link' => $this->getUrl('finance_tracker', 'distributionChannel')),
                'partner'             => array('title' => 'Partner',              'link' => $this->getUrl('finance_tracker', 'partner')),
                'amway'               => array('title' => 'AMWAY',                'link' => $this->getUrl('finance_tracker', 'amway')),
                'costTracker'         => array('title' => 'Cost Tracker',         'link' => $this->getUrl('finance_tracker', 'costTracker')),
                'cashIn'              => array('title' => 'Cash In',              'link' => $this->getUrl('cash_in_grid', 'list')),
                'cashOut'             => array('title' => 'Cash Out',             'link' => $this->getUrl('cash_out_grid', 'list'))
            ),
            'selected' => ''
        ));

        // FINANCE TRACKER
        $this->addFeature('finance_tracker', array(
            'class'           => 'leep_admin.feature.finance_tracker',
            'options'         => array(
            )
        ));

        // EDIT
        $this->addFeature('update_cost_tracker_data', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',                   
                'handler'       => 'leep_admin.finance_tracker.business.edit_cost_tracker_data_handler'
            )
        ));

        $this->addHook('update_cost_tracker_data_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.edit_cost_tracker_data_hook',
            'point'           => 'hook_after',
            'features'        => 'update_cost_tracker_data'  
        ));

        // CASH IN - LIST
        $this->addFeature('cash_in_grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'title'    => 'Cash In',
                'reader'   => 'leep_admin.finance_tracker.business.cash_in_grid',
                'filter'   => 'leep_admin.finance_tracker.business.cash_in_grid_filter_handler'
            )
        ));

        $this->setHookOptions('cash_in_grid', 'mainTabs', array(          
            'selected' => 'cashIn',
            'paginationDisplayLength' => 25
        ));

        $this->addHook('cash_in_grid_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.cash_in_grid_hook',
            'point'           => 'hook_after',
            'features'        => 'cash_in_grid'  
        ));

        // CASH IN - CREATE
        $this->addFeature('cash_in_create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.cash_in_create_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('cash_in_create', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Cash In - List', 'link' => $this->getUrl('cash_in_grid', 'list')),
                'create' => array('title' => 'Cash In - Add', 'link' => '#')
            ), 
            'selected' => 'create'
        ));

        // CASH IN - EDIT
        $this->addFeature('cash_in_edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.cash_in_edit_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('cash_in_edit', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Cash In - List', 'link' => $this->getUrl('cash_in_grid', 'list')),
                'edit' => array('title' => 'Cash In - Edit', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));        

        // CASH IN - DELETE
        $this->addFeature('cash_in_delete', array(
            'class'           => 'easy_crud.feature.delete',
            'options'         => array(
                'redirectFeature' => 'cash_in_grid',
                'handler'       => 'leep_admin.finance_tracker.business.cash_in_delete_handler'
            )
        ));

        // CASH OUT LIST
        $this->addFeature('cash_out_grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'title'    => 'Cash In',
                'reader'   => 'leep_admin.finance_tracker.business.cash_out_grid',
                'filter'   => 'leep_admin.finance_tracker.business.cash_out_grid_filter_handler'
            )
        ));

        $this->setHookOptions('cash_out_grid', 'mainTabs', array(          
            'selected' => 'cashOut',
            'paginationDisplayLength' => 25
        ));

        $this->addHook('cash_out_grid_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.cash_out_grid_hook',
            'point'           => 'hook_after',
            'features'        => 'cash_out_grid'  
        ));

        // CASH OUT - CREATE
        $this->addFeature('cash_out_create', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.cash_out_create_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('cash_out_create', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Cash Out - List', 'link' => $this->getUrl('cash_out_grid', 'list')),
                'create' => array('title' => 'Cash Out - Add', 'link' => '#')
            ), 
            'selected' => 'create'
        ));

        // CASH OUT - EDIT
        $this->addFeature('cash_out_edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.cash_out_edit_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('cash_out_edit', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Cash Out - List', 'link' => $this->getUrl('cash_out_grid', 'list')),
                'edit' => array('title' => 'Cash Out - Edit', 'link' => '#')
            ), 
            'selected' => 'edit'
        ));        

        // CASH OUT - DELETE
        $this->addFeature('cash_out_delete', array(
            'class'           => 'easy_crud.feature.delete',
            'options'         => array(
                'redirectFeature' => 'cash_out_grid',
                'handler'       => 'leep_admin.finance_tracker.business.cash_out_delete_handler'
            )
        ));

        // Submit Cost tracker
        $this->addFeature('submit_cost_tracker', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.submit_cost_tracker_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('submit_cost_tracker', 'mainTabs', array(
            'content'         => array(
                'create' => array('title' => 'Submit Cost Tracker', 'link' => '#')
            ), 
            'selected' => 'create'
        ));
        $this->addHook('submit_cost_tracker_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.submit_cost_tracker_hook',
            'point'           => 'hook_after',
            'features'        => 'submit_cost_tracker'  
        ));

        // View history submitted Cost tracker data
        $this->addFeature('view_history_cost_tracker', array(
            'class'        => 'easy_crud.feature.grid',
            'options'      => array(
                'id'       => 'AppForm',                   
                'title'    => 'Cost Tracker History',
                'reader'   => 'leep_admin.finance_tracker.business.cost_tracker_history_grid',
                'filter'   => 'leep_admin.finance_tracker.business.cost_tracker_history_filter_handler'
            )
        ));

        $this->setHookOptions('view_history_cost_tracker', 'mainTabs', array(          
            'selected' => 'xxx',
            'paginationDisplayLength' => 25
        ));

        $this->addHook('view_history_cost_tracker_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.cost_tracker_history_hook',
            'point'           => 'hook_after',
            'features'        => 'view_history_cost_tracker'  
        ));

        // Submit Cash data
        $this->addFeature('submit_cash_data', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.submit_cash_data_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('submit_cash_data', 'mainTabs', array(
            'content'         => array(
                'create' => array('title' => 'Submit Cost Tracker', 'link' => '#')
            ), 
            'selected' => 'create'
        ));
        $this->addHook('submit_cash_data_hook', array(
            'class'           => 'leep_admin.finance_tracker.business.submit_cash_data_hook',
            'point'           => 'hook_after',
            'features'        => 'submit_cash_data'  
        ));

        // Bulk change Cost Tracker Sector
        $this->addFeature('bulk_change_cost_tracker_sector', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.finance_tracker.business.bulk_change_cost_tracker_sector_handler',
                'submit_label'  => 'Process'
            )
        ));
        $this->setHookOptions('bulk_change_cost_tracker_sector', 'mainTabs', array(
            'selected' => 'cashOut'
        ));

        $this->setTemplate('cash_in_grid.list', 'LeepAdminBundle:FinanceTracker:cash_in_grid.html.twig');
        $this->setTemplate('cash_in_create.create', 'LeepAdminBundle:CRUD:form.html.twig');
        $this->setTemplate('cash_in_edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');
        $this->setTemplate('cash_out_grid.list', 'LeepAdminBundle:FinanceTracker:cash_out_grid.html.twig');
        $this->setTemplate('cash_out_create.create', 'LeepAdminBundle:CRUD:form.html.twig');
        $this->setTemplate('cash_out_edit.edit', 'LeepAdminBundle:CRUD:form.html.twig');
        $this->setTemplate('bulk_change_cost_tracker_sector.create', 'LeepAdminBundle:FinanceTracker:bulk_change_cost_tracker_sector.html.twig');
        $this->setTemplate('submit_cost_tracker.create', 'LeepAdminBundle:FinanceTracker:submit_cost_tracker.html.twig');
        $this->setTemplate('submit_cash_data.create', 'LeepAdminBundle:FinanceTracker:submit_cash_data.html.twig');   
        $this->setTemplate('finance_tracker.summary', 'LeepAdminBundle:FinanceTracker:summary.html.twig');
        $this->setTemplate('finance_tracker.distributionChannel', 'LeepAdminBundle:FinanceTracker:distributionChannel.html.twig');
        $this->setTemplate('finance_tracker.partner', 'LeepAdminBundle:FinanceTracker:partner.html.twig');
        $this->setTemplate('finance_tracker.amway', 'LeepAdminBundle:FinanceTracker:amway.html.twig');
        $this->setTemplate('finance_tracker.updateCost', 'LeepAdminBundle:FinanceTracker:edit_cost.html.twig');
        $this->setTemplate('finance_tracker.costTracker', 'LeepAdminBundle:FinanceTracker:cost_tracker.html.twig');
        $this->setTemplate('finance_tracker.cashIn', 'LeepAdminBundle:FinanceTracker:cash_in.html.twig');
        $this->setTemplate('finance_tracker.cashOut', 'LeepAdminBundle:FinanceTracker:cash_out.html.twig');
        $this->setTemplate('view_history_cost_tracker.list', 'LeepAdminBundle:FinanceTracker:cost_tracker_history.html.twig');
        $this->setTemplate('update_cost_tracker_data.edit', 'LeepAdminBundle:FinanceTracker:edit_cost_tracker_data.html.twig');
         
    }
}
