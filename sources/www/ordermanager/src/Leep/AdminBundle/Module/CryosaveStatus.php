<?php
namespace Leep\AdminBundle\Module;

class CryosaveStatus extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Cryosave',
            'title'         => 'Cryosave status'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Cryosave status',
            'create'        => 'Add new status'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Cryosave status',
            'reader'   => 'leep_admin.cryosave_status.business.grid_reader',    
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cryosave_status.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cryosave_status.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cryosave_status.business.delete_handler'
        ));     
    }
}
