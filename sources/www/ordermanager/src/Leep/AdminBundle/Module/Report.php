<?php
namespace Leep\AdminBundle\Module;

class Report extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Report',
            'title'         => 'Report'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Reports',
            'create'        => 'Add new report'
        ));
        $this->addHook('report', array(
            'class'           => 'leep_admin.hook.report',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Report',
            'reader'   => 'leep_admin.report.business.grid_reader',
            'filter'   => 'leep_admin.report.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Report:list.html.twig');

        ///LIST Search
        $this->addFeature('search_section', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',
                'reader'   => 'leep_admin.report.business.grid_search_section_reader',
                'filter'   => 'leep_admin.report.business.filter_search_section_handler'
            )
        ));
        $this->setTemplate('search_section.list', 'LeepAdminBundle:Report:list_search_section.html.twig');


        // CREATE
        $this->setFeatureOptions('create', array(
            'toEdit'        => true,
            'toEditFeature' => 'edit_section',
            'handler'       => 'leep_admin.report.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Report:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit report', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.report.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Report:edit.html.twig');

        // EDT SECTION
        $this->addFeature('edit_section', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.report.business.edit_section_handler'
            )
        ));
        $this->setHookOptions('edit_section', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit report', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setTemplate('edit_section.edit', 'LeepAdminBundle:Report:edit_section.html.twig');

        // EDT COPY
        $this->addFeature('edit_copy', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.report.business.edit_copy_handler'
            )
        ));
        $this->setHookOptions('edit_copy', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit report - Copy', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setTemplate('edit_copy.edit', 'LeepAdminBundle:Report:edit_copy.html.twig');

        // ATTACHMENT
        $this->addFeature('attachment', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.report.business.attachment_handler'
            )
        ));
        $this->setHookOptions('attachment', 'mainTabs', array(
            'extra'         => array(
                'attachment' => array('title' => 'Resources', 'link' => '#')
            ),
            'selected' => 'attachment'
        ));
        $this->setTemplate('attachment.create', 'LeepAdminBundle:Report:resource_create.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.report.business.delete_handler'
        ));

        // FEATURE
        $this->addFeature('report', array(
            'class'           => 'leep_admin.feature.report'
        ));
        $this->setHookOptions('report', 'mainTabs', array(
            'selected'      => 'report'
        ));
        $this->setTemplate('report.scanTextBlock', 'LeepAdminBundle:Report:scan_text_block.html.twig');


        // GENERATE
        $this->addFeature('generate', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.report.business.generate_handler'
            )
        ));
        $this->setHookOptions('generate', 'mainTabs', array(
            'extra'         => array(
                'generate' => array('title' => 'Generate', 'link' => '#')
            ),
            'selected'      => 'generate'
        ));
        $this->setTemplate('generate.create', 'LeepAdminBundle:Report:generate.html.twig');

    }
}
