<?php
namespace Leep\AdminBundle\Module;

class Bill extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Billing',
            'title'         => 'Invoice'
        ));
        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Bill',
            'create'        => 'Add new bill'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Bill',
            'reader'   => 'leep_admin.bill.business.grid_reader',
            'filter'   => 'leep_admin.bill.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:Bill:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.bill.business.create_handler',
            'toEdit'        => true,
            'toEditFeature' => 'edit',
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:Bill:create.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit bill', 'link' => '#')
            ),
            'selected'      => 'edit',
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.bill.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:Bill:edit.html.twig');

        // EDIT STATUS
        $this->addFeature('edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.bill.business.edit_status_handler'
            )
        ));
        $this->setHookOptions('edit_status', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_status.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // BULK EDIT STATUS
        $this->addFeature('bulk_edit_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.bill.business.bulk_edit_status_handler'
            )
        ));
        $this->addHook('bulk_edit_status_hook', array(
            'class'           => 'leep_admin.bill.business.bulk_edit_status_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_status'
        ));
        $this->setHookOptions('bulk_edit_status', 'mainTabs', array(
            'content'         => array(
                'list' => array('title' => 'Bills', 'link' => $this->getUrl('grid', 'list')),
                'edit' => array('title' => 'Edit status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('bulk_edit_status.edit', 'LeepAdminBundle:Bill:bulk_edit_status_form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.bill.business.delete_handler'
        ));
        // CREATE ORDER
        $this->addFeature('create_order', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.bill.business.create_order_handler',
                'submit_label'  => 'Create'
            )
        ));
        $this->setTemplate('create_order.create', 'LeepAdminBundle:Bill:create_order.html.twig');
        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.bill',
            'options'         => array()
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));
    }
}
