<?php
namespace Leep\AdminBundle\Module;

class RecipeBookLayout extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Recipe',
            'title'         => 'Recipe book layout'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Recipe book layout',
            'create'        => 'Add new recipe book layout'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Recipe book layout',
            'reader'   => 'leep_admin.recipe_book_layout.business.grid_reader',
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.recipe_book_layout.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit recipe book layout', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.recipe_book_layout.business.edit_handler'
        ));
        
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.recipe_book_layout.business.delete_handler'
        ));       
    }
}
