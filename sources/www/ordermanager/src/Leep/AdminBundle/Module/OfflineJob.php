<?php
namespace Leep\AdminBundle\Module;

class OfflineJob extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'OfflineJob',
            'title'         => 'Offline Job'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.offline_job.business.grid_reader',
                'filter'   => 'leep_admin.offline_job.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Offline job', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:OfflineJob:list.html.twig');   

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.offline_job'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content'       => array(),
            'selected'      => ''
        ));
    }
}
