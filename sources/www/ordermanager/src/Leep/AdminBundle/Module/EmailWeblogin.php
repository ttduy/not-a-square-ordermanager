<?php
namespace Leep\AdminBundle\Module;

class EmailWeblogin extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'EmailSystem',
            'title'         => 'EmailSystem'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Email Weblogin',
            'create'        => '',
            'disable'       => ['create']
        ));

        // EDIT
        $this->addFeature('edit', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.email_weblogin.business.edit_handler'
            )
        ));

        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit Weblogin Setting', 'link' => '#')
            ),
            'selected' => 'edit'
        ));

        $this->setTemplate('edit.edit', 'LeepAdminBundle:EmailWeblogin:form.html.twig');
    }
}
