<?php
namespace Leep\AdminBundle\Module;

class DistributionChannel extends Crud {
    public function load() {
        parent::load();
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'DistributionChannel',
            'title'         => 'DistributionChannel'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Distribution Channels',
            'create'        => 'Add new channel'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'DistributionChannel',
            'reader'   => 'leep_admin.distribution_channel.business.grid_reader',
            'filter'   => 'leep_admin.distribution_channel.business.filter_handler',
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:DistributionChannel:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.distribution_channel.business.create_handler'
        ));
        $this->setTemplate('create.create', 'LeepAdminBundle:DistributionChannel:form.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit distribution channel', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.distribution_channel.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:DistributionChannel:form.html.twig');

        // BULK EDIT DC SETTING
        $this->addFeature('bulk_edit_dc_setting', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.bulk_edit_dc_setting'
            )
        ));
        $this->setTemplate('bulk_edit_dc_setting.edit', 'LeepAdminBundle:DistributionChannel:bulk_edit_dc_setting.html.twig');

        $this->addHook('bulk_edit_dc_setting_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.bulk_edit_dc_setting_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_dc_setting'
        ));

        // BULK EDIT DC REPORT SETTING
        $this->addFeature('bulk_edit_dc_report_setting', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.bulk_edit_dc_report_setting'
            )
        ));
        $this->setTemplate('bulk_edit_dc_report_setting.edit', 'LeepAdminBundle:DistributionChannel:bulk_edit_dc_report_setting.html.twig');

        $this->addHook('bulk_edit_dc_report_setting_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.bulk_edit_dc_report_setting_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_dc_report_setting'
        ));

        // BULK EDIT DC WEB LOGIN
        $this->addFeature('bulk_edit_dc_web_login', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.bulk_edit_dc_web_login'
            )
        ));
        $this->setTemplate('bulk_edit_dc_web_login.edit', 'LeepAdminBundle:DistributionChannel:bulk_edit_dc_web_login.html.twig');

        $this->addHook('bulk_edit_dc_web_login_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.bulk_edit_dc_web_login_hook',
            'point'           => 'hook_after',
            'features'        => 'bulk_edit_dc_web_login'
        ));


        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.distribution_channel.business.view_handler'
        ));
        $this->setTemplate('view.view', 'LeepAdminBundle:DistributionChannel:view.html.twig');

        // VIEW WEB LOGIN
        $this->setFeatureOptions('view_web_login', array(
            'handler'       => 'leep_admin.distribution_channel.business.view_web_login_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.distribution_channel.business.delete_handler'
        ));

        // GENERAL
        $this->addFeature('dc', array(
            'class'           => 'leep_admin.feature.distribution_channel',
            'options'         => array()
        ));
        $this->setHookOptions('dc', 'mainTabs', array(
            'extra'         => array(),
            'selected'      => 'edit'
        ));
        $this->setTemplate('dc.googleMap', 'LeepAdminBundle:DistributionChannel:google_map.html.twig');
        $this->setTemplate('dc.googleMapCountryNote', 'LeepAdminBundle:DistributionChannel:google_map_country_note.html.twig');

        // EDIT COMPLAINTS
        $this->addFeature('edit_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.edit_complaint_handler'
            )
        ));
        $this->setHookOptions('edit_complaint', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit complaint', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_complaint.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT FEEDBACKS
        $this->addFeature('edit_feedback', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.edit_feedback_handler'
            )
        ));
        $this->setHookOptions('edit_feedback', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_feedback.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // COPY
        $this->addFeature('copy', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.copy_handler',
                'submit_label'  => 'Copy'
            )
        ));
        $this->setHookOptions('copy', 'mainTabs', array(
            'extra'         => array(
                'copy' => array('title' => 'Copy distribution channel', 'link' => '#')
            ),
            'selected'      => 'copy'
        ));
        $this->setTemplate('copy.edit', 'LeepAdminBundle:CRUD:form.html.twig');

        // EDIT DC INFO STATUS
        $this->addFeature('edit_marketing_status', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.edit_marketing_status_handler'
            )
        ));
        $this->addHook('edit_marketing_status_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.edit_marketing_status_hook',
            'point'           => 'hook_after',
            'features'        => 'edit_marketing_status'
        ));
        $this->setHookOptions('edit_marketing_status', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit DC Info Status', 'link' => '#')
            ),
            'selected' => 'edit'
        ));
        $this->setTemplate('edit_marketing_status.edit', 'LeepAdminBundle:DistributionChannel:form_update_marketing_status.html.twig');

        // EMAIL
        $this->setHookOptions('email', 'mainTabs', array(
            'selected'      => 'dc_email'
        ));
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.email_handler',
                'submit_label'  => 'Send'
            )
        ));
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'
        ));
        $this->setTemplate('email.create', 'LeepAdminBundle:DistributionChannel:email.html.twig');

        // DC - DASHBOARD
        $this->addFeature('dashboard', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'       => 'CrudGrid',
                'title'    => 'Marking Status - Reminder',
                'reader'   => 'leep_admin.distribution_channel.business.dashboard_grid_reader'
            )
        ));

        $this->setHookOptions('dashboard', 'mainTabs', array(
            'extra'         => array(
                'dashboard' => array('title' => 'Distribution Channel - Dashboard', 'link' =>  $this->getUrl('dashboard', 'list'))
            ),
            'selected'   => 'dashboard'
        ));
        $this->addHook('dashboard_hook', array(
            'class'           => 'leep_admin.distribution_channel.business.dashboard_hook',
            'point'           => 'hook_after',
            'features'        => 'dashboard'
        ));
        $this->setTemplate('dashboard.list', 'LeepAdminBundle:DistributionChannel:dashboard.html.twig');


        // LOG COMPLAINT
        $this->addFeature('log_complaint', array(
            'class'           => 'easy_crud.feature.edit',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.distribution_channel.business.log_complaint_handler',
                'submit_label'  => 'Log'
            )
        ));
        $this->setHookOptions('log_complaint', 'mainTabs', array(
            'extra'         => array(
                'log' => array('title' => 'Log complaint', 'link' => '#')
            ),
            'selected'      => 'log'
        ));
        $this->setTemplate('log_complaint.edit', 'LeepAdminBundle:CRUD:popup_form_no_tab.html.twig');

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.distribution_channel'
        ));
    }
}
