<?php
namespace Leep\AdminBundle\Module;

class Service extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Service',
            'title'         => 'Service'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Services',
            'create'        => 'Add new service'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Service',
            'reader'   => 'leep_admin.service.business.grid_reader',        
            'filter'   => 'leep_admin.service.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.service.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit service', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.service.business.edit_handler'
        ));
    }
}
