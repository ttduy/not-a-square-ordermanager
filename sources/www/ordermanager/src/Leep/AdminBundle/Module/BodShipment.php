<?php
namespace Leep\AdminBundle\Module;

class BodShipment extends Crud {   
    public function load() { 
        parent::load();     

        $id = $this->container->get('request')->get('id', 0);

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.customer_report_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Order',
            'title'         => 'Report'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Shipments',
            'reader'   => 'leep_admin.bod_shipment.business.grid_reader',       
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'selected' => 'shipment_list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:BodShipment:list.html.twig');

        // CREATE
        $this->setHookOptions('create', 'mainTabs', array(
            'selected'      => 'shipment_create'
        ));
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.bod_shipment.business.create_handler'
        ));        
        $this->setTemplate('create.create', 'LeepAdminBundle:BodShipment:form.html.twig');

        // EMAIL
        $this->addHook('email_hook', array(
            'class'           => 'leep_admin.bod_shipment.hook.email_hook',
            'point'           => 'hook_after',
            'features'        => 'email'  
        ));  

        $this->setHookOptions('email', 'mainTabs', array(
            'selected'      => 'shipment_email'
        ));
        $this->addFeature('email', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'            => 'AppForm',
                'handler'       => 'leep_admin.bod_shipment.business.email_handler'
            )
        ));        
        $this->setTemplate('email.create', 'LeepAdminBundle:BodShipment:email.html.twig');

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'shipment_edit' => array('title' => 'Edit shipment', 'link' => '#')
            ),
            'selected'      => 'shipment_edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.bod_shipment.business.edit_handler'
        ));
        $this->setTemplate('edit.edit', 'LeepAdminBundle:BodShipment:form.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.bod_shipment.business.delete_handler'
        ));       
    }
}
