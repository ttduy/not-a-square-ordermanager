<?php
namespace Leep\AdminBundle\Module;

class TodoCategory extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'StrategyPlanningTool',
            'title'         => 'Categories'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Categories',
            'create'        => 'Add new category'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Categories',
            'reader'   => 'leep_admin.todo_category.business.grid_reader'      
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.todo_category.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit category', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.todo_category.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.todo_category.business.delete_handler'
        ));       
    }
}
