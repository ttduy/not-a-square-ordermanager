<?php
namespace Leep\AdminBundle\Module;

class CmsContactMessage extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'CMSOrder',
            'title'         => 'Contact Message'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Contact Message',
            'create'        => 'Add new message'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Contact Message',
            'reader'   => 'leep_admin.cms_contact_message.business.grid_reader',
            'filter'   => 'leep_admin.cms_contact_message.business.filter_handler'  
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.cms_contact_message.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit message', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.cms_contact_message.business.edit_handler'
        ));
        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.cms_contact_message.business.delete_handler'
        ));       
    }
}
