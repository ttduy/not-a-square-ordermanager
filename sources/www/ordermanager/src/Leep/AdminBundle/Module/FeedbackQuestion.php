<?php
namespace Leep\AdminBundle\Module;

class FeedbackQuestion extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'FeedbackQuestion',
            'title'         => 'Feedback question'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Feedback questions',
            'create'        => 'Add new feedback question'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Feedback questions',
            'reader'   => 'leep_admin.feedback_question.business.grid_reader',        
            'filter'   => 'leep_admin.feedback_question.business.filter_handler'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:FeedbackQuestion:list.html.twig');

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.feedback_question.business.create_handler',
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit feedback question', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.feedback_question.business.edit_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.feedback_question.business.delete_handler'
        ));       

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.feedback_question',
            'options'         => array()
        ));        
        $this->setHookOptions('feature', 'mainTabs', array(
            'selected' => 'list'
        ));    
    }
}
