<?php
namespace Leep\AdminBundle\Module;

class SampleImportJob extends Base { 
    public function load() {
        parent::load();

        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'RussiaMWAProcessor',
            'title'         => 'Sample import'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));
        
        // LIST
        $this->addFeature('grid', array(
            'class'           => 'easy_crud.feature.grid',
            'options'         => array(
                'id'            => 'AppForm',                   
                'reader'   => 'leep_admin.sample_import_job.business.grid_reader',
                'filter'   => 'leep_admin.sample_import_job.business.filter_handler',
            )
        ));
        $this->setHookOptions('grid', 'mainTabs', array(          
            'content'  => array(
                'list' => array('title' => 'Sample import', 'link' => '#'),
            ),
            'selected' => 'list'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:SampleImportJob:list.html.twig');   

        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.sample_import_job'
        ));
        $this->setHookOptions('feature', 'mainTabs', array(
            'content'       => array(),
            'selected'      => ''
        ));
        $this->setTemplate('feature.importFromLytech', 'LeepAdminBundle:SampleImportJob:import_from_lytech.html.twig');   
    }
}
