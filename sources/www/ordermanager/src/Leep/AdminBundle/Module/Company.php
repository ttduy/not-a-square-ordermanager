<?php
namespace Leep\AdminBundle\Module;

class Company extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'Company',
            'title'         => 'Company'
        ));

        $this->setHookOptions('*', 'mainTabs', array(
            'list'          => 'Companies',
            'create'        => 'Add new company'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Company',
            'reader'   => 'leep_admin.company.business.grid_reader',        
            'filter'   => 'leep_admin.company.business.filter_handler'
        ));

        // CREATE
        $this->setFeatureOptions('create', array(
            'handler'       => 'leep_admin.company.business.create_handler'
        ));        

        // EDIT
        $this->setHookOptions('edit', 'mainTabs', array(
            'extra'         => array(
                'edit' => array('title' => 'Edit company', 'link' => '#')
            ),
            'selected'      => 'edit'
        ));
        $this->setFeatureOptions('edit', array(
            'handler'       => 'leep_admin.company.business.edit_handler'
        ));

        // VIEW
        $this->setFeatureOptions('view', array(
            'handler'       => 'leep_admin.company.business.view_handler'
        ));

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.company.business.delete_handler'
        ));       
    }
}
