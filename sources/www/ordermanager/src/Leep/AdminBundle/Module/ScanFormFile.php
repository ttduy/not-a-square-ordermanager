<?php
namespace Leep\AdminBundle\Module;

class ScanFormFile extends Crud {   
    public function load() { 
        parent::load();       
        // ALL
        $this->setHookOptions('*', 'header', array(
            'menuSelected'  => 'ScanFormFile',
            'title'         => 'Scan Form File'
        ));

        $this->addHook('mainTabs', array(
            'class'           => 'leep_admin.hook.scan_form_file_main_tabs',
            'point'           => 'hook_before',
            'features'        => '*'
        ));

        // LIST
        $this->setFeatureOptions('grid', array(
            'title'    => 'Scan Form File',
            'filter'   => 'leep_admin.scan_form_file.business.filter_handler',
            'reader'   => 'leep_admin.scan_form_file.business.grid_reader'      
        ));
        $this->setHookOptions('grid', 'mainTabs', array(
            'extra'         => array(
                
            ),
            'selected'      => 'list'
        ));
        $this->addHook('grid_hook', array(
            'class'           => 'leep_admin.scan_form_file.business.grid_reader_hook',
            'point'           => 'hook_after',
            'features'        => 'grid'
        ));
        $this->setTemplate('grid.list', 'LeepAdminBundle:ScanFormFile:list.html.twig');

        // DETECTING TEMPLATE
        $this->addFeature('detecting_template', array(
            'class'           => 'easy_crud.feature.create',
            'options'         => array(
                'id'                => 'AppForm',
                'handler'           => 'leep_admin.scan_form_file.business.detecting_template_handler',
                'submit_label'      => 'Save',
                'toRedirect'        => true,
                'toCreateFeature'   => 'detecting_template'
            )
        ));
        $this->addHook('detecting_template_hook', array(
            'class'           => 'leep_admin.scan_form_file.business.detecting_template_hook',
            'point'           => 'hook_after',
            'features'        => 'detecting_template'
        ));
        $this->setTemplate('detecting_template.create', 'LeepAdminBundle:ScanFormFile:detecting_template.html.twig');

        // DELETE
        $this->setFeatureOptions('delete', array(
            'handler'       => 'leep_admin.scan_form_file.business.delete_handler'
        ));       


        // FEATURE
        $this->addFeature('feature', array(
            'class'           => 'leep_admin.feature.scan_form_file',
            'options'         => array(
            )
        ));
    }
}
