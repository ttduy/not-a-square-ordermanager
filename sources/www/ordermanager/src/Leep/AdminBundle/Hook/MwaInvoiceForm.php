<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MwaInvoiceForm extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');

        $data['viewInvoicePdfUrl'] =  $manager->getUrl('leep_admin', 'mwa_invoice', 'invoice_form_edit', 'pdf');
        $data['paginationDisplayLength'] = 500;
    }    
}
