<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class CollectivePaymentForm extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');

        $data['viewPaymentPdfUrl'] =  $manager->getUrl('leep_admin', 'collective_payment', 'payment_form_pdf', 'pdf');
        $data['paginationDisplayLength'] = 500;
        $data['sDom'] = 'rtilp';
    }    
}
