<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['mainTabs'] = array(
            'content' => $options['content'],
            'selected' => $options['selected']
        );

        $helperSecurity = $controller->get('leep_admin.helper.security');        
        $keyView = $helperSecurity->getKey($this->getModule()->getName(), 'View');
        if (!$helperSecurity->hasPermission($keyView)) {
            if (isset($data['mainTabs']['content']['list'])) {
                unset($data['mainTabs']['content']['list']);
            }
        }

        $keyModify = $helperSecurity->getKey($this->getModule()->getName(), 'Modify');
        if (!$helperSecurity->hasPermission($keyModify)) {
            if (isset($data['mainTabs']['content']['create'])) {
                unset($data['mainTabs']['content']['create']);
            }
        }

        if (isset($options['paginationDisplayLength'])) {
            $data['paginationDisplayLength'] = intval($options['paginationDisplayLength']);
        }
    }
}
