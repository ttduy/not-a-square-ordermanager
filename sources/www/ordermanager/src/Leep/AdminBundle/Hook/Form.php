<?php
namespace Leep\AdminBundle\Hook;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class Form extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id = $controller->getRequest()->get('id', 0);
        $mapping = $controller->get('easy_mapping');
        $data['formItemMapping'] = $mapping->getMapping('LeepAdmin_FormItem_CtrlMapping');

        // FormVersion
        $formStep = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:FormStep')->findOneById($id);
        if ($formStep) {
            $data['idFormVersion'] = $formStep->getIdFormVersion();            
        }
    }
}
