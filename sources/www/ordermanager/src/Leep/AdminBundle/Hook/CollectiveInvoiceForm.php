<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class CollectiveInvoiceForm extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');

        $data['viewInvoicePdfUrl'] =  $manager->getUrl('leep_admin', 'collective_invoice', 'invoice_form_pdf', 'pdf');
        $data['paginationDisplayLength'] = 500;
        $data['sDom'] = 'rtilp';
    }    
}
