<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use App\Database\MainBundle\Entity\Log;

class LogHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        /*
        $manager = $controller->get('easy_module.manager');

        $data['pageTitle'] = 'Novo';
        $data['logoutUrl'] = $controller->generateUrl('Security_Logout');
        */
        $method_post = 'POST';
        $method_get = 'GET';

        // tracking feature / action
        /*
                ,   '/category/edit/edit'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit category'
                                                                                ,   'data_key' => array('getCategoryname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductCategoriesDnaPlus'
                                                                                                            , 'id_key' => 'id')

                '/category/edit/edit' : key - uri path without query string
                'method' : POST | GET
                'title' : the first part of Log title
                'data_key' : 
                    - without 'repository', this is array of key/value couple:
                        + key is 'name' attribute of input.
                        + value is title of related input in Log title.
                    - with 'repository', this is array of key/value couple:
                        + key is a function defined in Entity used to get expected info to be used in Log title.
                        + value is title of related info (will be taken by function defined by 'key') in Log title.
                'repository' : it's defined by 2 information:
                    - 'entity': entity path
                    - 'id_key': the unique argument value will be used function "findOneById()"
        */

        $arr_tracking_features = array(
                    '/customer_info/unlock/edit'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Unlock Customer & Order'
                                                                                ,   'data_key' => array('getCustomerNumber' => 'Customer Number')
                                                                                ,   'repository' => array(
                                                                                                            'entity' => 'AppDatabaseMainBundle:CustomerInfo'
                                                                                                            , 'id_key' => 'id'
                                                                                                            , 'relatedData' => array(
                                                                                                                    'title'             => 'Order number',
                                                                                                                    'entity'            => 'AppDatabaseMainBundle:Customer',
                                                                                                                    'id_foreign_key'    => 'idCustomerInfo',
                                                                                                                    'getInfoMethod'     => 'getOrdernumber'
                                                                                                                )
                                                                                                        )
                                                                            )
                ,   '/category/create/create'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new category'
                                                                                ,   'data_key' => array('categoryName' => 'Name')
                                                                            )
                ,   '/category/edit/edit'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit category'
                                                                                ,   'data_key' => array('getCategoryname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductCategoriesDnaPlus'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/category/delete/delete'                       => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete category'
                                                                                ,   'data_key' => array('getCategoryname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductCategoriesDnaPlus'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/product/create/create'                        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new product'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/product/edit/edit'                            => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit product'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductsDnaPlus'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/product/delete/delete'                        => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete product'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductsDnaPlus'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/gene/create/create'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new gene'
                                                                                ,   'data_key' => array('geneName' => 'Name')
                                                                            )
                ,   '/gene/edit/edit'                               => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit gene'
                                                                                ,   'data_key' => array('getGenename' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Genes'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/gene/delete/delete'                           => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete gene'
                                                                                ,   'data_key' => array('getGenename' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Genes'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/massarray/create/create'                      => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new massarray'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/massarray/edit/edit'                          => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit massarray'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Massarray'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/massarray/delete/delete'                      => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete massarray'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Massarray'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/pyros/create/create'                          => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new pyros'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/pyros/edit/edit'                              => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit pyros'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Pyros'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/pyros/delete/delete'                          => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete pyros'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Pyros'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/price_category/create/create'                 => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new price category'
                                                                                ,   'data_key' => array('priceCategory' => 'Name')
                                                                            )
                ,   '/price_category/edit/edit'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit price category'
                                                                                ,   'data_key' => array('getPricecategory' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:PriceCategories'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/price_category/copy/edit'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Copy price category'
                                                                                ,   'data_key' => array('getPricecategory' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:PriceCategories'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/price_category/delete/delete'                 => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete price category'
                                                                                ,   'data_key' => array('getPricecategory' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:PriceCategories'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/product_category_price/ajax_update/update'    => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Edit category price'
                                                                                ,   'data_key' => array('getCategoryname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductCategoriesDnaPlus'
                                                                                                            , 'id_key' => 'categoryId')
                                                                            )

                ,   '/product_price/ajax_update/update'             => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Edit product price'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:ProductsDnaPlus'
                                                                                                            , 'id_key' => 'productId')
                                                                            )

                ,   '/acquisiteur/create/create'                    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new acquisiteur'
                                                                                ,   'data_key' => array('acquisiteur' => 'Name')
                                                                            )
                ,   '/acquisiteur/edit/edit'                        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit acquisiteur'
                                                                                ,   'data_key' => array('getAcquisiteur' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Acquisiteur'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/acquisiteur/delete/delete'                    => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete acquisiteur'
                                                                                ,   'data_key' => array('getAcquisiteur' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Acquisiteur'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/partner/create/create'                        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new partner'
                                                                                ,   'data_key' => array('partner' => 'Name')
                                                                            )
                ,   '/partner/edit/edit'                            => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit partner'
                                                                                ,   'data_key' => array('getPartner' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Partner'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/partner/delete/delete'                        => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete partner'
                                                                                ,   'data_key' => array('getPartner' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Partner'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/distribution_channel/create/create'           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new distribution channel'
                                                                                ,   'data_key' => array('distributionChannel' => 'Name')
                                                                            )
                ,   '/distribution_channel/edit/edit'               => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit distribution channel'
                                                                                ,   'data_key' => array('getDistributionchannel' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:DistributionChannels'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/distribution_channel/delete/delete'           => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete distribution channel'
                                                                                ,   'data_key' => array('getDistributionchannel' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:DistributionChannels'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/special_product/create/create'                => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new special product'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/special_product/edit/edit'                    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit special product'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:SpecialProduct'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/special_product/delete/delete'                => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete special product'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:SpecialProduct'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/material/create/create'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new material'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/material/edit/edit'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit material'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Materials'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/material/delete/delete'                       => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete material'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Materials'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/material_type/create/create'                  => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new material type'
                                                                                ,   'data_key' => array('matType' => 'Name')
                                                                            )
                ,   '/material_type/edit/edit'                      => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit material type'
                                                                                ,   'data_key' => array('getMattype' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MatType'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/material_type/delete/delete'                  => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete material type'
                                                                                ,   'data_key' => array('getMattype' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MatType'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/company/create/create'                        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new company'
                                                                                ,   'data_key' => array('companyName' => 'Name')
                                                                            )
                ,   '/company/edit/edit'                            => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit company'
                                                                                ,   'data_key' => array('getCompanyname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Companies'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/company/delete/delete'                        => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete company'
                                                                                ,   'data_key' => array('getCompanyname' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Companies'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/customer/create/create'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new customer'
                                                                                ,   'data_key' => array('orderNumber' => 'Order number')
                                                                            )
                ,   '/customer/edit/edit'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/customer/delete/delete'                       => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/customer/edit_activity/edit'                  => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit activity of customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/customer/edit_status/edit'                    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit status of customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/customer/double_check/create'                    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Do double-check customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/customer_report/change_status/create'                    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Send email and change status of customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/customer/copy/edit'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Copy customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                //,   '/customer/customer_export/export'              => array($method_post)
                ,   '/email/email/create'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Send email to customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                
                ,   '/invoice/edit/edit'                            => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Update invoice of customer'
                                                                                ,   'data_key' => array('getOrdernumber' => 'Order number')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Customer'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/collective_invoice/invoice_form/list'         => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new collective invoice'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/collective_invoice/invoice_form_edit/list'    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit collective invoice'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectiveInvoice'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/collective_invoice/delete/delete'             => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete collective invoice'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectiveInvoice'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                //,   '/collective_invoice/email/create'              => array($method_post)
                ,   '/collective_invoice/edit_activity/edit'        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit activity of collective invoice'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectiveInvoice'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/collective_payment/payment_form/list'         => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new collective payment'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/collective_payment/payment_form_edit/list'    => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit collective payment'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectivePayment'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/collective_payment/delete/delete'             => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete collective payment'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectivePayment'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/collective_payment/edit_activity/edit'        => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit activity of collective payment'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:CollectivePayment'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/email_template/create/create'                 => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new email template'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/email_template/edit/edit'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit email template'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:EmailTemplate'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/email_template/delete/delete'                 => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete email template'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:EmailTemplate'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/status/create/create'                         => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new status'
                                                                                ,   'data_key' => array('txt' => 'Name')
                                                                            )
                ,   '/status/edit/edit'                             => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit status'
                                                                                ,   'data_key' => array('getTxt' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Status'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/status/delete/delete'                         => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete status'
                                                                                ,   'data_key' => array('getTxt' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Status'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/mwa_sample/create/create'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new mwa sample'
                                                                                ,   'data_key' => array('sampleId' => 'ID')
                                                                            )
                ,   '/mwa_sample/edit/edit'                         => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit mwa sample'
                                                                                ,   'data_key' => array('getSampleId' => 'ID')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaSample'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/mwa_sample/delete/delete'                     => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete mwa sample'
                                                                                ,   'data_key' => array('getSampleId' => 'ID')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaSample'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/mwa_valid_code/create/create'                 => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new mwa valid code'
                                                                                ,   'data_key' => array('code' => 'Code')
                                                                            )
                ,   '/mwa_valid_code/edit/edit'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit mwa valid code'
                                                                                ,   'data_key' => array('getCode' => 'Code')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaValidCode'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/mwa_valid_code/delete/delete'                 => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete mwa valid code'
                                                                                ,   'data_key' => array('getCode' => 'Code')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaValidCode'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/mwa_status/create/create'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new mwa status'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/mwa_status/edit/edit'                         => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit mwa status'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaStatus'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/mwa_status/delete/delete'                     => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete mwa status'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:MwaStatus'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/widget_statistic/create/create'               => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new widget'
                                                                                ,   'data_key' => array('name' => 'Name')
                                                                            )
                ,   '/widget_statistic/edit/edit'                   => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit widget'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:WidgetStatistic'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/widget_statistic/delete/delete'               => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete widget'
                                                                                ,   'data_key' => array('getName' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:WidgetStatistic'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                ,   '/parameter/edit/edit'                          => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit parameter'
                                                                            )

                ,   '/web_user/create/create'                       => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new web user'
                                                                                ,   'data_key' => array('username' => 'Name')
                                                                            )
                ,   '/web_user/edit/edit'                           => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit web user'
                                                                                ,   'data_key' => array('getUsername' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:WebUsers'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/web_user/delete/delete'                       => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete web user'
                                                                                ,   'data_key' => array('getUsername' => 'Name')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:WebUsers'
                                                                                                            , 'id_key' => 'id')
                                                                            )

                // Pellet 
                ,   '/pellet/create/create'                 => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Add new pellet'
                                                                                ,   'data_key' => array('getId' => 'Id')
                                                                            )
                ,   '/pellet/edit/edit'                     => array(
                                                                                    'method' => array($method_post)
                                                                                ,   'title' => 'Edit pellet'
                                                                                ,   'data_key' => array('getId' => 'Id')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Pellet'
                                                                                                            , 'id_key' => 'id')
                                                                            )
                ,   '/pellet/delete/delete'                 => array(
                                                                                    'method' => array($method_get)
                                                                                ,   'title' => 'Delete pellet'
                                                                                ,   'data_key' => array('getId' => 'Id')
                                                                                ,   'repository' => array('entity' => 'AppDatabaseMainBundle:Pellet'
                                                                                                            , 'id_key' => 'id')
                                                                            )

            );

        // get information of current logged in user
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $userSession = $userManager->getUserSession();
        $userName = "";
        $userId = 0;
        $title = "";

        if ($userSession != null) {
            $userName = $userSession->getUser()->getUsername();
            $userId = $userSession->getUser()->getId();
        }

        // get URI
        $pathInfo = $controller->get('request')->getPathInfo();

        // get sending data method
        $sending_data_method = $controller->get('request')->getMethod();

        // just log after form was submitted (method = "POST")
        if (array_key_exists($pathInfo, $arr_tracking_features) 
                && in_array($sending_data_method, $arr_tracking_features[$pathInfo]['method']))
        {
            $tobe_logged_data = array();

            // query string
            $get_data = $controller->get('request')->getQueryString();

            // include user information into $tobe_logged_data
            $tobe_logged_data['user'] = $userName;

            // include module / action path information into $tobe_logged_data
            $tobe_logged_data['uri'] = $pathInfo . (($get_data) ? '?' . $get_data : '');

            // process submitted data

            $post_data = null;
            if ($sending_data_method == $method_post)
            {
                $post_data = $controller->get('request')->request->all();

                // remove 'form' key
                if (isset($post_data['form']))
                {
                    $post_data = $post_data['form'];
                }
            }

            $submitted_data = $post_data;
            // merge get data with post data
            if ($get_data)
            {
                parse_str($get_data, $get_data);

                if (isset($submitted_data))
                {
                    $submitted_data = array_merge($submitted_data, $get_data);
                }
                else
                {
                    $submitted_data = $get_data;
                }

            }

            // un-comment below code if '_token' is not logged            
            if (isset($submitted_data['_token']))
            {
                unset($submitted_data['_token']);
            }
            

            // build title of log record
            $title = $this->buildLogTitle($arr_tracking_features[$pathInfo], $submitted_data, $controller);

            // assign submitted data to main logged data
            $tobe_logged_data['data'] = $submitted_data;

            // initialize Log entity
            $log = new Log();

            // fulfill web user id
            $log->setIdWebUser($userId);

            // fulfill title
            $log->setTitle($title);
            
            // fulfill log time
            $log->setLogTime(new \DateTime("now"));

            // fulfill log data, data is JSON-based data
            $log->setLogData(json_encode($tobe_logged_data));

            // save to database
            $em = $controller->get('doctrine')->getManager();
            $em->persist($log);
            $em->flush();
        }
    }

    // build title of log record
    private function buildLogTitle($arr_feature_info, $arr_submitted_data, $controller)
    {
        $str_title = $arr_feature_info['title'];

        $arr_values = null;

        // if 'repository' property exists, it means we have to lookup data in database
        if (isset($arr_feature_info['repository']))
        {
            $entity = $arr_feature_info['repository']['entity']; // entity path
            $id_key = $arr_feature_info['repository']['id_key']; // get record by id
            $id = isset($arr_submitted_data[$id_key]) ? $arr_submitted_data[$id_key] : 0;

            $db_object = $controller->get('doctrine')->getRepository($entity)->findOneById($id);

            if ($db_object && isset($arr_feature_info['data_key']))
            {
                // build title basing on 'data_key' which includes get_function and related title
                foreach($arr_feature_info['data_key'] as $get_function => $title)
                {
                    $arr_values[] = $title . ": " . $db_object->$get_function();
                }

                if (isset($arr_values))
                {
                    $str_title .= ' (' . implode(', ', $arr_values) . ')';
                }
            }

            // get related data if needs
            if (isset($arr_feature_info['repository']['relatedData'])) {
                $relatedData = $arr_feature_info['repository']['relatedData'];
                $title = $relatedData['title']; // entity title
                $entity = $relatedData['entity']; // entity path
                $foreignKey = $relatedData['id_foreign_key']; // get record by id
                $getInfoMethod = $relatedData['getInfoMethod']; // get what info of record
                $method = 'findBy' . ucfirst($foreignKey);

                $records = $controller->get('doctrine')->getRepository($entity)->$method($id);

                if ($records) {
                    $recordInfo = array();
                    foreach ($records as $entry) {
                        $recordInfo[] = $entry->$getInfoMethod();
                    }

                    if (!empty($recordInfo)) {
                        $str_title .= ' [' . $title . ': ' . implode(', ', $recordInfo) . ']';
                    }
                }
            }
        }
        else if (isset($arr_feature_info['data_key']))
        {
            foreach($arr_feature_info['data_key'] as $key => $title)
            {
                $arr_values[] = isset($arr_submitted_data[$key]) ? "{$title}: {$arr_submitted_data[$key]}" : '';
            }

            if (isset($arr_values))
            {
                $str_title .= ' (' . implode(', ', $arr_values) . ')';
            }
        }


        return $str_title;
    }


}