<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class Security extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');

        $baseModule = $this->getModule()->getName();

        if ($baseModule == 'customer_invoice') {
            $baseModule = 'customer';
        }
        $pathInfo = $controller->get('request')->getPathInfo();

        // Check common grid
        if (strpos($pathInfo, '/grid/list') != 0) {
            $keyView = $helperSecurity->getKey($baseModule, 'View');
            if (!$helperSecurity->hasPermission($keyView)) {
                die('Access denied');
            }
        }

        // Check common modification
        if (strpos($pathInfo, '/create/create') != 0 ||
            strpos($pathInfo, '/edit/edit') != 0 ||
            strpos($pathInfo, '/delete/delete') != 0) {
            $keyModify = $helperSecurity->getKey($baseModule, 'Modify');
            if (!$helperSecurity->hasPermission($keyModify)) {
                die('Access denied');
            }
        }

        // Specific
        if (strpos($pathInfo, '/report/delete/delete') !== FALSE) {
            if (!$helperSecurity->hasPermission('reportDelete')) {
                die('Access denied');
            }
        }
        if (strpos($pathInfo, '/finance_tracker/finance_tracker') !== FALSE) {
            if (!$helperSecurity->hasPermission('financeTrackerView')) {
                die('Access denied');
            }
        }
        if (strpos($pathInfo, '/material/material_finance_tracker') !== FALSE) {
            if (!$helperSecurity->hasPermission('materialView')) {
                die('Access denied');
            }
        }
    }
}
