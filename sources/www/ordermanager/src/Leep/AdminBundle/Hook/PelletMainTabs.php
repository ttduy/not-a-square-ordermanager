<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class PelletMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'list'          => array('title' => 'Pellets', 'link' => $mgr->getUrl('leep_admin', 'pellet', 'grid', 'list')),
                'create'        => array('title' => 'Add new Pellet', 'link' => $mgr->getUrl('leep_admin', 'pellet', 'create', 'create'))
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : 'list'
        );
        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
        if (isset($options['content'])) {
            $data['mainTabs']['content'] = $options['content'];
        }
    }
}

