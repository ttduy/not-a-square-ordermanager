<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EmployeeFeedbackResponse extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        $employeeFeedbackQuestion = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findOneById($id);
        $data['employeeFeedbackQuestion'] = $employeeFeedbackQuestion;

        if ($employeeFeedbackQuestion->getIdType() == Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE ||
            $employeeFeedbackQuestion->getIdType() == Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE) {
            $summary = json_decode($employeeFeedbackQuestion->getSummary(), true);
            $choices = Business\EmployeeFeedbackQuestion\Util::parseChoices($employeeFeedbackQuestion->getData());

            $numResponses = $employeeFeedbackQuestion->getNumResponses();
            $stats = array();
            foreach ($choices as $id => $choice) {
                $total = isset($summary[$id]) ? $summary[$id] : 0;
                $stats[] = array(
                    'name'    => $choice,
                    'total'   => $total,
                    'percent' => $numResponses == 0 ? 0 : number_format($total * 100 / $numResponses, 2)
                );
            }

            $data['stats'] = $stats;
        }
    }
}
