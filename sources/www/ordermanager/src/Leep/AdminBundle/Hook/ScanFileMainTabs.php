<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class ScanFileMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'list'         => array('title' => 'Scan File', 'link' => $mgr->getUrl('leep_admin', 'scan_file', 'grid', 'list'))
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
    }
}
