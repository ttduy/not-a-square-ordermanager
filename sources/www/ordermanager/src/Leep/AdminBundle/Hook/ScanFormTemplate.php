<?php
namespace Leep\AdminBundle\Hook;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class ScanFormTemplate extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id = $controller->getRequest()->get('id', 0);
        $manager = $controller->get('easy_module.manager');        
        $data['viewScanFormTemplatePdfUrl'] = $manager->getUrl('leep_admin', 'scan_form_template', 'feature', 'downloadTemplate', array('id' => $id));
        $data['checkFormTemplateUrl'] = $manager->getUrl('leep_admin', 'scan_form_template', 'feature', 'checkTemplate', array('id' => $id));
    }
}