<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class Customer extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id = $controller->get('request')->query->get('id', 0);
        $manager = $controller->get('easy_module.manager');
        $data['emailSendingUrl'] = $manager->getUrl('leep_admin', 'email', 'email',  'create', array('id' => $id, 'mode' => 'customer'));

        $helperSecurity = $controller->get('leep_admin.helper.security');
        $data['customerModifyPermission'] = $helperSecurity->hasPermission('customerModify');

        $customer = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        if ($customer) {
            $data['isAddedInvoiced'] = $customer->getIsAddedInvoiced();
        }
    }
}
