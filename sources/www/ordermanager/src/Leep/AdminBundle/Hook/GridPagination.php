<?php
namespace Leep\AdminBundle\Hook;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class GridPagination extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['paginationDisplayLength'] = 500;
    }
}
