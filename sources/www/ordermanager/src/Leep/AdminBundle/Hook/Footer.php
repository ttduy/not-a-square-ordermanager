<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class Footer extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['cacheBoxData'] = str_replace("'", "\'", json_encode($controller->get('leep_admin.helper.cache_box_data')->getCacheBoxData()));
    }
}
