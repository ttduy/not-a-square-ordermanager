<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class Header extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');

        $data['appRequestId'] = $controller->getRequest()->get('id', 0);
        $data['pageTitle'] = 'Order Manager';
        $data['quickAccess'] = array(
            'quickForm' => ['nameShortcut' => 'Form',
                                'link' => $manager->getUrl('leep_admin', 'form', 'grid', 'list')],
            'quickNutriMe' => ['nameShortcut' => 'NutriMe',
                                'link' => $manager->getUrl('leep_admin', 'geno_nutri_me', 'create', 'create')],
            'quickPellet' => ['nameShortcut' => 'Pellet',
                                'link' => $manager->getUrl('leep_admin', 'geno_pellet', 'grid', 'list')],
            'quickBatch' => ['nameShortcut'=> 'Batch',
                                'link' => $manager->getUrl('leep_admin', 'geno_batch', 'grid', 'list')],
            'quickOrder' => ['nameShortcut' => 'Order',
                                'link' => $manager->getUrl('leep_admin', 'customer', 'grid', 'list')],
            'quickContact' => ['nameShortcut' => 'Contact',
                                'link' => $manager->getUrl('leep_admin', 'contact', 'grid', 'list')],
            'quickSearch' => ['nameShortcut' => 'Search',
                                'link' => $manager->getUrl('leep_admin', 'search', 'feature', 'search')],
            'quickRemoveStock' => ['nameShortcut' => 'Remove stock',
                                'link' => $manager->getUrl('leep_admin', 'removeStock', 'feature', 'removeStock')]
        );
        $data['logoutUrl'] = $controller->generateUrl('Security_Logout');
        $data['exitSwitchUserUrl'] = $manager->getUrl('leep_admin', 'app_utils', 'app_utils', 'exitSwitchTranslator', array('_switch_user' => '_exit'));
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $userSession = $userManager->getUserSession();
        if ($userSession != null) {
            $data['username'] = $userSession->getUser()->getUsername();
        }

        $data['isLockReport'] = $controller->get('service_container')->getParameter('isLockReport', false);

        $menuBuilder = $controller->get('leep_admin.helper.menu_builder');

        $this->buildMenu($menuBuilder, $controller);

        $data['menu'] = array(
            'content' => $menuBuilder->getMenu(),
            'selected' => $options['menuSelected']
        );

        $data['links'] = array(
            'orderBackup' => array(
                'getMatchedOrders'  => $manager->getUrl('leep_admin', 'order_backup', 'feature', 'getMatchedOrders')
            ),
            'rating'    => array(
                'ratingDC' => $manager->getUrl('leep_admin', 'rating', 'create', 'create', array('type' => Business\Rating\Constant::RATING_TYPE_DISTRIBUTION_CHANNEL)),
                'ratingAcquisiteur' => $manager->getUrl('leep_admin', 'rating', 'create', 'create', array('type' => Business\Rating\Constant::RATING_TYPE_ACQUISITEUR)),
                'ratingPartner' => $manager->getUrl('leep_admin', 'rating', 'create', 'create', array('type' => Business\Rating\Constant::RATING_TYPE_PARTNER)),
                'viewDCRatingHistory' => $manager->getUrl('leep_admin', 'rating', 'feature', 'dcRatingHistory')
            ),
            'distributionChannel' => array(
                'ajaxRead' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'ajaxRead'),
                'countryNoteCreate' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'countryNoteCreate'),
                'countryNoteUpdate' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'countryNoteUpdate'),
                'countryNoteDelete' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'countryNoteDelete'),
                'continentNoteUpdate' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'continentNoteUpdate'),
                'exportDC' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'exportCsv'),
                'exportRating' => $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'exportRatingCsv'),
                'bulkEdit' => $manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_setting', 'edit'),
                'bulkEditReportSetting' => $manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_report_setting', 'edit'),
                'bulkEditWebLogin' => $manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_web_login', 'edit'),
                'webLoginMassEmail' => $manager->getUrl('leep_admin', 'distribution_channel', 'feature', 'webLoginMassEmail'),
                'editLink' => $manager->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit'),
                'getPartnerIdUrl'  => $manager->getUrl('leep_admin', 'distribution_channel', 'feature', 'getPartnerId')
            ),
            'partner'             => array(
                'ajaxRead' => $manager->getUrl('leep_admin', 'partner', 'partner', 'ajaxRead'),
                'webLoginMassEmail' => $manager->getUrl('leep_admin', 'partner', 'feature', 'webLoginMassEmail'),
                'edit'  => $manager->getUrl('leep_admin', 'partner', 'edit', 'edit'),
                'exportPartner' => $manager->getUrl('leep_admin', 'partner', 'feature', 'exportPartnerToCsv')
            ),
            'emailTemplate'       => array(
                'ajaxRead' => $manager->getUrl('leep_admin', 'email_template', 'email_template', 'ajaxRead'),
                'ajaxReadIdEmailTemplate' => $manager->getUrl('leep_admin', 'email_template', 'email_template', 'ajaxReadIdEmailTemplate')
            ),
            'formulaTemplate'     => array(
                'execute'      => $manager->getUrl('leep_admin', 'formula_template', 'feature', 'execute'),
            ),
            'material'     => array(
                'bulkEdit'     => $manager->getUrl('leep_admin', 'material', 'material', 'bulkEdit'),
            ),
            'textBlock'           => array(
                'synchronizeGroup' => $manager->getUrl('leep_admin', 'text_block', 'feature', 'synchronizeGroup'),
                'copyLanguage'     => $manager->getUrl('leep_admin', 'text_block', 'feature', 'copyLanguage'),
                'cache'            => $manager->getUrl('leep_admin', 'text_block', 'feature', 'cache'),
                'downloadAll'      => $manager->getUrl('leep_admin', 'text_block', 'feature', 'downloadAll'),
                'syncServers'      => $manager->getUrl('leep_admin', 'text_block', 'feature', 'syncServers')
            ),
            'mwaValidCode'        => array(
                'validate'   => $manager->getUrl('leep_admin', 'mwa_valid_code', 'feature', 'validate'),
                'deleteAll'  => $manager->getUrl('leep_admin', 'mwa_valid_code', 'feature', 'deleteAll'),
                'download'   => $manager->getUrl('leep_admin', 'mwa_valid_code', 'feature', 'download'),
            ),
            'customer'            => array(
                'editOrder'               => $manager->getUrl('leep_admin', 'customer', 'edit', 'edit'),
                'bulkEditStatus'          => $manager->getUrl('leep_admin', 'customer', 'customer', 'bulkEditStatus'),
                'bulkEditSetting'         => $manager->getUrl('leep_admin', 'customer', 'bulk_edit_setting', 'edit'),
                'bulkEditStatusValidate'  => $manager->getUrl('leep_admin', 'customer', 'customer', 'bulkEditStatusValidate'),
                'bulkMergeCheckOrder'     => $manager->getUrl('leep_admin', 'customer', 'customer', 'bulkMergeCheckOrder'),
                'bulkEditResultReady'     => $manager->getUrl('leep_admin', 'customer', 'customer', 'bulkEditResultReady'),
                'checkDuplication'        => $manager->getUrl('leep_admin', 'customer', 'customer', 'checkDuplication'),
                'editColor'               => $manager->getUrl('leep_admin', 'customer', 'customer', 'editColor'),
                'toggleRecallFlag'        => $manager->getUrl('leep_admin', 'customer', 'customer', 'toggleRecallFlag'),
                'exportMassarrays'        => $manager->getUrl('leep_admin', 'customer', 'customer', 'exportMassarrays'),
                'exportSNPS'              => $manager->getUrl('leep_admin', 'customer', 'customer', 'exportSNPS'),
                'updateLabDetails'        => $manager->getUrl('leep_admin', 'customer', 'customer', 'updateLabDetails'),
                'exportCustomers'         => $manager->getUrl('leep_admin', 'customer', 'customer', 'exportCustomers'),
                'viewDuplication'         => $manager->getUrl('leep_admin', 'customer', 'customer', 'viewDuplication'),
                'getAcquisiteurIdUrl'     => $manager->getUrl('leep_admin', 'customer', 'feature', 'getAcquisiteurId'),
                'getAcquisiteurIdsUrl'     => $manager->getUrl('leep_admin', 'customer', 'feature', 'getAcquisiteurIds')
            ),
            'mwaSample'           => array(
                'bulkEdit'     => $manager->getUrl('leep_admin', 'mwa_sample', 'feature', 'bulkEdit'),
                'bulkValidate' => $manager->getUrl('leep_admin', 'mwa_sample', 'feature', 'bulkValidate'),
                'bulkGetStatus' => $manager->getUrl('leep_admin', 'mwa_sample', 'feature', 'bulkGetStatus'),
                'bulkEditTat'  => $manager->getUrl('leep_admin', 'mwa_sample', 'feature', 'bulkEditTat'),
                'computePattern' => $manager->getUrl('leep_admin', 'mwa_sample', 'feature', 'computePattern'),
            ),
            'cryosaveSample'           => array(
                'bulkEdit'     => $manager->getUrl('leep_admin', 'cryosave_sample', 'feature', 'bulkEdit'),
                'bulkValidate' => $manager->getUrl('leep_admin', 'cryosave_sample', 'feature', 'bulkValidate'),
                'export'     => $manager->getUrl('leep_admin', 'cryosave_sample', 'feature', 'export'),
            ),
            'report'              => array(
                'edit'          => $manager->getUrl('leep_admin', 'report', 'edit', 'edit'),
                'edit_section'  => $manager->getUrl('leep_admin', 'report', 'edit_section', 'edit'),
                'editCopy'      => $manager->getUrl('leep_admin', 'report', 'edit_copy', 'edit'),
                'scanTextBlock'    => $manager->getUrl('leep_admin', 'report', 'report', 'scanTextBlock'),
                'checkTranslateUrl' => $manager->getUrl('leep_admin', 'report', 'report', 'checkTranslateReport')
            ),
            'russiaMwaSample'           => array(
                'bulkEdit'     => $manager->getUrl('leep_admin', 'russia_mwa_sample', 'feature', 'bulkEdit'),
                'bulkValidate' => $manager->getUrl('leep_admin', 'russia_mwa_sample', 'feature', 'bulkValidate'),
                'bulkGetStatus' => $manager->getUrl('leep_admin', 'russia_mwa_sample', 'feature', 'bulkGetStatus'),
                'bulkEditTat'  => $manager->getUrl('leep_admin', 'russia_mwa_sample', 'feature', 'bulkEditTat'),
                'computePattern' => $manager->getUrl('leep_admin', 'russia_mwa_sample', 'feature', 'computePattern')
            ),
            'currency'                  => array(
                'getCurrencyRate' => $manager->getUrl('leep_admin', 'currency', 'feature', 'getCurrencyRate')
            ),
            'gene_manager'        => array(
                'submitResult'  => $manager->getUrl('leep_admin', 'gene_manager', 'submit_result', 'create'),
                'viewResultHistory' =>  $manager->getUrl('leep_admin', 'gene_manager', 'grid_result_history', 'list'),
                'exportGeneResultByOrderNumber' => $manager->getUrl('leep_admin', 'gene_manager', 'feature', 'exportGeneResultByOrderNumber')
            ),
            'customerReport'      => array(
                'fixFileName' => $manager->getUrl('leep_admin', 'customer_report', 'feature', 'fixFileName'),
                'unLinkFile' => $manager->getUrl('leep_admin', 'customer_report', 'feature', 'unLinkFile'),
                'loadCustomerData'  => $manager->getUrl('leep_admin', 'customer_report', 'feature', 'loadCustomerData'),
                'computeFinalReportData' => $manager->getUrl('leep_admin', 'customer_report', 'feature', 'computeFinalReportData'),
                'viewCustomerData' => $manager->getUrl('leep_admin', 'customer_report', 'feature', 'viewCustomerData')
            ),
            'foodTableFoodItem'   => array(
                'synchronizeGroup' => $manager->getUrl('leep_admin', 'food_table_food_item', 'feature', 'synchronizeGroup'),
            ),
            'translate_text_block'        => array(
                'saveTextBlock'  => $manager->getUrl('leep_admin', 'translate_text_block', 'feature', 'saveTextBlock')
            ),
            'sampleImportJob'     => array(
                'importFromLytech' => $manager->getUrl('leep_admin', 'sample_import_job', 'feature', 'importFromLytech'),
                'viewLogUrl'    => $manager->getUrl('leep_admin', 'sample_import_job', 'feature', 'viewLog')
            ),
            'scanForm'           => array(
                'generateCode'     => $manager->getUrl('leep_admin', 'scan_form', 'feature', 'generateCode'),
                'downloadScanFormPdf'     => $manager->getUrl('leep_admin', 'scan_form', 'feature', 'downloadScanFormPdf'),
            ),
            'scanFile'           => array(
                'list'   => $manager->getUrl('leep_admin', 'scan_file', 'grid', 'list'),
                'processPendingForCreation' => $manager->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForCreation'),
                'processPendingForUpdation' => $manager->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForUpdation'),
                'formulaGenerateOrder' => $manager->getUrl('leep_admin', 'scan_file', 'feature', 'formulaGenerateOrder'),
                'formulaUpdateOrder' => $manager->getUrl('leep_admin', 'scan_file', 'feature', 'formulaUpdateOrder'),
                'selectTemplate' => $manager->getUrl('leep_admin', 'scan_file', 'detect_template', 'edit'),
                'realignTemplate' => $manager->getUrl('leep_admin', 'scan_file', 'realign_template', 'edit'),
                'fillBasicData' => $manager->getUrl('leep_admin', 'scan_file', 'fill_basic_data', 'edit'),
                'generateOrder' => $manager->getUrl('leep_admin', 'scan_file', 'generate_order', 'edit'),
                'fillAllData' => $manager->getUrl('leep_admin', 'scan_file', 'fill_all_data', 'edit'),
                'updateOrder' => $manager->getUrl('leep_admin', 'scan_file', 'update_order', 'edit'),
                'markError'   => $manager->getUrl('leep_admin', 'scan_file', 'feature', 'markError')
            ),
            'plate'        => array(
                'edit'           => $manager->getUrl('leep_admin', 'plate', 'edit', 'edit'),
                'copy'           => $manager->getUrl('leep_admin', 'plate', 'copy', 'create'),
                'saveAnalyse'    => $manager->getUrl('leep_admin', 'plate', 'feature', 'saveAnalyse')
            ),
            'productPrice' => array(
                'importPrice'    => $manager->getUrl('leep_admin', 'product_price', 'import_price', 'edit')
            ),
            'integratedInvoice' => array(
                'bulkEditStatus'    => $manager->getUrl('leep_admin', 'integrated_invoice', 'bulk_edit_status', 'edit')
            ),
            'billing'    => array(
                'bulkProcess'       => $manager->getUrl('leep_admin', 'billing', 'bulk_process', 'edit'),
            ),
            'bill'       => array(
                'bulkEditStatus'    => $manager->getUrl('leep_admin', 'bill', 'bulk_edit_status', 'edit'),
                'viewOrderEntries'  => $manager->getUrl('leep_admin', 'bill', 'feature', 'viewOrderEntries')
            ),
            'todo'       => array(
                'createTask'        => $manager->getUrl('leep_admin', 'todo_task', 'create', 'create'),
            ),
            'genoPellet'     => array(
                'exportPelletEntries' => $manager->getUrl('leep_admin', 'geno_pellet', 'feature', 'exportPelletEntries'),
            ),
            'genoMaterial'     => array(
                'exportMaterial' => $manager->getUrl('leep_admin', 'geno_material', 'feature', 'exportMaterial'),
            ),
            'pellet'     => array(
                'exportPelletEntries' => $manager->getUrl('leep_admin', 'pellet', 'feature', 'exportPelletEntries'),
                'moveNextSection'   => $manager->getUrl('leep_admin', 'pellet', 'feature', 'moveNextSection'),
            ),
            'employeeFeedback'   => array(
                'viewForm'      => $manager->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'viewForm'),
                'viewSummary'           => $manager->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'viewSummary'),
                'updateStats'           => $manager->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'updateStats'),
                'viewEditAnswer' => $manager->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'viewEditAnswer'),
            ),
            'feedback'   => array(
                'viewSummary'       => $manager->getUrl('leep_admin', 'feedback_question', 'feature', 'viewSummary'),
                'updateStats'       => $manager->getUrl('leep_admin', 'feedback_question', 'feature', 'updateStats')
            ),
            'nutriMe'    => array(
                'bulkEditStatus'    => $manager->getUrl('leep_admin', 'nutri_me', 'feature', 'bulkEditStatus'),
                'bulkEditStatusValidate' => $manager->getUrl('leep_admin', 'nutri_me', 'feature', 'bulkEditStatusValidate'),
            ),
            'form'     => array(
                'editStep'   => $manager->getUrl('leep_admin', 'form', 'edit_step', 'edit'),
                'editItem'   => $manager->getUrl('leep_admin', 'form', 'edit_item', 'edit'),
                'makeVersion' => $manager->getUrl('leep_admin', 'form', 'feature', 'makeVersion'),
                'createVersionLog' => $manager->getUrl('leep_admin', 'form', 'create_version_log', 'create'),
                'showVersionHistoryLogs' => $manager->getUrl('leep_admin', 'form', 'feature', 'showVersionHistoryLogs'),
            ),
            'mailSender'     => array(
                'preview_email'   => $manager->getUrl('leep_admin', 'mail_sender', 'feature', 'previewEmail')
            ),
            'emailOutgoing'     => array(
                'bulkProcess'   => $manager->getUrl('leep_admin', 'email_outgoing', 'bulk_process', 'edit'),
                'uploadImage'   => $manager->getUrl('leep_admin', 'email_mass_sender', 'feature', 'uploadImage'),
                'viewUploadImage'   => $manager->getUrl('leep_admin', 'email_mass_sender', 'feature', 'uploadImage'),
            ),
            'overviewOrder'     => array(
                'overview'      => $manager->getUrl('leep_admin', 'overview_order', 'overview', 'edit')
            ),
            'genoStorage'    => array(
                'visualizeStorageProcess' => $manager->getUrl('leep_admin', 'geno_storage', 'visualize', 'edit')
            ),
            'genoNutriMe'    => array(
                'bulkEditStatus'    => $manager->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'bulkEditStatus'),
                'bulkEditStatusValidate' => $manager->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'bulkEditStatusValidate'),
            ),
            'lead'  => array(
                'exportLead' => $manager->getUrl('leep_admin', 'lead', 'feature', 'exportLead')
            ),
            'acquisiteur'   => array(
                'edit'  => $manager->getUrl('leep_admin', 'acquisiteur', 'edit', 'edit')
            ),
            'customerInfo' => array(
                'exportCsv'  => $manager->getUrl('leep_admin', 'customer_info', 'feature', 'exportToCsv')
            )
        );
    }


    public function buildMenu(&$menuBuilder, $controller) {
        $manager = $controller->get('easy_module.manager');

        //------------------------------------------------------------------------------------------------------------

        // DASHBOARD
        $menuBuilder->addMenuItem('Dashboard')
                    ->setLink($manager->getUrl('leep_admin', 'dashboard', 'dashboard', 'list'))
                    ->setClass('dashboard')
                    ->setTitle('Dashboard')
                    ->setPermission('*');

        // PRODUCT
        $menu = $menuBuilder->addMenuItem('Product')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Product');

        $productMenuGroup = $menu->addMenuGroupItem('ProductSubMenus');
        $productMenuGroup->addSubMenuItem('CategoryList')
                        ->setLink($manager->getUrl('leep_admin', 'category', 'grid', 'list'))
                        ->setTitle('Category - List')
                        ->setPermission('productView');
        $productMenuGroup->addSubMenuItem('CategoryCreate')
                        ->setLink($manager->getUrl('leep_admin', 'category', 'create', 'create'))
                        ->setTitle('Category - Add new')
                        ->setPermission('productModify');
        $productMenuGroup->addSubMenuItem('ProductList')
                        ->setLink($manager->getUrl('leep_admin', 'product', 'grid', 'list'))
                        ->setTitle('Product - List')
                        ->setPermission('productView');
        $productMenuGroup->addSubMenuItem('ProductCreate')
                        ->setLink($manager->getUrl('leep_admin', 'product', 'create', 'create'))
                        ->setTitle('Product - Add new')
                        ->setPermission('productModify');
        $productMenuGroup->addSubMenuItem('ProductTypeList')
                        ->setLink($manager->getUrl('leep_admin', 'product_type', 'grid', 'list'))
                        ->setTitle('Product Type - List')
                        ->setPermission('productGroupView');
        $productMenuGroup->addSubMenuItem('ProductTypeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'product_type', 'create', 'create'))
                        ->setTitle('Product Type - Add new')
                        ->setPermission('productGroupModify');
        $productMenuGroup->addSubMenuItem('GeneList')
                        ->setLink($manager->getUrl('leep_admin', 'gene', 'grid', 'list'))
                        ->setTitle('Gene - List')
                        ->setPermission('productView');
        $productMenuGroup->addSubMenuItem('GeneCreate')
                        ->setLink($manager->getUrl('leep_admin', 'gene', 'create', 'create'))
                        ->setTitle('Gene - Add new')
                        ->setPermission('productModify');

        // MASSARRAY
        $menu = $menuBuilder->addMenuItem('Massarray')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Massarray');

        $massarrayMenuGroup = $menu->addMenuGroupItem('MassarraySubMenus');
        $massarrayMenuGroup->addSubMenuItem('MassarrayList')
                        ->setLink($manager->getUrl('leep_admin', 'massarray', 'grid', 'list'))
                        ->setTitle('Massarray - List')
                        ->setPermission('massarrayView');
        $massarrayMenuGroup->addSubMenuItem('MassarrayCreate')
                        ->setLink($manager->getUrl('leep_admin', 'massarray', 'create', 'create'))
                        ->setTitle('Massarray - Add new')
                        ->setPermission('massarrayModify');

        // GENE MANAGER
        $menu = $menuBuilder->addMenuItem('GeneManager')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Gene Manager');

        $geneManagerMenuGroup = $menu->addMenuGroupItem('GeneManagerSubMenus');
        $geneManagerMenuGroup->addSubMenuItem('GeneManagerList')
                        ->setLink($manager->getUrl('leep_admin', 'gene_manager', 'grid', 'list'))
                        ->setTitle('Gene Manager - List')
                        ->setPermission('geneManagerView');
        $geneManagerMenuGroup->addSubMenuItem('ResultGroupList')
                        ->setLink($manager->getUrl('leep_admin', 'result_group', 'grid', 'list'))
                        ->setTitle('Result Group - List')
                        ->setPermission('resultGroupView');
        $geneManagerMenuGroup->addSubMenuItem('ResultGroupList')
                        ->setLink($manager->getUrl('leep_admin', 'result_group', 'create', 'create'))
                        ->setTitle('Result Group - Add New')
                        ->setPermission('resultGroupModify');

        // PYROS
        $menu = $menuBuilder->addMenuItem('Pyros')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Pyros');

        $pyrosMenuGroup = $menu->addMenuGroupItem('PyrosSubMenus');
        $pyrosMenuGroup->addSubMenuItem('PyrosList')
                        ->setLink($manager->getUrl('leep_admin', 'pyros', 'grid', 'list'))
                        ->setTitle('Pyros - List')
                        ->setPermission('pyrosView');
        $pyrosMenuGroup->addSubMenuItem('PyrosCreate')
                        ->setLink($manager->getUrl('leep_admin', 'pyros', 'create', 'create'))
                        ->setTitle('Pyros - Add New')
                        ->setPermission('pyrosModify');

        // PELLET
        $menu = $menuBuilder->addMenuItem('Pellet')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Pellet');

        $pelletMenuGroup = $menu->addMenuGroupItem('PelletSubMenus');
        $pelletMenuGroup->addSubMenuItem('PelletList')
                        ->setLink($manager->getUrl('leep_admin', 'pellet', 'grid', 'list'))
                        ->setTitle('Pellet - List')
                        ->setPermission('pelletView');
        $pelletMenuGroup->addSubMenuItem('PelletCreate')
                        ->setLink($manager->getUrl('leep_admin', 'pellet', 'create', 'create'))
                        ->setTitle('Pellet - Add New')
                        ->setPermission('pelletModify');
        $pelletMenuGroup->addSubMenuItem('CurrentShipmentInUse')
                        ->setLink($manager->getUrl('leep_admin', 'pellet', 'stockInUseGrid', 'list'))
                        ->setTitle('Current Shipment In Use')
                        ->setPermission('pelletView');

        // NUTRIME
        $menu = $menuBuilder->addMenuItem('NutriMe')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('NutriMe');

        $nutriMeMenuGroup = $menu->addMenuGroupItem('NutriMeSubMenus');
        $nutriMeMenuGroup->addSubMenuItem('NutriMeList')
                        ->setLink($manager->getUrl('leep_admin', 'nutri_me', 'grid', 'list'))
                        ->setTitle('NutriMe - List')
                        ->setPermission('nutrimeView');
        $nutriMeMenuGroup->addSubMenuItem('NutriMeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'nutri_me', 'create', 'create'))
                        ->setTitle('NutriMe - Add New')
                        ->setPermission('nutrimeModify');

        // PLATE
        $menu = $menuBuilder->addMenuItem('Plate')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Mass Portal');

        $plateMenuGroup = $menu->addMenuGroupItem('PlateSubMenus');
        $plateMenuGroup->addSubMenuItem('PlateList')
                        ->setLink($manager->getUrl('leep_admin', 'plate', 'grid', 'list'))
                        ->setTitle('Plate - List')
                        ->setPermission('plateView');
        $plateMenuGroup->addSubMenuItem('PlateCreate')
                        ->setLink($manager->getUrl('leep_admin', 'plate', 'create', 'create'))
                        ->setTitle('Plate - Add New')
                        ->setPermission('plateModify');
        $plateMenuGroup->addSubMenuItem('PlateTypeList')
                        ->setLink($manager->getUrl('leep_admin', 'plate_type', 'grid', 'list'))
                        ->setTitle('Plate Type - List')
                        ->setPermission('plateTypeView');
        $plateMenuGroup->addSubMenuItem('PlateTypeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'plate_type', 'create', 'create'))
                        ->setTitle('Plate Type - Add New')
                        ->setPermission('plateTypeModify');
        $plateMenuGroup->addSubMenuItem('DemoBankList')
                        ->setLink($manager->getUrl('leep_admin', 'demo_bank', 'grid', 'list'))
                        ->setTitle('Demo Bank - List')
                        ->setPermission('demoBankView');
        $plateMenuGroup->addSubMenuItem('DemoBankCreate')
                        ->setLink($manager->getUrl('leep_admin', 'demo_bank', 'create', 'create'))
                        ->setTitle('Demo Bank - Add New')
                        ->setPermission('demoBankModify');
        $plateMenuGroup->addSubMenuItem('DemoBankPreview')
                        ->setLink($manager->getUrl('leep_admin', 'demo_bank', 'feature', 'preview'))
                        ->setTitle('Demo Bank - Preview')
                        ->setPermission('demoBankView');
        $plateMenuGroup->addSubMenuItem('FamVicMappingRuleEdit')
                        ->setLink($manager->getUrl('leep_admin', 'plate', 'fam_vic_mapping_rule', 'edit'))
                        ->setTitle('Fam/Vic Conversion')
                        ->setPermission('plateModify');

        // PRICE
        $menu = $menuBuilder->addMenuItem('Price')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Price');

        $priceMenuGroup = $menu->addMenuGroupItem('PriceSubMenus');
        $priceMenuGroup->addSubMenuItem('PriceCategoryList')
                        ->setLink($manager->getUrl('leep_admin', 'price_category', 'grid', 'list'))
                        ->setTitle('Price Category - List')
                        ->setPermission('priceCategoryView');
        $priceMenuGroup->addSubMenuItem('PriceCategoryCreate')
                        ->setLink($manager->getUrl('leep_admin', 'price_category', 'create', 'create'))
                        ->setTitle('Price Category - Add New')
                        ->setPermission('priceCategoryModify');
        $priceMenuGroup->addSubMenuItem('CategoryPrice')
                        ->setLink($manager->getUrl('leep_admin', 'product_category_price', 'grid', 'list'))
                        ->setTitle('Category Price')
                        ->setPermission('categoryPriceView');
        $priceMenuGroup->addSubMenuItem('ProductPrice')
                        ->setLink($manager->getUrl('leep_admin', 'product_price', 'grid', 'list'))
                        ->setTitle('Product Price')
                        ->setPermission('productPriceView');
        $priceMenuGroup->addSubMenuItem('ExtraPriceReportDelivery')
                        ->setLink($manager->getUrl('leep_admin', 'extra_price', 'edit_price_report_delivery', 'edit'))
                        ->setTitle('Extra Price - Report Delivery')
                        ->setPermission('priceCategoryModify');

        // PRESS
        $menu = $menuBuilder->addMenuItem('Press')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Press');

        $pressMenuGroup = $menu->addMenuGroupItem('PressSubMenus');
        $pressMenuGroup->addSubMenuItem('PressList')
                        ->setLink($manager->getUrl('leep_admin', 'press', 'grid', 'list'))
                        ->setTitle('Press - List')
                        ->setPermission('pressView');
        $pressMenuGroup->addSubMenuItem('PressCreate')
                        ->setLink($manager->getUrl('leep_admin', 'press', 'create', 'create'))
                        ->setTitle('Press - Add New')
                        ->setPermission('pressModify');

        // ACQUISITEUR
        $menu = $menuBuilder->addMenuItem('Acquisiteur')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Acquisiteur');

        $acquisiteurMenuGroup = $menu->addMenuGroupItem('AcquisiteurSubMenus');
        $acquisiteurMenuGroup->addSubMenuItem('AcquisiteurList')
                        ->setLink($manager->getUrl('leep_admin', 'acquisiteur', 'grid', 'list'))
                        ->setTitle('Acquisiteur - List')
                        ->setPermission('acquisiteurView');
        $acquisiteurMenuGroup->addSubMenuItem('AcquisiteurCreate')
                        ->setLink($manager->getUrl('leep_admin', 'acquisiteur', 'create', 'create'))
                        ->setTitle('Acquisiteur - Add New')
                        ->setPermission('acquisiteurModify');

        // PARTNER
        $menu = $menuBuilder->addMenuItem('Partner')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Partner');

        $partnerMenuGroup = $menu->addMenuGroupItem('PartnerSubMenus');
        $partnerMenuGroup->addSubMenuItem('PartnerList')
                        ->setLink($manager->getUrl('leep_admin', 'partner', 'grid', 'list'))
                        ->setTitle('Partner - List')
                        ->setPermission('partnerView');
        $partnerMenuGroup->addSubMenuItem('PartnerCreate')
                        ->setLink($manager->getUrl('leep_admin', 'partner', 'create', 'create'))
                        ->setTitle('Partner - Add New')
                        ->setPermission('partnerModify');

        // DISTRIBUTION CHANNEL
        $menu = $menuBuilder->addMenuItem('DistributionChannel')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Distribution Channel');

        $dcMenuGroup = $menu->addMenuGroupItem('DCSubMenus');
        $dcMenuGroup->addSubMenuItem('DCList')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'grid', 'list'))
                        ->setTitle('Distribution Channel - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('DCCreate')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'create', 'create'))
                        ->setTitle('Distribution Channel - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('DCDashboard')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'dashboard', 'list'))
                        ->setTitle('Distribution Channel - Dashboard')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('SpecialProductList')
                        ->setLink($manager->getUrl('leep_admin', 'special_product', 'grid', 'list'))
                        ->setTitle('Special Product - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('SpecialProductCreate')
                        ->setLink($manager->getUrl('leep_admin', 'special_product', 'create', 'create'))
                        ->setTitle('Special Product - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('DCTypeList')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel_type', 'grid', 'list'))
                        ->setTitle('Type - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('DCTypeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel_type', 'create', 'create'))
                        ->setTitle('Type - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('ViewOnGoogleMapList')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'googleMap'))
                        ->setTitle('View On Google Map')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('MarketingStatusList')
                        ->setLink($manager->getUrl('leep_admin', 'marketing_status', 'grid', 'list'))
                        ->setTitle('DC Info Status - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('MarketingStatusCreate')
                        ->setLink($manager->getUrl('leep_admin', 'marketing_status', 'create', 'create'))
                        ->setTitle('DC Info Status - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('VisualGroupProductList')
                        ->setLink($manager->getUrl('leep_admin', 'visible_group_product', 'grid', 'list'))
                        ->setTitle('Visible Group Product - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('VisualGroupProductCreate')
                        ->setLink($manager->getUrl('leep_admin', 'visible_group_product', 'create', 'create'))
                        ->setTitle('Visible Group Product - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('VisualGroupReportList')
                        ->setLink($manager->getUrl('leep_admin', 'visible_group_report', 'grid', 'list'))
                        ->setTitle('Visible Group Report - List')
                        ->setPermission('distributionChannelView');
        $dcMenuGroup->addSubMenuItem('VisualGroupReportCreate')
                        ->setLink($manager->getUrl('leep_admin', 'visible_group_report', 'create', 'create'))
                        ->setTitle('Visible Group Report - Add New')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('BulkEditDCSetting')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_setting', 'edit'))
                        ->setTitle('Bulk Edit DC Setting')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('BulkEditDCReportSetting')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_report_setting', 'edit'))
                        ->setTitle('Bulk Edit DC Report Setting')
                        ->setPermission('distributionChannelModify');
        $dcMenuGroup->addSubMenuItem('BulkEditDCWebLogin')
                        ->setLink($manager->getUrl('leep_admin', 'distribution_channel', 'bulk_edit_dc_web_login', 'edit'))
                        ->setTitle('Bulk Edit DC Web Login')
                        ->setPermission('distributionChannelModify');

        // LEAD
        $menu = $menuBuilder->addMenuItem('Lead')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Lead');

        $leadMenuGroup = $menu->addMenuGroupItem('LeadSubMenus');

        $leadMenuGroup->addSubMenuItem('LeadList')
                        ->setLink($manager->getUrl('leep_admin', 'lead', 'grid', 'list'))
                        ->setTitle('Lead - List')
                        ->setPermission('leadView');

        $leadMenuGroup->addSubMenuItem('LeadCreate')
                        ->setLink($manager->getUrl('leep_admin', 'lead', 'create', 'create'))
                        ->setTitle('Lead - Add New')
                        ->setPermission('leadModify');

        $leadMenuGroup->addSubMenuItem('LeadStatusList')
                        ->setLink($manager->getUrl('leep_admin', 'lead_status_value', 'grid', 'list'))
                        ->setTitle('Lead Status - List')
                        ->setPermission('leadStatusValueView');

        $leadMenuGroup->addSubMenuItem('LeadStatusCreate')
                        ->setLink($manager->getUrl('leep_admin', 'lead_status_value', 'create', 'create'))
                        ->setTitle('Lead Status - Add New')
                        ->setPermission('leadStatusValueModify');

        $leadMenuGroup->addSubMenuItem('LeadTypeList')
                        ->setLink($manager->getUrl('leep_admin', 'lead_type', 'grid', 'list'))
                        ->setTitle('Lead Category - List')
                        ->setPermission('leadTypeView');

        $leadMenuGroup->addSubMenuItem('LeadTypeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'lead_type', 'create', 'create'))
                        ->setTitle('Lead Category - Add new')
                        ->setPermission('leadTypeModify');

        //---------------------------------------------

        // MATERIAL
        $menu = $menuBuilder->addMenuItem('Material')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Material');

        $materialMenuGroup = $menu->addMenuGroupItem('MaterialSubMenus');
        $materialMenuGroup->addSubMenuItem('SetCurrentLot')
                        ->setLink($manager->getUrl('leep_admin', 'supplement_type', 'feature', 'list'))
                        ->setTitle('Set Current Lot')
                        ->setPermission('materialView');
        $materialMenuGroup->addSubMenuItem('MaterialTypeList')
                        ->setLink($manager->getUrl('leep_admin', 'material_type', 'grid', 'list'))
                        ->setTitle('Material Type - List')
                        ->setPermission('materialView');
        $materialMenuGroup->addSubMenuItem('MaterialTypeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'material_type', 'create', 'create'))
                        ->setTitle('Material Type - Add New')
                        ->setPermission('materialModify');
        $materialMenuGroup->addSubMenuItem('MaterialList')
                        ->setLink($manager->getUrl('leep_admin', 'material', 'grid', 'list'))
                        ->setTitle('Material - List')
                        ->setPermission('materialView');
        $materialMenuGroup->addSubMenuItem('MaterialCreate')
                        ->setLink($manager->getUrl('leep_admin', 'material', 'create', 'create'))
                        ->setTitle('Material - Add New')
                        ->setPermission('materialModify');

        $materialMenuGroup = $menu->addMenuGroupItem('SupplierOfferSubMenus');
        $materialMenuGroup->addSubMenuItem('SupplierOfferList')
                            ->setLink($manager->getUrl('leep_admin', 'supplier_offer', 'grid', 'list'))
                            ->setTitle('Supplier Offer ​- List')
                            ->setPermission('supplierOfferView');
        $materialMenuGroup->addSubMenuItem('SupplierOfferCreate')
                            ->setLink($manager->getUrl('leep_admin', 'supplier_offer', 'create', 'create'))
                            ->setTitle('Supplier Offer -​ Add New')
                            ->setPermission('supplierOfferModify');

        $materialMenuGroup->addSubMenuItem('MaterialFinanceTracker')
                        ->setLink($manager->getUrl('leep_admin', 'material', 'material_finance_tracker', 'materialType'))
                        ->setTitle('Material Finance Tracker')
                        ->setPermission('materialView');

        // GENO MATERIAL
        $menu = $menuBuilder->addMenuItem('GenoMaterial')
                        ->setLink('javascript:;')
                        ->setClass('dashboard')
                        ->setTitle('Geno Material');

        // Core geno material group
        $genoMaterialMenuGroup = $menu->addMenuGroupItem('GenoMaterialSubMenus');
        $genoMaterialMenuGroup->addSubMenuItem('GenoMaterialList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_material', 'grid', 'list'))
                        ->setTitle('Geno Material - List')
                        ->setPermission('genoMaterialView');

        $genoMaterialMenuGroup->addSubMenuItem('GenoMaterialAddNew')
                        ->setLink($manager->getUrl('leep_admin', 'geno_material', 'create', 'create'))
                        ->setTitle('Geno Material - Add new')
                        ->setPermission('genoMaterialModify');

        $genoMaterialMenuGroup->addSubMenuItem('GenoMaterialType')
                        ->setLink($manager->getUrl('leep_admin', 'geno_material_type', 'grid', 'list'))
                        ->setTitle('Geno Material Type - List')
                        ->setPermission('genoMaterialTypeView');

        $genoMaterialMenuGroup->addSubMenuItem('GenoMaterialType')
                        ->setLink($manager->getUrl('leep_admin', 'geno_material_type', 'create', 'create'))
                        ->setTitle('Geno Material Type - Add new')
                        ->setPermission('genoMaterialTypeModify');


        // Geno batch group
        $genoBatchMenuGroup = $menu->addMenuGroupItem('GenoBatchSubMenus');
        $genoBatchMenuGroup->addSubMenuItem('GenoBatchList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_batch', 'grid', 'list'))
                        ->setTitle('Geno Batch - List')
                        ->setPermission('genoBatchView');

        $genoBatchMenuGroup->addSubMenuItem('GenoBatchAddNew')
                        ->setLink($manager->getUrl('leep_admin', 'geno_batch', 'create', 'create'))
                        ->setTitle('Geno Batch - Add new')
                        ->setPermission('genoBatchModify');


        // GENO STORAGE
        $menu = $menuBuilder->addMenuItem('GenoStorage')
                        ->setLink('javascript:;')
                        ->setClass('dashboard')
                        ->setTitle('Geno Storage');

        $genoStorageMenuGroup = $menu->addMenuGroupItem('GenoStorageSubMenus');
        $genoStorageMenuGroup->addSubMenuItem('GenoStorageAddNew')
                        ->setLink($manager->getUrl('leep_admin', 'geno_storage', 'create', 'create'))
                        ->setTitle('Geno Storage - Add new')
                        ->setPermission('genoStorageModify');

        $genoStorageMenuGroup->addSubMenuItem('GenoStorageList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_storage', 'grid', 'list'))
                        ->setTitle('Geno Storage - List')
                        ->setPermission('genoStorageView');


        // GENO PELLET PRODUCTION
        $menu = $menuBuilder->addMenuItem('GenoPelletProduction')
                        ->setLink('javascript:;')
                        ->setClass('dashboard')
                        ->setTitle('Geno Pellet');

        // Geno Pellet
        $genoPelletMenuGroup = $menu->addMenuGroupItem('GenoPelletSubMenus');
        $genoPelletMenuGroup->addSubMenuItem('GenoPelletAddNew')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet', 'create', 'create'))
                        ->setTitle('Pellet - Add new')
                        ->setPermission('genoPelletModify');

        $genoPelletMenuGroup->addSubMenuItem('GenoPelletList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet', 'grid', 'list'))
                        ->setTitle('Pellet - List')
                        ->setPermission('genoPelletView');

        $genoPelletMenuGroup->addSubMenuItem('CurrentShipmentInUse')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet', 'stockInUseGrid', 'list'))
                        ->setTitle('Current Shipment In Use')
                        ->setPermission('genoPelletView');

        // Geno pellet formula
        $genoPelletFormulaMenuGroup = $menu->addMenuGroupItem('GenoPelletFormulaSubMenus');
        $genoPelletFormulaMenuGroup->addSubMenuItem('GenoPelletFormulaAddNew')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet_formula', 'create', 'create'))
                        ->setTitle('Pellet Formula - Add new')
                        ->setPermission('genoPelletFormulaModify');

        $genoPelletFormulaMenuGroup->addSubMenuItem('GenoPelletFormulaList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet_formula', 'grid', 'list'))
                        ->setTitle('Pellet Formula - List')
                        ->setPermission('genoPelletFormulaView');

        // Geno pellet production
        $GenoPelletProductionMenuGroup = $menu->addMenuGroupItem('GenoPelletProductionSubMenus');
        $GenoPelletProductionMenuGroup->addSubMenuItem('StartPelletProduction')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'listFormula'))
                        ->setTitle('Start Pellet Production')
                        ->setIsNewTab(true)
                        ->setPermission('genoPelletProductionModify');
        $GenoPelletProductionMenuGroup->addSubMenuItem('GenoPelletProductionList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_pellet_production', 'grid', 'list'))
                        ->setTitle('Pellet Production - List')
                        ->setPermission('genoPelletProductionView');


        // GENO NUTRIME
        $menu = $menuBuilder->addMenuItem('GenoNutriMe')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Geno NutriMe');

        $genoNutriMeMenuGroup = $menu->addMenuGroupItem('GenoNutriMeSubMenus');
        $genoNutriMeMenuGroup->addSubMenuItem('GenoNutriMeList')
                        ->setLink($manager->getUrl('leep_admin', 'geno_nutri_me', 'grid', 'list'))
                        ->setTitle('NutriMe - List')
                        ->setPermission('genoNutrimeView');
        $genoNutriMeMenuGroup->addSubMenuItem('GenoNutriMeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'geno_nutri_me', 'create', 'create'))
                        ->setTitle('NutriMe - Add New')
                        ->setPermission('genoNutrimeModify');


        // COMPANY
        $menu = $menuBuilder->addMenuItem('Company')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Company');

        $companyMenuGroup = $menu->addMenuGroupItem('CompanySubMenus');
        $companyMenuGroup->addSubMenuItem('CompanyList')
                        ->setLink($manager->getUrl('leep_admin', 'company', 'grid', 'list'))
                        ->setTitle('Company - List')
                        ->setPermission('companyView');
        $companyMenuGroup->addSubMenuItem('CompanyCreate')
                        ->setLink($manager->getUrl('leep_admin', 'company', 'create', 'create'))
                        ->setTitle('Company - Add New')
                        ->setPermission('companyModify');




        // // CONTRACT TRACKER
        // $menu = $menuBuilder->addMenuItem('ContractTracker')
        //             ->setLink('javascript:;')
        //             ->setClass('dashboard')
        //             ->setTitle('Contract Tracker');

        $geneManagerMenuGroup = $menu->addMenuGroupItem('DoctrineManagerSubMenus');
        $geneManagerMenuGroup->addSubMenuItem('GeneManagerList')
                        ->setLink($manager->getUrl('leep_admin', 'gene_manager', 'grid', 'list'))
                        ->setTitle('Contract Tracker - List')
                        ->setPermission('geneManagerView');

        // // $geneManagerMenuGroup->addSubMenuItem('ResultGroupList')
        // //                 ->setLink($manager->getUrl('leep_admin', 'result_group', 'grid', 'list'))
        // //                 ->setTitle('Result Group - List')
        // //                 ->setPermission('resultGroupView');
        // // $geneManagerMenuGroup->addSubMenuItem('ResultGroupList')
        // //                 ->setLink($manager->getUrl('leep_admin', 'result_group', 'create', 'create'))
        // //                 ->setTitle('Result Group - Add New')
        // //                 ->setPermission('resultGroupModify');




        // CUSTOMER INFO
        $menu = $menuBuilder->addMenuItem('CustomerInfo')
                    ->setLink('javascript:;')
                    ->setClass('dashboard')
                    ->setTitle('Customer Info');

        $customerInfoMenuGroup = $menu->addMenuGroupItem('CustomerInfoSubMenus');
        $customerInfoMenuGroup->addSubMenuItem('CustomerInfoList')
                        ->setLink($manager->getUrl('leep_admin', 'customer_info', 'grid', 'list'))
                        ->setTitle('Customer Info - List')
                        ->setPermission('customerInfoView');
        $customerInfoMenuGroup->addSubMenuItem('CompanyCreate')
                        ->setLink($manager->getUrl('leep_admin', 'customer_info', 'create', 'create'))
                        ->setTitle('Customer Info - Add New')
                        ->setPermission('customerInfoModify');
        $customerInfoMenuGroup->addSubMenuItem('CustomerInfoMerge')
                        ->setLink($manager->getUrl('leep_admin', 'customer_info', 'merge', 'edit'))
                        ->setTitle('Customer Info - Merge')
                        ->setPermission('customerInfoModify');

        // ORDER
        $menu = $menuBuilder->addMenuItem('Order')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Order');

        $orderMenuGroup = $menu->addMenuGroupItem('OrderSubMenus');
        $orderMenuGroup->addSubMenuItem('OrderList')
                        ->setLink($manager->getUrl('leep_admin', 'customer', 'grid', 'list'))
                        ->setTitle('Order - List')
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('OrderCreate')
                        ->setLink($manager->getUrl('leep_admin', 'customer', 'create', 'create'))
                        ->setTitle('Order - Add New')
                        ->setPermission('customerModify');
        $orderMenuGroup->addSubMenuItem('ProductFilter')
                        ->setLink($manager->getUrl('leep_admin', 'customer_product_filter', 'grid', 'list'))
                        ->setTitle('Product Filter')
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('OverviewOrder')
                        ->setLink($manager->getUrl('leep_admin', 'overview_order', 'overview', 'edit'))
                        ->setTitle('Overview Order')
                        ->setIsNewTab(true)
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('ColorCodeFilter')
                        ->setLink($manager->getUrl('leep_admin', 'customer_color_filter', 'grid', 'list'))
                        ->setTitle('Color Code Filter')
                        ->setPermission('customerColorFilterView');
        $orderMenuGroup->addSubMenuItem('AnalysisTime')
                        ->setLink($manager->getUrl('leep_admin', 'customer_analysis_time', 'create', 'create'))
                        ->setTitle('Analysis Time')
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('FormQuestion')
                        ->setLink($manager->getUrl('leep_admin', 'form_question', 'grid', 'list'))
                        ->setTitle('Form Question')
                        ->setPermission('formQuestionView');
        $orderMenuGroup->addSubMenuItem('PendingOrder')
                        ->setLink($manager->getUrl('leep_admin', 'order_pending', 'grid', 'list'))
                        ->setTitle('Pending Order')
                        ->setPermission('orderPendingView');
        $orderMenuGroup->addSubMenuItem('OrderBackupList')
                        ->setLink($manager->getUrl('leep_admin', 'order_backup', 'grid', 'list'))
                        ->setTitle('Order Backup - List')
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('OrderBackupCreate')
                        ->setLink($manager->getUrl('leep_admin', 'order_backup', 'create', 'create'))
                        ->setTitle('Order Backup - Add New')
                        ->setPermission('customerModify');
        $orderMenuGroup->addSubMenuItem('BodShipmentTracking')
                        ->setLink($manager->getUrl('leep_admin', 'bod_shipment_tracking', 'grid', 'list'))
                        ->setTitle('BOD Shipment Tracking')
                        ->setPermission('customerView');
        $orderMenuGroup->addSubMenuItem('BillList')
                        ->setLink($manager->getUrl('leep_admin', 'bod_shipment_log', 'grid', 'list'))
                        ->setTitle('BOD shipment log')
                        ->setPermission('customerView');

        // SUB ORDER
        $menu = $menuBuilder->addMenuItem('SubOrder')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Sub Order');

        $orderMenuGroup = $menu->addMenuGroupItem('SubOrderSubMenus');
        $orderMenuGroup->addSubMenuItem('SubOrderList')
                        ->setLink($manager->getUrl('leep_admin', 'sub_order', 'grid', 'list'))
                        ->setTitle('Sub Order - List')
                        ->setPermission('customerView');

        // CMS ORDER
        $menu = $menuBuilder->addMenuItem('CMSOrder')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('CMS Order');

        $menuGroup = $menu->addMenuGroupItem('CMSOrderSubMenus');
        $menuGroup->addSubMenuItem('CMSOrderList')
                        ->setLink($manager->getUrl('leep_admin', 'cms_order', 'grid', 'list'))
                        ->setTitle('Order - List')
                        ->setPermission('cmsOrderView');
        $menuGroup->addSubMenuItem('CMSOrderCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cms_order', 'create', 'create'))
                        ->setTitle('Order - Add New')
                        ->setPermission('cmsOrderModify');

        $menuGroup->addSubMenuItem('CMSMessageList')
                        ->setLink($manager->getUrl('leep_admin', 'cms_contact_message', 'grid', 'list'))
                        ->setTitle('Messages - List')
                        ->setPermission('cmsOrderView');
        $menuGroup->addSubMenuItem('CMSMessageCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cms_contact_message', 'create', 'create'))
                        ->setTitle('Messages - Add New')
                        ->setPermission('cmsOrderModify');


        $menuGroup->addSubMenuItem('CMSProductList')
                        ->setLink($manager->getUrl('leep_admin', 'cms_product', 'grid', 'list'))
                        ->setTitle('Product - List')
                        ->setPermission('cmsOrderView');
        $menuGroup->addSubMenuItem('CMSProductCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cms_product', 'create', 'create'))
                        ->setTitle('Product - Add New')
                        ->setPermission('cmsOrderModify');

        $menuGroup->addSubMenuItem('CMSSourceList')
                        ->setLink($manager->getUrl('leep_admin', 'cms_source', 'grid', 'list'))
                        ->setTitle('Source - List')
                        ->setPermission('cmsOrderView');
        $menuGroup->addSubMenuItem('CMSSourceCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cms_source', 'create', 'create'))
                        ->setTitle('Source - Add New')
                        ->setPermission('cmsOrderModify');

        $menuGroup->addSubMenuItem('CMSStatusList')
                        ->setLink($manager->getUrl('leep_admin', 'cms_order_status_value', 'grid', 'list'))
                        ->setTitle('Order Status - List')
                        ->setPermission('cmsOrderView');
        $menuGroup->addSubMenuItem('CMSSourceCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cms_order_status_value', 'create', 'create'))
                        ->setTitle('Order Status - Add New')
                        ->setPermission('cmsOrderModify');

        // Genlife order
        $menu = $menuBuilder->addMenuItem('Genlife')
            ->setLink('javascript:;')
            ->setClass('users')
            ->setTitle('Genlife');
        $menuGroup = $menu->addMenuGroupItem('GenlifeSubMenus');
        $menuGroup->addSubMenuItem('GenlifeSampleRegistrationList')
                   ->setLink($manager->getUrl('leep_admin', 'genlife_sample_registration', 'grid', 'list'))
                   ->setTitle('Sample Registration - List')
                   ->setPermission('genlifeSampleRegistrationView');
        $menuGroup->addSubMenuItem('GenlifeTextBlockList')
                   ->setLink($manager->getUrl('leep_admin', 'genlife_text_block', 'grid', 'list'))
                   ->setTitle('Text Block - List')
                   ->setPermission('genlifeTextBlockView');



        //SHIPMENT MANAGER
        $menu = $menuBuilder->addMenuItem('ShipmentManager')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Shipment Manager');
        $shipmentManagerMenuGroup=$menu->addMenuGroupItem('ShipmentManagerMenus');
        $shipmentManagerMenuGroup->addSubMenuItem('ShipmentManagerList')
                        ->setLink($manager->getUrl('leep_admin', 'shipment_manager', 'grid', 'list'))
                        ->setTitle('Shipment Manager - List')
                        ->setPermission('shipmentManagerView');
        $shipmentManagerMenuGroup->addSubMenuItem('ShipmentCreate')
                        ->setLink($manager->getUrl('leep_admin', 'shipment_manager', 'create', 'create'))
                        ->setTitle('Shipment - Add New')
                        ->setPermission('shipmentManagerModify');


        // BILLING
        $menu = $menuBuilder->addMenuItem('Billing')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Billing');

        $billingMenuGroup = $menu->addMenuGroupItem('BillingSubMenus');
        $billingMenuGroup->addSubMenuItem('PricingSetting')
                        ->setLink($manager->getUrl('leep_admin', 'pricing_setting', 'edit', 'edit'))
                        ->setTitle('Auto pricing setting')
                        ->setPermission('billingModify');
        $billingMenuGroup->addSubMenuItem('CustomerPricing')
                        ->setLink($manager->getUrl('leep_admin', 'billing', 'customer_pricing_grid', 'list'))
                        ->setTitle('Pricing detail')
                        ->setPermission('billingView');

        $billingMenuGroup->addSubMenuItem('BillDashboard')
                        ->setLink($manager->getUrl('leep_admin', 'bill', 'feature', 'dashboard'))
                        ->setTitle('Bill - Dashboard')
                        ->setPermission('billingView');
        $billingMenuGroup->addSubMenuItem('BillList')
                        ->setLink($manager->getUrl('leep_admin', 'bill', 'grid', 'list'))
                        ->setTitle('Bill - List')
                        ->setPermission('billingView');
        $billingMenuGroup->addSubMenuItem('BillCreate')
                        ->setLink($manager->getUrl('leep_admin', 'bill', 'create', 'create'))
                        ->setTitle('Bill - Add New')
                        ->setPermission('billingModify');
        $billingMenuGroup->addSubMenuItem('InvoiceNoTaxTextList')
                        ->setLink($manager->getUrl('leep_admin', 'invoice_no_tax_text', 'grid', 'list'))
                        ->setTitle('Invoice No Tax Text - List')
                        ->setPermission('billingView');
        $billingMenuGroup->addSubMenuItem('InvoiceNoTaxTextCreate')
                        ->setLink($manager->getUrl('leep_admin', 'invoice_no_tax_text', 'create', 'create'))
                        ->setTitle('Invoice No Tax Text - Add new')
                        ->setPermission('billingModify');
        $billingMenuGroup->addSubMenuItem('PaymentNoTaxTextList')
                        ->setLink($manager->getUrl('leep_admin', 'payment_no_tax_text', 'grid', 'list'))
                        ->setTitle('Payment No Tax Text - List')
                        ->setPermission('billingView');
        $billingMenuGroup->addSubMenuItem('PaymentNoTaxTextCreate')
                        ->setLink($manager->getUrl('leep_admin', 'payment_no_tax_text', 'create', 'create'))
                        ->setTitle('Payment No Tax Text - Add new')
                        ->setPermission('billingModify');

        // COLLECTIVE INVOICE
        $menu = $menuBuilder->addMenuItem('CollectiveInvoice')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Collective Invoice');

        $collectiveInvoiceMenuGroup = $menu->addMenuGroupItem('CollectiveInvoiceSubMenus');
        $collectiveInvoiceMenuGroup->addSubMenuItem('CollectiveInvoiceList')
                        ->setLink($manager->getUrl('leep_admin', 'collective_invoice', 'grid', 'list'))
                        ->setTitle('Collective Invoice - List')
                        ->setPermission('collectiveInvoiceView');
        $collectiveInvoiceMenuGroup->addSubMenuItem('CollectiveInvoiceCreate')
                        ->setLink($manager->getUrl('leep_admin', 'collective_invoice', 'invoice_form', 'list'))
                        ->setTitle('Collective Invoice - Add New')
                        ->setPermission('collectiveInvoiceModify');

        // COLLECTIVE PAYMENT
        $menu = $menuBuilder->addMenuItem('CollectivePayment')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Collective Payment');

        $collectivePaymentMenuGroup = $menu->addMenuGroupItem('CollectivePaymentSubMenus');
        $collectivePaymentMenuGroup->addSubMenuItem('CollectivePaymentList')
                        ->setLink($manager->getUrl('leep_admin', 'collective_payment', 'grid', 'list'))
                        ->setTitle('Collective Payment - List')
                        ->setPermission('collectivePaymentView');
        $collectivePaymentMenuGroup->addSubMenuItem('CollectivePaymentCreate')
                        ->setLink($manager->getUrl('leep_admin', 'collective_payment', 'payment_form', 'list'))
                        ->setTitle('Collective Payment - Add New')
                        ->setPermission('collectivePaymentModify');

        // EMAIL TEMPLATE
        $menu = $menuBuilder->addMenuItem('EmailTemplate')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Email Template');

        $emailTemplateMenuGroup = $menu->addMenuGroupItem('EmailTemplateSubMenus');
        $emailTemplateMenuGroup->addSubMenuItem('EmailTemplateList')
                        ->setLink($manager->getUrl('leep_admin', 'email_template', 'grid', 'list'))
                        ->setTitle('Email Template - List')
                        ->setPermission('emailTemplateView');
        $emailTemplateMenuGroup->addSubMenuItem('EmailTemplateCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_template', 'create', 'create'))
                        ->setTitle('Email Template - Add New')
                        ->setPermission('emailTemplateModify');
        // STATUS
        $menu = $menuBuilder->addMenuItem('Status')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Status');

        $statusMenuGroup = $menu->addMenuGroupItem('StatusSubMenus');
        $statusMenuGroup->addSubMenuItem('OrderStatusList')
                        ->setLink($manager->getUrl('leep_admin', 'status', 'grid', 'list'))
                        ->setTitle('Order Status - List')
                        ->setPermission('statusView');
        $statusMenuGroup->addSubMenuItem('OrderStatusCreate')
                        ->setLink($manager->getUrl('leep_admin', 'status', 'create', 'create'))
                        ->setTitle('Order Status - Add New')
                        ->setPermission('statusModify');
        // $statusMenuGroup->addSubMenuItem('LeadStatusList')
        //                 ->setLink($manager->getUrl('leep_admin', 'lead_status_value', 'grid', 'list'))
        //                 ->setTitle('Lead Status - List')
        //                 ->setPermission('leadStatusValueView');
        // $statusMenuGroup->addSubMenuItem('LeadStatusCreate')
        //                 ->setLink($manager->getUrl('leep_admin', 'lead_status_value', 'create', 'create'))
        //                 ->setTitle('Lead Status - Add New')
        //                 ->setPermission('leadStatusValueModify');
        $statusMenuGroup->addSubMenuItem('CustomerDashboardStatus')
                        ->setLink($manager->getUrl('leep_admin', 'customer_dashboard_status', 'edit', 'edit'))
                        ->setTitle('Customer Dashboard Status')
                        ->setPermission('statusModify');

        // CURRENCY
        $menu = $menuBuilder->addMenuItem('Currency')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Currency');

        $currencyMenuGroup = $menu->addMenuGroupItem('CurrencySubMenus');
        $currencyMenuGroup->addSubMenuItem('CurrencyList')
                        ->setLink($manager->getUrl('leep_admin', 'currency', 'grid', 'list'))
                        ->setTitle('Currency - List')
                        ->setPermission('currencyView');
        $currencyMenuGroup->addSubMenuItem('CurrencyCreate')
                        ->setLink($manager->getUrl('leep_admin', 'currency', 'create', 'create'))
                        ->setTitle('Currency - Add New')
                        ->setPermission('currencyModify');

        // SCAN FORM
        $menu = $menuBuilder->addMenuItem('ScanForm')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Scan Form');

        // SCAN FORM TEMPLATE
        $menu = $menuBuilder->addMenuItem('ScanFormTemplate')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Scan Form');

        $scanFormTemplateMenuGroup = $menu->addMenuGroupItem('ScanFormTemplateSubMenus');
        $scanFormTemplateMenuGroup->addSubMenuItem('ScanFormTemlateList')
                        ->setLink($manager->getUrl('leep_admin', 'scan_form_template', 'grid', 'list'))
                        ->setTitle('Template - List')
                        ->setPermission('scanFormView');
        $scanFormTemplateMenuGroup->addSubMenuItem('ScanFormTemplateCreate')
                        ->setLink($manager->getUrl('leep_admin', 'scan_form_template', 'create', 'create'))
                        ->setTitle('Template - Add New')
                        ->setPermission('scanFormModify');
        $scanFormTemplateMenuGroup->addSubMenuItem('ScanFormFile')
                        ->setLink($manager->getUrl('leep_admin', 'scan_file', 'grid', 'list'))
                        ->setTitle('Scan Files - List')
                        ->setPermission('scanFormModify');
        $scanFormTemplateMenuGroup->addSubMenuItem('ScanFormFile')
                        ->setLink($manager->getUrl('leep_admin', 'scan_file', 'create', 'create'))
                        ->setTitle('Scan Files - Upload')
                        ->setPermission('scanFormModify');

        // FORM
        $menu = $menuBuilder->addMenuItem('Form')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Form');

        $formMenuGroup = $menu->addMenuGroupItem('FormSubMenus');
        $formMenuGroup->addSubMenuItem('FormList')
                        ->setLink($manager->getUrl('leep_admin', 'form', 'grid', 'list'))
                        ->setTitle('Form - List')
                        ->setPermission('formView');
        $formMenuGroup->addSubMenuItem('FormCreate')
                        ->setLink($manager->getUrl('leep_admin', 'form', 'create', 'create'))
                        ->setTitle('Form - Add New')
                        ->setPermission('formModify');
        $formMenuGroup->addSubMenuItem('UseForm')
                        ->setLink($manager->getUrl('leep_admin', 'form', 'feature', 'listForm'))
                        ->setTitle('Use Form')
                        ->setIsNewTab(true)
                        ->setPermission('formSessionView');
        $formMenuGroup->addSubMenuItem('ViewSession')
                        ->setLink($manager->getUrl('leep_admin', 'form', 'form_session_grid', 'list'))
                        ->setTitle('View Sessions')
                        ->setPermission('formSessionModify');
        $formMenuGroup->addSubMenuItem('AuthorizedPersonnelList')
                        ->setLink($manager->getUrl('leep_admin', 'authorized_personnel', 'grid', 'list'))
                        ->setTitle('Authorized Personnel - List')
                        ->setPermission('authorizedPersonnelView');
        $formMenuGroup->addSubMenuItem('AuthorizedPersonnelCreate')
                        ->setLink($manager->getUrl('leep_admin', 'authorized_personnel', 'create', 'create'))
                        ->setTitle('Authorized Personnel - Add New')
                        ->setPermission('authorizedPersonnelModify');
        $formMenuGroup->addSubMenuItem('genoFormProcess')
                      ->setLink($manager->getUrl('leep_admin', 'form', 'feature', 'showGenoForm'))
                      ->setTitle('Geno Form')
                      ->setIsNewTab(true)
                      ->setPermission('genoFormView');

        // FEEDBACK QUESTION
        $menu = $menuBuilder->addMenuItem('FeedbackQuestion')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Feedback Questions');

        $feedbackQuestionMenuGroup = $menu->addMenuGroupItem('FeedbackQuestionSubMenus');
        $feedbackQuestionMenuGroup->addSubMenuItem('FeedbackQuestionList')
                        ->setLink($manager->getUrl('leep_admin', 'feedback_question', 'grid', 'list'))
                        ->setTitle('Feedback Question - List')
                        ->setPermission('feedbackQuestionView');
        $feedbackQuestionMenuGroup->addSubMenuItem('FeedbackQuestionCreate')
                        ->setLink($manager->getUrl('leep_admin', 'feedback_question', 'create', 'create'))
                        ->setTitle('Feedback Question - Add New')
                        ->setPermission('feedbackQuestionModify');

        // EMPLOYEE FEEDBACK QUESTION
        $menu = $menuBuilder->addMenuItem('EmployeeFeedbackQuestion')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Employee Satisfaction');

        $employeeFeedbackQuestionMenuGroup = $menu->addMenuGroupItem('EmployeeFeedbackQuestionSubMenus');
        $employeeFeedbackQuestionMenuGroup->addSubMenuItem('EmployeeFeedbackQuestionList')
                        ->setLink($manager->getUrl('leep_admin', 'employee_feedback_question', 'grid', 'list'))
                        ->setTitle('Employee Satisfaction Question - List')
                        ->setPermission('employeeFeedbackQuestionView');
        $employeeFeedbackQuestionMenuGroup->addSubMenuItem('EmployeeFeedbackQuestionCreate')
                        ->setLink($manager->getUrl('leep_admin', 'employee_feedback_question', 'create', 'create'))
                        ->setTitle('Employee Satisfaction Question - Add New')
                        ->setPermission('employeeFeedbackQuestionModify');

        // GCMS PROCESSOR
        $menu = $menuBuilder->addMenuItem('GCMSProcessor')
                    ->setLink($manager->getUrl('leep_admin', 'gcms_processor', 'create', 'create'))
                    ->setClass('widgets')
                    ->setTitle('GCMS Processor')
                    ->setPermission('customerView');

        // CRYOSAVE
        $menu = $menuBuilder->addMenuItem('Cryosave')
                    ->setLink('javascript:;')
                    ->setClass('widgets')
                    ->setTitle('Cryosave');

        $cryosaveMenuGroup = $menu->addMenuGroupItem('CryosaveSubMenus');
        $cryosaveMenuGroup->addSubMenuItem('SampleList')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_sample', 'grid', 'list'))
                        ->setTitle('Sample - List')
                        ->setPermission('cryosaveSampleView');
        $cryosaveMenuGroup->addSubMenuItem('SampleCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_sample', 'create', 'create'))
                        ->setTitle('Sample - Add New')
                        ->setPermission('cryosaveSampleModify');
        $cryosaveMenuGroup->addSubMenuItem('LaboratoryList')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_laboratory', 'grid', 'list'))
                        ->setTitle('Laboratory - List')
                        ->setPermission('cryosaveUserView');
        $cryosaveMenuGroup->addSubMenuItem('LaboratoryCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_laboratory', 'create', 'create'))
                        ->setTitle('Laboratory - Add New')
                        ->setPermission('cryosaveUserModify');
        $cryosaveMenuGroup->addSubMenuItem('ExternalUserList')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_user', 'grid', 'list'))
                        ->setTitle('External User - List')
                        ->setPermission('cryosaveUserView');
        $cryosaveMenuGroup->addSubMenuItem('ExternalUserCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_user', 'create', 'create'))
                        ->setTitle('External User - Add New')
                        ->setPermission('cryosaveUserModify');
        $cryosaveMenuGroup->addSubMenuItem('StatusList')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_status', 'grid', 'list'))
                        ->setTitle('Status - List')
                        ->setPermission('cryosaveUserView');
        $cryosaveMenuGroup->addSubMenuItem('StatusCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_status', 'create', 'create'))
                        ->setTitle('Status - Add New')
                        ->setPermission('cryosaveUserModify');
        $cryosaveMenuGroup->addSubMenuItem('UploadResult')
                        ->setLink($manager->getUrl('leep_admin', 'cryosave_sample', 'result_attachment', 'create'))
                        ->setTitle('Upload Result')
                        ->setPermission('cryosaveSampleModify');

        // MWA PROCESSOR
        $menu = $menuBuilder->addMenuItem('MWAProcessor')
                    ->setLink('javascript:;')
                    ->setClass('widgets')
                    ->setTitle('MWAProcessor');

        $mwaProcessorMenuGroup = $menu->addMenuGroupItem('MwaProcessorSubMenus');
        $mwaProcessorMenuGroup->addSubMenuItem('SampleList')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_sample', 'grid', 'list'))
                        ->setTitle('Sample - List')
                        ->setPermission('mwaProcessorView');
        $mwaProcessorMenuGroup->addSubMenuItem('SampleCreate')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_sample', 'create', 'create'))
                        ->setTitle('Sample - Add New')
                        ->setPermission('mwaProcessorModify');
        $mwaProcessorMenuGroup->addSubMenuItem('ValidCodeList')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_valid_code', 'grid', 'list'))
                        ->setTitle('Valid Code - List')
                        ->setPermission('mwaProcessorView');
        $mwaProcessorMenuGroup->addSubMenuItem('ValidCodeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_valid_code', 'create', 'create'))
                        ->setTitle('Valid Code - Add New')
                        ->setPermission('mwaProcessorModify');
        $mwaProcessorMenuGroup->addSubMenuItem('StatusList')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_status', 'grid', 'list'))
                        ->setTitle('Status - List')
                        ->setPermission('mwaProcessorView');
        $mwaProcessorMenuGroup->addSubMenuItem('StatusCreate')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_status', 'create', 'create'))
                        ->setTitle('Status - Add New')
                        ->setPermission('mwaProcessorModify');
        $mwaProcessorMenuGroup->addSubMenuItem('InvoiceList')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_invoice', 'grid', 'list'))
                        ->setTitle('Invoice - List')
                        ->setPermission('mwaProcessorView');
        $mwaProcessorMenuGroup->addSubMenuItem('InvoiceCreate')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_invoice', 'grid', 'list'))
                        ->setTitle('Invoice - Add New')
                        ->setPermission('mwaProcessorModify');
        $mwaProcessorMenuGroup->addSubMenuItem('MwaDashboard')
                        ->setLink($manager->getUrl('leep_admin', 'mwa_dashboard', 'mwa_dashboard', 'list'))
                        ->setTitle('MWA Dashboard')
                        ->setPermission('mwaProcessorView');

        // Russia MAW PROCESSOR
        $menu = $menuBuilder->addMenuItem('RussiaMWAProcessor')
                    ->setLink('javascript:;')
                    ->setClass('widgets')
                    ->setTitle('Russia MWAProcessor');

        $russiaMwaProcessorMenuGroup = $menu->addMenuGroupItem('RussiaMwaProcessorSubMenus');
        $russiaMwaProcessorMenuGroup->addSubMenuItem('SampleList')
                        ->setLink($manager->getUrl('leep_admin', 'russia_mwa_sample', 'grid', 'list'))
                        ->setTitle('Sample - List')
                        ->setPermission('russiaMwaProcessorView');
        $russiaMwaProcessorMenuGroup->addSubMenuItem('SampleList')
                        ->setLink($manager->getUrl('leep_admin', 'russia_mwa_sample', 'create', 'create'))
                        ->setTitle('Sample - Add New')
                        ->setPermission('russiaMwaProcessorModify');
        $russiaMwaProcessorMenuGroup->addSubMenuItem('MwaDashboard')
                        ->setLink($manager->getUrl('leep_admin', 'russia_mwa_dashboard', 'mwa_dashboard', 'list'))
                        ->setTitle('MWA Dashboard')
                        ->setPermission('russiaMwaProcessorView');
        $russiaMwaProcessorMenuGroup->addSubMenuItem('ImportFromLytech')
                        ->setLink($manager->getUrl('leep_admin', 'sample_import_job', 'grid', 'list'))
                        ->setTitle('Import From Lytech')
                        ->setPermission('russiaMwaProcessorModify');

        // Billing Cockpit
        $menu = $menuBuilder->addMenuItem('BillingCockpit')
                    ->setLink('javascript:;')
                    ->setClass('widgets')
                    ->setTitle('Billing Cockpit');

        $billingCockpitMenuGroup = $menu->addMenuGroupItem('BillingCockpitSubMenus');
        $billingCockpitMenuGroup->addSubMenuItem('Invoice')
                        ->setLink($manager->getUrl('leep_admin', 'integrated_invoice', 'grid', 'list'))
                        ->setTitle('Invoice')
                        ->setPermission('integratedInvoiceView');
        $billingCockpitMenuGroup->addSubMenuItem('NewInvoiceList')
                        ->setLink($manager->getUrl('leep_admin', 'invoice', 'grid', 'list'))
                        ->setTitle('New Invoice - List')
                        ->setPermission('integratedInvoiceView');
        $billingCockpitMenuGroup->addSubMenuItem('NewInvoiceCreate')
                        ->setLink($manager->getUrl('leep_admin', 'invoice', 'grid', 'list'))
                        ->setTitle('New Invoice - Add New')
                        ->setPermission('integratedInvoiceModify');

        // FINANCE TRACKER
        $menu = $menuBuilder->addMenuItem('FinanceTracker')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Finance Tracker');

        $financeTrackerMenuGroup = $menu->addMenuGroupItem('FinanceTrackerSubMenus');
        $financeTrackerMenuGroup->addSubMenuItem('Summary')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'summary'))
                        ->setTitle('Summary')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('DistributionChannel')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'distributionChannel'))
                        ->setTitle('Distribution Channel')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('Partner')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'partner'))
                        ->setTitle('Partner')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('Amway')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'amway'))
                        ->setTitle('AMWAY')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CostTracker')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'costTracker'))
                        ->setTitle('Cost Tracker')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CashInList')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'cash_in_grid', 'list'))
                        ->setTitle('Cash In - List')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CashInCreate')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'cash_in_create', 'create'))
                        ->setTitle('Cash In - Add New')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CashOutList')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'cash_out_grid', 'list'))
                        ->setTitle('Cash Out - List')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CashOutCreate')
                        ->setLink($manager->getUrl('leep_admin', 'finance_tracker', 'cash_out_create', 'create'))
                        ->setTitle('Cash Out - Add New')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CostTrackerSectorList')
                        ->setLink($manager->getUrl('leep_admin', 'cost_tracker_sector', 'grid', 'list'))
                        ->setTitle('Cost Tracker Sector - List')
                        ->setPermission('financeTrackerView');
        $financeTrackerMenuGroup->addSubMenuItem('CostTrackerSectorCreate')
                        ->setLink($manager->getUrl('leep_admin', 'cost_tracker_sector', 'create', 'create'))
                        ->setTitle('Cost Tracker Sector - Add New')
                        ->setPermission('financeTrackerView');

        // SALES ANALYSIS
        $menu = $menuBuilder->addMenuItem('SalesAnalysis')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Sales Analysis');

        $salesAnalysisMenuGroup = $menu->addMenuGroupItem('SalesAnalysisSubMenus');
        $salesAnalysisMenuGroup->addSubMenuItem('Summary')
                        ->setLink($manager->getUrl('leep_admin', 'sales_analysis', 'summary', 'edit'))
                        ->setTitle('Summary')
                        ->setPermission('salesAnalysisView');

        // STATISTIC
        $menu = $menuBuilder->addMenuItem('Statistics')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Statistics');

        $statisticMenuGroup = $menu->addMenuGroupItem('StatisticsSubMenus');
        $statisticMenuGroup->addSubMenuItem('Summary')
                        ->setLink($manager->getUrl('leep_admin', 'statistic', 'statistic', 'summary'))
                        ->setTitle('Summary')
                        ->setPermission('statisticsView');

        // STRATEGY PLANNING TOOL
        $menu = $menuBuilder->addMenuItem('StrategyPlanningTool')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Strategy Planning');

        $strategyPlanningToolMenuGroup = $menu->addMenuGroupItem('StrategyPlanningToolSubMenus');
        $strategyPlanningToolMenuGroup->addSubMenuItem('CurrentTask')
                        ->setLink($manager->getUrl('leep_admin', 'todo_task', 'feature', 'dashboard'))
                        ->setTitle('Current Tasks')
                        ->setPermission('strategyPlanningToolView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('CpmpletedTask')
                        ->setLink($manager->getUrl('leep_admin', 'todo_task', 'feature', 'dashboardCompleted'))
                        ->setTitle('Completed Tasks')
                        ->setPermission('strategyPlanningToolView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('DeletedTask')
                        ->setLink($manager->getUrl('leep_admin', 'todo_task', 'feature', 'dashboardDeleted'))
                        ->setTitle('Deleted Tasks')
                        ->setPermission('strategyPlanningToolView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('GoalList')
                        ->setLink($manager->getUrl('leep_admin', 'todo_goal', 'grid', 'list'))
                        ->setTitle('Goal - List')
                        ->setPermission('strategyPlanningToolGoalView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('GoalCreate')
                        ->setLink($manager->getUrl('leep_admin', 'todo_goal', 'create', 'create'))
                        ->setTitle('Goal - Add New')
                        ->setPermission('strategyPlanningToolGoalModify');
        $strategyPlanningToolMenuGroup->addSubMenuItem('CategoryList')
                        ->setLink($manager->getUrl('leep_admin', 'todo_category', 'grid', 'list'))
                        ->setTitle('Category - List')
                        ->setPermission('strategyPlanningToolGoalView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('CategoryCreate')
                        ->setLink($manager->getUrl('leep_admin', 'todo_category', 'create', 'create'))
                        ->setTitle('Category - Add New')
                        ->setPermission('strategyPlanningToolGoalModify');
        $strategyPlanningToolMenuGroup->addSubMenuItem('ContinualImprovementList')
                        ->setLink($manager->getUrl('leep_admin', 'todo_continual_improvement', 'grid', 'list'))
                        ->setTitle('Continual Improvement - List')
                        ->setPermission('strategyPlanningToolView');
        $strategyPlanningToolMenuGroup->addSubMenuItem('ContinualImprovementCreate')
                        ->setLink($manager->getUrl('leep_admin', 'todo_continual_improvement', 'create', 'create'))
                        ->setTitle('Continual Improvement - Add New')
                        ->setPermission('strategyPlanningToolView');

        // MEETING
        $menu = $menuBuilder->addMenuItem('Meeting')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Meeting Log');

        $meetingMenuGroup = $menu->addMenuGroupItem('MeetingSubMenus');
        $meetingMenuGroup->addSubMenuItem('MeetingList')
                        ->setLink($manager->getUrl('leep_admin', 'meeting', 'grid', 'list'))
                        ->setTitle('Meeting - List')
                        ->setPermission('meetingView');
        $meetingMenuGroup->addSubMenuItem('MeetingCreate')
                        ->setLink($manager->getUrl('leep_admin', 'meeting', 'create', 'create'))
                        ->setTitle('Meeting - Add New')
                        ->setPermission('meetingModify');

        // LANGUAGE
        $menu = $menuBuilder->addMenuItem('Language')
                    ->setLink('javascript:;')
                    ->setClass('elements')
                    ->setTitle('Language');

        $languageMenuGroup = $menu->addMenuGroupItem('LanguageSubMenus');
        $languageMenuGroup->addSubMenuItem('LanguageList')
                        ->setLink($manager->getUrl('leep_admin', 'language', 'grid', 'list'))
                        ->setTitle('Language - List')
                        ->setPermission('languageView');
        $languageMenuGroup->addSubMenuItem('LanguageCreate')
                        ->setLink($manager->getUrl('leep_admin', 'language', 'create', 'create'))
                        ->setTitle('Language - Add New')
                        ->setPermission('languageModify');

        // TEXT BLOCK
        $menu = $menuBuilder->addMenuItem('TextBlock')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Text Block');

        $textBlockMenuGroup = $menu->addMenuGroupItem('TextBlockSubMenus');
        $textBlockMenuGroup->addSubMenuItem('GroupList')
                        ->setLink($manager->getUrl('leep_admin', 'text_block_group', 'grid', 'list'))
                        ->setTitle('Group - List')
                        ->setPermission('textBlockView');
        $textBlockMenuGroup->addSubMenuItem('GroupCreate')
                        ->setLink($manager->getUrl('leep_admin', 'text_block_group', 'create', 'create'))
                        ->setTitle('Group - Add New')
                        ->setPermission('textBlockModify');
        $textBlockMenuGroup->addSubMenuItem('TextBlockList')
                        ->setLink($manager->getUrl('leep_admin', 'text_block', 'grid', 'list'))
                        ->setTitle('Text Block - List')
                        ->setPermission('textBlockView');
        $textBlockMenuGroup->addSubMenuItem('GroupCreate')
                        ->setLink($manager->getUrl('leep_admin', 'text_block', 'create', 'create'))
                        ->setTitle('Text Block - Add New')
                        ->setPermission('textBlockModify');

        // TRANSLATOR
        $menu = $menuBuilder->addMenuItem('Translator')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Translator');

        $translatorMenuGroup = $menu->addMenuGroupItem('TranslatorSubMenus');
        $translatorMenuGroup->addSubMenuItem('WordCount')
                        ->setLink($manager->getUrl('leep_admin', 'translate_text_block', 'feature', 'dashboard'))
                        ->setTitle('Translation')
                        ->setPermission('translatorView');
        $translatorMenuGroup->addSubMenuItem('Search')
                        ->setLink($manager->getUrl('leep_admin', 'translate_text_block', 'grid', 'list'))
                        ->setTitle('All text blocks')
                        ->setPermission('translatorView');

        // FORMULA TEMPLATE
        $menu = $menuBuilder->addMenuItem('FormulaTemplate')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Formula Template');

        $formulaTemplateMenuGroup = $menu->addMenuGroupItem('FormulaTemplateSubMenus');
        $formulaTemplateMenuGroup->addSubMenuItem('FormulaTemplateList')
                        ->setLink($manager->getUrl('leep_admin', 'formula_template', 'grid', 'list'))
                        ->setTitle('Formula Template - List')
                        ->setPermission('formulaTemplateView');
        $formulaTemplateMenuGroup->addSubMenuItem('FormulaTemplateCreate')
                        ->setLink($manager->getUrl('leep_admin', 'formula_template', 'create', 'create'))
                        ->setTitle('Formula Template - Create')
                        ->setPermission('formulaTemplateModify');

        // FOOD TABLE
        $menu = $menuBuilder->addMenuItem('FoodTable')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Food Table');

        $foodTableMenuGroup = $menu->addMenuGroupItem('FoodTableSubMenus');
        $foodTableMenuGroup->addSubMenuItem('CategoryList')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_category', 'grid', 'list'))
                        ->setTitle('Category - List')
                        ->setPermission('foodTableView');
        $foodTableMenuGroup->addSubMenuItem('CategoryCreate')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_category', 'create', 'create'))
                        ->setTitle('Category - Create')
                        ->setPermission('foodTableModify');
        $foodTableMenuGroup->addSubMenuItem('IngredientList')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_ingredient', 'grid', 'list'))
                        ->setTitle('Ingredient - List')
                        ->setPermission('foodTableView');
        $foodTableMenuGroup->addSubMenuItem('IngredientCreate')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_ingredient', 'create', 'create'))
                        ->setTitle('Ingredient - Create')
                        ->setPermission('foodTableModify');
        $foodTableMenuGroup->addSubMenuItem('FoodItemList')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_food_item', 'grid', 'list'))
                        ->setTitle('Food Item - List')
                        ->setPermission('foodTableView');
        $foodTableMenuGroup->addSubMenuItem('FoodItemCreate')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_food_item', 'create', 'create'))
                        ->setTitle('Food Item - Create')
                        ->setPermission('foodTableModify');
        $foodTableMenuGroup->addSubMenuItem('SmileConversionList')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_smiley_conversion', 'grid', 'list'))
                        ->setTitle('Smile Conversion - List')
                        ->setPermission('foodTableView');
        $foodTableMenuGroup->addSubMenuItem('SmileConversionCreate')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_smiley_conversion', 'create', 'create'))
                        ->setTitle('Smile Conversion - Create')
                        ->setPermission('foodTableModify');
        $foodTableMenuGroup->addSubMenuItem('Parameter')
                        ->setLink($manager->getUrl('leep_admin', 'food_table_parameter', 'edit', 'edit'))
                        ->setTitle('Parameter')
                        ->setPermission('foodTableModify');

        // DRUG
        $menu = $menuBuilder->addMenuItem('Drug')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Drug');

        $drugMenuGroup = $menu->addMenuGroupItem('DrugSubMenus');
        $drugMenuGroup->addSubMenuItem('DrugList')
                        ->setLink($manager->getUrl('leep_admin', 'drug', 'grid', 'list'))
                        ->setTitle('Drug - List')
                        ->setPermission('drugView');
        $drugMenuGroup->addSubMenuItem('DrugCreate')
                        ->setLink($manager->getUrl('leep_admin', 'drug', 'create', 'create'))
                        ->setTitle('Drug - Add New')
                        ->setPermission('drugModify');

        // RECIPE
        $menu = $menuBuilder->addMenuItem('Recipe')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Recipe');

        $recipeMenuGroup = $menu->addMenuGroupItem('RecipeSubMenus');
        $recipeMenuGroup->addSubMenuItem('RecipeList')
                        ->setLink($manager->getUrl('leep_admin', 'recipe', 'grid', 'list'))
                        ->setTitle('Recipe - List')
                        ->setPermission('recipeView');
        $recipeMenuGroup->addSubMenuItem('RecipeCreate')
                        ->setLink($manager->getUrl('leep_admin', 'recipe', 'create', 'create'))
                        ->setTitle('Recipe - Create')
                        ->setPermission('recipeModify');
        $recipeMenuGroup->addSubMenuItem('BookLayoutList')
                        ->setLink($manager->getUrl('leep_admin', 'recipe_book_layout', 'grid', 'list'))
                        ->setTitle('Book Layout - List')
                        ->setPermission('recipeView');
        $recipeMenuGroup->addSubMenuItem('BookLayoutCreate')
                        ->setLink($manager->getUrl('leep_admin', 'recipe_book_layout', 'create', 'create'))
                        ->setTitle('Book Layout - Add New')
                        ->setPermission('recipeModify');

        // REPORT
        $menu = $menuBuilder->addMenuItem('Report')
                    ->setLink('javascript:;')
                    ->setClass('charts')
                    ->setTitle('Report');

        $reportMenuGroup = $menu->addMenuGroupItem('ReportSubMenus');
        $reportMenuGroup->addSubMenuItem('ReportList')
                        ->setLink($manager->getUrl('leep_admin', 'report', 'grid', 'list'))
                        ->setTitle('Report - List')
                        ->setPermission('reportView');
        $reportMenuGroup->addSubMenuItem('ReportCreate')
                        ->setLink($manager->getUrl('leep_admin', 'report', 'create', 'create'))
                        ->setTitle('Report - Create')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('ReportSearch')
                        ->setLink($manager->getUrl('leep_admin', 'report', 'search_section', 'list'))
                        ->setTitle('Report - Search Section')
                        ->setPermission('reportView');
        $reportMenuGroup->addSubMenuItem('Resource')
                        ->setLink($manager->getUrl('leep_admin', 'report', 'attachment', 'create'))
                        ->setTitle('Resources')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('Font')
                        ->setLink($manager->getUrl('leep_admin', 'application_font', 'grid', 'list'))
                        ->setTitle('Fonts')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('LanguageFont')
                        ->setLink($manager->getUrl('leep_admin', 'report_language_font', 'grid', 'list'))
                        ->setTitle('Language Fonts')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('Worker')
                        ->setLink($manager->getUrl('leep_admin', 'worker', 'grid', 'list'))
                        ->setTitle('Worker')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('ReportQueue')
                        ->setLink($manager->getUrl('leep_admin', 'report_queue', 'grid', 'list'))
                        ->setTitle('Report Queue')
                        ->setPermission('reportView');
        $reportMenuGroup->addSubMenuItem('Localization')
                        ->setLink($manager->getUrl('leep_admin', 'report_localization', 'grid', 'list'))
                        ->setTitle('Localization')
                        ->setPermission('reportModify');
        $reportMenuGroup->addSubMenuItem('GenerateReport')
                        ->setLink($manager->getUrl('leep_admin', 'report', 'generate', 'create'))
                        ->setTitle('Generate reports')
                        ->setPermission('reportView');

        // WIDGET
        $menu = $menuBuilder->addMenuItem('Widgets')
                    ->setLink('javascript:;')
                    ->setClass('widgets')
                    ->setTitle('Dashboard Widget');

        $widgetMenuGroup = $menu->addMenuGroupItem('WidgetSubMenus');
        $widgetMenuGroup->addSubMenuItem('WidgetList')
                        ->setLink($manager->getUrl('leep_admin', 'widget_statistic', 'grid', 'list'))
                        ->setTitle('Widget - List')
                        ->setPermission('dashboardWidgetView');
        $widgetMenuGroup->addSubMenuItem('WidgetCreate')
                        ->setLink($manager->getUrl('leep_admin', 'widget_statistic', 'create', 'create'))
                        ->setTitle('Widget - Create')
                        ->setPermission('dashboardWidgetModify');

        // PARAMETER
        $menu = $menuBuilder->addMenuItem('Parameter')
                    ->setLink($manager->getUrl('leep_admin', 'parameter', 'edit', 'edit'))
                    ->setClass('widgets')
                    ->setTitle('Parameter')
                    ->setPermission('parameterModify');

        // // CONTRACT TRACKER
        // $menu = $menuBuilder->addMenuItem('Widgets')
        //            ->setLink('javascript:;')
        //            ->setClass('widgets')
        //            ->setTitle('Dashboard Widget123');

        // LOG
        $menu = $menuBuilder->addMenuItem('Log')
                    ->setLink($manager->getUrl('leep_admin', 'log', 'grid', 'list'))
                    ->setClass('users')
                    ->setTitle('Log')
                    ->setPermission('logView');

        // MAIL SENDER
        $menu = $menuBuilder->addMenuItem('MailSender')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Mail Sender');

        $mailSenderMenuGroup = $menu->addMenuGroupItem('MailSenderSubMenus');
        $mailSenderMenuGroup->addSubMenuItem('EmailCreate')
                        ->setLink($manager->getUrl('leep_admin', 'mail_sender', 'email', 'create'))
                        ->setTitle('Create Email')
                        ->setPermission('mailSenderView');
        $mailSenderMenuGroup->addSubMenuItem('EmailInPool')
                        ->setLink($manager->getUrl('leep_admin', 'mail_sender', 'grid', 'list'))
                        ->setTitle('Email In Pool')
                        ->setPermission('mailSenderView');
        $mailSenderMenuGroup->addSubMenuItem('EmailSendingStatistic')
                        ->setLink($manager->getUrl('leep_admin', 'mail_sender', 'email_sending_stats', 'list'))
                        ->setTitle('Email Sending Statistics')
                        ->setPermission('mailSenderView');
        $mailSenderMenuGroup->addSubMenuItem('EmailInPool')
                        ->setLink($manager->getUrl('leep_admin', 'mail_sender', 'edit_blacklist', 'edit'))
                        ->setTitle('View Blacklist')
                        ->setPermission('mailSenderView');

        // MAIL SENDER
        $menu = $menuBuilder->addMenuItem('EmailSystem')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Email System');

        $emailSystemGroup = $menu->addMenuGroupItem('EmailSystemSubMenus');
        $emailSystemGroup->addSubMenuItem('EmailMethodList')
                        ->setLink($manager->getUrl('leep_admin', 'email_method', 'grid', 'list'))
                        ->setTitle('Email Method - List')
                        ->setPermission('emailMethodView');
        $emailSystemGroup->addSubMenuItem('EmailMethodCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_method', 'create', 'create'))
                        ->setTitle('Email Method - Create')
                        ->setPermission('emailMethodModify');
        $emailSystemGroup->addSubMenuItem('EmailOutgoingList')
                        ->setLink($manager->getUrl('leep_admin', 'email_outgoing', 'grid', 'list'))
                        ->setTitle('Email Outgoing - List')
                        ->setPermission('emailOutgoingView');
        $emailSystemGroup->addSubMenuItem('EmailOutgoingCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_outgoing', 'create', 'create'))
                        ->setTitle('Email Outgoing - Create')
                        ->setPermission('emailOutgoingModify');
        $emailSystemGroup->addSubMenuItem('EmailBlacklistList')
                        ->setLink($manager->getUrl('leep_admin', 'email_blacklist', 'grid', 'list'))
                        ->setTitle('Email Blacklist - List')
                        ->setPermission('emailBlacklistView');
        $emailSystemGroup->addSubMenuItem('EmailBlacklistCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_blacklist', 'create', 'create'))
                        ->setTitle('Email Blacklist - Create')
                        ->setPermission('emailBlacklistModify');
        $emailSystemGroup->addSubMenuItem('EmailMassSenderList')
                        ->setLink($manager->getUrl('leep_admin', 'email_mass_sender', 'grid', 'list'))
                        ->setTitle('Email Mass Sender - List')
                        ->setPermission('emailMassSenderView');
        $emailSystemGroup->addSubMenuItem('EmailMassSenderCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_mass_sender', 'create', 'create'))
                        ->setTitle('Email Mass Sender - Create')
                        ->setPermission('emailMassSenderModify');
        $emailSystemGroup->addSubMenuItem('EmailWebloginSetting')
                        ->setLink($manager->getUrl('leep_admin', 'email_weblogin', 'edit', 'edit'))
                        ->setTitle('Email Weblogin - Setting')
                        ->setPermission('emailWebloginModify');
        $emailSystemGroup->addSubMenuItem('EmailWebLoginBlacklistList')
                        ->setLink($manager->getUrl('leep_admin', 'email_weblogin_blacklist', 'grid', 'list'))
                        ->setTitle('Email WebLogin Blacklist - List')
                        ->setPermission('emailWebLoginBlacklistView');
        $emailSystemGroup->addSubMenuItem('EmailWebLoginBlacklistCreate')
                        ->setLink($manager->getUrl('leep_admin', 'email_weblogin_blacklist', 'create', 'create'))
                        ->setTitle('Email WebLogin Blacklist - Create')
                        ->setPermission('emailWebLoginBlacklistModify');

        // NOTIFICATION
        $menu = $menuBuilder->addMenuItem('Notification')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Notification');

        $notificationMenuGroup = $menu->addMenuGroupItem('NotificationSubMenus');
        $notificationMenuGroup->addSubMenuItem('NotificationList')
                        ->setLink($manager->getUrl('leep_admin', 'notification', 'grid', 'list'))
                        ->setTitle('Notification - List')
                        ->setPermission('notificationView');
        $notificationMenuGroup->addSubMenuItem('NotificationSubcribe')
                        ->setLink($manager->getUrl('leep_admin', 'notification_subscription', 'create', 'create'))
                        ->setTitle('Notification - Subscribe')
                        ->setPermission('notificationView');

        // IP LOG
        $menu = $menuBuilder->addMenuItem('IpLog')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('IP Log');

        $ipLogMenuGroup = $menu->addMenuGroupItem('IpLogSubMenus');
        $ipLogMenuGroup->addSubMenuItem('IpWhiteListList')
                        ->setLink($manager->getUrl('leep_admin', 'ip_whitelist', 'grid', 'list'))
                        ->setTitle('IP WhiteList - List')
                        ->setPermission('ipLogView');
        $ipLogMenuGroup->addSubMenuItem('IpWhiteListCreate')
                        ->setLink($manager->getUrl('leep_admin', 'ip_whitelist', 'create', 'create'))
                        ->setTitle('IP WhiteList - Add New')
                        ->setPermission('ipLogModify');
        $ipLogMenuGroup->addSubMenuItem('IpAccessLog')
                        ->setLink($manager->getUrl('leep_admin', 'ip_log', 'grid', 'list'))
                        ->setTitle('IP Access Log')
                        ->setPermission('ipLogView');

        // GLACIER
        $menu = $menuBuilder->addMenuItem('Glacier')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Glacier');

        $glacierMenuGroup = $menu->addMenuGroupItem('GlacierSubMenus');
        $glacierMenuGroup->addSubMenuItem('GlacierList')
                        ->setLink($manager->getUrl('leep_admin', 'glacier_file', 'grid', 'list'))
                        ->setTitle('Glacier - List')
                        ->setPermission('glacierView');
        $glacierMenuGroup->addSubMenuItem('GlacierCreate')
                        ->setLink($manager->getUrl('leep_admin', 'glacier_file', 'create', 'create'))
                        ->setTitle('Glacier - Add New')
                        ->setPermission('glacierModify');

        // OFFLINE JOB
        $menu = $menuBuilder->addMenuItem('OfflineJob')
                    ->setLink($manager->getUrl('leep_admin', 'offline_job', 'grid', 'list'))
                    ->setClass('users')
                    ->setTitle('Offline Job')
                    ->setPermission('offlineJobView');

        // WEB USER
        $menu = $menuBuilder->addMenuItem('WebUser')
                    ->setLink('javascript:;')
                    ->setClass('users')
                    ->setTitle('Web User');

        $webUserMenuGroup = $menu->addMenuGroupItem('WebUserSubMenus');
        $webUserMenuGroup->addSubMenuItem('WebUserList')
                        ->setLink($manager->getUrl('leep_admin', 'web_user', 'grid', 'list'))
                        ->setTitle('Web User - List')
                        ->setPermission('webUserView');
        $webUserMenuGroup->addSubMenuItem('WebUserCreate')
                        ->setLink($manager->getUrl('leep_admin', 'web_user', 'create', 'create'))
                        ->setTitle('Web User - Add New')
                        ->setPermission('webUserModify');
        $webUserMenuGroup->addSubMenuItem('TranslatorList')
                        ->setLink($manager->getUrl('leep_admin', 'translator_user', 'grid', 'list'))
                        ->setTitle('Translator - List')
                        ->setPermission('translatorUserView');
        $webUserMenuGroup->addSubMenuItem('TranslatorCreate')
                        ->setLink($manager->getUrl('leep_admin', 'translator_user', 'create', 'create'))
                        ->setTitle('Translator - Add New')
                        ->setPermission('translatorUserModify');


    }
}
