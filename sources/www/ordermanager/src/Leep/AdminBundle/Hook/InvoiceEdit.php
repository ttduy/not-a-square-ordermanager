<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class InvoiceEdit extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');
        $id = $controller->getRequest()->query->get('id', 0);

        $customer = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        if ($customer && $customer->getInvoiceNumber() != '') {
            $data['viewInvoicePdfUrl'] =  $manager->getUrl('leep_admin', 'customer_invoice', 'customer_form_pdf', 'pdf', array('id' => $id));
            $data['emailInvoiceUrl'] = $manager->getUrl('leep_admin', 'email', 'email', 'create', array('id' => $id, 'mode' => 'customer'));
        }
    }
}
