<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class NotificationMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'list'      => array(
                                        'title' => 'List', 
                                        'link' => $mgr->getUrl('leep_admin', 'notification', 'grid', 'list', array('id' => $id))
                                    ),
                'subscribe' => array(
                                        'title' => 'Subscribe', 
                                        'link' => $mgr->getUrl('leep_admin', 'notification_subscription', 'create', 'create', array('id' => $id))
                                    )
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }

        $keyView = $helperSecurity->getKey('notification', 'View');
        if (!$helperSecurity->hasPermission($keyView)) {
            unset($data['mainTabs']['content']['list']);
        }

        $keyView = $helperSecurity->getKey('notification_subscription', 'Modify');
        if (!$helperSecurity->hasPermission($keyView)) {
            unset($data['mainTabs']['content']['subscribe']);
        }
    }
}
