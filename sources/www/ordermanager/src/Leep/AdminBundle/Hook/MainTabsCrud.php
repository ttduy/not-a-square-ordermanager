<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class MainTabsCrud extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');

        $initContent = [];
        if (!isset($options['empty-init-content'])) {
            $initContent = [
                'list' => ['title' => $options['list'], 'link' => $this->getModule()->getUrl('grid', 'list')],
                'create'  => ['title' => $options['create'],  'link' => $this->getModule()->getUrl('create', 'create')]
            ];
        }

        $data['mainTabs'] = array(
            'content' => $initContent,
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }

        if (isset($options['disable'])) {
            foreach ($options['disable'] as $disableTab) {
                if (isset($data['mainTabs']['content'][$disableTab])) {
                    unset($data['mainTabs']['content'][$disableTab]);
                }
            }
        }

        $keyView = $helperSecurity->getKey($this->getModule()->getName(), 'View');
        if (!$helperSecurity->hasPermission($keyView)) {
            unset($data['mainTabs']['content']['list']);
        }

        $keyModify = $helperSecurity->getKey($this->getModule()->getName(), 'Modify');
        if (!$helperSecurity->hasPermission($keyModify)) {
            unset($data['mainTabs']['content']['create']);
        }

        if (isset($options['disabled'])) {
            foreach ($options['disabled'] as $k) {
                unset($data['mainTabs']['content'][$k]);
            }
        }
    }
}
