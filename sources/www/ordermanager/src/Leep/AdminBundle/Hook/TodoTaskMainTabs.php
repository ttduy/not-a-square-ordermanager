<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
       
class TodoTaskMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');

        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'current'     => array('title' => 'Current tasks', 'link' => $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'dashboard')),
                'completed'   => array('title' => 'Completed tasks', 'link' => $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'dashboardCompleted')),
                'deleted'   => array('title' => 'Deleted tasks', 'link' => $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'dashboardDeleted')),
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : 'current'
        );
    }
}
