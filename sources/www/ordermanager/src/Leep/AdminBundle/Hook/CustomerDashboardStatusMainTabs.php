<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class CustomerDashboardStatusMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'status_list'                       => array('title' => 'Status List', 'link' => $mgr->getUrl('leep_admin', 'status', 'grid', 'list', array('id' => $id))),
                'customer_dashboard_status'         => array('title' => 'Customer Dashboard Status', 'link' => $mgr->getUrl('leep_admin', 'customer_dashboard_status', 'edit', 'edit')),
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
    }
}
