<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class MailSenderMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'create_email'              => array('title' => 'Create Email', 'link' => $mgr->getUrl('leep_admin', 'mail_sender', 'email', 'create')),
                'email_in_pool'             => array('title' => 'Email in Pool', 'link' => $mgr->getUrl('leep_admin', 'mail_sender', 'grid', 'list')),
                'email_sending_statistic'   => array('title' => 'Email Sending Stats', 'link' => $mgr->getUrl('leep_admin', 'mail_sender', 'email_sending_stats', 'list')),
                'edit_blacklist'          => array('title' => 'View Blacklist', 'link' => $mgr->getUrl('leep_admin', 'mail_sender', 'edit_blacklist', 'edit'))
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
    }
}
