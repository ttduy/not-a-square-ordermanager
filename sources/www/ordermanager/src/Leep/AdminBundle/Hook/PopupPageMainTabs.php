<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class PopupPageMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');

        $idParent = $controller->get('request')->get('idParent', 0);
        $mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
                'list'         => array('title' => $options['list'], 'link' => $this->getModule()->getUrl('grid', 'list', array('idParent' => $idParent))),
                'create'       => array('title' => $options['create'], 'link' => $this->getModule()->getUrl('create', 'create', array('idParent' => $idParent))),
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }
    }
}
