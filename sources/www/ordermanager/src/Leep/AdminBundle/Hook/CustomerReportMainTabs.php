<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class CustomerReportMainTabs extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');
        $em = $controller->get('doctrine')->getEntityManager();
        $data['mainTabs'] = [
            'content' => [
                'report' => ['title' => 'Reports', 'link' => $mgr->getUrl('leep_admin', 'customer_report', 'grid', 'list', ['id' => $id])],
                'generate_report' => ['title' => 'Generate report', 'link' => $mgr->getUrl('leep_admin', 'customer_report', 'create', 'create', ['id' => $id])],
                'shipment_list' => ['title' => 'Shipments', 'link' => $mgr->getUrl('leep_admin', 'bod_shipment', 'grid', 'list', ['id' => $id])],
                'shipment_create' => ['title' => 'Add new shipment', 'link' => $mgr->getUrl('leep_admin', 'bod_shipment', 'create', 'create', ['id' => $id])],
                'shipment_email' => ['title' => 'Email shipment', 'link' => $mgr->getUrl('leep_admin', 'bod_shipment', 'email', 'create', ['id' => $id])],
                'shipment_ftp' => ['title' => 'FTP shipment', 'link' => $mgr->getUrl('leep_admin', 'customer_report', 'ftp_shipment', 'create', ['id' => $id])],
                'change_status' => ['title' => 'Change status', 'link' => $mgr->getUrl('leep_admin', 'customer_report', 'change_status', 'create', ['id' => $id])],
                'weblogin_shipment' => ['title' => 'Weblogin shipment', 'link' => $mgr->getUrl('leep_admin', 'customer', 'weblogin_shipment', 'create', ['id' => $id])],
            ],

            'selected' => isset($options['selected']) ? $options['selected'] : ''
        ];

        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }

        $data['idCustomer'] = $id;

        // Load customer data
        $doctrine = $controller->get('doctrine');
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        if ($customer) {
            // Attachments
            $attachments = array();
            $folder = $controller->get('service_container')->getParameter('attachment_dir').'/customers/'.$customer->getAttachmentKey();
            $files = @scandir($folder);
            if (!empty($files)) {
                foreach ($files as $file) {
                    if ($file != '.' && $file != '..' && $file != '.tmb') {
                        $attachments[] = array(
                            'link' => $controller->get('service_container')->getParameter('attachment_url').'/customers/'.$customer->getAttachmentKey().'/'.$file,
                            'name' => $file
                        );
                    }
                }
            }

            // Data
            $reportDeliveryOptions = array();
            if ($customer->getIsReportDeliveryDownloadAccess()) {
                $reportDeliveryOptions[] = 'Download access';
            }
            if ($customer->getIsReportDeliveryFtpServer()) {
                $reportDeliveryOptions[] = 'FTP Server';
            }
            if ($customer->getIsReportDeliveryPrintedBooklet()) {
                $reportDeliveryOptions[] = 'Printed booklet';
            }
            if ($customer->getIsReportDeliveryDontChargeBooklet()) {
                $reportDeliveryOptions[] = 'Don\'t charge booklet';
            }

            $formatter = $controller->get('leep_admin.helper.formatter');
            $data['customerInfo'] = array(
                'orderNumber' => $customer->getOrderNumber(),
                'name'        => $customer->getFirstName().' '.$customer->getSurName(),
                'distributionChannel' => $formatter->format($customer->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List'),
                'distributionChannelSupplementInfo' => '',
                'info'        => $customer->getOrderInfoText(),
                'reportDelivery'      => $formatter->format($customer->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery'),
                'attachments'  => $attachments,
                'showAllImagesLink' => $mgr->getUrl('leep_admin', 'customer', 'customer', 'showAllImages', array(
                    'key' => 'customers/'.$customer->getAttachmentKey()
                )),
                'reportDeliveryOptions' => $reportDeliveryOptions
            );
        }

        // Check if need warning
        $warningMessage = '';
        if ($customer) {
            $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
            if ($dc) {
                // set Distribution Channel Info
                $data['customerInfo']['distributionChannelSupplementInfo'] = $dc->getSupplementInfo();

                // set warning message
                if ($dc->getIsStopShipping()) {
                    $warningMessage = 'Distribution channel '.$dc->getDistributionChannel().' has been marked as "Stop shipping"';
                }
                else {
                    $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
                    if ($partner && $partner->getIsStopShipping()) {
                        $warningMessage = 'Partner '.$partner->getPartner().' has been marked as "Stop shipping"';
                    }
                }
            }
        }
        $data['warningMessage'] = $warningMessage;

        // Check if need fixed report

        $fixedReportMessage = '';
        $idReportGoesTo = 0;
        if ($customer) {
            $idReportGoesTo = $customer->getReportGoesToId();
            $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
            if ($dc) {
                if ($dc->getIsFixedReportGoesTo()) {
                    $reportDeliveryOptions = array();
                    if ($dc->getIsReportDeliveryDownloadAccess()) {
                        $reportDeliveryOptions[] = 'Download access';
                    }
                    if ($dc->getIsReportDeliveryFtpServer()) {
                        $reportDeliveryOptions[] = 'FTP Server';
                    }
                    if ($dc->getIsReportDeliveryPrintedBooklet()) {
                        $reportDeliveryOptions[] = 'Printed booklet';
                    }
                    if ($dc->getIsReportDeliveryDontChargeBooklet()) {
                        $reportDeliveryOptions[] = 'Don\'t charge booklet';
                    }

                    $fixedReportMessage = sprintf("ATTENTION! The reports of '%s' should always be sent to '%s' via '%s', report delivery options = '%s'",
                        $dc->getDistributionChannel(),
                        $formatter->format($dc->getReportGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportGoesTo'),
                        $formatter->format($dc->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery'),
                        implode(", ", $reportDeliveryOptions)
                    );
                    $idReportGoesTo = $dc->getReportGoesToId();
                }
            }
        }
        $data['fixedReportMessage'] = $fixedReportMessage;

        if ($idReportGoesTo == Business\DistributionChannel\Constant::REPORT_GOES_TO_PARTNER) {
            $data['reportToPartner'] = 1;
        }
        else if ($idReportGoesTo == Business\DistributionChannel\Constant::REPORT_GOES_TO_CUSTOMER) {
            $data['reportToCustomer'] = 1;
        }
        else if ($idReportGoesTo == Business\DistributionChannel\Constant::REPORT_GOES_TO_CHANNEL) {
            $data['reportToDC'] = 1;
        }

        // Load products
        $categories = array();
        $categoriesId = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBycustomerid($id);
        foreach ($result as $r) {
            $categories[] = $formatter->format($r->getCategoryId(), 'mapping', 'LeepAdmin_Category_List');
            $categoriesId[] = $r->getCategoryId();
        }
        $data['orderCategories'] = $categories;

        // CatProdLink
        $categoryProducts = array();
        $categoriesId[] = 0;
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CatProdLink', 'p')
            ->andWhere('p.categoryid in (:categoriesId)')
            ->setParameter('categoriesId', $categoriesId);
        $rows = $query->getQuery()->getResult();
        foreach ($rows as $r) {
            $categoryProducts[] = $r->getProductId();
        }


        $products = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($id);
        foreach ($result as $r) {
            $products[] = array(
                'isCategoryProduct'  => in_array($r->getProductId(), $categoryProducts),
                'name'  => $formatter->format($r->getProductId(), 'mapping', 'LeepAdmin_Product_List')
            );
        }
        $data['orderProducts'] = $products;

        // Load BOD Shipment Tracking Number
        if ($customer) {
            $rows = $em->getRepository('AppDatabaseMainBundle:BodShipmentTracking')->findBy(
                array(
                    'customerNumber' => $customer->getOrderNumber()
                ),
                array(
                    'id'          => 'DESC'
                )
            );
            $trackingNumbers = array();
            foreach ($rows as $r) {
                $trackingNumbers[] = array(
                    'timestamp'         => $formatter->format($r->getTimestamp(), 'datetime'),
                    'orderNumber'       => $r->getOrderNumber(),
                    'customerNumber'    => $r->getCustomerNumber(),
                    'shipmentDatetime'  => $r->getShipmentDatetime(),
                    'trackingNumber'    => $r->getTrackingNo(),
                    'trackingLink'      => $r->getTrackingLink()
                );
            }
            $data['trackingNumbers'] = $trackingNumbers;

            $data['hasTrackingInfo'] = count($trackingNumbers) > 0;
        }
    }
}
