<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class FeedbackResponse extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        $feedbackQuestion = $em->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findOneById($id);
        $data['feedbackQuestion'] = $feedbackQuestion;

        if ($feedbackQuestion->getIdType() == Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_CHOICE ||
            $feedbackQuestion->getIdType() == Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE) {
            $summary = json_decode($feedbackQuestion->getSummary(), true);
            $choices = Business\FeedbackQuestion\Util::parseChoices($feedbackQuestion->getData());

            $numResponses = $feedbackQuestion->getNumResponses();
            $stats = array();
            foreach ($choices as $id => $choice) {
                $total = isset($summary[$id]) ? $summary[$id] : 0;
                $stats[] = array(
                    'name'    => $choice,
                    'total'   => $total,
                    'percent' => $numResponses == 0 ? 0 : number_format($total * 100 / $numResponses, 2)
                );
            }

            $data['stats'] = $stats;
        }
    }
}
