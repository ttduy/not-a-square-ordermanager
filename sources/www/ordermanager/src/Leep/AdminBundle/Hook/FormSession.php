<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class FormSession extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['mainTabs'] = array(
            'content' => array(
                'list' => array('title' => 'Form session', 'link' => '#')
            ),
            'selected' => 'list'
        );

    }
}
