<?php
namespace Leep\AdminBundle\Hook;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class Report extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id = $controller->getRequest()->get('id', 0);
        $manager = $controller->get('easy_module.manager');        
        $data['viewReportPdfUrl'] = $manager->getUrl('leep_admin', 'report', 'report', 'viewPdf', array('id' => $id));
        $data['requestId'] = $id;

        $mapping = $controller->get('easy_mapping');
        $data['textToolMapping'] = $mapping->getMapping('LeepAdmin_TextTool_CtrlMapping');

        $data['downloadFullReport'] = $manager->getUrl('leep_admin', 'report', 'report', 'downloadFullReport');
        $data['addToReportQueue'] = $manager->getUrl('leep_admin', 'report', 'report', 'addToReportQueue');

        // Report sections
        $reportSection = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportSection')->findOneById($id);
        if ($reportSection) {
            $data['idReport'] = $reportSection->getIdReport();            
        }

        $helperSecurity = $controller->get('leep_admin.helper.security');
        $data['hasDeletePermission'] = $helperSecurity->hasPermission('reportDelete');
    }
}
