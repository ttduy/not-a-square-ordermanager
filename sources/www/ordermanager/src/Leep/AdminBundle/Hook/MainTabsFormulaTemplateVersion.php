<?php
namespace Leep\AdminBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class MainTabsFormulaTemplateVersion extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');

        $pathInfo = $controller->get('request')->getPathInfo();
        
        // Check common grid
        if (strpos($pathInfo, '/grid/list') != 0 ||
            strpos($pathInfo, '/grid/ajaxSource') != 0) {
            $idFormulaTemplate = $controller->get('request')->get('idFormulaTemplate', 0);
            $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($idFormulaTemplate);
        }
        else if (strpos($pathInfo, '/edit/edit') != 0) {                
            $idFormulaTemplateVersion = $controller->get('request')->get('id', 0);
            $formulaTemplateVersion = $em->getRepository('AppDatabaseMainBundle:FormulaTemplateVersion')->findOneById($idFormulaTemplateVersion);
            $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($formulaTemplateVersion->getIdFormulaTemplate());
        }
        else {
            throw new \Exception("ERROR!");
        }

        $data['mainTabs'] = array(
            'content' => array(
                'root_list'   => array('title' => 'Formula templates', 'link' => $mgr->getUrl('leep_admin', 'formula_template', 'grid', 'list')),
                'list'        => array('title' => 'View versions', 'link' => $mgr->getUrl('leep_admin', 'formula_template_version', 'grid', 'list', array('idFormulaTemplate' => $formulaTemplate->getId())))                  
            ),
            'selected' => isset($options['selected']) ? $options['selected'] : ''
        );
        if (isset($options['extra'])) {
            foreach ($options['extra'] as $k => $v) {
                $data['mainTabs']['content'][$k] = $v;
            }
        }

        $data['formulaName'] = $formulaTemplate->getName();
        $data['formulaDiffLink'] = $mgr->getUrl('leep_admin', 'formula_template', 'feature', 'showDiffTool', array('idFormulaTemplate' => $formulaTemplate->getId()));
    }
}
