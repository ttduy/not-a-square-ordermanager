<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SampleImportJob extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function dateCompare($date1, $date2) {
        if (($date1 != null && $date2 == null) ||
            ($date1 == null && $date2 != null)){
            return FALSE;
        }
        if ($date1 == null && $date2 == null) {
            return TRUE;
        }

        if (strcmp($date1->format('Y-m-d'), $date2->format('Y-m-d')) == 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function handleImportFromLytech($controller, &$data, $options = array()) {
        set_time_limit(120);
        $form = $controller->get('leep_admin.sample_import_job.business.import_lytech_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Import from lytech',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Proceed',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'importFromLytech')
        );

        $parsedSamples = $form->parsedSamples;
        $errorSamples = $form->errorSamples;

        // Load current samples for checking
        $currentSamples = array();
        $sampleIdArr = array('');
        foreach ($parsedSamples as $sample) {
            $sampleIdArr[] = $sample['sampleId'];
        }

        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('p.idGroup = :russiaGroup')
            ->andWhere('p.sampleId in (:sampleIdArr)')
            ->setParameter('russiaGroup', Business\MwaSample\Constants::GROUP_RUSSIA)
            ->setParameter('sampleIdArr', $sampleIdArr);
        $results = $query->getQuery()->getResult();
        foreach ($results as $sample) {
            $currentSamples[$sample->getSampleId()] = $sample;
        }

        // Load current import job for checking
        $currentImportSamples = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:SampleImportJob')->findAll();
        foreach ($results as $r) {
            $currentImportSamples[$r->getSampleId()] = 1;
        }

        // Check if "new" or "changed"
        $newSamples = array();
        $changedSamples = array();
        foreach ($parsedSamples as $sample) {
            $sampleId = $sample['sampleId'];
            if (isset($currentImportSamples[$sampleId])) {
                $sample['error'] = 'Already in import queue';
                $errorSamples[] = $sample;
                continue;
            }
            if (isset($currentSamples[$sampleId])) {
                // Compare
                $registeredDate = $sample['registeredDate'];
                $finishedDate = $sample['finishedDate'];
                $gew1 = $sample['gew1'];
                $gew2 = $sample['gew2'];
                $gew3 = $sample['gew3'];
                $gew4 = $sample['gew4'];
                $gew5 = $sample['gew5'];

                $curSample = $currentSamples[$sampleId];
                if ($this->dateCompare($curSample->getRegisteredDate(), $registeredDate) &&
                    $this->dateCompare($curSample->getFinishedDate(), $finishedDate) &&
                    $curSample->getGew1() == $gew1 &&
                    $curSample->getGew2() == $gew2 &&
                    $curSample->getGew3() == $gew3 &&
                    $curSample->getGew4() == $gew4 &&
                    $curSample->getGew5() == $gew5) {
                    // No change, do nothing
                }
                else {
                    $sample['existing'] = $curSample;
                    $changedSamples[] = $sample;
                }
            }
            else {
                $newSamples[] = $sample;
            }
        }

        // Do load
        $model = $form->getForm()->getData();
        if ($model->idMode == $form::MODE_CHECK_THEN_IMPORT) {
            foreach ($newSamples as $sample) {
                $job = new Entity\SampleImportJob();
                $job->setIdTask(Business\SampleImportJob\Constants::TASK_IMPORT_NEW_SAMPLE);
                $job->setSampleId($sample['sampleId']);
                $job->setData(json_encode($sample));
                $em->persist($job);
            }

            foreach ($changedSamples as $sample) {
                $job = new Entity\SampleImportJob();
                $job->setIdTask(Business\SampleImportJob\Constants::TASK_UPDATE_EXISTING_SAMPLE);
                $job->setSampleId($sample['sampleId']);
                $job->setData(json_encode($sample));
                $em->persist($job);
            }

            $em->flush();

            // save log in here
            $userManager = $controller->get('leep_admin.web_user.business.user_manager');
            $userId = $userManager->getUser()->getId();

            $currentLog = new Entity\ImportLytechLog();
            $currentLog->setUserId($userId);
            $currentLog->setImportTime(new \DateTime('now'));
            $startDate = $form->startDate;
            $currentLog->setStartDate($startDate);
            $endDate = $form->endDate;
            $currentLog->setEndDate($endDate);
            $currentLog->setContent($form->content);
            $currentLog->setErrorMessage('');
            if (empty($parsedSamples) && empty($errorSamples)) {
                $currentLog->setErrorMessage("Error! No sample imported.");
            }
            else{
                $newSamples_json = json_encode($newSamples);
                $currentLog->setNewSamples($newSamples_json);
                $changedSamples_json = json_encode($changedSamples);
                $currentLog->setChangedSamples($changedSamples_json);
                $errorSamples_json = json_encode($errorSamples);
                $currentLog->setErrorSamples($errorSamples_json);
            }

            $em->persist($currentLog);
            $em->flush();
        }

        // View
        $data['newSamples'] = $newSamples;
        $data['changedSamples'] = $changedSamples;
        $data['errorSamples'] = $errorSamples;
    }

    public function handleViewLog($controller, &$data, $options = array())
    {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $em = $controller->get('doctrine')->getEntityManager();
        $lytechImpotedLog = $em->getRepository("AppDatabaseMainBundle:ImportLytechLog")->findAll();
        $logs = [];
        foreach ($lytechImpotedLog as $log) {
            $currentUserName = $formatter->format($log->getUserId(), 'mapping', 'LeepAdmin_WebUser_List');
            $currentLog = ['id' => $log->getId(),
                            'name' => $currentUserName,
                            'importTime' => $log->getImportTime()->format('g:ia \o\n l jS F Y'),
                            'startDate' => $log->getStartDate()->format('d/m/Y'),
                            'endDate'   => $log->getEndDate()->format('d/m/Y'),
                            'errorMessage' => ''];
            $errorMessage = $log->getErrorMessage();
            if ($errorMessage) {
                $currentLog['errorMessage'] = $errorMessage;
            }
            else{
                $newSamples = json_decode($log->getNewSamples());
                $currentLog['newSamples'] = count($newSamples);
                $changedSamples = json_decode($log->getChangedSamples());
                $currentLog['changedSamples'] = count($changedSamples);
                $errorSamples = json_decode($log->getErrorSamples());
                $currentLog['errorSamples'] = count($errorSamples);
            }
            $logs[] = $currentLog;
        }
        $data['importLytechLogs'] = $logs;
        $mgr = $controller->get('easy_module.manager');
        $data['showLogDetailUrl'] = $mgr->getUrl('leep_admin', 'sample_import_job', 'feature', 'showLogDetail');
        $response = $controller->renderView('LeepAdminBundle:SampleImportJob:import_lytech_log.html.twig',$data);
        return new Response($response);
    }

    public function handleShowLogDetail($controller, &$data, $options=array())
    {
        $logId_str = $controller->get('request')->get('requestData', 0);
        $logId = intval($logId_str);
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('AppDatabaseMainBundle:ImportLytechLog', 'p')
                ->where('p.id = :logId')
                ->setParameter('logId', $logId);
        $log = $query->getQuery()->getArrayResult();
        $log = $log[0];

        $newSamples_str = $this->getStringSample(json_decode($log['newSamples'], true));
        $changedSamples_str = $this->getStringChangedSample(json_decode($log['changedSamples'], true));
        $errorSamples_str = $this->getStringSample(json_decode($log['errorSamples'], true));

        $data['content'] = $log['content'];
        $data['newSamples'] = $newSamples_str;
        $data['changedSamples'] = $changedSamples_str;
        $data['changedSamples'] = '';
        $data['errorSamples'] = $errorSamples_str;

        $response = $controller->renderView('LeepAdminBundle:SampleImportJob:import_lytech_log_detail.html.twig',$data);
        return new Response($response);
    }

    private function getStringSample($samples)
    {
        $samples_str_array = [];
        foreach ($samples as $sample) {
            $samples_str_array[] = $this->genereateSample($sample);
        }
        return $samples_str_array;
    }

    private function getStringChangedSample($samples)
    {
        $samples_str_array = [];
        foreach ($samples as $sample) {
            $tmp_sub_sample = array_slice($sample, 'existing');
            unset($sample['existing']);
            $tmp_sub_sample_str = $this->genereateSample($tmp_sub_sample);
            $tmp_sample_str = $this->genereateSample($tmp_sample);
            // $sample_str = $tmp_sample_str + ', Exisisting: (' + $tmp_sub_sample_str + ')';
            $samples_str_array[] = ['new' => $tmp_sample_str, 'existing' => $tmp_sub_sample_str];
        }
        return $samples_str_array;
    }

    private function genereateSample($sample)
    {
        $sample_str_array = [];
        foreach ($sample as $keyElement => $valueElement) {
            $value_tmp = $valueElement;
            if ($keyElement == 'registeredDate' || $keyElement == 'finishedDate') {
                $value_tmp = explode(' ', $valueElement['date'])[0];
            }
            $sample_str_array[] = sprintf("%s: %s", $keyElement, $value_tmp);
        }
        $sample_str = implode(', ', $sample_str_array);
        return $sample_str;
    }
}
