<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

use Aws\Sns\MessageValidator\Message;
use Aws\Sns\MessageValidator\MessageValidator;
use Guzzle\Http\Client;

use Leep\AdminBundle\Business\MailSender\Constants;

class MailSender extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function prepareMainTabs($controller, $selectedTab, $idEmailPool = null) {
        $mgr = $controller->get('easy_module.manager');

        if ($idEmailPool) {
            $requestParams = array('idEmailPool' => $idEmailPool);
        } else {
            $requestParams = array(
                'key' => $controller->get('request')->get('key', '')
            );
        }

        // prepare tabs to review
        $counter = 1;
        $arrTabs = array();
        foreach (Business\MailSender\Util::getReceiverTypeList() as $receiverType => $receiverTypeLabel) {
            $arrtabs[$receiverType] = array(
                                            'title' => $receiverTypeLabel, 
                                            'link' => $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'viewRecievers', array_merge(array('receiverType' => $receiverType), $requestParams))
                                        );
        }

        $mainTabs = array(
            'content' => $arrtabs,
            'selected' => $selectedTab
        );

        return $mainTabs;               
    }

    public function handleViewRecievers($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $receiverType = $request->get('receiverType', Business\MailSender\Util::RECEIVER_TYPE_CUSTOMER);
        $idEmailPool = $request->get('idEmailPool', null);

        $response = new Response('');

        if (in_array($receiverType, array_keys(Business\MailSender\Util::getReceiverTypeList()))) {

            $data['mainTabs'] = self::prepareMainTabs($controller, $receiverType, $idEmailPool);
            
            $data['emailList'] = Business\MailSender\Util::getReceiverEmailListByType($controller, $receiverType, $idEmailPool);
            $response = $controller->render('LeepAdminBundle:MailSender:receiver_list.html.twig', $data);
        }

        return $response;
    }

    public function handleCountRecievers($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $tempData = $controller->get('leep_admin.helper.temp_data');
        
        $params = $request->get('form');
        $params = (is_array($params) ? $params : array($params));
        $totalReceivers = Business\MailSender\Util::getReceiverTotal($controller, $params);

        $key = $tempData->generateKey();
        $tempData->save($key, $params);

        $viewFullReceiverListURL = $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'viewRecievers', array('key' => $key));
        $sendToData = "<a href='" . $viewFullReceiverListURL . "' target='_blank'>" .$totalReceivers . ' receivers' . "</a>";

        return new Response($sendToData);
    }

    public function handleViewEmailContent($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $idEmailPool = $request->get('id', 0);

        $emailPool = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($idEmailPool);

        $response = new Response('No Content Avaialble');

        if ($emailPool) {
            $response = new Response($emailPool->getContent());
        }

        return $response;
    }

    public function handlePreviewEmail($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $content = $request->get('content', null);

        return new Response($content);
    }

    public function handleSendLog($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        
        $idEmailPool = $controller->get('request')->get('id', 0);
        $data['idEmailPool'] = $idEmailPool;

        $logs = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:EmailPoolLog', 'p')
            ->andWhere('p.idEmailPool = :idEmailPool')
            ->setParameter('idEmailPool', $idEmailPool)
            ->orderBy('p.createdAt', 'ASC');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $logs[] = array(
                'time' => $formatter->format($r->getCreatedAt(), 'datetime'),
                'receiver'  => $r->getReceiver(),
                'log'  => $r->getMessage()
            );
        }

        $data['logs'] = $logs;

        return $controller->render('LeepAdminBundle:MailSender:log.html.twig', $data);
    }

    public function handleCreateOnePixelImage($controller, &$data, $options = array()) {
        // record the request
        self::logOnePixelImageRequest($controller);

        $filename = $this->container->getParameter('kernel.root_dir').'/../web/bundles/leepadmin/images/1pixel.png';
        return new StreamedResponse(function () use ($filename) {
            readfile($filename);
        }, 200, array('Content-Type' => 'image/png'));
    }

    private function logOnePixelImageRequest($controller) {
        // log the request
        $helperEncrypt = $controller->get('leep_admin.helper.encrypt');
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $idEmailPoolReceiver = $helperEncrypt->decrypt($request->get('code', null));

        if ($idEmailPoolReceiver) {
            $emailPoolReceiverView = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailPoolReceiverView')->findOneByIdEmailPoolReceiver($idEmailPoolReceiver);

            // 1 time log
            if (!$emailPoolReceiverView) {
                $emailPoolReceiver = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailPoolReceiver')->findOneByid($idEmailPoolReceiver);

                if ($emailPoolReceiver) {
                    $inst = new Entity\EmailPoolReceiverView();
                    $inst->setIdEmailPoolReceiver($idEmailPoolReceiver);
                    $inst->setCreatedAt(new \DateTime());
                    $em->persist($inst);
                    $em->flush();
                }                
            }
        }
    }

    public function handleReceiverViewLog($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        
        $idEmailPool = $controller->get('request')->get('id', 0);
        $data['idEmailPool'] = $idEmailPool;

        $views = array();
        $query = $em->createQueryBuilder();
        $query
            ->select('p.id, p.createdAt,
                        epr.email, epr.name
                    ')
            ->from('AppDatabaseMainBundle:EmailPoolReceiverView', 'p')
            ->innerJoin('AppDatabaseMainBundle:EmailPoolReceiver', 'epr', 'WITH', 'epr.id = p.idEmailPoolReceiver')
            ->andWhere('epr.idEmailPool = :idEmailPool')
            ->setParameter('idEmailPool', $idEmailPool)
            ->orderBy('p.createdAt', 'ASC');

        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $receiver = $r['email'];

            if ($r['name']) {
                $receiver = sprintf('%s <%s>', $r['name'], $r['email']);
            }

            $views[] = array(
                'time' => $formatter->format($r['createdAt'], 'datetime'),
                'receiver'  => $receiver
            );
        }

        $data['views'] = $views;

        return $controller->render('LeepAdminBundle:MailSender:email_pool_receiver_view.html.twig', $data);
    }

    public function handleAwsSns($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();

        $message = Message::fromRawPostData();
        $validator = new MessageValidator();
        $validator->validate($message);

        if ($message->get('Type') === 'SubscriptionConfirmation') {
            (new Client)->get($message->get('SubscribeURL'))->send();
            
            $response = new Response('OK');
            return $response;
        }
        
        $jsonMessage = json_decode($message->get('Message'), true);

        $type = trim($jsonMessage['notificationType']);

        // Parse target
        $destinations = $jsonMessage['mail']['destination']; 
        $email = isset($destinations[0]) ? $destinations[0] : '';

        // Parse content
        if ($type == 'Delivery') {
            $idType = Constants::EMAIL_POOL_SNS_LOG_TYPE_DELIVERY;
            $titleMessage = 'Delivery ['.$jsonMessage['delivery']['smtpResponse'].']';
        }
        else if ($type == 'Bounce') {
            $idType = Constants::EMAIL_POOL_SNS_LOG_TYPE_BOUNCE;
            $bounceType = isset($jsonMessage['bounce']['bounceType']) ? $jsonMessage['bounce']['bounceType'] : '';
            $bounceSubType = isset($jsonMessage['bounce']['bounceSubType']) ? $jsonMessage['bounce']['bounceSubType'] : '';

            $titleMessage = 'Bounce ['.$bounceType.' | '.$bounceSubType.']';
        }
        else if ($type == 'Complaint') {
            $idType = Constants::EMAIL_POOL_SNS_LOG_TYPE_COMPLAINT;
            $feedbackType = isset($jsonMessage['complaint']['complaintFeedbackType']) ? $jsonMessage['complaint']['complaintFeedbackType'] : '';
            $titleMessage = 'Complaint ['.$feedbackType.']';   
        }
        else {
            $idType = Constants::EMAIL_POOL_SNS_LOG_TYPE_UNKNOWN;
            $titleMessage = 'Unknown';
        }


        // Log the message
        $emailPoolSnsLog = new Entity\EmailPoolSnsLog();
        $emailPoolSnsLog->setCreatedAt(new \DateTime());
        $emailPoolSnsLog->setEmail($email);
        $emailPoolSnsLog->setIdType($idType);
        $emailPoolSnsLog->setMessage($titleMessage);
        $emailPoolSnsLog->setBody($message->get('Message'));
        $em->persist($emailPoolSnsLog);
        $em->flush();

        $response = new Response('OK');
        return $response;
    }

    public function handleOptOut($controller, &$data, $options = array()) {
        // log the request
        $helperEncrypt = $controller->get('leep_admin.helper.encrypt');
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');

        $idEmailPoolReceiver = $helperEncrypt->decrypt($request->get('code', null));

        $emailPoolReceiver = $em->getRepository('AppDatabaseMainBundle:EmailPoolReceiver')->findOneById($idEmailPoolReceiver);
        if ($emailPoolReceiver) {
            $data['email'] = $emailPoolReceiver->getEmail();

            $emailSenderFactory = $controller->get('leep_admin.mail_sender.business.email_sender_factory');
            $unsubscriber = $emailSenderFactory->createUnsubscriber($emailPoolReceiver->getType());
            if ($unsubscriber != null) {
                $unsubscriber->unsubscribe($emailPoolReceiver->getEmail());
            }
        }


        return $controller->render('LeepAdminBundle:MailSender:email_unsubscribe.html.twig', $data);
    }
}
