<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Partner extends AbstractFeature {
    CONST MIN_CODE = 1;
    CONST MAX_CODE = 1000;

    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleAjaxRead($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);

        $partner = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);
        if ($partner) {
            $arr = $controller->get('leep_admin.helper.common')->copyEntityToArray($partner);
            $resp = new Response();
            $resp->setContent(json_encode($arr));
            return $resp;
        }
        return new Response('FAIL');
    }

    public function handleGeneratePassword($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        // 8 digit-password
        $newPassword = $helperCommon->generateRandPasscode(8);

        return new Response($newPassword);
    }

    public function handleGenerateUsername($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        $inputName = $controller->get('request')->query->get('name');

        $newName = $helperCommon->generateUsername($inputName);

        return new Response($newName);
    }

    public function handleWebLoginMassEmail($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $mgr = $controller->get('easy_module.manager');
        $em = $controller->get('doctrine')->getEntityManager();
        $selected = $controller->get('request')->get('selected', null);
        $selectedId = explode(',', $selected);

        $partners = $em->getRepository('AppDatabaseMainBundle:Partner')->findById($selectedId);
        $data['partnerList'] = [];
        foreach ($partners as $partner) {
            // Generate email template for each dc
            $arrInputData = array(
                'name'          =>  $partner->getFirstname().' '.$partner->getSurname(),
                'usernamelogin' =>  $partner->getWebLoginUsername(),
                'passwordlogin' =>  $partner->getWebLoginPassword(),
                'idLanguage'    =>  $partner->getIdLanguage(),
                'type'          =>  3,
                'id'            =>  $partner->getId()
            );

            $emailTemplate = Business\EmailTemplate\Utils::getEmailTemplate($controller->get('service_container'), $arrInputData);
            $data['partnerList'][] = [
                'id' =>             $partner->getId(),
                'name' =>           $partner->getPartner(),
                'username' =>       $partner->getWebLoginUsername(),
                'password' =>       $partner->getWebLoginPassword(),
                'sendTo' =>         trim($partner->getContactemail()),
                'language' =>       $partner->getIdLanguage() == 0 ? 1 : $partner->getIdLanguage(),
                'emailSubject' =>   $emailTemplate['subject'],
                'emailContent' =>   $emailTemplate['content']
            ];
        }

        $data['languages'] = $mapping->getMapping('LeepAdmin_Customer_Language');
        $data['emailMethods'] = $mapping->getMapping('LeepAdmin_EmailMethod_List');
        $data['selected'] = $selected;
        $data['massUpdateLanguageUrl'] = $mgr->getUrl('leep_admin', 'partner', 'feature', 'massUpdateLanguage');
        $data['massWebLoginEmailSendUrl'] = $mgr->getUrl('leep_admin', 'partner', 'feature', 'sendMassWebLoginEmail');
        $data['generateUsernameUrl'] = $mgr->getUrl('leep_admin', 'partner', 'feature', 'generateUsername');
        $data['generatePasswordUrl'] = $mgr->getUrl('leep_admin', 'partner', 'feature', 'generatePassword');
        $data['saveUrl'] = $mgr->getUrl('leep_admin', 'partner', 'feature', 'saveUserNameAndPassword');

        if (isset($options['error'])) {
            $data['errorMsg'] = $options['error'];
        } else {
            $data['errorMsg'] = "";
        }

        if (isset($options['msg'])) {
            $data['otherMsg'] = $options['msg'];
        } else {
            $data['otherMsg'] = "";
        }

        return $controller->render('LeepAdminBundle:Partner:web_login_mass_email.html.twig', $data);
    }

    public function handleSaveUserNameAndPassword($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $username = $controller->get('request')->get('username', null);
        $password = $controller->get('request')->get('password', null);
        $helperCommon = $controller->get('leep_admin.helper.common');
        $id = $controller->get('request')->get('id', null);

        $receiver = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);

        // check username exist
        if ($username) {
            if ($helperCommon->isWebLoginUsernameExisted($username, 'Partner', $id)) {
                $options['error'] = "This Web Login Username " . $username . " for Partner($id) is already existed";
            } else {
                $receiver->setWebLoginUsername($username);
            }
        } else {
            $receiver->setWebLoginUsername(null);
        }

        if ($password) {
            $receiver->setWebLoginPassword($password);
        } else {
            $receiver->setWebLoginPassword(null);
        }

        $em->persist($receiver);
        $em->flush();

        if (!isset($options['error'])) {
            $options['msg'] = "Success";
        }

        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleMassUpdateLanguage($controller, &$data, $options = array()) {
        $selected = $controller->get('request')->get('selected', null);
        $languageId = $controller->get('request')->get('language', null);
        $em = $controller->get('doctrine')->getEntityManager();
        $selectedId = explode(',', $selected);

        $partners = $em->getRepository('AppDatabaseMainBundle:Partner')->findById($selectedId);
        foreach ($partners as $partner) {
            $partner->setIdLanguage($languageId);
            $em->persist($partner);
        }
        $em->flush();

        $options['msg'] = "Language updated";
        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleSendMassWebLoginEmail($controller, &$data, $options = array()) {
        $selected = $controller->get('request')->get('selected', null);
        $partnerList = $controller->get('request')->get('partner', []);
        $em = $controller->get('doctrine')->getEntityManager();
        $isSendNow = $controller->get('request')->get('isSendNow', false);
        $isAutoRetry = $controller->get('request')->get('isAutoRetry', false);
        $selectedId = explode(',', $selected);

        foreach ($partnerList as $id => $partner) {
            $emailOutgoing = new Entity\EmailOutgoing();
            $emailOutgoing->setCreationDatetime(new \DateTime());
            $emailOutgoing->setIdQueue(Business\EmailOutgoing\Constant::EMAIL_QUEUE_WEB_LOGIN_PN);
            $emailOutgoing->setIdEmailMethod($partner['emailMethod']);
            $emailOutgoing->setRecipients($partner['sendTo']);
            $emailOutgoing->setCc($partner['cc']);
            $emailOutgoing->setTitle($partner['emailSubject']);
            $emailOutgoing->setBody($partner['emailContent']);
            $emailOutgoing->setIsSendNow($isSendNow);
            $emailOutgoing->setIsAutoRetry($isAutoRetry);
            $emailOutgoing->setStatus(Business\EmailOutgoing\Constant::EMAIL_STATUS_OPEN);

            $em->persist($emailOutgoing);
        }

        $em->flush();

        $options['msg'] = "Email created";
        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleLoginCustomerDashboard($controller, &$data, $options=array())
    {
        $idPartner = $controller->get('request')->get('id');
        $randomCode = sha1(uniqid());

        $em = $controller->get('doctrine')->getEntityManager();
        $tmpInfo = new Entity\TemperateLoginInfo();
        $tmpInfo->setRandCode($randomCode);
        $tmpInfo->setEntityId($idPartner);
        $tmpInfo->setClickTime(new \DateTime('now'));
        $em->persist($tmpInfo);
        $em->flush();

        $requestInfo_js =json_encode(['0', $randomCode]);

        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $linkToCustomerDashboard = $config->getCustomerDashboardLoginUrl();
        $urlLogin = $linkToCustomerDashboard."/login/dropin?requestinfo={$requestInfo_js}";
        return new RedirectResponse($urlLogin);
    }

    public function handleExportPartnerToCsv($controller, &$data, $options=array())
    {
        // declare necessary parameter
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        // get request
        $selectedIds = $controller->get('request')->get('selected');

        // get filtered partner
        $filterForm = $this->container->get('leep_admin.partner.business.filter_handler');
        $filters = $filterForm->getCurrentFilter();

        // Let's query
        $query = $em->createQueryBuilder();
        $query->select('pa')
                ->from('AppDatabaseMainBundle:Partner', 'pa')
                ->where('pa.isdeleted = 0');
        if ($selectedIds == "all") {
            Business\Partner\Utils::applyFilter($query, $filters);
        }
        else{
            $query->where($query->expr()->in('pa.id', $selectedIds));
        }
        $partners = $query->getQuery()->getResult();

        // create export data
        $content = '';
        $content .= '"Partner"; "Name"; "Address"; "Email"; "Instution"; "Telephone"; "Fax"'."\r\n";
        foreach ($partners as $partner) {
            $rows = [];
            $rows[] = '"'.addslashes($partner->getPartner()).'"';
            $rows[] = '"'.addslashes($partner->getFirstname().' '.$partner->getSurname()).'"';
            $address = sprintf("%s, %s, %s, %s",
                $partner->getStreet(),
                $partner->getCity(),
                $partner->getPostcode(),
                $formatter->format($partner->getCountryId(), 'mapping', 'LeepAdmin_Country_List'));
            $rows[] = '"'.addslashes($address).'"';
            $rows[] = '"'.addslashes($partner->getContactemail()).'"';
            $rows[] = '"'.addslashes($partner->getInstitution()).'"';
            $rows[] = '"'.addslashes($partner->getTelephone()).'"';
            $rows[] = '"'.addslashes($partner->getFax()).'"';

            $content .= implode(";", $rows)."\r\n";
        }

        // let's export
        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=partners.csv');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }
}
