<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class ScanForm extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleDownloadScanFormCodeImportPdf($controller, &$data, $options = array()) {          
        $id = $controller->get('request')->get('id', 0);
        $importCode = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormCodeImport')->findOneById($id);

        $pdf = $controller->get('white_october.tcpdf')->create();    
        $pdf->isShowFooter = false;
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(false);;    
        $pdf->AddPage('L');

        $codes = explode("\n", $importCode->getCode());
        $curY = 7;
        foreach ($codes as $code) {
            $text = '';
            for ($i = 0; $i < strlen($code); $i++) {
                $text .= '<font face="courier" style="font-size: 40"><b>'.$code[$i].'</b></font>';
                $text .= '<font face="courier" style="font-size: 15">&nbsp;</font>';
            }

            $pdf->writeHTMLCell($w=0, $h=0, $x=15, $curY, $text, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);   
            $pdf->writeHTMLCell($w=0, $h=0, $x=162, $curY, $text, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);   
            
            $curY += 29;
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=code_import_'.$id.'.pdf');        
        $response->setContent($pdf->Output('', 'S'));
        return $response;
    }

    public function handleGenerateCode($controller, &$data, $options = array()) {                  
        $availableCodes = array();
        
        // Load scan form code
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormCode')->findAll();
        foreach ($result as $r) {
            $availableCodes[] = $r->getCode();
        }

        // Load import job
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormCodeImport')->findAll();
        foreach ($result as $r) {
            $arr = explode("\n", $r->getCode());
            foreach ($arr as $c) {
                $c = trim($c);
                if ($c != '' && strlen($c) == 10) {
                    $availableCodes[] = $c;
                }
            }
        }

        // Generate codes
        $codes = array();
        for ($i = 1; $i <= 7; $i++) {
            $randCode = '';
            $notFound = true;
            while ($notFound) {                
                $randCode = $this->generateRandCode(10);
                $notFound = false;
    
                foreach ($availableCodes as $code) {
                    if ($this->computeCodeDistance($code, $randCode) < 4) {
                        $notFound = true;
                        break;
                    }
                }

                foreach ($codes as $code) {
                    if ($this->computeCodeDistance($code, $randCode) < 4) {
                        $notFound = true;
                        break;
                    }
                }
            }
            $codes[] = $randCode;
        }
        return new Response(implode("\n", $codes));
    }

    protected function generateRandCode($length) {
        $alphabet = 'ACDEFGHJKLMNPQRSTUVWXYZ0123456789';

        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $pos = rand(0, strlen($alphabet) - 1);
            $str .= $alphabet[$pos];
        }

        return $str;
    }

    protected function computeCodeDistance($c1, $c2) {
        $distance = 0;
        $len = strlen($c1);
        for ($i = 0; $i < $len; $i++) {
            if ($c1[$i] != $c2[$i]) {
                $distance++;
            }
        }
        return $distance;
    }


    public function handleDownloadScanFormPdf($controller, &$data, $options = array()) {          
        $id = $controller->get('request')->get('id', 0);
        $scanForm = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanForm')->findOneById($id);

        $pdf = $controller->get('white_october.tcpdf')->create();    
        $pdf->isShowFooter = false;
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(false);;    
        $pdf->AddPage('');

        // Insert header
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '5', 'phase' => 10, 'color' => array(0, 0, 0));
        $pdf->Rect(
            $x = 11, 
            $y = 8, 
            $width = 40, 
            $height = 15, 
            'DF', 
            $style, 
            array(0, 0, 0)
        );

        // Write code
        $code = $scanForm->getCode();
        $text = '';
        for ($i = 0; $i < strlen($code); $i++) {
            $text .= '<font face="courier" style="font-size: 30"><b>'.$code[$i].'</b></font>';
            $text .= '<font face="courier" style="font-size: 15">&nbsp;</font>';
        }

        $pdf->writeHTMLCell($w=0, $h=0, $x=$pdf->getPageWidth() / 2 - 10, $y=8, $text, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);   

        // Insert image
        $imageFile = $controller->get('service_container')->getParameter('files_dir').'/scan_form/'.$scanForm->getFormImgFile();
        $imageSize = getimagesize($imageFile);
        $ratio = $imageSize[1] / $imageSize[0];
        $imageWidth = $pdf->getPageWidth() - 20;
        $imageHeight = intval(($imageSize[1] / $imageSize[0]) * $imageWidth);
        $pdf->Image(
            $file = $imageFile,
            $x = '',
            $y = 40, 
            $w = $imageWidth,
            $h = $imageHeight,
            $type = '',
            $link = '',
            $align = 'R',
            $resize = true,
            $dpi = 1300,
            $palign = 'L'
        );

        // Insert footer
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '5', 'phase' => 10, 'color' => array(0, 0, 0));
        $pdf->Rect(
            $x = $pdf->getPageWidth() - 51, 
            $y = $imageHeight + 55, 
            $width = 40, 
            $height = 15, 
            'DF', 
            $style, 
            array(0, 0, 0)
        );

        // Insert cut-line
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '5', 'phase' => 10, 'color' => array(0, 0, 0));
        $pdf->Line(0, $imageHeight + 83, $pdf->getPageWidth(), $imageHeight + 83, $style);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename='.$scanForm->getName().'.pdf');        
        $response->setContent($pdf->Output('', 'S'));
        return $response;
    }

    protected function cleanupFilename($name) {
        $name = str_replace('/', '', $name);
        $name = str_replace('..', '', $name);
        $name = str_replace('\\', '', $name);
        return $name;
    }

    public function handleViewScanFormImage($controller, &$data, $options = array()) {      
        $request = $controller->get('request');
        $dir = $this->cleanupFilename($request->get('dir'));
        $batch = $this->cleanupFilename($request->get('batch'));
        $recordId = $this->cleanupFilename($request->get('id'));
        $name = $this->cleanupFilename($request->get('name'));

        $fileDir = $controller->get('service_container')->getParameter('files_dir');
        $fileName = $fileDir.'/scan_form_images/'.$dir.'/'.$batch.'/'.$recordId.'/'.$name.'.jpg';        
        if (is_file($fileName)) {
            $resp = new Response();
            $resp->headers->set('Content-Type', 'image');
            $resp->headers->set('Content-Disposition', 'inline; filename='.$name.'.jpg');
            $resp->setContent(file_get_contents($fileName));
            return $resp;
        }
    }
}
