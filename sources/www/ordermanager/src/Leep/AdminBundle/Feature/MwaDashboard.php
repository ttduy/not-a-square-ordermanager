<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class MwaDashboard extends AbstractFeature {
    public function getActions() { return array('list'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    protected function fixFlawSampleStatistic($controller, &$status) {
        $found = false;
        foreach ($status as $i => $row) {
            if ($row['status'] == 'FLAWED_SAMPLE') {
                $found = true;
                $status[$i]['total'] = $this->overrideAllTimeStatus($controller, 2);
            }
        }
        if (!$found) {
            array_unshift($status, array(
                'status' => 'FLAWED_SAMPLE',
                'total' => $this->overrideAllTimeStatus($controller, 2)
            ));
        }
    }

    protected function overrideAllTimeStatus($controller, $status) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('COUNT(DISTINCT p.idMwaSample) as total')
            ->from('AppDatabaseMainBundle:MwaSample', 's')
            ->innerJoin('AppDatabaseMainBundle:MwaSamplePush', 'p', 'WITH', 's.id = p.idMwaSample')
            ->andWhere('s.idGroup = :group')
            ->andWhere('p.status = :status')
            ->setParameter('group', Business\MwaSample\Constants::GROUP_DEFAULT)
            ->setParameter('status', $status);
        return $query->getQuery()->getSingleScalarResult();
    }

    public function handleList($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        // Config
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $receivedStatusArr = json_decode($config->getMwaReceivedStatusArr(), true);
        $inprogressStatusArr = json_decode($config->getMwaInprogressStatusArr(), true);
        $completedStatusArr = json_decode($config->getMwaCompletedStatusArr(), true);

        // Status/order statistic
        $statusStat = array();
        $orderStat = array(
            'received'    => 0,
            'inprogress'  => 0,
            'completed'   => 0
        );
        $result = $em->createQueryBuilder()
            ->select('p.currentStatus, COUNT(p) as total')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('p.idGroup = '.Business\MwaSample\Constants::GROUP_DEFAULT)
            ->groupBy('p.currentStatus')
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $statusStat[] = array(
                'status' => $formatter->format($r['currentStatus'], 'mapping', 'LeepAdmin_MwaStatus_List'),
                'total'  => $r['total']
            );        

            foreach ($receivedStatusArr as $status) {
                if ($status == $r['currentStatus']) {
                    $orderStat['received'] += $r['total'];
                }
            }
            foreach ($inprogressStatusArr as $status) {
                if ($status == $r['currentStatus']) {
                    $orderStat['inprogress'] += $r['total'];
                }
            }
            foreach ($completedStatusArr as $status) {
                if ($status == $r['currentStatus']) {
                    $orderStat['completed'] += $r['total'];
                }
            }

        }

        $this->fixFlawSampleStatistic($controller, $statusStat);

        $data['orderStat'] = $orderStat;
        $data['statusStat'] = $statusStat;

        

        // TAT statistic
        $today = new \DateTime();

        $tatStat = array(
            'total'      => 0,
            'sum'        => 0,
            'average'    => 0,
            'shortest'   => 0,
            'longest'    => 0,
            'less7'  => 0,
            'less14'  => 0,
            'less21' => 0,
            'more21' => 0
        );
        $tatStatLast3 = array(
            'total'      => 0,
            'sum'        => 0,
            'average'    => 0,
            'shortest'   => 0,
            'longest'    => 0,
            'less7'  => 0,
            'less14'  => 0,
            'less21' => 0,
            'more21' => 0
        );
        $tatStatLast1 = array(
            'total'      => 0,
            'sum'        => 0,
            'average'    => 0,
            'shortest'   => 0,
            'longest'    => 0,
            'less7'  => 0,
            'less14'  => 0,
            'less21' => 0,
            'more21' => 0
        );
        $result = $em->createQueryBuilder()
            ->select('p.registeredDate, p.finishedDate')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('p.idGroup = '.Business\MwaSample\Constants::GROUP_DEFAULT)
            ->andWhere('p.registeredDate IS NOT NULL')
            ->andWhere('p.finishedDate IS NOT NULL')
            ->getQuery()->getResult();    
        foreach ($result as $r) {            
            $tat = $r['finishedDate']->getTimestamp()-$r['registeredDate']->getTimestamp();
            if ($tat >= 0) {
                $tat = round($tat / 86400) + 1;
                
                $tatStat['total']++;
                $tatStat['sum'] += $tat;
                $tatStat['shortest'] = min($tatStat['shortest'], $tat);
                $tatStat['longest'] = max($tatStat['longest'], $tat);
                if ($tat < 7) {
                    $tatStat['less7']++;
                }
                else if ($tat < 14) {
                    $tatStat['less14']++;
                }
                else if ($tat < 21) {
                    $tatStat['less21']++;
                }
                else {
                    $tatStat['more21']++;
                }

                // Last 3 months
                $diff = $today->getTimestamp() - $r['registeredDate']->getTimestamp();
                if ($diff <= 7776000) {
                    $tatStatLast3['total']++;
                    $tatStatLast3['sum'] += $tat;
                    $tatStatLast3['shortest'] = min($tatStatLast3['shortest'], $tat);
                    $tatStatLast3['longest'] = max($tatStatLast3['longest'], $tat);
                    if ($tat < 7) {
                        $tatStatLast3['less7']++;
                    }
                    else if ($tat < 14) {
                        $tatStatLast3['less14']++;
                    }
                    else if ($tat < 21) {
                        $tatStatLast3['less21']++;
                    }
                    else {
                        $tatStatLast3['more21']++;
                    }
                }

                // Last month
                if ($diff <= 2592000) {
                    $tatStatLast1['total']++;
                    $tatStatLast1['sum'] += $tat;
                    $tatStatLast1['shortest'] = min($tatStatLast1['shortest'], $tat);
                    $tatStatLast1['longest'] = max($tatStatLast1['longest'], $tat);
                    if ($tat < 7) {
                        $tatStatLast1['less7']++;
                    }
                    else if ($tat < 14) {
                        $tatStatLast1['less14']++;
                    }
                    else if ($tat < 21) {
                        $tatStatLast1['less21']++;
                    }
                    else {
                        $tatStatLast1['more21']++;
                    }
                }
            }            
        }
        if ($tatStat['total'] != 0) {
            $tatStat['average'] = number_format($tatStat['sum'] / $tatStat['total'], 2);
            $tatStat['less7'] = number_format($tatStat['less7'] * 100 / $tatStat['total'], 2);
            $tatStat['less14'] = number_format($tatStat['less14'] * 100 / $tatStat['total'], 2);
            $tatStat['less21'] = number_format($tatStat['less21'] * 100 / $tatStat['total'], 2);
            $tatStat['more21'] = number_format($tatStat['more21'] * 100 / $tatStat['total'], 2);
        }
        if ($tatStatLast3['total'] != 0) {
            $tatStatLast3['average'] = number_format($tatStatLast3['sum'] / $tatStatLast3['total'], 2);
            $tatStatLast3['less7'] = number_format($tatStatLast3['less7'] * 100 / $tatStatLast3['total'], 2);
            $tatStatLast3['less14'] = number_format($tatStatLast3['less14'] * 100 / $tatStatLast3['total'], 2);
            $tatStatLast3['less21'] = number_format($tatStatLast3['less21'] * 100 / $tatStatLast3['total'], 2);
            $tatStatLast3['more21'] = number_format($tatStatLast3['more21'] * 100 / $tatStatLast3['total'], 2);
        }
        if ($tatStatLast1['total'] != 0) {
            $tatStatLast1['average'] = number_format($tatStatLast1['sum'] / $tatStatLast1['total'], 2);
            $tatStatLast1['less7'] = number_format($tatStatLast1['less7'] * 100 / $tatStatLast1['total'], 2);
            $tatStatLast1['less14'] = number_format($tatStatLast1['less14'] * 100 / $tatStatLast1['total'], 2);
            $tatStatLast1['less21'] = number_format($tatStatLast1['less21'] * 100 / $tatStatLast1['total'], 2);
            $tatStatLast1['more21'] = number_format($tatStatLast1['more21'] * 100 / $tatStatLast1['total'], 2);
        }

        $data['tatStatAll'] = $tatStat;
        $data['tatStatLast3'] = $tatStatLast3;
        $data['tatStatLast1'] = $tatStatLast1;
    }
}
