<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ShipmentManager extends AbstractFeature {
    public $container;
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleAjaxRead($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);

        $partner = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);
        if ($partner) {
            $arr = $controller->get('leep_admin.helper.common')->copyEntityToArray($partner);
            $resp = new Response();
            $resp->setContent(json_encode($arr));
            return $resp;
        }
        return new Response('FAIL');
    }

    public function handleGetAddress($controller, &$data, $options = array()){
        $request = $controller->get('request');
        $type = $request->query->get('type',0);
        $Dc_partner = $request->query->get('Dc_partner',0);
        $result=array();
        $em = $controller->get('doctrine')->getEntityManager();
        $query=$em->createQueryBuilder();
        if($type==0){
            $result = $query->select('p.street, p.city')
            ->from('AppDatabaseMainBundle:Partner', 'p')
            ->andWhere('p.id = :id')
            ->setParameter('id',  $Dc_partner)
            ->getQuery()->getResult();
        }
        else{
            $result = $query->select('p.street, p.city')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere('p.id = :id')
            ->setParameter('id',  $Dc_partner)
            ->getQuery()->getResult();
        }
        return new Response(json_encode($result[0]));
    }

    public function handleViewShipmentHistory($controller, &$data, $options = []) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idShipment = $controller->get('request')->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');

        $logs = $em->getRepository('AppDatabaseMainBundle:ShipmentHistory')->findBy(
            ['idShipment' => $idShipment],
            ['date' => 'DESC']
        );

        $data['logs'] = [];
        foreach ($logs as $log) {
            $data['logs'][] = [
            'id' =>     $log->getIdShipment(),
            'date' =>   $formatter->format($log->getDate(), 'datetime'),
            'address' =>   $log->getAddress(),
            'createBy' => $formatter->format($log->getCreateBy(), 'mapping', 'LeepAdmin_WebUser_List'),
            'status' =>   $log->getStatus(),
            'comment' =>   $log->getComment(),
            'changeBy' => $formatter->format($log->getChangeBy(), 'mapping', 'LeepAdmin_WebUser_List'),
            ];
        }
        return $controller->render('LeepAdminBundle:ShipmentManager:shipment_log_view.html.twig', $data);
    }
}
