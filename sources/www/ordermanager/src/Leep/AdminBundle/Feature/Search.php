<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Search extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleSearch($controller, &$data, $options = array()) {

        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        $filter = $controller->get('leep_admin.search.business.filter_handler');
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'Form_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Apply filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => '',
            'visiblity' => 'hide'
        );

        $grid = $controller->get('leep_admin.search.business.grid_reader');

        // Search List
        $data['searchList'] = array();
        $id = 0;
        $sections = Business\Search\Constant::getFindIn();
        foreach ($sections as $sectionId => $sectionName) {
            $data['searchList'][] = array(
                'id'      => 'id',
                'name'    => $sectionName,
                'status'  => '',
                'grid'    => array(
                    'id'          =>  'Grid'.$sectionId,
                    'header'      =>  $grid->getTableHeader(),
                    'ajaxSource'  =>  $mgr->getUrl('leep_admin', 'search', 'grid', 'ajaxSource', array('search' => $sectionId))
                )
            );
        }
        return $controller->render('LeepAdminBundle:Search:list.html.twig', $data);
    }
}
