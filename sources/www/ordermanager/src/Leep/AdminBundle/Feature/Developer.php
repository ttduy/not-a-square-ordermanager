<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Developer extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleTestNewCombobox($controller, &$data, $options = array()) {    
        return $controller->render('LeepAdminBundle:Developer:test_new_combobox.html.twig', $data);
    }

    public function handleEncode($controller, &$data, $options = array()) {            
        $request = $controller->get('request');
        if ($request->getMethod() == 'POST') {
            $file = $request->files->get('file');
            mb_internal_encoding('UTF-8');
            $content = file_get_contents($file);
            $content = mb_convert_encoding($content, 'CP850');

            $files = explode('.', $file->getClientOriginalName());
            $name = $files[0].'-encoded.'.$files[1];

            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment; filename='.$name);                    
            $response->setContent($content);

            return $response;
        }
        return $controller->render('LeepAdminBundle:Developer:test.html.twig', $data);
    }
}
