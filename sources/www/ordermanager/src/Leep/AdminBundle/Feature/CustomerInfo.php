<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CustomerInfo extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleExistCustomerInfo($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        // get submitted data
        $customerNumber = $request->get('customerNumber', '');
        $idCustomer = $request->get('idCustomer', '');
        $externalBarcode = $request->get('externalBarcode', '');
        $firstName = $request->get('firstName', '');
        $surName = $request->get('surName', '');
        $dateOfBirth = $request->get('dateOfBirth', '');
        $postCode = $request->get('postCode', '');
        $isDestroySample = $request->get('isDestroySample', '');
        $isFutureResearchParticipant = $request->get('isFutureResearchParticipant', '');
        $isNotContacted = $request->get('isNotContacted', '');
        $detailInformation = $request->get('detailInformation', '');


        $returnedData = array(
            'data' => null,
            'error' => false,
            'msg'   => '',
            'customerInfos' => array()
        );

        // Validation
        $firstName = trim($firstName);
        $surName = trim($surName);
        $dateOfBirth = \DateTime::createFromFormat('d/m/Y', trim($dateOfBirth));
        $postCode = trim($postCode);

        if (empty($firstName)) {
            $errors[] = 'First Name';
        }
        if (empty($surName)) {
            $errors[] = 'Sur Name';
        }

        if (!empty($errors)) {
            $returnedData['error'] = true;
            $returnedData['msg'] = 'Must not be empty: '.implode(', ', $errors);
        }
        else {
            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($customer->getIdCustomerInfo());

            // Search customer
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerInfo', 'p');
            $query->andWhere('p.idDistributionChannel = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $customerInfo->getIdDistributionChannel());

            if ($firstName) {
                $query->andWhere('p.firstName = :firstName')
                    ->setParameter('firstName', $firstName);
            }
            if ($surName) {
                $query->andWhere('p.surName = :surName')
                    ->setParameter('surName', $surName);
            }
            if ($dateOfBirth) {
                $query->andWhere('p.dateofbirth = :dateOfBirth')
                    ->setParameter('dateOfBirth', $dateOfBirth->format('Y-m-d'));
            }
            if ($postCode) {
                $query
                    ->andWhere('p.postCode = :postCode')
                    ->setParameter('postCode', $postCode);
            }
            $result = $query->getQuery()->getResult();


            if ($result) {
                $customerNumbers = array();
                $c = 0;
                foreach ($result as $r) {
                    $customerNumbers[] = $r->getCustomerNumber();
                    $returnedData['customerInfos'][$r->getId()] = $r->getCustomerNumber();
                    if ($c > 10) {
                        break;
                    }
                }

                $returnedData['error'] = false;
                $returnedData['msg'] = 'FOUND! Possible matched: '.implode(', ', $customerNumbers);
            } else {
                $returnedData['error'] = true;
                $returnedData['msg'] = "Not existing yet";
            }
        }

        return new Response(json_encode($returnedData, JSON_UNESCAPED_SLASHES));
    }

    public function handleGeneratePassword($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        // 8 digit-password
        $newPassword = $helperCommon->generateRandPasscode(8);

        return new Response($newPassword);
    }

    public function handleGenerateUsername($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        $inputName = $controller->get('request')->query->get('name');

        $newName = $helperCommon->generateUsername($inputName);

        return new Response($newName);
    }


    public function handleViewComplaints($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'customer_info', 'grid',  'list');
        $request = $controller->get('request');

        $idWidget = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');

        // Filter handlers
        $filterHandler = $controller->get('leep_admin.customer_info.business.filter_handler');
        $sessionId = $filterHandler->getSessionId();
        $filterModel = $filterHandler->getDefaultFormModel();

        $filterModel->idDistributionChannel = $request->get('idDistributionChannel', 0);
        $filterModel->complaintStatus = -1;

        // Update filters
        $session = $controller->get('request')->getSession();
        $session->set($sessionId, $filterModel);

        // Redirect
        return $controller->redirect($url);
    }
    public function handleViewFeedback($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'customer_info', 'grid',  'list');
        $request = $controller->get('request');

        $idWidget = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');

        // Filter handlers
        $filterHandler = $controller->get('leep_admin.customer_info.business.filter_handler');
        $sessionId = $filterHandler->getSessionId();
        $filterModel = $filterHandler->getDefaultFormModel();

        $filterModel->idDistributionChannel = $request->get('idDistributionChannel', 0);
        $filterModel->positiveFeedback = 1;

        // Update filters
        $session = $controller->get('request')->getSession();
        $session->set($sessionId, $filterModel);

        // Redirect
        return $controller->redirect($url);
    }

    public function handleLoginCustomerDashboard($controller, &$data, $options=array())
    {
        $idCustomerInfo = $controller->get('request')->get('id');
        $randomCode = sha1(uniqid());

        $em = $controller->get('doctrine')->getEntityManager();
        $tmpInfo = new Entity\TemperateLoginInfo();
        $tmpInfo->setRandCode($randomCode);
        $tmpInfo->setEntityId($idCustomerInfo);
        $tmpInfo->setClickTime(new \DateTime('now'));
        $em->persist($tmpInfo);
        $em->flush();

        $requestInfo_js =json_encode(['2', $randomCode]);

        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $linkToCustomerDashboard = $config->getCustomerDashboardLoginUrl();
        $urlLogin = $linkToCustomerDashboard."/login/dropin?requestinfo={$requestInfo_js}";
        return new RedirectResponse($urlLogin);
    }

    public function handleExportToCsv($controller, &$data, $options=array())
    {
        // declare necessary parameter
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        // get request
        $selectedIds = $controller->get('request')->get('selected');

        // get filter customer info
        $filterForm = $this->container->get('leep_admin.customer_info.business.filter_handler');
        $filters = $filterForm->getCurrentFilter();

        // let's query
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerInfo', 'p');
        if ($selectedIds == "all") {
            Business\CustomerInfo\Utils::applyFilter($query, $filters);
        }
        else{
            $query->where($query->expr()->in('p.id', $selectedIds));
        }
        $customersInfo = $query->getQuery()->getResult();

        // create export data
        $content = '';
        $content .= '"Customer number"; "Distribution Channel"; "Name"; "Gender"; "Birthday"; "Post Code"; "Email"; "Is destroy sample"; "Is future research participant"; "Is customer be contact"; "Is nutri me offered"; "Is not contact"; "Sample can be used for science"; "Locked";'."\r\n";
        foreach ($customersInfo as $customerInfo) {
            $rows = [];
            $rows[] = '"'.addslashes($customerInfo->getCustomerNumber()).'"';
            $rows[] = '"'.addslashes($formatter->format($customerInfo->getIdDistributionChannel(), 'mapping', 'LeepAdmin_DistributionChannel_List')).'"';
            $rows[] = '"'.addslashes($customerInfo->getFirstName().' '.$customerInfo->getSurName()).'"';
            $rows[] = '"'.addslashes($formatter->format($customerInfo->getGenderId(), 'mapping', 'LeepAdmin_Gender_List')).'"';
            $birthday = $customerInfo->getDateofbirth();
            $birthday_str = '';
            if ($birthday) {
                $birthday_str = $birthday->format("Y/m/d");
            }
            $rows[] = '"'.addslashes($birthday_str).'"';
            $rows[] = '"'.addslashes($customerInfo->getPostCode()).'"';
            $rows[] = '"'.addslashes($customerInfo->getEmail()).'"';
            $isDestroySample = $customerInfo->getIsDestroySample();
            $isDestroySample_str = "No";
            if ($isDestroySample == true) {
                $isDestroySample_str = "Yes";
            }
            $rows[] = '"'.addslashes($isDestroySample_str).'"';
            $isFutureResearchParticipant = $customerInfo->getIsFutureResearchParticipant();
            $isFutureResearchParticipant_str = "No";
            if ($isFutureResearchParticipant == true) {
                $isFutureResearchParticipant_str = "Yes";
            }
            $rows[] = '"'.addslashes($isFutureResearchParticipant_str).'"';
            $isCustomerBeContacted_str = "No";
            if ($customerInfo->getIsCustomerBeContacted() == true) {
                $isCustomerBeContacted_str = "Yes";
            }
            $rows[] = '"'.addslashes($isCustomerBeContacted_str).'"';
            $isNutrimeOffered_str = "No";
            if ($customerInfo->getIsNutrimeOffered() == true) {
                $isNutrimeOffered_str = "Yes";
            }
            $rows[] = '"'.addslashes($isNutrimeOffered_str).'"';
            $isNotContacted_str = "No";
            if ($customerInfo->getIsNotContacted() == true) {
                $isNotContacted_str = "Yes";
            }
            $rows[] = '"'.addslashes($isNotContacted_str).'"';
            $sampleCanBeUsedForScience_str = "No";
            if ($customerInfo->getSampleCanBeUsedForScience() == true) {
                $sampleCanBeUsedForScience_str = "Yes";
            }
            $rows[] = '"'.addslashes($sampleCanBeUsedForScience_str).'"';
            $lock = "No";
            if ($customerInfo->getIsLocked() == true) {
                $lock = "Yes";
            }
            $rows[] = '"'.addslashes($lock).'"';

            $content .= implode(";", $rows)."\r\n";
        }

        // let's export
        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=customersInfo.csv');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }
}
