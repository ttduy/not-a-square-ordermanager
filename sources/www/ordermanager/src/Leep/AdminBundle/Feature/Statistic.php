<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class Statistic extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function setDefaultTabs(&$data) {
        $mgr = $this->container->get('easy_module.manager');
        $mapping = $this->container->get('easy_mapping');
        $data['months'] = $mapping->getMapping('LeepAdmin_Month_Short');
    }

    private function applyFilter(&$data, $filter, $tab) {
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'MainFilter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => $this->getModule()->getUrl('statistic', $tab),
            'visiblity' => ''
        );
    }

    private function getCustomEntityManager() {        
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTH', 'Leep\AdminBundle\Doctrine\Extension\Month');            
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Doctrine\Extension\Year');            

        return $em;
    }

    private function prepareChartDataAmount($total, $year) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));
        $maxMonth = 12;
        if ($curYear == $year) {
            $maxMonth = intval($today->format('m'));
        }

        $arr = array();
        foreach ($total as $i => $d) {
            if ($i > $maxMonth) continue;
            $arr[] = array($i, $d);
        }
        return json_encode($arr);
    }


    private function prepareChartDataAmountPrognosis($realTotal, $total, $year, $dataIsArray = true) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));

        $maxMonth = 12;
        if ($curYear == $year) {
            $maxMonth = intval($today->format('m'));
        }

        $arr = array();
        foreach ($total as $i => $d) {
            if ($i <= $maxMonth) {
                if ($dataIsArray){
                    $arr[] = array($i, $realTotal[$i]['amount']);
                }else {
                    // use when $d is not an array
                    $arr[] = array($i, $realTotal[$i]);
                }
            }
            else {
                if ($dataIsArray){
                    $arr[] = array($i, $d['amount']);
                }else {
                    // use when $d is not an array
                    $arr[] = array($i, $d);
                }
            }
        }
        return json_encode($arr);
    }

    private function hideUnspecified(&$arr) {
        if (isset($arr[0])) {
            foreach ($arr[0]['data'] as $r) {
                if (!empty($r['amount']) || !empty($r['order'])) {
                    return;
                }
            }

            unset($arr[0]);
        }
    }

    private function prepareAnalyseArray(&$arr) {
        // Define values
        foreach ($arr as $k => $name) {
            $arr[$k] = array(
                'name'      => $name,
                'monthly'   => array(),
                'wholeYear' => 0
            );
            for ($i = 1; $i <= 12; $i++) {
                $arr[$k]['monthly'][$i] = 0;
            }
        }
    }

    // prepare Distribution Channel data
    private function prepareDistributionChannelInfo(&$summary, $filterData) {
        $em = $this->getCustomEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.creationDate) as month, COUNT(p.id) as totalRecords')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere('YEAR(p.creationDate) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $totalRecords = intval($r['totalRecords']);
            $summary['perFactor']['distributionChannel']['monthly'][$month] = $totalRecords;
            $summary['perFactor']['distributionChannel']['wholeYear'] += $totalRecords;
            $summary['total']['monthly'][$month] += intval($r['totalRecords']);
            $summary['total']['wholeYear'] += intval($r['totalRecords']);
        }
    }

    // prepare Partner data
    private function preparePartnerInfo(&$summary, $filterData) {
        $em = $this->getCustomEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.creationDate) as month, COUNT(p.id) as totalRecords')
            ->from('AppDatabaseMainBundle:Partner', 'p')
            ->andWhere('YEAR(p.creationDate) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $totalRecords = intval($r['totalRecords']);
            $summary['perFactor']['partner']['monthly'][$month] = $totalRecords;
            $summary['perFactor']['partner']['wholeYear'] += $totalRecords;
            $summary['total']['monthly'][$month] += intval($r['totalRecords']);
            $summary['total']['wholeYear'] += intval($r['totalRecords']);
        }
    }

    // prepare Lead data
    private function prepareLeadInfo(&$summary, $filterData) {
        $em = $this->getCustomEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.creationDate) as month, COUNT(p.id) as totalRecords')
            ->from('AppDatabaseMainBundle:Lead', 'p')
            ->andWhere('YEAR(p.creationDate) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $totalRecords = intval($r['totalRecords']);
            $summary['perFactor']['lead']['monthly'][$month] = $totalRecords;
            $summary['perFactor']['lead']['wholeYear'] += $totalRecords;
            $summary['total']['monthly'][$month] += intval($r['totalRecords']);
            $summary['total']['wholeYear'] += intval($r['totalRecords']);
        }
    }

    // prepare Customer data
    private function prepareCustomerInfo(&$summary, $filterData) {
        $em = $this->getCustomEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, COUNT(p.id) as totalRecords')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('YEAR(p.dateordered) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $totalRecords = intval($r['totalRecords']);
            $summary['perFactor']['customer']['monthly'][$month] = $totalRecords;
            $summary['perFactor']['customer']['wholeYear'] += $totalRecords;
            $summary['total']['monthly'][$month] += intval($r['totalRecords']);
            $summary['total']['wholeYear'] += intval($r['totalRecords']);
        }
    }

    public function handleSummary($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'summary';

        $filter = $controller->get('leep_admin.statistic.business.filter_handler');
        $this->applyFilter($data, $filter, 'summary');
        
        $filterData = $filter->getCurrentFilter();

        // Init summary
        $summary = array();

        // Total statistic
        $total = array();
        $total['monthly'] = array();
        $total['wholeYear'] = 0;
        for ($i = 1; $i <= 12; $i++) {
            $total['monthly'][$i] = 0;
        }

        $summary['total'] = $total;

        // check filter on 'type'
        switch($filterData->type) {
            case Business\Statistic\Constants::DISTRIBUTION_CHANNEL:
                // init default value
                $arrPerFactor = array();
                $arrPerFactor['distributionChannel'] = 'Distribution Channel';
                $this->prepareAnalyseArray($arrPerFactor);

                $summary['perFactor'] = $arrPerFactor;

                // prepare data
                $this->prepareDistributionChannelInfo($summary, $filterData);
                $data['totalDistributionChannelJson'] = $this->prepareChartDataAmount($summary['perFactor']['distributionChannel']['monthly'], $filterData->year);
                break;
            case Business\Statistic\Constants::PARTNER:
                // init default value
                $arrPerFactor = array();
                $arrPerFactor['partner'] = 'Partner';
                $this->prepareAnalyseArray($arrPerFactor);

                $summary['perFactor'] = $arrPerFactor;

                // prepare data
                $this->preparePartnerInfo($summary, $filterData);
                $data['totalPartnerJson'] = $this->prepareChartDataAmount($summary['perFactor']['partner']['monthly'], $filterData->year);
                break;
            case Business\Statistic\Constants::LEAD:
                // init default value
                $arrPerFactor = array();
                $arrPerFactor['lead'] = 'Lead';
                $this->prepareAnalyseArray($arrPerFactor);

                $summary['perFactor'] = $arrPerFactor;

                // prepare data
                $this->prepareLeadInfo($summary, $filterData);
                $data['totalLeadJson'] = $this->prepareChartDataAmount($summary['perFactor']['lead']['monthly'], $filterData->year);
                break;
            case Business\Statistic\Constants::CUSTOMER:
                // init default value
                $arrPerFactor = array();
                $arrPerFactor['customer'] = 'Customer';
                $this->prepareAnalyseArray($arrPerFactor);

                $summary['perFactor'] = $arrPerFactor;

                // prepare data
                $this->prepareCustomerInfo($summary, $filterData);
                $data['totalCustomerJson'] = $this->prepareChartDataAmount($summary['perFactor']['customer']['monthly'], $filterData->year);
                break;
            default:
                // init default value for each factor
                $arrPerFactor = array();
                $arrPerFactor['distributionChannel'] = 'Distribution Channel';
                $arrPerFactor['partner'] = 'Partner';
                $arrPerFactor['lead'] = 'Lead';
                $arrPerFactor['customer'] = 'Customer';
                $this->prepareAnalyseArray($arrPerFactor);

                $summary['perFactor'] = $arrPerFactor;

                // prepare data for each factor
                $this->prepareDistributionChannelInfo($summary, $filterData);
                $this->preparePartnerInfo($summary, $filterData);
                $this->prepareLeadInfo($summary, $filterData);
                $this->prepareCustomerInfo($summary, $filterData);

                $data['totalDistributionChannelJson'] = $this->prepareChartDataAmount($summary['perFactor']['distributionChannel']['monthly'], $filterData->year);
                $data['totalPartnerJson'] = $this->prepareChartDataAmount($summary['perFactor']['partner']['monthly'], $filterData->year);
                $data['totalLeadJson'] = $this->prepareChartDataAmount($summary['perFactor']['lead']['monthly'], $filterData->year);
                $data['totalCustomerJson'] = $this->prepareChartDataAmount($summary['perFactor']['customer']['monthly'], $filterData->year);
        }

        // Set data
        $data['statistic'] = $summary;
    }
}
