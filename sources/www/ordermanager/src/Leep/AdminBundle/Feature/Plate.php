<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class Plate extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleDownloadHistory($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id', 0);

        $resultHistory = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:PlateRawResult')->findOneById($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=plateRawResultSubmission_'.$resultHistory->getCreationDatetime()->format("YmdHis").'.csv');
        $response->setContent("\xEF\xBB\xBF".$resultHistory->getRawData());

        return $response;
    }

    public function handleAnalyse($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id', 0);
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        // Plate
        $plate = $em->getRepository('AppDatabaseMainBundle:Plate')->findOneById($id);
        $data['plate'] = array(
            'timestamp' => $formatter->format($plate->getCreationDatetime(), 'datetime'),
            'type'      => $formatter->format($plate->getIdPlateType(), 'mapping', 'LeepAdmin_Plate_Type'),
            'name'      => $plate->getName()
        );

        // Genes
        $rowName = array('a','b','c','d','e','f','g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p');
        $wellIndexGeneMap = array();
        $wellIndexName = array();
        for ($r = 0; $r < 16; $r++) {
            for ($c = 0; $c < 24; $c++) {
                $wellIndexGeneMap[$r*24+$c+1] = 0;
                $wellIndexName[$r*24+$c+1] = $rowName[$r].($c+1);
            }
        }

        $results = $em->getRepository('AppDatabaseMainBundle:PlateTypeWell')->findByIdPlateType($plate->getIdPlateType());        
        $genes = array();
        foreach ($results as $r) {
            if (!isset($genes[$r->getIdGene()])) {
                $genes[$r->getIdGene()] = array(
                    'id'        => $r->getIdGene(),
                    'name'      => $formatter->format($r->getIdGene(), 'mapping', 'LeepAdmin_Gene_List'),
                    'samples'   => array()
                );
            }

            $wellIndexGeneMap[$r->getRow() * 24 + $r->getCol() + 1] = $r->getIdGene();
        }

        // Load assignment
        $currentAssignment = array();
        $result = $em->getRepository('AppDatabaseMainBundle:PlateResult')->findByIdPlateRawResult($plate->getIdCurrentPlateRawResult());
        foreach ($result as $r) {
            $currentAssignment[$r->getWellIndex()] = $r->getAssignment();
        }

        // Results
        $maxFam = 0;
        $maxVic = 0;
        $maxV = 0;
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:PlateRawResultDetail', 'p')
            ->andWhere('p.idPlateRawResult = :idPlateRawResult')
            ->setParameter('idPlateRawResult', $plate->getIdCurrentPlateRawResult())
            ->addOrderBy('p.cycle', 'ASC')
            ->addOrderBy('p.wellIndex', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idGene = $wellIndexGeneMap[$r->getWellIndex()];
            if (!isset($genes[$idGene])) { continue; }
            if (!isset($genes[$idGene]['samples'][$r->getWellIndex()])) {
                $value = '';
                if (isset($currentAssignment[$r->getWellIndex()])) {
                    $value = $formatter->format($currentAssignment[$r->getWellIndex()], 'mapping', 'LeepAdmin_Plate_AssignmentMap');
                }
                $genes[$idGene]['samples'][$r->getWellIndex()] = array(
                    'name'       => $wellIndexName[$r->getWellIndex()],
                    'pos'        => $r->getWellIndex(),
                    'isSelected' => false,
                    'style'      => array(
                    ),
                    'value'      => $value,
                    'cycles'     => array(),
                    'result'     => array(
                        'fam'       => 0,
                        'vic'       => 0,
                        'rox'       => 0
                    )
                );
            }

            if ($r->getFam() > 0 || $r->getVic() > 0 || $r->getRox() > 0) {
                $genes[$idGene]['samples'][$r->getWellIndex()]['cycles'][] = array(
                    'fam' => $r->getFam(),
                    'vic' => $r->getVic(),
                    'rox' => $r->getRox()
                );
                $genes[$idGene]['samples'][$r->getWellIndex()]['result'] = array(
                    'fam' => $r->getFam(),
                    'vic' => $r->getVic(),
                    'rox' => $r->getRox()
                );
            }

            $maxFam = $maxFam > $r->getFam() ? $maxFam : $r->getFam();
            $maxVic = $maxVic > $r->getVic() ? $maxVic : $r->getVic();

            $maxV = $maxV > $r->getFam() ? $maxV : $r->getFam();
            $maxV = $maxV > $r->getVic() ? $maxV : $r->getVic();
            $maxV = $maxV > $r->getRox() ? $maxV : $r->getRox();
        }

        $genesMap = array();
        foreach ($genes as $idGene => $value) {
            $genes[$idGene]['samples'] = array_values($genes[$idGene]['samples']);            
            $genesMap[$idGene] = $value;
        }

        // Style
        $data['genesMap'] = $genesMap;
        $data['genes'] = json_encode($genes);
        $data['maxFam'] = $maxFam*1.04;
        $data['maxVic'] = $maxVic*1.04;
        $data['maxV'] = $maxV*1.04;
        $data['idPlate'] = $plate->getId();

        return $controller->render('LeepAdminBundle:Plate:analyse.html.twig', $data);
    }

    public function handleSaveAnalyse($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $content = json_decode($request->getContent(), true);
        $em = $controller->get('doctrine')->getEntityManager();

        $plate = $em->getRepository('AppDatabaseMainBundle:Plate')->findOneById($content['id']);

        // Parse assignment
        $map = $controller->get('easy_mapping')->getMapping('LeepAdmin_Plate_AssignmentMap');
        $map = array_flip($map);
        $newAssignment = array();
        $assignment = $content['assignment'];
        foreach ($assignment as $k => $v) {
            $idWellIndex = intval($k);
            $value = isset($map[$v]) ? $map[$v] : 0;
            $newAssignment[$idWellIndex] = $value;
        }

        // Update
        $dbHelper = $controller->get('leep_admin.helper.database');
        $filters = array('idPlateRawResult' => $plate->getIdCurrentPlateRawResult());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:PlateResult', $filters);

        foreach ($newAssignment as $idWellIndex => $value) {
            $result = new Entity\PlateResult();
            $result->setIdPlateRawResult($plate->getIdCurrentPlateRawResult());
            $result->setWellIndex($idWellIndex);
            $result->setAssignment($value);
            $em->persist($result);
        }
        $em->flush();

        $responseData = array('status' => 'SUCCESS');
        $resp = new Response(); 
        $resp->setContent(json_encode($responseData));
        $resp->headers->set('Content-Type', 'application/json');
        return $resp;
    }
}
