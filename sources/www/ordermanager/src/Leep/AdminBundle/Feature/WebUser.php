<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;

use Leep\AdminBundle\Business\EmailMassSender\Util as EmailUtil;

class WebUser extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleOptOut($controller, &$data, $options = array()) {
        $helperEncrypt = $controller->get('leep_admin.helper.encrypt');
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $unscribedEmail = null;

        list($type, $id) = explode('_', $helperEncrypt->decrypt($request->get('code', null)));

        $entity = null;
        $getEmailMethod = null;
        switch ($type) {
            case 1:
                $entity = 'CustomerInfo';
                $getEmailMethod = 'getEmail';
                break;
            case 2:
                $entity = 'DistributionChannels';
                $getEmailMethod = 'getContactemail';
                break;
            case 3:
                $entity = 'Partner';
                $getEmailMethod = 'getContactemail';
                break;
        }

        if ($entity && $id) {
            $info = $doctrine->getRepository('AppDatabaseMainBundle:' . $entity)->findOneById($id);
            if ($info) {
                $info->setIsNotContacted(1);
                $em->persist($info);

                // add to blacklist
                $infoEmail = $info->$getEmailMethod();
                if ($infoEmail) {
                    $email = $doctrine->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findOneByEmail($infoEmail);
                    if (!$email) {
                        $email = new Entity\EmailOptOutList();
                        $email->setEmail($infoEmail);
                        $em->persist($email);
                    }

                    $unscribedEmail = $infoEmail;
                }
                $em->flush();
            }
        }

        $data['email'] = $unscribedEmail;
    }

    public function handleWebLoginOptOut($controller, &$data, $options = array()) {
        $currentUserId = $controller->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $helperEncrypt = $controller->get('leep_admin.helper.encrypt');
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $emailList = $helperEncrypt->decrypt($request->get('email', null));
        $type = $helperEncrypt->decrypt($request->get('type', null));
        $id = $helperEncrypt->decrypt($request->get('id', null));

        if (empty($emailList)) {
            return;
        }

        $emailList = EmailUtil::extractRecipient($emailList);

        $data['email'] = "";
        foreach ($emailList as $emailInfo) {
            $data['email'] .= $emailInfo['name']."<";
            foreach ($emailInfo['email'] as $email) {
                $data['email'] .= "$email;";

                // Check if email existed, if yes, skip
                $check = $em->getRepository('AppDatabaseMainBundle:EmailWebloginOptOutList')->findByEmail($email);
                if (!empty($check)) {
                    continue;
                }

                $optOut = new Entity\EmailWebloginOptOutList();
                $optOut->setEmail($email);
                $optOut->setCreatedDate(new \DateTime());
                $optOut->setCreatedBy($currentUserId);

                $em->persist($optOut);
            }
            $data['email'] .= "> ";
        }

        $entity = null;
        if ($type == Business\EmailMassSender\Constant::RECEIVER_TYPE_CUSTOMER) {
            $entity = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);
        } else if ($type == Business\EmailMassSender\Constant::RECEIVER_TYPE_DISTRIBUTION_CHANNEL) {
            $entity = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
        } else if ($type == Business\EmailMassSender\Constant::RECEIVER_TYPE_PARTNER) {
            $entity = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);
        }

        if ($entity) {
            $entity->setIsWebLoginOptOut(false);
            $em->persist($entity);
        }

        $em->flush();
        return $controller->render('LeepAdminBundle:EmailMassSender:email_unsubscribe.html.twig', $data);
    }
}
