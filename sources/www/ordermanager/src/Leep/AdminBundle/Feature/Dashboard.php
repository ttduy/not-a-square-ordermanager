<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Dashboard extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    private function getCustomerStatistic($controller, $options) {
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.status, COUNT(p) as total')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('p.isdeleted = 0')
            ->groupBy('p.status');

        $mapping = $controller->get('easy_mapping');
        $statusMapping = $mapping->getMapping('LeepAdmin_Customer_Status');
        $status = array();
        $status[0] = array(
            'title' => 'Other',
            'value' => 0
        );
        foreach ($statusMapping as $k => $v) {
            $status[$k] = array(
                'title' => $v,
                'value' => 0
            );
        }


        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $idStatus = intval($row['status']);

            $status[$idStatus]['value'] = $row['total'];
        }

        return $status;
    }

    public function getWidgetStatistic($controller) {
        $doctrine = $controller->get('doctrine');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');

        $widgets = array();

        // Get all widget
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatistic')->findBy(array(), array('sortOrder' => 'ASC'));
        foreach ($results as $r) {
            $widgets[$r->getId()] = array(
                'name' => $r->getName(),
                'lastUpdate' => $formatter->format($r->getLastUpdate(), 'datetime'),
                'mustBeStatus' => array(),
                'mustNotBeStatus' => array(),
                'mustBeProduct' => array(),
                'mustBeCategory' => array(),
                'timePeriods' => array(),
                'calculates'  => array(),
                'result' => array(),
                'updateLink' => $mgr->getUrl('leep_admin', 'dashboard', 'dashboard', 'update', array('id' => $r->getId())),
                'customerLink' => $mgr->getUrl('leep_admin', 'dashboard', 'dashboard', 'seeCustomer', array('id' => $r->getId())),
            );
        }

        // Load time periods
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticTimePeriod')->findBy(array(), array('idTimePeriod' => 'ASC'));
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['timePeriods'][$r->getIdTimePeriod()] = $formatter->format($r->getIdTimePeriod(), 'mapping', 'LeepAdmin_Widget_TimePeriod');
            }
        }

        // Load calculates
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCalculate')->findBy(array(), array('idCalculate' => 'ASC'));
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['calculates'][$r->getIdCalculate()] = $formatter->format($r->getIdCalculate(), 'mapping', 'LeepAdmin_Widget_Calculate');
            }
        }

        // Load result
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticResult')->findAll();
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['result'][$r->getIdTimePeriod().'_'.$r->getIdCalculate()] = $r->getResult();
            }
        }

        // Must be status
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatus')->findAll();
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['mustBeStatus'][] = $formatter->format($r->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
            }
        }

        // Must not be status
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatusNot')->findAll();
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['mustNotBeStatus'][] = $formatter->format($r->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
            }
        }

        // Must be product
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticProduct')->findAll();
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['mustBeProduct'][] = $formatter->format($r->getIdProduct(), 'mapping', 'LeepAdmin_Product_List');
            }
        }

        // Must be category
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCategory')->findAll();
        foreach ($results as $r) {
            if (isset($widgets[$r->getIdWidgetStatistic()])) {
                $widgets[$r->getIdWidgetStatistic()]['mustBeCategory'][] = $formatter->format($r->getIdCategory(), 'mapping', 'LeepAdmin_Category_List');
            }
        }

        // To string
        foreach ($widgets as $idWidget => $widget) {
            $widgets[$idWidget]['mustBeStatus'] = implode(', ', $widgets[$idWidget]['mustBeStatus']);
            $widgets[$idWidget]['mustNotBeStatus'] = implode(', ', $widgets[$idWidget]['mustNotBeStatus']);
            $widgets[$idWidget]['mustBeProduct'] = implode(', ', $widgets[$idWidget]['mustBeProduct']);
            $widgets[$idWidget]['mustBeCategory'] = implode(', ', $widgets[$idWidget]['mustBeCategory']);
        }


        return $widgets;
    }

    public function handleList($controller, &$data, $options = array()) {
        $securityHelper = $controller->get('leep_admin.helper.security');

        if (!$securityHelper->hasPermission('dashboardView')) {
            // Remove all widget if you don't have permission to view dashboard
            $data['widgets'] = array();
        }
        else {
            $widgets = $this->getWidgetStatistic($controller);

            $idRevenueEstimation = Business\WidgetStatistic\Constant::CALCULATE_ESTIMATED_REVENUE;
            if (!$securityHelper->hasPermission('dashboardWidgetViewFinancial')) {
                foreach ($widgets as $idWidget => $widget) {
                    if (isset($widget['calculates'][$idRevenueEstimation])) {
                        unset($widgets[$idWidget]['calculates'][$idRevenueEstimation]);
                        if (empty($widgets[$idWidget]['calculates'])) {
                            unset($widgets[$idWidget]);
                        }
                    }
                }
            }

            $data['pendingOrdersInfo'] = $this->listPendingOrder($controller, $data, $options);
            $data['widgets'] = $widgets;
        }
    }

    public function handleUpdate($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'dashboard', 'dashboard',  'list');

        $idWidget = $controller->get('request')->get('id', 0);
        Business\WidgetStatistic\Utils::updateWidget($controller, $idWidget);

        return $controller->redirect($url);
    }

    public function handleSeeCustomer($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'customer', 'grid',  'list');

        $idWidget = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');

        // Filter handlers
        $filterHandler = $controller->get('leep_admin.customer.business.filter_handler');
        $sessionId = $filterHandler->getSessionId();
        $filterModel = $filterHandler->getDefaultFormModel();

        // Load data
        $filterModel->mustBeStatus = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatus')->findByIdWidgetStatistic($idWidget);
        foreach ($result as $r) {
            $filterModel->mustBeStatus[] = $r->getStatus();
        }

        $filterModel->mustNotBeStatus = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatusNot')->findByIdWidgetStatistic($idWidget);
        foreach ($result as $r) {
            $filterModel->mustNotBeStatus[] = $r->getStatus();
        }

        $filterModel->mustBeProduct = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticProduct')->findByIdWidgetStatistic($idWidget);
        foreach ($result as $r) {
            $filterModel->mustBeProduct[] = $r->getIdProduct();
        }

        $filterModel->mustBeCategory = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCategory')->findByIdWidgetStatistic($idWidget);
        foreach ($result as $r) {
            $filterModel->mustBeCategory[] = $r->getIdCategory();
        }

        // Update filters
        $session = $controller->get('request')->getSession();
        $session->set($sessionId, $filterModel);

        // Redirect
        return $controller->redirect($url);
    }

    private function listPendingOrder($controller, &$data, $options=array())
    {
        // TODO: get pending order data
        $em = $controller->get('doctrine')->getEntityManager();

        /// get all pending orders
        $pendingOrders = $em->getRepository("AppDatabaseMainBundle:OrderPending")->findAll();

        /// create list contain order pending and their category, products
        $pendingOrdersInfo = [];
        foreach ($pendingOrders as $pendingOrder) {
            $currentInfo = [];
            $id = $pendingOrder->getId();
            $currentInfo['id']  =  $id;
            $currentInfo['dc'] = $this->getDCName($em, $pendingOrder);
            $currentInfo['customer'] = $this->getCustomer($em, $id);
            $currentCategoryId = $pendingOrder->getOrderedCategories();
            $currentInfo['categories'] = $this->getCategoriesName($em, $currentCategoryId);
            $currentInfo['products'] = $this->getProductsName($em, $pendingOrder);
            $currentInfo['numberProducts'] = count($currentInfo['products']);
            $pendingOrdersInfo[] = $currentInfo;
        }

        return $pendingOrdersInfo;

    }

    private function getDCName($em, $pendingOrder){
        $dcId = $pendingOrder->getIdDistributionChannel();
        $dcName = "";
        if ($dcId) {
            $dc = $em->getRepository("AppDatabaseMainBundle:DistributionChannels")->findOneById($dcId);
            $dcName = $dc->getDistributionchannel();
        }
        return $dcName;
    }

    private function getCustomer($em, $pendingOrderId){
        $pendingOrder = $em->getRepository("AppDatabaseMainBundle:OrderPending")->findOneById($pendingOrderId);
        $contactInfo = [];
        $contactTitle = $pendingOrder->getContactTitle();
        if ($contactTitle) {
            $contactInfo[] = $contactTitle;
        }
        $firstName = $pendingOrder->getContactFirstName();
        if ($firstName) {
            $contactInfo[] = $firstName;
        }
        $surName = $pendingOrder->getContactSurName();
        if ($surName) {
            $contactInfo[] = $surName;
        }
        $contactInfo_str = implode(' ', $contactInfo);
        return "{$contactInfo_str}";
    }

    private function getCategoriesName($em, $categoriesId_str){
        $categoriesId = explode(',', $categoriesId_str);
        if ($categoriesId_str) {
            $query = $em->createQueryBuilder();
            $query->select('pc.categoryname')
                    ->from('AppDatabaseMainBundle:ProductCategoriesDnaPlus','pc')
                    ->add('where', $query->expr()->in('pc.id', ':categoriesId'))
                    ->setParameter('categoriesId', $categoriesId);
            $queryResult = $query->getQuery()->getArrayResult();
            $categoryName = [];
            foreach ($queryResult as $qr) {
                $categoryName[]=$qr['categoryname'];
            }
            return $categoryName;
        }
        return null;
    }

    private function getProductsName($em, $pendingOrder){
        // get all products info in categories
        $categoriesId_str = $pendingOrder->getOrderedCategories();
        $categoriesId = explode(',', $categoriesId_str);
        $productsCategories = [];
        if ($categoriesId_str) {
            $query = $em->createQueryBuilder();
            $query->select('cp.productid')
                    ->from('AppDatabaseMainBundle:CatProdLink', 'cp')
                    ->add('where', $query->expr()->in('cp.categoryid', ':categoriesId'))
                    ->setParameter('categoriesId', $categoriesId);
            $queryResult = $query->getQuery()->getArrayResult();
            $productsCategories = [];
            foreach ($queryResult as $qr) {
                $productsCategories[] = $qr['productid'];
            }
            // get unique array
            $productsCategories = array_unique($productsCategories, SORT_REGULAR);
        }

        // find product exist in list pending ordered product but don't exist in category product
        $productsId_str = $pendingOrder->getOrderedProducts();
        $productsId = explode(',', $productsId_str);
        $tmp = array_merge($productsCategories, $productsId);
        $productsId = array_unique($tmp, SORT_REGULAR);

        // get all products info provided in pending order
        $products = [];
        $productsName = [];
        if(!empty($productsId)){
            $query = $em->createQueryBuilder();
            $query->select('p.id, p.name')
                    ->from('AppDatabaseMainBundle:ProductsDnaPlus','p')
                    ->add('where', $query->expr()->in('p.id', ':productsId'))
                    ->setParameter('productsId', $productsId);
            $queryResult = $query->getQuery()->getArrayResult();
            foreach ($queryResult as $qr) {
                $products[$qr['id']] = $qr['name'];
                $productsName[] = $qr['name'];
            }
        }

        // return value
        return $productsName;
    }

    public function handleDemoFunction($controller, &$data, $options=array())
    {
        $array = [137, 136, 128, 126, 130, 131, 135, 123, 140, 132, 125, 139, 134, 124, 138, 133, 129, 127, 184, 109, 143];
        $em = $controller->get('doctrine')->getEntityManager();
        // print_r($this->getProductsName($em, $array));
        die;
        exit;
    }
}
