<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class DemoBank extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handlePreview($controller, &$data, $options = array()) {
    	$mapping = $controller->get('easy_mapping');
    	$em = $controller->get('doctrine')->getEntityManager();
    	$mgr = $controller->get('easy_module.manager');

        $data['mainTabs'] = array(
            'content' => array(
            	'list'		=> array('title' => 'DemoBank', 'link' => $mgr->getUrl('leep_admin', 'demo_bank', 'grid', 'list')),
            	'create'	=> array('title' => 'Add new DemoBank', 'link' => $mgr->getUrl('leep_admin', 'demo_bank', 'create', 'create')),
                'preview' 	=> array('title' => 'Preview', 'link' => '#')
            ),
            'selected' => 'preview'
        );

        $arrGenePoints = array();
        $result = $em->getRepository('AppDatabaseMainBundle:DemoBank')->findAll();
        if ($result) {
        	foreach ($result as $entry) {
        		if (!array_key_exists($entry->getIdGene(), $arrGenePoints)) {
        			$arrGenePoints[$entry->getIdGene()] = array();
        		}

        		$arrGenePoints[$entry->getIdGene()][] = array($entry->getFam(), $entry->getVic());
        	}
        }

        $arrGenes = array();

        if (sizeof($arrGenePoints) > 0) {
	        $queryGeneList= $em->createQueryBuilder();
	        $result = $queryGeneList->select('p.idGene, g.genename')
				        			->from('AppDatabaseMainBundle:DemoBank', 'p')
				        			->innerJoin('AppDatabaseMainBundle:Genes', 'g', 'WITH', 'g.id = p.idGene')
				        			->andWhere('p.idGene IN (:genes)')
				        			->setParameter('genes', array_keys($arrGenePoints))
				        			->getQuery()
				        			->getResult();

			if ($result) {
				foreach ($result as $entry) {
					$arrGenes[$entry['idGene']] = $entry['genename'];
				}
			}
        }

    	$data['genePoints'] = json_encode($arrGenePoints);
        $data['geneList'] = json_encode($arrGenes);

    	return $controller->render('LeepAdminBundle:DemoBank:preview.html.twig', $data);
    }
}
