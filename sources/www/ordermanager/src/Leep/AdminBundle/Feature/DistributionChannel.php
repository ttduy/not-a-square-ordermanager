<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\Rating\Utils as RatingUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DistributionChannel extends AbstractFeature {
    //public $AMINO_ID  = array('T/T', 'C/C', 'T/C');
    public $aminoCollection = [];
    // public 
    const DC_ID     = 1540;

    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleGeneratePassword($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        // 8 digit-password
        $newPassword = $helperCommon->generateRandPasscode(8);

        return new Response($newPassword);
    }

    public function handleGenerateUsername($controller, &$data, $options = array()) {
        $helperCommon = $controller->get('leep_admin.helper.common');

        $inputName = $controller->get('request')->query->get('name');

        $newName = $helperCommon->generateUsername($inputName);

        return new Response($newName);
    }

    public function handleAjaxRead($controller, &$data, $options = array()) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);

        $dc = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
        if ($dc) {
            $arr = $controller->get('leep_admin.helper.common')->copyEntityToArray($dc);
            $arr['reportGoesToName'] = $formatter->format($dc->getReportGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportGoesTo');

            $resp = new Response();
            $resp->setContent(json_encode($arr));
            return $resp;
        }
        return new Response('FAIL');
    }

    protected function buildGoogleMapTabs($controller, &$data, $selected) {
        $mgr = $this->container->get('easy_module.manager');
        // Set main tabs
        $data['mainTabs'] = array(
            'content' => array(
                'distributionChannel'     => array('title' => 'Distribution channels',     'link' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'googleMap')),
                'countryNote'             => array('title' => 'Country notes',             'link' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'googleMapCountryNote')),
            ),
            'selected' => $selected
        );
    }

    public function handleGoogleMap($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $doctrine = $controller->get('doctrine');
        $this->buildGoogleMapTabs($controller, $data, 'distributionChannel');

        // Load dc marker
        $imageUrl = $controller->get('service_container')->getParameter('attachment_url').'/../upload/distribution_channel_type_flag';
        $dcMarker = array();
        $dcTypes = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannelType')->findAll();
        foreach ($results as $r) {
            if (trim($r->getFlagIcon()) == '') {
                $dcMarker[$r->getId()] = $imageUrl.'/default.png';
            }
            else {
                $dcMarker[$r->getId()] = $imageUrl.'/'.$r->getFlagIcon();
            }

            $dcTypes[$r->getId()] = array(
                'flag' => $dcMarker[$r->getId()],
                'name' => $r->getName()
            );
        }
        $dcMarker['default'] = $imageUrl.'/default.png';
        $dcMarker['customer'] = $imageUrl.'/customer.png';
        $dcMarker['rating'] = $imageUrl.'/star.png';
        $data['flags'] = $dcMarker;
        $data['dcTypes'] = $dcTypes;

        // Load distribution channel address
        $dcAddress = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        foreach ($results as $r) {
            $lat = $r->getLatitude();
            $long = $r->getLongitude();
            if (!empty($lat) && !empty($long)) {
                $ratingTitle = $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $r->getRating());
                $dcAddress[] = array(
                    'lat' => $lat,
                    'long' => $long,
                    'flag' => isset($dcMarker[$r->getTypeId()]) ? $r->getTypeId() : 'default',
                    'idRating' => $r->getRating(),
                    'idType' => $r->getTypeId(),
                    'title' => str_replace('"', '\"', sprintf("%s %s %s(%s, %s %s, %s, %s, %s)",
                        ($ratingTitle ? $ratingTitle . ' -' : ''),
                        $r->getDistributionChannel(),
                        $r->getPartnerId() == 0 ? '' : (' / '.$formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List').' '),
                        $r->getStreet(),
                        $r->getCity(),
                        $r->getPostCode(),
                        $formatter->format($r->getCountryId(), 'mapping', 'LeepAdmin_Country_List'),
                        $r->getTelephone(),
                        $r->getContactemail()
                    ))
                );
            }
        }
        $data['dcAddress'] = $dcAddress;
        $data['dcAddressJson'] = str_replace("'", "\'",json_encode($dcAddress));

        // get Rating list
        $data['dcRatings'] = array(0 => 'All') + $mapping->getMapping('LeepAdmin_Rating_Levels');

        // Load customer address
        $customerAddress = array();
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('p.latitude, p.longitude, p.ordernumber, p.street, p.city, p.postcode, p.countryid')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $lat = $r['latitude'];
            $long = $r['longitude'];

            if (!empty($lat) && !empty($long)) {
                $customerAddress[] = array(
                    'lat' => $lat,
                    'long' => $long,
                    'flag' => 'customer',
                    'title' => str_replace('"', '\"', sprintf("%s (%s, %s %s, %s)",
                        $r['ordernumber'],
                        $r['street'],
                        $r['city'],
                        $r['postcode'],
                        $formatter->format($r['countryid'], 'mapping', 'LeepAdmin_Country_List')
                    ))
                );
            }
        }
        $data['customerAddress'] =  $customerAddress;
        $data['customerAddressJson'] = str_replace("'", "\'", json_encode($customerAddress));
    }

    public function handleGoogleMapCountryNote($controller, &$data, $options = array()) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $doctrine = $controller->get('doctrine');

        $this->buildGoogleMapTabs($controller, $data, 'countryNote');

        // Load country notes
        $countryNotes = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapCountryNote')->findAll();
        foreach ($results as $r) {
            $countryNotes[] = array(
                'country' => $r->getCountry(),
                'lat'     => $r->getLatitude(),
                'long'    => $r->getLongitude(),
                'isAvailable' => $r->getIsAvailable(),
                'note'    => $r->getNote(),
                'note2'   => $r->getNote2()
            );
        }
        $data['countryNotes'] = $countryNotes;

        // Load continent notes
        $continentNotes = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapContinentNote')->findAll();
        foreach ($results as $r) {
            $continentNotes[] = array(
                'id'          => $r->getId(),
                'continent'   => $r->getContinent(),
                'isAvailable' => $r->getIsAvailable(),
                'note'        => $r->getNote(),
                'note2'       => $r->getNote2()
            );
        }
        $data['continentNotes'] = $continentNotes;
    }

    public function handleCountryNoteCreate($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $countryNote = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapCountryNote')->findOneByCountry($request->get('country', ''));
        if ($countryNote) {
            return new Response($countryNote->getId());
        }

        $countryNote = new Entity\GoogleMapCountryNote();
        $countryNote->setCountry($request->get('country', ''));
        $countryNote->setLatitude($request->get('lat', 0));
        $countryNote->setLongitude($request->get('long', 0));
        $countryNote->setIsAvailable(true);
        $countryNote->setNote('');
        $em->persist($countryNote);
        $em->flush();

        return new Response($countryNote->getId());
    }

    public function handleCountryNoteUpdate($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $countryNote = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapCountryNote')->findOneByCountry($request->get('country', ''));
        if (!$countryNote) {
            return new Response('');
        }

        $countryNote->setIsAvailable($request->get('isAvailable'));
        $countryNote->setNote($request->get('note'));
        $countryNote->setNote2($request->get('note2'));
        $em->flush();

        return new Response('OK');
    }

    public function handleContinentNoteUpdate($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $continentNote = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapContinentNote')->findOneById($request->get('id', ''));
        if (!$continentNote) {
            return new Response('');
        }

        $continentNote->setIsAvailable($request->get('isAvailable'));
        $continentNote->setNote($request->get('note'));
        $continentNote->setNote2($request->get('note2'));
        $em->flush();

        return new Response('OK');
    }
    public function handleCountryNoteDelete($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $controller->get('request');

        $countryNote = $doctrine->getRepository('AppDatabaseMainBundle:GoogleMapCountryNote')->findOneByCountry($request->get('country', ''));
        if (!$countryNote) {
            return new Response('');
        }

        $em->remove($countryNote);
        $em->flush();

        return new Response('OK');
    }

    public function handleExportRatingCsv($controller, &$data, $options = []) {
        $request = $controller->get('request');
        $model = $request->get('form', null);

        $ratingLevelMinPrefix = 'ratingLevelMin_';
        $ratingLevelMaxPrefix = 'ratingLevelMax_';

        $ratingRules = array();
        foreach ($model as $k => $v) {
            if (strpos($k, $ratingLevelMinPrefix) === 0) {
                $arr = explode('_', $k);
                if (count($arr) == 2) {
                    $idRating = intval($arr[1]);
                    $ratingRules[$idRating]['min'] = $v;
                }
            }
            if (strpos($k, $ratingLevelMaxPrefix) === 0) {
                $arr = explode('_', $k);
                if (count($arr) == 2) {
                    $idRating = intval($arr[1]);
                    $ratingRules[$idRating]['max'] = $v;
                }
            }
        }

        if (!array_key_exists('isNoRating', $model)) {
            $model['isNoRating'] = false;
        }

        if (!array_key_exists('distributionChannel', $model)) {
            $model['distributionChannel'] = [];
        }

        if (!array_key_exists('partners', $model)) {
            $model['partners'] = [];
        }

        $startDate = \DateTime::createFromFormat('d/m/Y', $model['startDate']);
        $endDate = \DateTime::createFromFormat('d/m/Y', $model['endDate']);
        $ratingResult = RatingUtils::filter(
            $controller,
            $startDate,
            $endDate,
            $ratingRules,
            $model['type'],
            $model['isNoRating'],
            $model['distributionChannel'],
            $model['partners']
        );

        $content = '';
        $content .= '"Distribution channel"; "Total Order"; "Current Rating"; "New Rating"'."\r\n";
        foreach ($ratingResult as $result) {
            $row = [];
            $row[] = '"'.$result['name'].'"';
            $row[] = '"'.$result['totalOfOrders'].'"';
            $row[] = '"'.$result['currentRatingLabel'].'"';
            $row[] = '"'.$result['ratingLabel'].'"';

            $content .= implode(";", $row)."\r\n";
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=distributionChannels.csv');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }

    public function handleExportCsv($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $filterForm = $this->container->get('leep_admin.distribution_channel.business.filter_handler');
        $filters = $filterForm->getCurrentFilter();

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        Business\DistributionChannel\Utils::applyFilter($queryBuilder, $filters);

        $distributionChannels = $queryBuilder->getQuery()->getResult();

        $content = '';
        $content .= '"Distribution channel"; "Name"; "Address"; "Email"; "Type"; "Institution"; "Partner"'."\r\n";
        foreach ($distributionChannels as $dc) {
            $rows = array();
            $rows[] = '"'.addslashes($dc->getDistributionChannel()).'"';
            $rows[] = '"'.addslashes($dc->getFirstName().' '.$dc->getSurName()).'"';
            $address = sprintf("%s, %s %s, %s",
                $dc->getStreet(),
                $dc->getCity(),
                $dc->getPostCode(),
                $formatter->format($dc->getCountryId(), 'mapping', 'LeepAdmin_Country_List')
            );
            $rows[] = '"'.addslashes($address).'"';
            $rows[] = '"'.addslashes($dc->getContactEmail()).'"';
            $rows[] = '"'.addslashes($formatter->format($dc->getTypeId(), 'mapping', 'LeepAdmin_DistributionChannel_Type')).'"';
            $rows[] = '"'.addslashes($dc->getInstitution()).'"';
            $rows[] = '"'.addslashes($formatter->format($dc->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List')).'"';

            $content .= implode(";", $rows)."\r\n";
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=distributionChannels.csv');
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }

    public function handleViewComplaint($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'distribution_channel', 'grid',  'list');
        $request = $controller->get('request');

        $idWidget = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');

        // Filter handlers
        $filterHandler = $controller->get('leep_admin.distribution_channel.business.filter_handler');
        $sessionId = $filterHandler->getSessionId();
        $filterModel = $filterHandler->getDefaultFormModel();

        $filterModel->complaintStatus = $request->get('status', 0);

        // Update filters
        $session = $controller->get('request')->getSession();
        $session->set($sessionId, $filterModel);

        // Redirect
        return $controller->redirect($url);
    }

    public function handleWebLoginMassEmail($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $mgr = $controller->get('easy_module.manager');
        $em = $controller->get('doctrine')->getEntityManager();
        $selected = $controller->get('request')->get('selected', null);
        $selectedId = explode(',', $selected);

        $distributionChannels = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findById($selectedId);
        $data['dcList'] = [];
        foreach ($distributionChannels as $dc) {
            // Generate email template for each dc
            $arrInputData = array(
                'name'          =>  $dc->getFirstname() . ' ' . $dc->getSurname(),
                'usernamelogin' =>  $dc->getWebLoginUsername(),
                'passwordlogin' =>  $dc->getWebLoginPassword(),
                'idLanguage'    =>  $dc->getIdLanguage() == 0 ? 1 : $dc->getIdLanguage(),
                'type'          =>  2,
                'id'            =>  $dc->getId()
            );

            $emailTemplate = Business\EmailTemplate\Utils::getEmailTemplate($controller->get('service_container'), $arrInputData);
            $data['dcList'][] = [
                'id' =>             $dc->getId(),
                'name' =>           $dc->getDistributionChannel(),
                'username' =>       $dc->getWebLoginUsername(),
                'password' =>       $dc->getWebLoginPassword(),
                'sendTo' =>         trim($dc->getContactemail()),
                'language' =>       $dc->getIdLanguage() == 0 ? 1 : $dc->getIdLanguage(),
                'emailSubject' =>   $emailTemplate['subject'],
                'emailContent' =>   $emailTemplate['content']
            ];
        }

        $data['languages'] = $mapping->getMapping('LeepAdmin_Customer_Language');
        $data['emailMethods'] = $mapping->getMapping('LeepAdmin_EmailMethod_List');
        $data['selected'] = $selected;
        $data['massUpdateLanguageUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'massUpdateLanguage');
        $data['massWebLoginEmailSendUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'sendMassWebLoginEmail');
        $data['generateUsernameUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generateUsername');
        $data['generatePasswordUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generatePassword');
        $data['saveUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'saveUserNameAndPassword');

        if (isset($options['error'])) {
            $data['errorMsg'] = $options['error'];
        } else {
            $data['errorMsg'] = "";
        }

        if (isset($options['msg'])) {
            $data['otherMsg'] = $options['msg'];
        } else {
            $data['otherMsg'] = "";
        }

        return $controller->render('LeepAdminBundle:DistributionChannel:web_login_mass_email.html.twig', $data);
    }

    public function handleSaveUserNameAndPassword($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $username = $controller->get('request')->get('username', null);
        $password = $controller->get('request')->get('password', null);
        $helperCommon = $controller->get('leep_admin.helper.common');
        $id = $controller->get('request')->get('id', null);

        $receiver = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);

        // check username exist
        if ($username) {
            if ($helperCommon->isWebLoginUsernameExisted($username, 'DistributionChannels', $id)) {
                $options['error'] = "This Web Login Username " . $username . " for DC($id) is already existed";
            } else {
                $receiver->setWebLoginUsername($username);
            }
        } else {
            $receiver->setWebLoginUsername(null);
        }

        if ($password) {
            $receiver->setWebLoginPassword($password);
        } else {
            $receiver->setWebLoginPassword(null);
        }

        $em->persist($receiver);
        $em->flush();

        if (!isset($options['error'])) {
            $options['msg'] = "Success";
        }

        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleMassUpdateLanguage($controller, &$data, $options = array()) {
        $selected = $controller->get('request')->get('selected', null);
        $languageId = $controller->get('request')->get('language', null);
        $em = $controller->get('doctrine')->getEntityManager();
        $selectedId = explode(',', $selected);

        $distributionChannels = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findById($selectedId);
        foreach ($distributionChannels as $dc) {
            $dc->setIdLanguage($languageId);
            $em->persist($dc);
        }
        $em->flush();

        $options['msg'] = "Language updated";
        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleSendMassWebLoginEmail($controller, &$data, $options = array()) {
        $selected = $controller->get('request')->get('selected', null);
        $dcList = $controller->get('request')->get('dc', []);
        $em = $controller->get('doctrine')->getEntityManager();
        $isSendNow = $controller->get('request')->get('isSendNow', false);
        $isAutoRetry = $controller->get('request')->get('isAutoRetry', false);
        $selectedId = explode(',', $selected);

        foreach ($dcList as $id => $dc) {
            $emailOutgoing = new Entity\EmailOutgoing();
            $emailOutgoing->setCreationDatetime(new \DateTime());
            $emailOutgoing->setIdQueue(Business\EmailOutgoing\Constant::EMAIL_QUEUE_WEB_LOGIN_DC);
            $emailOutgoing->setIdEmailMethod($dc['emailMethod']);
            $emailOutgoing->setRecipients($dc['sendTo']);
            $emailOutgoing->setCc($dc['cc']);
            $emailOutgoing->setTitle($dc['emailSubject']);
            $emailOutgoing->setBody($dc['emailContent']);
            $emailOutgoing->setIsAutoRetry($isAutoRetry);
            $emailOutgoing->setIsSendNow($isSendNow);
            $emailOutgoing->setStatus(Business\EmailOutgoing\Constant::EMAIL_STATUS_OPEN);

            $em->persist($emailOutgoing);
        }

        $em->flush();

        $options['msg'] = "Email created";
        return $this->handleWebLoginMassEmail($controller, $data, $options);
    }

    public function handleLoginCustomerDashboard($controller, &$data, $options=array())
    {
        $idDistributionChannel = $controller->get('request')->get('id');
        $randomCode = sha1(uniqid());

        $em = $controller->get('doctrine')->getEntityManager();
        $tmpInfo = new Entity\TemperateLoginInfo();
        $tmpInfo->setRandCode($randomCode);
        $tmpInfo->setEntityId($idDistributionChannel);
        $tmpInfo->setClickTime(new \DateTime('now'));
        $em->persist($tmpInfo);
        $em->flush();

        $requestInfo_js =json_encode(['1', $randomCode]);

        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $linkToCustomerDashboard = $config->getCustomerDashboardLoginUrl();
        $urlLogin = $linkToCustomerDashboard."/login/dropin?requestinfo={$requestInfo_js}";
        return new RedirectResponse($urlLogin);
    }

    public function handleGetPartnerId($controller, &$data, $options=array())
    {
        $distributionChannelId = $controller->get('request')->get('dcId');

        $em = $controller->get('doctrine')->getEntityManager();
        $distributionChannel = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($distributionChannelId);
        $partnerId = $distributionChannel->getPartnerId();
        if (!empty($partnerId)) {
            return new Response(strval($partnerId));
        }
        return new Response(strval(''));
    }


    // // build statistics popup
    public function handleGeneStatistics($controller, &$data, $options=[]){

        $em = $controller->get('doctrine')->getEntityManager();                     //doctrine EM
        $geneId = $controller->get('request')->get('geneId', 0);                    //get distributionChannelId
        $dcId = $controller->get('request')->get('dcId', 0);

        // // get name of DC
        $dcName = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findBy(['id'  => $dcId]);

        $data['dcName'] = [];
        foreach ($dcName as $log) {
            $data['dcName'][] = [
                'dcName'  => $log->getDistributionChannel(),
            ];
        }

        // ******************************************
        // // count the customer belong to this DC
        $countIn = $em->createQueryBuilder();
        $countIn->select('count(p.idDistributionChannel) as num')
                    ->from("AppDatabaseMainBundle:CustomerInfo", 'p')
                    ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 'q', 'WITH', 'p.idDistributionChannel = q.id')
                    ->where('q.id = :id')
                    ->setParameter('id', $dcId);
        $resultIn = $countIn->getQuery()->getResult();

        $data['countIn'] = [];
        foreach ($resultIn as $r) {
            $data['countIn'][] = [
                 'countIn' => $r['num'],
            ];
        }
        // print_r($data['countIn']);
        // die;
        // // ************************************************
        // // total records
        $totalCount = $em->createQueryBuilder();
        $totalCount->select('count(p.idDistributionChannel) as total')
                    ->from("AppDatabaseMainBundle:CustomerInfo", 'p');
        $res = $totalCount->getQuery()->getResult();

        // // *********************************
        // // customer doesn't belong to distribution channel
        $data['countNotIn'] = $res[0]['total'] - $data['countIn'][0]['countIn'];

        // // *********************************
        // render gene name
        $data['geneName'] = $geneId;

        // // ****************
        // // get ID of gene
        $geneID = $em->getRepository('AppDatabaseMainBundle:Genes')->findBy(['genename'  => $geneId]);

        $data['geneID'] = [];
        foreach ($geneID as $log) {
            $data['geneID'][] = [
                'geneID'  => $log->getId(),
            ];
        }
        // // ************************************
        
        $id = $data['geneID'][0]['geneID'];
        
        // // find all customer with an DC has the gene X
        $customerGene = $em->createQueryBuilder();
        $customerGene->select('q.idCustomer')
                        ->from("AppDatabaseMainBundle:Genes", 'p')
                        ->innerJoin("AppDatabaseMainBundle:CustomerInfoGene", 'q', 'WITH', 'p.id = q.idGene')
                        ->innerJoin("AppDatabaseMainBundle:CustomerInfo", 'r', 'WITH', 'q.idCustomer = r.id')
                        ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 's', 'WITH', 's.id = r.idDistributionChannel')
                        ->where('q.idGene = :idGene')
                        ->andWhere('r.idDistributionChannel = :idDC')
                        ->setParameter('idGene', $id)
                        ->setParameter('idDC', $dcId);
        $results = $customerGene->getQuery()->getResult();
        
        // // total number of gene of DC
        $data['geneCountIn'] = count($results);
        
        // // *************************************
        // // total gene X
        $totalGene = $em->createQueryBuilder();
        $totalGene->select('q.idCustomer')
                        ->from("AppDatabaseMainBundle:Genes", 'p')
                        ->innerJoin("AppDatabaseMainBundle:CustomerInfoGene", 'q', 'WITH', 'p.id = q.idGene')
                        ->where('q.idGene = :idGene')
                        ->setParameter('idGene', $id);
        $result_ = $totalGene->getQuery()->getResult();

        // // rest total number
        $data['restTotalGene'] = count($result_) - $data['geneCountIn'];
        // print_r(count($result_));
        // die;                        

        // // get all amino axit from geneX
        $aminoCouple = $em->createQueryBuilder();
        $aminoCouple->select('DISTINCT p.result')
                        ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                        ->where('p.idGene = :idGene')
                        ->setParameter('idGene', $id);
        $couple = $aminoCouple->getQuery()->getResult();
        // print_r($couple);
        // die;
        $data['couple'] = [];
        foreach ($couple as $r) {
            $data['couple'][] = [
                'couple'    => $r['result'],
            ];
        }  

        // // DEBUG -> create amino Collection
        for($i = 0; $i < count($data['couple']); $i++){
            $this->aminoCollection[$i] = $data['couple'][$i]['couple'];
        }

        // // DEBUG

        $test_ = $em->createQueryBuilder();
        $test_->select('p.result, COUNT(p.id)')
                ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                ->innerJoin("AppDatabaseMainBundle:CustomerInfo", 'q', 'WITH', 'p.idCustomer = q.id')
                ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 'r', 'WITH', 'q.idDistributionChannel = r.id')
                ->innerJoin("AppDatabaseMainBundle:Genes", 's', 'WITH', 'p.idGene = s.id')
                ->where('p.idGene = :idGene')
                ->andWhere($test_->expr()->in('p.result', ':idAmino'))
                ->andWhere('q.idDistributionChannel =:idDC')
                ->addGroupBy('p.result')
                ->setParameter('idGene', $id)
                ->setParameter('idAmino', $this->aminoCollection)
                ->setParameter('idDC', $dcId);

        $res_ = $test_->getQuery()->getResult();


        $data['geneResult'] = [];
        foreach ($res_ as $r_) {
            $data['geneResult'][] = [
                'name'       => $r_['result'],
                'total'      => $r_[1],
                'percentage' => strval($r_[1]/$data['geneCountIn']*100)
            ];
        }
        
        // // customer of DC has C/C, T/T, T/C,... (without LAK1,...)
        $test__ = $em->createQueryBuilder();
        $test__->select('p.result', $test__->expr()->countDistinct('p.idCustomer'))
                ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                ->innerJoin("AppDatabaseMainBundle:Genes", 'q', 'WITH', 'q.id = p.idGene')
                ->innerJoin("AppDatabaseMainBundle:CustomerInfo", 'r', 'WITH', 'r.id = p.idCustomer')
                ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 's', 'WITH', 's.id = r.idDistributionChannel')
                ->where('q.id <>:idGene')
                ->andWhere('s.id =:idDC')
                ->andWhere($test__->expr()->in('p.result', ':idAmino'))
                ->addGroupBy('p.result')
                ->setParameter('idGene', $id)
                ->setParameter('idDC', $dcId)
                ->setParameter('idAmino', $this->aminoCollection);

        $res__ = $test__->getQuery()->getResult();

        $data['outGeneResult'] = [];
        foreach ($res__ as $r__) {
            $data['outGeneResult'][] = [
                'name'       => $r__['result'],
                'total'      => $r__[1],
                'percentage' => strval($r__[1]/(int)$data['countIn'][0]['countIn']*100),
            ];
        }

        // // ********************************
        // // >>>>>>> Part 2: not in current DC
        $test___ = $em->createQueryBuilder();
        $test___->select('p.result, COUNT(p.id)')
                ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                ->innerJoin("AppDatabaseMainBundle:CustomerInfo", 'q', 'WITH', 'p.idCustomer = q.id')
                ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 'r', 'WITH', 'q.idDistributionChannel = r.id')
                ->innerJoin("AppDatabaseMainBundle:Genes", 's', 'WITH', 'p.idGene = s.id')
                ->where('p.idGene = :idGene')
                ->andWhere($test___->expr()->in('p.result', ':idAmino'))
                ->andWhere('q.idDistributionChannel <>:idDC')
                ->addGroupBy('p.result')
                ->setParameter('idGene', $id)
                ->setParameter('idAmino', $this->aminoCollection)
                ->setParameter('idDC', $dcId);

        $res___ = $test___->getQuery()->getResult();


        $data['notDCGeneResult'] = [];
        foreach ($res___ as $r_) {
            $data['notDCGeneResult'][] = [
                'name'       => $r_['result'],
                'total'      => $r_[1],
                'percentage' => strval($r_[1]/$data['restTotalGene']*100)
            ];
        }

        $test____ = $em->createQueryBuilder();
        $test____->select('p.result', $test__->expr()->countDistinct('p.idCustomer'))
                ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                ->innerJoin("AppDatabaseMainBundle:Genes", 'q', 'WITH', 'q.id = p.idGene')
                ->innerJoin("AppDatabaseMainBundle:CustomerInfo", 'r', 'WITH', 'r.id = p.idCustomer')
                ->innerJoin("AppDatabaseMainBundle:DistributionChannels", 's', 'WITH', 's.id = r.idDistributionChannel')
                ->where('q.id <>:idGene')
                ->andWhere('s.id <>:idDC')
                ->andWhere($test__->expr()->in('p.result', ':idAmino'))
                ->addGroupBy('p.result')
                ->setParameter('idGene', $id)
                ->setParameter('idDC', $dcId)
                ->setParameter('idAmino', $this->aminoCollection);
        $res____ = $test____->getQuery()->getResult();

        $data['notDCNotGeneResult'] = [];
        foreach ($res____ as $r) {
            $data['notDCNotGeneResult'][] = [
                'name'       => $r['result'],
                'total'      => $r[1],
                'percentage' => strval($r[1]/$data['countNotIn']*100)
            ];
        }

        // // back url and dcID
        $mgr = $controller->get('easy_module.manager');
        $data['backUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'popUpDC');
        $data['dcId'] = $dcId;
        
        // // render to template
        $response = $controller->render('LeepAdminBundle:DistributionChannel:distribution_channel_statistics.html.twig', $data);
        return $response;
    }

    public function handlePopUpDC($controller, &$data, $options=[]){
        $em = $controller->get('doctrine')->getEntityManager();             //doctrine EM
        $dcId = $controller->get('request')->get('id', 0);                  //get geneId
        $mgr = $controller->get('easy_module.manager');

        $header = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findBy(['id'  => $dcId]);

        $data['header'] = [];
        foreach ($header as $log) {
            $data['header'][] = [
                'id'      => $log->getId(),        
                'header'  => $log->getDistributionChannel(),
            ];
        }
        
        $data['statisticUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'geneStatistics');
        $data['backUrl'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'popUpDC');
        $response = $controller->render('LeepAdminBundle:DistributionChannel:manage_gene.html.twig', $data);
        return $response;
    }
}