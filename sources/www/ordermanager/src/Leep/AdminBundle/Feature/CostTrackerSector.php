<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class CostTrackerSector extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleBulkDelete($controller, &$data, $options = array()) {
        $selectedId = $controller->get('request')->get('selectedId', 0);

        $msg = "SUCCESS";
        if ($selectedId) {
            $selectedId = explode(',', $selectedId);
            array_push($selectedId, 0);
            $em = $controller->get('doctrine')->getEntityManager();
            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:CostTrackerSector', 's')
                ->andWhere('s.id in (:selectedId)')
                ->setParameter(':selectedId', $selectedId)
                ->getQuery()
                ->execute();
            $em->flush();
        } else {
            $msg = "FAIL";
        }

        return new Response('SUCCESS');
    }

    public function handleUpdateIsFor($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();

        $idSector = $controller->get('request')->get('idSector', 0);
        $forWhat = $controller->get('request')->get('forWhat', 0);
        $newValue = $controller->get('request')->get('value', 0);

        $arrForWhat = array(
                        'costTracker'   => 'setIsForCostTracker',
                        'cashManager'   => 'setIsForCashManager',
                        'bookKeeper'    => 'setIsForBookKeeper',
                        'isInTotal'     => 'setIsInTotal',
                    );

        $msg = "FAIL";
        if ($idSector && array_key_exists($forWhat, $arrForWhat) && in_array($newValue, array(0, 1))) {
            $sector = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findOneById($idSector);

            if ($sector) {
                $newValue = ($newValue) ? $newValue : null;
                $sector->$arrForWhat[$forWhat]($newValue);
                $em->persist($sector);
                $em->flush();

                $msg = 'SUCCESS';
            }
        }

        return new Response($msg);
    }
}
