<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class BaseFeature extends AbstractFeature {
    public function getActions() { return []; }
    public function getDefaultOptions() { return []; }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
}
