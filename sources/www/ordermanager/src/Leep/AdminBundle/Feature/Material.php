<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Material extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handleTurnOffExpiryDateWarning($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');

        $idMaterial = $request->get('id', 0);
        $material = $em->getRepository('AppDatabaseMainBundle:Materials')->findOneById($idMaterial);

        $responseMsg = "FAIL";

        if ($material) {
            $material->setExpiryDateWarningDisabled(1);
            $em->flush();

            $responseMsg = "SUCCESS";
        }

        return new Response($responseMsg);
    }
    public function handleBulkEdit($controller, &$data, $options = array()) {
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $em = $controller->get('doctrine')->getEntityManager();

        $form = $controller->get('leep_admin.material.business.bulk_edit_stock_handler');
        $form->selectedId = $arr;
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit material',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => '' //$this->getModule()->getUrl($this->getName(), 'bulkEditStatus', array('orderNumbers' => $selectedIdRaw))
        );

        return $controller->render('LeepAdminBundle:Material:bulk_edit.html.twig', $data);
    }
}
