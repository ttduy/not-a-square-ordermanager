<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class EmailTemplate extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleAjaxRead($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);
        $idCustomer = $request->get('idCustomer', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');
        $container = $controller->get('service_container');
        $em = $controller->get('doctrine')->getEntityManager();

        $emailTemplate = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($id);
        $customer = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $orderedProducts = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($idCustomer);
        $productList = array();
        foreach ($orderedProducts as $p) {
            $productList[] = $formatter->format($p->getProductId(), 'mapping', 'LeepAdmin_Product_List');
        }

        $sampleArrivedDate = null;
        $sampleArrivedDateStatus = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedStatus')->findOneBy(array(
            'customerid' => $customer->getId(),
            'status' => 25 // Sample arrived
        ));
        if ($sampleArrivedDateStatus) {
            $sampleArrivedDate = $sampleArrivedDateStatus->getStatusDate();
        }

        $distributionChannelData = '';
        $dc = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        if ($dc) {
            $distributionChannelData = $dc->getFirstName().' '.$dc->getSurName().', '.$dc->getInstitution().', '.$dc->getStreet().' '.$dc->getPostCode().' '.$dc->getCity().', '.$dc->getContactEmail();
        }
        if ($emailTemplate) {
            $env = new \Twig_Environment(new \Twig_Loader_String(), array('autoescape' => false));
            $container = $controller->get('service_container');

            $query = $em->createQueryBuilder();
            $query->select('p.externalBarcode')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->eq('p.id', ':idCustomerInfo'))
                ->setParameter('idCustomerInfo', $customer->getIdCustomerInfo());
            $externalBarcode = $query->getQuery()->getSingleScalarResult();

            $data = array(
                'orderNumber' => $customer->getOrderNumber(),
                'externalBarcode' => $externalBarcode,
                'orderDate'   => $formatter->format($customer->getDateOrdered(), 'date'),
                'name'        => $customer->getFirstName().' '.$customer->getSurName(),
                'address'     => $customer->getStreet().' '.$customer->getPostCode().' '.$customer->getCity().', '.
                                $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List'),
                'productList' => implode(', ', $productList),
                'distributionChannel' => $distributionChannelData,
                'sampleArrivedDate' => empty($sampleArrivedDate) ? "(unknown)" : $sampleArrivedDate->format('d/m/Y'),
                'reportLink'  => $container->getParameter('public_report_link').'?key='.$customer->getShipmentKey(),
                'reportAccessCode'  => $customer->getShipmentCode(),
                'feedbackQuestionLink' => $container->getParameter('application_url').'/feedback_form',
                'DNCREPORTTYPE'  => $formatter->format($customer->getIdDncReportType(), 'mapping', 'LeepAdmin_Report_DncReportType')
            );

            $arr = array(
                'subject'    => $env->render($emailTemplate->getSubject(), $data),
                'content'    => $env->render($emailTemplate->getContent(), $data),
                'toCustomer' => $emailTemplate->getToCustomer(),
                'toDistributionChannel' => $emailTemplate->getToDistributionChannel(),
                'toPartner' => $emailTemplate->getToPartner(),
                'toOthers'  => $emailTemplate->getToOthers()
            );

            $resp = new Response();
            $resp->setContent(json_encode($arr));
            return $resp;
        }
        return new Response('FAIL');
    }

    public function handleAjaxReadIdEmailTemplate($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $status = $request->query->get('status', 0);
        $idCustomer = $request->query->get('idCustomer', 0);
        $em = $controller->get('doctrine')->getEntityManager();

        // DISTRIBUTION EMAIL TEMPLATE
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if ($customer) {
            $filters = array(
                'status' => $status,
                'distributionchannelid' => $customer->getDistributionChannelId()
            );
            $r = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findOneBy($filters);
            if ($r) {
                $arr = array('idEmailTemplate' => $r->getEmailTemplateId());
                $resp = new Response();
                $resp->setContent(json_encode($arr));
                return $resp;
            }
        }
        return new Response('FAIL');
    }
    public function handleUpdateTemplate($controller, &$data, $options = array()) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $templates = $em->getRepository('AppDatabaseMainBundle:EmailTemplate')->findBy(['isResaveContent' => 0], ['id' => 'ASC']);
        $content = [];
        foreach ($templates as $key => $template) {
            $content[] = [
                'id'    => $template->getId(),
                'content' => $template->getContent()
            ];
        }
        $data['total'] = count($templates);
        $data['content'] = json_encode($content);
        return $controller->render('LeepAdminBundle:EmailTemplates:update_template.html.twig', $data);
    }
    public function handleAjaxUpdateTemplate($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $content = $request->request->get('content');
        $id = $request->request->get('id');
        $em = $controller->get('doctrine')->getEntityManager();
        $flag = 'FALSE';
        $emailTemplate = $em->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($id);
        if($emailTemplate) {
            $emailTemplate->setContentWithParagraph($content);
            $emailTemplate->setIsResaveContent(1);
            $em->flush();
            $flag = 'TRUE';
        }

        return new Response($flag);
    }
}
