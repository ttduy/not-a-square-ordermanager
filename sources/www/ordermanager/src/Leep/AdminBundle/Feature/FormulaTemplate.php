<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class FormulaTemplate extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleExecute($controller, &$data, $options = array()) {
        $formula = $controller->get('request')->request->get('formula', '');
        $testInputValue = $controller->get('request')->request->get('testInputValue', '');

        $input = array();
        $rows = explode("\n", $testInputValue);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $input[trim($arr[0])] = trim($arr[1]);
            }
        }

        $executor = $controller->get('leep_admin.formula_template.business.executor');
        $executor->execute($formula, $input);
        $errors = $executor->errors;
        $output = $executor->output;

        $msg = '';
        if (!empty($errors)) {
            $msg .= implode("\n", $errors);

            $msg .= "\n".'------------------------'."\n";
        }
        foreach ($output as $k => $v) {
            $msg .= $k.' = '.$v."\n";
        }

        return new Response($msg);
    }

    public function handleShowDiffTool($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $idFormulaTemplate = $controller->get('request')->get('idFormulaTemplate', 0);
        $formulaTemplate = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($idFormulaTemplate);
        if (empty($formulaTemplate)) {
            die("ERROR!");
        }

        $data['idFormulaTemplate'] = $formulaTemplate->getId();
        $data['computeDiffUrl'] = $mgr->getUrl('leep_admin', 'formula_template', 'feature', 'computeDiff');
        $data['formulaTemplate'] = $formulaTemplate;

        return $controller->render('LeepAdminBundle:FormulaTemplate:show_diff_tool.html.twig', $data);
    }


    private function getFormulaTemplateByVersion($em, $formulaTemplate, $fromVersion) {
        if ($fromVersion == 'HEAD') {
            return $formulaTemplate->getFormula();
        }

        $version = $em->getRepository('AppDatabaseMainBundle:FormulaTemplateVersion')->findOneBy(array(
            'idFormulaTemplate' => $formulaTemplate->getId(),
            'number'            => $fromVersion
        ));
        if ($version) {
            return $version->getFormula();
        }

        $version = $em->getRepository('AppDatabaseMainBundle:FormulaTemplateVersion')->findOneBy(array(
            'idFormulaTemplate' => $formulaTemplate->getId(),
            'versionAlias'      => $fromVersion
        ));
        if ($version) {
            return $version->getFormula();
        }

        return '';
    }

    public function handleComputeDiff($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $idFormulaTemplate = $request->get('idFormulaTemplate', '');
        $fromVersion = trim($request->get('fromVersion', ''));
        $toVersion = trim($request->get('toVersion', ''));

        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($idFormulaTemplate);
        $fromTemplate = $this->getFormulaTemplateByVersion($em, $formulaTemplate, $fromVersion);
        $toTemplate = $this->getFormulaTemplateByVersion($em, $formulaTemplate, $toVersion);

        $errors = array();
        if (empty($fromTemplate)) {
            $errors[] = "Can't find version ".$fromVersion;
        }
        if (empty($toTemplate)) {
            $errors[] = "Can't find version ".$toVersion;
        }
        $data['errors'] = $errors;

        $data['task'] = 'Diff result: '.$formulaTemplate->getName().' - '.$fromVersion.' vs '.$toVersion;

        if (empty($errors)) {
            $from = explode("\n", $fromTemplate);
            $to = explode("\n", $toTemplate);

            $options = array(
                'ignoreWhitespace' => true
            );
            $diff = new \Actinarium\Diff\Diff($from, $to, $options);
            $renderer = new \Actinarium\Diff\Renderer\Html\SideBySideRenderer();
            $data['diff'] = $diff->Render($renderer);
        }

        return $controller->render('LeepAdminBundle:FormulaTemplate:diff_result.html.twig', $data);
    }
}
