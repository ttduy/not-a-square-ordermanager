<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class CustomerImport extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleElfinder($controller, &$data, $options = array()) {
        $data['processAttachmentUrl'] = $this->getModule()->getUrl($this->getName(), 'process');
    }

    private function getProductGenes($controller) {
        $products = array();
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ProdGeneLink')->findAll();
        foreach ($result as $row) {
            if (!isset($products[$row->getProductId()])) {
                $products[$row->getProductId()] = array();
            }
            $products[$row->getProductId()][] = $row->getGeneId();
        }

        return $products;
    }

    private function getCategoryProducts($controller) {
        $categories = array();
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CatProdLink')->findAll();
        foreach ($result as $row) {
            if (!isset($categories[$row->getCategoryId()])) {
                $categories[$row->getCategoryId()] = array();
            }
            $categories[$row->getCategoryId()][] = $row->getProductId();
        }
        return $categories;
    }

    public function handleConfig($controller, &$data, $options = array()) {
        $form = $controller->get('leep_admin.customer.business.import_config_edit_handler');
        $form->execute();
        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Import Config',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'config', array())
        );
    }

    public $distributionChannelArr = null;
    public function fillDistributionChannelInformation($controller, $customer) {
        if ($this->distributionChannelArr == null) {
            $this->distributionChannelArr = array();

            $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
            foreach ($result as $r) {
                $domain = trim($r->getDomain());
                if (!empty($domain)) {
                    if ($r->getIsDeleted() == 0) {
                        $this->distributionChannelArr[$domain] = $r;
                    }
                }
            }
        }

        $customer->setDistributionChannelId(0);
        $domain = trim($customer->getDomain());
        if (!empty($domain) && isset($this->distributionChannelArr[$domain])) {
            $dc = $this->distributionChannelArr[$domain];

            $customer->setDistributionChannelId($dc->getId());
            $customer->setCompanyInfo($dc->getCompanyInfo());
            $customer->setLaboratoryInfo($dc->getLaboratoryInfo());
            $customer->setContactInfo($dc->getContactInfo());
            $customer->setLetterExtraText($dc->getLetterExtraText());
            $customer->setText7($dc->getText7());
            $customer->setText8($dc->getText8());
            $customer->setText9($dc->getText9());
            $customer->setText10($dc->getText10());
            $customer->setText11($dc->getText11());

            $customer->setDoctorsReporting($dc->getDoctorsReporting());
            $customer->setAutomaticReporting($dc->getAutomaticReporting());
            $customer->setNoReporting($dc->getNoReporting());
            $customer->setInvoicingAndPaymentInfo($dc->getInvoicingAndPaymentInfo());

            $customer->setPriceCategoryId($dc->getPriceCategoryId());
            $customer->setReportDeliveryEmail($dc->getReportDeliveryEmail());
            $customer->setInvoiceDeliveryEmail($dc->getInvoiceDeliveryEmail());
            $customer->setReportGoesToId($dc->getReportGoesToId());
            $customer->setReportDeliveryId($dc->getReportDeliveryId());
            $customer->setFtpServerName($dc->getFtpServerName());
            $customer->setFtpUsername($dc->getFtpUsername());
            $customer->setFtpPassword($dc->getFtpPassword());

            $customer->setRebrandingNick($dc->getRebrandingNick());
            $customer->setHeaderFileName($dc->getHeaderFileName());
            $customer->setFooterFileName($dc->getFooterFileName());
            $customer->setTitleLogoFileName($dc->getTitleLogoFileName());
            $customer->setBoxLogoFileName($dc->getBoxLogoFileName());

        }
    }

    public function handleImport($controller, $data, $options = array()) {
        $file = Business\Customer\Util::getImportFile($this->container);

        if ($file != '') {
            @ini_set('memory_limit', '1024M');        
            @ini_set('max_execution_time', '10000');

            $validator = $this->container->get('leep_admin.customer.business.import_validator');
            $validator->initialize();

            $em = $this->container->get('doctrine')->getEntityManager();

            $categoryProducts = $this->getCategoryProducts($controller);
            $productGenes = $this->getProductGenes($controller);

            $helperCommon = $this->container->get('leep_admin.helper.common');

            $noSuccess = 0;
            $noFailed = 0;
            $raw = array();
            $failedData = '';
            $result = Business\Customer\Util::parseCvs($this->container, $file, $raw);
            $failedData .= $raw[0];
            $this->data = array();
            $i = 1;
            foreach ($result as $row) {
                $rawRow = $row;
                $isValid = $validator->validate($row);
                
                if ($isValid) {
                    $pRow = $validator->process($rawRow);

                    $key = $helperCommon->generateRandAttachmentDir();

                    $customer = new Entity\Customer();
                    $customer->setOrderNumber($pRow['idCustomerInfo']);
                    $customer->setOrderNumber($pRow['orderNumber']);                    
                    $customer->setDomain($pRow['domain']);
                    $customer->setDateOrdered($pRow['dateOrdered']);
                    $customer->setStatus(23);
                    $customer->setGenderId($pRow['gender']);
                    $customer->setDateOfBirth($pRow['dateOfBirth']);
                    $customer->setTitle($pRow['title']);
                    $customer->setFirstName($pRow['firstName']);
                    $customer->setSurName($pRow['surName']);
                    $customer->setStreet($pRow['street']);
                    $customer->setStreet2($pRow['street2']);
                    $customer->setPostCode($pRow['postCode']);
                    $customer->setCity($pRow['city']);
                    $customer->setCountryId($pRow['country']);
                    $customer->setEmail($pRow['email']);
                    $customer->setTelephone($pRow['telephone']);
                    $customer->setFax('');
                    $customer->setNotes('');
                    $customer->setAttachmentKey($key);
                    $customer->setIsDoubleChecked(1);
                    $this->fillDistributionChannelInformation($controller, $customer);

                    $em->persist($customer);
                    $em->flush();

                    $orderedStatus = new Entity\OrderedStatus();
                    $orderedStatus->setCustomerId($customer->getId());
                    $orderedStatus->setStatusDate(new \DateTime());
                    $orderedStatus->setStatus(23);
                    $orderedStatus->setIsCompleted(0);
                    $orderedStatus->setSortOrder(1);
                    $em->persist($orderedStatus);

                    $geneSelected = array();

                    foreach ($pRow['products'] as $productId) {
                        $orderProduct = new Entity\OrderedProduct();
                        $orderProduct->setCustomerId($customer->getId());
                        $orderProduct->setProductId($productId);
                        $em->persist($orderProduct);

                        foreach ($productGenes[$productId] as $geneId) {
                            $geneSelected[$geneId] = 1;
                        }
                    }

                    foreach ($pRow['categories'] as $categoryId) {
                        $orderCategory = new Entity\OrderedCategory();
                        $orderCategory->setCustomerId($customer->getId());
                        $orderCategory->setCategoryId($categoryId);
                        $em->persist($orderCategory);

                        if (isset($categoryProducts[$categoryId])) {
                            foreach ($categoryProducts[$categoryId] as $productId) {
                                if (isset($productGenes[$productId])) {
                                    foreach ($productGenes[$productId] as $geneId) {
                                        $geneSelected[$geneId] = 1;
                                    }    
                                }
                            }
                        }
                    }

                    /*
                    WAITING FOR CONFIRMATION ABOUT THIS FEATURE - CUSTOMER (ORDER) IMPORT
                    foreach ($geneSelected as $geneId => $t) {
                        $geneResult = new Entity\CustomerInfoGene();
                        $geneResult->setIdCustomer(?);
                        $geneResult->setIdGene($geneId);
                        $geneResult->setResult('');
                        $em->persist($geneResult);
                    }*/
                    
                    $em->flush();

                    $noSuccess++;
                }
                else {
                    $failedData .= $raw[$i];
                    $noFailed++;
                }
                $i++;
            }

            if ($noFailed == 0) {
                @unlink($file);
            }
            else {
                file_put_contents($file, $failedData);
            }
        }
        $controller->get('session')->setFlash('success', $noSuccess);
        $controller->get('session')->setFlash('failed', $noFailed);
        return $controller->redirect($this->getModule()->getUrl($this->getName(), 'list'));
    }

    public function handleList($controller, &$data, $options = array()) {
        $request = $controller->getRequest();
        $session = $controller->get('session');
        if ($session->hasFlash('success') && $session->hasFlash('failed')) {
            $noSuccess = $session->getFlash('success');
            $noFailed = $session->getFlash('failed');
            $data['messages'] = array("Import finished! ".$noSuccess." has been added successfully, ".$noFailed.' failed');
            $session->removeFlash('success');
            $session->removeFlash('failed');
        }


        $data['attachmentUrl'] = $this->getModule()->getUrl($this->getName(), 'elfinder');

        $data['importUrl'] = $this->getModule()->getUrl($this->getName(), 'import');
        $data['refreshUrl'] = $this->getModule()->getUrl($this->getName(), 'list');
        $data['configUrl']  = $this->getModule()->getUrl($this->getName(), 'config');

        $handler = $this->container->get('leep_admin.customer.business.import_grid_reader');
        $data['grid'] = array(
            'id' => 'ImportGrid',
            'title' => 'Data review',
            'header' => $handler->getTableHeader(),
            'ajaxSource' => $this->getModule()->getUrl($this->getName(), 'ajaxSource', $controller->get('request')->query->all())
        );
    }

    public function handleAjaxSource($controller, &$data, $options = array()) {
        $handler = $this->container->get('leep_admin.customer.business.import_grid_reader');
        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($handler);

        return new Response(json_encode($resp));
    }

    public function handleProcess($controller, &$data, $options = array()) {        
        $dir = $this->container->getParameter('attachment_dir').DIRECTORY_SEPARATOR.'import';
        
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $opts = array(
            'root'            => $dir,
            'URL'             => $this->container->getParameter('attachment_url').'/import/',
            'rootAlias'       => 'Import',      
        );
        $fm = new \Leep\AdminBundle\Business\Base\elFinder($opts); 
        
        ob_start();
        $fm->run();
        $content = ob_get_contents();
        
        return new Response($content);  
    }
}
