<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;

class Ipn extends AbstractFeature {
    public function getActions() { return array('list'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleRead($controller, &$data, $options = array()) {    
        $content = $controller->get('request')->getContent();
        $em = $controller->get('doctrine')->getEntityManager();

        $dir = $controller->get('service_container')->getParameter('kernel.root_dir');
        $file = $dir.'/tmp/ipn.txt';
        $cacert = $dir.'/certs/cacert.pem';

        #file_put_contents($file, var_export($content, true));       

        $ipnLog = new Entity\IpnLog();
        $ipnLog->setTimestamp(new \DateTime());
        $ipnLog->setMessage($content);
        $em->persist($ipnLog);
        $em->flush();
        
        $arr = explode('&', $content);
        $post = array();
        foreach ($arr as $keyval) {
            $a = explode ('=', $keyval);
            if (count($a) == 2) {
                $post[$a[0]] = urldecode($a[1]);
            }
        }

        $paypalUrl = 'https://www.paypal.com/cgi-bin/webscr';
        if (isset($post['test_ipn']) && $post['test_ipn']) {
            $paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        }

        $req = 'cmd=_notify-validate';
        foreach ($post as $k => $v) {
            $req.= "&".$k.'='.urlencode($v);
        }

        $ch = curl_init($paypalUrl);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        curl_setopt($ch, CURLOPT_CAINFO, $cacert);

        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) {
            $ipnLog->setIsValid(0);
            $em->flush();

            curl_close($ch);
            die();
        } else {            
            curl_close($ch);
        }

        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));
        if (strcmp ($res, "VERIFIED") == 0) {
            $ipnLog->setIsValid(1);
            $em->flush();

            $amount = floatval($post['mc_gross']);

            // Update entry
            $orderCode = trim($post['custom']);
            $cmsOrder = $em->getRepository('AppDatabaseMainBundle:CmsOrder')->findOneByOrderCode($orderCode);
            if ($cmsOrder) {
                $paypalData = array();
                foreach ($post as $k => $v) {
                    $paypalData[] = $k.' = '.$v;
                }

                $cmsOrderPayment = new Entity\CmsOrderPayment();
                $cmsOrderPayment->setIdCmsOrder($cmsOrder->getId());
                $cmsOrderPayment->setTimestamp(new \DateTime());
                $cmsOrderPayment->setAmount($amount);
                $cmsOrderPayment->setIdMethod(Business\CmsOrderPayment\Constant::CMS_PAYMENT_METHOD_PAYPAL);
                $cmsOrderPayment->setData(implode("\n", $paypalData));
                $em->persist($cmsOrderPayment);
                $em->flush();

                Business\CmsOrderPayment\Utils::updateCmsOrder($controller, $cmsOrder->getId());
            }
        }

        $response = new Response($content);
        return $response;
    }
}
