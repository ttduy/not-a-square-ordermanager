<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class TodoContinualImprovement extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleSetStatus($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $idTask = $controller->get('request')->get('id', 0);
        $idStatus = $controller->get('request')->get('status', 0);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $arrStatus = $mapping->getMapping('LeepAdmin_TodoGoal_Status');
        $response = "Done";
        if (array_key_exists($idStatus, $arrStatus)) {
            $task = $doctrine->getRepository('AppDatabaseMainBundle:TodoContinualImprovement')->findOneById($idTask);
            $task->setStatus($idStatus);
            $em->persist($task);
            $em->flush();            
        } else {
            $response = "Invalid Status";
        }
        
        return new Response($response);
    }
}
