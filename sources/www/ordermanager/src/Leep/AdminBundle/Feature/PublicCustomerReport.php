<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class PublicCustomerReport extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleCustomerDisableAccessKey($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $common = $controller->get('leep_admin.helper.common');

        $sessionHelper = $controller->get('leep_admin.helper.session_extra');

        $sessionKey = $sessionHelper->getSessionKeyFromRequest();
        $idCustomer = $sessionHelper->get('idCustomer', $sessionKey);

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if (empty($customer)) {
            return $this->sendErrorPage($controller, $data);
        }
        $customer->setShipmentKey('');
        $customer->setShipmentCode('');
        $em->flush();

        $session = $request->getSession();
        $session->start();
        $session->remove('_customer_report');
        $session->save();

        return $this->sendMessagePage($controller, $data, "The link has been disabled");
    }

    public function handleAccessReport($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $sessionHelper = $controller->get('leep_admin.helper.session_extra');
        $mgr = $controller->get('easy_module.manager');
        $errors = array();

        if ($request->getMethod() == 'POST') {
            // Validation
            $key = $request->get('key', FALSE);
            $orderNumber = trim($request->get('orderNumber', FALSE));
            $passcode = trim($request->get('passcode', ''));

            if (empty($key) || empty($orderNumber) || ($passcode === '')) {
                $errors[] = "Login failed";
            }
            else {
                // Process login
                $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($orderNumber);
                if (empty($customer)) {
                    $errors[] = "Order number ".$orderNumber." doesn't exist";
                }
                else {
                    if ($customer->getShipmentKey() === '') {
                        $errors[] = "The link has been disabled! Please contact us for access";
                    }
                    else {
                        if ($customer->getShipmentCode() !== $passcode) {
                            $errors[] = "Login failed! Wrong passcode";
                        }
                        if ($customer->getShipmentKey() !== $key) {
                            $errors[] = "Login failed! Wrong key, please access via the link in the email";
                        }
                    }
                }

                // Check
                if (empty($errors)) {
                    // Login success
                    $key = $sessionHelper->generateNewSession();
                    $sessionHelper->save('idCustomer', $customer->getId(), $key);

                    $url = $controller->generateUrl('leep_admin_public_your_report', array('_msid' => $key));
                    return new RedirectResponse($url);
                }
            }
        }

        $data['key'] = $request->get('key', '');
        $data['errors'] = implode("<br/>", $errors);

        return $controller->render('LeepAdminBundle:CustomerReport:access_report.html.twig', $data);
    }

    public function handleViewYourReport($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $sessionHelper = $controller->get('leep_admin.helper.session_extra');

        $sessionKey = $sessionHelper->getSessionKeyFromRequest();
        $idCustomer = $sessionHelper->get('idCustomer', $sessionKey);

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if (!$customer) {
            return $this->sendErrorPage($controller, $data);
        }
        if ($customer->getShipmentKey() == '') {
            return $this->sendErrorPage($controller, $data);
        }

        // DC
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        $idReportNameType = empty($dc) ? 0 : $dc->getIdReportNameType();

        // Information
        $data['customerOrderNumber'] = $customer->getOrderNumber();
        $data['customerName'] = $customer->getSurName().' '.$customer->getFirstName();
        $data['disableLink'] = $controller->generateUrl('leep_admin_public_disable_access_link', array('_msid' => $sessionKey));

        // Report
        $reports = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerReport', 'p')
            ->andWhere('p.idCustomer = :idCustomer')
            ->andWhere('p.isVisibleToCustomer = :isVisibleToCustomer')
            ->orderBy('p.inputTime', 'DESC')
            ->setParameter('isVisibleToCustomer', 1)
            ->setParameter('idCustomer', $customer->getId());
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($r->getIdReport());

            $reportName = $customer->getOrderNumber().' - '.$report->getName();
            if ($idReportNameType == Business\DistributionChannel\Constant::REPORT_NAME_TYPE_WITH_CUSTOMER_NAME) {
                $language = $formatter->format($r->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List');
                $reportName = $customer->getSurName().' '.$customer->getFirstName().' - '.$reportName.' - '.$language;
            }

            $reports[] = array(
                'time' => $formatter->format($r->getInputTime(), 'datetime'),
                'name' => $reportName,
                'downloadLink' => $controller->generateUrl('leep_admin_public_download_report', array(
                    'id'  => $r->getId(),
                    '_msid' => $sessionKey
                ))
            );

            if ($r->getFoodTableExcelFilename() != '') {
                $reports[] = array(
                    'time' => $formatter->format($r->getInputTime(), 'datetime'),
                    'name' => $reportName. ' (Food table excel)',
                    'downloadLink' => $controller->generateUrl('leep_admin_public_download_report', array(
                        'id'  => $r->getId(),
                        'isExcel' => true,
                        '_msid' => $sessionKey
                    ))
                );
            }

            if ($r->getXmlFilename() != '') {
                $reports[] = array(
                    'time' => $formatter->format($r->getInputTime(), 'datetime'),
                    'name' => $reportName. ' (XML)',
                    'downloadLink' => $controller->generateUrl('leep_admin_public_download_report', array(
                        'id'  => $r->getId(),
                        'isXml' => true,
                        '_msid' => $sessionKey
                    ))
                );

            }
        }
        $data['reports'] = $reports;

        return $controller->render('LeepAdminBundle:CustomerReport:view_your_report.html.twig', $data);
    }

    public function handleDownloadReport($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $reportHelper = $controller->get('leep_admin.helper.report_helper');
        $em = $controller->get('doctrine')->getEntityManager();
        $sessionHelper = $controller->get('leep_admin.helper.session_extra');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $isExcel = $request->get('isExcel', 0);
        $isXml = $request->get('isXml', 0);

        $sessionKey = $sessionHelper->getSessionKeyFromRequest();
        $idCustomer = $sessionHelper->get('idCustomer', $sessionKey);
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);

        // DC
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        $idReportNameType = empty($dc) ? 0 : $dc->getIdReportNameType();

        // Report
        $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($request->get('id', 0));
        if ((!$customer) || (!$customerReport) || ($customerReport->getIsVisibleToCustomer() != 1) || ($customerReport->getIdCustomer() != $customer->getId())) {
            return $this->sendErrorPage($controller, $data);
        }

        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($customerReport->getIdReport());
        $reportName = $customer->getOrderNumber().' - '.$report->getName();
        if ($idReportNameType == Business\DistributionChannel\Constant::REPORT_NAME_TYPE_WITH_CUSTOMER_NAME) {
            $language = $formatter->format($customerReport->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List');
            $reportName = $customer->getSurName().' '.$customer->getFirstName().' - '.$reportName.' - '.$language;
        }

        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report/';
        $tempDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';

        if ($isExcel) {
            $filename = $customerReport->getFoodTableExcelFilename();
            $downloadFile = $fileDir.$filename;
            if (!is_file($downloadFile)) {
                $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                if ($result) {
                    $downloadFile = "$tempDir/$filename";
                }
            }

            $fileResponse = new BinaryFileResponse($downloadFile);
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xlsx');
            return $fileResponse;
        }

        if ($isXml) {
            $filename = $customerReport->getXmlFilename();
            $downloadFile = $fileDir.$filename;
            if (!is_file($downloadFile)) {
                $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                if ($result) {
                    $downloadFile = "$tempDir/$filename";
                }
            }

            $fileResponse = new BinaryFileResponse($downloadFile);
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xml');
            return $fileResponse;
        }

        $pdfFiles = array(
            $customerReport->getWebCoverFilename(),
            $customerReport->getFilename()
        );
        if ($dc->getIsShowBodCoverPage() && ($customerReport->getBodCoverFilename() != '')) {
            $pdfFiles[] = $customerReport->getBodCoverFilename();
        }

        return $reportHelper->serveReport(
            $fileDir,
            $pdfFiles,
            $reportName
        );
    }

    protected function sendErrorPage($controller, $data) {
        return $controller->render('LeepAdminBundle:CustomerReport:error.html.twig', $data);
    }
    protected function sendMessagePage($controller, $data, $message) {
        $data['message'] = $message;
        return $controller->render('LeepAdminBundle:CustomerReport:message.html.twig', $data);
    }
}
