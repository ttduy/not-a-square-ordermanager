<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Easy\CrudBundle\Feature\Grid;

class CollectiveInvoice extends Grid {    
    public function handleList($controller, &$data, $options = array()) {
        parent::handleList($controller, $data, $options);

        $id = $controller->get('request')->query->get('id', 0);
        $mgr = $controller->get('easy_module.manager');
        $data['viewInvoiceUrl'] = $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_pdf',  'pdf',  array('id' => $id));
        $data['invoiceId'] = $id;
        $data['emailInvoiceUrl'] = $mgr->getUrl('leep_admin', 'collective_invoice', 'email',  'create',  array('id' => $id, 'mode' => 'collective_invoice'));
    }
}