<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class RemoveStock extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleRemoveStock($controller, &$data, $options = array()) {

        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get("easy_module.manager");
        $data['getSubtanceNameUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceName');
        $data['getSubtanceLoadUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceLoad');
        $data['getBatchIdUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getBatchId');
        $data['getGenoMaterialTypeUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getGenoMaterialType');
        $data['getAmountInStockUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getAmountInStock');
        $data['getLocationUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getLocation');
        $data['removeStockProcessUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'removeStockProcess');

        return $controller->render('LeepAdminBundle:RemoveStock:removeStock.html.twig', $data);
    }
}
