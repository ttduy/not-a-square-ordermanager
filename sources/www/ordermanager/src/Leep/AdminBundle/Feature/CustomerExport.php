<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class CustomerExport extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $content = '';
    public function add($line) {
        $this->content .= $line."\n";
    }

    private function formatAnswer($question, $answer) {
        if ($question->getControlTypeId() == Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX) {
            return $question->getQuestion().';'.$answer;
        }
        else if ($question->getControlTypeId() == Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO ||
                 $question->getControlTypeId() == Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX) {
            $r = '';
            $opt = explode(';', $question->getControlConfig());
            foreach ($opt as $k => $v) {
                $r .= $question->getQuestion().'?'.$v;
                if ($k == $answer) {
                    $r.= ';x';
                }
                else {
                    $r .= ";";
                }
                $r.= "\n";
            }
            return $r;
        }
        else if ($question->getControlTypeId() == Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE) {
            $answer = json_decode($answer, true);
            $r = '';
            $opt = explode(';', $question->getControlConfig());
            foreach ($opt as $k => $v) {
                $r .= $question->getQuestion().'?'.$v;
                if (in_array($k, $answer)) {
                    $r.= ';x';
                }
                else {
                    $r .= ";";
                }
                $r.= "\n";
            }
            return $r;
        }
    }

    public function length($v, $length) {
        //while (mb_strlen($v, 'UTF-8') < $length) $v.= ' ';
        return $v;
    }

    public function handleExport($controller, $data, $options = array()) {
        $orderNumber = $this->container->get('request')->query->get('orderNumber', '_');

        $formatter = $this->container->get('leep_admin.helper.formatter');

        $doctrine = $this->container->get('doctrine');
        $order = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneBy(array('ordernumber' => $orderNumber, 'isdeleted' => 0));
        if (!$order) {
            return new Response("Order number doesn't exist");
        }
        $idOrder = $order->getId();
        $idCustomer = $order->getIdCustomerInfo();
        $distributionChannel = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($order->getDistributionChannelId());
        $statusList = $doctrine->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBycustomerid($idOrder);
        $orderGenes = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findByIdCustomer($idCustomer);
        $orderQuestions = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CustomerQuestion')->findBycustomerid($idOrder);
        foreach ($result as $row) {
            $orderQuestions[$row->getQuestionId()] = $row->getAnswer();
        }

        $len = 21;
        $this->add("ORDER INFO");
        $this->add($this->length("Order Number", $len).";".$order->getOrderNumber());
        $this->add($this->length("Distribution Channel", $len).";".$formatter->format($order->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List'));
        $this->add($this->length("Domain", $len).";".$order->getDomain());
        $this->add($this->length("Date ordered", $len).";".$formatter->format($order->getDateOrdered(), 'date'));
        $this->add($this->length("Order age", $len).";".$order->getOrderAge());
        
        if (!empty($distributionChannel)) {
            $this->add("\nDISTRIBUTION CHANNEL");
            $this->add("DistributionChannel;".$distributionChannel->getDistributionChannel());
            $this->add("UID Number;".$distributionChannel->getUidNumber());
            $this->add("Type;".$formatter->format($distributionChannel->getTypeId(), 'mapping', 'LeepAdmin_DistributionChannel_Type'));
            $this->add("Price Category;".$formatter->format($distributionChannel->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List'));
            $this->add("Gender;".$formatter->format($distributionChannel->getGenderId(), 'mapping', 'LeepAdmin_Gender_List'));
            $this->add("Title;".$distributionChannel->getTitle());
            $this->add("First Name;".$distributionChannel->getFirstName());
            $this->add("Last Name;".$distributionChannel->getSurName());
            $this->add("Institution;".$distributionChannel->getInstitution());
            $this->add("Street;".$distributionChannel->getStreet());
            $this->add("Post Code;".$distributionChannel->getPostCode());
            $this->add("City;".$distributionChannel->getCity());
            $this->add("Country;".$formatter->format($distributionChannel->getCountryId(), 'mapping', 'LeepAdmin_Country_List'));
            $this->add("Contact Email;".$distributionChannel->getContactEmail());
            $this->add("Telephone;".$distributionChannel->getTelephone());
            $this->add("Fax;".$distributionChannel->getFax());

            $this->add("Rebranding Nick;".$distributionChannel->getRebrandingNick());
            $this->add("Header file name;".$distributionChannel->getHeaderFileName());
            $this->add("Footer file name;".$distributionChannel->getFooterFileName());
            $this->add("Title Logo file name;".$distributionChannel->getTitleLogoFileName());
            $this->add("Box Logo file name;".$distributionChannel->getBoxLogoFileName());            

            $this->add("Company Info;".$formatter->format($distributionChannel->getCompanyInfo(), 'single_line'));
            $this->add("Laboratory Info;".$formatter->format($distributionChannel->getLaboratoryInfo(), 'single_line'));
            $this->add("Contact Info;".$formatter->format($distributionChannel->getContactInfo(), 'single_line'));
            $this->add("Contact Us;".$formatter->format($distributionChannel->getContactUs(), 'single_line'));
            $this->add("Letter extra Text;".$distributionChannel->getLetterExtraText());
            $this->add("Clinician info Text;".$distributionChannel->getClinicianInfoText());
            $this->add("Text 7;".$distributionChannel->getText7());
            $this->add("Text 8;".$distributionChannel->getText8());
            $this->add("Text 9;".$distributionChannel->getText9());
            $this->add("Text 10;".$distributionChannel->getText10());
            $this->add("Text 11;".$distributionChannel->getText11());

            $this->add("Doctors Reporting;".$distributionChannel->getDoctorsReporting());
            $this->add("Automatic Reporting;".$distributionChannel->getAutomaticReporting());
            $this->add("No Reporting;".$distributionChannel->getNoReporting());
            $this->add("Invoicing and Payment info;".$formatter->format($distributionChannel->getInvoicingAndPaymentInfo(), 'single_line'));

            $this->add("Price Category;".$formatter->format($distributionChannel->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List'));
            $this->add("Report delivery Email;".$distributionChannel->getReportDeliveryEmail());
            $this->add("Invoice delivery Email;".$distributionChannel->getInvoiceDeliveryEmail());
            $this->add("Invoice goes to;".$formatter->format($distributionChannel->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo'));
            $this->add("Report goes to;".$formatter->format($distributionChannel->getReportGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportGoesTo'));
            $this->add("Report Delivery;".$formatter->format($distributionChannel->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery'));

            $this->add("Card type;".$formatter->format($distributionChannel->getCardTypeId(), 'mapping', 'LeepAdmin_CardType_List'));
            $this->add("Number;".$distributionChannel->getCardNumber());
            $this->add("Expiry Date;".$formatter->format($distributionChannel->getCardExpiryDate(), 'date'));
            $this->add("CVC Code;".$distributionChannel->getCardCvcCode());
            $this->add("Card First Name;".$distributionChannel->getCardFirstName());
            $this->add("Card Sur Name;".$distributionChannel->getCardSurName());
        }

        $len = 13;
        $this->add("\nCONTACT INFORMATION");
        $this->add($this->length("Gender", $len).";".$formatter->format($order->getGenderId(), 'mapping', 'LeepAdmin_Gender_List'));
        $this->add($this->length("Date of birth", $len).";".$formatter->format($order->getDateOfBirth(), 'date'));
        $this->add($this->length("Title", $len).";".$order->getTitle());
        $this->add($this->length("First Name", $len).";".$order->getFirstName());
        $this->add($this->length("Last Name", $len).";".$order->getSurName());

        $len = 10;
        $this->add("\nADDRESS");
        $this->add($this->length("Street", $len).";".$order->getStreet());
        $this->add($this->length("Street 2", $len).";".$order->getStreet2());
        $this->add($this->length("Post Code", $len).";".$order->getPostCode());
        $this->add($this->length("City", $len).";".$order->getCity());
        $this->add($this->length("Country", $len).";".$formatter->format($order->getCountryId(), 'mapping', 'LeepAdmin_Country_List'));
        $this->add($this->length("Email", $len).";".$order->getEmail());
        $this->add($this->length("Telephone", $len).";".$order->getTelephone());
        $this->add($this->length("Fax", $len).";".$order->getFax());
        $this->add($this->length("Notes", $len).";".$formatter->format($order->getNotes(), 'single_line'));

        $this->add("\nSETTINGS");
        $this->add("Language;".$formatter->format($order->getLanguageId(), 'mapping', 'LeepAdmin_Customer_Language'));

        $this->add("\nREPORT DETAILS");
        $this->add("Company Info;".$formatter->format($order->getCompanyInfo(), 'single_line'));
        $this->add("Laboratory Info;".$formatter->format($order->getLaboratoryInfo(), 'single_line'));
        $this->add("Contact Info;".$formatter->format($order->getContactInfo(), 'single_line'));
        $this->add("Contact Us;".$formatter->format($order->getContactUs(), 'single_line'));
        $this->add("Letter extra Text;".$order->getLetterExtraText());
        $this->add("Clinician info Text;".$order->getClinicianInfoText());
        $this->add("Text 7;".$order->getText7());
        $this->add("Text 8;".$order->getText8());
        $this->add("Text 9;".$order->getText9());
        $this->add("Text 10;".$order->getText10());
        $this->add("Text 11;".$order->getText11());

        $this->add("\nGS SETTINGS");
        $this->add("Doctors Reporting;".$order->getDoctorsReporting());
        $this->add("Automatic Reporting;".$order->getAutomaticReporting());
        $this->add("No Reporting;".$order->getNoReporting());
        $this->add("Invoicing and Payment info;".$formatter->format($order->getInvoicingAndPaymentInfo(), 'single_line'));

        $this->add("\nDELIVERY");
        $this->add("Price Category;".$formatter->format($order->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List'));
        $this->add("Report delivery Email;".$order->getReportDeliveryEmail());
        $this->add("Invoice delivery Email;".$order->getInvoiceDeliveryEmail());
        $this->add("Invoice goes to;".$formatter->format($order->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo'));
        $this->add("Report goes to;".$formatter->format($order->getReportGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportGoesTo'));
        $this->add("Report Delivery;".$formatter->format($order->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery'));

        $this->add("\nREBRANDER");
        $this->add("Rebranding Nick;".$order->getRebrandingNick());
        $this->add("Header file name;".$order->getHeaderFileName());
        $this->add("Footer file name;".$order->getFooterFileName());
        $this->add("Title Logo file name;".$order->getTitleLogoFileName());
        $this->add("Box Logo file name;".$order->getBoxLogoFileName());

        $this->add("\nSTATUS");
        foreach ($statusList as $status) {
            $this->add(
                $formatter->format($status->getStatusDate(), 'date'). ";".
                $formatter->format($status->getStatus(), 'mapping', 'LeepAdmin_Customer_Status').";".
                ($status->getIsCompleted() == 1 ? "Completed" : "Incomplete")
            );
        }



        $orderInfo = Business\Customer\Util::getOrderInformation($this->container, $order->getId());
        $this->add("\nCATEGORIES ORDERED");
        foreach ($orderInfo['orderCategory'] as $categoryId => $v) {
            $this->add($formatter->format($categoryId, 'mapping', 'LeepAdmin_Category_List'));
        }

        $len = 15;
        $this->add("\nPRODUCT ORDERED");
        foreach ($orderInfo['orderProduct'] as $productId => $v) {
            $this->add(
                $this->length($formatter->format($productId, 'mapping', 'LeepAdmin_Product_List'), $len). ';'.($v == 1 ? 'x' : '')
            );
        }
        $this->add("\nGENES TO BE ANALYZED");
        foreach ($orderGenes as $gene) {
            $this->add(
                $this->length($formatter->format($gene->getIdGene(), 'mapping', 'LeepAdmin_Gene_List'), 12). ';'.
                $gene->getResult()
            );
        }

        // Question
        $this->add("\nQUESTIONS");
        $productQuestions = Business\Customer\Util::getProductQuestions($this->container);
        foreach ($orderInfo['orderProduct'] as $productId => $v) {
            if (isset($productQuestions[$productId])) {
                foreach ($productQuestions[$productId] as $question) {
                    $fieldId = $question->getId();
                    if (isset($orderQuestions[$fieldId])) {
                        $this->add(
                            $this->formatAnswer($question, $orderQuestions[$fieldId])."\n"
                        );
                    }
                }
            }
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=customer.csv');        
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".$this->content);

        return $response;
    }
}
