<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class MwaValidCode extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleValidate($controller, &$data, $options = array()) {
        $sampleId = trim($controller->getRequest()->get('sampleId', ''));

        $sampleIds = explode("\n", $sampleId);
        $resp = '';
        $i = 1;
        foreach ($sampleIds as $sampleId) {
            $sampleId = triM($sampleId);
            if (!empty($sampleId)) {
                $mwaSample = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaSample')->findBySampleId($sampleId);
                if (!empty($mwaSample)) {
                    $resp .='<br/><b style="color: red">'.$i.'. '.$sampleId.': Already existed</b>';   
                }
                else {
                    $code = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaValidCode')->findByCode($sampleId);
                    if (!empty($code)) {
                        $resp .='<br/><b style="color: green">'.$i.'. '.$sampleId.': Valid</b>';
                    }
                    else {
                        $resp .='<br/><b style="color: red">'.$i.'. '.$sampleId.': Invalid</b>';
                    }
                }
                $i++;
            }
        }
        return new Response($resp);
    }

    public function handleDeleteAll($controller, &$data, $options = array()) {
        $query = $controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:MwaValidCode', 'p')->getQuery()->execute();

        $manager = $controller->get('easy_module.manager');
        $listUrl = $manager->getUrl('leep_admin', 'mwa_valid_code', 'grid', 'list');

        return $controller->redirect($listUrl);
    }

    public function handleDownload($controller, &$data, $options = array()) {
        $result = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaValidCode')->findAll();

        $validCodes = '';
        foreach ($result as $r) {
            $validCodes .= $r->getCode()."\r\n";
        }

        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/csv; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'inline; filename=valid_code.csv');        
        $resp->setContent($validCodes);
        return $resp;
    }
}
