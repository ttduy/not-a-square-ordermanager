<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ScanFile extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleDownloadPdf($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $scanFormFile= $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($id);

  /*      $attachmentDir = $this->container->getParameter('attachment_dir') . '/'. Business\ScanFormTemplate\Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;

        $fileResponse = new BinaryFileResponse($attachmentDir.'/'.$scanFormTemplate->getTemplateFile());          
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$scanFormTemplate->getTemplateFile());        
        return $fileResponse;*/
    }


    public function handleViewError($controller, &$data, $options = array()) {
        $data = array();

        $id = $controller->get('request')->get('id');
        $scanFormFile= $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($id);

        if ($scanFormFile) {
            $data['errorMessage'] = $scanFormFile->getErrorMessage();
        }

        return $controller->render('LeepAdminBundle:ScanFormFile:view_error.html.twig', $data);
    }

    public function handleProcessPendingForCreation($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $em = $controller->get('doctrine')->getEntityManager();

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneByStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING);
        if (!$scanFile) {
            return $controller->render('LeepAdminBundle:ScanFile:no_available.html.twig', $data);
        }

        $url = $mgr->getUrl('leep_admin', 'scan_file', 'detect_template', 'edit', array('id' => $scanFile->getId()));
        return new RedirectResponse($url);
    }

    public function handleProcessPendingForUpdation($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $em = $controller->get('doctrine')->getEntityManager();

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneByStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING);
        if (!$scanFile) {
            return $controller->render('LeepAdminBundle:ScanFile:no_available.html.twig', $data);
        }

        $url = $mgr->getUrl('leep_admin', 'scan_file', 'fill_all_data', 'edit', array('id' => $scanFile->getId()));
        return new RedirectResponse($url);
    }

    public function handleViewScanFile($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $filterPageNumber = $controller->get('request')->get('pageNumber', 0);
        $alignX = $controller->get('request')->get('x', 0);
        $alignY = $controller->get('request')->get('y', 0);
        $scanFile = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $utils = $controller->get('leep_admin.scan_form_template.business.utils');
        

        $scanPdfFile = $helper->getFile($scanFile->getId());

        $zones = array();
        $templateZones = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormTemplateZone')->findByIdScanFormTemplate($scanFile->getIdScanFormTemplate());
        foreach ($templateZones as $z) {
            $pageNumber = intval($z->getPageNumber());
            $zones[$pageNumber] = $utils->parseZonesDefinition($z->getZonesDefinition());
        }

        $pdf = $controller->get('white_october.tcpdf')->create();   
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->setPageUnit('pt');
        $pdf->SetMargins(0,0,0,true);
        $pdf->setTextColor(255, 0, 0);
        $pdf->setDrawColor(255, 0, 0);
        $pdf->setFontSize(8);

        $numPage = $pdf->setSourceFile($scanPdfFile);
        for ($i = 1; $i <= $numPage; $i++) {          
            if ($filterPageNumber != 0) {
                if ($filterPageNumber != $i) continue;
            }   

            $template = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($template);
            if ($size['w'] < $size['h']) {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            else {
                $pdf->AddPage('L', array($size['h'], $size['w']));
            }
            $pdf->useTemplate($template);

            // Draw Ruler
            $pdf->setTextColor(0, 0, 255);
            $pdf->setFontSize(5);
            $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(200, 200, 200));
            for ($r = 20; $r < $size['h'] - 40; $r += 20) {
                $pdf->Line(0, $r, $size['w'], $r, $style);
                $pdf->setX(0);
                $pdf->setY($r-10);
                $pdf->cell(100, 20, '  '.$r);
            }
            for ($c = 20; $c < $size['w'] - 20; $c += 20) {
                $pdf->Line($c, 0, $c, $size['h'], $style);

                for ($r = 0; $r < $size['h'] - 40; $r += 160) {
                    $pdf->setX(0);
                    $pdf->setY(0);
                    $pdf->startTransform();
                    $pdf->translateX($c-5);
                    $pdf->translateY($r);
                    $pdf->cell(100, 20, $c);
                    $pdf->stopTransform();
                }
            }

            if (isset($zones[$i])) {
                // Errors
                $pdf->setTextColor(255, 102, 0);
                $pdf->setDrawColor(255, 102, 0);
                $pdf->setFontSize(8);
                $pdf->setX(0);
                $y = 15;
                foreach ($zones[$i]['errors'] as $error) {
                    $pdf->setY($y);
                    $pdf->cell(100, 15, "[ERROR] ".$error);
                    $y += 15;
                }

                // Zones
                $pdf->setTextColor(255, 0, 0);
                $pdf->setDrawColor(255, 0, 0);
                $pdf->setFontSize(8);
                foreach ($zones[$i]['zones'] as $zone) {
                    $posX = intval($zone['x']) + intval($alignX);
                    $posY = intval($zone['y']) + intval($alignY);
                    $width = intval($zone['width']);
                    $height = intval($zone['height']);

                    $pdf->setX(0);
                    $pdf->setY(0);
                    $pdf->startTransform();
                    $pdf->translateX($posX);
                    $pdf->translateY($posY);
                    $pdf->Cell($width, $height, $zone['key'], 1, 1, 'C', 0, '', 0);
                    $pdf->stopTransform();
                }
            }
        }  

        $fileResponse = new Response();          
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'inline; filename='.$scanFile->getId().'.pdf');      
        $fileResponse->setContent($pdf->Output('', 'S'));  

        return $fileResponse;
    }

    public function handleFormulaGenerateOrder($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idScanFile = $controller->get('request')->request->get('id', '');
        $inputData = $controller->get('request')->request->get('inputData', '');

        $input = array();
        $rows = explode("\n", $inputData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $input[trim($arr[0])] = trim($arr[1]);
            }
        }

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($idScanFile);
        $scanTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($scanFile->getIdScanFormTemplate());
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($scanTemplate->getIdFormulaTemplateCreate());

        $executor = $controller->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $input);
        $errors = $executor->errors;
        $output = $executor->output;

        $result = array(
            'isError' => false,
            'idOrder' => 0,
            'message' => ''
        );

        $msg = '';
        if (!empty($errors)) {
            $result['isError'] = true;
            $msg .= implode("\n", $errors);

            $msg .= "\n".'------------------------'."\n";
        }
        foreach ($output as $k => $v) {
            $msg .= $k.' = '.$v."\n";
        }
        $result['message'] = $msg;
        if (isset($output['OrderNumberId'])) {
            $idOrder = intval($output['OrderNumberId']);

            $result['idOrder'] = $idOrder;
            $scanFile->setIdOrder($idOrder);
            $em->flush();

            $attachmentDir = $controller->get('service_container')->getParameter('attachment_dir');
            $attachmentKey = $controller->get('leep_admin.helper.common')->generateRandAttachmentDir('customers');
            mkdir($attachmentDir.'/customers/'.$attachmentKey, 0777);

            $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);
            $order->setAttachmentKey($attachmentKey);
            $em->flush();

            $srcFile = $controller->get('leep_admin.scan_file.business.helper')->getFile($scanFile->getId());
            $targetFile = $attachmentDir.'/customers/'.$attachmentKey.'/'.$scanFile->getFilename();
            @copy($srcFile, $targetFile);
            @chmod($targetFile, 0777);
        }

        return new Response(json_encode($result));
    }

    public function handleFormulaUpdateOrder($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idScanFile = $controller->get('request')->request->get('id', '');
        $inputData = $controller->get('request')->request->get('inputData', '');

        $input = array();
        $rows = explode("\n", $inputData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $input[trim($arr[0])] = trim($arr[1]);
            }
        }

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($idScanFile);
        $scanTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($scanFile->getIdScanFormTemplate());
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($scanTemplate->getIdFormulaTemplateUpdate());

        $executor = $controller->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $input);
        $errors = $executor->errors;
        $output = $executor->output;

        $result = array(
            'isError' => false,
            'idOrder' => 0,
            'message' => ''
        );

        $msg = '';
        if (!empty($errors)) {
            $result['isError'] = true;
            $msg .= implode("\n", $errors);

            $msg .= "\n".'------------------------'."\n";
        }
        foreach ($output as $k => $v) {
            $msg .= $k.' = '.$v."\n";
        }
        $result['message'] = $msg;

        return new Response(json_encode($result));
    }

    public function handleMarkError($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        $id = $controller->get('request')->get('id', 0);
        $isClosed = $controller->get('request')->get('isClosed', 0);

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        if ($scanFile->getStatus() == Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING) {
            $url = $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForCreation');
        }
        else if ($scanFile->getStatus() == Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING) {
            $url = $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForUpdation');
        }
        else {
            $url = $mgr->getUrl('leep_admin', 'scan_file', 'grid', 'list');
        }

        if ($isClosed) {
            $scanFile->setStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_CLOSED);
        }
        else {
            $scanFile->setStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_ERROR);
        }
        $em->flush();

        return new RedirectResponse($url);
    }

    public function handleDownloadFile($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $helper = $controller->get('leep_admin.scan_file.business.helper');

        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $scanFileFilename = $helper->getFile($id);

        $fileResponse = new BinaryFileResponse($scanFileFilename);          
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);  
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'inline; filename='.$scanFile->getFilename());      
        return $fileResponse;
    }
}
