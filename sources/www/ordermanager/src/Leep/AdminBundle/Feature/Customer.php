<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Leep\AdminBundle\Business\Customer\Util;

class Customer extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleShowCapture($controller, &$data, $options = array()) {
        $key = $controller->get('request')->query->get('key', false);
        $data['key'] = $key;
    }

    public function handleShowAllImages($controller, &$data, $options = array()) {
        $key = $controller->get('request')->query->get('key', false);
        $data['key'] = $key;

        $images = array();
        $dir = $controller->get('service_container')->getParameter('attachment_dir').'/'.$key;
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry != '.tmb') {
                    $images[] = $entry;
                }
            }
        }
        $data['images'] = $images;
        $data['imgPath'] = $controller->get('service_container')->getParameter('attachment_url').'/'.$key;
        closedir($handle);

        return $controller->render('LeepAdminBundle:Customer:show_all_images.html.twig', $data);
    }

    public function handleSaveImage($controller, &$data, $options = array()) {
        $key = $controller->get('request')->query->get('key', false);
        $dir = $this->container->getParameter('attachment_dir').DIRECTORY_SEPARATOR.$key;

        $filename = $dir.DIRECTORY_SEPARATOR.date('YmdHis') . '.jpg';
        $result = file_put_contents( $filename, file_get_contents('php://input') );
        if (!$result) {
            return new Response('failed');
        }

        return new Response('ok');
    }

    public function handleBulkEditStatus($controller, &$data, $options = array()) {
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $em = $controller->get('doctrine')->getEntityManager();

        $form = $controller->get('leep_admin.customer.business.bulk_edit_status_handler');
        $form->selectedId = $arr;
        $form->selectedStatus = $controller->get('request')->get('selectedStatus', '');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit status',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'bulkEditStatus', array('orderNumbers' => $selectedIdRaw))
        );

        return $controller->render('LeepAdminBundle:Customer:bulk_edit_status.html.twig', $data);
    }


    public function handleBulkEditStatusValidate($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('orderNumbers', '');
        $tmp = explode("\n", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $foundOrders = array();

        $em = $controller->get('doctrine')->getEntityManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:Customer', 'd')
            ->andWhere('d.ordernumber in (:arr)')
            ->andWhere('d.isdeleted = 0')
            ->setParameter('arr', $arr)
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $foundOrders[$r->getOrderNumber()] = $r;
        }

        $data = array();
        $today = new \DateTime();
        $todayTs = $today->getTimestamp();
        foreach ($arr as $orderNumber) {
            if ($orderNumber === 0) continue;
            if (!isset($foundOrders[$orderNumber])) {
                $data[] = array(
                    'id'          => 0,
                    'orderNumber' => $orderNumber,
                    'age'         => '',
                    'products'    => array('Order number not found'),
                    'status'      => '',
                    'color'       => '#FFCCCC',
                    'labDetails'  => ''
                );
            }
            else {
                $age = '';
                $customer = $foundOrders[$orderNumber];
                if ($customer->getDateOrdered()) {
                    $age = intval(($todayTs - $customer->getDateOrdered()->getTimestamp()) / 86400);
                }

                $data[] = array(
                    'id'          => $customer->getId(),
                    'orderNumber' => $orderNumber,
                    'age'         => $age,
                    'products'    => Business\Customer\Util::getProducts($controller, $customer->getId()),
                    'status'      => Business\Customer\Util::getCurrentStatus($controller, $customer->getId()),
                    'color'       => $customer->getColorCode(),
                    'labDetails'  => $customer->getLaboratoryDetails()
                );
            }
        }

        $links['customerEditLink'] = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit');
        $links['customerCopyLink'] = $mgr->getUrl('leep_admin', 'customer', 'copy', 'edit');

        return $controller->render('LeepAdminBundle:Customer:bulk_edit_status_validate.html.twig', array('data' => $data, 'links' => $links));
    }

    public function handleBulkEditResultReady($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $doneOrders = array();
        //////////////////////////////////
        // x. Grab customer list
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.ordernumber, p.idCustomerInfo')
            ->from('AppDatabaseMainBundle:Customer', 'p');

        Business\GeneManager\Utils::applyGeneManagerFilter($controller, $query);

        $orders = $query->getQuery()->getResult();

        //////////////////////////////////
        // x. Process DONE Order
        foreach ($orders as $order) {
            $idOrder = intval($order['id']);
            $idCustomer = intval($order['idCustomerInfo']);

            $massarray = Business\GeneManager\Utils::checkMassarray($controller, $idOrder, $idCustomer);
            $isCompleted = true;
            foreach ($massarray as $arr) {
                foreach ($arr['genes'] as $gene) {
                    $geneResult = trim($gene['result']);
                    if (empty($geneResult)) {
                        $isCompleted = false;
                        break;
                    }
                }
            }
            if ($isCompleted) {
                $doneOrders[] = $order['id'];
            }
        }

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'customer', 'customer', 'bulkEditStatus', array('selectedId' => implode(',', $doneOrders), 'selectedStatus' => 7));
        return new RedirectResponse($url);
    }


    public function handleEditColor($controller, &$data, $options = array()) {
        $form = $controller->get('leep_admin.customer.business.edit_color_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Apply new color',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Apply',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'editColor')
        );

        return $controller->render('LeepAdminBundle:Customer:form_edit_color.html.twig', $data);
    }

    public function handleToggleRecallFlag($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idCustomer = $controller->get('request')->get('idCustomer', 0);

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if ($customer) {
            $isRecall = $customer->getIsRecall();
            $customer->setIsRecall(!$isRecall);
            $em->flush();
        }

        return new Response('ok');
    }

    public function handleExportMassarrays($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mapping = $controller->get('easy_mapping');
        $today = new \DateTime();
        $tmpFolder = $controller->get('service_container')->getParameter('temp_dir');
        $tmpFile = $today->format('YmdHis').'.xlsx';

        // Excel file
        $objPHPExcel = new \PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValue('A1', 'ORDERNUMBER');
        $massarray = $mapping->getMappingFiltered('LeepAdmin_Massarray_ExportedList');
        $col = 'A';
        $sheet->getStyle('A1')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setWidth(18);
        foreach ($massarray as $id => $name) {
            $col++;
            $sheet->setCellValue($col.'1', $name);
            $sheet->getStyle($col.'1')->getFont()->setBold(true);
            $sheet->getColumnDimension($col)->setWidth(11);
        }


        // Search massarray
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('p.id, p.ordernumber, p.idCustomerInfo')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        Business\GeneManager\Utils::applyGeneManagerFilter($controller, $queryBuilder);

        $rows = $queryBuilder->getQuery()->getResult();

        $index = 1;
        foreach ($rows as $row) {
            $isMissing = false;
            $idOrder = intval($row['id']);
            $idCustomerInfo = intval($row['idCustomerInfo']);
            $massarray = Business\GeneManager\Utils::checkMassarray($controller, $idOrder, $idCustomerInfo);
            foreach ($massarray as $array) {
                if ($array['numMissing'] > 0) {
                    $isMissing = true;
                }
            }

            if ($isMissing) {
                $index++;
                $sheet->setCellValue('A'.$index, $row['ordernumber']);

                $col = 'A';
                foreach ($massarray as $array) {
                    $col++;
                    if ($array['numMissing'] > 0) {
                        $sheet->setCellValue($col.$index, 1);
                    }
                }
            }
        }

        // Apply styling
        $sheet->getStyle('A1:'.$col.$index)->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);


        // Export Excel
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($tmpFolder.'/'.$tmpFile);

        $fileResponse = new BinaryFileResponse($tmpFolder.'/'.$tmpFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename=EXPORT_MASSARRAYS.xlsx');
        return $fileResponse;
    }


    public function handleExportSNPS($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mapping = $controller->get('easy_mapping');
        $today = new \DateTime();
        $tmpFolder = $controller->get('service_container')->getParameter('temp_dir');
        $tmpFile = $today->format('YmdHis').'.xlsx';

        // Excel file
        $objPHPExcel = new \PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValue('A1', 'ORDERNUMBER');
        $sheet->getStyle('A1')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setWidth(18);
        $sheet->setCellValue('B1', 'MULTIPLEX');
        $sheet->getStyle('B1')->getFont()->setBold(true);
        $sheet->getColumnDimension('B')->setWidth(18);
        $sheet->setCellValue('C1', 'SNP ID');
        $sheet->getStyle('C1')->getFont()->setBold(true);
        $sheet->getColumnDimension('C')->setWidth(18);

        // Search massarray
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('p.id, p.ordernumber, p.idCustomerInfo')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        Business\GeneManager\Utils::applyGeneManagerFilter($controller, $queryBuilder);

        $rows = $queryBuilder->getQuery()->getResult();

        $array = array();
        foreach ($rows as $row) {
            $idOrder = intval($row['id']);
            $idCustomerInfo = intval($row['idCustomerInfo']);
            $isShowOrderNumber = false;

            $massarray = Business\GeneManager\Utils::checkMassarray($controller, $idOrder, $idCustomerInfo);
            foreach ($massarray as $marray) {
                if ($marray['numMissing'] > 0) {
                    foreach ($marray['genes'] as $gene) {
                        if (trim($gene['result']) == '') {
                            $array[] = array(
                                'orderNumber' => $row['ordernumber'],
                                'massarray'   => $marray['name'],
                                'rsnumber'    => $gene['rsnumber']
                            );
                        }
                    }
                }
            }
        }

        // Print
        $index = 1;
        $pr = array('orderNumber' => '', 'massarray' => '', 'rsnumber' => '');
        foreach ($array as $r) {
            $index++;
            if (strcmp($r['orderNumber'], $pr['orderNumber']) != 0) {
                $sheet->setCellValue('A'.$index, $r['orderNumber']);
                $sheet->setCellValue('B'.$index, $r['massarray']);
                $sheet->setCellValue('C'.$index, $r['rsnumber']);
            }
            else {
                if (strcmp($r['massarray'], $pr['massarray']) != 0) {
                    $sheet->setCellValue('B'.$index, $r['massarray']);
                }
                $sheet->setCellValue('C'.$index, $r['rsnumber']);
            }
            $pr = $r;
        }

        // Apply styling
        $sheet->getStyle('A1:'.'C'.$index)->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);


        // Export Excel
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($tmpFolder.'/'.$tmpFile);

        $fileResponse = new BinaryFileResponse($tmpFolder.'/'.$tmpFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename=EXPORT_SNPS.xlsx');
        return $fileResponse;
    }

    public function handleUpdateLabDetails($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $idCustomer = $request->get('pk', 0);
        $labDetails = $request->get('value', '');

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if ($customer) {
            $customer->setLaboratoryDetails($labDetails);
            $em->flush();
            return new Response('OK');
        }

        $response = new Response();
        $response->setStatusCode(500);
        $response->setContent("Error! Not updated yet!");
        return $response;
    }

    public function handleExportCustomers($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $customerFilterBuilder = $controller->get('leep_admin.customer.business.customer_filter_builder');

        $filters = $controller->get('leep_admin.customer.business.filter_handler')->getCurrentFilter();

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('p.ordernumber, p.dateofbirth, p.firstname, p.surname,
                      p.street, p.city, p.postcode, p.countryid, p.email, p.dateordered')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        $customerFilterBuilder->buildQuery($queryBuilder, $filters);


        $customers = $queryBuilder->getQuery()->getResult();
        $today = new \DateTime();

        $content = '';
        $content .= '"Order Number"; "Name"; "Age"; "Birthday"; Address"; "Email"'."\r\n";
        foreach ($customers as $customer) {
            $rows = array();
            $rows[] = '"'.addslashes($customer['ordernumber']).'"';
            $rows[] = '"'.addslashes($customer['firstname'].' '.$customer['surname']).'"';

            $age = '';
            $date = $customer['dateordered'];
            if ($date) {
                $todayTs = $today->getTimestamp();
                $age = intval(($todayTs - $date->getTimestamp()) / 86400).'d';
            }
            $rows[] = '"'.addslashes($age).'"';

            if ($customer['dateofbirth'] == null) {
                $rows[] = '""';
                $rows[] = '""';
            }
            else {
                $interval = $today->diff($customer['dateofbirth']);
                $rows[] = '"'.addslashes($customer['dateofbirth']->format('d/m/Y')).'"';
            }

            $address = sprintf("%s, %s %s, %s",
                $customer['street'],
                $customer['city'],
                $customer['postcode'],
                $formatter->format($customer['countryid'], 'mapping', 'LeepAdmin_Country_List')
            );
            $rows[] = '"'.addslashes($address).'"';
            $rows[] = '"'.addslashes($customer['email']).'"';

            $content .= implode(";", $rows)."\r\n";
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=customers.csv');
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }


    public function handleViewProductGene($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $id = $request->get('id', 0);

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);

        $productGenes = array();

        $idProducts = explode(",", $request->get('products'));
        foreach ($idProducts as $idProduct) {
            $product = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findOneById($idProduct);
            if ($product) {
                $query = $em->createQueryBuilder();
                $query->select('p.genename, p.scientificname, p.rsnumber, p.groupid, ma.name')
                    ->from('AppDatabaseMainBundle:Genes', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ProdGeneLink', 'pg', 'with', 'pg.geneid = p.id')
                    ->leftJoin('AppDatabaseMainBundle:MassarrayGeneLink', 'mg', 'WITH', 'mg.geneid = p.id')
                    ->leftJoin('AppDatabaseMainBundle:Massarray', 'ma', 'WITH', 'ma.id = mg.massarrayid')
                    ->andWhere('pg.productid = '.$product->getId());

                if ($customer) {
                    $query
                        ->addSelect('ci.result')
                        ->leftJoin('AppDatabaseMainBundle:CustomerInfoGene', 'ci', 'WITH', '(p.id = ci.idGene) AND (ci.idCustomer = :idCustomer)')
                        ->setParameter('idCustomer', $customer->getIdCustomerInfo());
                }
                $results = $query->getQuery()->getResult();

                $genes = array();
                foreach ($results as $r) {
                    $genes[] = array(
                        'name'                => $r['genename'],
                        'scientificName'      => $r['scientificname'],
                        'rsnumber'            => $r['rsnumber'],
                        'resultOption'        => $formatter->format($r['groupid'], 'mapping', 'LeepAdmin_Gene_ResultOptions'),
                        'massarray'           => $r['name'],
                        'result'              => isset($r['result']) ? $r['result'] : ''
                    );
                }

                $productGenes[] = array(
                    'product'     => $product->getName(),
                    'genes'       => $genes
                );
            }
        }

        $data['productGenes'] = $productGenes;

        return $controller->render('LeepAdminBundle:Customer:view_product_gene.html.twig', $data);
    }

    public function handleBulkMergeCheckOrder($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $arr = [];
        $selectedIdRaw = $controller->get('request')->get('customerNumbers', '');
        $tmp = explode("\n", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $foundOrders = array();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d, e.customerNumber')
            ->from('AppDatabaseMainBundle:Customer', 'd')
            ->leftJoin('AppDatabaseMainBundle:CustomerInfo', 'e', 'WITH', 'd.idCustomerInfo = e.id')
            ->andWhere('e.customerNumber in (:arr)')
            ->andWhere('d.isdeleted = 0')
            ->setParameter('arr', $arr)
            ->getQuery()->getResult();

        $customerNumbers = $arr;
        foreach ($result as $r) {
            $foundOrders[] = array(
                'order' => $r[0],
                'customerNumber' => $r['customerNumber']
            );
        }

        $data = array();
        $today = new \DateTime();
        $todayTs = $today->getTimestamp();
        foreach ($foundOrders as $record) {
            $age = '';
            $order = $record['order'];
            $customerNumber = $record['customerNumber'];

            if ($order->getDateOrdered()) {
                $age = intval(($todayTs - $order->getDateOrdered()->getTimestamp()) / 86400);
            }

            $data[] = array(
                'id'          => $order->getId(),
                'idCustomerInfo' => $order->getIdCustomerInfo(),
                'customerNumber' => $customerNumber,
                'orderNumber' => $order->getOrderNumber(),
                'age'         => $age,
                'firstName'   => $order->getFirstName(),
                'surName'     => $order->getSurName(),
                'birthday'    => $formatter->format($order->getDateOfBirth(), 'date'),
                'gender'      => $formatter->format($order->getGenderId(), 'mapping', 'LeepAdmin_Gender_List'),
                'address'     => sprintf("%s, %s %s, %s",
                    $order->getStreet(),
                    $order->getCity(),
                    $order->getPostCode(),
                    $formatter->format($order->getCountryId(), 'mapping', 'LeepAdmin_Country_List')
                ),
                'color'       => $order->getColorCode(),
            );
        }

        $conflictResult = Util::checkForConflict($this->container, $customerNumbers);
        $hasConflict = $conflictResult[0] > 0;
        $hasSoftConflict = $conflictResult[1] > 0;

        $links = array();
        $links['customerEditLink'] = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit');
        $links['customerInfoEditLink'] = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit');

        return $controller->render('LeepAdminBundle:Customer:bulk_merge_check_order.html.twig', [
            'data' =>               $data,
            'links' =>              $links,
            'geneDatas' =>          $conflictResult[2],
            'hasConflict' =>        $hasConflict,
            'hasSoftConflict' =>    $hasSoftConflict
        ]);
    }

    public function handleViewDuplication($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $request = $controller->get('request');
        $fromDate = $request->get('from', '');
        $toDate = $request->get('to', '');
        $mustBeProduct = $request->get('must', null);
        $mustNotBeProduct = $request->get('must_not', null);

        if (empty($mustBeProduct) || $mustBeProduct == 'null') {
            $mustBeProduct = [];
        } else {
            $mustBeProduct = array_map('trim', explode(',', $mustBeProduct));
        }

        if (empty($mustNotBeProduct) || $mustNotBeProduct == 'null') {
            $mustNotBeProduct = [];
        } else {
            $mustNotBeProduct = array_map('trim', explode(',', $mustNotBeProduct));
        }

        if (!empty($fromDate)) {
            $dateObj = \DateTime::createFromFormat("d/m/Y", $fromDate);
            if (!$dateObj) {
                $fromDate = '2015-01-01';
            } else {
                $fromDate = $dateObj->format("Y-m-d");
            }
        } else {
            $fromDate = '2015-01-01';
        }

        if (!empty($toDate)) {
            $dateObj = \DateTime::createFromFormat("d/m/Y", $toDate);
            if (!$dateObj) {
                $toDate = '';
            } else {
                $toDate = $dateObj->format("Y-m-d");
            }
        }

        $orders = array();
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.idCustomerInfo, p.ordernumber, p.firstname, p.surname, p.dateofbirth, p.genderid, p.street, p.city, p.postcode, p.countryid, e.customerNumber')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:CustomerInfo', 'e', 'WITH', 'p.idCustomerInfo = e.id')
            ->andWhere('p.dateordered >= :fromDate')
            ->andWhere('p.idCustomerInfo != 0')
            ->setParameter('fromDate', $fromDate);

        if (!empty($toDate)) {
            $query->andWhere('p.dateordered <= :toDate')
                ->setParameter('toDate', $toDate);
        }

        if (!empty($mustBeProduct)) {
            $orCond =[];
            foreach ($mustBeProduct as $idProduct) {
                $orCond[] = '(FIND_IN_SET('.$idProduct.', p.productList) > 0)';
            }
            $query->andWhere('('.implode(' OR ', $orCond).')');
        }

        if (!empty($mustNotBeProduct)) {
            $orCond = [];
            foreach ($mustNotBeProduct as $idProduct) {
                $orCond[] = '(FIND_IN_SET('.$idProduct.', p.productList) = 0)';
            }
            $query->andWhere('('.implode(' AND ', $orCond).')');
        }

        $rows = $query->getQuery()->getResult();
        $hashes = array();
        foreach ($rows as $r) {
            $r['country'] = $formatter->format($r['countryid'], 'mapping', 'LeepAdmin_Country_List');
            $r['gender'] = $formatter->format($r['genderid'], 'mapping', 'LeepAdmin_Gender_List');
            $r['birthday'] = empty($r['dateofbirth']) ? '' : $r['dateofbirth']->format('d/m/Y');
            $orders[] = $r;
            $k = sha1(sprintf("%s-%s-%s-%s",
                $r['firstname'],
                $r['surname'],
                $r['birthday'],
                $r['genderid']
            ));

            if (!isset($hashes[$k])) {
                $hashes[$k] = array();
            }
            $hashes[$k][] = $r;
        }

        $duplicated = array();
        foreach ($hashes as $entries) {
            if (count($entries) >= 2) {
                $idCustomerInfo = $entries[0]['idCustomerInfo'];
                $found = false;
                foreach ($entries as $order) {
                    if ($order['idCustomerInfo'] != $idCustomerInfo) {
                        $found = true;
                    }
                }

                if ($found) {
                    $duplicated[] = $entries;
                }
            }
        }

        $links = array();
        $links['customerEditLink'] = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit');
        $links['customerInfoEditLink'] = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit');

        return $controller->render('LeepAdminBundle:Customer:view_duplication.html.twig', array('duplicated' => $duplicated, 'links' => $links));
    }

    public function parseCommaList($str) {
        $arr = array();
        $selectedIdRaw = $str;
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = intval($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }
        return $arr;
    }
    public function handleCheckDuplication($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $request = $controller->get('request');
        $orderIdList = $this->parseCommaList($request->get('selectedId', ''));
        $orderIdList[] = 0;

        // WARNING: Duplicate code, find a way to handle me
        $orders = array();
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.idCustomerInfo, p.ordernumber, p.firstname, p.surname, p.dateofbirth, p.genderid, p.street, p.city, p.postcode, p.countryid, e.customerNumber')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:CustomerInfo', 'e', 'WITH', 'p.idCustomerInfo = e.id')
            //->andWhere('p.dateordered >= :cDate')
            ->andWhere('p.idCustomerInfo != 0');
        $rows = $query->getQuery()->getResult();
        $hashes = array();
        foreach ($rows as $r) {
            if ($r['firstname'] == '' &&
                $r['surname'] == '' &&
                $r['dateofbirth'] == null &&
                $r['genderid'] == 0) {
                continue;
            }

            $r['country'] = $formatter->format($r['countryid'], 'mapping', 'LeepAdmin_Country_List');
            $r['gender'] = $formatter->format($r['genderid'], 'mapping', 'LeepAdmin_Gender_List');
            $r['birthday'] = empty($r['dateofbirth']) ? '' : $r['dateofbirth']->format('d/m/Y');
            $orders[] = $r;
            $k = sha1(sprintf("%s-%s-%s-%s-%s-%s",
                $r['firstname'],
                $r['surname'],
                $r['birthday'],
                $r['genderid'],
                $r['postcode'],
                $r['countryid']
            ));

            if (!isset($hashes[$k])) {
                $hashes[$k] = array();
            }
            $hashes[$k][] = $r;
        }

        $duplicated = array();
        foreach ($hashes as $entries) {
            if (count($entries) >= 2) {
                $idCustomerInfo = $entries[0]['idCustomerInfo'];
                $found = false;
                foreach ($entries as $order) {
                    if ($order['idCustomerInfo'] != $idCustomerInfo) {
                        $found = true;
                    }
                }

                $isRelevant = false;
                foreach ($entries as $order) {
                    if (in_array($order['id'], $orderIdList)) {
                        $isRelevant = true;
                    }
                }

                if ($found && $isRelevant) {
                    $duplicated[] = $entries;
                }
            }
        }

        $data['duplicated'] = $duplicated;

        $links = array();
        $links['customerEditLink'] = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit');
        $links['customerInfoEditLink'] = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit');
        $data['links'] = $links;

        return $controller->render('LeepAdminBundle:Customer:check_duplication.html.twig', $data);
    }

    public function handleGetAcquisiteurId($controller, &$data, $options = array()) {
        $info_json = $controller->get('request')->get('param');
        $info = json_decode($info_json);
        $customerId = $info[0];
        $position = $info[1];
        $em = $controller->get('doctrine')->getEntityManager();
        $customer = $em->getRepository("AppDatabaseMainBundle:Customer")->findOneById($customerId);
        // print_r($customer);
        // die;
        $acquisituerId = '';
        if ($position == '1') {
            $acquisituerId = $customer->getIdAcquisiteur1();
        }
        if ($position == '2') {
            $acquisituerId = $customer->getIdAcquisiteur2();
        }
        else{
            $acquisituerId = $customer->getIdAcquisiteur3();
        }
        // return new Response("Call server successfully");
        // print_r($acquisituerId);
        // die;
        return new Response($acquisituerId);
    }

    public function handleGetAcquisiteurIds($controller, &$data, $optios=array()) {
        $customerNumber = $controller->get('request')->get('number');
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.idAcquisiteur1 as idAc1, p.idAcquisiteur2 as idAc2, p.idAcquisiteur3 as idAc3')
                ->from('AppDatabaseMainBundle:Customer','p')
                ->andWhere('p.ordernumber = :customerNumber')
                ->setParameter('customerNumber', $customerNumber);
        $queryResult = $query->getQuery()->getResult();
        $idAc_arr = $queryResult[0];

        $acquisituerId1 = $idAc_arr['idAc1'];
        $acquisituerId2 = $idAc_arr['idAc2'];
        $acquisituerId3 = $idAc_arr['idAc3'];
        $acquisituerIds_json = json_encode([$acquisituerId1, $acquisituerId2, $acquisituerId3]);
        return new Response($acquisituerIds_json);
    }
}
