<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ScanFormTemplate extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleDownloadTemplate($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $scanFormTemplate = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($id);
        $utils = $controller->get('leep_admin.scan_form_template.business.utils');
        
        $attachmentDir = $this->container->getParameter('files_dir') . '/'. Business\ScanFormTemplate\Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;

        $templateFile = $attachmentDir.'/'.$scanFormTemplate->getTemplateFile();

        $zones = array();
        $templateZones = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormTemplateZone')->findByIdScanFormTemplate($id);
        foreach ($templateZones as $z) {
            $pageNumber = intval($z->getPageNumber());
            $zones[$pageNumber] = $utils->parseZonesDefinition($z->getZonesDefinition());
        }

        $pdf = $controller->get('white_october.tcpdf')->create();   
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->setPageUnit('pt');
        $pdf->SetMargins(0,0,0,true);
        $pdf->setTextColor(255, 0, 0);
        $pdf->setDrawColor(255, 0, 0);
        $pdf->setFontSize(8);

        $numPage = $pdf->setSourceFile($templateFile);
        for ($i = 1; $i <= $numPage; $i++) {                    
            $template = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($template);
            if ($size['w'] < $size['h']) {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            else {
                $pdf->AddPage('L', array($size['h'], $size['w']));
            }
            $pdf->useTemplate($template);

            // Draw Ruler
            $pdf->setTextColor(0, 0, 255);
            $pdf->setFontSize(5);
            $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(200, 200, 200));
            for ($r = 20; $r < $size['h'] - 40; $r += 20) {
                $pdf->Line(0, $r, $size['w'], $r, $style);
                $pdf->setX(0);
                $pdf->setY($r-10);
                $pdf->cell(100, 20, '  '.$r);
            }
            for ($c = 20; $c < $size['w'] - 20; $c += 20) {
                $pdf->Line($c, 0, $c, $size['h'], $style);

                for ($r = 0; $r < $size['h'] - 40; $r += 160) {
                    $pdf->setX(0);
                    $pdf->setY(0);
                    $pdf->startTransform();
                    $pdf->translateX($c-5);
                    $pdf->translateY($r);
                    $pdf->cell(100, 20, $c);
                    $pdf->stopTransform();
                }
            }

            if (isset($zones[$i])) {
                // Errors
                $pdf->setTextColor(255, 102, 0);
                $pdf->setDrawColor(255, 102, 0);
                $pdf->setFontSize(8);
                $pdf->setX(0);
                $y = 15;
                foreach ($zones[$i]['errors'] as $error) {
                    $pdf->setY($y);
                    $pdf->cell(100, 15, "[ERROR] ".$error);
                    $y += 15;
                }

                // Zones
                $pdf->setTextColor(255, 0, 0);
                $pdf->setDrawColor(255, 0, 0);
                $pdf->setFontSize(8);
                foreach ($zones[$i]['zones'] as $zone) {
                    $posX = intval($zone['x']);
                    $posY = intval($zone['y']);
                    $width = intval($zone['width']);
                    $height = intval($zone['height']);

                    $pdf->setX(0);
                    $pdf->setY(0);
                    $pdf->startTransform();
                    $pdf->translateX($posX);
                    $pdf->translateY($posY);
                    $pdf->Cell($width, $height, $zone['key'], 1, 1, 'C', 0, '', 0);
                    $pdf->stopTransform();
                }
            }
        }  

        $fileResponse = new Response();          
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'inline; filename='.$scanFormTemplate->getTemplateFile());      
        $fileResponse->setContent($pdf->Output('', 'S'));  

        return $fileResponse;
    }


    public function handleCheckTemplate($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $utils = $controller->get('leep_admin.scan_form_template.business.utils');


        $scanFormTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($id);

        $helper = $controller->get('leep_admin.helper.common');
        $scanFormDir = $controller->get('service_container')->getParameter('files_dir').'/scan_form';
        $templateFile = $scanFormDir.'/templates/'.$scanFormTemplate->getId().'.pdf';
        $templateCheckFolder = $scanFormDir.'/templates_check/'.$scanFormTemplate->getId();
        @mkdir($templateCheckFolder);

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ScanFormTemplateExpectedTokens', 'p')
            ->andWhere('p.idScanFormTemplate = '.$scanFormTemplate->getId())
            ->getQuery()
            ->execute();

        $builder = $controller->get('form.factory')->createBuilder('form');
        $zoneTypeManager = $this->container->get('leep_admin.scan_file.business.zone_type_manager');                

        $extraSpaceX = 10; 
        $extraSpaceY = 5;
        $zonesArgs = array();
        $zonesView = array();
        $templateZones = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplateZone')->findByIdScanFormTemplate($id);
        foreach ($templateZones as $zone) {
            $zonesDef = $utils->parseZonesDefinition($zone->getZonesDefinition());
            foreach ($zonesDef['zones'] as $z) {
                $x = $z['x'] - $extraSpaceX; if ($x < 0) { $x = 0; }
                $y = $z['y'] - $extraSpaceY; if ($y < 0) { $y = 0; }
                $width = $z['width'] + $extraSpaceX*2;
                $height = $z['height'] + $extraSpaceY*2;
                $zonesArgs[] = sprintf("%s,%s,%s,%s,%s,%s",
                    $zone->getPageNumber(),
                    $x, $y, $width, $height, $z['key']
                );

                $zonesView[] = array(
                    'key'   => $z['key'],
                    'zone'  => $z['zone'],
                    'notes' => isset($z['params']['Notes']) ? $z['params']['Notes'] : '',
                    'file'  => $z['key'].'.jpg'
                );

                if ($z['idType'] != Business\ScanFormTemplate\Constant::TEMPLATE_DETECTION_ZONE) {                    
                    $zoneTypeManager->buildFormToken($builder, array(
                        'id'           => $z['key'],
                        'idType'       => $z['idType'],
                        'image'        => $z['key'].'jpg',
                        'params'       => $z['params'],
                        'data'         => ''
                    ));
                }

                // Detection zones
                if ($z['idType'] == Business\ScanFormTemplate\Constant::TEMPLATE_DETECTION_ZONE) {
                    $expectedTokens = new Entity\ScanFormTemplateExpectedTokens();
                    $expectedTokens->setIdScanFormTemplate($scanFormTemplate->getId());
                    $expectedTokens->setImage('/templates_check/'.$scanFormTemplate->getId().'/'.$z['key'].'.jpg');
                    $em->persist($expectedTokens);
                    $em->flush();
                }
            }
        }

        // Processing
        $jarFile = $controller->get('service_container')->getParameter('kernel.root_dir').'/../scripts/pdfImageExtractor/build/jar/pdfImageExtractor.jar';
        $cmd = sprintf("java -jar %s %s %s %s",
            '"'.$jarFile.'"',
            '"'.$templateFile.'"',
            '"'.$templateCheckFolder.'"',
            '"'.implode(';', $zonesArgs).'"'
        );
        $out = $helper->executeCommand($cmd);

        $data['form'] = $builder->getForm()->createView();

        // Presentation
        $fileViewUrl = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils',  'viewImage', array('file' => '/scan_form/templates_check/'.$scanFormTemplate->getId()));

        $data['fileViewUrl'] = $fileViewUrl;
        $data['zones'] = $zonesView;

        return $controller->render('LeepAdminBundle:ScanFormTemplate:checkTemplate.html.twig', $data);
    }

    public function handleTest($controller,  &$data, $options = array()) {
        $file = '/var/www/DEMO_PURE01_LONG_TABLE.pdf';


        $pdf = $controller->get('white_october.tcpdf')->create();  
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $pdf->setPageUnit('pt');
        $pdf->SetMargins(0,0,0,true);
        $pdf->setTextColor(255, 0, 0);
        $pdf->setDrawColor(255, 0, 0);
        $pdf->setFontSize(8);
        $pdf->setFont('courier');

        $numPage = $pdf->setSourceFile($file);

        for ($i = 81; $i <= 81; $i++) {
            $template = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($template);
            if ($size['w'] < $size['h']) {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            else {
                $pdf->AddPage('L', array($size['h'], $size['w']));
            }
            $pdf->useTemplate($template);
        }

        $fileResponse = new Response();          
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'inline; filename=Test.pdf');      
        $fileResponse->setContent($pdf->Output('', 'S'));  

        return $fileResponse;
    }
}
