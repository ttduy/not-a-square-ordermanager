<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class AppUtils extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }


    protected function cleanupFilename($name) {
        $name = str_replace('/', '', $name);
        $name = str_replace('..', '', $name);
        $name = str_replace('\\', '', $name);
        return $name;
    }

    public function handleViewFile($controller, &$data, $options = array()) {
        $fileDir = $controller->get('service_container')->getParameter('files_dir');

        $dir = $controller->get('request')->get('dir');
        $name = $controller->get('request')->get('name');
        $dir = $this->cleanupFilename($dir);
        $name = $this->cleanupFilename($name);

        $filename = $fileDir.'/'.$dir.'/'.$name;
        if (is_file($filename)) {
            $fileResponse = new BinaryFileResponse($filename);          
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            return $fileResponse;
        }
    }   

    public function handleViewAttachment($controller, &$data, $options = array()) {
        $fileDir = $controller->get('service_container')->getParameter('files_dir');

        $dir = $controller->get('request')->get('dir');
        $name = $controller->get('request')->get('name');
        $dir = $this->cleanupFilename($dir);
        $name = $this->cleanupFilename($name);

        $filename = $fileDir.'/'.$dir.'/'.$name;
        if (is_file($filename)) {
            $resp = new Response();
            $resp->headers->set('Content-Type', 'image');
            $resp->headers->set('Content-Disposition', 'inline; filename='.$name);
            $resp->setContent(file_get_contents($filename));
            return $resp;
        }
    }   

    public function handleViewImage($controller, &$data, $options = array()) {
        $fileDir = $controller->get('service_container')->getParameter('files_dir');

        $file = $controller->get('request')->get('file');
        $file = str_replace('..', '', $file);

        $filename = $fileDir.'/'.$file;
        if (is_file($filename)) {
            $fileResponse = new BinaryFileResponse($filename);          
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $fileResponse->headers->set('Content-Type', 'image');
            $fileResponse->headers->set('Content-Disposition', 'inline');   
            return $fileResponse;
        }
        return new Response("");
    }   
    public function handleSwitchTranslator($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature',  'dashboard');

        return new RedirectResponse($url);
    }

    public function handleExitSwitchTranslator($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'translator_user', 'grid',  'list');

        return new RedirectResponse($url);
    }

    public function handleGetReportSections($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');   
        $query = $controller->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('(p.idType IS NULL) OR (p.idType != 2)')
            ->addOrderBy('p.idReport', 'ASC')
            ->addOrderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();

        $map = array();
        foreach ($sections as $section) {
            $title = $mapping->getMappingTitle('LeepAdmin_Report_List', $section->getIdReport());
            if (empty($title)) {
                continue;
            }
            $title .=' - '.$section->getName();
            $map[] = array(
                'id' => $section->getId(),
                'text' => $title
            );
        }
        return new Response(json_encode($map));
    }
}
