<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class Rating extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleFilterDCRating($controller, &$data, $options = array()) {
        return $this->getFilterResult($controller, $data, $options, Business\Rating\Constant::RATING_TYPE_DISTRIBUTION_CHANNEL);
    }

    public function handleFilterAcquisiteurRating($controller, &$data, $options = array()) {
        return $this->getFilterResult($controller, $data, $options, Business\Rating\Constant::RATING_TYPE_ACQUISITEUR);
    }

    public function handleFilterPartnerRating($controller, &$data, $options = array()) {
        return $this->getFilterResult($controller, $data, $options, Business\Rating\Constant::RATING_TYPE_PARTNER);
    }

    public function handleDcRatingHistory($controller, &$data, $options = array()) {
        return $this->getRatingHistory($controller, $data, $options, 'DistributionChannel');
    }

    public function handleAcquisiteurRatingHistory($controller, &$data, $options = array()) {
        return $this->getRatingHistory($controller, $data, $options, 'Acquisiteur');
    }

    public function handlePartnerRatingHistory($controller, &$data, $options = array()) {
        return $this->getRatingHistory($controller, $data, $options, 'Partner');
    }

    public function getFilterResult($controller, &$data, $options = array(), $type) {
        $startDate = $controller->get('request')->get('startDate', null);
        $endDate = $controller->get('request')->get('endDate', null);
        $rules = $controller->get('request')->get('rules', null);

        $arrEntries = array();
        if ($startDate && $endDate && $rules) {
            list($day, $month, $year) = explode('/', $startDate);
            $startDate = date_create($year . '-' . $month . '-' . $day);

            list($day, $month, $year) = explode('/', $endDate);
            $endDate = date_create($year . '-' . $month . '-' . $day);

            $rules = json_decode($rules, true);
            $arrRules = array();
            foreach ($rules as $rule) {
                $arrRules[$rule['level']] = array(
                                                'min'   => $rule['min'],
                                                'max'   => $rule['max']
                                            );
            }

            $arrEntries = Business\Rating\Utils::filter($controller, $startDate, $endDate, $arrRules, $type);
        }

        return new Response(json_encode($arrEntries));
    }

    public function getRatingHistory($controller, &$data, $options = array(), $type) {
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $id = $controller->get('request')->get('id', null);
        $query = $controller->get('doctrine')->getEntityManager()->createQueryBuilder();

        $targetTable = $type . 'RatingHistory';
        $targetField = 'id' . $type;

        $result = $query->select('p.createdAt, p.oldRating, p.newRating, p.totalOfOrders, r.startDate, r.endDate, r.rules')
            ->from('AppDatabaseMainBundle:'. $targetTable, 'p')
            ->leftJoin('AppDatabaseMainBundle:Rating', 'r', 'WITH', 'p.idRating = r.id')
            ->andWhere('p.'.$targetField. ' = :id')
            ->setParameter('id', $id)
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        $arrData = array();
        if ($result) {
            foreach ($result as $entry) {
                $rules = @json_decode($entry['rules'], true);
                $rulesText = array();
                foreach ($rules as $rating => $range) {
                    $rulesText[] = sprintf("%s: %s to %s",
                        $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $rating),
                        $range['min'],
                        $range['max']
                    );
                }

                $arrData[] = array(
                    'createdAt'     => $formatter->format($entry['createdAt'], 'date'),
                    'period'        => $formatter->format($entry['startDate'], 'date').' to '.$formatter->format($entry['endDate'], 'date'),
                    'oldRating'     => $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $entry['oldRating']),
                    'newRating'     => $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $entry['newRating']),
                    'totalOfOrders' => $entry['totalOfOrders'],
                    'rules'         => $rulesText
                );
            }
        }

        $data['histories'] = $arrData;

        return $controller->render('LeepAdminBundle:Rating:rating_history.html.twig', $data);
    }
}
