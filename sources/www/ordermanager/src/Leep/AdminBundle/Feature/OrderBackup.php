<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Leep\AdminBundle\Business\OrderBackup\Constant;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OrderBackup extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleResetBackup($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $em = $controller->get('doctrine')->getEntityManager();
        $orderBackup = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneById($id);

        $orderBackup->setStatus(Constant::ORDER_BACKUP_STATUS_OPEN);
        $em->persist($order->backup);
        $em->flush();

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'order_backup', 'grid', 'list');
        return new RedirectResponse($url);
    }

    public function handleDownloadExportRecord($controller, &$data, $options = array()) {
        $fileIndex = $controller->get('request')->query->get('index', 1);
        $fileFolder = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
        $file = "$fileFolder/export_customer_gene_data_$fileIndex.csv";

        $fileResponse = new BinaryFileResponse($file);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/zip; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$file);
        $fileResponse->headers->set('Content-Length', filesize($file));
        return $fileResponse;
    }

    public function handleVerifyBackup($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $md5 = $controller->get('request')->get('md5', null);
        $orderBackup = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneById($id);

        $backupDir = Business\OrderBackup\Util::getBackupDir($controller);

        $result = '';
        if ($orderBackup->getStatus() != Constant::ORDER_BACKUP_STATUS_READY) {
            $result = 'Order backup not yet ready or has been verified.';
        } else if ($orderBackup && $md5) {
            $md5Hash = $orderBackup->getMd5Hash();
            if ($md5Hash == $md5) {
                $result = 'Verify successfully';

                $em = $controller->get('doctrine')->getEntityManager();
                $orderBackup->setStatus(Constant::ORDER_BACKUP_STATUS_VERIFIED);
                $em->persist($orderBackup);
                $em->flush();

                // Remove backup files
                $fileList = explode(";", $orderBackup->getBackupFiles());
                foreach ($fileList as $fileDir) {
                    if (is_file($fileDir)) {
                        unlink($fileDir);
                    }
                }

                $orderBackup->setStatus(Constant::ORDER_BACKUP_STATUS_DONE);
                $em->persist($orderBackup);
                $em->flush();
            } else {
                $result = 'Failure: File not match.';
            }
        }

        $mgr = $controller->get('easy_module.manager');
        $action = $mgr->getUrl('leep_admin', 'order_backup', 'feature', 'verifyBackup', array('id' => $id));
        return $controller->render('LeepAdminBundle:OrderBackup:verify_backup.html.twig', [
            'id' => $id,
            'action' => $action,
            'result' => $result
        ]);
    }

    public function handleDownloadBackupFile($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $backupDir = Business\OrderBackup\Util::getBackupDir($controller);

        $id = $controller->get('request')->get('id');
        $orderBackup = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneById($id);

        $fileName = $orderBackup->getFile();
        if (empty($fileName)) {
            $fileName = Util::createBackupFile($controller, $orderBackup->id);
            $orderBackup->setFile($fileName);
            $em->persist($orderBackup);
            $em->flush();
        }

        $fileDir = $backupDir.$fileName;

        $fileResponse = new BinaryFileResponse($fileDir);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/zip; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$fileName);
        $fileResponse->headers->set('Content-Length', filesize($fileDir));
        return $fileResponse;
    }

    public function handleGetMatchedOrders($controller, &$data, $options = array()) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $helperCommon = $controller->get('leep_admin.helper.common');

        $id = $controller->get('request')->get('id', null);
        $startDate = $controller->get('request')->get('startDate', null);
        $endDate = $controller->get('request')->get('endDate', null);

        $orders = Business\OrderBackup\Util::getOrderList($controller, $id, $startDate, $endDate);
        $arrOrders = [];
        foreach ($orders as $order) {
            $arrOrders[] = [
                'orderNumber' =>            $order['ordernumber'],
                'customerName' =>           trim(implode(' ', array($order['firstName'], $order['surName']))),
                'distributionChannel' =>    $order['distributionchannel'],
                'dateOrdered' =>            $formatter->format($order['dateordered'], 'date')
            ];
        }

        $data['matchedOrders'] = $arrOrders;

        return $controller->render('LeepAdminBundle:OrderBackup:matched_orders.html.twig', $data);
    }

    public function handleListBackupInfo($controller, &$data, $options = []) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $backups = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findAll();
        $response = [];
        foreach ($backups as $backup) {
            $response[] = [
                'id' =>     $backup->getId(),
                'name' =>   $backup->getName(),
                'des' =>    $backup->getDescription(),
                'start' =>  $formatter->format($backup->getStartDate(), 'date'),
                'end' =>    $formatter->format($backup->getEndDate(), 'date'),
                'file' =>   $backup->getFile(),
                'status' => Constant::getStatusList()[$backup->getStatus()],
                'date' =>   $formatter->format($backup->getBackupDate(), 'date'),
                'num' =>    $backup->getNumberOfOrders()
            ];
        }

        return new Response(json_encode($response));
    }
}
