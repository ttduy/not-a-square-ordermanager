<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;

class Invoice extends AbstractFeature {
    public function getActions() { return array('list'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleDashboard($controller, &$data, $options = array()) {    
        return $controller->render('LeepAdminBundle:Invoice:dashboard.html.twig', $data);
    }

    public function handlePickCustomers($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $grid = $controller->get('leep_admin.invoice.business.customer_picker_grid_reader');
        $filter = $controller->get('leep_admin.invoice.business.customer_picker_filter_handler');

        $gridTitle = 'Pick customer(s)';

        $data['mainTabs'] = array(
            'content' => array(
                'list'          => array('title' => 'Customers', 'link' => '#'),
            ),
            'selected' => 'list'
        );
        $data['grid'] = array(
            'id' => 'CustomerPicker',
            'title' => $gridTitle,
            'header' => $grid->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'invoice', 'feature', 'pickCustomersAjaxSource', $request->query->all())
        );

        $filter->execute();
        $data['filter'] = array(
            'id'       => 'CustomerPicker_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => $mgr->getUrl('leep_admin', 'invoice', 'feature', 'pickCustomers', $request->query->all()),
            'visiblity' => 'hide'
        );
        if ($request->getMethod() == 'POST') {
            $data['filter']['visiblity'] = '';
        }

        $data['selectCustomersUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'selectCustomers', $request->query->all());

        return $controller->render('LeepAdminBundle:Invoice:pick_customers.html.twig', $data);
    }


    public function handlePickCustomersAjaxSource($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $grid = $controller->get('leep_admin.invoice.business.customer_picker_grid_reader');
        $filter = $controller->get('leep_admin.invoice.business.customer_picker_filter_handler');

        $grid->filters = $filter->getCurrentFilter();
        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($grid);

        return new Response(json_encode($resp));
    }

    public function handleViewCustomersAjaxSource($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $grid = $controller->get('leep_admin.invoice.business.customer_viewer_grid_reader');

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($grid);

        return new Response(json_encode($resp));
    }

    public function handleSelectCustomers($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $tempDataHelper = $controller->get('leep_admin.helper.temp_data');

        $url = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'pickCustomers', array(
            'idDistributionChannel' => $request->get('idDistributionChannel', 0),
            'customerDataSessionKey' => $request->get('customerDataSessionKey', ''),
            'idInvoice' => $request->get('idInvoice', 0)
        ));

        $key = $request->get('customerDataSessionKey');        
        $currentSelected = $tempDataHelper->get($key, array());
        $customerIds = explode(',', $request->get('selectedId'));
        foreach ($customerIds as $idCustomer) {
            $idCustomer = intval($idCustomer);
            
            if ($idCustomer) {
                $price = Business\Customer\Util::computePrice($controller, $idCustomer, 'invoice_dc');

                $priceDetail = array();
                $totalAmount = 0;
                foreach ($price['items']['categories'] as $idCategory => $amount) {                    
                    $row = array(
                        'product' => $formatter->format($idCategory, 'mapping', 'LeepAdmin_Category_List'),
                        'amount'  => $amount
                    );
                    $priceDetail[] = $row;
                    $totalAmount += $amount;
                }
                foreach ($price['items']['products'] as $idProduct => $amount) {
                    $row = array(
                        'product' => $formatter->format($idProduct, 'mapping', 'LeepAdmin_Product_List'),
                        'amount'  => $amount
                    );
                    if (isset($price['specialProducts'][$idProduct])) {
                        $row['product'] = $formatter->format($price['specialProducts'][$idProduct], 'mapping', 'LeepAdmin_SpecialProduct_List');
                    }
                    $priceDetail[] = $row;
                    $totalAmount += $amount;
                }

                $currentSelected[$idCustomer] = array(
                    'idCustomer'    => $idCustomer,
                    'price'         => $totalAmount,
                    'priceDetail'   => $priceDetail,
                    'priceDate'     => new \DateTime()
                );
            }
        }        
        $tempDataHelper->save($key, $currentSelected);

        return new RedirectResponse($url);
    }

    public function handleRemoveCustomers($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $tempDataHelper = $controller->get('leep_admin.helper.temp_data');

        $key = $request->get('customerDataSessionKey');        
        $isClearAll = $request->get('isClearAll', false);

        if ($isClearAll) {
            $tempDataHelper->save($key, array());
        }
        else {
            $customers = $tempDataHelper->get($key, array());        
            $removeIdList = explode(',', $request->get('selectedId'));
            $removeIdArray = array();
            foreach ($removeIdList as $id) {
                $id = intval($id);
                if (isset($customers[$id])) {
                    unset($customers[$id]);
                }
            }

            $tempDataHelper->save($key, $customers);
        }

        return new Response("OK");
    }

    public function handleUpdateCustomerData($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $tempDataHelper = $controller->get('leep_admin.helper.temp_data');

        $field = $request->get('field');
        $idCustomer = $request->get('idCustomer');
        $value = $request->get('value');
        $key = $request->get('customerDataSessionKey');        
        
        $customers = $tempDataHelper->get($key, array());    
        if (isset($customers[$idCustomer])) {
            $customers[$idCustomer][$field] = $value;
            $tempDataHelper->save($key, $customers);
        }
        return new Response("OK");
    }

    public function handleInvoicePdfAjaxSource($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $grid = $controller->get('leep_admin.invoice.business.invoice_pdf_grid_reader');

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($grid);

        return new Response(json_encode($resp));
    }

    public function handleGenerateInvoice($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $generator = $controller->get('leep_admin.invoice.business.invoice_generator');

        $idInvoice = $request->get('id', 0);
        $invoice = $doctrine->getRepository('AppDatabaseMainBundle:Invoice')->findOneById($idInvoice);
        $info = $generator->generateInvoice($invoice);

        $invoice->setTotalAmount($info['grandTotal']);

        $invoicePdf = new Entity\InvoicePdf();
        $invoicePdf->setIdInvoice($invoice->getId());
        $invoicePdf->setGenerateDate(new \DateTime());
        $invoicePdf->setInvoiceFile($info['file']);
        $invoicePdf->setTotalAmount($info['grandTotal']);
        $em->persist($invoicePdf);
        $em->flush();
        
        return new Response('OK');
    }

    public function handleViewInvoice($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->get('id', 0);

        $invoicePdf = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:InvoicePdf')->findOneById($id);

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';

        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename=invoice.pdf');
        $resp->setContent(file_get_contents($filePath.'/'.$invoicePdf->getInvoiceFile()));
        return $resp;
    }

    public function handleGenerateReminderNotice($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $request->get('id');
        $type = $request->get('type');

        $statusReminder = Business\Invoice\Constant::INVOICE_STATUS_REMINDERS_SENT;
        $today = new \DateTime();
        $today->setTime(0, 0, 0);

        $query = $em->createQueryBuilder();
        switch ($type) {
            case 'collective_invoice':
                $invoice = $em->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($id);
                if ($invoice->getStatusDate()->getTimestamp() < $today->getTimestamp() ||
                    $invoice->getInvoiceStatus() != $statusReminder) {
                    $query->select('MAX(p.sortorder)')
                        ->from('AppDatabaseMainBundle:CollectiveInvoiceStatus', 'p')
                        ->andWhere('p.collectiveinvoiceid = :id')
                        ->setParameter('id', $id);
                    $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;
                            
                    $newStatus = new Entity\CollectiveInvoiceStatus();
                    $newStatus->setCollectiveInvoiceId($id);
                    $newStatus->setStatus($statusReminder);
                    $newStatus->setStatusDate($today);
                    $newStatus->setSortOrder($sortOrder);
                    $em->persist($newStatus);

                    $invoice->setInvoiceStatus($statusReminder);
                    $invoice->setStatusDate($today);
                    $em->persist($invoice);
                    $em->flush();
                }

                break;
            case 'customer_invoice':
                $invoice = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
                if ($invoice->getInvoiceStatusDate()->getTimestamp() < $today->getTimestamp() ||
                    $invoice->getInvoiceStatus() != $statusReminder) {
                    $query->select('MAX(p.sortorder)')
                        ->from('AppDatabaseMainBundle:InvoiceStatus', 'p')
                        ->andWhere('p.customerid = :id')
                        ->setParameter('id', $id);
                    $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;

                    $newStatus = new Entity\InvoiceStatus();
                    $newStatus->setCustomerId($id);
                    $newStatus->setStatus($statusReminder);
                    $newStatus->setStatusDate($today);
                    $newStatus->setSortOrder($sortOrder);
                    $em->persist($newStatus);

                    $invoice->setInvoiceStatus($statusReminder);
                    $invoice->setInvoiceStatusDate($today);
                    $em->persist($invoice);
                    $em->flush();
                }

                break;
            case 'mwa_invoice':
                $invoice = $em->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($id);
                if ($invoice->getStatusDate()->getTimestamp() < $today->getTimestamp() ||
                    $invoice->getStatus() != $statusReminder) {
                    $query->select('MAX(p.sortOrder)')
                        ->from('AppDatabaseMainBundle:MwaInvoiceStatus', 'p')
                        ->andWhere('p.idMwaInvoice = :id')
                        ->setParameter('id', $id);
                    $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;

                    $newStatus = new Entity\MwaInvoiceStatus();
                    $newStatus->setIdMwaInvoice($id);
                    $newStatus->setStatus($statusReminder);
                    $newStatus->setStatusDate($today);
                    $newStatus->setSortOrder($sortOrder);
                    $em->persist($newStatus);

                    $invoice->setStatus($statusReminder);
                    $invoice->setStatusDate($today);
                    $em->persist($invoice);
                    $em->flush();
                }

                break;
            default:
                die("Error! Invalid type");
        }

        $response = Business\Invoice\InvoiceReminderUtil::buildPdf($controller, $invoice, $type);
        return $response;
    }
}
