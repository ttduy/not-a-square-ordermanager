<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\GenoPelletProduction\Constant;
use Leep\AdminBundle\Business\GenoPelletProduction\Utils;

class GenoPelletProduction extends AbstractFeature {
    public function getActions() { return []; }
    public function getDefaultOptions() { return []; }
    static private $substancesList = [];

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleListFormula($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $name = trim($request->get('name', ""));
        $content = trim($request->get('content', ""));
        $substances = $request->get('substances', []);
        $isClearAll = $request->get('isClearAll', 0);

        if ($isClearAll) {
            $name = "";
            $content = "";
            $substances = [];
        }

        $queryBuilder = $em->createQueryBuilder()
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoPelletFormulas', 'p');

        if ($name != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', "%$name%");
        }

        if ($content != '') {
            $queryBuilder->andWhere('p.content LIKE :content')
                ->setParameter('content', "%$content%");
        }

        $substanceNameList = Business\GenoMaterial\Utils::getSubstanceList($this->container);
        if (!empty($substances)) {
            $substances = array_map('trim', $substances);
            foreach ($substances as $substanceId) {
                $substance = $substanceNameList[$substanceId];
                $queryBuilder->andWhere("p.content LIKE '%/$substance/%'");
            }
        }

        $formulas = $queryBuilder->getQuery()->getResult();

        $data['formulas'] = [];
        foreach ($formulas as $formula) {
            $data['formulas'][] = [
                'id' =>             $formula->getId(),
                'name' =>           $formula->getName(),
                'createdDate' =>    $formatter->format($formula->getCreatedDate(), 'date'),
                'modifiedDate' =>   $formatter->format($formula->getModifiedDate(), 'date'),
                'createdBy' =>      $formatter->format($formula->getCreatedBy(), 'mapping', 'LeepAdmin_WebUser_List'),
                'modifiedBy' =>     $formatter->format($formula->getModifiedBy(), 'mapping', 'LeepAdmin_WebUser_List'),
            ];
        }

        $data['startProductionUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'startProduction');
        $data['name'] = $name;
        $data['content'] = $content;
        $data['selected_substances'] = $substances;
        $data['substances'] = Business\GenoMaterial\Utils::getSubstanceList($controller);

        return $controller->render('LeepAdminBundle:GenoPelletProduction:list_formula.html.twig', $data);
    }

    public function handleRestartProduction($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id', 0);
        $em = $controller->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        // Clear all previous production steps
        $production = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductions')->findOneById($id);
        Business\GenoPelletProduction\Utils::clearAllStep($controller, $id);

        // Parse pellet content
        $content = Business\GenoPelletFormula\Utils::parsePelletContent($controller, $production->getContent());

        // Reset production data
        $production->setStartDate(new \DateTime());
        $production->setStartBy($userManager->getUser()->getId());
        $production->setStatus(Business\GenoPelletProduction\Constant::PRODUCTION_STATUS_NEW);

        $em->persist($production);
        $em->flush();

        // Generate production steps
        Business\GenoPelletProduction\Utils::generateSteps($controller, $content, $production->getId());
        $em->flush();

        return $this->displayProduction($production, $controller, $data, $options);
    }

    public function handleStartProduction($controller, &$data, $options = array()) {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        $formula = $em->getRepository('AppDatabaseMainBundle:GenoPelletFormulas')->findOneById($id);

        // Parse pellet content
        $content = Business\GenoPelletFormula\Utils::parsePelletContent($controller, $formula->getContent());
        if (gettype($content) == "string") {
            return new Response("Pellet formula error: $content");
        }

        // Create new production
        $production = new Entity\GenoPelletProductions();
        $production->setContent($formula->getContent());
        $production->setStartDate(new \DateTime());
        $production->setStartBy($userManager->getUser()->getId());
        $production->setStatus(Business\GenoPelletProduction\Constant::PRODUCTION_STATUS_NEW);
        $production->setFormulaName($formula->getName());

        $em->persist($production);
        $em->flush();

        // Generate production steps
        Business\GenoPelletProduction\Utils::generateSteps($controller, $content, $production->getId());
        $em->flush();

        return $this->displayProduction($production, $controller, $data, $options);
    }

    public function handleViewStepStatus($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');

        $steps = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findBy(
            ['productionId' => $id],
            ['step' => 'ASC']
        );
        $data['logDataList'] = [];
        $stepStatus = Business\GenoPelletProduction\Constant::getProductionStepStatus();
        $stepTypes = Business\GenoPelletProduction\Constant::getProductionStepTypes();
        foreach ($steps as $step) {
            $data['logDataList'][] = [
                'date' =>   $formatter->format($step->getDate(), 'date'),
                'status' => $stepStatus[$step->getStatus()],
                'type' =>   $stepTypes[$step->getType()],
                'value' =>  $step->getValue()
            ];
        }

        $data['checkAddedSubstances'][] = "Run from view step";
        return $controller->render('LeepAdminBundle:GenoPelletProduction:log_view.html.twig', $data);
    }

    public function handleUpdateStep($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $value = trim($controller->get('request')->get('value', 0));

        $step = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneById($id);

        // Clean all previous error
        $step->setError("");
        $type = $step->getType();

        $isSuccess = false;
        $stepValue = json_decode($step->getValue(), true);
        if (empty($value)) {
            $step->setError("Error: Empty value!");
        } else if ($type == Constant::STEP_TYPE_SCAN_ID_CARD) {
            $stepValue['id'] = $value;
            $isSuccess = true;
        } else if ($type == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
            $substanceName = $stepValue['name'];

            // Find lot number
            $lotNumber = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findOneByValue($value);
            if (!$lotNumber) {
                $step->setError("Error: Invalid lot number");
            } else {
                // Use lot number to get batch and then get it's material
                $batch = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findOneById($lotNumber->getBatchId());
                $material = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findOneById($batch->getIdGenoMaterial());

                // Check if this is the correct material
                if ($material->getShortName() != $substanceName) {
                    $step->setError("Error: Incorrect substance! Required $substanceName but ".$material->getShortName()." was input.");
                } else if ($batch->getAmountInStock() <= 0) {
                    $step->setError("Error: This batch is empty! Please find another one.");
                } else {
                    $stepValue['lots'] = $value;
                    $isSuccess = true;
                }
            }
        } else if ($type == Constant::STEP_TYPE_ENTER_AMOUNT) {
            // Get current lot number
            if ($stepValue['critical']) {
                $prevStep = $step->getStep() - 2;
            } else {
                $prevStep = $step->getStep() - 1;
            }

            $prevStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
                'productionId' =>   $step->getProductionId(),
                'step' =>           $prevStep
            ]);

            $lotNumber = json_decode($prevStep->getValue(), true)['lots'];
            $requiredValue = $stepValue['required'];

            // Use lot number to find batch
            $lotNumber = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findOneByValue($lotNumber);
            $batch = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findOneById($lotNumber->getBatchId());

            // Check amount used
            $amountUsed = intval($value);
            if ($amountUsed < 0) {
                $amountUsed = 0;
            }

            if ($amountUsed > $batch->getAmountInStock()) {
                $step->setError("Error: You use more than there are in stock! Currently have: ".$batch->getAmountInStock()."g.");
            } else {
                // Update storage loading
                Business\GenoStorage\Utils::updateStorage($controller, $batch, -intval($amountUsed), "Pellet production #".$step->getProductionId()." step: ".$step->getStep().".");

                // Update required amount
                $requiredValue = $requiredValue - $amountUsed;
                if ($requiredValue < 0) {
                    $requiredValue = 0;
                }

                // If still required more amount, go back to previous step
                $stepValue['amounts'] = $amountUsed;
                if ($requiredValue > 0) {
                    Business\GenoPelletProduction\Utils::appendSubstanceStepAfter(
                        $controller,
                        $step->getProductionId(),
                        $step->getStep(),
                        $stepValue['substance'],
                        $requiredValue,
                        $stepValue['critical']
                    );
                }

                $isSuccess = true;
            }
        } else {
            $stepValue['id'] = $value;
            $isSuccess = true;
        }

        if ($isSuccess) {
            $step->setValue(json_encode($stepValue));
            $step->setStatus(Constant::STEP_STATUS_FINISHED);
            $step->setDate(new \DateTime());

            // Process next step
            $nextStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
                'productionId' =>   $step->getProductionId(),
                'step' =>           ($step->getStep() + 1)
            ]);

            if ($nextStep) {
                $nextStep->setStatus(Constant::STEP_STATUS_PROCESSING);
                $step->setDate(new \DateTime());
                $em->persist($nextStep);
            }
        }

        $em->persist($step);
        $em->flush();

        $production = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductions')->findOneById($step->getProductionId());
        return $this->displayProduction($production, $controller, $data, $options);
    }

    public function handleSkipStep($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        $step = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneById($id);

        // Clean all previous error
        $step->setError("");

        $stepValue = json_decode($step->getValue(), true);
        $step->setStatus(Constant::STEP_STATUS_SKIPED);
        $step->setDate(new \DateTime());
        $em->persist($step);

        $nextStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
            'productionId' =>   $step->getProductionId(),
            'step' =>           ($step->getStep() + 1)
        ]);

        if ($nextStep->getType() == Constant::STEP_TYPE_RE_AUTHORIZE) {
            $nextStep->setStatus(Constant::STEP_STATUS_SKIPED);
            $nextStep->setDate(new \DateTime());
            $em->persist($nextStep);

            $nextStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
                'productionId' =>   $nextStep->getProductionId(),
                'step' =>           ($nextStep->getStep() + 1)
            ]);
        }

        if ($step->getType() == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
            $nextStep->setStatus(Constant::STEP_STATUS_SKIPED);
            $nextStep->setDate(new \DateTime());
            $em->persist($nextStep);

            $nextStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
                'productionId' =>   $nextStep->getProductionId(),
                'step' =>           ($nextStep->getStep() + 1)
            ]);
        }

        if ($nextStep) {
            $nextStep->setStatus(Constant::STEP_STATUS_PROCESSING);
            $nextStep->setDate(new \DateTime());
            $em->persist($nextStep);
        }

        $em->flush();

        $production = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductions')->findOneById($step->getProductionId());
        return $this->displayProduction($production, $controller, $data, $options);
    }

    public function handleShowListSubstance($controller, &$data, $options=array())
    {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr= $controller->get('easy_module.manager');

        // get request information: stepId, type add
        $stepId = json_encode($controller->get('request')->get('stepid'));

        // get subtance data
        $substances = Business\GenoMaterial\Utils::getSubstanceList($this->container);
        $substanceData = [];
        foreach ($substances as $id => $substance) {
            $substanceData[] = ['idSubstance' => $id, 'substanceName' => $substance];
        }

        // chunk substance array
        $substanceData = array_chunk($substanceData, intval(count($substanceData) / 3));
        $data['currentStepId'] = $stepId;
        $data['substancesList'] = $substanceData;
        $data['addSubstancesUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'addSubstances');

        $url = $controller->renderView('LeepAdminBundle:GenoPelletProduction:list_substances.html.twig', $data);

        return new Response($url);
    }

    public function handleAddSubstances($controller, &$data, $options = array())
    {
        $em = $controller->get('doctrine')->getEntityManager();
        $addSubstancesInfos = json_decode($controller->get('request')->get('addSubstancesInfos', 0));

        // addSubstancesInfo is an array have the contents: [current step Id, list substance need to add, type add];
        $currentStepId = $addSubstancesInfos[0];
        $addSubstancesList = $addSubstancesInfos[1];
        $typeAdd = $addSubstancesInfos[2];
        $numberAddedSubstances = $this->countJumpSteps($em, $addSubstancesList);

        $currentStepId = preg_replace('/[^0-9]/', '', $currentStepId);
        $currentStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneById(intval($currentStepId));
        $orderCurrentStep = $currentStep->getStep();

        // get production information
        $productionId = $currentStep->getProductionId();

        $addedStep = [];
        $steps = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findByProductionId($productionId);

        if ($typeAdd == "before") {
            // set the current step to state new
            $currentStep->setStatus(Constant::STEP_STATUS_NEW);
            $em->persist($currentStep);

            // update order step of current step
            foreach ($steps as $step) {
                $orderStep = $step->getStep();
                if ($orderStep >= $orderCurrentStep) {
                    $orderStep +=  $numberAddedSubstances;
                    $step->setStep($orderStep);
                    $em->persist($step);
                }
            }
            $em->flush();

            // set the order step for added substance, added substance's step start from current step's order step
            $newSubstanceOrderStep = $orderCurrentStep;
            $this->setNewSubstances($em, $productionId, $newSubstanceOrderStep, $addSubstancesList, $addedStep);

            // put added substance to database
            $em->flush();

            // set the first substance in new subtance list to in processing
            $firstStep = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy(['productionId' => $productionId,
                                                                                                            'step' => $orderCurrentStep]);

            $firstStep->setStatus(Constant::STEP_STATUS_PROCESSING);
            $em->persist($firstStep);

            // put added substance to database
            $em->flush();
            $data['checkAddedSubstances'][] = "add before successfully";
        }
        elseif ($typeAdd == "after") {
            // set the order step for added substance, their order step start from the last substance exiest in database
            $numberStepSubtance = 1;
            $valueCurrentStep = json_decode($currentStep->getValue(), true);
            if ($this->checkIsCritical($em, $valueCurrentStep['name']) == true) {
                $numberStepSubtance = 2;
            }

            foreach ($steps as $step) {
                $orderStep = $step->getStep();
                if ($orderStep > ($orderCurrentStep + $numberStepSubtance)) {
                    $orderStep +=  $numberAddedSubstances;
                    $step->setStep($orderStep);
                    $em->persist($step);
                }
            }
            $em->flush();

            // set the order step for added substance, added substance's step start from order step of current step
            $newSubstanceOrderStep = $orderCurrentStep + $numberStepSubtance + 1;
            $this->setNewSubstances($em, $productionId, $newSubstanceOrderStep, $addSubstancesList, $addedStep);

            // put added substance to database
            $em->flush();
        }
        $production = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductions')->findOneById($productionId);

        return ($this->displayProduction($production, $controller, $data, $options));
    }

    //--------------------------------------------------------------------------------------------------------------------------------------
    private function displayProduction($production, $controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        // Parse pellet content
        $content = Business\GenoPelletFormula\Utils::parsePelletContent($controller, $production->getContent());

        $check = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findOneBy([
            'productionId' =>   $production->getId(),
            'status' =>         Constant::STEP_STATUS_PROCESSING
        ]);

        if (!$check) {
            $production->setStatus(Constant::PRODUCTION_STATUS_FINISHED);
            $em->persist($production);
            $em->flush();

            // this production is finished! redirect back to pellet formula list
            $data['urlProductionList'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'grid', 'list');
            $data['urlFormulaList'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'listFormula');
            $data['formulaName'] = $production->getFormulaName();
            $data['formulaId'] = $production->getId();

            return $controller->render('LeepAdminBundle:GenoPelletProduction:finished.html.twig', $data);
        }

        $production->setStatus(Constant::PRODUCTION_STATUS_PROCESSING);
        $em->persist($production);
        $em->flush();

        $queryBuilder = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->createQueryBuilder('t');
        $totalStep = $queryBuilder->select('count(t.id)')
            ->where('t.productionId = :productionId')
            ->setParameter('productionId', $production->getId())
            ->getQuery()
            ->getSingleScalarResult();

        // Get all steps
        $steps = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findBy([
            'productionId' =>   $production->getId(),
            'status' =>         [Constant::STEP_STATUS_SKIPED, Constant::STEP_STATUS_FINISHED, Constant::STEP_STATUS_PROCESSING]
        ], [
            'step' => 'DESC'
        ]);

        $data['pelletType'] = $content['type'];
        $data['pelletLotNumber'] = $content['lot'];
        $data['totalAmount'] = $content['total'];
        $data['totalStep'] = $totalStep;
        $data['updateStepUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'updateStep');
        $data['skipStepUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'skipStep');
        $data['showListSubstanceUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'showListSubstance');
        $data['addSubstancesUrl'] = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'addSubstances');
        $data['defaultValue'] = "";

        $data['prevSteps'] = [];
        $stepStatus = Constant::getProductionStepStatus();
        foreach ($steps as $step) {
            // Skip new step
            if ($step->getStatus() == Constant::STEP_STATUS_PROCESSING) {
                $data['currentStep'] = [
                    'id' =>         $step->getId(),
                    'title' =>      $this->generateStepTitle($step),
                    'helper' =>     $this->generateStepHelper($controller, $step),
                    'error' =>      $step->getError(),
                    'skippable' =>  $step->getType() != Constant::STEP_TYPE_SCAN_ID_CARD && $step->getType() != Constant::STEP_TYPE_RE_AUTHORIZE,
                    'isScanSubstance' => '0',
                    'storageSample' => ''
                ];
                if ($step->getType() == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
                    $data['currentStep']['isScanSubstance'] = '1';
                    $data['currentStep']['storageSample'] = $this->getStorageSampleLots($em);
                }
                $data['stepIndex'] = $step->getStep();
                if ($step->getType() == Constant::STEP_TYPE_ENTER_AMOUNT) {
                    $value = json_decode($step->getValue(), true);
                    $data['defaultValue'] = $value['required'];
                }
            } else {
                $data['prevSteps'][] = [
                    'title' =>  $this->generateStepTitle($step),
                    'value' =>  ($step->getStatus() == Constant::STEP_STATUS_SKIPED ? 'Skiped!' : $this->getStepDidplayValue($step)),
                ];
            }
        }

        return $controller->render('LeepAdminBundle:GenoPelletProduction:production_steps.html.twig', $data);
    }

    private function getStepDidplayValue($step) {
        $value = json_decode($step->getValue(), true);
        if ($step->getType() == Constant::STEP_TYPE_SCAN_ID_CARD || $step->getType() == Constant::STEP_TYPE_RE_AUTHORIZE) {
            return $value['id'];
        } else if ($step->getType() == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
            return $value['lots'];
        } else {
            return $value['amounts'];
        }
    }

    private function generateStepTitle($step) {
        $title = Constant::getProductionStepTitle()[$step->getType()];
        if ($step->getType() == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
            $value = json_decode($step->getValue(), true);
            $substanceName = $value['name'];
            $title .= "$substanceName.";
        }

        return "Step ".($step->getStep() + 1).": $title";
    }

    private function generateStepHelper($controller, $step) {
        $em = $controller->get('doctrine')->getEntityManager();
        $result = [
            'msg' =>        '',
            'expected' =>   '',
            'isWarning' =>  false
        ];

        if ($step->getType() == Constant::STEP_TYPE_SCAN_SUBSTANCE) {
            $value = json_decode($step->getValue(), true);

            // Find substance's material
            $material = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findOneByShortName($value['name']);

            // Get all batch that contain material
            $batches = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findBy(
                ['idGenoMaterial' => $material->getId()],
                ['expiryDate' => 'ASC']
            );

            // Locate each batches (With amount greater than zero) location
            $locations = [];
            $today = new \DateTime();
            $locationList = Business\GenoStorage\Utils::getLocationList($controller);
            foreach ($batches as $batch) {
                if ($batch->getAmountInStock() <= 0 ||
                    $batch->getStatus() != Business\GenoBatch\Constant::STATUS_IN_STOCK ||
                    !$batch->getLocation()) {
                    continue;
                }

                $expireMsg = "";
                if ($batch->getExpiryDate()) {
                    $diff = $today->diff($batch->getExpiryDate());
                    if ($diff->invert) {
                        $expireMsg = "Expired! (".$diff->format('%R%ad').").";
                    } else {
                        $expireMsg = "Expired in ".$diff->format('%R%ad').".";
                    }
                }

                $location = $locationList[$batch->getLocation()]." with ".$batch->getAmountInStock()."g. $expireMsg";
                if ((!$batch->getIsStorageSampleCollected()) &&
                    ($this->enableCollectSubstance($em, $batch->getIdGenoMaterial()))) {
                    $result['isWarning'] = true;
                    $location = $location.'<span style="color: #e60000; font-size:140%"> Warning! you need to collect a storage sample of this batch!</span>';
                }
                $locations[] = $location;
            }

            $result['msg'] = "Currently located at:\n".implode("\n", $locations);
        } else if ($step->getType() == Constant::STEP_TYPE_ENTER_AMOUNT) {
            $value = json_decode($step->getValue(), true);
            $result['msg'] = "Required ".$value['required']."g.";
            $result['expected'] = intval($value['required']);
        }

        return $result;
    }

    private function checkIsCritical($em, $shortName)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.isCriticalSubstance')
                ->from('AppDatabaseMainBundle:GenoMaterials', 'p')
                ->andWhere('p.shortName = :shortName')
                ->setParameter('shortName', $shortName);
        $queryResult = $query->getQuery()->getResult();
        $isCritical = $queryResult[0]['isCriticalSubstance'];
        if ($isCritical) {
            return true;
        }
        return false;
    }

    private function setNewSubstances($em, $productionId, &$orderStep, $addSubstancesList, &$addedStep)
    {
        // $currentOrderStep = $orderStep;
        foreach ($addSubstancesList as $substanceId) {
            $query = $em->createQueryBuilder();
            $query->select('p.shortName, p.isCriticalSubstance')
                    ->from('AppDatabaseMainBundle:GenoMaterials', 'p')
                    ->andWhere('p.id = :substanceId')
                    ->setParameter('substanceId', $substanceId);
            $queryResult = $query->getQuery()->getResult();
            $substanceName = $queryResult[0]['shortName'];
            $isCritical = $queryResult[0]['isCriticalSubstance'];

            // create step
            $addedStep[] = Utils::createStepPublic($em, Constant::STEP_TYPE_SCAN_SUBSTANCE, Constant::STEP_STATUS_NEW, [
                'name' => $substanceName,
                'losts' => ''],
                $productionId, $orderStep);
            $orderStep++;

            if ($isCritical) {
                $addedStep[] = Utils::createStepPublic($em, Constant::STEP_TYPE_RE_AUTHORIZE, Constant::STEP_STATUS_NEW, [
                    'id' => ''],
                    $productionId, $orderStep);
                $orderStep++;
            }

            $addedStep[] = Utils::createStepPublic($em, Constant::STEP_TYPE_ENTER_AMOUNT, Constant::STEP_STATUS_NEW, [
                'substance' => $substanceName,
                'required' => 0,
                'amounts' => 0,
                'critical' => $isCritical],
                $productionId, $orderStep);
            $orderStep++;
        }
    }

    private function countJumpSteps($em, $substancesId)
    {
        $countJump = 0;
        foreach ($substancesId as $substanceId) {
            $countJump += 2;
            $query = $em->createQueryBuilder();
            $query->select('p.shortName, p.isCriticalSubstance')
                    ->from('AppDatabaseMainBundle:GenoMaterials', 'p')
                    ->andWhere('p.id = :substanceId')
                    ->setParameter('substanceId', $substanceId);
            $queryResult = $query->getQuery()->getResult();
            $substanceName = $queryResult[0]['shortName'];
            $isCritical = $queryResult[0]['isCriticalSubstance'];

            if ($isCritical) {
                $countJump += 1;
            }
        }

        return $countJump;
    }

    private function getStorageSampleLots($em)
    {
        // init
        $storageSampleLotsList = [];

        // get all internal lot have batch tick storage sample collected
        $trueValue = '1';
        $query = $em->createQueryBuilder();
        $query->select('p.value as lotName, p.batchId')
                ->from('AppDatabaseMainBundle:GenoLotNumbers', 'p')
                ->innerJoin('AppDatabaseMainBundle:GenoBatches', 'gb', 'WITH', 'p.batchId = gb.id')
                ->innerJoin('AppDatabaseMainBundle:GenoMaterials', 'gm', 'WITH', 'gb.idGenoMaterial = gm.id')
                ->where('gb.isStorageSampleCollected = 0')
                ->andWhere('gm.enableCollectStorageSampleWarning = 1');

        $queryResult = $query->getQuery()->getResult();

        // save in array
        foreach ($queryResult as $qr) {
            $storageSampleLotsList[] = preg_replace('/[^A-Za-z0-9\-]/', '', $qr['lotName']);
        }

        // convert to json
        $storageSampleLotsList_json = json_encode($storageSampleLotsList);

        // return value
        return $storageSampleLotsList_json;
    }

    private function enableCollectSubstance($em, $genoMaterialId)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.enableCollectStorageSampleWarning')
                ->from('AppDatabaseMainBundle:GenoMaterials', 'p')
                ->where($query->expr()->eq('p.id', ':idGenoMaterial'))
                ->setParameter('idGenoMaterial', $genoMaterialId);
        $enable = $query->getQuery()->getSingleScalarResult();
        if ($enable) {
            return true;
        }
        return false;
    }
}
