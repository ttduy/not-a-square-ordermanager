<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class TodoTask extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    private function buildDashboard($controller, &$data, $status = Business\TodoTask\Constants::STATUS_ACTIVE) {
        // Apply filter
        $filter = $controller->get('leep_admin.todo_task.business.filter_handler');
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'Form_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Apply filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => '',
            'visiblity' => 'hide'
        );
        if ($controller->getRequest()->getMethod() == 'POST') {
            $data['filter']['visiblity'] = '';
        }

        $parameters = array('status' => $status);

        // Overall goals        
        $gridGoal = $controller->get('leep_admin.todo_task.business.grid_goal_reader');
        $data['gridGoal'] = array(
            'id' => 'GridGoal',
            'title' => 'Overall goals',
            'header' => $gridGoal->getTableHeader(),
            'ajaxSource' => $this->getModule()->getUrl($this->getName(), 'ajaxSourceGoal', $parameters)
        );

        // Overall continual improvement        
        $gridContinualImprovement = $controller->get('leep_admin.todo_task.business.grid_continual_improvement_reader');
        $data['gridContinualImprovement'] = array(
            'id' => 'GridContinualImprovement',
            'title' => 'Overall continual improvement',
            'header' => $gridContinualImprovement->getTableHeader(),
            'ajaxSource' => $this->getModule()->getUrl($this->getName(), 'ajaxSourceContinualImprovement', $parameters)
        );

        // Overall tasks        
        $taskTitle = '';
        switch ($status) {
            case Business\TodoTask\Constants::STATUS_ACTIVE:
                $taskTitle = 'Current task(s)';
                break;
            case Business\TodoTask\Constants::STATUS_COMPLETED:
                $taskTitle = 'Completed task(s)';
                break;
            case Business\TodoTask\Constants::STATUS_DELETED:
                $taskTitle = 'Deleted task(s)';
                break;
        }

        $gridTask = $controller->get('leep_admin.todo_task.business.grid_task_reader');
        $data['gridTask'] = array(
            'id' => 'GridTask',
            'title' => $taskTitle,
            'header' => $gridTask->getTableHeader(),
            'ajaxSource' => $this->getModule()->getUrl($this->getName(), 'ajaxSourceTask', $parameters)
        );
    }

    public function handleAjaxSourceGoal($controller, &$data, $options = array()) {
        $filter = $controller->get('leep_admin.todo_task.business.filter_handler');

        $gridGoal = $controller->get('leep_admin.todo_task.business.grid_goal_reader');
        $gridGoal->filters = $filter->getCurrentFilter();

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($gridGoal);

        return new Response(json_encode($resp));
    }

    public function handleAjaxSourceContinualImprovement($controller, &$data, $options = array()) {
        $filter = $controller->get('leep_admin.todo_task.business.filter_handler');

        $gridContinualImprovement = $controller->get('leep_admin.todo_task.business.grid_continual_improvement_reader');
        $gridContinualImprovement->filters = $filter->getCurrentFilter();

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($gridContinualImprovement);

        return new Response(json_encode($resp));
    }

    public function handleAjaxSourceTask($controller, &$data, $options = array()) {
        $filter = $controller->get('leep_admin.todo_task.business.filter_handler');

        $gridTask = $controller->get('leep_admin.todo_task.business.grid_task_reader');
        $gridTask->filters = $filter->getCurrentFilter();

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($gridTask);

        return new Response(json_encode($resp));
    }

    public function handleDashboard($controller, &$data, $options = array()) {       
        $data['mainTabs']['selected'] = 'current';
        $this->buildDashboard($controller, $data);
        return $controller->render('LeepAdminBundle:TodoTask:dashboard.html.twig', $data);
    }

    public function handleDashboardCompleted($controller, &$data, $options = array()) {         
        $data['mainTabs']['selected'] = 'completed';
        $this->buildDashboard($controller, $data, Business\TodoTask\Constants::STATUS_COMPLETED);
        return $controller->render('LeepAdminBundle:TodoTask:dashboard.html.twig', $data);
    }

    public function handleDashboardDeleted($controller, &$data, $options = array()) {         
        $data['mainTabs']['selected'] = 'deleted';
        $this->buildDashboard($controller, $data, Business\TodoTask\Constants::STATUS_DELETED);
        return $controller->render('LeepAdminBundle:TodoTask:dashboard.html.twig', $data);
    }

    public function handleDeleteTask($controller, &$data, $options = array()) {
        $idTask = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $task = $doctrine->getRepository('AppDatabaseMainBundle:TodoTask')->findOneById($idTask);
        $task->setStatus(Business\TodoTask\Constants::STATUS_DELETED);
        $em->persist($task);
        $em->flush();
        
        return new Response('Done');
    }

    public function handleCompleteTask($controller, &$data, $options = array()) {
        $idTask = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $task = $doctrine->getRepository('AppDatabaseMainBundle:TodoTask')->findOneById($idTask);
        $task->setStatus(Business\TodoTask\Constants::STATUS_COMPLETED);
        $em->persist($task);
        $em->flush();
        
        return new Response('Done');
    }

    public function handleRedoTask($controller, &$data, $options = array()) {
        $idTask = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $task = $doctrine->getRepository('AppDatabaseMainBundle:TodoTask')->findOneById($idTask);
        $task->setStatus(Business\TodoTask\Constants::STATUS_ACTIVE);
        $em->persist($task);
        $em->flush();
        
        return new Response('Done');
    }


    public function handleSetColor($controller, &$data, $options = array()) {
        $idTask = $controller->get('request')->get('id', 0);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $color = $controller->get('request')->get('color', '');

        $task = $doctrine->getRepository('AppDatabaseMainBundle:TodoTask')->findOneById($idTask);
        $task->setColor('#'.trim($color));
        $em->persist($task);
        $em->flush();
        
        return new Response('Done');
    }

    public function handleApplyTaskFilter($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $em = $controller->get('doctrine')->getEntityManager();

        $url = $mgr->getUrl('leep_admin', 'todo_task', 'feature',  'dashboard');        

        $idGoal = $controller->get('request')->get('idGoal', 0);
        $idContinualImprovement = $controller->get('request')->get('idContinualImprovement', 0);
        
        // Filter handlers
        $filterHandler = $controller->get('leep_admin.todo_task.business.filter_handler');
        $sessionId = $filterHandler->getSessionId();
        $filterModel = $filterHandler->getDefaultFormModel();
        $filterModel->webUsers = array();
        if (!empty($idGoal)) {
            $filterModel->goals = array($idGoal);
        }
        if (!empty($idContinualImprovement)) {
            $filterModel->continualImprovements = array($idContinualImprovement);
        }

        // Update filters        
        $session = $controller->get('request')->getSession();          
        $session->set($sessionId, $filterModel);

        // Redirect
        return $controller->redirect($url);
    }
}
