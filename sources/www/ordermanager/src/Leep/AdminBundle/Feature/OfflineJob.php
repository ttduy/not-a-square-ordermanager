<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class OfflineJob extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleViewResult($controller, &$data, $options = array()) {        
        $idOfflineJob = $controller->get('request')->get('id', 0);

        $offlineJob = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OfflineJob')->findOneById($idOfflineJob);
        if ($offlineJob->getStatus() == Business\OfflineJob\Constants::OFFLINE_JOB_FINISHED) { 
            $handler = Business\OfflineJob\Constants::getHandler($offlineJob->getIdHandler(), $this->container);
            return $handler->view($offlineJob);
        }

        die();
    }
}
