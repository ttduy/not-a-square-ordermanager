<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Bill extends AbstractFeature {
    private $contentCSV = '';
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handleGetBillingDeliveryInfo($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);
        $idType = $request->query->get('idType', 0);

        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        $deliveryInfo = $billingHandler->getBillingHandlerByType($idType)->getDeliveryInfo($idType, $id);
        return new Response(json_encode($deliveryInfo));
    }

    public function handleGetBillingInvoiceCustomer($controller, &$data, $options = array()) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);

        $invoiceCustomer = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($id);
        $arr = array(
            'totalAmount' => '',
            'overrideAmount' => ''
        );
        if ($invoiceCustomer) {
            $arr = array(
                'totalAmount'    => $invoiceCustomer->getTotalAmount(),
                'overrideAmount' => $invoiceCustomer->getOverrideAmount(),
                'invoiceAddressIsUsed' => 0
            );

        }
        $customerInfo = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($invoiceCustomer->getIdCustomer());
        if ($customerInfo && $customerInfo->getInvoiceAddressIsUsed()) {
            $arr['invoiceAddressIsUsed'] = $customerInfo->getInvoiceAddressIsUsed();
            $arr['invoiceAddressClientName'] = $customerInfo->getInvoiceAddressClientName();
            $arr['invoiceAddressCompanyName'] = $customerInfo->getInvoiceAddressCompanyName();
            $arr['invoiceAddressStreet'] = $customerInfo->getInvoiceAddressStreet();
            $arr['invoiceAddressPostCode'] = $customerInfo->getInvoiceAddressPostCode();
            $arr['invoiceAddressCity'] = $customerInfo->getInvoiceAddressCity();
            $arr['invoiceAddressIdCountry'] = $customerInfo->getInvoiceAddressIdCountry();
            $arr['invoiceAddressTelephone'] = $customerInfo->getInvoiceAddressTelephone();
            $arr['invoiceAddressFax'] = $customerInfo->getInvoiceAddressFax();
        }
        $resp = new Response();
        $resp->setContent(json_encode($arr));
        return $resp;
    }

    public function handleGetTargetBillingData($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $id = $request->query->get('id', 0);
        $idType = $request->query->get('idType', 0);
        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        if ($id == 0) {
            $billingData = array();
        }
        else {
            $billingData = $billingHandler->getBillingHandlerByType($idType)->getTargetBillingData($idType,$id);
        }
       
        return new Response(json_encode($billingData));
    }

    public function handleOrderSelectorAjaxSource($controller, &$data, $options = array()) {
        $grid = $controller->get('leep_admin.bill.business.order_selector_grid_reader');
        $ajaxSourceHandler = $controller->get('easy_crud.ajax_source_handler');
        $ajaxSourceHandler->setRowVariables(true);
        $resp = $ajaxSourceHandler->handle($grid);
        return new Response(json_encode($resp));
    }

    public function handleBillingFilesAjaxSource($controller, &$data, $options = array()) {
        $grid = $controller->get('leep_admin.bill.business.billing_files_grid_reader');
        $ajaxSourceHandler = $controller->get('easy_crud.ajax_source_handler');
        $resp = $ajaxSourceHandler->handle($grid);
        return new Response(json_encode($resp));
    }
    public function handleBillingManualAjaxSource($controller, &$data, $options = array()) {
        $grid = $controller->get('leep_admin.bill.business.billing_manual_system_grid_reader');
        $ajaxSourceHandler = $controller->get('easy_crud.ajax_source_handler');
        $resp = $ajaxSourceHandler->handle($grid);
        return new Response(json_encode($resp));
    }

    public function handleGenerateBillPdf($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        $billingUtils = $controller->get('leep_admin.billing.business.utils');

        $idBill = $request->get('idBill', 0);
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($idBill);

        $output = $billingHandler->getBillingHandlerByType($bill->getIdType())->generatePdf($bill);
        if (!empty($output)) {
            $urlFileCSV = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/bills/'. $output['filename'].'.csv';
            $myfile = fopen($urlFileCSV, "a+");
            file_put_contents($urlFileCSV, $output['dataCSV']);

            // Make records
            $billFile = new Entity\BillFile();
            $billFile->setIdBill($idBill);
            $billFile->setName($output['name']);
            $billFile->setGenerateDate(new \DateTime());
            $billFile->setBillFilename($output['filename']);
            $billFile->setBillAmount($output['totalAmount']);
            $em->persist($billFile);
            $em->flush();

            $billingUtils->setBillFileAsActive($idBill, $billFile->getId());
            return new Response("SUCCESS");
        }

        return new Response("FAILED");
    }

    public function handleSetAsActivePdf($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $billingUtils = $controller->get('leep_admin.billing.business.utils');

        $billingUtils->setBillFileAsActive($request->get('idBill', 0), $request->get('idBillFile', 0));

        return new Response('SUCCESS');
    }

    public function handleDownloadBillFile($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $billFile = $em->getRepository('AppDatabaseMainBundle:BillFile')->findOneById($request->get('id', 0));
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($billFile->getIdBill());

        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/bills';
        $name = $billFile->getName().'.pdf';

        $fileResponse = new BinaryFileResponse($fileDir.'/'.$billFile->getBillFilename());
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$name);
        return $fileResponse;
    }
    public function handleDownloadCancelBillFile($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($request->get('idBill', 0));

        $query = $em->createQueryBuilder();
        $billFile = $query->select('p')
            ->from('AppDatabaseMainBundle:BillFile', 'p')
            ->andWhere('p.idBill = :idBill')
            ->setParameter('idBill', $bill->getId())
            ->orderBy('p.generateDate', 'DESC')
            ->setMaxResults(1)->getQuery()->getSingleResult();

        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/bills';
        $name = $billFile->getName().'.pdf';

        $fileResponse = new BinaryFileResponse($fileDir.'/'.$billFile->getBillFilename());
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$name);
        return $fileResponse;
    }
    public function handleCancelBill($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($request->get('id', 0));
        $number = $bill->getNumber();
        $number = str_replace('R', 'S', $number);
        $bill->setNumber($number);
        $bill->setIsCancel(1);
        $em->flush();
        return new Response('SUCCESS');
    }

    public function handleDownloadCSVExport($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $billFile = $em->getRepository('AppDatabaseMainBundle:BillFile')->findOneById($request->get('id', 0));
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($billFile->getIdBill());

        $urlFileCSV = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/bills/'. $billFile->getBillFileName().'.csv';
        $fileResponse = new BinaryFileResponse($urlFileCSV);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='. $billFile->getName().'.csv');
        return $fileResponse;
    }
    public function handleDenerateReminderNotice($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $reportHelper = $controller->get('leep_admin.helper.report_helper');
        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        $billingUtils = $controller->get('leep_admin.billing.business.utils');

        $idBill = $request->get('id', 0);
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($idBill);

        // Make status
        $billingUtils->addNewStatus($bill, Business\Bill\Constant::INVOICE_STATUS_REMINDERS_SENT);

        // Generate pdf
        $outputFile = $billingHandler->getBillingHandlerByType($bill->getIdType())->generateReminderNoticePdf($bill);
        $billFile = $em->getRepository('AppDatabaseMainBundle:BillFile')->findOneById($bill->getIdActiveBillFile());
        $reportName = 'reminder_notice_'.$bill->getNumber().'.pdf';
        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/bills';

        return $reportHelper->serveReport(
            $fileDir,
            array(
                $outputFile,
                $billFile->getBillFilename()
            ),
            $reportName
        );
    }

    public function handleDashboard($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mapping = $controller->get('easy_mapping');

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('IFNULL', 'Leep\AdminBundle\Business\Customer\DoctrineExt\IfNull');

        $data['mainTabs'] = array(
            'content' => array(
                'dashboard' => array('title' => 'Bill - Dashboard', 'link' => '#')
            ),
            'selected' => 'dashboard'
        );

        // Invoices
        $data['openForInvoicing'] = array(
            'customer'             => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoiceCustomer', '(p.idBill = 0) OR (p.idBill IS NULL)'),
            'distributionChannel'  => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoiceDistributionChannel', '(p.idBill = 0) OR (p.idBill IS NULL)'),
            'partner'              => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoicePartner', '(p.idBill = 0) OR (p.idBill IS NULL)'),
        );
        $data['invoiced'] = array(
            'customer'             => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoiceCustomer', '(p.idBill != 0)'),
            'distributionChannel'  => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoiceDistributionChannel', '(p.idBill != 0)'),
            'partner'              => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingInvoicePartner', '(p.idBill != 0)'),
        );

        $data['invoiceStatus'] = array(0 => '<None>') + $mapping->getMapping('LeepAdmin_Bill_InvoiceStatus');
        $data['invoiceStatusStatistic'] = array(
            'customer'             => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingInvoiceCustomer'),
            'distributionChannel'  => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingInvoiceDistributionChannel'),
            'partner'              => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingInvoicePartner')
        );

        // Payment
        $data['openForPayment'] = array(
            'distributionChannel'  => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentDistributionChannel', '(p.idBill = 0) OR (p.idBill IS NULL)'),
            'partner'              => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentPartner', '(p.idBill = 0) OR (p.idBill IS NULL)'),
            'acquisiteur'          => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentAcquisiteur', '(p.idBill = 0) OR (p.idBill IS NULL)'),
        );
        $data['payment'] = array(
            'distributionChannel'  => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentDistributionChannel', '(p.idBill != 0)'),
            'partner'              => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentPartner', '(p.idBill != 0)'),
            'acquisiteur'          => Business\Bill\Utils::computeTotalAmount($em, 'AppDatabaseMainBundle:BillingPaymentAcquisiteur', '(p.idBill != 0)'),
        );

        $data['paymentStatus'] = array(0 => '<None>') + $mapping->getMapping('LeepAdmin_Bill_PaymentStatus');
        $data['paymentStatusStatistic'] = array(
            'distributionChannel'  => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingPaymentDistributionChannel'),
            'partner'              => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingPaymentPartner'),
            'acquisiteur'          => Business\Bill\Utils::analysisAmount($em, 'AppDatabaseMainBundle:BillingPaymentAcquisiteur'),
        );

        return $controller->render('LeepAdminBundle:Bill:dashboard.html.twig', $data);
    }

    public function handleViewOrderEntries($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idCustomer = $controller->get('request')->get('idCustomer', 0);
        $idBillingInvoiceCustomer = $controller->get('request')->get('idBillingInvoiceCustomer', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');
        $manager = $controller->get('easy_module.manager');

        if ($idBillingInvoiceCustomer != 0) {
            $billingInvoiceCustomer = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($idBillingInvoiceCustomer);
            $idCustomer = $billingInvoiceCustomer->getIdCustomer();
        }

        $billEntries = array();
        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandler('invoiceCustomer')->getBillingEntries($idCustomer, $billEntries);
        $billingHandler->getBillingHandler('invoiceDistributionChannel')->getBillingEntries($idCustomer, $billEntries);
        $billingHandler->getBillingHandler('invoicePartner')->getBillingEntries($idCustomer, $billEntries);
        $billingHandler->getBillingHandler('paymentDistributionChannel')->getBillingEntries($idCustomer, $billEntries);
        $billingHandler->getBillingHandler('paymentPartner')->getBillingEntries($idCustomer, $billEntries);
        $billingHandler->getBillingHandler('paymentAcquisiteur')->getBillingEntries($idCustomer, $billEntries);

        $entryKeys = array();
        foreach ($billEntries as $billEntry) {
            if (isset($billEntry['entries'])) {
                foreach ($billEntry['entries'] as $k => $t) {
                    $entryKeys[$k] = $t['entry'];
                }
            }
        }
        $data['entryKeys'] = $entryKeys;
        $data['columnCode'] = array('INVOICE', 'PAY_DC', 'PAY_PARTNER', 'PAY_A1', 'PAY_A2', 'PAY_A3');
        $data['billEntries'] = $billEntries;

        $canRollbackPhase1 = true;
        foreach ($billEntries as $billEntry) {
            if (intval($billEntry['idBill']) != 0) {
                $canRollbackPhase1 = false;
            }
        }
        $data['canRollbackPhase1'] = $canRollbackPhase1;

        // Load customer detail

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $data['customer'] = $customer;
        $data['customerOrderDate'] = $formatter->format($customer->getDateOrdered(), 'date');
        $data['customerStatus'] = $formatter->format($customer->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
        $data['customerPriceCategory'] = $formatter->format($customer->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List');
        $data['customerInvoiceGoesTo'] = $formatter->format($customer->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo');
        $data['customerMarginGoesTo'] = $formatter->format($customer->getIdMarginGoesTo(), 'mapping', 'LeepAdmin_DistributionChannel_MarginGoesTo');
        $data['distributionChannel'] = $formatter->format($customer->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
        $data['reportDelivery'] = $formatter->format($customer->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery');
        $data['customerCountry'] = $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
        $data['editCustomerUrl'] = $manager->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $customer->getId()));
        $data['partner'] = '';
        $data['acquisiteurs'] = array();
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        if ($dc) {
            $data['partner'] = $formatter->format($dc->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');
        }

        $acquisiteurs = array();
        if ($customer->getIdAcquisiteur1() != 0){
            $acquisiteurs[] = '1. '.$formatter->format($customer->getIdAcquisiteur1(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate1(), 2).'%)';
        }
        if ($customer->getIdAcquisiteur2() != 0){
            $acquisiteurs[] = '2. '.$formatter->format($customer->getIdAcquisiteur2(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate2(), 2).'%)';
        }
        if ($customer->getIdAcquisiteur3() != 0){
            $acquisiteurs[] = '3. '.$formatter->format($customer->getIdAcquisiteur3(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate3(), 2).'%)';
        }

        $data['acquisiteurs'] = implode('<br/>', $acquisiteurs);

        // Link
        $data['editBillLink'] = $manager->getUrl('leep_admin', 'bill', 'edit', 'edit');
        $data['moveBackPhase1Link'] = $manager->getUrl('leep_admin', 'bill', 'feature', 'moveBackPhase', array('idCustomer' => $idCustomer));

        return $controller->render('LeepAdminBundle:Bill:view_customer_entries.html.twig', $data);
    }

    public function handleMoveBackPhase($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idCustomer = $controller->get('request')->get('idCustomer', 0);

        $billingHandler = $controller->get('leep_admin.billing.business.billing_handler');
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if (empty($customer)) {
            $data['isSuccess'] = false;
            $data['errorMessage'] = 'Cant find customer';
        }
        else {
            $data['orderNumber'] = $customer->getOrderNumber();
            if ($customer->getIsAddedInvoiced() == 1) {
                $billingHandler->getBillingHandler('invoiceCustomer')->removeBillingEntries($idCustomer);
                $billingHandler->getBillingHandler('invoiceDistributionChannel')->removeBillingEntries($idCustomer);
                $billingHandler->getBillingHandler('invoicePartner')->removeBillingEntries($idCustomer);
                $billingHandler->getBillingHandler('paymentDistributionChannel')->removeBillingEntries($idCustomer);
                $billingHandler->getBillingHandler('paymentPartner')->removeBillingEntries($idCustomer);
                $billingHandler->getBillingHandler('paymentAcquisiteur')->removeBillingEntries($idCustomer);

                $customer->setIsAddedInvoiced(0);
                $em->flush();
                $data['isSuccess'] = true;
            }
            else {
                $data['isSuccess'] = false;
                $data['errorMessage'] = 'Already moved to phase 1';
            }
        }

        return $controller->render('LeepAdminBundle:Bill:move_back_phase1.html.twig', $data);
    }
    public function handleAddNewOrder($controller, &$data, $options = array()) {
        $form = $controller->get('leep_admin.bill.business.create_order_handler');
        $form->execute();
        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Create order',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => ''
        );

        return $controller->render('LeepAdminBundle:Bill:create_order.html.twig', $data);
    }
}
