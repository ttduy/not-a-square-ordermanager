<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CryosaveSample extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function getSelectedCommaSamples($controller) {
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = intval($v);
            }
        }
        return $arr;
    }

    public function handleBulkEdit($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();

        $arr = $this->getSelectedCommaSamples($controller);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');

        $form = $controller->get('leep_admin.cryosave_sample.business.bulk_edit_handler');
        $form->selectedId = $arr;
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-update',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'bulkEdit', array('selectedId' => $selectedIdRaw))
        );

        return $controller->render('LeepAdminBundle:CryosaveSample:bulk_edit.html.twig', $data);
    }

    public function handleBulkValidate($controller, &$data, $options = array()) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $queryWrapper = $this->container->get('leep_admin.cryosave_sample.business.query_wrapper');
        $em = $this->container->get('doctrine')->getEntityManager();
        $common = $this->container->get('leep_admin.helper.common');

        $data = array();
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $selectedIds = explode("\n", $selectedIdRaw);
        foreach ($selectedIds as $id) {
            $criteria = array(
                'sampleCode'    => $id
            );

            $queryWrapper->addSearchCriteria($criteria);

            $cryosaveSample = $em->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneBy($criteria);
            if (!$cryosaveSample) {
                $data[] = array(
                    'color'      => 'red',
                    'sampleCode' => $id,
                    'status'     => 'Not found',
                    'result'     => '',
                    'order'      => '',
                    'language'   => '',
                    'report'     => '',
                    'uploaded'   => ''
                );
            }
            else {
                $lab = $em->getRepository('AppDatabaseMainBundle:CryosaveLaboratory')->findOneById($cryosaveSample->getIdLaboratory());
                $externalPdfPath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/external_pdf';
                $result   = file_exists($externalPdfPath.'/Cryosave/'.$lab->getCode().'/'.$cryosaveSample->getSampleCode().'.pdf') ? 'Done' : false;
                $order    = $formatter->format($cryosaveSample->getIdOrder(), 'mapping', 'LeepAdmin_CryosaveSample_Order');
                $language = $formatter->format($cryosaveSample->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List');
                $report = $cryosaveSample->getReportFilename();
                $uploaded = $cryosaveSample->getIsUploaded();

                $data[] = array(
                    'color'      => '',
                    'sampleCode' => $id,
                    'status'     => $formatter->format($cryosaveSample->getCurrentStatus(), 'mapping', 'LeepAdmin_CryosaveSample_Status'),
                    'result'     => $result,
                    'order'      => $order,
                    'language'   => $language,
                    'report'     => !empty($report)  ? "Generated" : '',
                    'uploaded'   => !empty($uploaded) ? "Uploaded" : ''
                );
            }
        }


        return $controller->render('LeepAdminBundle:CryosaveSample:bulk_edit_validate.html.twig', array('data' => $data));
    }

    public function handleDownloadReport($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $cryosaveSample = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneById($id);

        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report/';
        $tempDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
        $downloadFile = $fileDir.'/'.$cryosaveSample->getReportFilename();
        if (!is_file($downloadFile)) {
            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
            $result = $awsHelper->temporaryDownloadBackupFiles('report', $cryosaveSample->getReportFilename());
            if ($result) {
                $downloadFile = "$tempDir/".$cryosaveSample->getReportFilename();
            }
        }

        $fileResponse = new BinaryFileResponse();
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$cryosaveSample->getSampleCode().'.pdf');
        return $fileResponse;
    }

    public function handleExport($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();

        $arr = $this->getSelectedCommaSamples($controller);
        $rows = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CryosaveSample', 'p')
            ->andWhere('p.id IN (:arr)')
            ->setParameter('arr', $arr);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $rows[] = $r->getSampleCode();
        }

        $content = implode("\n", $rows);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=cryosave.csv');
        $response->setContent("\xEF\xBB\xBF".$content);

        return $response;
    }

    public function handleApiCreateSample($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');

        $cryosaveValidator = $controller->get('leep_admin.cryosave_sample.business.cryosave_api_validator');

        // Login
        $username = $request->get('username', '');
        $password = $request->get('password', '');
        $cryosaveUser = $cryosaveValidator->login($username, $password);
        if (!$cryosaveUser) {
            $message = array(
                'result'   => 'ERROR',
                'message'  => "Invalid username/password"
            );
            return new Response(json_encode($message));
        }

        $data = $request->get('data', '');
        $data = explode('_', $data);
        $result = $cryosaveValidator->process($data);

        // If failed
        if (!$result['result']) {
            $message = array(
                'result'   => 'ERROR',
                'message'  => $result['message']
            );
            return new Response(json_encode($message));
        }

        // If success
        $data = $result['data'];
        $cryosaveSample = new Entity\CryosaveSample();
        $cryosaveSample->setIdLaboratory($cryosaveUser->getIdLaboratory());
        $cryosaveSample->setTimestamp($data['Timestamp']);
        $cryosaveSample->setSampleCode($data['SampleCode']);
        $cryosaveSample->setIdSex($data['Sex']);
        $cryosaveSample->setBirthday($data['Birthday']);
        $cryosaveSample->setIdEthnicity($data['Ethnicity']);
        $cryosaveSample->setIdOrder($data['Order']);
        $cryosaveSample->setIdLanguage($data['Language']);
        $cryosaveSample->setIdSampleOrigin($data['SampleOrigin']);
        $cryosaveSample->setDateOfSample($data['DateOfSample']);
        $em->persist($cryosaveSample);
        $em->flush();

        $message = array(
            'result'   => 'SUCCESS',
            'message'  => 'The record has been created successfully'
        );
        return new Response(json_encode($message));
    }
}
