<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;

class FeedbackQuestion extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }


    public function handleForm($controller, &$data, $options = array()) {   
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $config = $controller->get('LeepAdmin.Helper.Common')->getConfig();

        $questionList = array();
        $questions = $em->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findAll();
        foreach ($questions as $question) {
            $questionList[] = array(
                'id'         => $question->getId(),
                'type'       => $question->getIdType(),
                'question'   => $question->getQuestion(),
                'choices'    => Business\FeedbackQuestion\Util::parseChoices($question->getData())
            );
        }

        $data['welcomeMessage'] = nl2br($config->getFeedbackWelcomeMessage());
        $data['questions'] = $questionList;
        $data['actionUrl'] = $mgr->getUrl('leep_admin', 'feedback_question', 'feature', 'formSubmit');

        return $controller->render('LeepAdminBundle:FeedbackQuestion:form.html.twig', $data);
    }

    public function handleFormSubmit($controller, &$data, $options = array()) {   
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $currentTime = new \DateTime();

        $name = $request->get('yourName', '');
        $parameters = $request->request->all();
        foreach ($parameters as $key => $value) {
            if (strpos($key, 'q_') === 0) {
                $feedbackId = substr($key, 2);

                if (empty($value)) {
                    continue;
                }

                $feedbackResponse = new Entity\FeedbackResponse();
                $feedbackResponse->setIdFeedbackQuestion($feedbackId);
                $feedbackResponse->setName($name);
                $feedbackResponse->setAnswer(json_encode($value));
                $feedbackResponse->setResponseTime($currentTime);
                $em->persist($feedbackResponse);
            }
        }
        $em->flush();

        //$this->updateSummary($em);

        return $controller->render('LeepAdminBundle:FeedbackQuestion:formSubmit.html.twig', $data);
    }


    public function handleUpdateStats($controller, &$data, $options = array()) {   
        Business\FeedbackQuestion\Util::updateStats($controller);
        return $controller->render('LeepAdminBundle:FeedbackQuestion:updateStats.html.twig', $data);
    }

    public function handleViewSummary($controller, &$data, $options = array()) {   
        $filter = $controller->get('leep_admin.feedback_question.business.summary_filter_handler');
        $filter->execute();

        $data['filter'] = array(
            'id'       => 'Form_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Apply Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => '',
            'visiblity' => ''
        );
        $filterData = $filter->getCurrentFilter();

        // All period
        $em = $controller->get('doctrine')->getEntityManager();
        $summary = array();

        $questions = $em->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findByIdType(Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_CHOICE); 
        foreach ($questions as $question) {
            $d = array();

            $d['question'] = $question->getQuestion();
            $choices = Business\FeedbackQuestion\Util::parseChoices($question->getData());
            $d['choices'] = $choices;

            // Overall period
            $stats = Business\FeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, null, null, true);
            $d['overallStats'] = $stats['stats'];
            $d['overallAverageScore'] = $stats['averageScore'];
            $d['overallAverageValue'] = $stats['averageValue'];

            // Period 1
            $stats = Business\FeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, $filterData->dateFrom1, $filterData->dateTo1);
            if (empty($stats)) {
                $d['period1'] = false;
            }
            else {
                $d['period1'] = true;
                $d['period1Stats'] = $stats['stats'];
                $d['period1AverageScore'] = $stats['averageScore'];
                $d['period1AverageValue'] = $stats['averageValue'];
            }

            // Period 2
            $stats = Business\FeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, $filterData->dateFrom2, $filterData->dateTo2);
            if (empty($stats)) {
                $d['period2'] = false;
            }
            else {
                $d['period2'] = true;
                $d['period2Stats'] = $stats['stats'];
                $d['period2AverageScore'] = $stats['averageScore'];
                $d['period2AverageValue'] = $stats['averageValue'];
            }

            $summary[] = $d;
        }
        $data['summary'] = $summary;

        return $controller->render('LeepAdminBundle:FeedbackQuestion:viewSummary.html.twig', $data);
    }
}
