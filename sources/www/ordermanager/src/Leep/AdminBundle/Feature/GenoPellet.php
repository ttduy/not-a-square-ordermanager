<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class GenoPellet extends AbstractFeature {
    public function getActions() { return []; }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function getNextSection($idSection) {
        $mapping = $this->container->get('easy_mapping');
        $sections = $mapping->getMapping('LeepAdmin_GenoPellet_Section');

        $found = false;
        foreach ($sections as $id => $section) {
            if ($found) {
                return $id;
            }

            if ($idSection == $id) {
                $found = true;
            }
        }
        return 0;
    }

    public function handleViewStockEntryHistory($controller, &$data, $options = []) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $entry = $controller->get('request')->get('entry', 'idGenoPellet');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $logs = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryHistory')->findBy(
            [$entry => $id],
            ['date' => 'DESC']
        );

        $data['logs'] = [];
        foreach ($logs as $log) {
            $toValue = $log->getToValue();
            if (strcmp($toValue, "Used up") !== 0) {
                $data['logs'][] = [
                'id' =>     $log->getIdGenoPelletEntry(),
                'date' =>   $formatter->format($log->getDate(), 'datetime'),
                'by' =>     $formatter->format($log->getUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                'what' =>   $log->getWhatChange(),
                'from' =>   $log->getFromValue(),
                'to' =>     $toValue,
                ];
            }
            $to[] = $log->getToValue();
        }

        return $controller->render('LeepAdminBundle:GenoPellet:stock_log_view.html.twig', $data);
    }

    public function handleMoveNextSection($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $now = new \DateTime();
        $idPelletEntry = $controller->get('request')->get('idPelletEntry', 0);

        $pelletEntry = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findOneById($idPelletEntry);

        // to be reloaded table data
        $arr = array();

        $msg = 'FAILED';
        if ($pelletEntry) {
            $currentSection = $pelletEntry->getSection();
            $idNextSection = $this->getNextSection($currentSection);

            $sections = Business\GenoPellet\Constant::getSections();
            $sectionFrom = $sections[$currentSection];
            $sectionTo = $sections[$idNextSection];
            Business\GenoPelletEntry\Utils::updateEntryHistory($controller, $this->entity, "Status (Section) [In Geno Pellet production]", $sectionFrom, $sectionTo);

            // save new section
            $pelletEntry->setSection($idNextSection);
            $em->persist($pelletEntry);
            $em->flush();

            $msg = 'SUCCESS';
        }

        return new Response($msg);
    }

    public function handleManageStock($controller, &$data, $options = array()) {
        $data['mainTabs']['content']['manageStock'] = array('title' => 'Manage stock', 'link' => '#');
        $data['mainTabs']['selected'] = 'manageStock';

        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $id = $controller->get('request')->get('id');

        // Load pellet
        $pellet = $em->getRepository('AppDatabaseMainBundle:GenoPellet')->findOneById($id);
        $data['name'] = $pellet->getName();
        $data['letter'] = $pellet->getLetter();

        $grid = $controller->get('leep_admin.geno_pellet_entry.business.grid_reader');

        // Status List
        $data['statusList'] = array();

        $sections = Business\GenoPellet\Constant::getSections();
        foreach ($sections as $sectionId => $sectionName) {
            $data['statusList'][] = array(
                'id'      => $sectionId,
                'name'    => $sectionName,
                'status'  => $sectionId,
                'grid'    => array(
                    'id'          =>  'Grid'.$sectionId,
                    'header'      =>  $grid->getTableHeader(),
                    'ajaxSource'  =>  $mgr->getUrl('leep_admin', 'geno_pellet_entry', 'grid', 'ajaxSource', array('id' => $id, 'status' => $sectionId))
                )
            );
        }

        $data['createPelletEntryLink'] = $mgr->getUrl('leep_admin', 'geno_pellet_entry', 'create', 'create', array('idPellet' => $id));

        return $controller->render('LeepAdminBundle:GenoPellet:manage_stock.html.twig', $data);
    }

    public function handleExportPelletEntries($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        // Prepare model
        $pelletData = array();
        $pellets = $em->getRepository('AppDatabaseMainBundle:GenoPellet')->findBy(array(), array('letter' => 'ASC'));
        foreach ($pellets as $p) {
            $pelletData[$p->getId()] = array(
                'pellet'    => $p,
                'hasData'   => false,
                'entries'   => array()
            );
        }

        $pelletEntries = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findBy(array(), array('shipment' => 'ASC'));
        foreach ($pelletEntries as $entry) {
            $idPellet = intval($entry->getIdPellet());
            $idSection = intval($entry->getSection());

            if (isset($pelletData[$idPellet]) && in_array($idSection, array(30, 40, 50))) {
                if (!isset($pelletData[$idPellet]['entries'][$idSection])) {
                    $pelletData[$idPellet]['entries'][$idSection] = array();
                }

                if (intval($entry->getLoading()) > 0) {
                    $pelletData[$idPellet]['entries'][$idSection][] = $entry;
                    $pelletData[$idPellet]['hasData'] = true;
                }
            }
        }

        // Export Excel
        $currentRow = 4;
        $today = new \DateTime();
        $tmpFolder = $controller->get('service_container')->getParameter('temp_dir');
        $tmpFile = 'geno_pellet_'.$today->format('YmdHis').'.xlsx';

        $objPHPExcel = new \PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();;
        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(30);
        $s = 'C';
        for ($i = 0; $i < 21; $i++) {
            $sheet->getColumnDimension($s)->setWidth(16);
            $s++;
        }

        // Print header
        $sheet->setCellValue('A'.$currentRow, 'Pellet Number');
        $sheet->mergeCells('A'.$currentRow.':A'.($currentRow+1));
        $sheet->setCellValue('B'.$currentRow, 'Pellet Name');
        $sheet->mergeCells('B'.$currentRow.':B'.($currentRow+1));
        $sheet->mergeCells('C'.$currentRow.':G'.$currentRow);
        $sheet->setCellValue('C'.$currentRow, 'Received but not tested');
        $sheet->mergeCells('H'.$currentRow.':L'.$currentRow);
        $sheet->setCellValue('H'.$currentRow, 'Tested and in Stock');
        $sheet->mergeCells('M'.$currentRow.':Q'.$currentRow);
        $sheet->setCellValue('M'.$currentRow, 'Tested and in Use');
        $sheet->getStyle('A'.$currentRow.':Q'.($currentRow+1))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
        $sheet->getStyle('A'.$currentRow.':Q'.($currentRow+1))
            ->applyFromArray(array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'D9D9D9')
                )
            ));
        foreach (array('C', 'H', 'M') as $start) {
            $s = $start;
            $sheet->setCellValue($s.($currentRow+1), 'Lot Nr.'); $s++;
            $sheet->setCellValue($s.($currentRow+1), 'Stock (kg)'); $s++;
            $sheet->setCellValue($s.($currentRow+1), 'Exp. Date'); $s++;
            $sheet->setCellValue($s.($currentRow+1), 'Stock Lab (kg)'); $s++;
            $sheet->setCellValue($s.($currentRow+1), 'Exp. D. Lab'); $s++;
            // $sheet->setCellValue($s.($currentRow+1), 'Stock Store (kg)'); $s++;
            // $sheet->setCellValue($s.($currentRow+1), 'Exp. D. Store'); $s++;
        }
        $currentRow += 2;

        foreach ($pelletData as $pellet) {
            //if (!$pellet['hasData']) continue;

            // Print content
            $sheet->setCellValue('A'.$currentRow, $pellet['pellet']->getLetter());
            $sheet->setCellValue('B'.$currentRow, $pellet['pellet']->getName());
            if (!$pellet['hasData']) {
                $color = new \PHPExcel_Style_Color();
                $color->setRGB('999999');
                $sheet->getStyle('A'.$currentRow.':B'.$currentRow)
                    ->getFont()
                    ->setColor($color);
            }


            $step = $currentRow;
            if (isset($pellet['entries'][30])) {
                $row = $currentRow;
                foreach ($pellet['entries'][30] as $entry) {
                    $sheet->setCellValue('C'.$row, $entry->getShipment());
                    $sheet->setCellValue('D'.$row, round($entry->getLoading() / 1000, 2));
                    $sheet->setCellValue('E'.$row, $formatter->format($entry->getExpiryDate(), 'date'));
                    $row++;
                }
                $step = max($row-1, $step);
            }
            if (isset($pellet['entries'][40])) {
                $row = $currentRow;
                foreach ($pellet['entries'][40] as $entry) {
                    $sheet->setCellValue('H'.$row, $entry->getShipment());
                    $sheet->setCellValue('I'.$row, round($entry->getLoading() / 1000, 2));
                    $sheet->setCellValue('J'.$row, $formatter->format($entry->getExpiryDate(), 'date'));
                    $row++;
                }
                $step = max($row-1, $step);
            }
            if (isset($pellet['entries'][50])) {
                $row = $currentRow;
                foreach ($pellet['entries'][50] as $entry) {
                    $sheet->setCellValue('M'.$row, $entry->getShipment());
                    $sheet->setCellValue('N'.$row, round($entry->getLoading() / 1000, 2));
                    $sheet->setCellValue('O'.$row, $formatter->format($entry->getExpiryDate(), 'date'));
                    $row++;
                }
                $step = max($row-1, $step);
            }


            $sheet->getStyle('A'.$currentRow.':Q'.$step)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);

            $sheet->getStyle('F'.$currentRow.':G'.$step)->applyFromArray(array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E2F0D9')
                )
            ));
            $sheet->getStyle('K'.$currentRow.':L'.$step)->applyFromArray(array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E2F0D9')
                )
            ));
            $sheet->getStyle('P'.$currentRow.':Q'.$step)->applyFromArray(array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E2F0D9')
                )
            ));

            $currentRow = $step + 1;
        }

        // Print
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($tmpFolder.'/'.$tmpFile);

        $fileResponse = new BinaryFileResponse($tmpFolder.'/'.$tmpFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename=Pellet'.$today->format('mdY').'.xlsx');
        return $fileResponse;
    }
}
