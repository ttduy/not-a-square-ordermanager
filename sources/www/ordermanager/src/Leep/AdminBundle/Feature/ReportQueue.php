<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class ReportQueue extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }


    public function handleViewLog($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $idReportQueue = $controller->get('request')->get('id', 0);
        $data['idReportQueue'] = $idReportQueue;

        $logs = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportQueueLog', 'p')
            ->andWhere('p.idReportQueue = :idReportQueue')
            ->orderBy('p.logTime', 'ASC')
            ->setParameter('idReportQueue', $idReportQueue);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $logs[] = array(
                'time' => $formatter->format($r->getLogTime(), 'datetime'),
                'log'  => $r->getLogText()
            );
        }

        $data['logs'] = $logs;

        return $controller->render('LeepAdminBundle:ReportQueue:log.html.twig', $data);
    }

    public function handleDelete($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idReportQueue = $controller->get('request')->get('id', 0);
        $reportQueue = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        $em->remove($reportQueue);
        $em->flush();

        $this->deleteLog($controller, $idReportQueue);

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'report_queue', 'grid', 'list');
        return new RedirectResponse($url);
    }

    public function handleReset($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idReportQueue = $controller->get('request')->get('id', 0);
        $reportQueue = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
        $reportQueue->setProgress(0);
        $reportQueue->setIsWarning(0);
        $reportQueue->setLastUpdated(null);
        $reportQueue->setOutput(null);
        $em->flush();

        $this->deleteLog($controller, $idReportQueue);

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'report_queue', 'grid', 'list');
        return new RedirectResponse($url);
    }

    public function handleMarkImportant($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idReportQueue = $controller->get('request')->get('id', 0);
        $reportQueue = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        $reportQueue->setIdPriority(1);
        $em->flush();

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'report_queue', 'grid', 'list');
        return new RedirectResponse($url);
    }

    public function handleUnmarkImportant($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idReportQueue = $controller->get('request')->get('id', 0);
        $reportQueue = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        $reportQueue->setIdPriority(null);
        $em->flush();

        $mgr = $controller->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'report_queue', 'grid', 'list');
        return new RedirectResponse($url);
    }

    protected function deleteLog($controller, $idReportQueue) {
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ReportQueueLog', 'p')
            ->andWhere('p.idReportQueue = :idReportQueue')
            ->setParameter('idReportQueue', $idReportQueue)
            ->getQuery()->execute();

    }

    public function handleViewResult($controller, &$data, $options = array()) {
        $idReportQueue = $controller->get('request')->get('id', 0);
        $reportHelper = $controller->get('leep_admin.helper.report_helper');
        $isLowResolution = $controller->get('request')->get('isLowResolution', 0);
        $isExcel = $controller->get('request')->get('isExcel', 0);
        $isExcel2 = $controller->get('request')->get('isExcel2', 0);
        $isXml = $controller->get('request')->get('isXml', 0);

        $reportQueue = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if ($reportQueue->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED ||
            $reportQueue->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD) {
            $output = @unserialize($reportQueue->getOutput());
            if (isset($output['filename'])) {
                $reportName = $this->getReportQueueFilename($controller, $reportQueue);
                $reportName = str_replace('&', '_', $reportName);
                $reportName = str_replace(',', '_', $reportName);
                $reportName = str_replace('+', '_', $reportName);

                if ($reportQueue->getIdJobType() == Business\ReportQueue\Constant::JOB_TYPE_BUILD_CUSTOMER_REPORT
                    || $reportQueue->getIdJobType() == Business\ReportQueue\Constant::JOB_TYPE_BUILD_DEMO_REPORT) {
                    $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report/';
                    $tempDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
                    $files = array();
                    if ($isLowResolution) {
                        if (isset($output['filename_web_cover_low_resolution'])) {
                            $files[] = $output['filename_web_cover_low_resolution'];
                        }
                        if (isset($output['filename_low_resolution'])) {
                            $files[] = $output['filename_low_resolution'];
                        }
                        if (isset($output['filename_bod_cover_low_resolution'])) {
                            $files[] = $output['filename_bod_cover_low_resolution'];
                        }
                    } else if ($isExcel) {
                        $filename = $output['filename_food_table_excel'];
                        $downloadFile = $fileDir.$filename;
                        if (!is_file($downloadFile)) {
                            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                            $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                            if ($result) {
                                $downloadFile = "$tempDir/$filename";
                            }
                        }

                        $fileResponse = new BinaryFileResponse($downloadFile);
                        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xlsx');
                        return $fileResponse;
                    } elseif ($isExcel2) {
                        $filename = $output['filename_food_table_excel_2'];
                        $downloadFile = $fileDir.$filename;
                        if (!is_file($downloadFile)) {
                            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                            $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                            if ($result) {
                                $downloadFile = "$tempDir/$filename";
                            }
                        }

                        $fileResponse = new BinaryFileResponse($downloadFile);
                        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xlsx');
                        return $fileResponse;
                    } else if ($isXml) {
                        $filename = $output['filename_report_xml'];
                        $downloadFile = $fileDir.$filename;
                        if (!is_file($downloadFile)) {
                            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                            $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                            if ($result) {
                                $downloadFile = "$tempDir/$filename";
                            }
                        }

                        $fileResponse = new BinaryFileResponse($downloadFile);
                        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xml');
                        return $fileResponse;
                    } else {
                        if (isset($output['filename_web_cover'])) {
                            $files[] = $output['filename_web_cover'];
                        }
                        if (isset($output['filename'])) {
                            $files[] = $output['filename'];
                        }
                        if (isset($output['filename_bod_cover'])) {
                            $files[] = $output['filename_bod_cover'];
                        }
                    }

                    return $reportHelper->serveReport($fileDir, $files, $reportName);
                }
                else {
                    $filename = $output['filename'];
                    if ($isLowResolution) {
                        $filename = $output['filename_low_resolution'];
                    }

                    $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report/';
                    $tempDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
                    $downloadFile = $fileDir.$filename;
                    if ($reportQueue->getIdJobType() == Business\ReportQueue\Constant::JOB_TYPE_BUILD_SHIPMENT) {
                        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/shipment/';
                        $downloadFile = $fileDir.$filename;
                        if (!is_file($downloadFile)) {
                            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                            $result = $awsHelper->temporaryDownloadBackupFiles('shipment', $filename);
                            if ($result) {
                                $downloadFile = "$tempDir/$filename";
                            }
                        }
                    } else {
                        if (!is_file($downloadFile)) {
                            $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                            $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                            if ($result) {
                                $downloadFile = "$tempDir/$filename";
                            }
                        }
                    }

                    $fileResponse = new BinaryFileResponse($downloadFile);
                    $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                    $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName);
                    return $fileResponse;
                }
            }
        }

        die();
    }

    protected function getReportQueueFilename($controller, $reportQueue) {
        $em = $controller->get('doctrine')->getEntityManager();
        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($reportQueue->getIdReport());
        switch ($reportQueue->getIdJobType()) {
            case Business\ReportQueue\Constant::JOB_TYPE_BUILD_DEMO_REPORT:
                return 'Demo - '.$report->getName().'.pdf';
            case Business\ReportQueue\Constant::JOB_TYPE_BUILD_CUSTOMER_REPORT:
                $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($reportQueue->getIdCustomer());
                return $customer->getOrderNumber().' - '.$report->getName().'.pdf';
            case Business\ReportQueue\Constant::JOB_TYPE_BUILD_SHIPMENT:
                $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($reportQueue->getIdCustomer());
                $input = unserialize($reportQueue->getInput());
                $shipmentId = Business\BodShipment\Utils::getShipmentId($controller, $input['idBodShipment']);

                return $customer->getOrderNumber().'_'.$shipmentId.'.zip';
            case Business\ReportQueue\Constant::JOB_TYPE_BUILD_CRYOSAVE_REPORT:
                $cryosaveSample = $em->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneById($reportQueue->getIdCustomer());
                return $cryosaveSample->getSampleCode().' - '.$report->getName().'.pdf';

            case Business\ReportQueue\Constant::JOB_TYPE_BUILD_DISTRIBUTION_CHANNEL_PRICE:
                $input = unserialize($reportQueue->getInput());
                return $input['reportData']['dcName'].'.pdf';
        }

        return 'file.pdf';
    }
}
