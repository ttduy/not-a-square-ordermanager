<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class GenoMaterial extends AbstractFeature {
    public $filters;
    public function getActions() { return []; }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }


    public function handleExportMaterial($controller, &$data, $options = array()) {

       
        $formatter = $controller->get('leep_admin.helper.formatter');
        $typeList = Business\GenoMaterialType\Utils::getGenoMaterialTypeList($this->container);
        
       
        //new code
        $filters=$this->container->get('request')->getSession()->get('GenoMaterialFilterHandler');
        $em = $controller->get('doctrine')->getEntityManager();
        $queryBuilder=$controller->get('doctrine')->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('p')->from('AppDatabaseMainBundle:GenoMaterials','p');
        if (trim($filters->shortName) != ''){
                
                $queryBuilder->andWhere('p.shortName LIKE :shortName')
                ->setParameter('shortName', '%'.trim($filters->shortName).'%');
        }

        if (trim($filters->fullName) != ''){
                $queryBuilder->andWhere('p.fullName LIKE :fullName')
                ->setParameter('fullName', '%'.trim($this->filters->fullName).'%');
        }

            // get type
        if(!empty($filters->types)){
                $queryBuilder->andWhere('p.type in (:types)')->setParameter('types', $filters->types);
        }
        $queryBuilder->orderBy('p.shortName', 'ASC');
        $materials = $queryBuilder->getQuery()->getResult();
        //<end mycode>
        // Prepare model
        $materialData = array();
        $materialData = $this->generateData($materials);
        // Export Excel
        $today = new \DateTime();
        $tmpFolder = $controller->get('service_container')->getParameter('temp_dir');
        $tmpFile = 'geno_material_'.$today->format('YmdHis').'.xlsx';

        $objPHPExcel = new \PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();;
        $sheet->getColumnDimension('A')->setWidth(50);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $arrayLabelCell = range('A', 'Z');
        $arrayHeadText = ['Status', 'Location', 'Expiry Date', 'External Lot', 'Stock In Batch'];
        $arrayHeadKey = ['status', 'location', 'expiryDate', 'externalLot', 'stockBatch'];

        // Print header
        $sheet->setCellValue('A1', 'FULL NAME');
        $sheet->mergeCells('A1:A2');

        $sheet->setCellValue('B1', 'TARGET LOAD');
        $sheet->mergeCells('B1:B2');

        $sheet->setCellValue('C1', 'STOCK');
        $sheet->mergeCells('C1:C2');
        $sheet->setCellValue('D1', 'BATCHES');
        $sheet->mergeCells('D1:E1');

        $sheet->setCellValue('F1', 'CHECK');
        $sheet->mergeCells('F1:H1');

        $rowId = 2;

        // Set head Batch
        for($i = 0; $i< count($arrayHeadText); $i++) {
            $sheet->setCellValue($arrayLabelCell[$i+3].$rowId, $arrayHeadText[$i]);
            $sheet->getColumnDimension($arrayLabelCell[$i+3])->setAutoSize(true);
        }

        // set Cell Null
        $sheet->setCellValue('I1', 'CHECK');
        $sheet->mergeCells('I1:K1');
        $sheet->setCellValue('I2', 'Expiry Date');
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->setCellValue('J2', 'External Lot');
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->setCellValue('K2', 'Stock In Batch');
        $sheet->getColumnDimension('K')->setAutoSize(true);

        // set style Head
        $sheet->getStyle('A1:K1')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);

        $sheet->getStyle('A1:K1')
            ->applyFromArray(array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'D9D9D9')
                )
            ));
        $sheet->getStyle('A2:K2')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);

        $sheet->getStyle('D2:K2')
            ->applyFromArray(array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'D9D9D9')
                )
            ));
        $sheet->getStyle('F1:F2')
                ->applyFromArray(array(
                    'font' => [
                        'color' => array('rgb' => 'f44336')
                        ]
                ));
        // Set style Cell Null
        $sheet->getStyle('I1:I2')
                ->applyFromArray(array(
                    'font' => [
                        'color' => array('rgb' => 'f44336')
                        ]
                ));
        $sheet->getStyle('F2:K2')
                ->applyFromArray(array(
                    'font' => [
                        'color' => array('rgb' => 'f44336')
                        ]
                ));
        ///////
        $rowId = 3;
        foreach ($materialData as $key => $value) {
            if (count($value['batch']) > 0) {
                $end = count($value['batch'])+ $rowId;
                $end = (($end -1) >= $rowId) ? $end-1 : $end;
                $sheet->setCellValue('A'. $rowId, $value['fullName']);
                $sheet->mergeCells('A'. $rowId.':A'. ($end));

                $sheet->setCellValue('B'. $rowId, $value['targetLoad']);
                $sheet->mergeCells('B'. $rowId.':B'. ($end));

                $sheet->getStyle('B'. $rowId)->getAlignment()->setHorizontal(
                    \PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                );

                $sheet->setCellValue('C'. $rowId, $value['stock']);
                $sheet->mergeCells('C'. $rowId.':C'. ($end));

                $sheet->getStyle('C'. $rowId)->getAlignment()->setHorizontal(
                    \PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                );

                $rowCol = $rowId;
                foreach ($value['batch'] as $key => $batch) {
                    for($i = 0; $i< count($arrayHeadKey); $i++) {
                        $sheet->setCellValue($arrayLabelCell[$i+3].$rowCol, $batch[$arrayHeadKey[$i]]);
                        $sheet->getStyle($arrayLabelCell[$i+3].$rowCol)->getAlignment()->setHorizontal(
                            \PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                        );
                    }

                    // font color CHECK
                    $sheet->getStyle('F'. $rowCol.':H'. $rowCol)
                            ->applyFromArray(array(
                                'font' => [
                                    'color' => array('rgb' => 'f44336')
                                    ]
                            ));
                    $rowCol++;
                }
                $rowId= $end+1;
            }
        }
        // my code
        $rowId=$rowId+5;
        $sheet->setCellValue('A'. $rowId, 'Allgemeine Information:');
        $sheet->getStyle('A'. $rowId)
            ->applyFromArray(array(
                'font' => [
                        'color' => array('rgb' => 'ffffff')
                        ],
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '808080')
                )
            ))
            ->getFont()->setBold(true);

        $rowId=$rowId+1;
        $sheet->setCellValue('A'. $rowId, 'Weiße Tonnen mit rotem Deckel');
        $sheet->getStyle('A'. $rowId)
            ->applyFromArray(array(
                
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '969696')
                )
            ));
        $rowId=$rowId+1;
        $arrayKeyNr=array('Nr. 7068 = 3120g','Nr. 7055 = 2680g','Nr. 7026 = 1200g','Nr. 7015 = 1000g','Nr. 7011 = 800g','Nr. 7006 = 318g');
        $char='A';
        for($i=0;$i<count($arrayKeyNr);$i++){
            $sheet->setCellValue($char. $rowId, $arrayKeyNr[$i]);
            if($i<count($arrayKeyNr)-1)
            $char ++;
        }
        $sheet->getStyle('A'. $rowId. ':' .$char. $rowId)
            ->applyFromArray(array(
                
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C0C0C0')
                )
            ));
        $rowId=$rowId+1;
        $sheet->setCellValue('A'. $rowId, 'Lock and lock boxen');
        $sheet->getStyle('A'. $rowId)
            ->applyFromArray(array(
                
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '969696')
                )
            ));
        $rowId=$rowId+1;
        $arrayKeyl=array('5.5 l = 604g','3.9 l = 339g','2.3 l = 248g','0.6 l = 120g','0.35 l = 81g');
        $char='A';
        for($i=0;$i<count($arrayKeyl);$i++){
            $sheet->setCellValue($char. $rowId, $arrayKeyl[$i]);
            if($i<count($arrayKeyl)-1)
            $char ++;
        }
        $sheet->getStyle('A'. $rowId. ':' .$char. $rowId)
            ->applyFromArray(array(
                
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C0C0C0')
                )
            ));
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save($tmpFolder.'/'.$tmpFile);

        $fileResponse = new BinaryFileResponse($tmpFolder.'/'.$tmpFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename=material'.$today->format('mdYHis').'.xlsx');
        return $fileResponse;
    }
    private function generateData($materials) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $data = [];
        foreach ($materials as $key => $material) {
            $data[$material->getId()] = [];
            $batches = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findByIdGenoMaterial($material->getId());
            $q = $em->createQueryBuilder();
            $batches = $q->select('p')
                        ->from('AppDatabaseMainBundle:GenoBatches', 'p')
                        ->andWhere('p.idGenoMaterial =:idGenoMaterial')
                        ->setParameter('idGenoMaterial', $material->getId())
                        ->andWhere('p.status != :status_order')
                        ->setParameter('status_order', Business\GenoBatch\Constant::STATUS_ORDERED)
                        ->andWhere('p.status != :status_used_up')
                        ->setParameter('status_used_up', Business\GenoBatch\Constant::STATUS_USED_UP)->getQuery()->getResult();
            $dataBatch = [];
            $sumWeight = 0;
            $sumPrice = 0;
            foreach ($batches as $key => $batch) {
                $query = $em->createQueryBuilder();
                $rs = $query->select('sto.name as storageName', 'sto.warningThreshold as warningThreshold', 'cell.name as cellName', 'cell.level as level')
                    ->from('AppDatabaseMainBundle:GenoStorages', 'sto')
                    ->innerJoin('AppDatabaseMainBundle:GenoStorageCell', 'cell', 'with', 'sto.id = cell.storageId')
                    ->andWhere('cell.id = :location')
                    ->setParameter('location', $batch->getLocation())
                    ->getQuery()->getOneOrNullResult();
                // set data is null
                $dataBatch [$batch->getId()] = [];
                $dataBatch [$batch->getId()] ['supplierName'] = '';
                $dataBatch [$batch->getId()] ['externalLot']  = '';
                $dataBatch [$batch->getId()] ['expiryDate']   = '';
                $dataBatch [$batch->getId()] ['status']       = '';
                $dataBatch [$batch->getId()] ['location']     = '';
                $dataBatch [$batch->getId()] ['stockBatch']   = '';
                if (count($rs) != 0) {
                    $dataBatch [$batch->getId()] ['location']    = $rs['storageName']. ' => '. $rs['cellName']. '('. $rs['level'].')';
                }
                $expiryDate = $batch->getExpiryDate();
                $expiryDate = !empty($expiryDate) ? $expiryDate->format('d/m/Y') : '';

                // stock in Batch
                $amountBatch = floatval($batch->getAmountInStock());
                $price   = floatval(floatval($batch->getAmountInStock()) * floatval($batch->getPricePerGram()));
                $dataBatch [$batch->getId()] ['supplierName'] = $batch->getSupplierName();
                $dataBatch [$batch->getId()] ['externalLot']  = $batch->getExternalLot();
                $dataBatch [$batch->getId()] ['expiryDate']   = $expiryDate;
                $dataBatch [$batch->getId()] ['stockBatch']   = (number_format($amountBatch, 2)).'/ €'.(number_format($price, 2));
                $dataBatch [$batch->getId()] ['status']       = Business\GenoBatch\Constant::getStatus()[$batch->getStatus()];

                $sumWeight += floatval($batch->getAmountInStock());
                $sumPrice += floatval(floatval($batch->getAmountInStock()) * floatval($batch->getPricePerGram()));
            }
            $data[$material->getId()]['fullName'] = $material->getFullName();
            $data[$material->getId()]['batch']  = $dataBatch;
            $data[$material->getId()]['targetLoad'] = $material->getTargetLoad();
            $data[$material->getId()]['stock'] = (number_format($sumWeight, 2)).'/ €'.(number_format($sumPrice, 2));
        }
        return $data;
    }
}
