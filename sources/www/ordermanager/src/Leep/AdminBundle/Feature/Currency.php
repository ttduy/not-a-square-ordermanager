<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class Currency extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleGetCurrencyRate($controller, &$data, $options = array()) {
        $idCurrency = $controller->get('request')->get('idCurrency', 0);

        $currency = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Currency')->findOneById($idCurrency);
        return new Response($currency->getExchangeRate());
    }
}
