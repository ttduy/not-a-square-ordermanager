<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\RedirectResponse;


class LeadType extends AbstractFeature {
	public function getActions() {
		return [];
	}

	public $container;

	public function __construct($container) {
		$this->container = $container;
	}

	public function handleViewLeadEntry($controller, &$data, $options=[]){
		$em = $controller->get('doctrine')->getEntityManager();
		$id = $controller->get('request')->get('id', 0);

		$formatter = $controller->get('leep_admin.helper.formatter');

		// title of popup
		$title = $em->getRepository('AppDatabaseMainBundle:LeadType')->findBy(['id' => $id]);

		$data['title'] = [];
		foreach ($title as $log){
			$data['title'][] = [
				'type'			=> $log->getType(),
				'description'	=> $log->getDescription(),
			];
		}

		// header of table => only the name of status (First Contact, Second Contact,...)
		$header = $em->createQueryBuilder();
		$header->select('p.name')
				->from("AppDatabaseMainBundle:LeadStatusValue", 'p')
				->innerJoin("AppDatabaseMainBundle:LeadCategoryStatus", 'q', 'WITH', 'q.idLeadStatus = p.id')
				->innerJoin("AppDatabaseMainBundle:LeadType", 'r', 'WITH', 'r.id = q.idLeadCategory')
				->where('r.id = :id')
				->setParameter('id', $id);
		$result = $header->getQuery()->getResult();

		$data['header'] = [];		
		foreach ($result as $r) {
			$data['header'][] = [
				'name'	=> $r['name'],
			];
		}

		// body of table => contains the name of lead and the status value
		$body = $em->createQueryBuilder();

		$body->select('p.dateDiff, q.name, q.id as Lid, p.id as LSid')
				->from("AppDatabaseMainBundle:LeadStatus", 'p')
				->innerJoin("AppDatabaseMainBundle:Lead", 'q', 'WITH', 'q.id = p.idLead')
				->innerJoin("AppDatabaseMainBundle:LeadType", 'r', 'WITH', 'r.id = q.idCategory')
				->where('r.id = :id')
				->setParameter('id', $id);
		$res = $body->getQuery()->getResult();



		$body_ = [];
		foreach ($res as $r) {
			if(!isset($body_[$r['Lid']])) {
				$body_[$r['Lid']] = ['leadName'	=> $r['name'], 'leadStatus'	=> [$r['LSid']	=> $r['dateDiff']]];
			}
			else{
				$body_[$r['Lid']]['leadStatus'][$r['LSid']] = $r['dateDiff'];
			}
		}

		$data['body'] = $body_;

		

		// user
		$user = $em->createQueryBuilder();
		$data['user'] = [];
		$user->select('p.username')
				->from("AppDatabaseMainBundle:WebUsers", 'p');
		$res_ = $user->getQuery()->getResult();

		foreach($res_ as $r){
			$data['user'][] = [
				'username'	=> $r['username'],
			];
		}

		// // distribution channel
		$dc = $em->createQueryBuilder();
		$dc->select('p.distributionchannel')
			->from("AppDatabaseMainBundle:DistributionChannels", 'p');
		$r = $dc->getQuery()->getResult();

		// print_r($r);
		// die;

		$data['dc'] = [];
		foreach($r as $k){
			$data['dc'][] = [
				'name'	=> $k['distributionchannel'],
			];
		}		
		

		// // Render to template
		$response = $controller->render('LeepAdminBundle:Lead:lead_category_status.html.twig', $data);
		return $response;
	}
}
