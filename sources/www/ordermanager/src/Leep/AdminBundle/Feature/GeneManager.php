<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class GeneManager extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleDownloadResultHistory($controller, &$data, $options = array()) {        
        $id = $controller->get('request')->get('id', 0);

        $resultHistory = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:GeneManagerResultHistory')->findOneById($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=geneDataImport_'.$resultHistory->getTimestamp()->format("YmdHis").'.txt');
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".$resultHistory->getResult());

        return $response;
    }

    public function handleExportGeneResultByOrderNumber($controller, &$data, $options = array()) {
        $orderNumber = trim($controller->get('request')->get('orderNumber', ''));
        if (empty($orderNumber))  {
            die("Please specify order number");
        }

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.result')
            ->from('AppDatabaseMainBundle:GeneManagerResultHistory', 'p')
            ->andWhere('(p.result LIKE :orderNumber)')
            ->setParameter('orderNumber', '%'.$orderNumber.'%')
            ->orderBy('p.id', 'ASC');
        $results = $query->getQuery()->getResult();

        $geneResult = array();
        foreach ($results as $r) {
            $content = $r['result'];    
            $lines = explode("\n", $content);
            foreach ($lines as $line) {
                if (strpos($line, $orderNumber) === 0) {
                    $c = $line[strlen($orderNumber)];
                    if (('A' <= $c && $c <= 'Z') ||
                        ('a' <= $c && $c <= 'z') ||
                        ('0' <= $c && $c <= '9')) {
                    }
                    else {
                        $geneResult[] = $line;
                    }
                }
            }
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=geneResult_'.$orderNumber.'.txt');
        $response->setContent("\xEF\xBB\xBF".implode("\n", $geneResult));
        return $response;
    }

    public function handleViewErrors($controller, &$data, $options = array()) {        
        $id = $controller->get('request')->get('id', 0);
        $em = $controller->get('doctrine')->getEntityManager();

        $errorRecords = array(
            Business\GeneManager\Constant::ERROR_TYPE_CONFLICT_ERROR => array(
                'name'    => 'Conflict errors',
                'errors'  => array()
            ),
            Business\GeneManager\Constant::ERROR_TYPE_DATA_ERROR     => array(
                'name'    => 'Data errors',
                'errors'  => array()
            ),
            Business\GeneManager\Constant::ERROR_TYPE_FORMAT_ERROR   => array(
                'name'    => 'Format errors',
                'errors'  => array()
            ),
            Business\GeneManager\Constant::ERROR_TYPE_WARNING   => array(
                'name'    => 'Warning',
                'errors'  => array()
            ),
            Business\GeneManager\Constant::TYPE_SUCCESS   => array(
                'name'    => 'Success Records',
                'errors'  => array()
            ),
        );

        $result = $em->getRepository('AppDatabaseMainBundle:GeneManagerResultHistoryError')->findByIdGeneManagerResultHistory($id);
        foreach ($result as $error) {
            $errorRecords[$error->getIdErrorType()]['errors'][] = unserialize($error->getErrorData());
        }

        $data['errorRecords'] = $errorRecords;

        return $controller->render('LeepAdminBundle:GeneManager:view_errors.html.twig', $data);
    }
}
