<?php
    namespace Leep\AdminBundle\Feature;

    use Easy\ModuleBundle\Module\AbstractFeature;
    use Symfony\Component\HttpFoundation\Response;
    use Leep\AdminBundle\Business;
    use App\Database\MainBundle\Entity;
    use Leep\AdminBundle\Helper\BinaryFileResponse;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    use Symfony\Component\HttpFoundation\RedirectResponse;

    class GenoStorage extends AbstractFeature{

        const START_INDEX = 0;

        public function getActions(){
            return array();
        }

        public function getDefaultOptions(){
            return array();
        }

        public $container;
        public function __construct($container){
            $this->container = $container;
        }

        public function handleViewLog($controller, &$data, $options = array()) {
            $em = $controller->get('doctrine')->getEntityManager();
            $id = $controller->get('request')->get('id', 0);
            $formatter = $controller->get('leep_admin.helper.formatter');

            $cells = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findByStorageId($id);
            $cellList = [];
            $cellIdList = [];
            foreach ($cells as $cell) {
                $cellIdList[] = $cell->getId();
                $cellList[$cell->getId()] = $cell->getName()." (".$cell->getLevel().")";
            }

            $logs = $em->getRepository('AppDatabaseMainBundle:GenoStorageCellLogs')->findBy(
                ['cellId' => $cellIdList],
                ['cellId' => 'ASC', 'date' => 'ASC']);

            $data['logs'] = [];
            foreach ($logs as $log) {
                $data['logs'][] = [
                    'cell' =>   $cellList[$log->getCellId()],
                    'date' =>   $formatter->format($log->getDate(), 'datetime'),
                    'by' =>     $formatter->format($log->getUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                    'amount' => $log->getAmount(),
                    'helper' => $log->getHelper()
                ];
            }

            return $controller->render('LeepAdminBundle:GenoStorage:log_view.html.twig', $data);
        }

        public function handleVisualizeLoad($controller, &$data, $options=array()){
            // TODO: process in here
            $request = $controller->get('request');
            $selectedGenoStorageList = $request->get('selected', '');
            $selectedGenoStorageList = substr_replace($selectedGenoStorageList, "", -1); // remove the final comma

            $genoStoragesId = (explode(",", $selectedGenoStorageList));

            $em = $this->container->get('doctrine')->getEntityManager();

            $genoStoragesLevels = array();

            $i = 0;
            foreach ($genoStoragesId as $idCode){
                // append all level
                $query = $em->createQueryBuilder();
                $query->select('p.name, p.warningThreshold')
                        ->from('AppDatabaseMainBundle:GenoStorages', 'p')
                        ->where('p.id = :idGenoStorage')
                        ->setParameter('idGenoStorage', $idCode);
                $storageInfo = $query->getQuery()->getResult();

                // append all levels of each geno storage
                $genoLevels = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell','app_main')->
                findBy(array('storageId' => $idCode));
                $genoStoragesLevels[$i] = ['name' => $storageInfo[self::START_INDEX]['name'], 'warningThreshold' => $storageInfo[self::START_INDEX]['warningThreshold'], 'levels' => $genoLevels];
                $i = $i + 1;
            }

            $genoBatches = $em->getRepository('AppDatabaseMainBundle:GenoBatches','app_main')->findAll();
            $genoMaterials = $em->getRepository('AppDatabaseMainBundle:GenoMaterials','app_main')->findAll();

            // Update geno material name in geno Batch
            $index = 0;
            $genoStoragesCells = array();
            $warningThresholds = [];
            foreach ($genoStoragesLevels as $storages) {
                $storageCells  = ['storageName' => $storages['name'],
                                    'cells'     => array()];
                $storageDetail = $storages['levels'];
                foreach ($storageDetail as $cell) {
                    $storageCell = [];
                    $cellId = strval($cell->getId());
                    $numberMaterialContain = 0;
                    $genoMaterialsContain = array();
                    $i = 0;
                    // get detail info of each batch as: shortname geno material, amount in stock. Save them to genoMaterialsContain
                    foreach ($genoBatches as $genoBatch) {
                        $this->createData($genoBatch, $genoMaterials, $cell, $genoMaterialsContain, $i);
                    }
                    // set the werning threshold of each cell, if cell don't have warning threshold, assign default as storage warning threshold
                    $warningThresholdDefault = 0;
                    if ($storages['warningThreshold']) {
                        $warningThresholdDefault = $storages['warningThreshold'];
                    }

                    $cellThreshold = $cell->getWarningThreshold();
                    // $warningThresholds[$cellId] = $cellThreshold;
                    if (empty($cellThreshold)) {
                        $warningThresholds[$cellId] = $warningThresholdDefault;
                        $cellThreshold = $warningThresholdDefault;
                    }
                    // with all batch have same geno material, we merge them and get the total amount in stock
                    $geno = array();
                    foreach ($genoMaterialsContain as $key => $value) {
                        $subKeys = array_keys($value);
                        $shortName = $subKeys[self::START_INDEX];
                        if (array_key_exists($shortName, $geno)) {
                            $geno[$shortName] += $value[$shortName];
                        }
                        else{
                            $numberMaterialContain++;
                            $geno[$shortName] = $value[$shortName];
                        }
                    }
                    // format geno Material info to show in viasualize form
                    $infoGeno = array();
                    foreach ($geno as $key => $value) {
                        $valueStr = strval($value);
                        $info = $key.' ('.$valueStr.'g)';
                        array_push($infoGeno, $info);
                    }

                    $storageCell['id'] = $cellId;
                    $storageCell['genoMaterialsContain'] = $infoGeno;
                    $storageCell['numberGenoMaterialContain'] = $numberMaterialContain;
                    $storageCell['level'] = $cell->getLevel();
                    $storageCell['warningThreshold'] = $cellThreshold;
                    $storageCell['loading'] = $cell->getLoading();
                    $storageCell['name'] = $cell->getName();
                    array_push($storageCells['cells'], $storageCell);
                }

                usort($storageCells['cells'], array($this, "compareStorageCell"));
                $genoStoragesCells[$index] = $storageCells;
                $index++;
            }

            // write to page
            $data['genoStorages'] = $genoStoragesCells;
            $response = $controller->renderView('LeepAdminBundle:GenoStorage:visualize_process_form.html.twig',$data);
            return new Response($response);
        }

        private function createData($genoBatch, $genoMaterials, $cell, &$genoMaterialsContain, &$index){
            if ($cell->getId() == $genoBatch->getLocation()) {
                foreach ($genoMaterials as $genoMaterial) {
                    if ($genoBatch->getIdGenoMaterial() == $genoMaterial->getId()) {
                        $shortName = $genoMaterial->getShortName();
                        $amountInStock = $genoBatch->getAmountInStock();
                        $infoGenoMaterial = array($shortName => $amountInStock);
                        $genoMaterialsContain[$index] = $infoGenoMaterial;
                        $index++;
                        break;
                    }
                }
            }
        }

        private function  compareStorageCell($storageCell1, $storageCell2)
        {
            $levelCell1 = $storageCell1['level'];
            $levelCell2 = $storageCell2['level'];
            if ($levelCell1 > $levelCell2) {
                return 1;
            }
            return -1;
        }

    }
 ?>
