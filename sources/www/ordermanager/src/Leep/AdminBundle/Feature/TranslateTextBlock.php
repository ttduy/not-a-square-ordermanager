<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class TranslateTextBlock extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $untranslatedPattern = array(
        "###",
        "-- No translation yet --",
        "????",
        "***MISSING TEXT***",
        "--- TRANSLATE ME ---",
        "***MISSING TEXT****",
        "--- TRANSLATE ME ---",
        "####",
        ""
    );

    public function handleDashboard($controller, &$data, $options = []) {
        $request = $controller->get('request');
        $mgr = $this->container->get('easy_module.manager');
        $utils = $controller->get('leep_admin.translate_text_block.business.utils');
        $mapping = $controller->get('easy_mapping');
        $sourceLanguages = $utils->getSourceLanguages();
        $targetLanguages = $utils->getTargetLanguages();
        $reports = $utils->getReportMapping();
        $headers = [];
        $wordCount = [];
        $languageNameMap = [];

        // Load alphabetsR
        $alphabets = [];
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($result as $r) {
            $alphabet = $r->getAlphabet();
            if (empty($alphabet)) {
                $alphabet = '';
            }

            $alphabets[$r->getId()] = $alphabet;
        }

        // Source header
        foreach ($sourceLanguages as $idLanguage) {
            $headers[] = $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage);
            $languageNameMap[$idLanguage] = $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage);
        }

        $numSourceLanguages = count($sourceLanguages);
        $data['sourceLanguageHeader'] = $headers;

        // Target header
        $targetLanguageHeader = [];
        foreach ($targetLanguages as $idLanguage) {
            $targetLanguageHeader[$idLanguage] = $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage);
            $languageNameMap[$idLanguage] = $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage);
        }

        $data['targetLanguageHeader'] = $targetLanguageHeader;

        $tempLanguageWordMap = [];

        // Compute word counts
        $em = $controller->get('doctrine')->getEntityManager();
        foreach ($reports as $idReport => $reportName) {
            $row = [];
            $row[] = ['span' => $numSourceLanguages, 'value' => $reportName];
            foreach ($sourceLanguages as $idLanguage) {
                $totalTranslated = 0;
                $totalUntranslated = 0;
                $total = 0;
                $query = $em->createQueryBuilder();
                $query->select('p.body, p.idTextBlock')
                    ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'r.idTextBlock = p.idTextBlock')
                    ->andWhere('r.idReport = :idReport')
                    ->andWhere('p.idLanguage = :idLanguage')
                    ->setParameter('idReport', $idReport)
                    ->setParameter('idLanguage', $idLanguage);
                $result = $query->getQuery()->getResult();
                $languageName = $languageNameMap[$idLanguage];
                foreach ($result as $textBlock) {
                    $numWord = str_word_count(str_replace('-', ' ', $textBlock['body']), 0, $alphabets[$idLanguage]);
                    $tempLanguageWordMap[$textBlock['idTextBlock']][$languageName] = $numWord;
                    $total += $numWord;
                }

                $row[] = ['span' => $numSourceLanguages, 'value' => $total];
            }

            foreach ($targetLanguages as $idLanguage) {
                $totalTranslated = 0;
                $totalUntranslated = 0;
                $total = 0;
                $query = $em->createQueryBuilder();
                $query->select('p.body, p.idTextBlock')
                    ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'r.idTextBlock = p.idTextBlock')
                    ->andWhere('r.idReport = :idReport')
                    ->andWhere('p.idLanguage = :idLanguage')
                    ->setParameter('idReport', $idReport)
                    ->setParameter('idLanguage', $idLanguage);
                $result = $query->getQuery()->getResult();
                $rowResult = [];
                foreach ($result as $textBlock) {
                    $numWord = str_word_count(str_replace('-', ' ', $textBlock['body']), 0, $alphabets[$idLanguage]);
                    foreach ($sourceLanguages as $idSourceLanguage) {
                        $languageName = $languageNameMap[$idSourceLanguage];
                        if (!isset($rowResult[$languageName])) {
                            $rowResult[$languageName] = 0;
                        }

                        if (in_array(trim($textBlock['body']), $this->untranslatedPattern)) {
                            $rowResult[$languageName] += $tempLanguageWordMap[$textBlock['idTextBlock']][$languageName];
                        }
                    }
                }

                if (empty($result)) {
                    foreach ($sourceLanguages as $idSourceLanguage) {
                        $languageName = $languageNameMap[$idSourceLanguage];
                        if (!isset($rowResult[$languageName])) {
                            $rowResult[$languageName] = 0;
                        }
                    }
                }

                foreach ($rowResult as $languageName => $value) {
                    $row[] = ['span' => 1, 'value' => $languageName];
                    $row[] = ['span' => 1, 'value' => $value];
                    $wordCount[] = $row;
                    $row = [];
                }
            }

            $wordCount[] = $row;
        }

        $data['wordCount'] = $wordCount;

        // Compute word count for all report of this account / by language
        $totalWordCount = [];
        $row = [];
        $row[] = ['span' => $numSourceLanguages, 'value' => 'All of your reports'];
        foreach ($sourceLanguages as $idLanguage) {
            $totalTranslated = 0;
            $totalUntranslated = 0;
            $total = 0;
            $query = $em->createQueryBuilder();
            $query->select('p.body')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'r.idTextBlock = p.idTextBlock')
                ->andWhere('r.idReport IN (:reports)')
                ->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('reports', array_keys($reports))
                ->setParameter('idLanguage', $idLanguage)
                ->groupBy('p.idTextBlock');
            $result = $query->getQuery()->getResult();
            foreach ($result as $textBlock) {
                $numWord = str_word_count(str_replace('-', ' ', $textBlock['body']), 0, $alphabets[$idLanguage]);
                $total += $numWord;
            }

            $row[] = ['span' => $numSourceLanguages, 'value' => $total];
        }

        foreach ($targetLanguages as $idLanguage) {
            $totalTranslated = 0;
            $totalUntranslated = 0;
            $total = 0;
            $query = $em->createQueryBuilder();
            $query->select('p.body, p.idTextBlock')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'r.idTextBlock = p.idTextBlock')
                ->andWhere('r.idReport IN (:reports)')
                ->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('reports', array_keys($reports))
                ->setParameter('idLanguage', $idLanguage)
                ->groupBy('p.idTextBlock');
            $result = $query->getQuery()->getResult();
            $rowResult = [];
            foreach ($result as $textBlock) {
                $numWord = str_word_count(str_replace('-', ' ', $textBlock['body']), 0, $alphabets[$idLanguage]);
                if (in_array(trim($textBlock['body']), $this->untranslatedPattern)) {
                    foreach ($sourceLanguages as $idSourceLanguage) {
                        $languageName = $languageNameMap[$idSourceLanguage];
                        if (!isset($rowResult[$languageName])) {
                            $rowResult[$languageName] = 0;
                        }

                        $rowResult[$languageName] += $tempLanguageWordMap[$textBlock['idTextBlock']][$languageName];
                    }
                }
            }

            foreach ($rowResult as $languageName => $value) {
                $row[] = ['span' => 1, 'value' => $languageName];
                $row[] = ['span' => 1, 'value' => $value];
                $totalWordCount[] = $row;
                $row = [];
            }
        }

        $data['totalWordCount'] = $totalWordCount;


        ////////////////////////////////////////////////////////
        // x. Translation progress
        $translationStatistics = [];
        foreach ($reports as $idReport => $reportName) {
            $translationStatistics[$idReport] = array(
                'name'  => $reportName,
                'idReport' => $idReport,
                'stats' => []
            );

            foreach ($targetLanguages as $idLanguage) {
                // Num untranslated
                $query = $em->createQueryBuilder();
                $query->select('COUNT(p) as total')
                    ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 't', 'WITH', 'p.idTextBlock = t.idTextBlock')
                    ->andWhere('t.idReport = :idReport')
                    ->andWhere('p.idLanguage = :idLanguage')
                    ->setParameter('idReport', $idReport)
                    ->setParameter('idLanguage', $idLanguage)
                    ->andWhere('(p.isTranslated = 0) OR (p.isTranslated IS NULL)');
                $totalUntranslated = intval($query->getQuery()->getSingleScalarResult());

                // Num translated
                $query = $em->createQueryBuilder();
                $query->select('COUNT(p) as total')
                    ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 't', 'WITH', 'p.idTextBlock = t.idTextBlock')
                    ->andWhere('t.idReport = :idReport')
                    ->andWhere('p.idLanguage = :idLanguage')
                    ->setParameter('idReport', $idReport)
                    ->setParameter('idLanguage', $idLanguage)
                    ->andWhere('p.isTranslated = 1');
                $totalTranslated = intval($query->getQuery()->getSingleScalarResult());

                $translationStatistics[$idReport]['stats'][$idLanguage] = array(
                    'numTranslated'    => $totalTranslated,
                    'numUntranslated'  => $totalUntranslated
                );
            }
        }

        $data['translationStatistics'] = $translationStatistics;

        // Links
        $data['linkPerformTranslation'] = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'translate');
        $data['linkViewAllTranslation'] = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'viewAllTranslation');
    }

    public function handleSaveTextBlock($controller, &$data, $options = []) {
        $validator = $controller->get('leep_admin.text_block.business.constraint_validator');
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $now = new \DateTime();

        $idTextBlock = $request->get('idTextBlock', 0);
        $idLanguage = $request->get('idLanguage', 0);
        $body = $request->get('body', '');

        $textBlock = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($idTextBlock);

        // Validate constraints
        $errors = $validator->validate($textBlock, $body);
        if (!empty($errors)) {
            $msg = [];
            foreach ($errors as $err) {
                $msg []= ' - '.$err;
            }
            return new Response(implode(' <br/> ', $msg));
        }

        // Permission checking
        $utils = $controller->get('leep_admin.translate_text_block.business.utils');
        $targetLanguages = $utils->getTargetLanguages();
        if (!in_array($idLanguage, $targetLanguages)) {
            return new Response('FAILED');
        }

        // Remove duplication
        $textBlocks = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findByName(trim($textBlock->getName()));
        foreach ($textBlocks as $textBlock) {
            if ($textBlock->getId() != $idTextBlock) {
                $em->remove($textBlock);
            }
        }
        $em->flush();

        // Save text blocks

        $criteria = array(
            'idTextBlock' => $idTextBlock,
            'idLanguage'  => $idLanguage
        );
        $textBlockLanguage = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findOneBy($criteria);
        if (empty($textBlockLanguage)) {
            $textBlockLanguage = new Entity\TextBlockLanguage();
            $textBlockLanguage->setIdTextBlock($idTextBlock);
            $textBlockLanguage->setIdLanguage($idLanguage);
        }

        $textBlockLanguage->setBody($body);
        $textBlockLanguage->setIsCached(0);
        $em = $controller->get('doctrine')->getEntityManager();
        $em->persist($textBlockLanguage);
        $em->flush();

        return new Response('SUCCESS');
    }

    public function handleTranslate($controller, &$data, $options = []) {
        $mgr = $this->container->get('easy_module.manager');
        $request = $this->container->get('request');
        $em = $this->container->get('doctrine')->getEntityManager();
        $form = $controller->get('leep_admin.translate_text_block.business.translate_handler');
        $mapping = $controller->get('easy_mapping');
        $utils = $controller->get('leep_admin.translate_text_block.business.utils');


        $idReport = $request->get('idReport', 0);
        $idLanguage = $request->get('idLanguage', 0);
        $idTextBlock = $request->get('idTextBlock', 0);
        $data['idReport'] = $idReport;
        $data['idLanguage'] = $idLanguage;
        $data['idTextBlock'] = $idTextBlock;

        // Pop a text block
        if ($idTextBlock) {
            $textBlockLanguage = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findOneBy(array(
                'idLanguage'   => $idLanguage,
                'idTextBlock'  => $idTextBlock
            ));
        }
        else {
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportTextBlock', 't')
                ->leftJoin('AppDatabaseMainBundle:TextBlockLanguage', 'p', 'WITH', 't.idTextBlock = p.idTextBlock')
                ->andWhere('t.idReport = :idReport')
                ->andWhere('p.idLanguage = :idLanguage')
                ->andWhere('(p.isTranslated = 0) OR (p.isTranslated IS NULL)')
                ->setParameter('idReport', $idReport)
                ->setParameter('idLanguage', $idLanguage)
                ->setMaxResults(1);
            $textBlockLanguage = null;
            $results = $query->getQuery()->getResult();
            foreach ($results as $r) {
                $textBlockLanguage = $r;
                break;
            }
        }

        if (!empty($textBlockLanguage)) {
            $textBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($textBlockLanguage->getIdTextBlock());
            $data['textBlock'] = $textBlock;
            $form->textBlockLanguage = $textBlockLanguage;

            // RULES
            $rules = [];
            if ($textBlock->getMaxLength() !== null) {
                $rules['maxLength'] = $textBlock->getMaxLength();
            }
            if ($textBlock->getIsUppercase()) {
                $rules['isUppercase'] = true;
            }
            if ($textBlock->getNumLineBreak() !== null) {
                $rules['numLineBreak'] = $textBlock->getNumLineBreak();
            }

            $data['rules'] = $rules;

            // SOURCE LANGUAGES
            $sourceLanguages = $utils->getSourceLanguages();
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idLanguage IN (:languages)')
                ->andWhere('p.idTextBlock = :idTextBlock')
                ->setParameter('languages', $sourceLanguages)
                ->setParameter('idTextBlock', $textBlockLanguage->getIdTextBlock());
            $results = $query->getQuery()->getResult();

            $sourceBlocks = [];
            foreach ($results as $r) {
                $sourceBlocks[] = array(
                    'name' => $mapping->getMappingTitle('LeepAdmin_Language_List', $r->getIdLanguage()),
                    'body' => $r->getBody()
                );
            }
            $data['sourceBlocks'] = $sourceBlocks;

            // Check duplication
            $query = $em->createQueryBuilder();
            $query->select('COUNT(p) AS total')
                ->from('AppDatabaseMainBundle:TextBlock', 'p')
                ->andWhere('p.name = :name')
                ->setParameter('name', trim($textBlock->getName()));
            $numTextBlock = intval($query->getQuery()->getSingleScalarResult());

            $data['numTextBlock'] = $numTextBlock;
        }

        $form->execute();
        if ((count($form->getErrors()) == 0) && ($request->getMethod() == 'POST')) {
            $url = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'translate', array(
                'idReport'    => $idReport,
                'idLanguage'  => $idLanguage
            ));

            return new RedirectResponse($url);
        }

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Translate',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'translate')
        );

        $data['targetLanguage'] = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($idLanguage);

        $data['linkViewTranslation'] = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'viewTranslation');

        return $controller->render('LeepAdminBundle:TranslateTextBlock:translate.html.twig', $data);
    }

    public function handleViewTranslation($controller, &$data, $options = []) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();

        $idTextBlock = $request->get('idTextBlock', 0);
        $idLanguage = $request->get('idLanguage', 0);
        $textBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($idTextBlock);

        $translations = [];
        $query = $em->createQueryBuilder();
        $query->select('l')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->innerJoin('AppDatabaseMainBundle:TextBlockLanguage', 'l', 'WITH', 'p.id = l.idTextBlock')
            ->andWhere('p.name = :name')
            ->andWhere('l.idLanguage = :idLanguage')
            ->setParameter('idLanguage', $idLanguage)
            ->setParameter('name', trim($textBlock->getName()));
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $translations[] = $r->getBody();
        }

        $data['translations'] = $translations;

        return $controller->render('LeepAdminBundle:TranslateTextBlock:viewTranslation.html.twig', $data);
    }

    public function handleViewAllTranslation($controller, &$data, $options = []) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $utils = $controller->get('leep_admin.translate_text_block.business.utils');
        $mapping = $controller->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');


        $idReport = $request->get('idReport', 0);
        $idLanguage = $request->get('idLanguage', 0);


        $sourceLanguages = $utils->getSourceLanguages();
        $languages = $sourceLanguages;
        $languages[] = $idLanguage;
        $query = $em->createQueryBuilder();
        $query->select('p.idTextBlock, p.idLanguage, p.body, b.name, b.id')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'r.idTextBlock = p.idTextBlock')
            ->innerJoin('AppDatabaseMainBundle:TextBlock', 'b', 'WITH', 'r.idTextBlock = b.id')
            ->andWhere('r.idReport = :idReport')
            ->andWhere('p.idLanguage in (:languages)')
            ->setParameter('idReport', $idReport)
            ->setParameter('languages', $languages);
        $results = $query->getQuery()->getResult();
        $translations = [];
        foreach ($results as $r) {
            if (!isset($translations[$r['idTextBlock']])) {
                $translations[$r['idTextBlock']] = array(
                    'id'      => $r['id'],
                    'name'    => '[['.$r['name'].']]',
                    'blocks'  => []
                );
            }

            $translations[$r['idTextBlock']]['blocks'][$r['idLanguage']] = $r['body'];
        }
        $data['translations'] = $translations;
        $data['numCol'] = count($languages);

        $sourceLanguageList = [];
        foreach ($sourceLanguages as $idSourceLanguage) {
            $sourceLanguageList[] = array(
                'id'    => $idSourceLanguage,
                'name'  => $mapping->getMappingTitle('LeepAdmin_Language_List', $idSourceLanguage)
            );
        }
        $data['sourceLanguageList'] = $sourceLanguageList;
        $data['targetLanguage'] = array(
            'id'    => $idLanguage,
            'name'  => $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage)
        );

        $data['linkPerformTranslation'] = $mgr->getUrl('leep_admin', 'translate_text_block', 'feature', 'translate', array(
            'idReport'    => $idReport,
            'idLanguage'  => $idLanguage
        ));

        return $controller->render('LeepAdminBundle:TranslateTextBlock:viewAllTranslation.html.twig', $data);
    }
}
