<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class GenoBatch extends AbstractFeature {
    public function getActions() { return []; }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleViewLog($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');

        $logs = $em->getRepository('AppDatabaseMainBundle:GenoBatchLogs')->findByBatchId($id);
        $data['logs'] = [];

        foreach ($logs as $log) {
            $data['logs'][] = [
                'date' =>   $formatter->format($log->getDate(), 'datetime'),
                'by' =>     $formatter->format($log->getUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                'amount' => $log->getAmount(),
                'in' =>     $log->getHelper()
            ];
        }

        return $controller->render('LeepAdminBundle:GenoBatch:log_view.html.twig', $data);
    }

    public function handleViewStatusHistoryLog($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');

        $logs = $em->getRepository('AppDatabaseMainBundle:GenoBatchStatusHistory')->findByIdGenoBatch($id);
        if (empty($logs)) {
            // Create new base log
            $batch = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findOneById($id);
            $log = new Entity\GenoBatchStatusHistory();
            $log->setIdGenoBatch($id);
            $log->setStatus($batch->getStatus());
            $log->setDate(null);
            $log->setUser(null);
            $log->setDescription("This status was set before Log feature is implemented!");
            $em->persist($log);
            $em->flush();

            // Add to log
            $logs = [$log];
        }

        $data['logs'] = [];
        foreach ($logs as $log) {
            $date = "Unknown";
            if ($log->getDate() != null) {
                $date = $formatter->format($log->getDate(), 'datetime');
            }

            $user = "Unknown";
            if ($log->getUser() != null) {
                $user = $formatter->format($log->getUser(), 'mapping', 'LeepAdmin_WebUser_List');
            }

            $data['logs'][] = [
                'date' =>           $date,
                'by' =>             $user,
                'status' =>         Business\GenoBatch\Constant::getStatus()[$log->getStatus()],
                'description' =>    $log->getDescription()
            ];
        }

        return $controller->render('LeepAdminBundle:GenoBatch:status_log_view.html.twig', $data);
    }
}
