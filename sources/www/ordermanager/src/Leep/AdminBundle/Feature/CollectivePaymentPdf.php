<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class CollectivePaymentPdf extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handlePdf($controller, &$data, $options = array()) {        
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $idCollectivePayment = $controller->get('request')->query->get('id', 0);        
        $collectivePayment = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:CollectivePayment')->findOneById($idCollectivePayment);

        $isRegenerate = $controller->get('request')->get('isRegenerate', 0);
        if ($isRegenerate) {
            $result = Business\CollectivePayment\Utils::buildPdf($controller, $idCollectivePayment);
            if ($result instanceof Response) {
                return $result;
            }
        }    
        else {
            if (trim($collectivePayment->getPaymentFile()) == '') {
                $resp = new Response();
                $resp->setContent("No collective payment pdf is available, please regenerate new collective payment");
                return $resp;
            }
        }

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/payments';

        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename=collective_payment_'.$collectivePayment->getId().'.pdf');        
        $resp->setContent(file_get_contents($filePath.'/'.$collectivePayment->getPaymentFile()));
        return $resp;
    }


    public function handleEmail($controller, &$data, $options = array()) {        

    }
}
