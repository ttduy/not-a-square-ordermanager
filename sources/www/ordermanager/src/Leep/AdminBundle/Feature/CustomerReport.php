<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CustomerReport extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleDownloadCustomerData($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $customerReport = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($id);


        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=customer_data.txt');
        $response->setContent("\xEF\xBB\xBF".str_replace("\n", "\r\n", $customerReport->getCustomerData()));

        return $response;
    }

    public function handleLoadCustomerData($controller, &$data, $options = array()) {
        $orderNumber = $controller->get('request')->get('orderNumber');

        if ($orderNumber[0] == '!') {
            $cryosaveSample = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneBySampleCode(substr($orderNumber, 1));
            if ($cryosaveSample) {
                $sampleData = Business\CryosaveSample\Utils::loadSampleData($controller->get('service_container'), $cryosaveSample);
                return new Response($sampleData);
            }
        }
        else {
            $order = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($orderNumber);
            if ($order) {
                $orderData = Business\CustomerReport\Utils::loadOrderData($controller->get('service_container'), $order->getId());
                return new Response($orderData);
            }
        }
        return new Response("Order number ".$orderNumber." don't exist");
    }

    public function handleDownloadReportData($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $customerReport = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($id);


        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=report_data.txt');
        $response->setContent("\xEF\xBB\xBF".str_replace("\n", "\r\n", $customerReport->getReportFinalData()));

        return $response;
    }

    public function handleViewCustomerData($controller, &$data, $options = array()) {
        $idOrder = $controller->get('request')->get('idCustomer');
        $orderData = Business\CustomerReport\Utils::loadOrderData($controller->get('service_container'), $idOrder);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=customer_data.txt');
        $response->setContent("\xEF\xBB\xBF".str_replace("\n", "\r\n", $orderData));

        return $response;
    }

    public function handleDelete($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $customerReport = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($id);
        $idCustomer = $customerReport->getIdCustomer();

        $handler = $controller->get('leep_admin.customer_report.business.delete_handler');
        $handler->execute();

        return $controller->redirect($this->getModule()->getUrl('grid', 'list', array('id' => $idCustomer)));
    }

    public function handleView($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $reportHelper = $controller->get('leep_admin.helper.report_helper');

        $id = $controller->get('request')->get('id');
        $isDownload = $controller->get('request')->get('isDownload', 0);
        $isLowResolution = $controller->get('request')->get('isLowResolution', 0);
        $isExcel = $controller->get('request')->get('isExcel', 0);
        $isExcel2 = $controller->get('request')->get('isExcel2', 0);
        $isXml = $controller->get('request')->get('isXml', 0);
        $customerReport = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($id);

        $fileUrl = $controller->get('service_container')->getParameter('attachment_url').'/../report/'.$customerReport->getFilename();

        if ($isDownload) {
            $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report';
            $tempDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
            $filename = $isLowResolution ? $customerReport->getLowResolutionFilename() : $customerReport->getFilename();

            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($customerReport->getIdCustomer());
            $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($customerReport->getIdReport());
            $reportName = $customer->getOrderNumber().' - '.$report->getName();

            if ($isLowResolution) {
                return $reportHelper->serveReport(
                    $fileDir,
                    array(
                        $customerReport->getWebCoverLowResolutionFilename(),
                        $customerReport->getLowResolutionFilename(),
                        $customerReport->getBodCoverLowResolutionFilename()
                    ),
                    $reportName
                );
            }

            if ($isExcel) {
                $filename = $customerReport->getFoodTableExcelFilename();
                $downloadFile = $fileDir.'/'.$filename;
                if (!is_file($downloadFile)) {
                    $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                    $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                    if ($result) {
                        $downloadFile = "$tempDir/$filename";
                    }
                }

                $fileResponse = new BinaryFileResponse($downloadFile);
                $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xlsx');
                return $fileResponse;
            } else if ($isExcel2) {
                $filename = $customerReport->getFoodTableExcelFile2Name();
                $downloadFile = $fileDir.'/'.$filename;
                if (!is_file($downloadFile)) {
                    $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                    $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                    if ($result) {
                        $downloadFile = "$tempDir/$filename";
                    }
                }

                $fileResponse = new BinaryFileResponse($downloadFile);
                $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xlsx');
                return $fileResponse;
            } else if ($isXml) {
                $filename = $customerReport->getXmlFilename();
                $downloadFile = $fileDir.'/'.$filename;
                if (!is_file($downloadFile)) {
                    $awsHelper = $controller->get('leep_admin.helper.aws_backup_files_helper');
                    $result = $awsHelper->temporaryDownloadBackupFiles('report', $filename);
                    if ($result) {
                        $downloadFile = "$tempDir/$filename";
                    }
                }

                $fileResponse = new BinaryFileResponse($downloadFile);
                $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$reportName.'.xml');
                return $fileResponse;
            } else {
                return $reportHelper->serveReport(
                    $fileDir,
                    array(
                        $customerReport->getWebCoverFilename(),
                        $customerReport->getFilename(),
                        $customerReport->getBodCoverFilename()
                    ),
                    $reportName
                );
            }
        }

        return $controller->redirect($fileUrl);
    }

    public function handleBuildAllReport($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $idCustomer = $controller->get('request')->get('id', 0);
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);

        // Check language
        $language = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findOneById($customer->getLanguageId());

        // Load customer data
        $data['customer'] = array(
            'orderNumber' => $customer->getOrderNumber(),
            'name' => $customer->getFirstName().' '.$customer->getSurName(),
            'language' => empty($language) ? ' ' : $language->getName(),
            'distributionChannel' => $formatter->format($customer->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List')
        );

        if (!$language) {
            $data['errors'] = array("Customer language is invalid");
            return;
        }

        // Check no offline job pending
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p')
            ->andWhere('(p.percentage IS NULL) OR (p.percentage < 100)')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $idCustomer);
        $numOfflineJob = $query->getQuery()->getSingleScalarResult();
        if (!empty($numOfflineJob)) {
            $data['errors'] = array("There's already offline jobs in queue for this customer, please wait");
            return;
        }

        // Check there's available report
        $applicableReports = Business\CustomerReport\Utils::getApplicableReports($controller->get('service_container'), $idCustomer);
        $customerReports = $doctrine->getRepository('AppDatabaseMainBundle:CustomerReport')->findByIdCustomer($idCustomer);
        foreach ($customerReports as $customerReport) {
            $idReport = $customerReport->getIdReport();
            if (isset($applicableReports[$idReport])) {
                unset($applicableReports[$idReport]);
            }
        }

        if (empty($applicableReports)) {
            $data['errors'] = array("All reports has been built");
            return;
        }

        // Build report
        $form = $controller->get('leep_admin.customer_report.business.create_all_handler');
        $form->applicableReports = array_keys($applicableReports);
        $form->idLanguage = $customer->getLanguageId();
        $form->execute();
        $data['errors'] = $form->getErrors();
        $data['messages'] = $form->getMessages();
    }

    public function handleGenerateAccessKey($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $common = $controller->get('leep_admin.helper.common');

        $idCustomer = $request->get('id', 0);
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $customer->setShipmentKey($common->generateRandAttachmentDir());
        $customer->setShipmentCode($common->generateRandPasscode());
        $customer->setShipmentKeyDatetime(new \DateTime());
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'bod_shipment', 'email', 'create', array('id' => $idCustomer));

        return new RedirectResponse($url);
    }

    public function handleDisableAccessKey($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $common = $controller->get('leep_admin.helper.common');

        $idCustomer = $request->get('id', 0);
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $customer->setShipmentKey('');
        $customer->setShipmentCode('');
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'bod_shipment', 'email', 'create', array('id' => $idCustomer));

        return new RedirectResponse($url);
    }

    public function handleChangeVisibility($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        $idCustomerReport = $request->get('id', 0);
        $visiblity = $request->get('visibility', 0);
        $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($idCustomerReport);
        $customerReport->setIsVisibleToCustomer($visiblity);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'customer_report', 'grid', 'list', array('id' => $customerReport->getIdCustomer()));

        return new RedirectResponse($url);
    }

    public function handleFixFileName($controller, &$data, $options = array()) {

        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $id = $request->query->get('id', 0); // CustomerReport ID
        if ($id == 0) {
            return new Response("FAIL");
        }
        $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($id);
        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($customerReport->getIdReport());
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($customerReport->getIdCustomer());

        if ($report && $customer) {
            $query = $em->createQueryBuilder();
            $query->select('p.externalBarcode')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->eq('p.id', ':idCustomerInfo'))
                ->setParameter('idCustomerInfo', $customer->getIdCustomerInfo());
            $externalBarcode = $query->getQuery()->getSingleScalarResult();

            $shortName = $report->getShortName();
            $result = $externalBarcode."_".$shortName;

            $arr['customerReport'] = $customerReport->getId();
            $arr['result'] = $result;

            $resp = new Response();
            $resp->setContent(json_encode($arr));
            $resp->headers->set('Content-Type', 'application/json');
            return $resp;
        }

        return new Response("FAIL");
    }

    public function handleViewFiles($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $downloadDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/sftpDownload/';

        $id = $request->get('id', 0);
        $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($order->getDistributionChannelId());
        $dcId = $dc->getId();

        if (empty($dc)) {
            die("Data error, no DC set for this client");
        }

        $host = $dc->getSftpHost();
        $username = $dc->getSftpUsername();
        $password = $dc->getSftpPassword();
        $dirPath = $dc->getSftpFolder();

        $connection = ssh2_connect($host, 22);
        if($connection === false) {
            die("Cannot connect to server: " . $host);
        }

        ssh2_auth_password($connection, $username, $password);
        $sftp = ssh2_sftp($connection);

        $files = [];
        $dirHandle = opendir('ssh2.sftp://'.$sftp.'/'.$dirPath);

        while (false !== ($file = readdir($dirHandle))) {
            if ($file != '.' && $file != '..') {
                $files[] = [
                    'name' => $file,
                    'download_link' => $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'downloadSftpReport', ['id' => $id, 'file_name' => $file])
                ];
            }
        }

        $data['files'] = $files;

        // Downloadable Files
        if($dc->getCanDownloadViaSftp()) {
            $host = $dc->getSftpDownloadHost();
            $username = $dc->getSftpDownloadUsername();
            $password = $dc->getSftpDownloadPassword();
            $dirPath = $dc->getSftpDownloadFolder();

            $connection = ssh2_connect($host, 22);
            if($connection === false) {
                die("Cannot connect to server: " . $host);
            }

            ssh2_auth_password($connection, $username, $password);
            $sftp = ssh2_sftp($connection);

            $files = [];
            $dirHandle = opendir('ssh2.sftp://'.$sftp.'/'.$dirPath);

            while (false !== ($file = readdir($dirHandle))) {
                if ($file != '.' && $file != '..') {
                    $remoteFileUrl = 'ssh2.sftp://'.$sftp.'/'.$dirPath.'/'.$file;
                    $localFileUrl = "$downloadDir$dcId/$file";
                    $files[] = [
                        'name' => $file,
                        'downloadLink' => $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'downloadSftpReport', ['file' => $localFileUrl]),
                        'downloadable' => is_file($localFileUrl) && filesize($remoteFileUrl) == filesize($localFileUrl)
                    ];
                }
            }

            $data['downloadableFiles'] = $files;
        }

        return $controller->render('LeepAdminBundle:CustomerReport:ftp_shipment_view_files.html.twig', $data);
    }

    public function handleDownloadSftpReport($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $fileUrl = $request->get('file', '');
        if (!is_file($fileUrl)) {
            return new Response("ERROR: File $fileUrl not found!");
        }

        $fileResponse = new BinaryFileResponse($fileUrl);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        // $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$fileName);
        return $fileResponse;
    }

    public function handleUnLinkFile($controller, &$data, $options = array()) {
        /*
        $request = $controller->get('request');
        $fileName = $request->query->get('fileName', ''); // CustomerReport ID
        $dirPath = "/home/ordermanager/temp/";

        $host = '54.221.232.120';
        $username = 'ordermanager';
        $password = 'qwerasdfZXCV';

        $connection = ssh2_connect($host, 22);
        if($connection === false) {
            echo "Cannot connect to server: " . $host;
        }
        ssh2_auth_password($connection, $username, $password);
        $sftp = ssh2_sftp($connection);

        $dirHandle = opendir('ssh2.sftp://'.$sftp.$dirPath);

        if (ssh2_sftp_unlink($sftp, $dirPath.$fileName)) {
            return new Response("SUCCESS");
        }

        return new Response("FAIL");*/
    }
}
