<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class ScanFormBatch extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    protected function isDoneVerifyStatus($controller, $idScanFormBatch, $statusField, $recordStatus = Business\ScanFormRecord\Constants::STATUS_PROCESSED) {
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as total')
            ->from('AppDatabaseMainBundle:ScanFormRecord', 'p')
            ->andWhere('p.idScanFormBatch = :idScanFormBatch')
            ->andWhere('p.status = :statusProcessed')
            ->andWhere('p.'.$statusField.' = 0 OR p.'.$statusField.' IS NULL')
            ->setParameter('statusProcessed', $recordStatus)
            ->setParameter('idScanFormBatch', $idScanFormBatch);
        $total = $query->getQuery()->getSingleScalarResult();

        return (intval($total) == 0);
    }

    public function handleVerify($controller, &$data, $options = array()) {      
        $mgr = $controller->get('easy_module.manager');
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        //////////////////////////////////////
        // x. Compute next actions
        $scanFormBatch = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormBatch')->findOneById($id);
        $repository = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormRecord');

        $verifySteps = array(
            'processed'  => array(
                'field'     => 'verifyProcessedStatus',
                'action'    => 'verifyProcessed',
                'recordStatus'  => Business\ScanFormRecord\Constants::STATUS_DONE
            ),
            'textbox'    => array(
                'field'     => 'verifyTextboxStatus',
                'action'    => 'verifyTextbox',
                'recordStatus'  => Business\ScanFormRecord\Constants::STATUS_DONE
            ),
            'textarea'   => array(
                'field'     => 'verifyTextareaStatus',
                'action'    => 'verifyTextarea',
                'recordStatus'  => Business\ScanFormRecord\Constants::STATUS_DONE
            ),
            'checkbox'   => array(
                'field'     => 'verifyCheckboxStatus',
                'action'    => 'verifyCheckbox',
                'recordStatus'  => Business\ScanFormRecord\Constants::STATUS_DONE
            ),
            'form'       => array(
                'field'     => 'verifyFormStatus',
                'action'    => 'verifyForm',
                'recordStatus'  => Business\ScanFormRecord\Constants::STATUS_ERROR
            )
        );
        
        foreach ($verifySteps as $actionName => $verifyData) {
            if ($this->isDoneVerifyStatus($controller, $id, $verifyData['field'], $verifyData['recordStatus']) == false) {
                $url = $mgr->getUrl('leep_admin', 'scan_form_batch', 'feature', $verifyData['action'], array('id' => $id));
                return new RedirectResponse($url);
            }
        }

        // Everything is done, change batch status
        $scanFormBatch->setStatus(Business\ScanFormBatch\Constants::STATUS_DONE);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'scan_form_batch', 'grid', 'list');

        return new RedirectResponse($url);
    }

    protected function handleVerifyMethod($controller, &$data, $verifiedId) {
        $mgr = $controller->get('easy_module.manager');
        $id = $controller->get('request')->get('id', 0);
        $url = $mgr->getUrl('leep_admin', 'scan_form_batch', 'feature', 'verify', array('id' => $id));
        
        $verifier = $controller->get($verifiedId);
        if ($controller->get('request')->getMethod() == 'POST') {
            $verifier->submit();            
            return new RedirectResponse($url);
        }
        $result = $verifier->view($data);
        if ($result === FALSE) {
            return new RedirectResponse($url);
        }
    }

    public function handleVerifyProcessed($controller, &$data, $options = array()) {  
        return $this->handleVerifyMethod($controller, $data, 'leep_admin.scan_form_batch.business.verifier.processed');
    }
    public function handleVerifyTextbox($controller, &$data, $options = array()) {  
        return $this->handleVerifyMethod($controller, $data, 'leep_admin.scan_form_batch.business.verifier.textbox');
    }
    public function handleVerifyTextarea($controller, &$data, $options = array()) {  
        return $this->handleVerifyMethod($controller, $data, 'leep_admin.scan_form_batch.business.verifier.textarea');
    }
    public function handleVerifyCheckbox($controller, &$data, $options = array()) {  
        return $this->handleVerifyMethod($controller, $data, 'leep_admin.scan_form_batch.business.verifier.checkbox');
    }
    public function handleVerifyForm($controller, &$data, $options = array()) {  
        return $this->handleVerifyMethod($controller, $data, 'leep_admin.scan_form_batch.business.verifier.form');
    }
}
