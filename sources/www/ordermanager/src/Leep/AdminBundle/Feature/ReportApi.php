<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ReportApi extends AbstractFeature {
    public $worker = null;
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleDo($controller, &$data, $options = array()) {
        $request = $controller->get('request');

        $api = $request->get('api');
        $apiHandler = $this->getApi($controller, $api);
        if (empty($apiHandler)) {
            return new Response("Can't find API");
        }

        // Authentication
        if (!$apiHandler->authenticate()) {
            return new Response("Can't authenticate");
        }

        // Execute
        $resp = $apiHandler->execute();
        if ($resp instanceof Response) {
            return $resp;
        }
        return new Response(serialize($resp));
    }

    public function handleCall($controller, &$data, $options = array()) {
        $request = $controller->get('request');

        $api = $request->get('api');
        $apiHandler = $this->getApi2($controller, $api);
        if (empty($apiHandler)) {
            return new Response("Can't find API");
        }

        // Authentication
        if (!$apiHandler->authenticate()) {
            return new Response("Can't authenticate");
        }

        // Execute
        $resp = $apiHandler->execute();
        if ($resp instanceof Response) {
            return $resp;
        }
        return new Response(json_encode($resp));
    }

    protected function getApi($controller, $api) {
        $handlers = array(
            'checkResources'                   =>    'leep_admin.report_api.check_resources',
            'checkFonts'                       =>    'leep_admin.report_api.check_fonts',
            'checkTextBlocks'                  =>    'leep_admin.report_api.check_text_blocks',
            'checkSyncRequest'                 =>    'leep_admin.report_api.check_sync_request',
            'downloadResource'                 =>    'leep_admin.report_api.download_resource',
            'downloadFont'                     =>    'leep_admin.report_api.download_font',
            'downloadReportFile'               =>    'leep_admin.report_api.download_report_file',
            'getReport'                        =>    'leep_admin.report_api.get_report',
            'getReportData'                    =>    'leep_admin.report_api.get_report_data',
            'getCryosaveReportData'            =>    'leep_admin.report_api.get_cryosave_report_data',
            'selectJob'                        =>    'leep_admin.report_api.select_job',
            'submitBuildDemoReportJob'         =>    'leep_admin.report_api.submit_build_demo_report_job',
            'submitBuildCustomerReportJob'     =>    'leep_admin.report_api.submit_build_customer_report_job',
            'submitBuildShipmentJob'           =>    'leep_admin.report_api.submit_build_shipment_job',
            'submitBuildCryosaveReportJob'     =>    'leep_admin.report_api.submit_build_cryosave_report_job',
            'submitBuildDcPriceReportJob'      =>    'leep_admin.report_api.submit_build_dc_price_report_job',
            'submitJobStatus'                  =>    'leep_admin.report_api.submit_job_status',
            'updateLowResolutionFile'          =>    'leep_admin.report_api.update_low_resolution_file',
            'updateReportWebCoverFile'         =>    'leep_admin.report_api.update_report_web_cover_file',
            'updateReportBodCoverFile'         =>    'leep_admin.report_api.update_report_bod_cover_file',
            'updateFoodTableExcelFile'         =>    'leep_admin.report_api.update_food_table_excel_file',
            'updateFoodTableExcelFile2'        =>    'leep_admin.report_api.update_food_table_excel_file_2',
            'updateReportXmlFile'              =>    'leep_admin.report_api.update_report_xml_file',
            'updateSyncRequest'                =>    'leep_admin.report_api.update_sync_request',
            'getFoodTableIngredient'           =>    'leep_admin.report_api.get_food_table_ingredient',
            'getFoodTableItem'                 =>    'leep_admin.report_api.get_food_table_item',
            'getFoodTableParameter'            =>    'leep_admin.report_api.get_food_table_parameter',
            'getFoodTableSmileyConversion'     =>    'leep_admin.report_api.get_food_table_smiley_conversion',
            'getLanguageFont'                  =>    'leep_admin.report_api.get_language_font',
            'getBodShipment'                   =>    'leep_admin.report_api.get_bod_shipment',
            'getLocalization'                  =>    'leep_admin.report_api.get_localization',
            'getRecipeLayouts'                 =>    'leep_admin.report_api.get_recipe_layouts',
            'getRecipe'                        =>    'leep_admin.report_api.get_recipe',
            'getLanguages'                     =>    'leep_admin.report_api.get_languages',
            'getUntranslatedPattern'           =>    'leep_admin.report_api.get_untranslated_pattern'
        );

        if (isset($handlers[$api])) {
            return $controller->get($handlers[$api]);
        }
        return null;
    }

    protected function getApi2($controller, $api) {
        $handlers = array(
            'getTextBlock'                     =>    'leep_admin.report_api.get_text_block',
            'createCmsOrder'                   =>    'leep_admin.cms_order_api.create_cms_order',
            'createCmsContactMessage'          =>    'leep_admin.cms_order_api.create_cms_contact_message',
            'genlifeApi'                       =>    'leep_admin.genlife_sample_registration.api.handler_genlife_api',
        );

        if (isset($handlers[$api])) {
            return $controller->get($handlers[$api]);
        }
        return null;
    }
}
