<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

use Aws\Sns\MessageValidator\Message;
use Aws\Sns\MessageValidator\MessageValidator;
use Guzzle\Http\Client;

use Leep\AdminBundle\Business\EmailMassSender\Constant;

class EmailMassSender extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleUploadImage($controller, &$data, $options = array()) {
        $this->container->get('kernel')->getRootdir();
        $webDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/attachments/redactor/';
        $viewDir = "/attachments/redactor/";

        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);
        if ($_FILES['file']['type'] == 'image/png'
        || $_FILES['file']['type'] == 'image/jpg'
        || $_FILES['file']['type'] == 'image/gif'
        || $_FILES['file']['type'] == 'image/jpeg'
        || $_FILES['file']['type'] == 'image/pjpeg')
        {
            // setting file's mysterious name
            $filename = md5(date('YmdHis')).'.jpg';
            $file = $webDir.$filename;

            // copying
            move_uploaded_file($_FILES['file']['tmp_name'], $file);

            return new Response(json_encode(['filelink' => $controller->get('request')->getUri()."/../../..".$viewDir.$filename]));
        }
        return new Response();
    }

    public function handleCountRecievers($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $tempData = $controller->get('leep_admin.helper.temp_data');

        $params = $request->get('form');
        $params = (is_array($params) ? $params : array($params));

        $totalReceivers = 0;
        $recipientsFactory = $this->container->get('leep_admin.email_mass_sender.business.recipients_factory');
        $model = (new Business\EmailMassSender\CreateModel())->loadFromParams($params);
        foreach (Constant::getReceiverTypeList() as $receiverType => $receiverName) {
            $totalReceivers += $recipientsFactory->grab($receiverType, $model, true);
        }

        $key = $tempData->generateKey();
        $tempData->save($key, $params);

        $viewFullReceiverListURL = $mgr->getUrl('leep_admin', 'email_mass_sender', 'feature', 'viewRecievers', array('key' => $key));
        $sendToData = "<a href='" . $viewFullReceiverListURL . "' target='_blank'>" .$totalReceivers . ' receivers' . "</a>";

        return new Response($sendToData);
    }

    public function handleViewRecievers($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $tempData = $controller->get('leep_admin.helper.temp_data');

        $key = $this->container->get('request')->get('key', '');
        $params = $tempData->get($key, array());
        $model = (new Business\EmailMassSender\CreateModel())->loadFromParams($params);

        $recipientsFactory = $this->container->get('leep_admin.email_mass_sender.business.recipients_factory');
        $data = array( 'tabs' => array() );
        $response = new Response();
        foreach (Constant::getReceiverTypeList() as $receiverType => $receiverName) {
            $emailList = $recipientsFactory->grab($receiverType, $model);
            $content = array();
            foreach ($emailList as $emailData) {
                $email = $emailData['email'];
                $name = $emailData['name'];
                $content[] = "$name &lt$email&gt";
            }

            $data['tabs'][$receiverType] = array(
                'title' =>      $receiverName,
                'content' =>    implode('</br>', $content)
            );
        }

        $response = $controller->render('LeepAdminBundle:EmailMassSender:receiver_list.html.twig', $data);
        return $response;
    }

    public function handleOptOut($controller, &$data, $options = array()) {
        // log the request
        $helperEncrypt = $controller->get('leep_admin.helper.encrypt');
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');

        $email = $helperEncrypt->decrypt($request->get('email', null));
        $type = $helperEncrypt->decrypt($request->get('type', Constant::RECEIVER_TYPE_PREDEFINED_RECIPIENT));
        $id = $helperEncrypt->decrypt($request->get('id', null));
        $name = $helperEncrypt->decrypt($request->get('name', null));

        $data['email'] = "$name <$email>";
        $recipientsFactory = $controller->get('leep_admin.email_mass_sender.business.recipients_factory');
        $recipientsFactory->unsubscribe($type, array_map('trim', explode(';', $email)), $id);

        return $controller->render('LeepAdminBundle:EmailMassSender:email_unsubscribe.html.twig', $data);
    }
}
