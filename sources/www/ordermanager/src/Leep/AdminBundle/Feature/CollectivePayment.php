<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Easy\CrudBundle\Feature\Grid;

class CollectivePayment extends Grid {    
    public function handleList($controller, &$data, $options = array()) {
        parent::handleList($controller, $data, $options);

        $id = $controller->get('request')->query->get('id', 0);
        $mgr = $controller->get('easy_module.manager');
        $data['viewPaymentUrl'] = $mgr->getUrl('leep_admin', 'collective_payment', 'payment_form_pdf',  'pdf',  array('id' => $id));    
    }
}