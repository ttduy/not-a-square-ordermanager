<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class MwaSample extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleBulkEdit($controller, &$data, $options = array()) {        
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = intval($v);
            }
        }

        $em = $controller->get('doctrine')->getEntityManager();    

        $form = $controller->get('leep_admin.mwa_sample.business.bulk_edit_handler');
        $form->selectedId = $arr;
        //$form->status = $status;
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit sample',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'bulkEdit', array('selectedId' => $selectedIdRaw))
        );

        return $controller->render('LeepAdminBundle:MwaSample:bulk_edit.html.twig', $data);
    }

    private function getBulkSamples($controller) {
        $sampleId = $controller->get('request')->get('sampleId', '');
        $sampleIds = explode("\n", $sampleId);

        $errors = array();
        $status = array();
        foreach ($sampleIds as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $criteria = array(
                    'sampleId' => $sampleId,
                    'idGroup'  => Business\MwaSample\Constants::GROUP_DEFAULT
                );
                $sample = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBy($criteria);
                if (!empty($sample)) {
                    if (!isset($status[$sample->getCurrentStatus()])) {
                        $status[$sample->getCurrentStatus()] = array(
                            'status'  => $sample->getCurrentStatus(),
                            'samples' => array()
                        );
                    }
                    $status[$sample->getCurrentStatus()]['samples'][] = $sample->getSampleId();
                }
                else {
                    $errors[] = $sampleId;
                }
            }
        }
        return array($status, $errors);
    }

    public function handleBulkValidate($controller, &$data, $options = array()) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        list($status, $error) = $this->getBulkSamples($controller);

        $resp = '';
        if (!empty($error)) {
            $resp .= '<b style="color:red">These samples don\'t exists:</b> <br/>'.implode('</br>', $error).'<br/>';
        }

        if (count($status) > 1) {
            $resp .= '<br/><b style="color:red">All samples must have the same status</b>';
            foreach ($status as $s) {
                $name = $formatter->format($s['status'], 'mapping', 'LeepAdmin_MwaStatus_List');
                $resp .='<br/><b style="color: red">'.$name.':</b> <br/>'.implode('<br/>', $s['samples']).'<br/>';
            }
        }

        if (empty($resp)) {            
            $s = array_pop($status);
            $name = $formatter->format($s['status'], 'mapping', 'LeepAdmin_MwaStatus_List');
            $resp .='<b style="color: green">Valid. Status = '.$name.'</b>';
        }

        return new Response($resp);
    }

    public function handleBulkGetStatus($controller, &$data, $options = array()) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        list($status, $error) = $this->getBulkSamples($controller);

        $resp = '<option></option>';
        if (count($status) == 1 && empty($error)) {
            $s = array_pop($status);
            $statusId = $s['status'];

            $nextStatus = Business\MwaSample\Utils::getNextStatus($controller->get('service_container'), $statusId);
            foreach ($nextStatus as $k => $v) {
                $resp .= '<option value="'.$k.'">'.$v."</option>";
            }
        }
        return new Response($resp);
    }


    public function handleViewLastError($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id', 0);
        $sample = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaSample')->findOneById($id);
        $data['lastError'] = $sample->getLastError();

        return $controller->render('LeepAdminBundle:MwaSample:last_error.html.twig', $data);
    }

    public function handleUpdateResult($controller, &$data, $options = array()) {
        $sampleId = $controller->get('request')->query->get('sampleId', false);
        $result = $controller->get('request')->query->get('result', false);
        $acceptPatterns = array('PATTERN1', 'PATTERN2','PATTERN3','PATTERN4','PATTERN5','PATTERN6');
        if ($sampleId == false || (!in_array($result, $acceptPatterns))) {
            $response = new Response('Failed');
            $response->setStatusCode(400);
            return $response;
        }
        $criteria = array(
            'sampleId' => $sampleId,
            'idGroup'  => Business\MwaSample\Constants::GROUP_DEFAULT
        );
        $samples = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaSample')->findBy($criteria);
        if (empty($samples)) {
            $response = new Response('Failed');
            $response->setStatusCode(400);
            return $response;
        }
        foreach ($samples as $sample) {
            $sample->setResultPattern($result);
        }
        $controller->getDoctrine()->getEntityManager()->flush();
        $response = new Response('OK');
        $response->setStatusCode(200);
        return $response;
    }

    public function handleBulkEditTat($controller, &$data, $options = array()) {        
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = intval($v);
            }
        }

        $em = $controller->get('doctrine')->getEntityManager();    

        $form = $controller->get('leep_admin.mwa_sample.business.bulk_edit_tat_handler');
        $form->selectedId = $arr;
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit tat sample',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'bulkEditTat', array('selectedId' => $selectedIdRaw))
        );

        return $controller->render('LeepAdminBundle:MwaSample:bulk_edit_tat.html.twig', $data);
    }


    public function handleComputePattern($controller, &$data, $options = array()) {        
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = intval($v);
            }
        }

        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();    
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $formulaTemplate = $doctrine->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($config->getIdFormulaTemplateMwaSample());

        $form = $controller->get('leep_admin.mwa_sample.business.compute_pattern_handler');
        $form->formulaTemplate = $formulaTemplate;
        $form->selectedId = $arr;
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Compute pattern',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Compute',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'computePattern', array('selectedId' => $selectedIdRaw))
        );

        $data['resultData'] = $form->resultData;

        return $controller->render('LeepAdminBundle:MwaSample:compute_pattern.html.twig', $data);
    }
}
