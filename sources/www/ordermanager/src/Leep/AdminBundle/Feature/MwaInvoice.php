<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Easy\CrudBundle\Feature\Grid;

class MwaInvoice extends Grid {    
    public function handleList($controller, &$data, $options = array()) {
        parent::handleList($controller, $data, $options);

        $id = $controller->get('request')->query->get('id', 0);
        $mgr = $controller->get('easy_module.manager');
        $data['viewInvoiceUrl'] = $mgr->getUrl('leep_admin', 'mwa_invoice', 'invoice_form_edit',  'pdf',  array('id' => $id));
        $data['invoiceId'] = $id;
    }


    public function handlePdf($controller, &$data, $options = array()) { 
        set_time_limit(120);
           
        $idMwaInvoice = $controller->get('request')->query->get('id', 0);       
        $mwaInvoice = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($idMwaInvoice);
        $isRegenerate = $controller->get('request')->get('isRegenerate', 0);
        if ($isRegenerate || trim($mwaInvoice->getInvoiceFile()) == '') {
            $result = Business\MwaInvoiceForm\InvoiceUtil::buildPdf($controller, $idMwaInvoice);
            if ($result instanceof Response) {
                return $result;
            }
        }    

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';

        
        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename=mwa_invoice.pdf');       
        $resp->setContent(file_get_contents($filePath.'/'.$mwaInvoice->getInvoiceFile()));
        return $resp;
    }

}