<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;

class EmployeeFeedbackQuestion extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleViewEditAnswer($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $config = $controller->get('LeepAdmin.Helper.Common')->getConfig();
        $mapping = $controller->get('easy_mapping');

        $questionList = array();
        $questions = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findAll();
        foreach ($questions as $question) {
            $questionList[] = array(
                'id'         => $question->getId(),
                'type'       => $question->getIdType(),
                'question'   => $question->getQuestion(),
                'choices'    => Business\EmployeeFeedbackQuestion\Util::parseChoices($question->getData())
            );
        }

        $data['welcomeMessage'] = nl2br($config->getFeedbackWelcomeMessage());
        $data['questions'] = $questionList;
        $data['actionUrl'] = $mgr->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'submitForm');
        $data['canEditUser'] = true;
        $data['userList'] = $mapping->getMapping('LeepAdmin_WebUser_List');

        return $controller->render('LeepAdminBundle:EmployeeFeedbackQuestion:form.html.twig', $data);
    }


    public function handleViewForm($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $config = $controller->get('LeepAdmin.Helper.Common')->getConfig();

        $questionList = array();
        $questions = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findAll();
        foreach ($questions as $question) {
            $questionList[] = array(
                'id'         => $question->getId(),
                'type'       => $question->getIdType(),
                'question'   => $question->getQuestion(),
                'choices'    => Business\EmployeeFeedbackQuestion\Util::parseChoices($question->getData())
            );
        }

        $data['welcomeMessage'] = nl2br($config->getFeedbackWelcomeMessage());
        $data['questions'] = $questionList;
        $data['actionUrl'] = $mgr->getUrl('leep_admin', 'employee_feedback_question', 'feature', 'submitForm');
        $data['canEditUser'] = false;

        return $controller->render('LeepAdminBundle:EmployeeFeedbackQuestion:form.html.twig', $data);
    }

    public function handleSubmitForm($controller, &$data, $options = array()) {
        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $currentTime = new \DateTime();
        $userId = $userManager->getUser()->getId();
        if ($request->get('selectedUserId', null)) {
            $userId = $request->get('selectedUserId');
        }

        $parameters = $request->request->all();
        foreach ($parameters as $key => $value) {
            if (strpos($key, 'q_') === 0) {
                $feedbackId = substr($key, 2);

                if (empty($value)) {
                    continue;
                }

                $employeeFeedbackResponse = new Entity\EmployeeFeedbackResponse();
                $employeeFeedbackResponse->setIdEmployeeFeedbackQuestion($feedbackId);
                $employeeFeedbackResponse->setIdUser($userId);
                $employeeFeedbackResponse->setAnswer(json_encode($value));
                $employeeFeedbackResponse->setResponseTime($currentTime);
                $em->persist($employeeFeedbackResponse);
            }
        }
        $em->flush();

        //$this->updateSummary($em);

        return $controller->render('LeepAdminBundle:EmployeeFeedbackQuestion:formSubmit.html.twig', $data);
    }


    public function handleUpdateStats($controller, &$data, $options = array()) {
        Business\EmployeeFeedbackQuestion\Util::updateStats($controller);
        return $controller->render('LeepAdminBundle:EmployeeFeedbackQuestion:updateStats.html.twig', $data);
    }

    public function handleViewSummary($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        if (!$helperSecurity->hasPermission('employeeFeedbackResponseModify')) {
            die('Access denied');
        }

        $filter = $controller->get('leep_admin.employee_feedback_question.business.summary_filter_handler');
        $filter->execute();

        $data['filter'] = array(
            'id'       => 'Form_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Apply Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => '',
            'visiblity' => ''
        );
        $filterData = $filter->getCurrentFilter();

        // All period
        $em = $controller->get('doctrine')->getEntityManager();
        $summary = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:EmployeeFeedbackQuestion', 'p')
            ->andWhere('p.idType = '.Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE.' OR '.
                       'p.idType = '.Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE)
            ->orderBy('p.id', 'ASC');

        $questions = $query->getQuery()->getResult();
        foreach ($questions as $question) {
            $d = array();

            $d['question'] = $question->getQuestion();
            $choices = Business\EmployeeFeedbackQuestion\Util::parseChoices($question->getData());
            $d['choices'] = $choices;

            // Overall period
            $stats = Business\EmployeeFeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, null, null, true);
            $d['overallStats'] = $stats['stats'];
            $d['overallAverageScore'] = $stats['averageScore'];
            $d['overallAverageValue'] = $stats['averageValue'];

            // Period 1
            $stats = Business\EmployeeFeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, $filterData->dateFrom1, $filterData->dateTo1);

            if (empty($stats)) {
                $d['period1'] = false;
            }
            else {
                $d['period1'] = true;
                $d['period1Stats'] = $stats['stats'];
                $d['period1AverageScore'] = $stats['averageScore'];
                $d['period1AverageValue'] = $stats['averageValue'];
            }

            // Period 2
            $stats = Business\EmployeeFeedbackQuestion\Util::computeStats($controller, $question->getId(), $choices, $filterData->dateFrom2, $filterData->dateTo2);
            if (empty($stats)) {
                $d['period2'] = false;
            }
            else {
                $d['period2'] = true;
                $d['period2Stats'] = $stats['stats'];
                $d['period2AverageScore'] = $stats['averageScore'];
                $d['period2AverageValue'] = $stats['averageValue'];
            }

            $summary[] = $d;
        }



        $data['summary'] = $summary;

        return $controller->render('LeepAdminBundle:EmployeeFeedbackQuestion:viewSummary.html.twig', $data);
    }
}
