<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class Form extends AbstractFeature {
    private $locations;

    public function __construct($container) {

        $this->locations = Business\GenoStorage\Utils::getLocationList($container);
    }

    public function getActions() { return array('update'); }

    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleShowVersionHistoryLogs($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');

        $logs = $em->getRepository('AppDatabaseMainBundle:FormVersionLogs')->findBy(
            ['formVersionId' => $id],
            ['createdDate' => 'ASC']
        );

        $data['logs'] = [];
        foreach ($logs as $log) {
            $data['logs'][] = [
                'date' =>   $formatter->format($log->getCreatedDate(), 'datetime'),
                'by' =>     $formatter->format($log->getCreatedBy(), 'mapping', 'LeepAdmin_WebUser_List'),
                'notes' =>  $log->getNotes()
            ];
        }

        return $controller->render('LeepAdminBundle:Form:log_form_version_history.html.twig', $data);
    }

    public function handleMakeVersion($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $idFormVersion = $controller->get('request')->get('id', 0);
        $em = $controller->get('doctrine')->getEntityManager();

        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($idFormVersion);
        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());

        $query = $em->createQueryBuilder();
        $query->select('MAX(p.version)')
            ->from('AppDatabaseMainBundle:FormVersion', 'p')
            ->andWhere('p.idForm = :idForm')
            ->setParameter('idForm', $form->getId());
        $maxVersion = intval($query->getQuery()->getSingleScalarResult());

        // Clone form version
        $newFormVersion = new Entity\FormVersion();
        $newFormVersion->setIdForm($formVersion->getIdForm());
        $newFormVersion->setCreationDatetime(new \DateTime());
        $newFormVersion->setVersion($maxVersion + 1);
        $em->persist($newFormVersion);
        $em->flush();

        $form->setIdActiveFormVersion($newFormVersion->getId());
        $em->flush();

        // Clone steps
        $formSteps = $em->getRepository('AppDatabaseMainBundle:FormStep')->findByIdFormVersion($formVersion->getId());
        foreach ($formSteps as $formStep) {
            $newFormStep = new Entity\FormStep();
            $newFormStep->setIdFormVersion($newFormVersion->getId());
            $newFormStep->setName($formStep->getName());
            $newFormStep->setSortOrder($formStep->getSortOrder());
            $em->persist($newFormStep);
            $em->flush();

            $formItems = $em->getRepository('AppDatabaseMainBundle:FormItem')->findByIdFormStep($formStep->getId());
            foreach ($formItems as $formItem) {
                $newFormItem = new Entity\FormItem();
                $newFormItem->setIdFormStep($newFormStep->getId());
                $newFormItem->setIdType($formItem->getIdType());
                $newFormItem->setData($formItem->getData());
                $newFormItem->setSortOrder($formItem->getSortOrder());
                $em->persist($newFormItem);
            }
            $em->flush();
        }

        $url = $mgr->getUrl('leep_admin', 'form', 'edit_step', 'edit', array('id' => $newFormVersion->getId()));
        return new RedirectResponse($url);
    }

    private function setListFormTabs($controller, &$data) {
        $mgr = $controller->get('easy_module.manager');
        $data['mainTabs'] = array(
            'content' => array(
                'listForm'              => array('title' => 'Form',                'link' => $mgr->getUrl('leep_admin', 'form', 'feature', 'listForm')),
                'listActiveFormSession' => array('title' => 'My Active form session', 'link' => $mgr->getUrl('leep_admin', 'form', 'feature', 'listActiveFormSession')),
            )
        );
    }

    public function handleListForm($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');

        $forms = array();
        $query = $em->createQueryBuilder();
        $query->select('p.formNumber, p.name, p.colorCode, v.id, v.version, v.creationDatetime')
            ->from('AppDatabaseMainBundle:Form', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormVersion', 'v', 'WITH', 'p.idActiveFormVersion = v.id')
            ->orderBy('p.sortOrder', 'ASC');
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $arr = explode('|', $r['colorCode']);
            $bgColor = trim($arr[0]);
            $textColor = isset($arr[1]) ? trim($arr[1]) : '#FFFFFF';

            $forms[] = array(
                'id'            => $r['id'],
                'number'        => $r['formNumber'],
                'name'          => $r['name'],
                'bgColor'       => $bgColor,
                'textColor'     => $textColor,
                'version'       => $r['version'],
                'time'          => $formatter->format($r['creationDatetime'], 'datetime')
            );
        }
        $data['forms'] = $forms;

        $data['useFormUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'startForm');

        $this->setListFormTabs($controller, $data);
        $data['mainTabs']['selected'] = 'listForm';

        return $controller->render('LeepAdminBundle:Form:listForm.html.twig', $data);
    }

    public function handleListActiveFormSession($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');

        $formSessions = array();

        $query = $em->createQueryBuilder();
        $query->select('p.id, p.creationDatetime, f.name, f.formNumber, f.colorCode, v.version')
            ->from('AppDatabaseMainBundle:FormSession', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormVersion', 'v', 'WITH', 'p.idFormVersion = v.id')
            ->innerJoin('AppDatabaseMainBundle:Form', 'f', 'WITH' , 'v.idForm = f.id')
            ->andWhere('p.idWebUser = :idWebUser')
            ->andWhere('p.status = :statusOpen')
            ->setParameter('idWebUser', $userManager->getUser()->getId())
            ->setParameter('statusOpen', Business\Form\Constant::STATUS_OPEN)
            ->orderBy('p.creationDatetime', 'DESC');
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $r['creationDatetime'] = $formatter->format($r['creationDatetime'], 'datetime');
            $formSessions[] = $r;
        }

        $data['formSessions'] = $formSessions;
        $data['viewFormUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'viewForm');

        $this->setListFormTabs($controller, $data);
        $data['mainTabs']['selected'] = 'listActiveFormSession';

        return $controller->render('LeepAdminBundle:Form:listActiveFormSession.html.twig', $data);
    }

    public function handleStartForm($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');

        $id = $request->get('id', 0);
        $idWebUser = $userManager->getUser()->getId();
        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($id);

        $newFormSession = new Entity\FormSession();
        $newFormSession->setCreationDatetime(new \DateTime());
        $newFormSession->setIdWebUser($idWebUser);
        $newFormSession->setIdFormVersion($formVersion->getId());
        $newFormSession->setIdActiveFormStep(0);
        $newFormSession->setStatus(Business\Form\Constant::STATUS_OPEN);
        $em->persist($newFormSession);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'form', 'feature', 'viewForm', array('id' => $newFormSession->getId()));
        return new RedirectResponse($url);
    }

    public function handleViewForm($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $formHandler = $controller->get('leep_admin.form.business.form_handler');

        $this->setListFormTabs($controller, $data);
        $data['mainTabs']['content']['useForm'] = array('title' => 'Use form', 'link' => '#');
        $data['mainTabs']['selected'] = 'useForm';

        // Load form session
        $idFormSession = $request->get('id', 0);

        $formHandler->viewFormSession($data, $idFormSession);

        $data['formActionUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'doForm');
        $data['switchStepUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'switchStep');
        $data['idFormSession'] = $idFormSession;

        return $controller->render('LeepAdminBundle:Form:viewForm.html.twig', $data);
    }

    public function handleSwitchStep($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $idFormSession = $request->get('idFormSession', 0);
        $idStep = $request->get('idStep', 0);

        $formSession = $em->getRepository('AppDatabaseMainBundle:FormSession')->findOneById($idFormSession);
        $formSession->setIdActiveFormStep($idStep);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'form', 'feature', 'viewForm', array('id' => $idFormSession));
        return new RedirectResponse($url);
    }

    public function handleDoForm($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');
        $formHandler = $controller->get('leep_admin.form.business.form_handler');

        $idFormSession = $request->get('idFormSession', 0);
        $formAction = $request->get('formAction', 'save');

        $formHandler->saveFormStep($data, $idFormSession, $formAction);

        $url = $mgr->getUrl('leep_admin', 'form', 'feature', 'viewForm', array('id' => $idFormSession));

        return new RedirectResponse($url);
    }

    public function handleDeleteFormSession($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $idFormSession = $request->get('id', 0);

        $formSession = $em->getRepository('AppDatabaseMainBundle:FormSession')->findOneById($idFormSession);

        if ($formSession->getStatus() == Business\Form\Constant::STATUS_OPEN) {
            $em->remove($formSession);
            $em->flush();
        }

        $url = $mgr->getUrl('leep_admin', 'form', 'form_session_grid', 'list');

        return new RedirectResponse($url);

    }

    public function handleShowGenoForm($controller, &$data, $options = array()){
        $mgr = $controller->get('easy_module.manager');
        $data['printBarcodePage'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'printBarcode');
        $data['moveStockPage'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'moveStock');
        $data['removeStockPage'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'removeStock');
        $data['createPelletPage'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'createPellet');

        $url = 'LeepAdminBundle:GenoForm:geno_form.html.twig';
        $response = $controller->renderView($url, $data);
        return new Response($response);
    }

    public function handlePrintBarcode($controller, &$data, $options = array()){
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $mgr = $controller->get('easy_module.manager');
        $mapping = $controller->get('easy_mapping');

        $data['getSubtanceNameUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceName');
        $data['getSubtanceLoadUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceLoad');
        $data['getBatchIdUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getBatchId');
        $data['getGenoMaterialTypeUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getGenoMaterialType');
        $data['getAmountInStockUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getAmountInStock');
        $data['getLocationUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getLocation');
        $data['getOrderedByUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getOrderedBy');
        $data['getOrderedDateUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getOrderedDate');
        $data['getOrderedPriceUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getOrderedPrice');
        $data['getUnitPriceUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getUnitPrice');
        $data['getReceivedByUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getReceivedBy');
        $data['getRecievedDateUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getRecievedDate');
        $data['getExpiredDateUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getExpiredDate');
        $data['updateSelectedPrinterReport'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'updateSelectedPrinterReport');
        $data['generateReportUrl'] = $mgr->getUrl('leep_admin', 'report', 'report', 'viewHoldReportPdf');

        $data['reportList'] = $mapping->getMapping('LeepAdmin_Report_List');
        $data['languageList'] = $mapping->getMapping('LeepAdmin_Language_List');
        $data['selectedPrinterReport'] = $config->getGenoBatchPrinterReportTemplateId();

        $url = 'LeepAdminBundle:GenoForm:printBarcode.html.twig';
        $response = $controller->renderView($url, $data);

        return new Response($response);
    }

    public function handleUpdateSelectedPrinterReport($controller, &$data, $options = []) {
        $em = $controller->get('doctrine')->getEntityManager();
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $id = $controller->get('request')->get('id', 0);

        $config->setGenoBatchPrinterReportTemplateId($id);
        $em->persist($config);
        $em->flush();

        return new Response('SUCCESS');
    }

    public function handleMoveStock($controller, &$data, $options = array()){
        $mgr = $controller->get("easy_module.manager");
        $data['getSubtanceNameUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceName');
        $data['getSubtanceLoadUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceLoad');
        $data['getBatchIdUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getBatchId');
        $data['getGenoMaterialTypeUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getGenoMaterialType');
        $data['getAmountInStockUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getAmountInStock');
        $data['getLocationUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getLocation');
        $data['moveBatchUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'moveBatch');
        $data['getLocationInfoUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getLocationInfo');

        $url = 'LeepAdminBundle:GenoForm:moveStock.html.twig';

        $response = $controller->renderView($url, $data);
        return new Response($response);
    }

    public function handleRemoveStock($controller, &$data, $options = array())
    {
        $mgr = $controller->get("easy_module.manager");
        $data['getSubtanceNameUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceName');
        $data['getSubtanceLoadUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getSubtanceLoad');
        $data['getBatchIdUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getBatchId');
        $data['getGenoMaterialTypeUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getGenoMaterialType');
        $data['getAmountInStockUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getAmountInStock');
        $data['getLocationUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'getLocation');
        $data['removeStockProcessUrl'] = $mgr->getUrl('leep_admin', 'form', 'feature', 'removeStockProcess');

        $url = "LeepAdminBundle:GenoForm:removeStock.html.twig";

        $response = $controller->renderView($url, $data);
        return new Response($response);
    }

    public function handleCreatePellet($controller, &$data, $options=array())
    {
        $mgr = $controller->get("easy_module.manager");
        $href = $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'listFormula');
        return new Response($href);
    }

    public function handleGetBatchId($controller, &$data, $options = array()){
        // get barcode input
        $barcode = $controller->get('request')->get('barcode');
        // TODO: query get barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.batchId')
              ->from('AppDatabaseMainBundle:GenoLotNumbers', 'g')
              ->andWhere('g.value = :barcode')
              ->setParameter('barcode', $barcode);

        // $batchId = $query->getQuery()->getSingleScalarResult();
        $batchId = $query->getQuery()->getSingleScalarResult();
        return new Response($batchId);
    }

    public function handleGetSubtanceName($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.idGenoMaterial')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $genoMaterialId = $query->getQuery()->getSingleScalarResult();

        $query->select('gm.fullName')
              ->from('AppDatabaseMainBundle:GenoMaterials','gm')
              ->andWhere('gm.id = :genoMaterialId')
              ->setParameter('genoMaterialId', $genoMaterialId);
        $fullSubtanceName = $query->getQuery()->getSingleScalarResult();

        // return data
        return new Response($fullSubtanceName);
    }

    public function handleGetSubtanceLoad($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.loading')
              ->from('AppDatabaseMainBundle:GenoBatches','g')
              ->where('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $batchLoading = $query->getQuery()->getSingleScalarResult();

        // return data
        return new Response($batchLoading);
    }

    public function handleGetGenoMaterialType($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));
        // create mapping to get more library
        $mapping = $controller->get('easy_mapping');

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.idGenoMaterial')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $genoMaterialId = $query->getQuery()->getSingleScalarResult();

        $query->select('gm.type')
              ->from('AppDatabaseMainBundle:GenoMaterials', 'gm')
              ->andWhere('gm.id = :genoMaterialId')
              ->setParameter('genoMaterialId', $genoMaterialId);
        $genoMaterialType = $query->getQuery()->getSingleScalarResult();

        $typeList =  Business\GenoMaterialType\Utils::getGenoMaterialTypeList($controller);
        $typeName = isset($typeList[$genoMaterialType]) ? $typeList[$genoMaterialType] : "";

        // return geno material type
        return new Response($typeName);
    }

    public function handleGetAmountInStock($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.amountInStock')
              ->from("AppDatabaseMainBundle:GenoBatches", 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter("batchId", $batchId);
        $currentAmountInStock = $query->getQuery()->getSingleScalarResult();

        // return
        return new Response($currentAmountInStock);
    }

    public function handleGetLocation($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.location')
              ->from("AppDatabaseMainBundle:GenoBatches", 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $currentLocation = $query->getQuery()->getSingleScalarResult();
        $type = gettype($currentLocation);

        $fullLocation = $this->locations[$currentLocation];

        // return
        return new Response($fullLocation);
    }

    public function handleGetLocationInfo($controller, &$data, $options = array())
    {
        $idLocationArr = array();
        $fullInfoLocationArr = array();
        $i = 0;
        foreach ($this->locations as $key => $value) {
            $idLocationArr[$i] = $key;
            $fullInfoLocationArr[$i] = $value;
            $i++;
        }
        $locationInfo = [$idLocationArr, $fullInfoLocationArr];
        $locationInfo_json = json_encode($locationInfo);

        return new Response($locationInfo_json);
    }

    public function handleGetOrderedBy($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.orderedBy')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $orderedById = $query->getQuery()->getSingleScalarResult();

        // get orderedBy name
        $query->select('u.username')
              ->from('AppDatabaseMainBundle:WebUsers', 'u')
              ->andWhere('u.id = :orderedById')
              ->setParameter('orderedById', $orderedById);
        $nameOrderedBy = $query->getQuery()->getSingleScalarResult();

        return new Response($nameOrderedBy);
    }

    public function handleGetOrderedDate($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));
        $formatter = $controller->get('leep_admin.helper.formatter');

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $batch = $query->getQuery()->getResult();

        return new Response($formatter->format($batch[0]->getOrderedDate(), 'date'));
    }

    public function handleGetOrderedPrice($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.orderedPrice')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $orderedPrice = $query->getQuery()->getSingleScalarResult();

        return new Response($orderedPrice);
    }

    public function handleGetUnitPrice($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.unitPrice')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $unitPrice = $query->getQuery()->getSingleScalarResult();

        return new Response($unitPrice);
    }

    public function handleGetReceivedBy($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g.recievedBy')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $receivedById = $query->getQuery()->getSingleScalarResult();

        // get orderedBy name
        $query->select('u.username')
              ->from('AppDatabaseMainBundle:WebUsers', 'u')
              ->andWhere('u.id = :receivedById')
              ->setParameter('receivedById', $receivedById);
        $nameReceivedBy = $query->getQuery()->getSingleScalarResult();

        return new Response($nameReceivedBy);
    }

    public function handleGetRecievedDate($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));
        $formatter = $controller->get('leep_admin.helper.formatter');

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $batch = $query->getQuery()->getResult();

        return new Response($formatter->format($batch[0]->getRecievedDate(), 'date'));
    }

    public function handleGetExpiredDate($controller, &$data, $options = array()){
        // get batch Id
        $batchId = intval($controller->get('request')->get('batchId'));
        $formatter = $controller->get('leep_admin.helper.formatter');

        // query data from databse. only select batch correspond passed barcode
        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('g')
              ->from('AppDatabaseMainBundle:GenoBatches', 'g')
              ->andWhere('g.id = :batchId')
              ->setParameter('batchId', $batchId);
        $batch = $query->getQuery()->getResult();

        return new Response($formatter->format($batch[0]->getExpiryDate(), 'date'));
    }

    public function handleMoveBatch($controller, &$data, $options = array()){
        $movingData = json_decode($controller->get('request')->get('passData'));
        // Passdata contain data with the order: batch Id, amount need to move, destination cell ID
        $batchId = intval($movingData[0]);
        $amountMove = intval($movingData[1]);
        $destinationCellId = intval($movingData[2]);

        $done = Business\GenoStorage\Utils::moveGenoBatch($controller, $batchId, $amountMove, $destinationCellId);
        if ($done == "1") {
            return new Response("1");
        }

        return new Response("0");
    }

    public function handleRemoveStockProcess($controller, &$data, $options = array()){
        $removeData = json_decode($controller->get('request')->get('passData'), true);

        $batchId = intval($removeData[0]);
        $removeAmount = intval($removeData[1]);

        $done = Business\GenoStorage\Utils::removeGenoBatch($controller, $batchId, $removeAmount);
        if ($done == "1") {
            return new Response("1");
        }

        return new Response("1");
    }
}
