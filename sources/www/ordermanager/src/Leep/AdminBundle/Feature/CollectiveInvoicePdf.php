<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business\CollectiveInvoice\Utils;

class CollectiveInvoicePdf extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handlePdf($controller, &$data, $options = array()) {        
        $idCollectiveInvoice = $controller->get('request')->query->get('id', 0);       

        $collectiveInvoice = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($idCollectiveInvoice);

        $isRegenerate = $controller->get('request')->get('isRegenerate', 0);
        if ($isRegenerate) {
            $result = Utils::buildPdf($controller, $idCollectiveInvoice);
            if ($result instanceof Response) {
                return $result;
            }
        }    
        else {
            if (trim($collectiveInvoice->getInvoiceFile()) == '') {
                $resp = new Response();
                $resp->setContent("No invoice is available, please regenerate new invoice");
                return $resp;
            }
        }

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';

        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename=invoice.pdf');        
        $resp->setContent(file_get_contents($filePath.'/'.$collectiveInvoice->getInvoiceFile()));
        return $resp;
    }

    public function handleEmail($controller, &$data, $options = array()) {        

    }
}
