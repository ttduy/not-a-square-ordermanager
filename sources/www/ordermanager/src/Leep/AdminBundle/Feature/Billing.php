<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Billing extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handleViewPriceChanges($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $pricingHelper = $controller->get('leep_admin.billing.business.pricing_helper');

        $idCustomer = $request->get('idCustomer', 0);
        $entryCode = $request->get('entryCode', '');

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $data['customerPriceCategory'] = $formatter->format($customer->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List');
        $data['customer'] = $customer;
        $data['customerOrderDate'] = $formatter->format($customer->getDateOrdered(), 'date');

        if ($entryCode[0] == 'P') {
            $idProduct = intval(substr($entryCode, 2));
            $data['item'] = $formatter->format($idProduct, 'mapping', 'LeepAdmin_Product_List');
            $data['itemType'] = 'Product';
            $data['currentPrice'] = $pricingHelper->getProductPrice($customer->getPriceCategoryId(), $idProduct);
            
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:PricesHistory', 'p')
                ->andWhere('p.productid = '.$idProduct)
                ->andWhere('p.pricecatid = '.$customer->getPriceCategoryId())
                ->orderBy('p.priceTimestamp', 'DESC')
                ->setMaxResults(500);
            $result = $query->getQuery()->getResult();
            $dates = array();
            $data['historyPrices'] = array();
            foreach ($result as $r) {
                $date = $formatter->format($r->getPriceTimestamp(), 'date');
                if (!isset($dates[$date])) {
                    $dates[$date] = 1;
                    $data['historyPrices'][] = array(
                        'date'   => $date,
                        'price'  => $r->getPrice(),
                        'dcMargin' => $r->getDCMargin(),
                        'partnerMargin' => $r->getPartMargin()
                    );
                }
            }            
        }
        else if ($entryCode[0] == 'C') {
            $idCategory = intval(substr($entryCode, 2));
            $data['item'] = $formatter->format($idCategory, 'mapping', 'LeepAdmin_Category_List');
            $data['itemType'] = 'Category';
            $data['currentPrice'] = $pricingHelper->getCategoryPrice($customer->getPriceCategoryId(), $idCategory);
            
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:CategoryPricesHistory', 'p')
                ->andWhere('p.categoryid = '.$idCategory)
                ->andWhere('p.pricecatid = '.$customer->getPriceCategoryId())
                ->orderBy('p.priceTimestamp', 'DESC')
                ->setMaxResults(500);
            $result = $query->getQuery()->getResult();
            $dates = array();
            $data['historyPrices'] = array();
            foreach ($result as $r) {
                $date = $formatter->format($r->getPriceTimestamp(), 'date');
                if (!isset($dates[$date])) {
                    $dates[$date] = 1;
                    $data['historyPrices'][] = array(
                        'date'   => $date,
                        'price'  => $r->getPrice(),
                        'dcMargin' => $r->getDCMargin(),
                        'partnerMargin' => $r->getPartMargin()
                    );
                }
            }    

        }
        return $controller->render('LeepAdminBundle:Billing:view_price_changes.html.twig', $data);
    }

    public function handleLoadLatestPrice($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');

        $idCustomer = $request->get('idCustomer', 0);
        $entryCode = $request->get('entryCode', '');
        $pricingHelper = $controller->get('leep_admin.billing.business.pricing_helper');

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);

        if ($entryCode[0] == 'P') {
            $idProduct = intval(substr($entryCode, 2));
            $currentPrice = $pricingHelper->getProductPrice($customer->getPriceCategoryId(), $idProduct);
            if (!empty($currentPrice)) {
                $resp = new Response();
                $resp->setContent(json_encode($currentPrice));
                return $resp;
            }
        }
        else if ($entryCode[0] == 'C') {
            $idCategory = intval(substr($entryCode, 2));
            $currentPrice = $pricingHelper->getCategoryPrice($customer->getPriceCategoryId(), $idCategory);
            if (!empty($currentPrice)) {
                $resp = new Response();
                $resp->setContent(json_encode($currentPrice));
                return $resp;
            }
        }
        
        return new Response('FAIL');
    }
}
