<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class AjaxUpdate extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleUpdate($controller, &$data, $options = array()) {
        $fieldId = $controller->getRequest()->get('field');
        if (isset($options['handlers'][$fieldId])) {
            $handler = $options['handlers'][$fieldId];
            if (is_string($handler)) {
                $handler = $controller->get($handler);
            }

            $handler->update($controller);

            return new Response('OK');            
        }   

        return new Response('FAIL');
    }
}
