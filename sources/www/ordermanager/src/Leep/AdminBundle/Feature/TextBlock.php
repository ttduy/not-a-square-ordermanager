<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class TextBlock extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleSynchronizeGroup($controller, &$data, $options = array()) {
        $form = $controller->get('leep_admin.text_block.business.synchronize_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Synchronize group',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Process',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'synchronizeGroup')
        );

        return $controller->render('LeepAdminBundle:TextBlock:synchronize_group.html.twig', $data);
    }

    public function handleSyncServers($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $form = $controller->get('leep_admin.text_block.business.sync_servers_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Sync servers',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Sync',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'syncServers')
        );


        $records = $em->getRepository('AppDatabaseMainBundle:SyncRequest')->findAll();
        if (!empty($records)) {
            $syncRequest = array_pop($records);
            $now = new \DateTime();
            $ts = round(($now->getTimestamp() - $syncRequest->getTimestamp()->getTimestamp()) / 60.0, 2);
            $lu = '';
            if ($syncRequest->getLastUpdated() != null) {
                $lu = round(($now->getTimestamp() - $syncRequest->getLastUpdated()->getTimestamp()) / 60.0, 2).'m ago'; 
            }
            $data['syncRequest'] = array(
                'user'            => $formatter->format($syncRequest->getIdUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                'timestamp'       => $ts.'m ago',
                'lastUpdated'     => $lu,
                'isSyncTextBlock' => $syncRequest->getIsSyncTextBlock() ? 'Yes' : '',
                'isSyncTextBlockStatus' => $formatter->format($syncRequest->getIsSyncTextBlockStatus(), 'mapping', 'LeepAdmin_SyncServers_Status'),
                'isSyncResources' => $syncRequest->getIsSyncResources() ? 'Yes' : '',
                'isSyncResourcesStatus' => $formatter->format($syncRequest->getIsSyncResourcesStatus(), 'mapping', 'LeepAdmin_SyncServers_Status'),
            );
        }

        return $controller->render('LeepAdminBundle:TextBlock:sync_servers.html.twig', $data);
    }

    public function handleCache($controller, &$data, $options = array()) {
        set_time_limit(500);
        $em = $controller->get('doctrine')->getEntityManager();
        $cache = $controller->get('text_block_cache');
        
        $query = $em->createQueryBuilder();
        $query->select('tb.name, p.idLanguage, p.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->leftJoin('AppDatabaseMainBundle:TextBlock', 'tb', 'WITH', 'tb.id = p.idTextBlock');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $cache->setLanguage($r['idLanguage']);
            $cache->saveBlock($r['name'], $r['body']);
        }

        return $controller->render('LeepAdminBundle:TextBlock:cache.html.twig', $data);
    }

    private function csvEncode($text) {
        return '"'.str_replace('"', '""', $text).'"';
    }

    private function archiveGroup($controller, $idGroup) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        
        // Search groups
        $group = $doctrine->getRepository('AppDatabaseMainBundle:TextBlockGroup')->findOneById($idGroup);

        // Group header
        $groupHeader = array();
        $groupHeader[] = $this->csvEncode('GROUP');
        $groupHeader[] = $this->csvEncode($group->getName());

        // Builder header
        $header = array();
        $header[] = $this->csvEncode('Text block');
        $languages = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($languages as $l) {
            $header[] = $this->csvEncode($l->getName());
        }        

        // Load text blocks
        $query = $em->createQueryBuilder();
        $query->select('tb.id, tb.name, p.idLanguage, p.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->leftJoin('AppDatabaseMainBundle:TextBlock', 'tb', 'WITH', 'tb.id = p.idTextBlock')
            ->andWhere('tb.idGroup = :idGroup')
            ->setParameter('idGroup', $idGroup);
        $result = $query->getQuery()->getResult();

        // Builder 
        $textBlocks = array();
        foreach ($result as $r) {
            if (!isset($textBlocks[$r['id']])) {
                $textBlocks[$r['id']] = array();
                $textBlocks[$r['id']]['name'] = $r['name'];
            }
            $textBlocks[$r['id']][$r['idLanguage']] = $r['body'];
        }

        // Re-order
        $exportRows = array();
        $exportRows[] = implode(",", $groupHeader);
        $exportRows[] = implode(",", $header);
        foreach ($textBlocks as $textBlock) {
            $textBlockRow = array();
            $textBlockRow[] = $this->csvEncode($textBlock['name']);
            foreach ($languages as $l) {
                $body = isset($textBlock[$l->getId()]) ? $textBlock[$l->getId()] : '';
                $textBlockRow[] = $this->csvEncode($body);
            }

            $exportRows[] = implode(",", $textBlockRow);
        }

        return $exportRows;
    }

    public function handleDownload($controller, &$data, $options = array()) {        
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        
        // Search groups
        $idGroup = $controller->get('request')->get('id', 0);
        $group = $doctrine->getRepository('AppDatabaseMainBundle:TextBlockGroup')->findOneById($idGroup);

        // Archive
        $exportRows = $this->archiveGroup($controller, $idGroup);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename='.$group->getName().'.csv');        
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".implode("\r\n", $exportRows));

        return $response;
    }

    public function handleDownloadAll($controller, &$data, $options = array()) {        
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        
        // Search groups
        $exportRowsAll = array();
        $groups = $doctrine->getRepository('AppDatabaseMainBundle:TextBlockGroup')->findAll();
        foreach ($groups as $group) {
            $exportRows = $this->archiveGroup($controller, $group->getId());
            $exportRowsAll = array_merge($exportRowsAll, $exportRows);
            $exportRowsAll[] = '"",""';
            $exportRowsAll[] = '"",""';
            $exportRowsAll[] = '"",""';
        }

        // Archive        

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=text_block_archive.csv');        
        //$response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        $response->setContent("\xEF\xBB\xBF".implode("\r\n", $exportRowsAll));

        return $response;
    }

    /////////////////////////////////////////////////
    // x. TEXT BLOCK DUPLICATION REMOVAL
    public function handleRemoveDuplicates($controller, &$data, $options = array()) {        
        $em = $controller->get('doctrine')->getEntityManager();

        $mapping = $controller->get('easy_mapping');
        $mgr = $controller->get('easy_module.manager');

        $data['reports'] = array(0 => 'All') + $mapping->getMapping('LeepAdmin_Report_List');
        $data['removeDuplicatesReportUrl'] = $mgr->getUrl('leep_admin', 'text_block', 'feature', 'removeDuplicatesReport');

        return $controller->render('LeepAdminBundle:TextBlock:remove_duplicates.html.twig', $data);
    }

    public function handleRemoveDuplicatesReport($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $request = $controller->get('request');
        $mapping = $controller->get('easy_mapping');

        $idReport = $request->get('idReport', 0);
        $idTextBlock = $request->get('idTextBlock', 0);
        if ($idTextBlock != 0) {
            // Update text block
            $textBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($idTextBlock);

            // Id of duplication
            $idArr = array();
            $results = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findByName($textBlock->getName());
            foreach ($results as $r) {
                $idArr[] = $r->getId();
            }

            // Compute new body language
            $languages = $mapping->getMapping('LeepAdmin_Language_List');
            $newBody = array();
            foreach ($languages as $idLanguage => $name) {
                $tb = $request->get('language_'.$idLanguage, '');
                if ($tb != '') {
                    $idTextBlockChoiced = intval($tb);

                    $tbLanguage = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findOneBy(array(
                        'idTextBlock' => $idTextBlockChoiced,
                        'idLanguage'  => $idLanguage
                    ));   
                    if ($tbLanguage) {
                        $newBody[] = array(
                            'idLanguage'  => $idLanguage,
                            'body' => $tbLanguage->getBody()
                        );
                    }
                }
            }

            // Delete
            $results = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findByName($textBlock->getName());
            foreach ($results as $r) {
                if ($r->getId() != $idTextBlock) {
                    $em->remove($r);
                }
            }
            $em->flush();

            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idTextBlock in (:idArr)')
                ->setParameter('idArr', $idArr)
                ->getQuery()->execute();

            // Insert
            foreach ($newBody as $row) {
                $tbl = new Entity\TextBlockLanguage();
                $tbl->setIdTextBlock($idTextBlock);
                $tbl->setIdLanguage($row['idLanguage']);
                $tbl->setBody($row['body']);
                $em->persist($tbl);
            }
            $em->flush();

        }

        // Search new duplicates
        $query = $em->createQueryBuilder();
        $query->select('p.name, COUNT(p) AS total')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->groupBy('p.name');
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            if ($r['total'] >= 2) {
                $duplicates[] = $r['name'];
            }
        }

        if (empty($duplicates)) {
            die("No duplication found");
        }

        $query = $em->createQueryBuilder();
        if ($idReport == 0) {
            $query->select('t.id, t.name')
                ->from('AppDatabaseMainBundle:TextBlock', 't')
                ->andWhere('t.name IN (:duplicates)')
                ->setParameter('duplicates', $duplicates)
                ->setMaxResults(1);
        }
        else {
            $query->select('t.id, t.name')
                ->from('AppDatabaseMainBundle:ReportTextBlock', 'p')
                ->innerJoin('AppDatabaseMainBundle:TextBlock', 't', 'WITH', 'p.idTextBlock = t.id')
                ->andWhere('p.idReport = :idReport')
                ->andWhere('t.name IN (:duplicates)')
                ->setParameter('idReport', $idReport)
                ->setParameter('duplicates', $duplicates)
                ->setMaxResults(1);
        }
        $result = $query->getQuery()->getResult();
        $idTextBlock = 0;
        foreach ($result as $r) {
            $idTextBlock = $r['id'];
            $textBlockName = $r['name'];
        }

        if ($idTextBlock != 0) {
            // Get duplicates
            $data['idTextBlock'] = $idTextBlock;
            $data['textBlockName'] = $textBlockName;

            $bodyData = array();

            $idArr = array();
            $results = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findByName($textBlockName);
            foreach ($results as $r) {
                $idArr[] = $r->getId();
            }

            $data['numTextBlocks'] = count($idArr);

            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idTextBlock in (:idArr)')
                ->setParameter('idArr', $idArr);
            $results = $query->getQuery()->getResult();
            foreach ($results as $r) {
                $idLanguage = $r->getIdlanguage();
                $idTextBlock = $r->getIdTextBlock();
                $body = $r->getBody();

                if (!isset($bodyData[$idLanguage])) {
                    $bodyData[$idLanguage] = array(
                        'idLanguage' => $idLanguage,
                        'name' => $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage),
                        'data' => array()
                    );
                }

                $bodyData[$idLanguage]['data'][] = array(
                    'idTextBlock' => $idTextBlock,
                    'body'        => $body
                );
            }

            $data['bodyData'] = $bodyData;
        }   

        return $controller->render('LeepAdminBundle:TextBlock:remove_duplicates_report.html.twig', $data);
    }



    public function handleCopyLanguage($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $form = $controller->get('leep_admin.text_block.business.copy_language_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Copy Text Block',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Copy',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'copyLanguage')
        );

        $data['backupTextBlockLanguage'] = $mgr->getUrl('leep_admin', 'text_block', 'feature', 'backupTextBlockLanguage');

        return $controller->render('LeepAdminBundle:TextBlock:copy_language.html.twig', $data);
    }

    public function handleBackupTextBlockLanguage($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('idLanguage', 0);

        $today = new \DateTime();
        $language = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($id);

        $query = $em->createQueryBuilder();
        $query->select('b.name, p.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->leftJoin('AppDatabaseMainBundle:TextBlock', 'b', 'WITH', 'p.idTextBlock = b.id')
            ->andWhere('p.idLanguage = :idLanguage')
            ->setParameter('idLanguage', $id);
        $result = $query->getQuery()->getResult();
        $text = '"Text Block"; "Text"'."\r\n";

        foreach ($result as $r) {
            $text .= $this->csvEncode($r['name']).';'.$this->csvEncode($r['body'])."\n";
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename='.$language->getName().'_'.$today->format('YmdHis').'.csv');        
        $response->setContent("\xEF\xBB\xBF".$text);

        return $response;
    }
}
