<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Easy\CrudBundle\Feature\Grid;

class FoodTableFoodItem extends Grid {    
    public function handleSynchronizeGroup($controller, &$data, $options = array()) {
        $form = $controller->get('leep_admin.food_table_food_item.business.synchronize_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Synchronize group',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Process',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'synchronizeGroup')
        );

        return $controller->render('LeepAdminBundle:FoodTableFoodItem:synchronize_group.html.twig', $data);
    }
}
