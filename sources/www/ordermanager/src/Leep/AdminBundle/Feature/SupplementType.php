<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SupplementType extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleList($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');

        $data['grids'] = array();

        // Grid for Supplement Type 
        $supplement_type_reader = $controller->get('leep_admin.supplement_type.business.grid_reader');
        $data['grids']['grid_supplement_type'] = array(
            'id' => 'GridId_Supplement_Type',
            'title' => Business\SupplementType\Constant::LB_SUPPLEMENT_TYPE_LIST,
            'header' => $supplement_type_reader->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'supplement_type', 'grid', 'ajaxSource')
        );

        // Grid for Pellet - Tested and In Use
        $sectionId = Business\Pellet\Constant::SECTION_TESTED_AND_IN_USE;
        $sectionTitle = Business\Pellet\Constant::getSections()[$sectionId];

        $data['grids']['grid_pellet_'.$sectionId] = array(
            'id' => 'GridId_Pellet_'.$sectionId,
            'title' => $sectionTitle,
            'header' => $controller->get('leep_admin.pellet.business.stock_in_use_grid_reader')->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'pellet', 'stockInUseGrid', 'ajaxSource')
        );

        // Grid for Material Used Up - Selected
        $material_used_up_in_use_reader = $controller->get('leep_admin.supplement_type.business.used_up_material_selected_grid_reader');
        $data['grids']['grid_material_used_up'] = array(
            'id' => 'GridId_Material_Used_Up_In_Use',
            'title' => Business\Material\Constant::LB_MATERIAL_USED_UP_IN_USE_LIST,
            'header' => $material_used_up_in_use_reader->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'supplement_type', 'used_up_material_selected_grid', 'ajaxSource', array("isSelected" => 1))
        );

        // Filter for Supplement Type
        $filter = $controller->get('leep_admin.supplement_type.business.filter_handler');
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'Main_Filter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => $mgr->getUrl('leep_admin', 'supplement_type', 'feature', 'list'),
            'visiblity' => 'hide'
        );

        if ($controller->getRequest()->getMethod() == 'POST') {
            $data['filter']['visiblity'] = '';
        }
    }

    public function handleViewChanges($controller, &$data, $options = array()) {          
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $id = $controller->get('request')->get('id', 0);

        $supplementTypes = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:SupplementTypeLog', 'p')
            ->andWhere('p.idSupplementType = :idSupplementType')
            ->setParameter('idSupplementType', $id)
            ->orderBy('p.timestamp', 'DESC');
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $supplementTypes[] = array(
                'timestamp' => $formatter->format($r->getTimestamp(), 'datetime'),
                'user'      => $formatter->format($r->getIdWebUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                'name'      => $r->getName(),
                'letter'    => $r->getLetter(),
                'lotInUse'  => $r->getLotInUse()
            );
        }
        $data['supplementTypes'] = $supplementTypes;

        return $controller->render('LeepAdminBundle:SupplementType:view_changes.html.twig', $data);
    }
}
