<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class FinanceTracker extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function setDefaultTabs(&$data) {
        $mgr = $this->container->get('easy_module.manager');
        $mapping = $this->container->get('easy_mapping');
        $data['months'] = $mapping->getMapping('LeepAdmin_Month_Short');

        $data['editCostUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'updateCost');
        $data['editCostTrackerDataUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'update_cost_tracker_data', 'edit');
        $data['submitCostTrackerDataUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'submit_cost_tracker', 'create');
        $data['viewCostTrackerHistoryUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'view_history_cost_tracker', 'list');
    }

    private function applyFilter(&$data, $filter, $tab) {
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'MainFilter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => $this->getModule()->getUrl('finance_tracker', $tab),
            'visiblity' => ''
        );
    }

    private function getCostTrackerSectors() {
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
    }

    private function getCustomEntityManager() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTH', 'Leep\AdminBundle\Doctrine\Extension\Month');
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Doctrine\Extension\Year');

        return $em;
    }

    private function prepareChartDataOrder($total, $year) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));
        $maxMonth = 12;
        if ($curYear == $year) {
            $maxMonth = intval($today->format('m'));
        }

        $arr = array();
        foreach ($total as $i => $d) {
            if ($i > $maxMonth) continue;
            $arr[] = array($i, $d['order']);
        }
        return json_encode($arr);
    }

    private function prepareChartDataAmount($total, $year, $dataIsArray = true, $endMonth = null) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));
        $maxMonth = 12;
        if ($curYear == $year) {
            $maxMonth = intval($today->format('m'));
        }

        if ($endMonth) {
            $maxMonth = $endMonth;
        }

        $arr = array();
        foreach ($total as $i => $d) {
            if ($i > $maxMonth) continue;

            // if $d is an array
            if ($dataIsArray){
                $arr[] = array($i, $d['amount']);
            }else {
                // use when $d is not an array
                $arr[] = array($i, $d);
            }
        }
        return json_encode($arr);
    }


    private function prepareChartDataAmountPrognosis($total, $year, $dataIsArray = true, $startMonth = null) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));

        $arr = array();
        foreach ($total as $i => $d) {
            if ($startMonth && $i < $startMonth) {
                continue;
            }

            if ($dataIsArray){
                $arr[] = array($i, $total[$i]['amount']);
            }else {
                // use when $d is not an array
                $arr[] = array($i, $total[$i]);
            }
        }

        return json_encode($arr);
    }

    private function hideUnspecified(&$arr) {
        if (isset($arr[0])) {
            foreach ($arr[0]['data'] as $r) {
                if (!empty($r['amount']) || !empty($r['order'])) {
                    return;
                }
            }

            unset($arr[0]);
        }
    }

    private function prepareAnalyseArray(&$arr) {
        // Define values
        foreach ($arr as $k => $name) {
            $arr[$k] = array(
                'name'   => $name,
                'data'   => array()
            );
            for ($i = 1; $i <= 12; $i++) {
                $arr[$k]['data'][$i] = array('order' => 0, 'amount' => 0);
            }
        }
    }

    private function sortData(&$arr, $filterData) {
        $sortComparer = new Business\FinanceTracker\DataSortComparer();
        if (strpos($filterData->sortBy, 'order_') === 0) {
            $month = intval(substr($filterData->sortBy, 6));
            $sortComparer->month = $month;
            $sortComparer->compareBy = 'order';
            $sortComparer->sortDirection = $filterData->sortDirection;

            usort($arr, array($sortComparer, 'compare'));
        }
        else if (strpos($filterData->sortBy, 'amount_') === 0) {
            $month = intval(substr($filterData->sortBy, 7));
            $sortComparer->month = $month;
            $sortComparer->compareBy = 'amount';
            $sortComparer->sortDirection = $filterData->sortDirection;

            usort($arr, array($sortComparer, 'compare'));
        }
    }

    public function handleSummary($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'summary';
        $monthCurrent = ((new \Datetime())->format('m') <= 12 && (new \Datetime())->format('m') > 1) ? (new \Datetime())->format('m')-1: 1;

        $filter = $controller->get('leep_admin.finance_tracker.business.summary_filter_handler');
        $this->applyFilter($data, $filter, 'summary');

        $filterData = $filter->getCurrentFilter();
        if (intval((new \Datetime())->format('Y')) !== intval($filterData->year)){
            $monthCurrent=12;
        }
        $em = $this->getCustomEntityManager();
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');

        // Get list of summary
        $summary = array();
        $summary['amway'] = 'Amway';
        $summary['other'] = 'Other';
        $this->prepareAnalyseArray($summary);
        // Config
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('order' => 0, 'amount' => 0);
        }

        $grandTotalOrder = 0;
        $grandTotalAmount = 0;
        // Compute amway revenue
        $summary['amway']['grandTotal'] = array('order' => 0, 'amount' => 0);
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.registeredDate) as month, p.idGroup, COUNT(p) as totalOrder')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('YEAR(p.registeredDate) = :year')
            ->groupBy('month, p.idGroup')
            ->setParameter('year', intval($filterData->year));

        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $idGroup = intval($r['idGroup']);
            $pricePerOrder = 0;
            switch ($idGroup) {
                case Business\MwaSample\Constants::GROUP_DEFAULT:
                    $pricePerOrder = $config->getAmwayDefaultRevenue();
                    break;
                case Business\MwaSample\Constants::GROUP_RUSSIA:
                    $pricePerOrder = $config->getAmwayRussiaRevenue();
                    break;
            }
            $totalAmount = $r['totalOrder'] * floatval($pricePerOrder);

            $month = $r['month'];
            $summary['amway']['data'][$month]['order'] += intval($r['totalOrder']);
            $summary['amway']['data'][$month]['amount'] += $totalAmount;

            $total[$month]['order'] += intval($r['totalOrder']);
            $total[$month]['amount'] += $totalAmount;

            $summary['amway']['grandTotal']['order'] += intval($r['totalOrder']);
            $summary['amway']['grandTotal']['amount'] += $totalAmount;

            $grandTotalOrder  += intval($r['totalOrder']);
            $grandTotalAmount += $totalAmount;
        }

        // Compute other revenue
        $summary['other']['grandTotal'] = array('order' => 0, 'amount' => 0);
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, COUNT(p) as totalOrder, SUM(p.revenueEstimation) as totalAmount')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('YEAR(p.dateordered) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year));
        $query = $this->addExtraCustomerFilter($controller, $em, $query);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $summary['other']['data'][$month]['order'] += intval($r['totalOrder']);
            $summary['other']['data'][$month]['amount'] += floatval($r['totalAmount']);

            $total[$month]['order'] += intval($r['totalOrder']);
            $total[$month]['amount'] += floatval($r['totalAmount']);

            $summary['other']['grandTotal']['order'] += intval($r['totalOrder']);
            $summary['other']['grandTotal']['amount'] += floatval($r['totalAmount']);

            $grandTotalOrder  += intval($r['totalOrder']);
            $grandTotalAmount += floatval($r['totalAmount']);
        }

        // count month have data
        // $monthCurrent = 0;
        // foreach ($summary['other']['data'] as $key => $value) {
        //     if ($value['order'] != 0 || $value['amount'] != 0) $monthCurrent = $key;
        // }
        // Compute "Invoice created"
        $invoice = array();
        $wholeYearInvoice = 0;
        for ($i = 1; $i <= 12; $i++) {
            $invoice[$i] = array('amount' => 0);
        }
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.invoiceDate) as month, SUM(p.invoiceAmount * (100-p.taxValue*p.isWithTax)/100) as totalAmount')
            ->from('AppDatabaseMainBundle:IntegratedInvoice', 'p')
            ->andWhere('YEAR(p.invoiceDate) = :year')
            ->andWhere('(p.invoiceStatus != :invoiceCancelled) AND (p.invoiceStatus != :freeAnalysis)')
            ->groupBy('month')
            ->setParameter('year', intval($filterData->year))
            ->setParameter('invoiceCancelled', Business\Invoice\Constant::INVOICE_STATUS_CANCELLED)
            ->setParameter('freeAnalysis', Business\Invoice\Constant::INVOICE_STATUS_FREE_ANALYSIS);
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $month = $r['month'];
            $invoice[$month]['amount'] = floatval($r['totalAmount']);
            $wholeYearInvoice += $invoice[$month]['amount'];
        }
        // Load cost per month
        $cost = array();
        $wholeYearCost = 0;
        $cash = array();
        $wholeYearCash = 0;
        $profit = array();
        $wholeYearProfit = 0;
        $wholeYearProfitPercentage = 0;

        for ($i = 1; $i <= 12; $i++) {
            $cost[$i] = array('amount' => 0);
            $cash[$i] = array('amount' => 0);
            $profit[$i] = array('amount' => $total[$i]['amount']);
        }
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FinanceCostPerMonth', 'p')
            ->andWhere('p.year = :year')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = intval($r->getMonth());
            $cost[$month]['amount'] = floatval($r->getCost());
            $wholeYearCost += $cost[$month]['amount'];
            $cash[$month]['amount'] = floatval($r->getCash());
            $wholeYearCash += $cash[$month]['amount'];
            $profit[$month]['amount'] = floatval($total[$month]['amount']) - floatval($r->getCost());
            $profit[$month]['percentOfTotal'] = ((floatval($total[$month]['amount']) > 0 && $profit[$month]['amount'] > 0) ? $profit[$month]['amount']/floatval($total[$month]['amount']) : 0) * 100;
            $wholeYearProfit += $profit[$month]['amount'];
        }
        // calculate percentage of whole year profit over whole year amount
        $wholeYearProfitPercentage = (($grandTotalAmount > 0 && $wholeYearProfit > 0) ? ($wholeYearProfit/$grandTotalAmount) : 0) * 100;
        // load data for cost tracker - about Book Keeper
        $cashOut = $utils->summarizeLinkedCashOut(intval($filterData->year));

        $totalCostTrackerAmount = array(
            'data' => array(),
            'wholeYear' => 0,
            'chartData' => null,
            'chartColor' => $utils->generateRandomColor()
        );
        for ($i = 1; $i <= 12; $i++) {
            $totalCostTrackerAmount['data'][$i] = 0;
        }
        $query = $em->createQueryBuilder();
        $query->select('c.cost, c.costTrackerSectorId, c.month')
            ->from('AppDatabaseMainBundle:CostTrackerSectorPerMonth', 'c')
            ->innerJoin('AppDatabaseMainBundle:CostTrackerSector', 'cts', 'WITH', 'c.costTrackerSectorId = cts.id AND cts.isForBookKeeper = 1')
            ->andWhere('c.year = :year')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = intval($r['month']);
            $costTrackerSectorId = intval($r['costTrackerSectorId']);
            if (!isset($totalCostTrackerAmount['data'][$month])) {
                $totalCostTrackerAmount['data'][$month] = 0;
            }

            $costTrackerCost = 0;
            if (isset($cashOut[$costTrackerSectorId]) &&
                isset($cashOut[$costTrackerSectorId][$month])) {
                $costTrackerCost = $cashOut[$costTrackerSectorId][$month];
            }
            if ($r['cost'] != '') {
                $costTrackerCost = $r['cost'];
            }
            $costTrackerCost = floatval($costTrackerCost);

            $totalCostTrackerAmount['data'][$month] += $costTrackerCost;
            $totalCostTrackerAmount['wholeYear'] += $costTrackerCost;
        }

        $totalCostTrackerAmount['chartData'] = $this->prepareChartDataAmount($totalCostTrackerAmount['data'], $filterData->year, false);
        //// prediction
        $prediction['amway']= [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['other'] = [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['total'] = [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['invoice'] = 0;
        $prediction['cost']= 0;
        $prediction['profit']= [
            'amount'            => 0,
            'percentOfTotal'    => 0
        ];
        $prediction['bookKeeper'] = 0;
        $countMonthCost= 1;
        $countMonthBookKeeper= 1;
        $countMonthProfitAmount= 1;
        $countMonthProfitPercentOfTotal= 1;
        $countMonthInvoice = 1;
        $countMonthTotalOrder = 1;
        $countMonthTotalAmount= 1;
        $countMonthTotalAmwayOrder= 1;
        $countMonthTotalAmwayAmount= 1;
        $countMonthTotalOtherOrder = 1;
        $countMonthTotalOtherAmount= 1;
        $totalAmountAllMonth = 0;
        foreach ($summary['amway']['data'] as $key => $value) {
            if (floatval($value['order']) != 0 && $key <= $monthCurrent) {
                $prediction['amway']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalAmwayOrder++;
            }
            if (floatval($value['amount']) != 0 && $key <= $monthCurrent) {
                $prediction['amway']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalAmwayAmount++;
            }
        }
        foreach ($summary['other']['data'] as $key => $value) {
            if (floatval($value['order']) != 0 && $key <= $monthCurrent) {
                $prediction['other']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalOtherOrder++;
            }
            if (floatval($value['amount']) != 0 && $key <= $monthCurrent) {
                $prediction['other']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalOtherAmount++;
            }
        }
        foreach ($invoice as $key => $value) {
            if (floatval($value['amount']) != 0 && $key <= $monthCurrent) {
                $prediction['invoice']+= floatval($value['amount']);
                if ($key!= 1) $countMonthInvoice++;
            }
        }
        foreach ($cost as $key => $value) {
            if (floatval($value['amount']) != 0 && $key <= $monthCurrent) {
                $prediction['cost']+= floatval($value['amount']);
                if ($key!= 1) $countMonthCost++;
            }
        }
        foreach ($profit as $key => $value) {
            if (floatval($value['amount']) != 0 && $key <= $monthCurrent) {
                $prediction['profit']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthProfitAmount++;
            }
        }
        foreach ($total as $key => $value) {
            if (floatval($value['order']) != 0  && $key <= $monthCurrent) {
                $prediction['total']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalOrder++;
            }
            if (floatval($value['amount']) != 0  && $key <= $monthCurrent) {
                $prediction['total']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalAmount++;
            }
        }
        foreach ($totalCostTrackerAmount['data'] as $key => $value) {
            if (floatval($value) != 0  && $key <= $monthCurrent) {
                $prediction['bookKeeper']+= floatval($value);
                if ($key!= 1) $countMonthBookKeeper++;
            }
        }
        $totalAmountAllMonth = $prediction['total']['amount']/ $countMonthTotalAmount;
        $profitPerMonth = $prediction['profit']['amount']/ $countMonthProfitAmount;
        $perCent = 100*$profitPerMonth/$totalAmountAllMonth;

        $prediction['amway']['order']= 12*( $prediction['amway']['order']/$countMonthTotalAmwayOrder);
        $prediction['amway']['amount']= 12*( $prediction['amway']['amount']/$countMonthTotalAmwayAmount);
        $prediction['other']['order'] = 12* ($prediction['other']['order']/ $countMonthTotalOtherOrder);
        $prediction['other']['amount'] = 12* ($prediction['other']['amount']/ $countMonthTotalOtherAmount);
        $prediction['invoice']= 12* ($prediction['invoice']/ $countMonthInvoice);
        $prediction['cost']= 12*($prediction['cost']/ $countMonthCost);

        // prediction profit
        $prediction['profit']['amount']= $profitPerMonth*12;
        $prediction['profit']['percentOfTotal']= $perCent;
        $prediction['bookKeeper']= 12* ($prediction['bookKeeper']/ $countMonthBookKeeper);
        //// last year
        $predictionLastYear = $this->getPredictionLastYear($controller, intval($filterData->year)-1);
        $summary['other']['prediction']['order'] = $prediction['other']['order'];
        $summary['other']['prediction']['amount'] = $prediction['other']['amount'];
        $summary['amway']['prediction']['order'] = $prediction['amway']['order'];
        $summary['amway']['prediction']['amount'] = $prediction['amway']['amount'];

        $summary['other']['predictionLastYear']['order'] = $predictionLastYear['other']['order'];
        $summary['other']['predictionLastYear']['amount'] = $predictionLastYear['other']['amount'];
        $summary['amway']['predictionLastYear']['order'] = $predictionLastYear['amway']['order'];
        $summary['amway']['predictionLastYear']['amount'] = $predictionLastYear['amway']['amount'];

        // Set data
        $data['costTrackerData'] = $totalCostTrackerAmount;
        $data['statistic'] = $summary;
        $data['total'] = $total;
        $data['cost'] = $cost;
        $data['wholeYearCost'] = $wholeYearCost;
        $data['prediction']= $prediction;
        $data['predictionLastYear']= $predictionLastYear;
        $data['cash'] = $cash;
        $data['wholeYearCash'] = $wholeYearCash;
        $data['profit'] = $profit;
        $data['wholeYearProfit'] = $wholeYearProfit;
        $data['wholeYearProfitPercentage'] = $wholeYearProfitPercentage;
        $data['invoice'] = $invoice;
        $data['wholeYearInvoice'] = $wholeYearInvoice;
        $data['grandTotalOrder'] = $grandTotalOrder;
        $data['grandTotalAmount'] = $grandTotalAmount;
        $data['totalOrderJson'] = $this->prepareChartDataOrder($total, $filterData->year);
        $data['totalAmountJson'] = $this->prepareChartDataAmount($total, $filterData->year);
        $data['totalCostJson'] = $this->prepareChartDataAmount($cost, $filterData->year);
        $data['totalCashJson'] = $this->prepareChartDataAmount($cash, $filterData->year);
        $data['totalInvoiceJson'] = $this->prepareChartDataAmount($invoice, $filterData->year);
    }

    public function getPredictionLastYear($controller, $year) {
        $em = $this->getCustomEntityManager();

        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');
        // Get list of summary
        $summary = array();
        $summary['amway'] = 'Amway';
        $summary['other'] = 'Other';
        $this->prepareAnalyseArray($summary);
        // Config
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('order' => 0, 'amount' => 0);
        }

        $grandTotalOrder = 0;
        $grandTotalAmount = 0;
        // Compute amway revenue
        $summary['amway']['grandTotal'] = array('order' => 0, 'amount' => 0);
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.registeredDate) as month, p.idGroup, COUNT(p) as totalOrder')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('YEAR(p.registeredDate) = :year')
            ->groupBy('month, p.idGroup')
            ->setParameter('year', intval($year));
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $idGroup = intval($r['idGroup']);
            $pricePerOrder = 0;
            switch ($idGroup) {
                case Business\MwaSample\Constants::GROUP_DEFAULT:
                    $pricePerOrder = $config->getAmwayDefaultRevenue();
                    break;
                case Business\MwaSample\Constants::GROUP_RUSSIA:
                    $pricePerOrder = $config->getAmwayRussiaRevenue();
                    break;
            }
            $totalAmount = $r['totalOrder'] * floatval($pricePerOrder);

            $month = $r['month'];
            $summary['amway']['data'][$month]['order'] += intval($r['totalOrder']);
            $summary['amway']['data'][$month]['amount'] += $totalAmount;

            $total[$month]['order'] += intval($r['totalOrder']);
            $total[$month]['amount'] += $totalAmount;

            $summary['amway']['grandTotal']['order'] += intval($r['totalOrder']);
            $summary['amway']['grandTotal']['amount'] += $totalAmount;

            $grandTotalOrder  += intval($r['totalOrder']);
            $grandTotalAmount += $totalAmount;
        }

        // Compute other revenue
        $summary['other']['grandTotal'] = array('order' => 0, 'amount' => 0);
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, COUNT(p) as totalOrder, SUM(p.revenueEstimation) as totalAmount')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('YEAR(p.dateordered) = :year')
            ->groupBy('month')
            ->setParameter('year', intval($year));
        $query = $this->addExtraCustomerFilter($controller, $em, $query);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = $r['month'];
            $summary['other']['data'][$month]['order'] += intval($r['totalOrder']);
            $summary['other']['data'][$month]['amount'] += floatval($r['totalAmount']);

            $total[$month]['order'] += intval($r['totalOrder']);
            $total[$month]['amount'] += floatval($r['totalAmount']);

            $summary['other']['grandTotal']['order'] += intval($r['totalOrder']);
            $summary['other']['grandTotal']['amount'] += floatval($r['totalAmount']);

            $grandTotalOrder  += intval($r['totalOrder']);
            $grandTotalAmount += floatval($r['totalAmount']);
        }

        // Compute "Invoice created"
        $invoice = array();
        $wholeYearInvoice = 0;
        for ($i = 1; $i <= 12; $i++) {
            $invoice[$i] = array('amount' => 0);
        }
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.invoiceDate) as month, SUM(p.invoiceAmount * (100-p.taxValue*p.isWithTax)/100) as totalAmount')
            ->from('AppDatabaseMainBundle:IntegratedInvoice', 'p')
            ->andWhere('YEAR(p.invoiceDate) = :year')
            ->andWhere('(p.invoiceStatus != :invoiceCancelled) AND (p.invoiceStatus != :freeAnalysis)')
            ->groupBy('month')
            ->setParameter('year', intval($year))
            ->setParameter('invoiceCancelled', Business\Invoice\Constant::INVOICE_STATUS_CANCELLED)
            ->setParameter('freeAnalysis', Business\Invoice\Constant::INVOICE_STATUS_FREE_ANALYSIS);
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $month = $r['month'];
            $invoice[$month]['amount'] = floatval($r['totalAmount']);
            $wholeYearInvoice += $invoice[$month]['amount'];
        }
        // Load cost per month
        $cost = array();
        $wholeYearCost = 0;
        $cash = array();
        $wholeYearCash = 0;
        $profit = array();
        $wholeYearProfit = 0;
        $wholeYearProfitPercentage = 0;

        for ($i = 1; $i <= 12; $i++) {
            $cost[$i] = array('amount' => 0);
            $cash[$i] = array('amount' => 0);
            $profit[$i] = array('amount' => $total[$i]['amount']);
        }
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FinanceCostPerMonth', 'p')
            ->andWhere('p.year = :year')
            ->setParameter('year', intval($year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = intval($r->getMonth());
            $cost[$month]['amount'] = floatval($r->getCost());
            $wholeYearCost += $cost[$month]['amount'];
            $cash[$month]['amount'] = floatval($r->getCash());
            $wholeYearCash += $cash[$month]['amount'];
            $profit[$month]['amount'] = floatval($total[$month]['amount']) - floatval($r->getCost());
            $profit[$month]['percentOfTotal'] = ((floatval($total[$month]['amount']) > 0 && $profit[$month]['amount'] > 0) ? $profit[$month]['amount']/floatval($total[$month]['amount']) : 0) * 100;
            $wholeYearProfit += $profit[$month]['amount'];
        }
        // calculate percentage of whole year profit over whole year amount
        $wholeYearProfitPercentage = (($grandTotalAmount > 0 && $wholeYearProfit > 0) ? ($wholeYearProfit/$grandTotalAmount) : 0) * 100;
        // load data for cost tracker - about Book Keeper
        $cashOut = $utils->summarizeLinkedCashOut(intval($year));

        $totalCostTrackerAmount = array(
            'data' => array(),
            'wholeYear' => 0,
            'chartData' => null,
            'chartColor' => $utils->generateRandomColor()
        );
        for ($i = 1; $i <= 12; $i++) {
            $totalCostTrackerAmount['data'][$i] = 0;
        }
        $query = $em->createQueryBuilder();
        $query->select('c.cost, c.costTrackerSectorId, c.month')
            ->from('AppDatabaseMainBundle:CostTrackerSectorPerMonth', 'c')
            ->innerJoin('AppDatabaseMainBundle:CostTrackerSector', 'cts', 'WITH', 'c.costTrackerSectorId = cts.id AND cts.isForBookKeeper = 1')
            ->andWhere('c.year = :year')
            ->setParameter('year', intval($year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $month = intval($r['month']);
            $costTrackerSectorId = intval($r['costTrackerSectorId']);
            if (!isset($totalCostTrackerAmount['data'][$month])) {
                $totalCostTrackerAmount['data'][$month] = 0;
            }

            $costTrackerCost = 0;
            if (isset($cashOut[$costTrackerSectorId]) &&
                isset($cashOut[$costTrackerSectorId][$month])) {
                $costTrackerCost = $cashOut[$costTrackerSectorId][$month];
            }
            if ($r['cost'] != '') {
                $costTrackerCost = $r['cost'];
            }
            $costTrackerCost = floatval($costTrackerCost);

            $totalCostTrackerAmount['data'][$month] += $costTrackerCost;
        }

        //// prediction
        $prediction['amway']= [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['other'] = [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['total'] = [
            'order'    => 0,
            'amount'   => 0
        ];
        $prediction['invoice'] = 0;
        $prediction['cost']= 0;
        $prediction['profit']= [
            'amount'            => 0,
            'percentOfTotal'    => 0
        ];
        $prediction['bookKeeper'] = 0;
        $countMonthCost= 1;
        $countMonthBookKeeper= 1;
        $countMonthProfitAmount= 1;
        $countMonthProfitPercentOfTotal= 1;
        $countMonthInvoice = 1;
        $countMonthTotalOrder = 1;
        $countMonthTotalAmount= 1;
        $countMonthTotalAmwayOrder= 1;
        $countMonthTotalAmwayAmount= 1;
        $countMonthTotalOtherOrder = 1;
        $countMonthTotalOtherAmount= 1;
        $totalAmountAllMonth = 0;
        foreach ($summary['amway']['data'] as $key => $value) {
            if (floatval($value['order']) != 0) {
                $prediction['amway']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalAmwayOrder++;
            }
            if (floatval($value['amount']) != 0) {
                $prediction['amway']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalAmwayAmount++;
            }
        }
        foreach ($summary['other']['data'] as $key => $value) {
            if (floatval($value['order']) != 0) {
                $prediction['other']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalOtherOrder++;
            }
            if (floatval($value['amount']) != 0) {
                $prediction['other']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalOtherAmount++;
            }
        }
        foreach ($invoice as $key => $value) {
            if (floatval($value['amount']) != 0) {
                $prediction['invoice']+= floatval($value['amount']);
                if ($key!= 1) $countMonthInvoice++;
            }
        }
        foreach ($cost as $key => $value) {
            if (floatval($value['amount']) != 0) {
                $prediction['cost']+= floatval($value['amount']);
                if ($key!= 1) $countMonthCost++;
            }
        }
        foreach ($profit as $key => $value) {
            if (floatval($value['amount']) != 0) {
                $prediction['profit']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthProfitAmount++;
            }
        }
        foreach ($total as $key => $value) {
            if (floatval($value['order']) != 0) {
                $prediction['total']['order']+= floatval($value['order']);
                if ($key!= 1) $countMonthTotalOrder++;
            }
            if (floatval($value['amount']) != 0) {
                $prediction['total']['amount']+= floatval($value['amount']);
                if ($key!= 1) $countMonthTotalAmount++;
            }
        }
        foreach ($totalCostTrackerAmount['data'] as $key => $value) {
            if (floatval($value) != 0) {
                $prediction['bookKeeper']+= floatval($value);
                if ($key!= 1) $countMonthBookKeeper++;
            }
        }
        $totalAmountAllMonth = $prediction['total']['amount']/ $countMonthTotalAmount;
        $profitPerMonth = $prediction['profit']['amount']/ $countMonthProfitAmount;
        $perCent = 100*$profitPerMonth/$totalAmountAllMonth;

        $prediction['amway']['order']= 12*( $prediction['amway']['order']/$countMonthTotalAmwayOrder);
        $prediction['amway']['amount']= 12*( $prediction['amway']['amount']/$countMonthTotalAmwayAmount);
        $prediction['other']['order'] = 12* ($prediction['other']['order']/ $countMonthTotalOtherOrder);
        $prediction['other']['amount'] = 12* ($prediction['other']['amount']/ $countMonthTotalOtherAmount);
        $prediction['invoice']= 12* ($prediction['invoice']/ $countMonthInvoice);
        $prediction['cost']= 12*($prediction['cost']/ $countMonthCost);
        // preditction profit
        $prediction['profit']['amount']= $profitPerMonth*12;
        $prediction['profit']['percentOfTotal']= $perCent;
        $prediction['bookKeeper']= 12* ($prediction['bookKeeper']/ $countMonthBookKeeper);
        return $prediction;
    }

    public function handleDistributionChannel($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'distributionChannel';

        $filter = $controller->get('leep_admin.finance_tracker.business.distribution_channel_filter_handler');
        $this->applyFilter($data, $filter, 'distributionChannel');

        $filterData = $filter->getCurrentFilter();
        $em = $this->getCustomEntityManager();

        // Get list of distribution channels
        $distributionChannel = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p');
        if (trim($filterData->name) != '') {
            $query->andWhere('p.distributionchannel LIKE :name')
                ->setParameter('name', '%'.trim($filterData->name).'%');
        }
        if ($filterData->idType != 0) {
            $query->andWhere('p.typeid = :idType')
                ->setParameter('idType', $filterData->idType);
        }
        if (!empty($filterData->distributionChannels)) {
            $query->andWhere('p.id IN (:distributionChannels)')
                ->setParameter('distributionChannels', $filterData->distributionChannels);
        }
        if ($filterData->sortBy == 'name') {
            $query->orderBy('p.distributionchannel', $filterData->sortDirection);
        }

        // Hide (unspecified) in filtered
        if ($filterData->idType == 0 && trim($filterData->name) == '' && empty($filterData->distributionChannels)) {
            $distributionChannel[0] = '(unspecified)';
        }

        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $distributionChannel[$r->getId()] = $r->getDistributionChannel();
        }

        // Define values
        $this->prepareAnalyseArray($distributionChannel);

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('order' => 0, 'amount' => 0);
        }
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, p.distributionchannelid, COUNT(p) as totalOrder, SUM(p.revenueEstimation) as totalAmount')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('YEAR(p.dateordered) = :year')
            ->groupBy('month, p.distributionchannelid')
            ->setParameter('year', intval($filterData->year));
        $query = $this->addExtraCustomerFilter($controller, $em, $query);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idDistributionChannel = $r['distributionchannelid'];
            if (!isset($distributionChannel[$idDistributionChannel])) {
                if (isset($distributionChannel[0])) {
                    $idDistributionChannel = 0;
                }
            }

            if (isset($distributionChannel[$idDistributionChannel])) {
                $month = $r['month'];
                $distributionChannel[$idDistributionChannel]['data'][$month]['order'] += intval($r['totalOrder']);
                $distributionChannel[$idDistributionChannel]['data'][$month]['amount'] += floatval($r['totalAmount']);

                $total[$month]['order'] += intval($r['totalOrder']);
                $total[$month]['amount'] += floatval($r['totalAmount']);
            }
        }

        $this->hideUnspecified($distributionChannel);

        $this->sortData($distributionChannel, $filterData);

        // Set data
        $data['statistic'] = $distributionChannel;
        $data['total'] = $total;
        $data['totalOrderJson'] = $this->prepareChartDataOrder($total, $filterData->year);
        $data['totalAmountJson'] = $this->prepareChartDataAmount($total, $filterData->year);
    }

    public function handlePartner($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'partner';

        $filter = $controller->get('leep_admin.finance_tracker.business.partner_filter_handler');
        $this->applyFilter($data, $filter, 'partner');

        $filterData = $filter->getCurrentFilter();
        $em = $this->getCustomEntityManager();

        // Get list of partners
        $partner = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Partner', 'p');
        if (trim($filterData->name) != '') {
            $query->andWhere('p.partner LIKE :name')
                ->setParameter('name', '%'.trim($filterData->name).'%');
        }
        if (trim($filterData->name) == '') {
            $partner[0] = '(unspecified)';
        }

        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $partner[$r->getId()] = $r->getPartner();
        }

        // Define values
        $this->prepareAnalyseArray($partner);

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('order' => 0, 'amount' => 0);
        }
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, dc.partnerid, COUNT(p) as totalOrder, SUM(p.revenueEstimation) as totalAmount')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.distributionchannelid')
            ->andWhere('YEAR(p.dateordered) = :year')
            ->groupBy('month, dc.partnerid')
            ->setParameter('year', intval($filterData->year));
        $query = $this->addExtraCustomerFilter($controller, $em, $query);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idPartner = intval($r['partnerid']);
            if (!isset($partner[$idPartner])) {
                if (isset($partner[0])) {
                    $idPartner = 0;
                }
            }

            if (isset($partner[$idPartner])) {
                $month = $r['month'];
                $partner[$idPartner]['data'][$month]['order'] += intval($r['totalOrder']);
                $partner[$idPartner]['data'][$month]['amount'] += floatval($r['totalAmount']);

                $total[$month]['order'] += intval($r['totalOrder']);
                $total[$month]['amount'] += floatval($r['totalAmount']);
            }
        }

        $this->hideUnspecified($partner);

        $this->sortData($partner, $filterData);

        // Set data
        $data['statistic'] = $partner;
        $data['total'] = $total;
        $data['totalOrderJson'] = $this->prepareChartDataOrder($total, $filterData->year);
        $data['totalAmountJson'] = $this->prepareChartDataAmount($total, $filterData->year);
    }

    public function handleAmway($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'amway';

        $filter = $controller->get('leep_admin.finance_tracker.business.amway_filter_handler');
        $this->applyFilter($data, $filter, 'amway');

        $filterData = $filter->getCurrentFilter();
        $em = $this->getCustomEntityManager();

        // Get list of amways
        $amway = array();
        $amway[0] = '(unspecified)';
        $amway[Business\MwaSample\Constants::GROUP_DEFAULT] = 'Amway default';
        $amway[Business\MwaSample\Constants::GROUP_RUSSIA] = 'Amway russia';

        // Define values
        $this->prepareAnalyseArray($amway);

        // Config
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('order' => 0, 'amount' => 0);
        }
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.registeredDate) as month, p.idGroup, COUNT(p) as totalOrder')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('YEAR(p.registeredDate) = :year')
            ->groupBy('month, p.idGroup')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idGroup = intval($r['idGroup']);
            if (!isset($amway[$idGroup])) {
                if (isset($amway[0])) {
                    $idGroup = 0;
                }
            }

            if (isset($amway[$idGroup])) {
                $pricePerOrder = 0;
                switch ($idGroup) {
                    case Business\MwaSample\Constants::GROUP_DEFAULT:
                        $pricePerOrder = $config->getAmwayDefaultRevenue();
                        break;
                    case Business\MwaSample\Constants::GROUP_RUSSIA:
                        $pricePerOrder = $config->getAmwayRussiaRevenue();
                        break;
                }
                $totalAmount = $r['totalOrder'] * floatval($pricePerOrder);

                $month = $r['month'];
                $amway[$idGroup]['data'][$month]['order'] += intval($r['totalOrder']);
                $amway[$idGroup]['data'][$month]['amount'] += $totalAmount;

                $total[$month]['order'] += intval($r['totalOrder']);
                $total[$month]['amount'] += $totalAmount;
            }
        }

        $this->hideUnspecified($amway);

        // Set data
        $data['statistic'] = $amway;
        $data['total'] = $total;
        $data['totalOrderJson'] = $this->prepareChartDataOrder($total, $filterData->year);
        $data['totalAmountJson'] = $this->prepareChartDataAmount($total, $filterData->year);

    }

    protected function addExtraCustomerFilter($controller, $em, $query) {
        $emConfig = $controller->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $orderCancelled = 11;
        $query->andWhere('FIND_IN_SET('.$orderCancelled.', p.statusList) = 0');

        return $query;
    }


    public function handleUpdateCost($controller, &$data, $options = array()) {
        $filter = $controller->get('leep_admin.finance_tracker.business.summary_filter_handler');
        $filterData = $filter->getCurrentFilter();

        $form = $controller->get('leep_admin.finance_tracker.business.edit_cost_handler');
        $form->year = $filterData->year;
        $form->execute();
        $data['form'] = array(
            'id'       => 'Form',
            'title'    => 'Edit cost per month',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Save',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl('finance_tracker', 'updateCost', array('year' => $controller->get('request')->get('year', 0))),
            'editId'   => $controller->get('request')->get('id', 0)
        );
    }

    // Cost Tracker
    public function handleCostTracker($controller, &$data, $options = array()) {
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'costTracker';

        $filter = $controller->get('leep_admin.finance_tracker.business.cost_tracker_filter_handler');
        $this->applyFilter($data, $filter, 'costTracker');

        $filterData = $filter->getCurrentFilter();
        $em = $this->getCustomEntityManager();

        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');

        // set default start month for prognosis graph, as 1
        $startMonth = 1;

        // Compute statistic
        $chartTypes = array('costTracker', 'cashManager', 'bookKeeper');
        $statistic = array();
        $total = array();
        $totalPrognosis = array();

        $totalByChartType = array();
        $totalPrognosisByChartType = array();

        $wholeYearTotalCost = 0;
        $wholeYearTotalCostAverage = 0;
        $wholeYearTotalPrognosisCost = 0;
        $default_cost_per_month = array();

        for ($i = 1; $i <= 12; $i++) {
            $default_cost_per_month[$i] = $total[$i] = $totalPrognosis[$i] = 0;
        }

        foreach ($chartTypes as $chartType) {
            for ($i = 1; $i <= 12; $i++) {
                if (!array_key_exists($chartType, $totalByChartType)) {
                    $totalByChartType[$chartType] = array();
                }

                if (!array_key_exists($chartType, $totalPrognosisByChartType)) {
                    $totalPrognosisByChartType[$chartType] = array();
                }

                $totalByChartType[$chartType][$i] = $totalPrognosisByChartType[$chartType][$i] = 0;
            }
        }

        $query = $em->createQueryBuilder();
        $query->select('c.cost, c.costPrognosis, c.month, s.id AS sectorId, s.sortOrder, s.txt,
                        s.isForCostTracker, s.isForCashManager, s.isForBookKeeper, s.isInTotal')
            ->from('AppDatabaseMainBundle:CostTrackerSector', 's')
            ->leftJoin('AppDatabaseMainBundle:CostTrackerSectorPerMonth', 'c', 'WITH', 's.id = c.costTrackerSectorId AND c.year = :year')
            ->addOrderBy('s.sortOrder', 'ASC')
            ->addOrderBy('c.month', 'ASC')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();

        $utils = $controller->get('leep_admin.finance_tracker.business.utils');
        $cashOut = $utils->summarizeLinkedCashOut(intval($filterData->year));

        foreach ($result as $r) {
            $month = intval($r['month']);

            $costTrackerSectorId = $r['sectorId'];

            $cost = '';
            if (isset($cashOut[$costTrackerSectorId][$month])) {
                $cost = $cashOut[$costTrackerSectorId][$month];
            }
            if ($r['cost'] != '') {
                $cost = $r['cost'];
            }

            $cost = floatval($cost);
            $costPrognosis = floatval($r['costPrognosis']);

            // check if this sector is not initialized yet, initialize it
            if (!array_key_exists($costTrackerSectorId, $statistic)) {
                $statistic[$costTrackerSectorId] = array(
                    'name' => $r['txt'],
                    'isForCostTracker' => $r['isForCostTracker'],
                    'isForCashManager' => $r['isForCashManager'],
                    'isForBookKeeper' => $r['isForBookKeeper'],
                    'isInTotal' => $r['isInTotal'],
                    'data' => $default_cost_per_month,
                    'dataPrognosis' => $default_cost_per_month,
                    'dataPrognosisStartMonth' => 1, // Jan
                    'wholeYear' => 0,
                    'wholeYearPrognosis' => 0,
                    'wholeYearAverage' => 0,
                    'wholeYearPrognosisAverage' => 0,
                    'chartData' => null,
                    'chartColor' => $utils->generateRandomColor()
                );
            }

            if ($month){
                $statistic[$costTrackerSectorId]['data'][$month] = $cost;
                $statistic[$costTrackerSectorId]['wholeYear'] += $cost;

                $statistic[$costTrackerSectorId]['dataPrognosis'][$month] = $costPrognosis;
                $statistic[$costTrackerSectorId]['wholeYearPrognosis'] += $costPrognosis;

                // calculate total Prognosis per month
                $totalPrognosis[$month] += $costPrognosis;

                // calculate total cost per month for each chart type
                if ($r['isInTotal']) {
                    // calculate total cost per month (for table result)
                    $total[$month] += $cost;

                    $types = array();

                    if ($r['isForCostTracker']) {
                        $types[] = 'costTracker';
                    }

                    if ($r['isForCashManager']) {
                        $types[] = 'cashManager';
                    }

                    if ($r['isForBookKeeper']) {
                        $types[] = 'bookKeeper';
                    }

                    foreach ($types as $type) {
                        $totalByChartType[$type][$month] += $cost;
                        $totalPrognosisByChartType[$type][$month] += $costPrognosis;
                    }

                    // calculate total cost for year (for result table)
                    $wholeYearTotalCost += $cost;
                }

                // calculate total prognosis cost for year
                $wholeYearTotalPrognosisCost += $costPrognosis;
            }

            // check the breakpoint where $cost is 0
            // set start point for prognosis graph
            if ($cost > 0) {
                $statistic[$costTrackerSectorId]['dataPrognosisStartMonth'] = $month;

                // re-define prognosis for this month
                $statistic[$costTrackerSectorId]['dataPrognosis'][$month] = $cost;
            }
        }

        // process total data
        $noMonths = 0;
        foreach ($totalPrognosis as $month => $montlyCost) {
            if ($total[$month] > 0) {
                $totalPrognosis[$month] = $total[$month];
                $noMonths++;
            }
        }

        // prepare data for chart
        foreach ($statistic as $sectorId => $sector) {
            $statistic[$sectorId]['chartData'] = $this->prepareChartDataAmount($statistic[$sectorId]['data'], $filterData->year, false, $statistic[$sectorId]['dataPrognosisStartMonth']);
            $statistic[$sectorId]['chartDataPrognosis'] = $this->prepareChartDataAmountPrognosis($statistic[$sectorId]['dataPrognosis'], $filterData->year, false, $statistic[$sectorId]['dataPrognosisStartMonth']);
            $statistic[$sectorId]['wholeYearAverage'] = round($statistic[$sectorId]['wholeYear'] / $noMonths, 2);
            $statistic[$sectorId]['wholeYearPrognosisAverage'] = $statistic[$sectorId]['wholeYearPrognosis'] / $noMonths;

            $wholeYearTotalCostAverage += $statistic[$sectorId]['wholeYearAverage'];
        }

        $chartTotalData = array();
        foreach ($chartTypes as $chartType) {
            $totalPrognosisStartMonth = 1;
            foreach ($totalPrognosisByChartType[$chartType] as $month => $montlyCost) {
                if ($totalByChartType[$chartType][$month] > 0) {
                    $totalPrognosisStartMonth = $month;
                    $totalPrognosisByChartType[$chartType][$month] = $totalByChartType[$chartType][$month];
                }
            }

            $totalEndMonth = $totalPrognosisStartMonth;

            if (!array_key_exists($chartType, $chartTotalData)) {
                $chartTotalData[$chartType] = array();
            }

            $chartTotalData[$chartType]['data'] = $this->prepareChartDataAmount($totalByChartType[$chartType], $filterData->year, false, $totalEndMonth);
            $chartTotalData[$chartType]['dataPrognosis'] = $this->prepareChartDataAmountPrognosis($totalPrognosisByChartType[$chartType], $filterData->year, false, $totalPrognosisStartMonth);
        }

        $data['totalChartData']['chartData'] = $chartTotalData;

        // Set data
        $data['statistic'] = $statistic;
        $data['total'] = $total;
        $data['totalPrognosis'] = $totalPrognosis;
        $data['totalChartData']['color'] = $utils->generateRandomColor();
        $data['wholeYearTotalCost'] = $wholeYearTotalCost;
        $data['wholeYearTotalPrognosisCost'] = $wholeYearTotalPrognosisCost;
        $data['wholeYearAverageCost'] = $wholeYearTotalCost / $noMonths;
    }

    public function handleDownloadHistory($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id', 0);

        $resultHistory = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSubmitDataHistory')->findOneById($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=geneDataImport_'.$resultHistory->getTimestamp()->format("YmdHis").'.txt');
        $response->setContent("\xEF\xBB\xBF".$resultHistory->getData());

        return $response;
    }
}
