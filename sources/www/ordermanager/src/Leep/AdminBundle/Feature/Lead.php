<?php

    namespace Leep\AdminBundle\Feature;

    use Easy\ModuleBundle\Module\AbstractFeature;
    use Symfony\Component\HttpFoundation\Response;
    use Leep\AdminBundle\Business;
    use App\Database\MainBundle\Entity;
    use Leep\AdminBundle\Helper\BinaryFileResponse;
    use Symfony\Component\HttpFoundation\ResponseHeaderBag;
    use Leep\AdminBundle\Business\Rating\Utils as RatingUtils;

    class Lead extends AbstractFeature
    {
        public function getActions() { return array(); }
        public function getDefaultOptions() { return array(); }

        public $container;
        public function __construct($container) {
            $this->container = $container;
        }

        public function handleExportLead($controller, &$data, $options = array())
        {
            $em = $controller->get('doctrine')->getEntityManager();
            $formatter = $this->container->get('leep_admin.helper.formatter');
            $leadFilterBuilder = $controller->get('leep_admin.lead.business.filter_handler');

            $filters = $controller->get("leep_admin.lead.business.filter_handler")->getCurrentFilter();


            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->select('p.name, p.idType, p.idPartner, p.idDistributionChannel, p.idAcquisiteur, p.street,
                    p.postcode, p.city, p.idCountry, p.contactEmail, p.telephone, p.source')
                ->from('AppDatabaseMainBundle:Lead', 'p');
            $emConfig = $controller->get('doctrine')->getEntityManager()->getConfiguration();
            $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

            if (trim($filters->name) != '') {
                $queryBuilder->andWhere('p.name LIKE :name')
                    ->setParameter('name', '%'.trim($filters->name).'%');
            }
            if (!empty($filters->idType)) {
                $queryBuilder->andWhere('p.idType = :idType')
                    ->setParameter('idType', $filters->idType);
            }
            if (!empty($filters->currentStatus)) {
                $queryBuilder->andWhere('p.currentStatus = :currentStatus')
                    ->setParameter('currentStatus', $filters->currentStatus);
            }
            if (!empty($filters->status)) {
                $queryBuilder->andWhere('FIND_IN_SET('.$filters->status.', p.statusList) > 0');
            }
            if (!empty($filters->idPartner)) {
                $queryBuilder->andWhere('p.idPartner = :idPartner')
                    ->setParameter('idPartner', $filters->idPartner);
            }
            if (!empty($filters->idAcquisiteur)) {
                $queryBuilder->andWhere('p.idAcquisiteur = :idAcquisiteur')
                    ->setParameter('idAcquisiteur', $filters->idAcquisiteur);
            }
            if (!empty($filters->idDistributionChannel)) {
                $queryBuilder->andWhere('p.idDistributionChannel = :idDistributionChannel')
                    ->setParameter('idDistributionChannel', $filters->idDistributionChannel);
            }
            $leads = $queryBuilder->getQuery()->getResult();

            // create header
            $content = '';
            $content .= '"Lead Name"; "Type"; "Partner"; "Distribution Channel"; "Acquisiteur"; "Address"; "Email"; "Telephone"; "Source"'."\r\n";


            // write data with valid format in rows array
            foreach ($leads as $lead) {
                $rows = [];
                $rows[] = '"'.addslashes($lead['name']).'"';
                $rows[] = '"'.addslashes($formatter->format($lead['idType'], 'mapping', 'LeepAdmin_DistributionChannel_Type')).'"';
                $rows[] = '"'.addslashes($formatter->format($lead['idPartner'], 'mapping', 'LeepAdmin_Partner_List')).'"';
                $rows[] = '"'.addslashes($formatter->format($lead['idDistributionChannel'], 'mapping', 'LeepAdmin_DistributionChannel_List')).'"';
                $rows[] = '"'.addslashes($formatter->format($lead['idAcquisiteur'], 'mapping', 'LeepAdmin_Acquisiteur_List')).'"';
                $address = sprintf("%s, %s %s, %s",
                    $lead['street'],
                    $lead['city'],
                    $lead['postcode'],
                    $formatter->format($lead['idCountry'], 'mapping', 'LeepAdmin_Country_List')
                );
                $rows[] = '"'.addslashes($address).'"';
                $rows[] = '"'.addslashes($lead['contactEmail']).'"';
                $rows[] = '"'.addslashes($lead['telephone']).'"';
                $rows[] = '"'.addslashes($lead['source']).'"';
                $content .= implode(";", $rows)."\r\n";
            }

            // Create Response
            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream: charset = UTF-8');
            $response->headers->set('Content-Disposition', 'inline; filename = lead.csv');
            $response->setContent("\xEF\xBB\xBF".$content);

            return $response;
        }
    }

 ?>
