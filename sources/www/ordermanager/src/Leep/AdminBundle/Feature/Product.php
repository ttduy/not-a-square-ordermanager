<?php
namespace Leep\AdminBundle\Feature;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Product extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }
    public function handleExportProductGenes($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $content = [];
        $urlFile = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/product/Product_Genes.xlsx';
        $myfile = fopen($urlFile, "a+");
        $products = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findALl();
        $content [] = "PRODUCT NAME,PRODUCT GENES\n";


        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator('ORDERMANAGER')
                   ->setLastModifiedBy('ORDERMANAGER')
                   ->setTitle("Product Genes")
                   ->setSubject("Product Genes");
        ///// task closed
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle("Product Genes");

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(150);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'PRODUCT NAME');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'PRODUCT GENES');
        $rowId = 2;

        foreach ($products as $key => $product) {
            if ($product) {
                $productGenes = $em->getRepository('AppDatabaseMainBundle:ProdGeneLink')->findBy(['productid' => $product->getId()]);
                $arrayGene = [];
                if ($productGenes) {
                    foreach ($productGenes as $key => $value) {
                        $gene = $em->getRepository('AppDatabaseMainBundle:Genes')->findOneById($value->getGeneId());
                        if ($gene) {
                            $arrayGene[] = $gene->getGeneName();
                        }
                    }
                }
                $strGene = implode(' , ', $arrayGene);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $rowId, $product->getName());
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $rowId, $strGene);
                $rowId++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setRGB('3B73BC');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setRGB('3B73BC');

        // Export Excel
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $filename = (new \Datetime())->format('Y-mm-dd_h:i:s').'-Product_Name_Genes.xlsx';
        $objWriter->save($urlFile);

        $fileResponse = new BinaryFileResponse($urlFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='. $filename);
        return $fileResponse;
    }
    public function handleCorrectProductGenes($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Prod gene link preload
        $prodGeneLink = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:ProdGeneLink', 'app_main')->findAll();
        foreach ($result as $r) {
            if (!isset($prodGeneLink[$r->getProductId()])) {
                $prodGeneLink[$r->getProductId()] = array();
            }
            $prodGeneLink[$r->getProductId()][] = $r->getGeneId();
        }

        // Active genes
        $activeGenes = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:Genes')->findALl();
        foreach ($result as $r) {
            if ($r->getIsDeleted() == 0) {
                $activeGenes[$r->getId()] = 1;
            }
        }

        $idProduct = $controller->getRequest()->get('idProduct', 0);

        set_time_limit(200);

        $query = $em->createQueryBuilder();
        $query->select('p.id, c.id as idOrder, c.idCustomerInfo')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->leftJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'c.id = p.customerid')
            ->andWhere('p.productid = :idProduct')
            ->setParameter('idProduct', $idProduct);

        $toBeAnalyse = $query->getQuery()->getResult();

        foreach ($toBeAnalyse as $toBeAnalyseRecord) {
            $idCustomer = $toBeAnalyseRecord['idCustomerInfo'];
            $idOrder = $toBeAnalyseRecord['idOrder'];

            // Load customer product ordered
            $orderedProducts = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct', 'app_main')->findBycustomerid($idOrder);
            $geneExpected = array();
            foreach ($orderedProducts as $op) {
                if (isset($prodGeneLink[$op->getProductId()])) {
                    foreach ($prodGeneLink[$op->getProductId()] as $idGene) {
                        $geneExpected[$idGene] = 1;
                    }
                }
            }
            // Load customer gene ordered
            $geneResults = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findByIdCustomer($idCustomer);
            foreach ($geneResults as $gr) {
                $idGene = $gr->getIdGene();
                if (isset($geneExpected[$idGene])) {
                    unset($geneExpected[$idGene]);
                }
                else {
                    $em->remove($gr);
                }
            }

            foreach ($geneExpected as $idGene => $tmp) {
                if (isset($activeGenes[$idGene])) {
                    $cig = new Entity\CustomerInfoGene();
                    $cig->setIdGene($idGene);
                    $cig->setIdCustomer($idCustomer);
                    $cig->setResult('');
                    $em->persist($cig);
                }
            }
        }
        $em->flush();

        return new Response("DONE");
    }
}
