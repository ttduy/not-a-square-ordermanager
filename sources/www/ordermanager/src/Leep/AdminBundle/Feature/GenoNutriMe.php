<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class GenoNutriMe extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleStartProduction($controller, &$data, $options = array()) {
        $options['currentStep'] = -1;

        // go to update production
        return $this->handleUpdateProduction($controller, $data, $options);
    }

    public function handleUpdateProduction($controller, &$data, $options = array()) {
        $codeParser = $controller->get('leep_admin.geno_nutri_me.business.code_parser');
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        // Get request value
        $input = trim(strtoupper($controller->get('request')->get('inputText', "")));
        $step = $controller->get('request')->get('step', -1);
        $id = $controller->get('request')->get('id', 0);

        if (isset($options['currentStep'])) {
            $step = $options['currentStep'];
        }

        // Get current nutrime production
        $nutriMe = $em->getRepository('AppDatabaseMainBundle:GenoNutriMe')->findOneById($id);

        // Parse code
        $d = $codeParser->parse($nutriMe->getBarCode());
        if ($d['error'] != '') {
            die("Error in barcode: ". $d['error']);
        }

        $d = $d['data'];

        // Update last step
        $data['error'] = "";
        if (isset($d['drugs'][$step])) {
            $current = $d['drugs'][$step];
            $type = trim(strtoupper($current['type']));
            $amount = floatval($current['amount']);
            $usedAmount = $amount;

            // Check type
            if ($input != $type) {
                $data['error'] = "Incorrect durg, please try again!";
                $step--;
            }

            // Check instock
            else {
                $pellet = $em->getRepository('AppDatabaseMainBundle:GenoPellet')->findOneByLetter($type);
                if ($pellet) {
                    $inStock = Business\GenoPellet\Utils::getPelletUsableStock($controller, $pellet->getId());
                    if ($inStock['total'] < $amount) {
                        $data['error'] = "Warning! You are using more than there are in stock. Currently has: ".$inStock['total']."g!";
                        // ATENTION! This is an warning, not error, so do not turn back to the previous step.
                    } else {
                        // Update pellet stock usage
                        while ($amount > 0) {
                            $firstStock = array_shift($inStock['stock']);
                            if (!$firstStock) {
                                break;
                            }

                            $amount = $this->useStock($controller, $firstStock, $amount, $nutriMe->getId());
                        }
                    }
                    $start_th = 1;

                    // query get the last process th
                    $flag = false;
                    $query = $em->createQueryBuilder();
                    $query->select('p, MAX(p.startTh) as lastStartTh')
                            ->from("AppDatabaseMainBundle:GenoNutriMeSnapshot", 'p')
                            ->where('p.nutriMeId = :id')
                            ->setParameter('id', $id);
                    $queryResult = $query->getQuery()->getResult();
                    $lastStartTh = $queryResult[0]['lastStartTh'];
                    if ($step == 0) {
                        // last process is not null: start th is the last th + 1
                        if (isset($lastStartTh)) {
                            $start_th = $lastStartTh + 1;
                        }
                    }
                    else{
                        $start_th = $lastStartTh;
                        $flag = true;
                    }

                    // let update Geno Nutrime snapshot
                    // create geno nutri me snapshot
                    $snapshot = new Entity\genoNutriMeSnapshot();
                    $snapshot->setNutriMeId($id);
                    $snapshot->setPelletName($pellet->getName());
                    $snapshot->setPelletType($type);
                    $snapshot->setPelletActiveIngredientName($pellet->getActiveIngredientName());
                    $snapshot->setStartTh($start_th);
                    $snapshot->setCreatedDate(new \DateTime());
                    $snapshot->setTypeAmount($usedAmount);
                    // get current user
                    $userManager = $controller->get('leep_admin.web_user.business.user_manager');
                    $snapshot->setWebUserId($userManager->getUser()->getId());
                    $entries = $em->getRepository("AppDatabaseMainBundle:GenoPelletEntry")->findBy(['idPellet' => $pellet->getId()]);
                    foreach ($entries as $entry) {
                        $currentShipment = $entry->getShipment();
                        if($currentShipment){
                            $snapshot->setPelletShipment($currentShipment);
                            break;
                        }
                    }

                    if ($flag == true) {
                        $currentPelletType = trim($type);
                        $tmpStock = $em->getRepository("AppDatabaseMainBundle:GenoNutriMeSnapshot")->findBy([
                            'startTh'       =>  $start_th,
                            'pelletType'    => $currentPelletType,
                            'nutriMeId'     => $id
                        ]);

                        if (!($tmpStock)) {
                            $em->persist($snapshot);
                            $em->flush();
                        }
                    }
                    else{
                        $em->persist($snapshot);
                        $em->flush();
                    }

                    // Load current lots
                    $currentLots = $this->captureCurrentLots($controller);
                    $nutriMe->setCurrentLots(serialize($currentLots));

                    $em->persist($nutriMe);
                    $em->flush();
                } else {
                    $data['error'] = "Pellet letter {$type} not existed!";
                    $step--;
                }
            }
        }

        // Basic info
        $data['id'] = $id;
        $data['step'] = ++$step;
        $data['orderNumber'] = $d['orderNumber'];
        $data['supplementDaysOrder'] = $d['supplementDaysOrder'];
        $data['product'] = $d['product'];
        $data['totalAmount'] = number_format($d['total'], 2, ".", "")."g";
        $data['totalPack'] = $d['numPack'];

        // Get done step
        $data['doneSteps'] = [];
        for ($index = 0; $index < $step; $index++) {
            if (isset($d['drugs'][$index])) {
                $data['doneSteps'][] = $d['drugs'][$index];
            }
        }
        $data['doneSteps'] = array_reverse($data['doneSteps']);

        // Current step data
        $data['isLastScan'] = false;
        if (isset($d['drugs'][$step])) {
            $data['isFinalStep'] = false;
            $current = $d['drugs'][$step];

            $pellet = $em->getRepository('AppDatabaseMainBundle:GenoPellet')->findOneByLetter($current['type']);
            if (!$pellet) {
                die("Error: Pellet type ".$current['type']." not existed!");
            }

            $inStock = Business\GenoPellet\Utils::getPelletUsableStock($controller, $pellet->getId());
            $amountInStock = $inStock['total'];

            $current = $d['drugs'][$step];
            $data['msg1'] = 'Scan';
            $data['msg2'] = $current['type']."($amountInStock"."g)";
            $data['msg3'] = 'And use';
            $data['msg4'] = $current['amount'];
            $data['drugsJson'] = json_encode($d['drugs']);

            $data['actionUrl'] =  $mgr->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'updateProduction', array('id' => $id));

            if ($step == (count($d['drugs']) - 2)) {
                $data['isLastScan'] = true;
            }
        }

        // Final step
        else {
            $data['isFinalStep'] = true;
            $data['msg1'] = 'Completed! Total units should be';
            $data['msg2'] = $d['total']."g";

            $data['actionUrl'] =  $mgr->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'doneProduction', array('id' => $id));
        }

        return $controller->render('LeepAdminBundle:GenoNutriMe:production.html.twig', $data);
    }

    private function useStock($controller, $stock, $amount, $helper) {
        $em = $controller->get('doctrine')->getEntityManager();
        $currentUser = $controller->get('leep_admin.web_user.business.user_manager')->getUser()->getUsername();

        $entry = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findOneById($stock['id']);
        if ($entry) {
            $usedAmount = $amount;
            if ($usedAmount > $entry->getLoading()) {
                $usedAmount = $entry->getLoading();
            }

            // create new pellet entity with section of "USED UP"
            $newPelletEntry = new Entity\GenoPelletEntry();
            $newPelletEntry->setIdPellet($entry->getIdPellet());
            $newPelletEntry->setLoading($usedAmount);
            $newPelletEntry->setSection(Business\Pellet\Constant::SECTION_USED_UP);
            $newPelletEntry->setNote($entry->getNote());

            $newPelletEntry->setShipment($entry->getShipment());
            $newPelletEntry->setOrdered($entry->getOrdered());
            $newPelletEntry->setOrderedDate($entry->getOrderedDate());
            $newPelletEntry->setOrderedBy($entry->getOrderedBy());
            $newPelletEntry->setReceived($entry->getReceived());
            $newPelletEntry->setReceivedDate($entry->getReceivedDate());
            $newPelletEntry->setReceivedBy($entry->getReceivedBy());

            $newPelletEntry->setExpiryDate($entry->getExpiryDate());
            $newPelletEntry->setExpiryDateWarningDisabled($entry->getExpiryDateWarningDisabled());
            $newPelletEntry->setIsSelected($entry->getIsSelected());

            $newPelletEntry->setDateTaken(new \DateTime());
            $newPelletEntry->setUsedFor("NutriMe Production #".$helper);
            $newPelletEntry->setTakenBy($currentUser);
            $newPelletEntry->setIdSuperEntry($entry->getId());

            $em->persist($newPelletEntry);
            $em->flush();

            $sectionTo = Business\GenoPellet\Constant::getSections()[Business\Pellet\Constant::SECTION_USED_UP];
            Business\GenoPelletEntry\Utils::updateEntryHistory($controller, $newPelletEntry, "Status (Section) [In Geno NutriMe production]", "", $sectionTo);
            Business\GenoPelletEntry\Utils::updateEntryHistory($controller, $newPelletEntry, "Loading (g) [In Geno NutriMe production]", "", $usedAmount);

            // copy W/W
            $weights = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryWeight')->findByIdPelletEntry($entry->getId());
            if ($weights) {
                foreach ($weights as $eachWeight) {
                    $newWeight = new Entity\GenoPelletEntryWeight();
                    $newWeight->setIdPelletEntry($newPelletEntry->getId());
                    $newWeight->setTxt($eachWeight->getTxt());
                    $newWeight->setWeight($eachWeight->getWeight());
                    $newWeight->setSortOrder($eachWeight->getSortOrder());
                    $newWeight->setTargetWeight($eachWeight->getTargetWeight());
                    $em->persist($newWeight);
                }
                $em->flush();
            }

            // copy W/W
            $weights = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryAdditives')->findByIdPelletEntry($entry->getId());
            if ($weights) {
                foreach ($weights as $eachWeight) {
                    $newWeight = new Entity\GenoPelletEntryAdditives();
                    $newWeight->setIdPelletEntry($newPelletEntry->getId());
                    $newWeight->setName($eachWeight->getName());
                    $newWeight->setWeight($eachWeight->getWeight());
                    $em->persist($newWeight);
                }
                $em->flush();
            }

            // Update entry
            $loadingFrom = $entry->getLoading();
            $loadingTo = $entry->getLoading() - $usedAmount;
            if ($loadingTo < 0) {
                $loadingTo = 0;
            }

            Business\GenoPelletEntry\Utils::updateEntryHistory($controller, $entry, "Loading (g) [In Geno NutriMe production]", $loadingFrom, $loadingTo);
            $entry->setLoading($loadingTo);

            $em->persist($entry);
            $amount = $amount - $usedAmount;
            if ($amount < 0) {
                $amount = 0;
            }

            $em->flush();
        }

        return $amount;
    }

    public function handleDoneProduction($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $request = $controller->get('request');

        $id = $request->get('id', 0);
        $actualUnit = $request->get('inputText', 0);

        $nutriMe = $em->getRepository('AppDatabaseMainBundle:GenoNutriMe')->findOneById($id);
        $nutriMe->setActualUnit(floatval($actualUnit));
        $nutriMe->setProcessedOn(new \DateTime());
        $nutriMe->setProcessedBy($userManager->getUser()->getId());

        // Load current lots
        $currentLots = $this->captureCurrentLots($controller);
        $nutriMe->setCurrentLots(serialize($currentLots));

        $em->persist($nutriMe);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'geno_nutri_me', 'edit', 'edit', array('id' => $id));
        return new RedirectResponse($url);
    }


    private function captureCurrentLots($container){
        $em = $container->get('doctrine')->getEntityManager();
        $currentLots = array();

        // get supplement type
        $currentLots['supplementType'] = array();
        $result = $em->getRepository('AppDatabaseMainBundle:SupplementType')->findAll();
        foreach ($result as $r) {
            $currentLots['supplementType'][] = array(
                'name'     => $r->getName(),
                'letter'   => $r->getLetter(),
                'lotInUse' => $r->getLotInUse()
            );
        }

        // return current lot data
        return $currentLots;
    }

    public function handleBulkEditStatus($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        $form = $controller->get('leep_admin.geno_nutri_me.business.bulk_edit_status_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit status',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Update',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $mgr->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'bulkEditStatus')
        );

        return $controller->render('LeepAdminBundle:GenoNutriMe:bulk_edit_status.html.twig', $data);
    }


    public function handleBulkEditStatusValidate($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('orderNumbers', '');
        $tmp = explode("\n", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $foundOrders = array();

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:GenoNutriMe', 'd')
            ->andWhere('d.orderNumber in (:arr)')
            ->setParameter('arr', $arr)
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $foundOrders[$r->getOrderNumber()] = $r;
        }

        $data = array();
        $today = new \DateTime();
        $todayTs = $today->getTimestamp();
        foreach ($arr as $orderNumber) {
            if ($orderNumber === 0) continue;
            if (!isset($foundOrders[$orderNumber])) {
                $data[] = array(
                    'id'          => 0,
                    'orderNumber' => $orderNumber,
                    'customerName' => '',
                    'color'       => '#FFCCCC',
                    'status'      => 'Order not found'
                );
            }
            else {
                $nutriMe = $foundOrders[$orderNumber];

                $status = '';
                if ($nutriMe->getCurrentStatusDate()) {
                    $age = intval(($todayTs - $nutriMe->getCurrentStatusDate()->getTimestamp()) / 86400);
                    $status = (($age === '') ? '': "(${age}d) ").$formatter->format($nutriMe->getCurrentStatus(), 'mapping', 'LeepAdmin_GenoNutriMe_Status');
                }

                $data[] = array(
                    'id'          => $nutriMe->getId(),
                    'orderNumber' => $orderNumber,
                    'customerName' => $nutriMe->getCustomerName(),
                    'color'       => '',
                    'status'      => $status
                );
            }
        }

        return $controller->render('LeepAdminBundle:GenoNutriMe:bulk_edit_status_validate.html.twig', array('data' => $data));
    }
}
