<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class Report extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleViewHoldReportPdf($controller, &$data, $options = array()) {
        set_time_limit(200);
        $idReport = $controller->getRequest()->get('id', 0);
        $idLanguage = $controller->getRequest()->get('idLanguage', 1);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $isDownload = $controller->getRequest()->get('isDownload', 0);
        $reportData = $controller->getRequest()->get('reportData', '');

        $reportData = str_replace('||', "=", $reportData);
        $reportData = str_replace('@@', "\n", $reportData);

        $builder = $controller->get('leep_admin.report.business.builder');
        $builder->init();
        $builder->applyLanguage($idLanguage);
        $builder->parseConfigData($reportData);
        $builder->begin();

        // Load report items
        $query = $em->createQueryBuilder();
        $sections = $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $idReport)
            ->orderBy('p.sortOrder', 'ASC')
            ->getQuery()->getResult();

        foreach ($sections as $section) {
            $query = $em->createQueryBuilder();
            $items = $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $section->getId())
                ->orderBy('p.sortOrder', 'ASC')
                ->getQuery()->getResult();

            foreach ($items as $row) {
                $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
            }

            $builder->moveNextPage();
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=report_'.$idReport.'.pdf');
        if ($isDownload) {
            $response->headers->set('Content-Disposition', 'attachment; filename=report_'.$idReport.'.pdf');
        }
        $response->setContent($builder->getPdf()->Output('', 'S'));
        return $response;
    }

    public function handleViewPdf($controller, &$data, $options = array()) {
        set_time_limit(200);

        $idReportSection = $controller->getRequest()->get('id', 0);
        $idLanguage = $controller->getRequest()->get('idLanguage', 1);
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $isDownload = $controller->getRequest()->get('isDownload', 0);

        $builder = $controller->get('leep_admin.report.business.builder');
        $builder->init();
        $builder->applyLanguage($idLanguage);

        // Load report test data
        $query = $em->createQueryBuilder();
        $query->select('r.testData, rs.idReport')
            ->from('AppDatabaseMainBundle:ReportSection', 'rs')
            ->leftJoin('AppDatabaseMainBundle:Report', 'r', 'WITH', 'rs.idReport = r.id')
            ->andWhere('rs.id = :idReportSection')
            ->setParameter('idReportSection', $idReportSection);
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $builder->parseConfigData($row['testData']);
            $idReport = intval($row['idReport']);
            break;
        }

        // Load common items
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportItem', 'p')
            ->leftJoin('AppDatabaseMainBundle:ReportSection', 'rs', 'WITH', 'p.idReportSection = rs.id')
            ->andWhere('rs.idReport = :idReport')
            ->setParameter('idReport', $idReport)
            ->andWhere('p.idTextToolType in (:commonType)')
            ->setParameter('commonType', array(
                Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_LAYOUT,
                Business\Report\Constants::TEXT_TOOL_TYPE_CHAPTER_TOC,
                //Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER
            ))
            ->orderBy('rs.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
        }

        $builder->begin();

        // Load report items
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportItem', 'p')
            ->andWhere('p.idReportSection = :idReportSection')
            ->setParameter('idReportSection', $idReportSection)
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'inline; filename=report_section_'.$idReportSection.'.pdf');
        if ($isDownload) {
            $response->headers->set('Content-Disposition', 'attachment; filename=report_section_'.$idReportSection.'.pdf');
        }
        $response->setContent($builder->getPdf()->Output('', 'S'));
        return $response;
    }

    public function handleDownloadFullReport($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');

        $idReport = $controller->getRequest()->get('idReport', 0);
        $report = $doctrine->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
        $idLanguage = $controller->getRequest()->get('idLanguage', 1);
        $language = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findOneById($idLanguage);

        $reportData = array(
            'idLanguage' => $idLanguage,
            'idReport'   => $idReport,
            'reportData' => $report->getTestData()
        );

        $maxJob = 10;
        $totalActiveJob = Business\OfflineJob\Utils::getCurrentActiveJobs($controller);
        if ($totalActiveJob >= $maxJob) {
            return new Response('<b style="color: red">You can\'t have more than '.$maxJob.' active jobs in queue</b>');
        }

        $description = array(
            'Report: '.$report->getName(),
            'Language: '.$language->getName()
        );

        $offlineJob = new Entity\OfflineJob();
        $offlineJob->setIdHandler(Business\OfflineJob\Constants::OFFLINE_JOB_HANDLER_TEST_REPORT);
        $offlineJob->setIdWebUser($userManager->getUser()->getId());
        $offlineJob->setInputTime(new \DateTime());
        $offlineJob->setDescription(implode('<br/>', $description));
        $offlineJob->setStatus(Business\OfflineJob\Constants::OFFLINE_JOB_PENDING);
        $offlineJob->setParameters(json_encode($reportData));
        $em->persist($offlineJob);
        $em->flush();

        return new Response('<b style="color: green">Job has been placed! Job ID: '.$offlineJob->getId().'</b>');
    }

    public function handleAddToReportQueue($controller, &$data, $options = array()) {
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');

        $idReport = $controller->getRequest()->get('idReport', 0);
        $report = $doctrine->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
        $idLanguage = $controller->getRequest()->get('idLanguage', 1);
        $isCmyk = $controller->getRequest()->get('isCmyk', 0);
        $language = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findOneById($idLanguage);

        $input = array(
            'idLanguage' => $idLanguage,
            'idReport'   => $idReport,
            'isCmyk'     => $isCmyk,
            'reportData' => $report->getTestData()
        );

        $description = array(
            'Report: '.$report->getName(),
            'Language: '.$language->getName()
        );
        if ($isCmyk) {
            $description[] = "CMYK: Yes";
        }

        $reportQueue = new Entity\ReportQueue();
        $reportQueue->setIdWebUser($userManager->getUser()->getId());
        $reportQueue->setInputTime(new \DateTime());
        $reportQueue->setDescription(implode('<br/>', $description));
        $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
        $reportQueue->setInput(serialize($input));
        $reportQueue->setIdReport($idReport);
        $reportQueue->setIdJobType(Business\ReportQueue\Constant::JOB_TYPE_BUILD_DEMO_REPORT);
        $em->persist($reportQueue);
        $em->flush();

        return new Response('<b style="color: green">Report has been put to queue. ID: '.$reportQueue->getId().'</b>');
    }

    public function handleScanTextBlock($controller, &$data, $options = array()) {
        set_time_limit(120);

        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();
        $extractor = $controller->get('leep_admin.report.business.text_block_extractor');
        $selectedId = $controller->get('request')->get('selectedId', '');

        $reportIds = array(0);
        foreach (explode(',', $selectedId) as $idReport) {
            $reportIds[] = intval($idReport);
        }

        // Clear old data
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ReportTextBlock', 'p')
            ->andWhere('p.idReport in (:reportIds)')
            ->setParameter('reportIds', $reportIds)
            ->getQuery()->getResult();

        // Scan reports
        $errorReports = array();
        $scanReports = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Report', 'p')
            ->andWhere('p.id in (:reportIds)')
            ->setParameter('reportIds', $reportIds);
        $reports = $query->getQuery()->getResult();

        foreach ($reports as $report) {
            $errorBlocks = array();

            $textBlocks = $extractor->extractTextBlockFromReport($report->getId());
            foreach ($textBlocks as $textBlockName => $textBlockId) {
                if ($textBlockId == 0) {
                    $errorBlocks[] = $textBlockName;
                }
                else {
                    $reportBlock = new Entity\ReportTextBlock();
                    $reportBlock->setIdReport($report->getId());
                    $reportBlock->setIdTextBlock($textBlockId);
                    $em->persist($reportBlock);
                }
            }

            if (!empty($errorBlocks)) {
                $errorReports[] = array(
                    'name' => $report->getName(),
                    'errorTextBlocks' => implode("\n", $errorBlocks)
                );
            }
            $scanReports[] = $report->getName();
        }
        $em->flush();

        $data['scanReports'] = $scanReports;
        $data['errorReports'] = $errorReports;
    }

    public function handleImageHelper($controller, &$data, $options = array()) {
        $images = $controller->get('leep_admin.report.business.util')->getResources();
        $data['images'] = $images;

        $pdfs = $controller->get('leep_admin.report.business.util')->getExternalPdf();
        $data['pdfs'] = $pdfs;

        return $controller->render('LeepAdminBundle:Report:image_helper.html.twig', $data);
    }

    public function handleDownloadShipmentPackage($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getManager();
        $request = $controller->get('request');

        $id = $request->get('idBodShipment', 0);
        $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($id);
        $isLowRes = $request->get('isLowRes', 0);

        $packageFile = $bodShipment->getPackageFile();
        if (!empty($packageFile)) {
            $downloadDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../scripts/pdfshipment/files/output';
            $downloadFile = $downloadDir.'/'.$bodShipment->getPackageFile();

            $fileResponse = new BinaryFileResponse($downloadFile);
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        } else {
            $downloadDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/shipment';
            $file = $isLowRes ? $bodShipment->getLowResolutionFile() : $bodShipment->getHighResolutionFile();

            $downloadFile = $downloadDir.'/'.$file;

            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($bodShipment->getIdCustomer());
            $shipmentId = Business\BodShipment\Utils::getShipmentId($controller, $bodShipment->getId());

            $fileResponse = new BinaryFileResponse($downloadFile);
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $customer->getOrderNumber().'_'.$shipmentId.'.zip');
        }

        return $fileResponse;
    }

    public function handleCheckTranslateReport($controller, &$data, $options=array())
    {
        $mapping = $controller->get('easy_mapping');
        $em = $controller->get('doctrine')->getEntityManager();
        $reportIds_str = $controller->get('request')->get('selectedId');
        $reportIds = array_map('intval', explode(',', $reportIds_str));
        array_pop($reportIds);

        $reports = [];
        foreach ($reportIds as $reportId) {
            $query = $em->createQueryBuilder();
            $query->select('count(rb.idTextBlock) as numberTextBlock')
                    ->from('AppDatabaseMainBundle:ReportTextBlock','rb')
                    ->where($query->expr()->eq('rb.idReport', ':reportId'))
                    ->setParameter('reportId', $reportId);
            $result = $query->getQuery()->getSingleScalarResult();
            $numberTextBlock = $result;

            $query = $em->createQueryBuilder();
            $query->select('tb.idLanguage')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'tb')
                ->leftJoin('AppDatabaseMainBundle:ReportTextBlock', 'rb', 'WITH', 'tb.idTextBlock = rb.idTextBlock')
                ->where($query->expr()->andX(
                        $query->expr()->eq('rb.idReport', ':reportId'),
                        $query->expr()->eq('tb.isTranslated', ':isTrue')))
                ->addGroupBy('tb.idLanguage')
                ->andHaving($query->expr()->eq('count(tb.idTextBlock)', ':numberTextBlock'))
                ->setParameter('reportId', $reportId)
                ->setParameter('isTrue', true)
                ->setParameter('numberTextBlock', $numberTextBlock);
            $resultsCompleteReport = $query->getQuery()->getResult();

            $languagesComplete = [];
            $languagesCompleteNames = [];
            foreach ($resultsCompleteReport as $language) {
                $languagesComplete[] = $language['idLanguage'];
                $languageName = $mapping->getMappingTitle('LeepAdmin_Language_List', $language['idLanguage']);
                $languagesCompleteNames[] = $languageName;
            }

            $resultsIncompleteReport = [];
            $query = $em->createQueryBuilder();
            $query->select('tb.idLanguage, count(tb.idTextBlock) as countTextBlock')
                    ->from('AppDatabaseMainBundle:TextBlockLanguage', 'tb')
                    ->leftJoin('AppDatabaseMainBundle:ReportTextBlock', 'rb', 'WITH', 'tb.idTextBlock = rb.idTextBlock')
                    ->where($query->expr()->andX(
                        $query->expr()->eq('rb.idReport', ':idReport'),
                        $query->expr()->eq('tb.isTranslated', ':isTrue')
                    ))
                    ->addGroupBy('tb.idLanguage')
                    ->setParameter(':isTrue', true)
                    ->setParameter('idReport', $reportId);
            if (!empty($languagesComplete)) {
                $query->andHaving($query->expr()->notIn('tb.idLanguage', ':listLanguage'))
                      ->setParameter('listLanguage', $languagesComplete);
            }
            $resultsIncompleteReport = $query->getQuery()->getResult();

            $query = $em->createQueryBuilder();
            $query->select('r.id, r.name')
                    ->from('AppDatabaseMainBundle:Report', 'r')
                    ->where($query->expr()->in('r.id', ':reportIds'))
                    ->setParameter('reportIds', $reportIds);
            $resultNames = $query->getQuery()->getResult();

            $reportsName = [];
            foreach ($resultNames as $result) {
                $reportsName[$result['id']] = $result['name'];
            }

            $incompleteLanguages = [];
            $incompleteLanguagesName = [];
            foreach ($resultsIncompleteReport as $record) {
                $languageName = $mapping->getMappingTitle('LeepAdmin_Language_List', $record['idLanguage']);
                $numberIncompleteTextBlock = $numberTextBlock - $record['countTextBlock'];
                $languageInfo = ['languageId' => $record['idLanguage'],
                                 'languageName' => $languageName,
                                 'count_text_block' => $numberIncompleteTextBlock];
                $incompleteLanguages[] = $languageInfo;
            }
            $reports[$reportId] = ['id' => $reportId,
                                     'reportName' => $reportsName[$reportId],
                                     'languages'  => $incompleteLanguages,
                                     'languagesComplete' => $languagesCompleteNames];
        }

        $mgr = $controller->get('easy_module.manager');
        $data['getLanguageDetailUrl'] = $mgr->getUrl('leep_admin', 'report', 'report', 'checkTranslateReportDetail');
        $data['reportsTextBlock'] = $reports;
        return $controller->render('LeepAdminBundle:Report:view_translate_report.html.twig', $data);
    }


    public function handleCheckTranslateReportDetail($controller, &$data, $options=array())
    {
        $reportDetail = $controller->get('request')->get('inputLanguageDetail');
        $reportLanguage = json_decode($reportDetail); // format request: [language_id, report_id]
        $reportId = $reportLanguage[1];
        $languageId = $reportLanguage[0];
        $em = $controller->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('tb.idTextBlock')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'tb')
                ->leftJoin('AppDatabaseMainBundle:ReportTextBlock', 'rb', 'WITH', 'tb.idTextBlock = rb.idTextBlock')
                ->where($query->expr()->andX(
                        $query->expr()->eq('rb.idReport', ':reportId'),
                        $query->expr()->orX(
                            $query->expr()->eq('tb.isTranslated', ':isFalse'),
                            $query->expr()->isNull('tb.isTranslated')
                        ),
                        $query->expr()->eq('tb.idLanguage', ':languageId')))
                ->setParameter('reportId', intval($reportId))
                ->setParameter('languageId', intval($languageId))
                ->setParameter('isFalse', false);
        $resultsTextBlock = $query->getQuery()->getResult();

        $textBlockIds = [];
        foreach ($resultsTextBlock as $textBlock) {
            $textBlockIds[] = $textBlock['idTextBlock'];
        }

        $namesTextBlock_chunk = [];
        if (!empty($textBlockIds)) {
            $query = $em->createQueryBuilder();
            $query->select('q.id, q.name')
                    ->from('AppDatabaseMainBundle:TextBlock', 'q')
                    ->where($query->expr()->in('q.id', ':textBlockIds'))
                    ->setParameter('textBlockIds', $textBlockIds);
            $namesTextBlock = $query->getQuery()->getResult();
            $namesTextBlock_chunk = array_chunk($namesTextBlock, intval(count($namesTextBlock) / 4) + 1);
        }

        $query = $em->createQueryBuilder();
        $query->select('r.name')
                ->from('AppDatabaseMainBundle:Report', 'r')
                ->where($query->expr()->eq('r.id', ':reportId'))
                ->setParameter('reportId', $reportId);
        $reportName = $query->getQuery()->getSingleScalarResult();

        $mapping = $controller->get('easy_mapping');
        $languageName = $mapping->getMappingTitle('LeepAdmin_Language_List', $languageId);

        $report = ['reportName' => $reportName,
                    'languageName' => $languageName,
                    'textBlockNames' => $namesTextBlock_chunk];

        $data['reportTextBlockLanguage'] = $report;
        $mgr = $controller->get('easy_module.manager');
        $data['editTextBlockUrl'] = $mgr->getUrl('leep_admin', 'text_block', 'edit', 'edit');
        $url = $controller->renderView('LeepAdminBundle:Report:view_translate_report_detail.html.twig', $data);
        return new Response($url);
    }


    public function printTextBlockNormal($controller, &$data, $options = array())
    {
        // get all reports in report_text_block table. With each report get all their text blocks and check these
        // text blocks with correspond text blocks in text_block_language table if their body(in text_block_language)
        // is null. If null, save text block id (get text block name in text_block).

        $reportIds_str = $controller->get('request')->get('selectedId');
        $reportIds = explode(',', $reportIds_str);
        array_pop($reportIds);

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('rb.idReport, rb.idTextBlock')
                ->from('AppDatabaseMainBundle:ReportTextBlock', 'rb')
                ->where($query->expr()->in('rb.idReport', ':reportIds'))
                ->setParameter('reportIds', $reportIds);
        $results = $query->getQuery()->getResult();

        $query = $em->createQueryBuilder();
        $query->select('r.id, r.name')
                ->from('AppDatabaseMainBundle:Report', 'r')
                ->where($query->expr()->in('r.id', ':reportIds'))
                ->setParameter('reportIds', $reportIds);
        $resultNames = $query->getQuery()->getResult();

        $reportsName = [];
        foreach ($resultNames as $result) {
            $reportsName[$result['id']] = $result['name'];
        }

        $reports = [];
        foreach ($results as $result) {
            $languages = $this->getInfoLanguages($controller, $result['idReport'], $result['idTextBlock']);
            if(!array_key_exists($result['idReport'], $reports)) {
                $reports[$result['idReport']] = ['reportName' => $reportsName[$result['idReport']],
                                                 'languages' => $languages];
            }
            else{
                $existedLanguages = $reports[$result['idReport']]['languages'];
                foreach ($languages as $idLanguage => $languageData) {
                    $this->updateLanguage($existedLanguages, $idLanguage, $languageData);
                    $reports[$result['idReport']]['languages'] = $existedLanguages;
                }
            }
        }

        $countTb = [];
        $counts = 0;
        foreach ($reports as $id => $report) {
            $tmps = [];
            foreach ($report['languages'] as $language) {
                $tmp = ['name' => $language['languageName'],
                        'count_text_block' => count($language['textblocks'])];
                $counts += $tmp['count_text_block'];
                $tmps[] = $tmp;
            }
            $countTb[$id] = $tmps;
        }

        $data['reportsTextBlock'] = $reports;
        return $controller->render('LeepAdminBundle:Report:view_translate_report.html.twig', $data);
    }

    private function getInfoLanguages($controller, $reportId, $textBlockId)
    {
        $mapping = $controller->get('easy_mapping');
        $em = $controller->get('doctrine')->getEntityManager();

         $query = $em->createQueryBuilder();
         $query->select('p.idLanguage')
                ->from('AppDatabaseMainBundle:TextBlockLanguage','p')
                ->where($query->expr()->andX(
                    $query->expr()->eq('p.idTextBlock', ':textBlockId'),
                    $query->expr()->neq('p.isTranslated', true)
                ))
                ->setParameter('textBlockId', $textBlockId);
        $results = $query->getQuery()->getResult();

        $query = $em->createQueryBuilder();
        $query->select('q.name')
                ->from('AppDatabaseMainBundle:TextBlock', 'q')
                ->where($query->expr()->eq('q.id', ':idTextBlock'))
                ->setParameter('idTextBlock', $textBlockId);
        $nameTextBlock = $query->getQuery()->getSingleScalarResult();

        $languages = [];
        foreach ($results as $result) {
            $currentLanguageName = $mapping->getMappingTitle('LeepAdmin_Language_List', $result['idLanguage']);
            $languages[$result['idLanguage']] = ['languageName' => $currentLanguageName,
                                                    'textblocks' => [$nameTextBlock]];
        }

        return $languages;
    }

    private function updateLanguage(&$existedLanguages, $idLanguage, $languageData)
    {
        if (array_key_exists($idLanguage, $existedLanguages)) {
            $existedLanguages[$idLanguage]['textblocks'][] = $languageData['textblocks'][0];
        }
        else{
            $existedLanguages[$idLanguage] = $languageData;
        }
    }
}
