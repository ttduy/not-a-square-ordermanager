<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ScanFormFile extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleDownloadPdf($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $scanFormFile= $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($id);

  /*      $attachmentDir = $this->container->getParameter('attachment_dir') . '/'. Business\ScanFormTemplate\Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;

        $fileResponse = new BinaryFileResponse($attachmentDir.'/'.$scanFormTemplate->getTemplateFile());          
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$scanFormTemplate->getTemplateFile());        
        return $fileResponse;*/
    }

    public function handleDownloadFile($controller, &$data, $options = array()) {
        $id = $controller->get('request')->get('id');
        $scanFormFile= $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($id);
/*
        $attachmentDir = $this->container->getParameter('attachment_dir') . '/'. Business\ScanFormTemplate\Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;

        $fileResponse = new BinaryFileResponse($attachmentDir.'/'.$scanFormTemplate->getTemplateFile());          
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $fileResponse->headers->set('Content-Disposition', 'attachment; filename='.$scanFormTemplate->getTemplateFile());        
        return $fileResponse;*/
    }

    public function handleViewError($controller, &$data, $options = array()) {
        $data = array();

        $id = $controller->get('request')->get('id');
        $scanFormFile= $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($id);

        if ($scanFormFile) {
            $data['errorMessage'] = $scanFormFile->getErrorMessage();
        }

        return $controller->render('LeepAdminBundle:ScanFormFile:view_error.html.twig', $data);
    }
}
