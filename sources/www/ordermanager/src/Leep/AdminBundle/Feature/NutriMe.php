<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class NutriMe extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
            'handlers' => array(
            )
        );
    }

    public function handleStartProduction($controller, &$data, $options = array()) {    
        $em = $controller->get('doctrine')->getEntityManager();
        $codeParser = $controller->get('leep_admin.nutri_me.business.code_parser');
        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        $nutriMe = $em->getRepository('AppDatabaseMainBundle:NutriMe')->findOneById($id);
        $d = $codeParser->parse($nutriMe->getBarCode());
        if ($d['error'] != '') {
            die("Error in barcode: ". $d['error']);
        }
        $d = $d['data'];

        // Basic info
        $data['id'] = $id;
        $data['orderNumber'] = $d['orderNumber'];
        $data['supplementDaysOrder'] = $d['supplementDaysOrder'];
        $data['product'] = $d['product'];
        $data['totalAmount'] = $d['total'];
        $data['totalPack'] = $d['numPack'];
        $data['drugsJson'] = json_encode($d['drugs']);

        $data['doneProductionLink'] =  $mgr->getUrl('leep_admin', 'nutri_me', 'feature', 'doneProduction', array('id' => $id));            

        return $controller->render('LeepAdminBundle:NutriMe:production.html.twig', $data);
    }

    public function handleDoneProduction($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $request = $controller->get('request');

        $id = $request->get('id', 0);
        $actualUnit = $request->get('inputFinalText', 0);

        $nutriMe = $em->getRepository('AppDatabaseMainBundle:NutriMe')->findOneById($id);
        $nutriMe->setActualUnit(floatval($actualUnit));
        $nutriMe->setProcessedOn(new \DateTime());
        $nutriMe->setProcessedBy($userManager->getUser()->getId());

        // Load current lots
        $currentLots = $this->captureCurrentLots($controller);        
        $nutriMe->setCurrentLots(serialize($currentLots));

        $em->persist($nutriMe);
        $em->flush();

        $url = $mgr->getUrl('leep_admin', 'nutri_me', 'edit', 'edit', array('id' => $id));
        return new RedirectResponse($url);
    }

    private function captureCurrentLots($container) {
        $em = $container->get('doctrine')->getEntityManager();
        $currentLots = array();

        // get supplement type
        $currentLots['supplementType'] = array();
        $result = $em->getRepository('AppDatabaseMainBundle:SupplementType')->findAll();
        foreach ($result as $r) {
            $currentLots['supplementType'][] = array(
                'name'     => $r->getName(),
                'letter'   => $r->getLetter(),
                'lotInUse' => $r->getLotInUse()
            );
        }

        // get tested and in-use Pellet
        $currentLots['testedAndInUsePellet'] = array();
        $query = $em->createQueryBuilder();
        $query->select('
                pe.idPellet as id,
                pe.cups, pe.section, p.notes, pe.shipment,
                p.letter, p.name as pelletName,
                p.activeIngredientName as activeIngredient
            ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'pe')
            ->leftJoin('AppDatabaseMainBundle:Pellet', 'p', 'WITH', 'pe.idPellet = p.id')
            ->andWhere('pe.section = :sectionInd')
            ->setParameter('sectionInd', Business\Pellet\Constant::SECTION_TESTED_AND_IN_USE);

        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $currentLots['testedAndInUsePellet'][] = array(
                'name'     => $r['pelletName'],
                'letter'   => $r['letter'],
                'shipment' => $r['shipment'],
                'activeIngredient' => $r['activeIngredient']
            );
        }

        // get selected used-up-Material
        $currentLots['selectedUsedUpMaterial'] = array();
        $query = $em->createQueryBuilder();
        $query->select('p.quantity, p.usedfor, p.datetaken, p.takenby, m.name as materialName')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('p.isSelected = :isSelected')
            ->setParameter('isSelected', 1)
            ->orderBy('p.datetaken', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $currentLots['selectedUsedUpMaterial'][] = array(
                'materialName'  => $r['materialName'],
                'quantity'      => $r['quantity'],
                'usedFor'       => $r['usedfor'],
                'dateTaken'     => ($r['datetaken'] ? $r['datetaken']->format('d/m/Y') : ''),
                'takenBy'       => $r['takenby']
            );
        }

        return $currentLots;
    }



    public function handleBulkEditStatus($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();        
        $mgr = $controller->get('easy_module.manager');
        
        $form = $controller->get('leep_admin.nutri_me.business.bulk_edit_status_handler');
        $form->execute();

        $data['form'] = array(
            'id'       => 'FormId',
            'title'    => 'Bulk-edit status',
            'view'     => $form->getForm()->createView(),
            'submitLabel' => 'Update',
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $mgr->getUrl('leep_admin', 'nutri_me', 'feature', 'bulkEditStatus')
        );

        return $controller->render('LeepAdminBundle:NutriMe:bulk_edit_status.html.twig', $data);
    }


    public function handleBulkEditStatusValidate($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $arr = array(0);
        $selectedIdRaw = $controller->get('request')->get('orderNumbers', '');
        $tmp = explode("\n", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $foundOrders = array();

        $em = $controller->get('doctrine')->getEntityManager();        
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:NutriMe', 'd')
            ->andWhere('d.orderNumber in (:arr)')
            ->setParameter('arr', $arr)
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $foundOrders[$r->getOrderNumber()] = $r;
        }

        $data = array();
        $today = new \DateTime();
        $todayTs = $today->getTimestamp();
        foreach ($arr as $orderNumber) {
            if ($orderNumber === 0) continue;
            if (!isset($foundOrders[$orderNumber])) {
                $data[] = array(
                    'id'          => 0,
                    'orderNumber' => $orderNumber,
                    'customerName' => '',
                    'color'       => '#FFCCCC',
                    'status'      => 'Order not found'
                );
            }
            else {        
                $nutriMe = $foundOrders[$orderNumber];

                $status = '';
                if ($nutriMe->getCurrentStatusDate()) {
                    $age = intval(($todayTs - $nutriMe->getCurrentStatusDate()->getTimestamp()) / 86400);
                    $status = (($age === '') ? '': "(${age}d) ").$formatter->format($nutriMe->getCurrentStatus(), 'mapping', 'LeepAdmin_NutriMe_Status');
                }   

                $data[] = array(
                    'id'          => $nutriMe->getId(),
                    'orderNumber' => $orderNumber,
                    'customerName' => $nutriMe->getCustomerName(),
                    'color'       => '',
                    'status'      => $status
                );
            }
        }

        return $controller->render('LeepAdminBundle:NutriMe:bulk_edit_status_validate.html.twig', array('data' => $data));
    }

}
