<?php
namespace Leep\AdminBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class MaterialFinanceTracker extends AbstractFeature {
    public function getActions() { return array(); }
    public function getDefaultOptions() { return array(); }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function setDefaultTabs(&$data) {
        $mgr = $this->container->get('easy_module.manager');
        $data['mainTabs'] = array(
            'content' => array(
                'materialType' => array('title' => 'Material type', 'link' => $mgr->getUrl('leep_admin', 'material', 'material_finance_tracker', 'materialType')),
            )
        ); 

        $mapping = $this->container->get('easy_mapping');
        $data['months'] = $mapping->getMapping('LeepAdmin_Month_Short');
    }

    private function prepareAnalyseArray(&$arr) {
        // Define values
        foreach ($arr as $k => $name) {
            $arr[$k] = array(
                'name'     => $name,
                'starting' => 0,
                'data'     => array()
            );
            for ($i = 1; $i <= 12; $i++) {
                $arr[$k]['data'][$i] = array('ordered' => 0, 'usedUp' => 0, 'current' => 0);
            }
        }
    }

    private function hideUnspecified(&$arr) {
        if (isset($arr[0])) {
            foreach ($arr[0]['data'] as $r) {
                if (!empty($r['ordered']) || !empty($r['usedUp']) || !empty($r['current']) ) {
                    return;
                }
            }

            unset($arr[0]);
        }
    }

    private function applyFilter(&$data, $filter, $tab) {
        $filter->execute();
        $data['filter'] = array(
            'id'       => 'MainFilter',
            'view'     => $filter->getForm()->createView(),
            'submitLabel' => 'Filter',
            'messages' => $filter->getMessages(),
            'errors'   => $filter->getErrors(),
            'url'      => $this->getModule()->getUrl('material_finance_tracker', $tab),
            'visiblity' => ''
        );
    }

    private function sortData(&$arr, $filterData) {
        $sortComparer = new Business\FinanceTracker\DataSortComparer();
        /*
        if (strpos($filterData->sortBy, 'order_') === 0) {
            $month = intval(substr($filterData->sortBy, 6));
            $sortComparer->month = $month;
            $sortComparer->compareBy = 'order';
            $sortComparer->sortDirection = $filterData->sortDirection;

            usort($arr, array($sortComparer, 'compare'));
        }
        else if (strpos($filterData->sortBy, 'amount_') === 0) {
            $month = intval(substr($filterData->sortBy, 7));
            $sortComparer->month = $month;
            $sortComparer->compareBy = 'amount';
            $sortComparer->sortDirection = $filterData->sortDirection;

            usort($arr, array($sortComparer, 'compare'));
        }
        */
    }

    private function prepareChartData($total, $year, $field) {
        $today = new \DateTime();
        $curYear = intval($today->format('Y'));
        $maxMonth = 12;
        if ($curYear <= $year) {
            $maxMonth = intval($today->format('m'));
        }

        $arr = array();
        foreach ($total as $i => $d) {
            if ($i > $maxMonth) continue;
            $arr[] = array($i, $d[$field]);
        }
        return json_encode($arr);
    }

    private function getCustomEntityManager() {        
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTH', 'Leep\AdminBundle\Doctrine\Extension\Month');            
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Doctrine\Extension\Year');            

        return $em;
    }

    private function getUnit($unit) {
        if (empty($unit)) {
            return 1;
        }
        return floatval($unit);
    }

    public function handleMaterialType($controller, &$data, $options = array()) {   
        $this->setDefaultTabs($data);
        $data['mainTabs']['selected'] = 'materialType';

        $filter = $controller->get('leep_admin.material_finance_tracker.business.material_type_filter_handler');
        $this->applyFilter($data, $filter, 'materialType');
        
        $filterData = $filter->getCurrentFilter();
        $em = $this->getCustomEntityManager();

        // Get list of material types
        $materialTypes = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MatType', 'p')
            ->andWhere('p.isdeleted = 0');

        if (!empty($filterData->materialTypes)) {
            $query->andWhere('p.id IN (:materialTypes)')
                ->setParameter('materialTypes', $filterData->materialTypes);
        }
        if ($filterData->sortBy == 'name') {
            $query->orderBy('p.mattype', $filterData->sortDirection);
        }

        // Hide (unspecified) in filtered
        if (empty($filterData->materialTypes)) {
            $materialTypes[0] = '(unspecified)';
        }

        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $materialTypes[$r->getId()] = $r->getMatType();
        }

        // Define values
        $this->prepareAnalyseArray($materialTypes);

        // Compute statistic
        $total = array();
        for ($i = 1; $i <= 12; $i++) {
            $total[$i] = array('current' => 0, 'ordered' => 0, 'usedUp' => 0);
        }
        $totalStarting = 0;

        //////////////////////////////////////////////
        // x. ORDERED AMOUNT
        // Compute current ordered amount
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, m.materialtypeid, p.quantity, m.price, m.unitsperprice')
            ->from('AppDatabaseMainBundle:MaterialsPurchase', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('YEAR(p.dateordered) < :year')
            //->groupBy('month, m.materialtypeid')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idMaterialType = $r['materialtypeid'];
            if (!isset($materialTypes[$idMaterialType])) {
                if (isset($materialTypes[0])) {
                    $idMaterialType = 0;
                }
            }

            if (isset($materialTypes[$idMaterialType])) {
                $month = $r['month'];
                $amount = floatval($r['quantity']) * floatval($r['price']) / $this->getUnit($r['unitsperprice']);

                $materialTypes[$idMaterialType]['starting'] += $amount;
                $totalStarting += $amount;
            }
        }

        // Compute ordered amount
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.dateordered) as month, m.materialtypeid, p.quantity, m.price, m.unitsperprice')
            ->from('AppDatabaseMainBundle:MaterialsPurchase', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('YEAR(p.dateordered) = :year')
            //->groupBy('month, m.materialtypeid')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idMaterialType = $r['materialtypeid'];
            if (!isset($materialTypes[$idMaterialType])) {
                if (isset($materialTypes[0])) {
                    $idMaterialType = 0;
                }
            }

            if (isset($materialTypes[$idMaterialType])) {
                $month = $r['month'];
                $amount = floatval($r['quantity']) * floatval($r['price']) / $this->getUnit($r['unitsperprice']);

                $materialTypes[$idMaterialType]['data'][$month]['ordered'] += $amount;
                $total[$month]['ordered'] += $amount;
            }
        }

        //////////////////////////////////////////////
        // x. ORDERED AMOUNT
        // Compute current used up amount
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.datetaken) as month, m.materialtypeid, p.quantity, m.price, m.unitsperprice')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('YEAR(p.datetaken) < :year')
            //->groupBy('month, m.materialtypeid')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idMaterialType = $r['materialtypeid'];
            if (!isset($materialTypes[$idMaterialType])) {
                if (isset($materialTypes[0])) {
                    $idMaterialType = 0;
                }
            }

            if (isset($materialTypes[$idMaterialType])) {
                $month = $r['month'];
                $amount = floatval($r['quantity']) * floatval($r['price']) / $this->getUnit($r['unitsperprice']);

                $materialTypes[$idMaterialType]['starting'] -= $amount;
                $totalStarting -= $amount;
            }
        }

        // Compute used up amount
        $query = $em->createQueryBuilder();
        $query->select('MONTH(p.datetaken) as month, m.materialtypeid, p.quantity, m.price, m.unitsperprice')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('YEAR(p.datetaken) = :year')
            //->groupBy('month, m.materialtypeid')
            ->setParameter('year', intval($filterData->year));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idMaterialType = $r['materialtypeid'];
            if (!isset($materialTypes[$idMaterialType])) {
                if (isset($materialTypes[0])) {
                    $idMaterialType = 0;
                }
            }

            if (isset($materialTypes[$idMaterialType])) {
                $month = $r['month'];
                $amount = floatval($r['quantity']) * floatval($r['price']) / $this->getUnit($r['unitsperprice']);

                $materialTypes[$idMaterialType]['data'][$month]['usedUp'] += $amount;
                $total[$month]['usedUp'] += $amount;
            }
        }

        $this->hideUnspecified($materialTypes);
        $this->sortData($materialTypes, $filterData);

        foreach ($materialTypes as $idMaterialType => $m) {
            $value = $m['starting'];
            for ($i = 1; $i <= 12; $i++) {
                $value += $m['data'][$i]['ordered'] - $m['data'][$i]['usedUp'];
                $materialTypes[$idMaterialType]['data'][$i]['current'] = $value;
            }
        }

        $value = $totalStarting;
        for ($i = 1; $i <= 12; $i++) {
            $value += $total[$i]['ordered'] - $total[$i]['usedUp'];
            $total[$i]['current'] = $value;
        }

        $today = new \DateTime();
        $data['isSameYear'] = ($filterData->year >= intval($today->format('Y')));
        $data['currentMonth'] = $today->format('m');

        // Set data
        $data['statistic'] = $materialTypes;
        $data['total'] = $total;
        $data['totalOrderedJson'] = $this->prepareChartData($total, $filterData->year, 'ordered');
        $data['totalUsedUpJson'] = $this->prepareChartData($total, $filterData->year, 'usedUp');
        $data['totalCurrentJson'] = $this->prepareChartData($total, $filterData->year, 'current');
    }
}