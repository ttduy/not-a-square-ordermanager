<?php
namespace Leep\AdminBundle\Listener;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Database\MainBundle\Entity;
use App\AdminBundle\Business;

class CustomerEntityListener implements \Doctrine\Common\EventSubscriber {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getSubscribedEvents() {
        return array('postPersist', 'postUpdate');
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        if ($entity instanceof Entity\Customer) {
            if ($entity->getStatus() == 25) { // Fix me
                $entity->setArrivalDate($entity->getStatusDate());
            }
        }        
    }
    
    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        if ($entity instanceof Entity\Customer) {
            if ($entity->getStatus() == 25) { // Fix me
                $entity->setArrivalDate($entity->getStatusDate());
            }
        }
    }
}
