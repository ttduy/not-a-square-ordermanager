<?php
namespace Leep\AdminBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Response;
use App\Database\MainBundle\Entity;

class IpCheckListener
{
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            // don't do anything if it's not the master request
            return;
        }
/*
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $templating = $this->container->get('templating');

        // Log
        $clientIP = trim($_SERVER['REMOTE_ADDR']);
        $log = new Entity\IpLog();
        $log->setIpAddress($clientIP);
        $log->setLogTime(new \DateTime());
        $em->persist($log);
        $em->flush();

        // Check
        $ip = $doctrine->getRepository('AppDatabaseMainBundle:IpWhitelist')->findOneByIpAddress($clientIP);
        if (!$ip) {
            $data = array(
                'pageTitle' => 'IP Blocked',
                'ip' => $clientIP
            );
            $page = $templating->render('LeepAdminBundle:Ip:blocked.html.twig', $data);
            $event->setResponse(new Response($page));
        }
        */
    }
}
