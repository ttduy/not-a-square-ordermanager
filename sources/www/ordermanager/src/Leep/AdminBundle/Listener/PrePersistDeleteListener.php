<?php
namespace Leep\AdminBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class PrePersistDeleteListener 
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if (method_exists($entity, 'setIsDeleted')) {
            $entity->setIsDeleted(0);
        }
    }
}