<?php
namespace Leep\AdminBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Database\MainBundle\Entity;

class EncryptListener 
{
    public $container;
    public $helperEncrypt;
    public $encryptMap;
    public function __construct($container) {
        $this->container = $container;
        $this->helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $this->encryptMap = array(
            array(
                'instance' => new Entity\WebUsers(),
                'fields'   => array('password')
            )
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        foreach ($this->encryptMap as $map) {
            if ($entity instanceof $map['instance']) {
                foreach ($map['field'] as $field) {
                    $setMethod = 'set'.ucfirst($field);
                    $getMethod = 'get'.ucfirst($field);
                    $entity->$setMethod($this->helperEncrypt->encrypt($entity->$getMethod()));
                }                
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $args) {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        foreach ($this->encryptMap as $map) {
            if ($entity instanceof $map['instance']) {
                foreach ($map['field'] as $field) {
                    $getMethod = 'get'.ucfirst($field);
                    $args->setNewValue($field, $this->helperEncrypt->encrypt($entity->$getMethod()));
                }                
            }
        }
        if ($entity instanceof Entity\WebUsers) {
            $args->setNewValue('password', $this->helperEncrypt->encrypt($entity->getPassword()));
        }
    }

    public function postLoad(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        if ($entity instanceof Entity\WebUsers) {
            $entity->setPassword($this->helperEncrypt->decrypt($entity->getPassword()));
        }

    }
}