<?php
namespace Leep\AdminBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;
use Firebase\JWT\JWT;

class FireWall
{
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
    public function onRequestAll(GetResponseEvent $event) {
        // $request = $event->getRequest();
        // $session = $this->container->get('session');
        // $token = $this->container->get('security.context')->getToken();
        // if (strpos($request->getRequestUri(), '/callback') === 0) {
        //     $this->onHandleCallback($event);
        // }
        // if (strpos($request->getRequestUri(), '/Login/Show') == false) {
        //     //Validate access token
        //     $accessToken = $session->get('_firewall_access_token', '');
        //     if (empty($accessToken)) {
        //         $data = array(
        //             'callback'   => $this->container->getParameter('app_base_url').'/callback',
        //             'redirect'   => $request->getUri(),
        //             'client_id'  => $this->container->getParameter('novogenia_ordermanager_client')
        //         );
        //         $url = $this->container->getParameter('oauth_id_url').'/authorize?'.http_build_query($data);
        //         $event->setResponse(new RedirectResponse($url));
        //     }
        // }
    }

    // public function onCheckAccessToken(GetResponseEvent $event)
    // {
    //     print_r('check request');
    //
    // }
    // public function onHandleCallback(GetResponseEvent $event) {
    //     $request = $event->getRequest();
    //     $code = $request->get('code', '');
    //     $redirectUrl = $request->get('redirect', '/');
    //     $url = $this->container->getParameter('oauth_id_url').'/token?code='.$code;
    //     $client = new \GuzzleHttp\Client();
    //     die();
    //
    //     $data = json_decode($response->getBody()->getContents(), true);
    //     if (isset($data['access_token']) && isset($data['id_token']) && $data['id_token'] != '') {
    //         $accessToken = $data['access_token'];
    //         $jwt = (array) JWT::decode($data['id_token'], $this->getParameter('task_secret'), array('HS256'));
    //         // Validation
    //         if (!isset($jwt['username']) or !isset($jwt['name']) or !isset($jwt['iss']) or !isset($jwt['app']) or !isset($jwt['is_grant']) or
    //             empty($jwt['is_grant']) or
    //             ($jwt['app'] != $this->getParameter('oauth_app_name'))) {
    //             die('Invalid JWT!');
    //         }
    //         // Done
    //         $session = $this->get('session');
    //         $session->set('_firewall_access_token', $accessToken);
    //         $session->set('_firewall_expire', time() + $this->getParameter('oauth_expire_time'));
    //         $session->set('_firewall_jwt', $jwt);
    //         $event->setResponse(new RedirectResponse($redirectUrl));
    //     }
    // }
    // public function logout(GetResponseEvent $event) {
    //     // $request = $event->getRequest();
    //     // if (strpos($request->getRequestUri(), '/logout') === 0) {
    //     //     $session = $this->get('session');
    //     //     $session->clear();
    //     //     $url = $this->getParameter('oauth_id_url').'/logout';
    //     //     $event->setResponse(new RedirectResponse($url));
    //     //}
    // }
}
