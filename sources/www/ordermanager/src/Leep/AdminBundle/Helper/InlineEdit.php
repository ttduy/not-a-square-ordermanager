<?php
namespace Leep\AdminBundle\Helper;

class InlineEdit {    
    public function getHtml($field, $id, $targetUrl) {
        $html = '<span id="'.$id.'_field">'.$field.'</span>';

        $html .= '<span style="display: none" id="'.$id.'_textbox"><input style="width: 100px;" type="textbox" id="'.$id.'" value="'.$field.'" />';
        $html .= '&nbsp;&nbsp;<a href="javascript:ajaxUpdate(\''.$id.'\', \''.$targetUrl.'\')"><div class="button-submit"></div></a></span>';
        $html .= '&nbsp;&nbsp;<a id="'.$id.'_link" href="javascript:showAjaxUpdate(\''.$id.'\')"><div class="button-edit"></div></a>';

        return $html;
    }
}