<?php
namespace Leep\AdminBundle\Helper;

class CacheBoxData {    
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $mapping = array();

    public function hasKey($key) {
        return isset($this->mapping[$key]);
    }   

    public function putKey($key, $arr) {
        $this->mapping[$key] = $arr;
    }

    public function getCacheBoxData() {
        return $this->mapping;
    }

    public function getKey($key) {
        if (isset($this->mapping[$key])) {
            return $this->mapping[$key];
        }
        return null;
    }
}
