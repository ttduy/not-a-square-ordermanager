<?php
namespace Leep\AdminBundle\Helper;

class Formatter {  
    public $container;
    public $mapping;
    public function __construct($container) {
        $this->container = $container;
        $this->mapping = $container->get('easy_mapping');
    }
    public function format($value, $type, $opt = array()) {
        switch ($type) {
            case 'date':
                return $value != null ? $value->format('d/m/Y') : '';
            case 'datetime':
                return $value != null ? $value->format('d/m/Y H:i:s') : '';    
            case 'money':
                $symbol = "\xE2\x82\xAc";
                if (!empty($opt)) {
                    $symbol = $opt;
                }
                return $symbol.number_format($value, 2);
            case 'mapping':
                return $this->mapping->getMappingTitle($opt, $value);
            case 'single_line':
                return str_replace("\r\n", ' ', $value);
        }
        return $value;
    }
}
