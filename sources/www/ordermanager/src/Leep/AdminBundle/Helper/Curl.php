<?php
namespace Leep\AdminBundle\Helper;

class Curl {    
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function getPageContent($url, $method = 'GET', $data = array()) {
        $s = curl_init(); 

        curl_setopt($s, CURLOPT_URL, $url); 
        curl_setopt($s, CURLOPT_TIMEOUT, 30); 
        curl_setopt($s, CURLOPT_MAXREDIRS, 5); 
        curl_setopt($s, CURLOPT_RETURNTRANSFER,true); 
        if ($method == 'POST') {
            curl_setopt($s, CURLOPT_POST, true); 
            curl_setopt($s, CURLOPT_POSTFIELDS, $data); 
        }
            
        $content = curl_exec($s); 
        curl_close($s); 
        return $content;
    }
}
