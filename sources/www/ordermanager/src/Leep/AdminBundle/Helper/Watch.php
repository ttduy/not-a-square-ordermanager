<?php
namespace Leep\AdminBundle\Helper;

class Watch {  
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $logs = array();
    public function log($msg) {
        $this->logs[] = array('msg' => $msg, 'time' => new \DateTime(), 'utime' => microtime(true)); 
    }

    public function write($filename) {
        file_put_contents($filename, "--------------------------\n", FILE_APPEND);
        $previousTime = null;
        foreach ($this->logs as $log) {
            $totalTime = ($previousTime == null) ? '' : ($log['utime'] - $previousTime);
            file_put_contents($filename, 
                sprintf("%s [%s]: %s\n", $log['time']->format('d/m/Y H:i:s'), $totalTime, $log['msg']),
                FILE_APPEND
            );
            $previousTime = $log['utime'];
        }
    }
}
