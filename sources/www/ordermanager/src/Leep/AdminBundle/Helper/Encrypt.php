<?php
namespace Leep\AdminBundle\Helper;


use App\Database\MainBundle\Entity;

class Encrypt {  
    public $container;
    public function __construct($container) {
        $this->container = $container;
        $this->key = $container->getParameter('key');
        $this->iv  = $container->getParameter('iv');
    }

    public function encrypt($value) {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->key, $value, MCRYPT_MODE_CBC, $this->iv)));
    }

    public function decrypt($value) {
        $value = base64_decode($value);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->key, $value, MCRYPT_MODE_CBC, $this->iv); 
        return trim($decrypttext); 
    }
}
