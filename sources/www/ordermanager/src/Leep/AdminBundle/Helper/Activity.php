<?php
namespace Leep\AdminBundle\Helper;


use App\Database\MainBundle\Entity;

class Activity {  
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getCurrentTimeId() {
        $date = new \DateTime('now');

        $hour = $date->format('H');
        $time = $date->format('i');
        if ($time < 15) { $i = 0; }
        else if ($time < 30) { $i = 1; }
        else if ($time < 45) { $i = 2; }
        else { $i = 3; }

        return $hour * 4 + $i;
    }

    public function getMaxSortOrder($repository, $idField, $idValue) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('p.sortorder')
            ->from('AppDatabaseMainBundle:'.$repository, 'p')
            ->andWhere('p.'.$idField.' = :id')
            ->setParameter('id', $idValue)
            ->getQuery()->getResult();
        $maxSortOrder = 0;
        foreach ($result as $r) {
            if ($r['sortorder'] > $maxSortOrder) {
                $maxSortOrder = $r['sortorder'];
            }
        }
        $maxSortOrder += 1;
        return $maxSortOrder;
    }

    public function addCustomerActivity($customerId, $activityTypeId, $notes) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $customerActivity = new Entity\CustomerActivity();
        $customerActivity->setCustomerId($customerId);
        $customerActivity->setActivityDate(new \DateTime());
        $customerActivity->setActivityTimeId($this->getCurrentTimeId());
        $customerActivity->setActivityTypeId($activityTypeId);
        $customerActivity->setNotes($notes);
        $customerActivity->setSortOrder($this->getMaxSortOrder('CustomerActivity', 'customerid', $customerId));
        $em->persist($customerActivity);
        $em->flush();
    }

    public function addCollectiveInvoiceActivity($collectiveInvoiceId, $activityTypeId, $notes) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $collectiveInvoiceActivity = new Entity\CollectiveInvoiceActivity();
        $collectiveInvoiceActivity->setCollectiveInvoiceId($collectiveInvoiceId);
        $collectiveInvoiceActivity->setActivityDate(new \DateTime());
        $collectiveInvoiceActivity->setActivityTimeId($this->getCurrentTimeId());
        $collectiveInvoiceActivity->setActivityTypeId($activityTypeId);
        $collectiveInvoiceActivity->setNotes($notes);
        $collectiveInvoiceActivity->setSortOrder($this->getMaxSortOrder('CollectiveInvoiceActivity', 'collectiveinvoiceid', $collectiveInvoiceId));
        $em->persist($collectiveInvoiceActivity);
        $em->flush();
    }

    public function addCollectivePaymentActivity($collectivePaymentId, $activityTypeId, $notes) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $collectivePaymentActivity = new Entity\CollectivePaymentActivity();
        $collectivePaymentActivity->setCollectivePaymentId($collectivePaymentId);
        $collectivePaymentActivity->setActivityDate(new \DateTime());
        $collectivePaymentActivity->setActivityTimeId($this->getCurrentTimeId());
        $collectivePaymentActivity->setActivityTypeId($activityTypeId);
        $collectivePaymentActivity->setNotes($notes);
        $collectivePaymentActivity->setSortOrder($this->getMaxSortOrder('CollectivePaymentActivity', 'collectivepaymentid', $collectivePaymentId));
        $em->persist($collectivePaymentActivity);
        $em->flush();
    }
    
}
