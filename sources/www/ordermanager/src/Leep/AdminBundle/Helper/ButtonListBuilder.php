<?php
namespace Leep\AdminBundle\Helper;

class ButtonListBuilder {
    public $buttons = array();

    public function addButton($title, $link, $imageCode = 'button-detail') {
        $this->buttons[] = '<a href="'.$link.'"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }
    public function addNewTabButton($title, $link, $imageCode = 'button-detail') {
        $this->buttons[] = '<a target="_blank" href="'.$link.'"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }

    public function addLink($title, $link) {
        $this->buttons[] = '<a href="'.$link.'">'.$title."</a>";
    }

    public function addNewTabLink($title, $link) {
        $this->buttons[] = '<a target="_blank" href="'.$link.'">'.$title."</a>";
    }

    public function getLink($title, $link) {
        return '<a href="'.$link.'">'.$title."</a>";
    }

    public function addImage($title, $imageCode = 'button-detail') {
        $this->buttons[] = '<div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div>';
    }

    public function addPopupButton($title, $link, $imageCode = 'button-detail') {
        $this->buttons[] = '<a href="'.$link.'" class="dataTablePopupButton"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }

    public function addConfirmButton($title, $link, $imageCode = 'button-delete', $confirm = 'Do you really want to delete this item?\nThis process can not be undone!') {
        $this->buttons[] = '<a href="#" onclick="swal({   title: \'Attention!\',   text: \''. $confirm . '\',   type: \'warning\',   showCancelButton: true,   confirmButtonColor: \'#DD6B55\',   confirmButtonText: \'Yes, delete it!\',   cancelButtonText: \'No, cancel plx!\',   closeOnConfirm: true,   closeOnCancel: true }, function(isConfirm){   if (isConfirm) {  location.href=\''.$link.'\';   } else {     return false;   } })"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }

    public function addConfirmButtonAction($title, $link, $imageCode = 'button-delete', $confirm, $confirmYes, $confirmNo, $linkAfter, $linkDownload) {
        $this->buttons[] = '<a href="#" onclick="swal({   title: \'Attention!\',   text: \''. $confirm . '\',   type: \'warning\',   showCancelButton: true,   confirmButtonColor: \'#DD6B55\',   confirmButtonText: \''. $confirmYes.'\',   cancelButtonText: \''. $confirmNo.'\',   closeOnConfirm: true,   closeOnCancel: true }, function(isConfirm){
            if (isConfirm)
            {
                jQuery.ajax({
                    url: \''. $link.'\',
                    type: \'POST\',
                    async: false,
                    context: document.body
                }).
                done(function(data){
                    if (data != \'SUCCESS\') {
                        alert(\'Fail\');
                    }
                }).
                error(function(jqXHR, textStatus, errorThrown ){
                });
                ///
                jQuery.ajax({
                    url: \''. $linkAfter.'\',
                    type: \'POST\',
                    async: false,
                    context: document.body
                }).
                done(function(data){
                    if (data != \'SUCCESS\') {
                        alert(\'Data null! Failed to generate pdf\');
                    }
                    else {
                        location.href=\''. $linkDownload.'\';
                    }

                }).
                error(function(jqXHR, textStatus, errorThrown ){
                    alert(\'Data null! Failed to generate pdf\');
                });
                setTimeout(function() {
                    location.reload();
                }, 5000)
            }
            else {
                return false;
            }
        })"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }

    public function getHtml() {
        return implode('&nbsp;&nbsp;', $this->buttons);
    }

    public function addPromptButton($title, $promptLabel = '', $defaultContent = '', $imageCode = 'button-detail') {
        $this->buttons[] = '<a href="#" onclick="swal({   title: \'' . $title . '\',   text: \'' . $promptLabel . '\', inputValue: \'' . $defaultContent . '\', type: \'input\',   showCancelButton: false,   closeOnConfirm: true}, function(inputValue){   return false;})"><div class="'.$imageCode.'" alt="'.$title.'" title="'.$title.'"></div></a>';
    }
}
