<?php
namespace Leep\AdminBundle\Helper;

class Stopwatch {    
    private $container;
    private $stopwatch = null;
    private $stopWatchEvents = array();
    private $allowToWork = false;
    private $logFile = null;

    public function __construct($container) {
        $this->container = $container;
        $this->stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();

        if ($this->container->hasParameter('stopwatch_switcher')) {
            $this->allowToWork = $this->container->getParameter('stopwatch_switcher');
        }

        if ($this->container->hasParameter('stopwatch_log')) {
            $this->logFile = $this->container->getParameter('stopwatch_log');
        }
    }

    public function start($event, $category = null) {
        if ($this->allowToWork) {
            if ($category) {
                $this->stopWatchEvents[$event] = $this->stopwatch->start($event, $category);
            } else {
                $this->stopWatchEvents[$event] = $this->stopwatch->start($event);
            }            
        }

        return isset($this->stopWatchEvents[$event]) ? $this->stopWatchEvents[$event] : null;
    }

    public function stop($event) {
        if ($this->allowToWork) {
            $this->stopWatchEvents[$event] = $this->stopwatch->stop($event);
        }

        return isset($this->stopWatchEvents[$event]) ? $this->stopWatchEvents[$event] : null;
    }

    public function lap($event) {
        if ($this->allowToWork) {
            $this->stopWatchEvents[$event] = $this->stopwatch->lap($event);
        }
        return isset($this->stopWatchEvents[$event]) ? $this->stopWatchEvents[$event] : null;
    }

    public function exportToWeb() {
        $data = '';

        if ($this->allowToWork) {
            $data = $this->getStats();
        }

        return $data;
    }

    public function exportToFile() {
        if ($this->allowToWork && $this->logFile) {
            $file = fopen($this->logFile, 'a');
            fwrite($file, "\n\n********************\n");
            fwrite($file, 'TIME: ' . date('d/m/Y H:i:s'));
            fwrite($file, $this->getStats("\n"));
            fclose($file);
        }
    }

    private function getStats($lineBreak = '<br/>') {
        $info = 'no stats';

        if (!empty($this->stopWatchEvents)) {
            $info = '';
            foreach ($this->stopWatchEvents as $eventName => $eventStats) {
                $eventData = array(
                            'EVENT : ' . $eventName,
                            'Period: ' . $lineBreak . $this->getPeriods($eventStats, $lineBreak),
                            //'Start: ' . $eventStats->getStartTime(),
                            //'End: ' . $eventStats->getEndTime(),
                            //'Duration: ' . $this->formatTime($eventStats->getDuration()),
                            //'Memory: ' .  $this->formatMemory($eventStats->getMemory())
                        );
                $info .=  $lineBreak . '============='  . $lineBreak;
                $info .= implode($lineBreak . '--'  . $lineBreak, $eventData);
            }
        }

        return $info;
    }



    private function getPeriods($eventStats, $lineBreak = '<br/>') {
        $data = array();

        $periods = $eventStats->getPeriods();
        if (!empty($periods)) {
            foreach($periods as $period) {
                $data[] = implode(' | ', array(
                                                'start: ' . $period->getStartTime(),
                                                'end: ' . $period->getEndTime(),
                                                'duration: ' . $this->formatTime($period->getDuration()),
                                                'memory: ' .  $this->formatMemory($period->getMemory())
                                            )
                            );
            }
        }


        return implode($lineBreak, $data);
    }

    private function formatMemory($value) {
        return number_format($value / (1024*1024)) . ' Mb';
    }

    function formatTime($milliseconds)
    {
        return number_format($milliseconds/1000, 2);
    }
}
