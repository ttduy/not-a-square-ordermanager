<?php
namespace Leep\AdminBundle\Helper;

class TextBlockCache {
    public $container;
    public $directory = '';
    public $idLanguage = 1;
    public function __construct($container, $directory) {
        $this->container = $container;
        $this->directory = $directory;
    }

    public function setLanguage($idLanguage) {
        $this->idLanguage = $idLanguage;
    }

    public function getDirectory() {
        $dirName = $this->directory.DIRECTORY_SEPARATOR.'block_'.$this->idLanguage;
        if (!is_dir($dirName)) {
            mkdir($dirName);
        }
        return $dirName;
    }

    public function getBlockFile($block) {
        return $this->getDirectory().DIRECTORY_SEPARATOR.md5($block);
    }

    public function saveBlock($block, $data) {
        $blockFile = $this->getBlockFile($block);
        file_put_contents($blockFile, $data);
    }

    public function contains($block) {
        $blockFile = $this->getBlockFile($block);
        return is_file($blockFile);
    }

    public function getBlock($block) {
        $blockFile = $this->getBlockFile($block);
        return file_get_contents($blockFile);
    }
}
