<?php
namespace Leep\AdminBundle\Helper;

use App\Database\MainBundle\Entity;

class SessionExtra {
    public $container;
    public $sessionKey = '_msid';
    public function __construct($container) { 
        $this->container = $container;
    }

    public function getSessionKeyFromRequest($requestKey = null) {
        if ($requestKey == null) {
            $requestKey = $this->sessionKey;
        }
        return $this->container->get('request')->get($requestKey, '');
    }

    public function loadSession($sessionKey) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $session = $em->getRepository('AppDatabaseMainBundle:SessionExtra')->findOneBySessionKey($sessionKey);
        if ($session) {
            return @unserialize($session->getSessionValue());
        }
        return array();
    }

    public function generateNewSession() {
        $em = $this->container->get('doctrine')->getEntityManager();
        while (true) {
            $sessionKey = $this->container->get('leep_admin.helper.common')->generateRandString(20);
            $session = $em->getRepository('AppDatabaseMainBundle:SessionExtra')->findOneBySessionKey($sessionKey);
            if (empty($session)) {
                $s = new Entity\SessionExtra();
                $s->setSessionKey($sessionKey);
                $s->setSessionValue(serialize(array()));
                $s->setSessionTime(new \DateTime());
                $em->persist($s);
                $em->flush();

                return $sessionKey;
            }
        }
        return '';
    }

    public function save($k, $v, $sessionKey) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $session = $em->getRepository('AppDatabaseMainBundle:SessionExtra')->findOneBySessionKey($sessionKey);
        if ($session) {
            $data = unserialize($session->getSessionValue());
            $data[$k] = $v;
            $data['uip'] = $_SERVER['REMOTE_ADDR'];
            $session->setSessionValue(serialize($data));
            $em->flush();
        }
    }
    public function get($k, $sessionKey) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $session = $em->getRepository('AppDatabaseMainBundle:SessionExtra')->findOneBySessionKey($sessionKey);
        if ($session) {
            $data = unserialize($session->getSessionValue());
            if (!isset($data['uip'])) {
                return null;
            }
            if (strcmp($data['uip'], $_SERVER['REMOTE_ADDR']) === FALSE) {
                return null;
            }

            return isset($data[$k]) ? $data[$k] : null;
        }
        return null;
    }
}
