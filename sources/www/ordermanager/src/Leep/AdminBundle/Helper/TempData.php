<?php
namespace Leep\AdminBundle\Helper;

class TempData {  
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function get($key, $emptyValue = false) {
        $session = $this->container->get('request')->getSession();

        if ($session->has('temp_data_'.$key)) {
            return $session->get('temp_data_'.$key);
        }
        
        return $emptyValue;
    }

    public function save($key, $data) {
        $session = $this->container->get('request')->getSession();

        $session->set('temp_data_'.$key, $data);
        $session->save();
    }

    public function generateKey() {
        $common = $this->container->get('leep_admin.helper.common');
        $key = $common->generateRandString(16);
        $session = $this->container->get('request')->getSession();
        while ($session->has('temp_data_'.$key)) {
            $key = $common->generateRandString(16);
        }
        return $key;
    }
}
