<?php
namespace Leep\AdminBundle\Helper;

class MenuBuilder {    
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $menus = array();

    public function addMenuItem($id) {
       $item = new MenuType\MenuItem();
       $item->setId($id);

       $this->menus[] = $item;

       return $item;
    }

    public function getMenu() {
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $arrMenu = array();

        foreach ($this->menus as $entry) {            
            $menu = array(
                'link'  => $entry->link,
                'class'  => $entry->class,
                'title'  => $entry->title
            );

            $subMenus = array();
            if (!empty($entry->menuGroupItems)) {
                foreach ($entry->menuGroupItems as $menuGroup) {
                    if (!empty($menuGroup->subMenuItems)) {
                        foreach ($menuGroup->subMenuItems as $subMenuItem) {
                            if ($subMenuItem->permission && $helperSecurity->hasPermission($subMenuItem->permission)) {
                                $subMenus[] = array(
                                    'link'      => $subMenuItem->link,
                                    'title'     => $subMenuItem->title,
                                    'newTab'  => $subMenuItem->isNewTab
                                );
                            }
                        }
                    }
                }
            }

            if (!empty($subMenus)) {
                $menu['childs'] = $subMenus;
                $arrMenu[$entry->id] = $menu;
            }
            else {
                if ($entry->permission) {
                    if ($entry->permission === '*') {
                        $arrMenu[$entry->id] = $menu;
                    }
                    else if ($helperSecurity->hasPermission($entry->permission)) {
                        $arrMenu[$entry->id] = $menu;
                    }
                }
            }
        }

        return $arrMenu;
    }
}
