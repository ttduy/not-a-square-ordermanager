<?php
namespace Leep\AdminBundle\Helper\MenuType;

class MenuGroupItem {
    public $id;
    public $subMenuItems = array();

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function addSubMenuItem($id) {
        $item = new SubMenuItem();
        $item->setId($id);
        $this->subMenuItems[] = $item;
        return $item;
    }
}