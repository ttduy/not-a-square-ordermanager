<?php
namespace Leep\AdminBundle\Helper\MenuType;

class MenuItem {
    public $id;
    public $title;
    public $class;
    public $link;
    public $permission;
    public $menuGroupItems = array();

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }
    public function setClass($class) {
        $this->class = $class;
        return $this;
    }
    public function setPermission($permission) {
        $this->permission = $permission;
        return $this;
    }
    public function addMenuGroupItem($id) {
        $item = new MenuGroupItem();
        $item->setId($id);
        $this->menuGroupItems[] = $item;
        return $item;
    }
}