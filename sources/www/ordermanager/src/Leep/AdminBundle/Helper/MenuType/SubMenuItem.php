<?php
namespace Leep\AdminBundle\Helper\MenuType;

class SubMenuItem {
    public $id;
    public $title;
    public $link;
    public $permission;
    public $isNewTab = false;

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }
    public function setIsNewTab($isNewTab) {
        $this->isNewTab = $isNewTab;
        return $this;
    }
    public function setPermission($permission) {
        $this->permission = $permission;
        return $this;
    }
}