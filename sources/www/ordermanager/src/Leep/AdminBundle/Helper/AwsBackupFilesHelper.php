<?php
namespace Leep\AdminBundle\Helper;

use Leep\AdminBundle\Business;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

class AwsBackupFilesHelper {
    private $container;
    private $s3;

    public function __construct($container) {
        $this->container = $container;

        // Instantiate an S3 client
        $this->s3 = S3Client::factory([
            'credentials' => [
                'key'    => 'AKIAINK6TVPLDQ4R6YQA',
                'secret' => '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',
            ],

            'region' => 'us-east-1'
        ]);
    }

    public function temporaryDownloadBackupFiles($type, $fileName) {
        $tmpDir = $this->container->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
        $folderKey = '';
        $keys = [];
        if ($type == 'report') {
            $keys = ['customer_reports', 'cryo_sample_reports', 'dead_customer_reports'];
        } else if ($type == 'shipment') {
            $keys = ['shipments', 'dead_shipments'];
        }

        foreach ($keys as $key) {
            if ($this->s3->doesObjectExist('ordermanager-disk-backup', "$key/$fileName")) {
                $folderKey = $key;
                break;
            }
        }

        if (empty($folderKey)) {
            return false;
        }

        try {
            $remoteFile = $this->s3->getObject(array(
                'Bucket' => 'ordermanager-disk-backup',
                'Key' =>    "$folderKey/$fileName",
                'SaveAs' => "$tmpDir/$fileName"
            ));

            if (!is_file("$tmpDir/$fileName")) {
                return false;
            }
        } catch (\Exception $e) {
            return "Exception: {$e->getMessage()}.\n";
        }

        return true;
    }
}