<?php
namespace Leep\AdminBundle\Helper;

class Database {    
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function loadList($repository, $filters, $keyField, $valueField) {
        $list = array();

        $result = $this->container->get('doctrine')->getRepository($repository)->findBy($filters);
        $keyMethod = 'get'.ucfirst($keyField);
        $valueMethod = 'get'.ucfirst($valueField);
        foreach ($result as $row) {
            $key = $row->$keyMethod();
            $value = $row->$valueMethod();

            $list[$key] = $value;
        }
        return $list;
    }

    public function delete($em, $repository, $filters) {
        $query = $em->createQueryBuilder();

        $query->delete($repository, 'p');
        foreach ($filters as $k => $v) {
            $query->andWhere('p.'.$k.' = :'.$k)
                  ->setParameter($k, $v);
        }

        $query->getQuery()->execute();
    }
}
