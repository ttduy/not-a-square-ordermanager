<?php
namespace Leep\AdminBundle\Helper;

class Common {
    public $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function humanFileSize($bytes, $decimals = 2) {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    public function removeDirectory($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            $delTarget = "$dir/$file";
            (is_dir($delTarget)) ? self::removeDirectory($delTarget) : unlink($delTarget);
        }
        return rmdir($dir);
    }

    public function arrayToXML($array, &$xml_user_info) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    self::arrayToXML($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    self::arrayToXML($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public function generateRandString($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = substr( str_shuffle( $chars ), 0, $length );
        $size = strlen( $chars );
        for( $i = 0; $i < $length; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }

        return $str;
    }

    public function generateRandPasscode($length = 6) {
        $input = array();
        for ($i = 0; $i < $length; $i++) {
            $input[] = rand(0, 9);
        }

        return implode('', $input);
    }

    /* maximum number of digits generated/appended to generated name is 1 as default */
    public function generateUsername($inputName, $maxGeneratedTime = 1) {
        $inputName = self::createSlug(trim($inputName));
        $newName = '';
        $notFound = true;

        if ($inputName) {
            $newName = str_replace(' ', '', $inputName);

            $counter = 0;
            while($notFound === true && $counter <= $maxGeneratedTime) {
                $randNumber = '';

                if ($counter > 0) {
                    $randNumber = $this->generateRandPasscode($counter);
                }

                $newName .= $randNumber;

                // check redundant
                $p = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneByWebLoginUsername($newName);
                $d = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneByWebLoginUsername($newName);
                $c = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneByWebLoginUsername($newName);

                if (empty($p) && empty($d) && empty($c)) {
                    $notFound = false;
                }

                $counter++;
            }
        }

        // if not found unique name after $maxGeneratedTime, return empty
        return (($notFound) ? '' : $newName);
    }

    public function generateRandAttachmentDir($key = '') {
        $attachmentDir = $this->container->getParameter('attachment_dir').'/'.$key;
        $dir = $this->generateRandString(16);
        while (is_dir($attachmentDir.'/'.$dir)) {
            $dir = $this->generateRandString(16);
        }

        return $dir;
    }

    public function copyEntityToArray($entity) {
        $array = array();
        $entityMethods = get_class_methods($entity);

        if (is_array($entityMethods)) {
            foreach ($entityMethods as $method) {
                if (preg_match('/get(.+)/', $method, $arr)) {
                    if (isset($arr[1])) {
                        $val = $entity->$method();
                        if (is_object($val) && $val instanceof \DateTime) {
                            $val = $val->format('Y-m-d H:i:s');
                        }
                        $array[lcfirst($arr[1])] = $val;
                    }
                }
            }
        }
        return $array;
    }

    public function parseEmaiLList($emailList) {
        $list = explode(';', $emailList);
        $arr = array();
        foreach ($list as $email) {
            $email = trim($email);
            if (!empty($email)) {
                $arr[] = $email;
            }
        }
        return $arr;
    }

    public function getConfig() {
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Config')->findAll();
        foreach ($result as $r) {
            return $r;
        }
        return null;
    }

    public function isExternalLab() {
        if ($this->container->get('leep_admin.helper.security')->hasPermission('cryosaveExternalLab')) {
            return true;
        }
        return false;
    }

    public function getIdLaboratory() {
        if ($this->isExternalLab()) {
            return $this->container->get('leep_admin.helper.security')->userSession->getUser()->getIdLaboratory();
        }
        return $this->getConfig()->getIdNovoLaboratory();
    }

    public function getYourLaboratory() {
        $idLaborary = $this->getIdLaboratory();
        $em = $this->container->get('doctrine')->getEntityManager();

        return $em->getRepository('AppDatabaseMainBundle:CryosaveLaboratory')->findOneById($idLaborary);
    }

    public function isUsernameExisted($username) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $webUser = $em->getRepository('AppDatabaseMainBundle:WebUsers')->findOneByUsername($username);
        $translatorUser = $em->getRepository('AppDatabaseMainBundle:TranslatorUser')->findOneByUsername($username);
        $cryosaveUser = $em->getRepository('AppDatabaseMainBundle:CryosaveUser')->findOneByUsername($username);

        if ($webUser || $translatorUser || $cryosaveUser) {
            return true;
        }
        return false;
    }

    // For reporting format
    public function nl2br($text) {
        return str_replace("\r\n", '##BR##', $text);
    }

    public function isWebLoginUsernameExisted($username, $exceptedEntity = null, $exceptedEntityRecordId = null) {
        if (trim($username) == '') {
            return false;
        }

        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();

        if ($exceptedEntity == 'Partner') {
            $query->select('p')
                ->from('AppDatabaseMainBundle:Partner', 'p')
                ->andWhere('p.id <> :id')
                ->andWhere('p.webLoginUsername = :username')
                ->setParameter('id', $exceptedEntityRecordId)
                ->setParameter('username', $username);
            $p = $query->getQuery()->getResult();
        } else {
            $p = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneByWebLoginUsername($username);
        }

        if ($exceptedEntity == 'DistributionChannels') {
            $query->select('p')
                ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
                ->andWhere('p.id <> :id')
                ->andWhere('p.webLoginUsername = :username')
                ->setParameter('id', $exceptedEntityRecordId)
                ->setParameter('username', $username);
            $d = $query->getQuery()->getResult();
        } else {
            $d = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneByWebLoginUsername($username);
        }


        if ($exceptedEntity == 'CustomerInfo') {
            $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerInfo', 'p')
                ->andWhere('p.id <> :id')
                ->andWhere('p.webLoginUsername = :username')
                ->setParameter('id', $exceptedEntityRecordId)
                ->setParameter('username', $username);
            $c = $query->getQuery()->getResult();
        } else {
            $c = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneByWebLoginUsername($username);
        }

        $blnExist = false;
        if ($p || $d || $c) {
            $blnExist = true;
        }

        return $blnExist;
    }

    public function createSlug($text)
    {
        $text = str_replace(' ', '', trim($text));
        $text = preg_replace('~[^\\pL\d]+~u', '', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);

        return $text;
    }

    public function replaceDoubleBracesTag($placeHolder, $replacement, $subject) {
        $pattern = '/\{\{[&nbsp;|\s]*(' . $placeHolder . ')[&nbsp;|\s]*\}\}/i';
        return preg_replace($pattern, $replacement, $subject);
    }


    public function executeCommand($cmd) {
        $output = array();
        $returnVar = false;
        exec($cmd, $output, $returnVar);

        return $output;
    }

    function xcopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            self::xcopy("$source/$entry", "$dest/$entry", $permissions);
        }

        // Clean up
        $dir->close();
        return true;
    }
}
