<?php
namespace Leep\AdminBundle\Helper;

use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ReportHelper {
    public $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function serveReport($fileDir, $files, $pdfFilename) {
        $tempDir = $this->container->getParameter('kernel.root_dir').'/../files/temp';
        $mainFile = '';
        $count = 0;
        foreach ($files as $file) {
            if ($file != '') {
                $mainFile = $file;
                $count++;
            }
        }

        // No merger needed
        if ($count == 1) {
            $downloadFile = $fileDir.'/'.$mainFile;
            if (!is_file($downloadFile)) {
                $awsHelper = $this->container->get('leep_admin.helper.aws_backup_files_helper');
                $result = $awsHelper->temporaryDownloadBackupFiles('report', $mainFile);
                if ($result) {
                    $downloadFile = "$tempDir/$mainFile";
                }
            }

            $fileResponse = new BinaryFileResponse($downloadFile);
            $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
            $fileResponse->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
            $fileResponse->headers->set('Content-Disposition', 'attachment; filename="'.$pdfFilename.'.pdf"');
            return $fileResponse;
        }

        // Merge needed
        $pdf = $this->container->get('white_october.tcpdf')->create();
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);

        foreach ($files as $file) {
            $downloadFile = $fileDir.'/'.$file;
            if (!is_file($downloadFile)) {
                $awsHelper = $this->container->get('leep_admin.helper.aws_backup_files_helper');
                $result = $awsHelper->temporaryDownloadBackupFiles('report', $file);
                if ($result) {
                    $downloadFile = "$tempDir/$file";
                }
            }

            if (is_file($downloadFile)) {
                $numPage = $pdf->setSourceFile($downloadFile);
                for ($i = 1; $i <= $numPage; $i++) {
                    $template = $pdf->importPage($i);
                    $size = $pdf->getTemplateSize($template);
                    if ($size['w'] < $size['h']) {
                        $pdf->AddPage('P', array($size['w'], $size['h']));
                    } else {
                        $pdf->AddPage('L', array($size['h'], $size['w']));
                    }

                    $pdf->useTemplate($template);
                }
            }
        }

        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename="'.$pdfFilename.'.pdf"');
        $resp->setContent($pdf->Output('', 'S'));

        return $resp;
    }
}
