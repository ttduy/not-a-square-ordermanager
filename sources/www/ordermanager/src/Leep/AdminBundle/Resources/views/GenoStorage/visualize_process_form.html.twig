{% extends 'DesignStarlightBundle:Layout:NormalPage.html.twig' %}

{% block ContentHeader %}
    <link rel="stylesheet" href="{{ asset('bundles/designstarlight/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('bundles/designstarlight/css/style.darkblue.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('bundles/designstarlight/css/visualize_geno_storage.css') }}" type="text/css">
    <!--[if IE 9]>
        <link rel="stylesheet" media="screen" href="{{ asset('bundles/designstarlight/css/ie9.css') }}"/>
    <![endif]-->
    <!--[if IE 8]>
        <link rel="stylesheet" media="screen" href="{{ asset('bundles/designstarlight/css/ie8.css') }}"/>
    <![endif]-->
    <!--[if IE 7]>
        <link rel="stylesheet" media="screen" href="{{ asset('bundles/designstarlight/css/ie7.css') }}"/>
    <![endif]-->

    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/jquery-ui-1.11.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/jquery-migrate-1.2.1.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/jquery.colorbox-min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/custom/general.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/designstarlight/js/custom.js') }}"></script>

    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{ asset('bundles/designstarlight/js/plugins/excanvas.min.js') }}"></script><![endif]-->
    <link rel="stylesheet" media="screen" href="{{ asset('bundles/framework/source/select2-3.4.8/select2.css') }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ asset('bundles/framework/source/select2-3.4.8/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/framework/source/select2-3.4.8/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bundles/framework/js/searchable_cache_box.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('bundles/leepadmin/css/sweet-alert.css') }}" type="text/css">
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/sweet-alert.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/redactor/redactor.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/table/table.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/fontcolor/fontcolor.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/fontfamily/fontfamily.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/fontsize/fontsize.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/fullscreen/fullscreen.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/imagemanager/imagemanager.js')}}"></script>
    <script type="text/javascript" src="{{ asset('bundles/leepadmin/plugin/redactor1025/plugins/textdirection/textdirection.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('bundles/leepadmin/plugin/redactor/redactor.css') }}" type="text/css">
{% endblock %}

{% block CustomHeaderScript %}
    <link rel="stylesheet" href="{{ asset('bundles/leepadmin/css/app_style.css') }}" type="text/css">
{% endblock %}


{% block MainContent %}
<div class="content">
<!-- START OF MAIN CONTENT -->
    <div class='mainwrapper'>
        <div class='mainwrapperinner'>
            {% block header %}
                <head>
                    <div class="contenttitle radiusbottom0">
                        <h2 class="table"><span>Visualize geno storages status</span></h2>
                    </div>
                <head>
            {% endblock %}

            {% block content %}
                <br>
                <br>
                <div class="scroll">
                    <table class="drawTable">
                        <thead>
                            <tr>
                                {% for genoStorage in genoStorages %}
                                    <th class="thStorageTable">
                                        {{ genoStorage['storageName'] }}
                                    </th>
                                {% endfor %}
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                {% for genoStorage in genoStorages %}
                                    {% set cellList = genoStorage['cells'] %}
                                    {% if cellList %}
                                        <td class="rawList">
                                            {% for cell in cellList %}
                                                {% set cellLoading = cell['loading'] %}
                                                {% set warningThreshold = cell['warningThreshold'] %}
                                                {% set cellName = cell['name'] %}
                                                <li class="rawName">{{ cellName }}</li>
                                                {% if cellLoading <= 0 %}
                                                    <p class="fillEmpty"></p>
                                                {% elseif cellLoading > warningThreshold %}
                                                    {% if cell['numberGenoMaterialContain'] < 3 %}
                                                        <div class="fillHardDefault">
                                                            {% set batchs = cell['genoMaterialsContain'] %}
                                                            {% for batch in batchs %}
                                                                <p>
                                                                    {{ batch }}
                                                                </p>
                                                            {% endfor %}
                                                        </div>
                                                    {% else %}
                                                        <div class="fillHardFit">
                                                            {% set batchs = cell['genoMaterialsContain'] %}
                                                            {% for batch in batchs %}
                                                                <p>
                                                                    {{ batch }}
                                                                </p>
                                                            {% endfor %}
                                                        </div>
                                                    {% endif %}
                                                {% else %}
                                                    {% if cell['numberGenoMaterialContain'] < 3 %}
                                                        <div class="fillMediumDefault">
                                                            {% set batchs = cell['genoMaterialsContain'] %}
                                                            {% for batch in batchs %}
                                                                <p>
                                                                    {{ batch }}
                                                                </p>
                                                            {% endfor %}
                                                        </div>
                                                    {% else %}
                                                        <div class="fillMediumFit">
                                                            {% set batchs = cell['genoMaterialsContain'] %}
                                                            {% for batch in batchs %}
                                                                <p>
                                                                    {{ batch }}
                                                                </p>
                                                            {% endfor %}
                                                        </div>
                                                    {% endif %}
                                                {% endif %}
                                            {% endfor %}
                                        </td>
                                    {% else %}
                                        <td class="emptyGenoStorage">Empty Geno Storage</td>
                                    {% endif %}
                                {% endfor %}
                            </tr>
                        </tbody>
                    </table>
                 </div> <!-- end of scroll -->
            {% endblock %}
        </div><!--mainwrapper inner -->
    </div><!--mainwrapper-->
    </div>
<!-- END OF MAIN CONTENT -->
{% endblock %}
