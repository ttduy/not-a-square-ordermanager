<?php
namespace Leep\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class ImgTemplateGenerator extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'img_template_generator';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $moduleManager = $this->container->get('easy_module.manager');
        $view->vars['uploadUrl'] = $moduleManager->getUrl('leep_admin', 'email_mass_sender', 'feature', 'uploadImage');
    }
}