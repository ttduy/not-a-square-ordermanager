<?php
namespace Leep\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class FileAttachment extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'key' =>            'default',
            'snapshot' =>       true,
            'mode' =>           'normal',
            'allowCloneDir' =>  false
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'file_attachment';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        // Ensure attachment directory
        $attachmentDir = $this->container->getParameter('attachment_dir').'/'.$options['key'];
        if (!is_dir($attachmentDir)) {
            @mkdir($attachmentDir);
        }

        $moduleManager = $this->container->get('easy_module.manager');
        $view->vars['attachmentUrl'] = $moduleManager->getUrl('leep_admin', 'attachment', 'attachment', 'elfinder', array('key' => $options['key'], 'mode' => $options['mode'], 'allowCloneDir' => $options['allowCloneDir']));
        $view->vars['key'] = $options['key'];
        $view->vars['capturePageUrl'] = $moduleManager->getUrl('leep_admin', 'customer', 'customer', 'showCapture', array('key' => $options['key']));
        $view->vars['snapshot'] = $options['snapshot'];
        $view->vars['showAllImageUrl'] = $moduleManager->getUrl('leep_admin', 'customer', 'customer', 'showAllImages', array('key' => $options['key']));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
    }
}