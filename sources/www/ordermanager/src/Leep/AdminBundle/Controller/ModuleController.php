<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleController extends Controller
{   
    public function handleAction($module = null, $feature = null, $action = null) {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', $module);
        $resp = $module->handle($this, $feature, $action);

        return $resp;
    }
}
