<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FeedbackController extends Controller
{   
    public function indexAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'feedback_question');
        return $module->handle($this, 'feature', 'form');
    }

    public function submitAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'feedback_question');
        return $module->handle($this, 'feature', 'formSubmit');
    }
}
