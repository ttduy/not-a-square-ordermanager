<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PublicController extends Controller
{   
    public function accessReportAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'public_customer_report');
        return $module->handle($this, 'feature', 'accessReport');
    }

    public function yourReportAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'public_customer_report');
        return $module->handle($this, 'feature', 'viewYourReport');
    }

    public function disableAccessLinkAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'public_customer_report');
        return $module->handle($this, 'feature', 'customerDisableAccessKey');
    }

    public function downloadReportAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'public_customer_report');
        return $module->handle($this, 'feature', 'downloadReport');
    }
    
    public function downloadShipmentAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'public_customer_report');
        return $module->handle($this, 'feature', 'downloadShipment');
    }
}
