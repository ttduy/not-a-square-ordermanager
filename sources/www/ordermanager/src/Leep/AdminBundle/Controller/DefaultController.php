<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{   
    public function indexAction() {
        $manager = $this->get('easy_module.manager');

        $helperSecurity = $this->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('strategyPlanningToolView')) {
        	return $this->redirect($manager->getUrl('leep_admin', 'todo_task', 'feature', 'dashboard'));
        }
        return $this->redirect($manager->getUrl('leep_admin', 'dashboard', 'dashboard', 'list'));    
    }
}
