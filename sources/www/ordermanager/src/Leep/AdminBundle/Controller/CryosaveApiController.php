<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CryosaveApiController extends Controller
{   
    public function createSampleAction() {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('leep_admin', 'cryosave_sample');
        return $module->handle($this, 'feature', 'apiCreateSample');
    }
}
