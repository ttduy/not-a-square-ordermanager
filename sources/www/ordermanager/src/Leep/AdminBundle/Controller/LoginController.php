<?php

namespace Leep\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Security\Core\SecurityContext;

class LoginController extends Controller
{   
    public function showAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $formErrors = array();
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $formError = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $formError = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }        
        
        $data = array(
            'pageTitle' => 'Login',
            'form_url' => $this->generateUrl('Security_LoginCheck'),
            'form_error' => $formError
        );
        return $this->render('LeepAdminBundle:Login:show.html.twig', $data);
    }
}
