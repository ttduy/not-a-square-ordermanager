<?php
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;

class Constants {
    const ENTRY_DIRECTION_MINUS = 1;
    const ENTRY_DIRECTION_PLUS  = 2;

    public static function getAccountEntryDirection() {
        return array(
            self::ENTRY_DIRECTION_MINUS   => 'Minus',
            self::ENTRY_DIRECTION_PLUS    => 'Plus'
        );
    }
}