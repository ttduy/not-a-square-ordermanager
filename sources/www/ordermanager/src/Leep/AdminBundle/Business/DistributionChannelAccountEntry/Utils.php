<?php
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;

class Utils {
    public static function updateDistributionChannelAccount($container, $id) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);

        $query = $em->createQueryBuilder();
        $query->select('p.idDirection, SUM(p.amount) AS totalAmount')
            ->from('AppDatabaseMainBundle:DistributionChannelAccountEntry', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id)
            ->groupBy('p.idDirection');
        $results = $query->getQuery()->getResult();

        $totalAmount = 0;
        foreach ($results as $r) {
            $idDirection = $r['idDirection'];
            $amount = $r['totalAmount'];
            if ($idDirection == Constants::ENTRY_DIRECTION_MINUS) {
                $totalAmount -= $amount;
            }
            else {
                $totalAmount += $amount;
            }
        }

        if ($dc) {
            $dc->setAccountBalance($totalAmount);
            $em->persist($dc);
            $em->flush();
        }
    }
}