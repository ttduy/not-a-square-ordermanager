<?php
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannelAccountEntry', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->entryDate = $entity->getEntryDate();
        $model->idDirection = $entity->getIdDirection();
        $model->amount = $entity->getAmount();
        $model->entryNote = $entity->getEntryNote();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('entryDate', 'datepicker', array(
            'label'    => 'Entry date',
            'required' => false
        ));
        $builder->add('idDirection', 'choice', array(
            'label'    => 'Direction',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_AccountEntry_Direction')
        ));
        $builder->add('amount', 'text', array(
            'label'    => 'Amount',
            'required' => false
        ));
        $builder->add('entryNote', 'textarea', array(
            'label'    => 'Entry note',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));   

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        
        
        $this->entity->setEntryDate($model->entryDate);
        $this->entity->setIdDirection($model->idDirection);
        $this->entity->setAmount($model->amount);
        $this->entity->setEntryNote($model->entryNote);
        $em->persist($this->entity);
        $em->flush();

        Utils::updateDistributionChannelAccount($this->container, $this->entity->getIdDistributionChannel());

        parent::onSuccess();
    }
}