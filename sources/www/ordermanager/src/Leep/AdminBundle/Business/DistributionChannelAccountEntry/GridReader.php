<?php
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('entryDate', 'entryNote', 'minus', 'plus', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Entry date',   'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Note',         'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Minus',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Plus',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',       'width' => '15%', 'sortable' => 'false'),
        );
    }
    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.entryDate';
        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->addOrderBy('p.id', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_DistributionChannelAccountEntryGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannelAccountEntry', 'p');
        $idDistributionChannel = $this->container->get('request')->get('idParent', 0);
        $queryBuilder->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $idDistributionChannel);
            
        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.entryDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }
        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.entryDate <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }
        if (!empty($this->filters->entryNote)) {
            $queryBuilder->andWhere('p.entryNote LIKE :entryNote')
                ->setParameter('entryNote', '%'.trim($this->filters->entryNote).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $idParent = $this->container->get('request')->get('idParent', 0);

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel_account_entry', 'edit', 'edit', array('idParent' => $idParent, 'id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'distribution_channel_account_entry', 'delete', 'delete', array('idParent' => $idParent, 'id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellPlus($row) {
        if ($row->getIdDirection() == Constants::ENTRY_DIRECTION_PLUS) {
            return '&euro;'.$row->getAmount();
        }
        return '';
    }
    public function buildCellMinus($row) {
        if ($row->getIdDirection() == Constants::ENTRY_DIRECTION_MINUS) {
            return '&euro;'.$row->getAmount();
        }
        return '';
    }
}