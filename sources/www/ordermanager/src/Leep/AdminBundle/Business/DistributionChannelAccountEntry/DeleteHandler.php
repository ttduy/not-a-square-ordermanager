<?php 
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $entry = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannelAccountEntry')->findOneById($id);
        $idDistributionChannel = $entry->getIdDistributionChannel();
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($entry);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        Utils::updateDistributionChannelAccount($this->container, $idDistributionChannel);

        return new RedirectResponse($mgr->getUrl('leep_admin', 'distribution_channel_account_entry', 'grid', 'list', array('idParent' => $idDistributionChannel)));
    }
}