<?php
namespace Leep\AdminBundle\Business\DistributionChannelAccountEntry;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $entryDate;
    public $idDirection;
    public $amount;
    public $entryNote;
}
