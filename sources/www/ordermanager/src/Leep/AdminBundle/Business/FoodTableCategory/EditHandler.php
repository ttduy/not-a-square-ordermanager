<?php
namespace Leep\AdminBundle\Business\FoodTableCategory;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableCategory', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model->name);

        parent::onSuccess();
    }
}