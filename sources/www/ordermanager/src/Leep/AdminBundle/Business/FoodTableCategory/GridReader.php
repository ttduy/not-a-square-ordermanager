<?php
namespace Leep\AdminBundle\Business\FoodTableCategory;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FoodTableCategory', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('foodTableModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'food_table_category', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'food_table_category', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}