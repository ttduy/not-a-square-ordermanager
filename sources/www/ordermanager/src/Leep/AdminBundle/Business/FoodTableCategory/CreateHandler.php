<?php
namespace Leep\AdminBundle\Business\FoodTableCategory;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $category = new Entity\FoodTableCategory();
        $category->setName($model->name);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($category);
        $em->flush();
        
        parent::onSuccess();
    }
}