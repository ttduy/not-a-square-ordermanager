<?php
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start date',
            'required' => false
        ));
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End date',
            'required' => false
        ));
        $builder->add('entryNote', 'text', array(
            'label'    => 'Entry note',
            'required' => false
        ));

        return $builder;
    }
}