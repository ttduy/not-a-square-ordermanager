<?php
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $entryDate;
    public $idDirection;
    public $amount;
    public $entryNote;
}
