<?php
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $entryDate;
    public $idDirection;
    public $amount;
    public $entryNote;
}
