<?php
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Leep\AdminBundle\Business;

class Utils {
    public static function updatePartnerAccount($container, $id) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);

        $query = $em->createQueryBuilder();
        $query->select('p.idDirection, SUM(p.amount) AS totalAmount')
            ->from('AppDatabaseMainBundle:PartnerAccountEntry', 'p')
            ->andWhere('p.idPartner = :idPartner')
            ->setParameter('idPartner', $id)
            ->groupBy('p.idDirection');
        $results = $query->getQuery()->getResult();

        $totalAmount = 0;
        foreach ($results as $r) {
            $idDirection = $r['idDirection'];
            $amount = $r['totalAmount'];
            if ($idDirection == Business\DistributionChannelAccountEntry\Constants::ENTRY_DIRECTION_MINUS) {
                $totalAmount -= $amount;
            }
            else {
                $totalAmount += $amount;
            }
        }

        if ($partner) {
            $partner->setAccountBalance($totalAmount);
            $em->persist($partner);
            $em->flush();
        }
    }
}