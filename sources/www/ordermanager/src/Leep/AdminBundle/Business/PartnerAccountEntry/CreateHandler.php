<?php
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->entryDate = new \DateTime();
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('entryDate', 'datepicker', array(
            'label'    => 'Entry date',
            'required' => false
        ));
        $builder->add('idDirection', 'choice', array(
            'label'    => 'Direction',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_AccountEntry_Direction')
        ));
        $builder->add('amount', 'text', array(
            'label'    => 'Amount',
            'required' => false
        ));
        $builder->add('entryNote', 'textarea', array(
            'label'    => 'Entry note',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $idPartner = $this->container->get('request')->get('idParent');
        
        $entry = new Entity\PartnerAccountEntry();
        $entry->setIdPartner($idPartner);
        $entry->setEntryDate($model->entryDate);
        $entry->setIdDirection($model->idDirection);
        $entry->setAmount($model->amount);
        $entry->setEntryNote($model->entryNote);
        $em->persist($entry);
        $em->flush();

        Utils::updatePartnerAccount($this->container, $idPartner);

        parent::onSuccess();
    }
}
