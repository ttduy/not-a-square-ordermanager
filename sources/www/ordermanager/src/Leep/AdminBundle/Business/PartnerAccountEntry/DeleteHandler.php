<?php 
namespace Leep\AdminBundle\Business\PartnerAccountEntry;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $entry = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PartnerAccountEntry')->findOneById($id);
        $idPartner = $entry->getIdPartner();
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($entry);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        Utils::updatePartnerAccount($this->container, $idPartner);

        return new RedirectResponse($mgr->getUrl('leep_admin', 'partner_account_entry', 'grid', 'list', array('idParent' => $idPartner)));
    }
}