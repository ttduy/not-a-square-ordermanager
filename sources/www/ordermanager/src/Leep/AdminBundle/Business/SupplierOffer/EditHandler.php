<?php  
namespace Leep\AdminBundle\Business\SupplierOffer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public $fileKey = false;

    public function generateFileKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->fileKey = $form['fileKey'];
        }
        if ($this->fileKey == '') {
            $this->fileKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:SupplierOffer')
            ->findOneById($id);
    }

    public function convertToFormModel($supplierOffer) {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $model = new EditModel();

        if ($supplierOffer->getFileKey() == '') {
            $this->generateFileKey();
            $model->fileKey = $this->fileKey;
            $supplierOffer->setFileKey($this->fileKey);
            $em->flush();
        }
 
        $model->fileKey = $supplierOffer->getFileKey();
        $this->fileKey = $supplierOffer->getFileKey();

        $model->date = $supplierOffer->getDate();
        $model->file = [];
        $model->file['file'] = $supplierOffer->getFile();
        $model->supplier = $supplierOffer->getSupplier();
        $model->materials = [];
        $result = $this->container->get('doctrine')
                                  ->getRepository('AppDatabaseMainBundle:SupplierOfferMaterial')
                                  ->findByIdSupplierOffer($supplierOffer->getId());
        foreach ($result as $r) {
            $model->materials[] = $r->getIdMaterial();
        }                                  
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('date', 'datepicker', array(
            'label'     => 'Date',
            'required'  => false
        ));
        $builder->add('fileKey', 'hidden');
        $builder->add('file', 'file_attachment', array(
            'label'    => 'File',
            'required' => false,
            'key'      => 'supplier_offer/'.$this->fileKey,
            'snapshot' => false
        ));
        $builder->add('supplier', 'text', array(
            'label'     => 'Supplier',
            'required'  => true
        ));
        $builder->add('materials', 'choice', array(
            'label'    => 'Material',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'    => 'height: 250px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Material_List')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $this->entity->setDate($model->date);
        $this->entity->setSupplier($model->supplier);
        $em->persist($this->entity);
        $em->flush();
        $dbHelper = $em->createQueryBuilder();        
        $dbHelper->delete('AppDatabaseMainBundle:SupplierOfferMaterial', 'som')
            ->andWhere('som.idSupplierOffer IN (:idSupplierOffer)')
            ->setParameter('idSupplierOffer', $this->entity->getId())->getQuery()->execute();

        foreach ($model->materials as $idMaterial) {
            $supplierOfferMaterial = new Entity\SupplierOfferMaterial();
            $supplierOfferMaterial->setIdSupplierOffer($this->entity->getId());
            $supplierOfferMaterial->setIdMaterial($idMaterial);
            $em->persist($supplierOfferMaterial);
        }        
        $em->flush();        
        parent::onSuccess();
    }
}
