<?php  
namespace Leep\AdminBundle\Business\SupplierOffer;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;

    public function getColumnMapping() {
        return array('date', 'supplier', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Date', 'width' => '15%', 'sortable' => 'false'),            
            array('title' => 'Supplier', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Action', 'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_SupplierOfferGridReader');
    }

    public function buildQuery($query) {
        $query->select('p')
              ->from('AppDatabaseMainBundle:SupplierOffer', 'p');
        if (!empty($this->filters->date)) {
            $query->andWhere('p.date = :date')
            ->setParameter('date', $this->filters->date);
        }
        if ($this->filters->supplier != '') {
            $query->andWhere('p.supplier = :supplier')
            ->setParameter('supplier', $this->filters->supplier);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $security = $this->container->get('leep_admin.helper.security');

        if ($security->hasPermission('supplierOfferModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'supplier_offer', 'edit', 'edit', array(
                'id'    => $row->getId()
            )), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'supplier_offer', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
?>