<?php  
namespace Leep\AdminBundle\Business\SupplierOffer;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public $fileKey = false;

    public function generateFileKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->fileKey = $form['fileKey'];
        }
        if ($this->fileKey === false) {
            $this->fileKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function getDefaultFormModel(){
        $this->generateFileKey();

        $model = new CreateModel();
        $model->date = new \DateTime();        
        $model->fileKey = $this->fileKey;

        return $model;
    }

    public function buildForm($builder){
        $mapping = $this->container->get('easy_mapping');
        $builder->add('date', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));
        $builder->add('fileKey', 'hidden');
        $builder->add('file', 'file_attachment', array(
            'label'    => 'File',
            'required' => false,
            'key'      => 'supplier_offer/'.$this->fileKey,
            'snapshot' => false
        ));
        $builder->add('supplier', 'text', array(
            'label'    => 'Supplier',
            'required' => true
        ));
        $builder->add('materials', 'choice', array(
            'label'    => 'Material',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'    => 'height: 250px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Material_List')
        ));
    }

    public function onSuccess(){
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $supplierOffer = new Entity\SupplierOffer();
        $supplierOffer->setDate($model->date);
        $supplierOffer->setSupplier($model->supplier);
        $supplierOffer->setFileKey($model->fileKey);
        $em->persist($supplierOffer);
        $em->flush();

        foreach ($model->materials as $idMaterial) {
            $supplierOfferMaterial = new Entity\SupplierOfferMaterial();
            $supplierOfferMaterial->setIdSupplierOffer($supplierOffer->getId());
            $supplierOfferMaterial->setIdMaterial($idMaterial);
            $em->persist($supplierOfferMaterial);
        }
        $em->flush();

        parent::onSuccess();
    }
}
?>