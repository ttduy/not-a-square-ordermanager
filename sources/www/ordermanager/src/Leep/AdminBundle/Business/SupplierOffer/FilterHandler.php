<?php  
namespace Leep\AdminBundle\Business\SupplierOffer;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('date', 'datepicker', array(
            'label'    => 'Date',
            'required' => false            
        ));
        $builder->add('supplier', 'text', array(
            'label'    => 'Supplier',
            'required' => false
        ));
    }
}
?>