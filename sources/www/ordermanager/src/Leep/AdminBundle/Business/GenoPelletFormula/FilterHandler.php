<?php
namespace Leep\AdminBundle\Business\GenoPelletFormula;

use Leep\AdminBundle\Business\Base\AppFilterHandler;
use Leep\AdminBundle\Business;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', [
            'label' =>      'Name',
            'required' =>   false
        ]);

        $builder->add('content', 'text', [
            'label' =>      'Formula contain',
            'required' =>   false
        ]);

        $builder->add('substances', 'choice', array(
            'label'    => 'Contain substances',
            'required' => false,
            'choices'  => Business\GenoMaterial\Utils::getSubstanceList($this->container),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        return $builder;
    }
}