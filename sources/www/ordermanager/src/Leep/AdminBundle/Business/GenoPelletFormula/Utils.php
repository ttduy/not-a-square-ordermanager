<?php
namespace Leep\AdminBundle\Business\GenoPelletFormula;

class Utils {

    public static function parsePelletContent($container, $inputStr) {
        $em = $container->get('doctrine')->getEntityManager();
        $input = explode('/', $inputStr);

        // Check required length
        if (count($input) < 3) {
            return 'Missing pellet content.';
        }

        // Check if missing data
        if (count($input) % 2 == 0) {
            return 'Missing substance amount.';
        }

        // Parse common value
        $result = [];
        $result['type'] = trim($input[0]);
        $result['lot'] = trim($input[1]);
        $result['total'] = explode('TOTAL', trim($input[2]));

        if (count($result['total']) != 2 || $result['total'][0] != "") {
            return 'Invalid Total value!';
        } else {
            $result['total'] = $result['total'][1];
        }

        // Parse recipe
        $result['recipe'] = [];
        $result['critical'] = [];
        for ($index = 3; $index < count($input); $index+=2) {
            $substance = trim($input[$index]);
            $amount = trim($input[$index + 1]);

            // Check if substance exist
            $check = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findOneByShortName($substance);
            if (!$check) {
                return "Substance: $substance not exist!";
            }

            // Substance amount must be larger than zero
            if (floatval($amount) <= 0) {
                return "Subtance ($substance) has zero amount.";
            }

            // Add to recipe list
            $result['recipe'][$substance] = $amount;

            // Check if this is an critical substance
            if ($check->getIsCriticalSubstance()) {
                $result['critical'][] = $substance;
            }
        }

        return $result;
    }
}