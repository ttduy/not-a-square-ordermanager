<?php
namespace Leep\AdminBundle\Business\GenoPelletFormula;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletFormulas')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->content = $entity->getContent();
        $model->name = $entity->getName();
        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true,
        ));

        $builder->add('content', 'textarea', array(
            'label'    => 'Code',
            'attr'     => array(
                'rows'        => 30,
                'cols'        => 70
            ),
            'required' => true,
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $name = trim($model->name);
        if (empty($name)) {
            $this->errors[] = "Invalid empty name!";
            return false;
        } else {
            $check = $em->getRepository('AppDatabaseMainBundle:GenoPelletFormulas')->findOneByName($name);
            if ($check && $check->getId() != $this->entity->getId()) {
                $this->errors[] = "Formula name: $name already exist!";
                return false;
            }
        }

        // From user content, parse pellet recipe
        $content = Utils::parsePelletContent($this->container, $model->content);
        if (gettype($content) == 'string') {
            $this->errors[] = $content;
            return false;
        }

        // Save to database
        $this->entity->setName($name);
        $this->entity->setContent($model->content);
        $this->entity->setModifiedDate(new \DateTime());
        $this->entity->setModifiedBy($userManager->getUser()->getId());

        parent::onSuccess();
        $this->reloadForm();
    }
}
