<?php
namespace Leep\AdminBundle\Business\GenoPelletFormula;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('createdDate', 'createdBy', 'modifiedDate', 'modifiedBy', 'name', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Created date',    'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Created by',      'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Modified date',   'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Modified by',     'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Name',            'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',          'width' => '20%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GenoPelletFormulaGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoPelletFormulas', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->content) != '') {
            $queryBuilder->andWhere('p.content LIKE :content')
                ->setParameter('content', '%'.trim($this->filters->content).'%');
        }

        $substanceNameList = Business\GenoMaterial\Utils::getSubstanceList($this->container);
        if (!empty($this->filters->substances)) {
            $substances = array_map('trim', $this->filters->substances);
            foreach ($substances as $substanceId) {
                $substance = $substanceNameList[$substanceId];
                $queryBuilder->andWhere("p.content LIKE '%/$substance/%'");
            }
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoPelletFormulaModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_pellet_formula', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_pellet_formula', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
