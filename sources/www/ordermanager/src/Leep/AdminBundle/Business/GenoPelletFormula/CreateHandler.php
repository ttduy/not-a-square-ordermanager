<?php
namespace Leep\AdminBundle\Business\GenoPelletFormula;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true,
        ));

        $builder->add('content', 'textarea', array(
            'label'    => 'Code',
            'attr'     => array(
                'rows'        => 30,
                'cols'        => 70
            ),
            'required' => true,
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $name = trim($model->name);
        if (empty($name)) {
            $this->errors[] = "Invalid empty name!";
            return false;
        } else {
            $check = $em->getRepository('AppDatabaseMainBundle:GenoPelletFormulas')->findOneByName($name);
            if ($check) {
                $this->errors[] = "Formula name: $name already exist!";
                return false;
            }
        }

        // From user content, parse pellet recipe
        $content = Utils::parsePelletContent($this->container, $model->content);
        if (gettype($content) == 'string') {
            $this->errors[] = $content;
            return false;
        }

        // Save to database
        $genoPellet = new Entity\GenoPelletFormulas();
        $genoPellet->setName($name);
        $genoPellet->setContent($model->content);
        $genoPellet->setCreatedDate(new \DateTime());
        $genoPellet->setCreatedBy($userManager->getUser()->getId());
        $genoPellet->setModifiedDate(new \DateTime());
        $genoPellet->setModifiedBy($userManager->getUser()->getId());

        $em->persist($genoPellet);
        $em->flush();

        parent::onSuccess();
    }
}