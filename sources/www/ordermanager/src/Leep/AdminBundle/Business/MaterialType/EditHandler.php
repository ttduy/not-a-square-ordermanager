<?php
namespace Leep\AdminBundle\Business\MaterialType;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MatType', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->matType = $entity->getMatType();;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('matType',            'text', array(
            'label' => 'Material Type',
            'required' => true
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setMatType($model->matType);
        
        parent::onSuccess();
    }
}
