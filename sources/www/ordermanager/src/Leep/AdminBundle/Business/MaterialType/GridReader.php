<?php
namespace Leep\AdminBundle\Business\MaterialType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('matType', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Material Type',     'width' => '70%', 'sortable' => 'true'),
            array('title' => 'Action',            'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:MatType', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('materialModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'material_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'material_type', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }
}
