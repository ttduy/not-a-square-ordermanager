<?php
namespace Leep\AdminBundle\Business\MaterialType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('matType', 'text', array(
            'label'    => 'Material Type',
            'required' => true
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $matType = new Entity\MatType();
        $matType->setMatType($model->matType);

        $em->persist($matType);
        $em->flush();

        parent::onSuccess();
    }
}
