<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $creationDate;
    public $idCategory;
    public $idType;
    public $idContinualImprovement;
    public $idGoal;
    public $idWebUser;
    public $idMeeting;
    public $name;    
    public $description;    
    public $status;
}
