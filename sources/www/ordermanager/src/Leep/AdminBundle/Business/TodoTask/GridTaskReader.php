<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Easy\CrudBundle\Grid\StaticGridDataReader;

class GridTaskReader extends StaticGridDataReader {
    public $filters;
    public $container;
    public $formatter;
    public $colorCodes = array();
    public function __construct($container) {
        $this->container = $container;
        $this->formatter = $this->container->get('leep_admin.helper.formatter');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',           'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Age',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Assignee',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Color'   ,       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',         'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function initialize() {
        $paramter = $this->container->get('leep_admin.helper.common')->getConfig();
        $codes = explode("\n", $paramter->getStrategyPlanningColorCode());
        $this->colorCodes = array();
        $this->colorCodes[] = '';
        foreach ($codes as $code) {
            if (trim($code) != '') {
                $this->colorCodes[] = trim($code);
            }
        }
    }

    public function setData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $this->initialize();

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $status = $this->container->get('request')->get('status', 0);

        $this->data = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:TodoCategory', 'p');
        if (!empty($this->filters->categories)) {
            $query->andWhere('p.id IN (:categories)')
                ->setParameter('categories', $this->filters->categories);
        }
        $categories = $query->getQuery()->getResult();
        foreach ($categories as $category) {
            $this->data[] = array(
                '<b style="color:#3b4650">'.$category->getName().'</b>',
                '', '', '',
                $this->getCategoryActions($category)
            );

            // Search tasks
            $query = Utils::buildTaskQuery($this->container, $this->filters, $status);
            $query->andWhere('p.idCategory = '.$category->getId());
            $tasks = $query->getQuery()->getResult();
            
            foreach ($tasks as $task) {
                $this->data[] = array(
                    '<div style="margin-left: 40px">'.$this->getTaskDescription($task).'</div>',
                    $this->getAge($task->getCreationDate()),
                    $this->formatter->format($task->getIdWebUser(), 'mapping', 'LeepAdmin_WebUser_List'),
                    $this->getColorCell($task),
                    $this->getTaskActions($task)
                );
            }
        }
    }

    private function getAge($date) {
        if (!empty($date)) {
            $today = new \DateTime();
            $diff = $today->getTimestamp() - $date->getTimestamp();
            return intval($diff / 86400).' day(s)';
        }
        return '';
    }

    private function getTaskDescription($task) {
        $html = '<b>'.$task->getName().'</b><br/>';
        if (trim($task->getDescription()) != '') {
            $html .= nl2br($task->getDescription()).'<br/>';
        }

        if ($task->getIdGoal() != 0) {
            $html .= '<i>'.$this->formatter->format($task->getIdGoal(), 'mapping', 'LeepAdmin_TodoGoal_List').'</i>';
        }
        else {
            $html .= '<i>'.$this->formatter->format($task->getIdContinualImprovement(), 'mapping', 'LeepAdmin_TodoContinualImprovement_List').'</i>';   
        }
        
        if ($task->getIdMeeting() != 0) {
            $html .= '<br/><i>'.$this->formatter->format($task->getIdMeeting(), 'mapping', 'LeepAdmin_Meeting_List').'</i>';
        }
        $html .= '<input type="hidden" class="task_color" color-code="'.$task->getColor().'" />';
        return $html;
    }

    private function getTaskActions($task) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');
        $status = $this->container->get('request')->get('status', 0);

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('strategyPlanningToolModify')) {
            $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'todo_task', 'edit', 'edit', array('id' => $task->getId())), 'button-edit');

            switch ($status) {
                case Constants::STATUS_ACTIVE:
                    $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'completeTask', array('id' => $task->getId()));
                    $href = "javascript:callTaskAjax('".$href."')";
                    $builder->addButton('Complete', $href, 'button-checked');

                    $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'deleteTask', array('id' => $task->getId()));
                    $href = "javascript:callTaskAjax('".$href."')";
                    $builder->addButton('Delete', $href, 'button-delete');

                    break;

                case Constants::STATUS_COMPLETED:
                    $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'redoTask', array('id' => $task->getId()));
                    $href = "javascript:callTaskAjax('".$href."')";
                    $builder->addButton('Mark un-completed', $href, 'button-redo');

                    $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'deleteTask', array('id' => $task->getId()));
                    $href = "javascript:callTaskAjax('".$href."')";
                    $builder->addButton('Delete', $href, 'button-delete');
                    break;
                case Constants::STATUS_DELETED:
                    $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'redoTask', array('id' => $task->getId()));
                    $href = "javascript:callTaskAjax('".$href."')";
                    $builder->addButton('Mark un-completed', $href, 'button-redo');
                    break;
            }
        }

        return $builder->getHtml();
    }


    private function getCategoryActions($category) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('strategyPlanningToolModify')) {
            $builder->addPopupButton('Add', $mgr->getUrl('leep_admin', 'todo_task', 'create', 'create', array('idCategory' => $category->getId())), 'button-add');
        }

        return $builder->getHtml();
    }

    private function getColorCell($task) {
        $colors = array();
        $mgr = $this->container->get('easy_module.manager');

        foreach ($this->colorCodes as $code) {
            $href = $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'setColor', array('id' => $task->getId(), 'color' => trim($code, '#')));
            $href = "javascript:callTaskAjax('".$href."')";
            $colors[] = '<a href="'.$href.'"><div style="display: inline-block; width: 30px; height: 20px; background-color: '.$code.'; border: 1px solid"></div></a>';
        }

        return implode('&nbsp;', $colors);
    }
}
