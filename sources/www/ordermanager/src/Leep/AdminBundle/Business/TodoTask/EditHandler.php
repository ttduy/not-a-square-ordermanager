<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TodoTask', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->creationDate = $entity->getCreationDate();
        $model->idCategory = $entity->getIdCategory();
        $model->idType = ($entity->getIdGoal() != 0) ? Constants::TYPE_GOAL : Constants::TYPE_CONTINUAL_IMPROVEMENT; 
        $model->idGoal = $entity->getIdGoal();
        $model->idContinualImprovement = $entity->getIdContinualImprovement();
        $model->idWebUser = $entity->getIdWebUser();
        $model->idMeeting = $entity->getIdMeeting();
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();
        $model->status = $entity->getStatus();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Goal type',
            'required' => true,
            'choices'  => Constants::getTaskGoalTypes()
        ));
        $builder->add('idContinualImprovement', 'choice', array(
            'label'    => 'Continual improvement',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoContinualImprovement_List')
        ));
        $builder->add('idGoal', 'choice', array(
            'label'    => 'Goal',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_List')
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoCategory_List')
        ));
        $builder->add('idWebUser', 'choice', array(
            'label'    => 'Assign to',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'     => 6,
                'cols'     => 80
            )
        ));
        $builder->add('idMeeting', 'choice', array(
            'label'    => 'From Meeting',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Meeting_List')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'empty_value' => false,
            'choices' => $mapping->getMapping('LeepAdmin_TodoTask_Status')
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setIdCategory($model->idCategory);
        $this->entity->setIdWebUser($model->idWebUser);        
        $this->entity->setName($model->name);
        $this->entity->setDescription($model->description);
        $this->entity->setStatus($model->status);
        $this->entity->setIdMeeting($model->idMeeting);
        if ($model->idType == Constants::TYPE_GOAL) {
            $this->entity->setIdGoal($model->idGoal);
            $this->entity->setIdContinualImprovement(0);
        }
        else {
            $this->entity->setIdGoal(0);
            $this->entity->setIdContinualImprovement($model->idContinualImprovement);
        }
        
        parent::onSuccess();
    }
}
