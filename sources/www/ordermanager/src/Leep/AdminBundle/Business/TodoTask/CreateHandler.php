<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();

        $request = $this->container->get('request');

        $model = new CreateModel();        
        $model->creationDate = new \DateTime();
        $model->idWebUser = $user->getId();
        $model->idCategory = $this->container->get('request')->get('idCategory', 0);
        $model->idType = Constants::TYPE_GOAL;
        $model->idMeeting = $request->get('idMeeting', 0);
        $model->name = $request->get('name', '');
        $model->description = $request->get('description', '');
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Goal type',
            'required' => true,
            'choices'  => Constants::getTaskGoalTypes()
        ));
        $builder->add('idContinualImprovement', 'choice', array(
            'label'    => 'Continual improvement',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoContinualImprovement_List')
        ));
        $builder->add('idGoal', 'choice', array(
            'label'    => 'Goal',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_List')
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoCategory_List')
        ));
        $builder->add('idWebUser', 'choice', array(
            'label'    => 'Assign to',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'     => 6,
                'cols'     => 80
            )
        ));
        $builder->add('idMeeting', 'choice', array(
            'label'    => 'From Meeting',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Meeting_List')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'empty_value' => false,
            'choices' => $mapping->getMapping('LeepAdmin_TodoTask_Status')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $task = new Entity\TodoTask();
        $task->setCreationDate($model->creationDate);
        $task->setIdCategory($model->idCategory);
        $task->setIdWebUser($model->idWebUser);
        $task->setName($model->name);
        $task->setDescription($model->description);
        $task->setStatus(Constants::STATUS_ACTIVE);
        $task->setIdMeeting($model->idMeeting);
        if ($model->idType == Constants::TYPE_GOAL) {
            $task->setIdGoal($model->idGoal);
            $task->setIdContinualImprovement(0);
        } 
        else {
            $task->setIdGoal(0);
            $task->setIdContinualImprovement($model->idContinualImprovement);
        }
        
        $em->persist($task);
        $em->flush();

        parent::onSuccess();
    }
}
