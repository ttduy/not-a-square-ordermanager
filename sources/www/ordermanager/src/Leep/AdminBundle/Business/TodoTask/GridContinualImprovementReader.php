<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridContinualImprovementReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'idUser', 'idFinishIn', 'creationDate', 'numberTask', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',          'width' => '30%', 'sortable' => 'true'),
            array('title' => 'User',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Finish in',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Age',           'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Tasks',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_TodoTaskContinualImprovementGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->setMaxResults(99999);
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:TodoContinualImprovement', 'p');
        if (!empty($this->filters->continualImprovements)) {
            $queryBuilder->andWhere('p.id IN (:continualImprovements)')
                ->setParameter('continualImprovements', $this->filters->continualImprovements);
        }
    }

    public function buildCellNumberTask($row) {
        $status = $this->container->get('request')->get('status', 0);
        $query = Utils::buildTaskQuery($this->container, $this->filters, $status);
        $query->select('COUNT(p)')
            ->andWhere('p.idContinualImprovement = '.$row->getId());
        $numTask = $query->getQuery()->getSingleScalarResult(); 

        $footer = '';
        switch ($status) {
            case Constants::STATUS_ACTIVE:
                $footer = 'current task(s)';
                break;
            case Constants::STATUS_COMPLETED:
                $footer = 'completed task(s)';
                break;
            case Constants::STATUS_DELETED:
                $footer = 'deleted task(s)';
                break;
        }

        return $numTask.' '.$footer;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('strategyPlanningToolModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'todo_continual_improvement', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'todo_continual_improvement', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellName($row) {
        $formatter = $this->container->get('LeepAdmin.Helper.Formatter');
        $html = '<b>'.$row->getName().'</b><br/>';

        if ($row->getDescription() != '') {
            $html .= $row->getDescription().'<br/>';
        }
        if ($row->getMilestones() != '') {
            $html .= "<br/>Milestones: <br/>".nl2br($row->getMilestones())."<br/>";
        }

        $html .= '<i>'.$formatter->format($row->getStatus(), 'mapping', 'LeepAdmin_TodoGoal_Status').'</i><br/>';

        return $html;
    }
}
