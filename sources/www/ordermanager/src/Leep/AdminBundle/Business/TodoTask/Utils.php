<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Leep\AdminBundle\Business;

class Utils {
    public static function buildTaskQuery($container, $filters, $status = Business\TodoTask\Constants::STATUS_ACTIVE) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:TodoTask', 'p')
            ->andWhere('p.status = :status')
            ->setParameter('status', $status);
            
        if (!empty($filters->webUsers)) {
            $query->andWhere('p.idWebUser IN (:webUsers)')
                ->setParameter('webUsers', $filters->webUsers);
        }
        if (!empty($filters->goals)) {
            $query->andWhere('p.idGoal IN (:goals)')
                ->setParameter('goals', $filters->goals);
        }
        if (!empty($filters->continualImprovements)) {
            $query->andWhere('p.idContinualImprovement IN (:continualImprovements)')
                ->setParameter('continualImprovements', $filters->continualImprovements);
        }
        if (!empty($filters->categories)) {
            $query->andWhere('p.idCategory IN (:categories)')
                ->setParameter('categories', $filters->categories);
        }
        if (trim($filters->text) != '') {
            $query->andWhere('(p.name LIKE :text) OR (p.description LIKE :text)')
                ->setParameter('text', '%'.trim($filters->text).'%');
        }
        if ($filters->idMeeting != 0) {
            $query->andWhere('p.idMeeting = :idMeeting')
                ->setParameter('idMeeting', intval($filters->idMeeting));
        }

        return $query;
    }
}