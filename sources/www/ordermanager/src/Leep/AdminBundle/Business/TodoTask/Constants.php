<?php
namespace Leep\AdminBundle\Business\TodoTask;

class Constants {
	const STATUS_ACTIVE = 0;
	const STATUS_COMPLETED = 1;
	const STATUS_DELETED = 2;

	public static function getStatus() {
		return array(
			self::STATUS_ACTIVE   => 'Current',
			self::STATUS_COMPLETED   => 'Completed',
			self::STATUS_DELETED  => 'Deleted'
		);
	}

	const TYPE_GOAL = 1;
	const TYPE_CONTINUAL_IMPROVEMENT = 2;
	public static function getTaskGoalTypes() {
		return array(
			self::TYPE_GOAL                      => 'Goal',
			self::TYPE_CONTINUAL_IMPROVEMENT     => 'Continual improvement'
		);
	}
}