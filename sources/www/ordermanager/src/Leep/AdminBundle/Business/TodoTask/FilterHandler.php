<?php
namespace Leep\AdminBundle\Business\TodoTask;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();

        $model = new FilterModel();
        $model->webUsers = array(
            $user->getId()
        );

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('goals', 'choice', array(
            'label'       => 'Goals',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMapping('LeepAdmin_TodoGoal_List')
        ));
        $builder->add('continualImprovements', 'choice', array(
            'label'       => 'Continual improvements',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMapping('LeepAdmin_TodoContinualImprovement_List')
        ));
        $builder->add('categories', 'choice', array(
            'label'       => 'Categories',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMapping('LeepAdmin_TodoCategory_List')
        ));
        $builder->add('webUsers', 'choice', array(
            'label'       => 'Assignees',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('idMeeting', 'choice', array(
            'label'       => 'Meeting',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_Meeting_List')
        ));
        $builder->add('text', 'text', array(
            'label'       => 'Search text',
            'required'    => false
        ));

        return $builder;
    }
}