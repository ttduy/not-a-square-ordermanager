<?php
namespace Leep\AdminBundle\Business\MarketingStatus;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('txt', 'text', array(
            'label'    => 'DC Info Status',
            'required' => false
        ));

        return $builder;
    }
}