<?php
namespace Leep\AdminBundle\Business\MarketingStatus;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('txt', 'text', array(
            'label'    => 'DC Info Status',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $status = new Entity\MarketingStatus();
        $status->setTxt($model->txt);
        $status->setSortOrder($model->sortOrder);

        $em->persist($status);
        $em->flush();

        parent::onSuccess();
    }
}
