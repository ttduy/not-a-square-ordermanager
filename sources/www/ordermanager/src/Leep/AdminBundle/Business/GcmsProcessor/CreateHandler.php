<?php
namespace Leep\AdminBundle\Business\GcmsProcessor;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('barcode', 'textarea', array(
            'label'    => 'Barcodes',
            'required' => false,
            'attr'    => array(
                'rows'    => 5,
                'cols'    => 70
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $rows = explode("\n", $model->barcode);
        if (count($rows) == 3) {
            $a = trim($rows[0]);
            $b = trim($rows[1]);
            $c = trim($rows[2]);
            if (strcmp($a, $b) == 0 && strcmp($a, $c) == 0) {
                $this->messages = array("Barcodes identical – Status was changed");
                $this->builder->setData($this->getDefaultFormModel());
                $this->form = $this->builder->getForm();
            }
            else {
                $this->errors = array("Barcodes are not identical");
            }
        }
        else {
            $this->errors = array("Please input exact 3 rows");
        }
    }
}
