<?php
namespace Leep\AdminBundle\Business\FoodTableParameter;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;


class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $config = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableParameter')->findAll();
        foreach ($config as $c) {
            return $c;
        }
    }
    
    public function convertToFormModel($entity) { 
        return array(
            'colorGreenRiskamount'      => $entity->getColorGreenRiskamount(),
            'colorRedRiskamount'        => $entity->getColorRedRiskamount(),  
            'foodScale'                 => $entity->getFoodScale(),
            'warnungId'                 => $entity->getWarnungId(),
            'laktoseId'                 => $entity->getLaktoseId(),
            'glutenId'                  => $entity->getGlutenId(),
            'gId'                       => $entity->getGId(),
            'kcalId'                    => $entity->getKcalId(),
            'eiwId'                     => $entity->getEiwId(),
            'kohId'                     => $entity->getKohId(),
            'fettId'                    => $entity->getFettId(),
        );

    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('colorGreenRiskamount', 'text', array(
            'label'    => 'Color green when risk amount % greater than',            
            'required' => false
        ));
        $builder->add('colorRedRiskamount', 'text', array(
            'label'    => 'Color red when risk amount % less than',
            'required' => false
        ));        
        $builder->add('foodScale', 'text', array(
            'label'    => 'Food item scale max value',
            'required' => false
        ));
        
        $builder->add('sectionIngredient', 'section', array(
            'label'    =>   'Particular ingredients',
            'property_path' => false
        ));
        $builder->add('warnungId', 'choice', array(
            'label'    => 'Warnung',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('laktoseId', 'choice', array(
            'label'    => 'Laktose',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('glutenId', 'choice', array(
            'label'    => 'Gluten',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('gId', 'choice', array(
            'label'    => 'G',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('kcalId', 'choice', array(
            'label'    => 'Kcal',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('eiwId', 'choice', array(
            'label'    => 'Eiw',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('kohId', 'choice', array(
            'label'    => 'Koh',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        $builder->add('fettId', 'choice', array(
            'label'    => 'Fett',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableIngredient_List')
        ));
        
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        foreach ($model as $k => $v) {
            $setMethod = 'set'.ucfirst($k);
            if (method_exists($this->entity, $setMethod)) {
                $this->entity->$setMethod($v);
            }
        }

        parent::onSuccess();
    }
}