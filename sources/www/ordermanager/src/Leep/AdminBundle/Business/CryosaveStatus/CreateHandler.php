<?php
namespace Leep\AdminBundle\Business\CryosaveStatus;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort order',
            'required' => false
        ));

    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $status = new Entity\CryosaveStatus();
        $status->setName($model->name);
        $status->setCode($model->code);
        $status->setSortOrder($model->sortOrder);

        $em->persist($status);
        $em->flush();

        parent::onSuccess();
    }
}
