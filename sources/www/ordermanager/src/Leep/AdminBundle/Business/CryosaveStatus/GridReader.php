<?php
namespace Leep\AdminBundle\Business\CryosaveStatus;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'code', 'sortOrder', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Code', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Sort order', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CryosaveStatus', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cryosaveUserModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cryosave_status', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cryosave_status', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
