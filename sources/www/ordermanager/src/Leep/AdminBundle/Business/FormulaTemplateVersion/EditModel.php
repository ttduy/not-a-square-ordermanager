<?php
namespace Leep\AdminBundle\Business\FormulaTemplateVersion;

class EditModel {
    public $name;
    public $number;
    public $formula;
    public $testInputValue;
    public $testOutputValue;
    public $changesNote;

    public $isOverrideHead;
}