<?php
namespace Leep\AdminBundle\Business\FormulaTemplateVersion;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormulaTemplateVersion', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) { 
        $em = $this->container->get('doctrine')->getEntityManager();
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($entity->getIdFormulaTemplate());

        $model = new EditModel(); 
        $model->name = $formulaTemplate->getName();
        $model->number = $entity->getNumber();
        $model->changesNote = $entity->getChangesNote();
        if ($entity->getVersionAlias() != '') {
            $model->number .= ' / '.$entity->getVersionAlias();
        }
        $model->formula = $entity->getFormula();
        $model->testInputValue = $entity->getTestInput();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'label', array(
            'label'    => 'Name',
            'required' => false
        ));        
        $builder->add('number', 'label', array(
            'label'    => 'Number',
            'required' => false
        ));        
        $builder->add('formula', 'textarea', array(
            'label'    => 'Formula',
            'required' => true,
            'attr'     => array(
                'rows' => 30, 'cols' => 130,
            )
        ));        
        $builder->add('testInputValue', 'textarea', array(
            'label'    => 'Test Input Value',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 80
            )
        ));
        $builder->add('testOutputValue', 'textarea', array(
            'label'    => 'Test Output Value',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 80
            )
        ));
        $builder->add('changesNote', 'textarea', array(
            'label'    => 'Changes notes',
            'required' => false,
            'attr'     => array(
                'rows' => 5, 'cols' => 80
            )
        ));

        $builder->add('sectionVersioning', 'section', array(
            'label'    => 'Versioning',
            'property_path' => false
        ));
        $builder->add('isOverrideHead', 'checkbox', array(
            'label'    => 'Override HEAD?',
            'required' => false,
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $this->entity->setFormula($model->formula);
        $this->entity->setTestInput($model->testInputValue);
        $this->entity->setChangesNote($model->changesNote);

        if ($model->isOverrideHead) {
            $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($this->entity->getIdFormulaTemplate());
            $formulaTemplate->setFormula($model->formula);
            $formulaTemplate->setTestInputValue($model->testInputValue);
            $em->flush();

            $this->messages[] = "Formula ".$formulaTemplate->getName()." has been overrided by this version";
        }

        parent::onSuccess();
    }
}