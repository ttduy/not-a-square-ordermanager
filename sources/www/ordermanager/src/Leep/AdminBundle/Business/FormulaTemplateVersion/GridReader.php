<?php
namespace Leep\AdminBundle\Business\FormulaTemplateVersion;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('timestamp', 'number',  'changesNote', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',     'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Number',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Changes note',  'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FormulaTemplateVersionGridReader');
    }

    public function buildQuery($queryBuilder) {
        $request = $this->container->get('request');

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FormulaTemplateVersion', 'p');
        $queryBuilder->andWhere('p.idFormulaTemplate = :idFormulaTemplate')
            ->setParameter('idFormulaTemplate', $request->get('idFormulaTemplate', 0));

        if (trim($this->filters->number) != '') {
            $queryBuilder->andWhere('p.number = :number')
                ->setParameter('number', intval($this->filters->number));
        }
        if (trim($this->filters->versionAlias) != '') {
            $queryBuilder->andWhere('p.versionAlias LIKE :versionAlias')
                ->setParameter('versionAlias', '%'.trim($this->filters->versionAlias).'%');
        }            
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.number', 'DESC');        
    }

    public function buildCellNumber($row) {
        $text = $row->getNumber();
        if ($row->getVersionAlias() != '') {
            $text .= ' / '.$row->getVersionAlias();
        }
        return $text;
    }

    public function buildCellChangesNote($row) {
        return nl2br($row->getChangesNote());
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('formulaTemplateModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'formula_template_version', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        return $builder->getHtml();
    }
}