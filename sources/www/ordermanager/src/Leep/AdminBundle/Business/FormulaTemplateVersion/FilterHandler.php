<?php
namespace Leep\AdminBundle\Business\FormulaTemplateVersion;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('number', 'text', array(
            'label'       => 'Number',
            'required'    => false
        ));
        $builder->add('versionAlias', 'text', array(
            'label'       => 'Version alias',
            'required'    => false
        ));

        return $builder;
    }
}