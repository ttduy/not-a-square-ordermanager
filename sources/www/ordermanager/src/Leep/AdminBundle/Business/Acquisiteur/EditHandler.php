<?php
namespace Leep\AdminBundle\Business\Acquisiteur;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->uidNumber = $entity->getUidNumber();
        $model->domain = $entity->getDomain();
        $model->acquisiteur = $entity->getAcquisiteur();
        $model->creationDate = $entity->getCreationDate();
        $model->genderId = $entity->getGenderId();
        $model->title = $entity->getTitle();
        $model->firstName = $entity->getFirstName();
        $model->surName = $entity->getSurName();
        $model->institution = $entity->getInstitution();

        $model->street = $entity->getStreet();
        $model->postCode = $entity->getPostCode();
        $model->city = $entity->getCity();
        $model->countryId = $entity->getCountryId();
        $model->contactEmail = $entity->getContactEmail();
        $model->telephone = $entity->getTelephone();
        $model->fax = $entity->getFax();
        $model->statusDeliveryEmail = $entity->getStatusDeliveryEmail();
        $model->invoiceDeliveryEmail = $entity->getInvoiceDeliveryEmail();

        $model->accountName = $entity->getAccountName();
        $model->accountNumber = $entity->getAccountNumber();
        $model->bankCode = $entity->getBankCode();
        $model->bic = $entity->getBic();
        $model->iban = $entity->getIban();
        $model->otherNotes = $entity->getOtherNotes();

        $model->idMarginPaymentMode = $entity->getIdMarginPaymentMode();
        $model->commissionPaymentWithTax = $entity->getCommissionPaymentWithTax();

        $model->rating = $entity->getRating();
        $model->isNotContacted = $entity->getIsNotContacted();
        $model->accountingCode = $entity->getAccountingCode();
        $model->isPaymentWithTax = $entity->getIsPaymentWithTax();
        $model->paymentNoTaxText = $entity->getPaymentNoTaxText();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('acquisiteur', 'text', array(
            'label'    => 'Acquisiteur Name',
            'required' => true
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));

        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false
        ));
        $builder->add('sectionPaymentSetting', 'section', array(
            'label'    => 'Payment Settings',
            'property_path' => false
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));

        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $this->entity->setUidNumber($model->uidNumber);
        $this->entity->setDomain($model->domain);
        $this->entity->setAcquisiteur($model->acquisiteur);
        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setGenderId($model->genderId);
        $this->entity->setTitle($model->title);
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setInstitution($model->institution);

        $this->entity->setStreet($model->street);
        $this->entity->setPostCode($model->postCode);
        $this->entity->setCity($model->city);
        $this->entity->setCountryId($model->countryId);
        $this->entity->setContactEmail($model->contactEmail);
        $this->entity->setTelephone($model->telephone);
        $this->entity->setFax($model->fax);
        $this->entity->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $this->entity->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);

        $this->entity->setAccountName($model->accountName);
        $this->entity->setAccountNumber($model->accountNumber);
        $this->entity->setBankCode($model->bankCode);
        $this->entity->setBIC($model->bic);
        $this->entity->setIBAN($model->iban);
        $this->entity->setOtherNotes($model->otherNotes);

        $this->entity->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $this->entity->setCommissionPaymentWithTax($model->commissionPaymentWithTax);

        $this->entity->setRating($model->rating);
        $this->entity->setIsNotContacted($model->isNotContacted);
        $this->entity->setIsPaymentWithTax($model->isPaymentWithTax);
        $this->entity->setPaymentNoTaxText($model->paymentNoTaxText);

        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->accountingCode != $this->entity->getAccountingCode()) {
            // validation accounting code
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
            $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
            $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

            if ($dc || $partner || $acquisiteur) {
                // Validation
                $this->errors += ['Accounting code existed. Please try a diffrent code!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
        }
        $this->entity->setAccountingCode($model->accountingCode);
        parent::onSuccess();
    }
}
