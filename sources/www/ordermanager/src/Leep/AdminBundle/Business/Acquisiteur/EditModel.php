<?php
namespace Leep\AdminBundle\Business\Acquisiteur;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $uidNumber;
    public $domain;
    public $acquisiteur;
    public $creationDate;
    public $genderId;
    public $title;
    public $firstName;
    public $surName;
    public $institution;

    public $street;
    public $postCode;
    public $city;
    public $countryId;
    public $contactEmail;
    public $telephone;
    public $fax;
    public $statusDeliveryEmail;
    public $invoiceDeliveryEmail;

    public $accountName;
    public $accountNumber;
    public $bankCode;
    public $bic;
    public $iban;
    public $otherNotes;

    public $idMarginPaymentMode;
    public $isPaymentWithTax;
    public $isNotContacted;
    public $accountingCode;
    public $commissionPaymentWithTax;
    public $paymentNoTaxText;

    public $rating;
}
