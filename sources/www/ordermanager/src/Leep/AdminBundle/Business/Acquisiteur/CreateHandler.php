<?php
namespace Leep\AdminBundle\Business\Acquisiteur;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $em = $this->container->get('doctrine')->getEntityManager();
        // accountingCode
        $query = $em->createQueryBuilder();
        $accountingCodeMaxDC = $query->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryPartner = $em->createQueryBuilder();
        $accountingCodeMaxPartner = $queryPartner->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Partner', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();


        $queryAcquisiteur = $em->createQueryBuilder();
        $accountingCodeMaxAcqui = $queryAcquisiteur->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Acquisiteur', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();
        $max1 = ($accountingCodeMaxDC > $accountingCodeMaxPartner) ? $accountingCodeMaxDC : $accountingCodeMaxPartner;
        $maxAC = ($accountingCodeMaxAcqui > $max1) ? $accountingCodeMaxAcqui : $max1;

        $model = new CreateModel();
        $model->genderId = Business\Base\Constant::GENDER_MALE;
        $model->creationDate = new \DateTime();
        $model->accountingCode = $maxAC+1;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('acquisiteur', 'text', array(
            'label'    => 'Acquisiteur Name',
            'required' => true
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));

        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false
        ));
        $builder->add('sectionPaymentSetting', 'section', array(
            'label'    => 'Payment Settings',
            'property_path' => false
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));
        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $acquisiteur = new Entity\Acquisiteur();
        $acquisiteur->setUidNumber($model->uidNumber);
        $acquisiteur->setDomain($model->domain);
        $acquisiteur->setAcquisiteur($model->acquisiteur);
        $acquisiteur->setCreationDate($model->creationDate);
        $acquisiteur->setGenderId($model->genderId);
        $acquisiteur->setTitle($model->title);
        $acquisiteur->setFirstName($model->firstName);
        $acquisiteur->setSurName($model->surName);
        $acquisiteur->setInstitution($model->institution);

        $acquisiteur->setStreet($model->street);
        $acquisiteur->setPostCode($model->postCode);
        $acquisiteur->setCity($model->city);
        $acquisiteur->setCountryId($model->countryId);
        $acquisiteur->setContactEmail($model->contactEmail);
        $acquisiteur->setTelephone($model->telephone);
        $acquisiteur->setFax($model->fax);
        $acquisiteur->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $acquisiteur->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);

        $acquisiteur->setAccountName($model->accountName);
        $acquisiteur->setAccountNumber($model->accountNumber);
        $acquisiteur->setBankCode($model->bankCode);
        $acquisiteur->setBIC($model->bic);
        $acquisiteur->setIBAN($model->iban);
        $acquisiteur->setOtherNotes($model->otherNotes);

        $acquisiteur->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $acquisiteur->setCommissionPaymentWithTax($model->commissionPaymentWithTax);

        $acquisiteur->setIsNotContacted($model->isNotContacted);
        $acquisiteur->setRating($model->rating);
        $acquisiteur->setIsPaymentWithTax($model->isPaymentWithTax);
        $acquisiteur->setPaymentNoTaxText($model->paymentNoTaxText);

        // validation accounting code
        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
        $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
        $acquisiteurOld = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

        if ($dc || $partner || $acquisiteurOld) {
            // Validation
            $this->errors += ['Accounting code existed. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        $acquisiteur->setAccountingCode($model->accountingCode);
        $em->persist($acquisiteur);
        $em->flush();

        parent::onSuccess();
    }
}
