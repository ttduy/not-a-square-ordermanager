<?php
namespace Leep\AdminBundle\Business\Acquisiteur;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');


        $id = $this->container->get('request')->query->get('id', 0);
        $acquisiteur = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($id);
        $data[] = $this->addSection('Acquisiteur');
        $data[] = $this->add('UID Number', $acquisiteur->getUidNumber());
        $data[] = $this->add('Domain', $acquisiteur->getDomain());
        $data[] = $this->add('Acquisiteur', $acquisiteur->getAcquisiteur());
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $acquisiteur->getGenderId()));
        $data[] = $this->add('Title', $acquisiteur->getTitle());
        $data[] = $this->add('First Name', $acquisiteur->getFirstName());
        $data[] = $this->add('Surname', $acquisiteur->getSurName());
        $data[] = $this->add('Institution', $acquisiteur->getInstitution());

        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $acquisiteur->getStreet());
        $data[] = $this->add('Post Code', $acquisiteur->getPostCode());
        $data[] = $this->add('City', $acquisiteur->getCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $acquisiteur->getCountryId()));
        $data[] = $this->add('Contact Email', $acquisiteur->getContactEmail());
        $data[] = $this->add('Telephone', $acquisiteur->getTelephone());
        $data[] = $this->add('Fax', $acquisiteur->getFax());

        $data[] = $this->addSection('Delivery');
        $data[] = $this->add('Status Delivery Email', $acquisiteur->getStatusDeliveryEmail());
        $data[] = $this->add('Invoice Delivery Email', $acquisiteur->getInvoiceDeliveryEmail());

        $data[] = $this->addSection('Bank account');
        $data[] = $this->add('Account Name', $acquisiteur->getAccountName());
        $data[] = $this->add('Account Number', $acquisiteur->getAccountNumber());
        $data[] = $this->add('Bank Code', $acquisiteur->getBankCode());
        $data[] = $this->add('BIC', $acquisiteur->getBIC());
        $data[] = $this->add('IBAN', $acquisiteur->getIBAN());
        $data[] = $this->add('Other Notes', $acquisiteur->getOtherNotes());

        $data[] = $this->addSection('Others');
        $data[] = $this->add('Rating', $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $acquisiteur->getRating()));

        return $data;
    }
}
