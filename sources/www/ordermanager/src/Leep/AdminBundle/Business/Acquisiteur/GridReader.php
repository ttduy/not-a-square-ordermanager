<?php
namespace Leep\AdminBundle\Business\Acquisiteur;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('acquisiteur', 'domain', 'accountingCode', 'genderId', 'name', 'institution', 'rating', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Acquisiteur',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Domain',              'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Accoungting Code',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Gender',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Institution',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Rating',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',              'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Acquisiteur', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        if (trim($this->filters->domain) != '') {
            $queryBuilder->andWhere('p.domain LIKE :domain')
                ->setParameter('domain', '%'.trim($this->filters->domain).'%');
        }
        if (trim($this->filters->acquisiteur) != '') {
            $queryBuilder->andWhere('p.acquisiteur LIKE :acquisiteur')
                ->setParameter('acquisiteur', '%'.trim($this->filters->acquisiteur).'%');
        }
        if ($this->filters->genderId != 0) {
            $queryBuilder->andWhere('p.genderid = :genderId')
                ->setParameter('genderId', $this->filters->genderId);
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('(p.firstname LIKE :name) OR (p.surname LIKE :name)')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->institution) != '') {
            $queryBuilder->andWhere('p.institution LIKE :institution')
                ->setParameter('institution', '%'.trim($this->filters->institution).'%');
        }
        if ($this->filters->rating != 0) {
            $queryBuilder->andWhere('p.rating = :rating')
                ->setParameter('rating', $this->filters->rating);
        }
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellRating($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $mapping = $this->container->get('easy_mapping');

        $html = array();

        if ($row->getRating()) {
            $html[] = $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $row->getRating());
        }

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('acquisiteurView')) {
            $builder->addPopupButton('View History', $mgr->getUrl('leep_admin', 'rating', 'feature', 'acquisiteurRatingHistory', array('id' => $row->getId())), 'button-detail');
            $html[] = $builder->getHtml();
        }

        return implode('&nbsp;&nbsp;', $html);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $row->getId())), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('acquisiteurModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'acquisiteur', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'acquisiteur', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
