<?php
    namespace Leep\AdminBundle\Business\Acquisiteur;

    /**
     *
     */
    class Utils
    {
        static public function getAcquisiteurList($container){
            $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $query->select('p.id, p.acquisiteur')
                  ->from('AppDatabaseMainBundle:Acquisiteur', 'p');
            $queryResult = $query->getQuery()->getResult();
            $acquisiteurList = [];
            foreach ($queryResult as $qr) {
                $acquisiteurList[$qr['id']] = $qr['acquisiteur'];
            }
            
            return $acquisiteurList;
        }
    }

 ?>
