<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->sortOrder = 99;

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort order',
            'required' => false
        ));
        $url = '';//$this->container->get('router')->generate('app_admin_helper_show', array('page' => 'formula'));
        $builder->add('formula', 'textarea', array(
            'label'    => 'Formula',
            'required' => true,
            'attr'     => array(
                'rows' => 30, 'cols' => 80,
            ),
            'label_attr' => array(
                'helpText' => '<a href="'.$url.'" target="_blank" class="button-help"></a>'
            )
        ));
        $url = '';//$this->container->get('router')->generate('app_admin_helper_show', array('page' => 'formula_test_input'));
        $builder->add('testInputValue', 'textarea', array(
            'label'    => 'Test Input Value',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 80
            ),
            'label_attr' => array(
                'helpText' => '<a href="'.$url.'" target="_blank" class="button-help"></a>'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $formulaTemplate = new Entity\FormulaTemplate();
        $formulaTemplate->setName($model->name);
        $formulaTemplate->setSortOrder($model->sortOrder);
        $formulaTemplate->setFormula($model->formula);
        $formulaTemplate->setTestInputValue($model->testInputValue);

        $em = $this->container->get('doctrine')->getEntityManager();
        $em->persist($formulaTemplate);
        $em->flush();

        parent::onSuccess();
    }
}