<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

class EditModel {
    public $name;
    public $sortOrder;
    public $formula;
    public $testInputValue;
    public $testOutputValue;

    public $isMakeVersion;
    public $versionAlias;
    public $changesNote;
}