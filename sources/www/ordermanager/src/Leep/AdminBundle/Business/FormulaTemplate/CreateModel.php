<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

class CreateModel {
    public $name;
    public $sortOrder;
    public $formula;
    public $testInputValue;
    public $testOutputValue;
}