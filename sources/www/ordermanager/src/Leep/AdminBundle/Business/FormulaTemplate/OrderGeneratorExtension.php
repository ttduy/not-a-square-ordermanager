<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class OrderGeneratorExtension extends \Twig_Extension
{
    public $container = null;
    public $order = null;
    public $customer = null;
    public $em = null;
    public $mapping = null;
    public $orderedProducts = array();
    public $orderedCategories = array();
    public $formQuestions = array();
    public $formAnswer = array();
    public $formIdTypes = array();
    public function __construct($container) {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getEntityManager();
        $this->mapping = $container->get('easy_mapping');
    }
    public $currentSortOrder = -1;

    public function getFunctions()
    {
        return array(
            'clearOrderStatus'       => new \Twig_Function_Method($this, 'clearOrderStatus'),
            'clearOrderCategoryAndProduct' => new \Twig_Function_Method($this, 'clearOrderCategoryAndProduct'),
            'clearOrderQuestion'     => new \Twig_Function_Method($this, 'clearOrderQuestion'),
            'generateOrder'          => new \Twig_Function_Method($this, 'generateOrder'),
            'loadOrder'              => new \Twig_Function_Method($this, 'loadOrder'),
            'updateOrderField'       => new \Twig_Function_Method($this, 'updateOrderField'),
            'today'                  => new \Twig_Function_Method($this, 'today'),
            'addOrderCategory'       => new \Twig_Function_Method($this, 'addOrderCategory'),
            'addOrderProduct'        => new \Twig_Function_Method($this, 'addOrderProduct'),
            'addOrderStatus'         => new \Twig_Function_Method($this, 'addOrderStatus'),
            'updateOrderQuestion'    => new \Twig_Function_Method($this, 'updateOrderQuestion'),
            'saveOrderNow'           => new \Twig_Function_Method($this, 'saveOrderNow'),
        );
    }

    public function clearOrderStatus() {
        $query = $this->em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid = '.$this->order->getId())
            ->getQuery()
            ->execute();
        return '';
    }

    public function clearOrderCategoryAndProduct() {
        $query = $this->em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->andWhere('p.customerid = '.$this->order->getId())
            ->getQuery()
            ->execute();
        $this->orderedProducts = array();

        $query = $this->em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:OrderedCategory', 'p')
            ->andWhere('p.customerid = '.$this->order->getId())
            ->getQuery()
            ->execute();
        $this->orderedCategories = array();
        return '';
    }

    public function clearOrderQuestion() {
        $query = $this->em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:OrderedQuestion', 'p')
            ->andWhere('p.idCustomer = '.$this->order->getId())
            ->getQuery()
            ->execute();
        $this->formAnswer = array();
    }

    public function generateOrder($orderNumber)
    {
        $order = $this->em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($orderNumber);
        $customer = $this->em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneByCustomerNumber($orderNumber);

        if ($order) {
            throw new Exception\FormulaDebugException("Order Number '".$orderNumber."' is already existed!");
        }
        if ($customer) {
            throw new Exception\FormulaDebugException("Customer Number '".$orderNumber."' is already existed!");
        }

        $this->customer = new Entity\CustomerInfo();
        $this->customer->setCustomerNumber($orderNumber);
        $this->customer->setIdDistributionChannel(0);
        $this->customer->setIsDestroySample(0);
        $this->customer->setIsFutureResearchParticipant(0);
        $this->em->persist($this->customer);
        $this->em->flush();

        $this->order = new Entity\Customer();
        $this->order->setOrderNumber($orderNumber);
        $this->order->setIdCustomerInfo($this->customer->getId());
        $this->em->persist($this->order);
        $this->em->flush();

        return 'OrderNumberId = '.$this->order->getId()."\n";
    }

    public function loadOrder($orderNumber)
    {
        $this->order = $this->em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($orderNumber);
        if (!$this->order) {
            throw new Exception\FormulaDebugException("Order Number '".$orderNumber."' doesn't exist!");
        }

        $this->customer = $this->em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($this->order->getIdCustomerInfo());
        if (!$this->customer) {
            throw new Exception\FormulaDebugException("Customer of '".$orderNumber."' doesn't exist!");
        }

        // Load order products
        $products = $this->em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($this->order->getId());
        foreach ($products as $p) {
            $this->orderedProducts[] = $p->getProductId();
        }

        // Load order categories
        $categories = $this->em->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBycustomerid($this->order->getId());
        foreach ($categories as $c) {
            $this->orderedCategories[] = $c->getCategoryId();
        }
        return '';
    }

    public function saveOrderNow() {
        // Default invoice status
        if ($this->order->getInvoiceGoesToId() == Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER) {
            $this->order->setInvoiceStatus(Business\Invoice\Constant::INVOICE_STATUS_TO_BE_DONE);

            $invoiceStatusRow = $this->em->getRepository('AppDatabaseMainBundle:InvoiceStatus')->findOneBy(array(
                'customerid' => $this->order->getId(),
                'status'     => Business\Invoice\Constant::INVOICE_STATUS_TO_BE_DONE
            ));
            if (empty($invoiceStatusRow)) {
                $invoiceStatusRow = new Entity\InvoiceStatus();
                $invoiceStatusRow->setCustomerId($this->order->getId());
                $invoiceStatusRow->setStatusDate(new \DateTime());
                $invoiceStatusRow->setStatus(Business\Invoice\Constant::INVOICE_STATUS_TO_BE_DONE);
                $invoiceStatusRow->setSortOrder(1);
                $this->em->persist($invoiceStatusRow);
            }
        }

        // Save order products / categories
        $this->order->setCategoryList(implode(',', $this->orderedCategories));
        $this->order->setProductList(implode(',', $this->orderedProducts));

        // Update product list to combine category product and ordered product
        $this->em->flush();

        // Save form question
        foreach ($this->formAnswer as $id => $answer) {
            $idType = isset($this->formIdTypes[$id]) ? $this->formIdTypes[$id] : Business\FormQuestion\Constant::FORM_QUESTION_TYPE_TEXTBOX;

            $answerEntry = new Entity\OrderedQuestion();
            $answerEntry->setIdCustomer($this->order->getId());
            $answerEntry->setIdFormQuestion($id);
            $answerEntry->setAnswer(Business\FormQuestion\Util::encodeAnswer($idType, $answer));
            $this->em->persist($answerEntry);
        }
        $this->em->flush();

        // Post processing

        Business\CustomerInfo\Utils::ensureDuplication($this->em, $this->customer);
        Business\Customer\Util::updateStatusList($this->container, $this->order->getId());
        Business\Customer\Util::updateRevenueEstimation($this->container, $this->order->getId());
        $this->em->flush();

        return '';
    }
    public function today() {
        $today = new \DateTime();
        return $today->format('d/m/Y');
    }

    public function addOrderCategory($category) {
        $categoryMap = $this->getRevertMap('LeepAdmin_Category_List');
        if (!isset($categoryMap[$category])) {
            throw new Exception\FormulaDebugException("Category '".$category."' doesn't exist!");
        }
        $categoryId = $categoryMap[$category];

        if (!in_array($categoryId, $this->orderedCategories)) {
            $entry = new Entity\OrderedCategory();
            $entry->setCategoryId($categoryId);
            $entry->setCustomerId($this->order->getId());
            $this->em->persist($entry);

            $this->orderedCategories[] = $categoryId;
        }

        // Add products
        $linkedProducts = $this->em->getRepository('AppDatabaseMainBundle:CatProdLink')->findBycategoryid($categoryId);
        foreach ($linkedProducts as $p) {
            $productId = $p->getProductId();
            if (!in_array($productId, $this->orderedProducts)) {
                $entry = new Entity\OrderedProduct();
                $entry->setProductId($productId);
                $entry->setCustomerId($this->order->getId());
                $this->em->persist($entry);

                $this->orderedProducts[] = $productId;
            }
        }
    }

    public function addOrderProduct($product) {
        $productMap = $this->getRevertMap('LeepAdmin_Product_List');
        if (!isset($productMap[$product])) {
            throw new Exception\FormulaDebugException("Product '".$product."' doesn't exist!");
        }
        $productId = $productMap[$product];

        if (!in_array($productId, $this->orderedProducts)) {
            $entry = new Entity\OrderedProduct();
            $entry->setProductId($productId);
            $entry->setCustomerId($this->order->getId());
            $this->em->persist($entry);

            $this->orderedProducts[] = $productId;
        }

        return '';
    }

    protected function getRevertMap($mapkey) {
        $map = $this->mapping->getMapping($mapkey);
        $revertMap = array();
        foreach ($map as $k => $v) {
            $revertMap[$v] = $k;
        }
        return $revertMap;
    }

    public function addOrderStatus($status, $date = '') {
        if ($this->currentSortOrder == -1) {
            $query = $this->em->createQueryBuilder();
            $query->select('MAX(p.sortorder)')
                ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
                ->andWhere('p.customerid = :idCustomer')
                ->setParameter('idCustomer', $this->order->getId());
            $this->currentSortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;
        }

        $revertMap = $this->getRevertMap('LeepAdmin_Customer_Status');
        if (!isset($revertMap[$status])) {
            throw new Exception\FormulaDebugException("Status '".$status."' doesn't exist!");
        }

        //
        $statusDate = null;
        if ($date != '') {
            $statusDate = \DateTime::createFromFormat('d/m/Y', $date);
        }

        // Add new status
        $statusEntry = new Entity\OrderedStatus();
        $statusEntry->setCustomerId($this->order->getId());
        if ($statusDate) {
            $statusEntry->setStatusDate($statusDate);
        }
        $statusEntry->setStatus($revertMap[$status]);
        $statusEntry->setIsNotFilterable(0);
        $statusEntry->setSortOrder($this->currentSortOrder++);
        $this->em->persist($statusEntry);


        $this->order->setStatus($revertMap[$status]);
        if ($statusDate) {
            $this->order->setStatusDate($statusDate);
        }

        return '';
    }

    public function updateOrderQuestion($key, $value) {
        if ($this->formQuestions == null) {
            $formQuestions = Business\Customer\Util::getFormQuestions($this->container);
            $this->formQuestions = array();
            foreach ($formQuestions as $q) {
                $this->formQuestions[$q['reportKey']] = array(
                    'id'       => $q['id'],
                    'idType'   => $q['idType']
                );
            }
        }

        if (!isset($this->formQuestions[$key])) {
            throw new Exception\FormulaDebugException("Question '".$key."' doesn't exist!");
        }

        $t = $this->formQuestions[$key];
        $id = $t['id'];
        $idType = $t['idType'];
        $this->formIdTypes[$id] = $idType;
        if ($idType == Business\FormQuestion\Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE) {
            if (!isset($this->formAnswer[$id])) {
                if (is_array($value)) {
                    $this->formAnswer[$id] = $value;
                }
                else {
                    $this->formAnswer[$id] = array($value);
                }
            }
            else {
                if (is_array($value)) {
                    foreach ($value as $v) {
                        $this->formAnswer[$id][] = $v;
                    }
                }
                else {
                    $this->formAnswer[$id][] = $value;
                }
            }
        }
        else {
            $this->formAnswer[$id] = $value;
        }

        return '';
    }

    public function getName()
    {
        return 'order_generator_extension';
    }


    //////////////////////////////////////////////////////////
    // x. Order Field
    protected $orderFieldHandlers = array(
        'DistributionChannel'               => array('updateOrderFieldDistributionChannel'),
        'DateOrdered'                       => array('updateOrderFieldDateOrdered'),
        'Name'                              => array('updateOrderFieldName'),
        'Birthday'                          => array('updateOrderFieldBirthday'),
        'Title'                             => array('updateCustomerSimpleField', 'setTitle'),
        'Gender'                            => array('updateCustomerSimpleMap', array('setGenderId', 'LeepAdmin_Gender_List')),
        'ExternalBarcode'                   => array('updateCustomerSimpleField', 'setExternalBarcode'),
        'DestroySample'                     => array('updateCustomerSimpleField', 'setIsDestroySample'),
        'FutureResearchParticipant'         => array('updateCustomerSimpleField', 'setIsFutureResearchParticipant'),
        'CustomerOptOut'                    => array('updateCustomerSimpleField', 'setIsNotContacted'),
        'CustomerInformation'               => array('updateCustomerSimpleField', 'setDetailInformation'),

        // Order
        'TrackingCode'                      => array('updateOrderSimpleField', 'setTrackingCode'),
        'OrderNumberForVARIO'               => array('updateOrderSimpleField', 'setDnaSampleOrderNumberVario'),
        'OrderInfo'                         => array('updateOrderSimpleField', 'setOrderInfoText'),
        'Domain'                            => array('updateOrderSimpleField', 'setDomain'),
        'LaboratoryDetails'                 => array('updateOrderSimpleField', 'setLaboratoryDetails'),

        // Address
        'AddressStreet'                     => array('updateOrderSimpleField', 'setStreet'),
        'AddressStreet2'                    => array('updateOrderSimpleField', 'setStreet2'),
        'AddressPostCode'                   => array('updateCustomerSimpleField', 'setPostCode'),
        'AddressCity'                       => array('updateOrderSimpleField', 'setCity'),
        'AddressCountry'                    => array('updateOrderSimpleMap', array('setCountryId', 'LeepAdmin_Country_List')),
        'AddressEmail'                      => array('updateCustomerSimpleField', 'setEmail'),
        'AddressTelephone'                  => array('updateOrderSimpleField', 'setTelephone'),
        'AddressFax'                        => array('updateOrderSimpleField', 'setFax'),
        'AddressNotes'                      => array('updateOrderSimpleField', 'setNotes'),

        // Invoice Address
        'InvoiceAddressExist'               => array('updateOrderSimpleField', 'setInvoiceAddressIsUsed'),
        'InvoiceAddressCompanyName'         => array('updateOrderSimpleField', 'setInvoiceAddressCompanyName'),
        'InvoiceAddressClientName'          => array('updateOrderSimpleField', 'setInvoiceAddressClientName'),
        'InvoiceAddressStreet'              => array('updateOrderSimpleField', 'setInvoiceAddressStreet'),
        'InvoiceAddressPostCode'            => array('updateOrderSimpleField', 'setInvoiceAddressPostCode'),
        'InvoiceAddressCity'                => array('updateOrderSimpleField', 'setInvoiceAddressCity'),
        'InvoiceAddressCountry'             => array('updateOrderSimpleMap', array('setInvoiceAddressIdCountry', 'LeepAdmin_Country_List')),
        'InvoiceAddressTelephone'           => array('updateOrderSimpleField', 'setInvoiceAddressTelephone'),
        'InvoiceAddressFax'                 => array('updateOrderSimpleField', 'setInvoiceAddressFax'),

        // Report Setting
        'Language'                          => array('updateOrderSimpleMap', array('setLanguageId', 'LeepAdmin_Language_List')),
        'ReportSettingCompanyInfo'          => array('updateOrderSimpleField', 'setCompanyInfo'),
        'ReportSettingLaboratoryInfo'       => array('updateOrderSimpleField', 'setLaboratoryInfo'),
        'ReportSettingContactInfo'          => array('updateOrderSimpleField', 'setContactInfo'),
        'ReportSettingContactUs'            => array('updateOrderSimpleField', 'setContactUs'),
        'ReportSettingLetterExtraText'      => array('updateOrderSimpleField', 'setLetterExtraText'),
        'ReportSettingClinicianInfoText'    => array('updateOrderSimpleField', 'setClinicianInfoText'),
        'ReportSettingText7'                => array('updateOrderSimpleField', 'setText7'),
        'ReportSettingText8'                => array('updateOrderSimpleField', 'setText8'),
        'ReportSettingText9'                => array('updateOrderSimpleField', 'setText9'),
        'ReportSettingText10'               => array('updateOrderSimpleField', 'setText10'),
        'ReportSettingText11'               => array('updateOrderSimpleField', 'setText11'),

        // GSSetting
        'GSSettingDoctorsReporting'         => array('updateOrderSimpleField', 'setDoctorsReporting'),
        'GSSettingAutomaticReporting'       => array('updateOrderSimpleField', 'setAutomaticReporting'),
        'GSSettingNoReporting'              => array('updateOrderSimpleField', 'setNoReporting'),
        'GSSettingInvoiceAndPaymentInfo'    => array('updateOrderSimpleField', 'setInvoicingAndPaymentInfo'),
        'GSSettingDncReportType'            => array('updateOrderSimpleMap', array('setIdDncReportType', 'LeepAdmin_Report_DncReportType')),

        // Rebrander
        'RebranderRebrandingNick'           => array('updateOrderSimpleField', 'setRebrandingNick'),
        'RebranderHeaderFilename'           => array('updateOrderSimpleField', 'setHeaderFileName'),
        'RebranderFooterFilename'           => array('updateOrderSimpleField', 'setFooterFileName'),
        'RebranderTitleLogoFilename'        => array('updateOrderSimpleField', 'setTitleLogoFileName'),
        'RebranderBoxLogoFilename'          => array('updateOrderSimpleField', 'setBoxLogoFileName'),

        // Invoice setting
        'InvoicePriceCategory'              => array('updateOrderSimpleMap', array('setPriceCategoryId', 'LeepAdmin_PriceCategory_List')),
        'InvoiceReportDeliveryEmail'        => array('updateOrderSimpleField', 'setReportDeliveryEmail'),
        'InvoiceInvoiceDeliveryEmail'       => array('updateOrderSimpleField', 'setInvoiceDeliveryEmail'),
        'InvoiceInvoiceGoesTo'              => array('updateOrderSimpleMap', array('setInvoiceGoesToId', 'LeepAdmin_DistributionChannel_InvoiceGoesTo')),
        'InvoiceReportGoesTo'               => array('updateOrderSimpleMap', array('setReportGoesToId', 'LeepAdmin_DistributionChannel_ReportGoesTo')),
        'InvoiceReportDelivery'             => array('updateOrderSimpleMap', array('setReportDeliveryId', 'LeepAdmin_DistributionChannel_ReportDelivery')),
        'InvoicePreferredPayment'           => array('updateOrderSimpleMap', array('setIdPreferredPayment', 'LeepAdmin_DistributionChannel_PreferredPayment')),
        'InvoiceNutriMeGoesTo'              => array('updateOrderSimpleMap', array('setIdNutrimeGoesTo', 'LeepAdmin_DistributionChannel_NutrimeGoesTo')),

        'InvoiceFtpServer'                  => array('updateOrderSimpleField', 'setFtpServerName'),
        'InvoiceFtpUsername'                => array('updateOrderSimpleField', 'setFtpUsername'),
        'InvoiceFtpPassword'                => array('updateOrderSimpleField', 'setFtpPassword'),

        'PricingMarginGoesTo'               => array('updateOrderSimpleMap', array('setIdMarginGoesTo', 'LeepAdmin_DistributionChannel_MarginGoesTo')),
        'PricingMarginOverrideDc'           => array('updateOrderSimpleField', 'setMarginOverrideDc'),
        'PricingMarginOverridePartner'      => array('updateOrderSimpleField', 'setMarginOverridePartner'),
        'PricingPriceOverride'              => array('updateOrderSimpleField', 'setPriceOverride'),
        'PricingAcquisiteur1'               => array('updateOrderSimpleMap', array('setIdAcquisiteur1', 'LeepAdmin_Acquisiteur_List')),
        'PricingAcquisiteurCommission1'     => array('updateOrderSimpleField', 'setAcquisiteurCommissionRate1'),
        'PricingAcquisiteur2'               => array('updateOrderSimpleMap', array('setIdAcquisiteur2', 'LeepAdmin_Acquisiteur_List')),
        'PricingAcquisiteurCommission2'     => array('updateOrderSimpleField', 'setAcquisiteurCommissionRate2'),
        'PricingAcquisiteur3'               => array('updateOrderSimpleMap', array('setIdAcquisiteur3', 'LeepAdmin_Acquisiteur_List')),
        'PricingAcquisiteurCommission3'     => array('updateOrderSimpleField', 'setAcquisiteurCommissionRate3'),

    );

    public function updateOrderField($key, $value) {
        $key = trim($key);

        if (!isset($this->orderFieldHandlers[$key])) {
            throw new Exception\FormulaDebugException("updateOrderField: unknown field '".$key."'");
        }

        $arr = $this->orderFieldHandlers[$key];
        $func = $arr[0];
        if (isset($arr[1])) {
            $this->$func($value, $arr[1], $key);
        } else {
            $this->$func($value);
        }

        return '';
    }

    protected function updateOrderFieldDistributionChannel($value) {
        $dc = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneBydistributionchannel($value);
        if (empty($dc)) {
            throw new Exception\FormulaDebugException("updateOrderField:DistributionChannel: Can't find '".$value."'");
        }

        $this->order->setDistributionChannelId($dc->getId());
        $this->customer->setIdDistributionChannel($dc->getId());

        $this->customer->setIsFutureResearchParticipant($dc->getIsFutureREsearchParticipant());
        $this->customer->setIsDestroySample($dc->getIsDestroySample());
        $this->customer->setIsNotContacted($dc->getIsNotContacted());

        $this->order->setCompanyInfo($dc->getCompanyInfo());
        $this->order->setLaboratoryInfo($dc->getLaboratoryInfo());
        $this->order->setContactInfo($dc->getContactInfo());
        $this->order->setContactUs($dc->getContactUs());
        $this->order->setLetterExtraText($dc->getLetterExtraText());
        $this->order->setClinicianInfoText($dc->getClinicianInfoText());
        $this->order->setText7($dc->getText7());
        $this->order->setText8($dc->getText8());
        $this->order->setText9($dc->getText9());
        $this->order->setText10($dc->getText10());
        $this->order->setText11($dc->getText11());

        $this->order->setDoctorsReporting($dc->getDoctorsReporting());
        $this->order->setAutomaticReporting($dc->getAutomaticReporting());
        $this->order->setNoReporting($dc->getNoReporting());
        $this->order->setInvoicingAndPaymentInfo($dc->getInvoicingAndPaymentInfo());
        $this->order->setIdDncReportType($dc->getIdDncReportType());

        $this->order->setRebrandingNick($dc->getRebrandingNick());
        $this->order->setHeaderFileName($dc->getHeaderFileName());
        $this->order->setFooterFileName($dc->getFooterFileName());
        $this->order->setTitleLogoFileName($dc->getTitleLogoFileName());
        $this->order->setBoxLogoFileName($dc->getBoxLogoFileName());

        $this->order->setPriceCategoryId($dc->getPriceCategoryId());
        $this->order->setReportDeliveryEmail($dc->getReportDeliveryEmail());
        $this->order->setInvoiceDeliveryEmail($dc->getInvoiceDeliveryEmail());
        $this->order->setInvoiceGoesToId($dc->getInvoiceGoesToId());
        $this->order->setReportGoesToId($dc->getReportGoesToId());
        $this->order->setReportDeliveryId($dc->getReportDeliveryId());
        $this->order->setFtpServerName($dc->getFtpServerName());
        $this->order->setFtpUsername($dc->getFtpUsername());
        $this->order->setFtpPassword($dc->getFtpPassword());
        $this->order->setIdPreferredPayment($dc->getIdPreferredPayment());
        $this->order->setIdMarginGoesTo($dc->getIdMarginGoesTo());
        $this->order->setIdNutrimeGoesTo($dc->getIdNutrimeGoesTo());

        $this->order->setIdAcquisiteur1($dc->getAcquisiteurId1());
        $this->order->setAcquisiteurCommissionRate1($dc->getCommission1());
        $this->order->setIdAcquisiteur2($dc->getAcquisiteurId2());
        $this->order->setAcquisiteurCommissionRate2($dc->getCommission2());
        $this->order->setIdAcquisiteur3($dc->getAcquisiteurId3());
        $this->order->setAcquisiteurCommissionRate3($dc->getCommission3());
    }

    protected function updateOrderFieldDateOrdered($value) {
        $dateOrdered = \DateTime::createFromFormat('d/m/Y', $value);
        if (!$dateOrdered) {
            throw new Exception\FormulaDebugException("updateOrderField:DateOrdered: Invalid date '".$value."'");
        }

        $this->order->setDateOrdered($dateOrdered);
    }

    protected function updateOrderFieldExternalBarcode($value) {
        $this->customer->setExternalBarcode($value);
    }

    protected function updateOrderFieldBirthday($value) {
        if ($value == '') {
            $this->customer->setDateOfBirth(null);
        }
        else {
            $dateOrdered = \DateTime::createFromFormat('d/m/Y', $value);
            if (!$dateOrdered) {
                throw new Exception\FormulaDebugException("updateOrderField:Birthday: Invalid date '".$value."'");
            }

            $this->customer->setDateOfBirth($dateOrdered);
        }
    }

    protected function updateCustomerSimpleField($value, $setFunc, $key) {
        $this->customer->$setFunc($value);
    }
    protected function updateCustomerSimpleMap($value, $params, $key) {
        $func = $params[0];
        if ($value == '') {
            $this->customer->$func(0);
            return '';
        }

        $mappingKey = $params[1];

        $map = $this->mapping->getMapping($mappingKey);
        foreach ($map as $k => $v) {
            if (trim($v) == trim($value)) {
                $this->customer->$func($k);
                return '';
            }
        }

        throw new Exception\FormulaDebugException("[".$key."] Value '".$value."' doesn't exist!");
    }
    protected function updateOrderSimpleMap($value, $params, $key) {
        $func = $params[0];
        if ($value == '') {
            $this->order->$func(0);
            return '';
        }
        $mappingKey = $params[1];

        $map = $this->mapping->getMapping($mappingKey);
        foreach ($map as $k => $v) {
            if (trim($v) == trim($value)) {
                $this->order->$func($k);
                return '';
            }
        }
        throw new Exception\FormulaDebugException("[".$key."] Value '".$value."' doesn't exist!");
    }
    protected function updateOrderSimpleField($value, $setFunc, $key) {
        $this->order->$setFunc($value);
    }

    protected function updateOrderFieldName($value) {
        $tokens = explode(' ', $value);
        $firstname = array_shift($tokens);
        $surname = implode(' ', $tokens);
        $this->customer->setFirstName($firstname);
        $this->customer->setSurName($surname);
    }
}