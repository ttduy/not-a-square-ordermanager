<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

class FormulaExtension extends \Twig_Extension
{
    public $container = null;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            'max' => new \Twig_Function_Method($this, 'maxFunction'),
            'min' => new \Twig_Function_Method($this, 'minFunction'),
            'error'    => new \Twig_Function_Method($this, 'error'),
            'round_up' => new \Twig_Function_Method($this, 'roundUpFunction'),
            'round_down' => new \Twig_Function_Method($this, 'roundDownFunction'),
            'processOutput' => new \Twig_Function_Method($this, 'processOutput'),
            'printVars' => new \Twig_Function_Method($this, 'printVars'),
            'callFormulaTemplate' => new \Twig_Function_Method($this, 'callFormulaTemplate'),
            'includeFormulaTemplate' => new \Twig_Function_Method($this, 'includeFormulaTemplate'),
            'extractReturnValues' => new \Twig_Function_Method($this, 'extractReturnValues'),
            'loadPellet' => new \Twig_Function_Method($this, 'loadPellet'),
            'loadPelletEntry' => new \Twig_Function_Method($this, 'loadPelletEntry'),
            'get_expired_date' => new \Twig_Function_Method($this, 'getExpiredDate'),
            'load_drug_ids' => new \Twig_Function_Method($this, 'loadDrugIds'),
            'load_drug_data' => new \Twig_Function_Method($this, 'loadDrug'),
            'load_all_drugs' => new \Twig_Function_Method($this, 'loadAllDrugs'),
            'export_drug_var' => new \Twig_Function_Method($this, 'exportDrug'),
            'view_drug' => new \Twig_Function_Method($this, 'viewDrug'),
            'run_formula' => new \Twig_Function_Method($this, 'runFormula'),
            'export_player' => new \Twig_Function_Method($this, 'exportPlayer'),
            'load_player_data' => new \Twig_Function_Method($this, 'loadPlayerData'),
            'return_data' => new \Twig_Function_Method($this, 'returnData'),
        );
    }

    public function maxFunction()
    {
        $args = func_get_args();
        $noArg = count($args);
        $max = $args[0];
        for ($i = 0; $i < $noArg; $i++) {
            $max = max($max, $args[$i]);
        }
        return $max;
    }

    public function minFunction()
    {
        $args = func_get_args();
        $noArg = count($args);
        $min = $args[0];
        for ($i = 0; $i < $noArg; $i++) {
            $min = min($min, $args[$i]);
        }
        return $min;
    }

    public function roundUpFunction() {
        $args = func_get_args();
        $precision = 0;
        if (isset($args[1])) {
            $precision = intval($args[1]);
        }

        return round($args[0], $precision, PHP_ROUND_HALF_UP);
    }

    public function roundDownFunction() {
        $args = func_get_args();
        $precision = 0;
        if (isset($args[1])) {
            $precision = intval($args[1]);
        }

        return round($args[0], $precision, PHP_ROUND_HALF_DOWN);
    }

    private function is_suitable($data) {
        foreach ($data as $key => $value) {
            if (is_array($value) || is_object($value)) {
                return false;
            }
        }
        return true;
    }

    public function processOutput() {
        $args = func_get_args();
        if (isset($args[0])) {
            $data = $args[0];
            if (is_array($data)) {
                return '['.implode(',', $data).']';
            }
            return $data;
        }
        return '';
    }

    public function printVars($context) {
        $vars = array();
        foreach ($context as $key => $value) {
            if (!$value instanceof Twig_Template) {
                $vars[] = $key.' = '.$value;
            }
        }

        throw new Exception\FormulaDebugException(implode("\n", $vars));
    }

    public function error($message) {
        throw new Exception\FormulaDebugException($message);
    }

    public function callFormulaTemplate($templateName, $inputParameter) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneByName($templateName);
        if (empty($formulaTemplate)) {
            throw new Exception\FormulaDebugException("Can't find the formula '".$templateName."'");
        }

        $executor = $this->container->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $inputParameter);

        if (count($executor->errors) > 0) {
            throw new Exception\FormulaDebugException("Error the formula '".$templateName."': ".implode("\n", $executor->errors));
        }

        return $executor->output;
    }

    private function processContext($context) {
        $contextReturn = [];
        foreach ($context as $key => $value) {
            if (is_array($value)) {
                $value = "[".implode(', ', $value)."]";
            }
            $contextReturn[$key] = $value;
        }
        return $contextReturn;
    }

    public function includeFormulaTemplate($templateName, &$context) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneByName($templateName);
        if (empty($formulaTemplate)) {
            throw new Exception\FormulaDebugException("Can't find the formula '".$templateName."'");
        }

        $input = $this->processContext($context);
        $executor = $this->container->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $input, $isInclude = true);

        if (count($executor->errors) > 0) {
            throw new Exception\FormulaDebugException("Error the formula '".$templateName."': ".implode("\n", $executor->errors));
        }
        $context = $executor->output;

        return '';
    }

    public function extractReturnValues($returnValues, &$context) {
        foreach ($returnValues as $k => $v) {
            $context[$k] = $v;
        }
        return '';
    }

    public function loadPellet(&$context, $idPellet, $prefix) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $pellet = $em->getRepository('AppDatabaseMainBundle:Pellet')->findOneById($idPellet);

        $context[$prefix.'Name'] = $pellet->getName();
        $context[$prefix.'Letter'] = $pellet->getLetter();
        return '';
    }
    public function loadPelletEntry(&$context, $idPelletEntry, $prefix) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $pelletEntry = $em->getRepository('AppDatabaseMainBundle:PelletEntry')->findOneById($idPelletEntry);
        $weights = $em->getRepository('AppDatabaseMainBundle:PelletEntryWeight')->findByIdPelletEntry($idPelletEntry);
        $additives = $em->getRepository('AppDatabaseMainBundle:PelletEntryAdditives')->findByIdPelletEntry($idPelletEntry);

        $context[$prefix.'LotNumber'] = $pelletEntry->getShipment();
        $context[$prefix.'ExpiryDate'] = $formatter->format($pelletEntry->getExpiryDate(), 'date');
        $ingredientArr = array();
        foreach ($weights as $w) {
            $ingredientArr[] = $w->getTxt().' ('.number_format($w->getWeight(), 2).'%)';
        }
        $context[$prefix.'Ingredients'] = implode(', ', $ingredientArr);
        $additivesArr = array();
        foreach ($additives as $w) {
            $additivesArr[] = $w->getName().' ('.number_format($w->getWeight(), 2).'%)';
        }
        $context[$prefix.'Additives'] = implode(', ', $additivesArr);

        return '';
    }

    public function getName()
    {
        return 'formula_extension';
    }

    public function getExpiredDate($numberMonth)
    {
        // get current date time and plus with a number months.
        // The new date after added is expired date that we need find
        if($numberMonth < 0){
            throw new Exception\FormulaDebugException("Error! Number of month can be negative value");
        }
        if (!filter_var($numberMonth, FILTER_VALIDATE_INT)) {
            throw new Exception\FormulaDebugException("Error! Number of month must be integer value");
        }
        $currentDate = new \DateTime();
        $expiredDate = $currentDate->add(new \DateInterval('P'.(strval($numberMonth)).'M'));
        $expiredDate_str = $expiredDate->format('d/m/Y');
        return $expiredDate_str;
    }

    public function loadDrugIds() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from('AppDatabaseMainBundle:Drug', 'p');
        $res = $query->getQuery()->getArrayResult();
        $ids = [];
        foreach ($res as $key => $value) {
            $ids[] = strval($value['id']);
        }
        return $ids;
    }

    private function getDrugIngredients($drug) {
        $tmp = $drug;
        unset($tmp['ingredients']);
        $ingredients_str = $drug['ingredients'];
        $ingredients = explode("\n", $ingredients_str);
        foreach ($ingredients as $ingredient) {
            $items = explode("=", $ingredient);
            $key = trim($items[0]);
            $value = floatval(trim($items[1]));
            $tmp[$key] = $value;
        }
        return $tmp;
    }

    public function loadDrug($drugId) {
        $drugId = intval($drugId);
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('AppDatabaseMainBundle:Drug', 'p')
                ->where($query->expr()->eq('p.id', ':drugId'))
                ->setParameter('drugId', $drugId);
        $res = $query->getQuery()->getArrayResult();
        $fixedDrug = $this->getDrugIngredients($res[0]);
        return $fixedDrug;
    }

    public function loadAllDrugs() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('AppDatabaseMainBundle:Drug', 'p');
        $results = $query->getQuery()->getArrayResult();
        return $results;
    }

    public function exportDrug($drug, $nameVariable, $var3) {
        $stringEcho =  'DRUG_'.strval($drug['id']).'_'.$nameVariable.'='.strval($var3);
        return $stringEcho;
    }

    public function viewDrug($drug) {
        $drugFixed = [];
        foreach ($drug as $key => $value) {
            $drugFixed[] = $key.'='.$value;
        }
        $drugFixed_str = implode("\n", $drugFixed);
        return $drugFixed_str;
    }

    private function isJson($variable) {
         json_decode($variable);
         return (json_last_error() == JSON_ERROR_NONE);
    }

    public function runFormula($dataInput, $templateName) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneByName($templateName);
        if (empty($formulaTemplate)) {
            throw new Exception\FormulaDebugException("Can't find the formula '".$templateName."'");
        }

        $input = $dataInput;
        $executor = $this->container->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $input, $isInclude = false);

        if (count($executor->errors) > 0) {
            throw new Exception\FormulaDebugException("Error the formula '".$templateName."': ".implode("\n", $executor->errors));
        }
        $result = $executor->output;
        $returnData = $result['RETURN_DATA'];
        if ($this->isJson($returnData)) {
            $returnData = json_decode($returnData, JSON_OBJECT_AS_ARRAY);
        }
        return $returnData;
    }

    public function exportPlayer($order, $fieldName, $fieldValue) {
        if ($order < 10) {
            $order = '0'.strval($order);
        }
        else {
            $order = strval($order);
        }
        $dumpString = 'PLAYER'.$order.'_'.$fieldName.' = '.$fieldValue."\n";
        return $dumpString;
    }

    public function loadPlayerData($orderNumber) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from("AppDatabaseMainBundle:Customer", 'p')
                ->where($query->expr()->eq('p.ordernumber', ':orderNumber'))
                ->setParameter('orderNumber', $orderNumber);
        $result = $query->getQuery()->getArrayResult();
        $customer = $result[0];
        foreach ($customer as $key => $value) {
            if (is_a($value, 'DateTime')) {
                $customer[$key] = $value->format('Y-m-d H:i:s');
            }
        }
        unset($customer['attachmentkey']);
        return $customer;
    }

    public function returnData($data) {
        if (is_array($data)) {
            $data_json = json_encode($data);
            return $data_json;
        }
        return $data;
    }
}