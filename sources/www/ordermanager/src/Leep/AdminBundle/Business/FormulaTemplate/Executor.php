<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

class Executor {
    public $container = null;
    public $errors = array();
    public $output = array();
    public $env = null;
    public function __construct($container) {
        $this->container = $container;

        $formulaTemplateDir = $container->getParameter('kernel.root_dir').'/cache/formula_templates';
        if (!is_dir($formulaTemplateDir)) {
            @mkdir($formulaTemplateDir);
            @mkdir($formulaTemplateDir.'/templates');
            @mkdir($formulaTemplateDir.'/templates_cache');
        }

        $loader = new \Twig_Loader_Filesystem($formulaTemplateDir.'/templates');
        $this->env = new \Twig_Environment($loader, array(
            'cache' => $formulaTemplateDir.'/templates_cache',
            'auto_reload' => false
        ));
        $this->env->addExtension(new FormulaExtension($this->container));
        $this->env->addExtension(new OrderGeneratorExtension($this->container));
    }

    public function execute($formula, $input, $isInclude = false) {
        $this->output = $input;
        $this->compile($formula, $input, $isInclude);
    }

    public function processInput($input) {
        foreach ($input as $k => $v) {
            $v = trim($v);
            if (strlen($v) >= 2) {
                if ($v[0] == '[' && $v[strlen($v)-1] == ']') {
                    if ($v[1] == '[' && $v[strlen($v)-2] == ']') {

                    }
                    else {
                        $arr = explode(',', substr($v, 1, strlen($v)-2));
                        $v = array();
                        foreach ($arr as $a) {
                            $v[] = trim($a);
                        }
                        $input[$k] = $v;
                    }
                }
            }
        }
        return $input;
    }

    public function compile($formula, $input, $isInclude) {
        set_time_limit(600);
        ini_set('memory_limit', '3048M');
        $formulaTemplateDir = $this->container->getParameter('kernel.root_dir').'/cache/formula_templates/templates';
        $formula.="
{% for k, v in _context %}
  {% if k != '_parent' or (_context['_disable_process_output'] is defined and k in _context['_disable_process_output']) %}
    {{ k }} = {{ processOutput(v) | raw }}
  {% endif %}
{% endfor %}";

        $result = '';
        try {
            $processedInput = $this->processInput($input);

            $templateName = sha1($formula).'.tpl.twig';
            @file_put_contents($formulaTemplateDir.'/'.$templateName, $formula);
            $result = $this->env->render($templateName, $processedInput);
        }
        catch (\Exception $e) {
            $baseException = $e->getPrevious();
            if ($baseException instanceof Exception\FormulaDebugException) {
                $this->errors[] = "VARIABLES DATA AT LINE ".$e->getTemplateLine().":";
                $this->errors[] = $baseException->getMessage();
            }
            else {
                $this->errors[] = "Error at line: ".$e->getTemplateLine();
                $this->errors[] = "Message: ".$e->getRawMessage();
            }
        }

        $v = explode("\n", $result);
        foreach ($v as $line) {
            $line = trim($line);
            if (!empty($line)) {
                $arr = explode('=', $line);
                if (isset($arr[1])) {
                    $k = trim($arr[0]);
                    $p = strpos($line, '=');
                    $v = trim(substr($line, $p + 1));
                    if ($k != '_parent') {
                        $this->output[$k] = $v;
                    }
                }
            }
        }

        if ($isInclude) {
            $this->output = $this->processInput($this->output);
        }
    }
}
