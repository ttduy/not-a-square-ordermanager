<?php
namespace Leep\AdminBundle\Business\FormulaTemplate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormulaTemplate', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        $model->sortOrder = $entity->getSortOrder();
        $model->formula = $entity->getFormula();
        $model->testInputValue = $entity->getTestInputValue();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));        
        $builder->add('formula', 'textarea', array(
            'label'    => 'Formula',
            'required' => true,
            'attr'     => array(
                'rows' => 30, 'cols' => 130,
            )
        ));        
        $builder->add('testInputValue', 'textarea', array(
            'label'    => 'Test Input Value',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 80
            )
        ));

        // Versioning
        $builder->add('sectionVersioning', 'section', array(
            'label'    => 'Versioning',
            'property_path' => false
        ));
        $builder->add('isMakeVersion', 'checkbox', array(
            'label'    => 'Make a version?',
            'required' => false,
        ));
        $builder->add('versionAlias', 'text', array(
            'label'    => 'Version alias',
            'required' => false,
        ));
        $builder->add('changesNote', 'textarea', array(
            'label'    => 'Changes notes',
            'required' => false,
            'attr'     => array(
                'rows' => 5, 'cols' => 80
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $this->entity->setName($model->name);
        $this->entity->setSortOrder($model->sortOrder);
        $this->entity->setFormula($model->formula);
        $this->entity->setTestInputValue($model->testInputValue);
        
        if ($model->isMakeVersion) {
            // Check version alias
            if ($model->versionAlias != '') {
                $formulaTemplateVersion = $em->getRepository('AppDatabaseMainBundle:FormulaTemplateVersion')->findOneBy(array(
                    'idFormulaTemplate' => $this->entity->getId(),
                    'versionAlias'      => trim($model->versionAlias)
                ));
                if (!empty($formulaTemplateVersion)) {
                    $this->errors[] = "Alias '".$model->versionAlias."' is already existed!";
                    return;
                }
            }

            // Max number
            $query = $em->createQueryBuilder();
            $query->select('MAX(p.number) AS number') 
                ->from('AppDatabaseMainBundle:FormulaTemplateVersion', 'p')
                ->andWhere('p.idFormulaTemplate = :idFormulaTemplate')
                ->setParameter('idFormulaTemplate', $this->entity->getId());
            $number = $query->getQuery()->getSingleScalarResult();

            // Make new record
            $formulaTemplateVersion = new Entity\FormulaTemplateVersion();
            $formulaTemplateVersion->setIdFormulaTemplate($this->entity->getId());
            $formulaTemplateVersion->setFormula($model->formula);
            $formulaTemplateVersion->setTestInput($model->testInputValue);
            $formulaTemplateVersion->setVersionAlias($model->versionAlias);
            $formulaTemplateVersion->setTimestamp(new \DateTime());
            $formulaTemplateVersion->setChangesNote($model->changesNote);
            $formulaTemplateVersion->setNumber($number + 1);
            $em->persist($formulaTemplateVersion);
            $em->flush();

            $this->messages[] = "Make new version, number = ".($number+1);
        }

        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}