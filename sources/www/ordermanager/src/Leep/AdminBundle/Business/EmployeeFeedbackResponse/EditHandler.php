<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackResponse;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmployeeFeedbackResponse', 'app_main')->findOneById($id);
    }

    public $feedbackQuestion = null;
    public function getFeedbackQuestion() {
        if ($this->feedbackQuestion == null) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $this->feedbackQuestion = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findOneById($this->entity->getIdEmployeeFeedbackQuestion());
        }
        return $this->feedbackQuestion;
    }

    public function convertToFormModel($entity) {
        $data = array();
        $q = $this->getFeedbackQuestion();
        $data['answer'] = json_decode($entity->getAnswer(), true);
        
        return $data;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $q = $this->getFeedbackQuestion();
        $builder->add('sectionQuestion', 'section', array(
            'label'  => 'Question: '.$q->getQuestion(), 
            'property_path' => false
        ));
        switch ($q->getIdType()) {
            case Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_TEXT:
                $builder->add('answer', 'textarea', array(
                    'label'  => 'Answer'
                ));
                break;
            case Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE:
                $choices = Business\EmployeeFeedbackQuestion\Util::parseChoices($q->getData());
                $builder->add('answer', 'choice', array(
                    'label'  => 'Answer',
                    'choices' => $choices
                ));
                break;
            case Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE:
                $choices = Business\EmployeeFeedbackQuestion\Util::parseChoices($q->getData());
                $builder->add('answer', 'choice', array(
                    'label'  => 'Answer',
                    'multiple' => 'multiple',
                    'choices' => $choices
                ));
                break;
        }
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $q = $this->getFeedbackQuestion();
        $this->entity->setAnswer(json_encode($model['answer']));
        
        parent::onSuccess();
    }
}
