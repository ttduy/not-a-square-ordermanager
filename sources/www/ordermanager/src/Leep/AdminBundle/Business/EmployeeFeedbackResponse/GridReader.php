<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackResponse;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $employeeFeedbackQuestion;
    public $choices = array();
    public function getColumnMapping() {
        return array('responseTime', 'idUser', 'answer', 'action');
    }
    
    public function __construct($container) {
        parent::__construct($container);

        $id = $this->container->get('request')->get('id', 0);
        $this->employeeFeedbackQuestion = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findOneById($id);
        $this->choices = Business\EmployeeFeedbackQuestion\Util::parseChoices($this->employeeFeedbackQuestion->getData());
    }

    public function getTableHeader() {
        return array(        
            array('title' => 'Date',        'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Name',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Answer',      'width' => '45%', 'sortable' => 'false'),  
            array('title' => 'Action',      'width' => '20%', 'sortable' => 'false'),          
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmployeeFeedbackResponseGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmployeeFeedbackResponse', 'p');
        $queryBuilder->andWhere('p.idEmployeeFeedbackQuestion = :idEmployeeFeedbackQuestion')
            ->setParameter('idEmployeeFeedbackQuestion', $this->container->get('request')->get('id', 0));
    }

    public function buildCellAnswer($row) {
        if ($this->employeeFeedbackQuestion->getIdType() == Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_TEXT) {
            return nl2br(json_decode($row->getAnswer()));
        }
        else if ($this->employeeFeedbackQuestion->getIdType() == Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE) {
            $choiceId = json_decode($row->getAnswer());
            return isset($this->choices[$choiceId]) ? $this->choices[$choiceId] : $choiceId;
        }
        else if ($this->employeeFeedbackQuestion->getIdType() == Business\EmployeeFeedbackQuestion\Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE) {
            $choiceArr = array();
            $choices = json_decode($row->getAnswer(), true);
            foreach ($choices as $choiceId) {
                if (isset($this->choices[$choiceId])) {
                    $choiceArr[] = $this->choices[$choiceId];
                }
            }
            return implode("<br/>", $choiceArr);
        }
        return '';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('employeeFeedbackResponseModify')) {
            if ($userManager->getUser()->getUsername() == 'Michael.huttegger') {
                $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'employee_feedback_response', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            }
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'employee_feedback_response', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
            
        return $builder->getHtml();
    }
}
