<?php 
namespace Leep\AdminBundle\Business\EmployeeFeedbackResponse;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $id = $this->container->get('request')->query->get('id', 0);
        $feedbackResponse = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackResponse')->findOneById($id);

        $idFeedbackQuestion = $feedbackResponse->getIdEmployeeFeedbackQuestion();

        $em->remove($feedbackResponse);
        $em->flush();


        return new RedirectResponse($mgr->getUrl('leep_admin', 'employee_feedback_response', 'grid', 'list', array('id' => $idFeedbackQuestion)));
    }
}