<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class NewsletterEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');
		$this->inputs = array(
				'sectionNewsletterRecipient'	=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Newsletter Recipients'
																		)
												),
				'toNewsletterRecipient'			=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Newsletter Recipients',
																            'required' => false
																		)
												),
				'toNewsletterRecipientFilterByDistributionChannel'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Distribution Channel',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
                'toNewsletterRecipientFilterByPartner'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Partner',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
				'toNewsletterRecipientFilterByTimeSinceLastOrder'	=>	array(
														'type'		=> 'text',
														'setting'	=> array(
																            'label'    	=> 'Time Since Last Order',
																            'required' 	=> false
																		)
												),
                'toNewsletterRecipientFilterByDCType'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Distribution Channel Type',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
			);
	}
}