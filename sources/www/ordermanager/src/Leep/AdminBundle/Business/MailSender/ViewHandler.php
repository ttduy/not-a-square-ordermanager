<?php
namespace Leep\AdminBundle\Business\MailSender;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');
        $doctrine = $this->container->get('doctrine');

        $id = $this->container->get('request')->query->get('id', 0);

        $sendToParams = array(
            'receiverType'  =>  Business\MailSender\Util::RECEIVER_TYPE_CUSTOMER,
            'idEmailPool'   =>  $id
        );

        $sendToUrl = $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'viewRecievers', $sendToParams);
        $vieEmailContentUrl = $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'viewEmailContent', array('id' => $id));

        $targetBlankUrlTpl = '<a href="%s" target="_blank">%s</a>'; 
        $sendToValue = '';
        
        $emailPool = $doctrine->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($id);

        // prepare receivers info
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $noReceivers = $query->select('count(p.id)')
            ->from('AppDatabaseMainBundle:EmailPoolReceiver', 'p')
            ->andWhere('p.idEmailPool = :idEmailPool')
            ->setParameter('idEmailPool', $id)
            ->getQuery()
            ->getSingleScalarResult();

        if ($noReceivers > 0) {
            $sendToValue = sprintf('<a href="%s" target="_blank">%s receiver(s)</a>', $sendToUrl, $noReceivers);
        }

        // prepare attachments
        $attachmentHtml = '';
        $attachmentTpl = '<a href="%s/%s?t=%s" target="_blank">%s</a>';
        $attachments = $doctrine->getRepository('AppDatabaseMainBundle:EmailPoolAttachment')->findByIdEmailPool($id);
        $attachmentURL = $this->container->getParameter('attachment_url') . '/'. Util::EMAIL_ATTACHMENT_FOLDER;

        if ($attachments) {
            foreach ($attachments as $attachment) {
                $today = new \DateTime();

                if ($attachmentHtml) {
                    $attachmentHtml .= '<br/>';
                }

                $attachmentHtml .= sprintf($attachmentTpl, $attachmentURL, $attachment->getName(), $today->format('YmdHis'), $attachment->getName());
            }
        }

        $data[] = $this->addSection('Email');
        $data[] = $this->add('Subject', $emailPool->getSubject());
        $data[] = $this->add('Sent to', $sendToValue);
        $data[] = $this->add('Content', sprintf($targetBlankUrlTpl, $vieEmailContentUrl, 'View'));
        $data[] = $this->add('Attachments', $attachmentHtml);
        $data[] = $this->add('Created At', $emailPool->getCreatedAt()->format('d/m/Y H:i:s'));

        return $data;
    }
}
