<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

abstract class AbstractEmailSenderInput {
	protected $container;
	protected $inputs = array();

	function __construct($container) {
		$this->container = $container;
		$this->config();
	}

	function build(&$builder) {
		foreach ($this->inputs as $inputName => $inputConfig) {
			$builder->add($inputName, $inputConfig['type'], $inputConfig['setting']);
		}
	}

	function getIdInputs() {
		return array_keys($this->inputs);
	}

	abstract function config();
}