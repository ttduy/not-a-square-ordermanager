<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class AcquisiteurEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toAcquisiteur']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Acquisiteur', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($filters['toAcquisiteurFilterByDistributionChannel'] || $filters['toAcquisiteurFilterByDCType']) {
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.acquisiteurid1 = p.id OR dc.acquisiteurid2 = p.id OR dc.acquisiteurid3 = p.id');

                $filterByDCs = $filters['toAcquisiteurFilterByDistributionChannel'];
                $filterByDCTypes = $filters['toAcquisiteurFilterByDCType'];

                if ($filterByDCs) {
                    $query->andWhere('dc.id IN (:dcIds)')
                    ->setParameter('dcIds', $filterByDCs);                    
                }

                if ($filterByDCTypes) {
                    $query->andWhere('dc.typeid IN (:dcTypeIds)')
                    ->setParameter('dcTypeIds', $filterByDCTypes);                    
                }
            }

            if ($filters['toAcquisiteurFilterByPartner']) {
                $query->innerJoin('AppDatabaseMainBundle:Partner', 'partner', 'WITH', 'partner.acquisiteurid1 = p.id OR partner.acquisiteurid2 = p.id OR partner.acquisiteurid3 = p.id')
                    ->andWhere('partner.id IN (:partnerIds)')
                    ->setParameter('partnerIds', $filters['toAcquisiteurFilterByPartner']);
            }

            if ($filters['toAcquisiteurFilterByTimeSinceLastOrder']) {
                // TO BE IMPLEMENTED LATER
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }
/*
            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 1
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }
*/
            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                //$countries = explode(',', $filters['filterByCountries']);
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }
/*
            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                //$languages = explode(',', $filters['filterByLanguages']);
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }
            */
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_ACQUISITEUR;
                        $item['name'] = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $item['email'] = trim($row->getContactemail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => false,
                                                    'isNutrimeOffered'          => false,
                                                    'isNotContacted'            => $row->getIsNotContacted()
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}