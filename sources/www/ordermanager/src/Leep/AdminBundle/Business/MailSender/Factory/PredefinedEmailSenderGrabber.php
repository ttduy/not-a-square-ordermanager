<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PredefinedEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();
        $request = $this->container->get('request');

        $arrItems = array();

        if ($filters['toPredefinedList']) {
            $rows = explode("\n", $filters['toPredefinedList']);
            foreach ($rows as $row) {
                if (trim($row) != '') {
                    $receiverInfo = $this->parseEmail(trim($row));
                    $arrItems[$receiverInfo['email']] = $receiverInfo;
                }
            }
        }

        return ($getTotalOnly ? sizeof($arrItems) : $arrItems);
	}

    function parseEmail($emailRow) {
        $receiverInfo = Business\MailSender\Util::getReceiverDataTemplate();
        $receiverInfo['type'] = Business\MailSender\Util::RECEIVER_TYPE_PREDEFINED_RECIPIENT;
        $receiverInfo['email'] = $emailRow;

        $arrParts = explode('<', $emailRow);

        if (sizeOf($arrParts) == 2) {
            $receiverInfo['name'] = trim($arrParts[0]);

            $closingSignPos = strpos($arrParts[1], '>');
            $email = $arrParts[1];
            if ($closingSignPos) {
                $email = substr($arrParts[1], 0, $closingSignPos);
            }
            $receiverInfo['email'] = trim($email);
        }

        return $receiverInfo;
    }
}