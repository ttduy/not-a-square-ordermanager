<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class EmailSenderFactory {
    private $container;
    function __construct($container) {
        $this->container = $container;
    }

    public $registry = array(
        Business\MailSender\Util::RECEIVER_TYPE_CUSTOMER => array(
            'EmailGrabber' => array(
                'leep_admin.mail_sender.business.customer_email_sender_grabber'
            ),
            'Input'         => 'leep_admin.mail_sender.business.customer_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.customer_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_DISTRIBUTION_CHANNEL => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.distribution_channel_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.distribution_channel_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.distribution_channel_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_PARTNER => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.partner_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.partner_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.partner_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_LEAD => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.lead_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.lead_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.lead_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_ACQUISITEUR => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.acquisiteur_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.acquisiteur_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.acquisiteur_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_NEWSLETTER_RECIPIENT => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.newsletter_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.newsletter_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.newsletter_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_PRESS => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.press_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.press_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.press_email_sender_unsubscriber'
        ),
        Business\MailSender\Util::RECEIVER_TYPE_PREDEFINED_RECIPIENT => array(
            'EmailGrabber' => array('leep_admin.mail_sender.business.predefined_email_sender_grabber'),
            'Input'         => 'leep_admin.mail_sender.business.predefined_email_sender_input',
            'Unsubscriber'  => 'leep_admin.mail_sender.business.predefined_email_sender_unsubscriber'
        )
    );

    public function createEmailGrabber($idType) {
        if (isset($this->registry[$idType])) {
            $arrEmailGrabbers = array();
            $input = $this->createInput($idType);
            foreach ($this->registry[$idType]['EmailGrabber'] as $eachGrabber) {
                $eachGrabber = $this->container->get($eachGrabber);
                $eachGrabber->setInput($input);
                $arrEmailGrabbers[] = $eachGrabber;
            }
            return $arrEmailGrabbers;
        }
        return null;
    }

    public function createInput($idType) {
        if (isset($this->registry[$idType])) {
            return $this->container->get($this->registry[$idType]['Input']);
        }
        return null;
    }

    public function createUnsubscriber($idType) {
        if (isset($this->registry[$idType])) {
            return $this->container->get($this->registry[$idType]['Unsubscriber']);
        }
        return null;
    }

    public function getRequestParams($postData = null) {     
        $filters = array();
        foreach ($this->registry as $key => $eachRegistry) {
            $input = $this->createInput($key);
            if ($input) {
                foreach ($input->getIdInputs() as $idInput) {
                    $filters[$idInput] = isset($postData[$idInput]) ? $postData[$idInput] : null;
                }
            }
        }
        return $filters;
    }
}