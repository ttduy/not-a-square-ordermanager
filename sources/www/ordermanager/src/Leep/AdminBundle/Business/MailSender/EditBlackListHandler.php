<?php
namespace Leep\AdminBundle\Business\MailSender;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditBlackListHandler extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }
    public function convertToFormModel($entity) { 
        $emails = implode("\r\n", $this->getBlackList());
        return array('emails' => $emails);
    }

    private function getBlackList() {
        $arrEmails = array();

        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrEmails[$entry->getId()] = $entry->getEmail();
            }
        }

        return $arrEmails;
    }

    public function buildForm($builder) {
        $builder->add('sectionBlackList', 'section', array(
            'label' => 'BlackList'
        ));

        $builder->add('emails', 'textarea', array(
            'label'     => 'BlackList',
            'required'  => false,
            'attr'      => array('cols' => 50, 'rows' => 20)
        ));        

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        if ($model['emails']) {
            // get in-DB black list
            $arrEmailsInDB = $this->getBlackList();

            // get just-submited black list
            $arrEmailsSubmited = explode("\r\n", $model['emails']);

            // get new ones
            $arrNewOnes = array_diff($arrEmailsSubmited, $arrEmailsInDB);

            // get to be removed ones
            $arrToBeRemoved = array_diff($arrEmailsInDB, $arrEmailsSubmited);

            foreach ($arrNewOnes as $entry) {
                // avoid empty line
                $entry = trim($entry);
                if ($entry) {
                    $newOne = new Entity\EmailOptOutList();
                    $newOne->setEmail($entry);
                    $em->persist($newOne);                    
                }
            }

            $em->flush();

            foreach ($arrToBeRemoved as $entry) {
                $queryDel = $em->createQueryBuilder();
                $queryDel->delete('AppDatabaseMainBundle:EmailOptOutList', 'p')
                        ->andWhere('p.id IN (:ids)')
                        ->setParameter('ids', array_keys($arrToBeRemoved))
                        ->getQuery()
                        ->execute();
            }

        } else {
            // remove all black list
            $queryDel = $em->createQueryBuilder();
            $queryDel->delete('AppDatabaseMainBundle:EmailOptOutList', 'p')
                    ->getQuery()
                    ->execute();
        }

        $this->messages[] = 'The record has been updated successfully';
    }
}