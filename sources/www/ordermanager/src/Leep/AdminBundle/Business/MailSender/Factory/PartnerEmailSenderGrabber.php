<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PartnerEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toPartner']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Partner', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($filters['toPartnerFilterByDistributionChannel']) {
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.partnerid = p.id')
                    ->andWhere('dc.id IN (:dcIds)')
                    ->setParameter('dcIds', $filters['toPartnerFilterByDistributionChannel']);
            }

            if ($filters['toPartnerFilterByDistributionChannelCity']) {
                $val = $filters['toPartnerFilterByDistributionChannelCity'];
                $query->andWhere('p.city LIKE :partnerCity')
                    ->setParameter('partnerCity', '%' . $val . '%');
            }

            if ($filters['toPartnerFilterByAcquisiteur']) {
                $query->andWhere('p.acquisiteurid1 IN (:acquisiteurIds) OR p.acquisiteurid2 IN (:acquisiteurIds) OR p.acquisiteurid3 IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $filters['toPartnerFilterByAcquisiteur']);
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }
/*
            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }
            */
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_PARTNER;
                        $item['name'] = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $item['email'] = trim($row->getContactemail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => $row->getIsCustomerBeContacted(),
                                                    'isNutrimeOffered'          => $row->getIsNutrimeOffered(),
                                                    'isNotContacted'            => $row->getIsNotContacted()
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}