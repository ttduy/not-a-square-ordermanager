<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class AcquisiteurEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneByContactemail($email);
        if ($acquisiteur) {
            $acquisiteur->setIsNotContacted(1);
            $em->persist($acquisiteur);
            $em->flush();
        }
    }
}