<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class CustomerEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByEmail($email);
        if ($customer) {
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($customer->getIdCustomerInfo());
            $customerInfo->setIsNotContacted(1);
            $em->persist($customerInfo);
            $em->flush();
        }
    }
}
