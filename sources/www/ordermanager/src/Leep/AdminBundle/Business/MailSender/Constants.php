<?php
namespace Leep\AdminBundle\Business\MailSender;

class Constants {
    const MANDRILL_MESSAGE_TAG = 'om_message_tag';

    const EMAIL_POOL_SNS_LOG_TYPE_DELIVERY  = 1;    
    const EMAIL_POOL_SNS_LOG_TYPE_BOUNCE    = 2;
    const EMAIL_POOL_SNS_LOG_TYPE_COMPLAINT = 3;
    const EMAIL_POOL_SNS_LOG_TYPE_UNKNOWN   = 99;

    public static function getEmailPoolSNSLogTypes() {
        return array(
                self::EMAIL_POOL_SNS_LOG_TYPE_DELIVERY              =>  'Delivery',
                self::EMAIL_POOL_SNS_LOG_TYPE_BOUNCE             	=>  'Bounce',
                self::EMAIL_POOL_SNS_LOG_TYPE_COMPLAINT     		=>  'Complaint',
                self::EMAIL_POOL_SNS_LOG_TYPE_UNKNOWN               =>  'Unknown'
            );
    }

    const EMAIL_POOL_SENDING_LOG_TYPE_SENT          = 10;
    const EMAIL_POOL_SENDING_LOG_TYPE_OPEN          = 20;
    const EMAIL_POOL_SENDING_LOG_TYPE_CLICK         = 30;
    const EMAIL_POOL_SENDING_LOG_TYPE_HARD_BOUNCE   = 40;
    const EMAIL_POOL_SENDING_LOG_TYPE_SOFT_BOUNCE   = 50;
    const EMAIL_POOL_SENDING_LOG_TYPE_REJECT        = 60;
    const EMAIL_POOL_SENDING_LOG_TYPE_COMPLAINT     = 70;
    const EMAIL_POOL_SENDING_LOG_TYPE_UNSUB         = 80;
    const EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_OPEN   = 90;
    const EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_CLICK  = 100;

    public static function getEmailPoolSendingLogTypes() {
        return array(
                self::EMAIL_POOL_SENDING_LOG_TYPE_SENT              =>  'Sent',
                self::EMAIL_POOL_SENDING_LOG_TYPE_OPEN              =>  'Opens',
                self::EMAIL_POOL_SENDING_LOG_TYPE_CLICK             =>  'Clicks',
                self::EMAIL_POOL_SENDING_LOG_TYPE_HARD_BOUNCE       =>  'Hard Bounces',
                self::EMAIL_POOL_SENDING_LOG_TYPE_SOFT_BOUNCE       =>  'Soft Bouces',
                self::EMAIL_POOL_SENDING_LOG_TYPE_REJECT            =>  'Rejects',
                self::EMAIL_POOL_SENDING_LOG_TYPE_COMPLAINT         =>  'Complaints',
                self::EMAIL_POOL_SENDING_LOG_TYPE_UNSUB             =>  'Unsubs',
                self::EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_OPEN       =>  'Unique Opens',
                self::EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_CLICK      =>  'Unique Clicks'
            );
    }
}