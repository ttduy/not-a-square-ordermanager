<?php
namespace Leep\AdminBundle\Business\MailSender;

use Easy\ModuleBundle\Module\AbstractHook;

class EmailHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
    	$emailSenderFactory = $controller->get('leep_admin.mail_sender.business.email_sender_factory');
    	
        $arrTabs = array();

        foreach (array_keys(Util::getReceiverTypeList()) as $eachReceiverType) {
            $input = $emailSenderFactory->createInput($eachReceiverType);

            if ($input) {
            	$arrTabs[$eachReceiverType] = array(
            											'input' => $input->getIdInputs(),
            											'title'	=> Util::getReceiverTypeList()[$eachReceiverType]
            										);
            }
        }

        $data['whoToTabs'] = $arrTabs;
        $data['countReceiversURL'] = $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'countRecievers');
    }
}
