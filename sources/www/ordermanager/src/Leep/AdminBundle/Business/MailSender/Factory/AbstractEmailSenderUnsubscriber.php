<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

abstract class AbstractEmailSenderUnsubscriber {
    protected $container;
    function __construct($container) {
        $this->container = $container;
    }

    public function unsubscribe($email) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $email = trim($email);

        $e = $em->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findOneByEmail($email);
        if (empty($e)) {
            $emailOptOutList = new Entity\EmailOptOutList();
            $emailOptOutList->setEmail($email);
            $em->persist($emailOptOutList);
            $em->flush();
        }
    }
}
