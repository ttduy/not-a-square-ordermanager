<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class LeadEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toLead']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactEmail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactEmail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Lead', 'p')
                ->andWhere('p.contactEmail IS NOT NULL');

            if ($filters['toLeadFilterByDistributionChannel']) {
                $query->andWhere('p.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $filters['toLeadFilterByDistributionChannel']);
            }

            if ($filters['toLeadFilterByPartner']) {
                $query->andWhere('p.idPartner IN (:partnerIds)')
                    ->setParameter('partnerIds', $filters['toLeadFilterByPartner']);
            }

            if ($filters['toLeadFilterByAcquisiteur']) {
                $query->andWhere('p.idAcquisiteur IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $filters['toLeadFilterByAcquisiteur']);
            }
/*
            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(ci.isNotContacted = 0 OR ci.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 1
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(ci.isCustomerBeContacted = 1)');
            }
            */
            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                //$countries = explode(',', $filters['filterByCountries']);
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('countries', $countries);
            }
/*
            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                //$languages = explode(',', $filters['filterByLanguages']);
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }
*/
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_LEAD;
                        $item['name'] = trim($row->getFirstName() . ' ' . $row->getSurName());
                        $item['email'] = trim($row->getContactEmail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => false,
                                                    'isNutrimeOffered'          => false,
                                                    'isNotContacted'            => false
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}