<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class DistributionChannelEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('REGEXP', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Regexp');

        $comparatorTypeFactory = $this->container->get('leep_admin.comparator_type.comparator_type_factory');

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toDistributionChannel']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:DistributionChannels', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($filters['toDistributionChannelFilterByPartner']) {
                $query->andWhere('p.partnerid IN (:partnerIds)')
                    ->setParameter('partnerIds', $filters['toDistributionChannelFilterByPartner']);
            }

            if ($filters['toDistributionChannelFilterByAcquisiteur']) {
                $query->andWhere('p.acquisiteurid1 IN (:acquisiteurIds) OR p.acquisiteurid2 IN (:acquisiteurIds) OR p.acquisiteurid3 IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $filters['toDistributionChannelFilterByAcquisiteur']);
            }

            if ($filters['toDistributionChannelFilterByRating']) {
                $query->andWhere('p.rating IN (:selectedRatings)')
                    ->setParameter('selectedRatings', $filters['toDistributionChannelFilterByRating']);
            }

            // Amount of Customers
            if ($comparatorTypeFactory->hasSelectComparatorType($filters['toDistributionChannelFilterByCustomerAmount'])) {
                $val = $filters['toDistributionChannelFilterByCustomerAmount'];

                $q = $em->createQueryBuilder();
                $q->select('p.idDistributionChannel')
                    ->from('AppDatabaseMainBundle:CustomerInfo', 'p')
                    ->groupBy('p.idDistributionChannel');

                $comparatorTypeFactory->buildQuery($q, 'COUNT(p.id)', $val, 'having');
                $result = $q->getQuery()->getResult();

                $arrDC = array(0);
                if ($result) {
                    foreach ($result as $entry) {
                        $arrDC[] = $entry['idDistributionChannel'];
                    }
                }

                $query->andWhere('p.id IN (:arrDCByCustomerTotals)')
                    ->setParameter('arrDCByCustomerTotals', $arrDC);
            }

            // sold/not sold Products
            // Ordered Products filter && Not Ordered Products filter
            if ($filters['toDistributionChannelFilterBySoldProducts'] || $filters['toDistributionChannelFilterByNotSoldProducts']) {
                $soldProducts = $filters['toDistributionChannelFilterBySoldProducts'];
                $notSoldProducts = $filters['toDistributionChannelFilterByNotSoldProducts'];
                $soldProductsNoItems = $filters['toDistributionChannelFilterByNoItemPerSoldProducts'];
                $soldProductsTopSell = $filters['toDistributionChannelFilterByIsTopSellSoldProducts'];

                if (!empty($soldProducts)) {
                    $query->innerJoin('AppDatabaseMainBundle:DistributionChannelsProductSell', 'ps', 'WITH', 'ps.idDistributionChannel = p.id')
                        ->andWhere('ps.idProduct IN (:soldProductIds)')
                        ->setParameter('soldProductIds', $soldProducts);

                    if ($soldProductsNoItems) {
                        $comparatorTypeFactory->buildQuery($query, 'ps.total', $soldProductsNoItems);
                    }

                    if ($soldProductsTopSell) {
                        $query->andWhere('ps.isTopSell = 1');
                    }
                }

                if (!empty($notSoldProducts)) {
                    $excludedDCIds = array(0);
                    $queryExcludedOrder = $em->createQueryBuilder();
                    $result = $queryExcludedOrder->select('p.distributionchannelid')
                                            ->from('AppDatabaseMainBundle:Customer', 'p')
                                            ->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                                            ->setParameter('regexp', ',' . implode('|', $notSoldProducts) . ',')
                                            ->groupBy('p.distributionchannelid')
                                            ->getQuery()
                                            ->getResult();
                    if ($result) {
                        foreach ($result as $entry) {
                            if ($entry['distributionchannelid']) {
                                $excludedDCIds[] = $entry['distributionchannelid'];
                            }
                        }
                    }
                    
                    $query->andWhere('p.id NOT IN (:notOrderedProductexcludedDCIds)')
                            ->setParameter('notOrderedProductexcludedDCIds', $excludedDCIds);
                }
            }

            if ($filters['toDistributionChannelFilterByWithComplaints']) {
                $q = $em->createQueryBuilder();
                $result = $q->select('p.idDistributionChannel')
                            ->from('AppDatabaseMainBundle:DistributionChannelsComplaint', 'p')
                            ->groupBy('p.idDistributionChannel')
                            ->getQuery()
                            ->getResult();

                $arrDCWithComplaintIds = array(0);
                if ($result) {
                    foreach ($result as $entry) {
                        $arrDCWithComplaintIds[] = $entry['idDistributionChannel'];
                    }
                }

                $query->andWhere('p.id IN (:arrDCWithComplaintIds)')
                    ->setParameter('arrDCWithComplaintIds', $arrDCWithComplaintIds);
            }

            if ($filters['toDistributionChannelFilterByWithFeedbacks']) {
                $query->andWhere('p.numberFeedback IS NOT NULL AND p.numberFeedback > 0');
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                //$countries = explode(',', $filters['filterByCountries']);
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }
/*
            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                //$languages = explode(',', $filters['filterByLanguages']);
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }
*/
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_DISTRIBUTION_CHANNEL;
                        $item['name'] = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $item['email'] = trim($row->getContactemail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => $row->getIsCustomerBeContacted(),
                                                    'isNutrimeOffered'          => $row->getIsNutrimeOffered(),
                                                    'isNotContacted'            => $row->getIsNotContacted()
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}