<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class NewsletterEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toNewsletterRecipient']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.email)');
            } else {
                $query->select('p')
                    ->orderBy('p.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:NewsletterRecipient', 'p')
                ->andWhere('p.email IS NOT NULL');

            if ($filters['toNewsletterRecipientFilterByDistributionChannel'] || $filters['toNewsletterRecipientFilterByDCType']) {
                $filterByDCs = $filters['toNewsletterRecipientFilterByDistributionChannel'];
                $filterByDCTypes = $filters['toNewsletterRecipientFilterByDCType'];

                if ($filterByDCs) {
                    $query->andWhere('p.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $filterByDCs);                    
                }

                if ($filterByDCTypes) {
                    $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.idDistributionChannel');

                    $query->andWhere('dc.typeid IN (:dcTypeIds)')
                    ->setParameter('dcTypeIds', $filterByDCTypes);                    
                }
            }

            if ($filters['toNewsletterRecipientFilterByPartner']) {
                $query->andWhere('p.idPartner IN (:partnerIds)')
                    ->setParameter('partnerIds', $filters['toNewsletterRecipientFilterByPartner']);
            }

            if ($filters['toNewsletterRecipientFilterByTimeSinceLastOrder']) {
                // TO BE IMPLEMENTED LATER
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }
            
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_NEWSLETTER_RECIPIENT;
                        $item['name'] = trim($row->getFirstName() . ' ' . $row->getSurName());
                        $item['email'] = trim($row->getEmail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => false,
                                                    'isNutrimeOffered'          => false,
                                                    'isNotContacted'            => $row->getIsNotContacted()
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}