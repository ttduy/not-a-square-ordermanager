<?php
namespace Leep\AdminBundle\Business\MailSender;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('subject', 'progress', 'emailRead', 'status', 'createdAt', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Subject',            'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Progress',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Email read',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status',             'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Created At',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '10%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_MailSenderGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailPool', 'p');

        if (trim($this->filters->subject) != '') {
            $queryBuilder->andWhere('p.subject LIKE :subject')
                ->setParameter('subject', '%'.trim($this->filters->subject).'%');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.createdAt', 'DESC');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'mail_sender', 'view', 'view', array('id' => $row->getId())), 'button-detail');
        $builder->addPopupButton('Send Log', $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'sendLog', array('id' => $row->getId())), 'button-activity');
        $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'mail_sender', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');

        return $builder->getHtml();
    }

    public function buildCellProgress($row) {
        $tpl = '<font color="%s">%s %s</font><br/>';
        $html = '';
        $html .= sprintf($tpl, 'green', intval($row->getNumSending()), 'sending');
        $html .= sprintf($tpl, 'blue', intval($row->getNumSent() + $row->getNumQueued() + $row->getNumScheduled()), 'sent');
        $html .= sprintf($tpl, 'red', intval($row->getNumRejected() + $row->getNumInvalid() + $row->getNumIgnored() + $row->getNumFailed()), 'error');

        return $html;
    }

    public function buildCellEmailRead($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $tpl = '<b>%s</b> ';
        $numView = '<font>' . intval($row->getNumView()) . ' read</font>';
        $html = sprintf($tpl, $numView);

        $builder->addPopupButton('Receiver View Log', $mgr->getUrl('leep_admin', 'mail_sender', 'feature', 'receiverViewLog', array('id' => $row->getId())), 'button-email');
        $html .= $builder->getHtml();

        return $html;
    }

    public function buildCellStatus($row) {
        $mapping = $this->container->get('easy_mapping');
        //$builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $html = '';

        $html .= $mapping->getMappingTitle('LeepAdmin_Email_Pool_Status_List', $row->getStatus());

        //$html .= '<br/>' . $builder->getHtml();

        return $html;
    }
}