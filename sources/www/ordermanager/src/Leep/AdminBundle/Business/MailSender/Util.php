<?php
namespace Leep\AdminBundle\Business\MailSender;

use App\Database\MainBundle\Entity;

class Util {
    const EMAIL_POOL_RECEIVER_STATUS_SENDING         = 10;
    const EMAIL_POOL_RECEIVER_STATUS_SENT            = 20;
    const EMAIL_POOL_RECEIVER_STATUS_QUEUED          = 30;
    const EMAIL_POOL_RECEIVER_STATUS_SCHEDULED       = 40;
    const EMAIL_POOL_RECEIVER_STATUS_REJECTED        = 50;
    const EMAIL_POOL_RECEIVER_STATUS_INVALID         = 60;
    const EMAIL_POOL_RECEIVER_STATUS_IGNORED         = 70;
    const EMAIL_POOL_RECEIVER_STATUS_FAILED          = 80;

    public static function getEmailPoolReceiverStatusList() {
        return array(
                self::EMAIL_POOL_RECEIVER_STATUS_SENDING                     =>  'Sending',
                self::EMAIL_POOL_RECEIVER_STATUS_SENT                        =>  'Sent',
                self::EMAIL_POOL_RECEIVER_STATUS_QUEUED                      =>  'Queued',
                self::EMAIL_POOL_RECEIVER_STATUS_SCHEDULED                   =>  'Scheduled',
                self::EMAIL_POOL_RECEIVER_STATUS_REJECTED                    =>  'Rejected',
                self::EMAIL_POOL_RECEIVER_STATUS_INVALID                     =>  'Invalid',
                self::EMAIL_POOL_RECEIVER_STATUS_IGNORED                     =>  'Ignored',
                self::EMAIL_POOL_RECEIVER_STATUS_FAILED                      =>  'Failed'
            );
    }

    public static function getEmailPoolReceiverStatusListColor() {
        return array(
                self::EMAIL_POOL_RECEIVER_STATUS_SENDING                     =>  'green',
                self::EMAIL_POOL_RECEIVER_STATUS_SENT                        =>  'blue',
                self::EMAIL_POOL_RECEIVER_STATUS_QUEUED                      =>  'gray',
                self::EMAIL_POOL_RECEIVER_STATUS_SCHEDULED                   =>  'gray',
                self::EMAIL_POOL_RECEIVER_STATUS_REJECTED                    =>  'red',
                self::EMAIL_POOL_RECEIVER_STATUS_INVALID                     =>  'red',
                self::EMAIL_POOL_RECEIVER_STATUS_IGNORED                     =>  'red',
                self::EMAIL_POOL_RECEIVER_STATUS_FAILED                      =>  'red'
            );
    }

    const EMAIL_POOL_STATUS_OPEN                = 10;
    const EMAIL_POOL_STATUS_PROCESSING          = 20;
    const EMAIL_POOL_STATUS_PENDING_FOR_SENDING = 30;
    const EMAIL_POOL_STATUS_DONE                = 40;

    public static function getEmailPoolStatusList() {
        return array(
                self::EMAIL_POOL_STATUS_OPEN                    =>  'Open',
                self::EMAIL_POOL_STATUS_PROCESSING              =>  'Processing',
                self::EMAIL_POOL_STATUS_PENDING_FOR_SENDING     =>  'Pending for sending',
                self::EMAIL_POOL_STATUS_DONE                    =>  'Done'
            );
    }

    const EMAIL_ATTACHMENT_FOLDER = 'email';

    const RECEIVER_TYPE_CUSTOMER                = 10;
    const RECEIVER_TYPE_DISTRIBUTION_CHANNEL    = 20;
    const RECEIVER_TYPE_PARTNER                 = 30;
    const RECEIVER_TYPE_LEAD                    = 40;
    const RECEIVER_TYPE_NEWSLETTER_RECIPIENT    = 50;
    const RECEIVER_TYPE_PREDEFINED_RECIPIENT    = 60;
    const RECEIVER_TYPE_ACQUISITEUR             = 70;
    const RECEIVER_TYPE_PRESS                   = 80;

    public static function getReceiverTypeList() {
        return array(
                self::RECEIVER_TYPE_CUSTOMER                =>  'Customer',
                self::RECEIVER_TYPE_DISTRIBUTION_CHANNEL    =>  'Distribution Channel',
                self::RECEIVER_TYPE_PARTNER                 =>  'Partner',
                self::RECEIVER_TYPE_LEAD                    =>  'Lead',
                self::RECEIVER_TYPE_ACQUISITEUR             =>  'Acquisiteur',
                self::RECEIVER_TYPE_NEWSLETTER_RECIPIENT    =>  'Newsletter Recipient',
                self::RECEIVER_TYPE_PRESS                   =>  'Press',
                self::RECEIVER_TYPE_PREDEFINED_RECIPIENT    =>  'Predefined Recipient'
            );
    }

    public static function getFilterParams($container, $params = null) {
        $emailSenderFactory = $container->get('leep_admin.mail_sender.business.email_sender_factory');
        $tempData = $container->get('leep_admin.helper.temp_data');

        if (empty($params)) {        
            $key = $container->get('request')->get('key', '');
            $params = $tempData->get($key, array());
        }

        $filter = array();
        $commonParams = array(
            'includeOptedOutParties',
            'includeMustNotContactedParties',
            'filterByCountries',
            'filterByLanguages'
        );

        foreach ($commonParams as $p) {
            $filter[$p] = isset($params[$p]) ? $params[$p] : null;            
        }

        return array_merge($filter, $emailSenderFactory->getRequestParams($params));        
    }


    public static function getRequestParams($container, $arrData = null) {
        $tempData = $container->get('leep_admin.helper.temp_data');
        $key = $container->get('request')->get('key', '');
        $params = $tempData->get($key, array());

        $emailSenderFactory = $container->get('leep_admin.mail_sender.business.email_sender_factory');
        $constraints = array();

        $arrParams = array(
                                'includeOptedOutParties',
                                'includeMustNotContactedParties',
                                'filterByCountries',
                                'filterByLanguages'
                            );
        if ($arrData) {
            foreach ($arrParams as $entry) {
                $val = null;
                if (array_key_exists($entry, $arrData)) {
                    $val = $arrData[$entry];
                }
                $constraints[$entry] = $val;
            }
        } else {
            foreach ($arrParams as $entry) {
                $constraints[$entry] = isset($params[$entry]) ? $params[$entry] : '';
            }
        }

        return array_merge($constraints, $emailSenderFactory->getRequestParams($arrData));
    }

    public static function getReceiverTotal($container, $arrData = null) {
        $totalRow = 0;

        $emailSenderFactory = $container->get('leep_admin.mail_sender.business.email_sender_factory');
        $filters = Util::getFilterParams($container, $arrData);
        foreach (array_keys(Util::getReceiverTypeList()) as $eachReceiverType) {
            $emailSenderGrabbers = $emailSenderFactory->createEmailGrabber($eachReceiverType);
            if ($emailSenderGrabbers) {
                foreach ($emailSenderGrabbers as $eachEmailGrabber) {
                    // get total
                    $totalRow += $eachEmailGrabber->grab($filters, true);
                }
            }
        }

        return $totalRow;
    }

    public static function getReceiverDataTemplate() {
        return array(
                        'type'      => '', // customerInfo, customer, dc, partner, lead newletter
                        'email'     => '',
                        'name'      => '',
                        'others'    => array()
                    );
    }

    public static function getReceiverEmailListByType($container, $receiverType, $idEmailPool = null, $separator = '<br/>') {
        $emailList = '';

        $arrReceiverList = array();
        if (!$idEmailPool) {
            $emailSenderFactory = $container->get('leep_admin.mail_sender.business.email_sender_factory');
            $filters = Util::getFilterParams($container);

            // get email sender grabber
            $emailSenderGrabbers = $emailSenderFactory->createEmailGrabber($receiverType);

            if ($emailSenderGrabbers) {
                foreach ($emailSenderGrabbers as $eachEmailGrabber) {
                    $arrReceiverList = array_merge($arrReceiverList, $eachEmailGrabber->grab($filters));
                }
            }
        }
        else {
            
            $arrReceiverList = self::getReceiversByEmailPool($container, $idEmailPool, $receiverType);
        }

        // get email
        $arrEmails = array();
        foreach($arrReceiverList as $eachReceiver) {
            if (!array_key_exists($eachReceiver['email'], $arrEmails)) {
                $emailInfo = $eachReceiver['email'] . ($eachReceiver['name'] ? ' (' . $eachReceiver['name'] . ')' : '');

                if ($emailList) {
                    $emailList .= $separator;
                }
                $emailList .= $emailInfo;

                $arrEmails[$eachReceiver['email']] = 1;
            }
        }

        return $emailList;
    }

    public static function getReceiversByEmailPool($container, $idEmailPool, $type) {
        $em = $container->get('doctrine')->getEntityManager();
        $arrItems = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:EmailPoolReceiver', 'p')
            ->andWhere('p.type = :type')
            ->setParameter('type', $type)
            ->andWhere('p.idEmailPool = :idEmailPool')
            ->setParameter('idEmailPool', $idEmailPool)
            ->orderBy('p.email', 'ASC');

        $result = $query->getQuery()->getResult();

        if ($result) {
            foreach ($result as $row) {
                $item = self::getReceiverDataTemplate();
                $item['type'] = $row->getType();
                $item['name'] = $row->getName();
                $item['email'] = $row->getEmail();

                $arrItems[$item['email']] = $item;
            }
        }

        return $arrItems;
    }

    public static function getReceiverLogStatus($val) {
        // "sent", "queued", "scheduled", "rejected", or "invalid"
        $ind = array_search(ucfirst($val), self::getEmailPoolReceiverStatusList());
        return ($ind ? $ind : null);
    }

    public static function createEmailPoolLog($container, $idEmailPool, $logStatus, $receiverEmail, $logMessage = null, $receiverName = null) {
        $em = $container->get('doctrine')->getEntityManager();

        if (!$logMessage) {
            $logMessage = ($logStatus ? self::getEmailPoolReceiverStatusList()[$logStatus] : null);
        }

        // log - receiver info
        $logReceiver = $receiverEmail;
        if ($receiverName) {
            $logReceiver = sprintf('%s <%s>', $receiverName, $receiverEmail);
        }

        $sendingLog = new Entity\EmailPoolLog();
        $sendingLog->setIdEmailPool($idEmailPool);
        $sendingLog->setStatus($logStatus);
        $sendingLog->setMessage($logMessage);
        $sendingLog->setReceiver($logReceiver);
        $sendingLog->setCreatedAt(new \DateTime());
        $em->persist($sendingLog);
        $em->flush();
    }
}