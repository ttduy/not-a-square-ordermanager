<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class CustomerInfoEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toCustomer']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.email)');
            } else {
                $query->select('p')
                    ->orderBy('p.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:CustomerInfo', 'p')
                ->andWhere('p.email IS NOT NULL');

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 1
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_CUSTOMER;
                        $item['name'] = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $item['email'] = trim($row->getEmail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => $row->getIsCustomerBeContacted(),
                                                    'isNutrimeOffered'          => $row->getIsNutrimeOffered(),
                                                    'isNotContacted'            => $row->getIsNotContacted(),
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}