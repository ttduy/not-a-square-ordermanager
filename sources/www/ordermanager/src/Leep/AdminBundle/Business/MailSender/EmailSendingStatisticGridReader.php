<?php
namespace Leep\AdminBundle\Business\MailSender;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class EmailSendingStatisticGridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('time', 'sent', 'opens', 'clicks', 'hardBounces', 'softBounces', 'rejects', 'complaints', 'unsubs', 'uniqueOpens', 'uniqueClicks');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Time',                'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Sent',                'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Opens',               'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Clicks',              'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Hard Bounces',        'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Soft Bounces',        'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Rejects',             'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Complaints',          'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Unsubs',              'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Unique Opens',        'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Unique Clicks',       'width' => '8%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmailSendingStatisticGridReader');
    }

    public function buildQuery($queryBuilder) {
        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Date');

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailPoolSendingStatistic', 'p');

        if ($this->filters->startDate) {
            $queryBuilder->andWhere('DATE(p.time) >= :startDate')
                        ->setParameter('startDate', $this->filters->startDate);
        }

        if ($this->filters->endDate) {
            $queryBuilder->andWhere('DATE(p.time) >= :endDate')
                        ->setParameter('endDate', $this->filters->endDate);
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.time', 'DESC');
    }
}