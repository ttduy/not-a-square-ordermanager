<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PressEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');
		$this->inputs = array(
				'sectionPress'			=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Press'
																		)
												),
				'toPress'				=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Press',
																            'required' => false
																		)
												),
				'toPressFilterByType'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Press Type',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_Press_Types'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
			);
	}
}