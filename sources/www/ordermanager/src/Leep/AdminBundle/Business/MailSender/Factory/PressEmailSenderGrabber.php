<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PressEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toPress']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.email)');
            } else {
                $query->select('p')
                    ->orderBy('p.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Press', 'p')
                ->andWhere('p.email IS NOT NULL');

            if ($filters['toPressFilterByType']) {
                $val = $filters['toPressFilterByType'];
                $query->andWhere('p.type IN (:types)')
                    ->setParameter('types', $val);                    
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isOptedOut = 0
                $query->andWhere('(p.isOptedOut = 0 OR p.isOptedOut IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having canBeContactedDirectly = 1
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having canBeContactedDirectly = 1
                $query->andWhere('(p.canBeContactedDirectly = 1)');
            }

            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('idCountry', $countries);
            }

            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }
            
            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_PRESS;
                        $item['name'] = trim($row->getFirstName() . ' ' . $row->getSurName());
                        $item['email'] = trim($row->getEmail());
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => $row->getCanBeContactedDirectly(),
                                                    'isNutrimeOffered'          => false,
                                                    'isNotContacted'            => $row->getIsOptedOut()
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}