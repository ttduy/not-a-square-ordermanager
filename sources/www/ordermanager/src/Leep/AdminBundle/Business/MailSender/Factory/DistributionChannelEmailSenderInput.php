<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class DistributionChannelEmailSenderInput extends AbstractEmailSenderInput {
    function config() {
        $mapping = $this->container->get('easy_mapping');

        $this->inputs = array(
                'sectionDC'                                     =>  array(
                                                                    'type'      => 'section',
                                                                    'setting'   => array(
                                                                                        'label' => 'Distribution Channel'
                                                                                    )
                                                                ),
                'toDistributionChannel'                         =>  array(
                                                                    'type'      => 'checkbox',
                                                                    'setting'   => array(
                                                                                        'label'    => 'Distribution Channel',
                                                                                        'required' => false
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByPartner'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Partner',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByAcquisiteur'      =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Acquisiteur',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByRating'           =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Rating',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Rating_Levels'),
                                                                                        'multiple'  => 'multiple'
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByCustomerAmount'   =>  array(
                                                                    'type'      => 'app_comparator_type_select',
                                                                    'setting'   => array(
                                                                                        'label'     => '',
                                                                                        'required'  => false,
                                                                                        'selectLabel'  => 'Amount of customers'
                                                                                    )
                                                                ),
                'toDistributionChannelFilterBySoldProducts'    =>  array(
                                                                            'type'      => 'choice',
                                                                            'setting'   => array(
                                                                                                'label'     => 'Sold Products',
                                                                                                'required'  => false,
                                                                                                'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
                                                                                                'multiple'  => 'multiple',
                                                                                                'attr'      => array('style' => 'height:300px')
                                                                                            )
                                                                    ),
                'toDistributionChannelFilterByNoItemPerSoldProducts'   =>  array(
                                                                    'type'      => 'app_comparator_type_select',
                                                                    'setting'   => array(
                                                                                        'label'     => '',
                                                                                        'required'  => false,
                                                                                        'selectLabel'  => 'Sold Product: The number of Items'
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByIsTopSellSoldProducts'   =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Sold Product: Top Sell',
                                                                                        'required'  => false,
                                                                                        'choices'   => array(0 => 'No', 1 => 'Yes'),
                                                                                        'expanded'  => 'expanded'
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByNotSoldProducts'    =>  array(
                                                                            'type'      => 'choice',
                                                                            'setting'   => array(
                                                                                                'label'     => 'NOT-Sold Products',
                                                                                                'required'  => false,
                                                                                                'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
                                                                                                'multiple'  => 'multiple',
                                                                                                'attr'      => array('style' => 'height:300px')
                                                                                            )
                                                                    ),
                'toDistributionChannelFilterByWithComplaints'   =>  array(
                                                                    'type'      => 'checkbox',
                                                                    'setting'   => array(
                                                                                        'label'    => 'Has Complaints',
                                                                                        'required' => false
                                                                                    )
                                                                ),
                'toDistributionChannelFilterByWithFeedbacks'    =>  array(
                                                                    'type'      => 'checkbox',
                                                                    'setting'   => array(
                                                                                        'label'    => 'Has Feedbacks',
                                                                                        'required' => false
                                                                                    )
                                                                ),
            );
    }
}