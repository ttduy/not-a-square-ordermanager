<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PartnerEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');
		$this->inputs = array(
				'sectionPartner'	=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Partner'
																		)
												),
				'toPartner'			=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Partner',
																            'required' => false
																		)
												),
				'toPartnerFilterByDistributionChannel'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Distribution Channel',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
				'toPartnerFilterByDistributionChannelCity'	=>	array(
														'type'		=> 'text',
														'setting'	=> array(
																            'label'    	=> 'City',
																            'required' 	=> false
																		)
												),
                'toPartnerFilterByAcquisiteur'      =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Acquisiteur',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
			);
	}
}