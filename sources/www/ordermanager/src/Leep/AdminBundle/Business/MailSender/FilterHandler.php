<?php
namespace Leep\AdminBundle\Business\MailSender;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => false
        ));

        return $builder;
    }
}