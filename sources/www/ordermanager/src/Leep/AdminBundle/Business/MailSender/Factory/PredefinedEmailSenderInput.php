<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PredefinedEmailSenderInput extends AbstractEmailSenderInput {
    function config() {
        $this->inputs = array(
                'sectionPredefinedList'    =>  array(
                                                        'type'      => 'section',
                                                        'setting'   => array(
                                                                            'label' => 'Predefined List'
                                                                        )
                                                ),
                'toPredefinedList'         =>  array(
                                                        'type'      => 'textarea',
                                                        'setting'   => array(
                                                                            'label'     => 'Predefined List',
                                                                            'required'  => false,
                                                                            'attr'      => array(
                                                                                    'style' => 'width: 300px; height: 100px'
                                                                                )
                                                                        )
                                                )
            );
    }
}