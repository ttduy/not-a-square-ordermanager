<?php
namespace Leep\AdminBundle\Business\MailSender;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class EmailHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $fmsg =  $this->container->get('request')->getSession()->getFlashBag()->get('msgEmailCreation');
        if ($fmsg) {
            $this->messages = $fmsg;
        }

        return null;
    }

    public function buildForm($builder) {
        $emailSenderFactory = $this->container->get('leep_admin.mail_sender.business.email_sender_factory');
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        // BASIC INFO
        $builder->add('sectionSendTo', 'section', array(
            'label'    => 'Who To?',
            'property_path' => false
        ));

        $strToWhoInputIds = '';
        foreach (array_keys(Util::getReceiverTypeList()) as $eachReceiverType) {
            $input = $emailSenderFactory->createInput($eachReceiverType);

            if ($input) {
                $input->build($builder);

                if ($strToWhoInputIds) {
                    $strToWhoInputIds .= ',';
                }

                $strToWhoInputIds .= implode(',', $input->getIdInputs());
            }
        }

        $builder->add('toWhoInputIds', 'hidden', array(
            'data'    => $strToWhoInputIds
        ));

        $builder->add('sectionEmailContent', 'section', array(
            'label'    => 'Email Content',
            'property_path' => false
        ));

        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));

        $builder->add('content', 'app_text_editor', array(
            'label'    => 'Content',
            'required' => false
        ));

        $builder->add('contentHtml', 'textarea', array(
            'label'    => 'Content (HTML)',
            'required' => false,
            'attr'     => array(
                'cols'   => 120,
                'rows'   => 10
            )
        ));

        $builder->add('uploadFiles', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_file_uploader'),
            'label' => 'File Upload',
            'required' => false,
            'options'  => array(
                'path'     => Util::EMAIL_ATTACHMENT_FOLDER
            )
        ));

        $builder->add('sectionLimitation', 'section', array(
            'label'    => 'Limitation?',
            'property_path' => false
        ));
        $builder->add('includeOptedOutParties', 'checkbox', array(
            'label'    => 'Include opted-out party',
            'required' => false
        ));
        $builder->add('includeMustNotContactedParties', 'checkbox', array(
            'label'    => 'Include not-contacted party',
            'required' => false
        ));
        $builder->add('filterByCountries', 'choice', array(
            'label'    => 'Filter by countries',
            'multiple' => 'multiple',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List'),
            'attr'     => array('style' => 'height:300px')
        ));
        $builder->add('filterByLanguages', 'choice', array(
            'label'    => 'Filter by languages',
            'multiple' => 'multiple',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'attr'     => array('style' => 'height:300px')
        ));
    }

    public function onSuccess() {
        $mgr = $this->container->get('easy_module.manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // create email pool
        $emailPool = new Entity\EmailPool();
        $emailPool->setSubject($model['subject']);
        if (trim($model['contentHtml']) == '') {
            $emailPool->setContent($model['content']);
        }
        else {
            $emailPool->setContent($model['contentHtml']);
        }
        $emailPool->setStatus(Util::EMAIL_POOL_STATUS_OPEN);
        $emailPool->setNumSuccess(0);
        $emailPool->setNumFailed(0);
        $emailPool->setNumPending(0);
        $emailPool->setCreatedAt(new \DateTime());

        $em->persist($emailPool);
        $em->flush();

        // attachments
        if ($model['uploadFiles']) {
            $counter = 1;
            foreach ($model['uploadFiles'] as $eachUploadFile) {
                if ($eachUploadFile['attachment']) {
                    $attachmentDir = $this->container->getParameter('attachment_dir') . '/'. Util::EMAIL_ATTACHMENT_FOLDER;
                    $extension = pathinfo($eachUploadFile['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
                    $name = $emailPool->getId() . '_' . $counter . ($extension ? '.' . $extension : '');
                    $eachUploadFile['attachment']->move($attachmentDir, $name);

                    // email pool attachment
                    $newAttachment = new Entity\EmailPoolAttachment();
                    $newAttachment->setIdEmailPool($emailPool->getId());
                    $newAttachment->setName($name);
                    $newAttachment->setOriginalName($eachUploadFile['attachment']->getClientOriginalName());
                    $em->persist($newAttachment);
                    $em->flush();

                    $counter++;
                }
            }
        }

        $emailSenderFactory = $this->container->get('leep_admin.mail_sender.business.email_sender_factory');
        $filters = Util::getFilterParams($this->container, $model);
        foreach (array_keys(Util::getReceiverTypeList()) as $eachReceiverType) {
            $emailSenderGrabbers = $emailSenderFactory->createEmailGrabber($eachReceiverType);

            if ($emailSenderGrabbers) {
                foreach ($emailSenderGrabbers as $eachEmailGrabber) {
                    $eachEmailGrabber->grabThenSave($filters, $emailPool->getId());
                }
            }
        }

        $justCreatedEmailURL = $mgr->getUrl('leep_admin', 'mail_sender', 'view', 'view', array('id' => $emailPool->getId()));
        $justCreatedEmail = "<a href='" . $justCreatedEmailURL . "' target='_blank'>Just-created Email</a>";

        $this->messages = [];
        $this->messages[] = "Finished! Click to view " . $justCreatedEmail;

        $this->container->get('request')->getSession()->getFlashBag()->set('msgEmailCreation', $this->messages);
    }
}