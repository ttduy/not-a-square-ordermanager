<?php
namespace Leep\AdminBundle\Business\MailSender;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class EmailSendingStatisticFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new EmailSendingStatisticFilterModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start Date',
            'required' => false
        ));
        
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End Date',
            'required' => false
        ));

        return $builder;
    }
}