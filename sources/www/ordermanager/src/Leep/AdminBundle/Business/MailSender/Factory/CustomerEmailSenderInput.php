<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class CustomerEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');

		$this->inputs = array(
				'sectionCustomer'				=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Customer'
																		)
												),
				'toCustomer'					=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Customer',
																            'required' => false
																		)
												),
				'toCustomerByDistributionChannel'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Distribution Channel',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
				'toCustomerByReportLanguage'		=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Report Language',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_Language_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
				'toCustomerByCity'					=>	array(
														'type'		=> 'text',
														'setting'	=> array(
																            'label'    	=> 'City',
																            'required' 	=> false
																		)
												),
				'toCustomerByGender'				=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Gender',
																            'required' 	=> false,
																            'choices'  	=> 	$mapping->getMapping('LeepAdmin_Gender_List'),
																            'expanded' 	=> 'expanded'
																		)
												),
				'toCustomerByAge'					=>	array(
														'type'		=> 'app_comparator_type_select',
														'setting'	=> array(
                                                                            'label'     => '',
                                                                            'required'  => false,
                                                                            'selectLabel'  => 'Age'
																		)
												),
				'toCustomerByTimeSinceLastOrder'	=>	array(
														'type'		=> 'app_comparator_type_select',
														'setting'	=> array(
																            'label'    	=> '',
																            'required' 	=> false,
																            'selectLabel'  => 'N Day Since Last Order'
																		)
												),
				'toCustomerByOrderedProduct'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Products Ordered',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_Product_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
				'toCustomerByNotOrderedProduct'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Products NOT Ordered',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_Product_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												)
			);
	}
}