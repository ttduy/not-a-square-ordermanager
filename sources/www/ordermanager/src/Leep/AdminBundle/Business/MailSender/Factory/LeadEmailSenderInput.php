<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class LeadEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');
		$this->inputs = array(
				'sectionLead'					=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Lead'
																		)
												),
				'toLead'						=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Lead',
																            'required' => false
																		)
												),
				'toLeadFilterByDistributionChannel'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Distribution Channel',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
                'toLeadFilterByPartner'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Partner',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
                'toLeadFilterByAcquisiteur'      =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Acquisiteur',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
			);
	}
}