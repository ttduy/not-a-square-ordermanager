<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

abstract class AbstractEmailSenderGrabber {
	protected $container;
	protected $input;

	function __construct($container) {
		$this->container = $container;
	}

	function setInput($input) {
		$this->input = $input;
	}

	abstract function grab($filter);

	function grabThenSave($filters, $idEmailPool) {
		$em = $this->container->get('doctrine')->getEntityManager();
		$emailList = $this->grab($filters);
		$emailPool = $em->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($idEmailPool);

		if (sizeof($emailList) > 0 && $emailPool) {
			// email string
			$emailString = null;

	        // save receiver info
	        $counter = 0;
	        $timeToFlush = 1000;
	        foreach ($emailList as $email => $data) {
	            $counter++;
	            $receiver = new Entity\EmailPoolReceiver();
	            $receiver->setIdEmailPool($emailPool->getId());
	            $receiver->setEmail($email);
	            $receiver->setName($data['name']);
	            $receiver->setType($data['type']);
	            $em->persist($receiver);

	            if ($counter == $timeToFlush) {
	                $em->flush();
	                $counter = 0;
	            }

	            if ($emailString) {
	            	$emailString .= ';';
	            }
	            $emailString .= $email;
	        }

	        $em->flush();
		}
	}

	function getReceiverDetails($filters) {
		return $this->grab($filters);
	}
}