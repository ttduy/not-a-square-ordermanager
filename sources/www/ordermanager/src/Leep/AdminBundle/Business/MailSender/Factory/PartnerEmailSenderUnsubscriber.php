<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PartnerEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneByContactemail($email);

        if ($partner) {
            $partner->setIsNotContacted(1);
            $em->persist($partner);
            $em->flush();
        }
    }
}
