<?php
namespace Leep\AdminBundle\Business\MailSender;

use Easy\ModuleBundle\Module\AbstractHook;

class EmailSendingStatisticGridReaderHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $em = $controller->get('doctrine')->getEntityManager();

        // not sure about the way of calculating the total message, asked support and waiting for their confirmation
        $queryTotal = $em->createQueryBuilder();
        $result = $queryTotal->select('sum(p.sent) + sum(p.hardBounces) + sum(p.softBounces) AS TOTAL')
                                ->from('AppDatabaseMainBundle:EmailPoolSendingStatistic', 'p')
                                ->getQuery()
                                ->getResult();

        $totalMessage = 0;
        if ($result) {
            $totalMessage = $result[0]['TOTAL'];
        }

        $query = $em->createQueryBuilder();
        $result = $query->select('
                                    sum(p.sent) as numSent,
                                    sum(p.opens) as numOpens,
                                    sum(p.clicks) as numClicks,
                                    sum(p.hardBounces) as numHardBounces,
                                    sum(p.softBounces) as numSoftBounces,
                                    sum(p.rejects) as numRejects,
                                    sum(p.complaints) as numComplaints,
                                    sum(p.unsubs) as numUnsubs,
                                    sum(p.uniqueOpens) as numUniqueOpens,
                                    sum(p.uniqueClicks) as numUniqueClicks
                                ')
                                ->from('AppDatabaseMainBundle:EmailPoolSendingStatistic', 'p')
                                ->getQuery()
                                ->getResult();

        $summary = array(
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_SENT),
                'num' => $this->getVal($result, 'numSent'),
                'percent' => $this->calculatePercentage($result, 'numSent', null, $totalMessage)
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_OPEN),
                'num' => $this->getVal($result, 'numOpens'),
                'percent' => $this->calculatePercentage($result, 'numOpens', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_CLICK),
                'num' => $this->getVal($result, 'numClicks'),
                'percent' => $this->calculatePercentage($result, 'numClicks', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_HARD_BOUNCE),
                'num' => $this->getVal($result, 'numHardBounces'),
                'percent' => $this->calculatePercentage($result, 'numHardBounces', null, $totalMessage)
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_SOFT_BOUNCE),
                'num' => $this->getVal($result, 'numSoftBounces'),
                'percent' => $this->calculatePercentage($result, 'numSoftBounces', null, $totalMessage)
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_REJECT),
                'num' => $this->getVal($result, 'numRejects'),
                'percent' => $this->calculatePercentage($result, 'numRejects', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_COMPLAINT),
                'num' => $this->getVal($result, 'numComplaints'),
                'percent' => $this->calculatePercentage($result, 'numComplaints', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_UNSUB),
                'num' => $this->getVal($result, 'numUnsubs'),
                'percent' => $this->calculatePercentage($result, 'numUnsubs', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_OPEN),
                'num' => $this->getVal($result, 'numUniqueOpens'),
                'percent' => $this->calculatePercentage($result, 'numUniqueOpens', 'numSent')
            ),
            array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Email_Pool_Sending_Log_Types', Constants::EMAIL_POOL_SENDING_LOG_TYPE_UNIQUE_CLICK),
                'num' => $this->getVal($result, 'numUniqueClicks'),
                'percent' => $this->calculatePercentage($result, 'numUniqueClicks', 'numSent')
            )
        );

        $data['summary'] = $summary;
    }

    private function calculatePercentage($data, $col, $colTotal = null, $total = null) {
        $val = 0;

        if ($data && array_key_exists($col, $data[0])) {
            $data = $data[0];
            $totalNum = ($total ? $total : (array_key_exists($colTotal, $data) ? $data[$colTotal] : 0));

            $val = $data[$col] ? ($data[$col] / $totalNum) * 100 : 0;
        }

        return $val;
    }

    private function getVal($data, $col) {
        $val = 0;
        if ($data && array_key_exists($col, $data[0])) {
            $val = $data[0][$col];
        }

        return $val;
    }
}
