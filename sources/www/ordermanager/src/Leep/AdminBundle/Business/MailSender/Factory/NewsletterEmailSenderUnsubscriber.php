<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class NewsletterEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $newsletterRecipient = $em->getRepository('AppDatabaseMainBundle:NewsletterRecipient')->findOneByEmail($email);
        if ($newsletterRecipient) {
            $newsletterRecipient->setIsNotContacted(1);
            $em->persist($newsletterRecipient);
            $em->flush();
        }
    }
}
