<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class AcquisiteurEmailSenderInput extends AbstractEmailSenderInput {
	function config() {
		$mapping = $this->container->get('easy_mapping');
		$this->inputs = array(
				'sectionAcquisiteur'	=>	array(
														'type'		=> 'section',
														'setting'	=> array(
																			'label'	=> 'Acquisiteur'
																		)
												),
				'toAcquisiteur'			=>	array(
														'type'		=> 'checkbox',
														'setting'	=> array(
																            'label'    => 'Acquisiteur',
																            'required' => false
																		)
												),
				'toAcquisiteurFilterByDistributionChannel'	=>	array(
														'type'		=> 'choice',
														'setting'	=> array(
																            'label'    	=> 'Filter by Distribution Channel',
																            'required' 	=> false,
																            'choices'  	=> $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
																            'multiple' 	=> 'multiple',
																            'attr'		=> array('style' => 'height:300px')
																		)
												),
                'toAcquisiteurFilterByPartner'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Filter by Partner',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
				'toAcquisiteurFilterByTimeSinceLastOrder'	=>	array(
														'type'		=> 'text',
														'setting'	=> array(
																            'label'    	=> 'Filter by Time Since Last Order',
																            'required' 	=> false
																		)
												),
                'toAcquisiteurFilterByDCType'          =>  array(
                                                                    'type'      => 'choice',
                                                                    'setting'   => array(
                                                                                        'label'     => 'Filter by Distribution Channel Type',
                                                                                        'required'  => false,
                                                                                        'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
                                                                                        'multiple'  => 'multiple',
                                                                                        'attr'      => array('style' => 'height:300px')
                                                                                    )
                                                                ),
			);
	}
}