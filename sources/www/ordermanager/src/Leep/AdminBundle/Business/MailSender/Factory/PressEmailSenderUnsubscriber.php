<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class PressEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $press = $em->getRepository('AppDatabaseMainBundle:Press')->findOneByEmail($email);
        if ($press) {
            $press->setIsOptedOut(1);
            $em->persist($press);
            $em->flush();
        }
    }
}