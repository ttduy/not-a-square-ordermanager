<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class CustomerEmailSenderGrabber extends AbstractEmailSenderGrabber {
	function grab($filters, $getTotalOnly = false) {
		$em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('REGEXP', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Regexp');
        $emConfig->addCustomDatetimeFunction('DATEDIFF', 'Leep\AdminBundle\Business\Customer\DoctrineExt\DateDiff');
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Year');

        $comparatorTypeFactory = $this->container->get('leep_admin.comparator_type.comparator_type_factory');

        $arrItems = array();
        $totalRow = 0;

        if ($filters['toCustomer']) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct ci.email)');
            } else {
                $query->select('
                                    p.id, ci.firstName, ci.surName, 
                                    ci.email, ci.isCustomerBeContacted, ci.isNotContacted,
                                    ci.isNutrimeOffered
                                ')
                	->orderBy('ci.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Customer', 'p')
                ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'ci', 'WITH', 'p.idCustomerInfo = ci.id')
                ->andWhere('ci.email IS NOT NULL');

            // DC filter
            if ($filters['toCustomerByDistributionChannel']) {
                $query->andWhere('ci.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $filters['toCustomerByDistributionChannel']);
            }

            // Report Language filter
            if ($filters['toCustomerByReportLanguage']) {
                $query->andWhere('p.languageid IN (:repLangIds)')
                    ->setParameter('repLangIds', $filters['toCustomerByReportLanguage']);
            }

            // Order's City filter
            if ($filters['toCustomerByCity']) {
                $val = $filters['toCustomerByCity'];
                $query->andWhere('p.city LIKE :orderCity')
                    ->setParameter('orderCity', '%' . $val . '%');
            }

            // CustomerInfo's Gender filter
            if ($filters['toCustomerByGender']) {
                $val = $filters['toCustomerByGender'];
                $query->andWhere('ci.genderid = :customerInfoGender')
                    ->setParameter('customerInfoGender', $val);
            }

            // CustomerInfo's Age filter
            if ($comparatorTypeFactory->hasSelectComparatorType($filters['toCustomerByAge'])) {
                $val = $filters['toCustomerByAge'];
                $comparatorTypeFactory->buildQuery($query, '(YEAR(CURRENT_DATE()) - YEAR(ci.dateofbirth))', $val);
            }

            // Time Since Last Order filter
            if ($filters['toCustomerByTimeSinceLastOrder']) {
                $val = $filters['toCustomerByTimeSinceLastOrder'];
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.distributionchannelid');
                $comparatorTypeFactory->buildQuery($query, 'DATEDIFF(CURRENT_DATE(), dc.lastOrderDate)', $val);
            }
            
            // Ordered Products filter && Not Ordered Products filter
            if ($filters['toCustomerByOrderedProduct'] || $filters['toCustomerByNotOrderedProduct']) {
                $orderedProducts = $filters['toCustomerByOrderedProduct'];
                $notOrderedProducts = $filters['toCustomerByNotOrderedProduct'];

                if (!empty($orderedProducts)) {
                    $query->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                            ->setParameter('regexp', ',' . implode('|', $orderedProducts) . ',');
                }

                if (!empty($notOrderedProducts)) {
                    $excludedCustomerInfos = array(0);
                    $queryExcludedOrder = $em->createQueryBuilder();
                    $result = $queryExcludedOrder->select('p.idCustomerInfo')
                                            ->from('AppDatabaseMainBundle:Customer', 'p')
                                            ->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                                            ->setParameter('regexp', ',' . implode('|', $notOrderedProducts) . ',')
                                            ->andWhere('p.idCustomerInfo IS NOT NULL')
                                            ->groupBy('p.idCustomerInfo')
                                            ->getQuery()
                                            ->getResult();
                    if ($result) {
                        foreach ($result as $entry) {
                            if ($entry['idCustomerInfo']) {
                                $excludedCustomerInfos[] = $entry['idCustomerInfo'];
                            }
                        }
                    }

                    $query->andWhere('p.idCustomerInfo NOT IN (:notOrderedProductExcludedOrderIds)')
                            ->setParameter('notOrderedProductExcludedOrderIds', $excludedCustomerInfos);
                }
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$filters['includeOptedOutParties']) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(ci.isNotContacted = 0 OR ci.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$filters['includeMustNotContactedParties']) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(ci.isCustomerBeContacted = 1)');
            }

            if ($filters['filterByCountries']) {
                $countries = $filters['filterByCountries'];
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($filters['filterByLanguages']) {
                $languages = $filters['filterByLanguages'];
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.idCustomerInfo');

                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $item = array();
                        $item['type'] = Business\MailSender\Util::RECEIVER_TYPE_CUSTOMER;
                        $item['name'] = trim($row['firstName'] . ' ' . $row['surName']);
                        $item['email'] = trim($row['email']);
                        $item['others'] = array(
                                                    'isCustomerBeContacted'     => $row['isCustomerBeContacted'],
                                                    'isNutrimeOffered'          => $row['isNutrimeOffered'],
                                                    'isNotContacted'            => $row['isNotContacted'],
                                                );
                        $arrItems[$item['email']] = $item;
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
	}
}