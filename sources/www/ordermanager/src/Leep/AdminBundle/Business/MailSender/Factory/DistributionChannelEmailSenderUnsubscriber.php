<?php
namespace Leep\AdminBundle\Business\MailSender\Factory;

use Leep\AdminBundle\Business;

class DistributionChannelEmailSenderUnsubscriber extends AbstractEmailSenderUnsubscriber {
    public function unsubscribe($email) {
        parent::unsubscribe($email);

        $em = $this->container->get('doctrine')->getEntityManager();

        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneByContactemail($email);

        if ($dc) {
            $dc->setIsNotContacted(1);
            $em->persist($dc);
            $em->flush();
        }
    }
}
