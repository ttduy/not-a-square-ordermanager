<?php
namespace Leep\AdminBundle\Business\FeedbackResponse;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $feedbackQuestion;
    public $choices = array();
    public function getColumnMapping() {
        return array('responseTime', 'name', 'answer', 'action');
    }
    
    public function __construct($container) {
        parent::__construct($container);

        $id = $this->container->get('request')->get('id', 0);
        $this->feedbackQuestion = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findOneById($id);
        $this->choices = Business\FeedbackQuestion\Util::parseChoices($this->feedbackQuestion->getData());
    }

    public function getTableHeader() {
        return array(        
            array('title' => 'Date',        'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Name',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Answer',      'width' => '45%', 'sortable' => 'false'),  
            array('title' => 'Action',      'width' => '20%', 'sortable' => 'false'),          
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FeedbackResponseGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FeedbackResponse', 'p');
        $queryBuilder->andWhere('p.idFeedbackQuestion = :idFeedbackQuestion')
            ->setParameter('idFeedbackQuestion', $this->container->get('request')->get('id', 0));
    }

    public function buildCellAnswer($row) {
        if ($this->feedbackQuestion->getIdType() == Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_TEXT) {
            return nl2br(json_decode($row->getAnswer()));
        }
        else if ($this->feedbackQuestion->getIdType() == Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_CHOICE) {
            $choiceId = intval(json_decode($row->getAnswer()));
            return isset($this->choices[$choiceId]) ? $this->choices[$choiceId] : '';
        }
        else if ($this->feedbackQuestion->getIdType() == Business\FeedbackQuestion\Constant::FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE) {
            $choiceArr = array();
            $choices = json_decode($row->getAnswer(), true);
            foreach ($choices as $choiceId) {
                $choiceId = intval($choiceId);
                if (isset($this->choices[$choiceId])) {
                    $choiceArr[] = $this->choices[$choiceId];
                }
            }
            return implode("<br/>", $choiceArr);
        }
        return '';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('feedbackQuestionModify')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'feedback_response', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
            
        return $builder->getHtml();
    }
}
