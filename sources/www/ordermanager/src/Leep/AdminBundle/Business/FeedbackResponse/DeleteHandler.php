<?php 
namespace Leep\AdminBundle\Business\FeedbackResponse;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $id = $this->container->get('request')->query->get('id', 0);
        $feedbackResponse = $em->getRepository('AppDatabaseMainBundle:FeedbackResponse')->findOneById($id);

        $idFeedbackQuestion = $feedbackResponse->getIdFeedbackQuestion();

        $em->remove($feedbackResponse);
        $em->flush();


        return new RedirectResponse($mgr->getUrl('leep_admin', 'feedback_response', 'grid', 'list', array('id' => $idFeedbackQuestion)));
    }
}