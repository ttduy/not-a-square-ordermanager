<?php
namespace Leep\AdminBundle\Business\EmailTemplate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->toOthers = 'status@novogenia.com; ';
        $model->keywords = Constant::KEYWORDS;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'     => array(
                'rows'    => 15, 'cols' => 80
            )
        ));
        $builder->add('contentWithParagraph',  'app_text_editor', array(
            'label'    => 'Emaill will be show',
            'required' => false
        ));
        $builder->add('imgTemplate', 'img_template_generator', [
            'label' => 'Image template',
            'property_path' => false,
            'required' => false
        ]);
        $builder->add('keywords', 'label', array(
            'label'    => 'Keywords',
            'required' => false
        ));
        $builder->add('toCustomer', 'checkbox', array(
            'label'    => 'Send to customer?',
            'required' => false
        ));
        $builder->add('toDistributionChannel', 'checkbox', array(
            'label'    => 'Send to distribution channel?',
            'required' => false
        ));
        $builder->add('toPartner', 'checkbox', array(
            'label'    => 'Send to partner?',
            'required' => false
        ));
        $builder->add('toOthers',  'text', array(
            'label'    => 'Send to others (separated by ;)',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $emailTemplate = new Entity\EmailTemplate();
        $emailTemplate->setName($model->name);
        $emailTemplate->setSubject($model->subject);
        $emailTemplate->setContent($model->content);
        $emailTemplate->setToCustomer($model->toCustomer);
        $emailTemplate->setToDistributionChannel($model->toDistributionChannel);
        $emailTemplate->setToPartner($model->toPartner);
        $emailTemplate->setToOthers($model->toOthers);

        $em->persist($emailTemplate);
        $em->flush();

        parent::onSuccess();
    }
}
