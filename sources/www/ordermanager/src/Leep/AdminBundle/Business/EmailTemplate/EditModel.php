<?php
namespace Leep\AdminBundle\Business\EmailTemplate;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $name;
    public $subject;
    public $content;
    public $keywords;
    public $toCustomer;
    public $toDistributionChannel;
    public $toPartner;
    public $toOthers;
    public $contentWithParagraph;
}
