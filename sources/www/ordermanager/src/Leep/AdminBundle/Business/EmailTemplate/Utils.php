<?php
namespace Leep\AdminBundle\Business\EmailTemplate;

class Utils {
    const CUSTOMER = 1;

    public static function canSendEmail($controller, $entity, $arrEmails) {
        $blnOk = false;

        // opted out yet ?
        if (!$entity->getIsNotContacted() && sizeof($arrEmails) > 0) {
            $query = $controller->get('doctrine')->getEntityManager()->createQueryBuilder();
            $total = $query->select('count(p.id)')
                            ->from('AppDatabaseMainBundle:EmailOptOutList', 'p')
                            ->andWhere('p.email IN (:emails)')
                            ->setParameter('emails', $arrEmails)
                            ->getQuery()
                            ->getSingleScalarResult();

            // if one of them is in blacklist ?
            if (!$total) {
                $blnOk = true;
            }
        }

        return $blnOk;
    }

    private static function getAddress($container, $customer)
    {
        $formatter = $container->get('leep_admin.helper.formatter');
        $street = $customer->getStreet();
        $postCode = $customer->getPostcode();
        $city = $customer->getCity();
        $country = $formatter->format($customer->getCountryid(), 'mapping', 'LeepAdmin_Country_List');

        $address = sprintf(' %s, %s, %s, %s', $street, $city, $postCode, $country);
        $address = str_replace(' ,', '', $address);
        return $address;
    }

    /**
    * $arrInputData includes 'name', 'usernameLogin', 'passwordLogin', 'idLanguage', 'id', 'type'
    **/
    public static function getEmailTemplate($container, $arrInputData) {
        $mgr = $container->get('easy_module.manager');
        $helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $container->get('doctrine');

        $emailTemplate = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($config->getCustomerDashboardLoginEmailTemplate());

        $arrData = array(
                    'subject'   => '',
                    'content'   => ''
                );

        if ($emailTemplate) {
            $arrParams = array('code' => $helperEncrypt->encrypt($arrInputData['type'] . '_' . $arrInputData['id']));
            $optOutUrl = $container->getParameter('application_url') . $mgr->getUrl('leep_admin', 'web_user', 'feature', 'optOut') . '?' . http_build_query($arrParams);

            //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // WEB LOGIN OPT OUT
            // Get email list
            $entity = null;
            $email = '';
            $id = $arrInputData['id'];
            $type = $arrInputData['type'];

            switch ($type) {
                case 1:
                    $entity = 'CustomerInfo';
                    $getEmailMethod = 'getEmail';
                    break;
                case 2:
                    $entity = 'DistributionChannels';
                    $getEmailMethod = 'getContactemail';
                    break;
                case 3:
                    $entity = 'Partner';
                    $getEmailMethod = 'getContactemail';
                    break;
            }

            $orderNumber = '';
            $orderDate = '';
            $productList = '';
            $distributionChannel = '';
            $address = '';
            if ($entity && $id) {
                $info = $doctrine->getRepository('AppDatabaseMainBundle:' . $entity)->findOneById($id);
                $flag= false;
                if ($info) {
                    $email = $info->$getEmailMethod();
                    $flag = true;
                }
                if($flag == true && $type == self::CUSTOMER){
                    $orderNumber = $info->getCustomerNumber();
                    $idDc = $info->getIdDistributionChannel();
                    $customer = $doctrine->getRepository('AppDatabaseMainBundle:customer')->findOneByordernumber($orderNumber);
                    $orderDate = $customer->getDateordered()->format('Y-m-d');
                    $productList = $customer->getProductList();
                    $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($idDc);
                    $distributionChannel = $dc->getDistributionchannel();
                    $address = self::getAddress($container, $customer);
                }
            }

            $email = $helperEncrypt->encrypt($email);
            $optOutWebLoginUrl = $container->getParameter('application_url') . $mgr->getUrl('leep_admin', 'web_user', 'feature', 'webLoginOptOut').'?email='.$email;
            // print_r($optOutWebLoginUrl);
            //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            $env = new \Twig_Environment(new \Twig_Loader_String(), array('autoescape' => false));
            $arrData = array(
                'name'          =>      $arrInputData['name'],
                'usernamelogin' =>      $arrInputData['usernamelogin'],
                'passwordlogin' =>      $arrInputData['passwordlogin'],
                'orderNumber'   =>      $orderNumber,
                'orderDate'     =>      $orderDate,
                'address'       =>      $address,
                'productList'   =>      $productList,
                'distributionChannel'   =>  $distributionChannel,
                'OPTOUTLINK'    =>      $optOutUrl,
                'OPTOUT_WEBLOGIN' =>    $optOutWebLoginUrl
            );

            $subject = $env->render($emailTemplate->getSubject(), $arrData);
            $content = $env->render($emailTemplate->getContent(), $arrData);


            // apply defined Text Blocks
            $arrData = array(
                            'subject' => self::applyTextBlocks($container, $subject, $arrInputData['idLanguage']),
                            'content' => self::applyTextBlocks($container, $content, $arrInputData['idLanguage'])
                        );
        }

        return $arrData;
    }

    public static function applyTextBlocks($container, $content, $idLanguage = null) {
        $result = $content;

        $regex = '/\[\[(.*?)\]\]/ism';
        if (preg_match_all($regex, $content, $tokens)) {
            $arrTranslated = array();
            foreach ($tokens[0] as $key => $entry) {
                $name = trim(substr($entry, 2, strlen($entry) - 4));
                $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
                $textBlock = $query->select('l.body')
                            ->from('AppDatabaseMainBundle:TextBlock', 'p')
                            ->innerJoin('AppDatabaseMainBundle:TextBlockLanguage', 'l', 'WITH', 'l.idTextBlock = p.id')
                            ->andWhere('p.name = :name')
                            ->setParameter('name', $name)
                            ->andWhere('l.idLanguage = :idLanguage')
                            ->setParameter('idLanguage', $idLanguage)
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getResult();

                if ($textBlock) {
                    $arrTranslated[$entry] = $textBlock[0]['body'];
                } else {
                    $arrTranslated[$entry] = $tokens[1][$key];
                }
            }
            // replace
            $result = str_replace(array_keys($arrTranslated), $arrTranslated, $content);
        }

        return $result;
    }


    public static function convertToHtmlContent($content) {
        $arr = explode("\n", $content);
        return implode("<br/>", $arr);
    }
}