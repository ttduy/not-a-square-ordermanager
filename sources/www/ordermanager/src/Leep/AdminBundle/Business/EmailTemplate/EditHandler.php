<?php
namespace Leep\AdminBundle\Business\EmailTemplate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailTemplate', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->subject = $entity->getSubject();
        $model->content = $entity->getContent();
        $model->contentWithParagraph = $entity->getContent();
        $model->toCustomer = $entity->getToCustomer();
        $model->toDistributionChannel = $entity->getToDistributionChannel();
        $model->toPartner = $entity->getToPartner();
        $model->toOthers = $entity->getToOthers();
        $model->keywords = Constant::KEYWORDS;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');


        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'     => array(
                'rows'    => 15, 'cols' => 80
            )
        ));
        $builder->add('contentWithParagraph',  'app_text_editor', array(
            'label'    => 'Emaill will be show',
            'required' => false
        ));
        $builder->add('imgTemplate', 'img_template_generator', [
            'label' => 'Image template',
            'property_path' => false,
            'required' => false,
            'attr'     => array(
                'rows'    => 15, 'cols' => 80
            )
        ]);
        $builder->add('toCustomer', 'checkbox', array(
            'label'    => 'Send to customer?',
            'required' => false
        ));
        $builder->add('toDistributionChannel', 'checkbox', array(
            'label'    => 'Send to distribution channel?',
            'required' => false
        ));
        $builder->add('toPartner', 'checkbox', array(
            'label'    => 'Send to partner?',
            'required' => false
        ));
        $builder->add('toOthers',  'text', array(
            'label'    => 'Send to others (separated by ;)',
            'required' => false
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setName($model->name);
        $this->entity->setSubject($model->subject);
        $this->entity->setContent($model->content);
        $this->entity->setToCustomer($model->toCustomer);
        $this->entity->setToDistributionChannel($model->toDistributionChannel);
        $this->entity->setToPartner($model->toPartner);
        $this->entity->setToOthers($model->toOthers);
        $this->entity->setContentWithParagraph($model->contentWithParagraph);


        parent::onSuccess();
    }
}
