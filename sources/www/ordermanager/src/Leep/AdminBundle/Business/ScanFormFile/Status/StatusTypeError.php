<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypeError extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_ERROR;
	}

    public function getStatusTitle() {
    	return 'Error';
    }

    public function getProgressTitle() {
    	return 'Error';
    }

    public function getStatusAction() {
        $builder = $this->getButtonBuilder();
        $builder->addPopupButton('View Error', $this->getModuleManager()->getUrl('leep_admin', 'scan_form_file', 'feature', 'viewError', array('id' => $this->entity->getId())), 'button-edit');

        return $builder->getHtml();
    }

    public function getStatisticAction() {
        $builder = $this->getButtonBuilder();
        $builder->addLink('Filter Error File', 'javascript:setFilterForErrorFiles()', 'button-edit');
        return $builder->getHtml();
    }
}