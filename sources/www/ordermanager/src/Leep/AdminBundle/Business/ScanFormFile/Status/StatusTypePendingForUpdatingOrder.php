<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypePendingForUpdatingOrder extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER;
	}

    public function getStatusTitle() {
    	return 'Pending for updating orders';
    }

    public function getProgressTitle() {
    	return 'Update order';
    }

    public function getProgressAction() {
        $builder = $this->getButtonBuilder();
        $builder->addButton('View Order', $this->getModuleManager()->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $this->entity->getIdOrder())), 'button-edit');

        return $builder->getHtml();
    }

    public function getStatisticAction() {
        $builder = $this->getButtonBuilder();
        //$builder->addButton('Update order', $this->getModuleManager()->getUrl('leep_admin', 'scan_form_file', 'feature', 'detectTemplate', array('id' => $this->entity->getIdOrder())), 'button-edit');
        $builder->addLink('Update orders', '#', 'button-edit');
        return $builder->getHtml();
    }
}