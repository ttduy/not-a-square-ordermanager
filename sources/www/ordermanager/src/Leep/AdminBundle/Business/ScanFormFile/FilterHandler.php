<?php
namespace Leep\AdminBundle\Business\ScanFormFile;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->isHideErrorStatus = true;
        $model->isHideClosedStatus = true;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => Constant::getScanFormFileStatusList($this->container)
        ));

        $builder->add('isHideErrorStatus', 'checkbox', array(
            'label'    => 'Is Hide Error',
            'required' => false
        ));

        $builder->add('isHideClosedStatus', 'checkbox', array(
            'label'    => 'Is Hide Closed',
            'required' => false
        ));

        return $builder;
    }
}