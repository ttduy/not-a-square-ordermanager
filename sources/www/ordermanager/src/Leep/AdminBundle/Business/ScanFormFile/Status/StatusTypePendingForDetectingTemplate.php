<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypePendingForDetectingTemplate extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE;
	}

    public function getStatusTitle() {
    	return 'Pending for detecting template';
    }

    public function getProgressTitle() {
    	return 'Detect template';
    }

    public function getStatisticAction() {
        $builder = $this->getButtonBuilder();
        $builder->addNewTabLink('Detect Template', $this->getModuleManager()->getUrl('leep_admin', 'scan_form_file', 'detecting_template', 'create'), 'button-edit');
        return $builder->getHtml();
    }
}