<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypePendingForProcessing extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING;
	}

    public function getStatusTitle() {
    	return 'Pending for processing';
    }

    public function getProgressTitle() {
    	return 'Process';
    }
}