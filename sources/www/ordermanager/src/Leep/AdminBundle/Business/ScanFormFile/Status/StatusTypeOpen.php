<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypeOpen extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_OPEN;
	}

    public function getStatusTitle() {
    	return 'Open';
    }

    public function getProgressTitle() {
    	return 'Open';
    }

    public function getStatisticAction() {
        return '';
    }
}