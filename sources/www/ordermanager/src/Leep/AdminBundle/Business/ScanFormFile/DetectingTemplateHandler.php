<?php
namespace Leep\AdminBundle\Business\ScanFormFile;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DetectingTemplateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $arrData = array();

        return $arrData;
    }

    public function buildForm($builder) {
        $builder->add('idScanFormFile', 'hidden');
        $builder->add('idScanFormTemplate', 'hidden');

        $builder->add('scanFormFileSection', 'section', array(
                'label' => 'Scan Form File'
            ));
        
        $builder->add('scanFormTemplateSection', 'section', array(
                'label' => 'Scan Form Template'
            ));
        

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $scanFormFile = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($model['idScanFormFile']);
        $scanFormTemplate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFile')->findOneById($model['idScanFormTemplate']);

        if (!$scanFormFile || !$scanFormTemplate || $scanFormFile->getStatus() != Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE) {
            $this->errors[] = "Invalid Requested";
            return false;
        }

        $scanFormFile->setIdScanFormTemplate($scanFormTemplate->getId());
        $scanFormFile->setStatus(Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING);
        $em->persist($scanFormTemplate);
        $em->flush();

        parent::onSuccess();
    }
}