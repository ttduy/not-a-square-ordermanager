<?php
namespace Leep\AdminBundle\Business\ScanFormFile;

use Easy\ModuleBundle\Module\AbstractHook;

class GridReaderHook extends AbstractHook {
    public $todayTs;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $data['stats'] = $this->buildStats($controller);
        $data['errorStatus'] = Constant::SCAN_FORM_FILE_STATUS_ERROR;
    }

    private function buildStats($controller) {
        $statusTypeManager = $controller->get('leep_admin.scan_form_file.business.status_type_manager');
        $arrStatisticData = array();

        foreach (array_keys(Constant::getScanFormFileStatusListForStatistics($controller)) as $status) {
            $statusType = $statusTypeManager->getStatusType($status);
            $arrStatisticData[$statusType->getId()] = $statusType->getStatisticInfoLine();
        }

        return $arrStatisticData;
    }
}
