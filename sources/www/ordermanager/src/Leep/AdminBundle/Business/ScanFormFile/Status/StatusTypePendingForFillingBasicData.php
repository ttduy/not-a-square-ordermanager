<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypePendingForFillingBasicData extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA;
	}

    public function getStatusTitle() {
    	return 'Pending for filling basic data';
    }

    public function getProgressTitle() {
    	return 'Fill basic data';
    }

    public function getProgressAction() {
        $builder = $this->getButtonBuilder();
        $builder->addButton('View Data', $this->getModuleManager()->getUrl('leep_admin', 'scan_form_file', 'feature', 'viewData', array('id' => $this->entity->getId())), 'button-edit');

        return $builder->getHtml();
    }

    public function getStatisticAction() {
        $builder = $this->getButtonBuilder();
        //$builder->addButton('Fill basic data', $this->getModuleManager()->getUrl('leep_admin', 'scan_form_file', 'feature', 'detectTemplate', array('id' => $this->entity->getIdOrder())), 'button-edit');
        $builder->addLink('Fill basic data', '#', 'button-edit');
        return $builder->getHtml();
    }
}