<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypePendingForCreatingOrder extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER;
	}

    public function getStatusTitle() {
    	return 'Pending for creating order';
    }

    public function getProgressTitle() {
    	return 'Create order';
    }

    public function getProgressAction() {
        $builder = $this->getButtonBuilder();
        $builder->addButton('View Order', $this->getModuleManager()->getUrl('leep_admin', 'customer', 'create', 'create', array('idScanFormFile' => $this->entity->getId())), 'button-edit');

        return $builder->getHtml();
    }

    public function getStatisticAction() {
        $builder = $this->getButtonBuilder();
        //$builder->addButton('Create Order', $this->getModuleManager()->getUrl('leep_admin', 'customer', 'feature', 'bulkCreateOrder', array('id' => $this->entity->getIdOrder())), 'button-edit');
        $builder->addLink('Create Order', '#', 'button-edit');
        return $builder->getHtml();
    }
}