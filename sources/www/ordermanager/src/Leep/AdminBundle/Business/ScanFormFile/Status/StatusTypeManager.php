<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile;

class StatusTypeManager {
    public $container;

    public $arrStatusType = array(
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_OPEN                               => 'leep_admin.scan_form_file.business.status_type_open',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE     => 'leep_admin.scan_form_file.business.status_type_pending_for_detecting_template',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING             => 'leep_admin.scan_form_file.business.status_type_pending_for_processing',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA     => 'leep_admin.scan_form_file.business.status_type_pending_for_filling_basic_data',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER         => 'leep_admin.scan_form_file.business.status_type_pending_for_creating_order',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA       => 'leep_admin.scan_form_file.business.status_type_pending_for_filling_all_data',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER         => 'leep_admin.scan_form_file.business.status_type_pending_for_updating_order',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_CLOSED                             => 'leep_admin.scan_form_file.business.status_type_closed',
            ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_ERROR                              => 'leep_admin.scan_form_file.business.status_type_error',
        );

    public function __construct($container) {
        $this->container = $container;
    }

    public function getStatusType($id) {
        $statusType = null;

        if (array_key_exists($id, $this->arrStatusType)) {
            $statusType = $this->container->get($this->arrStatusType[$id]);
        }

        return $statusType;
    }
}