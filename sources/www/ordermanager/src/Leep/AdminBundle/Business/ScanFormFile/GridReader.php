<?php
namespace Leep\AdminBundle\Business\ScanFormFile;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'creationDatetime', 'file', 'status', 'progress', 'action');
    } 
    
    public function getTableHeader() {
        return array(
            array('title' => 'Id',                  'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Creation Time',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'File',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status',              'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Progress',            'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',              'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ScanFormFileGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFormFile', 'p');
        
        if ($this->filters->status) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }

        if ($this->filters->isHideErrorStatus) {
            $queryBuilder->andWhere('p.status <> :statusError')
                ->setParameter('statusError', Constant::SCAN_FORM_FILE_STATUS_ERROR);
        }

        if ($this->filters->isHideClosedStatus) {
            $queryBuilder->andWhere('p.status <> :statusClosed')
                ->setParameter('statusClosed', Constant::SCAN_FORM_FILE_STATUS_CLOSED);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormModify')) {
            $builder->addButton('Download PDF', $mgr->getUrl('leep_admin', 'scan_form_file', 'feature', 'downloadPdf', array('id' => $row->getId())), 'button-pdf');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'scan_form_file', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellStatus($row) {
        $statusTypeManager = $this->container->get('leep_admin.scan_form_file.business.status_type_manager');

        $html = '';

        if ($row->getStatus()) {
            $statusType = $statusTypeManager->getStatusType($row->getStatus());
            $statusType->setEntity($row);
            $html .= $statusType->getStatusInfoLine();
        }

        return $html;
    }

    public function buildCellProgress($row) {
        return Utils::getProgressInfo($this->container, $row);
    }

    public function buildCellFile($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormView')) {
            $builder->addButton('Download', $mgr->getUrl('leep_admin', 'scan_form_file', 'feature', 'downloadFile', array('id' => $row->getId())), 'button-pdf');
        }

        return $builder->getHtml();
    }
}