<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile\Constant;

class StatusTypeClosed extends StatusTypeAbstract {
	public function getId() {
		return Constant::SCAN_FORM_FILE_STATUS_CLOSED;
	}

    public function getStatusTitle() {
    	return 'Closed';
    }

    public function getProgressTitle() {
    	return 'Closed';
    }
}