<?php 
namespace Leep\AdminBundle\Business\ScanFormFile;

class Constant {
    const SCAN_FORM_FILE_STATUS_OPEN                                  = 10;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE        = 20;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING                = 30;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA        = 40;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER            = 50;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA          = 60;
    const SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER            = 70;
    const SCAN_FORM_FILE_STATUS_CLOSED                                = 80;
    const SCAN_FORM_FILE_STATUS_ERROR                                 = 90;

    public static function getScanFormFileStatusList($container) {
        $statusTypeMgr = $container->get('leep_admin.scan_form_file.business.status_type_manager');
        return array(
            self::SCAN_FORM_FILE_STATUS_OPEN                                => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_OPEN)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE      => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING              => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA      => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER          => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA        => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER          => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_CLOSED                              => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_CLOSED)->getStatusTitle(),
            self::SCAN_FORM_FILE_STATUS_ERROR                               => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_ERROR)->getStatusTitle()
        );
    }

    public static function getScanFormFileStatusListForStatistics($container) {
        $arr = self::getScanFormFileStatusList($container);
        unset($arr[self::SCAN_FORM_FILE_STATUS_CLOSED]);
        return $arr;
    }

    public static function getScanFormFileStatusListForProgressInfo($container) {
        $statusTypeMgr = $container->get('leep_admin.scan_form_file.business.status_type_manager');
        return array(
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE      => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE)->getProgressTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING              => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING)->getProgressTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA      => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA)->getProgressTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER          => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_CREATING_ORDER)->getProgressTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA        => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_ALL_DATA)->getProgressTitle(),
            self::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER          => $statusTypeMgr->getStatusType(self::SCAN_FORM_FILE_STATUS_PENDING_FOR_UPDATING_ORDER)->getProgressTitle()
        );
    }

    const SCAN_FORM_FILE_TOKEN_PART_ACTUAL_IMAGE    = 10;
    const SCAN_FORM_FILE_TOKEN_PART_EXTRA_IMAGE     = 20;

    public static function getScanFormFileTokenPartList() {
        return array(
            self::SCAN_FORM_FILE_TOKEN_PART_ACTUAL_IMAGE     => 'Actual Images',
            self::SCAN_FORM_FILE_TOKEN_PART_EXTRA_IMAGE      => 'Extra Images'
        );
    }
}
