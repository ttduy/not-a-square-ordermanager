<?php
namespace Leep\AdminBundle\Business\ScanFormFile\Status;
use Leep\AdminBundle\Business\ScanFormFile;

abstract class StatusTypeAbstract {
    public $container;
    public $entity;

    public function __construct($container) {
        $this->container = $container;
    }

    public function getButtonBuilder() {
        return $this->container->get('leep_admin.helper.button_list_builder');
    }

    public function getModuleManager() {
        return $this->container->get('easy_module.manager');
    }

    public function getId() {
        return null;
    }

    public function getStatusTitle() {
        return '';
    }
    public function getStatusAction() {
        return '';
    }
    
    public function getStatisticTotal() {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $count = $query->select('count(p.id)')
            ->from('AppDatabaseMainBundle:ScanFormFile', 'p')
            ->andWhere('p.status = :status')
            ->setParameter('status', $this->getId())
            ->getQuery()
            ->getSingleScalarResult();

        return $count;
    }

    public function getStatisticAction() {
        return '';
    }
    public function getStatisticTitle() {
        return $this->getStatusTitle();
    }


    public function getProgressTitle() {
        return '';
    }
    public function getProgressAction() {
        return '';
    }

    public function getStatusInfoLine() {
        return $this->getStatusTitle() . ' ' . $this->getStatusAction();
    }

    public function getProgressInfoLine() {
        return $this->getProgressTitle() . ': ' . $this->isDone() . ' ' . $this->getProgressAction();
    }

    public function getStatisticInfoLine() {
        $total = $this->getStatisticTotal();
        return array(
                    'statsTitle'   => $this->getStatisticTitle(),
                    'total'         => $total,
                    'action'        => $total ? $this->getStatisticAction() : ''
                );
    }    

    public function isDone() {
        return $this->getId() < $this->entity->getStatus() ? 'Done' : '--';
    }

    public function setEntity($scanFormFile) {
        $this->entity = $scanFormFile;
    }
}