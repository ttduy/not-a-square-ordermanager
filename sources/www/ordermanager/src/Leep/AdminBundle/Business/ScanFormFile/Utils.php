<?php
namespace Leep\AdminBundle\Business\ScanFormFile;

class Utils {
    public static function getProgressInfo($container, $objScanFormFile) {
    	$statusTypeManager = $container->get('leep_admin.scan_form_file.business.status_type_manager');

    	$str = '';
    	$currentStatus = $objScanFormFile->getStatus();

    	foreach (array_keys(Constant::getScanFormFileStatusListForProgressInfo($container)) as $status) {
    		$statusType = $statusTypeManager->getStatusType($status);
    		$statusType->setEntity($objScanFormFile);

    		if ($str) {
    			$str .= "<br/>";
    		}
    		$str .= $statusType->getProgressInfoLine();
    	}

    	return $str;
    }

    public static function getOldestFilePendingForDetectingTemplate($controller) {
        $query = $controller->get('doctrine')->getEntityManager()->createQueryBuilder();
        $result = $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormFile', 'p')
            ->andWhere('p.status = :detectingTemplateStatus')
            ->setParameter('detectingTemplateStatus', Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE)
            ->orderBy('p.creationDatetime', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        $data = null;
        if ($result) {
            $data = $result[0];
        }

        return $data;

    }
}