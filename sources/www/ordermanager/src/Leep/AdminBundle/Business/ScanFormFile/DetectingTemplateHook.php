<?php
namespace Leep\AdminBundle\Business\ScanFormFile;

use Easy\ModuleBundle\Module\AbstractHook;

class DetectingTemplateHook extends AbstractHook {
    private $idScanFormFile;
    private $controller;
    public function handle($controller, &$data, $options = array()) {
        $this->controller = $controller;

        $this->buildScanFormFileData($data);

        $this->buildScanFormTemplateData($data);
    }

    private function buildScanFormFileData(&$data) {
        // get the oldest Scan Form File which is pending for detecting template
        $scanFormFile = Utils::getOldestFilePendingForDetectingTemplate($this->controller);

        $scanFormFileData = null;
        if ($scanFormFile) {
            $this->idScanFormFile = $scanFormFile->getId();
            $scanFormFileData = array(
                'id' => ($scanFormFile ? $scanFormFile->getId() : ''),
                'downloadPdfUrl' => '#'
            );
        }
        
        $data['scanFormFile'] = $scanFormFileData;
    }

    private function buildScanFormTemplateData(&$data) {
        $arrTemplates = array();
        if ($this->idScanFormFile) {
            $arrTemplates = $this->getScanFormTemplatesFullData();
        }

        $data['arrTemplates'] = $arrTemplates;
    }

    private function getScanFormTemplatesFullData() {
        $mgr = $this->controller->get('easy_module.manager');
        $query = $this->controller->get('doctrine')->getEntityManager()->createQueryBuilder();
        $result = $query->select('
                                p.id, p.name,
                                t.image
                            ')
                        ->from('AppDatabaseMainBundle:ScanFormTemplate', 'p')
                        ->leftJoin('AppDatabaseMainBundle:ScanFormTemplateExpectedTokens', 't', 'WITH', 't.idScanFormTemplate = p.id')
                        ->orderBy('p.name', 'ASC')
                        ->getQuery()
                        ->getResult();

        $arrTemplates = array();

        if ($result) {
            foreach ($result as $entry) {
                if (!array_key_exists($entry['id'], $arrTemplates)) {
                    $arrTemplates[$entry['id']] = array(
                        'name'          => $entry['name'],
                        'image'         => array(),
                        'actualImages'  => array()
                    );
                }

                $arrTemplates[$entry['id']]['image'][] = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils',  'viewImage', array('file' => '/scan_form/'.$entry['image']));
            }
        }

        // get actual tokens by scan form file
        $result = $this->controller->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormFileActualTokens')->findByIdScanFormFile($this->idScanFormFile);
        if ($result) {
            foreach ($result as $entry) {
                if (array_key_exists($entry->getIdScanFormTemplate(), $arrTemplates)) {
                    $arrTemplates[$entry->getIdScanFormTemplate()]['actualImages'][] = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils',  'viewImage', array('file' => '/scan_form/'.$entry->getImage()));                    
                }
            }
        }

        return $arrTemplates;
    }
}
