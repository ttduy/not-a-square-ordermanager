<?php
namespace Leep\AdminBundle\Business\Base;

class Constant {
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    public static function getGenders() {
        return array(
            self::GENDER_MALE   => 'Male',
            self::GENDER_FEMALE => 'Female'
        );
    }


    public static function getTimeslot() {
        $arr = array();
        for ($i = 0; $i < 24; $i++) {
            $hour = $i < 10 ? ('0'.$i) : $i; 
            $arr[$i*4+0] = $hour.':00';
            $arr[$i*4+1] = $hour.':15';
            $arr[$i*4+2] = $hour.':30';
            $arr[$i*4+3] = $hour.':45';
        }
        return $arr;
    }


    const CARD_TYPE_VISA     = 1;
    const CARD_TYPE_MASTER   = 2;
    const CARD_TYPE_DISCOVER = 3;
    const CARD_TYPE_AMERICAN_EXPRESS = 4;
    public static function getCardTypes() {
        return array(
            self::CARD_TYPE_VISA     => 'Visa',
            self::CARD_TYPE_MASTER   => 'Master',
            self::CARD_TYPE_DISCOVER => 'Discover',
            self::CARD_TYPE_AMERICAN_EXPRESS => 'American Express'
        );
    }

    const COMPLAINT_TOPIC_DATA_ENTRY_PROBLEMS         = 10;
    const COMPLAINT_TOPIC_TURN_AROUND_TIME_PROBLEMS   = 20;
    const COMPLAINT_TOPIC_DELIVERY_PROBLEMS           = 30;
    const COMPLAINT_TOPIC_ANALYSIS_RESULT_PROBLEMS    = 40;
    const COMPLAINT_TOPIC_REPORT_PROBLEMS             = 50;
    const COMPLAINT_TOPIC_PRODUCT_QUALITY_PROBLEMS    = 60;
    public static function getComplaintTopics() {
        return array(
            self::COMPLAINT_TOPIC_DATA_ENTRY_PROBLEMS         => 'Data entry problems',
            self::COMPLAINT_TOPIC_TURN_AROUND_TIME_PROBLEMS   => 'Turn around time problems',
            self::COMPLAINT_TOPIC_DELIVERY_PROBLEMS           => 'Delivery problems',
            self::COMPLAINT_TOPIC_ANALYSIS_RESULT_PROBLEMS    => 'Analysis result problems',
            self::COMPLAINT_TOPIC_REPORT_PROBLEMS             => 'Report problems',
            self::COMPLAINT_TOPIC_PRODUCT_QUALITY_PROBLEMS    => 'Product quality problems',
        );
    }
    public static function getComplaintTopicsFilter() {
        return array(
            self::COMPLAINT_TOPIC_DATA_ENTRY_PROBLEMS         => 'Has Data entry problems',
            self::COMPLAINT_TOPIC_TURN_AROUND_TIME_PROBLEMS   => 'Has Turn around time problems',
            self::COMPLAINT_TOPIC_DELIVERY_PROBLEMS           => 'Has Delivery problems',
            self::COMPLAINT_TOPIC_ANALYSIS_RESULT_PROBLEMS    => 'Has Analysis result problems',
            self::COMPLAINT_TOPIC_REPORT_PROBLEMS             => 'Has Report problems',
            self::COMPLAINT_TOPIC_PRODUCT_QUALITY_PROBLEMS    => 'Has Product quality problems',
        );

    }

    const WEB_LOGIN_STATUS_ACTIVE = 10;
    const WEB_LOGIN_STATUS_INACTIVE = 20;

    // list of web login status
    public static function getWebLoginStatusList() {
        return array(
            self::WEB_LOGIN_STATUS_ACTIVE => 'Active',
            self::WEB_LOGIN_STATUS_INACTIVE => 'Inactive'
        );
    }
}