<?php
namespace Leep\AdminBundle\Business\Base;

abstract class AppDeleteHandler {  
    public $container;
    public $id;
    public $idRedirect;

    public function __construct($container) {
        $this->container = $container;        
    }

    abstract function execute();
}

