<?php
namespace Leep\AdminBundle\Business\Base;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;

abstract class AppEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_main';
    }

    public function reloadForm() {
        $request = $this->container->get('request');
        $this->entity = $this->loadEntity($request);
        $formModel = $this->convertToFormModel($this->entity);

        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
