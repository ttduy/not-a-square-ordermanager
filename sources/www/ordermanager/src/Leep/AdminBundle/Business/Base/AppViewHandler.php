<?php
namespace Leep\AdminBundle\Business\Base;

abstract class AppViewHandler {  
    public $container;
    public $id;

    public function __construct($container) {
        $this->container = $container;        
    }

    public function add($label, $value) {
        return array(
            'type' => 'row', 
            'label' => $label, 
            'content' => $value
        );
    }
    public function addSection($label) {
        return array(
            'type' => 'section',
            'label' => $label
        );
    }
}
