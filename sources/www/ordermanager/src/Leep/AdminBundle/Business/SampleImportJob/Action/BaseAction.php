<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Action;

class BaseAction {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
}