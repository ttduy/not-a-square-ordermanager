<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Action;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ChangeStatusAction extends BaseAction {
    public $status;
    public function __construct($container, $status) {
        parent::__construct($container);
        $this->status = $status;
    }    

    public function process($job) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Search status ID
        $status = $doctrine->getRepository('AppDatabaseMainBundle:MwaStatus')->findOneByName($this->status);
        if (empty($status)) {
            $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
            $job->setLastUpdate("Can't find the status: ".$this->status);

            return FALSE;
        }
        $statusId = $status->getId();

        // Search sample
        $data = json_decode($job->getData(), true);

        $job->setStatus(0);
        $sample = $doctrine->getRepository('AppDatabaseMainBundle:MwaSample')->findOneById($data['idMwaSample']);
        if ($sample) {
            // Check if done
            if ($sample->getCurrentStatus() == $statusId) {
                $job->setLastUpdate("Status changed to ".$this->status);

                return TRUE;
            }

            // If not done, check if it's possible
            $criteria = array(
                'idMwaStatus' => $sample->getCurrentStatus(),
                'idMwaNextStatus' => $statusId
            );
            $result = $doctrine->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findOneBy($criteria);
            if (empty($result)) {
                $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
                $job->setLastUpdate("Can't move to status: ".$this->status." from status ".$formatter->format($sample->getCurrentStatus(), 'mapping', 'LeepAdmin_MwaStatus_List'));

                return FALSE;    
            }

            // If it's possible, check if ready
            if ($sample->getIsSendNow() == 1 && 
                $sample->getPushStatus() == $statusId) {
                $job->setStatus(Business\SampleImportJob\Constants::STATUS_WAITING);
                $job->setLastUpdate("Waiting for status update");

                return FALSE;
            }

            // Else, set the job
            $job->setStatus(Business\SampleImportJob\Constants::STATUS_WAITING);
            $job->setLastUpdate("Set sample push sending on");
            $sample->setIsSendNow(1);
            $sample->setSendingStatus(0);
            $sample->setPushStatus($statusId);
            $sample->setPushDate(new \DateTime());
            $sample->setPushDatetime(new \DateTime());

            return FALSE;
        }
        else {
            $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
            $job->setLastUpdate("Can't find the sample");

            return FALSE;
        }
    }
}
