<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Action;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DetectPatternAction extends BaseAction {
    public function process($job) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $data = json_decode($job->getData(), true);

        $job->setStatus(0);
        $sample = $doctrine->getRepository('AppDatabaseMainBundle:MwaSample')->findOneById($data['idMwaSample']);
        if ($sample) {
            $formula = $doctrine->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($config->getIdFormulaTemplateMwaSample());
            if (empty($formula)) {
                $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
                $job->setLastUpdate("Can't find the formula template, please set formula template in parameter");
                
                return FALSE;
            }
            else {
                if (trim($sample->getGew1()) == '' || 
                    trim($sample->getGew2()) == '' ||
                    trim($sample->getGew3()) == '' ||
                    trim($sample->getGew4()) == '' ||
                    trim($sample->getGew5()) == '') {
                    $job->setLastUpdate("Skip pattern detection as gene result hasn't available");

                    return TRUE;
                }

                $pattern = Business\MwaSample\Utils::computePattern($this->container, $formula, $sample);
                if (in_array($pattern, array('PATTERN1', 'PATTERN2', 'PATTERN3', 'PATTERN4', 'PATTERN5', 'PATTERN6'))) {
                    $sample->setResultPattern($pattern);
                    $em->persist($sample);              

                    $job->setLastUpdate("Compute pattern: ".$pattern);

                    return TRUE;
                }
                else {
                    $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
                    $job->setLastUpdate("Incorrect pattern: ".$pattern);

                    return FALSE;
                }  
            }            
        }
        else {
            $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
            $job->setLastUpdate("Can't find the sample");

            return FALSE;
        }
    }
}
