<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Action;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class AddNewSampleAction extends BaseAction {
    private function parseDate($dateArr) {
        if (is_array($dateArr) && isset($dateArr['date'])) {
            return \DateTime::createFromFormat('Y-m-d H:i:s', $dateArr['date']);
        }
        return null;
    }
    public function process($job) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $em = $this->container->get('doctrine')->getEntityManager();

        $job->setLastUpdate('Add sample to database');

        $data = json_decode($job->getData(), true);

        $sample = new Entity\MwaSample();
        $sample->setSampleId($data['sampleId']);
        $sample->setSampleGroup("LYTECH IMPORT");
        $sample->setCurrentStatus($config->getMwaInitialStatus());
        $sample->setRegisteredDate($this->parseDate($data['registeredDate']));
        $sample->setFinishedDate($this->parseDate($data['finishedDate']));
        $sample->setIdGroup(Business\MwaSample\Constants::GROUP_RUSSIA);
        $sample->setGew1($data['gew1']);
        $sample->setGew2($data['gew2']);
        $sample->setGew3($data['gew3']);
        $sample->setGew4($data['gew4']);
        $sample->setGew5($data['gew5']);
        $em->persist($sample);
        $em->flush();

        $data['idMwaSample'] = $sample->getId();
        $job->setData(json_encode($data));
        $em->persist($job);

        return TRUE;
    }
}