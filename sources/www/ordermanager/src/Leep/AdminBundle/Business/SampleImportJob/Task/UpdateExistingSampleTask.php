<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Task;

use Leep\AdminBundle\Business\SampleImportJob\Constants;

class UpdateExistingSampleTask extends SequentialTask {
    public function getSequenceActions() {
        return array(
            Constants::ACTION_UPDATE_EXISTING_SAMPLE            => $this->container->get('leep_admin.sample_import_job.action.update_existing_sample'),
            Constants::ACTION_DETECT_PATTERN                    => $this->container->get('leep_admin.sample_import_job.action.detect_pattern')
        );
    }
}