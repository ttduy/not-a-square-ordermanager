<?php
namespace Leep\AdminBundle\Business\SampleImportJob;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idTask', 'sampleId', 'idNextAction', 'status', 'lastUpdate');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Task',          'width' => '15%',  'sortable' => 'false'),
            array('title' => 'Sample',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Next Action',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Last Update',   'width' => '40%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_SampleImportJobGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.lastUpdateTimestamp', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:SampleImportJob', 'p');

        if ($this->filters->idTask != 0) {
            $queryBuilder->andWhere('p.idTask = :idTask')
                ->setParameter('idTask', $this->filters->idTask);
        }
        if ($this->filters->status != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }
        if (trim($this->filters->sample) != '') {
            $queryBuilder->andWhere('p.sample LIKE :sample')
                ->setParameter('sample', '%'.trim($this->filters->sample).'%');
        }
    }

    public function buildCellLastUpdate($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        return $formatter->format($row->getLastUpdateTimestamp(), 'datetime').'</br>'.$row->getLastUpdate();
    }
}