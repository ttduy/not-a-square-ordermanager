<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Task;

use Leep\AdminBundle\Business\SampleImportJob\Constants;

class ImportNewSampleTask extends SequentialTask {
    public function getSequenceActions() {
        return array(
            Constants::ACTION_ADD_NEW_SAMPLE                    => $this->container->get('leep_admin.sample_import_job.action.add_new_sample'),
            Constants::ACTION_DETECT_PATTERN                    => $this->container->get('leep_admin.sample_import_job.action.detect_pattern'),
            Constants::ACTION_CHANGE_STATUS_ARRIVED             => $this->container->get('leep_admin.sample_import_job.action.change_status_arrived'),
            Constants::ACTION_CHANGE_STATUS_ANALYSIS_STARTED    => $this->container->get('leep_admin.sample_import_job.action.change_status_analysis_started'),
        );
    }
}