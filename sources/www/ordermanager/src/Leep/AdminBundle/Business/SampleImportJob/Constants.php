<?php
namespace Leep\AdminBundle\Business\SampleImportJob;

class Constants {
    // TASKS
    const TASK_IMPORT_NEW_SAMPLE = 1;
    const TASK_UPDATE_EXISTING_SAMPLE = 2;

    public static function getTasks() {
        return array(
            self::TASK_IMPORT_NEW_SAMPLE       => 'Import new sample',
            self::TASK_UPDATE_EXISTING_SAMPLE  => 'Update existing sample'
        );
    }

    public static function getTaskHandler($idTask, $container) {
        switch ($idTask) {
            case self::TASK_IMPORT_NEW_SAMPLE:
                return $container->get('leep_admin.sample_import_job.task.import_new_sample');
            case self::TASK_UPDATE_EXISTING_SAMPLE:
                return $container->get('leep_admin.sample_import_job.task.update_existing_sample');
        }
    }

    // ACTIONS 
    const ACTION_ADD_NEW_SAMPLE                       = 1;
    const ACTION_UPDATE_EXISTING_SAMPLE               = 2;
    const ACTION_DETECT_PATTERN                       = 20;
    const ACTION_CHANGE_STATUS_ARRIVED                = 30;
    const ACTION_CHANGE_STATUS_ANALYSIS_STARTED       = 31;
    const ACTION_CLEAN_UP                             = 100;
    public static function getActions() {
        return array(
            self::ACTION_ADD_NEW_SAMPLE                   => 'Add new sample',
            self::ACTION_UPDATE_EXISTING_SAMPLE           => 'Update existing sample',
            self::ACTION_DETECT_PATTERN                   => 'Detect pattern',
            self::ACTION_CHANGE_STATUS_ARRIVED            => 'Change status - Arrived',
            self::ACTION_CHANGE_STATUS_ANALYSIS_STARTED   => 'Change status - Analysis started',
            self::ACTION_CLEAN_UP                         => 'Clean up'
        );
    }

    // STATUS
    const STATUS_WAITING = 1;
    const STATUS_ERROR   = 2;
    public static function getStatus() {
        return array(
            self::STATUS_WAITING                  => 'Waiting',
            self::STATUS_ERROR                    => 'Error'
        );
    }
}
