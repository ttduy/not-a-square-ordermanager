<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Task;

abstract class SequentialTask {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    abstract public function getSequenceActions();

    public function process($job) {        
        $sequenceActions = $this->getSequenceActions();
        // Assign first action
        if ($job->getIdNextAction() == 0) {
            foreach ($sequenceActions as $idAction => $action) {
                $job->setLastUpdate('Assign first task');
                return $idAction;
            }
        }
        
        // Find the corresponding action
        $isMoveNextAction = FALSE;
        foreach ($sequenceActions as $idAction => $action) {
            if ($isMoveNextAction) {
                return $idAction;
            }

            if ($job->getIdNextAction() == $idAction) {
                $result = $action->process($job);
                if ($result === TRUE) {
                    $isMoveNextAction = true;
                }
                else {
                    return $job->getIdNextAction();
                }
            }
        }

        return -1;
    }   
}
