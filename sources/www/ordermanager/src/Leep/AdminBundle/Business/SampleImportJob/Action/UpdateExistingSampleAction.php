<?php
namespace Leep\AdminBundle\Business\SampleImportJob\Action;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateExistingSampleAction extends BaseAction {
    private function parseDate($dateArr) {
        if (is_array($dateArr) && isset($dateArr['date'])) {
            return \DateTime::createFromFormat('Y-m-d H:i:s', $dateArr['date']);
        }
        return null;
    }
    public function process($job) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $job->setLastUpdate('Update existing sample');

        $data = json_decode($job->getData(), true);

        $job->setStatus(0);
        $sample = $doctrine->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBy(array(
            'sampleId' => $data['sampleId'],
            'idGroup'  => Business\MwaSample\Constants::GROUP_RUSSIA
        ));
        if ($sample) {                
            $sample->setRegisteredDate($this->parseDate($data['registeredDate']));
            $sample->setFinishedDate($this->parseDate($data['finishedDate']));
            $sample->setGew1($data['gew1']);
            $sample->setGew2($data['gew2']);
            $sample->setGew3($data['gew3']);
            $sample->setGew4($data['gew4']);
            $sample->setGew5($data['gew5']);
            $em->persist($sample);

            $data['idMwaSample'] = $sample->getId();
            $job->setData(json_encode($data));
            $em->persist($job);

            return TRUE;
        }
        else {
            $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
            $job->setLastUpdate("Can't find the sample");

            return FALSE;
        }
    }
}
