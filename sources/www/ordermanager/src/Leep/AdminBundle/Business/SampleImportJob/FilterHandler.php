<?php
namespace Leep\AdminBundle\Business\SampleImportJob;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('idTask', 'choice', array(
            'label'    => 'Task',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_SampleImportJob_Task'),
            'empty_value' => false
        ));
        $builder->add('sample', 'text', array(
            'label'    => 'Sample',
            'required' => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_SampleImportJob_Status'),
            'empty_value' => false
        ));

        return $builder;
    }
}