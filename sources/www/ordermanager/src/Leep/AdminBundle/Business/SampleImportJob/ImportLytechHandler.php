<?php
namespace Leep\AdminBundle\Business\SampleImportJob;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class ImportLytechHandler extends BaseCreateHandler {
    const MODE_CHECK_ONLY = 1;
    const MODE_CHECK_THEN_IMPORT = 2;

    public $parsedSamples = array();
    public $errorSamples = array();

    public $startDate;
    public $startTime;

    public $content;

    public function getDefaultFormModel() {
        $today = new \DateTime();
        $todayTo30 = new \DateTime();
        $todayTo30->setTimestamp($today->getTimestamp() - 30*24*60*60);

        $model = new ImportLytechModel();
        $model->startDate = $todayTo30;
        $model->endDate = $today;
        $model->idMode = self::MODE_CHECK_ONLY;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start date',
            'required' => true
        ));
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End date',
            'required' => true
        ));
        $builder->add('idMode', 'choice', array(
            'label'    => 'Mode',
            'required' => true,
            'choices'  => array(
                self::MODE_CHECK_ONLY         => 'Check only',
                self::MODE_CHECK_THEN_IMPORT  => 'Check then import',
            )
        ));
    }

    private function checkLytechSuccess($content) {
        if (empty($content)) {
            $this->errors[] = "Login failed";
            return false;
        }

        $features = array(
            '<td class="style2">ID</td>',
            '<td class="style2">ARRIVED</td>',
            '<td class="style2">SAMPLE STATUS</td>',
            '<td class="style2">RESULTS GENERATED</td>',
            '<td class="style2">FABP2 (Ala54Thr,G/A) rs1799883</td>',
            '<td class="style2">ADRB2 (Gln27Glu, C/G) rs1042714</td>',
            '<title>RESULT</title>'
        );
        foreach ($features as $feature) {
            if (strpos($content, $feature) === FALSE) {
                $this->errors[] = "Can\'t recognize page structure";
                return false;
            }
        }

        return true;
    }

    private function parseLytechDate($date) {
        if (empty($date)) {
            return null;
        }

        return \DateTime::createFromFormat('d.m.Y', $date);
    }

    private $callCache = array();
    private function convertCall($geneName, $call) {
        $call = trim($call);

        if (empty($call)) {
            return '';
        }
        if (!isset($this->callCache[$geneName])) {
            $doctrine = $this->container->get('doctrine');
            $criteria = array('genename' => trim($geneName), 'isdeleted' => 0);
            $gene = $doctrine->getRepository('AppDatabaseMainBundle:Genes')->findOneBy($criteria);
            if (!$gene) {
                die("Can\'t find gene ".$geneName);
            }

            $mapping = array();
            $results = $doctrine->getRepository('AppDatabaseMainBundle:Results')->findBygroupid($gene->getGroupId());
            foreach ($results as $r) {
                $mapping[$r->getCallResult()] = $r->getResult();
            }

            $this->callCache[$geneName] = $mapping;
        }

        if (isset($this->callCache[$geneName][$call])) {
            return $this->callCache[$geneName][$call];
        }
        return false;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        if (empty($model->startDate) || empty($model->endDate)) {
            $this->errors[] = "Please define the period";
            return;
        }

        //////////////////////////////////////////////////
        // x. Grab data
        $curl = $this->container->get('leep_admin.helper.curl');
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        // set start date and start time
        $this->startDate = $model->startDate;
        $this->endDate = $model->endDate;

        // Do login
        $post = array(
            'dateorg0' => $model->startDate->format('d/m/Y'),
            'dateorg1' => $model->endDate->format('d/m/Y'),
            'codeorg'  => $config->getLytechUsername(),
            'codepas'  => $config->getLytechPassword()
        );
        $content = $curl->getPageContent($config->getLytechUrl(), 'POST', $post);

        $this->content = $content;

        if ($this->checkLytechSuccess($content)) {
            $tdNote = '\<td\sclass\=\"style3\"\>(.*?)\<\/td\>';
            $pattern = '/'.str_repeat($tdNote, 9).'/ism';

            preg_match_all($pattern, $content, $matches);
            if (isset($matches[9])) {
                foreach ($matches[0] as $i => $m) {
                    $sampleId = trim($matches[1][$i]);
                    if (empty($sampleId)) {
                        continue;
                    }

                    $arrivedDate = $this->parseLytechDate($matches[2][$i]);
                    $sampleStatus = $matches[3][$i];
                    $resultGeneratedDate = $this->parseLytechDate($matches[4][$i]);

                    $gew1 = $this->convertCall('GEW1', $matches[5][$i]);
                    $gew2 = $this->convertCall('GEW2', $matches[6][$i]);
                    $gew3 = $this->convertCall('GEW3', $matches[7][$i]);
                    $gew4 = $this->convertCall('GEW4', $matches[8][$i]);
                    $gew5 = $this->convertCall('GEW5', $matches[9][$i]);

                    $error = '';
                    if ($arrivedDate === FALSE || $resultGeneratedDate === FALSE) {
                        $error = 'Datetime has wrong format. Expect: d.m.Y';
                    }
                    if ($gew1 === FALSE || $gew2 === FALSE || $gew2 === FALSE || $gew2 === FALSE || $gew2 === FALSE) {
                        $error = 'Can\'t convert GEW call value';
                    }

                    if (empty($error)) {
                        $this->parsedSamples[] = array(
                            'sampleId' => $sampleId,
                            'registeredDate' => $arrivedDate,
                            'finishedDate' => $resultGeneratedDate,
                            'gew1'     => $gew1,
                            'gew2'     => $gew2,
                            'gew3'     => $gew3,
                            'gew4'     => $gew4,
                            'gew5'     => $gew5
                        );
                    }
                    else {
                        $this->errorSamples[] = array(
                            'sampleId'   => $sampleId,
                            'registeredDate' => $matches[2][$i],
                            'finishedDate' => $matches[4][$i],
                            'gew1'         => $matches[5][$i],
                            'gew2'         => $matches[6][$i],
                            'gew3'         => $matches[7][$i],
                            'gew4'         => $matches[8][$i],
                            'gew5'         => $matches[9][$i],
                            'error'        => $error
                        );
                    }
                }
            }
        }

        $this->messages[] = 'Done';
    }
}
