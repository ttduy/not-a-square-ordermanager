<?php
namespace Leep\AdminBundle\Business\CmsOrder\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppCmsOrderProduct extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\CmsOrder\FormType\AppCmsOrderProductModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_cms_order_product_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('productCode', 'text', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'width: 150px'
            )
        ));
        $builder->add('extraData', 'text', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'width: 90px'
            )
        ));
        $builder->add('priceWithoutTax', 'text', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'width: 90px'
            )
        ));
        $builder->add('priceWithTax', 'text', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'width: 90px'
            )
        ));
    }
}