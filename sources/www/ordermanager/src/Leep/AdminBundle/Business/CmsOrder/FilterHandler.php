<?php
namespace Leep\AdminBundle\Business\CmsOrder;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('orderCode', 'text', array(
            'label'    => 'Order Code',
            'required' => false
        ));
        $builder->add('idSource', 'choice', array(
            'label'    => 'Source',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_CmsSource_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        return $builder;
    }
}