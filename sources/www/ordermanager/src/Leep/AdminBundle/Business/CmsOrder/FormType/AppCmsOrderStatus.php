<?php
namespace Leep\AdminBundle\Business\CmsOrder\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppCmsOrderStatus extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\CmsOrder\FormType\AppCmsOrderStatusModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_cms_order_status_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('status', 'choice', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'min-width: 150px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_CmsOrderStatusValue_List')
        ));
        $builder->add('statusDate', 'datepicker', array(
            'required' => false,
            'attr'     => array(
                'style'   =>   'width: 90px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'required' => false,
            'attr'     => array(
                'rows'   => 2,
                'cols'   => 50
            )
        ));
    }
}