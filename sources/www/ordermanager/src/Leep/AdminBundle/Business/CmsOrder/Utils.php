<?php
namespace Leep\AdminBundle\Business\CmsOrder;

class Utils {
    public static function renderView($controller, $id) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $cmsOrder = $em->getRepository('AppDatabaseMainBundle:CmsOrder')->findOneById($id);

        $products = array();
        $result = $em->getRepository('AppDatabaseMainBundle:CmsOrderProduct')->findByIdCmsOrder($id, array('id' => 'ASC'));
        foreach ($result as $r) {
            $extraData = json_decode($r->getExtraData());
            $recommendationTextBox = '';
            if ($extraData){
                foreach ($extraData as $key => $value) {
                    if ( 'recommendationTextBox' == $key) {
                        $recommendationTextBox = $value;
                    }
                }
            }

            $products[] = array(
                'name'                    => $formatter->format($r->getProductCode(), 'mapping', 'LeepAdmin_CmsOrderProduct_List'),
                'code'                    => $r->getProductCode(),
                'priceWithoutTax'         => number_format($r->getPriceWithoutTax(), 2),
                'recommendationTextBox'   => $recommendationTextBox
            );
        }

        $discounts = array();
        $result = $em->getRepository('AppDatabaseMainBundle:CmsOrderDiscount')->findByIdCmsOrder($id, array('id' => 'ASC'));
        foreach ($result as $r) {
            $discounts[] = array(
                'title'             => $r->getTitle(),
                'code'              => $r->getDiscountCode(),
                'amount'            => number_format($r->getAmount(), 2)
            );
        }

        $data = array(
            'cmsOrder'          => $cmsOrder,
            'products'          => $products,
            'discount'          => $discounts,
            'couponCode'        => $cmsOrder->getCouponCode(),
            'subtotalAmount'    => number_format($cmsOrder->getSubtotalAmount(), 2),
            'discountAmount'    => number_format($cmsOrder->getDiscountAmount(), 2),
            'totalAmount'       => number_format($cmsOrder->getTotalAmount(), 2),
            'taxAmount'         => number_format($cmsOrder->getTaxAmount(), 2),
            'grandTotalAmount'  => number_format($cmsOrder->getGrandTotalAmount(), 2),
        );

        return $controller->get('templating')->render('LeepAdminBundle:CmsOrder:view.html.twig', $data);
    }
}
