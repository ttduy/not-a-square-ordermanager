<?php
namespace Leep\AdminBundle\Business\CmsOrder;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->timestamp = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionBasic', 'section', array(
            'label'    => 'Order Information'
        ));
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('orderCode', 'text', array(
            'label'    => 'Order Code',
            'required' => false
        ));
        $builder->add('idSource', 'choice', array(
            'label'    => 'Source',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsSource_List')
        ));
        $builder->add('currencyCode', 'text', array(
            'label'    => 'Currency Code',
            'required' => false
        ));
        $builder->add('languageCode', 'text', array(
            'label'    => 'Language Code',
            'required' => false
        ));
        $builder->add('userIp', 'text', array(
            'label'    => 'User IP',
            'required' => false
        ));

        $builder->add('sectionOrderDetail', 'section', array(
            'label'    => 'Order Details'
        ));
        $builder->add('fullname', 'text', array(
            'label'    => 'Fullname',
            'required' => false
        ));
        $builder->add('telephoneNumber', 'text', array(
            'label'    => 'Telephone Number',
            'required' => false
        ));
        $builder->add('previousOrderNumber', 'text', array(
            'label'    => 'Previous Order Number (if any)',
            'required' => false
        ));
        $builder->add('emailAddress', 'text', array(
            'label'    => 'Email Address',
            'required' => false
        ));
        $builder->add('birthday', 'text', array(
            'label'    => 'Birthday',
            'required' => false
        ));



        $builder->add('sectionInvoice', 'section', array(
            'label'    => 'Invoice Address'
        ));
        $builder->add('invoiceFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('invoiceSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('invoiceStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoicePostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceCountryCode', 'text', array(
            'label'    => 'Country Code',
            'required' => false
        ));



        $builder->add('sectionShipping', 'section', array(
            'label'    => 'Shipping Address'
        ));
        $builder->add('shippingIsSameInvoice', 'checkbox', array(
            'label'    => 'Shipping is same invoice?',
            'required' => false
        ));
        $builder->add('shippingFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('shippingSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('shippingStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('shippingCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('shippingPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('shippingCountryCode', 'text', array(
            'label'    => 'Country Code',
            'required' => false
        ));


        $builder->add('sectionNotes', 'section', array(
            'label'    => 'Notes'
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));

        $builder->add('sectionProductOrder', 'section', array(
            'label'    => 'Products'
        ));
        $builder->add('cmsProducts', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppCmsOrderProduct($this->container)
        ));


        $builder->add('sectionStatus', 'section', array(
            'label'    => 'Status'
        ));
        $builder->add('cmsStatus', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppCmsOrderStatus($this->container)
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        foreach ($model->cmsProducts as $p) {
            if (empty($p->productCode)) {
                $error[] = "Empty product code (Products Section)!";
                return false;
            }
        }

        $cmsOrder = new Entity\CmsOrder();
        $cmsOrder->setOrderCode($model->orderCode);
        $cmsOrder->setTimestamp($model->timestamp);
        $cmsOrder->setIdSource($model->idSource);
        $cmsOrder->setCurrencyCode($model->currencyCode);
        $cmsOrder->setLanguageCode($model->languageCode);
        $cmsOrder->setUserIp($model->userIp);
        $cmsOrder->setFullname($model->fullname);
        $cmsOrder->setTelephoneNumber($model->telephoneNumber);
        $cmsOrder->setPreviousOrderNumber($model->previousOrderNumber);
        $cmsOrder->setEmailAddress($model->emailAddress);
        $cmsOrder->setBirthday($model->birthday);

        $cmsOrder->setInvoiceFirstName($model->invoiceFirstName);
        $cmsOrder->setInvoiceSurName($model->invoiceSurName);
        $cmsOrder->setInvoiceStreet($model->invoiceStreet);
        $cmsOrder->setInvoiceCity($model->invoiceCity);
        $cmsOrder->setInvoicePostCode($model->invoicePostCode);
        $cmsOrder->setInvoiceCountryCode($model->invoiceCountryCode);

        $cmsOrder->setShippingIsSameInvoice($model->shippingIsSameInvoice);
        $cmsOrder->setShippingFirstName($model->shippingFirstName);
        $cmsOrder->setShippingSurName($model->shippingSurName);
        $cmsOrder->setShippingStreet($model->shippingStreet);
        $cmsOrder->setShippingCity($model->shippingCity);
        $cmsOrder->setShippingPostCode($model->shippingPostCode);
        $cmsOrder->setShippingCountryCode($model->shippingCountryCode);
        $cmsOrder->setNotes($model->notes);
        $em->persist($cmsOrder);
        $em->flush();

        // Product List
        $totalPriceWithoutTax = 0;
        $totalPriceWithTax = 0;
        foreach ($model->cmsProducts as $p) {
            $product = new Entity\CmsOrderProduct();
            $product->setIdCmsOrder($cmsOrder->getId());
            $product->setProductCode($p->productCode);
            $product->setExtraData($p->extraData);
            $product->setPriceWithoutTax($p->priceWithoutTax);
            $product->setPriceWithTax($p->priceWithTax);
            $em->persist($product);

            $totalPriceWithoutTax += $p->priceWithoutTax;
            $totalPriceWithTax += $p->priceWithTax;
        }

        $cmsOrder->setTotalWithoutTax($totalPriceWithoutTax);
        $cmsOrder->setTotalWithTax($totalPriceWithTax);

        // Status List
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->cmsStatus as $s) {
            $status = new Entity\CmsOrderStatus();
            $status->setIdCmsOrder($cmsOrder->getId());
            $status->setStatus($s->status);
            $status->setStatusDate($s->statusDate);
            $status->setNotes($s->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $currentStatus = $s->status;
            $currentStatusDate = $s->statusDate;
        }
        $cmsOrder->setCurrentStatus($currentStatus);
        $cmsOrder->setCurrentStatusDate($currentStatusDate);

        $em->persist($cmsOrder);
        $em->flush();

        parent::onSuccess();
    }
}
