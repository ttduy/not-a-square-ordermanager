<?php
namespace Leep\AdminBundle\Business\CmsOrder;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $orderCode;
    public $timestamp;
    public $idSource;
    public $currencyCode;
    public $languageCode;
    public $userIp;
    public $fullname;
    public $telephoneNumber;
    public $previousOrderNumber;
    public $emailAddress;
    public $birthday;
    
    public $invoiceFirstName;
    public $invoiceSurName;
    public $invoiceStreet;
    public $invoiceCity;
    public $invoicePostCode;
    public $invoiceCountryCode;

    public $shippingIsSameInvoice;
    public $shippingFirstName;
    public $shippingSurName;
    public $shippingStreet;
    public $shippingCity;
    public $shippingPostCode;
    public $shippingCountryCode;
    public $notes;
    
    public $cmsProducts;
    public $cmsStatus;
}
