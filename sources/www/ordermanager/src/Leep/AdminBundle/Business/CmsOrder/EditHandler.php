<?php
namespace Leep\AdminBundle\Business\CmsOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CmsOrder', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();
        $model->orderCode = $entity->getOrderCode();
        $model->timestamp = $entity->getTimestamp();
        $model->idSource = $entity->getIdSource();
        $model->currencyCode = $entity->getCurrencyCode();
        $model->languageCode = $entity->getLanguageCode();
        $model->userIp = $entity->getUserIp();
        $model->fullname = $entity->getFullname();
        $model->telephoneNumber = $entity->getTelephoneNumber();
        $model->previousOrderNumber = $entity->getPreviousOrderNumber();
        $model->emailAddress = $entity->getEmailAddress();
        $model->birthday = $entity->getBirthday();

        $model->invoiceFirstName = $entity->getInvoiceFirstName();
        $model->invoiceSurName = $entity->getInvoiceSurName();
        $model->invoiceStreet = $entity->getInvoiceStreet();
        $model->invoiceCity = $entity->getInvoiceCity();
        $model->invoicePostCode = $entity->getInvoicePostCode();
        $model->invoiceCountryCode = $entity->getInvoiceCountryCode();

        $model->shippingIsSameInvoice = $entity->getShippingIsSameInvoice();
        $model->shippingFirstName = $entity->getShippingFirstName();
        $model->shippingSurName = $entity->getShippingSurName();
        $model->shippingStreet = $entity->getShippingStreet();
        $model->shippingCity = $entity->getShippingCity();
        $model->shippingPostCode = $entity->getShippingPostCode();
        $model->shippingCountryCode = $entity->getShippingCountryCode();
        $model->notes = $entity->getNotes();

        // Product
        $model->cmsProducts = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CmsOrderProduct', 'p')
            ->andWhere('p.idCmsOrder = :idCmsOrder')
            ->setParameter('idCmsOrder', $entity->getId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $p) {
            $product = new FormType\AppCmsOrderProductModel();
            $product->productCode = $p->getProductCode();
            $product->extraData = $p->getExtraData();
            $product->priceWithoutTax = $p->getPriceWithoutTax();
            $product->priceWithTax = $p->getPriceWithTax();

            $model->cmsProducts[] = $product;
        }

        // Status
        $model->cmsStatus = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CmsOrderStatus', 'p')
            ->andWhere('p.idCmsOrder = :idCmsOrder')
            ->setParameter('idCmsOrder', $entity->getId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $p) {
            $status = new FormType\AppCmsOrderStatusModel();
            $status->status = $p->getStatus();
            $status->statusDate = $p->getStatusDate();
            $status->notes = $p->getNotes();

            $model->cmsStatus[] = $status;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionBasic', 'section', array(
            'label'    => 'Order Information'
        ));
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('orderCode', 'text', array(
            'label'    => 'Order Code',
            'required' => false
        ));
        $builder->add('idSource', 'choice', array(
            'label'    => 'Source',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsSource_List')
        ));
        $builder->add('currencyCode', 'text', array(
            'label'    => 'Currency Code',
            'required' => false
        ));
        $builder->add('languageCode', 'text', array(
            'label'    => 'Language Code',
            'required' => false
        ));
        $builder->add('userIp', 'text', array(
            'label'    => 'User IP',
            'required' => false
        ));
        $builder->add('sectionOrderDetail', 'section', array(
            'label'    => 'Order Details'
        ));
        $builder->add('fullname', 'text', array(
            'label'    => 'Fullname',
            'required' => false
        ));
        $builder->add('telephoneNumber', 'text', array(
            'label'    => 'Telephone Number',
            'required' => false
        ));
        $builder->add('previousOrderNumber', 'text', array(
            'label'    => 'Previous Order Number (if any)',
            'required' => false
        ));
        $builder->add('emailAddress', 'text', array(
            'label'    => 'Email Address',
            'required' => false
        ));
        $builder->add('birthday', 'text', array(
            'label'    => 'Birthday',
            'required' => false
        ));



        $builder->add('sectionInvoice', 'section', array(
            'label'    => 'Invoice Address'
        ));
        $builder->add('invoiceFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('invoiceSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('invoiceStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoicePostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceCountryCode', 'text', array(
            'label'    => 'Country Code',
            'required' => false
        ));



        $builder->add('sectionShipping', 'section', array(
            'label'    => 'Shipping Address'
        ));
        $builder->add('shippingIsSameInvoice', 'checkbox', array(
            'label'    => 'Shipping is same invoice?',
            'required' => false
        ));
        $builder->add('shippingFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('shippingSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('shippingStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('shippingCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('shippingPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('shippingCountryCode', 'text', array(
            'label'    => 'Country Code',
            'required' => false
        ));


        $builder->add('sectionNotes', 'section', array(
            'label'    => 'Notes'
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));

        $builder->add('sectionProductOrder', 'section', array(
            'label'    => 'Products'
        ));
        $builder->add('cmsProducts', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppCmsOrderProduct($this->container)
        ));

        $builder->add('sectionStatus', 'section', array(
            'label'    => 'Status'
        ));
        $builder->add('cmsStatus', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppCmsOrderStatus($this->container)
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        foreach ($model->cmsProducts as $p) {
            if (empty($p->productCode)) {
                $error[] = "Empty product code (Products Section)!";
                return false;
            }
        }

        $this->entity->setOrderCode($model->orderCode);
        $this->entity->setTimestamp($model->timestamp);
        $this->entity->setIdSource($model->idSource);
        $this->entity->setCurrencyCode($model->currencyCode);
        $this->entity->setLanguageCode($model->languageCode);
        $this->entity->setUserIp($model->userIp);
        $this->entity->setFullname($model->fullname);
        $this->entity->setTelephoneNumber($model->telephoneNumber);
        $this->entity->setPreviousOrderNumber($model->previousOrderNumber);
        $this->entity->setEmailAddress($model->emailAddress);
        $this->entity->setBirthday($model->birthday);

        $this->entity->setInvoiceFirstName($model->invoiceFirstName);
        $this->entity->setInvoiceSurName($model->invoiceSurName);
        $this->entity->setInvoiceStreet($model->invoiceStreet);
        $this->entity->setInvoiceCity($model->invoiceCity);
        $this->entity->setInvoicePostCode($model->invoicePostCode);
        $this->entity->setInvoiceCountryCode($model->invoiceCountryCode);

        $this->entity->setShippingIsSameInvoice($model->shippingIsSameInvoice);
        $this->entity->setShippingFirstName($model->shippingFirstName);
        $this->entity->setShippingSurName($model->shippingSurName);
        $this->entity->setShippingStreet($model->shippingStreet);
        $this->entity->setShippingCity($model->shippingCity);
        $this->entity->setShippingPostCode($model->shippingPostCode);
        $this->entity->setShippingCountryCode($model->shippingCountryCode);
        $this->entity->setNotes($model->notes);
        $em->persist($this->entity);
        $em->flush();

        // Product
        $totalPriceWithoutTax = 0;
        $totalPriceWithTax = 0;
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('idCmsOrder' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CmsOrderProduct', $filters);
        foreach ($model->cmsProducts as $p) {
            $product = new Entity\CmsOrderProduct();
            $product->setIdCmsOrder($this->entity->getId());
            $product->setProductCode($p->productCode);
            $product->setExtraData($p->extraData);
            $product->setPriceWithoutTax($p->priceWithoutTax);
            $product->setPriceWithTax($p->priceWithTax);
            $em->persist($product);

            $totalPriceWithoutTax += $p->priceWithoutTax;
            $totalPriceWithTax += $p->priceWithTax;
        }
        $this->entity->setTotalWithoutTax($totalPriceWithoutTax);
        $this->entity->setTotalWithTax($totalPriceWithTax);

        // Status
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('idCmsOrder' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CmsOrderStatus', $filters);
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->cmsStatus as $s) {
            $status = new Entity\CmsOrderStatus();
            $status->setIdCmsOrder($this->entity->getId());
            $status->setStatus($s->status);
            $status->setStatusDate($s->statusDate);
            $status->setNotes($s->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $currentStatus = $s->status;
            $currentStatusDate = $s->statusDate;
        }
        $this->entity->setCurrentStatus($currentStatus);
        $this->entity->setCurrentStatusDate($currentStatusDate);

        $em->flush();

        parent::onSuccess();
    }
}
