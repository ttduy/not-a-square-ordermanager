<?php
namespace Leep\AdminBundle\Business\CmsOrder;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }
    public function getColumnMapping() {
        return array('timestamp', 'orderCode', 'idSource', 'fullname', 'currencyCode', 'languageCode', 'totalAmountWithTax', 'orderPayment', 'currentStatus', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Timestamp',                   'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Order Code',                  'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Source',                      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Fullname',                    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Currency',                    'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Language',                    'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Price (with Tax)',            'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Payment',                     'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Status',                      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '8%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CmsOrderGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CmsOrder', 'p');
        if (trim($this->filters->orderCode) != '') {
            $queryBuilder->andWhere('p.orderCode LIKE :orderCode')
                ->setParameter('orderCode', '%'.trim($this->filters->orderCode).'%');
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->idSource) != 0) {
            $queryBuilder->andWhere('p.message = :idSource')
                ->setParameter('idSource', intval($this->filters->idSource));
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.timestamp', 'DESC');
    }

    public function buildCellOrderPayment($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'cms_order_payment', 'grid', 'list', array('idParent' => $row->getId())), 'button-edit');

        $html = number_format($row->getTotalPayment(), 2);
        $html .= '&nbsp;&nbsp;'.$builder->getHtml();

        return $html;
    }

    public function buildCellTotalWithTax($row) {
        return number_format($row->getTotalWithTax(), 2);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cmsOrderModify')) {
            $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'cms_order', 'feature', 'view', array('id' => $row->getId())), 'button-detail');
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cms_order', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cms_order', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }
    public function buildCellCurrentStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_CmsOrderStatusValue_List', $row->getCurrentStatus());

        $age = $this->getAge($row->getCurrentStatusDate());
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }
}
