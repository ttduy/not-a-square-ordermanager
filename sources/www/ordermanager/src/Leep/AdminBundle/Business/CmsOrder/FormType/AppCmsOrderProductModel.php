<?php
namespace Leep\AdminBundle\Business\CmsOrder\FormType;

class AppCmsOrderProductModel {
    public $productCode;
    public $extraData;
    public $priceWithoutTax;
    public $priceWithTax;
}
