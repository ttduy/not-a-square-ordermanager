<?php
namespace Leep\AdminBundle\Business\SalesAnalysis;

use Leep\AdminBundle\Business\Base\AppEditHandler;

class SummaryHandler extends AppEditHandler {
    private $products;
    private $categories;
    private $colorPool = [];

    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        // Load filter config
        $filterConfig = $config->getSalesAnalysisFilterConfig();
        if ($filterConfig && trim($filterConfig) != "") {
            $filterConfig = json_decode($filterConfig);
            return $filterConfig;
        } else {
            return (object) [
                'year' => null,
                'products' => [],
                'categories' => []
            ];
        }
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));

        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => ['style' => 'min-height:100px; min-height: 150px; resize: both']
        ));

        $builder->add('categories', 'choice', array(
            'label'    => 'Product Categories',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Category_List'),
            'multiple' => 'multiple',
            'attr'     => ['style' => 'min-height:100px; min-height: 150px; resize: both']
        ));
    }

    public function onSuccess() {
        // Save filter config
        $em = $this->container->get('doctrine')->getEntityManager();
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $filterConfig = $this->getForm()->getData();

        // Validate year
        $date = \DateTime::createFromFormat('Y', $filterConfig->year);
        if (!$date) {
            $this->errors[] = "Invalid Year value!";
            return false;
        }

        // HACK FIX: Reset to array
        // Sometime, the result is an assosiative array. And json encode to object instead of array
        $products = [];
        foreach ($filterConfig->products as $product) {
            $products[] = $product;
        }

        $filterConfig->products = $products;

        $categories = [];
        foreach ($filterConfig->categories as $category) {
            $categories[] = $category;
        }

        $filterConfig->categories = $categories;

        // Save config
        $config->setSalesAnalysisFilterConfig(json_encode($filterConfig));
        $em->persist($config);
        $em->flush();
    }

    public function postProcessUpdateView(&$data, $options) {
        // Config entity manager
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTH', 'Leep\AdminBundle\Doctrine\Extension\Month');
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Doctrine\Extension\Year');

        // Default
        $this->init();
        $filterConfig = $this->getForm()->getData();
        $result = [];

        // Get filter date (Default today)
        $thisYear = intval((new \DateTime())->format('Y'));
        if (\DateTime::createFromFormat('Y', $filterConfig->year)) {
            $thisYear = intval($filterConfig->year);
        }

        // Get filter result for this year
        $result['thisYear'] = $this->getSalesAnalysisData($em, $thisYear, $filterConfig);

        // Get filter result for last year
        $lastYear = $thisYear - 1;
        $result['lastYear'] = $this->getSalesAnalysisData($em, $lastYear, $filterConfig);
        $result['chartData'] = $this->createChartData($result['thisYear']);
        $data['salesAnalysis'] = $result;
    }

    private function init() {
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->products = [];
        $products = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findAll();
        foreach ($products as $product) {
            $this->products[$product->getId()] = $product->getName();
        }

        $this->categories = [];
        $categories = $em->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findAll();
        foreach ($categories as $category) {
            $this->categories[$category->getId()] = $category->getCategoryname();
        }
    }

    private function getSalesAnalysisData($em, $year, $filterConfig) {
        $result = [
            'products' => [],
            'categories' => [],
            'total_products' => [],
            'total_categories' => [],
            'total_all' => []
        ];

        // Filter ordered products
        foreach ($filterConfig->products as $productId) {
            if (!isset($this->products[$productId])) {
                continue;
            }

            $query = $em->createQueryBuilder();
            $orderedList = $query->select('c.dateordered, o.customerid')
                ->from('AppDatabaseMainBundle:OrderedProduct', 'o')
                ->innerJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'o.customerid = c.id')
                ->where('YEAR(c.dateordered) = :year')->setParameter('year', intval($year))
                ->andWhere('o.productid = :product')->setParameter('product', $productId)
                ->getQuery()->getResult();

            $result['products'][$productId] = [
                'name' => $this->products[$productId],
                'total' => 0,
                'totalByOrder' => 0,
                'customers' => [],
                'data' => [
                    ['customers' => [], 'total' => 0], //Jan
                    ['customers' => [], 'total' => 0], //Feb
                    ['customers' => [], 'total' => 0], //Mar
                    ['customers' => [], 'total' => 0], //Apr
                    ['customers' => [], 'total' => 0], //May
                    ['customers' => [], 'total' => 0], //Jun
                    ['customers' => [], 'total' => 0], //Jul
                    ['customers' => [], 'total' => 0], //Aug
                    ['customers' => [], 'total' => 0], //Sep
                    ['customers' => [], 'total' => 0], //Oct
                    ['customers' => [], 'total' => 0], //Nov
                    ['customers' => [], 'total' => 0], //Dec
                ]
            ];

            foreach ($orderedList as $order) {
                $date = $order['dateordered'];
                $id = $order['customerid'];
                $month = intval($date->format('m')) - 1;
                if (!in_array($id, $result['products'][$productId]['data'][$month])) {
                    $result['products'][$productId]['data'][$month]['customers'][] = $id;
                    $result['products'][$productId]['data'][$month]['total']++;
                    $result['products'][$productId]['total']++;
                    $result['products'][$productId]['customers'][] = $id;
                }
            }

            $result['products'][$productId]['customers'] = array_unique($result['products'][$productId]['customers']);
            $result['products'][$productId]['totalByOrder'] = count($result['products'][$productId]['customers']);
        }

        // Analysis and combine data
        $result['total_products'] = $this->analysisAndCombine($result['products']);

        // Filter ordered categories
        foreach ($filterConfig->categories as $categoryId) {
            if (!isset($this->categories[$categoryId])) {
                continue;
            }

            $query = $em->createQueryBuilder();
            $orderedList = $query->select('c.dateordered, o.customerid')
                ->from('AppDatabaseMainBundle:OrderedCategory', 'o')
                ->innerJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'o.customerid = c.id')
                ->where('YEAR(c.dateordered) = :year')->setParameter('year', intval($year))
                ->andWhere('o.categoryid = :category')->setParameter('category', $categoryId)
                ->getQuery()->getResult();

            $result['categories'][$categoryId] = [
                'name' => $this->categories[$categoryId],
                'total' => 0,
                'totalByOrder' => 0,
                'customers' => [],
                'data' => [
                    ['customers' => [], 'total' => 0], //Jan
                    ['customers' => [], 'total' => 0], //Feb
                    ['customers' => [], 'total' => 0], //Mar
                    ['customers' => [], 'total' => 0], //Apr
                    ['customers' => [], 'total' => 0], //May
                    ['customers' => [], 'total' => 0], //Jun
                    ['customers' => [], 'total' => 0], //Jul
                    ['customers' => [], 'total' => 0], //Aug
                    ['customers' => [], 'total' => 0], //Sep
                    ['customers' => [], 'total' => 0], //Oct
                    ['customers' => [], 'total' => 0], //Nov
                    ['customers' => [], 'total' => 0], //Dec
                ]
            ];

            foreach ($orderedList as $order) {
                $date = $order['dateordered'];
                $id = $order['customerid'];
                $month = intval($date->format('m')) - 1;
                if (!in_array($id, $result['categories'][$categoryId]['data'][$month])) {
                    $result['categories'][$categoryId]['data'][$month]['customers'][] = $id;
                    $result['categories'][$categoryId]['data'][$month]['total']++;
                    $result['categories'][$categoryId]['total']++;
                    $result['categories'][$categoryId]['customers'][] = $id;
                }
            }

            $result['categories'][$categoryId]['customers'] = array_unique($result['categories'][$categoryId]['customers']);
            $result['categories'][$categoryId]['totalByOrder'] = count($result['categories'][$categoryId]['customers']);
        }

        // Analysis and combine data
        $result['total_categories'] = $this->analysisAndCombine($result['categories']);
        $result['total_all'] = $this->totalAnalysisCombine($result);
        return $result;
    }

    private function analysisAndCombine($analysisData) {
        $result = [
            'total' => 0,
            'customers' => [],
            'totalByOrder' => 0,
            'data' => [
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jan
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Feb
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Mar
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Apr
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //May
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jun
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jul
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Aug
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Sep
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Oct
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Nov
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Dec
            ]
        ];

        foreach ($analysisData as $id => $entry) {
            foreach ($entry['data'] as $month => $order) {
                $result['data'][$month]['customers'] += $order['customers'];
                $result['data'][$month]['total'] += $order['total'];
            }
        }

        foreach ($result['data'] as $month => $entry) {
            $result['data'][$month]['customers'] = array_unique($entry['customers']);
            $result['data'][$month]['totalByOrder'] = count($result['data'][$month]['customers']);
            $result['total'] += $entry['total'];
            $result['customers'] += $result['data'][$month]['customers'];
        }

        $result['customers'] = array_unique($result['customers']);
        $result['totalByOrder'] = count($result['customers']);
        return $result;
    }

    private function totalAnalysisCombine($analysisData) {
        $result = [
            'total' => 0,
            'customers' => [],
            'totalByOrder' => 0,
            'data' => [
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jan
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Feb
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Mar
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Apr
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //May
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jun
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Jul
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Aug
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Sep
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Oct
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Nov
                ['customers' => [], 'total' => 0, 'totalByOrder' => 0], //Dec
            ]
        ];

        $productsEntry = $analysisData['total_products'];
        $categoriesEntry = $analysisData['total_categories'];

        foreach ($result['data'] as $month => $entry) {
            $result['data'][$month]['customers'] = array_unique($productsEntry['data'][$month]['customers'] + $categoriesEntry['data'][$month]['customers']);
            $result['data'][$month]['total'] = $productsEntry['data'][$month]['total'] + $categoriesEntry['data'][$month]['total'];
            $result['data'][$month]['totalByOrder'] = count($result['data'][$month]['customers']);
            $result['total'] += $result['data'][$month]['total'];
            $result['customers'] += $result['data'][$month]['customers'];
        }

        $result['customers'] = array_unique($result['customers']);
        $result['totalByOrder'] = count($result['customers']);
        return $result;
    }

    private function createChartData($analysisData) {
        $totalCharts = count($analysisData['products']) + count($analysisData['categories']);
        $result = [];
        foreach ($analysisData['products'] as $product) {
            $charts = [];
            foreach ($product['data'] as $month => $data) {
                $charts[] = [$month + 1, $data['total']];
            }

            $color = $this->chooseNextColor();
            $result[$product['name']] = [
                'color' => $color,
                'charts' => json_encode($charts)
            ];
        }

        foreach ($analysisData['categories'] as $category) {
            $charts = [];
            foreach ($category['data'] as $month => $data) {
                $charts[] = [$month + 1, $data['total']];
            }

            $color = $this->chooseNextColor();
            $result[$category['name']] = [
                'color' => $color,
                'charts' => json_encode($charts)
            ];
        }

        return $result;
    }

    private function chooseNextColor() {
        do {
            $color = $this->randomColor();
        } while (in_array($color, $this->colorPool));

        $this->colorPool[] = $color;
        return "#$color";
    }

    private function randomPolorPart() {
        return str_pad(dechex(mt_rand(0,255)), 2, '0', STR_PAD_LEFT);
    }

    private function randomColor() {
        return $this->randomPolorPart().$this->randomPolorPart().$this->randomPolorPart();
    }
}
