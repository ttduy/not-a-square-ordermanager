<?php
namespace Leep\AdminBundle\Business\SubOrder;

class FilterModel {
	public $orderNumber;
	public $barcode;
	public $dc;
    public $name;
    public $mustBeStatus;
    public $mustNotBeStatus;
}