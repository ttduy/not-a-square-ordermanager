<?php
namespace Leep\AdminBundle\Business\SubOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditStatusHandler extends AppEditHandler {   
    public $idSubOrder = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SubOrder', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $this->idSubOrder = $entity->getId();

        $statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:SubOrderStatus', 'p')
            ->andWhere('p.idSubOrder = :idSubOrder')
            ->setParameter('idSubOrder', $entity->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new FormType\SubOrderStatusBlockModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $statusList[] = $m;
        }

        $data = array(
                        'statusList'    => $statusList, 
                        'orderNumber'   => '',
                        'barcode'       => $entity->getBarcode(),
                        'productGroup'  => '',
                        'customerName'  => ''
                    );

        // get some identification info
        $queryInfo = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $info = $queryInfo->select('
                                        pg.name AS productGroupName,
                                        ci.firstName, ci.surName,
                                        o.ordernumber as orderNumber
                                    ')
                        ->from('AppDatabaseMainBundle:SubOrder', 'p')
                        ->innerJoin('AppDatabaseMainBundle:ProductGroup', 'pg', 'WITH', 'pg.id = p.idProductGroup')
                        ->innerJoin('AppDatabaseMainBundle:Customer', 'o', 'WITH', 'o.id = p.idOrder')
                        ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'ci', 'WITH', 'ci.id = o.idCustomerInfo')
                        ->andWhere('p.id = :idSubOrder')
                        ->setParameter('idSubOrder', $entity->getId())
                        ->getQuery()
                        ->getSingleResult();

        if ($info) {
            $data['orderNumber']  = $info['orderNumber'];
            $data['productGroup'] = $info['productGroupName'];
            $data['customerName'] = $info['firstName'] . ' ' . $info['surName'];
        }

        return $data;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('section1',          'section', array(
            'label' => 'Sub Order',
            'property_path' => false
        ));
        $builder->add('orderNumber', 'label', array(
            'label' => 'Order Number',
            'required'      => false
        ));
        $builder->add('productGroup', 'label', array(
            'label' => 'Product Group',
            'required'      => false
        ));
        $builder->add('barcode', 'label', array(
            'label' => 'Barcode',
            'required'      => false
        ));
        $builder->add('customerName', 'label', array(
            'label' => 'Customer Name',
            'required'      => false
        ));

        $builder->add('section2',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\SubOrderStatusBlock($this->container),
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();

        $filters = array('idSubOrder' => $this->idSubOrder);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:SubOrderStatus', $filters);

        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model['statusList'] as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\SubOrderStatus();
            $status->setIdSubOrder($this->idSubOrder);
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
            
            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }

        $this->entity->setCurrentStatus($lastStatus);
        $this->entity->setCurrentStatusDate($lastDate);

        $em->flush();        

        parent::onSuccess();
    }
}
