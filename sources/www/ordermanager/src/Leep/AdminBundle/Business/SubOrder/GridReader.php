<?php
namespace Leep\AdminBundle\Business\SubOrder;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'orderNumber', 'group', 'barcode', 'customerName', 'dc', 'age', 'status', 'info', 'action');
    }
    
    public function getTableHeader() {
        return array(
            array('title' => 'Id',              'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Order Number',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Group',           'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Barcode',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Customer Name',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'DC',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Age',             'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Status',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Info',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',          'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $orderNumber = trim($this->filters->orderNumber);
        $barcode = trim($this->filters->barcode);
        $dc = $this->filters->dc;
        $name = trim($this->filters->name);
        $mustBeStatus = $this->filters->mustBeStatus;
        $mustNotBeStatus = $this->filters->mustNotBeStatus;

        $queryBuilder
            ->select('
                        p.id,
                        p.barcode,
                        p.currentStatus AS status,
                        p.currentStatusDate as statusDate,
                        p.idOrder,
                        c.ordernumber AS orderNumber,
                        c.dateordered AS dateOrdered,
                        c.orderInfoText,
                        c.laboratoryDetails,
                        ci.firstName AS customerFirstName, ci.surName AS customerSurName,
                        dc.distributionchannel,
                        pg.name AS productGroupName
                    ')
            ->from('AppDatabaseMainBundle:SubOrder', 'p')
            ->innerJoin('AppDatabaseMainBundle:ProductGroup', 'pg', 'WITH', 'pg.id = p.idProductGroup')
            ->innerJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'c.id = p.idOrder')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = c.distributionchannelid')
            ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'ci', 'WITH', 'ci.id = c.idCustomerInfo');

        if ($barcode) {
            $queryBuilder->andWhere('p.barcode LIKE :barcode')
                ->setParameter('barcode', '%'.$barcode.'%');
        }

        if ($name) {
            $queryBuilder->andWhere('pg.name LIKE :name')
                ->setParameter('name', '%'.$name.'%');
        }

        if ($orderNumber) {
            $queryBuilder->andWhere('c.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%' . $orderNumber . '%');
        }

        if ($dc) {
            $queryBuilder->andWhere('c.distributionchannelid = :dc')
                ->setParameter('dc', $dc);
        }

        if ($mustBeStatus) {
            $queryBuilder->andWhere('p.currentStatus = :mustBeStatus')
                ->setParameter('mustBeStatus', $mustBeStatus);
        }

        if ($mustNotBeStatus) {
            $queryBuilder->andWhere('p.currentStatus IS NULL OR p.currentStatus <> :mustNotBeStatus')
                ->setParameter('mustNotBeStatus', $mustNotBeStatus);
        }
    }

    public function buildCellId($row) {
        return $row['id'];
    }

    public function buildCellOrderNumber($row) {
        return $row['orderNumber'];
    }

    public function buildCellGroup($row) {
        return $row['productGroupName'];
    }

    public function buildCellBarcode($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $html = $row['barcode'];

        $builder->addPopupButton('Edit Barcode', $mgr->getUrl('leep_admin', 'sub_order', 'edit_barcode', 'edit', array('id' => $row['id'])), 'button-edit');

        $html .= ' ' . $builder->getHtml();

        return $html;
    }

    public function buildCellCustomerName($row) {
        return $row['customerFirstName'] . ' ' . $row['customerSurName'];
    }

    public function buildCellDC($row) {
        return $row['distributionchannel'];
    }

    public function buildCellAge($row) {
        return Business\Customer\Util::getOrderAge($row['dateOrdered']);
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            $builder->addPopupButton('Update Status', $mgr->getUrl('leep_admin', 'sub_order', 'edit_status', 'edit', array('id' => $row['id'])), 'button-edit');
        }

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row['status']);
        
        $age = Business\Customer\Util::getOrderAge($row['statusDate']);        
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellInfo($row) {
        $html = '';
        $infoText = $row['orderInfoText'];
        if (!empty($infoText)) {
            $html .='<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        $laboratoryDetails = $row['laboratoryDetails'];
        if (!empty($laboratoryDetails)) {
            $html .='<a class="simpletip"><div class="button-activity"></div><div class="hide-content">'.nl2br($laboratoryDetails).'</div></a>';   
        }
        return $html;
    }


    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $row['idOrder'])), 'button-edit');
        }

        return $builder->getHtml();
    }
}