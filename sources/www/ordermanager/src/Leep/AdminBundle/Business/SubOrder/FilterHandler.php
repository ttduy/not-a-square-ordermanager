<?php
namespace Leep\AdminBundle\Business\SubOrder;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('orderNumber', 'text', array(
            'label'    => 'orderNumber',
            'required' => false
        ));

        $builder->add('barcode', 'text', array(
            'label'    => 'Barcode',
            'required' => false
        ));

        $builder->add('dc', 'choice', array(
            'label'    => 'DC',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        $builder->add('mustBeStatus', 'choice', array(
            'label'    => 'Must be status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status')
        ));

        $builder->add('mustNotBeStatus', 'choice', array(
            'label'    => 'Must not be status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status')
        ));

        return $builder;
    }
}