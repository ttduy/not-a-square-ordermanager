<?php
namespace Leep\AdminBundle\Business\Statistic;

class Constants {
    const DISTRIBUTION_CHANNEL  = 10;
    const PARTNER               = 20;
    const LEAD                  = 30;
    const CUSTOMER              = 40;

    public static function getMonthShorts() {
        return array(
            1  => 'Jan',
            2  => 'Feb',
            3  => 'Mar',
            4  => 'Apr',
            5  => 'May',
            6  => 'Jun',
            7  => 'Jul',
            8  => 'Aug',
            9  => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec',
        );
    }

    public static function getFactorTypes() {
        return array(
            Constants::DISTRIBUTION_CHANNEL    => 'Distribution Channel',
            Constants::PARTNER                 => 'Partner',
            Constants::LEAD                    => 'Lead',
            Constants::CUSTOMER                => 'Customer'
        );
    }
}