<?php
namespace Leep\AdminBundle\Business\Statistic;

use Leep\AdminBundle\Business\Base\AppFilterHandler;
use Leep\AdminBundle\Business;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));

        return $model;
    }

    public function buildForm($builder) {
        // filter by year
        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));

        // filter by type
        $builder->add('type', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => Business\Statistic\Constants::getFactorTypes()
        ));

        return $builder;
    }
}