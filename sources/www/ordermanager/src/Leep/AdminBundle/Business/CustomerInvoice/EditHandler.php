<?php
namespace Leep\AdminBundle\Business\CustomerInvoice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public $attachmentKey = false;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        
        $model = new EditModel();  
        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();

        $model->orderNumber = $entity->getOrderNumber();
        $model->orderInfoText = nl2br($entity->getOrderInfoText());
        $model->preferredPayment = $formatter->format($entity->getIdPreferredPayment(), 'mapping', 'LeepAdmin_DistributionChannel_PreferredPayment');
        $model->reportDelivery = $formatter->format($entity->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery');
        $model->invoiceNumber = $entity->getInvoiceNumber();        
        $model->invoiceDate = $entity->getInvoiceDate();
        $model->invoiceFromCompanyId = $entity->getInvoiceFromCompanyId();
        $model->isWithTax = $entity->getIsWithTax();
        $model->taxValue = $entity->getTaxValue();
        $model->invoiceStatus = $entity->getInvoiceStatus();
        $model->postage = $entity->getPostage();
        $model->priceOverride = $entity->getPriceOverride();
        $model->originalPrice = $entity->getRevenueEstimation();
        $model->marginOverrideDc = $entity->getMarginOverrideDc();
        $model->marginOverridePartner = $entity->getMarginOverridePartner();
        $model->invoiceInfo = $entity->getInvoiceInfo();
        $model->currencyRate = new Business\FormType\AppCurrencyRateModel();
        $model->currencyRate->idCurrency = $entity->getIdCurrency();
        $model->currencyRate->exchangeRate = $entity->getExchangeRate();

        $model->textAfterDescription = $entity->getTextAfterDescription();
        $model->textFooter = $entity->getTextFooter();
        
        $model->invoiceAddressIsUsed = $entity->getInvoiceAddressIsUsed();
        $model->invoiceAddressClientName = $entity->getInvoiceAddressClientName();
        $model->invoiceAddressCompanyName = $entity->getInvoiceAddressCompanyName();
        $model->invoiceAddressStreet = $entity->getInvoiceAddressStreet();
        $model->invoiceAddressPostCode = $entity->getInvoiceAddressPostCode();
        $model->invoiceAddressCity = $entity->getInvoiceAddressCity();
        $model->invoiceAddressIdCountry = $entity->getInvoiceAddressIdCountry();
        $model->invoiceAddressTelephone = $entity->getInvoiceAddressTelephone();
        $model->invoiceAddressFax = $entity->getInvoiceAddressFax();


        if ($model->invoiceNumber == null || trim($model->invoiceNumber) == '') {
            $model->invoiceNumber = 'R-'.$entity->getOrderNumber();
        }

        // Load status list
        $model->statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:InvoiceStatus', 'p')
            ->andWhere('p.customerid = :customerId')
            ->setParameter('customerId', $entity->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppInvoiceStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }

        // Price
        $price = Business\Customer\Util::computePrice($this->container, $entity->getId(), 'invoice_customer');
        $model->totalPrice = $formatter->format($price['total'], 'money');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'customers/'.$this->attachmentKey
        ));
        
        $builder->add('sectionCustomerOrder',          'section', array(
            'label' => 'Customer Order',
            'property_path' => false
        ));
        $builder->add('orderNumber',            'label', array(
            'label' => 'Order Number',
            'required'      => false
        ));
        $builder->add('orderInfoText',            'label', array(
            'label' => 'Order Info',
            'required'      => false
        ));
        $builder->add('preferredPayment',            'label', array(
            'label' => 'Preferred payment',
            'required'      => false
        ));
        $builder->add('reportDelivery',            'label', array(
            'label' => 'Report delivery',
            'required'      => false
        ));

        $builder->add('sectionInvoice',          'section', array(
            'label' => 'Invoice',
            'property_path' => false
        ));
        $builder->add('invoiceNumber',          'text', array(
            'label' => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate',            'datepicker', array(
            'label' => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId',   'choice', array(
            'label' => 'Invoice From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax',            'checkbox', array(
            'label' => 'With Tax?',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
        $builder->add('invoiceStatus',           'choice', array(
            'label' => 'Invoice Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CustomerInvoice_Status')
        ));
        $builder->add('postage',            'text', array(
            'label' => 'Postage',
            'required' => false
        ));
        $builder->add('totalPrice',   'label', array(
            'label' => 'Total Price',
            'required'  => false
        ));
        $builder->add('priceOverride',            'text', array(
            'label' => 'Price-override',
            'required' => false
        ));
        $builder->add('originalPrice',            'label', array(
            'label' => 'Original price',
            'required' => false
        ));
        $builder->add('marginOverrideDc', 'text', array(
            'label'    => 'Margin override DC (%)',
            'required' => false
        ));
        $builder->add('marginOverridePartner', 'text', array(
            'label'    => 'Margin override partner (%)',
            'required' => false
        ));
        $builder->add('invoiceInfo',            'textarea', array(
            'label' => 'Invoice Info',
            'required' => false
        ));
        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',            
            'required' => false
        ));

        $builder->add('sectionInvoiceAddress',          'section', array(
            'label' => 'Invoice address',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceAddressClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceAddressCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));        
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionExtraText',          'section', array(
            'label' => 'Extra Text',
            'property_path' => false
        ));
        $builder->add('textAfterDescription',      'textarea', array(
            'label' => 'After description',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));
        $builder->add('textFooter',      'textarea', array(
            'label' => 'Footer',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));

        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppInvoiceStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false,
                'statusMapping' => 'LeepAdmin_CustomerInvoice_Status'
            )
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        if ($this->entity->getInvoiceNumber() == '' && !empty($model->invoiceNumber)) {
            $helperActivity = $this->container->get('leep_admin.helper.activity');
            $notes = 'Invoice generated';
            $helperActivity->addCustomerActivity($this->entity->getId(), Business\Customer\Constant::ACTIVITY_TYPE_INVOICE_CREATED, $notes);
        }

        $this->entity->setInvoiceNumber($model->invoiceNumber);
        $this->entity->setInvoiceDate($model->invoiceDate);
        $this->entity->setInvoiceFromCompanyId($model->invoiceFromCompanyId);
        $this->entity->setIsWithTax($model->isWithTax);
        $this->entity->setTaxValue($model->taxValue);
        $this->entity->setInvoiceStatus($model->invoiceStatus);
        $this->entity->setPostage($model->postage);
        $this->entity->setPriceOverride($model->priceOverride);
        $this->entity->setMarginOverrideDc($model->marginOverrideDc);
        $this->entity->setMarginOverridePartner($model->marginOverridePartner);
        $this->entity->setInvoiceInfo($model->invoiceInfo);
        $this->entity->setIsInvoiceCustomer(1);
        if ($model->currencyRate) {
            $this->entity->setIdCurrency($model->currencyRate->idCurrency);
            $this->entity->setExchangeRate($model->currencyRate->exchangeRate);
        }

        $this->entity->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $this->entity->setInvoiceAddressClientName($model->invoiceAddressClientName);
        $this->entity->setInvoiceAddressCompanyName($model->invoiceAddressCompanyName);
        $this->entity->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $this->entity->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $this->entity->setInvoiceAddressCity($model->invoiceAddressCity);
        $this->entity->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $this->entity->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $this->entity->setInvoiceAddressFax($model->invoiceAddressFax);
        $this->entity->setPriceOverride($model->priceOverride);
        
        $this->entity->setTextAfterDescription($model->textAfterDescription);
        $this->entity->setTextFooter($model->textFooter);
        
        // Update status
        $em = $this->container->get('doctrine')->getEntityManager();

        $dbHelper = $this->container->get('leep_admin.helper.database');    
        $filters = array('customerid' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:InvoiceStatus', $filters);

        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\InvoiceStatus();
            $status->setCustomerId($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
            
            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }

        $this->entity->setInvoiceStatus($lastStatus);
        $this->entity->setInvoiceStatusDate($lastDate);

        $em->flush();

        parent::onSuccess();

        // Check if delete
        $isDeleted = $this->container->get('request')->get('isDelete', 0);
        if ($isDeleted) {
            $this->entity->setInvoiceNumber('');
            $this->entity->setInvoiceDate(NULL);
            $this->entity->setInvoiceFromCompanyId(0);
            $this->entity->setIsWithTax(false);
            $this->entity->setTaxValue(0);
            $this->entity->setInvoiceStatus(0);
            $this->entity->setInvoiceStatusDate(NULL);
            $this->entity->setPostage(0);
            $this->entity->setPriceOverride('');
            $this->entity->setInvoiceInfo('');
            $this->entity->setIsInvoiceCustomer(0);
            $this->entity->setIdCurrency(0);
            $this->entity->setExchangeRate(0);
            $this->entity->setTextAfterDescription('');
            $this->entity->setTextFooter('');
            
            $dbHelper = $this->container->get('leep_admin.helper.database');    
            $filters = array('customerid' => $this->entity->getId());
            $dbHelper->delete($em, 'AppDatabaseMainBundle:InvoiceStatus', $filters);
            $em->flush();
            
            $this->reloadForm();
        }        
    }
}
