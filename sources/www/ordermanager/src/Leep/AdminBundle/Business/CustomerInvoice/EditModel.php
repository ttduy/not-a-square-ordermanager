<?php
namespace Leep\AdminBundle\Business\CustomerInvoice;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $attachmentKey;
    public $attachment;

    public $orderNumber;
    public $orderInfoText;
    public $preferredPayment;
    public $reportDelivery;

    public $invoiceAddressIsUsed;
    public $invoiceAddressCompanyName;
    public $invoiceAddressClientName;
    public $invoiceAddressStreet;
    public $invoiceAddressPostCode;
    public $invoiceAddressCity;
    public $invoiceAddressIdCountry;
    public $invoiceAddressTelephone;
    public $invoiceAddressFax;
    
    public $invoiceNumber;
    public $invoiceDate;
    public $invoiceFromCompanyId;
    public $isWithTax;
    public $taxValue;
    public $invoiceStatus;
    public $postage;
    public $totalPrice;
    public $priceOverride;
    public $originalPrice;
    public $marginOverrideDc;
    public $marginOverridePartner;
    public $invoiceInfo;
    public $currencyRate;

    public $statusList;

    public $textAfterDescription;
    public $textFooter;
}
