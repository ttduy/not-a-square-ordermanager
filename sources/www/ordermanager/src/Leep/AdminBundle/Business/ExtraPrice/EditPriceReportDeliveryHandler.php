<?php
namespace Leep\AdminBundle\Business\ExtraPrice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditPriceReportDeliveryHandler extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    private function getCountries() {
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Countries')->findAll();
    }

    public function convertToFormModel($entity) { 
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = array();
        $results = $em->getRepository('AppDatabaseMainBundle:ExtraPriceReportDelivery')->findAll();
        foreach ($results as $r) {
            $model['price_'.$r->getIdCountry()] = $r->getPrice();
            $model['nutri_me_price_'.$r->getIdCountry()] = $r->getNutriMePrice();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        // Extra charge
        $countries = $this->getCountries();

        foreach ($countries as $country) {
            $builder->add('price_'.$country->getId(), 'text', array(
                'label' => $country->getCountry(),
                'required' => false,
                'attr' => array(
                    'style' => 'min-width: 90px; max-width: 90px'
                )
            ));            
            $builder->add('nutri_me_price_'.$country->getId(), 'text', array(
                'label' => $country->getCountry(),
                'required' => false,
                'attr' => array(
                    'style' => 'min-width: 90px; max-width: 90px'
                )
            ));            
        }
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ExtraPriceReportDelivery', 'p')
            ->getQuery()->execute();

        $countries = $this->getCountries();
        foreach ($countries as $country) {
            $idCountry = intval($country->getId());
            $price = isset($model['price_'.$idCountry]) ? floatval($model['price_'.$idCountry]) : 0;
            $nutrimePrice = isset($model['nutri_me_price_'.$idCountry]) ? floatval($model['nutri_me_price_'.$idCountry]) : 0;

            $extraPrice = new Entity\ExtraPriceReportDelivery();
            $extraPrice->setIdCountry($idCountry);
            $extraPrice->setPrice($price);
            $extraPrice->setNutriMePrice($nutrimePrice);
            $em->persist($extraPrice);
        }
        $em->flush();

        $this->messages[] = "The records has been updated successfully";
    }
}