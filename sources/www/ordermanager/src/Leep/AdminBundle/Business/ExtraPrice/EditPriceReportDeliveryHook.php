<?php
namespace Leep\AdminBundle\Business\ExtraPrice;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class EditPriceReportDeliveryHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $countries = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Countries')->findAll();
        $map = array();
        foreach ($countries as $country) {
            $map[$country->getId()] = $country->getCountry();
        }

        $data['countries'] = $map;
    }
}
