<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

use Easy\ModuleBundle\Module\AbstractHook;

class BulkEditStatusHook extends AbstractHook {
    public $todayTs;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        $selectedInvoices = $controller->get('request')->get('selected', '');
        $data['selectedInvoices'] = $selectedInvoices;
        $selectedInvoices = explode(',', $selectedInvoices);

        $invoices = array();
        foreach ($selectedInvoices as $selectedId) {
            if (trim($selectedId) != '') {
                list($id, $type) = explode('_', $selectedId);

                $invoice = $em->getRepository('AppDatabaseMainBundle:IntegratedInvoice')->findOneBy(array(
                    'id' => $id,
                    'invoiceType' => $type
                ));
                if ($invoice) {
                    $invoices[] = array(
                        'date' => $formatter->format($invoice->getInvoiceDate(), 'date'),
                        'type' => $formatter->format($invoice->getInvoiceType(), 'mapping', 'LeepAdmin_InvoiceType'),
                        'name' => $invoice->getInvoiceName(),
                        'number' => $invoice->getInvoiceNumber(),
                        'status' => $formatter->format($invoice->getInvoiceStatus(), 'mapping', 'LeepAdmin_CustomerInvoice_Status'),
                        'age'  => $this->computeAge($invoice->getStatusDate()),
                        'amount' => $formatter->format($invoice->getInvoiceAmount(), 'money')
                    );
                }
            }
        }
        $data['invoices'] = $invoices;
    }

    private function computeAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }
}
