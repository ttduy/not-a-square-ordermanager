<?php 
namespace Leep\AdminBundle\Business\IntegratedInvoice;

class Constant {
    const INVOICE_TYPE_COLLECTIVE_INVOICE  = 1;
    const INVOICE_TYPE_CUSTOMER_INVOICE    = 2;
    const INVOICE_TYPE_MWA_INVOICE         = 3;


    public static function getInvoiceTypes() {
        return array(
            self::INVOICE_TYPE_COLLECTIVE_INVOICE         => 'Collective invoice',
            self::INVOICE_TYPE_CUSTOMER_INVOICE           => 'Customer invoice',
            self::INVOICE_TYPE_MWA_INVOICE                => 'MWA invoice',
        );
    }
}