<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business\Invoice\Constant as InvoiceConstant;

class StatisticHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();

        $stats = array();
        $query = $em->createQueryBuilder();
        $query->select('p.invoiceStatus, COUNT(p) as totalInvoice, SUM(p.invoiceAmount) as totalAmount')
            ->from('AppDatabaseMainBundle:IntegratedInvoice', 'p')
            ->groupBy('p.invoiceStatus');
        $result = $query->getQuery()->getResult();        
        foreach ($result as $r) {
            $stats[$r['invoiceStatus']] = array('totalInvoice' => $r['totalInvoice'], 'totalAmount' => $r['totalAmount']);
        }

        $mappingArr = array(
            InvoiceConstant::INVOICE_STATUS_WAIT_WITH_INVOICE   => 'Wait with invoice',
            InvoiceConstant::INVOICE_STATUS_INVOICE_SENT        => 'Invoice sent',
            InvoiceConstant::INVOICE_STATUS_PAID                => 'Paid',
            InvoiceConstant::INVOICE_STATUS_NOT_IN_BOOKEEPING   => 'Not in bookeeping',
            InvoiceConstant::INVOICE_STATUS_CANCELLED           => 'Invoice cancelled',
            InvoiceConstant::INVOICE_STATUS_FREE_ANALYSIS       => 'Free analysis'
        );
        $statistic = array();

        foreach ($mappingArr as $k => $mK) {
            if (isset($stats[$k])) {
                $statistic[$mK] = array(
                    'totalInvoice' => $stats[$k]['totalInvoice'],
                    'totalAmount' => number_format($stats[$k]['totalAmount'], 2)
                );
            }
            else {
                $statistic[$mK] = array('totalInvoice' => 0, 'totalAmount' => '0.00');
            }
        }

        $paidTotalInvoice = 0;
        $paidTotalAmount = 0;
        $paidStatus = array(
            InvoiceConstant::INVOICE_STATUS_PAID,
            InvoiceConstant::INVOICE_STATUS_PAID_WITH_CREDIT_CARD,
            InvoiceConstant::INVOICE_STATUS_PAID_BY_BANK_TRANSFER,
            InvoiceConstant::INVOICE_STATUS_PAID_BY_PAYPAL,
        );
        foreach ($paidStatus as $i) {
            if (isset($stats[$i])) {
                $paidTotalInvoice += $stats[$i]['totalInvoice'];
                $paidTotalAmount += $stats[$i]['totalAmount'];
            }
        }

        $statistic['Paid'] = array('totalInvoice' => $paidTotalInvoice, 'totalAmount' => number_format($paidTotalAmount, 2));

        $data['statistic'] = $statistic;
        $data['statusList'] = $mappingArr;
    }
}
