<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }
    public function getColumnMapping() {
        return array('invoiceDate', 'invoiceType', 'invoiceName', 'invoiceNumber', 'invoiceAmount', 'invoiceStatus', 'reminderCount', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Date',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Type',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Name',             'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Number',           'width' => '15%', 'sortable' => 'true'),            
            array('title' => 'Amount',           'width' => '10%', 'sortable' => 'true'),    
            array('title' => 'Status',           'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Reminder count',   'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Action',           'width' => '10%', 'sortable' => 'false')            
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.invoiceDate';
        $this->columnSortMapping[] = 'p.invoiceType';
        $this->columnSortMapping[] = 'p.invoiceName';
        $this->columnSortMapping[] = 'p.invoiceNumber';
        $this->columnSortMapping[] = 'p.invoiceAmount';
        $this->columnSortMapping[] = 'p.invoiceStatus';

        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        //$queryBuilder->orderBy('p.invoiceDate', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_IntegratedInvoiceReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:IntegratedInvoice', 'p');
        
        if ($this->filters->invoiceType != 0) {
            $queryBuilder->andWhere('p.invoiceType = :invoiceType')
                ->setParameter('invoiceType', $this->filters->invoiceType);
        }
        if (trim($this->filters->invoiceName) != '') {
            $queryBuilder->andWhere('p.invoiceName LIKE :invoiceName')
                ->setParameter('invoiceName', '%'.trim($this->filters->invoiceName).'%');
        }
        if (trim($this->filters->invoiceNumber) != '') {
            $queryBuilder->andWhere('p.invoiceNumber LIKE :invoiceNumber')
                ->setParameter('invoiceNumber', '%'.trim($this->filters->invoiceNumber).'%');
        }
        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.invoiceDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }        
        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.invoiceDate <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }        
        if (!empty($this->filters->invoiceStatus)) {
            $queryBuilder->andWhere('p.invoiceStatus in (:invoiceStatus)')
                ->setParameter('invoiceStatus', $this->filters->invoiceStatus);
        }
    }

    public function buildCellInvoiceName($row) {
        return $row->getInvoiceName()."<input type='hidden' class='hiddenId' value='".$row->getId().'_'.$row->getInvoiceType()."'/>";
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $type = '';
        switch ($row->getInvoiceType()) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_pdf',  'pdf',  array('id' => $row->getId())), 'button-pdf');
                $type = 'collective_invoice';
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'customer_invoice', 'customer_form_pdf',  'pdf',  array('id' => $row->getId())), 'button-pdf');
                $type = 'customer_invoice';
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'mwa_invoice', 'invoice_form_edit',  'pdf',  array('id' => $row->getId())), 'button-pdf');
                $type = 'mwa_invoice';
                break;
        }
        
        // Generate reminder notice
        $builder->addNewTabButton('ReminderNotice', $mgr->getUrl('leep_admin', 'invoice', 'feature', 'generateReminderNotice', array('id' => $row->getId(), 'type' => $type)), 'button-activity');

        return $builder->getHtml();
    }

    public function buildCellReminderCount($row) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        switch ($row->getInvoiceType()) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $query->select('COUNT(p)')
                    ->from('AppDatabaseMainBundle:CollectiveInvoiceStatus', 'p')
                    ->andWhere('p.collectiveinvoiceid = '.$row->getId())
                    ->andWhere('p.status = '.Business\Invoice\Constant::INVOICE_STATUS_REMINDERS_SENT);
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $query->select('COUNT(p)')
                    ->from('AppDatabaseMainBundle:InvoiceStatus', 'p')
                    ->andWhere('p.customerid = '.$row->getId())
                    ->andWhere('p.status = '.Business\Invoice\Constant::INVOICE_STATUS_REMINDERS_SENT);
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $query->select('COUNT(p)')
                    ->from('AppDatabaseMainBundle:MwaInvoiceStatus', 'p')
                    ->andWhere('p.idMwaInvoice = '.$row->getId())
                    ->andWhere('p.status = '.Business\Invoice\Constant::INVOICE_STATUS_REMINDERS_SENT);
                break;
        }
        $count = intval($query->getQuery()->getSingleScalarResult());
        return $count;
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellInvoiceAmount($row) {
        return "&euro;".number_format(floatval($row->getInvoiceAmount()), 2);
    }

    public function buildCellInvoiceStatus($row) {        
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_CustomerInvoice_Status', $row->getInvoiceStatus());        
        $age = $this->getAge($row->getStatusDate()); 

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();     

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('integratedInvoiceModify')) {
            $builder->addButton('Edit status', $mgr->getUrl('leep_admin', 'integrated_invoice', 'edit_status', 'edit', array('id' => $row->getId().'_'.$row->getInvoiceType())), 'button-edit');
        }

        return $builder->getHtml().'&nbsp;'.(($age === '') ? '': "(${age}d) ").$status;
    }
}
