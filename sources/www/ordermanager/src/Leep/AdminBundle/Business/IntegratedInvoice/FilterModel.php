<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

class FilterModel {
    public $invoiceType;
    public $invoiceName;
    public $invoiceNumber;
    public $startDate;
    public $endDate;
    public $invoiceStatus;
}