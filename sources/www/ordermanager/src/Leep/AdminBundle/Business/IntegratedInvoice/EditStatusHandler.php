<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditStatusHandler extends AppEditHandler {   
    public $id;
    public $invoiceType;

    public function loadEntity($request) {
        $arr = explode('_', $request->query->get('id'));
        $this->id = intval($arr[0]);        
        $this->invoiceType = intval($arr[1]);

        return null;
    }

    public function convertToFormModel($entity) {
        $statusList = array();

        $model = array();

        // Load invoice info
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        switch ($this->invoiceType) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $collectiveInvoice = $doctrine->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($this->id);
                $model['type'] = 'Collective Invoice';
                $model['invoiceName'] = $collectiveInvoice->getName();
                $model['invoiceNumber'] = $collectiveInvoice->getInvoiceNumber();
                $model['invoiceDate'] = $formatter->format($collectiveInvoice->getInvoiceDate(), 'date');
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($this->id);
                $model['type'] = 'Customer Invoice';
                $model['invoiceName'] = $customer->getInvoiceNumber();
                $model['invoiceNumber'] = $customer->getInvoiceNumber();
                $model['invoiceDate'] = $formatter->format($customer->getInvoiceDate(), 'date');
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $mwaInvoice = $doctrine->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($this->id);
                $model['type'] = 'MWA Invoice';
                $model['invoiceName'] = $mwaInvoice->getName();
                $model['invoiceNumber'] = $mwaInvoice->getInvoiceNumber();
                $model['invoiceDate'] = $formatter->format($mwaInvoice->getInvoiceDate(), 'date');
                break;
        }   

        // Load status list
        $query = $em->createQueryBuilder();
        $query->select('p');
        switch ($this->invoiceType) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $query->from('AppDatabaseMainBundle:CollectiveInvoiceStatus', 'p')
                    ->andWhere('p.collectiveinvoiceid = :id')
                    ->orderBy('p.sortorder', 'ASC');
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $query->from('AppDatabaseMainBundle:InvoiceStatus', 'p')
                    ->andWhere('p.customerid = :id')
                    ->orderBy('p.sortorder', 'ASC');
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $query->from('AppDatabaseMainBundle:MwaInvoiceStatus', 'p')
                    ->andWhere('p.idMwaInvoice = :id')
                    ->orderBy('p.sortOrder', 'ASC');
                break;
        }
        $query->setParameter('id', $this->id);
            

        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppInvoiceStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $statusList[] = $m;
        }

        $model['statusList'] = $statusList;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('section1',          'section', array(
            'label' => 'Invoice',
            'property_path' => false
        ));
        $builder->add('type',     'label', array(
            'label' => 'Type',
            'required' => false
        ));
        $builder->add('invoiceName',     'label', array(
            'label' => 'Invoice Name',
            'required' => false
        ));
        $builder->add('invoiceNumber',   'label', array(
            'label' => 'Invoice Number',
            'required' => false
        ));
        $builder->add('invoiceDate',     'label', array(
            'label' => 'Invoice Date',
            'required' => false
        ));
        

        $builder->add('section2',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new Business\FormType\AppInvoiceStatusRow($this->container),
            'options'  => array(
                'statusMapping' => ($this->invoiceType == Constant::INVOICE_TYPE_CUSTOMER_INVOICE) ? 'LeepAdmin_CustomerInvoice_Status' : 'LeepAdmin_Invoice_Status',                
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        // Delete old status
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        switch ($this->invoiceType) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $filters = array('collectiveinvoiceid' => $this->id);
                $dbHelper->delete($em, 'AppDatabaseMainBundle:CollectiveInvoiceStatus', $filters);
                $collectiveInvoice = $doctrine->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($this->id);
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $filters = array('customerid' => $this->id);
                $dbHelper->delete($em, 'AppDatabaseMainBundle:InvoiceStatus', $filters);
                $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($this->id);
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $filters = array('idMwaInvoice' => $this->id);
                $dbHelper->delete($em, 'AppDatabaseMainBundle:MwaInvoiceStatus', $filters);
                $mwaInvoice = $doctrine->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($this->id);
                break;
        }        

        // Save status list
        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model['statusList'] as $statusRow) {
            if (empty($statusRow)) continue;
            switch ($this->invoiceType) {
                case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                    $status = new Entity\CollectiveInvoiceStatus();
                    $status->setCollectiveInvoiceId($collectiveInvoice->getId());
                    break;
                case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                    $status = new Entity\InvoiceStatus();
                    $status->setCustomerId($customer->getId());
                    break;
                case Constant::INVOICE_TYPE_MWA_INVOICE:
                    $status = new Entity\MwaInvoiceStatus();
                    $status->setIdMwaInvoice($mwaInvoice->getId());
                    break;
            } 
            
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
                
            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }

        // Save new status
        switch ($this->invoiceType) {
            case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                $collectiveInvoice->setInvoiceStatus($lastStatus);
                $collectiveInvoice->setStatusDate($lastDate);
                $em->persist($collectiveInvoice);                
                break;
            case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                $customer->setInvoiceStatus($lastStatus);
                $customer->setInvoiceStatusDate($lastDate);
                $em->persist($customer);                
                break;
            case Constant::INVOICE_TYPE_MWA_INVOICE:
                $mwaInvoice->setStatus($lastStatus);
                $mwaInvoice->setStatusDate($lastDate);
                $em->persist($mwaInvoice);                
                break;
        }      

        $em->flush();        

        $this->messages[] = 'The status has been updated successfully';
    }
}
