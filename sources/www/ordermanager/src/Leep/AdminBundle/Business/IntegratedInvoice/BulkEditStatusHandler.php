<?php
namespace Leep\AdminBundle\Business\IntegratedInvoice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditStatusHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) { 
        $model = new BulkEditStatusModel(); 
        $model->selectedInvoices = $this->container->get('request')->get('selected', '');
        $model->statusDate = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedInvoices', 'hidden');
        $builder->add('statusDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false
        ));
        $builder->add('status',     'choice', array(
            'label'  => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CustomerInvoice_Status'),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 2
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $selectedInvoices = explode(',', $model->selectedInvoices);
        foreach ($selectedInvoices as $selectedInvoice) {
            if (trim($selectedInvoice) != '') {
                list($id, $type) = explode('_', $selectedInvoice);

                // Find max status
                $query = $em->createQueryBuilder();
                switch ($type) {
                    case Constant::INVOICE_TYPE_COLLECTIVE_INVOICE:
                        $query->select('MAX(p.sortorder)')
                            ->from('AppDatabaseMainBundle:CollectiveInvoiceStatus', 'p')
                            ->andWhere('p.collectiveinvoiceid = :id')
                            ->setParameter('id', $id);
                        
                        $newStatus = new Entity\CollectiveInvoiceStatus();
                        $newStatus->setCollectiveInvoiceId($id);

                        $collectiveInvoice = $em->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($id);
                        $collectiveInvoice->setInvoiceStatus($model->status);
                        $collectiveInvoice->setStatusDate($model->statusDate);
                        $em->persist($collectiveInvoice);
                        $em->flush();

                        break;
                    case Constant::INVOICE_TYPE_CUSTOMER_INVOICE:
                        $query->select('MAX(p.sortorder)')
                            ->from('AppDatabaseMainBundle:InvoiceStatus', 'p')
                            ->andWhere('p.customerid = :id')
                            ->setParameter('id', $id);

                        $newStatus = new Entity\InvoiceStatus();
                        $newStatus->setCustomerId($id);

                        $customerInvoice = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
                        $customerInvoice->setInvoiceStatus($model->status);
                        $customerInvoice->setInvoiceStatusDate($model->statusDate);
                        $em->persist($customerInvoice);
                        $em->flush();

                        break;
                    case Constant::INVOICE_TYPE_MWA_INVOICE:
                        $query->select('MAX(p.sortOrder)')
                            ->from('AppDatabaseMainBundle:MwaInvoiceStatus', 'p')
                            ->andWhere('p.idMwaInvoice = :id')
                            ->setParameter('id', $id);

                        $newStatus = new Entity\MwaInvoiceStatus();
                        $newStatus->setIdMwaInvoice($id);

                        $mwaInvoice = $em->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($id);
                        $mwaInvoice->setStatus($model->status);
                        $mwaInvoice->setStatusDate($model->statusDate);
                        $em->persist($mwaInvoice);
                        $em->flush();

                        break;
                }
                $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;

                // New status
                $newStatus->setStatus($model->status);
                $newStatus->setStatusDate($model->statusDate);
                $newStatus->setSortOrder($sortOrder);
                $newStatus->setNotes($model->notes);
                $em->persist($newStatus);

            }
        }
        $em->flush();

        $this->messages[] = 'Status has been changed successfully';
    }
}