<?php
namespace Leep\AdminBundle\Business\CmsProduct;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('code', 'name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Code', 'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Name', 'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CmsProduct', 'p');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.name', 'ASC');        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cmsOrderModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cms_product', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cms_product', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
