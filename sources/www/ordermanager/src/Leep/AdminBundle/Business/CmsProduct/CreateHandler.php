<?php
namespace Leep\AdminBundle\Business\CmsProduct;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $product = new Entity\CmsProduct();
        $product->setCode($model->code);
        $product->setName($model->name);

        $em->persist($product);
        $em->flush();

        parent::onSuccess();
    }
}
