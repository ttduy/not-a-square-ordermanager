<?php
namespace Leep\AdminBundle\Business\CmsProduct;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CmsProduct', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->code = $entity->getCode();
        $model->name = $entity->getName();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setCode($model->code);
        
        parent::onSuccess();
    }
}
