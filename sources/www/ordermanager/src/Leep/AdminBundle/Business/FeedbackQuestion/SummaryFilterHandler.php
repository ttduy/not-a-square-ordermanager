<?php
namespace Leep\AdminBundle\Business\FeedbackQuestion;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class SummaryFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new SummaryFilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('dateFrom1', 'datepicker', array(
            'label'    => 'Period 1 - Date From',
            'required' => false
        ));

        $builder->add('dateFrom2', 'datepicker', array(
            'label'    => 'Period 2 - Date From',
            'required' => false
        ));
        $builder->add('dateTo1', 'datepicker', array(
            'label'    => 'Period 1 - Date To',
            'required' => false
        ));
        $builder->add('dateTo2', 'datepicker', array(
            'label'    => 'Period 2 - Date To',
            'required' => false
        ));
        return $builder;
    }
}