<?php 
namespace Leep\AdminBundle\Business\FeedbackQuestion;

class Constant {
    const FEEDBACK_QUESTION_TYPE_TEXT                = 1;
    const FEEDBACK_QUESTION_TYPE_CHOICE              = 2;
    const FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE     = 3;

    public static function getFeedbackQuestionTypes() {
        return array(
            self::FEEDBACK_QUESTION_TYPE_TEXT       => 'Text',
            self::FEEDBACK_QUESTION_TYPE_CHOICE     => 'Single choice',
            self::FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE   => 'Multiple choice'
        );
    }
}