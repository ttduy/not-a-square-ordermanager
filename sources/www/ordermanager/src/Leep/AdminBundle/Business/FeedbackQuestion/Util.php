<?php
namespace Leep\AdminBundle\Business\FeedbackQuestion;
         
class Util {
    public static function parseChoices($data) {
        $choices = array();

        $rows = explode("\n", $data);
        foreach ($rows as $r) {
            $a = explode('|', $r);
            $key = $a[0];
            $value = isset($a[1]) ? $a[1] : $a[0];

            $choices[trim($key)] = trim($value);
        }

        return $choices;
    }

    public static function loadFeedbackQuestions($container) {
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findAll();
        $questions = array();
        foreach ($result as $r) {
            $questions[] = $r->getQuestion();
        }
        return $questions;
    }

    public static function updateStats($container) {
        $em = $container->get('doctrine')->getEntityManager();
        
        $numResponses = array();
        $query = $em->createQueryBuilder();
        $query->select('p.idFeedbackQuestion, COUNT(p) total')
            ->from('AppDatabaseMainBundle:FeedbackResponse', 'p')
            ->groupBy('p.idFeedbackQuestion');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $numResponses[$r['idFeedbackQuestion']] = intval($r['total']);
        }

        $questions = $em->getRepository('AppDatabaseMainBundle:FeedbackQuestion')->findAll();
        foreach ($questions as $question) {
            $idFeedbackQuestion = $question->getId();
            $question->setNumResponses(isset($numResponses[$idFeedbackQuestion]) ? $numResponses[$idFeedbackQuestion] : 0);

            $stats = array();
            switch ($question->getIdType()) {
                case Constant::FEEDBACK_QUESTION_TYPE_CHOICE:
                    $responses = $em->getRepository('AppDatabaseMainBundle:FeedbackResponse')->findByIdFeedbackQuestion($idFeedbackQuestion);
                    foreach ($responses as $response) {
                        $answer = json_decode($response->getAnswer());
                        $answer = intval($answer);
                        if (!isset($stats[$answer])) {
                            $stats[$answer] = 0;
                        }
                        $stats[$answer]++;
                    }
                    break;
                
                case Constant::FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE:
                    $responses = $em->getRepository('AppDatabaseMainBundle:FeedbackResponse')->findByIdFeedbackQuestion($idFeedbackQuestion);
                    foreach ($responses as $response) {
                        $answers = json_decode($response->getAnswer(), true);
                        foreach ($answers as $answer) {
                            if (!isset($stats[$answer])) {
                                $stats[$answer] = 0;
                            }
                            $stats[$answer]++;
                        }
                    }
                    break;
            }
            $question->setSummary(json_encode($stats));
        }
        $em->flush();
    }

    public static function computeStats($container, $idFeedbackQuestion, $choices, $dateFrom, $dateTo, $isOverall = false) {
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FeedbackResponse', 'p')
            ->andWhere('p.idFeedbackQuestion = :idFeedbackQuestion')
            ->setParameter('idFeedbackQuestion', $idFeedbackQuestion);
        if (!$isOverall) {
            if (!empty($dateFrom) && !empty($dateTo)) {
                $query->andWhere('p.responseTime >= :dateFrom')
                    ->andWhere('p.responseTime <= :dateTo')
                    ->setParameter('dateFrom', $dateFrom)
                    ->setParameter('dateTo', $dateTo);
            }
            else {
                return false;
            }
        }
        $responses = $query->getQuery()->getResult();

        $stats = array();
        foreach ($choices as $k => $v) {
            $stats[$k] = array('total' => 0, 'percentage' => 0);
        }

        $totalScore = 0;
        $numResponses = 0;
        $numScore = 0;
        foreach ($responses as $response) {
            $answer = json_decode($response->getAnswer());
            $answer = intval($answer);

            if (isset($stats[$answer])) {
                $stats[$answer]['total']++;
                $numResponses++;
                if ($answer != 99) {
                    $totalScore+=$answer;
                    $numScore++;
                }
            }            
        }

        foreach ($stats as $k => $v) {
            $stats[$k]['percentage'] = ($numResponses == 0) ? 0 : number_format($v['total'] * 100 / $numResponses, 0);
        }

        $averageScore = ($numScore == 0) ? 0 : ($totalScore / $numScore);
        $roundedScore = round($averageScore);
        $averageValue = isset($choices[$roundedScore]) ? $choices[$roundedScore] : 'Unknown';

        return array('stats' => $stats, 'averageScore' => $averageScore, 'averageValue' => $averageValue);
    }
}