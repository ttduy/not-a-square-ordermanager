<?php
namespace Leep\AdminBundle\Business\EmailWebLoginBlacklist;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return new CreateModel();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $optOut = new Entity\EmailWebloginOptOutList();
        $optOut->setEmail($model->email);
        $optOut->setCreatedDate(new \DateTime());
        $optOut->setCreatedBy($currentUserId);
        $em->persist($optOut);
        $em->flush();

        parent::onSuccess();
    }
}
