<?php
namespace Leep\AdminBundle\Business\EmailWebLoginBlacklist;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'email', 'createdDate', 'createdBy', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Id', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Email', 'width' => '35%', 'sortable' => 'true'),
            array('title' => 'Date', 'width' => '15%', 'sortable' => 'true'),
            array('title' => 'By', 'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmailWebLoginBlacklistGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailWebloginOptOutList', 'p');

        if (trim($this->filters->email) != '') {
            $queryBuilder->andWhere('p.email LIKE :email')
                ->setParameter('email', '%'.trim($this->filters->email).'%');
        }
    }

    public function buildCellCreatedDate($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        return $formatter->format($row->getCreatedDate(), 'date');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('emailWebLoginBlacklistModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'email_weblogin_blacklist', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'email_weblogin_blacklist', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
