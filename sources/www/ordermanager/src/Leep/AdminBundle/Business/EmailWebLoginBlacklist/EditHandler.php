<?php
namespace Leep\AdminBundle\Business\EmailWebLoginBlacklist;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailWebloginOptOutList', 'app_main')
            ->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->email = $entity->getEmail();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('email', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $model = $this->getForm()->getData();

        $this->entity->setEmail($model->email);
        $this->entity->setCreatedDate(new \DateTime());
        $this->entity->setCreatedBy($currentUserId);
        parent::onSuccess();
    }
}
