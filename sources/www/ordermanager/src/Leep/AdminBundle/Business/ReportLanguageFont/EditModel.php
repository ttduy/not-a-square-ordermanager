<?php
namespace Leep\AdminBundle\Business\ReportLanguageFont;

class EditModel {
    public $language;
    public $idRegular;
    public $idRegularItalic;
    public $idBold;
    public $idLight;
    public $idLightItalic;
    public $idMedium;
    public $idThin;
    public $idSpecial;
}