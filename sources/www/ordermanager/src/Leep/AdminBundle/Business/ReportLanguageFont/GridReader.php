<?php
namespace Leep\AdminBundle\Business\ReportLanguageFont;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'font', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',      'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Font',      'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',    'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.name, 
                l.idRegular,
                l.idRegularItalic,
                l.idBold,
                l.idLight,
                l.idLightItalic,
                l.idMedium,
                l.idThin,
                l.idSpecial
            ')
            ->from('AppDatabaseMainBundle:Language', 'p')
            ->leftJoin('AppDatabaseMainBundle:ReportLanguageFont', 'l', 'WITH', 'p.id = l.idLanguage');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');
    }

    public function buildCellFont($row) {        
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $fonts = array(
            array(
                'name' => 'Regular', 
                'field' => 'idRegular'
            ),
            array(
                'name' => 'Regular italic', 
                'field' => 'idRegularItalic'
            ),
            array(
                'name' => 'Bold', 
                'field' => 'idBold'
            ),
            array(
                'name' => 'Light', 
                'field' => 'idLight'
            ),
            array(
                'name' => 'Light italic', 
                'field' => 'idLightItalic'
            ),
            array(
                'name' => 'Medium', 
                'field' => 'idMedium'
            ),
            array(
                'name' => 'Thin', 
                'field' => 'idThin'
            ),
            array(
                'name' => 'Special', 
                'field' => 'idSpecial'
            ),            
        );
        
        $rowFormat =   "
            <div>
                <div style='float:left; width: 150px'>%s</div>
                <div style='float:left;'>: %s</div>
                <div style='clear:both'></div>
            </div>";
        $html = '';
        foreach ($fonts as $f) {
            $value = $formatter->format($row[$f['field']], 'mapping', 'LeepAdmin_ApplicationFont_List');
            if (empty($value)) {
                $value = '--';
            }
            $html .= sprintf($rowFormat, $f['name'], $value);
        }

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('reportModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'report_language_font', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
        }

        return $builder->getHtml();
    }
}