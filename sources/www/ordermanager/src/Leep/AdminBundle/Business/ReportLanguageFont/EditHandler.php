<?php
namespace Leep\AdminBundle\Business\ReportLanguageFont;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->language = $entity->getName();
        $languageFont = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLanguageFont')->findOneByIdLanguage($entity->getId());
        if ($languageFont) {
            $model->idRegular = $languageFont->getIdRegular();
            $model->idRegularItalic = $languageFont->getIdRegularItalic();
            $model->idBold = $languageFont->getIdBold();
            $model->idLight = $languageFont->getIdLight();
            $model->idLightItalic = $languageFont->getIdLightItalic();
            $model->idMedium = $languageFont->getIdMedium();
            $model->idThin = $languageFont->getIdThin();
            $model->idSpecial = $languageFont->getIdSpecial();
        }
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('language', 'label', array(
            'label' => 'Language',
            'required' => false
        ));
        
        $builder->add('idRegular', 'choice', array(
            'label' => 'Regular',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));        
        $builder->add('idRegularItalic', 'choice', array(
            'label' => 'Regular italic',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idBold', 'choice', array(
            'label' => 'Bold',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idLight', 'choice', array(
            'label' => 'Light',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idLightItalic', 'choice', array(
            'label' => 'Light italic',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idMedium', 'choice', array(
            'label' => 'Medium',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idThin', 'choice', array(
            'label' => 'Thin',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));
        $builder->add('idSpecial', 'choice', array(
            'label' => 'Special',
            'choices' => $mapping->getMapping('LeepAdmin_ApplicationFont_List'),
            'required' => false
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $languageFont = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLanguageFont')->findOneByIdLanguage($this->entity->getId());
        if (empty($languageFont)) {
            $languageFont = new Entity\ReportLanguageFont();
            $languageFont->setIdLanguage($this->entity->getId());
            $em->persist($languageFont);
            $em->flush();
        }

        $languageFont->setIdRegular($model->idRegular);
        $languageFont->setIdRegularItalic($model->idRegularItalic);
        $languageFont->setIdBold($model->idBold);
        $languageFont->setIdLight($model->idLight);
        $languageFont->setIdLightItalic($model->idLightItalic);
        $languageFont->setIdMedium($model->idMedium);
        $languageFont->setIdThin($model->idThin);
        $languageFont->setIdSpecial($model->idSpecial);
        $em->persist($languageFont);
        $em->flush();

        parent::onSuccess();
    }
}