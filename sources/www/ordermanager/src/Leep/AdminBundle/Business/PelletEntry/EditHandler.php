<?php
namespace Leep\AdminBundle\Business\PelletEntry;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PelletEntry', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();  
        
        $model->cups = $entity->getCups();
        $model->section = $entity->getSection();
        $model->note = $entity->getNote();

        $arrWeights = array();
        $result = $em->getRepository('AppDatabaseMainBundle:PelletEntryWeight')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\Pellet\FormType\AppPelletEntryWeightModel();
                $weight->txt = $entry->getTxt();
                $weight->weight = $entry->getWeight();
                $arrWeights[] = $weight;
            }
        }
        $model->weightList = $arrWeights;

        $model->additivesList = array();
        $result = $em->getRepository('AppDatabaseMainBundle:PelletEntryAdditives')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\Pellet\FormType\AppPelletEntryWeightModel();
                $weight->txt = $entry->getName();
                $weight->weight = $entry->getWeight();
                $model->additivesList[] = $weight;
            }
        }

        $model->shipment = $entity->getShipment();
        $model->exterialLot = $entity->getExterialLot();
        $model->ordered = $entity->getOrdered();
        $model->created = $entity->getCreated();
        $model->createdBy = $entity->getCreatedBy();
        $model->received = $entity->getReceived();
        $model->receivedStatus = $entity->getReceivedStatus();
        $model->receivedBy = $entity->getReceivedBy();

        $model->expiryDate = $entity->getExpiryDate();
        $model->expiryDateWarningDisabled = $entity->getExpiryDateWarningDisabled();

        $model->isSelected = $entity->getIsSelected();
        $model->dateTaken = $entity->getDateTaken();
        $model->usedFor = $entity->getUsedFor();
        $model->takenBy = $entity->getTakenBy();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('sectionPelletInfo', 'section', array(
            'label'         => 'Pellet Info',
            'property_path' => false
        ));
        $builder->add('cups', 'text', array(
            'label'    => 'Cup(s)',
            'required' => false
        ));
        $builder->add('section', 'choice', array(
            'label'    => 'Section',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Pellet_Section')
        ));
        $builder->add('note', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 3, 'cols' => 70
            )
        ));

        $builder->add('sectionWeight', 'section', array(
            'label'         => 'Weight per Weight (%)',
            'property_path' => false
        ));
        $builder->add('weightList', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_pellet_entry_weight'),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditives', 'section', array(
            'label'         => 'ADDITIVES',
            'property_path' => false
        ));
        $builder->add('additivesList', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_pellet_entry_weight'),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionOrdering', 'section', array(
            'label'         => 'Ordering',
            'property_path' => false
        ));
        $builder->add('shipment', 'text', array(
            'label'    => 'Interial Lot',
            'required' => false
        ));
        $builder->add('exterialLot', 'text', array(
            'label'    => 'Exterial Lot',
            'required' => false
        ));
        $builder->add('ordered', 'text', array(
            'label'    => 'Ordered (k)',
            'required' => false
        ));
        $builder->add('created', 'datepicker', array(
            'label'    => 'Order date',
            'required' => false
        ));
        $builder->add('createdBy', 'text', array(
            'label'    => 'Order by',
            'required' => false
        ));
        $builder->add('receivedStatus', 'checkbox', array(
            'label'    => 'Received?',
            'required' => false
        ));
        $builder->add('received', 'datepicker', array(
            'label'    => 'Received date',
            'required' => false
        ));
        $builder->add('receivedBy', 'text', array(
            'label'    => 'Received by',
            'required' => false
        ));


        $builder->add('sectionExpiracy', 'section', array(
            'label'         => 'Expiration',
            'property_path' => false
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Expiry warning disabled?',
            'required' => false
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));
        $builder->add('isSelected', 'checkbox', array(
            'label'    => 'Is use?',
            'required' => false
        ));
        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));
        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));
        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();       
        
        $this->entity->setCups($model->cups);
        $this->entity->setSection($model->section);
        $this->entity->setNote($model->note);
        
        $this->entity->setShipment($model->shipment);
        $this->entity->setExterialLot($model->exterialLot);
        $this->entity->setOrdered($model->ordered);
        $this->entity->setCreated($model->created);
        $this->entity->setCreatedBy($model->createdBy);
        $this->entity->setReceived($model->received);
        $this->entity->setReceivedStatus($model->receivedStatus);
        $this->entity->setReceivedBy($model->receivedBy);

        $this->entity->setExpiryDate($model->expiryDate);
        $this->entity->setExpiryDateWarningDisabled($model->expiryDateWarningDisabled);

        $this->entity->setIsSelected($model->isSelected);
        $this->entity->setDateTaken($model->dateTaken);
        $this->entity->setUsedFor($model->usedFor);
        $this->entity->setTakenBy($model->takenBy);

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:PelletEntryWeight', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->weightList) {
            foreach ($model->weightList as $eachWeight) {
                $newWeight = new Entity\PelletEntryWeight();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setTxt($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:PelletEntryAdditives', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->additivesList) {
            foreach ($model->additivesList as $eachWeight) {
                $newWeight = new Entity\PelletEntryAdditives();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setName($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        parent::onSuccess();
    }
}
