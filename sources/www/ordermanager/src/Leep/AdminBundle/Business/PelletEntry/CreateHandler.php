<?php
namespace Leep\AdminBundle\Business\PelletEntry;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->created = new \DateTime();
        $model->section = $this->container->get('request')->get('idSection', 0);

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('sectionPelletInfo', 'section', array(
            'label'         => 'Pellet Info',
            'property_path' => false
        ));
        $builder->add('cups', 'text', array(
            'label'    => 'Cup(s)',
            'required' => false
        ));
        $builder->add('section', 'choice', array(
            'label'    => 'Section',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Pellet_Section')
        ));
        $builder->add('note', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 3, 'cols' => 70
            )
        ));

        $builder->add('sectionWeight', 'section', array(
            'label'         => 'Weight per Weight (%)',
            'property_path' => false
        ));
        $builder->add('weightList', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_pellet_entry_weight'),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditives', 'section', array(
            'label'         => 'ADDITIVES',
            'property_path' => false
        ));
        $builder->add('additivesList', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_pellet_entry_weight'),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionOrdering', 'section', array(
            'label'         => 'Ordering',
            'property_path' => false
        ));
        $builder->add('shipment', 'text', array(
            'label'    => 'Interial Lot',
            'required' => false
        ));
        $builder->add('exterialLot', 'text', array(
            'label'    => 'Exterial Lot',
            'required' => false
        ));
        $builder->add('ordered', 'text', array(
            'label'    => 'Ordered (k)',
            'required' => false
        ));
        $builder->add('created', 'datepicker', array(
            'label'    => 'Order date',
            'required' => false
        ));
        $builder->add('createdBy', 'text', array(
            'label'    => 'Order by',
            'required' => false
        ));
        $builder->add('receivedStatus', 'checkbox', array(
            'label'    => 'Received?',
            'required' => false
        ));
        $builder->add('received', 'datepicker', array(
            'label'    => 'Received date',
            'required' => false
        ));
        $builder->add('receivedBy', 'text', array(
            'label'    => 'Received by',
            'required' => false
        ));


        $builder->add('sectionExpiracy', 'section', array(
            'label'         => 'Expiration',
            'property_path' => false
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Expiry warning disabled?',
            'required' => false
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));
        $builder->add('isSelected', 'checkbox', array(
            'label'    => 'Is use?',
            'required' => false
        ));
        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));
        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));
        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $idPellet = $this->container->get('request')->get('idPellet', 0);

        $entry = new Entity\PelletEntry();
        $entry->setIdPellet($idPellet);
        $entry->setCups($model->cups);
        $entry->setSection($model->section);
        $entry->setNote($model->note);

        $entry->setShipment($model->shipment);
        $entry->setExterialLot($model->exterialLot);
        $entry->setOrdered($model->ordered);
        $entry->setCreated($model->created);
        $entry->setCreatedBy($model->createdBy);
        $entry->setReceived($model->received);
        $entry->setReceivedStatus($model->receivedStatus);
        $entry->setReceivedBy($model->receivedBy);

        $entry->setExpiryDate($model->expiryDate);
        $entry->setExpiryDateWarningDisabled($model->expiryDateWarningDisabled);

        $entry->setIsSelected($model->isSelected);
        $entry->setDateTaken($model->dateTaken);
        $entry->setUsedFor($model->usedFor);
        $entry->setTakenBy($model->takenBy);

        $em->persist($entry);
        $em->flush();

        if ($model->weightList) {
            foreach ($model->weightList as $eachWeight) {
                $newWeight = new Entity\PelletEntryWeight();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setTxt($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        if ($model->additivesList) {
            foreach ($model->additivesList as $eachWeight) {
                $newWeight = new Entity\PelletEntryAdditives();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setName($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        parent::onSuccess();
    }
}
