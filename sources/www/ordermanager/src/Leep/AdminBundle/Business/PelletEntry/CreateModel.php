<?php
namespace Leep\AdminBundle\Business\PelletEntry;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $cups;
    public $section;
    public $note;
    public $weightList;
    public $additivesList;
    
    public $shipment;
    public $exterialLot;
    public $ordered;
    public $created;
    public $createdBy;
    public $received;
    public $receivedStatus;
    public $receivedBy;

    public $expiryDate;
    public $expiryDateWarningDisabled;

    public $isSelected;
    public $dateTaken;
    public $usedFor;
    public $takenBy;
}
