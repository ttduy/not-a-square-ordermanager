<?php 
namespace Leep\AdminBundle\Business\PelletEntry;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $pelletEntry = $doctrine->getRepository('AppDatabaseMainBundle:PelletEntry')->findOneById($id);
        $idPellet = $pelletEntry->getIdPellet();
        $em->remove($pelletEntry);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'pellet', 'feature', 'manageStock', array('id' => $idPellet));

        return new RedirectResponse($url);
    }
}