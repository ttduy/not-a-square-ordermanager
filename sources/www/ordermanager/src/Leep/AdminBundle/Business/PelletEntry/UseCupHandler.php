<?php
namespace Leep\AdminBundle\Business\PelletEntry;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormError;

class UseCupHandler extends AppEditHandler {
    public $availableCup = null;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        
        $entity = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PelletEntry', 'app_main')->findOneById($id);
        if ($entity) {
            $this->availableCup = $entity->getCups();
        }

        return $entity;
    }

    public function convertToFormModel($entity) {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        return array(
                        'cups'      => null, 
                        'dateTaken' => new \DateTime(),
                        'usedFor'   => null,
                        'takenBy'   => $userManager->getUser()->getUsername()
                        );
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('sectionUseCup', 'section', array(
            'label'         => 'Use Cup(s)',
            'property_path' => false
        ));

        $builder->add('cups', 'text', array(
            'label'    => 'Cup(s)',
            'required' => false,
            'attr'      => array('placeholder'  => $this->availableCup)
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));
        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));
        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));
        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();       
        
        if ($model['cups'] && $model['cups'] <= $this->availableCup) {
            $usedCups = $model['cups'];
            $dateTaken = ($model['dateTaken'] ? $model['dateTaken'] : null);
            $usedFor = ($model['usedFor'] ? $model['usedFor'] : null);
            $takenBy = ($model['takenBy'] ? $model['takenBy'] : null);
            $isSelected = 1;

            // create new pellet entity with section of "USED UP"
            $newPelletEntry = new Entity\PelletEntry();
            $newPelletEntry->setIdPellet($this->entity->getIdPellet());
            $newPelletEntry->setCups($usedCups);
            $newPelletEntry->setSection(Business\Pellet\Constant::SECTION_USED_UP);
            $newPelletEntry->setNote($this->entity->getNote());
            
            $newPelletEntry->setShipment($this->entity->getShipment());
            $newPelletEntry->setOrdered($this->entity->getOrdered());
            $newPelletEntry->setCreated($this->entity->getCreated());
            $newPelletEntry->setCreatedBy($this->entity->getCreatedBy());
            $newPelletEntry->setReceived($this->entity->getReceived());
            $newPelletEntry->setReceivedStatus($this->entity->getReceivedStatus());
            $newPelletEntry->setReceivedBy($this->entity->getReceivedBy());

            $newPelletEntry->setExpiryDate($this->entity->getExpiryDate());
            $newPelletEntry->setExpiryDateWarningDisabled($this->entity->getExpiryDateWarningDisabled());

            $newPelletEntry->setIsSelected($isSelected);
            $newPelletEntry->setDateTaken($dateTaken);
            $newPelletEntry->setUsedFor($usedFor);
            $newPelletEntry->setTakenBy($takenBy);
            $newPelletEntry->setIdSuperEntry($this->entity->getId());

            $em->persist($newPelletEntry);
            $em->flush();

            $this->entity->setCups($this->entity->getCups() - $usedCups);

            // copy W/W
            $weights = $em->getRepository('AppDatabaseMainBundle:PelletEntryWeight')->findByIdPelletEntry($this->entity->getId());
            if ($weights) {
                foreach ($weights as $eachWeight) {
                    $newWeight = new Entity\PelletEntryWeight();
                    $newWeight->setIdPelletEntry($newPelletEntry->getId());
                    $newWeight->setTxt($eachWeight->getTxt());
                    $newWeight->setWeight($eachWeight->getWeight());
                    $newWeight->setSortOrder($eachWeight->getSortOrder());
                    $em->persist($newWeight);
                }
                $em->flush();
            }

            parent::onSuccess();
        } else {
            $this->getForm()->get('cups')->addError(new FormError("The number of Cups to be used must be equal or less than " . $this->availableCup));
        }
    }
}
