<?php
namespace Leep\AdminBundle\Business\PelletEntry;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('pelletInfo', 'ordering', 'usage', 'note', 'action');
    }

    public $rowTemplate = '';
    public function __construct($container) {
        parent::__construct($container);

        $this->rowTemplate = 
            "<div>
                <div style='float:left; width: 110px'>%s</div>
                <div style='float:left;color: #069'>%s</div>
                <div style='clear:both'></div>
            </div>";
    }
    
    public function getTableHeader() {
        return array(
            array('title' => 'Pellet entry info',       'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Ordering',                'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Usage',                   'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Notes',                   'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Action',                  'width' => '10%', 'sortable' => 'false'),
              
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_PelletEntryGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('
                    p.id,
                    p.cups,
                    p.expiryDate,
                    p.expiryDateWarningDisabled,
                    p.shipment,
                    p.exterialLot,
                    p.ordered,
                    p.created,
                    p.createdBy,
                    p.receivedStatus,
                    p.receivedBy,
                    p.isSelected,
                    p.usedFor,
                    p.dateTaken,
                    p.takenBy,
                    p.section,
                    pe.expiryDateWarningThreshold
                ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'p')
            ->innerJoin('AppDatabaseMainBundle:Pellet', 'pe', 'WITH', 'pe.id = p.idPellet');

        $request = $this->container->get('request');
        $id = $request->get('id', 0);
        $status = $request->get('status', 0);

        $queryBuilder->andWhere('p.idPellet = :idPellet')->setParameter('idPellet', $id);
        $queryBuilder->andWhere('p.section = :status')->setParameter('status', $status);
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.created', 'DESC');
    }

    public function buildCellPelletInfo($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');

        return 
            sprintf($this->rowTemplate, 'ID', $row['id']).
            sprintf($this->rowTemplate, 'Cups', $row['cups']).
            sprintf($this->rowTemplate, 'Expiry date', $formatter->format($row['expiryDate'], 'date') . $this->getWarningInfo($row)).
            sprintf($this->rowTemplate, 'Expiry disabled', $row['expiryDateWarningDisabled'] == 0 ? "---" : "Disabled").
            sprintf($this->rowTemplate, 'W/W (%)', $this->getWeightList($row)).
            sprintf($this->rowTemplate, 'Additives', $this->getAdditivesList($row));
    }

    public function buildCellOrdering($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');

        return 
            sprintf($this->rowTemplate, 'Interial Lot', $row['shipment']).
            sprintf($this->rowTemplate, 'Exterial Lot', $row['exterialLot']).
            sprintf($this->rowTemplate, 'Ordered (k)', $row['ordered']).
            sprintf($this->rowTemplate, 'Order date', $formatter->format($row['created'], 'date')).
            sprintf($this->rowTemplate, 'Order by', $row['createdBy']).
            sprintf($this->rowTemplate, 'Received?', $row['receivedStatus'] == 0 ? "---" : "Yes").
            sprintf($this->rowTemplate, 'Received by', $row['receivedBy']);
    }

    public function buildCellUsage($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        return 
            sprintf($this->rowTemplate, 'In use?', $row['isSelected'] == 0 ? "---" : "Yes").
            sprintf($this->rowTemplate, 'Used for', $row['usedFor']).
            sprintf($this->rowTemplate, 'Date taken', $formatter->format($row['dateTaken'], 'date')).
            sprintf($this->rowTemplate, 'Taken by', $row['takenBy']);

    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'pellet_entry', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');

        if ($row['section'] == Business\Pellet\Constant::SECTION_TESTED_AND_IN_USE) {
            $builder->addPopupButton('Use Cup', $mgr->getUrl('leep_admin', 'pellet_entry', 'use_cup', 'edit', array('id' => $row['id'])), 'button-submit');
        }
        $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'pellet_entry', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        
        $html = $builder->getHtml();
        return $html;
    }

    public function getWeightList($row) {
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PelletEntryWeight')->findByIdPelletEntry($row['id']);

        $arrWeights = array();
        if ($result) {
            foreach ($result as $entry) {
                $arrWeights[] = $entry->getTxt() . ' - ' . $entry->getWeight();
            }
        }

        return (!empty($arrWeights) ? implode('<br/>', $arrWeights) : '');
    }

    public function getAdditivesList($row) {
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PelletEntryAdditives')->findByIdPelletEntry($row['id']);

        $arrWeights = array();
        if ($result) {
            foreach ($result as $entry) {
                $arrWeights[] = $entry->getName() . ' - ' . $entry->getWeight();
            }
        }

        return (!empty($arrWeights) ? implode('<br/>', $arrWeights) : '');
    }

    private function getWarningInfo($row) {
        $html = '';
        if (!$row['expiryDateWarningDisabled'] && in_array($row['section'], Business\Pellet\Constant::expiryWarningSections())) {
            if ($row['expiryDate'] != null) {
                $today = new \DateTime();
                $today->setTime(0,0,0);
                $expiryDate = $row['expiryDate'];
                $expiryDate->setTime(0,0,0);
                $interval = date_diff($today, $expiryDate);
                $days = intval($interval->format('%R%a'));
                if ($days < $row['expiryDateWarningThreshold']) {
                    $html .= '&nbsp;';
                    $htmlTpl = '<div style="color:%s">%s</div>';
                    if ($days >= 0) {
                        $html .= sprintf($htmlTpl, 'orange', 'To be expired in ' . $days . ' day(s)');
                    } else {
                        $html .= sprintf($htmlTpl, 'red', 'Expired '.abs($days).' day(s)');
                    }
                }
            }
        }
        return $html;
    }
}
