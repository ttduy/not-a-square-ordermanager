<?php
namespace Leep\AdminBundle\Business\Massarray;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('name', 'genes', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',               'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Genes',              'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Massarray', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
       
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellGenes($row) {
        $mapping = $this->container->get('easy_mapping');
        $genes = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MassarrayGeneLink')->findBymassarrayid($row->getId());
        foreach ($result as $r) {
            $genes[] = $mapping->getMappingTitle('LeepAdmin_Gene_List', $r->getGeneId());
        }
        return implode(', ', $genes);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('massarrayModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'massarray', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            //$builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'massarray', 'view', 'view', array('id' => $row->getId())), 'button-detail');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'massarray', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }
}
