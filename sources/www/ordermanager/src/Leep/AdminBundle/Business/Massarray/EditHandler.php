<?php
namespace Leep\AdminBundle\Business\Massarray;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Massarray', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        $model->genes = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from("AppDatabaseMainBundle:MassarrayGeneLink", 'p')
            ->andWhere('p.massarrayid = '.$entity->getId());
        $genes = $query->getQuery()->getResult();
        foreach ($genes as $pLink) {
            $model->genes[] = $pLink->getGeneId();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        

        $builder->add('genes', 'choice', array(
            'label'    => 'Genes',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Gene_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();       
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setName($model->name);
        
        // update genes                
        $filters = array('massarrayid' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MassarrayGeneLink', $filters);
        foreach ($model->genes as $idGene) {
            $massarrayGene = new Entity\MassarrayGeneLink();
            $massarrayGene->setMassarrayId($this->entity->getId());
            $massarrayGene->setGeneId($idGene);
            $em->persist($massarrayGene);
        }
        $em->flush();


        parent::onSuccess();
    }
}
