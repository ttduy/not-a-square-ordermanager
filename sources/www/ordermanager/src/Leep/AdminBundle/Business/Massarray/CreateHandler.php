<?php
namespace Leep\AdminBundle\Business\Massarray;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('genes', 'choice', array(
            'label'    => 'Genes',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Gene_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $massarray = new Entity\Massarray();
        $massarray->setName($model->name);

        $em->persist($massarray);
        $em->flush();

        foreach ($model->genes as $idGene) {
            $massarrayGene = new Entity\MassarrayGeneLink();
            $massarrayGene->setMassarrayId($massarray->getId());
            $massarrayGene->setGeneId($idGene);
            $em->persist($massarrayGene);
        }
        $em->flush();  

        parent::onSuccess();
    }
}
