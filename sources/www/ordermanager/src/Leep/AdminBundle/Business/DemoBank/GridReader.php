<?php
namespace Leep\AdminBundle\Business\DemoBank;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idGene', 'idFamVicMappingRule', 'fam', 'vic', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Gene',                        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Scientist Assignment',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'FAM',                         'width' => '20%', 'sortable' => 'false'),
            array('title' => 'VIC',                         'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_DemoBank_GridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DemoBank', 'p');

        if ($this->filters->idGene) {
            $queryBuilder->andWhere('p.idGene = :idGene')
                ->setParameter('idGene', $this->filters->idGene);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('demoBankModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'demo_bank', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'demo_bank', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}