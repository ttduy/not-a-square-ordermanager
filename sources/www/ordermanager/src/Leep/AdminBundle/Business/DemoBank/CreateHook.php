<?php
namespace Leep\AdminBundle\Business\DemoBank;

use Easy\ModuleBundle\Module\AbstractHook;

class CreateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
    	$em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');

        $result = $em->getRepository('AppDatabaseMainBundle:FamVicMappingRule')->findAll();

        $arrFamVicMappingRules = array();
        if ($result) {
        	foreach ($result as $entry) {
        		if (!array_key_exists($entry->getIdResultGroup(), $arrFamVicMappingRules)) {
        			$arrFamVicMappingRules[$entry->getIdResultGroup()] = array();
        		}
        		$arrFamVicMappingRules[$entry->getIdResultGroup()][$entry->getId()] = $entry->getResult();
        	}
        }

        $arrGeneResultGroups = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        if ($result) {
        	foreach ($result as $entry) {
        		$arrGeneResultGroups[$entry->getId()] = $entry->getGroupid();
        	}
        }

        $data['famVicMappingRules'] = json_encode($arrFamVicMappingRules);
        $data['geneResultGroups'] = json_encode($arrGeneResultGroups);
    }
}
