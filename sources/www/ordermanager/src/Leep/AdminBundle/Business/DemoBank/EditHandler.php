<?php
namespace Leep\AdminBundle\Business\DemoBank;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DemoBank', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->idGene = $entity->getIdGene();
        $model->idFamVicMappingRule = $entity->getIdFamVicMappingRule();
        $model->fam = $entity->getFam();
        $model->vic = $entity->getVic();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('fam', 'text', array(
            'label' => 'FAM',
            'required' => true
        ));

        $builder->add('vic', 'text', array(
            'label' => 'VIC',
            'required' => true
        ));

        $builder->add('idGene', 'choice', array(
            'label' => 'Gene',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Gene_List')
        ));

/*        $builder->add('idFamVicMappingRule', 'choice', array(
            'label' => 'Scientist Assignment',
            'required' => true,
            'choices'   => array()
        ));
*/

        
        $builder->add('idFamVicMappingRule', 'hidden');

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setIdGene($model->idGene);
        $this->entity->setIdFamVicMappingRule($model->idFamVicMappingRule);
        $this->entity->setFam($model->fam);
        $this->entity->setVic($model->vic);

        parent::onSuccess();
    }
}