<?php
namespace Leep\AdminBundle\Business\DemoBank;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idGene', 'choice', array(
            'label'    => 'Gene',
            'required' => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Gene_List')
        ));

        return $builder;
    }
}