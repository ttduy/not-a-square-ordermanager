<?php
namespace Leep\AdminBundle\Business\DemoBank;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('fam', 'text', array(
            'label' => 'FAM',
            'required' => true
        ));

        $builder->add('vic', 'text', array(
            'label' => 'VIC',
            'required' => true
        ));

        $builder->add('idGene', 'choice', array(
            'label' => 'Gene',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Gene_List')
        ));

        $builder->add('idFamVicMappingRule', 'hidden');

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = $this->getForm()->getData();

        $demoBank = new Entity\DemoBank();
        $demoBank->setFam($model->fam);
        $demoBank->setVic($model->vic);
        $demoBank->setIdGene($model->idGene);
        $demoBank->setIdFamVicMappingRule($model->idFamVicMappingRule);
        
        $em->persist($demoBank);
        $em->flush();
        
        parent::onSuccess();
    }
}