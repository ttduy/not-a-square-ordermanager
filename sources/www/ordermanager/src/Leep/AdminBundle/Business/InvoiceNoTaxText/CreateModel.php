<?php
namespace Leep\AdminBundle\Business\InvoiceNoTaxText;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $textGermany;
    public $textEnglish;
    public $name;
    public $sortOrder;
}
