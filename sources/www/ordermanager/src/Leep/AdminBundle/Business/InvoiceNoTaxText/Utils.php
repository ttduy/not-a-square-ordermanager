<?php
namespace Leep\AdminBundle\Business\InvoiceNoTaxText;

class Utils {
    public static function getText($container, $id) {
        $em = $container->get('doctrine')->getEntityManager();
        $invoiceNoTaxText = $em->getRepository('AppDatabaseMainBundle:InvoiceNoTaxText')->findOneById($id);
        if ($invoiceNoTaxText) {
            return [
                'textGermany' => $invoiceNoTaxText->getTextGermany(),
                'textEnglish' => $invoiceNoTaxText->getTextEnglish()
            ];
        }

        return [
            'textGermany' => '',
            'textEnglish' => ''
        ];
    }
}
