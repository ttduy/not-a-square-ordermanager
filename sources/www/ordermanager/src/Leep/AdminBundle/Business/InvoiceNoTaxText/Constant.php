<?php
namespace Leep\AdminBundle\Business\InvoiceNoTaxText;

class Constant {
    public static function get($container) {
        $em = $container->get('doctrine')->getEntityManager();
        $invoiceNoTaxText = $em->getRepository('AppDatabaseMainBundle:InvoiceNoTaxText')->findBy([], ['sortOrder' => 'DESC']);
        $arrayList = [0 => ''];
        foreach ($invoiceNoTaxText as $key => $value) {
            $arrayList[$value->getId()] = $value->getName();
        }

        return $arrayList;
    }
}
