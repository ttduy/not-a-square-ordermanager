<?php
namespace Leep\AdminBundle\Business\InvoiceNoTaxText;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false,
        ));
        $builder->add('textGermany', 'text', array(
            'label'    => 'German text',
            'required' => true
        ));
        $builder->add('textEnglish', 'text', array(
            'label'    => 'English text',
            'required' => true
        ));
        return $builder;
    }
}
