<?php
namespace Leep\AdminBundle\Business\TextBlockGroup;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $language = new Entity\TextBlockGroup();
        $language->setName($model->name);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($language);
        $em->flush();
        
        parent::onSuccess();
    }
}