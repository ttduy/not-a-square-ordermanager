<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

class CreateModel {
    public $includeOptedOutParties;
    public $includeMustNotContactedParties;
    public $filterByCountries;
    public $filterByLanguages;

    public $idQueue;
    public $idEmailMethod;
    public $title;
    public $body;
    public $contentHtml;
    public $attachments;
    public $isSendNow;
    public $status;

    // Tab's properties
    public $recipientsProperties = array();

    public function __set($name, $value = null) {
        $this->recipientsProperties[$name] = $value;
    }

    public function __get($name) {
        if (!array_key_exists($name, $this->recipientsProperties)) {
            $this->recipientsProperties[$name] = null;
        }

        return $this->recipientsProperties[$name];
    }

    public function getRecipientsProperties() {
        return $this->recipientsProperties;
    }

    public function setRecipientsProperties($properties = array()) {
        return $this->recipientsProperties = $properties;
    }

    public function loadFromParams($params = array()) {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }
}
