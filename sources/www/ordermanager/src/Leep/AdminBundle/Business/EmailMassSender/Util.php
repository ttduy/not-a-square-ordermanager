<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

use Leep\AdminBundle\Business\EmailOutgoing\Constant as EmailOutgoingConstant;

class Util {

    static public function extractRecipient($recipients) {
        $recipients = array_map('trim', explode('>', $recipients));
        $result = [];
        foreach ($recipients as $recipient) {
            if (empty($recipient)) {
                continue;
            }

            $recipientName = '';
            $recipientEmail = $recipient;

            $arrParts = explode('<', trim($recipient));
            if (sizeOf($arrParts) == 2) {
                $recipientName = trim($arrParts[0]);
                $recipientEmail = $arrParts[1];
            }

            $recipientEmail = array_map('trim', explode(';', $recipientEmail));
            if ($recipientName[0] == ';') {
                $recipientName = ltrim($recipientName, ';');
            }

            $result[] = [
                'name' =>   $recipientName,
                'email' =>  $recipientEmail
            ];
        }

        return $result;
    }

    static public function updateMassSenderStatus($em, $massSender) {
         $emailOutgoingList = $em->getRepository("AppDatabaseMainBundle:EmailOutgoing")
            ->createQueryBuilder('p')
            ->where("p.idMassSender = :idMassSender")
            ->setParameter('idMassSender', $massSender->getId())
            ->getQuery()
            ->iterate();

        $statistic = [
            'NumOpen' =>    0,
            'NumSending' => 0,
            'NumSent' =>    0,
            'NumError' =>   0,
            'NumSendNow' => 0
        ];

        foreach ($emailOutgoingList as $emailOutgoing) {
            $emailOutgoing = $emailOutgoing[0];
            $status = $emailOutgoing->getStatus();
            switch ($status) {
                case EmailOutgoingConstant::EMAIL_STATUS_OPEN:
                    $statistic['NumOpen']++;
                    break;

                case EmailOutgoingConstant::EMAIL_STATUS_SENDING:
                    $statistic['NumSending']++;
                    break;

                case EmailOutgoingConstant::EMAIL_STATUS_SENT:
                    $statistic['NumSent']++;
                    break;

                default:
                    $statistic['NumError']++;
                    break;
            }

            if ($emailOutgoing->getIsSendNow()) {
                $statistic['NumSendNow']++;
            }

            $em->detach($emailOutgoing);
        }

        foreach ($statistic as $key => $value) {
            $method = "set$key";
            $massSender->$method($value);
        }

        $em->persist($massSender);

        if ($massSender->getNumOpen() == 0 && $massSender->getNumSending() == 0) {
            $massSender->setStatus(Constant::MASS_SENDER_STATUS_DONE);
        } else if ($massSender->getNumSending() > 0) {
            $massSender->setStatus(Constant::MASS_SENDER_STATUS_PROCESSING);
        } else if ($massSender->getNumSending() == 0 && $massSender->getNumSent() == 0 && $massSender->getNumError() == 0) {
            $massSender->setStatus(Constant::MASS_SENDER_STATUS_OPEN);
        }

        $em->flush();
    }
}
