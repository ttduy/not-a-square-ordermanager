<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

use Easy\ModuleBundle\Module\AbstractHook;

class CreateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $recipientsSectionFactory = $controller->get('leep_admin.email_mass_sender.business.recipients_factory');

        $arrTabs = array();
        foreach (Constant::getReceiverTypeList() as $receiverType => $receiverName) {
            $inputList = $recipientsSectionFactory->getInputList($receiverType);

            if ($inputList) {
                $arrTabs[$receiverType] = array(
                    'input' => $inputList,
                    'title' => $receiverName
                );
            }
        }

        $data['whoToTabs'] = $arrTabs;
        $data['countReceiversURL'] = $mgr->getUrl('leep_admin', 'email_mass_sender', 'feature', 'countRecievers');
    }
}
