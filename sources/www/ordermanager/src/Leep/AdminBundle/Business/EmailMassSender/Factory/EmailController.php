<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

use App\Database\MainBundle\Entity;

abstract class EmailController {
    protected $container;
    function __construct($container) {
        $this->container = $container;
    }

    abstract public function getInputList();
    abstract public function build($builder, $mapping);

    public function unsubscribe($emailList, $id = null) {
        $em = $this->container->get('doctrine')->getEntityManager();
        foreach ($emailList as $email) {
            $e = $em->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findOneByEmail($email);
            if (empty($e)) {
                $emailOptOutList = new Entity\EmailOptOutList();
                $emailOptOutList->setEmail($email);
                $em->persist($emailOptOutList);
                $em->flush();
            }
        }
    }

    abstract public function grab($model, $getTotalOnly = false);
}