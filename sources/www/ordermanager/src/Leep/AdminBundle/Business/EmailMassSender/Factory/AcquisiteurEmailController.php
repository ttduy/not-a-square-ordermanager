<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class AcquisiteurEmailController extends EmailController {

    public function getInputList() {
        return array('sectionAcquisiteur', 'toAcquisiteur', 'toAcquisiteurFilterByDistributionChannel',
            'toAcquisiteurFilterByPartner', 'toAcquisiteurFilterByTimeSinceLastOrder', 'toAcquisiteurFilterByDCType');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionAcquisiteur', 'section', array(
            'label'         => 'Acquisiteur',
            'property_path' => false,
        ));

        $builder->add('toAcquisiteur', 'checkbox', array(
            'label'    => 'Acquisiteur',
            'required' => false
        ));

        $builder->add('toAcquisiteurFilterByDistributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toAcquisiteurFilterByPartner', 'choice', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toAcquisiteurFilterByTimeSinceLastOrder', 'text', array(
            'label'     => 'Filter by Time Since Last Order',
            'required'  => false
        ));

        $builder->add('toAcquisiteurFilterByDCType', 'choice', array(
            'label'     => 'Filter by Distribution Channel Type',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($id);
            if ($acquisiteur) {
                $acquisiteur->setIsNotContacted(1);
                $em->persist($acquisiteur);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($model->toAcquisiteur) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Acquisiteur', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($model->toAcquisiteurFilterByDistributionChannel || $model->toAcquisiteurFilterByDCType) {
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.acquisiteurid1 = p.id OR dc.acquisiteurid2 = p.id OR dc.acquisiteurid3 = p.id');

                $filterByDCs = $model->toAcquisiteurFilterByDistributionChannel;
                $filterByDCTypes = $model->toAcquisiteurFilterByDCType;

                if ($filterByDCs) {
                    $query->andWhere('dc.id IN (:dcIds)')
                    ->setParameter('dcIds', $filterByDCs);
                }

                if ($filterByDCTypes) {
                    $query->andWhere('dc.typeid IN (:dcTypeIds)')
                    ->setParameter('dcTypeIds', $filterByDCTypes);
                }
            }

            if ($model->toAcquisiteurFilterByPartner) {
                $query->innerJoin('AppDatabaseMainBundle:Partner', 'partner', 'WITH', 'partner.acquisiteurid1 = p.id OR partner.acquisiteurid2 = p.id OR partner.acquisiteurid3 = p.id')
                    ->andWhere('partner.id IN (:partnerIds)')
                    ->setParameter('partnerIds', $model->toAcquisiteurFilterByPartner);
            }

            if ($model->toAcquisiteurFilterByTimeSinceLastOrder) {
                // TO BE IMPLEMENTED LATER
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                //$countries = explode(',', $model->filterByCountries);
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $query->groupBy('p.contactemail');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $email = trim($row->getContactemail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}