<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

use Leep\AdminBundle\Business\EmailMassSender\Constant;

class RecipientsFactory {
    private $container;
    function __construct($container) {
        $this->container = $container;
    }

    public $registry = array(
        Constant::RECEIVER_TYPE_CUSTOMER => 'leep_admin.email_mass_sender.business.customer_email_controller',
        Constant::RECEIVER_TYPE_DISTRIBUTION_CHANNEL => 'leep_admin.email_mass_sender.business.distribution_channel_email_controller',
        Constant::RECEIVER_TYPE_PARTNER => 'leep_admin.email_mass_sender.business.partner_email_controller',
        Constant::RECEIVER_TYPE_LEAD => 'leep_admin.email_mass_sender.business.lead_email_controller',
        Constant::RECEIVER_TYPE_ACQUISITEUR => 'leep_admin.email_mass_sender.business.acquisiteur_email_controller',
        Constant::RECEIVER_TYPE_PRESS => 'leep_admin.email_mass_sender.business.press_email_controller',
        Constant::RECEIVER_TYPE_PREDEFINED_RECIPIENT => 'leep_admin.email_mass_sender.business.predefined_recipient_email_controller',
        Constant::RECEIVER_TYPE_NEWSLETTER_RECIPIENT => 'leep_admin.email_mass_sender.business.newsletter_recipient_email_controller',
    );

    public function build($type, $builder, $mapping) {
        if (array_key_exists($type, $this->registry))
            $this->container->get($this->registry[$type])->build($builder, $mapping);
    }

    public function getInputList($type) {
        if (array_key_exists($type, $this->registry))
            return $this->container->get($this->registry[$type])->getInputList();

        return null;
    }

    public function unsubscribe($type, $emailList, $id = null) {
        if (array_key_exists($type, $this->registry))
            return $this->container->get($this->registry[$type])->unsubscribe($emailList, $id = null);
    }

    public function grab($type, $model, $getTotalOnly = false) {
        if (array_key_exists($type, $this->registry))
            return $this->container->get($this->registry[$type])->grab($model, $getTotalOnly);
    }
}