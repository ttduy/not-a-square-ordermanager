<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('createdDate', 'title', 'progress', 'sendNow', 'read', 'status', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Created Datetime', 'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Title', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Progress', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Send now', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Email read', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Status', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmailMassSenderGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailMassSender', 'p');

        if (!empty($this->filters->title)) {
            $queryBuilder->andWhere('p.title LIKE :title')
                ->setParameter('title', '%'.$this->filters->title.'%');
        }

        if (!empty($this->filters->status)) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.createdDate', 'DESC');
    }

    public function buildCellProgress($row) {
        return sprintf("<font color='orange'>%s open</font><br/><font color='green'>%s sending</font><br/><font color='blue'>%s sent</font><br/><font color='red'>%s error</font>",
            $row->getNumOpen(),
            $row->getNumSending(),
            $row->getNumSent(),
            $row->getNumError());
    }

    public function buildCellRead($row) {
        return $row->getNumRead().' read';
    }

    public function buildCellSendNow($row) {
        return $row->getNumSendNow().' in queue';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $id = $row->getId();
        if ($helperSecurity->hasPermission('emailMassSenderModify')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'email_mass_sender', 'delete', 'delete', array('id' => $id)), 'button-delete');

            $em = $this->container->get('doctrine')->getEntityManager();
            $emailOutgoingList = $em->getRepository('AppDatabaseMainBundle:EmailOutgoing')->findBy(array(
                'idMassSender' => $id
            ));

            $bulkList = array();
            if ($emailOutgoingList) {
                foreach ($emailOutgoingList as $email) {
                    $bulkList[] = $email->getId();
                }
            }

            $builder->addPopupButton('Send', $mgr->getUrl('leep_admin', 'email_outgoing', 'bulk_process', 'edit', array('selected' => implode(',', $bulkList))), 'button-edit');
        }

        // $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'email_mass_sender', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        return $builder->getHtml();
    }
}
