<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class PartnerEmailController extends EmailController {

    public function getInputList() {
        return array('sectionPartner', 'toPartner', 'toPartnerFilterByDistributionChannel',
            'toPartnerFilterByDistributionChannelCity', 'toPartnerFilterByAcquisiteur');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionPartner', 'section', array(
            'label'         => 'Partner',
            'property_path' => false,
        ));

        $builder->add('toPartner', 'checkbox', array(
            'label'    => 'Partner',
            'required' => false
        ));

        $builder->add('toPartnerFilterByDistributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toPartnerFilterByDistributionChannelCity', 'text', array(
            'label'     => 'City',
            'required'  => false
        ));

        $builder->add('toPartnerFilterByAcquisiteur', 'choice', array(
            'label'     => 'Acquisiteur',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
            'multiple'  => 'multiple'
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $newsletterRecipient = $em->getRepository('AppDatabaseMainBundle:NewsletterRecipient')->findOneById($id);
            if ($newsletterRecipient) {
                $newsletterRecipient->setIsNotContacted(1);
                $em->persist($newsletterRecipient);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($model->toPartner) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Partner', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($model->toPartnerFilterByDistributionChannel) {
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.partnerid = p.id')
                    ->andWhere('dc.id IN (:dcIds)')
                    ->setParameter('dcIds', $model->toPartnerFilterByDistributionChannel);
            }

            if ($model->toPartnerFilterByDistributionChannelCity) {
                $val = $model->toPartnerFilterByDistributionChannelCity;
                $query->andWhere('p.city LIKE :partnerCity')
                    ->setParameter('partnerCity', '%' . $val . '%');
            }

            if ($model->toPartnerFilterByAcquisiteur) {
                $query->andWhere('p.acquisiteurid1 IN (:acquisiteurIds) OR p.acquisiteurid2 IN (:acquisiteurIds) OR p.acquisiteurid3 IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $model->toPartnerFilterByAcquisiteur);
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$model->includeMustNotContactedParties) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($model->filterByLanguages) {
                $languages = $model->filterByLanguages;
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $query->groupBy('p.contactemail');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $email = trim($row->getContactemail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}