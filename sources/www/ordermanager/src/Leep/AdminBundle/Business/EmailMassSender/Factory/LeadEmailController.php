<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class LeadEmailController extends EmailController {

    public function getInputList() {
        return array('sectionLead', 'toLead', 'toLeadFilterByDistributionChannel', 'toLeadFilterByPartner',
            'toLeadFilterByAcquisiteur');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionLead', 'section', array(
            'label'         => 'Lead',
            'property_path' => false,
        ));

        $builder->add('toLead', 'checkbox', array(
            'label'    => 'Lead',
            'required' => false
        ));

        $builder->add('toLeadFilterByDistributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toLeadFilterByPartner', 'choice', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toLeadFilterByAcquisiteur', 'choice', array(
            'label'     => 'Acquisiteur',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
            'multiple'  => 'multiple'
        ));
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($model->toLead) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactEmail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactEmail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Lead', 'p')
                ->andWhere('p.contactEmail IS NOT NULL');

            if ($model->toLeadFilterByDistributionChannel) {
                $query->andWhere('p.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $model->toLeadFilterByDistributionChannel);
            }

            if ($model->toLeadFilterByPartner) {
                $query->andWhere('p.idPartner IN (:partnerIds)')
                    ->setParameter('partnerIds', $model->toLeadFilterByPartner);
            }

            if ($model->toLeadFilterByAcquisiteur) {
                $query->andWhere('p.idAcquisiteur IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $model->toLeadFilterByAcquisiteur);
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                //$countries = explode(',', $model->filterByCountries);
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.contactEmail');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstName() . ' ' . $row->getSurName());
                        $email = trim($row->getContactEmail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}