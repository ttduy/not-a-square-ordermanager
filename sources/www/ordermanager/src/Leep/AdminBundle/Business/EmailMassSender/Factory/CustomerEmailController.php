<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class CustomerEmailController extends EmailController {

    public function getInputList() {
        return array('sectionCustomer', 'toCustomer', 'toCustomerByDistributionChannel', 'toCustomerByReportLanguage',
            'toCustomerByCity', 'toCustomerByGender', 'toCustomerByAge', 'toCustomerByTimeSinceLastOrder',
            'toCustomerByOrderedProduct', 'toCustomerByNotOrderedProduct');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionCustomer', 'section', array(
            'label'         => 'Customer',
            'property_path' => false,
        ));

        $builder->add('toCustomer', 'checkbox', array(
            'label'    => 'Customer',
            'required' => false
        ));

        $builder->add('toCustomerByDistributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toCustomerByReportLanguage', 'choice', array(
            'label'     => 'Report Language',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Language_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toCustomerByCity', 'text', array(
            'label'     => 'City',
            'required'  => false
        ));

        $builder->add('toCustomerByGender', 'choice', array(
            'label'     => 'Gender',
            'required'  => false,
            'choices'   =>  $mapping->getMapping('LeepAdmin_Gender_List'),
            'expanded'  => 'expanded'
        ));

        $builder->add('toCustomerByAge', 'app_comparator_type_select', array(
            'label'     => '',
            'required'  => false,
            'selectLabel'  => 'Age'
        ));

        $builder->add('toCustomerByTimeSinceLastOrder', 'app_comparator_type_select', array(
            'label'     => '',
            'required'  => false,
            'selectLabel'  => 'N Day Since Last Order'
        ));

        $builder->add('toCustomerByOrderedProduct', 'choice', array(
            'label'     => 'Products Ordered',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toCustomerByNotOrderedProduct', 'choice', array(
            'label'     => 'Products NOT Ordered',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
            if ($customer) {
                $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($customer->getIdCustomerInfo());
                $customerInfo->setIsNotContacted(1);
                $em->persist($customerInfo);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('REGEXP', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Regexp');
        $emConfig->addCustomDatetimeFunction('DATEDIFF', 'Leep\AdminBundle\Business\Customer\DoctrineExt\DateDiff');
        $emConfig->addCustomDatetimeFunction('YEAR', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Year');

        $comparatorTypeFactory = $this->container->get('leep_admin.comparator_type.comparator_type_factory');

        $arrItems = array();
        $totalRow = 0;

        if ($model->toCustomer) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct ci.email)');
            } else {
                $query->select('
                                    p.id, ci.firstName, ci.surName,
                                    ci.email, ci.isCustomerBeContacted, ci.isNotContacted,
                                    ci.isNutrimeOffered
                                ')
                    ->orderBy('ci.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Customer', 'p')
                ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'ci', 'WITH', 'p.idCustomerInfo = ci.id')
                ->andWhere('ci.email IS NOT NULL');

            // DC filter
            if ($model->toCustomerByDistributionChannel) {
                $query->andWhere('ci.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $model->toCustomerByDistributionChannel);
            }

            // Report Language filter
            if ($model->toCustomerByReportLanguage) {
                $query->andWhere('p.languageid IN (:repLangIds)')
                    ->setParameter('repLangIds', $model->toCustomerByReportLanguage);
            }

            // Order's City filter
            if ($model->toCustomerByCity) {
                $val = $model->toCustomerByCity;
                $query->andWhere('p.city LIKE :orderCity')
                    ->setParameter('orderCity', '%' . $val . '%');
            }

            // CustomerInfo's Gender filter
            if ($model->toCustomerByGender) {
                $val = $model->toCustomerByGender;
                $query->andWhere('ci.genderid = :customerInfoGender')
                    ->setParameter('customerInfoGender', $val);
            }

            // CustomerInfo's Age filter
            if ($comparatorTypeFactory->hasSelectComparatorType($model->toCustomerByAge)) {
                $val = $model->toCustomerByAge;
                $comparatorTypeFactory->buildQuery($query, '(YEAR(CURRENT_DATE()) - YEAR(ci.dateofbirth))', $val);
            }

            // Time Since Last Order filter
            if ($model->toCustomerByTimeSinceLastOrder) {
                $val = $model->toCustomerByTimeSinceLastOrder;
                $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.distributionchannelid');
                $comparatorTypeFactory->buildQuery($query, 'DATEDIFF(CURRENT_DATE(), dc.lastOrderDate)', $val);
            }

            // Ordered Products filter && Not Ordered Products filter
            if ($model->toCustomerByOrderedProduct || $model->toCustomerByNotOrderedProduct) {
                $orderedProducts = $model->toCustomerByOrderedProduct;
                $notOrderedProducts = $model->toCustomerByNotOrderedProduct;

                if (!empty($orderedProducts)) {
                    $query->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                            ->setParameter('regexp', ',' . implode('|', $orderedProducts) . ',');
                }

                if (!empty($notOrderedProducts)) {
                    $excludedCustomerInfos = array(0);
                    $queryExcludedOrder = $em->createQueryBuilder();
                    $result = $queryExcludedOrder->select('p.idCustomerInfo')
                                            ->from('AppDatabaseMainBundle:Customer', 'p')
                                            ->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                                            ->setParameter('regexp', ',' . implode('|', $notOrderedProducts) . ',')
                                            ->andWhere('p.idCustomerInfo IS NOT NULL')
                                            ->groupBy('p.idCustomerInfo')
                                            ->getQuery()
                                            ->getResult();
                    if ($result) {
                        foreach ($result as $entry) {
                            if ($entry['idCustomerInfo']) {
                                $excludedCustomerInfos[] = $entry['idCustomerInfo'];
                            }
                        }
                    }

                    $query->andWhere('p.idCustomerInfo NOT IN (:notOrderedProductExcludedOrderIds)')
                            ->setParameter('notOrderedProductExcludedOrderIds', $excludedCustomerInfos);
                }
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(ci.isNotContacted = 0 OR ci.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$model->includeMustNotContactedParties) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(ci.isCustomerBeContacted = 1)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($model->filterByLanguages) {
                $languages = $model->filterByLanguages;
                $query->andWhere('p.languageid IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.idCustomerInfo');
                $query->groupBy('ci.email');

                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row['firstName'] . ' ' . $row['surName']);
                        $email = trim($row['email']);
                        $id = $row['id'];
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}