<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ViewHandler extends AppViewHandler {
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $id = $this->container->get('request')->query->get('id', 0);
        $EmailOutgoingLog = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailOutgoingLog');
        $logs = $EmailOutgoingLog->findBy(
            array(
                'idEmailOutgoing' => $id
            ),
            array(
                'logTimestamp' =>   'ASC',
                'id' =>             'ASC'
            ));

        $data['logs'] = array();
        foreach ($logs as $log) {
            $data['logs'][] = array (
                'timestamp' =>  $formatter->format($log->getLogTimestamp(), 'datetime'),
                'message' =>    $log->getLogMessage()
            );
        }

        return $data;
    }
}
