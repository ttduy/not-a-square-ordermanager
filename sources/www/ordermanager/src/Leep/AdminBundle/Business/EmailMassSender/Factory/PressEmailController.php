<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class PressEmailController extends EmailController {

    public function getInputList() {
        return array('sectionPress', 'toPress', 'toPressFilterByType');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionPress', 'section', array(
            'label'         => 'Press',
            'property_path' => false,
        ));

        $builder->add('toPress', 'checkbox', array(
            'label'    => 'Press',
            'required' => false
        ));

        $builder->add('toPressFilterByType', 'choice', array(
            'label'     => 'Press Type',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Press_Types'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $press = $em->getRepository('AppDatabaseMainBundle:Press')->findOneById($id);
            if ($press) {
                $press->setIsOptedOut(1);
                $em->persist($press);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($model->toPress) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.email)');
            } else {
                $query->select('p')
                    ->orderBy('p.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:Press', 'p')
                ->andWhere('p.email IS NOT NULL');

            if ($model->toPressFilterByType) {
                $val = $model->toPressFilterByType;
                $query->andWhere('p.type IN (:types)')
                    ->setParameter('types', $val);
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isOptedOut = 0
                $query->andWhere('(p.isOptedOut = 0 OR p.isOptedOut IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having canBeContactedDirectly = 1
            // otherwise
            if (!$model->includeMustNotContactedParties) {
                // only about entries having canBeContactedDirectly = 1
                $query->andWhere('(p.canBeContactedDirectly = 1)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('idCountry', $countries);
            }

            if ($model->filterByLanguages) {
                $languages = $model->filterByLanguages;
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $query->groupBy('p.email');

                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $email = trim($row->getEmail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}