<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

class Constant {
    ## Email Receiver types
    const RECEIVER_TYPE_CUSTOMER                = 10;
    const RECEIVER_TYPE_DISTRIBUTION_CHANNEL    = 20;
    const RECEIVER_TYPE_PARTNER                 = 30;
    const RECEIVER_TYPE_LEAD                    = 40;
    const RECEIVER_TYPE_NEWSLETTER_RECIPIENT    = 50;
    const RECEIVER_TYPE_PREDEFINED_RECIPIENT    = 60;
    const RECEIVER_TYPE_ACQUISITEUR             = 70;
    const RECEIVER_TYPE_PRESS                   = 80;

    public static function getReceiverTypeList() {
        return array(
            self::RECEIVER_TYPE_CUSTOMER                =>  'Customer',
            self::RECEIVER_TYPE_DISTRIBUTION_CHANNEL    =>  'Distribution Channel',
            self::RECEIVER_TYPE_PARTNER                 =>  'Partner',
            self::RECEIVER_TYPE_LEAD                    =>  'Lead',
            self::RECEIVER_TYPE_ACQUISITEUR             =>  'Acquisiteur',
            self::RECEIVER_TYPE_NEWSLETTER_RECIPIENT    =>  'Newsletter Recipient',
            self::RECEIVER_TYPE_PRESS                   =>  'Press',
            self::RECEIVER_TYPE_PREDEFINED_RECIPIENT    =>  'Predefined Recipient'
        );
    }

    const MASS_SENDER_STATUS_OPEN           = 10;
    const MASS_SENDER_STATUS_PROCESSING     = 20;
    const MASS_SENDER_STATUS_DONE           = 30;

    public static function getStatus() {
        return array(
            self::MASS_SENDER_STATUS_OPEN =>            'open',
            self::MASS_SENDER_STATUS_PROCESSING =>      'processing',
            self::MASS_SENDER_STATUS_DONE =>            'done'
        );
    }
}
