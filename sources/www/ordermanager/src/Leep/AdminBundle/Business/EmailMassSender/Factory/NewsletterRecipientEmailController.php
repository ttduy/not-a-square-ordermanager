<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class NewsletterRecipientEmailController extends EmailController {

    public function getInputList() {
        return array('sectionNewsletterRecipient', 'toNewsletterRecipient',
            'toNewsletterRecipientFilterByDistributionChannel', 'toNewsletterRecipientFilterByPartner',
            'toNewsletterRecipientFilterByTimeSinceLastOrder', 'toNewsletterRecipientFilterByDCType');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionNewsletterRecipient', 'section', array(
            'label'         => 'Newsletter Recipients',
            'property_path' => false,
        ));

        $builder->add('toNewsletterRecipient', 'checkbox', array(
            'label'    => 'Newsletter Recipients',
            'required' => false
        ));

        $builder->add('toNewsletterRecipientFilterByDistributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toNewsletterRecipientFilterByPartner', 'choice', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toNewsletterRecipientFilterByTimeSinceLastOrder', 'text', array(
            'label'     => 'Time Since Last Order',
            'required'  => false
        ));

        $builder->add('toNewsletterRecipientFilterByDCType', 'choice', array(
            'label'     => 'Distribution Channel Type',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($id);
            if ($acquisiteur) {
                $acquisiteur->setIsNotContacted(1);
                $em->persist($acquisiteur);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrItems = array();
        $totalRow = 0;

        if ($model->toNewsletterRecipient) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.email)');
            } else {
                $query->select('p')
                    ->orderBy('p.email', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:NewsletterRecipient', 'p')
                ->andWhere('p.email IS NOT NULL');

            if ($model->toNewsletterRecipientFilterByDistributionChannel || $model->toNewsletterRecipientFilterByDCType) {
                $filterByDCs = $model->toNewsletterRecipientFilterByDistributionChannel;
                $filterByDCTypes = $model->toNewsletterRecipientFilterByDCType;

                if ($filterByDCs) {
                    $query->andWhere('p.idDistributionChannel IN (:dcIds)')
                    ->setParameter('dcIds', $filterByDCs);
                }

                if ($filterByDCTypes) {
                    $query->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.idDistributionChannel');

                    $query->andWhere('dc.typeid IN (:dcTypeIds)')
                    ->setParameter('dcTypeIds', $filterByDCTypes);
                }
            }

            if ($model->toNewsletterRecipientFilterByPartner) {
                $query->andWhere('p.idPartner IN (:partnerIds)')
                    ->setParameter('partnerIds', $model->toNewsletterRecipientFilterByPartner);
            }

            if ($model->toNewsletterRecipientFilterByTimeSinceLastOrder) {
                // TO BE IMPLEMENTED LATER
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 0
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$model->includeMustNotContactedParties) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                $query->andWhere('p.idCountry IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($model->filterByLanguages) {
                $languages = $model->filterByLanguages;
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $query->groupBy('p.email');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstName() . ' ' . $row->getSurName());
                        $email = trim($row->getEmail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}