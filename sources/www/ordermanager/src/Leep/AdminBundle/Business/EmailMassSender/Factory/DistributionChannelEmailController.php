<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

class DistributionChannelEmailController extends EmailController {

    public function getInputList() {
        return array('sectionDC', 'toDistributionChannel', 'toDistributionChannelFilterByPartner',
            'toDistributionChannelFilterByAcquisiteur', 'toDistributionChannelFilterByRating',
            'toDistributionChannelFilterByCustomerAmount', 'toDistributionChannelFilterBySoldProducts',
            'toDistributionChannelFilterByNoItemPerSoldProducts', 'toDistributionChannelFilterByIsTopSellSoldProducts',
            'toDistributionChannelFilterByNotSoldProducts', 'toDistributionChannelFilterByWithComplaints',
            'toDistributionChannelFilterByWithFeedbacks');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionDC', 'section', array(
            'label'         => 'Distribution Channel',
            'property_path' => false,
        ));

        $builder->add('toDistributionChannel', 'checkbox', array(
            'label'    => 'Distribution Channel',
            'required' => false
        ));

        $builder->add('toDistributionChannelFilterByPartner', 'choice', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toDistributionChannelFilterByAcquisiteur', 'choice', array(
            'label'     => 'Acquisiteur',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toDistributionChannelFilterByRating', 'choice', array(
            'label'     => 'Rating',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Rating_Levels'),
            'multiple'  => 'multiple'
        ));

        $builder->add('toDistributionChannelFilterByCustomerAmount', 'app_comparator_type_select', array(
            'label'     => '',
            'required'  => false,
            'selectLabel'  => 'Amount of customers'
        ));

        $builder->add('toDistributionChannelFilterBySoldProducts', 'choice', array(
            'label'     => 'Sold Products',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toDistributionChannelFilterByNoItemPerSoldProducts', 'app_comparator_type_select', array(
            'label'     => '',
            'required'  => false,
            'selectLabel'  => 'Sold Product: The number of Items'
        ));

        $builder->add('toDistributionChannelFilterByIsTopSellSoldProducts', 'choice', array(
            'label'     => 'Sold Product: Top Sell',
            'required'  => false,
            'choices'   => array(0 => 'No', 1 => 'Yes'),
            'expanded'  => 'expanded'
        ));

        $builder->add('toDistributionChannelFilterByNotSoldProducts', 'choice', array(
            'label'     => 'NOT-Sold Products',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('toDistributionChannelFilterByWithComplaints', 'checkbox', array(
            'label'    => 'Has Complaints',
            'required' => false
        ));

        $builder->add('toDistributionChannelFilterByWithFeedbacks', 'checkbox', array(
            'label'    => 'Has Feedbacks',
            'required' => false
        ));
    }

    public function unsubscribe($emailList, $id = null) {
        parent::unsubscribe($emailList);

        if ($id) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
            if ($dc) {
                $dc->setIsNotContacted(1);
                $em->persist($dc);
                $em->flush();
            }
        }
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('REGEXP', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Regexp');

        $comparatorTypeFactory = $this->container->get('leep_admin.comparator_type.comparator_type_factory');

        $arrItems = array();
        $totalRow = 0;

        if ($model->toDistributionChannel) {
            $query = $em->createQueryBuilder();

            if ($getTotalOnly) {
                $query->select('count(distinct p.contactemail)');
            } else {
                $query->select('p')
                    ->orderBy('p.contactemail', 'ASC');
            }

            // include more filters here
            $query->from('AppDatabaseMainBundle:DistributionChannels', 'p')
                ->andWhere('p.contactemail IS NOT NULL');

            if ($model->toDistributionChannelFilterByPartner) {
                $query->andWhere('p.partnerid IN (:partnerIds)')
                    ->setParameter('partnerIds', $model->toDistributionChannelFilterByPartner);
            }

            if ($model->toDistributionChannelFilterByAcquisiteur) {
                $query->andWhere('p.acquisiteurid1 IN (:acquisiteurIds) OR p.acquisiteurid2 IN (:acquisiteurIds) OR p.acquisiteurid3 IN (:acquisiteurIds)')
                    ->setParameter('acquisiteurIds', $model->toDistributionChannelFilterByAcquisiteur);
            }

            if ($model->toDistributionChannelFilterByRating) {
                $query->andWhere('p.rating IN (:selectedRatings)')
                    ->setParameter('selectedRatings', $model->toDistributionChannelFilterByRating);
            }

            // Amount of Customers
            if ($comparatorTypeFactory->hasSelectComparatorType($model->toDistributionChannelFilterByCustomerAmount)) {
                $val = $model->toDistributionChannelFilterByCustomerAmount;

                $q = $em->createQueryBuilder();
                $q->select('p.idDistributionChannel')
                    ->from('AppDatabaseMainBundle:CustomerInfo', 'p')
                    ->groupBy('p.idDistributionChannel');

                $comparatorTypeFactory->buildQuery($q, 'COUNT(p.id)', $val, 'having');
                $result = $q->getQuery()->getResult();

                $arrDC = array(0);
                if ($result) {
                    foreach ($result as $entry) {
                        $arrDC[] = $entry['idDistributionChannel'];
                    }
                }

                $query->andWhere('p.id IN (:arrDCByCustomerTotals)')
                    ->setParameter('arrDCByCustomerTotals', $arrDC);
            }

            // sold/not sold Products
            // Ordered Products filter && Not Ordered Products filter
            if ($model->toDistributionChannelFilterBySoldProducts || $model->toDistributionChannelFilterByNotSoldProducts) {
                $soldProducts = $model->toDistributionChannelFilterBySoldProducts;
                $notSoldProducts = $model->toDistributionChannelFilterByNotSoldProducts;
                $soldProductsNoItems = $model->toDistributionChannelFilterByNoItemPerSoldProducts;
                $soldProductsTopSell = $model->toDistributionChannelFilterByIsTopSellSoldProducts;

                if (!empty($soldProducts)) {
                    $query->innerJoin('AppDatabaseMainBundle:DistributionChannelsProductSell', 'ps', 'WITH', 'ps.idDistributionChannel = p.id')
                        ->andWhere('ps.idProduct IN (:soldProductIds)')
                        ->setParameter('soldProductIds', $soldProducts);

                    if ($soldProductsNoItems) {
                        $comparatorTypeFactory->buildQuery($query, 'ps.total', $soldProductsNoItems);
                    }

                    if ($soldProductsTopSell) {
                        $query->andWhere('ps.isTopSell = 1');
                    }
                }

                if (!empty($notSoldProducts)) {
                    $excludedDCIds = array(0);
                    $queryExcludedOrder = $em->createQueryBuilder();
                    $result = $queryExcludedOrder->select('p.distributionchannelid')
                                            ->from('AppDatabaseMainBundle:Customer', 'p')
                                            ->andWhere("REGEXP(CONCAT(CONCAT(',', p.productList), ','), :regexp) = 1")
                                            ->setParameter('regexp', ',' . implode('|', $notSoldProducts) . ',')
                                            ->groupBy('p.distributionchannelid')
                                            ->getQuery()
                                            ->getResult();
                    if ($result) {
                        foreach ($result as $entry) {
                            if ($entry['distributionchannelid']) {
                                $excludedDCIds[] = $entry['distributionchannelid'];
                            }
                        }
                    }

                    $query->andWhere('p.id NOT IN (:notOrderedProductexcludedDCIds)')
                            ->setParameter('notOrderedProductexcludedDCIds', $excludedDCIds);
                }
            }

            if ($model->toDistributionChannelFilterByWithComplaints) {
                $q = $em->createQueryBuilder();
                $result = $q->select('p.idDistributionChannel')
                            ->from('AppDatabaseMainBundle:DistributionChannelsComplaint', 'p')
                            ->groupBy('p.idDistributionChannel')
                            ->getQuery()
                            ->getResult();

                $arrDCWithComplaintIds = array(0);
                if ($result) {
                    foreach ($result as $entry) {
                        $arrDCWithComplaintIds[] = $entry['idDistributionChannel'];
                    }
                }

                $query->andWhere('p.id IN (:arrDCWithComplaintIds)')
                    ->setParameter('arrDCWithComplaintIds', $arrDCWithComplaintIds);
            }

            if ($model->toDistributionChannelFilterByWithFeedbacks) {
                $query->andWhere('p.numberFeedback IS NOT NULL AND p.numberFeedback > 0');
            }

            // if includeOptedOutParties is checked, include further entries having isNotContacted = 1
            // otherwise
            if (!$model->includeOptedOutParties) {
                // only about entries having isNotContacted = 0
                $query->andWhere('(p.isNotContacted = 0 OR p.isNotContacted IS NULL)');
            }

            // if includeMustNotContactedParties is checked, include further entries having isCustomerBeContacted = 0
            // otherwise
            if (!$model->includeMustNotContactedParties) {
                // only about entries having isCustomerBeContacted = 1
                $query->andWhere('(p.isCustomerBeContacted = 1)');
            }

            if ($model->filterByCountries) {
                $countries = $model->filterByCountries;
                //$countries = explode(',', $model->filterByCountries']);
                $query->andWhere('p.countryid IN (:countries)')
                    ->setParameter('countries', $countries);
            }

            if ($model->filterByLanguages) {
                $languages = $model->filterByLanguages;
                $query->andWhere('p.idLanguage IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            if ($getTotalOnly) {
                $totalRow = $query->getQuery()->getSingleScalarResult();
            } else {
                $query->groupBy('p.id');
                $query->groupBy('p.contactemail');
                $result = $query->getQuery()->getResult();

                if ($result) {
                    foreach ($result as $row) {
                        $name = trim($row->getFirstname() . ' ' . $row->getSurname());
                        $email = trim($row->getContactemail());
                        $id = $row->getId();
                        if (!empty($email)) {
                            $arrItems[] = array(
                                'name' =>   $name,
                                'email' =>  $email,
                                'id' =>     $id
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? $totalRow : $arrItems);
    }
}