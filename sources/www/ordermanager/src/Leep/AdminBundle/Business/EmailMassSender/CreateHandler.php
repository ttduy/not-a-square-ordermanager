<?php
namespace Leep\AdminBundle\Business\EmailMassSender;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\EmailOutgoing\Constant as OutgoingConstant;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $this->buildLimitationSection($builder, $mapping);
        $this->buildRecipientsSection($builder, $mapping);
        $this->buildEmailSection($builder, $mapping);

        return $builder;
    }

    private function buildRecipientsSection($builder, $mapping) {
        $builder->add('sectionPrecipients', 'section', array(
            'label'    => 'Precipients',
            'property_path' => false,
            'attr' => array('is_tab' => true)
        ));

        $recipientsFactory = $this->container->get('leep_admin.email_mass_sender.business.recipients_factory');
        foreach (Constant::getReceiverTypeList() as $receiverType => $receiverName) {
            $recipientsFactory->build($receiverType, $builder, $mapping);
        }
    }

    private function buildLimitationSection($builder, $mapping) {
        $builder->add('sectionLimitation', 'section', array(
            'label'    => 'Limitation?',
            'property_path' => false
        ));

        $builder->add('includeOptedOutParties', 'checkbox', array(
            'label'    => 'Include opted-out party',
            'required' => false
        ));

        $builder->add('includeMustNotContactedParties', 'checkbox', array(
            'label'    => 'Include not-contacted party',
            'required' => false
        ));

        $builder->add('filterByCountries', 'choice', array(
            'label'    => 'Filter by countries',
            'multiple' => 'multiple',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List'),
            'attr'     => array('style' => 'height:300px')
        ));

        $builder->add('filterByLanguages', 'choice', array(
            'label'    => 'Filter by languages',
            'multiple' => 'multiple',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'attr'     => array('style' => 'height:300px')
        ));
    }

    private function buildEmailSection($builder, $mapping) {
        $builder->add('sectionEmail', 'section', array(
            'label'    => 'Email',
            'property_path' => false
        ));

        $builder->add('idQueue', 'choice', array(
            'label'    => 'Queue',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Queue')
        ));

        $builder->add('idEmailMethod', 'choice', array(
            'label'    => 'Email Method',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('body', 'app_text_editor', array(
            'label'    => 'Content',
            'required' => false
        ));

        $builder->add('contentHtml', 'textarea', array(
            'label'    => 'Content (HTML)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width:100%',
                'rows'   => 10
            )
        ));

        $builder->add('isSendNow', 'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment(s)',
            'property_path' => false
        ));

        $builder->add('attachments', 'container_collection', array(
            'type' =>       $this->container->get('leep_admin.form_types.app_file_uploader'),
            'label' =>      'Attachments',
            'required' =>   false,
            'options'  =>   array(
                'path'     => OutgoingConstant::EMAIL_ATTACHMENT_FOLDER
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // Create Mass Sender Object
        $massSender = new Entity\EmailMassSender();
        $massSender->setCreatedDate(new \Datetime());
        $massSender->setTitle($model->title);
        $massSender->setNumSending(0);
        $massSender->setNumSent(0);
        $massSender->setNumError(0);
        $massSender->setNumRead(0);
        $massSender->setStatus(Constant::MASS_SENDER_STATUS_OPEN);

        // Get Recipients List
        $recipientsFactory = $this->container->get('leep_admin.email_mass_sender.business.recipients_factory');
        $recipients = array();
        $numRecipients = 0;
        foreach (Constant::getReceiverTypeList() as $receiverType => $receiverName) {
            $result = $recipientsFactory->grab($receiverType, $model);
            $recipients[] = array(
                'type' => $receiverType,
                'email' => $result
            );

            $numRecipients += count($result);
        }

        $massSender->setNumOpen($numRecipients);

        if ($model->isSendNow) {
            $massSender->setNumSendNow($numRecipients);
        } else {
            $massSender->setNumSendNow(0);
        }

        $em->persist($massSender);
        $em->flush();

        // Move attachments into folder
        $attachments_data = [];

        if ($model->attachments) {
            foreach ($model->attachments as $fileUpload) {
                if ($fileUpload['attachment']) {
                    $attachmentDir = $this->container->getParameter('files_dir') . '/'. OutgoingConstant::EMAIL_ATTACHMENT_FOLDER;
                    $extension = pathinfo($fileUpload['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
                    $name = uniqid('email_', true) . ($extension ? '.' . $extension : '');
                    $fileUpload['attachment']->move($attachmentDir, $name);
                    $attachments_data[] = [
                        'name' =>       $name,
                        'original' =>   $fileUpload['attachment']->getClientOriginalName()
                    ];
                }
            }
        }

        // With each recipient, create an out-going email
        $count = 0;
        foreach ($recipients as $recipient) {
            foreach ($recipient['email'] as $recipientData) {
                $recipientName = $recipientData['name'];
                $recipientEmail = $recipientData['email'];
                $recipientId = $recipientData['id'];

                $emailOutgoing = new Entity\EmailOutgoing();
                $emailOutgoing->setCreationDatetime(new \DateTime());
                $emailOutgoing->setIdMassSender($massSender->getId());
                $emailOutgoing->setIdQueue($model->idQueue);
                $emailOutgoing->setIdEmailMethod($model->idEmailMethod);
                $emailOutgoing->setTitle($model->title);
                $emailOutgoing->setRecipientsType($recipient['type']);
                $emailOutgoing->setRecipientsId($recipientId);

                if (!empty($model->contentHtml)) {
                    $emailOutgoing->setBody($model->contentHtml);
                } else {
                    $emailOutgoing->setBody($model->body);
                }

                $emailOutgoing->setIsSendNow($model->isSendNow);
                $emailOutgoing->setStatus(OutgoingConstant::EMAIL_STATUS_OPEN);
                $emailOutgoing->setRecipients("$recipientName <$recipientEmail>");
                $em->persist($emailOutgoing);
                $em->flush();

                $emailOutgoingAttachments = [];

                // attachments
                if (!empty($attachments_data)) {
                    foreach ($attachments_data as $attachment) {
                        $emailOutgoingAttachment = new Entity\EmailOutgoingAttachment();
                        $emailOutgoingAttachment->setIdEmailOutgoing($emailOutgoing->getId());
                        $emailOutgoingAttachment->setName($attachment['name']);
                        $emailOutgoingAttachment->setOriginalName($attachment['original']);
                        $em->persist($emailOutgoingAttachment);
                    }
                }

                $em->flush();
                if ($count++ >= 100) {
                    $em->clear();
                    $count = 0;
                }
            }
        }

        $em->flush();
        parent::onSuccess();
    }
}
