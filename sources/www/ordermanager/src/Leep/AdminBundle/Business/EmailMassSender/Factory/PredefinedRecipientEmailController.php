<?php
namespace Leep\AdminBundle\Business\EmailMassSender\Factory;

use Leep\AdminBundle\Business\EmailMassSender\Util;

class PredefinedRecipientEmailController extends EmailController {

    public function getInputList() {
        return array('sectionPredefinedList', 'toPredefinedList');
    }

    public function build($builder, $mapping) {
        $builder->add('sectionPredefinedList', 'section', array(
            'label'         => 'Predefined List',
            'property_path' => false,
        ));

        $builder->add('toPredefinedList', 'textarea', array(
            'label'     => 'Predefined List',
            'required'  => false,
            'attr'      => array(
                    'style' => 'width: 300px; height: 100px'
                )
        ));
    }

    public function grab($model, $getTotalOnly = false) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $request = $this->container->get('request');

        $arrItems = array();
        if ($model->toPredefinedList) {
            $rows = explode("\n", $model->toPredefinedList);
            $emailList = array();
            foreach ($rows as $row) {
                if (trim($row) != '') {
                    $recipients = Util::extractRecipient($row);
                    foreach ($recipients as $recipient) {
                        foreach ($recipient['email'] as $key => $recipientEmail) {
                            $index = array_search($recipientEmail, $emailList);

                            if ($index !== false) {
                                unset($recipient['email'][$key]);
                            } else {
                                $emailList[] = $recipientEmail;
                            }
                        }

                        if (count($recipient['email']) > 0) {
                            $arrItems[] = array(
                                'name' =>   $recipient['name'],
                                'email' =>  implode('; ', $recipient['email']),
                                'id' =>     null
                            );
                        }
                    }
                }
            }
        }

        return ($getTotalOnly ? sizeof($arrItems) : $arrItems);
    }
}