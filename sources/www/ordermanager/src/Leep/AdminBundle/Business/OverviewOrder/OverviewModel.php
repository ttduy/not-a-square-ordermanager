<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

class OverviewModel {
    // Tab's properties
    public $properties = array();

    public function __set($name, $value = null) {
        if ($name == 'currentTab' && empty($value)) {
            return;
        }

        $this->properties[$name] = $value;
    }

    public function __get($name) {
        if (!array_key_exists($name, $this->properties)) {
            $this->properties[$name] = null;
        }

        return $this->properties[$name];
    }

    public function __isset($name) {
        return array_key_exists($name, $this->properties);
    }
}
