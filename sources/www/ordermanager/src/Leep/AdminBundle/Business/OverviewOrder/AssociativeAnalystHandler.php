<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class AssociativeAnalystHandler {

    static public function getFieldsName() {
        return [// 'associativeAnalyst_dateFrom', 'associativeAnalyst_dateTo',
            'associativeAnalyst_productA', 'associativeAnalyst_productB'];
    }

    static public function buildForm($builder, $mapping) {
        // $builder->add('associativeAnalyst_dateFrom', 'datepicker', array(
        //     'label'    => 'From Date',
        //     'required' => false
        // ));

        // $builder->add('associativeAnalyst_dateTo', 'datepicker', array(
        //     'label'    => 'To Date',
        //     'required' => false
        // ));

        $builder->add('associativeAnalyst_productA', 'choice', array(
            'label'    => 'Product A',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        $builder->add('associativeAnalyst_productB', 'choice', array(
            'label'    => 'Product B',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));
        return $builder;
    }

    static public function onSuccess($container, $model) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $productAList = $model->associativeAnalyst_productA;
        $productBList = $model->associativeAnalyst_productB;
        if (empty($productAList) || empty($productBList)) {
            return false;
        }

        $opt = [];
        foreach ($productAList as $productId) {
            $opt[] = 'FIND_IN_SET('.$productId.', p.productList) > 0';
        }

        $query = $em->createQueryBuilder();
        $query->select('p');
        $query->from('AppDatabaseMainBundle:Customer', 'p');
        $orders = $em->getRepository('AppDatabaseMainBundle:Customer')
                ->createQueryBuilder('p')
                ->where("(".implode(' OR ', $opt).")")
                ->getQuery()
                ->getResult();

        $customers = [];
        foreach ($orders as $order) {
            $firstName = $order->getFirstName();
            $surName = $order->getSurName();
            $birthday = $order->getDateOfBirth();
            $dateOrder = $order->getDateOrdered();

            if ($birthday) {
                $birthday = $formatter->format($birthday, 'date');
            }

            $customerKey = "$firstName $surName ($birthday)";
            if (!array_key_exists($customerKey, $customers))  {
                $customers[$customerKey] = [
                    'A' =>          [],
                    'B' =>          [],
                    'AMinDate' =>   $dateOrder,
                    'AMaxDate' =>   $dateOrder
                ];
            }

            $customers[$customerKey]['A'][] = [
                'date' => $dateOrder
            ];

            if ($dateOrder) {
                if (!$customers[$customerKey]['AMinDate'] ||
                    $dateOrder->getTimestamp() < $customers[$customerKey]['AMinDate']->getTimestamp()) {
                    $customers[$customerKey]['AMinDate'] = $dateOrder;
                } else if (
                    !$customers[$customerKey]['AMaxDate'] ||
                    $dateOrder->getTimestamp() > $customers[$customerKey]['AMaxDate']->getTimestamp()) {
                    $customers[$customerKey]['AMaxDate'] = $dateOrder;
                }
            }
        }


        //------------------------------------------------------------------------------------------------------
        $opt = [];
        foreach ($productBList as $productId) {
            $opt[] = 'FIND_IN_SET('.$productId.', p.productList) > 0';
        }

        $query = $em->createQueryBuilder();
        $query->select('p');
        $query->from('AppDatabaseMainBundle:Customer', 'p');
        $orders = $em->getRepository('AppDatabaseMainBundle:Customer')
                ->createQueryBuilder('p')
                ->where("(".implode(' OR ', $opt).")")
                ->getQuery()
                ->getResult();

        $result = [];
        foreach ($orders as $order) {
            $firstName = $order->getFirstName();
            $surName = $order->getSurName();
            $birthday = $order->getDateOfBirth();
            $dateOrder = $order->getDateOrdered();

            if ($birthday) {
                $birthday = $formatter->format($birthday, 'date');
            }

            $customerKey = "$firstName $surName ($birthday)";

            if (!array_key_exists($customerKey, $customers)) {
                continue;
            }

            if ($dateOrder) {
                if ((!$customers[$customerKey]['AMinDate'] ||
                    $dateOrder->getTimestamp() > $customers[$customerKey]['AMinDate']->getTimestamp()) &&
                    !array_key_exists($customerKey, $result)) {
                    $result[$customerKey] = [
                        'name' =>       "$firstName $surName",
                        'birthday' =>   $birthday,
                        'AMaxDate' =>   ($customers[$customerKey]['AMaxDate'] ?
                                            $formatter->format($customers[$customerKey]['AMaxDate'], 'date') :
                                            'Unknown'),
                        'BMaxDate' =>   $dateOrder
                    ];
                }
            }

            if (array_key_exists($customerKey, $result) && $dateOrder &&
                (!$result[$customerKey]['BMaxDate'] || $dateOrder->getTimestamp() > $result[$customerKey]['BMaxDate']->getTimestamp())) {
                $result[$customerKey]['BMaxDate'] = $dateOrder;
            }
        }

        foreach ($result as $key => $customer) {
            if ($result[$key]['BMaxDate']) {
                $result[$key]['BMaxDate'] = $formatter->format($customer['BMaxDate'], 'date');
            } else {
                $result[$key]['BMaxDate'] = 'Unknown';
            }
        }

        $associativeAnalystResult = [
            'customers' =>  $result,
            'numA' =>       count($customers),
            'numB' =>       count($result),
            'percent' =>    number_format(count($result) / count($customers) * 100, 2)
        ];

        $model->resultType = 'associative-analyst';
        $model->result = $associativeAnalystResult;
    }
}
