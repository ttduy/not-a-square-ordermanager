<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class OverviewHandler extends AppEditHandler {

    public function loadEntity($request) {
        return false;
    }

    public function convertToFormModel($entity) {
        $model = new OverviewModel();
        $model->currentTab = array_keys($this->getTabHandlers())[0];
        $model->tabs = $this->getTabHandlers();
        return $model;
    }

    public function getTabHandlers() {
        return [
            'OverviewOrder' => [
                'title' =>      'Order Overview',
                'handler' =>    'OverviewOrderHandler'
            ],

            'ProductAnalyst' => [
                'title' =>      'Product Analyst',
                'handler' =>    'ProductAnalystHandler'
            ],

            'AssociativeAnalyst' => [
                'title' =>      'Associative Analyst',
                'handler' =>    'AssociativeAnalystHandler'
            ]
        ];
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $tabs = $this->getTabHandlers();
        foreach ($tabs as $tab) {
            $handler = $tab['handler'];
            $builder = call_user_func_array(
                "\Leep\AdminBundle\Business\OverviewOrder\\$handler::buildForm",
                [$builder, $mapping]);
        }

        $builder->add('currentTab', 'hidden', array(
            'label'    => 'Current Tab',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        if (!empty($model->currentTab)) {
            $handler = $model->currentTab.'Handler';
            $result = call_user_func_array(
                    "\Leep\AdminBundle\Business\OverviewOrder\\$handler::onSuccess",
                     [$this->container, $model]);

            if ($result === false) {
                $this->errors[] = "Error: Some required field is empty.";
            }
        }
    }
}
