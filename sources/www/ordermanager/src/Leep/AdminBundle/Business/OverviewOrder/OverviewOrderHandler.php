<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class OverviewOrderHandler {

    static public function getFieldsName() {
        return ['overviewProduct_dateFrom', 'overviewProduct_dateTo', 'overviewProduct_mustBeProduct'];
    }

    static public function buildForm($builder, $mapping) {
        $builder->add('overviewProduct_dateFrom', 'datepicker', array(
            'label'    => 'From Date',
            'required' => false
        ));

        $builder->add('overviewProduct_dateTo', 'datepicker', array(
            'label'    => 'To Date',
            'required' => false
        ));

        $builder->add('overviewProduct_mustBeProduct', 'choice', array(
            'label'    => 'Must be product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        return $builder;
    }

    static public function onSuccess($container, $model) {
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $productList = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findBy([], ['name' => 'ASC']);
        $productOverviewList = [];
        $mustBeProduct = [];
        foreach ($productList as $product) {
            $productOverviewList[$product->getId()] = [
                'name' =>       $product->getName(),
                'numOrder' =>   0,
                'visible' =>    false
            ];

            $mustBeProduct[] = $product->getId();
        }

        if (!empty($model->overviewProduct_mustBeProduct)) {
            $mustBeProduct = $model->overviewProduct_mustBeProduct;
        }

        foreach ($mustBeProduct as $idProduct) {
            $query = $em->createQueryBuilder();
            $query->select('count(p.id)');
            $query->from('AppDatabaseMainBundle:Customer','p');

            if (!empty($model->overviewProduct_dateFrom)) {
                $query->andWhere('p.dateordered >= :dateFrom')
                    ->setParameter('dateFrom', $model->overviewProduct_dateFrom);
            }

            if (!empty($model->overviewProduct_dateTo)) {
                $query->andWhere('p.dateordered <= :dateTo')
                    ->setParameter('dateTo', $model->overviewProduct_dateTo);
            }

            $query->andWhere('FIND_IN_SET('.$idProduct.', p.productList) > 0');
            $count = $query->getQuery()->getSingleScalarResult();

            $productOverviewList[$idProduct]['numOrder'] = $count;
            $productOverviewList[$idProduct]['visible'] = true;
        }

        $model->resultType = 'overview-product';
        $model->result = $productOverviewList;
    }
}
