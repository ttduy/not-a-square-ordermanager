<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class ProductAnalystHandler {

    static public function getFieldsName() {
        return [// 'productAnalyst_dateForm', 'productAnalyst_dateTo',
            'productAnalyst_products'];
    }

    static public function buildForm($builder, $mapping) {
        // $builder->add('productAnalyst_dateForm', 'datepicker', array(
        //     'label'    => 'From Date',
        //     'required' => false
        // ));

        // $builder->add('productAnalyst_dateTo', 'datepicker', array(
        //     'label'    => 'To Date',
        //     'required' => false
        // ));

        $builder->add('productAnalyst_products', 'choice', array(
            'label'    => 'Products',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));
        return $builder;
    }

    static public function onSuccess($container, $model) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $productList = $model->productAnalyst_products;
        if (empty($productList)) {
            return false;
        }

        $opt = [];
        foreach ($productList as $productId) {
            $opt[] = 'FIND_IN_SET('.$productId.', p.productList) > 0';
        }

        $query = $em->createQueryBuilder();
        $query->select('p');
        $query->from('AppDatabaseMainBundle:Customer', 'p');
        $orders = $em->getRepository('AppDatabaseMainBundle:Customer')
                ->createQueryBuilder('p')
                ->where("(".implode(' OR ', $opt).")")
                ->orderBy('p.firstname', 'ASC')
                ->getQuery()
                ->iterate();

        $allProductList = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findBy([], ['name' => 'ASC']);

        $products = [];
        foreach ($allProductList as $product) {
            if (in_array($product->getId(), $productList)) {
                $products[$product->getId()] = [
                    'name' =>       $product->getName(),
                    'numOrder' =>   0,
                    'customers' =>  [],
                    'customerOrderMoreThanTwice' => [],
                    'customerOrderMoreThanThrice' => [],
                ];
            }
        }

        $customers = [];
        foreach ($orders as $order) {
            $order = $order[0];
            $firstName = $order->getFirstName();
            $surName = $order->getSurName();
            $birthday = $order->getDateOfBirth();
            $dateOrder = $order->getDateOrdered();

            if ($birthday) {
                $birthday = $formatter->format($birthday, 'date');
            }

            $customerKey = "$firstName $surName ($birthday)";
            if (!array_key_exists($customerKey, $customers))  {
                $customers[$customerKey] = [
                    'name' =>       "$firstName $surName",
                    'birthday' =>   $birthday,
                    'orders' =>     []
                ];
            }

            if ($dateOrder) {
                $customers[$customerKey]['orders'][$dateOrder->getTimestamp()] = [
                    'orderNumber' =>    $order->getOrdernumber(),
                    'dateOrdered' =>    $dateOrder,
                    'products' =>       []
                ];
            } else {
                $customers[$customerKey]['orders'][-1 * $order->getId()] = [
                    'orderNumber' =>    $order->getOrdernumber(),
                    'dateOrdered' =>    null,
                    'products' =>       []
                ];
            }

            $prodcutIdList = array_map('intval', array_map('trim', explode(',', $order->getProductList())));
            $prodcutIdList = array_unique($prodcutIdList);
            foreach ($prodcutIdList as $productId) {
                if (array_key_exists($productId, $products)) {
                    $products[$productId]['numOrder']++;

                    if ($dateOrder) {
                        $customers[$customerKey]['orders'][$dateOrder->getTimestamp()]['products'][] =
                            $products[$productId]['name'];
                    } else {
                        $customers[$customerKey]['orders'][-1 * $order->getId()]['products'][] =
                            $products[$productId]['name'];
                    }

                    if (in_array($customerKey, $products[$productId]['customerOrderMoreThanTwice']) &&
                        !in_array($customerKey, $products[$productId]['customerOrderMoreThanThrice'])) {
                        $products[$productId]['customerOrderMoreThanThrice'][] = $customerKey;
                    } else if(in_array($customerKey, $products[$productId]['customers']) &&
                        !in_array($customerKey, $products[$productId]['customerOrderMoreThanTwice'])) {
                        $products[$productId]['customerOrderMoreThanTwice'][] = $customerKey;
                    } else if (!in_array($customerKey, $products[$productId]['customers'])) {
                        $products[$productId]['customers'][] = $customerKey;
                    }
                }
            }

            $em->detach($order);
        }

        $total = [
            'totalOrder' => 0,
            'totalCustomers' => 0,
            'totalCustomerOrderMoreThanTwice' => 0,
            'totalCustomerOrderMoreThanThrice' => 0,
        ];

        foreach ($products as $key => $product) {
            $total['totalOrder'] += $product['numOrder'];

            $products[$key]['numberCustomers'] = count($product['customers']);
            $total['totalCustomers'] += count($product['customers']);

            $products[$key]['numberCustomerOrderMoreThanTwice'] = count($product['customerOrderMoreThanTwice']);
            $total['totalCustomerOrderMoreThanTwice'] += count($product['customerOrderMoreThanTwice']);

            $products[$key]['numberCustomerOrderMoreThanThrice'] = count($product['customerOrderMoreThanThrice']);
            $total['totalCustomerOrderMoreThanThrice'] += count($product['customerOrderMoreThanThrice']);
        }

        foreach ($customers as $key => $customer) {

            ksort($customers[$key]['orders']);
            $firstOrder = reset($customers[$key]['orders']);
            $prevOrderedDate = null;
            foreach ($customers[$key]['orders'] as $orderedTimestamp => $order) {
                $customers[$key]['orders'][$orderedTimestamp]['products'] = implode(', ', $order['products']);

                if ($orderedTimestamp > 0) {
                    if (!$prevOrderedDate) {
                        $prevOrderedDate = $order['dateOrdered'];
                    }

                    $age = intval(($orderedTimestamp - $prevOrderedDate->getTimestamp()) / 86400);
                    $customers[$key]['orders'][$orderedTimestamp]['orderedDateInfo'] =
                        $formatter->format($order['dateOrdered'], 'date');
                    $customers[$key]['orders'][$orderedTimestamp]['orderedDateDiff'] = $age."d";
                } else {
                    $customers[$key]['orders'][$orderedTimestamp]['orderedDateInfo'] = 'Unknown';
                    $customers[$key]['orders'][$orderedTimestamp]['orderedDateDiff'] = '';
                }

                if (!$order['dateOrdered']) {
                    $prevOrderedDate = $order['dateOrdered'];
                }
            }
        }

        $model->resultType = 'product-analyst';
        $model->result = [
            'products' =>   $products,
            'total' =>      $total,
            'customers' =>  $customers
        ];
    }
}
