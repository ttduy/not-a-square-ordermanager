<?php
namespace Leep\AdminBundle\Business\OverviewOrder;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business\EmailOutgoing\Constant;

class OverviewHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $model = $data['form']['view']->vars['value'];
        $data['resultType'] = $model->resultType;
        $data['result'] = $model->result;
        $data['tabs'] = $model->tabs;
        $data['currentTab'] = $model->currentTab;

        $tabInputs = [];
        foreach ($model->tabs as $key => $tab) {
            $handler = $tab['handler'];
            $tabInputs[$key] = call_user_func(
                "\Leep\AdminBundle\Business\OverviewOrder\\$handler::getFieldsName");
        }

        $data['tabInputs'] = $tabInputs;
    }
}
