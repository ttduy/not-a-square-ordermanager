<?php
namespace Leep\AdminBundle\Business\OrderPending;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ViewHandler extends AppViewHandler {
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $id = $this->container->get('request')->query->get('id', 0);
        $orderPending = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderPending')->findOneById($id);

        $data[] = $this->addSection('Contact Information');
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $orderPending->getContactIdGender()));
        $data[] = $this->add('Date of birth', ($orderPending->getContactDateOfBirth() ? $orderPending->getContactDateOfBirth()->format('d/m/Y') : ''));
        $data[] = $this->add('Title', $orderPending->getContactTitle());
        $data[] = $this->add('First name', $orderPending->getContactFirstName());
        $data[] = $this->add('Surname', $orderPending->getContactSurName());
        $data[] = $this->add('Previous Order Number', $orderPending->getPreviousOrderNumber());

        $data[] = $this->addSection('Order Information');
        $data[] = $this->add('External barcode', $orderPending->getOrderExternalBarcode());
        $data[] = $this->add('Info', $orderPending->getOrderInfo());

        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $orderPending->getAddressStreet());
        $data[] = $this->add('Street 2', $orderPending->getAddressStreet2());
        $data[] = $this->add('Post code', $orderPending->getAddressPostCode());
        $data[] = $this->add('City', $orderPending->getAddressCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $orderPending->getAddressIdCountry()));
        $data[] = $this->add('Email', $orderPending->getAddressEmail());
        $data[] = $this->add('Telephone', $orderPending->getAddressTelephone());
        $data[] = $this->add('Fax', $orderPending->getAddressFax());
        $data[] = $this->add('Note', $orderPending->getAddressNote());

        $data[] = $this->addSection('Setting');
        $data[] = $this->add('Language', $mapping->getMappingTitle('LeepAdmin_Customer_Language', $orderPending->getSettingIdLanguage()));
        $data[] = $this->add('Report Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportGoesTo', $orderPending->getSettingReportGoesTo()));
        // $data[] = $this->add('Report Delivery', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportDelivery', $orderPending->getSettingReportDelivery()));
        $data[] = $this->add('NutriMe Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_NutrimeGoesTo', $orderPending->getSettingNutrimeGoesTo()));

        $data[] = $this->addSection('Report Delivery');
        $data[] = $this->add('Download Access', $orderPending->getIsReportDeliveryDownloadAccess() ? "Yes" : "No");
        $data[] = $this->add('Printed Booklet', $orderPending->getIsReportDeliveryPrintedBooklet() ? "Yes" : "No");

        $data[] = $this->addSection('Products');
        $data[] = $this->add('Ordered Categories', $this->buildCategories($orderPending));
        $data[] = $this->add('Ordered Products', $this->buildProducts($orderPending));
        $data[] = $this->add('Ordered Special Products', $this->buildSpecialProducts($orderPending));

        $data[] = $this->addSection('Questions');
        $data[] = $this->add('Questions', $this->buildQuestions($orderPending));

        return $data;
    }

    private function buildCategories($orderPending) {
        $content = '';
        $categories = $orderPending->getOrderedCategories();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        if ($categories) {
            $categories = explode(',', $categories);
            foreach ($categories as $categoryId) {
                if ($content) {
                    $content .= ", ";
                }
                $content .= $formatter->format($categoryId, 'mapping', 'LeepAdmin_Category_List');
            }
        }

        return $content;
    }

    private function buildProducts($orderPending) {
        $content = '';
        $products = $orderPending->getOrderedProducts();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        if ($products) {
            $products = explode(',', $products);
            foreach ($products as $productId) {
                if ($content) {
                    $content .= ", ";
                }
                $content .= $formatter->format($productId, 'mapping', 'LeepAdmin_Product_List');
            }
        }

        return $content;
    }

    private function buildSpecialProducts($orderPending) {
        $content = '';
        $products = $orderPending->getOrderedSpecialProducts();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        if ($products) {
            $products = explode(',', $products);
            foreach ($products as $productId) {
                if ($content) {
                    $content .= ", ";
                }
                $content .= $formatter->format($productId, 'mapping', 'LeepAdmin_SpecialProduct_List');
            }
        }

        return $content;
    }

    private function buildQuestions($orderPending) {
        $formQuestions = Business\Customer\Util::getFormQuestions($this->container);

        $formQuestionTypes = array();
        foreach ($formQuestions as $formQuestion) {
            $formQuestionTypes[$formQuestion['id']] = $formQuestion['idType'];
        }


        $content = '';
        $questions = json_decode($orderPending->getOrderedQuestions(), true);

        if ($questions) {
            foreach ($questions as $questionAnswerPair) {
                if ($content) {
                    $content .= "<br/> ";
                }
                $formQuestion = $formQuestions[$questionAnswerPair['id']];
                $lblQuestion = $formQuestion['question'];
                $answer = Business\FormQuestion\Util::decodeAnswer($formQuestion['idType'], $questionAnswerPair['answer']);

                if (!is_array($answer)) {
                    $answer = array($answer);
                }

                $arrAnswers = array();
                foreach ($answer as $eachOne) {
                    $finalAnswer = $eachOne;
                    if (array_key_exists($eachOne, $formQuestion['choices'])) {
                        $finalAnswer = $formQuestion['choices'][$eachOne];
                    }

                    $arrAnswers[] = $finalAnswer;
                }

                $content .= sprintf('%s : %s', $lblQuestion, implode(' | ', $arrAnswers));
            }
        }

        return $content;
    }
}
