<?php
namespace Leep\AdminBundle\Business\OrderPending;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('contactName', 'text', array(
            'label'       => 'Customer mame',
            'required'    => false
        ));

        $builder->add('idDistributionChannel', 'choice', array(
            'label'       => 'Distribution Channel',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));

        return $builder;
    }
}
