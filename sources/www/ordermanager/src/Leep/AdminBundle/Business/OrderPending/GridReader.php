<?php
namespace Leep\AdminBundle\Business\OrderPending;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;

    public function getColumnMapping() {
        return array('distributionChannel', 'uniqueOrderCode', 'orderExternalBarcode', 'customer', 'orderInfo', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Distribution Channel',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Unique order code',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'External Barcode',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Customer',                    'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Order Info',                  'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.contactTitle, p.contactFirstName, p.contactSurName, p.orderExternalBarcode, p.orderInfo, 
                    dc.distributionchannel dcName')
            ->from('AppDatabaseMainBundle:OrderPending', 'p')
            ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'dc.id = p.idDistributionChannel');

        if (trim($this->filters->contactName) != '') {
            $queryBuilder->andWhere('p.contactFirstName LIKE :contactName OR p.contactSurName LIKE :contactName')
                ->setParameter('contactName', '%'.trim($this->filters->contactName).'%');
        }

        if ($this->filters->idDistributionChannel) {
            $queryBuilder->andWhere('p.idDistributionChannel = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $this->filters->idDistributionChannel);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'order_pending', 'view', 'view', array('id' => $row['id'])), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productModify')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'order_pending', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
            $builder->addButton('Create', $mgr->getUrl('leep_admin', 'customer', 'create', 'create', array('id_pending_order' => $row['id'])), 'button-add');
        }

        return $builder->getHtml();
    }

    public function buildCellCustomer($row) {
        $arrInfo = array();
        if ($row['contactTitle']) {
            $arrInfo[] = $row['contactTitle'];
        }

        if ($row['contactFirstName']) {
            $arrInfo[] = $row['contactFirstName'];
        }

        if ($row['contactSurName']) {
            $arrInfo[] = $row['contactSurName'];
        }

        return implode(' ', $arrInfo);
    }

    public function buildCellDistributionChannel($row) {
        return $row['dcName'];
    }

    public function buildCellOrderInfo($row) {
        return $row['orderInfo'];
    }

    public function buildCellOrderExternalBarcode($row) {
        return $row['orderExternalBarcode'];
    }
}
