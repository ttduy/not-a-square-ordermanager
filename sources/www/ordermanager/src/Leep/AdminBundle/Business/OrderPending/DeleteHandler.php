<?php
namespace Leep\AdminBundle\Business\OrderPending;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $listId = explode(',', $id);
        $listId = array_filter($listId);
        $listId = array_map('intval', $listId);
        foreach ($listId as $idOrderPending) {
            $orderPending = $doctrine->getRepository('AppDatabaseMainBundle:OrderPending')->findOneById($idOrderPending);
            $em->remove($orderPending);
            $em->flush();
        }

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'order_pending', 'grid', 'list', array(
            'id' => $this->container->get('request')->query->get('id', 0)
        ));

        return new RedirectResponse($url);
    }
}