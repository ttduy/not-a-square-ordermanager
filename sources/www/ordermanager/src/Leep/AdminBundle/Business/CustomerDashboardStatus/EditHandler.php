<?php
namespace Leep\AdminBundle\Business\CustomerDashboardStatus;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;


class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) { 
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $statuses = $query->select('p')
                        ->from('AppDatabaseMainBundle:CustomerDashboardStatus', 'p')
                        ->orderBy('p.sortOrder', 'ASC')
                        ->getQuery()
                        ->getResult();

        $arrIdStatus = array();
        foreach($statuses as $status) {
            $arrIdStatus[] = array('id' => $status->getSortOrder(), 'status' => $status->getIdStatus());
        }

        return array('statusList' => $arrIdStatus);
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('statusList', 'collection', array(
            'label' => '',
            'type' => new FormType\AppCustomerDashboardStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Section',
                'required' => false
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // delete all existing statuses
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:CustomerDashboardStatus', 'p')
            ->getQuery()
            ->execute();

        if ($model['statusList']) {
            foreach ($model['statusList'] as $status) {
                // prevent empty status
                if ($status) {
                    $newStatus = new Entity\CustomerDashboardStatus();
                    $newStatus->setIdStatus($status['status']);
                    $newStatus->setSortOrder($status['id']);
                    $em->persist($newStatus);
                    $em->flush();                   
                }
            }
        }

        $this->reloadForm();
    }
}