<?php 
namespace Leep\AdminBundle\Business\Notification;

class Constant {
    const NOTIFICATION_TOPIC_DEFAULT                = 10;
    const ACQUISITEUR_COMMISION_DECAY               = 20;
    const LEAD_REMINDER                             = 30;
    const LEAD_SUBMISSION_BY_CUSTOMER               = 40;
    
    public static function getTopicList() {
        return array(
            self::NOTIFICATION_TOPIC_DEFAULT                    => 'Default Topic',
            self::ACQUISITEUR_COMMISION_DECAY                   => 'Acquisiteur Commision Decay',
            self::LEAD_REMINDER                                 => 'Lead Reminder',
            self::LEAD_SUBMISSION_BY_CUSTOMER                   => 'New Lead Submitted by Customer'
        );
    }
}
