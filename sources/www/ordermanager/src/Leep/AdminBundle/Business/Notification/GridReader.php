<?php
namespace Leep\AdminBundle\Business\Notification;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('timestamp', 'idTopic', 'message');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',   'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Topic',       'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Message',     'width' => '60%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_NotificationGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Notification', 'p');

        // filter on topic
        if (trim($this->filters->idTopic) != '') {
            $queryBuilder->andWhere('p.idTopic = :idTopic')
                ->setParameter('idTopic', $this->filters->idTopic);
        }

        // filter on message
        if (trim($this->filters->message) != '') {
            $queryBuilder->andWhere('p.message LIKE :message')
                ->setParameter('message', '%' . $this->filters->message . '%');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        // sort by time
        $queryBuilder->orderBy('p.timestamp', 'DESC');
    }
}
