<?php
namespace Leep\AdminBundle\Business\Notification;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idTopic', 'choice', array(
            'label'       => 'Topic',
            'required'    => false,
            'choices'    => $mapping->getMapping('LeepAdmin_Notification_Topic_List')
        ));
        
        $builder->add('message', 'text', array(
            'label'       => 'Message',
            'required'    => false
        ));
        
        return $builder;
    }
}