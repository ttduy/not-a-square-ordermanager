<?php
namespace Leep\AdminBundle\Business\NutriMe;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditStatusHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) {
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new BulkEditStatusModel();
        $model->statusDate = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedOrderNumbers', 'textarea', array(
            'label'    => 'Order numbers',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));
        $builder->add('currentOrder', 'label', array(
            'label'    => 'Selected orders',
            'required' => false
        ));
        $builder->add('status',   'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_NutriMe_Status')
        ));
        $builder->add('statusDate',     'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        if ($model->status == 0) {
            $this->errors = array('Please select status');
            return false;
        }

        $orderNumbers = explode("\n", $model->selectedOrderNumbers);
        $errors = array();
        $status = array();
        $updateOrders = array();
        foreach ($orderNumbers as $orderNumber) {
            $orderNumber = trim($orderNumber);
            if (!empty($orderNumber)) {
                $nutriMe = $em->getRepository('AppDatabaseMainBundle:NutriMe')->findOneByOrderNumber($orderNumber);
                if (!empty($nutriMe)) {
                    $updateOrders[] = $orderNumber;

                    // Max order
                    $query = $em->createQueryBuilder();
                    $query->select('MAX(p.sortOrder)')
                        ->from('AppDatabaseMainBundle:NutriMeStatus', 'p')
                        ->andWhere('p.idNutriMe = :idNutriMe')
                        ->setParameter('idNutriMe', $nutriMe->getId());
                    $maxSortOrder = $query->getQuery()->getSingleScalarResult();

                    // Add status record
                    $status = new Entity\NutriMeStatus();
                    $status->setIdNutriMe($nutriMe->getId());
                    $status->setStatusDate($model->statusDate);
                    $status->setStatus($model->status);
                    $status->setSortOrder($maxSortOrder+1);
                    $em->persist($status);

                    $nutriMe->setCurrentStatus($model->status);
                    $nutriMe->setCurrentStatusDate($model->statusDate);
                    $em->flush();

                    Utils::updateStatusList($em, $nutriMe);
                }
            }
        }
        $em->flush();

        $this->messages[] = 'Status has been updated successfully';
    }
}