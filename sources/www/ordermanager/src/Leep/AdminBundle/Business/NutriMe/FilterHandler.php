<?php
namespace Leep\AdminBundle\Business\NutriMe;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('orderNumber', 'text', array(
            'label'       => 'Order Number',
            'required'    => false
        ));
        $builder->add('customerName', 'text', array(
            'label'       => 'Customer Name',
            'required'    => false
        ));
        $builder->add('mustBeStatus', 'choice', array(
            'label'       => 'Must be status',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_NutriMe_Status'),
            'empty_value' => false,
            'multiple'    => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));
        $builder->add('mustNotBeStatus', 'choice', array(
            'label'       => 'Must not be status',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_NutriMe_Status'),
            'empty_value' => false,
            'multiple'    => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));
        $builder->add('currentStatus', 'choice', array(
            'label'       => 'Current status',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_NutriMe_Status'),
            'empty_value' => false
        ));
        
        return $builder;
    }
}