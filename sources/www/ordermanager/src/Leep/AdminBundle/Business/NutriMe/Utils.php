<?php
namespace Leep\AdminBundle\Business\NutriMe;

class Utils {
    public static function updateStatusList($em, $id) {
        $nutriMe = $em->getRepository('AppDatabaseMainBundle:NutriMe')->findOneById($id);
        $status = array();
        $results = $em->getRepository('AppDatabaseMainBundle:NutriMeStatus')->findByIdNutriMe($id);
        foreach ($results as $r) {
            $status[] = $r->getStatus();
        }

        $nutriMe->setStatusList(implode(',', $status));
        $em->persist($nutriMe);
        $em->flush();
    }
}