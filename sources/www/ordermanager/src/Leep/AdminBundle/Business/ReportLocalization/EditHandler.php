<?php
namespace Leep\AdminBundle\Business\ReportLocalization;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $localization = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLocalization')->findOneByIdLanguage($entity->getId());
        if ($localization) {
            $model->localization = $localization->getLocalization();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'label', array(
            'label'    => 'Name',
            'required' => false
        ));
           
        $builder->add('localization', 'textarea', array(
            'label'    => 'Localization',
            'required' => false,
            'attr'     => array(
                'rows'    => '30',
                'cols'    => '90'
            )
        ));                                 

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();       
        
        $localization = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLocalization')->findOneByIdLanguage($this->entity->getId());
        if (empty($localization)) {
            $localization = new Entity\ReportLocalization();
            $localization->setIdLanguage($this->entity->getId());
            $em->persist($localization);
            $em->flush();
        }

        $localization->setLocalization($model->localization);
        $em->flush();

        parent::onSuccess();
    }
}
