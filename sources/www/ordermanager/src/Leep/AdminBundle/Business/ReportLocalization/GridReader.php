<?php
namespace Leep\AdminBundle\Business\ReportLocalization;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'localization', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',              'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Localization',      'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',            'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ReportLocalizationGridReader');
        return null;
    }

    public function buildCellLocalization($row) {
        return nl2br($row['localization']);
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.name, l.localization')
            ->from('AppDatabaseMainBundle:Language', 'p')
            ->leftJoin('AppDatabaseMainBundle:ReportLocalization', 'l', 'WITH', 'p.id = l.idLanguage');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('reportModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'report_localization', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
        }

        return $builder->getHtml();
    }
}
