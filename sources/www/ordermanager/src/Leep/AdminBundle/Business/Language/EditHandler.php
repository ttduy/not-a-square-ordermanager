<?php
namespace Leep\AdminBundle\Business\Language;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        $model->sortOrder = $entity->getSortOrder();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
         $builder->add('code', 'text', array(
            'attr'     => array(
                'placeholder' => 'Code'
            ),
            'required' => false
        ));  
        $builder->add('sortOrder', 'text', array(
            'attr'     => array(
                'placeholder' => 'Sort Order'
            ),
            'required' => false
        ));        


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model->name);
        $this->entity->setCode($model->code);
        $this->entity->setSortOrder($model->sortOrder);

        parent::onSuccess();
    }
}