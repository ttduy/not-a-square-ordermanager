<?php
namespace Leep\AdminBundle\Business\Language;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
        $builder->add('code', 'text', array(
            'attr'     => array(
                'placeholder' => 'Code'
            ),
            'required' => false
        ));  
        $builder->add('sortOrder', 'text', array(
            'attr'     => array(
                'placeholder' => 'Sort Order'
            ),
            'required' => false
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $language = new Entity\Language();
        $language->setName($model->name);
        $language->setCode($model->code);
        $language->setSortOrder($model->sortOrder);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($language);
        $em->flush();
        
        parent::onSuccess();
    }
}