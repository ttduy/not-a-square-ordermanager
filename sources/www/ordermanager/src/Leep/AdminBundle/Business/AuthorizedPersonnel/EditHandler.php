<?php
namespace Leep\AdminBundle\Business\AuthorizedPersonnel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:AuthorizedPersonnel', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();

        $arrWebUserIds = array();
        if ($entity->getWebUsers()) {
            $arrWebUserIds = explode(',', $entity->getWebUsers());
        }
        $model->webUsers = $arrWebUserIds;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => false
        ));

        $builder->add('webUsers', 'choice', array(
            'label' => 'Web Users',
            'required' => true,
            'multiple'  => 'multiple',
            'choices'   => $mapping->getMapping('LeepAdmin_WebUser_List'),
            'attr'      => array('style' => 'height: 300px')
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model->name);

        $arrWebUserIds = array();
        if ($model->webUsers) {
            foreach ($model->webUsers as $entry) {
                $arrWebUserIds[] = $entry;
            }
        }
        $this->entity->setWebUsers(implode(',', $arrWebUserIds));

        parent::onSuccess();
    }
}