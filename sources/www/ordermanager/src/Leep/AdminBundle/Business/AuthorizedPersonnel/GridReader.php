<?php
namespace Leep\AdminBundle\Business\AuthorizedPersonnel;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'webUsers', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Web Users',   'width' => '50%', 'sortable' => 'true'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:AuthorizedPersonnel', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('authorizedPersonnelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'authorized_personnel', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'authorized_personnel', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellWebUsers($row) {
        $arrWebUserIds = array();
        $arrUserNames = array();

        if ($row->getWebUsers()) {
            $arrWebUserIds = explode(',', $row->getWebUsers());
        }

        if (!empty($arrWebUserIds)) {
            $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $result = $query->select('p')
                            ->from('AppDatabaseMainBundle:WebUsers', 'p')
                            ->andWhere('p.id IN (:ids)')
                            ->setParameter('ids', $arrWebUserIds)
                            ->getQuery()
                            ->getResult();

            if ($result) {
                foreach ($result as $entry) {
                    $arrUserNames[] = $entry->getUsername();
                }
            }
        }

        return implode('; ', $arrUserNames);
    }
}