<?php
namespace Leep\AdminBundle\Business\AuthorizedPersonnel;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => false
        ));

        $builder->add('webUsers', 'choice', array(
            'label' => 'Web Users',
            'required' => true,
            'multiple'  => 'multiple',
            'choices'   => $mapping->getMapping('LeepAdmin_WebUser_List'),
            'attr'      => array('style' => 'height: 300px')
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = $this->getForm()->getData();

        $newItem = new Entity\AuthorizedPersonnel();
        $newItem->setName($model->name);

        $arrWebUserIds = array();
        if ($model->webUsers) {
            foreach ($model->webUsers as $entry) {
                $arrWebUserIds[] = $entry;
            }
        }
        $newItem->setWebUsers(implode(',', $arrWebUserIds));

        $em->persist($newItem);
        $em->flush();
        
        parent::onSuccess();
    }
}