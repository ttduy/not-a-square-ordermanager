<?php
namespace Leep\AdminBundle\Business\Worker;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false
        ));
        $builder->add('apiKey', 'text', array(
            'label'    => 'API Key',
            'required' => false
        ));
        $builder->add('totalProcess', 'text', array(
            'label'    => 'Total Process',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $worker = new Entity\Worker();
        $worker->setName($model->name);
        $worker->setDescription($model->description);
        $worker->setApiKey($model->apiKey);
        $worker->setTotalProcess($model->totalProcess);

        $em->persist($worker);
        $em->flush();

        parent::onSuccess();
    }
}
