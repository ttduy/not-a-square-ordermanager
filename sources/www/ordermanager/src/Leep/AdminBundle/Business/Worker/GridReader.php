<?php
namespace Leep\AdminBundle\Business\Worker;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'description', 'lastCheckin', 'totalProcess', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Description', 'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Last checkin', 'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Total Process', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_WorkerGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Worker', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('reportWorkerModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'worker', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'worker', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellDescription($row) {
        return nl2br($row->getDescription());
    }
}
