<?php
namespace Leep\AdminBundle\Business\Worker;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Worker', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();
        $model->apiKey = $entity->getApiKey();
        $model->totalProcess = $entity->getTotalProcess();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false
        ));
        $builder->add('apiKey', 'text', array(
            'label'    => 'API Key',
            'required' => false
        ));
        $builder->add('totalProcess', 'text', array(
            'label'    => 'Total Process',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setDescription($model->description);
        $this->entity->setApiKey($model->apiKey);
        $this->entity->setTotalProcess($model->totalProcess);
        
        parent::onSuccess();
    }
}
