<?php
namespace Leep\AdminBundle\Business\MwaSample;

class FilterModel {
    public $sampleId;
    public $group;
    public $age;
    public $resultPattern;
    public $currentStatus;
    public $isSendNow;
    public $sendingStatus;
    public $idMwaInvoice;
    public $sampleList;
    public $complaintStatus;
}