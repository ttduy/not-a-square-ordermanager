<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ComputePatternHandler extends BulkEditTatHandler {
    public $formulaTemplate;
    public $resultData = array();

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedSamples', 'textarea', array(
            'label'    => 'Selected samples',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        if (empty($this->formulaTemplate)) {
            $this->errors[] = 'Please select formula template in parameter';
            return;
        }
        
        $samples = explode("\n", $model->selectedSamples);
        foreach ($samples as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $criteria = array(
                    'sampleId' => $sampleId,
                    'idGroup'  => $this->idGroup
                );
                $sample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBy($criteria);
                if (!empty($sample)) {
                    $resultPattern = Utils::computePattern($this->container, $this->formulaTemplate, $sample);
                    if (in_array($resultPattern, array('PATTERN1', 'PATTERN2', 'PATTERN3', 'PATTERN4', 'PATTERN5', 'PATTERN6'))) {
                        $sample->setResultPattern($resultPattern);
                        $em->persist($sample);
                        $this->resultData[$sampleId] = $resultPattern;
                    }
                    else {
                        $this->resultData[$sampleId] = 'Error! Pattern = '.$resultPattern;
                    }                    
                }                
                else {
                    $this->resultData[$sampleId] = 'Sample doesn\'t exist';
                }
            }
        }
        $em->flush();

        $this->messages[] = 'DONE';
    }
}