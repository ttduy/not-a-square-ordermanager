<?php
namespace Leep\AdminBundle\Business\MwaSample;

class BulkEditModel {
    public $selectedSamples;
    public $currentStatus;
    public $pushStatus;
    public $pushDate;
    public $isSendNow;
    public $isOverrideSending;
}