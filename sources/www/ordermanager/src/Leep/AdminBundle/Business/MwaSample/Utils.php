<?php
namespace Leep\AdminBundle\Business\MwaSample;

class Utils {
    public static function getNextStatus($container, $currentStatus) {
        $mapping = $container->get('easy_mapping');
        $nextStatus = array();
        $results = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findByIdMwaStatus($currentStatus);
        foreach ($results as $r) {
            $nextStatus[$r->getIdMwaNextStatus()] = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $r->getIdMwaNextStatus());
        }
        return $nextStatus;
    }

    public static function computePattern($container, $formula, $sample) {
        $input = array(
            'GEW1' => $sample->getGew1(),
            'GEW2' => $sample->getGew2(),
            'GEW3' => $sample->getGew3(),
            'GEW4' => $sample->getGew4(),
            'GEW5' => $sample->getGew5(),
        );
        $executor = $container->get('leep_admin.formula_template.business.executor');
        $executor->execute($formula->getFormula(), $input);
        if (isset($executor->output['PATTERN'])) {
            return $executor->output['PATTERN'];
        }
        return '';
    }
}