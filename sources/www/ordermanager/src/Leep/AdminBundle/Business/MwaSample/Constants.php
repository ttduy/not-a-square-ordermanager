<?php
namespace Leep\AdminBundle\Business\MwaSample;

class Constants {
    const RESULT_PATTERN_PATTERN1 = 'PATTERN1';
    const RESULT_PATTERN_PATTERN2 = 'PATTERN2';
    const RESULT_PATTERN_PATTERN3 = 'PATTERN3';
    const RESULT_PATTERN_PATTERN4 = 'PATTERN4';
    const RESULT_PATTERN_PATTERN5 = 'PATTERN5';
    const RESULT_PATTERN_PATTERN6 = 'PATTERN6';

    public static function getResultPatterns() {
        return array(
            self::RESULT_PATTERN_PATTERN1 => 'PATTERN1',
            self::RESULT_PATTERN_PATTERN2 => 'PATTERN2',
            self::RESULT_PATTERN_PATTERN3 => 'PATTERN3',
            self::RESULT_PATTERN_PATTERN4 => 'PATTERN4',
            self::RESULT_PATTERN_PATTERN5 => 'PATTERN5',
            self::RESULT_PATTERN_PATTERN6 => 'PATTERN6'
        );
    }


    const SENDING_STATUS_SEND              = 1;
    const SENDING_STATUS_ERROR             = 2;
    const SENDING_STATUS_SUCCESS           = 3;
    public static function getSendingStatus() {
        return array(
            self::SENDING_STATUS_SEND        => 'Sending',
            self::SENDING_STATUS_ERROR       => 'Error',
            self::SENDING_STATUS_SUCCESS     => 'Success',
        );
    }

    const GROUP_DEFAULT       = 1;
    const GROUP_RUSSIA        = 2;
}