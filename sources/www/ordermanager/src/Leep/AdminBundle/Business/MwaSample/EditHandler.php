<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $model = new EditModel(); 
        $model->sampleId = $entity->getSampleId();
        $model->group    = $entity->getSampleGroup();
        $model->resultPattern = $entity->getResultPattern();
        $model->currentStatus = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $entity->getCurrentStatus());
        $model->infoText = $entity->getInfoText();

        $model->pushStatus = $entity->getPushStatus();
        $model->pushDatetime = $entity->getPushDatetime();
        $model->isSendNow = $entity->getIsSendNow();
        $model->isOverrideSending = $entity->getIsOverrideSending();
        $model->sendingStatus = $mapping->getMappingTitle('LeepAdmin_MwaSample_SendingStatus', $entity->getSendingStatus());
        if ($entity->getSendingStatus() == 0) {
            $model->sendingStatus = '';
        }
        $model->gew = array(
            'gew1' => $entity->getGew1(),
            'gew2' => $entity->getGew2(),
            'gew3' => $entity->getGew3(),
            'gew4' => $entity->getGew4(),
            'gew5' => $entity->getGew5()
        );

        $model->pushList = array();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MwaSamplePush', 'p')
            ->andWhere('p.idMwaSample = :idMwaSample')
            ->setParameter('idMwaSample', $entity->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $model->pushList[] = array(
                'status'      => $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $r->getStatus()),
                'date'        => $formatter->format($r->getPushDate(), 'date'),
                'timestamp'   => $r->getTimestamp(),
                'sentOn'      => $formatter->format($r->getSentOn(), 'datetime'),
                'responseCode'=> $r->getServerResponseCode(),
                'responseRaw' => $r->getServerResponseRaw(),
            );
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionSampleInfo', 'section', array(
            'label'    => 'Sample Info',
            'property_path' => false
        ));
        $builder->add('sampleId', 'text', array(
            'label'    => 'Sample ID',
            'required' => true
        ));
        $builder->add('group', 'text', array(
            'label'    => 'Group',
            'required' => false
        ));
        $builder->add('resultPattern', 'choice', array(
            'label'    => 'Result Pattern',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_MwaSample_ResultPattern')
        ));
        $builder->add('currentStatus', 'label', array(
            'label'    => 'Current status',
            'required' => false
        ));
        $builder->add('infoText', 'textarea', array(
            'label'    => 'Info',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('gew', 'app_sample_gew_result', array(
            'label'    => 'GEW Result',
            'required' => false
        ));

        $builder->add('sectionSendPush', 'section', array(
            'label'    => 'Send Push',
            'property_path' => false
        ));
        $builder->add('pushStatus',   'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => Utils::getNextStatus($this->container, $this->entity->getCurrentStatus())
        ));
        $builder->add('pushDatetime',     'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('isSendNow',    'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));
        $builder->add('isOverrideSending',    'checkbox', array(
            'label'    => 'Override sending?',
            'required' => false
        ));
        $builder->add('sendingStatus',     'label', array(
            'label'    => 'Last Sending status',
            'property_path' => false,
            'required' => false
        ));

        $builder->add('sectionPushHistory', 'section', array(
            'label'    => 'Push history',
            'property_path' => false
        ));
        $builder->add('pushList', 'collection', array(
            'type' => new Form\AppPushResult($this->container),
            'allow_add' => false,
            'allow_delete' => false,
            'by_reference' => false,
            'options' => array(
                'required' => false
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setSampleId($model->sampleId);
        $this->entity->setSampleGroup($model->group);
        $this->entity->setResultPattern($model->resultPattern);
        $this->entity->setInfoText($model->infoText);

        $this->entity->setPushStatus($model->pushStatus);
        $this->entity->setPushDatetime($model->pushDatetime);
        if ($this->entity->getIsSendNow() == 0 && $model->isSendNow == 1) {
            $this->entity->setSendingStatus(0);
        }
        $this->entity->setSendingStatus(0);
        $this->entity->setIsSendNow($model->isSendNow);        
        $this->entity->setIsOverrideSending($model->isOverrideSending);

        $this->entity->setGew1($model->gew['gew1']);
        $this->entity->setGew2($model->gew['gew2']);
        $this->entity->setGew3($model->gew['gew3']);
        $this->entity->setGew4($model->gew['gew4']);
        $this->entity->setGew5($model->gew['gew5']);

        parent::onSuccess();
    }
}