<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class BulkEditTatHandler extends AppEditHandler {
    public $selectedId;
    public $idGroup;

    public function __construct($container, $group) {
        parent::__construct($container);

        if ($group == 'default') {
            $this->idGroup = Business\MwaSample\Constants::GROUP_DEFAULT;
        }
        else {
            $this->idGroup = Business\MwaSample\Constants::GROUP_RUSSIA;
        }
    }

    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) { 
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new BulkEditTatModel(); 
        $model->selectedSamples = array();
        $result = $this->getSelectedSamples();
        foreach ($result as $r) {
            $model->selectedSamples[] = $r->getSampleId();
        }
        $model->selectedSamples = implode("\n", $model->selectedSamples);

        return $model;
    }

    protected function getSelectedSamples() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:MwaSample', 'd')
            ->andWhere('d.idGroup = '.$this->idGroup)
            ->andWhere('d.id in (:arr)')
            ->setParameter('arr', $this->selectedId)
            ->getQuery()->getResult(); 
        return $result;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedSamples', 'textarea', array(
            'label'    => 'Selected samples',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));
        $builder->add('registeredDate', 'datepicker', array(
            'label'    => 'Registered date',
            'required' => false
        ));        
        $builder->add('finishedDate', 'datepicker', array(
            'label'    => 'Finished date',
            'required' => false
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $samples = explode("\n", $model->selectedSamples);
        $errors = array();
        $status = array();
        foreach ($samples as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $criteria = array(
                    'sampleId' => $sampleId,
                    'idGroup'  => $this->idGroup
                );
                $sample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBy($criteria);
                if (!empty($sample)) {
                    $sample->setRegisteredDate($model->registeredDate);
                    $sample->setFinishedDate($model->finishedDate);
                    $em->persist($sample);
                }                
            }
        }
        $em->flush();

        $this->messages[] = 'The record has been updated successfully';
    }
}