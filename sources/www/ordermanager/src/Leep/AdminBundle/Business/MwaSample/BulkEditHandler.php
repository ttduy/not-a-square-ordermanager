<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class BulkEditHandler extends AppEditHandler {
    public $selectedId;
    public $status;

    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) {
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new BulkEditModel();
        $model->selectedSamples = array();
        $result = $this->getSelectedSamples();
        foreach ($result as $r) {
            $model->selectedSamples[] = $r->getSampleId();
        }
        $model->selectedSamples = implode("\n", $model->selectedSamples);

        //$model->currentStatus = $formatter->format($this->status, 'mapping', 'LeepAdmin_MwaStatus_List');

        $model->pushDate = new \DateTime();
        $model->isSendNow = true;
        $model->isOverrideSending = false;

        return $model;
    }

    protected function getSelectedSamples() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:MwaSample', 'd')
            ->andWhere('d.idGroup = '.Business\MwaSample\Constants::GROUP_DEFAULT)
            ->andWhere('d.id in (:arr)')
            ->setParameter('arr', $this->selectedId)
            ->getQuery()->getResult();
        return $result;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedSamples', 'textarea', array(
            'label'    => 'Selected samples',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));
        $builder->add('currentStatus', 'label', array(
            'label'    => 'Validation',
            'required' => false
        ));
        $builder->add('pushStatus',   'choice', array(
            'label'    => 'Change Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List')
        ));
        $builder->add('pushDate',     'datetimepicker', array(
            'label'    => 'Date',
            'required' => false
        ));
        $builder->add('isSendNow',    'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));
        $builder->add('isOverrideSending',    'checkbox', array(
            'label'    => 'Override sending?',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $samples = explode("\n", $model->selectedSamples);
        $errors = array();
        $status = array();
        foreach ($samples as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $criteria = array(
                    'sampleId' => $sampleId,
                    'idGroup'  => Business\MwaSample\Constants::GROUP_DEFAULT
                );
                $sample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBy($criteria);
                if (!empty($sample)) {
                    if (!isset($status[$sample->getCurrentStatus()])) {
                        $status[$sample->getCurrentStatus()] = array(
                            'status'  => $sample->getCurrentStatus(),
                            'samples' => array()
                        );
                    }
                    $status[$sample->getCurrentStatus()]['samples'][] = $sample->getSampleId();
                } else {
                    $errors[] = $sampleId;
                }
            }
        }

        if (count($status) == 1 && empty($errors)) {
            $s = array_pop($status);
            $criteria = array(
                'idMwaStatus'     => $s['status'],
                'idMwaNextStatus' => $model->pushStatus
            );
            $c = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findOneBy($criteria);
            if ($c) {
                foreach ($s['samples'] as $sampleId) {
                    $criteria = array(
                        'sampleId' => $sampleId,
                        'idGroup'  => Business\MwaSample\Constants::GROUP_DEFAULT
                    );

                    $sample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBySampleId($sampleId);
                    $sample->setPushStatus($model->pushStatus);
                    $sample->setPushDatetime($model->pushDate);
                    $sample->setIsSendNow($model->isSendNow);
                    $sample->setSendingStatus(0);
                    $sample->setIsOverrideSending($model->isOverrideSending);
                    $em->persist($sample);
                    $em->flush();
                }
            }
        }
        else {
            if (count($status) != 1) {
                $this->errors[] = "All samples must have the same status";
            }
            if (!empty($errors)) {
                $this->errors[] = "Invalid sample id: ".implode(', ', $errors);
            }
        }

        $this->messages[] = 'The record has been updated successfully';
    }
}