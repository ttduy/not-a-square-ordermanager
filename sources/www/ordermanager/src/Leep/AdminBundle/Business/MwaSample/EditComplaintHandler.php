<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Leep\AdminBundle\Business\Partner\EditComplaintHandler as BaseEditComplaintHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends BaseEditComplaintHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MwaSampleComplaint', 'p')
            ->andWhere('p.idMwaSample = :idMwaSample')
            ->setParameter('idMwaSample', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idMwaSample' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MwaSampleComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\MwaSampleComplaint();
        $complaint->setIdMwaSample($id);
        return $complaint;
    }
}
