<?php
namespace Leep\AdminBundle\Business\MwaSample;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->resultPattern = 0;
        $model->isSendNow = 0;
        $model->sendingStatus = 0;
        $model->currentStatus = 0;
        $model->idMwaInvoice = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('sampleId', 'text', array(
            'label'    => 'Sample ID',
            'required' => false
        ));
        $builder->add('group', 'text', array(
            'label'    => 'Group',
            'required' => false
        ));
        $builder->add('age', 'text', array(
            'label'    => 'Age',
            'required' => false
        ));
        $builder->add('resultPattern', 'choice', array(
            'label'    => 'Result Pattern',
            'required' => false,
            'choices'  => array(0 => 'All', 'NONE' => 'No pattern', 'ANY' => 'Any pattern') + $mapping->getMapping('LeepAdmin_MwaSample_ResultPattern'),
            'empty_value' => false
        ));
        $builder->add('currentStatus', 'choice', array(
            'label'    => 'Current Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'empty_value' => false
        ));

        $builder->add('isSendNow',    'choice', array(
            'label'    => 'Send now?',
            'required' => false,
            'choices'  => array(0 => 'All', 1 => 'Yes', 2 => 'No'),
            'empty_value' => false
        ));
        $builder->add('sendingStatus',     'choice', array(
            'label'    => 'Sending status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_MwaSample_SendingStatus'),
            'empty_value' => false
        ));
        $builder->add('idMwaInvoice', 'choice', array(
            'label'    => 'Invoice number',
            'required' => false,
            'choices'  => array(0 => 'All', -1 => 'No invoice') + $mapping->getMappingFiltered('LeepAdmin_MwaInvoice_ListDefault'),
            'empty_value' => false
        ));
        $builder->add('sampleList', 'textarea', array(
            'label'    => 'Sample list',
            'required' => false,
            'attr'     => array('rows' => 5, 'cols' => 50)
        ));
        $builder->add('complaintStatus', 'choice', array(
            'label'    => 'Complaint status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_ComplaintStatus'),
            'empty_value' => false
        ));
        
        return $builder;
    }
}