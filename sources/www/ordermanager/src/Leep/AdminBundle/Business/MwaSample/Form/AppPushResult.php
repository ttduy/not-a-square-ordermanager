<?php
namespace Leep\AdminBundle\Business\MwaSample\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPushResult extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_push_result';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('status', 'label');
        $builder->add('date', 'label');
        $builder->add('timestamp', 'label');
        $builder->add('sentOn', 'label');
        $builder->add('responseCode', 'label');
        $builder->add('responseRaw', 'label');
    }
}