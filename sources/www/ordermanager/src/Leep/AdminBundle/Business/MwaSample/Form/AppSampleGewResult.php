<?php
namespace Leep\AdminBundle\Business\MwaSample\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppSampleGewResult extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_sample_gew_result';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        for ($i = 1; $i <= 5; $i++) {
            $builder->add('gew'.$i, 'text', array(
                'label' => 'GEW'.$i,
                'attr'  => array('style' => 'min-width: 80px; max-width: 80px')
            ));
        }
    }
}