<?php
namespace Leep\AdminBundle\Business\Pellet\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPelletEntry extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Pellet\FormType\AppPelletEntryModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_pellet_entry';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('id', 'text', array(
            'label' => 'Id',
            'required'  => false
        )); 
        $builder->add('created', 'datepicker', array(
            'label' => 'Created',
            'required'  => false
        ));
        $builder->add('createdBy',    'text', array(
            'label'  => 'created by',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('received', 'datepicker', array(
            'label' => 'Received',
            'required'  => false
        ));
        $builder->add('receivedStatus',     'checkbox', array(
            'label'  => 'Received status',
            'required' => false
        ));
        $builder->add('receivedBy', 'text', array(
            'label'  => 'Received by',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('cups', 'text', array(
            'label'  => 'Cups',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Disabled',
            'required' => false
        ));
        $builder->add('note',    'text', array(
            'label'  => 'Note',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
    }
}