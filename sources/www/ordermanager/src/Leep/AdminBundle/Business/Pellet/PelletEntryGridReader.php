<?php
namespace Leep\AdminBundle\Business\Pellet;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class PelletEntryGridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('pelletInfo', 'ordering', 'usage', 'note', 'action');
    }

    public $rowTemplate = '';
    public function __construct($container) {
        parent::__construct($container);

        $this->rowTemplate = 
            "<div>
                <div style='float:left; width: 110px'>%s</div>
                <div style='float:left;color: #069'>%s</div>
                <div style='clear:both'></div>
            </div>";
    }
    
    public function getTableHeader() {
        return array(
            array('title' => 'Pellet entry info',      'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Ordering',         'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Usage',            'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Notes',            'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',           'width' => '10%', 'sortable' => 'false'),
              
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_PelletEntryGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:PelletEntry', 'p');

        $request = $this->container->get('request');
        $id = $request->get('id', 0);
        $status = $request->get('status', 0);

        $queryBuilder->andWhere('p.idPellet = :idPellet')->setParameter('idPellet', $id);
        $queryBuilder->andWhere('p.section = :status')->setParameter('status', $status);
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.created', 'DESC');
    }

    public function buildCellPelletInfo($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');

        return 
            sprintf($this->rowTemplate, 'Cups', $row->getCups()).
            sprintf($this->rowTemplate, 'Expiry date', $formatter->format($row->getExpiryDate(), 'date')).
            sprintf($this->rowTemplate, 'Expiry disabled', $row->getExpiryDateWarningDisabled() == 0 ? "---" : "Disabled");
    }

    public function buildCellOrdering($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');

        return 
            sprintf($this->rowTemplate, 'Order date', $formatter->format($row->getCreated(), 'date')).
            sprintf($this->rowTemplate, 'Order by', $row->getCreatedBy()).
            sprintf($this->rowTemplate, 'Received?', $row->getReceivedStatus() == 0 ? "---" : "Yes").
            sprintf($this->rowTemplate, 'Received by', $row->getReceivedBy());
    }

    public function buildCellUsage($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        return 
            sprintf($this->rowTemplate, 'In use?', $row->getIsSelected() == 0 ? "---" : "Yes").
            sprintf($this->rowTemplate, 'Used for', $row->getUsedFor()).
            sprintf($this->rowTemplate, 'Date taken', $formatter->format($row->getDateTaken(), 'date')).
            sprintf($this->rowTemplate, 'Taken by', $row->getTakenBy());

    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'pellet', 'pelletEntryEdit', 'edit', array('id' => $row->getId())), 'button-edit');
        $html = $builder->getHtml();
        return $html;
    }
}
