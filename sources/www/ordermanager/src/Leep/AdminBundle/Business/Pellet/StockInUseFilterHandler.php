<?php
namespace Leep\AdminBundle\Business\Pellet;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class StockInUseFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new StockInUseFilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false
        ));
        
        return $builder;
    }
}