<?php
namespace Leep\AdminBundle\Business\Pellet;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Pellet', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();
        $model->name = $entity->getName();
        $model->letter = $entity->getLetter();
        $model->substanceName = $entity->getSubstanceName();
        $model->activeIngredientName = $entity->getActiveIngredientName();
        $model->weightPerCup = $entity->getWeightPerCup();
        $model->weightWarningThreshold = $entity->getWeightWarningThreshold();
        $model->expiryDateWarningThreshold = $entity->getExpiryDateWarningThreshold();
        $model->itemWarningThreshold = $entity->getItemWarningThreshold();
        $model->notes = $entity->getNotes();
        $model->supplierDeclaration = $entity->getSupplierDeclaration();
        $model->price = intval($entity->getPrice());
        $model->unitsPerPrice = intval($entity->getUnitsPerPrice());
        $model->unitDescription = $entity->getUnitDescription();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Pellet Name',
            'required' => true
        ));
        $builder->add('letter', 'text', array(
            'label'    => 'Pellet Letter',
            'required' => false
        ));
        $builder->add('substanceName', 'text', array(
            'label'    => 'Substance Name',
            'required' => false
        ));
        $builder->add('activeIngredientName', 'text', array(
            'label'    => 'Active Ingredient Name',
            'required' => false
        ));
        $builder->add('weightPerCup', 'text', array(
            'label'    => 'Weight per cup (kg)',
            'required' => false
        ));
        $builder->add('weightWarningThreshold', 'text', array(
            'label'    => 'Warning Threshold (kg)',
            'required' => false
        ));
        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));
        $builder->add('itemWarningThreshold', 'text', array(
            'label'    => 'Item - warning threshold (items)',
            'required' => false
        ));
        $builder->add('price', 'text', array(
            'label'    => 'Price',
            'required' => false
        ));
        $builder->add('unitsPerPrice', 'text', array(
            'label'    => 'Units Per Price',
            'required' => false
        ));
        $builder->add('unitDescription', 'text', array(
            'label'    => 'Unit Description',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
        $builder->add('supplierDeclaration', 'textarea', array(
            'label'    => 'Supplier Declaration',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setName($model->name);
        $this->entity->setLetter($model->letter);
        $this->entity->setSubstanceName($model->substanceName);
        $this->entity->setActiveIngredientName($model->activeIngredientName);
        $this->entity->setWeightPerCup($model->weightPerCup);
        $this->entity->setWeightWarningThreshold($model->weightWarningThreshold);
        $this->entity->setExpiryDateWarningThreshold($model->expiryDateWarningThreshold);
        $this->entity->setItemWarningThreshold($model->itemWarningThreshold);
        $this->entity->setNotes($model->notes);
        $this->entity->setSupplierDeclaration($model->supplierDeclaration);
        $this->entity->setPrice($model->price);
        $this->entity->setUnitsPerPrice($model->unitsPerPrice);
        $this->entity->setUnitDescription($model->unitDescription);

        parent::onSuccess();
    }
}
