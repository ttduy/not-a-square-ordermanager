<?php
namespace Leep\AdminBundle\Business\Pellet;

use Easy\ModuleBundle\Module\AbstractHook;

class PelletGridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $stock = array();
        $waiting = array();
        $stockChanges = array();

        $today = new \DateTime();
        $thisYear = $today->format('Y');
        $lastYear = $thisYear - 1;
        $objLastDecember = \DateTime::createFromFormat('Y/m/d', $lastYear . '/12/1');

        $stockSection = Constant::getStockSection();
        $waitingSection = Constant::getWaitingSection();

        $result = $em->getRepository('AppDatabaseMainBundle:PelletEntry')->findAll();
        foreach ($result as $entry) {
            $pelletId = $entry->getIdPellet();
            $receivedDate = $entry->getReceived();

            if ($receivedDate) {
                $interval = date_diff($objLastDecember, $receivedDate);
                $receivedDate = $receivedDate->format('Y/m/1');
                if (intval($interval->format('%R%s')) < 0) {
                    $receivedDate = null;
                }
            }

            if (empty($stock[$pelletId])) {
                $stock[$pelletId] = 0;
            }
            if (empty($waiting[$pelletId])) {
                $waiting[$pelletId] = 0;
            }

            if ($entry->getReceivedStatus() == 1 &&
                !$entry->getIsSelected() &&
                in_array($entry->getSection(), $stockSection)) {
                $stock[$pelletId] += $entry->getCups();

                if ($receivedDate) {
                    $objDate = \DateTime::createFromFormat('Y/m/d', $receivedDate);
                    $receivedDate = $objDate->format('Y/m/d');
                    if (empty($stockChanges[$receivedDate])) {
                        $stockChanges[$receivedDate] = array();
                    }

                    if (empty($stockChanges[$receivedDate][$pelletId])) {
                        $stockChanges[$receivedDate][$pelletId] = 0;
                    }

                    $stockChanges[$receivedDate][$pelletId] += $entry->getCups();
                }
            } else if (!$entry->getReceivedStatus() &&
                in_array($entry->getSection(), $waitingSection)) {
                $waiting[$pelletId] += $entry->getCups();
            }
        }

        // Compute price
        $pelletPrices = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Pellet')->findAll();
        foreach ($result as $m) {
            $unit = floatval($m->getUnitsPerPrice());
            if (empty($unit)) {
                $unit = 1;
            }
            $pelletPrices[$m->getId()] = floatval($m->getPrice()) / $unit;
        }

        $sumStock = 0;
        $sumWaiting = 0;
        $sumChanges = array();

        $date = $lastYear . '/12/1';
        $objDate = \DateTime::createFromFormat('Y/m/d', $date);
        $sumChanges[$objDate->format('Y/m/d')] = array(
                                    'view'      => 0,
                                    'change'    => 0,
                                    'sum'       => 0,
                                    'name'      => ucfirst($objDate->format('M Y'))
                            );

        for ($i = 1; $i <= 12; $i++) {
            $date = $thisYear . '/' . $i . '/1';
            $objDate = \DateTime::createFromFormat('Y/m/d', $date);
            $sumChanges[$objDate->format('Y/m/d')] = array(
                                        'view'      => 1,
                                        'change'    => 0,
                                        'sum'       => 0,
                                        'name'      => ucfirst($objDate->format('M Y'))
                                );
        }


        foreach ($stockChanges as $date => $arrPellets) {
            foreach ($arrPellets as $pelletId => $num) {
                $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
                $sumChanges[$date]['sum'] += $num * $price;
            }

            $sumChanges[$date]['sum'] = number_format($sumChanges[$date]['sum'], 2);
        }

        $amountChangesByMonths = array();
        $preSumChange = 0;
        foreach ($sumChanges as $eachMonth) {
            $eachMonth['change'] = $eachMonth['sum'] - $preSumChange;
            $preSumChange = $eachMonth['sum'];
            $amountChangesByMonths[] = $eachMonth;
        }

        foreach ($stock as $pelletId => $num) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumStock += $num * $price;
        }
        foreach ($waiting as $pelletId => $num) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumWaiting += $num * $price;
        }

        $data['amountInStock'] = number_format($sumStock, 2);
        $data['amountWaiting'] = number_format($sumWaiting, 2);
        $data['amountChangesByMonths'] = $amountChangesByMonths;
    }
}
