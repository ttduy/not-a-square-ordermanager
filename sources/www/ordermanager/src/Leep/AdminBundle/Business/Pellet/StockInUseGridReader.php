<?php
namespace Leep\AdminBundle\Business\Pellet;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class StockInUseGridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('name', 'letter', 'cups', 'shipment', 'activeIngredient', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',            'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Letter',          'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Cups',            'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Shipment',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'w/w (%)',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',          'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('
                    pe.id as id,
                    pe.cups, pe.shipment,
                    p.name, p.letter, p.activeIngredientName as activeIngredient
                ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'pe')
            ->leftJoin('AppDatabaseMainBundle:Pellet', 'p', 'WITH', 'pe.idPellet = p.id')
            ->andWhere('pe.section = :section')
            ->setParameter('section', Constant::SECTION_TESTED_AND_IN_USE);

        if (intval($this->filters->name) != 0) {
            $queryBuilder->andWhere('p.name = :name')
                ->setParameter('name', '%' . $this->filters->name . '%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $html = '';
        $html = '<span id="moveNextSection_'.$row['id'].'"><a href="javascript:moveNextSection(\''.$row['id'].'\')"><div class="button-redo"></div></a></span>&nbsp;';
        
        return $html;
    }
}
