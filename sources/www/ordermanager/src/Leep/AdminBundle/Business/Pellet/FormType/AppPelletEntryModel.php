<?php
namespace Leep\AdminBundle\Business\Pellet\FormType;

class AppPelletEntryModel {
    public $id;
    public $created;
    public $createdBy;
    public $received;
    public $receivedStatus;
    public $receivedBy;
    public $expiryDate;
    public $expiryDateWarningDisabled;
    public $section;
    public $cups;
    public $note;
}
