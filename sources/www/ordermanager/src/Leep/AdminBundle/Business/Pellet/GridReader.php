<?php
namespace Leep\AdminBundle\Business\Pellet;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters = array();

    // PRE GET RESULT
    public $stock = array();
    public $waiting = array();
    public $expiryNumDays = array();

    public function getColumnMapping() {
        return array('id', 'name', 'letter', 'substanceName', 'activeIngredientName', 'weightPerCup', 'weightWarningThreshold', 'stock', 'waiting', 'expiryWarning', 'notes', 'action');
    }

    public function getColumnSortMapping() {
        return array(
            'p.name',
            'p.letter'
        );
    }

    public function getTableHeader() {
        return array(
            array('title' => 'ID',                          'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Name',                        'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Letter',                      'width' => '6%', 'sortable' => 'true'),
            array('title' => 'Substance Name',              'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Active Ingredient Name',      'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Weight per cup',              'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Warning threshold',           'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Stock',                       'width' => '9%', 'sortable' => 'false'),
            array('title' => 'Waiting',                     'width' => '9%', 'sortable' => 'false'),
            array('title' => 'Expiry Warning',              'width' => '9%', 'sortable' => 'false'),
            array('title' => 'Notes',                       'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '8%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_PelletGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id,
                      p.name,
                      p.letter,
                      p.substanceName,
                      p.activeIngredientName,
                      p.weightPerCup,
                      p.weightWarningThreshold,
                      p.notes,
                      p.expiryDateWarningThreshold,
                      p.itemWarningThreshold')
            ->from('AppDatabaseMainBundle:Pellet', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->letter) != '') {
            $queryBuilder->andWhere('p.letter LIKE :letter')
                ->setParameter('letter', '%'.trim($this->filters->letter).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('pelletModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'pellet', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'pellet', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellNotes($row) {
        if (trim($row['notes']) == '') {
            return '';
        }
        $html ='<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($row['notes']).'</div></a>';
        return $html;
    }

    public function buildCellStock($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('pelletModify')) {
            $builder->addButton('Stock management', $mgr->getUrl('leep_admin', 'pellet', 'feature', 'manageStock', array('id' => $row['id'])), 'button-edit');
        }

        $itemId = $row['id'];
        $this->ensureStockWaiting($itemId);
        $html = $this->stock[$itemId]['item'] .' / €'.number_format($this->stock[$itemId]['value'], 2);

        $threshold = intval($row['weightWarningThreshold']);
        if ($this->stock[$itemId]['item'] < $threshold) {
            $html = '<b style="color: red">'.$html."</b>";
        }

        return $html.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellWaiting($row) {
        $itemId = $row['id'];
        $this->ensureStockWaiting($itemId);
        $html = $this->waiting[$itemId]['item'] .' / €'.number_format($this->waiting[$itemId]['value'], 2);
        return $html;
    }


    private function ensureStockWaiting($idPellet) {
        if (!isset($this->stock[$idPellet])) {
            $this->stock[$idPellet] = [
                'item' =>   0,
                'value' =>  0
            ];
        }
        if (!isset($this->waiting[$idPellet])) {
            $this->waiting[$idPellet] = [
                'item' =>   0,
                'value' =>  0
            ];
        }
    }

    public function buildCellExpiryWarning($row) {
        $idPellet = $row['id'];
        $html = '';
        if (isset($this->expiryNumDays[$idPellet])) {
            $days = $this->expiryNumDays[$idPellet];
            if ($days < $row['expiryDateWarningThreshold']) {
                $htmlTpl = '<div style="color:%s">%s</div>';
                if ($days >= 0) {
                    $html .= sprintf($htmlTpl, 'orange', 'To be expired in ' . $days . ' day(s)');
                } else {
                    $html .= sprintf($htmlTpl, 'red', 'Expired '.abs($days).' day(s)');
                }
            }
        }
        return $html;
    }

    /*
    stock is about
     + recieved but not tested
     + tested and in stock
     - tested and in use
     - used up
     - expired
     - unusable
    waiting is about
     - order but not received
    */
    public function preGetResult() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Compute stocks
        $stockRules = array();
        $stockRules[] = '(p.receivedStatus = 1)';
        $stockRules[] = '(p.isSelected = 0 OR p.isSelected IS NULL)';
        $stockRules[] = '(p.section IN ('.implode(',', Constant::getStockSection()).'))';

        $waitingRules = array();
        $waitingRules[] = '(p.receivedStatus = 0 OR p.receivedStatus IS NULL)';
        $waitingRules[] = '(p.section IN ('.implode(',', Constant::getWaitingSection()).'))';

        $query = $em->createQueryBuilder();
        $query->select('
            p.idPellet,
            (SUM(CASE WHEN ('.implode(' AND ', $stockRules).') THEN p.cups ELSE 0 END)) AS stock,
            (SUM(CASE WHEN ('.implode(' AND ', $waitingRules).') THEN p.cups ELSE 0 END)) AS waiting
            ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'p')
            ->addGroupBy('p.idPellet');

        $result = $query->getQuery()->getResult();

        foreach ($result as $pelletEntry) {
            $idPellet = $pelletEntry['idPellet'];
            $this->ensureStockWaiting($idPellet);

            $this->stock[$idPellet]['item'] = $pelletEntry['stock'];
            $this->waiting[$idPellet]['item'] = $pelletEntry['waiting'];
        }

        // Compute price
        $pelletPrices = [];
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Pellet')->findAll();
        foreach ($result as $m) {
            $unit = floatval($m->getUnitsPerPrice());
            if (empty($unit)) {
                $unit = 1;
            }

            $pelletPrices[$m->getId()] = floatval($m->getPrice()) / $unit;
        }

        foreach ($this->stock as $pelletId => $v) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $this->stock[$pelletId]['value'] = $price * $this->stock[$pelletId]['item'];
        }

        foreach ($this->waiting as $pelletId => $v) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $this->waiting[$pelletId]['value'] = $price * $this->waiting[$pelletId]['item'];
        }

        // Compute expiry
        $query = $em->createQueryBuilder();
        $query->select('p.idPellet, MIN(p.expiryDate) minExpiryDate')
            ->from('AppDatabaseMainBundle:PelletEntry', 'p')
            ->andWhere('(p.expiryDateWarningDisabled = 0) OR (p.expiryDateWarningDisabled IS NULL)')
            ->andWhere('p.expiryDate IS NOT NULL')
            ->andWhere('p.section in (:warningSection)')
            ->setParameter('warningSection', Constant::expiryWarningSections())
            ->addGroupBy('p.idPellet');
        $result = $query->getQuery()->getResult();
        $today = new \DateTime();
        $today->setTime(0,0,0);
        foreach ($result as $r) {
            $expiryDate = \DateTime::createFromFormat('Y-m-d', $r['minExpiryDate']);
            $expiryDate->setTime(0,0,0);
            $idPellet = intval($r['idPellet']);
            $interval = date_diff($today, $expiryDate);
            $days = intval($interval->format('%R%a'));
            $this->expiryNumDays[$idPellet] = $days;
        }
    }
}
