<?php
namespace Leep\AdminBundle\Business\Pellet;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $name;
    public $letter;
    public $substanceName;
    public $activeIngredientName;
    public $weightPerCup;
    public $weightWarningThreshold;
    public $expiryDateWarningThreshold;
    public $itemWarningThreshold;
    public $notes;
    public $supplierDeclaration;
    public $price;
    public $unitsPerPrice;
    public $unitDescription;
}
