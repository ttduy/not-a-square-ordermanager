<?php
namespace Leep\AdminBundle\Business\Pellet\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPelletEntryWeight extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Pellet\FormType\AppPelletEntryWeightModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_pellet_entry_weight';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('txt', 'text', array(
            'label' => 'Name',
            'required'  => false,
            'attr' => array('placeholder' => 'Micronutrient')
        )); 
        $builder->add('weight', 'text', array(
            'label' => 'Weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));
    }
}