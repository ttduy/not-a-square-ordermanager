<?php
namespace Leep\AdminBundle\Business\Pellet;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $model = new CreateModel();

        // 6 months
        $model->expiryDateWarningThreshold = 180;
        $model->price = 0;
        $model->unitsPerPrice = 1;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Pellet Name',
            'required' => true
        ));
        $builder->add('letter', 'text', array(
            'label'    => 'Pellet Letter',
            'required' => false
        ));
        $builder->add('substanceName', 'text', array(
            'label'    => 'Substance Name',
            'required' => false
        ));
        $builder->add('activeIngredientName', 'text', array(
            'label'    => 'Active Ingredient Name',
            'required' => false
        ));
        $builder->add('weightPerCup', 'text', array(
            'label'    => 'Weight per cup (kg)',
            'required' => false
        ));
        $builder->add('weightWarningThreshold', 'text', array(
            'label'    => 'Warning Threshold (kg)',
            'required' => false
        ));
        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));
        $builder->add('itemWarningThreshold', 'text', array(
            'label'    => 'Item - warning threshold (items)',
            'required' => false
        ));
        $builder->add('price', 'text', array(
            'label'    => 'Price',
            'required' => false
        ));
        $builder->add('unitsPerPrice', 'text', array(
            'label'    => 'Units Per Price',
            'required' => false
        ));
        $builder->add('unitDescription', 'text', array(
            'label'    => 'Unit Description',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
        $builder->add('supplierDeclaration', 'textarea', array(
            'label'    => 'Supplier Declaration',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
    }

    public function onSuccess() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $pellet = new Entity\Pellet();
        $pellet->setName($model->name);
        $pellet->setLetter($model->letter);
        $pellet->setSubstanceName($model->substanceName);
        $pellet->setActiveIngredientName($model->activeIngredientName);
        $pellet->setWeightPerCup($model->weightPerCup);
        $pellet->setWeightWarningThreshold($model->weightWarningThreshold);
        $pellet->setExpiryDateWarningThreshold($model->expiryDateWarningThreshold);
        $pellet->setItemWarningThreshold($model->itemWarningThreshold);
        $pellet->setNotes($model->notes);
        $pellet->setSupplierDeclaration($model->supplierDeclaration);
        $pellet->setPrice(intval($model->price));
        $pellet->setUnitsPerPrice(intval($model->unitsPerPrice));
        $pellet->setUnitDescription($model->unitDescription);
        $em->persist($pellet);
        $em->flush();

        parent::onSuccess();
    }
}
