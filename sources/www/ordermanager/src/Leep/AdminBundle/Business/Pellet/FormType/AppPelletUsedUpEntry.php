<?php
namespace Leep\AdminBundle\Business\Pellet\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPelletUsedUpEntry extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Pellet\FormType\AppPelletUsedUpEntryModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_pellet_used_up_entry';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('isSelected', 'checkbox', array(
            'label'  => 'In Use',
            'required' => false
        ));
        $builder->add('cups', 'text', array(
            'label'  => 'Cups',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('dateTaken', 'datepicker', array(
            'label' => 'Date taken',
            'required'  => false
        ));
        $builder->add('usedFor',    'text', array(
            'label'  => 'Used For',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('takenBy',    'text', array(
            'label'  => 'Taken By',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
    }
}