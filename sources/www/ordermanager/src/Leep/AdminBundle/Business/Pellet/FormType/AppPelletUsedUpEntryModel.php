<?php
namespace Leep\AdminBundle\Business\Pellet\FormType;

class AppPelletUsedUpEntryModel {
    public $id;
    public $isSelected;
    public $cups;
    public $dateTaken;
    public $usedFor;
    public $takenBy;
}
