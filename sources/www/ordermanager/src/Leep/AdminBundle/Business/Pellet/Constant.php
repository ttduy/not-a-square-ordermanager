<?php 
namespace Leep\AdminBundle\Business\Pellet;

class Constant {
    const SECTION_PURCHASE                 = 10;
    const SECTION_RECEIVED_BUT_NOT_TESTED  = 30;
    const SECTION_TESTED_AND_IN_STOCK      = 40;
    const SECTION_TESTED_AND_IN_USE        = 50;
    const SECTION_USED_UP                  = 60;
    const SECTION_EXPIRED                  = 70;
    const SECTION_UNUSABLE                 = 80;
    public static function getSections() {
        return array(
            self::SECTION_PURCHASE                    => 'Purchase',
            self::SECTION_RECEIVED_BUT_NOT_TESTED     => 'Received but not tested',
            self::SECTION_TESTED_AND_IN_STOCK         => 'Tested and in stock',
            self::SECTION_TESTED_AND_IN_USE           => 'Tested and in use',
            self::SECTION_USED_UP                     => 'Used up',
            self::SECTION_EXPIRED                     => 'Expired',
            self::SECTION_UNUSABLE                    => 'Unusable'
        );
    }

    public static function expiryWarningSections() {
        return array(
            self::SECTION_TESTED_AND_IN_STOCK,
            self::SECTION_TESTED_AND_IN_USE,
        );
    }

    public static function getStockSection() {
        return array(
            self::SECTION_RECEIVED_BUT_NOT_TESTED,
            self::SECTION_TESTED_AND_IN_STOCK,
            self::SECTION_TESTED_AND_IN_USE
        );
    }

    public static function getWaitingSection() {
        return array(
            self::SECTION_PURCHASE
        );
    }
}
