<?php
namespace Leep\AdminBundle\Business\Press;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'domain', 'idGender', 'publishingCompany', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Domain', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Gender', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Publishing Company', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action', 'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_PressGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Press', 'p');
        
        if (trim($this->filters->domain) != '') {
            $queryBuilder->andWhere('p.domain LIKE :domain')
                ->setParameter('domain', '%'.trim($this->filters->domain).'%');
        }

        if ($this->filters->idGender != 0) {
            $queryBuilder->andWhere('p.idGender = :idGender')
                ->setParameter('idGender', $this->filters->idGender);
        }

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('(p.firstName LIKE :name) OR (p.surName LIKE :name)')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'press', 'view', 'view', array('id' => $row->getId())), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('pressModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'press', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');        
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'press', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }        

        return $builder->getHtml();
    }
}
