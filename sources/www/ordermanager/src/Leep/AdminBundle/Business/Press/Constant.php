<?php 
namespace Leep\AdminBundle\Business\Press;

class Constant {
    const MAILLING_LIST_JOURNALIST_NATIONAL 		= 10;
    const MAILLING_LIST_JOURNALIST_INTERNATIONAL 	= 20;
    const MAILLING_LIST_JOURNALIST_ONLINE_PORTAL    = 30;
    public static function getMailingListTypes() {
        return array(
            self::MAILLING_LIST_JOURNALIST_NATIONAL     	=> 'Journalist National',
            self::MAILLING_LIST_JOURNALIST_INTERNATIONAL    => 'Journalist International',
            self::MAILLING_LIST_JOURNALIST_ONLINE_PORTAL 	=> 'Online Portal'
        );
    }

    const TYPE_PRIVATE_TV 						= 10;
    const TYPE_PUBLIC_TV 						= 20;
    const TYPE_RADIO 							= 30;
    const TYPE_MAGAZINE    						= 40;
    const TYPE_NEWSPAPER    					= 50;
    const TYPE_BLOG    							= 60;
    const TYPE_INFORMATION_SERVICE    			= 70;
    const TYPE_ORGANISATION_OR_FOUNDATION    	= 80;
    public static function getTypes() {
        return array(
            self::TYPE_PRIVATE_TV     				=> 'Private TV',
            self::TYPE_PUBLIC_TV    				=> 'Public TV',
            self::TYPE_RADIO 						=> 'Radio',
            self::TYPE_MAGAZINE 					=> 'Magazine',
            self::TYPE_NEWSPAPER 					=> 'Newspaper',
			self::TYPE_BLOG 						=> 'Blog',
			self::TYPE_INFORMATION_SERVICE 			=> 'Information Service',
			self::TYPE_ORGANISATION_OR_FOUNDATION 	=> 'Organisation / Foundation'
        );
    }
}