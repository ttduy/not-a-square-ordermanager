<?php
namespace Leep\AdminBundle\Business\Press;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Press', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->firstName = $entity->getFirstName();
        $model->surName = $entity->getSurName();
        $model->mailingList = $entity->getMailingList();
        $model->type = $entity->getType();
        $model->publishingCompany = $entity->getPublishingCompany();
        $model->company = $entity->getCompany();
        $model->isPress = $entity->getIsPress();
        $model->domain = $entity->getDomain();
        $model->idGender = $entity->getIdGender();
        $model->isUnisex = $entity->getIsUnisex();
        $model->idLanguage = $entity->getIdLanguage();
        $model->idContact = $entity->getIdContact();
        $model->mainFocus = $entity->getMainFocus();
        $model->isScientific = $entity->getIsScientific();
        $model->source = $entity->getSource();
        $model->title = $entity->getTitle();
        $model->creationDate = $entity->getCreationDate();

        $model->street = $entity->getStreet();
        $model->postalCode = $entity->getPostalCode();
        $model->city = $entity->getCity();
        $model->region = $entity->getRegion();
        $model->idCountry = $entity->getIdCountry();
        $model->email = $entity->getEmail();
        $model->officePhone = $entity->getOfficePhone();
        $model->mobilePhone = $entity->getMobilePhone();
        $model->fax = $entity->getFax();

        $model->isOptedOut = $entity->getIsOptedOut();
        $model->canBeContactedDirectly = $entity->getCanBeContactedDirectly();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));

        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));

        $builder->add('mailingList', 'choice', array(
            'label'    => 'Mailing List',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Press_Mailing_List_Types')
        ));

        $builder->add('type', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Press_Types')
        ));

        $builder->add('publishingCompany', 'text', array(
            'label'    => 'Publishing Company',
            'required' => false
        ));

        $builder->add('company', 'text', array(
            'label'    => 'Company',
            'required' => false
        ));

        $builder->add('isPress', 'checkbox', array(
            'label'    => 'Is Press',
            'required' => false
        ));

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));

        $builder->add('idGender', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));

        $builder->add('isUnisex', 'checkbox', array(
            'label'    => 'Is Unisex',
            'required' => false
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));

        $builder->add('idContact', 'text', array(
            'label'    => 'ID Contact',
            'required' => false
        ));

        $builder->add('mainFocus', 'text', array(
            'label'    => 'Main Focus',
            'required' => false
        ));

        $builder->add('isScientific', 'checkbox', array(
            'label'    => 'Is Scientific',
            'required' => false
        ));

        $builder->add('source', 'text', array(
            'label'    => 'Source',
            'required' => false
        ));

        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));


        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postalCode', 'text', array(
            'label'    => 'Postal Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('region', 'text', array(
            'label'    => 'Region',
            'required' => false
        ));
        $builder->add('idCountry', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('officePhone', 'text', array(
            'label'    => 'Office Phone',
            'required' => false
        ));
        $builder->add('mobilePhone', 'text', array(
            'label'    => 'Mobile Phone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('isOptedOut', 'checkbox', array(
            'label'    => 'Chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('canBeContactedDirectly', 'checkbox', array(
            'label'    => 'Can contact directly',
            'required' => false
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setMailingList($model->mailingList);
        $this->entity->setType($model->type);
        $this->entity->setPublishingCompany($model->publishingCompany);
        $this->entity->setCompany($model->company);
        $this->entity->setIsPress($model->isPress);
        $this->entity->setDomain($model->domain);
        $this->entity->setIdGender($model->idGender);
        $this->entity->setIsUnisex($model->isUnisex);
        $this->entity->setIdLanguage($model->idLanguage);
        $this->entity->setIdContact($model->idContact);
        $this->entity->setMainFocus($model->mainFocus);
        $this->entity->setIsScientific($model->isScientific);
        $this->entity->setSource($model->source);
        $this->entity->setTitle($model->title);
        $this->entity->setCreationDate($model->creationDate);
        
        $this->entity->setStreet($model->street);
        $this->entity->setPostalCode($model->postalCode);
        $this->entity->setCity($model->city);
        $this->entity->setRegion($model->region);
        $this->entity->setIdCountry($model->idCountry);
        $this->entity->setEmail($model->email);
        $this->entity->setOfficePhone($model->officePhone);
        $this->entity->setMobilePhone($model->mobilePhone);
        $this->entity->setFax($model->fax);

        $this->entity->setIsOptedOut($model->isOptedOut);
        $this->entity->setCanBeContactedDirectly($model->canBeContactedDirectly);

        parent::onSuccess();
    }
}
