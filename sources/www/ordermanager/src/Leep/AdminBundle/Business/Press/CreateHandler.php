<?php
namespace Leep\AdminBundle\Business\Press;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->genderId = Business\Base\Constant::GENDER_MALE;
        $model->creationDate = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));

        $builder->add('surName', 'text', array(
            'label'    => 'Surname',
            'required' => false
        ));

        $builder->add('mailingList', 'choice', array(
            'label'    => 'Mailing List',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Press_Mailing_List_Types')
        ));

        $builder->add('type', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Press_Types')
        ));

        $builder->add('publishingCompany', 'text', array(
            'label'    => 'Publishing Company',
            'required' => false
        ));

        $builder->add('company', 'text', array(
            'label'    => 'Company',
            'required' => false
        ));

        $builder->add('isPress', 'checkbox', array(
            'label'    => 'Is Press?',
            'required' => false
        ));

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));

        $builder->add('idGender', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));

        $builder->add('isUnisex', 'checkbox', array(
            'label'    => 'Is Unisex?',
            'required' => false
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));

        $builder->add('idContact', 'text', array(
            'label'    => 'Contact ID',
            'required' => false
        ));

        $builder->add('mainFocus', 'text', array(
            'label'    => 'Main Focus',
            'required' => false
        ));

        $builder->add('isScientific', 'checkbox', array(
            'label'    => 'Is Scientific?',
            'required' => false
        ));

        $builder->add('source', 'text', array(
            'label'    => 'Source',
            'required' => false
        ));

        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));


        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postalCode', 'text', array(
            'label'    => 'Postal Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('region', 'text', array(
            'label'    => 'Region',
            'required' => false
        ));
        $builder->add('idCountry', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('officePhone', 'text', array(
            'label'    => 'Office Phone',
            'required' => false
        ));
        $builder->add('mobilePhone', 'text', array(
            'label'    => 'Mobile Phone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('isOptedOut', 'checkbox', array(
            'label'    => 'Chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('canBeContactedDirectly', 'checkbox', array(
            'label'    => 'Can contact directly',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $press = new Entity\Press();
        $press->setFirstName($model->firstName);
        $press->setSurName($model->surName);
        $press->setMailingList($model->mailingList);
        $press->setType($model->type);
        $press->setPublishingCompany($model->publishingCompany);
        $press->setCompany($model->company);
        $press->setIsPress($model->isPress);
        $press->setDomain($model->domain);
        $press->setIdGender($model->idGender);
        $press->setIsUnisex($model->isUnisex);
        $press->setIdLanguage($model->idLanguage);
        $press->setIdContact($model->idContact);
        $press->setMainFocus($model->mainFocus);
        $press->setIsScientific($model->isScientific);
        $press->setSource($model->source);
        $press->setTitle($model->title);
        $press->setCreationDate($model->creationDate);
        
        $press->setStreet($model->street);
        $press->setPostalCode($model->postalCode);
        $press->setCity($model->city);
        $press->setRegion($model->region);
        $press->setIdCountry($model->idCountry);
        $press->setEmail($model->email);
        $press->setOfficePhone($model->officePhone);
        $press->setMobilePhone($model->mobilePhone);
        $press->setFax($model->fax);

        $press->setIsOptedOut($model->isOptedOut);
        $press->setCanBeContactedDirectly($model->canBeContactedDirectly);

        $em->persist($press);
        $em->flush();

        parent::onSuccess();
    }
}
