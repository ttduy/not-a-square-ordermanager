<?php
namespace Leep\AdminBundle\Business\Press;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $id = $this->container->get('request')->query->get('id', 0);
        $press = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Press')->findOneById($id);
        $data[] = $this->addSection('Press');
        $data[] = $this->add('First Name', $press->getFirstName());
        $data[] = $this->add('Surname', $press->getSurName());
        $data[] = $this->add('Mailing List', $mapping->getMappingTitle('LeepAdmin_Press_Mailing_List_Types', $press->getMailingList()));
        $data[] = $this->add('Type', $mapping->getMappingTitle('LeepAdmin_Press_Types', $press->getType()));
        $data[] = $this->add('Publishing Company', $press->getPublishingCompany());
        $data[] = $this->add('Company', $press->getCompany());
        $data[] = $this->add('Is Press', ($press->getIsPress() ? 'Yes' : 'No'));
        $data[] = $this->add('Domain', $press->getDomain());
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $press->getIdGender()));        
        $data[] = $this->add('Is Unisex', ($press->getIsUnisex() ? 'Yes' : 'No'));
        $data[] = $this->add('Language', $mapping->getMappingTitle('LeepAdmin_Language_List', $press->getIdLanguage()));        
        $data[] = $this->add('ID Contact', $press->getIdContact());
        $data[] = $this->add('Main Focus', $press->getMainFocus());
        $data[] = $this->add('Is Scientific', ($press->getIsScientific() ? 'Yes' : 'No'));
        $data[] = $this->add('Source', $press->getSource());
        $data[] = $this->add('Title', $press->getTitle());
        $data[] = $this->add('Creation Date', $formatter->format($press->getCreationDate(), 'date'));
        
        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $press->getStreet());
        $data[] = $this->add('Postal Code', $press->getPostalCode());
        $data[] = $this->add('City', $press->getCity());
        $data[] = $this->add('Region', $press->getRegion());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $press->getIdCountry()));        
        $data[] = $this->add('Email', $press->getEmail());
        $data[] = $this->add('Office Phone', $press->getOfficePhone());
        $data[] = $this->add('Mobile Phone', $press->getMobilePhone());
        $data[] = $this->add('Fax', $press->getFax());
        
        $data[] = $this->addSection('Others');
        $data[] = $this->add('Is Opted Out', ($press->getIsOptedOut() ? 'Yes' : 'No'));
        $data[] = $this->add('Can Be Contacted Directly', ($press->getCanBeContactedDirectly() ? 'Yes' : 'No'));

        return $data;
    }
}
