<?php
namespace Leep\AdminBundle\Business\Press;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('idGender', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Gender_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        return $builder;
    }
}