<?php
namespace Leep\AdminBundle\Business\Press;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $idLanguage;
    public $mailingList;
    public $type;
    public $idContact;
    public $isPress;
    public $domain;
    public $mainFocus;
    public $isScientific;
    public $source;
    public $idGender;
    public $isUnisex;
    public $title;
    public $firstName;
    public $surName;
    public $company;
    public $publishingCompany;
    public $street;
    public $postalCode;
    public $city;
    public $region;
    public $idCountry;
    public $email;
    public $officePhone;
    public $mobilePhone;
    public $fax;
    public $creationDate;
    public $isOptedOut;
    public $canBeContactedDirectly;
}
