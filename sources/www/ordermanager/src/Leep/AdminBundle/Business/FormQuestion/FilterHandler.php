<?php
namespace Leep\AdminBundle\Business\FormQuestion;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_FormQuestion_Type'),
            'empty_value' => false
        ));
        $builder->add('question', 'text', array(
            'label'    => 'Question',
            'required' => false
        ));
        $builder->add('reportKey', 'text', array(
            'label'    => 'Report key',
            'required' => false
        ));

        return $builder;
    }
}