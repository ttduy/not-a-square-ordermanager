<?php
namespace Leep\AdminBundle\Business\FormQuestion;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_FormQuestion_Type')
        ));
        $builder->add('question', 'text', array(
            'label'    => 'Question',
            'required' => true
        ));
        $builder->add('questionI18n', 'text', array(
            'label'    => 'Question (Multiple Language)',
            'required' => true
        ));
        $builder->add('questionData', 'textarea', array(
            'label'    => 'Data',
            'required' => false,
            'attr'     => array(
                'rows'   => 15, 'cols' => 70
            )
        ));
        $builder->add('questionDataI18n', 'textarea', array(
            'label'    => 'Data (Multiple Language)',
            'required' => false,
            'attr'     => array(
                'rows'   => 15, 'cols' => 70
            )
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort order',
            'required' => false
        ));
        $builder->add('reportKey', 'text', array(
            'label'    => 'Report key',
            'required' => false
        ));
        $builder->add('isHideCustomerDashboard', 'checkbox', array(
            'label'    => 'Hide in customer dashboard?',
            'required' => false
        ));
        $builder->add('isRequired', 'checkbox', array(
            'label'    => 'Required (Customer Dashboard)?',
            'required' => false
        ));
        $builder->add('isRequired', 'checkbox', array(
            'label'    => 'Required (Customer Dashboard)?',
            'required' => false
        ));

        $builder->add('hideFromPartners', 'choice', array(
            'label'    => 'Hide from partners',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $formQuestion = new Entity\FormQuestion();
        $formQuestion->setIdType($model->idType);
        $formQuestion->setQuestion($model->question);
        $formQuestion->setQuestionData($model->questionData);
        $formQuestion->setQuestionI18n($model->questionI18n);
        $formQuestion->setQuestionDataI18n($model->questionDataI18n);
        $formQuestion->setSortOrder($model->sortOrder);
        $formQuestion->setReportKey($model->reportKey);
        $formQuestion->setIsHideCustomerDashboard($model->isHideCustomerDashboard);
        $formQuestion->setIsRequired($model->isRequired);

        if (!empty($model->hideFromPartners)) {
            $formQuestion->setHideFromPartners($model->hideFromPartners);
        } else {
            $formQuestion->setHideFromPartners($model->null);
        }

        $em = $this->container->get('doctrine')->getEntityManager();
        $em->persist($formQuestion);
        $em->flush();

        parent::onSuccess();
    }
}