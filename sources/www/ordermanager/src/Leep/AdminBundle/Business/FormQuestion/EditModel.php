<?php
namespace Leep\AdminBundle\Business\FormQuestion;

class EditModel {
    public $idType;
    public $question;
    public $questionData;
    public $questionI18n;
    public $questionDataI18n;
    public $sortOrder;
    public $reportKey;
    public $isHideCustomerDashboard;
    public $isRequired;
    public $hideFromPartners;
}