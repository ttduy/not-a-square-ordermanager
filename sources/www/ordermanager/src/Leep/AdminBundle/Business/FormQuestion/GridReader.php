<?php
namespace Leep\AdminBundle\Business\FormQuestion;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idType', 'question', 'sortOrder', 'reportKey', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Type',               'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Question',           'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Sort Order',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Report Key',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FormQuestionGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');        
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FormQuestion', 'p');
        if (!empty($this->filters->idType)) {
            $queryBuilder->andWhere('p.idType = :idType')
                ->setParameter('idType', $this->filters->idType);
        }
        if (trim($this->filters->question) != '') {
            $queryBuilder->andWhere('p.question LIKE :question')
                ->setParameter('question', '%'.trim($this->filters->question).'%');
        }
        if (trim($this->filters->reportKey) != '') {
            $queryBuilder->andWhere('p.reportKey LIKE :reportKey')
                ->setParameter('reportKey', '%'.trim($this->filters->reportKey).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('formQuestionModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'form_question', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'form_question', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
            
        return $builder->getHtml();
    }

    public function buildCellNumResponses($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $builder->addPopupButton('View response', $mgr->getUrl('leep_admin', 'feedback_response', 'grid', 'list', array('id' => $row->getId())), 'button-detail');

        return intval($row->getNumResponses()).' response(s)&nbsp;&nbsp;&nbsp; '.$builder->getHtml();
    }
}