<?php
namespace Leep\AdminBundle\Business\FormQuestion;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormQuestion', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->idType = $entity->getIdType();
        $model->question = $entity->getQuestion();
        $model->questionData = $entity->getQuestionData();
        $model->questionI18n = $entity->getQuestionI18n();
        $model->questionDataI18n = $entity->getQuestionDataI18n();
        $model->sortOrder = $entity->getSortOrder();
        $model->reportKey = $entity->getReportKey();
        $model->isHideCustomerDashboard = $entity->getIsHideCustomerDashboard();
        $model->isRequired = $entity->getIsRequired();

        $hideFromPartners = $entity->getHideFromPartners();
        if (empty($hideFromPartners)) {
            $model->hideFromPartners = [];
        } else {
            $model->hideFromPartners = array_map('trim', explode(',', $hideFromPartners));
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_FormQuestion_Type')
        ));
        $builder->add('question', 'text', array(
            'label'    => 'Question',
            'required' => true
        ));
        $builder->add('questionI18n', 'text', array(
            'label'    => 'Question (Multiple Language)',
            'required' => true
        ));
        $builder->add('questionData', 'textarea', array(
            'label'    => 'Data',
            'required' => false,
            'attr'     => array(
                'rows'   => 15, 'cols' => 70
            )
        ));
        $builder->add('questionDataI18n', 'textarea', array(
            'label'    => 'Data (Multiple Language)',
            'required' => false,
            'attr'     => array(
                'rows'   => 15, 'cols' => 70
            )
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort order',
            'required' => false
        ));
        $builder->add('reportKey', 'text', array(
            'label'    => 'Report key',
            'required' => false
        ));
        $builder->add('isHideCustomerDashboard', 'checkbox', array(
            'label'    => 'Hide in customer dashboard?',
            'required' => false
        ));
        $builder->add('isRequired', 'checkbox', array(
            'label'    => 'Required (Customer Dashboard)?',
            'required' => false
        ));

        $builder->add('hideFromPartners', 'choice', array(
            'label'    => 'Hide from partners',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setIdType($model->idType);
        $this->entity->setQuestion($model->question);
        $this->entity->setQuestionData($model->questionData);
        $this->entity->setQuestionI18n($model->questionI18n);
        $this->entity->setQuestionDataI18n($model->questionDataI18n);
        $this->entity->setSortOrder($model->sortOrder);
        $this->entity->setReportKey($model->reportKey);
        $this->entity->setIsHideCustomerDashboard($model->isHideCustomerDashboard);
        $this->entity->setIsRequired($model->isRequired);
        if (!empty($model->hideFromPartners)) {
            $this->entity->setHideFromPartners(implode(',', $model->hideFromPartners));
        } else {
            $this->entity->setHideFromPartners(null);
        }

        parent::onSuccess();
    }
}