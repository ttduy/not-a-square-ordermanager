<?php 
namespace Leep\AdminBundle\Business\FormQuestion;

class Constant {
    const FORM_QUESTION_TYPE_SECTION         = 1;
    const FORM_QUESTION_TYPE_TEXTBOX         = 2;
    const FORM_QUESTION_TYPE_SINGLE_CHOICE   = 3;
    const FORM_QUESTION_TYPE_MULTIPLE_CHOICE = 4;
    public static function getFormQuestionTypes() {
        return array(
            self::FORM_QUESTION_TYPE_SECTION         => 'Section',
            self::FORM_QUESTION_TYPE_TEXTBOX         => 'Textbox',
            self::FORM_QUESTION_TYPE_SINGLE_CHOICE   => 'Single choice',
            self::FORM_QUESTION_TYPE_MULTIPLE_CHOICE => 'Multiple choices'
        );
    }
}
