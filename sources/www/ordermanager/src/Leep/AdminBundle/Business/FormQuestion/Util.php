<?php 
namespace Leep\AdminBundle\Business\FormQuestion;

class Util {
    public static function encodeAnswer($idType, $answer) {
        switch ($idType) {
            case Constant::FORM_QUESTION_TYPE_TEXTBOX:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE:
                if (is_array($answer)) {
                    return implode('|', $answer);
                }
        }
        return $answer;
    }

    public static function decodeAnswer($idType, $answer) {
        switch ($idType) {
            case Constant::FORM_QUESTION_TYPE_TEXTBOX:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE:
                return explode('|', $answer);
        }
        return '';
    }

    public static function decodeAnswerToReport($idType, $answer) {
        switch ($idType) {
            case Constant::FORM_QUESTION_TYPE_TEXTBOX:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE:
                return $answer;
            case Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE:
                return '['.str_replace('|', ',', $answer).']';                
        }
        return '';
    }
}
