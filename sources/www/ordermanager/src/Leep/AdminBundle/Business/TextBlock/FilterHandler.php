<?php
namespace Leep\AdminBundle\Business\TextBlock;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idGroup = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('idGroup', 'choice', array(
            'label'    => 'Group',            
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_TextBlockGroup_List'),
            'required' => false,
            'empty_value' => false
        ));
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        $builder->add('text', 'text', array(
            'label'    => 'Text',
            'required' => false
        ));
        return $builder;
    }
}