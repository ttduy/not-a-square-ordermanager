<?php
namespace Leep\AdminBundle\Business\TextBlock;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlock', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $array = array();
        $array['name'] = $entity->getName();
        $array['idGroup'] = $entity->getIdGroup();
        $array['maxLength'] = $entity->getMaxLength();
        $array['isUppercase'] = $entity->getIsUppercase();
        $array['numLineBreak'] = $entity->getNumLineBreak();
        $array['keyWords'] = $entity->getKeyWords();

        $textBlockLanguages = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdTextBlock($entity->getId());
        foreach ($textBlockLanguages as $tbLanguage) {
            $array['language_'.$tbLanguage->getIdLanguage()] = $tbLanguage->getBody();
        }        

        return $array;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('idGroup', 'choice', array(
            'label'    => 'Group',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TextBlockGroup_List')
        ));
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));        

        
        $builder->add('sectionConstraints', 'section', array(
            'label'    => 'Constraints',
            'property_path' => false
        ));
        $builder->add('maxLength', 'text', array(
            'label'    => 'Max Length',
            'required' => false
        ));
        $builder->add('isUppercase', 'checkbox', array(
            'label'    => 'Uppercase only?',
            'required' => false
        ));        
        $builder->add('numLineBreak', 'text', array(
            'label' => 'The number of line break',
            'required' => false
        ));
        $builder->add('keyWords', 'text', array(
            'label' => 'Key word',
            'required' => false
        ));


        $builder->add('sectionContent', 'section', array(
            'label'    => 'Text Blocks',
            'property_path' => false
        ));

        $languages = $mapping->getMapping('LeepAdmin_Language_List');
        foreach ($languages as $id => $name) {
            $builder->add('language_'.$id, 'textarea', array(
                'label'    => $name,
                'required' => false,
                'attr'     => array('rows' => 5, 'cols' => 80)
            ));
        }

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        

        // Check
        $existingTextBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneByName(trim($model['name']));
        if ($existingTextBlock) {
            if ($existingTextBlock->getId() != $this->entity->getId()) {
                $this->errors = array("The text block '".trim($model['name'])."' is already existed");
                return;
            }
        }

        // Save
        $this->entity->setName($model['name']);
        $this->entity->setIdGroup($model['idGroup']);
        $this->entity->setMaxLength($model['maxLength']);
        $this->entity->setIsUppercase($model['isUppercase']);
        $this->entity->setNumLineBreak($model['numLineBreak']);
        $this->entity->setKeyWords($model['keyWords']);

        $em = $this->container->get('doctrine')->getEntityManager();

        // Save text block languages
        $now = new \DateTime();
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:TextBlockLanguage', array('idTextBlock' => $this->entity->getId()));
        foreach ($model as $k => $v) {
            if (strpos($k, 'language_') === 0) {
                $idLanguage = substr($k, 9);
                $textBlockLanguage = new Entity\TextBlockLanguage();
                $textBlockLanguage->setIdLanguage($idLanguage);
                $textBlockLanguage->setIdTextBlock($this->entity->getId());
                $textBlockLanguage->setBody($v);
                $em->persist($textBlockLanguage);
            }
        }
        $em->flush();

        parent::onSuccess();
    }
}