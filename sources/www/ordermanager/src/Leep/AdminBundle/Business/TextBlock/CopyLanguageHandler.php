<?php
namespace Leep\AdminBundle\Business\TextBlock;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CopyLanguageHandler extends BaseCreateHandler {
    const OVERWRITE_ALL = 10;
    const OVERWRITE_ON_BLANK = 20;

    public function getDefaultFormModel() {
        $model = new CopyLanguageModel();

        return $model;
    }

    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('idSourceLanguage', 'choice', array(
            'label'    => 'Source Language',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('idTargetLanguage', 'choice', array(
            'label'    => 'Target Language',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('idOption', 'choice', array(
            'label'    => 'Option',
            'required' => false,
            'choices'  => array(
                self::OVERWRITE_ALL        => 'Overwrite ALL',
                self::OVERWRITE_ON_BLANK   => 'Overwrite ON BLANK'
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        

        if ($model->idOption == self::OVERWRITE_ALL) {
            $data = array(
                'idSourceLanguage' => $model->idSourceLanguage,
                'idTargetLanguage' => $model->idTargetLanguage
            );

            $job = new Entity\BackgroundJob();
            $job->setIdJobType(Business\BackgroundJob\Constants::BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ALL);
            $job->setTimestamp(new \DateTime());
            $job->setData(json_encode($data));
            $em->persist($job);
            $em->flush();

            $this->messages[] = "Request submitted (Overwrite ALL), please wait from 5-10 minutes";
        }
        else if ($model->idOption == self::OVERWRITE_ON_BLANK) {
            $data = array(
                'idSourceLanguage' => $model->idSourceLanguage,
                'idTargetLanguage' => $model->idTargetLanguage
            );

            $job = new Entity\BackgroundJob();
            $job->setIdJobType(Business\BackgroundJob\Constants::BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ON_BLANK);
            $job->setTimestamp(new \DateTime());
            $job->setData(json_encode($data));
            $em->persist($job);
            $em->flush();

            $this->messages[] = "Request submitted (Overwrite ON BLANK), please wait from 5-10 minutes";
        }
        else {
            $this->messages[] = "Please choose an option";
        }
    }
}