<?php
namespace Leep\AdminBundle\Business\TextBlock;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idGroup', 'name', 'constraints', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Group',       'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Name',        'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Constraints', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_TextBlockGridReader');
    }

    public function getCountSQL() {
        return 'COUNT(DISTINCT p)';
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('DISTINCT p.id, p.idGroup, p.name, p.maxLength, p.isUppercase, p.numLineBreak, p.keyWords')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->innerJoin('AppDatabaseMainBundle:TextBlockLanguage', 'l', 'WITH', 'l.idTextBlock = p.id');
        if ($this->filters->idGroup != 0) {
            $queryBuilder->andWhere('p.idGroup = :idGroup')
                ->setParameter('idGroup', intval($this->filters->idGroup));
        }
        if (!empty($this->filters->name)) {
            if (preg_match('/\[\[(.*)\]\]/ism', $this->filters->name, $matches)) {
                $queryBuilder->andWhere('p.name = :name')
                    ->setParameter('name', $matches[1]);
            }
            else {
                $queryBuilder->andWhere('p.name LIKE :name')
                    ->setParameter('name', '%'.trim($this->filters->name).'%');
            }
        }
        if (!empty($this->filters->text)) {
            $queryBuilder->andWhere('l.body LIKE :text')
                ->setParameter('text', '%'.trim($this->filters->text).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('textBlockModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'text_block', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'text_block', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellConstraints($row) {
        $constraints = array();
        if ($row['maxLength'] !== null) {
            $constraints[] = "Max Length: ".$row['maxLength'];
        }
        if ($row['isUppercase']) {
            $constraints[] = "Uppercase: Yes";
        }
        if ($row['numLineBreak'] !== null) {
            $constraints[] = "Num Line Break: ".$row['numLineBreak'];
        }
        if ($row['keyWords'] != '') {
            $constraints[] = "Keywords: ".$row['keyWords'];
        }
        return implode("<br/>", $constraints);
    }

    public function buildCellName($row) {
        return '[['.$row['name'].']]';
    }
}