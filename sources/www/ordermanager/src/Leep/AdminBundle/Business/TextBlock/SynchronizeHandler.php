<?php
namespace Leep\AdminBundle\Business\TextBlock;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SynchronizeHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SynchronizeModel();

        return $model;
    }

    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('idGroup', 'choice', array(
            'label'    => 'Group',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TextBlockGroup_List')
        ));
        $builder->add('attachment', 'file', array(
            'label'    => 'Attachment',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $separator = "\t";
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        

        if (!empty($model->attachment) && $model->attachment->isValid()) { 
            set_time_limit(240);            

            // Clear old data
            $textBlockIdArr = array(0);
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlock')->findByIdGroup($model->idGroup);
            foreach ($result as $row) {
                $textBlockIdArr[] = $row->getId();  
            }

            // Delete text language
            $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $query
                ->delete('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idTextBlock IN (:idTextBlockArr)')
                ->setParameter('idTextBlockArr', $textBlockIdArr)
                ->getQuery()->execute();      

            // Delete text block
            $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $query
                ->delete('AppDatabaseMainBundle:TextBlock', 'p')
                ->andWhere('p.idGroup = :idGroup')
                ->setParameter('idGroup', $model->idGroup)
                ->getQuery()->execute();
            
            // Move to tmp dir, encoding to UTF-8
            $tmpDir = $this->container->getParameter('kernel.root_dir').'/tmp';
            $model->attachment->move($tmpDir, 'text_block_upload.csv');

            // Import
            $supportLanguages = array();
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language')->findAll();
            foreach ($result as $r) {
                $supportLanguages[$r->getName()] = $r->getId();
            }
            
            if (($handle = fopen($tmpDir.'/text_block_upload.csv', "r")) !== FALSE) {
                // Parse header
                $lineMaxLength = 100000;
                $fields = array();
                $header = fgetcsv($handle, $lineMaxLength, $separator);
                foreach ($header as $i => $language) {
                    $language = trim($language);
                    if (isset($supportLanguages[$language])) {
                        $fields[] = array(
                            'idLanguage'    => $supportLanguages[$language],
                            'columnIndex'   => $i
                        );
                    }
                }
                // Parse content
                $dataSet = array();
                $rowId = 0;
                while (($data = fgetcsv($handle, $lineMaxLength, $separator)) !== FALSE) {
                    $name = trim($data[0]);
                    if (!empty($name)) {
                        $textBlock = new Entity\TextBlock();
                        $textBlock->setIdGroup($model->idGroup);
                        $textBlock->setName($name);
                        $em->persist($textBlock);
                        $dataSet[$rowId] = array(
                            'textBlock' => $textBlock,
                            'languages'   => array()
                        );

                        foreach ($fields as $f) {
                            $idLanguage = $f['idLanguage'];
                            $columnIndex = $f['columnIndex'];
                            $content = $data[$columnIndex];
                            $dataSet[$rowId]['languages'][] = array(
                                'idLanguage' => $idLanguage,
                                'content'    => $content
                            );                            
                        }
                    }
                    $rowId++;
                }
                $em->flush();

                foreach ($dataSet as $rowId => $content) {
                    foreach ($content['languages'] as $language) {
                        $textBlockLanguage = new Entity\TextBlockLanguage();
                        $textBlockLanguage->setIdTextBlock($content['textBlock']->getId());
                        $textBlockLanguage->setIdLanguage($language['idLanguage']);
                        $textBlockLanguage->setBody($language['content']);
                        $em->persist($textBlockLanguage);
                        //file_put_contents($tmpDir.'/test.txt', $language['content']."\n", FILE_APPEND);
                    }
                }
                $em->flush();
            }
            fclose($handle);
        }
        else {
            $this->errors[] = "Upload failed";
            return false;
        }

        //parent::onSuccess();
        $this->messages[] = "Textblock has been synchronized";
    }
}