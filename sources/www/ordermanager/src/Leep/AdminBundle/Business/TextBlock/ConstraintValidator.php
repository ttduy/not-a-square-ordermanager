<?php
namespace Leep\AdminBundle\Business\TextBlock;

class ConstraintValidator {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function validate($textBlock, $text) {
        $errors = array();

        $numLineBreak = 0;
        for ($i = 0; $i < mb_strlen($text,'UTF-8'); $i++) {
            if ($text[$i] == "\n") {
                $numLineBreak++;
            }
        }

        if ($textBlock->getMaxLength() !== null) {
            $strlen = mb_strlen($text,'UTF-8') - 2*$numLineBreak;
            if ($textBlock->getMaxLength() < $strlen ) {
                $errors[] = "The text is too long (".$strlen.' characters), max length = '.$textBlock->getMaxLength();
            }
        }
        if ($textBlock->getIsUppercase() !== null) {
            if ($textBlock->getIsUppercase() == 1) {
                $up = mb_strtoupper($text,'UTF-8');

                if (strcmp($up, $text) != 0) {
                    $errors[] = "The text must be in uppercase";
                }
            }
        }
        if ($textBlock->getNumLineBreak() !== null) {            
            if ($numLineBreak != $textBlock->getNumLineBreak()) {
                $errors[] = "The number of line break is ".$numLineBreak.', expect = '.$textBlock->getNumLineBreak();
            }
        }
        // Check key words
        if ($textBlock->getKeyWords() != '') {                        
            $keywords = explode(",", $textBlock->getKeyWords());
            $missingKeys = [];
            foreach ($keywords as $keyword) {
                if (substr_count($text, $keyword) === 0) {
                    $missingKeys[] = '"'.$keyword.'"';    
                }                
            }
            if (count($missingKeys) > 0) {
                $errors[] = "The text is missing key words: ".implode(", ", $missingKeys);
            }            
        }
        return $errors;
    }
}
