<?php
namespace Leep\AdminBundle\Business\TextBlock;

class Constants {
    const NO_TRANSLATION_YET = '-- No translation yet --';

    const SYNC_SERVER_STATUS_PENDING     = 0;
    const SYNC_SERVER_STATUS_IN_PROGRESS = 1;
    const SYNC_SERVER_STATUS_DONE        = 2;
    public static function getSyncServersStatus() {
        return array(
            self::SYNC_SERVER_STATUS_PENDING       => 'Pending',
            self::SYNC_SERVER_STATUS_IN_PROGRESS   => 'In progress',
            self::SYNC_SERVER_STATUS_DONE          => 'Done'
        );
    }
}