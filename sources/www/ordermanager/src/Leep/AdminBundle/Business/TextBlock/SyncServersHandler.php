<?php
namespace Leep\AdminBundle\Business\TextBlock;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SyncServersHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SyncServersModel();

        return $model;
    }

    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('isSyncTextBlocks', 'checkbox', array(
            'label'    => 'Sync text blocks now?',
            'required' => false
        ));
        $builder->add('isSyncResources', 'checkbox', array(
            'label'    => 'Sync resources now?',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $separator = "\t";
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $syncRequest = null;
        $records = $em->getRepository('AppDatabaseMainBundle:SyncRequest')->findAll();
        if (!empty($records)) {
            $syncRequest = array_pop($records);
        }
        else {
            $syncRequest = new Entity\SyncRequest();
        }

        $syncRequest->setTimestamp(new \DateTime());
        $syncRequest->setLastUpdated(null);
        $syncRequest->setIdUser($userManager->getUser()->getId());
        $syncRequest->setIsSyncTextBlock($model->isSyncTextBlocks);
        $syncRequest->setIsSyncResources($model->isSyncResources);
        $syncRequest->setIsSyncTextBlockStatus(0);
        $syncRequest->setIsSyncResourcesStatus(0);
        $em->persist($syncRequest);
        $em->flush();

        //parent::onSuccess();
        $this->messages[] = "Please wait 3-4 mins to proceed";
    }
}