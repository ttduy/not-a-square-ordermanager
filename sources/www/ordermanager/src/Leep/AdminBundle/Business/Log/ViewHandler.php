<?php
namespace Leep\AdminBundle\Business\Log;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $id = $this->container->get('request')->query->get('id', 0);
        $log = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Log')->findOneById($id);
        
        $arr = json_decode($log->getLogData(), true);
        foreach ($arr['data'] as $k => $v) {
            if (is_array($v)) {
                $data[] = $this->add($k, var_export($v, true));
            }
            else {
                $data[] = $this->add($k, $v);
            }
        }        

        return $data;
    }
}
