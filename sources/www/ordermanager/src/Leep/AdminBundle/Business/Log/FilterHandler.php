<?php
namespace Leep\AdminBundle\Business\Log;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idWebUser = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idWebUser', 'choice', array(
            'label'    => 'User',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));
        $builder->add('keyword', 'text', array(
            'label'    => 'Keyword',
            'required' => false
        ));
        return $builder;
    }
}