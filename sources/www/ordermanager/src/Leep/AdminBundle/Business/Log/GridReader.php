<?php
namespace Leep\AdminBundle\Business\Log;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idWebUser', 'logTime', 'title', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'User',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Time',     'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Activity', 'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_LogGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Log', 'p');        
        if (trim($this->filters->idWebUser) != 0) {
            $queryBuilder->andWhere('p.idWebUser LIKE :idWebUser')
                ->setParameter('idWebUser', '%'.trim($this->filters->idWebUser).'%');
        }
        if (trim($this->filters->keyword) != '')  {
            $queryBuilder->andWhere('(p.title LIKE :keyword) or (p.logData LIKE :keyword)')
                ->setParameter('keyword', '%'.trim($this->filters->keyword).'%');
        }
    }
    
    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'log', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        return $builder->getHtml();
    }
}
