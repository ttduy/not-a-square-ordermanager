<?php
namespace Leep\AdminBundle\Business\FoodTableIngredient;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'variableName', 'riskAmountMax', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',            'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Variable name',   'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Risk amount max', 'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Action',          'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FoodTableIngredient', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->variableName) != '') {
            $queryBuilder->andWhere('p.variableName LIKE :variableName')
                ->setParameter('variableName', '%'.$this->filters->variableName.'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('foodTableModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'food_table_ingredient', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'food_table_ingredient', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}