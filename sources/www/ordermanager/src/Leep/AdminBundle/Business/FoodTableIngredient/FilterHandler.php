<?php
namespace Leep\AdminBundle\Business\FoodTableIngredient;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('variableName', 'text', array(
            'label'    => 'Variable name',
            'required' => false
        ));
        return $builder;
    }
}