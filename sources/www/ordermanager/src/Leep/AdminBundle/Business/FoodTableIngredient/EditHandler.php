<?php
namespace Leep\AdminBundle\Business\FoodTableIngredient;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableIngredient', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->variableName = $entity->getVariableName();
        $model->riskAmountMax = $entity->getRiskAmountMax();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
        $builder->add('variableName', 'text', array(
            'attr'     => array(
                'placeholder' => 'Variable name'
            ),
            'required' => true
        ));
        $builder->add('riskAmountMax', 'text', array(
            'attr'     => array(
                'placeholder' => 'Risk amount max'
            ),
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        // Check if ingredient existed
        $ingredient = $em->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findOneByVariableName($model->variableName);
        if ($ingredient) {
            $this->errors[] = "Ingredient already existed!";
            return false;
        }

        // Add new ingredient
        $this->entity->setName($model->name);
        $this->entity->setVariableName($model->variableName);
        $this->entity->setRiskAmountMax($model->riskAmountMax);

        parent::onSuccess();
    }
}