<?php
namespace Leep\AdminBundle\Business\FoodTableIngredient;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
        $builder->add('variableName', 'text', array(
            'attr'     => array(
                'placeholder' => 'Variable name'
            ),
            'required' => true
        ));
        $builder->add('riskAmountMax', 'text', array(
            'attr'     => array(
                'placeholder' => 'Risk amount max'
            ),
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        // Check if ingredient existed
        $ingredient = $em->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findOneByVariableName($model->variableName);
        if ($ingredient) {
            $this->errors[] = "Ingredient already existed!";
            return false;
        }

        // Add new ingredient
        $ingredient = new Entity\FoodTableIngredient();
        $ingredient->setName($model->name);
        $ingredient->setVariableName($model->variableName);
        $ingredient->setRiskAmountMax($model->riskAmountMax);

        $em->persist($ingredient);
        $em->flush();

        parent::onSuccess();
    }
}