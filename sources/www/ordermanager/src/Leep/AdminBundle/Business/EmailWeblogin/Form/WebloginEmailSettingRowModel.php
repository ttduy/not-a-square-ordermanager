<?php
namespace Leep\AdminBundle\Business\EmailWeblogin\Form;

class WebloginEmailSettingRowModel {
    public $idStatus;
    public $idEmailTemplate;
    public $idEmailMethod;
    public $isSendNow;
    public $isAutoRetry;
}
