<?php
namespace Leep\AdminBundle\Business\EmailWeblogin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class WebloginEmailSettingRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return ['data_class' => 'Leep\AdminBundle\Business\EmailWeblogin\Form\WebloginEmailSettingRowModel'];
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'weblogin_email_setting_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idStatus', 'choice', [
            'label'  => 'Status',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'attr' => [
                'style' => 'min-width: 180px; max-width: 180px'
            ]
        ]);

        $builder->add('idEmailTemplate', 'choice', [
            'label'  => 'Email Template',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List'),
            'attr' => [
                'style' => 'min-width: 180px; max-width: 180px'
            ]
        ]);

        $builder->add('idEmailMethod', 'choice', array(
            'label'    => 'Email Method',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('isAutoRetry', 'checkbox', array(
            'label'    => 'Auto retry when fail?',
            'required' => false
        ));

        $builder->add('isSendNow', 'checkbox', array(
            'label'    => 'Send immediately?',
            'required' => false
        ));
    }
}