<?php
namespace Leep\AdminBundle\Business\EmailWeblogin;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $result = $query->select('p')->from('AppDatabaseMainBundle:WebloginEmailSetting', 'p')->getQuery()->getResult();
        $modelList = [];
        foreach ($result as $setting) {
            $model = new Form\WebloginEmailSettingRowModel();
            $model->id = $setting->getId();
            $model->idStatus = $setting->getIdStatus();
            $model->idEmailTemplate = $setting->getIdEmailTemplate();
            $model->idEmailMethod = $setting->getIdEmailMethod();
            $model->isSendNow = $setting->getIsSendNow();
            $model->isAutoRetry = $setting->getIsAutoRetry();
            $modelList[] = $model;
        }

        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $emailConfig = $config->getEmailWebLoginFilterConfig();
        if ($emailConfig && trim($emailConfig) != "") {
            $emailConfig = json_decode($emailConfig);
        } else {
            $emailConfig = (object)[
                'partners' =>   [],
                'dcList' =>     []
            ];
        }

        if (gettype($emailConfig->partners) != 'array') {
            $newPartners = [];
            foreach ($emailConfig->partners as $key => $value) {
                $newPartners[] = $value;
            }

            $emailConfig->partners = $newPartners;
        }

        if (gettype($emailConfig->dcList) != 'array') {
            $newDCList = [];
            foreach ($emailConfig->dcList as $key => $value) {
                $newDCList[] = $value;
            }

            $emailConfig->dcList = $newDCList;
        }

        return [
            'settingList' =>    $modelList,
            'partners' =>       $emailConfig->partners,
            'dcList' =>         $emailConfig->dcList
        ];
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('settingList', 'collection', array(
            'type' => new Form\WebloginEmailSettingRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Weblogin Email Setting',
                'required' => false
            )
        ));

        $builder->add('partners', 'choice', array(
            'label'    => 'Partners',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        $builder->add('dcList', 'choice', array(
            'label'    => 'Distribution Channels',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Clear all old setting
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:WebloginEmailSetting', []);

        // Update new setting
        $modelList = $this->getForm()->getData()['settingList'];
        foreach ($modelList as $model) {
            $setting = new Entity\WebloginEmailSetting();
            $setting->setIdStatus($model->idStatus);
            $setting->setIdEmailTemplate($model->idEmailTemplate);
            $setting->setIdEmailMethod($model->idEmailMethod);
            $setting->setIsSendNow($model->isSendNow);
            $setting->setIsAutoRetry($model->isAutoRetry);
            $em->persist($setting);
        }

        $partners = $this->getForm()->getData()['partners'];
        $dcList = $this->getForm()->getData()['dcList'];

        if (gettype($partners) != 'array') {
            $newPartners = [];
            foreach ($partners as $key => $value) {
                $newPartners[] = $value;
            }

            $partners = $newPartners;
        }

        if (gettype($dcList) != 'array') {
            $newDCList = [];
            foreach ($dcList as $key => $value) {
                $newDCList[] = $value;
            }

            $dcList = $newDCList;
        }

        // Save config
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $config->setEmailWebLoginFilterConfig(json_encode([
            'partners' =>   $partners,
            'dcList' =>     $dcList
        ]));

        $em->persist($config);
        $em->flush();
    }
}
