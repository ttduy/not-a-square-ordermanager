<?php
namespace Leep\AdminBundle\Business\EmailWeblogin;

class Constant {
    const REPORT_GOES_TO_PARTNER =              1;
    const REPORT_GOES_TO_CUSTOMER =             2;
    const REPORT_GOES_TO_DC =                   3;
    const REPORT_GOES_TO_CUSTOMER_AND_DC =      4;
    const REPORT_GOES_TO_PARTNER_AND_DC =       5;
    const REPORT_GOES_TO_CUSTOMER_AND_PARTNER = 6;
    const REPORT_GOES_TO_ALL =                  7;
    public static function getReportGoesTo() {
        return [
            self::REPORT_GOES_TO_PARTNER =>                 'Partner',
            self::REPORT_GOES_TO_CUSTOMER =>                'Customer',
            self::REPORT_GOES_TO_DC =>                      'Distribution Channel',
            self::REPORT_GOES_TO_CUSTOMER_AND_DC =>         'Customer and DC',
            self::REPORT_GOES_TO_PARTNER_AND_DC =>          'Partner and DC',
            self::REPORT_GOES_TO_CUSTOMER_AND_PARTNER =>    'Customer and Partner',
            self::REPORT_GOES_TO_ALL =>                     'All (Customer, DC, Partner)'
        ];
    }

    public static function getReportGoesToGroup($group=null) {
        $result = [
            'partner' => [
                self::REPORT_GOES_TO_PARTNER,
                self::REPORT_GOES_TO_CUSTOMER_AND_PARTNER,
                self::REPORT_GOES_TO_CUSTOMER_AND_DC,
                self::REPORT_GOES_TO_ALL
            ],
            'dc' => [
                self::REPORT_GOES_TO_DC,
                self::REPORT_GOES_TO_CUSTOMER_AND_DC,
                self::REPORT_GOES_TO_CUSTOMER_AND_DC,
                self::REPORT_GOES_TO_ALL
            ],
            'customer' => [
                self::REPORT_GOES_TO_CUSTOMER,
                self::REPORT_GOES_TO_CUSTOMER_AND_DC,
                self::REPORT_GOES_TO_CUSTOMER_AND_PARTNER,
                self::REPORT_GOES_TO_ALL
            ],
        ];

        if (!empty($group) && isset($result[$group])) {
            return $result[$group];
        } else {
            return $result;
        }
    }
}
