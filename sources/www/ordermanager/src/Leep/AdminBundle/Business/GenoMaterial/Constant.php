<?php
    namespace Leep\AdminBundle\Business\GenoMaterial;
    class Constant{
        const GENO_MATERIAL_FILE_DIR = 'geno_material_simple_files';

        const VITAMIN_CHOICE = 1;
        const MINARAL_CHOICE = 2;
        const OTHER_CHOICE = 999;
        public static function getTypeChoice(){
            return array(
                self::VITAMIN_CHOICE    => 'Vitamin',
                self::MINARAL_CHOICE    => 'Minaral',
                self::OTHER_CHOICE      => 'Other'
            );
        }
    }
 ?>
