<?php
namespace Leep\AdminBundle\Business\GenoMaterial;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;

class CopyHandler extends CreateHandler {
    //TODO: check attachment file duplicate name another file in database
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }

    public function getDefaultFormModel() {
        $model = new CreateModel();
        $this->generateAttachmentKey();

        $id = $this->container->get('request')->get('id');
        $entity = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneById($id);

        // Add new attachment key
        $model->attachmentKey = $this->attachmentKey;
        $model->fileAttachment = array();

        // Clone data
        $model->shortName = $entity->getShortName();
        $model->fullName = $entity->getFullName();
        $model->targetLoad = $entity->getTargetLoad();
        $model->warningThreshold = $entity->getWarningThreshold();
        $model->storageConditions = $entity->getStorageConditions();
        $model->type = $entity->getType();
        $model->unitsInStock = $entity->getUnitsInStock();
        $model->unitDescription = $entity->getUnitDescription();
        $model->isCriticalSubstance = $entity->getIsCriticalSubstance();

        // return model
        return $model;
    }
}
