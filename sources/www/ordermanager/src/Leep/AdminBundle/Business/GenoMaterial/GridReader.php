<?php
    namespace Leep\AdminBundle\Business\GenoMaterial;

    use Leep\AdminBundle\Business\Base\AppGridDataReader;
    use Leep\AdminBundle\Business;

    class GridReader extends AppGridDataReader
    {
        const MAX_TARGET_LOAD = 1.0;
        const MAX_PERCENT = 100.0;

        private $stock = [];
        private $waiting = [];
        private $typeList = [];

        // ======= declare basic attribute ========
        public $filters;

        // ======== function ======================
        public function getColumnMapping() {
            $this->typeList = Business\GenoMaterialType\Utils::getGenoMaterialTypeList($this->container);
            return array('name', 'targetLoad', 'type', 'warningThreshold', 'stock', 'waiting', 'isCriticalSubstance', 'modifiedDate', 'modifiedBy', 'action');
        }

        public function getTableHeader() {
            return array(
                array('title' => 'Name',                    'width' => '20%', 'sortable' => 'true'),
                array('title' => 'Target load',             'width' => '7%', 'sortable' => 'false'),
                array('title' => 'Type',                    'width' => '6%', 'sortable' => 'false'),
                array('title' => 'Warning Threshold',       'width' => '7%', 'sortable' => 'false'),
                array('title' => 'Stock',                   'width' => '10%', 'sortable' => 'false'),
                array('title' => 'Waiting',                 'width' => '8%', 'sortable' => 'false'),
                array('title' => 'Is critical substance',   'width' => '7%', 'sortable' => 'false'),
                array('title' => 'Modified Date',           'width' => '10%', 'sortable' => 'false'),
                array('title' => 'Modified By',             'width' => '10%', 'sortable' => 'false'),
                array('title' => 'Action',                  'width' => '15%', 'sortable' => 'false')
            );
        }

        public function buildQuery($queryBuilder) {
            # code...
            $queryBuilder->select('p')->from('AppDatabaseMainBundle:GenoMaterials','p');

            // get short name & full name
            if (trim($this->filters->shortName) != ''){
                $queryBuilder->andWhere('p.shortName LIKE :shortName')
                ->setParameter('shortName', '%'.trim($this->filters->shortName).'%');
            }

            if (trim($this->filters->fullName) != ''){
                $queryBuilder->andWhere('p.fullName LIKE :fullName')
                ->setParameter('fullName', '%'.trim($this->filters->fullName).'%');
            }

            // get type
            if(!empty($this->filters->types)){
                $queryBuilder->andWhere('p.type in (:types)')->setParameter('types', $this->filters->types);
            }
        }

        public function postQueryBuilder($queryBuilder) {
             $queryBuilder->orderBy('p.shortName', 'ASC');
        }

        public function buildCellName($row) {
            $fullName = $row->getFullName();
            if(!empty($fullName)){
                return $row->getFullName()." (".$row->getShortName().")";
            }
            return "(".$row->getShortName().")";
        }

        public function buildCellTargetLoad($row)
        {
            $targetLoad = $row->getTargetLoad();
            return $targetLoad;
        }

        public function buildCellType($row){
            $formatter = $this->container->get('leep_admin.helper.formatter');
            $idType = $row->getType();
            $typeName = isset($this->typeList[$idType]) ? $this->typeList[$idType] : "";
            return $typeName;
        }

        public function buildCellStock($row){
            // get all batches information(price per gram, amount in stock) use this Geno Materials
            $materialId = $row->getId();
            $em = $this->container->get('doctrine')->getEntityManager();
            $query = $em->createQueryBuilder();
            $query->select('p.amountInStock, p.pricePerGram')
                    ->from('AppDatabaseMainBundle:GenoBatches', 'p')
                    ->where($query->expr()->andX(
                        $query->expr()->eq('p.idGenoMaterial', ':id'),
                        $query->expr()->eq('p.status', ':inStock')))
                    ->setParameter('id', $materialId)
                    ->setParameter('inStock', Business\GenoBatch\Constant::STATUS_IN_STOCK);
            $queryResult = $query->getQuery()->getResult();

            $stock = ['sumWeight' => 0.0, 'sumPrice' => 0.0];
            foreach ($queryResult as $qr) {
                $currentAmount = $qr['amountInStock'];
                $stock['sumWeight'] += $currentAmount;
                $stock['sumPrice'] += floatval($currentAmount * $qr['pricePerGram']);
            }

            $builder = $this->container->get('leep_admin.helper.button_list_builder');
            $mgr = $this->getModuleManager();

            $html = (number_format($stock['sumWeight'], 2)).'/ €'.(number_format($stock['sumPrice'], 2));
            $units_in_stock = $stock['sumWeight'];
            $warningThreshold = floatval($row->getWarningThreshold());
            if (($units_in_stock >= 0) and ($warningThreshold >= 0)) {
                if($units_in_stock < $warningThreshold){
                    $html_tmp = strval($html);
                    $html = '<b style="color:red">'.$html_tmp.'</b>';
                }
            }
            return $html.'&nbsp;&nbsp'.$builder->getHtml();
        }

        public function buildCellWaiting($row){
            // get all batches information(price per gram, amount in stock) use this Geno Materials
            $materialId = $row->getId();
            $em = $this->container->get('doctrine')->getEntityManager();
            $query = $em->createQueryBuilder();
            $query->select('p.amountInStock, p.pricePerGram')
                ->from('AppDatabaseMainBundle:GenoBatches', 'p')
                ->where($query->expr()->andX(
                    $query->expr()->eq('p.idGenoMaterial', ':id'),
                    $query->expr()->orX(
                        $query->expr()->eq('p.status', ':inOrdered'),
                        $query->expr()->eq('p.status', ':inOnHold')
                    )))
                ->setParameter('id', $materialId)
                ->setParameter('inOrdered', Business\GenoBatch\Constant::STATUS_ORDERED)
                ->setParameter('inOnHold', Business\GenoBatch\Constant::STATUS_ON_HOLD);
            $queryResult = $query->getQuery()->getResult();

            $waiting = ['sumWeight' => 0.0, 'sumPrice' => 0.0];
            foreach ($queryResult as $qr) {
                $currentAmount = $qr['amountInStock'];
                $waiting['sumWeight'] += $currentAmount;
                $waiting['sumPrice'] += floatval($currentAmount * $qr['pricePerGram']);
            }

            $builder = $this->container->get('leep_admin.helper.button_list_builder');
            $mgr = $this->getModuleManager();

            $html = (number_format($waiting['sumWeight'], 2)).'/ €'.(number_format($waiting['sumPrice'], 2));
            return $html.'&nbsp;&nbsp'.$builder->getHtml();
        }

        public function buildCellIsCriticalSubstance($row){
            if ($row->getIsCriticalSubstance() == 1) {
                return 'Yes';
            }
            return 'No';
        }

        public function buildCellModifiedDate($row){
            $date_modify = $row->getModifiedDate();
            return date_format($date_modify, 'Y-m-d H:i:s');
        }

        public function buildCellModifiedBy($row)
        {
            $formatter = $this->container->get('leep_admin.helper.formatter');
            $id_editor = $row->getModifiedBy();
            return $formatter->format($id_editor, 'mapping', 'LeepAdmin_WebUser_List');
            return $id_editor;
        }

        public function buildCellAction($row)
        {
            $builder = $this->container->get('leep_admin.helper.button_list_builder');
            $mgr = $this->getModuleManager();

            $helperSecurity = $this->container->get('leep_admin.helper.security');
            if ($helperSecurity->hasPermission('genoMaterialModify')) {
                $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_material', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_material', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
                $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'geno_material', 'copy', 'create', array('id' => $row->getId())), 'button-copy');
            }

            return $builder->getHtml();
        }
    }

?>
