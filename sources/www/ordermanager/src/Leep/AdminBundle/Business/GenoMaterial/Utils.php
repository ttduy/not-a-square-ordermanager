<?php
namespace Leep\AdminBundle\Business\GenoMaterial;

class Utils {

    static public function getMaterialList($container) {
        $em = $container->get('doctrine')->getEntityManager();

        $materials = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findBy([], ['shortName' => 'ASC']);
        $result = [];
        foreach ($materials as $material) {
            $currentFullName = $material->getFullName();
            $currentShortName = $material->getShortName();
            if (!empty($currentFullName)) {
                $result[$material->getId()] = $currentFullName." (".$currentShortName.")";
            }
            else{
                $result[$material->getId()] = "( ".$currentShortName.")";
            }
        }

        return $result;
    }

    static public function getSubstanceList($container) {
        $em = $container->get('doctrine')->getEntityManager();

        $substances = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findBy([], ['shortName' => 'ASC']);
        $result = [];
        foreach ($substances as $substance) {
            $result[$substance->getId()] = $substance->getShortName();
        }

        return $result;
    }

    static public function getAllGenoSubstance($container) {
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.substanceFullName')
                ->from('AppDatabaseMainBundle:GenoMaterials', 'p');
        $results = $query->getQuery()->getResult();
        $substances = [];
        foreach ($results as $r) {
            if (!empty($r['substanceFullName'])) {
                $substances[$r['id']] = $r['substanceFullName'];
            }
        }
        return $substances;
    }
}
