<?php

namespace Leep\AdminBundle\Business\GenoMaterial;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\GenoMaterialType;


class CreateHandler extends BaseCreateHandler{
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }

    public function getDefaultFormModel() {
        $model = new CreateModel();
        $this->generateAttachmentKey();
        $model->attachmentKey = $this->attachmentKey;

        return $model;
    }

    // Let's build form
    public function buildForm($builder) {
        // process for attachment Key
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label' =>          'Attachments',
            'required' =>       false,
            'key' =>            'genomaterials/'.$this->attachmentKey,
            'snapshot' =>       false,
            'allowCloneDir' =>  true
        ));

        // create mapping to get more library
        $mapping = $this->container->get('easy_mapping');

        // ==========================
        // Section material
        $builder->add('sectionGenoMaterial','section', array(
            'label' => 'Geno Material',
            'property_path' => false
        ));

        /// short name
        $builder->add('shortName', 'text', array(
            'label' => 'Short name',
            'required' => true
        ));

        /// full name
        $builder->add('fullName', 'text', array(
            'label' => 'Full name',
            'required' => false
        ));
        /// number target load
        $builder->add('targetLoad', 'text', array(
            'label' => 'Target load',
            'required' => false
        ));

        /// warning threshold: number
        $builder->add('warningThreshold', 'text', array(
            'label' => 'Warning Threshold',
            'required' => false
        ));

        /// storage condidtion
        $builder->add('storageConditions', 'text', array(
            'label' => 'Storage conditions',
            'required' => false
        ));

        /// type number
        $builder->add('type', 'choice', array(
            'label' => 'Type',
            'required' => true,
            'choices'  => [0 => ''] + Business\GenoMaterialType\Utils::getGenoMaterialTypeList($this->container),
            'empty_value' => false
        ));

        /// Units in stock
        $builder->add('unitsInStock', 'text', array(
            'label' => 'Units in stock',
            'required' => false
        ));

        /// Unit description
        $builder->add('unitDescription', 'textarea', array(
            'label' => 'Unit description',
            'required' => false,
            'attr' => [
                'style' =>  'width:600px',
                'rows' =>   10
            ]
        ));

        /// check if geno matrial is critical subtance
        $builder->add('isCriticalSubstance', 'checkbox', array(
            'label' => 'Is critical subtance?',
            'required' =>   false
        ));

        $builder->add('enableCollectStorageSampleWarning', 'checkbox', array(
            'label' => 'Enable collect storage sample warning?',
            'required' =>   false
        ));

        $builder->add('sectionSubstance','section', array(
            'label' => 'Geno Substance',
            'property_path' => false
        ));
        $builder->add('substanceFullName', 'text', array(
            'label' => 'Full English substance name',
            'required' => false
        ));
        $builder->add('substanceTextBlock', 'text', array(
            'label' => 'Text block',
            'required' => false
        ));
    }

    // assign value
    public function onSuccess() {
        // get doctrine and form data
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // get user manager
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $shortName = trim($model->shortName);
        if (empty($shortName)) {
            $this->errors[] = "Error!";
            $this->getForm()->get('shortName')->addError(new FormError("Short name can not be empty!"));
            return false;
        }

        $check = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findOneByShortName($shortName);
        if ($check) {
            $this->errors[] = "Error!";
            $this->getForm()->get('shortName')->addError(new FormError("Geno Material short name already exist!"));
            return false;
        }

        $targetLoad = floatval($model->targetLoad);

        // Let's assign data
        $genoMaterial = new Entity\GenoMaterials();

        /// Assign form data form
        $genoMaterial->setShortName(trim($shortName));
        $genoMaterial->setFullName(trim($model->fullName));
        $genoMaterial->setTargetLoad($targetLoad);
        $genoMaterial->setWarningThreshold(intval($model->warningThreshold));
        $genoMaterial->setStorageConditions($model->storageConditions);
        $genoMaterial->setType($model->type);
        $genoMaterial->setUnitsInStock(intval($model->unitsInStock));
        $genoMaterial->setUnitDescription($model->unitDescription);
        $genoMaterial->setIsCriticalSubstance($model->isCriticalSubstance);
        $genoMaterial->setModifiedDate(new \DateTime('now'));
        $genoMaterial->setCreatedDate(new \DateTime('now'));
        $genoMaterial->setModifiedBy($userManager->getUser()->getId());
        $genoMaterial->setAttachmentKey($model->attachmentKey);
        $genoMaterial->setEnableCollectStorageSampleWarning($model->enableCollectStorageSampleWarning);
        $genoMaterial->setSubstanceFullName($model->substanceFullName);
        $genoMaterial->setSubstanceTextBlock($model->substanceTextBlock);

        // write to database
        $em->persist($genoMaterial);
        $em->flush();

        parent::onSuccess();
    }
}
