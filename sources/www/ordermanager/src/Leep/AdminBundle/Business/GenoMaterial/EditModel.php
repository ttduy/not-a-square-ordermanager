<?php
    namespace  Leep\AdminBundle\Business\GenoMaterial;

    class EditModel {
        public $attachment;
        public $attachmentKey;
        public $fileAttachment;

        public $shortName;
        public $fullName;
        public $targetLoad;
        public $warningThreshold;
        public $storageConditions;
        public $type;
        public $unitsInStock;
        public $unitDescription;
        public $isCriticalSubstance;
        public $enableCollectStorageSampleWarning;
        public $substanceFullName;
        public $substanceTextBlock;
    }
?>
