<?php
namespace Leep\AdminBundle\Business\GenoMaterial;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class GridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $stockChanges = array();

        $today = new \DateTime();
        $thisYear = $today->format('Y');
        $lastYear = $thisYear - 1;
        $objLastDecember = \DateTime::createFromFormat('Y/m/d', $lastYear . '/12/01');

        // Calculate current In-Stock / Waiting of batches
        $curInStockValue = 0;
        $curWaitingValue = 0;
        $batchesResult = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findAll();
        foreach ($batchesResult as $batch) {
            $amountInStock = $batch->getAmountInStock(); // gram
            $pricePerGram = $batch->getPricePerGram();
            if ($batch->getRecievedDate()) {
                $curInStockValue += ($amountInStock * $pricePerGram);
            } else {
                $curWaitingValue += ($amountInStock * $pricePerGram);
            }
        }

        // stock changing per month
        $stockChanges = array();
        $batchLogsResult = $em->getRepository('AppDatabaseMainBundle:GenoBatchLogs')->findAll();
        foreach( $batchLogsResult as $log) {
            $idBatch = $log->getBatchId();
            $date = $log->getDate();
            if ($date) {
                $interval = date_diff($objLastDecember, $date);
                $date = $date->format('Y/m/01');
                if (intval($interval->format('%R%s')) < 0) {
                    $date = null;
                } else {
                    if (empty($stockChanges[$date])) {
                        $stockChanges[$date] = array();
                    }
                    if (empty($stockChanges[$date][$idBatch])) {
                        $stockChanges[$date][$idBatch] = 0;
                    }
                    $stockChanges[$date][$idBatch] += $log->getAmount();
                }
            }
        }

        // Compute price per month
        $sumChanges = array();
        $date = $lastYear . '/12/01';
        $objDate = \DateTime::createFromFormat('Y/m/d', $date);
        $sumChanges[$objDate->format('Y/m/d')] = array(
                                    'view'      => 0,
                                    'change'    => 0,
                                    'sum'       => 0,
                                    'name'      => ucfirst($objDate->format('M Y'))
                            );
        for ($i = 1; $i <= 12; $i++) {
            $date = $thisYear . '/' . $i . '/01';
            $objDate = \DateTime::createFromFormat('Y/m/d', $date);
            $sumChanges[$objDate->format('Y/m/d')] = array(
                                        'view'      => 1,
                                        'change'    => 0,
                                        'sum'       => 0,
                                        'name'      => ucfirst($objDate->format('M Y'))
                                );
        }
        foreach ($stockChanges as $date => $arrBatchs) {
            foreach ($arrBatchs as $idBatch => $amount) {
                foreach ($batchesResult as $batch){
                    if ($batch->getId() == $idBatch){
                        if ($batch->getRecievedDate()) {
                            $pricePerGram = $batch->getPricePerGram();
                            if (isset($sumChanges[$date])) {
                                $sumChanges[$date]['sum'] += $amount * $pricePerGram;
                            }
                        }
                    }
                }
            }
            if (isset($sumChanges[$date])) {
                $sumChanges[$date]['sum'] = number_format($sumChanges[$date]['sum'], 2);
            }
        }

        $amountChangesByMonths = array();
        $preSumChange = 0;
        foreach ($sumChanges as $eachMonth) {
            $eachMonth['change'] = $eachMonth['sum'] - $preSumChange;
            $preSumChange = $eachMonth['sum'];
            $amountChangesByMonths[] = $eachMonth;
        }

        $sumStock = $curInStockValue;
        $sumWaiting = $curWaitingValue;

        $data['amountMaterialInStock'] = number_format($sumStock, 2);
        $data['amountMaterialWaiting'] = number_format($sumWaiting, 2);
        $data['amountChangesByMonths'] = $amountChangesByMonths;

        $sumMaterialInStock = $sumStock;
        $sumMaterialWaiting = $sumWaiting;


        //--------------------------------------------------------------------------------------------------------------
        // Calculate pellet
        $stock = array();
        $waiting = array();

        $stockSection = Business\GenoPellet\Constant::getStockSection();
        $waitingSection = Business\GenoPellet\Constant::getWaitingSection();

        $result = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findAll();
        foreach ($result as $entry) {
            $pelletId = $entry->getIdPellet();
            $receivedDate = $entry->getReceivedDate();

            if ($receivedDate) {
                $interval = date_diff($objLastDecember, $receivedDate);
                $receivedDate = $receivedDate->format('Y/m/01');
                if (intval($interval->format('%R%s')) < 0) {
                    $receivedDate = null;
                }
            }

            if (empty($stock[$pelletId])) {
                $stock[$pelletId] = 0;
            }
            if (empty($waiting[$pelletId])) {
                $waiting[$pelletId] = 0;
            }

            if ($entry->getReceived() == 1 &&
                !$entry->getIsSelected() &&
                in_array($entry->getSection(), $stockSection)) {
                $stock[$pelletId] += $entry->getLoading();  //getCup...//gram
                if ($receivedDate) {
                    $objDate = \DateTime::createFromFormat('Y/m/d', $receivedDate);
                    $receivedDate = $objDate->format('Y/m/d');
                    if (empty($stockChanges[$receivedDate])) {
                        $stockChanges[$receivedDate] = array();
                    }
                    if (empty($stockChanges[$receivedDate][$pelletId])) {
                        $stockChanges[$receivedDate][$pelletId] = 0;
                    }
                    $stockChanges[$receivedDate][$pelletId] += $entry->getLoading();
                }
            } else if (!$entry->getReceived() &&
                in_array($entry->getSection(), $waitingSection)) {
                $waiting[$pelletId] += $entry->getLoading();
            }
        }

        // Compute price
        $pelletPrices = array();
        $result = $em->getRepository('AppDatabaseMainBundle:GenoPellet')->findAll();
        foreach ($result as $m) {
            $pelletPrices[$m->getId()] = floatval($m->getPricePerGram());
        }

        $sumStock = 0;
        $sumWaiting = 0;
        $sumChanges = array();

        $date = $lastYear . '/12/01';
        $objDate = \DateTime::createFromFormat('Y/m/d', $date);
        $sumChanges[$objDate->format('Y/m/d')] = array(
                                    'view'      => 0,
                                    'change'    => 0,
                                    'sum'       => 0,
                                    'name'      => ucfirst($objDate->format('M Y'))
                            );

        for ($i = 1; $i <= 12; $i++) {
            $date = $thisYear . '/' . $i . '/01';
            $objDate = \DateTime::createFromFormat('Y/m/d', $date);
            $sumChanges[$objDate->format('Y/m/d')] = array(
                                        'view'      => 1,
                                        'change'    => 0,
                                        'sum'       => 0,
                                        'name'      => ucfirst($objDate->format('M Y'))
                                );
        }

        foreach ($stockChanges as $date => $arrPellets) {
            foreach ($arrPellets as $pelletId => $amout) {
                $pricePerGram = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
                if (isset($sumChanges[$date])) {
                    $sumChanges[$date]['sum'] += $amout * $pricePerGram;
                }
            }
            if (isset($sumChanges[$date])) {
                $sumChanges[$date]['sum'] = number_format($sumChanges[$date]['sum'], 2);
            }
        }

        $amountChangesByMonths = array();
        $preSumChange = 0;
        foreach ($sumChanges as $eachMonth) {
            $eachMonth['change'] = $eachMonth['sum'] - $preSumChange;
            $preSumChange = $eachMonth['sum'];
            $amountChangesByMonths[] = $eachMonth;
        }

        foreach ($stock as $pelletId => $num) {
            $pricePerGram = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumStock += $num * $pricePerGram;
        }
        foreach ($waiting as $pelletId => $num) {
            $pricePerGram = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumWaiting += $num * $pricePerGram;
        }

        $data['amountPelletInStock'] = number_format($sumStock, 2);
        $data['amountPelletWaiting'] = number_format($sumWaiting, 2);

        $amountInStock = $sumMaterialInStock + $sumStock;
        $data['amountInStock'] = number_format($amountInStock, 2);

        $amountWaiting = $sumMaterialWaiting + $sumWaiting;
        $data['amountWaiting'] = number_format($amountWaiting, 2);
    }
}
