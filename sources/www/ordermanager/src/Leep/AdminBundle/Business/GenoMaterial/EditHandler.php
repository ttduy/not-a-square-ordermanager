<?php
namespace Leep\AdminBundle\Business\GenoMaterial;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\GenoMaterialType;

class EditHandler extends AppEditHandler{
    public $attachmentKey = null;
    const MAX_TARGET_LOAD = 1.0;
    const MAX_PERCENT = 100;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if (empty($this->attachmentKey)) {
            $success = false;
            while (!$success) {
                $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
                $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
                if (!$duplicate) {
                    $success = true;
                }
            }
        }
    }

    public function loadEntity($request){
        // get id 's request' geno material
        $id = $request->query->get('id');
        // return the object
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity){
        // create model
        $model = new EditModel();
        // create entity
        $em = $this->container->get('doctrine')->getEntityManager();

        // check if attached key is empty
        $entityAttachmentKey = $entity->getAttachmentKey();
        if(empty($entityAttachmentKey)){
            $this->generateAttachmentKey();
            $entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }

        $this->attachmentKey = $entity->getAttachmentKey();

        // target load only have value in [0, 1] interval
        $targetLoad = floatval($entity->getTargetLoad());

        // let's convert
        $model->attachmentKey = $entity->getAttachmentKey();
        $model->shortName = $entity->getShortName();
        $model->fullName = $entity->getFullName();
        $model->targetLoad = $targetLoad;
        $model->warningThreshold = $entity->getWarningThreshold();
        $model->storageConditions = $entity->getStorageConditions();
        $model->type = $entity->getType();
        $model->unitsInStock = $entity->getUnitsInStock();
        $model->unitDescription = $entity->getUnitDescription();
        $model->isCriticalSubstance = $entity->getIsCriticalSubstance();

        // get the file
        $model->fileAttachment = array();
        $model->fileAttachment['file'] = $entity->getFileAttachment();

        // Gneo substance
        $model->substanceFullName = $entity->getSubstanceFullName();
        $model->substanceTextBlock = $entity->getSubstanceTextBlock();

        // return model
        return $model;
    }

    public function buildform($builder){
        // section attachment key
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genomaterials/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        // create mapping to get more library
        $mapping = $this->container->get('easy_mapping');

        // ==========================
        // Section material
        $builder->add('sectionGenoMaterial','section', array(
            'label' => 'Geno Material',
            'property_path' => false
        ));

        /// short name
        $builder->add('shortName', 'text', array(
            'label' => 'Short name',
            'required' => true
        ));

        /// full name
        $builder->add('fullName', 'text', array(
            'label' => 'Full name',
            'required' => false
        ));
        /// number target load
        $builder->add('targetLoad', 'text', array(
            'label' => 'Target load',
            'required' => false
        ));

        /// warning threshold: number
        $builder->add('warningThreshold', 'text', array(
            'label' => 'Warning Threshold',
            'required' => false
        ));

        /// storage condidtion
        $builder->add('storageConditions', 'text', array(
            'label' => 'Storage conditions',
            'required' => false
        ));

        /// type number
        $builder->add('type', 'choice', array(
            'label' => 'Type',
            'required' => true,
            'choices' => [0 => ''] + Business\GenoMaterialType\Utils::getGenoMaterialTypeList($this->container),
            'empty_value' => false
        ));

        /// Units in stock
        $builder->add('unitsInStock', 'text', array(
            'label' => 'Units in stock',
            'required' => false
        ));

        /// Unit description
        $builder->add('unitDescription', 'textarea', array(
            'label' => 'Unit description',
            'required' => false,
            'attr' => [
                'style' =>  'width:600px',
                'rows' =>   10
            ]
        ));

        /// check if geno matrial is critical subtance
        $builder->add('isCriticalSubstance', 'checkbox', array(
            'label' =>      'Is critical subtance?',
            'required' =>   false
        ));

        $builder->add('enableCollectStorageSampleWarning', 'checkbox', array(
            'label' => 'Enable collect storage sample warning?',
            'required' =>   false
        ));

        $builder->add('sectionSubstance','section', array(
            'label' => 'Geno Substance',
            'property_path' => false
        ));
        $builder->add('substanceFullName', 'text', array(
            'label' => 'Full English substance name',
            'required' => false
        ));
        $builder->add('substanceTextBlock', 'text', array(
            'label' => 'Text block',
            'required' => false
        ));
    }

    public function onSuccess(){
        // get doctrine and form data
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // get user manager
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        // Chekc target load is invalid?
        $targetLoad = floatval($model->targetLoad);

        /// Assign form data form
        $this->entity->setShortName(trim($model->shortName));
        $this->entity->setFullName(trim($model->fullName));
        $this->entity->setTargetLoad($targetLoad);
        $this->entity->setWarningThreshold(intval($model->warningThreshold));
        $this->entity->setStorageConditions($model->storageConditions);
        $this->entity->setType($model->type);
        $this->entity->setUnitsInStock(intval($model->unitsInStock));
        $this->entity->setUnitDescription($model->unitDescription);
        $this->entity->setIsCriticalSubstance($model->isCriticalSubstance);
        $this->entity->setEnableCollectStorageSampleWarning($model->enableCollectStorageSampleWarning);

        /// Assign from system data: modified date, modified by, created date
        $this->entity->setModifiedDate(new \DateTime());
        $this->entity->setModifiedBy($userManager->getUser()->getId());

        // Geno susbtance
        $this->entity->setSubstanceFullName($model->substanceFullName);
        $this->entity->setSubstanceTextBlock($model->substanceTextBlock);

        // write to database
        $em->flush();

        parent::onSuccess();
    }
}
?>
