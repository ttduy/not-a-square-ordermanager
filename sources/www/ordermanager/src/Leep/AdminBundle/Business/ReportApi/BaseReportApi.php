<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class BaseReportApi {
    public $container = null;
    public $worker = null;
    public function __construct($container) {
        $this->container = $container;
    }

    public function get($service) {
        return $this->container->get($service);
    }

    public function getParameter($parameter) {
        return $this->container->getParameter($parameter);
    }

    public function execute() {
    }

    public function authenticate() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $apiKey = $request->get('apiKey', '');

        $worker = $em->getRepository('AppDatabaseMainBundle:Worker')->findOneByApiKey(trim($apiKey));
        if ($worker) {
            $worker->setLastCheckin(new \DateTime());
            $em->flush();
            $this->worker = $worker;
            return true;
        }
        return false;
    }


    //////////////////////////////////////////////
    // x. SHARED FUNCTION
    public function profileFile(&$arr, $absFile, $file) {
        if (file_exists($absFile)) {
            $arr[] = array(
                'file'         => utf8_encode($file),
                'lastModified' => filemtime($absFile),
                'filesize'     => filesize($absFile)
            );
        }
    }
}