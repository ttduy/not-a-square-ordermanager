<?php
namespace Leep\AdminBundle\Business\ReportApi;

class CheckResourcesApi extends BaseReportApi {
    public function execute() {
        $reportUtils = $this->get('leep_admin.report.business.util');
        $attachmentDir = $this->getParameter('attachment_dir');

        $reportResources = array();

        // Resources
        $resources = $reportUtils->getResources();
        foreach ($resources as $resourceFile) {
            $this->profileFile($reportResources,
                $attachmentDir.'/report_resource'.$resourceFile, 
                '/report_resource'.$resourceFile
            );
        }

        // External PDF
        $externalPdfs = $reportUtils->getExternalPdf();
        foreach ($externalPdfs as $externalPdf) {
            $this->profileFile($reportResources,
                $attachmentDir.'/external_pdf'.$externalPdf,
                '/external_pdf'.$externalPdf
            );
        }

        // Third party PDF
        $thirdPartyPdfs = $reportUtils->getThirdPartyPdf();
        foreach ($thirdPartyPdfs as $thirdPartyPdf) {
            $this->profileFile($reportResources,
                $attachmentDir.'/third_party_pdf'.$thirdPartyPdf,
                '/third_party_pdf'.$thirdPartyPdf
            );
        }

        return $reportResources;
    }
}
