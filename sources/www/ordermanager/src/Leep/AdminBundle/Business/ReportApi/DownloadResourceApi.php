<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadResourceApi extends BaseReportApi {
    protected function cleanupFilename($name) {
        $name = str_replace('..', '', $name);
        return $name;
    }

    public function execute() {
        $request = $this->get('request');
        $resourceFile = $request->get('file');

        $attachmentDir = $this->getParameter('attachment_dir');
        $downloadFile = $attachmentDir.'/'.$this->cleanupFilename($resourceFile);

        $fileResponse = new BinaryFileResponse($downloadFile);          
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $fileResponse;
    }
}