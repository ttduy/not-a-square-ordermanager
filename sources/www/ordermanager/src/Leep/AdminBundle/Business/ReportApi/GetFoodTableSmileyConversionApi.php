<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetFoodTableSmileyConversionApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();

        $smileyConversions = array();
        $results = $em->getRepository('AppDatabaseMainBundle:FoodTableSmileyConversion')->findAll();
        foreach ($results as $r) {
            $smileyConversions[] = array(
                'from' => $r->getFromValue(),
                'to'   => $r->getToValue(),
                'value' => $r->getNormalizedValue(),
                'text' => $r->getText()
            );
        }

        return $smileyConversions;
    }
}
