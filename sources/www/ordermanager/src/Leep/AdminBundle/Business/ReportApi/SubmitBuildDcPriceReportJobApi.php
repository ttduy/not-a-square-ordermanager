<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SubmitBuildDcPriceReportJobApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReportQueue = $request->get('id', 0);
        $filename = $request->get('filename', '123');

        $job = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if (!empty($job) && $job->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING) {
            $output = array();
            $output['filename'] = $filename;
            // $now = new \DateTime();

            // $job->setProcessingTime($now->getTimestamp() - $job->getStartTime()->getTimestamp());
            // $job->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
            // $job->setEndTime($now);
            $job->setOutput(serialize($output));
            $em->flush();

            return "SUCCESS";
        }

        return "ERROR";
    }
}
