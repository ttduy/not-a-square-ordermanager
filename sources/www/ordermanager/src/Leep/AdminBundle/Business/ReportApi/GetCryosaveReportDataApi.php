<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business;

class GetCryosaveReportDataApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReport = $request->get('idReport', 0);
        $idCryosaveSample = $request->get('idCryosaveSample', 0);
        
        $cryosaveSample = $em->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneById($idCryosaveSample);
        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($report->getIdFormulaTemplate());

        $sampleData = Business\CryosaveSample\Utils::loadSampleData($this->container, $cryosaveSample, true);
        $finalData = $sampleData;

        $executor = $this->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $sampleData);
        $errors = $executor->errors;
        $output = $executor->output;
        if (!empty($errors)) {
            $errorMessage = '';
            $errorMessage .= "\n".'COMPILE ERROR -----------'."\n";
            $errorMessage .= implode("\n", $errors);
            $errorMessage .= "\n".'------------------------'."\n";

            return array(
                'result' => 'ERROR',
                'msg'    => $errorMessage
            );
        }
        else {                
            foreach ($output as $k => $v) {
                $finalData[$k] = $v;
            }
        }

        return array(
            'result'        => 'OK',
            'sampleData'  => $sampleData,
            'data'          => $finalData
        );
    }
}
