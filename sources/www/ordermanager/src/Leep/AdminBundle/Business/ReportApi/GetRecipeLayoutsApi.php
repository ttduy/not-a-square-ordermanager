<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetRecipeLayoutsApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();

        $layouts = array();

        $recipeLayouts = $em->getRepository('AppDatabaseMainBundle:RecipeBookLayout')->findAll();
        foreach ($recipeLayouts as $r) {
            $layouts[] = array(
                'id'   => $r->getName(),
                'data' => $r->getData()
            );
        }
        return $layouts;
    }
}
