<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetReportApi extends BaseReportApi {
    public function loadReport($idReport) {
        $em = $this->get('doctrine')->getEntityManager();
        // Load report
        $report = array();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $idReport)
            ->orderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();
        foreach ($sections as $section) {
            if ($section->getIsHide() == 1) {
                continue;
            }

            $sectionName = $section->getName();
            $sectionAlias = $section->getAlias();
            if ($section->getIdType() == 2) { // Include external report section
                $idSection = $section->getIdReportSectionLink();
                $linkedSection = $em->getRepository('AppDatabaseMainBundle:ReportSection')->findOneById($idSection);
                if (!$linkedSection) {
                    return ["ERROR" => "Error: empty linked report section #$idSection from section #".$section->getId()." (".$section->getName().")"];
                }

                $sectionName = $linkedSection->getName();
            } else if ($section->getIdType() == 3) { // Include external recipes
                $idReportLink = $section->getIdReportLink();

                $subReport = $this->loadReport($idReportLink);
                foreach ($subReport as $item) {
                    $report[] = $item;
                }
                continue;
            }
            else {
                $idSection = $section->getId();
            }


            // Load items for this section
            $sectionItems = array();

            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $idSection)
                ->orderBy('p.sortOrder', 'ASC');
            $reportItems = $query->getQuery()->getResult();
            foreach ($reportItems as $reportItem) {
                $idTextToolType = $reportItem->getIdTextToolType();
                $data = json_decode($reportItem->getData(), true);

                $sectionItems[] = array(
                    'type' => $idTextToolType,
                    'data' => $data
                );
            }

            $report[] = array(
                'items' => $sectionItems,
                'name'  => $sectionName,
                'alias' => $sectionAlias
            );
        }
        return $report;
    }

    public function execute() {
        $request = $this->get('request');
        $idReport = $request->get('id', 0);

        $report = $this->loadReport($idReport);

        return $report;
    }
}