<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadReportFileApi extends BaseReportApi {
    protected function cleanupFilename($name) {
        $name = str_replace('..', '', $name);
        return $name;
    }

    public function execute() {
        $request = $this->get('request');
        $resourceFile = $request->get('file');

        $reportDir = $this->getParameter('kernel.root_dir').'/../web/report';
        $tempDir = $this->getParameter('kernel.root_dir').'/../files/temp';
        $downloadFile = $reportDir.'/'.$this->cleanupFilename($resourceFile);
        if (!is_file($downloadFile)) {
            $awsHelper = $this->get('leep_admin.helper.aws_backup_files_helper');
            $result = $awsHelper->temporaryDownloadBackupFiles('report', $this->cleanupFilename($resourceFile));
            if ($result) {
                $downloadFile = "$tempDir/".$this->cleanupFilename($resourceFile);
            }
        }

        $fileResponse = new BinaryFileResponse($downloadFile);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $fileResponse;
    }
}