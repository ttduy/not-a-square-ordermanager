<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetRecipeApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();
        $idRecipe = $this->get('request')->get('idRecipe', '');

        $recipe = $em->getRepository('AppDatabaseMainBundle:Recipe')->findOneByRecipeId($idRecipe);

        if ($recipe) {
            return array(
                'recipeId'    => $recipe->getRecipeId(),
                'name'        => $recipe->getName(),
                'ingredient'  => $recipe->getIngredient(),
                'instruction' => $recipe->getInstruction(),
                'imageFile'   => $recipe->getImageFile(),
                'preparationTime' => $recipe->getPreparationTime(),
                'carb'        => $recipe->getCarb(),
                'prot'        => $recipe->getProt(),
                'fat'         => $recipe->getFat(),
                'isVegan'     => $recipe->getIsVegan(),
                'foodIngredients' => $recipe->getFoodIngredients(),
            );
        }

        return array();
    }
}
