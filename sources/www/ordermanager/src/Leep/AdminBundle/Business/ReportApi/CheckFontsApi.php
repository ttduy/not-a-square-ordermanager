<?php
namespace Leep\AdminBundle\Business\ReportApi;

class CheckFontsApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();
        $request = $this->get('request');

        $fonts = array();
        $fontDir = $this->getParameter('kernel.root_dir').'/../vendor/tecnick.com/tcpdf/fonts';

        $applicationFonts = $em->getRepository('AppDatabaseMainBundle:ApplicationFont')->findAll();
        foreach ($applicationFonts as $applicationFont) {
            $code = $applicationFont->getTcpdfCode();

            $this->profileFile($fonts, $fontDir.'/'.$code.'.php', '/'.$code.'.php');
            $this->profileFile($fonts, $fontDir.'/'.$code.'.z', '/'.$code.'.z');
            $this->profileFile($fonts, $fontDir.'/'.$code.'.ctg.z', '/'.$code.'.ctg.z');
        }

        $this->profileFile($fonts, $fontDir.'/custom1.php', '/custom1.php');
        $this->profileFile($fonts, $fontDir.'/custom1.z', '/custom1.z');
        $this->profileFile($fonts, $fontDir.'/custom1.ctg.z', '/custom1.ctg.z');

        $this->profileFile($fonts, $fontDir.'/customawsome.php', '/customawsome.php');
        $this->profileFile($fonts, $fontDir.'/customawsome.z', '/customawsome.z');
        $this->profileFile($fonts, $fontDir.'/customawsome.ctg.z', '/customawsome.ctg.z');

        return $fonts;
    }
}
