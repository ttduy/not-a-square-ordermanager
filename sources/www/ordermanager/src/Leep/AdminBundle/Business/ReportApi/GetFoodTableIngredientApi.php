<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetFoodTableIngredientApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();

        $foodTableIngredients = array();
        $ingredients = $em->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
        foreach ($ingredients as $ingredient) {
            $foodTableIngredients[$ingredient->getVariableName()] = array(
                'id'            => $ingredient->getId(),
                'name'          => $ingredient->getName(),
                'riskAmountMax' => $ingredient->getRiskAmountMax()
            );
        }

        return $foodTableIngredients;
    }
}
