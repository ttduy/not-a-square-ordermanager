<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business\TextBlock\Constants;

class CheckSyncRequestApi extends BaseReportApi {
    public function execute() {        
        $em = $this->get('doctrine')->getEntityManager();
        $records = $em->getRepository('AppDatabaseMainBundle:SyncRequest')->findAll();
        if (empty($records)) {
            return array('isSyncTextBlock' => false, 'isSyncTextBlockStatus' => 0, 'isSyncResources' => false, 'isSyncResourcesStatus' => 0);
        }    
        $syncRequest = array_pop($records);

        return array(
            'isSyncTextBlock'         => $syncRequest->getIsSyncTextBlock(),
            'isSyncTextBlockStatus'   => $syncRequest->getIsSyncTextBlockStatus(),
            'isSyncResources'         => $syncRequest->getIsSyncResources(),
            'isSyncResourcesStatus'   => $syncRequest->getIsSyncResourcesStatus()
        );
    }
}
