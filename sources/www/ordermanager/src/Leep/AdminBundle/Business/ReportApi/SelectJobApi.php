<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SelectJobApi extends BaseReportApi {
    public function selectReportQueueJob() {
        $em = $this->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->andWhere('p.status = :pendingStatus')
            ->orderBy('p.idPriority DESC, p.inputTime', 'ASC')
            ->setMaxResults(1)
            ->setParameter('pendingStatus', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);

        $isLockReport = $this->get('service_container')->getParameter('isLockReport');
        if ($isLockReport) {
            $idWebUserLockReport = $this->get('service_container')->getParameter('idWebUserLockReport');
            $query->andWhere('p.idWebUser = :idWebUser')
                ->setParameter('idWebUser', $idWebUserLockReport);
        }

        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            foreach ($results as $job) {
                $now = new \DateTime();

                if ($job->getIdJobType() ==  Business\ReportQueue\Constant::JOB_TYPE_BUILD_SHIPMENT) {
                    try {
                        $input = unserialize($job->getInput());
                        $idBodShipment = $input['idBodShipment'];
                        $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($idBodShipment);
                        if ($bodShipment) {
                            $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_PROCESSING);
                            $em->persist($bodShipment);
                            $em->flush();
                        }
                    } catch (\Exception $e) {}
                }

                $job->setIdWorker($this->worker->getId());
                $job->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING);
                $job->setStartTime($now);
                $em->flush();

                $log = new Entity\ReportQueueLog();
                $log->setLogTime($now);
                $log->setIdReportQueue($job->getId());
                $log->setLogText("Assign worker: ".$this->worker->getName());
                $em->persist($log);
                $em->flush();

                return $job;
            }
        }

        return null;
    }

    public function getTotalActiveJob() {
        $em = $this->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) AS totalActiveJob')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->andWhere('p.status = :statusProcessing')
            ->andWhere('p.idWorker = :idWorker')
            ->setParameter('statusProcessing', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING)
            ->setParameter('idWorker', $this->worker->getId());
        $totalActiveJob = intval($query->getQuery()->getSingleScalarResult());

        return $totalActiveJob;
    }

    public function execute() {
        if ($this->worker->getTotalProcess() <= $this->getTotalActiveJob()) {
            return "Too many job";
        }

        $job = $this->selectReportQueueJob();
        if (empty($job)) {
            return "No job available";
        }

        $input = unserialize($job->getInput());
        $input['id'] = $job->getId();
        $input['idJobType'] = $job->getIdJobType();
        return $input;
    }
}
