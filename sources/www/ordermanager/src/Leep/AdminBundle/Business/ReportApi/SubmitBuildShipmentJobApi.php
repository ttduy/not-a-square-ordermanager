<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SubmitBuildShipmentJobApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReportQueue = $request->get('id', 0);
        $highresFilename = $request->get('highresFilename', '');
        $lowresFilename = $request->get('lowresFilename', '');

        $job = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if (!empty($job) && $job->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING) {
            $output = array();
            $output['filename'] = $highresFilename;
            $output['filename_low_resolution'] = $lowresFilename;
            // $now = new \DateTime();

            // $job->setProcessingTime($now->getTimestamp() - $job->getStartTime()->getTimestamp());
            // $job->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
            // $job->setEndTime($now);
            $job->setOutput(serialize($output));
            $em->flush();

            $input = unserialize($job->getInput());

            $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($input['idBodShipment']);
            $bodShipment->setHighResolutionFile($highresFilename);
            $bodShipment->setLowResolutionFile($lowresFilename);
            $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_GENERATE_FINISHED);
            $em->flush();

            return "SUCCESS";
        }

        return "ERROR";
    }
}
