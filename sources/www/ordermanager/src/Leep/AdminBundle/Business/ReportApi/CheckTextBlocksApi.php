<?php
namespace Leep\AdminBundle\Business\ReportApi;

class CheckTextBlocksApi extends BaseReportApi {
    public function execute() {
        ini_set('memory_limit', '2048M');
        
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $idLanguage = $request->get('idLanguage', 0);

        $query = $em->createQueryBuilder();
        $query->select('e.idLanguage, p.name, e.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'e')
            ->innerJoin('AppDatabaseMainBundle:TextBlock', 'p', 'WITH', 'e.idTextBlock = p.id')
            ->andWhere('e.idLanguage = :idLanguage')
            ->setParameter('idLanguage', $idLanguage);
        $results = $query->getQuery()->getResult();

        $blocks = array();
        foreach ($results as $row) {
            $blocks[] = array(
                'name'  => utf8_encode($row['name']),
                'body'  => utf8_encode($row['body'])
            );
        }

        return $blocks;
    }
}
