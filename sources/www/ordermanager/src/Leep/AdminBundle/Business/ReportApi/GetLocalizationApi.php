<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetLocalizationApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();
        $idLanguage = $this->get('request')->get('idLanguage', 0);

        $reportLocalization = $em->getRepository('AppDatabaseMainBundle:ReportLocalization')->findOneByIdLanguage($idLanguage);
        return $reportLocalization->getLocalization();
    }
}
