<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetLanguageFontApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idLanguage = $request->get('idLanguage', 0);
        $languageFont = $em->getRepository('AppDatabaseMainBundle:ReportLanguageFont')->findOneByIdLanguage($idLanguage);
        if (empty($languageFont)) {
            return array();
        }

        $applicationFonts = array();
        $results = $em->getRepository('AppDatabaseMainBundle:ApplicationFont')->findAll();
        foreach ($results as $r) {
            $applicationFonts[$r->getId()] = $r->getTcpdfCode();
        }

        return array(
            'regular'        => isset($applicationFonts[$languageFont->getIdRegular()]) ? $applicationFonts[$languageFont->getIdRegular()] : '',
            'regular_italic' => isset($applicationFonts[$languageFont->getIdRegularItalic()]) ? $applicationFonts[$languageFont->getIdRegularItalic()] : '',
            'bold'           => isset($applicationFonts[$languageFont->getIdBold()]) ? $applicationFonts[$languageFont->getIdBold()] : '',
            'light'          => isset($applicationFonts[$languageFont->getIdLight()]) ? $applicationFonts[$languageFont->getIdLight()] : '',
            'light_italic'   => isset($applicationFonts[$languageFont->getIdLightItalic()]) ? $applicationFonts[$languageFont->getIdLightItalic()] : '',
            'medium'         => isset($applicationFonts[$languageFont->getIdMedium()]) ? $applicationFonts[$languageFont->getIdMedium()] : '',
            'thin'           => isset($applicationFonts[$languageFont->getIdThin()]) ? $applicationFonts[$languageFont->getIdThin()] : '',
            'palatino'       => isset($applicationFonts[$languageFont->getIdSpecial()]) ? $applicationFonts[$languageFont->getIdSpecial()] : '',   
        );
    }
}
