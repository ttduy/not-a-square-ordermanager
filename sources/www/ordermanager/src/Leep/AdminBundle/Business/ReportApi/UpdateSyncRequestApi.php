<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business\TextBlock\Constants;

class UpdateSyncRequestApi extends BaseReportApi {
    public function execute() {      
        $request = $this->get('request');

        $isSyncTextBlockStatus = $request->get('isSyncTextBlockStatus', false);
        $isSyncResourcesStatus = $request->get('isSyncResourcesStatus', false);

        $em = $this->get('doctrine')->getEntityManager();
        $records = $em->getRepository('AppDatabaseMainBundle:SyncRequest')->findAll();
        if (empty($records)) {
            return 'ok';
        }    
        $syncRequest = array_pop($records);

        if (!empty($isSyncTextBlockStatus)) {
            $syncRequest->setIsSyncTextBlockStatus($isSyncTextBlockStatus);
            $syncRequest->setLastUpdated(new \DateTime());
        }
        if (!empty($isSyncResourcesStatus)) {
            $syncRequest->setIsSyncResourcesStatus($isSyncResourcesStatus);
            $syncRequest->setLastUpdated(new \DateTime());
        }

        $em->flush();

        return 'ok';
    }
}
