<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetFoodTableItemApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();
        $request = $this->get('request');

        $foodItems = array('A');
        $foodTableItems = $request->get('foodItems', '');
        $results = explode(",", $foodTableItems);
        foreach ($results as $itemId) {
            $foodItems[] = trim($itemId);
        }


        $foodTableItems = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FoodTableFoodItem', 'p')
            ->andWhere('p.foodId in (:foodTableItems)')
            ->setParameter('foodTableItems', $foodItems);
        $results = $query->getQuery()->getResult();
        foreach ($results as $foodItem) {
            $foodTableItems[$foodItem->getFoodId()] = array(
                'name'        => $foodItem->getName(),
                'ingredients' => $this->parseIngredient($foodItem->getIngredientData())
            );
        }

        return $foodTableItems;
    }

    protected function parseIngredient($ingredientData) {
        $rows = explode("\n", $ingredientData);
        $ingredients = array();
        foreach ($rows as $r) {
            $arr = explode('=', $r);
            if (count($arr) == 2) {
                $ingredients[trim($arr[0])] = floatval($arr[1]);
            }
        }
        return $ingredients;
    }
}
