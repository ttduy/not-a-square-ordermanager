<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetTextBlockApi extends BaseReportApi {
    public function execute() {        
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $textBlocks = $request->get('t', array());
        if (empty($textBlocks)) {
            return array();
        }
        $formatter = $this->get('leep_admin.helper.formatter');

        $query = $em->createQueryBuilder();
        $query->select('p.id, e.idLanguage, p.name, e.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'e')
            ->innerJoin('AppDatabaseMainBundle:TextBlock', 'p', 'WITH', 'e.idTextBlock = p.id')
            ->andWhere('p.name in (:textBlocks)')
            ->setParameter('textBlocks', $textBlocks);
        $results = $query->getQuery()->getResult();

        $blocks = array();
        foreach ($results as $row) {
            $idTextBlock = intval($row['id']);
            $idLanguage = intval($row['idLanguage']);

            if (!isset($blocks[$idTextBlock])) {
                $blocks[$idTextBlock] = array(
                    'name' => utf8_encode($row['name']),
                    'text' => array()
                );
            }

            $languageCode = $formatter->format($idLanguage, 'mapping', 'LeepAdmin_Language_List');
            if ($languageCode != '') {
                $blocks[$idTextBlock]['text'][$languageCode] = utf8_encode($row['body']);
            }
        }

        return $blocks;
    }
}
