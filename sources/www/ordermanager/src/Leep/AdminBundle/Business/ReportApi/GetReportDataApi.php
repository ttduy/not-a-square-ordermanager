<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business;

class GetReportDataApi extends BaseReportApi {
    public function execute() {
        set_time_limit(7200); # FIX ME: this is dangerous!
        ini_set('memory_limit', '3048M');

        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReport = $request->get('idReport', 0);
        $idOrder = $request->get('idCustomer', 0);

        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
        $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($report->getIdFormulaTemplate());

        $orderData = Business\CustomerReport\Utils::loadOrderData($this->container, $idOrder);
        $orderData = $this->parseReportData($orderData);
        $orderData['REPORT_CODE'] = $report->getReportCode();
        $finalData = $orderData;

        $executor = $this->get('leep_admin.formula_template.business.executor');
        $executor->execute($formulaTemplate->getFormula(), $orderData);
        $errors = $executor->errors;
        $output = $executor->output;
        if (!empty($errors)) {
            $errorMessage = '';
            $errorMessage .= "\n".'COMPILE ERROR -----------'."\n";
            $errorMessage .= implode("\n", $errors);
            $errorMessage .= "\n".'------------------------'."\n";

            return array(
                'result' => 'ERROR',
                'msg'    => $errorMessage
            );
        }
        else {
            foreach ($output as $k => $v) {
                $finalData[$k] = $v;
            }
        }

        return array(
            'result'        => 'OK',
            'customerData'  => $orderData,
            'data'          => $finalData
        );
    }

    protected function parseReportData($reportData) {
        $data = array();
        $rows = explode("\n", $reportData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $data[trim($arr[0])] = trim($arr[1]);
            }
        }
        return $data;
    }
}
