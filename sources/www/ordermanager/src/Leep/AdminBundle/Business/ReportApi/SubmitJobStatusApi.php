<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SubmitJobStatusApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $msg = unserialize($request->get('msg', ''));
        $idReportQueue = $request->get('id', 0);

        $log = new Entity\ReportQueueLog();
        $log->setLogTime(new \DateTime());
        $log->setIdReportQueue($idReportQueue);
        $log->setLogText($msg['status'].' '.$msg['msg']);
        $em->persist($log);
        $em->flush();

        $reportQueue = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        $reportQueue->setLastUpdated(new \DateTime());
        $now = new \DateTime();
        $reportQueue->setProcessingTime($now->getTimestamp() - $reportQueue->getStartTime()->getTimestamp());
        if ($reportQueue->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING) {
            return "ERROR";
        }

        switch ($msg['status']) {
            case 'OK':
                $percentage = floatval($request->get('percentage', 0));
                if ($percentage > 0) {
                    $reportQueue->setProgress($percentage);
                }
                break;
            case 'WARNING':
                $reportQueue->setIsWarning(1);
                break;
            case 'DONE':
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
                $reportQueue->setEndTime($now);
                $reportQueue->setProgress(100);
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
                break;
            case 'ERROR':
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_ERROR);
                break;
            case 'READY':
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD);
            default:
                break;
        }

        $em->flush();
        if ($msg['status'] == 'ERROR') {
            try {
                $input = unserialize($reportQueue->getInput());
                $idBodShipment = $input['idBodShipment'];
                $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($idBodShipment);
                if ($bodShipment) {
                    $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_GENERATE_ERROR);
                    $em->persist($bodShipment);
                    $em->flush();
                }
            } catch (\Exception $e) {}
        }

        return "SUCCESS";
    }
}
