<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Business;

class GetBodShipmentApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $id = $request->get('id', 0);
        $data = array();

        $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($id);
        $data['idCustomer'] = $bodShipment->getIdCustomer();
        $data['customerOrderNumber'] = $bodShipment->getBodCustomerOrderNumber();
        $data['fromCompany'] = $bodShipment->getBodFromCompany();
        $data['fromPerson'] = $bodShipment->getBodFromPerson();
        $data['fromEmail'] = $bodShipment->getBodFromEmail();
        $data['contributorRole'] = $bodShipment->getBodContributorRole();
        $data['contributorName'] = $bodShipment->getBodContributorName();
        $data['itemTitle'] = $bodShipment->getBodItemTitle();
        $data['itemPaper'] = $bodShipment->getBodItemPaper();
        $data['itemBinding'] = $bodShipment->getBodItemBinding();
        $data['itemBack'] = $bodShipment->getBodItemBack();
        $data['itemFinish'] = $bodShipment->getBodItemFinish();
        $data['itemJacket'] = $bodShipment->getBodItemJacket();
        $data['orderNumber'] = $bodShipment->getBodOrderNumber();
        $data['orderCopies'] = $bodShipment->getBodOrderCopies();
        $data['orderAddressLine1'] = $bodShipment->getBodOrderAddressLine1();
        $data['orderAddressLine2'] = $bodShipment->getBodOrderAddressLine2();
        $data['orderAddressLine3'] = $bodShipment->getBodOrderAddressLine3();
        $data['orderStreet'] = $bodShipment->getBodOrderStreet();
        $data['orderZip'] = $bodShipment->getBodOrderZip();
        $data['orderCity'] = $bodShipment->getBodOrderCity();
        $data['orderCountry'] = $bodShipment->getBodOrderCountry();
        $data['orderCustomsValue'] = $bodShipment->getBodOrderCustomsValue();
        $data['orderShipping'] = $bodShipment->getBodOrderShipping();
        $data['deliveryNoteIdReport'] = $bodShipment->getBodDeliveryNoteIdReport();
        $data['deliveryNoteIdLanguage'] = $bodShipment->getBodDeliveryNoteIdLanguage();
        $data['customBodAccountNumber'] = $bodShipment->getCustomBodAccountNumber();
        $data['customBodBarcodeNumber'] = $bodShipment->getCustomBodBarcodeNumber();

        if (empty($data['customBodAccountNumber'])) {
            $data['customBodAccountNumber'] = '10273274';
        }

        if (empty($data['customBodBarcodeNumber'])) {
            $data['customBodBarcodeNumber'] = '11';
        }

        $data['books'] = array();
        $bodShipmentBooks = $em->getRepository('AppDatabaseMainBundle:BodShipmentBook')->findByIdBodShipment($id);
        foreach ($bodShipmentBooks as $book) {
            $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($book->getIdReport());
            if (!$customerReport) {
                return ['error' => "Error: Customer report no loger exist. Please create a new report queue!"];
            }

            $data['books'][] = array(
                'id'         => $book->getId(),
                'idReport'   => $customerReport->getIdReport(),
                'name'       => $customerReport->getName(),
                'idLanguage' => $customerReport->getIdLanguage(),
                'reportData' => $this->parseReportData($customerReport->getReportFinalData()),
                'reportFile' => $book->getReportFile()
            );
        }

        return $data;
    }

    protected function parseReportData($reportData) {
        $data = array();
        $rows = explode("\n", $reportData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $data[trim($arr[0])] = trim($arr[1]);
            }
        }
        return $data;
    }
}
