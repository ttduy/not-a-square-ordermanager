<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SubmitBuildCustomerReportJobApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReportQueue = $request->get('id', 0);
        $filename = $request->get('filename', '123');

        $job = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if (!empty($job) && $job->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING) {
            $output = array();
            $output['filename'] = $filename;

            // $job->setProcessingTime($now->getTimestamp() - $job->getStartTime()->getTimestamp());
            // $job->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
            // $job->setEndTime($now);
            $job->setOutput(serialize($output));
            $em->flush();

            $reportData = unserialize($request->get('reportData', ''));
            $customerData = unserialize($request->get('customerData', ''));

            $jobData = unserialize($job->getInput());
            if ($jobData['isSaveReport']) {
                $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($jobData['idReport']);

                $customerReport = new Entity\CustomerReport();
                $customerReport->setName($jobData['name']);
                $customerReport->setIdReport($jobData['idReport']);
                $customerReport->setIdCustomer($jobData['idCustomer']);
                $customerReport->setIdLanguage($jobData['idLanguage']);
                $customerReport->setIdFormulaTemplate($jobData['idFormulaTemplate']);
                $customerReport->setCustomerData($this->encodeReportData($customerData));
                $customerReport->setReportFinalData($this->encodeReportData($reportData));
                $customerReport->setFilename($filename);
                $customerReport->setInputTime(new \DateTime());
                $customerReport->setIsVisibleToCustomer($report->getIsVisibleByDefault());
                $em->persist($customerReport);
                $em->flush();

                $output['idCustomerReport'] = $customerReport->getId();
                $job->setOutput(serialize($output));
                $em->flush();
            }

            return "SUCCESS";
        }

        return "ERROR";
    }

    protected function encodeReportData($dataArr) {
        $arr = array();
        foreach ($dataArr as $k => $v) {
            $arr[] = $k.' = '.$v;
        }
        return implode("\r\n", $arr);
    }
}
