<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class SubmitBuildCryosaveReportJobApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReportQueue = $request->get('id', 0);
        $filename = $request->get('filename', '123');

        $job = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if (!empty($job) && $job->getStatus() == Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING) {
            $input = @unserialize($job->getInput());

            $output = array();
            $output['filename'] = $filename;
            // $now = new \DateTime();

            // $job->setProcessingTime($now->getTimestamp() - $job->getStartTime()->getTimestamp());
            // $job->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED);
            // $job->setEndTime($now);
            $job->setOutput(serialize($output));
            $em->flush();

            $reportData = unserialize($request->get('reportData', ''));
            $sampleData = unserialize($request->get('sampleData', ''));

            $jobData = unserialize($job->getInput());

            $cryosaveSample = $em->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneById($input['idCryosaveSample']);
            $cryosaveSample->setReportFilename($filename);
            $em->persist($cryosaveSample);

            if ($input['status'] != 0) {
                $sortOrder = $this->getMaxSortOrder($cryosaveSample->getId()) + 1;
                $cryosaveSample->setCurrentStatus($input['status']);
                $cryosaveSample->setCurrentStatusDate(new \DateTime());

                $newStatus = new Entity\CryosaveSampleStatus();
                $newStatus->setIdCryosaveSample($cryosaveSample->getId());
                $newStatus->setStatus($input['status']);
                $newStatus->setStatusDate(new \DateTime());
                $newStatus->setSortOrder($sortOrder);
                $em->persist($newStatus);
            }

            $em->flush();

            Business\CryosaveSample\Utils::updateStatusList($this->container, $cryosaveSample);

            return "SUCCESS";
        }

        return "ERROR";
    }

    protected function getMaxSortOrder($idSample) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('MAX(p.sortOrder)')
            ->from('AppDatabaseMainBundle:CryosaveSampleStatus', 'p')
            ->andWhere('p.idCryosaveSample = '.$idSample);
        return $query->getQuery()->getSingleScalarResult();
    }

    protected function encodeReportData($dataArr) {
        $arr = array();
        foreach ($dataArr as $k => $v) {
            $arr[] = $k.' = '.$v;
        }
        return implode("\r\n", $arr);
    }
}
