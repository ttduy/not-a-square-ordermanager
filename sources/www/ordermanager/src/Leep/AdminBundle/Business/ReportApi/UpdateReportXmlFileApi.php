<?php
namespace Leep\AdminBundle\Business\ReportApi;

use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class UpdateReportXmlFileApi extends BaseReportApi {
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $idReportQueue = $request->get('id', 0);
        $filename = trim($request->get('filename', ''));
        $job = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findOneById($idReportQueue);
        if (!empty($job) && !empty($filename)) {
            $output = unserialize($job->getOutput());
            $output['filename_report_xml'] = $filename;
            $job->setOutput(serialize($output));
            $em->flush();

            if (isset($output['idCustomerReport'])) {
                $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($output['idCustomerReport']);
                if ($customerReport) {
                    $customerReport->setXmlFilename($filename);
                }
                $em->flush();
            }

            return "SUCCESS";
        }

        return "ERROR";
    }
}
