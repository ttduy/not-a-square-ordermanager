<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetUntranslatedPatternApi extends BaseReportApi {
    public function execute() {    
        $config = $this->get('leep_admin.helper.common')->getConfig();

        return $config->getUntranslatedPattern();
    }
}
