<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetFoodTableParameterApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();

        $results = $em->getRepository('AppDatabaseMainBundle:FoodTableParameter')->findAll();
        foreach ($results as $r) {
            return $this->get('leep_admin.helper.common')->copyEntityToArray($r);
        }

        return array();
    }
}
