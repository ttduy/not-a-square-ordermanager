<?php
namespace Leep\AdminBundle\Business\ReportApi;

class GetLanguagesApi extends BaseReportApi {
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();

        $results = $em->getRepository('AppDatabaseMainBundle:Language')->findAll();
        $languages = array();

        foreach ($results as $r) {
            if ($r->getIsDeleted() != 1) {
                $languages[$r->getId()] = $r->getName();
            }
        }

        return $languages;
    }
}
