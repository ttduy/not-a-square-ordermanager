<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateAllHandler extends CreateHandler {    
    public $applicableReports = array();
    public $idLanguage = 1;
    public function getFormModel() {
        $model = new CreateModel();
        $model->reports = $this->applicableReports;
        $model->isSaveReport = true;
        $model->idLanguage = $this->idLanguage;
        $model->isUseNewQueue = 1;
        return $model;
    }
    public function execute() {
        $this->onSuccess();
    }
}
