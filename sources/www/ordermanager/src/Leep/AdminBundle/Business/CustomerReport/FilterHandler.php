<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idLanguage = 0;
        $model->idReport = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $idCustomer = $this->container->get('request')->get('id');

        $builder->add('idReport', 'choice', array(
            'label'    => 'Report',
            'required' => false,
            'choices'  => array(0 => 'All') + Utils::getApplicableReports($this->container, $idCustomer),
            'empty_value' => false
        ));
        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Language_List'),
            'empty_value' => false
        ));

        return $builder;
    }
}