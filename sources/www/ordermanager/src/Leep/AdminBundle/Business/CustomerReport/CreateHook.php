<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class CreateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);

        $data['reportsByDC'] = json_encode(Utils::getApplicableReportsByDC($controller, $id));
    }
}
