<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Leep\AdminBundle\Business;

class Utils {
    public static function generateReportPdf($container, $customerReport, $outputToFile = true, $notifier = null) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $builder = $container->get('leep_admin.report.business.builder');
        $builder->init();
        $builder->mode = 'full';
        $builder->applyLanguage($customerReport->getIdLanguage());
        $builder->parseConfigData($customerReport->getReportFinalData());


        // Load report items
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $customerReport->getIdReport())
            ->orderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();

        // Count total sections
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->andWhere('p.isHide = 0')
            ->setParameter('idReport', $customerReport->getIdReport());
        $totalSection = $query->getQuery()->getSingleScalarResult();

        $numberProcessed = 0;
        $tocPageIndex = 0;
        $tocTextTools = array();
        foreach ($sections as $section) {
            if ($section->getIsHide() == 1) {
                continue;
            }

            $aliasVariable = trim('Section'.$section->getAlias());
            if (isset($builder->configData[$aliasVariable]) && ($builder->configData[$aliasVariable] == 'Hide')) {
                continue;
            }

            if ($section->getIdType() == 2) {
                $sectionId = $section->getIdReportSectionLink();
            }
            else {
                $sectionId = $section->getId();
            }

            $isTOC = false;

            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $sectionId)
                ->orderBy('p.sortOrder', 'ASC');
            $result = $query->getQuery()->getResult();
            $textTools = array();
            foreach ($result as $row) {
                if ($row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER ||
                    $row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_LAYOUT) {
                    $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
                }
                else {
                    $textTools[] = array(
                        'type' => $row->getIdTextToolType(),
                        'data' => json_decode($row->getData(), true)
                    );
                }

                if ($row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_UTIL_TOC) {
                    $isTOC = true;
                }
            }

            if ($isTOC) {
                $tocPageIndex = $builder->pdf->getPage() + 1;
                $tocTextTools = $textTools;
            }
            else {
                $builder->prepareNextSection();
                $builder->resetLayout();
                $builder->moveNextPage();

                foreach ($textTools as $textTool) {
                    $builder->addTextTool($textTool['type'], $textTool['data']);
                }
                $builder->endPage();

                // Notify progress
                $numberProcessed++;
                if ($notifier != null) {
                    $notifier->updateProgress($numberProcessed, $totalSection);
                }
            }
        }


        // Post Process
        if (!empty($tocTextTools)) {
            $builder->resetLayout();
            $builder->pdf->setPrintFooter(false);
            //$builder->pdf->startPageGroup();
            $builder->pdf->startPage();

            $startingTOCPage = $builder->pdf->getPage();
            foreach ($tocTextTools as $textTool) {
                $builder->addTextTool($textTool['type'], $textTool['data']);
            }
            $builder->endPage();

            // Notify progress
            $numberProcessed++;
            if ($notifier != null) {
                $notifier->updateProgress($numberProcessed, $totalSection);
            }

            $endingTOCPage = $builder->pdf->getPage();
            for ($i = $startingTOCPage; $i <= $endingTOCPage; $i++) {
                $builder->pdf->movePage($i, $tocPageIndex);
                $tocPageIndex++;
            }
        }

        if ($outputToFile) {
            $outputFile = $container->getParameter('kernel.root_dir').'/../web/report/'.$customerReport->getFilename();
            $builder->getPdf()->Output($outputFile, 'F');
        }
        else {
            return $builder->getPdf();
        }
    }

    public static function getApplicableReportsByDC($container, $idOrder) {
        $em = $container->get('doctrine')->getEntityManager();

        $reports = array();

        $query = $em->createQueryBuilder();
        $query->select('rp.id, rp.name')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
            ->innerJoin('AppDatabaseMainBundle:VisibleGroupReportReport', 'vgr', 'WITH', 'vgr.idVisibleGroupReport = dc.idVisibleGroupReport')
            ->innerJoin('AppDatabaseMainBundle:Report', 'rp', 'WITH', 'rp.id = vgr.idReport')
            ->andWhere('p.id = :idOrder')
            ->setParameter('idOrder', $idOrder)
            ->orderBy('rp.name', 'ASC');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $reports[$r['id']] = $r['name'];
        }

        return $reports;
    }

    public static function getApplicableReports($container, $idCustomer) {
        $mapping = $container->get('easy_mapping');
        $em = $container->get('doctrine')->getEntityManager();

        $reports = array();

        // Select special products
        $hasSpecialReports = false;

        $query = $em->createQueryBuilder();
        $query->select('r.id, r.name, sp.idProduct')
            ->from('AppDatabaseMainBundle:OrderedSpecialProduct', 'op')
            ->innerJoin('AppDatabaseMainBundle:SpecialProduct', 'sp', 'WITH', 'sp.id = op.idSpecialProduct')
            ->innerJoin('AppDatabaseMainBundle:ReportSpecialProduct', 'rp', 'WITH', 'op.idSpecialProduct = rp.idSpecialProduct')
            ->innerJoin('AppDatabaseMainBundle:Report', 'r', 'WITH', 'rp.idReport = r.id')
            ->andWhere('op.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $idCustomer)
            ->orderBy('r.name', 'ASC');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $reports[$r['id']] = $r['name'];
            $idProduct = intval($r['idProduct']);

            $hasSpecialReports = true;
        }

        // Select normal products
        if (!$hasSpecialReports) {
            $query = $em->createQueryBuilder();
            $query->select('r.id, r.name, op.productid')
                ->from('AppDatabaseMainBundle:OrderedProduct', 'op')
                ->innerJoin('AppDatabaseMainBundle:ReportProduct', 'rp', 'WITH', 'op.productid = rp.idProduct')
                ->innerJoin('AppDatabaseMainBundle:Report', 'r', 'WITH', 'rp.idReport = r.id')
                ->andWhere('op.customerid = :idCustomer')
                ->setParameter('idCustomer', $idCustomer)
                ->orderBy('r.name', 'ASC');
            $results = $query->getQuery()->getResult();
            foreach ($results as $r) {
                $idProduct = $r['productid'];
                if (!isset($selectedSpecialProducts[$idProduct])) {
                    $reports[$r['id']] = $r['name'];
                }
            }
        }

        // get reports by selected DC
        $reports += self::getApplicableReportsByDC($container, $idCustomer);

        return $reports;
    }

    public static function loadOrderData($container, $idOrder) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $doctrine = $container->get('doctrine');
        $common = $container->get('leep_admin.helper.common');

        $results = array();

        // Load general info
        $order = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);
        $today = new \DateTime('');
        $dob = $order->getDateOfBirth();
        $age = 0;
        if ($dob) {
            $interval = $today->diff($dob);
            $age = intval($interval->format('%y'));

            $dob = $dob->format('d/m/Y');
        }
        else {
            $dob = '';
        }
        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($order->getDistributionChannelId());
        $customerInfo = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());
        $allCustomerOrder = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findBy(['idCustomerInfo' => $customerInfo->getId()]);
        $productInfoList = $doctrine->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findAll();
        $productNameList = [];
        foreach ($productInfoList as $productInfo) {
            $productNameList[$productInfo->getId()] = $productInfo->getCodename();
        }

        $results[] = "#### CUSTOMER INFO";
        $results[] = "CustomerNumber = ".$customerInfo->getCustomerNumber();
        $results[] = "WebLoginPassword = ".$customerInfo->getWebLoginPassword();

        $orders = [];
        $orderedProducts = [];
        foreach ($allCustomerOrder as $customerOrder) {
            $orders[] = $customerOrder->getOrderNumber();
            $productIdList = array_map('intval', array_map('trim', explode(',', $customerOrder->getProductList())));
            foreach ($productIdList as $productId) {
                if (array_key_exists($productId, $productNameList) &&
                    !in_array($productNameList[$productId], $orderedProducts)) {
                    $orderedProducts[] = $productNameList[$productId];
                }
            }
        }

        $externalBarcode = $customerInfo->getExternalBarcode();

        $results[] = "AllOrders = [".implode(', ', $orders)."]";
        $results[] = "ProductOrderedCustomer = [".implode(', ', $orderedProducts)."]";

        $results[] = "\n#### GENERAL INFO";
        $results[] = "ExternalBarcode = ".$externalBarcode;
        $results[] = "Title = ".$order->getTitle();
        $results[] = "FirstName = ".$order->getFirstName();
        $results[] = "SurName = ".$order->getSurName();
        $results[] = "DateOfBirth = ".$dob;
        $results[] = "Age = ".$age;
        $results[] = "DistributionChannel = ".$formatter->format($order->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
        $results[] = "Partner = ".(empty($dc) ? '' : $formatter->format($dc->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List'));
        $results[] = "Street1 = ".$order->getStreet();
        $results[] = "Street2 = ".$order->getStreet2();
        $results[] = "PLZ = ".$order->getPostCode();
        $results[] = "City = ".$order->getCity();
        $results[] = "Country = ".$formatter->format($order->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
        $results[] = "Ordernumber = ".$order->getOrderNumber();
        $results[] = "Gender = ".$formatter->format($order->getGenderId(), 'mapping', 'LeepAdmin_Gender_List');
        $results[] = "CompanyInfo = ".str_replace("\r\n", "", $common->nl2br($order->getCompanyInfo()));
        $results[] = "ContactInfo = ".str_replace("\r\n", "", $common->nl2br($order->getContactInfo()));
        $results[] = "RebrandingNick = ".$order->getRebrandingNick();
        $results[] = "DNCREPORTTYPE = ".$formatter->format($order->getIdDncReportType(), 'mapping', 'LeepAdmin_Report_DncReportType');
        $results[] = "Language = ".$formatter->format($order->getLanguageId(), 'mapping', 'LeepAdmin_Language_List');
        $results[] = "Email = ".$order->getEmail();
        $results[] = "Telephone = ".$order->getTelephone();
        $results[] = "NutriMeGoesTo = ".$formatter->format($order->getIdNutriMeGoesTo(), 'mapping', 'LeepAdmin_DistributionChannel_NutrimeGoesTo');
        $results[] = "CanDownloadReports = ".($order->getIsReportDeliveryDownloadAccess() == 0 ? "NO" : "YES");

        // DC Address
        if (empty($dc)) {
            $results[] = "DistributionChannelAddressStreet = ";
            $results[] = "DistributionChannelAddressPostCode = ";
            $results[] = "DistributionChannelAddressCity = ";
            $results[] = "DistributionChannelAddressCountry = ";

            $partner = null;
        }
        else {
            $results[] = "DistributionChannelAddressStreet = ".$dc->getStreet();
            $results[] = "DistributionChannelAddressPostCode = ".$dc->getPostCode();
            $results[] = "DistributionChannelAddressCity = ".$dc->getCity();
            $results[] = "DistributionChannelAddressCountry = ".$formatter->format($dc->getCountryId(), 'mapping', 'LeepAdmin_Country_List');

            $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
        }

        // Partner Address
        if (empty($partner)) {
            $results[] = "PartnerAddressStreet = ";
            $results[] = "PartnerAddressPostCode = ";
            $results[] = "PartnerAddressCity = ";
            $results[] = "PartnerAddressCountry = ";
        } else {
            $results[] = "PartnerAddressStreet = ".$partner->getStreet();
            $results[] = "PartnerAddressPostCode = ".$partner->getPostCode();
            $results[] = "PartnerAddressCity = ".$partner->getCity();
            $results[] = "PartnerAddressCountry = ".$formatter->format($partner->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
        }

        // Load customer order
        $results[] = "\n#### PRODUCT ORDERED";

        $categoriesOrdered = $doctrine->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBycustomerid($order->getId());
        $categories = array();
        foreach ($categoriesOrdered as $category) {
            $categories[] = $formatter->format($category->getCategoryId(), 'mapping', 'LeepAdmin_Category_Codename');
        }
        $results[] = 'CategoryOrdered = ['.implode(',', $categories).']';

        $productOrdered = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($order->getId());
        $products = array();
        foreach ($productOrdered as $product) {
            $products[] = $formatter->format($product->getProductId(), 'mapping', 'LeepAdmin_Product_Codename');
        }
        $results[] = 'ProductOrdered = ['.implode(',', $products).']';

        $specialProducts = array();
        foreach ($productOrdered as $product) {
            $idSpecialProduct = intval($product->getSpecialProductId());
            if ($idSpecialProduct != 0) {
                $specialProducts[] = $formatter->format($idSpecialProduct, 'mapping', 'LeepAdmin_SpecialProduct_Codename');
            }
        }
        $results[] = 'SpecialProductOrdered = ['.implode(',', $specialProducts).']';

        // Load status
        $forwardedDate = null;
        $results[] = "\n#### STATUS DATE";
        $captureStatus = array(
            'DateStatusArrived' => array('id' => 25, 'date' => null),
            'DateStatusStarted' => array('id' => 15, 'date' => null),
            'DateStatusReport'  => array('id' => 20, 'date' => null),
            'DateStatusNutriMeBoxCreated'    => array('id' => 54, 'date' => null),
            'DateStatusNutriMeLabelPrinted'  => array('id' => 53, 'date' => null),
            'DateStatusNutriMeSampleArrived' => array('id' => 52, 'date' => null),
            'DateStatusNutriMeSubmitted'     => array('id' => 55, 'date' => null),
        );
        $orderStatus = $doctrine->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBycustomerid($order->getId());
        foreach ($orderStatus as $cStatus) {
            $idStatus = $cStatus->getStatus();
            foreach ($captureStatus as $k => $v) {
                if ($v['id'] == $idStatus) {
                    $captureStatus[$k]['date'] = $cStatus->getStatusDate();
                }
            }

            if ($idStatus == 26) {
                $forwardedDate = $cStatus->getStatusDate();
            }
        }
        if ($captureStatus['DateStatusStarted']['date'] == null) {
            $captureStatus['DateStatusStarted']['date'] = $forwardedDate;
        }
        foreach ($captureStatus as $k => $v) {
            $date = '';
            if (!empty($v['date'])) {
                $date = $v['date']->format('d/m/Y');
            }
            $results[] = $k.' = '.$date;
        }

        $questionAnswer = array();
        // Load questions
        $results[] = "\n#### QUESTIONS";
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('pq.reportkey, cq.answer')
            ->from('AppDatabaseMainBundle:CustomerQuestion', 'cq')
            ->innerJoin('AppDatabaseMainBundle:ProductQuestion', 'pq', 'WITH', 'cq.questionid = pq.id')
            ->andWhere('cq.customerid = :customerId')
            ->setParameter('customerId', $order->getId());
        $resultSet = $query->getQuery()->getResult();
        foreach ($resultSet as $r) {
            $questionAnswer[trim($r['reportkey'])] = $r['answer'];
        }

        // Load new questions
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('f.reportKey, f.idType, p.answer')
            ->from('AppDatabaseMainBundle:OrderedQuestion', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormQuestion', 'f', 'WITH', 'p.idFormQuestion = f.id')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $order->getId());
        $resultSet = $query->getQuery()->getResult();
        foreach ($resultSet as $r) {
            $answer = Business\FormQuestion\Util::decodeAnswerToReport($r['idType'], $r['answer']);
            $answer = trim($answer);
            if (!isset($questionAnswer[trim($r['reportKey'])])) {
                $questionAnswer[trim($r['reportKey'])] = $answer;
            }
            else {
                if ($answer != '' && $answer != '[]') {
                    $questionAnswer[trim($r['reportKey'])] = $answer;
                }
            }
        }

        // Write questions
        foreach ($questionAnswer as $k => $v) {
            $results[] = $k.' = '.$v;
        }

        // Load genes
        $results[] = "\n#### GENE RESULT";
        $genes = array();
        $genesList = $doctrine->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        $pattern = array(' ', '-');
        foreach ($genesList as $gene) {
            $codeName = str_replace($pattern, '', $gene->getGeneName());
            $genes[$gene->getId()] = $codeName;
        }

        $geneResults = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findByIdCustomer($order->getIdCustomerInfo());
        foreach ($geneResults as $gr) {
            if (isset($genes[$gr->getIdGene()])) {
                $codeName = $genes[$gr->getIdGene()];
                $results[] = $codeName.' = '.$gr->getResult();
            }
        }

        // Report data
        $results[] = "\n#### DISTRIBUTION REPORT STYLE";
        $distributionChannel = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($order->getDistributionChannelId());
        if ($distributionChannel) {
            if (trim($distributionChannel->getReportLogo()) != '') {
                $results[] = 'DistributionChannelLogo = /DistributionChannels/'.$distributionChannel->getReportLogo();
            }
            else {
                $results[] = 'DistributionChannelLogo = ';
            }
            $results[] = 'DistributionChannelColor = '.$distributionChannel->getReportColor();
        }
        else {
            $results[] = 'DistributionChannelLogo = ';
            $results[] = 'DistributionChannelColor = ';
        }

        return implode("\n", $results);
    }
}