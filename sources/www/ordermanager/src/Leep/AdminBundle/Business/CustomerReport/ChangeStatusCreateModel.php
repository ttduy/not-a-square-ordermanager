<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Symfony\Component\Validator\ExecutionContext;

class ChangeStatusCreateModel {
    public $status;
    public $idCustomer;
    public $idEmailTemplate;
    public $emailList;
    public $subject;
    public $sendTo;
    public $cc;
    public $content;
}
