<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FtpShipmentHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $idOrder = $this->container->get('request')->get('id');
        $doctrine = $this->container->get('doctrine');
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);

        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionchannelid());

        $model = new FtpShipmentModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $idCustomer = $this->container->get('request')->get('id');

        $builder->add('sectionReport', 'section', array(
            'label'    => 'SFTP Upload Reports',
            'property_path' => false
        ));
        $builder->add('idCustomerReport', 'choice', array(
            'label'    => 'Report',
            'required' => false,
            'choices'  => Business\BodShipment\Utils::getApplicableReports($this->container, $idCustomer)
        ));
        $builder->add('filename', 'text', array(
            'label'    => 'Filename',
            'required' => false
        ));

        return $builder;
    }

    public function getFormModel() {
        return $this->getForm()->getData();
    }

    public function onSuccess() {
        set_time_limit(300);

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $model = $this->getFormModel();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        // Get distribution channel
        $idCustomer = $this->container->get('request')->get('id');
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionchannelid());
        if (!$dc->getIsDeliveryViaSftp()) {
            $this->errors = array('This DC hasn\'t set SFTP Delivery');
            return false;
        } else {
            if ($model->idCustomerReport == 0 || trim($model->filename) == '') {
                $this->errors = array("Missing information");
            }
            else {
                $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($model->idCustomerReport);
                $fileDir = $this->container->get('service_container')->getParameter('kernel.root_dir').'/../web/report';
                $tempDir = $this->container->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';

                // Upload via SFTP
                $host = $dc->getSftpHost();
                $username = $dc->getSftpUsername();
                $password = $dc->getSftpPassword();

                $connection = ssh2_connect($host, 22);
                if($connection === false) {
                    $this->errors = array("Cannot connect to server: " . $host);
                    return false;
                }

                ssh2_auth_password($connection, $username, $password);

                $sftp = ssh2_sftp($connection);

                // Send excel file
                if ($customerReport->getFoodTableExcelFilename() != '') {
                    $srcFile = $fileDir.'/'.$customerReport->getFoodTableExcelFilename();
                    if (!is_file($srcFile)) {
                        $awsHelper = $this->container->get('leep_admin.helper.aws_backup_files_helper');
                        $result = $awsHelper->temporaryDownloadBackupFiles('report', $customerReport->getFoodTableExcelFilename());
                        if ($result) {
                            $srcFile = "$tempDir/".$customerReport->getFoodTableExcelFilename();
                        }
                    }

                    $dstFile = $dc->getSftpFolder().'/'.$model->filename.'.xlsx';
                    $sftpStream = fopen("ssh2.sftp://$sftp/$dstFile", 'w');

                    if (!$sftpStream) {
                        $this->errors = array("Could not open remote file: " . $dstFile);
                        return false;
                    }
                    $dataToSend = file_get_contents($srcFile);
                    if (fwrite($sftpStream, $dataToSend) === false) {
                        $this->errors = array("Could not send data");
                        return false;
                    }
                    fclose($sftpStream);
                }

                // Send XML file
                if ($customerReport->getXmlFilename() != '') {
                    $srcFile = $fileDir.'/'.$customerReport->getXmlFilename();
                    if (!is_file($srcFile)) {
                        $awsHelper = $this->container->get('leep_admin.helper.aws_backup_files_helper');
                        $result = $awsHelper->temporaryDownloadBackupFiles('report', $customerReport->getXmlFilename());
                        if ($result) {
                            $srcFile = "$tempDir/".$customerReport->getXmlFilename();
                        }
                    }

                    $dstFile = $dc->getSftpFolder().'/'.$model->filename.'.xml';
                    $sftpStream = fopen("ssh2.sftp://$sftp/$dstFile", 'w');

                    if (!$sftpStream) {
                        $this->errors = array("Could not open remote file: " . $dstFile);
                        return false;
                    }
                    $dataToSend = file_get_contents($srcFile);
                    if (fwrite($sftpStream, $dataToSend) === false) {
                        $this->errors = array("Could not send data");
                        return false;
                    }
                    fclose($sftpStream);
                }

                // Log
                $customerReportName = sprintf("%s / %s / %s",
                    $formatter->format($customerReport->getInputTime(), 'datetime'),
                    $formatter->format($customerReport->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List'),
                    $customerReport->getName()
                );
                $log = new Entity\CustomerReportFtpLog();
                $log->setUploadTime(new \DateTime());
                $log->setIdCustomer($idCustomer);
                $log->setReportName($customerReportName);
                $log->setFileName($model->filename);
                $em->persist($log);
                $em->flush();
            }
        }

        parent::onSuccess();
        $this->messages = array('File has been uploaded successfully!');
    }
}