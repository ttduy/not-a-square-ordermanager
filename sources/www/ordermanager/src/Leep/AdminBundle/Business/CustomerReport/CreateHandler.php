<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $idOrder = $this->container->get('request')->get('id');
        $doctrine = $this->container->get('doctrine');
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);

        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionchannelid());

        $model = new CreateModel();
        $model->idLanguage = $customer->getLanguageId();
        $model->isSaveReport = true;
        $model->isUseNewQueue = true;
        $model->isGenerateXmlVersion = ($dc ? $dc->getIsGenerateXmlVersion() : false);

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $idOrder = $this->container->get('request')->get('id');

        $builder->add('sectionReport', 'section', array(
            'label'    => 'Generate reports',
            'property_path' => false
        ));
        $builder->add('reports', 'choice', array(
            'label'    => 'Report',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => Utils::getApplicableReports($this->container, $idOrder),
            'attr'    => array(
                'style'   => 'min-height: 300px'
            )
        ));
        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Language_List'),
        ));
        $builder->add('isSaveReport',  'checkbox', array(
            'label'    => 'Save report?',
            'required' => false
        ));
        $builder->add('isUseNewQueue', 'checkbox', array(
            'label'    => 'Is use new queue?',
            'required' => false
        ));
        $builder->add('isGenerateXmlVersion', 'checkbox', array(
            'label'    => 'Is generate XML version?',
            'required' => false
        ));
        return $builder;
    }

    public function getFormModel() {
        return $this->getForm()->getData();
    }
    public function onSuccess() {
        set_time_limit(120);

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $model = $this->getFormModel();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $idOrder = $this->container->get('request')->get('id');
        $language = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($model->idLanguage);
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);

        $customerData = Utils::loadOrderData($this->container, $idOrder);
        $customerDataArr = $this->parseReportData($customerData);

        if ($model->isUseNewQueue) {
            foreach ($model->reports as $idReport) {
                $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
                if (!$report->getIsReleased()) {
                    $this->errors[] = "Error: This report is not validated and released yet!";
                    return false;
                }

                $description = array(
                    'Report: '.$report->getName(),
                    'Language: '.$language->getName(),
                    'Order number: '.$customer->getOrderNumber(),
                );
                if ($model->isSaveReport) {
                    $description[] = 'Is saved: Yes';
                }
                if ($model->isGenerateXmlVersion) {
                    $description[] = 'Is generate XML Version: Yes';
                }

                // Build
                $input = array(
                    'name' => $report->getName(),
                    'idReport' => $idReport,
                    'idCustomer' => $idOrder,
                    'idLanguage' => $model->idLanguage,
                    'idFormulaTemplate' => $report->getIdFormulaTemplate(),
                    'customerData' => $customerDataArr,
                    'isSaveReport' => $model->isSaveReport,
                    'isGenerateXmlVersion' => $model->isGenerateXmlVersion
                );

                // Add to report queue
                $reportQueue = new Entity\ReportQueue();
                $reportQueue->setIdWebUser($userManager->getUser()->getId());
                $reportQueue->setInputTime(new \DateTime());
                $reportQueue->setDescription(implode('<br/>', $description));
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
                $reportQueue->setInput(serialize($input));
                $reportQueue->setIdReport($idReport);
                $reportQueue->setIdCustomer($idOrder);
                $reportQueue->setIsSaved($model->isSaveReport);
                $reportQueue->setIdJobType(Business\ReportQueue\Constant::JOB_TYPE_BUILD_CUSTOMER_REPORT);
                $em->persist($reportQueue);
                $em->flush();

                // Notify
                $this->messages[] = 'Report '.$report->getName().' has been placed to new queue, job ID = '.$reportQueue->getId();
            }
            return;
        }

        // PROCESS OLD QUEUE
        foreach ($model->reports as $idReport) {
            // Build description
            $report = $doctrine->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);
            $description = array(
                'Report: '.$report->getName(),
                'Language: '.$language->getName(),
                'Order number: '.$customer->getOrderNumber(),
            );
            if ($model->isSaveReport) {
                $description[] = 'Is saved: Yes';
            }

            // Compute final data
            $isSuccess = false;
            $errorMessage = '';
            $formulaTemplate = $doctrine->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($report->getIdFormulaTemplate());
            $finalData = array();
            if ($formulaTemplate) {
                $finalData = $customerDataArr;

                $executor = $this->container->get('leep_admin.formula_template.business.executor');
                $executor->execute($formulaTemplate->getFormula(), $customerDataArr);
                $errors = $executor->errors;
                $output = $executor->output;

                if (!empty($errors)) {
                    $errorMessage = '';
                    $errorMessage .= "\n".'COMPILE ERROR -----------'."\n";
                    $errorMessage .= implode("\n", $errors);
                    $errorMessage .= "\n".'------------------------'."\n";
                }
                else {
                    foreach ($output as $k => $v) {
                        $finalData[$k] = $v;
                    }
                    /*$rows = explode("\n", $report->getTestData());
                    foreach ($rows as $row) {
                        $arr = explode('=', $row);
                        if (isset($arr[1])) {
                            if (!isset($finalData[trim($arr[0])])) {
                                $finalData[trim($arr[0])] = trim($arr[1]);
                            }
                        }
                    }*/

                    $isSuccess = true;
                }
            }
            else {
                $errorMessage = "No formula template selected";
            }

            // Put offline job
            if ($isSuccess) {
                // Build report data
                $reportData = array(
                    'name' => $report->getName(),
                    'idReport' => $idReport,
                    'idCustomer' => $idOrder,
                    'idLanguage' => $model->idLanguage,
                    'idFormulaTemplate' => $report->getIdFormulaTemplate(),
                    'customerData' => $customerData,
                    'reportFinalData' => $this->encodeReportData($finalData),
                    'isSaveReport' => $model->isSaveReport
                );

                // Put job
                $offlineJob = new Entity\OfflineJob();
                $offlineJob->setIdHandler(Business\OfflineJob\Constants::OFFLINE_JOB_HANDLER_CUSTOMER_REPORT);
                $offlineJob->setIdWebUser($userManager->getUser()->getId());
                $offlineJob->setInputTime(new \DateTime());
                $offlineJob->setDescription(implode("<br/>", $description));
                $offlineJob->setStatus(Business\OfflineJob\Constants::OFFLINE_JOB_PENDING);
                $offlineJob->setParameters(json_encode($reportData));
                $offlineJob->setIdCustomer($idOrder);
                $offlineJob->setIdReport($idReport);
                $offlineJob->setIsSaved($model->isSaveReport);
                $em->persist($offlineJob);
                $em->flush();

                // Notify
                $this->messages[] = 'Report '.$report->getName().' has been placed, job ID = '.$offlineJob->getId();
            }
            else {
                $this->errors[] = 'Report '.$report->getName().' has failed to build report data, got this error: '.$errorMessage;
            }
        }
    }

    protected function parseReportData($reportData) {
        $data = array();
        $rows = explode("\n", $reportData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $data[trim($arr[0])] = trim($arr[1]);
            }
        }
        return $data;
    }

    protected function encodeReportData($arr) {
        $results = array();
        foreach ($arr as $k => $v) {
            $results[] = $k.' = '.$v;
        }
        return implode("\n", $results);
    }
}