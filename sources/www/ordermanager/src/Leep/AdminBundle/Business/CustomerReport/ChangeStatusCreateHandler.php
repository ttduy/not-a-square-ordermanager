<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ChangeStatusCreateHandler extends BaseCreateHandler {
    public $customer;
    public function getDefaultFormModel() {
        $id = $this->container->get('request')->query->get('id', 0);
        $this->customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);

        $model = new ChangeStatusCreateModel();     
        $model->status = 8; // Report submitted
        $model->idCustomer = $id;

        return $model;
    }

    public function getEmailList() {
        $emailList = array();
        $emailList[$this->customer->getEmail()] = 'Customer ('.$this->customer->getEmail().') <br/>';
        
        $doctrine = $this->container->get('doctrine');
        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($this->customer->getDistributionChannelId());
        if ($dc) {
            if ($dc->getStatusDeliveryEmail() != '') {
                $emailList[$dc->getStatusDeliveryEmail()] = 'Distribution Channel ('.$dc->getStatusDeliveryEmail().') <br/>';
            }
            $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
            if ($partner && $partner->getStatusDeliveryEmail() != '') {
                $emailList[$partner->getStatusDeliveryEmail()] = 'Partner ('.$partner->getStatusDeliveryEmail().') <br/>';
            }
        }
        return $emailList;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Customer_Status')
        ));
        
        $builder->add('sectionTemplate', 'section', array(
            'label'    => 'Template',
            'property_path' => false
        ));
        $builder->add('idCustomer', 'hidden');
        $builder->add('idEmailTemplate', 'choice', array(
            'label'    => 'Email Template',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_EmailTemplate_List'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeEmailTemplateId()'
            )
        ));
        $builder->add('emailList',   'choice', array(
            'label'    => 'Emails',
            'required' => false,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices'  => $this->getEmailList()
        ));
        $builder->add('sectionContent', 'section', array(
            'label'    => 'Email Content',
            'property_path' => false
        ));
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('sendTo', 'text', array(
            'label'    => 'Send To',
            'required' => true
        ));
        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'   => array(
                'rows'  => 15, 'cols' => 80
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $helperCommon = $this->container->get('leep_admin.helper.common');
        $helperActivity = $this->container->get('leep_admin.helper.activity');

        $isSuccess = false;
        try {
            $senderEmail = $this->container->getParameter('sender_email');
            $senderName  = $this->container->getParameter('sender_name');
            $message = \Swift_Message::newInstance()
                ->setSubject($model->subject)
                ->setFrom($senderEmail, $senderName)
                ->setTo($helperCommon->parseEmaiLList($model->sendTo))
                ->setCc($helperCommon->parseEmaiLList($model->cc))
                ->setBody($model->content, 'text/plain');
            $this->container->get('mailer')->send($message);
            $isSuccess = true;

            parent::onSuccess();
            $this->messages = array();
            $this->messages[] = 'The email has been sent, status has been updated';
            
            // Make activity
            $notes = 'Send email "'.$model->subject.'" to '.$model->sendTo;
            if (!empty($model->cc)) { $notes.= ' (cc: '.$model->cc.') '; }
            $helperActivity->addCustomerActivity($this->customer->getId(), Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT, $notes);

            // Change status
            $today = new \DateTime();

            $query = $em->createQueryBuilder();
            $query->select('MAX(p.sortorder) as maxSortOrder')
                ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
                ->andWhere('p.customerid = :idCustomer')
                ->setParameter('idCustomer', $model->idCustomer);
            $maxSortOrder = $query->getQuery()->getSingleScalarResult();

            $status = new Entity\OrderedStatus();
            $status->setCustomerId($model->idCustomer);
            $status->setStatusDate($today);
            $status->setStatus($model->status);
            $status->setIsNotFilterable(0);
            $status->setSortOrder($maxSortOrder+1);
            $em->persist($status);

            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($model->idCustomer);
            $customer->setStatus($model->status);
            $customer->setStatusDate($today);
            $em->flush();

            Business\Customer\Util::updateStatusList($this->container, $model->idCustomer);
        }
        catch (\Exception $e) {
            $isSuccess = false;
            $this->errors[] = $e->getMessage();
        }
    }
}
