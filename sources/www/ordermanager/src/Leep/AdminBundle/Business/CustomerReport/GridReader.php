<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('inputTime', 'idReport', 'idLanguage', 'data', 'isVisibleToCustomer', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Time',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Report',      'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Language',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Data',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Visible?',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CustomerReportGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CustomerReport', 'p');
        $queryBuilder->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $this->container->get('request')->get('id', 0));
        if ($this->filters->idReport != 0) {
            $queryBuilder->andWhere('p.idReport LIKE :idReport')
                ->setParameter('idReport', $this->filters->idReport);
        }
        if ($this->filters->idLanguage != 0) {
            $queryBuilder->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('idLanguage', $this->filters->idLanguage);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        //$builder->addNewTabButton('View pdf', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId())), 'button-pdf');
        $builder->addNewTabButton('Download pdf', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId(), 'isDownload' => 1)), 'button-down');
        if ($row->getLowResolutionFilename() != '') {
            $builder->addNewTabButton('Download pdf', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId(), 'isDownload' => 1, 'isLowResolution' => 1)), 'button-download-gray');
        }
        if ($row->getFoodTableExcelFilename() != '') {
            $builder->addNewTabButton('Download excel', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId(), 'isDownload' => 1, 'isExcel' => 1)), 'button-excel');
        }
        if ($row->getFoodTableExcelFile2Name() != '') {
            $builder->addNewTabButton('Download food table 2 excel', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId(), 'isDownload' => 1, 'isExcel2' => 1)), 'button-excel');
        }
        if ($row->getXmlFilename() != '') {
            $builder->addNewTabButton('Download XML', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'view', array('id' => $row->getId(), 'isDownload' => 1, 'isXml' => 1)), 'button-export');
        }
        $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'delete', array('id' => $row->getId())), 'button-delete');

        return $builder->getHtml();
    }

    public function buildCellIsVisibleToCustomer($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        if ($row->getIsVisibleToCustomer()) {
            $builder->addButton('View pdf', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'changeVisibility', array('id' => $row->getId(), 'visibility' => 0)), 'button-red-flag');
            return $builder->getHtml()." Visible";
        }
        else {
            $builder->addButton('View pdf', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'changeVisibility', array('id' => $row->getId(), 'visibility' => 1)), 'button-green-flag');
            return $builder->getHtml();
        }
    }

    public function buildCellData($row) {
        $mgr = $this->getModuleManager();

        $links = array();
        $links[] = '<a href="'.$mgr->getUrl('leep_admin', 'customer_report', 'feature', 'downloadCustomerData', array('id' => $row->getId())).'">Customer data</a>';
        $links[] = '<a href="'.$mgr->getUrl('leep_admin', 'customer_report', 'feature', 'downloadReportData', array('id' => $row->getId())).'">Report data</a>';

        return implode("<br/>", $links);
    }
}
