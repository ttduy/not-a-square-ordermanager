<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class ChangeStatusHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mapping = $controller->get('easy_mapping');
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $mgr = $controller->get('easy_module.manager');

        $em = $controller->get('doctrine')->getEntityManager();

        // ACTIVITIES
        $query = $em->createQueryBuilder();
        $query->select('a')
            ->from('AppDatabaseMainBundle:CustomerActivity', 'a')
            ->andWhere('a.customerid = :customerId')
            ->setParameter('customerId', $id)
            ->andWhere('a.activitytypeid = :activityTypeEmailId')
            ->setParameter('activityTypeEmailId', Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT)
            ->orderBy('a.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
            
        $activities = array();
        foreach ($result as $row) {
            $activities[] = array(
                'time' => $formatter->format($row->getActivityDate(), 'date').' '.$mapping->getMappingTitle('LeepAdmin_Time_List', $row->getActivityTimeId()),
                'notes' => $row->getNotes()
            );
        }
        $data['activities'] = $activities;
    }
}
