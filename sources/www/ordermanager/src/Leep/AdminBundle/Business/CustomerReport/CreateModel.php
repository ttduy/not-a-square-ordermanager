<?php
namespace Leep\AdminBundle\Business\CustomerReport;

class CreateModel {
    public $name;
    public $reports;
    public $idLanguage;
    public $isSaveReport;
    public $isUseNewQueue;
    public $isGenerateXmlVersion;
}