<?php
namespace Leep\AdminBundle\Business\CustomerReport;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class FtpShipmentHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mgr = $controller->get('easy_module.manager');
        $idCustomer = $controller->get('request')->query->get('id', 0);
        
        $ftpShipmentLog = array();
        $records = $em->getRepository('AppDatabaseMainBundle:CustomerReportFtpLog')->findByIdCustomer($idCustomer, array('uploadTime' => 'DESC'));
        foreach ($records as $r) {
            $ftpShipmentLog[] = array(
                'timestamp'    => $formatter->format($r->getUploadTime(), 'datetime'),
                'reportName'   => $r->getReportName(),
                'fileName'     => $r->getFileName()
            );
        }
        $data['ftpShipmentLog'] = $ftpShipmentLog;

        $data['viewFileLinkUrl'] = $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'viewFiles', array('id' => $idCustomer));
    }
}
