<?php
namespace Leep\AdminBundle\Business\FoodTableSmileyConversion;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableSmileyConversion', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->fromValue = $entity->getFromValue();
        $model->toValue = $entity->getToValue();
        $model->normalizedValue = $entity->getNormalizedValue();
        $model->text = $entity->getText();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('fromValue', 'text', array(
            'attr'     => array(
                'placeholder' => 'From value'
            ),
            'required' => true
        ));
        $builder->add('toValue', 'text', array(
            'attr'     => array(
                'placeholder' => 'To value'
            ),
            'required' => true
        ));
        $builder->add('normalizedValue', 'choice', array(
            'attr'     => array(
                'placeholder' => 'Normalized value'
            ),
            'required' => true,
            'choices'  => Utils::getNormalizedValues()
        ));
        $builder->add('text', 'text', array(
            'attr'     => array(
                'placeholder' => 'Text block'
            ),
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setFromValue($model->fromValue);
        $this->entity->setToValue($model->toValue);
        $this->entity->setNormalizedValue($model->normalizedValue);
        $this->entity->setText($model->text);

        parent::onSuccess();
    }
}