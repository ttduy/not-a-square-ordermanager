<?php
namespace Leep\AdminBundle\Business\FoodTableSmileyConversion;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('fromValue', 'toValue', 'normalizedValue', 'text', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'From value',          'width' => '15%', 'sortable' => 'true'),
            array('title' => 'To value',            'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Normalized value',    'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Text block',          'width' => '40%', 'sortable' => 'true'),
            array('title' => 'Action',              'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FoodTableSmileyConversion', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('foodTableModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'food_table_smiley_conversion', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'food_table_smiley_conversion', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}