<?php
namespace Leep\AdminBundle\Business\FoodTableSmileyConversion;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('fromValue', 'text', array(
            'attr'     => array(
                'placeholder' => 'From value'
            ),
            'required' => true
        ));
        $builder->add('toValue', 'text', array(
            'attr'     => array(
                'placeholder' => 'To value'
            ),
            'required' => true
        ));
        $builder->add('normalizedValue', 'choice', array(
            'attr'     => array(
                'placeholder' => 'Normalized value'
            ),
            'required' => true,
            'choices'  => Utils::getNormalizedValues()
        ));
        $builder->add('text', 'text', array(
            'attr'     => array(
                'placeholder' => 'Text block',
            ),
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $smileyConversion = new Entity\FoodTableSmileyConversion();
        $smileyConversion->setFromValue($model->fromValue);
        $smileyConversion->setToValue($model->toValue);
        $smileyConversion->setNormalizedValue($model->normalizedValue);
        $smileyConversion->setText($model->text);

        $em = $this->container->get('doctrine')->getEntityManager();
        $em->persist($smileyConversion);
        $em->flush();

        parent::onSuccess();
    }
}