<?php
namespace Leep\AdminBundle\Business\FoodTableSmileyConversion;

class Utils {
    public static function getNormalizedValues() {
        $arr = array();
        for ($i = 6; $i >= -6; $i--) {
            $arr[$i] = $i;
        }
        return $arr;
    }
}