<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }

    public function getDefaultFormModel() {
        $model = new CreateModel();
        $this->generateAttachmentKey();
        $model->attachmentKey = $this->attachmentKey;
        return $model;
    }

    public function buildForm($builder) {
        // section attachment
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genobatches/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        $mapping = $this->container->get('easy_mapping');

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionCommon', 'section', array(
            'label'    => 'Common',
            'property_path' => false
        ));

        $builder->add('idGenoMaterial', 'searchable_box', array(
            'label'    => 'Type',
            'required' => true,
            'choices'  => Business\GenoMaterial\Utils::getMaterialList($this->container),
            'empty_value' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => Constant::getStatus()
        ));

        $builder->add('location', 'searchable_box', array(
            'label'    => 'Location (In storage)',
            'required' => false,
            'choices'  => [0 => ''] + Business\GenoStorage\Utils::getLocationList($this->container),
            'empty_value' => false
        ));

        $builder->add('amountInStock', 'text', array(
            'label'    => 'Amount in stock (gram)',
            'required' => true,
        ));

        $builder->add('pricePerGram', 'text', array(
            'label'    => 'Price per gram',
            'required' => false
        ));

        $builder->add('isStorageSampleCollected', 'checkbox', array(
            'label'    => 'Is Storage Sample Collected?',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionInternalLotNumber', 'section', array(
            'label'    => 'Internal Lot Number',
            'property_path' => false
        ));

        $builder->add('internalLotNumbers', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_internal_lot_number'),
            'label' => 'Level',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order Information',
            'property_path' => false
        ));

        $builder->add('orderedBy', 'searchable_box', array(
            'label'    => 'Ordered by',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('orderedDate', 'datepicker', array(
            'label'    => 'Ordered date',
            'required' => true
        ));

        $builder->add('orderedPrice', 'text', array(
            'label'    => 'Order price',
            'required' => false
        ));

        $builder->add('orderQuantity', 'text', array(
            'label'    => 'Order quantity',
            'required' => false
        ));

        $builder->add('unitPrice', 'text', array(
            'label'    => 'Unit price',
            'required' => false
        ));

        $builder->add('unitQuantity', 'text', array(
            'label'    => 'Unit quantity',
            'required' => false
        ));

        $builder->add('supplierName', 'text', array(
            'label'    => 'Supplier\'s name',
            'required' => false
        ));

        $builder->add('fillers', 'text', array(
            'label'    => 'Fillers',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionRecive', 'section', array(
            'label'    => 'Recive Information',
            'property_path' => false
        ));

        $builder->add('recievedBy', 'searchable_box', array(
            'label'    => 'Recieved by',
            'required' => false,
            'choices'  => [0 => null] + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('recievedDate', 'datepicker', array(
            'label'    => 'Recieved date',
            'required' => false
        ));

        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));

        $builder->add('loading', 'text', array(
            'label'    => 'Loading',
            'required' => false
        ));

        $builder->add('externalLot', 'text', array(
            'label'    => 'External lot',
            'required' => false
        ));

        $builder->add('isShipmentOk', 'checkbox', array(
            'label'    => 'Is Shipment OK?',
            'required' => false
        ));

        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'style'   => 'width:100%',
                'rows'   => 10
            )
        ));
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $batch = new Entity\GenoBatches();

        // Validation
        $this->errors += Utils::commonValidation($this->container, $model);

        if (!empty($this->errors)) {
            return false;
        }

            // Check if loading have invalid value
        $loadingCurrent = floatval($model->loading);

        // Common
        $batch->setCreatedDate(new \DateTime());
        $batch->setCreatedBy($currentUserId);
        $batch->setModifiedDate(new \DateTime());
        $batch->setModifiedBy($currentUserId);
        $batch->setIdGenoMaterial($model->idGenoMaterial);
        $batch->setLocation($model->location);
        $batch->setAttachmentKey($model->attachmentKey);
        $batch->setIsStorageSampleCollected($model->isStorageSampleCollected);

        // Order information
        $batch->setOrderedBy($model->orderedBy);
        $batch->setOrderedDate($model->orderedDate);
        $batch->setOrderedPrice($model->orderedPrice);
        $batch->setOrderQuantity($model->orderQuantity);
        $batch->setUnitPrice($model->unitPrice);
        $batch->setUnitQuantity($model->unitQuantity);
        $batch->setPricePerGram($model->pricePerGram);
        $batch->setSupplierName($model->supplierName);
        $batch->setFillers($model->fillers);

        // Recieve information
        $batch->setRecievedBy($model->recievedBy);
        $batch->setRecievedDate($model->recievedDate);
        $batch->setExpiryDate($model->expiryDate);
        $batch->setLoading($loadingCurrent);
        $batch->setExternalLot($model->externalLot);
        $batch->setIsShipmentOk($model->isShipmentOk);
        $batch->setNotes($model->notes);

        $em->persist($batch);
        $em->flush();

        Utils::updateBatchStatus($this->container, $batch, $model->status, "Create new batch", false);

        // Update storage loading
        if (intval($model->amountInStock) < 0) {
            $model->amountInStock = 0;
        }

        Business\GenoStorage\Utils::updateStorage($this->container, $batch, intval($model->amountInStock), "Create new Batch");

        // Create internal lot number
        if ($model->internalLotNumbers) {
            foreach ($model->internalLotNumbers as $cellData) {
                $lotNumber = trim($cellData['value']);
                if (empty($lotNumber)) {
                    continue;
                }

                // Insert into table
                $lot = new Entity\GenoLotNumbers();
                $lot->setValue($lotNumber);
                $lot->setBatchId($batch->getId());
                $lot->setCreatedDate(new \DateTime());
                $lot->setCreatedBy($currentUserId);
                $lot->setModifiedDate(new \DateTime());
                $lot->setModifiedBy($currentUserId);

                $em->persist($lot);
            }

            $em->flush();
        }

        parent::onSuccess();
    }
}
