<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Form\FormError;

class EditHandler extends AppEditHandler {
    public $attachmentKey = null;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if (empty($this->attachmentKey)) {
            $success = false;
            while (!$success) {
                $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
                $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
                if (!$duplicate) {
                    $success = true;
                }
            }
        }
    }

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = new EditModel();

        // check if attached key is empty
        $entityAttachmentKey = $entity->getAttachmentKey();
        if(empty($entityAttachmentKey)){
            $this->generateAttachmentKey();
            $entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }
        $this->attachmentKey = $entity->getAttachmentKey();

        $model->attachmentKey = $entity->getAttachmentKey();
        $model->status = $entity->getStatus();
        $model->location = $entity->getLocation();
        $model->amountInStock = intval($entity->getAmountInStock());
        $model->idGenoMaterial = $entity->getIdGenoMaterial();
        $model->isStorageSampleCollected = $entity->getIsStorageSampleCollected();

        $model->orderedBy = $entity->getOrderedBy();
        $model->orderedDate = $entity->getOrderedDate();
        $model->orderedPrice = $entity->getOrderedPrice();
        $model->orderQuantity = $entity->getOrDerQuantity();
        $model->unitPrice = $entity->getUnitPrice();
        $model->unitQuantity = $entity->getUnitQuantity();
        $model->pricePerGram = $entity->getPricePerGram();
        $model->supplierName = $entity->getSupplierName();
        $model->fillers = $entity->getFillers();

        $model->recievedBy = $entity->getRecievedBy();
        $model->recievedDate = $entity->getRecievedDate();
        $model->expiryDate = $entity->getExpiryDate();
        $model->loading = $entity->getLoading();
        $model->externalLot = $entity->getExternalLot();
        $model->isShipmentOk = $entity->getIsShipmentOk();
        $model->notes = $entity->getNotes();

        // get the file
        $model->fileAttachment = array();
        $model->fileAttachment['file'] = $entity->getFileAttachment();

        $model->internalLotNumbers = [];

        $lotNumbers = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findByBatchId($entity->getId());

        foreach ($lotNumbers as $lot) {
            $model->internalLotNumbers[] = [
                'id' =>             $lot->getId(),
                'value' =>          $lot->getValue(),
                'createdDate' =>    $lot->getCreatedDate(),
                'createdBy' =>      $lot->getCreatedBy(),
                'modifiedDate' =>   $lot->getModifiedDate(),
                'modifiedBy' =>     $lot->getModifiedBy()
            ];
        }

        return $model;
    }

    public function buildForm($builder) {
        // section attachment key
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genobatches/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));


        $mapping = $this->container->get('easy_mapping');

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionCommon', 'section', array(
            'label'    => 'Common',
            'property_path' => false
        ));

        $builder->add('idGenoMaterial', 'searchable_box', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => Business\GenoMaterial\Utils::getMaterialList($this->container),
            'empty_value' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => Constant::getStatus()
        ));

        $builder->add('location', 'searchable_box', array(
            'label'    => 'Location (In storage)',
            'required' => false,
            'choices'  => [0 => ''] + Business\GenoStorage\Utils::getLocationList($this->container),
            'empty_value' => false
        ));

        $builder->add('amountInStock', 'text', array(
            'label'    => 'Amount in stock (gram)',
            'required' => true,
        ));

        $builder->add('pricePerGram', 'text', array(
            'label'    => 'Price per gram',
            'required' => false
        ));

        $builder->add('isStorageSampleCollected', 'checkbox', array(
            'label'    => 'Is Storage Sample Collected?',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionInternalLotNumber', 'section', array(
            'label'    => 'Internal Lot Number',
            'property_path' => false
        ));

        $builder->add('internalLotNumbers', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_internal_lot_number'),
            'label' => 'Level',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order Information',
            'property_path' => false
        ));

        $builder->add('orderedBy', 'searchable_box', array(
            'label'    => 'Ordered by',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('orderedDate', 'datepicker', array(
            'label'    => 'Ordered date',
            'required' => true
        ));

        $builder->add('orderedPrice', 'text', array(
            'label'    => 'Order price',
            'required' => false
        ));

        $builder->add('orderQuantity', 'text', array(
            'label'    => 'Order quantity',
            'required' => false
        ));

        $builder->add('unitPrice', 'text', array(
            'label'    => 'Unit price',
            'required' => false
        ));

        $builder->add('unitQuantity', 'text', array(
            'label'    => 'Unit quantity',
            'required' => false
        ));

        $builder->add('supplierName', 'text', array(
            'label'    => 'Supplier\'s name',
            'required' => false
        ));

        $builder->add('fillers', 'text', array(
            'label'    => 'Fillers',
            'required' => false
        ));

        //----------------------------------------------------------------------------------------------------------------------------------
        $builder->add('sectionRecive', 'section', array(
            'label'    => 'Recive Information',
            'property_path' => false
        ));

        $builder->add('recievedBy', 'searchable_box', array(
            'label'    => 'Recieved by',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => true
        ));

        $builder->add('recievedDate', 'datepicker', array(
            'label'    => 'Recieved date',
            'required' => false
        ));

        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));

        $builder->add('loading', 'text', array(
            'label'    => 'Loading',
            'required' => false
        ));

        $builder->add('externalLot', 'text', array(
            'label'    => 'External lot',
            'required' => false
        ));

        $builder->add('isShipmentOk', 'checkbox', array(
            'label'    => 'Is Shipment OK?',
            'required' => false
        ));

        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'style'   => 'width:100%',
                'rows'   => 10
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // Validation
        $this->errors += Utils::commonValidation($this->container, $model, $this->entity->getId());
        if (!empty($this->errors)) {
            return false;
        }

        // Check if loading have invalid value
        $loadingCurrent = floatval($model->loading);

        // Common
        $this->entity->setModifiedDate(new \DateTime());
        $this->entity->setModifiedBy($currentUserId);
        $this->entity->setIdGenoMaterial($model->idGenoMaterial);
        $this->entity->setIsStorageSampleCollected($model->isStorageSampleCollected);

        // Order information
        $this->entity->setOrderedBy($model->orderedBy);
        $this->entity->setOrderedDate($model->orderedDate);
        $this->entity->setOrderQuantity($model->orderQuantity);
        $this->entity->setOrderedPrice($model->orderedPrice);
        $this->entity->setUnitPrice($model->unitPrice);
        $this->entity->setUnitQuantity($model->unitQuantity);
        $this->entity->setPricePerGram($model->pricePerGram);
        $this->entity->setSupplierName($model->supplierName);
        $this->entity->setFillers($model->fillers);

        // Recieve information
        $this->entity->setRecievedBy($model->recievedBy);
        $this->entity->setRecievedDate($model->recievedDate);
        $this->entity->setExpiryDate($model->expiryDate);
        $this->entity->setLoading($loadingCurrent);
        $this->entity->setExternalLot($model->externalLot);
        $this->entity->setIsShipmentOk($model->isShipmentOk);
        $this->entity->setNotes($model->notes);

        Utils::updateBatchStatus($this->container, $this->entity, $model->status, "Update batch");

        // Update storage loading
        $model->amountInStock = intval($model->amountInStock);
        if ($model->amountInStock < 0) {
            $model->amountInStock = 0;
        }

        $updateAmount = $model->amountInStock - $this->entity->getAmountInStock();

        // Update storage amount
        Business\GenoStorage\Utils::updateStorage($this->container, $this->entity, $updateAmount, "Update Batch");

        // Check and update storage location
        Business\GenoStorage\Utils::changeBatchLocation($this->container, $this->entity, $model->location);

        // Get previous lot numbers
        $internalLotNumbers = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findByBatchId($this->entity->getId());
        $lotNumberIdList = [];

        // Create internal lot number
        if ($model->internalLotNumbers) {
            foreach ($model->internalLotNumbers as $cellData) {
                $lotNumber = trim($cellData['value']);
                $id = trim($cellData['id']);

                // Insert into table
                $lot = new Entity\GenoLotNumbers();
                $lot->setBatchId($this->entity->getId());
                $lot->setCreatedDate(new \DateTime());
                $lot->setCreatedBy($currentUserId);

                if ($id) {
                    // Or update
                    $lot = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findOneById($id);
                    $lotNumberIdList[] = $lot->getId();
                }

                // Update only if change
                if ($lot->getValue() != $lotNumber) {
                    $lot->setValue($lotNumber);
                    $lot->setModifiedDate(new \DateTime());
                    $lot->setModifiedBy($currentUserId);
                }

                if (!empty($lotNumber)) {
                    $em->persist($lot);
                }
            }

            $em->flush();
        }

        // Remove all un-updated lot number
        foreach ($internalLotNumbers as $lotNumber) {
            // Only delete cell that is not updated
            if (!in_array($lotNumber->getId(), $lotNumberIdList)) {
                $em->remove($lotNumber);
            }
        }

        $em->flush();

        $this->reloadForm();
        parent::onSuccess();
    }
}
