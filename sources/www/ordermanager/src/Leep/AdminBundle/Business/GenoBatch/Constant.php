<?php
namespace Leep\AdminBundle\Business\GenoBatch;

class Constant {
    const STATUS_IN_STOCK =     10;
    const STATUS_ON_HOLD =      20;
    const STATUS_ORDERED =      30;
    const STATUS_USED_UP =      40;
    const STATUS_CANCELLED =    50;
    const STATUS_EXPIRED =      60;

    public static function getStatus() {
        return array(
            self::STATUS_IN_STOCK =>    'In stock',
            self::STATUS_ON_HOLD =>     'On hold',
            self::STATUS_ORDERED =>     'Ordered',
            self::STATUS_USED_UP =>     'Used up',
            self::STATUS_CANCELLED =>   'Cancelled',
            self::STATUS_EXPIRED =>     'Expired',
        );
    }
}
