<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use App\Database\MainBundle\Entity;

class Utils {
    public static function commonValidation($container, $model, $id = null) {
        $em = $container->get('doctrine')->getEntityManager();
        $errors = [];

        // Validate internal lot number
        if ($model->internalLotNumbers) {
            $lotNumberList = [];
            foreach ($model->internalLotNumbers as $cellData) {
                $lotNumber = trim($cellData['value']);

                if (in_array($lotNumber, $lotNumberList)) {
                    $errors[] = "Duplicate lot number: $lotNumber!";
                }

                $lotNumberList[] = $lotNumber;

                $check = $em->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findOneByValue($lotNumber);
                if ($check) {
                    if ($id == null || $check->getBatchId() != $id) {
                        $errors[] = "Lot number: $lotNumber already exist!";
                    }
                }
            }
        }

        // Validate empty
        if (empty($model->idGenoMaterial) ||
            empty($model->status) ||
            empty($model->orderedBy) ||
            empty($model->orderedDate)) {
            $errors[] = "Empty required field!";
        }

        if ($model->status == Constant::STATUS_IN_STOCK && empty($model->location)) {
            $errors[] = "Empty location when batch is in stock!";
        }

        // Validate ordered and recieved date
        if ($model->orderedDate && $model->recievedDate && $model->orderedDate > $model->recievedDate) {
            $errors[] = "Recieved date must be later than ordered date!";
        }

        if (intval($model->amountInStock) < 0) {
            $errors[] = "Amount in stock must be greater or equal to zero.";
        }

        if ($model->recievedDate && $model->expiryDate && $model->recievedDate > $model->expiryDate) {
            $errors[] = "Expire date must be later than recieved date!";
        }

        if ($model->loading < 0 || $model->loading > 100) {
            $errors[] = "Loading must has value from zero to one hundred.";
        }

        return $errors;
    }

    public static function updateBatchStatus($container, $batch, $newStatus, $description = '', $autoCreateOldStatus = true) {
        $em = $container->get('doctrine')->getEntityManager();
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        if ($autoCreateOldStatus && $batch->getStatus() == $newStatus) {
            // Nothing to update!
            return;
        }

        // Check if we already have batch status
        $check = $em->getRepository('AppDatabaseMainBundle:GenoBatchStatusHistory')->findByIdGenoBatch($batch->getId());
        if (!$check && $autoCreateOldStatus) {
            $log = new Entity\GenoBatchStatusHistory();
            $log->setIdGenoBatch($batch->getId());
            $log->setStatus($batch->getStatus());
            $log->setDate(null);
            $log->setUser(null);
            $log->setDescription("This status was set before Log feature is implemented!");
            $em->persist($log);
            $em->flush();
        }

        $log = new Entity\GenoBatchStatusHistory();
        $log->setIdGenoBatch($batch->getId());
        $log->setStatus($newStatus);
        $log->setDate(new \DateTime());
        $log->setUser($currentUserId);
        $log->setDescription($description);
        $em->persist($log);
        $em->flush();

        $batch->setStatus($newStatus);
        $em->persist($batch);
        $em->flush();
    }
}
