<?php
namespace Leep\AdminBundle\Business\GenoBatch;

class CreateModel {
    public $status;
    public $idGenoMaterial;
    public $location;
    public $amountInStock;
    public $pricePerGram;
    public $isStorageSampleCollected;

    public $internalLotNumbers;

    public $orderedBy;
    public $orderedDate;
    public $orderedPrice;
    public $orderQuantity;
    public $unitPrice;
    public $unitQuantity;
    public $supplierName;
    public $fillers;

    public $recievedBy;
    public $recievedDate;
    public $expiryDate;
    public $loading;
    public $externalLot;
    public $isShipmentOk;
    public $notes;

    public $attachment;
    public $attachmentKey;
    public $fileAttachment;
}
