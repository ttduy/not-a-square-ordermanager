<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CopyHandler extends CreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }

    public function getDefaultFormModel() {
        $model = new CreateModel();

        $id = $this->container->get('request')->get('id');
        $entity = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches', 'app_main')->findOneById($id);

        $this->generateAttachmentKey();
        $model->attachmentKey = $this->attachmentKey;

        // Copy data
        $model->status = $entity->getStatus();
        $model->location = $entity->getLocation();
        $model->amountInStock = intval($entity->getAmountInStock());
        $model->idGenoMaterial = $entity->getIdGenoMaterial();

        $model->orderedBy = $entity->getOrderedBy();
        $model->orderedDate = $entity->getOrderedDate();
        $model->orderedPrice = $entity->getOrderedPrice();
        $model->unitPrice = $entity->getUnitPrice();
        $model->unitQuantity = $entity->getUnitQuantity();
        $model->supplierName = $entity->getSupplierName();
        $model->fillers = $entity->getFillers();

        $model->recievedBy = $entity->getRecievedBy();
        $model->recievedDate = $entity->getRecievedDate();
        $model->expiryDate = $entity->getExpiryDate();
        $model->loading = $entity->getLoading();
        $model->externalLot = $entity->getExternalLot();
        $model->isShipmentOk = $entity->getIsShipmentOk();
        $model->notes = $entity->getNotes();

        $model->internalLotNumbers = [];

        return $model;
    }
}
