<?php
namespace Leep\AdminBundle\Business\GenoBatch;

class FilterModel {
    public $status;
    public $idGenoMaterials;
    public $location;
    public $materialName;
    public $materialType;
    public $excludeStatus = [Constant::STATUS_USED_UP];

    public $orderedBy;
    public $orderedDate;
    public $supplierName;

    public $recievedBy;
    public $recievedDate;
}
