<?php
namespace Leep\AdminBundle\Business\GenoBatch\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppInternalLotNumber extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent() {
        return 'field';
    }

    public function getName() {
        return 'app_internal_lot_number';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $formatter = $this->container->get('leep_admin.helper.formatter');
        $data = $form->getData();

        if (isset($data['modifiedDate']) && $data['modifiedDate']) {
            $modifiedBy = $formatter->format($data['modifiedBy'], 'mapping', 'LeepAdmin_WebUser_List');
            $modifiedDate = $formatter->format($data['modifiedDate'], 'datetime');
            $view->vars['msg'] = "Last modified by $modifiedBy ($modifiedDate)";
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('id', 'hidden');

        $builder->add('value', 'text', [
            'label'    => 'Value',
            'required' => true
        ]);
    }
}