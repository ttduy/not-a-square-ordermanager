<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $locations;

    public function __construct($container) {
        parent::__construct($container);

        $this->locations = [0 => 'None yet'] + Business\GenoStorage\Utils::getLocationList($container);
    }

    public function getColumnMapping() {
        return array('createdDate', 'status', 'material', 'location', 'amountInStock', 'expiredIn', 'supplierName', 'type', 'loading', 'notes', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Created date', 'width' => '12%', 'sortable' => 'true'),
            array('title' => 'Status', 'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Name', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Location', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Amount in stock', 'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Expired in', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Supplier\'s name', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Type', 'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Loading', 'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Notes', 'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Action', 'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GenoBatchGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoBatches', 'p')
            ->innerJoin('AppDatabaseMainBundle:GenoMaterials', 'm', 'WITH', 'm.id = p.idGenoMaterial');

        if (!empty($this->filters->materialName)) {
            $queryBuilder->andWhere('m.shortName LIKE :materialName')
                ->setParameter('materialName', '%'.$this->filters->materialName.'%');
        }

        if (!empty($this->filters->status)) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }

        if (!empty($this->filters->excludeStatus)) {
            $queryBuilder->andWhere('p.status not in (:excludeStatus)')
                ->setParameter('excludeStatus', $this->filters->excludeStatus);
        }

        if (!empty($this->filters->idGenoMaterials)) {
            $queryBuilder->andWhere('p.idGenoMaterial IN (:idGenoMaterials)')
                ->setParameter('idGenoMaterials', $this->filters->idGenoMaterials);
        }

        if (!empty($this->filters->materialType)) {
            $queryBuilder->andWhere('m.type IN (:type)')
                ->setParameter('type', $this->filters->materialType);
        }

        if (!empty($this->filters->location)) {
            $queryBuilder->andWhere('p.location = :location')
                ->setParameter('location', $this->filters->location);
        }

        if (!empty($this->filters->orderedBy)) {
            $queryBuilder->andWhere('p.orderedBy = :orderedBy')
                ->setParameter('orderedBy', $this->filters->orderedBy);
        }

        if (!empty($this->filters->orderedDate)) {
            $queryBuilder->andWhere('p.orderedDate = :orderedDate')
                ->setParameter('orderedDate', $this->filters->orderedDate);
        }

        if (!empty($this->filters->supplierName)) {
            $queryBuilder->andWhere('p.supplierName = :supplierName')
                ->setParameter('supplierName', $this->filters->supplierName);
        }

        if (!empty($this->filters->recievedBy)) {
            $queryBuilder->andWhere('p.recievedBy = :recievedBy')
                ->setParameter('recievedBy', $this->filters->recievedBy);
        }

        if (!empty($this->filters->recievedDate)) {
            $queryBuilder->andWhere('p.recievedDate = :recievedDate')
                ->setParameter('recievedDate', $this->filters->recievedDate);
        }

        $queryBuilder
            ->addOrderBy('p.status', 'ASC')
            ->addOrderBy('p.expiryDate', 'ASC');
    }

    public function buildCellExpiredIn($row) {
        if ($row->getExpiryDate() != null) {
            $formatter = $this->container->get('leep_admin.helper.formatter');
            $dateStr = $formatter->format($row->getExpiryDate(), 'date');

            $today = new \DateTime();
            $diff = $today->diff($row->getExpiryDate());
            if ($diff->invert) {
                return "<span style='color:red;'>".$diff->format('%R%ad')." ($dateStr)</span>";
            }
            return $diff->format('%R%ad')." ($dateStr)";
        }

        return "";
    }

    public function buildCellMaterial($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $shortName = $formatter->format($row->getIdGenoMaterial(), 'mapping', 'LeepAdmin_GenoMaterial_List');
        $fullName = $formatter->format($row->getIdGenoMaterial(), 'mapping', 'LeepAdmin_GenoMaterial_List_FullName');
        $name = "";
        if (!empty($fullName)) {
            return $name = $fullName." (".$shortName.")";
        }
        return "({$shortName})";
    }

    public function buildCellType($row) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $material = $em->getRepository('AppDatabaseMainBundle:GenoMaterials')->findOneById($row->getIdGenoMaterial());
        if ($material) {
            $type = $em->getRepository('AppDatabaseMainBundle:GenoMaterialTypes')->findOneById($material->getType());
            if ($type) {
                return $type->getTypeName();
            }
        }
        return '';
    }

    public function buildCellLoading($row) {
        if ($row->getLoading() != null) {
            return $row->getLoading();
        }

        return "";
    }

    public function buildCellNotes($row) {
        $cellValue = '';
        $note = $row->getNotes();
        if (!empty($note)) {
            $cellValue =  '&nbsp;&nbsp;<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.$note.'</div></a>';
        }
        return $cellValue;
    }

    public function buildCellLocation($row) {
        if (!$row->getLocation()) {
            return "<span style='color: red;'>".$this->locations[$row->getLocation()]."</span>";
        }
        return isset($this->locations[$row->getLocation()]) ? $this->locations[$row->getLocation()] : "";
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '<span>'.Constant::getStatus()[$row->getStatus()].'</span>';

        $builder->addPopupButton('View history', $mgr->getUrl('leep_admin', 'geno_batch', 'feature', 'viewStatusHistoryLog', array('id' => $row->getId())), 'button-detail');

        return $html."&nbsp&nbsp".$builder->getHtml();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoBatchModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_batch', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_batch', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
            $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'geno_batch', 'copy', 'create', array('id' => $row->getId())), 'button-copy');
        }

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'geno_batch', 'feature', 'viewLog', array('id' => $row->getId())), 'button-detail');

        return $builder->getHtml();
    }
}
