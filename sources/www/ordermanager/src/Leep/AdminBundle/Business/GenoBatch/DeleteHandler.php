<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use Leep\AdminBundle\Business\Common\RealDeleteHandler as BaseDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DeleteHandler extends BaseDeleteHandler {
    public function __construct($container, $repository) {
        parent::__construct($container, $repository);
    }

    public function execute() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $id = $this->container->get('request')->query->get('id', 0);

        $record = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:'.$this->repository)->findOneById($id);
        Business\GenoStorage\Utils::updateStorage($this->container, $record, -$record->getAmountInStock(), "Delete");

        // Clear all batch's log
        $logs = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatchLogs')->findByBatchId($id);
        foreach ($logs as $log) {
            $em->remove($log);
        }

        // Clear all internal lot number
        $internalLots = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoLotNumbers')->findByBatchId($id);
        foreach ($internalLots as $lot) {
            $em->remove($lot);
        }

        // Clear all batch status history
        $batchStatusHistoryList = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatchStatusHistory')->findByIdGenoBatch($id);
        foreach ($batchStatusHistoryList as $item) {
            $em->remove($item);
        }

        $em->flush();
        parent::execute();
    }
}
