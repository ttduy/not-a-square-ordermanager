<?php
namespace Leep\AdminBundle\Business\GenoBatch;

use Leep\AdminBundle\Business\Base\AppFilterHandler;
use Leep\AdminBundle\Business;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('materialName', 'text', array(
            'label'    => 'Material\'s name',
            'required' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => Constant::getStatus()
        ));

        $builder->add('excludeStatus', 'choice', array(
            'label' =>      'Exclude status',
            'required' =>   false,
            'choices' =>    Constant::getStatus(),
            'multiple' =>   'multiple',
            'attr' =>       ['style' => 'min-height:400px; resize:both;'],
        ));

        $builder->add('idGenoMaterials', 'choice', array(
            'label'    => 'Material name',
            'required' => false,
            'choices'  => Business\GenoMaterial\Utils::getMaterialList($this->container),
            'multiple' => 'multiple',
            'attr'     => ['style' => 'min-height:400px; resize:both;']
        ));

        $builder->add('location', 'searchable_box', array(
            'label'    => 'Location (In storage)',
            'required' => true,
            'choices'  => [0 => 'All'] + Business\GenoStorage\Utils::getLocationList($this->container),
            'empty_value' => false
        ));

        $builder->add('materialType', 'choice', [
            'label' => 'Types',
            'required' => false,
            'choices' => Business\GenoMaterialType\Utils::getGenoMaterialTypeList($this->container),
            'multiple' => 'multiple',
            'attr' => ['style' => 'min-height:100px; resize:both;'],
            'empty_value' => false
        ]);


        $builder->add('orderedBy', 'searchable_box', array(
            'label'    => 'Ordered by',
            'required' => false,
            'choices'  => [0 => 'All'] + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('orderedDate', 'datepicker', array(
            'label'    => 'Ordered date',
            'required' => false
        ));

        $builder->add('supplierName', 'text', array(
            'label'    => 'Supplier\'s name',
            'required' => false
        ));

        $builder->add('recievedBy', 'searchable_box', array(
            'label'    => 'Recieved by',
            'required' => false,
            'choices'  => [0 => 'All'] + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('recievedDate', 'datepicker', array(
            'label'    => 'Recieved date',
            'required' => false
        ));

        return $builder;
    }
}
