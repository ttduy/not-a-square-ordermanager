<?php
namespace Leep\AdminBundle\Business\Parameter;

class Constants {
    const DATE_FORMAT_DMY = 'd/m/Y';
    const DATE_FORMAT_MDY = 'm/d/Y';
    const DATE_FORMAT_YMD = 'Y/m/d';
    public static function getDateFormat() {
        return array(
            self::DATE_FORMAT_DMY  => 'Day/Month/Year',
            self::DATE_FORMAT_MDY  => 'Month/Day/Year',
            self::DATE_FORMAT_YMD  => 'Year/Month/Day'
        );
    }
    public static function getKendoDateFormat() {
        return array(
            self::DATE_FORMAT_DMY  => 'dd/MM/yyyy',
            self::DATE_FORMAT_MDY  => 'MM/dd/yyyy',
            self::DATE_FORMAT_YMD  => 'yyyy/MM/dd'
        ); 
    }
    public static function getSymfonyDateFormat() {
        return array(
            self::DATE_FORMAT_DMY  => 'dd/MM/yyyy',
            self::DATE_FORMAT_MDY  => 'MM/dd/yyyy',
            self::DATE_FORMAT_YMD  => 'yyyy/MM/dd'
        );         
    }
}