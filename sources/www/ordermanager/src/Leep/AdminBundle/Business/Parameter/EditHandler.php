<?php
namespace Leep\AdminBundle\Business\Parameter;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;


class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $config = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Config')->findAll();
        foreach ($config as $c) {
            return $c;
        }
    }

    private function getLanguages() {
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language')->findAll();
    }
    private function getTextToolTypes() {
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextToolType')->findAll();
    }

    public function convertToFormModel($entity) {
        $parameter = array(
            'customerImportUrl'      => $entity->getCustomerImportUrl(),
            'customerImportUsername' => $entity->getCustomerImportUsername(),
            'customerImportPassword' => $entity->getCustomerImportPassword(),
            'mwaInitialStatus'       => $entity->getMwaInitialStatus(),
            'mwaPushUrl'             => $entity->getMwaPushUrl(),
            'mwaMessagePerSession'   => $entity->getMwaMessagePerSession(),
            'defaultTax'             => $entity->getDefaultTax(),
            'mwaReceivedStatusArr'   => json_decode($entity->getMwaReceivedStatusArr(), true),
            'mwaInprogressStatusArr' => json_decode($entity->getMwaInprogressStatusArr(), true),
            'mwaCompletedStatusArr'  => json_decode($entity->getMwaCompletedStatusArr(), true),
            'materialExpirationNotificationEmail' => $entity->getMaterialExpirationNotificationEmail(),
            'mwaInvoiceYourUid'        => $entity->getMwaInvoiceYourUid(),
            'mwaInvoicePurchaseOrder'  => $entity->getMwaInvoicePurchaseOrder(),
            'mwaInvoiceSupplierNumber' => $entity->getMwaInvoiceSupplierNumber(),
            'mwaInvoiceProduct'        => $entity->getMwaInvoiceProduct(),
            'amwayDefaultRevenue'      => $entity->getAmwayDefaultRevenue(),
            'amwayRussiaRevenue'       => $entity->getAmwayRussiaRevenue(),
            'retentionDay'             => $entity->getRetentionDay(),
            'idFormulaTemplateMwaSample' => $entity->getIdFormulaTemplateMwaSample(),
            'lytechUrl'                => $entity->getLytechUrl(),
            'lytechUsername'           => $entity->getLytechUsername(),
            'lytechPassword'           => $entity->getLytechPassword(),
            'sampleImportJobMinute'    => $entity->getSampleImportJobMinute(),
            'strategyPlanningColorCode' => $entity->getStrategyPlanningColorCode(),
            'feedbackWelcomeMessage'    => $entity->getFeedbackWelcomeMessage(),
            'customerColorCode'         => $entity->getCustomerColorCode(),
            'bodFromCompany'            => $entity->getBodFromCompany(),
            'bodFromPerson'             => $entity->getBodFromPerson(),
            'bodFromEmail'              => $entity->getBodFromEmail(),
            'bodItemTitle'              => $entity->getBodItemTitle(),
            'bodContributorRole'        => $entity->getBodContributorRole(),
            'bodContributorName'        => $entity->getBodContributorName(),
            'bodItemPaper'              => $entity->getBodItemPaper(),
            'bodItemBinding'            => $entity->getBodItemBinding(),
            'bodItemBack'               => $entity->getBodItemBack(),
            'bodItemFinish'             => $entity->getBodItemFinish(),
            'bodItemJacket'             => $entity->getBodItemJacket(),
            'bodOrderCustomsValue'      => $entity->getBodOrderCustomsValue(),
            'bodOrderShipping'          => $entity->getBodOrderShipping(),
            'bodIdDeliveryNoteReport'   => $entity->getBodIdDeliveryNoteReport(),
            'bodFtpServer'              => $entity->getBodFtpServer(),
            'bodFtpUsername'            => $entity->getBodFtpUsername(),
            'bodFtpPassword'            => $entity->getBodFtpPassword(),
            'idDefaultShipmentEmail'    => $entity->getIdDefaultShipmentEmail(),
            'idCryosaveOrderBaby111'    => $entity->getIdCryosaveOrderBaby111(),
            'idCryosaveOrderMilkome'    => $entity->getIdCryosaveOrderMilkome(),
            'idCryosaveOrderBloodom'    => $entity->getIdCryosaveOrderBloodom(),
            'idNovoLaboratory'          => $entity->getIdNovoLaboratory(),
            'cryosaveFtpServer'         => $entity->getCryosaveFtpServer(),
            'cryosaveFtpUsername'       => $entity->getCryosaveFtpUsername(),
            'cryosaveFtpPassword'       => $entity->getCryosaveFtpPassword(),
            'idBillingReportInvoiceCustomer'  => $entity->getIdBillingReportInvoiceCustomer(),
            'idBillingReportInvoiceDistributionChannel'  => $entity->getIdBillingReportInvoiceDistributionChannel(),
            'idBillingReportInvoicePartner'  => $entity->getIdBillingReportInvoicePartner(),
            'idBillingReportPaymentDistributionChannel'  => $entity->getIdBillingReportPaymentDistributionChannel(),
            'idBillingReportPaymentDistributionChannelInvoice' => $entity->getIdBillingReportPaymentDistributionChannelInvoice(),
            'idBillingReportPaymentPartner'  => $entity->getIdBillingReportPaymentPartner(),
            'idBillingReportPaymentPartnerInvoice' => $entity->getIdBillingReportPaymentPartnerInvoice(),
            'idBillingReportPaymentAcquisiteur'  => $entity->getIdBillingReportPaymentAcquisiteur(),
            'idBillingReportPaymentAcquisiteurInvoice' => $entity->getIdBillingReportPaymentAcquisiteurInvoice(),
            'idBillingReportReminderNotice'  => $entity->getIdBillingReportReminderNotice(),
            'billingBaselineDate'       => $entity->getBillingBaselineDate(),
            'billingText'               => $entity->getBillingText(),
            'nutriMeProducts'           => json_decode($entity->getNutriMeProducts(), true),
            'defaultWebLoginEmailPartner'               => $entity->getDefaultWebLoginEmailPartner(),
            'defaultWebLoginEmailDistributionChannel'   => $entity->getDefaultWebLoginEmailDistributionChannel(),
            'defaultWebLoginEmailCustomerInfo'          => $entity->getDefaultWebLoginEmailCustomerInfo(),
            'customerLeadDefaultStatus'                 => $entity->getCustomerLeadDefaultStatus(),
            'customerDashboardLoginURL'                 => $entity->getCustomerDashboardLoginURL(),
            'customerDashboardLanguages'                => Utils::decodeCustomerDashboardLanguages($entity->getCustomerDashboardLanguages()),
            'customerDashboardLoginEmailTemplate'       => $entity->getCustomerDashboardLoginEmailTemplate(),
            'emailMethodCustomerDashboard'              => $entity->getEmailMethodCustomerDashboard(),
            'shopOrderNutrimeBasedOnYourGeneUrl'        => $entity->getShopOrderNutrimeBasedOnYourGeneUrl(),
            'massMailerHost'                            => $entity->getMassMailerHost(),
            'massMailerPort'                            => $entity->getMassMailerPort(),
            'massMailerEncryption'                      => $entity->getMassMailerEncryption(),
            'massMailerUsername'                        => $entity->getMassMailerUsername(),
            'massMailerPassword'                        => $entity->getMassMailerPassword(),
            'massMailerSenderName'                      => $entity->getMassMailerSenderName(),
            'massMailerSenderEmail'                     => $entity->getMassMailerSenderEmail(),
            'massMailerRate'                            => $entity->getMassMailerRate(),
            'mandrillApiKey'                            => $entity->getMandrillApiKey(),
            'awsAccessKeyId'                            => $entity->getAwsAccessKeyId(),
            'awsSecretAccessKey'                        => $entity->getAwsSecretAccessKey(),
            'awsRegion'                                 => $entity->getAwsRegion(),
            'unlockPassword'                            => $entity->getUnlockPassword(),
            'dcMarketingStatusReminderLimitPassedDays'  => $entity->getDcMarketingStatusReminderLimitPassedDays(),
            'defaultRatingDistributionChannel'          => $entity->getDefaultRatingDistributionChannel(),
            'defaultRatingPartner'                      => $entity->getDefaultRatingPartner(),
            'defaultRatingAcquisiteur'                  => $entity->getDefaultRatingAcquisiteur(),
            'orderBackupDateDuration'                   => $entity->getOrderBackupDateDuration(),
            'untranslatedPattern'                       => $entity->getUntranslatedPattern(),
            'cmsContactMessageNotificationEmailList'    => $entity->getCmsContactMessageNotificationEmailList(),
            'emailMethodCmsOrder'                       => $entity->getEmailMethodCmsOrder()
        );

        // CUSTOMER DASHBOARD - TEXT BLOCK GROUP
        if ($entity->getCustomerDashboardIdTextBlockGroup()) {
            $parameter['customerDashboardIdTextBlockGroup'] = json_decode($entity->getCustomerDashboardIdTextBlockGroup(), true);
        }

        // Alphabet
        $languages = $this->getLanguages();
        foreach ($languages as $language) {
            $parameter['alphabet_'.$language->getId()] = $language->getAlphabet();
        }

        // Text tool colors
        $textToolTypes = $this->getTextToolTypes();
        foreach ($textToolTypes as $type) {
            $parameter['textToolColor_'.$type->getId()] = $type->getBgColor();
        }

        return $parameter;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // DISTRIBUTION CHANNEL
        $builder->add('sectionDC', 'section', array(
            'label'    => 'Distribution Channel',
            'property_path' => false
        ));
        $builder->add('dcMarketingStatusReminderLimitPassedDays', 'text', array(
            'label'    => 'DC Info Status - Reminder - Limit Passed Days',
            'required' => false
        ));

        // LEAD
        $builder->add('sectionLead', 'section', array(
            'label'    => 'Lead',
            'property_path' => false
        ));
        $builder->add('customerLeadDefaultStatus', 'choice', array(
            'label'    => 'Default status of Lead submitted by Customer',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_Status')
        ));

        // CUSTOMER
        $builder->add('sectionCustomer', 'section', array(
            'label'    => 'Customer',
            'property_path' => false
        ));
        $builder->add('customerColorCode', 'textarea', array(
            'label' => 'Customer color codes',
            'required' => false,
            'attr' => array(
                'rows' => 5, 'cols' => 70
            )
        ));
        $builder->add('unlockPassword', 'text', array(
            'label'    => 'Unlock Password',
            'required' => false
        ));
        $builder->add('orderBackupDateDuration', 'text', array(
            'label'    => 'Order Backup Duration (in days)',
            'required' => false
        ));

        // CUSTOMER IMPORT
        $builder->add('sectionCustomerImport', 'section', array(
            'label'    => 'Customer Import',
            'property_path' => false
        ));
        $builder->add('customerImportUrl', 'text', array(
            'label'    => 'URL',
            'required' => false
        ));
        $builder->add('customerImportUsername', 'text', array(
            'label'    => 'Username',
            'required' => false
        ));
        $builder->add('customerImportPassword', 'text', array(
            'label'    => 'Password',
            'required' => false
        ));

        // MWA PROCESSOR
        $builder->add('sectionMwaProcessor', 'section', array(
            'label'    => 'Mwa Processor',
            'property_path' => false
        ));
        $builder->add('mwaPushUrl', 'text', array(
            'label'    => 'Push URL',
            'required' => false
        ));
        $builder->add('mwaInitialStatus', 'choice', array(
            'label'    => 'Initial Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List')
        ));
        $builder->add('mwaMessagePerSession', 'text', array(
            'label'    => 'Messages per 5 minutes',
            'required' => false
        ));
        $builder->add('idFormulaTemplateMwaSample', 'choice', array(
            'label'    => 'Pattern Calculation Formula',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_FormulaTemplate_List')
        ));

        // MWA RUSSIA PROCESSOR
        $builder->add('sectionMwaRussiaProcessor', 'section', array(
            'label'    => 'Mwa Russia Processor',
            'property_path' => false
        ));
        $builder->add('lytechUrl', 'text', array(
            'label'    => 'Lytech URL',
            'required' => false
        ));
        $builder->add('lytechUsername', 'text', array(
            'label'    => 'Lytech Username',
            'required' => false
        ));
        $builder->add('lytechPassword', 'text', array(
            'label'    => 'Lytech Password',
            'required' => false
        ));
        $builder->add('sampleImportJobMinute', 'text', array(
            'label'    => 'Job per minutes',
            'required' => false
        ));

        // MWA INVOICE
        $builder->add('sectionMwaInvoice', 'section', array(
            'label'    => 'Mwa Invoice',
            'property_path' => false
        ));
        $builder->add('mwaInvoiceYourUid', 'text', array(
            'label'    => 'Default Your UID',
            'required' => false
        ));
        $builder->add('mwaInvoicePurchaseOrder', 'text', array(
            'label'    => 'Default Purchase Order',
            'required' => false
        ));
        $builder->add('mwaInvoiceSupplierNumber', 'text', array(
            'label'    => 'Default Supplier Number',
            'required' => false
        ));
        $builder->add('mwaInvoiceProduct', 'text', array(
            'label'    => 'Default Product',
            'required' => false
        ));

        // DEFAULT VALUES
        $builder->add('sectionDefaultValue', 'section', array(
            'label'    => 'Default values',
            'property_path' => false
        ));
        $builder->add('defaultTax', 'text', array(
            'label'    => 'Default Tax Value',
            'required' => false
        ));

        // FINANCE TRACKER
        $builder->add('sectionFinanceTracker', 'section', array(
            'label'    => 'Finance Tracker',
            'property_path' => false
        ));
        $builder->add('amwayDefaultRevenue', 'text', array(
            'label'    => 'Amway default revenue',
            'required' => false
        ));
        $builder->add('amwayRussiaRevenue', 'text', array(
            'label'    => 'Amway russia revenue',
            'required' => false
        ));

        // MWA DASHBOARD
        $builder->add('sectionMwaDashboard', 'section', array(
            'label'    => 'MWA Dashboard',
            'property_path' => false
        ));
        $builder->add('mwaReceivedStatusArr', 'choice', array(
            'label'    => 'Received status list',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'attr'     => array(
                'style'  => 'height: 120px'
            )
        ));
        $builder->add('mwaInprogressStatusArr', 'choice', array(
            'label'    => 'Inprogress status list',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'attr'     => array(
                'style'  => 'height: 120px'
            )
        ));
        $builder->add('mwaCompletedStatusArr', 'choice', array(
            'label'    => 'Completed status list',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'attr'     => array(
                'style'  => 'height: 120px'
            )
        ));

        // MATERIAL
        $builder->add('sectionMaterial', 'section', array(
            'label'    => 'Material',
            'property_path' => false
        ));
        $builder->add('materialExpirationNotificationEmail', 'text', array(
            'label' => 'Expiration notification emails',
            'required' => false,
            'attr'     => array(
                'class' => 'mediuminput'
            )
        ));


        // OFFLINE JOB
        $builder->add('sectionOfflineJob', 'section', array(
            'label'    => 'Offline job',
            'property_path' => false
        ));
        $builder->add('retentionDay', 'text', array(
            'label'    => 'Retention day',
            'required' => false
        ));

        // ALPHABET
        $builder->add('sectionLanguageAlphabet', 'section', array(
            'label'    => 'Language alphabet',
            'property_path' => false
        ));
        $languages = $this->getLanguages();
        foreach ($languages as $language) {
            $builder->add('alphabet_'.$language->getId(), 'text', array(
                'label' => $language->getName(),
                'required' => false,
                'attr'     => array(
                    'class' => 'mediuminput'
                )
            ));
        }

        // TEXT TOOL COLOR
        $builder->add('sectionTextToolColor', 'section', array(
            'label'    => 'Text Tool Colors',
            'property_path' => false
        ));
        $textToolTypes = $this->getTextToolTypes();
        foreach ($textToolTypes as $type) {
            $builder->add('textToolColor_'.$type->getId(), 'text', array(
                'label' => $type->getName(),
                'required' => false,
                'attr'     => array(
                    'class' => 'smallinput'
                )
            ));
        }

        // STRATEGY PLANNING TOOL
        $builder->add('sectionStrategyPlanningTool', 'section', array(
            'label'    => 'Strategy Planning Tool',
            'property_path' => false
        ));
        $builder->add('strategyPlanningColorCode', 'textarea', array(
            'label' => 'Color codes',
            'required' => false,
            'attr' => array(
                'rows' => 5, 'cols' => 70
            )
        ));

        // FEEDBACK
        $builder->add('sectionFeedback', 'section', array(
            'label'    => 'Feedback',
            'property_path' => false
        ));
        $builder->add('feedbackWelcomeMessage', 'textarea', array(
            'label' => 'Welcome message',
            'required' => false,
            'attr' => array(
                'rows' => 5, 'cols' => 70
            )
        ));

        // BOOK ON DEMAND
        $builder->add('sectionBookOnDemand', 'section', array(
            'label'    => 'Book on demand',
            'property_path' => false
        ));
        $builder->add('bodFromCompany', 'text', array(
            'label' => 'From company',
            'required' => false
        ));
        $builder->add('bodFromPerson', 'text', array(
            'label' => 'From person',
            'required' => false
        ));
        $builder->add('bodFromEmail', 'text', array(
            'label' => 'From email',
            'required' => false
        ));
        $builder->add('bodItemTitle', 'text', array(
            'label' => 'Item title',
            'required' => false
        ));
        $builder->add('bodContributorRole', 'text', array(
            'label' => 'Contributor role',
            'required' => false
        ));
        $builder->add('bodContributorName', 'text', array(
            'label' => 'Contributor name',
            'required' => false
        ));
        $builder->add('bodItemPaper', 'text', array(
            'label' => 'Item paper',
            'required' => false
        ));
        $builder->add('bodItemBinding', 'text', array(
            'label' => 'Item binding',
            'required' => false
        ));
        $builder->add('bodItemBack', 'text', array(
            'label' => 'Item back',
            'required' => false
        ));
        $builder->add('bodItemFinish', 'text', array(
            'label' => 'Item finish',
            'required' => false
        ));
        $builder->add('bodItemJacket', 'text', array(
            'label' => 'Item jacket',
            'required' => false
        ));
        $builder->add('bodOrderCustomsValue', 'text', array(
            'label' => 'Order customs value',
            'required' => false
        ));
        $builder->add('bodOrderShipping', 'text', array(
            'label' => 'Order shipping',
            'required' => false
        ));
        $builder->add('bodIdDeliveryNoteReport', 'choice', array(
            'label' => 'Delivery note report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idDefaultShipmentEmail', 'choice', array(
            'label' => 'Default shipment email',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
        ));
        $builder->add('bodFtpServer', 'text', array(
            'label' => 'FTP Server',
            'required' => false
        ));
        $builder->add('bodFtpUsername', 'text', array(
            'label' => 'FTP Username',
            'required' => false
        ));
        $builder->add('bodFtpPassword', 'text', array(
            'label' => 'FTP Password',
            'required' => false
        ));

        // CRYOSAVE
        $builder->add('sectionCryosave', 'section', array(
            'label'    => 'Cryosave',
            'property_path' => false
        ));
        $builder->add('idCryosaveOrderBaby111', 'choice', array(
            'label' => 'Baby111 report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idCryosaveOrderMilkome', 'choice', array(
            'label' => 'Milkome report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idCryosaveOrderBloodom', 'choice', array(
            'label' => 'Bloodom report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idNovoLaboratory', 'choice', array(
            'label' => 'Novo laboratory',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Laboratory')
        ));
        $builder->add('cryosaveFtpServer', 'text', array(
            'label' => 'FTP Server',
            'required' => false
        ));
        $builder->add('cryosaveFtpUsername', 'text', array(
            'label' => 'FTP Username',
            'required' => false
        ));
        $builder->add('cryosaveFtpPassword', 'text', array(
            'label' => 'FTP Password',
            'required' => false
        ));

        // BILLING
        $builder->add('sectionBilling', 'section', array(
            'label'    => 'Billing',
            'property_path' => false
        ));
        $builder->add('idBillingReportInvoiceCustomer', 'choice', array(
            'label' => 'Report - Invoice Customer',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportInvoiceDistributionChannel', 'choice', array(
            'label' => 'Report - Invoice DC',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportInvoicePartner', 'choice', array(
            'label' => 'Report - Invoice Partner',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentDistributionChannel', 'choice', array(
            'label' => 'Report - Payment DC (Credit notes)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentDistributionChannelInvoice', 'choice', array(
            'label' => 'Report - Payment DC (Invoice)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentPartner', 'choice', array(
            'label' => 'Report - Payment Partner (Credit notes)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentPartnerInvoice', 'choice', array(
            'label' => 'Report - Payment Partner (Invoice)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentAcquisiteur', 'choice', array(
            'label' => 'Report - Payment Acquisiteur (Credit notes)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportPaymentAcquisiteurInvoice', 'choice', array(
            'label' => 'Report - Payment Acquisiteur (Invoice)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('idBillingReportReminderNotice', 'choice', array(
            'label' => 'Report - Reminder notice',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('billingBaselineDate', 'datepicker', array(
            'label' => 'Baseline Date',
            'required' => false
        ));
        $builder->add('billingText', 'textarea', array(
            'label' => 'Billing Text',
            'required' => false,
            'attr'  => array(
                'rows' => 7,
                'cols' => 70
            )
        ));

        // NUTRI ME
        $builder->add('sectionNutriMe', 'section', array(
            'label'    => 'Nutri ME',
            'property_path' => false
        ));
        $builder->add('nutriMeProducts', 'choice', array(
            'label' => 'NutriME Products',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'attr'    => array(
                'style'   => 'min-height: 200px'
            )
        ));

        // WEB LOGIN EMAIL
        $builder->add('sectionWebLoginEmail', 'section', array(
            'label'    => 'Web Login Email',
            'property_path' => false
        ));

        $builder->add('defaultWebLoginEmailPartner', 'choice', array(
            'label' => 'Default Web Login Email - Partner',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
        ));

        $builder->add('defaultWebLoginEmailDistributionChannel', 'choice', array(
            'label' => 'Default Web Login Email - Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
        ));

        $builder->add('defaultWebLoginEmailCustomerInfo', 'choice', array(
            'label' => 'Default Web Login Email - Customer Info',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
        ));

        // CUSTOMER DASHBOARD LOGIN URL
        $builder->add('sectionCustomerDashboard', 'section', array(
            'label'    => 'Web Login',
            'property_path' => false
        ));

        $builder->add('customerDashboardLoginURL', 'text', array(
            'label'    => 'Customer Dashboard - Login URL',
            'required' => false
        ));

        $builder->add('customerDashboardIdTextBlockGroup', 'choice', array(
            'label'    => 'Customer Dashboard - Text Block Group',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_TextBlockGroup_List'),
            'attr'      => array(
                'style' => 'height: 300px'
            )
        ));

        $builder->add('customerDashboardLanguages', 'choice', array(
            'label'     => 'Customer Dashboard - Languages',
            'required'  => false,
            'multiple'  => 'multiple',
            'choices'   => $mapping->getMapping('LeepAdmin_Customer_Language'),
            'attr'      => array(
                'style' => 'height: 300px'
            )
        ));

        $builder->add('customerDashboardLoginEmailTemplate', 'choice', array(
            'label'    => 'Customer Dashboard - Login Email Template',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
        ));

        $builder->add('emailMethodCustomerDashboard', 'choice', array(
            'label'     => 'Customer Dashboard - Email Method',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('shopOrderNutrimeBasedOnYourGeneUrl', 'text', array(
            'label'    => 'Shop - Order NutriMe based on your genes URL',
            'required' => false
        ));

        $builder->add('shopOrderPrintedBookletUrl', 'text', array(
            'label'    => 'Shop - Order printed booklet URL',
            'required' => false
        ));

        // MASS MAILER
        $builder->add('sectionMassMailer', 'section', array(
            'label'     => 'Mass Mailer'
        ));

        $builder->add('massMailerHost', 'text', array(
            'label'     => 'Host',
            'required'  => false
        ));

        $builder->add('massMailerPort', 'text', array(
            'label'     => 'Port',
            'required'  => false
        ));

        $builder->add('massMailerEncryption', 'text', array(
            'label'     => 'Encryption',
            'required'  => false
        ));

        $builder->add('massMailerUsername', 'text', array(
            'label'     => 'Username',
            'required'  => false
        ));

        $builder->add('massMailerPassword', 'text', array(
            'label'     => 'Password',
            'required'  => false
        ));

        $builder->add('massMailerSenderName', 'text', array(
            'label'     => 'Sender Name',
            'required'  => false
        ));

        $builder->add('massMailerSenderEmail', 'text', array(
            'label'     => 'Sender Email',
            'required'  => false
        ));

        $builder->add('massMailerRate', 'text', array(
            'label'     => 'Rate',
            'required'  => false
        ));

        // MANDRILL
        $builder->add('sectionMandrill', 'section', array(
            'label'     => 'Mandrill'
        ));

        $builder->add('mandrillApiKey', 'text', array(
            'label'     => 'Mandrill API Key',
            'required'  => false
        ));

        // AWS
        $builder->add('sectionAws', 'section', array(
            'label'     => 'AWS'
        ));

        $builder->add('awsRegion', 'text', array(
            'label'     => 'Region',
            'required'  => false
        ));

        $builder->add('awsAccessKeyId', 'text', array(
            'label'     => 'Access Key ID',
            'required'  => false
        ));

        $builder->add('awsSecretAccessKey', 'text', array(
            'label'     => 'Secret Access Key',
            'required'  => false
        ));

        // RATING
        $builder->add('sectionRating', 'section', array(
            'label'     => 'Rating'
        ));
        $builder->add('defaultRatingDistributionChannel', 'textarea', array(
            'label'     => 'Default Rating - Distribution Channel',
            'required'  => false,
            'attr'      => array(
                'rows'    => 4,
                'cols'    => 70
            )
        ));
        $builder->add('defaultRatingPartner', 'textarea', array(
            'label'     => 'Default Rating - Partner',
            'required'  => false,
            'attr'      => array(
                'rows'    => 4,
                'cols'    => 70
            )
        ));
        $builder->add('defaultRatingAcquisiteur', 'textarea', array(
            'label'     => 'Default Rating - Acquisiteur',
            'required'  => false,
            'attr'      => array(
                'rows'    => 4,
                'cols'    => 70
            )
        ));

        // REPORT
        $builder->add('sectionReport', 'section', array(
            'label'     => 'Report'
        ));
        $builder->add('untranslatedPattern', 'textarea', array(
            'label'     => 'Untranslated Patterns',
            'required'  => false,
            'attr'      => array(
                'rows'    => 8,
                'cols'    => 70
            )
        ));

        // CMS
        $builder->add('sectionCms', 'section', array(
            'label'     => 'CMS'
        ));
        $builder->add('cmsContactMessageNotificationEmailList', 'text', array(
            'label'     => 'Notification emails (separated by ;)',
            'required'  => false
        ));
        $builder->add('emailMethodCmsOrder', 'choice', array(
            'label'     => 'Email method cms order',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        foreach ($model as $k => $v) {
            $setMethod = 'set'.ucfirst($k);
            if (method_exists($this->entity, $setMethod)) {
                $this->entity->$setMethod($v);
            }
        }
        $this->entity->setMwaReceivedStatusArr(json_encode($model['mwaReceivedStatusArr']));
        $this->entity->setMwaInprogressStatusArr(json_encode($model['mwaInprogressStatusArr']));
        $this->entity->setMwaCompletedStatusArr(json_encode($model['mwaCompletedStatusArr']));
        $this->entity->setNutriMeProducts(json_encode($model['nutriMeProducts']));
        $this->entity->setCustomerDashboardIdTextBlockGroup(json_encode($model['customerDashboardIdTextBlockGroup']));

        // Save alphabet
        $languages = $this->getLanguages();
        foreach ($languages as $language) {
            $key = 'alphabet_'.$language->getId();
            $value = isset($model[$key]) ? $model[$key] : '';
            $language->setAlphabet($value);
        }

        // Save text tool colors
        $textToolTypes = $this->getTextToolTypes();
        foreach ($textToolTypes as $type) {
            $key = 'textToolColor_'.$type->getId();
            $value = isset($model[$key]) ? $model[$key] : '';
            $type->setBgColor($value);
        }

        // Save customer dashboard - languages
        $this->entity->setCustomerDashboardLanguages(Utils::encodeCustomerDashboardLanguages($model['customerDashboardLanguages']));

        parent::onSuccess();
    }
}
