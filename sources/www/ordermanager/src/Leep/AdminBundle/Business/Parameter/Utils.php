<?php
namespace Leep\AdminBundle\Business\Parameter;

class Utils {
    public static function encodeCustomerDashboardLanguages($arrItems) {
        $result = null;
        if ($arrItems) {
            $result = implode(',', $arrItems);
        }

        return $result;
    }

    public static function decodeCustomerDashboardLanguages($data) {
        $result = array();
        if ($data) {
            $result = explode(',', $data);
        }

        return $result;
    }
}