<?php
namespace Leep\AdminBundle\Business\RecipeBookLayout;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:RecipeBookLayout', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        $model->data = $entity->getData();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));       
        $builder->add('data', 'textarea', array(
            'label'    => 'Layout Data',
            'required' => true,
            'attr'     => array(
                'rows'   => 40,
                'cols'   => 70
            )
        ));   

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();      
          
        $this->entity->setName($model->name);
        $this->entity->setData($model->data);

        parent::onSuccess();
    }
}