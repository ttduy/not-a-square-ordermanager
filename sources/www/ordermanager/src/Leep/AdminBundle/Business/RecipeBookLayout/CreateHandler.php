<?php
namespace Leep\AdminBundle\Business\RecipeBookLayout;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));       
        $builder->add('data', 'textarea', array(
            'label'    => 'Layout Data',
            'required' => true,
            'attr'     => array(
                'rows'   => 40,
                'cols'   => 70
            )
        ));         

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $layout = new Entity\RecipeBookLayout();
        $layout->setName($model->name);
        $layout->setData($model->data);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($layout);
        $em->flush();

        parent::onSuccess();
    }
}