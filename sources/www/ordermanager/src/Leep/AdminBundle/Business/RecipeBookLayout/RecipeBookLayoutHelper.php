<?php
namespace Leep\AdminBundle\Business\RecipeBookLayout;

class RecipeBookLayoutHelper {
    public $container;
    public $idLayout;
    public function __construct($container) {
        $this->container = $container;
    }

    public $config = array();

    protected function parseData($data) {
        $rows = explode("\n", $data);
        $a = array();

        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (count($arr) == 2) {
                $a[trim($arr[0])] = trim($arr[1]);
            }
        }

        return $a;
    }

    public function addLayout($id, $data) {
        $this->config[$id] = $this->parseData($data);
    }

    public function overrideLayoutData($data) {
        $arr = $this->parseData($data);
        foreach ($arr as $k => $v) {
            $arr = explode('.', $k);
            $idLayout = array_shift($arr);
            $this->config[$idLayout][implode('.', $arr)] = $v;
        }
    }

    public function setIdLayout($idLayout) {
        $this->idLayout = $idLayout;
    }

    public function get($var) {
        if (isset($this->config[$this->idLayout]) && isset($this->config[$this->idLayout][$var])) {
            return $this->config[$this->idLayout][$var];
        }
        return '';
    }
}