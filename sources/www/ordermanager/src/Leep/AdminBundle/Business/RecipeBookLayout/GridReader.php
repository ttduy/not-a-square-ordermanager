<?php
namespace Leep\AdminBundle\Business\RecipeBookLayout;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'data', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',           'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Data',           'width' => '55%', 'sortable' => 'false'),
            array('title' => 'Action',         'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:RecipeBookLayout', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('recipeModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'recipe_book_layout', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'recipe_book_layout', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }

    public function buildCellData($row) {
        return nl2br(substr($row->getData(), 0, 200)).'..';
    }
}