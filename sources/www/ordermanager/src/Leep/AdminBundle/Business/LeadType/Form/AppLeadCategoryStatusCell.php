<?php
namespace Leep\AdminBundle\Business\LeadType\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppLeadCategoryStatusCell extends AbstractType {
	protected $container;
	public function __construct($container){
		$this->container = $container;
	}

	public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\LeadType\Form\AppLeadCategoryStatusCellModel',
            //'typeMapping' => ''
        );
    }


	public function getParent(){
		return 'field';
	}

	//return to twig file
	public function getName(){
		return 'app_lead_category_status_cell';
	}

	public function finishView(FormView $view, FormInterface $form, array $options){
		parent::finishView($view, $form, $options);
	}
 
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$mapping = $this->container->get('easy_mapping');

		$builder->add('id', 'hidden');

		$builder->add('number', 'text',[
			'label'		=> 'Number',
			'required'	=> false,
			'attr'		=> [
				'class'		=> 'level',
				'style'		=>	'width: 25px; opacity: 0.7',
			]
		]);

		$builder->add('status', 'choice', array(
			'label'			=> 'Status',
			'required'		=> false,
			'choices'		=> $mapping->getMapping('LeepAdmin_Lead_Status'),
			'attr'			=> array(
				'style'			=> 'max-width: 150px; min-width: 150px'
			)
		));

		$builder->add('value', 'text', [
			'label'		=> 'Value',
			'required'	=>	false,
			'attr'		=> [
				'style'		=> 'width: 100px;'
			]
		]);
	}
}
