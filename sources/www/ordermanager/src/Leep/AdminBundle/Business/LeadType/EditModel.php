<?php
namespace Leep\AdminBundle\Business\LeadType;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
	public $type;
	public $description;
	public $idLeadStatus;
	public $statusList;
	public $status;
}
