<?php
namespace Leep\AdminBundle\Business\LeadType;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
	public function __construct($container) {
		parent::__construct($container);
	}

	public function execute() {
		$id = $this->container->get('request')->query->get('id', 0);
		$em = $this->container->get('doctrine')->getEntityManager();

		$leadType = $em->getRepository('AppDatabaseMainBundle:LeadType')->findOneById($id);
		//delete Lead itself
		$em->remove($leadType);
		$em->flush();
	}
}