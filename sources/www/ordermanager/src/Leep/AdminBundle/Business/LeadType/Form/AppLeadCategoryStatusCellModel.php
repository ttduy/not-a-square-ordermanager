<?php
namespace Leep\AdminBundle\Business\LeadType\Form;

class AppLeadCategoryStatusCellModel {
    public $id;
    public $status;
    public $value;
    public $number;
    public $level;
}
