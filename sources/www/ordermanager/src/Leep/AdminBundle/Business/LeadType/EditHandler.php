<?php
namespace Leep\AdminBundle\Business\LeadType;

use Leep\AdminBundle\Business\Base\AppEditHandler; 
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
	public function loadEntity($request) {
		$id = $request->query->get('id');
		return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:LeadType', 'app_main')->findOneById($id);
	}

	public function convertToFormModel($entity) {


		$model = new EditModel();
		$model->type = $entity->getType();
		$model->description = $entity->getDescription();

		//build lead status to form model
		$doctrine = $this->container->get('doctrine');
		$model->statusList = array();

		$results = $doctrine->getRepository('AppDatabaseMainBundle:LeadCategoryStatus')->findBy(['idLeadCategory' => $entity->getId()]);
		
		// // get the current status
		foreach($results as $status){
			$mapping = new Form\AppLeadCategoryStatusCellModel();
			$mapping->number = $status->getLevel();
			$mapping->status = $status->getIdLeadStatus();

			$model->statusList[] = $mapping;
		}

		return $model;
	}

	public function buildForm($builder) {
		$mapping = $this->container->get('easy_mapping');

		$builder->add('type', 'text', array(
			'label'		=> 'Name',
			'required'	=> true
		));

		$builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));

        // Cell
		$builder->add('statusSection', 'section', [
            'label' =>          'lead Status',
            'property_path' =>  false
        ]);

		$builder->add('statusList', 'container_collection', array(
			'label' => 'Status',
			'required' => false,
            'type' 	=> new Form\AppLeadCategoryStatusCell($this->container)
        ));

		return $builder;
	}

	public function onSuccess() {
		$model = $this->getForm()->getData();

		$em = $this->container->get('doctrine')->getManager();

		$this->entity->setType($model->type);
		$this->entity->setDescription($model->description);

		$delQuery = $em->createQueryBuilder();
		$delQuery->delete('AppDatabaseMainBundle:LeadCategoryStatus', 'p')
					->andWhere('p.idLeadCategory = :idLeadCategory')
					->setParameter('idLeadCategory', $this->entity->getId())
					->getQuery()
					->execute();

		foreach($model->statusList as $statusRow){
			if (empty($statusRow)) continue;
			$status = new Entity\LeadCategoryStatus();
			$status->setIdLeadStatus($statusRow->status);
			$status->setLeadStatusValue($statusRow->value);

			$status->setIdLeadCategory($this->entity->getId());
			$em->persist($status);
		}
		$em->flush();

		parent::onSuccess();
	}
}
