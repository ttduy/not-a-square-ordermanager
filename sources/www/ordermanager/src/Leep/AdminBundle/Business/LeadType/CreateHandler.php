<?php
namespace Leep\AdminBundle\Business\LeadType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
	public function getDefaultFormModel() {
		$model = new CreateModel();
		return $model;
	}

	public function buildForm($builder) {
		$mapping = $this->container->get('easy_mapping');

		$builder->add('leadSection', 'section', [
            'label' =>          'Lead Category',
            'property_path' =>  false
        ]);

		$builder->add('type', 'text', array (
			'label'		=> 'Name',
			'required'	=> true
		));
		
		$builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));

		// Cell
		$builder->add('statusSection', 'section', [
            'label' =>          'lead Status',
            'property_path' =>  false
        ]);


		$builder->add('idLeadStatus', 'container_collection', array(
			'label' => 'Status',
			'required' => false,
            'type' 	=> new Form\AppLeadCategoryStatusCell($this->container)
        ));

		// return $builder;

	}

	public function onSuccess() {
		$em = $this->container->get('doctrine')->getEntityManager();
		$model = $this->getForm()->getData();

		$leadType = trim($model->type);

		if(empty($leadType)){
			$this->errors[] = 'Errors';
			$this->getForm()->get('type')->addError(new FormError('Lead type must not be empty'));
			return false;
		}

		$type = new Entity\LeadType();
		$type->setType($model->type);
		$type->setDescription($model->description);
		$em->persist($type);
		$em->flush();

		// // save 
		foreach($model->idLeadStatus as $statusRow){
			if (empty($statusRow)) continue;
			$status = new Entity\LeadCategoryStatus();
			$status->setIdLeadStatus($statusRow->status);
			$status->setLeadStatusValue($statusRow->value);
			$status->setIdLeadCategory($type->getId());
			$status->setLevel($statusRow->number);
			$em->persist($status);
		}

		$em->flush();
		parent::onSuccess();
	}
}

