<?php
namespace Leep\AdminBundle\Business\LeadType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
	public $filter;
	public function getColumnMapping() {
		return array('type', 'description', 'action');
	}

    public function getTableHeader() {
        return array(
            array('title' => 'Name', 	         'width' => '40%', 'sortable' => 'true'),
            array('title' => 'Description',      'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',	         'width' => '20%', 'sortable' => 'false'),
        );
    }

	public function getFormatter() {
		return null;
	}

	public function buildQuery($queryBuilder) {
		$queryBuilder
			->select('p')
			->from('AppDatabaseMainBundle:LeadType', 'p');
	}

	public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('leadTypeModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'lead_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');

            $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'lead_type', 
                                                            'feature', 'viewLeadEntry', 
                                                            array('id' => $row->getId()), 'button-detail'));

            if (!$this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Lead', 'app_main')->findOneByIdCategory($row->getId())) {
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'lead_type', 'delete', 'delete', array('id' => $row->getId())), 
                                                'button-delete');
            }
            else{
                return $builder->getHtml() . '<span style="position: relative; margin-left: 4px; top: -4px; width:200px; color:#ff0000"><b>Cannot delete
                </b></span>';    
            }
        }

        return $builder->getHtml();
    }
}
