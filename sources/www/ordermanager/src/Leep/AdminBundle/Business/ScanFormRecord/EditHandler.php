<?php
namespace Leep\AdminBundle\Business\ScanFormRecord;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormRecord', 'app_main')->findOneById($id);
    }

    public function getScanImageUrl($dir, $batchName, $id, $name) {             
        $webUrl = $this->container->getParameter('app_web_url');
        return $webUrl.'/files/scan_form_images/'.$dir.'/'.$batchName.'/'.$id.'/'.$name.'.jpg';
    }    

    public function parseScanData($scanData) {
        $data = json_decode($scanData, true);
        if (!is_array($data)) {
            $data = array();
        }

        $rows = array();
        foreach ($data as $k => $v) {
            $rows[] = $k.' = '.$v;
        }
        return implode("\n", $rows);
    }

    public function convertToFormModel($entity) {
        $scanFormBatch = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormBatch')->findOneById($entity->getIdScanFormBatch());
        
        $model = new EditModel();
        $model->scanImage = $this->getScanImageUrl('form_parse', $scanFormBatch->getName(), $entity->getId(), 'marked');
        $model->scanData = $this->parseScanData($entity->getFormData());

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('sectionScanForm', 'section', array(
            'label'    => 'Scan Form',
            'property_path' => false
        ));
        $builder->add('scanImage', 'label', array(
            'label'    => 'Scan Image',
            'required' => false
        ));        
        $builder->add('scanData', 'textarea', array(
            'label'    => 'Scan data',
            'required' => false,
            'attr'     => array(
                'rows' => 12, 'cols' => 70
            )
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $data = json_decode($this->entity->getFormData(), true);
        if (!is_array($data)) {
            $data = array();
        }

        $rows = explode("\n", $model->scanData);
        foreach ($rows as $v) {
            $a = explode('=', $v);
            if (count($a) >= 2) {
                $key = trim(array_shift($a));
                $value = implode('=', $a);
                $data[$key] = trim($value);
            }
        }
        print_r($data);

        parent::onSuccess();              
    }
}
