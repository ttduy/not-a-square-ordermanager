<?php
namespace Leep\AdminBundle\Business\ScanFormRecord;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idScanFormBatch', 'inputTimestamp', 'imageFilename', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Batch',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Input time',   'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Image',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Status',       'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',       'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ScanFormRecordGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFormRecord', 'p')
            ->innerJoin('AppDatabaseMainBundle:ScanFormBatch', 'b', 'with', 'p.idScanFormBatch = b.id')
            ->andWhere('b.status = :batchStatusDone')
            ->setParameter('batchStatusDone', Business\ScanFormBatch\Constants::STATUS_DONE);
        if (trim($this->filters->idScanFormBatch) != '') {
            $queryBuilder->andWhere('p.idScanFormBatch = :idScanFormBatch')
                ->setParameter('idScanFormBatch', $this->filters->idScanFormBatch);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormDataModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'scan_form_record', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        return $builder->getHtml();
    }
}
