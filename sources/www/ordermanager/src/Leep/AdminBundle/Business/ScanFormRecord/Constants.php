<?php
namespace Leep\AdminBundle\Business\ScanFormRecord;

class Constants {
    const STATUS_PROCESSING       = 1;
    const STATUS_DONE             = 2;
    const STATUS_ERROR            = 99;

    public static function getScanFormRecordStatus() {
        return array(
            self::STATUS_PROCESSING     => 'Processing',
            self::STATUS_DONE           => 'Done',
            self::STATUS_ERROR          => 'Error'
        );
    }
}
