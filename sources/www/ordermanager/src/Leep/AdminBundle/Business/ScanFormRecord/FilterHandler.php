<?php
namespace Leep\AdminBundle\Business\ScanFormRecord;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idScanFormBatch', 'choice', array(
            'label'    => 'Scan form batch',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_ScanFormBatch_List'),
            'empty_value' => false
        ));

        return $builder;
    }
}