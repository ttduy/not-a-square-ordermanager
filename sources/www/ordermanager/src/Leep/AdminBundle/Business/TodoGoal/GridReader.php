<?php
namespace Leep\AdminBundle\Business\TodoGoal;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'creationDate', 'status', 'idFinishIn', 'statistics', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',          'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Creation date', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Status',        'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Finish in',     'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Statistics',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_TodoGoalGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:TodoGoal', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');            
        $builder->addButton('Detail', $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'applyTaskFilter', array('idGoal' => $row->getId())), 'button-detail');
        if ($helperSecurity->hasPermission('strategyPlanningToolGoalModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'todo_goal', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'todo_goal', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellStatistics($row) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p.status, COUNT(p) total')
            ->from('AppDatabaseMainBundle:TodoTask', 'p')
            ->andWhere('p.idGoal = :idGoal')
            ->setParameter('idGoal', $row->getId())
            ->groupBy('p.status');
        $results = $query->getQuery()->getResult();
        $status = array();
        foreach ($results as $r) {
            $status[$r['status']] = $r['total'];
        }

        $numActive = isset($status[Business\TodoTask\Constants::STATUS_ACTIVE]) ? $status[Business\TodoTask\Constants::STATUS_ACTIVE] : 0;
        $numCompleted = isset($status[Business\TodoTask\Constants::STATUS_COMPLETED]) ? $status[Business\TodoTask\Constants::STATUS_COMPLETED] : 0;

        return sprintf("%s active task(s)<br/>%s completed task(s)", $numActive, $numCompleted);
    }

    public function buildCellName($row) {
        $html = '<b>'.$row->getName().'</b><br/>';
        $html .= nl2br($row->getDescription())."<br/>";

        if ($row->getMilestones() != '') {
            $html .= "<br/>Milestones: <br/>".nl2br($row->getMilestones());
        }
        return $html;
    }
}
