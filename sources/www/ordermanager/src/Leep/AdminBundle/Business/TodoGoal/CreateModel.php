<?php
namespace Leep\AdminBundle\Business\TodoGoal;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $creationDate;
    public $name;
    public $description;
    public $milestones;
    public $status;
    public $idFinishIn;
}
