<?php
namespace Leep\AdminBundle\Business\TodoGoal;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->creationDate = new \DateTime();
        $model->status = Constants::STATUS_FOR_DISCUSSION;
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('milestones', 'textarea', array(
            'label'    => 'Milestones',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_Status')
        ));
        $builder->add('idFinishIn', 'choice', array(
            'label'    => 'Finish in',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_FinishIn')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $goal = new Entity\TodoGoal();
        $goal->setCreationDate($model->creationDate);
        $goal->setName($model->name);
        $goal->setDescription($model->description);
        $goal->setMilestones($model->milestones);
        $goal->setStatus($model->status);
        $goal->setIdFinishIn($model->idFinishIn);
        $em->persist($goal);
        $em->flush();

        parent::onSuccess();
    }
}
