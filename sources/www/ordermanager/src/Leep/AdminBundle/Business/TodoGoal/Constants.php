<?php
namespace Leep\AdminBundle\Business\TodoGoal;

class Constants {
    const STATUS_FOR_DISCUSSION    = 1;
    const STATUS_IN_PROGRESS       = 2;
    const STATUS_SOLVED            = 3;
    const STATUS_REJECTED_ON_HOLD  = 4;
    const STATUS_EFFECTIVENESS_PROVEN = 5;

    public static function getStatus() {
        return array(
            self::STATUS_FOR_DISCUSSION        => 'For discussion',
            self::STATUS_IN_PROGRESS           => 'In progress',
            self::STATUS_SOLVED                => 'Solved',
            self::STATUS_REJECTED_ON_HOLD      => 'Rejected/on hold',
            self::STATUS_EFFECTIVENESS_PROVEN  => 'Effectiveness proven'
        );
    }

    const FINISH_IN_NOW               = 1;
    const FINISH_IN_1_WEEK            = 2;
    const FINISH_IN_1_MONTH           = 3;
    const FINISH_IN_3_MONTHS          = 4; 
    const FINISH_IN_6_MONTHS          = 5;
    const FINISH_IN_1_YEAR            = 6;
    const FINISH_IN_2_YEARS           = 7;
    const FINISH_IN_5_YEARS           = 8;
    const FINISH_IN_NO_TIME_DEFINED   = 9;
    
    public static function getFinishIn() {
        return array(
            self::FINISH_IN_NOW                => 'Now',
            self::FINISH_IN_1_WEEK             => '1 week',
            self::FINISH_IN_1_MONTH            => '1 month',
            self::FINISH_IN_3_MONTHS           => '3 months',
            self::FINISH_IN_6_MONTHS           => '6 months', 
            self::FINISH_IN_1_YEAR             => '1 year', 
            self::FINISH_IN_2_YEARS            => '2 years',
            self::FINISH_IN_5_YEARS            => '5 years',
            self::FINISH_IN_NO_TIME_DEFINED    => 'No time defined'
        );
    }
}
