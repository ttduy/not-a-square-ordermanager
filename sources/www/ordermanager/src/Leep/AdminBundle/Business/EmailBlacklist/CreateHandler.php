<?php
namespace Leep\AdminBundle\Business\EmailBlacklist;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return new CreateModel();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $emailMethod = new Entity\EmailOptOutList();
        $emailMethod->setEmail($model->email);
        $em->persist($emailMethod);
        $em->flush();

        parent::onSuccess();
    }
}
