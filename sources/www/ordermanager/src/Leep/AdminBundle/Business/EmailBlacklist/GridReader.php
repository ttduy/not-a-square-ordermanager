<?php
namespace Leep\AdminBundle\Business\EmailBlacklist;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'email', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Id', 'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Email', 'width' => '50%', 'sortable' => 'true'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailOptOutList', 'p');

        if (trim($this->filters->email) != '') {
            $queryBuilder->andWhere('p.email LIKE :email')
                ->setParameter('email', '%'.trim($this->filters->email).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('emailBlacklistModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'email_blacklist', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'email_blacklist', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
