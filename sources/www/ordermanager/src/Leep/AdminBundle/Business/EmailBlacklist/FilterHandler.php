<?php
namespace Leep\AdminBundle\Business\EmailBlacklist;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

         $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));

        return $builder;
    }
}