<?php
namespace Leep\AdminBundle\Business\EmailBlacklist;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailOptOutList', 'app_main')
            ->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->email = $entity->getEmail();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('email', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $this->entity->setEmail($model->email);
        parent::onSuccess();
    }
}
