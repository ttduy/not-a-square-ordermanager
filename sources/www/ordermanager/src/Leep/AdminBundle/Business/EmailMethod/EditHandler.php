<?php
namespace Leep\AdminBundle\Business\EmailMethod;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailMethod', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->username = $entity->getUsername();
        $model->password = $entity->getPassword();
        $model->senderName = $entity->getSenderName();
        $model->senderEmail = $entity->getSenderEmail();
        $model->host = $entity->getHost();
        $model->port = $entity->getPort();
        $model->transport = $entity->getTransport();
        $model->encryption = $entity->getEncryption();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => false
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => false
        ));
        $builder->add('senderName', 'text', array(
            'label'    => 'Sender Name',
            'required' => false
        ));
        $builder->add('senderEmail', 'text', array(
            'label'    => 'Sender Email',
            'required' => false
        ));
        $builder->add('host', 'text', array(
            'label'    => 'Host',
            'required' => false
        ));
        $builder->add('port', 'text', array(
            'label'    => 'Port',
            'required' => false
        ));
        $builder->add('transport', 'text', array(
            'label'    => 'Transport',
            'required' => false
        ));
        $builder->add('encryption', 'text', array(
            'label'    => 'Encryption',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setUsername($model->username);
        $this->entity->setPassword($model->password);
        $this->entity->setSenderName($model->senderName);
        $this->entity->setSenderEmail($model->senderEmail);
        $this->entity->setHost($model->host);
        $this->entity->setPort($model->port);
        $this->entity->setTransport($model->transport);
        $this->entity->setEncryption($model->encryption);

        parent::onSuccess();
    }
}
