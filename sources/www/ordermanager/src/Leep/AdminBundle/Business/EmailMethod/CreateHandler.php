<?php
namespace Leep\AdminBundle\Business\EmailMethod;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->encryption = 'ssl';
        $model->transport = 'smtp';

        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => false
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => false
        ));
        $builder->add('senderName', 'text', array(
            'label'    => 'Sender Name',
            'required' => false
        ));
        $builder->add('senderEmail', 'text', array(
            'label'    => 'Sender Email',
            'required' => false
        ));
        $builder->add('host', 'text', array(
            'label'    => 'Host',
            'required' => false
        ));
        $builder->add('port', 'text', array(
            'label'    => 'Port',
            'required' => false
        ));
        $builder->add('transport', 'text', array(
            'label'    => 'Transport',
            'required' => false
        ));
        $builder->add('encryption', 'text', array(
            'label'    => 'Encryption',
            'required' => false
        ));


        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $emailMethod = new Entity\EmailMethod();
        $emailMethod->setName($model->name);
        $emailMethod->setUsername($model->username);
        $emailMethod->setPassword($model->password);
        $emailMethod->setSenderName($model->senderName);
        $emailMethod->setSenderEmail($model->senderEmail);
        $emailMethod->setHost($model->host);
        $emailMethod->setPort($model->port);
        $emailMethod->setTransport($model->transport);
        $emailMethod->setEncryption($model->encryption);
        $em->persist($emailMethod);
        $em->flush();

        parent::onSuccess();
    }
}
