<?php
namespace Leep\AdminBundle\Business\EmailMethod;

class EditModel {
    public $name;
    public $username;
    public $password;
    public $senderName;
    public $senderEmail;
    public $host;
    public $port;
    public $transport;
    public $encryption;
}
