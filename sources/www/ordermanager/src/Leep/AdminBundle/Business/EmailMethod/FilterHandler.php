<?php
namespace Leep\AdminBundle\Business\EmailMethod;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

         $builder->add('name', 'text', array(
            'label'    => 'Email Method\'s Name',
            'required' => false
        ));
        $builder->add('senderName', 'text', array(
            'label'    => 'Sender\'s Name',
            'required' => false
        ));
        $builder->add('senderEmail', 'text', array(
            'label'    => 'Sender\'s Email',
            'required' => false
        ));

        return $builder;
    }
}