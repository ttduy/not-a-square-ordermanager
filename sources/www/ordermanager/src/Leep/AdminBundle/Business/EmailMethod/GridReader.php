<?php
namespace Leep\AdminBundle\Business\EmailMethod;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'sender', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Sender', 'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailMethod', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->senderName) != '') {
            $queryBuilder->andWhere('p.senderName LIKE :senderName')
                ->setParameter('senderName', '%'.trim($this->filters->senderName).'%');
        }

        if (trim($this->filters->senderEmail) != '') {
            $queryBuilder->andWhere('p.senderEmail LIKE :senderEmail')
                ->setParameter('senderEmail', '%'.trim($this->filters->senderEmail).'%');
        }
    }

    public function buildCellSender($row) {
        return sprintf("%s (%s)", $row->getSenderEmail(), $row->getSenderName());
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('emailMethodModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'email_method', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'email_method', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
