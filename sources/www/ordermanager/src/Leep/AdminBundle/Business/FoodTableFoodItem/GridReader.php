<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idCategory', 'foodId', 'name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Category',    'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Food ID',     'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Name',        'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FoodItemGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:FoodTableFoodItem', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('(p.name LIKE :name) OR (p.foodId LIKE :name)')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->idCategory) != 0) {
            $queryBuilder->andWhere('p.idCategory = :idCategory')   
                ->setParameter('idCategory', $this->filters->idCategory);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('foodTableModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'food_table_food_item', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'food_table_food_item', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}