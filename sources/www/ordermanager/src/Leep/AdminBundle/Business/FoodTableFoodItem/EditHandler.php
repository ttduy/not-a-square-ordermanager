<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableFoodItem', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->foodId = $entity->getFoodId();
        $model->idCategory = $entity->getIdCategory();
        $model->ingredientData = Utils::loadIngredientData($this->container, $entity->getIngredientData());

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('foodId', 'text', array(
            'label'    => 'Food ID',
            'required' => true
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableCategory_List')
        ));

        $builder->add('sectionIngredient', 'section', array(
            'label'    => 'Ingredient',
            'property_path' => false
        ));
        $builder->add('ingredientData', 'textarea', array(
            'label'    => 'Ingredients',
            'attr'     => array(
                'rows'        => 20,
                'cols'        => 70
            ),
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setName($model->name);
        $this->entity->setFoodId($model->foodId);
        $this->entity->setIdCategory($model->idCategory);
        $this->entity->setGPortion($model->gPortion);
        $this->entity->setKcal($model->kcal);
        $this->entity->setProtG($model->protG);
        $this->entity->setCarbG($model->carbG);
        $this->entity->setFatG($model->fatG);
        $this->entity->setIngredientData($model->ingredientData);

        parent::onSuccess();
    }
}