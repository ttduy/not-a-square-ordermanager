<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SynchronizeHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SynchronizeModel();

        return $model;
    }

    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('attachment', 'file', array(
            'label'    => 'Attachment',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $separator = ",";
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        

        if (!empty($model->attachment) && $model->attachment->isValid()) { 
            set_time_limit(240);                                

            // Move to tmp dir, encoding to UTF-8
            $tmpDir = $this->container->getParameter('kernel.root_dir').'/tmp';
            $model->attachment->move($tmpDir, 'food_table_item_upload.csv');

            // Import            
            if (($handle = fopen($tmpDir.'/food_table_item_upload.csv', "r")) !== FALSE) {
                // Delete food table item
                $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
                $query
                    ->delete('AppDatabaseMainBundle:FoodTableFoodItem', 'p')
                    ->getQuery()->execute();

                // Delete food category
                $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
                $query
                    ->delete('AppDatabaseMainBundle:FoodTableCategory', 'p')
                    ->getQuery()->execute();

                // Parse header
                $lineMaxLength = 100000;
                $fields = array();
                $header = fgetcsv($handle, $lineMaxLength, $separator);

                // Synchronize ingredients                
                $existingIngredients = array();
                $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
                foreach ($result as $r) {
                    $existingIngredients[$r->getVariableName()] = 1;
                }

                $ingredientStartIndex = 7;
                $ingredients = array_slice($header, $ingredientStartIndex);
                foreach ($ingredients as $i => $ingredient) {
                    if (!isset($existingIngredients[$ingredient])) {
                        $ingredient = preg_replace('/[\,|\-|\?|\.|\/|\)|\(|\&|\+|\s]/i', '_', $ingredient);
                        $ingredient = preg_replace('/\_\_+/i', '_', $ingredient);
                        $ingredient = trim($ingredient,' _');

                        $newIngredient = new Entity\FoodTableIngredient();
                        $newIngredient->setName('[['.$ingredient.']]');
                        $newIngredient->setVariableName($ingredient);
                        $newIngredient->setRiskAmountMax(100);
                        $em->persist($newIngredient);                       

                        $ingredients[$i] = $ingredient;
                    }
                    $em->flush();
                }

                // Load data
                $dataSet = array();
                $rowId = 0;
                while (($data = fgetcsv($handle, $lineMaxLength, $separator)) !== FALSE) {
                    if (trim($data[0]) == '') {
                        continue;
                    }

                    $dataSet[] = $data;
                }

                // Synchronize category
                $categories = array();
                foreach ($dataSet as $data) {
                    $foodId = $data[0];
                    $categoryId = $data[2];
                    $categoryName = $data[3].' - '.$data[2];
                    $foodName = $data[5];
                    if (!isset($categories[$categoryId])) {
                        $newCategory = new Entity\FoodTableCategory();
                        $newCategory->setName($categoryName);
                        $em->persist($newCategory);
                        $em->flush();

                        $categories[$categoryId] = $newCategory->getId();
                    }
                    $idCategory = $categories[$categoryId];
                }

                // Synchronze food item
                foreach ($dataSet as $data) {
                    $foodId = $data[0];
                    $categoryId = $data[2];
                    $foodName = $data[5];
                    $idCategory = $categories[$categoryId];

                    $foodTableItem = new Entity\FoodTableFoodItem();
                    $foodTableItem->setIdCategory($idCategory);
                    $foodTableItem->setFoodId(trim($foodId));
                    $foodTableItem->setName('[['.trim($foodName).']]');

                    $ingredientList = array();
                    $i = 0;
                    foreach ($ingredients as $ingredientName) {
                        $ingredientList[] = $ingredientName.' = '.floatval($data[$ingredientStartIndex+$i]);
                        $i++;
                    }
                    $foodTableItem->setIngredientData(implode("\n", $ingredientList));
                    $em->persist($foodTableItem);
                }
                $em->flush();

                fclose($handle);
            }            
        }
        else {
            $this->errors[] = "Upload failed";
            return false;
        }

        //parent::onSuccess();
        $this->messages[] = "Food table item has been synchronized";
    }
}