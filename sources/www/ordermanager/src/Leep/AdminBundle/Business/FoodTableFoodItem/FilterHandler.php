<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idCategory = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_FoodTableCategory_List')
        ));
        return $builder;
    }
}