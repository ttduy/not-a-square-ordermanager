<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

class CreateModel {
    public $name;
    public $foodId;
    public $idCategory;
    public $gPortion;
    public $kcal;
    public $protG;
    public $carbG;
    public $fatG;

    public $ingredientData;
}