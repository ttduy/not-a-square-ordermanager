<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

class Utils {
    public static function loadIngredientData($container, $currentIngredient) {
        $curIngredients = array();
        $arr = explode("\n", $currentIngredient);
        foreach ($arr as $r) {
            $a = explode('=', $r);
            if (count($a) == 2) {
                $curIngredients[trim($a[0])] = floatval(trim($a[1]));
            }
        }

        $ingredients = array();
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
        foreach ($result as $r) {
            $varName = $r->getVariableName();
            $val = isset($curIngredients[$varName]) ? $curIngredients[$varName] : 0;
            $ingredients[] = $varName.' = '.$val;
        }
        return implode("\n", $ingredients);
    }
}