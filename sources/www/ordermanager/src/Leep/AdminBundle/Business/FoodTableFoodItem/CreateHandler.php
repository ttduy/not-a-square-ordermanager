<?php
namespace Leep\AdminBundle\Business\FoodTableFoodItem;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->ingredientData = Utils::loadIngredientData($this->container, '');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('foodId', 'text', array(
            'label'    => 'Food ID',
            'required' => true
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_FoodTableCategory_List')
        ));

        $builder->add('sectionIngredient', 'section', array(
            'label'    => 'Ingredient',
            'property_path' => false
        ));
        $builder->add('ingredientData', 'textarea', array(
            'label'    => 'Ingredients',
            'attr'     => array(
                'rows'        => 20,
                'cols'        => 70
            ),
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $foodItem = new Entity\FoodTableFoodItem();
        $foodItem->setName($model->name);
        $foodItem->setFoodId($model->foodId);
        $foodItem->setIdCategory($model->idCategory);
        $foodItem->setIngredientData($model->ingredientData);

        $em = $this->container->get('doctrine')->getEntityManager();
        $em->persist($foodItem);
        $em->flush();

        parent::onSuccess();
    }
}