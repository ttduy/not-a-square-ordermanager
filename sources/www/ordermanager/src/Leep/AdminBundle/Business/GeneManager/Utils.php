<?php
namespace Leep\AdminBundle\Business\GeneManager;

class Utils {
    public static function applyGeneManagerFilter($container, $queryBuilder) {
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        // Must be status : Same forwarded to analysis unit : 26 OR Show sample in Gene Manager: 60
        $queryBuilder->andWhere('(FIND_IN_SET(60, p.statusList) > 0)');

        /* Must not be status:
        *   - Results ready : 7        
        *   - Report submitted : 8
        *   - Order canceled : 11
        *   - FINISHED now archive : 23
        */
        $arr_must_not_be = array(7, 8, 11, 23);
        foreach ($arr_must_not_be as $mustNotBeStatus) {
            $queryBuilder->andWhere('FIND_IN_SET('.$mustNotBeStatus.', p.statusList) = 0');
        }
    }

    public static function checkMassarray($container, $idOrder, $idCustomer) {
        $mapping = $container->get('easy_mapping');
        $formatter = $container->get('leep_admin.helper.formatter');
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Prepare massarray
        $massarray = $mapping->getMappingFiltered('LeepAdmin_Massarray_ExportedList');
        foreach ($massarray as $id => $name) {
            $massarray[$id] = array(
                'name' => $name,
                'genes' => array(),
                'numMissing' => 0
            );
        }

        // Get customer info gene result
        $customerGenes = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $idCustomer);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $result = trim($r->getResult());
            if (empty($result)) { $result = ''; }

            $customerGenes[$r->getIdGene()] = $result;
        }

        // Get Massarray-result list        
        $query = $em->createQueryBuilder();
        $query->select('
                mg.massarrayid,
                pg.geneid')
                //cig.result')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->innerJoin('AppDatabaseMainBundle:ProdGeneLink', 'pg', 'WITH', 'p.productid = pg.productid')
            ->innerJoin('AppDatabaseMainBundle:MassarrayGeneLink', 'mg', 'WITH', 'mg.geneid = pg.geneid')
            ->andWhere('p.customerid = :idOrder')
            ->setParameter('idOrder', $idOrder)
            ->groupBy('mg.massarrayid, pg.geneid');
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $massarrayid = $r['massarrayid'];
            if (isset($massarray[$massarrayid])) {
                $idGene = intval($r['geneid']);
                $geneResult = isset($customerGenes[$idGene]) ? $customerGenes[$idGene] : '';

                $massarray[$massarrayid]['genes'][] = array(
                    'name'      => $formatter->format($r['geneid'], 'mapping', 'LeepAdmin_Gene_List'),
                    'rsnumber'  => $formatter->format($r['geneid'], 'mapping', 'LeepAdmin_Gene_RsNumber'),
                    'result'    => $geneResult
                );

                if (empty($geneResult)) {
                    $massarray[$massarrayid]['numMissing']++;
                }                
            }
        }

        return $massarray;
    }
}