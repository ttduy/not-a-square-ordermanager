<?php
namespace Leep\AdminBundle\Business\GeneManager;

class Parser {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $resultGroup = array();
    public $genes = array();
    public $orders = array();

    public function init() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Result groups
        $result = $em->getRepository('AppDatabaseMainBundle:Results')->findAll();
        foreach ($result as $r) {
            $idGroup = intval($r->getGroupId());
            $callResult = trim($r->getCallResult());
            $result = trim($r->getResult());

            if (!isset($this->resultGroup[$idGroup])) {
                $this->resultGroup[$idGroup] = array();
            }
            $this->resultGroup[$idGroup][$callResult] = $result;
        }

        // Gene mapping
        $result = $em->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        foreach ($result as $r) {
            if ($r->getIsDeleted() == 1) {
                continue;
            }

            $name = $r->getGeneName();
            $rsNumber = $r->getRsNumber();
            $idGroup = intval($r->getGroupId());

            $this->genes[$rsNumber] = array(
                'name'  => $name,
                'resultMap' => isset($this->resultGroup[$idGroup]) ? $this->resultGroup[$idGroup] : array()
            );
        }
    }

    public function parseResult($textResult) {
        $rows = explode("\n", $textResult);
        foreach ($rows as $row) {
            $this->parseRow($row);
        }
    }

    public function parseRow($row) {
        $tokens = explode("\t", $row);
        if (count($tokens) != 4) {
            return false;
        }

        $orderNumber = trim($tokens[0]);
        $rsNumber = trim($tokens[1]);
        $result = trim($tokens[3]);

        if (!isset($this->genes[$rsNumber])) {
            // Can't find RsNumber
            return false;
        }

        if (!isset($this->orders[$orderNumber])) {
            $this->orders[$orderNumber] = array();
        }
        if (!isset($this->orders[$orderNumber][$rsNumber])) {
            $this->orders[$orderNumber][$rsNumber] = array(
                'name'   => $this->genes[$rsNumber]['name'],
                'result' => array()
            );
        }

        $convertedResult = '';
        if (isset($this->genes[$rsNumber]['resultMap'][$result])) {
            $convertedResult = $this->genes[$rsNumber]['resultMap'][$result];
        }

        $this->orders[$orderNumber][$rsNumber]['result'][] = $convertedResult;
    }

    public function checkConflict() {
        $conflictOrders = array();

        foreach ($this->orders as $orderNumber => $genes) {
            foreach ($genes as $rsNumber => $gene) {
                if (!isset($gene['result'][0])) {
                    continue;
                }
                $result = $gene['result'][0];

                $isConflict = false;
                foreach ($gene['result'] as $otherResult) {
                    if (strcmp($result, $otherResult) != 0) {
                        $isConflict = true;
                    }
                }

                if ($isConflict) {
                    if (!isset($conflictOrders[$orderNumber])) {
                        $conflictOrders[$orderNumber] = array();
                    }

                    $conflictOrders[$orderNumber][$rsNumber] = $gene;
                }   
            }
        }

        return $conflictOrders;
    }
}