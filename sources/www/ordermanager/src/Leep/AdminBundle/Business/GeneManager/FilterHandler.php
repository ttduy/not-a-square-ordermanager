<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));        
        $builder->add('isRecall', 'choice', array(
            'label'    => 'Is Recall',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(
                0 => 'All',
                1 => 'Yes',
                2 => 'No'
            )
        ));        
        $builder->add('orderNumberList', 'textarea', array(
            'label'    => 'Order number list',
            'required' => false,
            'attr'     => array('rows' => 5, 'cols' => 50)
        ));
        
        return $builder;
    }
}