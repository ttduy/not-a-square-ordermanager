<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class ResultHistoryGridReader extends AppGridDataReader {
    public $filters;
    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        return array('timestamp', 'idWebUser', 'additionalInformation', 'previewData', 'error', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Datetime',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Edited by',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Additional information',   'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Result',      'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Error',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GeneManagerResultHistoryGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.timestamp', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.timestamp, p.previewData, p.numFormatError, p.numDataError, p.numConflictError, p.numSuccess, p.numWarning, p.status, p.idWebUsers', 'p.additionalInformation')
            ->from('AppDatabaseMainBundle:GeneManagerResultHistory', 'p');

        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('(p.result LIKE :orderNumber) OR (p.result LIKE :orderNumber2) OR (p.result LIKE :orderNumber3)')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber)."\t".'%')
                ->setParameter('orderNumber2', '%'.trim($this->filters->orderNumber)." %")
                ->setParameter('orderNumber3', '%'.trim($this->filters->orderNumber).";%");
        }

        if (!empty($this->filters->idWebUser)) {
            $queryBuilder->andWhere('p.idWebUsers = :idWebUser')
                ->setParameter('idWebUser', $this->filters->idWebUser);
        }
        if (!empty($this->filters->additionalInformation)) {
            $queryBuilder->andWhere('p.additionalInformation LIKE :additionalInformation')
                ->setParameter('additionalInformation', '%'. $this->filters->additionalInformation. '%');
        }

        if (intval($this->filters->idErrorType) != 0) {
            if (intval($this->filters->idErrorType) == Constant::ERROR_TYPE_ANY) {
                $queryBuilder->andWhere('(p.numFormatError > 0) OR (p.numDataError > 0) OR (p.numConflictError > 0)');
            }
            else if (intval($this->filters->idErrorType) == Constant::ERROR_TYPE_FORMAT_ERROR) {
                $queryBuilder->andWhere('(p.numFormatError > 0)');
            }
            else if (intval($this->filters->idErrorType) == Constant::ERROR_TYPE_DATA_ERROR) {
                $queryBuilder->andWhere('(p.numDataError > 0)');
            }
            else if (intval($this->filters->idErrorType) == Constant::ERROR_TYPE_CONFLICT_ERROR) {
                $queryBuilder->andWhere('(p.numConflictError > 0)');
            }
            else if (intval($this->filters->idErrorType) == Constant::ERROR_TYPE_WARNING) {
                $queryBuilder->andWhere('(p.numWarning > 0)');
            }
        }
    }

    public function buildCellPreviewData($row) {
        $mgr = $this->getModuleManager();

        $html = nl2br($row['previewData']).'...<br/>';
        $html .= '<a href="'.$mgr->getUrl('leep_admin', 'gene_manager', 'feature', 'downloadResultHistory', array('id' => $row['id'])).'">Download</a>';

        return $html;
    }

    public function buildCellIdWebUser($row) {
        $webUser = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUsers', 'app_main')->findOneById($row['idWebUsers']);
        if ($webUser) {
            return $webUser->getUsername()." (#".$row['idWebUsers'].")";
        }

        return '';
    }

    public function buildCellError($row) {
        $mgr = $this->getModuleManager();

        if ($row['status'] == Constant::STATUS_OPEN) {
            return "Pending for processing";
        }
        else if ($row['status'] == Constant::STATUS_INPROGRESS) {
            return "In progress";
        }

        $html = '';
        if (intval($row['numFormatError']) == 0) {
            $html .= '0 format errors';
        }
        else {
            $html .= '<b style="color: green">'.intval($row['numFormatError'])." format errors</b>";
        }
        $html .= '<br/>';

        if (intval($row['numDataError']) == 0) {
            $html .= '0 data errors';
        }
        else {
            $html .= '<b style="color: orange">'.intval($row['numDataError'])." data errors</b>";
        }
        $html .= '<br/>';

        if (intval($row['numConflictError']) == 0) {
            $html .= '0 conflict errors';
        }
        else {
            $html .= '<b style="color: red">'.intval($row['numConflictError'])." conflict errors</b>";
        }
        $html .= '<br/>';

        if (intval($row['numWarning']) == 0) {
            $html .= '0 warning';
        }
        else {
            $html .= '<b style="color: orange">'.intval($row['numWarning'])." warning</b>";
        }
        $html .= '<br/>';

        if (intval($row['numSuccess']) == 0) {
            $html .= '0 success';
        }
        else {
            $html .= '<b style="color: blue">'.intval($row['numSuccess'])." success records</b>";
        }
        $html .= '<br/>';


        $html .= '<br/><a target="_blank" href="'.$mgr->getUrl('leep_admin', 'gene_manager', 'feature', 'viewErrors', array('id' => $row['id'])).'">View errors</a>';

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('geneManagerView')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'gene_manager', 'delete_result_history', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }
}
