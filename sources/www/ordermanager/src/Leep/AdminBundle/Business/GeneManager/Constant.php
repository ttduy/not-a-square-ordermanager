<?php 
namespace Leep\AdminBundle\Business\GeneManager;

class Constant {
    const ERROR_TYPE_FORMAT_ERROR       = 1;
    const ERROR_TYPE_DATA_ERROR         = 2;
    const ERROR_TYPE_CONFLICT_ERROR     = 3;
    const ERROR_TYPE_WARNING            = 4;

    const TYPE_SUCCESS                = 98;
    const ERROR_TYPE_ANY              = 99;


    const STATUS_OPEN                 = 1;
    const STATUS_INPROGRESS           = 2;
    const STATUS_DONE                 = 3;
}
