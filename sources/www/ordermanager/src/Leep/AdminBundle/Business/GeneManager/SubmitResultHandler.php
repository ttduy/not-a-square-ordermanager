<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class SubmitResultHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SubmitResultModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('result', 'textarea', array(
            'label'    => 'Result',
            'required' => true,
            'attr'     => array(
                'style'   => 'width: 800px;height: 400px'
            )
        ));
        $builder->add('additionalInformation', 'text', array(
            'label'    => 'Additional information',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $parser = $this->container->get('leep_admin.gene_manager.business.submit_result_parser');
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');

        $doctrine = $this->container->get('doctrine');
        $model = $this->getForm()->getData();

        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        // Save history
        $resultHistory = new Entity\GeneManagerResultHistory();
        $resultHistory->setTimestamp(new \DateTime());
        $resultHistory->setResult($model->result);
        $resultHistory->setPreviewData(substr($model->result, 0, 100));
        $resultHistory->setNumFormatError(0);
        $resultHistory->setNumDataError(0);
        $resultHistory->setNumConflictError(0);
        $resultHistory->setNumSuccess(0);
        $resultHistory->setIdWebUsers($userManager->getUser()->getId());
        $resultHistory->setStatus(Constant::STATUS_OPEN);
        $resultHistory->setAdditionalInformation($model->additionalInformation);

        $em->persist($resultHistory);
        $em->flush();

        parent::onSuccess();

        $this->messages = array("Added to processing queue! Please wait 5-10 minutes for processing");

        $cacheBox->putKey('geneResult', array());
    }
}
