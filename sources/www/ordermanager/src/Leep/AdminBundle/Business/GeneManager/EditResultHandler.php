<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditResultHandler extends AppEditHandler {   
    public $idCustomerInfo = 0;
    public $massarrayId = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $arr_params = explode('_', $id);
        $cus_id = $arr_params[0];
        $this->massarrayId = $arr_params[1];
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($cus_id);
    }

    public function getGeneList($idCustomerInfo) {
        $mapping = $this->container->get('easy_mapping');

        // Result options
        $resultOptions = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Results')->findAll();
        foreach ($result as $row) {
            if (!isset($resultOptions[$row->getGroupId()])) {
                $resultOptions[$row->getGroupId()] = array(0 => '');
            }
            $resultOptions[$row->getGroupId()][$row->getResult()] = $row->getResult();
        }

        // get Massarray-Result list of specific Customer
        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('
                        cig.id as cig_id
                        , cig.result as cig_final_result
                        , g.genename g_name
                        , g.groupid g_group_id
                        , g.rsnumber
                        , r.result as r_optional_result
                      ')
                ->from('AppDatabaseMainBundle:CustomerInfoGene', 'cig')
                ->leftJoin('AppDatabaseMainBundle:Genes', 'g', 'WITH', 'g.id = cig.idGene')
                ->leftJoin('AppDatabaseMainBundle:Results', 'r', 'WITH', 'r.result = cig.result AND r.groupid = g.groupid')
                ->innerJoin('AppDatabaseMainBundle:MassarrayGeneLink', 'mg', 'WITH', 'mg.geneid = cig.idGene')
                ->andWhere('cig.idCustomer = :customer_id')
                ->andWhere('mg.massarrayid = :massarray_id')
                ->setParameter('customer_id', $idCustomerInfo)
                ->setParameter('massarray_id', $this->massarrayId);

        $result = $query->getQuery()->getResult();

        $genes = array();
        foreach ($result as $r) {
            $genes[$r['cig_id']] = array(
                'gene_name' => $r['rsnumber'].' ('.$r['g_name'].')',
                'final_result' => ($r['cig_final_result'] ? $r['cig_final_result'] : 0),
                'result_choices' => isset($resultOptions[$r['g_group_id']]) ? $resultOptions[$r['g_group_id']] : array(0 => '')
            );
        }
        
        return $genes;
    }

    public function convertToFormModel($entity) {
        $this->idCustomerInfo = $entity->getIdCustomerInfo();

        $model = array('orderNumber' => $entity->getOrderNumber());

        $genes = $this->getGeneList($this->idCustomerInfo);
        foreach ($genes as $id => $gene) {
            $model['geneResult_'.$id] = $gene['final_result'];
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('section1',          'section', array(
            'label' => 'Customer Order',
            'property_path' => false
        ));
        $builder->add('orderNumber',            'label', array(
            'label' => 'Order Number',
            'required'      => false
        ));
        $builder->add('section2',          'section', array(
            'label' => 'Result',
            'property_path' => false
        ));
        
        $genes = $this->getGeneList($this->idCustomerInfo);
        foreach ($genes as $id => $gene) {
            $builder->add('geneResult_'.$id, 'choice', array(
                'label' => $gene['gene_name'],
                'required' => false,
                'choices' => $gene['result_choices'],
                'data' => $gene['final_result'],
                'empty_value' => false,
            ));
        }

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        foreach ($model as $k => $v) {
            if (strpos($k, 'geneResult_') === 0) {
                $rId = intval(substr($k, 11));
                $row = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfoGene', 'app_main')->findOneBy(array(
                    'id'         => $rId
                ));

                if ($row != FALSE) {
                    $row->setResult($v);
                }
            }
        }

        parent::onSuccess();
    }
}
