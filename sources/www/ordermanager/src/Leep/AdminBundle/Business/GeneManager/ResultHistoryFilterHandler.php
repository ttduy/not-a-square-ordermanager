<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class ResultHistoryFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new ResultHistoryFilterModel();
        $model->idErrorType = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));
        $builder->add('idWebUser', 'choice', array(
            'label' =>      'Edited By',
            'required' =>   false,
            'choices' =>    $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('idErrorType', 'choice', array(
            'label'    => 'Error',
            'required' => false,
            'choices'  => array(
                0 => '',
                Constant::ERROR_TYPE_ANY             => 'Has errors',
                Constant::ERROR_TYPE_FORMAT_ERROR    => 'Has format errors',
                Constant::ERROR_TYPE_DATA_ERROR      => 'Has data errors',
                Constant::ERROR_TYPE_CONFLICT_ERROR  => 'Has conflict errors',
                Constant::ERROR_TYPE_WARNING         => 'Has warning',
            ),
            'empty_value' => false
        ));
        $builder->add('additionalInformation', 'text', array(
            'label' =>      'Additional information',
            'required' =>   false
        ));
        return $builder;
    }
}
