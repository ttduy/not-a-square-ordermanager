<?php
namespace Leep\AdminBundle\Business\GeneManager;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $productMapping = [
        'products' => [],
        'categories' => [],
        'special' => []
    ];

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        $em = $container->get('doctrine')->getEntityManager();
        $products = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findAll();
        foreach ($products as $product) {
            $productGenes = $em->getRepository('AppDatabaseMainBundle:ProdGeneLink')->findByProductid($product->getId());
            $genes = [];
            foreach ($productGenes as $gene) {
                $genes[] = $gene->getGeneid();
            }

            $this->productMapping['products'][$product->getId()] = [
                'name' => $product->getName(),
                'genes' => $genes
            ];
        }

        $categories = $em->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findAll();
        foreach ($categories as $category) {
            $genes = [];
            $products = $em->getRepository('AppDatabaseMainBundle:CatProdLink')->findByCategoryid($category->getId());
            foreach ($products as $product) {
                if (isset($this->productMapping['products'][$product->getProductid()])) {
                    $genes += $this->productMapping['products'][$product->getProductid()]['genes'];
                }
            }

            $this->productMapping['categories'][$category->getId()] = [
                'name' => $category->getCategoryname(),
                'genes' => array_unique($genes)
            ];
        }

        $specialProducts = $em->getRepository('AppDatabaseMainBundle:SpecialProduct')->findAll();
        foreach ($specialProducts as $product) {
            $this->productMapping['special'][$product->getId()] = $product->getName();
        }
    }

    public function getColumnMapping() {
        return array('orderNumber', 'age', 'distributionChannelId', 'status', 'products', 'massarray');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';
        $this->columnSortMapping[] = 'p.ordernumber';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Order Number',         'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Age',                  'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Distribution Channel', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Status',               'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Ordered products',             'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Massarray',            'width' => '30%', 'sortable' => 'false'),
        );
    }

    public function buildCellProducts($row) {
        $result = [
            'products' => [],
            'categories' => [],
            'special' => []
        ];

        $em = $this->container->get('doctrine')->getEntityManager();
        $orderedProducts = $em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findByCustomerid($row->getId());
        foreach ($orderedProducts as $product) {
            if (isset($this->productMapping['products'][$product->getProductId()]) && !empty($this->productMapping['products'][$product->getProductId()]['genes'])) {
                $result['products'][] = "<div>".$this->productMapping['products'][$product->getProductId()]['name']."</div>";
            }
        }

        $orderedCategories = $em->getRepository('AppDatabaseMainBundle:OrderedCategory')->findByCustomerid($row->getId());
        foreach ($orderedCategories as $category) {
            if (isset($this->productMapping['categories'][$category->getCategoryId()]) && !empty($this->productMapping['categories'][$category->getCategoryId()]['genes'])) {
                $result['categories'][] = "<div>".$this->productMapping['categories'][$category->getCategoryId()]['name']."</div>";
            }
        }

        $orderedSpecialProducts = $em->getRepository('AppDatabaseMainBundle:OrderedSpecialProduct')->findByIdCustomer($row->getId());
        foreach ($orderedSpecialProducts as $product) {
            if (isset($this->productMapping['special'][$product->getIdSpecialProduct()])) {
                $result['special'][] = "<div>".$this->productMapping['special'][$product->getIdSpecialProduct()]."</div>";
            }
        }

        $resultHtml = "";
        if (!empty($result['products']) && empty($result['categories'])) {
            $resultHtml .= "<div style='text-decoration:underline'>PRODUCTS</div>".implode('', $result['products']);
        }

        if (!empty($result['categories'])) {
            $resultHtml .= "<div style='text-decoration:underline'>PRODUCT CATEGORIES</div>".implode('', $result['categories']);
        }

        if (!empty($result['special'])) {
            $resultHtml .= "<div style='text-decoration:underline'>SPECIAL PRODUCT</div>".implode('', $result['special']);
        }

        return $resultHtml;
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GeneManagerGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');

        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber).'%');
        }
        if (intval($this->filters->isRecall) != 0) {
            if (intval($this->filters->isRecall) == 1) {
                $queryBuilder->andWhere('p.isRecall = 1');
            }
            else {
                $queryBuilder->andWhere('(p.isRecall = 0) OR (p.isRecall IS NULL)');
            }
        }
        if (trim($this->filters->orderNumberList) != '') {
            $list = explode("\n", $this->filters->orderNumberList);
            foreach ($list as $k => $v) {
                $list[$k] = trim($v);
            }
            $queryBuilder->andWhere('p.ordernumber in (:orderNumberList)')
                ->setParameter('orderNumberList', $list);
        }

        Utils::applyGeneManagerFilter($this->container, $queryBuilder);
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());

        return $status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellAge($row) {
        $age = $this->getAge($row->getDateOrdered());
        if ($age !== '') {
            return $age.'d';
        }
        return '';
    }


    public function buildCellOrderNumber($row) {
        $cellValue = $row->getOrderNumber();
        $infoText = $row->getOrderInfoText();

        if (!empty($infoText)) {
            $cellValue .=  '&nbsp;&nbsp;<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.$infoText.'</div></a>';
        }
        $laboratoryDetails = $row->getLaboratoryDetails();
        if (!empty($laboratoryDetails)) {
            $cellValue .='<a class="simpletip"><div class="button-activity"></div><div class="hide-content">'.nl2br($laboratoryDetails).'</div></a>';
        }

        if ($row->getDnaSampleOrderNumberVario() != '') {
            $cellValue .= '<br/>(DNA sample ordernumber: '.$row->getDnaSampleOrderNumberVario().')';
        }

        $cellValue .= '<br/>';
        if ($row->getIsRecall()) {
            $cellValue .= '<b style="color: red">Recall!</b>&nbsp;&nbsp;';
        }
        $cellValue .= '<span id="toggleRecallFlag_'.$row->getId().'"><a href="javascript:toggleRecallFlag(\''.$row->getId().'\')"><div class="button-red-flag"></div></a></span>';

        return $cellValue;
    }

    public function buildCellMassarray($row) {
        $mgr = $this->getModuleManager();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $massarray = Utils::checkMassarray($this->container, $row->getId(), $row->getIdCustomerInfo());

        // Build view
        $strMassarrayInfo = "";
        $strFmtDetailInfo = "
            <div>
                <div style='float:left; width: 150px'>%s</div>
                <div style='float:left;'>: %s</div>
                <div style='clear:both'></div>
            </div>";
        $strFmtEmptyGeneDetailInfo = "
            <div>
                <div style='float:left; width: 150px'>%s</div>
                <div style='clear:both'></div>
            </div>";

        foreach ($massarray as $mId => $m) {
            if (empty($m['genes'])) {
                $massarrayRow = '-';
            } else {
                $massarrayRow = "<a href='" . $mgr->getUrl('leep_admin', 'gene_manager', 'edit_result', 'edit', array('id' => $row->getId() . '_' . $mId)) . "' class='simpletip dataTablePopupButton'>%s<div class='hide-content'><div style='float:left'>%s</div><div style='float:left'>%s</div></div></a>";

                $title = 'COMPLETE';
                if ($m['numMissing'] != 0) {
                    $title = $m['numMissing'].' Gene(s) Missing';
                }

                $geneRows = "
                    <div>
                        <div style='text-decoration:underline; float:left; width: 150px'>ALL GENE RESULTS</div>
                        <div style='float:left;'></div>
                        <div style='clear:both'></div>
                    </div>";
                $emptyGeneRows = "
                    <div>
                        <div style='text-decoration:underline; float:left; width: 150px'>EMPTY GENE LIST</div>
                        <div style='clear:both'></div>
                    </div>";
                foreach ($m['genes'] as $gene) {
                    $result = $gene['result'];
                    if (empty($result)) {
                        $result = '-';
                        $emptyGeneRows .= sprintf($strFmtEmptyGeneDetailInfo, $gene['rsnumber'].' ('.$gene['name'].')');
                    }

                    $geneRows .= sprintf($strFmtDetailInfo, $gene['rsnumber'].' ('.$gene['name'].')', $result);
                }

                $massarrayRow = sprintf($massarrayRow, $title, $emptyGeneRows, $geneRows);
            }

            $strMassarrayInfo .= sprintf($strFmtDetailInfo, $m['name'], $massarrayRow);
        }
        return $strMassarrayInfo;
    }
}