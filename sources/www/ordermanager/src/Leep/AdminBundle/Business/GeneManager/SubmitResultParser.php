<?php
namespace Leep\AdminBundle\Business\GeneManager;

class SubmitResultParser {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $resultGroup = [];
    public $genes = [];
    public $orders = [];
    public $customers = [];
    public $rsNumberExtended = [];

    public function init() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Result groups
        $result = $em->getRepository('AppDatabaseMainBundle:Results')->findAll();
        foreach ($result as $r) {
            $idGroup = intval($r->getGroupId());
            $callResult = trim($r->getCallResult());
            $result = trim($r->getResult());

            if (!isset($this->resultGroup[$idGroup])) {
                $this->resultGroup[$idGroup] = [];
            }
            $this->resultGroup[$idGroup][$callResult] = $result;
        }

        // Gene mapping
        $result = $em->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        foreach ($result as $r) {
            if ($r->getIsDeleted() == 1) {
                continue;
            }

            $rsNumber = $r->getRsNumber();
            $idGroup = intval($r->getGroupId());

            $this->genes[$rsNumber] = array(
                'id'        => $r->getId(),
                'name'      => $r->getGeneName(),
                'resultMap' => isset($this->resultGroup[$idGroup]) ? $this->resultGroup[$idGroup] : []
            );

            if (!isset($this->rsNumberExtended[$rsNumber])) {
                $this->rsNumberExtended[$rsNumber] = [];
            }

            $this->rsNumberExtended[$rsNumber][$r->getId()] = array(
                'name'        => $r->getGeneName(),
                'resultMap'   => isset($this->resultGroup[$idGroup]) ? $this->resultGroup[$idGroup] : []
            );
        }

        // Unset multiple set
        foreach ($this->rsNumberExtended as $rsNumber => $arr) {
            if (count($arr) == 1) {
                unset($this->rsNumberExtended[$rsNumber]);
            }
        }
    }

    public function parseResult($result) {
        $rowResult = [];
        $parsedCache = [];

        $rows = explode("\n", $result);
        foreach ($rows as $row) {
            if (trim($row) != '') {
                $rowContent = $this->parseRow($row, $parsedCache);
                if (!empty($rowContent)) {
                    $rowResult[] = $rowContent;
                }
            }
        }

        return $rowResult;
    }

    public function geneCompare($a, $b) {
        if (strpos($a, '/') === FALSE) {
            return strcmp($a, $b);
        }

        $arr = explode('/', $a);
        $a1 = $arr[1].'/'.$arr[0];
        if (strcmp($a, $b) === 0 ||
            strcmp($a1, $b) === 0) {
            return 0;
        }

        return 1;
    }

    public function parseRow($row, &$parsedCache) {
        $tokens = explode(';', $row);

        $rowResult = array(
            'raw'         => $row,
            'tokens'      => $tokens,
            'error'       => '',
            'error_type'  => '',
            'warning'     => ''
        );

        /////////////////////////////////
        // x. Validate row formats
        if (count($tokens) != 4) {
            $rowResult['error'] = 'Invalid format, expecting 4 tokens, '.count($tokens).' found';
            $rowResult['error_type'] = Constant::ERROR_TYPE_FORMAT_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Validate tokens
        $orderNumber = trim($tokens[0]);
        $rsNumber = trim($tokens[1]);
        $result = trim($tokens[3]);

        /////////////////////////////////
        // x. Ignore 'NA'
        if ($result == 'NA') {
            $rowResult['warning'] = "Result is not available, ignore this row";
            return $rowResult;
        }

        /////////////////////////////////
        // x. Check order numbers
        $this->loadOrderNumber($orderNumber);

        $idOrder = isset($this->orders[$orderNumber]['id']) ? $this->orders[$orderNumber]['id'] : -1;
        $idCustomer = isset($this->orders[$orderNumber]['idCustomer']) ? $this->orders[$orderNumber]['idCustomer'] : -1;
        if ($idOrder == -1) {
            $rowResult['error'] = "Can't find order number '".$orderNumber."'";
            $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }

        $rowResult['idOrder'] = $idOrder;
        $rowResult['idCustomer'] = $idCustomer;

        /////////////////////////////////
        // x. Check rsNumber
        if (!isset($this->genes[$rsNumber])) {
            $rowResult['error'] = "Can't find rsnumber '".$rsNumber."'";
            $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Check result not empty
        if ($result == '') {
            $rowResult['error'] = "Result can't be empty";
            $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Handle multiple gene rsnumber
        if (isset($this->rsNumberExtended[$rsNumber])) {
            // Set GeneId, GeneName
            $genesName = [];
            $genesIdArr = [];
            foreach ($this->rsNumberExtended[$rsNumber] as $idGene => $gene) {
                $genesName[] = $gene['name'];
                $genesIdArr[] = $idGene;
            }

            $rowResult['idGene'] = $genesIdArr;
            $rowResult['geneName'] = implode(', ', $genesName);

            // Check translation to gene result
            $geneResults = [];
            $geneResultInterprets = [];
            foreach ($this->rsNumberExtended[$rsNumber] as $idGene => $gene) {
                $resultMap = $gene['resultMap'];
                if (!isset($resultMap[$result])) {
                    $rowResult['error'] = "Can't translate call result '".$result."' to gene result (gene ".$gene['name'].')';
                    $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
                    return $rowResult;
                }

                $geneResult = $resultMap[$result];
                $geneResults[$idGene] = $geneResult;
                $geneResultInterprets[] = $gene['name'].'='.$geneResult;
            }

            foreach ($geneResults as $r) {
                if ($this->geneCompare($r, $geneResult) != 0) {
                    $rowResult['error'] = "Each genes in ".implode(', ', $genesName)." interpret '".$result."' in different gene result (".implode(', ', $geneResultInterprets).')';
                    $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
                    return $rowResult;
                }
            }

            $rowResult['geneResult'] = $geneResult;
            $rowResult['geneResults'] = $geneResults;

            // Check if already exist
            $isZeroExisted = false;
            foreach ($this->rsNumberExtended[$rsNumber] as $idGene => $gene) {
                $resultMap = $gene['resultMap'];
                $geneResult = $resultMap[$result];

                if (isset($this->customers[$idCustomer][$idGene])) {
                    $currentGeneResult = $this->customers[$idCustomer][$idGene];
                    if ($currentGeneResult != '' && $currentGeneResult != '0') {
                        if ($this->geneCompare($currentGeneResult, $geneResult)) {
                            $rowResult['error'] = "The result of gene ".$gene['name']." is already existed (".$currentGeneResult."), can't be overrided to (".$geneResult.")";
                            $rowResult['error_type'] = Constant::ERROR_TYPE_CONFLICT_ERROR;
                            $rowResult['warning'] = '';
                            return $rowResult;
                        } else {
                            $rowResult['warning'] = "The result of gene ".$rowResult['geneName']." is already submitted, won't be updated further (".$geneResult.")";
                        }
                    } else {
                        $isZeroExisted = true;
                    }
                } else if (isset($parsedCache["$orderNumber::$rsNumber::$idGene"])) {
                    $cachedResult = $parsedCache["$orderNumber::$rsNumber::$idGene"];
                    if ($this->geneCompare($cachedResult, $geneResult)) {
                        $rowResult['error'] = "Duplicate submitted result! The result of rsnumber $rsNumber is already submitted ($cachedResult), can't be overrided to ($geneResult)";
                        $rowResult['error_type'] = Constant::ERROR_TYPE_CONFLICT_ERROR;
                        $rowResult['warning'] = '';
                        return $rowResult;
                    } else {
                        $rowResult['warning'] = "Duplicate submitted result for rsnumber $rsNumber, won't be updated further (".$geneResult.")";
                    }
                }
            }

            if ($isZeroExisted) {
                $rowResult['warning'] = '';
            }

            if ($rowResult['warning'] == '') {
                foreach ($this->rsNumberExtended[$rsNumber] as $idGene => $gene) {
                    $resultMap = $gene['resultMap'];
                    $geneResult = $resultMap[$result];
                    $this->customers[$idCustomer][$idGene] = $geneResult;
                    $parsedCache["$orderNumber::$rsNumber::$idGene"] = $geneResult;
                }
            }
        } else {
            // Set GeneId, GeneName
            $idGene = $this->genes[$rsNumber]['id'];
            $geneName = $this->genes[$rsNumber]['name'];
            $rowResult['idGene'] = $idGene;
            $rowResult['geneName'] = $geneName;

            // Check translation to gene result
            $resultMap = $this->genes[$rsNumber]['resultMap'];
            if (!isset($resultMap[$result])) {
                $rowResult['error'] = "Can't translate call result '".$result."' to gene result";
                $rowResult['error_type'] = Constant::ERROR_TYPE_DATA_ERROR;
                return $rowResult;
            }

            $geneResult = $resultMap[$result];
            $rowResult['geneResult'] = $geneResult;

            // Check if already exist
            if (isset($this->customers[$idCustomer][$idGene])) {
                $currentGeneResult = $this->customers[$idCustomer][$idGene];
                if ($currentGeneResult != '' && $currentGeneResult != '0') {
                    if ($this->geneCompare($currentGeneResult, $geneResult)) {
                        $rowResult['error'] = "The result of gene ".$geneName." is already existed (".$currentGeneResult."), can't be overrided to (".$geneResult.")";
                        $rowResult['error_type'] = Constant::ERROR_TYPE_CONFLICT_ERROR;
                        $rowResult['warning'] = '';
                        return $rowResult;
                    } else if (isset($parsedCache["$orderNumber::$rsNumber::$idGene"])) {
                        $cachedResult = $parsedCache["$orderNumber::$rsNumber::$idGene"];
                        if ($this->geneCompare($cachedResult, $geneResult)) {
                            $rowResult['error'] = "Duplicate submitted result! The result of rsnumber $rsNumber is already submitted ($cachedResult), can't be overrided to ($geneResult)";
                            $rowResult['error_type'] = Constant::ERROR_TYPE_CONFLICT_ERROR;
                            $rowResult['warning'] = '';
                            return $rowResult;
                        } else {
                            $rowResult['warning'] = "Duplicate submitted result for rsnumber $rsNumber, won't be updated further (".$geneResult.")";
                        }
                    } else {
                        $rowResult['warning'] = "The result of gene ".$geneName." is already submitted, won't be updated further (".$geneResult.")";
                    }
                }
            }

            if ($rowResult['warning'] == '') {
                $this->customers[$idCustomer][$idGene] = $geneResult;
                $parsedCache["$orderNumber::$rsNumber::$idGene"] = $geneResult;
            }
        }

        return $rowResult;
    }

    protected function loadOrderNumber($orderNumber) {
        if (!isset($this->orders[$orderNumber])) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($orderNumber);
            if (!empty($order)) {
                $this->orders[$orderNumber] = array(
                    'id'                => $order->getId(),
                    'idCustomer'        => $order->getIdCustomerInfo()
                );

                if(!isset($this->customers[$order->getIdCustomerInfo()])) {
                    $geneResult = [];
                    $result = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findByIdCustomer($order->getIdCustomerInfo());
                    foreach ($result as $r) {
                        $geneResult[$r->getIdGene()] = $r->getResult();
                    }

                    $this->customers[$order->getIdCustomerInfo()] = $geneResult;
                }
            }
            else {
                $this->orders[$orderNumber] = array(
                    'id'                => -1,
                    'idCustomer'        => -1
                );
            }
        }
    }
}
