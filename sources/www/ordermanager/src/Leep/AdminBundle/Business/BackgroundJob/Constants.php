<?php 
namespace Leep\AdminBundle\Business\BackgroundJob;

class Constants {
    const BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ALL         = 10;
    const BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ON_BLANK    = 20;
}