<?php
namespace Leep\AdminBundle\Business\DistributionChannelType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'flagIcon', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',     'width' => '50%', 'sortable' => 'true'),
            array('title' => 'Icon',     'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannelType', 'p');
    }


    public function buildCellFlagIcon($row) {
        if (trim($row->getFlagIcon() == '')) {
            return '';
        }
        $imageUrl = $this->container->getParameter('attachment_url').'/../upload/distribution_channel_type_flag/'.$row->getFlagIcon();
        $today = new \DateTime();
        return '<img src="'.$imageUrl.'?t='.$today->format("YmdHis").'" />';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'distribution_channel_type', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
