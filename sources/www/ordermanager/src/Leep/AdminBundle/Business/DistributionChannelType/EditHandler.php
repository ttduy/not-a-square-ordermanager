<?php
namespace Leep\AdminBundle\Business\DistributionChannelType;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    public $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannelType', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->flagIcon = array('logo' => $entity->getFlagIcon());

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('flagIcon',          'app_logo', array(
            'label'    => 'Google Map Flag Icon',
            'required' => false,
            'path'     => '../upload/distribution_channel_type_flag'
        ));
        
        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));

        return $builder;
    }

    public function onPreBind(DataEvent $event) {
        $model = $event->getData();

        $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/upload/distribution_channel_type_flag';
        if (isset($model['flagIcon']['attachment']) && !empty($model['flagIcon']['attachment'])) {
            $name = $this->id.'.'.pathinfo($model['flagIcon']['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model['flagIcon']['attachment']->move($attachmentDir, $name);
            $model['flagIcon']['logo'] = $name;
        }
        else {
            $model['flagIcon']['logo'] = $this->entity->getFlagIcon();
        }

        $event->setData($model);
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        if (!empty($model->flagIcon['logo'])) {
            $this->entity->setFlagIcon($model->flagIcon['logo']);
        }
        
        parent::onSuccess();
    }
}
