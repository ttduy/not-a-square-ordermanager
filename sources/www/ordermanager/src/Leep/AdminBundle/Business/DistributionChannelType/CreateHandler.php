<?php
namespace Leep\AdminBundle\Business\DistributionChannelType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('flagIcon',          'app_logo', array(
            'label'    => 'Google Map Flag Icon',
            'required' => false,
            'path'     => '../upload/distribution_channel_type_flag'
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $type = new Entity\DistributionChannelType();
        $type->setName($model->name);

        // Save flag
        if (!empty($model->flagIcon['attachment'])) {
            $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/upload/distribution_channel_type_flag';
            $name = $type->getId().'.'.pathinfo($model->flagIcon['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model->flagIcon['attachment']->move($attachmentDir, $name);
            $type->setFlagIcon($name);
        }

        $em->persist($type);
        $em->flush();

        parent::onSuccess();
    }
}
