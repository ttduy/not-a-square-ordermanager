<?php
namespace Leep\AdminBundle\Business\OrderBackup;

class Constant {
    const ORDER_BACKUP_STATUS_OPEN                  = 10;
    const ORDER_BACKUP_STATUS_PROCESSING            = 20;
    const ORDER_BACKUP_STATUS_UPLOADING             = 30;
    const ORDER_BACKUP_STATUS_READY                 = 40;
    const ORDER_BACKUP_STATUS_VERIFIED              = 50;
    const ORDER_BACKUP_STATUS_DONE                  = 60;
    const ORDER_BACKUP_STATUS_ERROR                 = 998;
    const ORDER_BACKUP_STATUS_EMPTY                 = 999;

    public static function getStatusList() {
        return array(
            self::ORDER_BACKUP_STATUS_OPEN              => 'Open',
            self::ORDER_BACKUP_STATUS_PROCESSING        => 'Processing',
            self::ORDER_BACKUP_STATUS_UPLOADING         => 'Uploading',
            self::ORDER_BACKUP_STATUS_READY             => 'Ready',
            self::ORDER_BACKUP_STATUS_VERIFIED          => 'Verified',
            self::ORDER_BACKUP_STATUS_DONE              => 'Done',
            self::ORDER_BACKUP_STATUS_EMPTY             => 'Empty Backup',
            self::ORDER_BACKUP_STATUS_ERROR             => 'Error'
        );
    }
}
