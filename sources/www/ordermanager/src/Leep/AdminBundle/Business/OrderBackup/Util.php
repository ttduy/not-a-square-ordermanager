<?php
namespace Leep\AdminBundle\Business\OrderBackup;

use Leep\AdminBundle\Business\Customer\Util as CustomerUtil;

class BackupEntity {

    private $properties;
    private $backupFolder;

    public function __construct($backupFolder) {
        $this->properties = [];
        $this->backupFolder = $backupFolder;
    }

    public function getBackupFolder() {
        return $this->backupFolder;
    }

    public function __get($name) {
        if (array_key_exists($name, $this->properties))
            return $this->properties[$name];

        return null;
    }

    public function __set($name, $value) {
        $this->properties[$name] = $value;
        return $this;
    }

    public function __isset($name) {
        return isset($this->properties[$name]);
    }

    public function properties() {
        return $this->properties;
    }

    public function getEntityById($controller, $repository, $id) {
        return $controller->get('doctrine')
            ->getRepository("AppDatabaseMainBundle:$repository")
            ->findOneById($id);
    }

    public function getEntitiesBy($controller, $repository, $field, $value) {
        $em = $controller->get('doctrine')->getEntityManager();
        return $em->getRepository("AppDatabaseMainBundle:$repository")
                ->createQueryBuilder('p')
                ->where("p.$field = :value")
                ->setParameter('value', $value)
                ->getQuery()
                ->iterate();
    }
}


//----------------------------------------------------------------------------------------------------------------------
// DISTRIBUTION CHANNEL
class DistributionChannel extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        if (!$entity) {
            $this->name = "Unknown";
            $this->domain = "Unknown";
            $this->type = "Unknown";
            $this->statusEmail = "Unknown";
            $this->reportEmail = "Unknown";
            $this->invoiceEmail = "Unknown";
        } else {
            $this->name = $entity->getDistributionchannel();
            $this->domain = $entity->getDomain();

            $type = $entity->getTypeid();
            $type = $this->getEntityById($controller, "DistributionChannelType", $type);
            if ($type) {
                $this->type = $type->getName();
            } else {
                $this->type = null;
            }

            $this->statusEmail = $entity->getStatusdeliveryemail();
            $this->reportEmail = $entity->getReportdeliveryemail();
            $this->invoiceEmail = $entity->getInvoicedeliveryemail();
        }

        return $this;
    }
}


//----------------------------------------------------------------------------------------------------------------------
// CUSTOMER INFO GENE
class Gene extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        $this->result = $entity->getResult();

        $gene = $this->getEntityById($controller, 'Genes', $entity->getIdGene());
        $this->geneName = $gene->getGenename();
        $this->scientificName = $gene->getScientificname();
        $this->rsnumber = $gene->getRsnumber();

        return $this;
    }
}


//----------------------------------------------------------------------------------------------------------------------
// CUSTOMER INFO
class CustomerInfo extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $em = $controller->get('doctrine')->getEntityManager();

        $this->customerNumber = $entity->getCustomerNumber();
        $this->distributionChannel = (new DistributionChannel($this->getBackupFolder()))->map($controller,
            $this->getEntityById($controller, 'DistributionChannels', $entity->getIdDistributionChannel()));

        $this->name = $entity->getFirstname().' '.$entity->getSurname();
        $this->dateOfBirth = $entity->getDateofbirth();
        $this->postCode = $entity->getPostcode();
        $this->gender = $formatter->format($entity->getGenderid(), 'mapping', 'LeepAdmin_Gender_List');
        $this->email = $entity->getEmail();

        // Save Customer Info Gene
        $customerInfoGenes = $this->getEntitiesBy($controller, 'CustomerInfoGene', 'idCustomer', $entity->getId());
        $genes = [];
        foreach ($customerInfoGenes as $gene) {
            $checkGeneExist = $this->getEntityById($controller, 'Genes', $gene[0]->getIdGene());
            if (!$checkGeneExist) {
                continue;
            }

            $genes[] = (new Gene($this->getBackupFolder()))->map($controller, $gene[0]);
            $em->detach($gene[0]);
        }

        $this->genes = $genes;

        return $this;
    }
}


//----------------------------------------------------------------------------------------------------------------------
// PRODUCT
class Product extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        if (!$entity) {
            $this->name = "Unknown";
            $this->codeName = "Unknown";
            $this->isNutriMe = "Unknown";
        } else {
            $this->name = $entity->getName();
            $this->codeName = $entity->getCodename();

            $isNutriMe = $entity->getIsNutriMe();
            if ($isNutriMe) {
                $this->isNutriMe = true;
            } else {
                $this->isNutriMe = false;
            }
        }

        return $this;
    }
}


//----------------------------------------------------------------------------------------------------------------------
// PRODUCT CATEGORY
class ProductCategory extends BackupEntity {

    private $productIdList;

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
        $this->productIdList = [];
    }

    public function getProductIdList() {
        return $this->productIdList;
    }

    public function map($controller, $entity) {
        if (!$entity) {
            $this->name = "Unknown";
            $this->codeName = "Unknown";
            $this->materialCosts = "Unknown";
            $this->productionCosts = "Unknown";
            $this->productIdList = "Unknown";
            $this->products = "Unknown";
        } else {
            $this->name = $entity->getCategoryname();
            $this->codeName = $entity->getCodename();
            $this->materialCosts = $entity->getMaterialcostsexclreports();
            $this->productionCosts = $entity->getProductioncosts();

            // Map Product Id into Product entity
            $this->productIdList = CustomerUtil::getCategoryProductLink($controller)[$entity->getId()];
            $products = [];
            foreach ($this->productIdList as $id) {
                $productEntity = $this->getEntityById($controller, 'ProductsDnaPlus', $id);
                if ($productEntity) {
                    $products[] = (new Product($this->getBackupFolder()))->map($controller, $productEntity);
                }
            }

            $this->products = $products;
        }

        return $this;
    }
}


//----------------------------------------------------------------------------------------------------------------------
// CUSTOMER REPORT
class Report extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        if (!$entity) {
            $this->name = "Unknown";
            $this->language = "Unknown";
            $this->fileName = "Unknown";
            $this->lowResolutionFileName = "Unknown";
            $this->webCoverFileName = "Unknown";
            $this->webCoverLowResolutionFileName = "Unknown";
            $this->bodCoverFileName = "Unknown";
            $this->bodCoverLowResolutionFileName = "Unknown";
            $this->xmlFileName = "Unknown";
            $this->foodTableExcelFileName = "Unknown";
            $this->customerData = "Unknown";
            $this->reportFinalData = "Unknown";
        } else {
            $formatter = $controller->get('leep_admin.helper.formatter');

            $this->name = $entity->getName();
            $this->language = $formatter->format($entity->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List');
            $this->fileName = $entity->getFilename();
            $this->lowResolutionFileName = $entity->getLowResolutionFilename();
            $this->webCoverFileName = $entity->getWebCoverFilename();
            $this->webCoverLowResolutionFileName = $entity->getWebCoverLowResolutionFilename();
            $this->bodCoverFileName = $entity->getBodCoverFilename();
            $this->bodCoverLowResolutionFileName = $entity->getBodCoverLowResolutionFilename();
            $this->foodTableExcelFileName = $entity->getFoodTableExcelFilename();
            $this->xmlFileName = $entity->getXmlFilename();
            $this->customerData = $entity->getCustomerData();
            $this->reportFinalData = $entity->getReportFinalData();

            $this->backupReportFiles($controller, $entity);
        }

        return $this;
    }

    private function backupReportFiles($controller, $customerReport) {
        $folder = $this->getBackupFolder()."/Reports";
        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report';

        // Backup file list:
        $files = [
            // low resolution files
            $customerReport->getWebCoverLowResolutionFilename(),
            $customerReport->getLowResolutionFilename(),
            $customerReport->getBodCoverLowResolutionFilename(),

            // Excel
            $customerReport->getFoodTableExcelFilename(),

            // XML
            $customerReport->getXmlFilename(),

            // Other
            $customerReport->getWebCoverFilename(),
            $customerReport->getFilename(),
            $customerReport->getBodCoverFilename()
        ];

        foreach ($files as $file) {
            if (empty($file)) {
                continue;
            }

            $fullPath = $fileDir.'/'.$file;
            $destination = $folder.'/'.$file;
            if (is_file($fullPath)) {
                copy($fullPath, $destination);
                Util::$fileDirList[] = $fullPath;
            }
        }
    }
}


//----------------------------------------------------------------------------------------------------------------------
// BOD SHIPMENT
class Shipment extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        if (!$entity) {
            $this->createdDate = "Unknown";
            $this->reportName = "Unknown";
            $this->bodCustomerOrderNumber = "Unknown";
            $this->bodFromCompany = "Unknown";
            $this->bodFromPerson = "Unknown";
            $this->bodItemTitle = "Unknown";
            $this->bodContributorRole = "Unknown";
            $this->bodContributorName = "Unknown";
            $this->bodItemPaper = "Unknown";
            $this->bodItemBinding = "Unknown";
            $this->bodOrderNumber = "Unknown";
            $this->bodOrderAddressLine1 = "Unknown";
            $this->bodOrderStreet = "Unknown";
            $this->bodOrderZip = "Unknown";
            $this->bodOrderCity = "Unknown";
            $this->bodOrderCountry = "Unknown";
            $this->packageFile = "Unknown";
            $this->highResolutionFile = "Unknown";
            $this->lowResolutionFile = "Unknown";
        } else {
            $this->createdDate = $entity->getCreationDatetime();
            $this->reportName = $entity->getCustomerReportName();
            $this->bodCustomerOrderNumber = $entity->getBodCustomerOrderNumber();
            $this->bodFromCompany = $entity->getBodFromCompany();
            $this->bodFromPerson = $entity->getBodFromPerson();
            $this->bodFromEmail = $entity->getBodFromEmail();
            $this->bodItemTitle = $entity->getBodItemTitle();
            $this->bodContributorRole = $entity->getBodContributorRole();
            $this->bodContributorName = $entity->getBodContributorName();
            $this->bodItemPaper = $entity->getBodItemPaper();
            $this->bodItemBinding = $entity->getBodItemBinding();
            $this->bodOrderNumber = $entity->getBodOrderNumber();
            $this->bodOrderAddressLine1 = $entity->getBodOrderAddressLine1();
            $this->bodOrderStreet = $entity->getBodOrderStreet();
            $this->bodOrderZip = $entity->getBodOrderZip();
            $this->bodOrderCity = $entity->getBodOrderCity();
            $this->bodOrderCountry = $entity->getBodOrderCountry();
            $this->packageFile = $entity->getPackageFile();
            $this->highResolutionFile = $entity->getHighResolutionFile();
            $this->lowResolutionFile = $entity->getLowResolutionFile();

            $this->backupShipmentFiles($controller, $entity);
        }

        return $this;
    }

    private function backupShipmentFiles($controller, $bodShipment) {
        $folder = $this->getBackupFolder()."/Shipments";
        $fileDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../web/report';

        $packageFile = $bodShipment->getPackageFile();
        $files = [];

        $backupDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../scripts/pdfshipment/files/output';
        if (!empty($packageFile)) {
            $file[] = $packageFile;
        } else {
            $backupDir = $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/shipment';

            $lowResolutionFile = $bodShipment->getLowResolutionFile();
            if (!empty($lowResolutionFile)) {
                $files[] = $lowResolutionFile;
            }

            $highResolutionFile = $bodShipment->getHighResolutionFile();
            if (!empty($highResolutionFile)) {
                $files[] = $highResolutionFile;
            }
        }

        foreach ($files as $file) {
            $fullPath = $backupDir.'/'.$file;
            $destination = $folder.'/'.$file;
            if (is_file($fullPath)) {
                copy($fullPath, $destination);
                Util::$fileDirList[] = $fullPath;
            }
        }
    }
}


//----------------------------------------------------------------------------------------------------------------------
// CUSTOMER
class Customer extends BackupEntity {

    public function __construct($backupFolder) {
        parent::__construct($backupFolder);
    }

    public function map($controller, $entity) {
        $formatter = $controller->get('leep_admin.helper.formatter');
        $em = $controller->get('doctrine')->getEntityManager();

        $this->customerInfo = (new CustomerInfo($this->getBackupFolder()))->map($controller,
            $this->getEntityById($controller, 'CustomerInfo', $entity->getIdCustomerInfo()));

        $this->orderNumber = $entity->getOrdernumber();
        $this->dateOrdered = $entity->getDateordered();

        $this->name = $entity->getFirstname().' '.$entity->getSurname();
        $this->address = sprintf("%s, %s %s, %s",
                $entity->getStreet(),
                $entity->getCity(),
                $entity->getPostcode(),
                $formatter->format($entity->getCountryid(), 'mapping', 'LeepAdmin_Country_List'));

        $this->email = $entity->getEmail();
        $this->telephone = $entity->getTelephone();

        // Map Product Category List
        $productCategoryIdList = array_map('trim', explode(',', $entity->getCategoryList()));
        $productCategories = [];
        $productList = [];
        foreach ($productCategoryIdList as $id) {
            $categoryEntity = $this->getEntityById($controller, 'ProductCategoriesDnaPlus', intval($id));
            if (!$categoryEntity)
                continue;

            $productCategory = (new ProductCategory($this->getBackupFolder()))->map($controller, $categoryEntity);

            // List all product in product category (To filter product list later)
            $productList = array_merge($productList, $productCategory->getProductIdList());
            $productCategories[] = $productCategory;
        }

        $this->productCategories = $productCategories;

        // Map Product List (Filter all product that has existed in Product Category List)
        $productIdList = array_map('trim', explode(',', $entity->getProductList()));
        $products = [];
        foreach ($productIdList as $id) {
            $id = intval($id);
            if ($id == 0) {
                continue;
            }

            // Skip products that already exist
            if (in_array($id, $productList)) {
                continue;
            }

            $productEntity = $this->getEntityById($controller, 'ProductsDnaPlus', $id);
            if ($productEntity) {
                $products[] = (new Product($this->getBackupFolder()))->map($controller, $productEntity);
            }

            $productList[] = $id;
        }

        $this->products = $products;

        // Save Customer Report
        $customerReports = $this->getEntitiesBy($controller, 'CustomerReport', 'idCustomer', $entity->getId());
        $reports = [];
        foreach ($customerReports as $report) {
            $reports[] = (new Report($this->getBackupFolder()))->map($controller, $report[0]);
            $em->detach($report[0]);
        }

        $this->reports = $reports;

        // Save Customer Shipment (BodShipment)
        $bodShipments = $this->getEntitiesBy($controller, 'BodShipment', 'idCustomer', $entity->getId());
        $shipments = [];
        foreach ($bodShipments as $shipment) {
            $shipments[] = (new Shipment($this->getBackupFolder()))->map($controller, $shipment[0]);
            $em->detach($shipment[0]);
        }

        $this->shipments = $shipments;
        return $this;
    }
}

class Util {
    static public $fileDirList = [];

    static function getBackupDir($controller) {
        return $controller->get('service_container')->getParameter('kernel.root_dir').'/../files/orderBackup/';
    }

    static private function extractToXml($name, $value, $domTree) {
        if ($value instanceof BackupEntity) {
            $entity = $value;

            // Replace name with entity class name
            $name = explode('\\', get_class($entity));
            $name = end($name);

            $properties = $entity->properties();
            $xmlEntity = $domTree->createElement($name);

            foreach ($properties as $name => $value) {
                $xmlEntity->appendChild(self::extractToXml(ucfirst($name), $value, $domTree));
            }

            return $xmlEntity;
        }

        if (gettype($value) == 'array') {
            $xmlEntity = $domTree->createElement($name);

            foreach ($value as $key => $item) {
                $xmlEntity->appendChild(self::extractToXml($key, $item, $domTree));
            }

            return $xmlEntity;
        }

        if ($value instanceof \DateTime) {
            $value = $value->format('Y-m-d H:i:s');
        } else if (gettype($value) == 'boolean') {
            $value = $value ? 'true' : 'false';
        }

        return $domTree->createElement($name, htmlspecialchars($value));
    }

    static function createBackupFile($controller, $id) {
        self::$fileDirList = [];
        $orderBackup = $controller->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:OrderBackup')
            ->findOneById($id);

        $startDateStr = $orderBackup->getStartDate()->format('d-m-Y');
        $endDateStr = $orderBackup->getEndDate()->format('d-m-Y');
        $folderName = 'backup_'.$orderBackup->getName()."_".$startDateStr."_".$endDateStr;
        $folderDir = self::getBackupDir($controller).$folderName;
        echo $folderDir;
        mkdir($folderDir);

        // Create zip file
        $zipname = "$folderName.zip";
        $zip = new \ZipArchive();
        $zip->open(self::getBackupDir($controller).$zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $em = $controller->get('doctrine')->getEntityManager();

        // Backup Order (Customer) List
        $startDate = $orderBackup->getStartDate()->format('Y-m-d');
        $endDate = $orderBackup->getEndDate()->format('Y-m-d');

        $customerList =
            $em->getRepository("AppDatabaseMainBundle:Customer")
                ->createQueryBuilder('p')
                ->where('p.dateordered >= :startDate AND p.dateordered <= :endDate')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate)
                ->getQuery()
                ->iterate();

        $numOrder = 0;
        $folderList = [];
        foreach ($customerList as $customer) {
            $numOrder++;
            $customer = $customer[0];
            $orderNumber = $customer->getOrderNumber();
            $folder = $folderDir.'/'.$orderNumber;
            mkdir($folder);
            mkdir($folder."/Reports");
            mkdir($folder."/Shipments");
            $folderList[] = $folder;

            $customerEntity = (new Customer($folder))->map($controller, $customer);

            // create a dom document with encoding utf8
            $domTree = new \DOMDocument('1.0', 'UTF-8');

            // create the root element of the xml tree and append it to the document created
            $xmlRoot = $domTree->appendChild($domTree->createElement("xml"));
            $xmlRoot->appendChild(self::extractToXml('Customer', $customerEntity, $domTree));

            $domTree->save($folder."/".$customer->getOrderNumber().".xml");
            $em->detach($customer);

            if ($handle = opendir($folder)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php') &&
                        $entry != "Reports" && $entry != "Shipments") {
                        $zip->addFile($folder."/$entry", "$orderNumber/$entry");
                    }
                }

                closedir($handle);
            }

            if ($handle = opendir($folder."/Reports")) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php')) {
                        $zip->addFile($folder."/Reports/$entry", "$orderNumber/Reports/$entry");
                    }
                }

                closedir($handle);
            }

            if ($handle = opendir($folder."/Shipments")) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php')) {
                        $zip->addFile($folder."/Shipments/$entry", "$orderNumber/Shipments/$entry");
                    }
                }

                closedir($handle);
            }
        }

        $zip->close();

        foreach ($folderList as $folder) {
            if ($handle = opendir($folder)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php') &&
                        $entry != "Reports" && $entry != "Shipments") {
                        unlink($folder."/$entry");
                    }
                }

                closedir($handle);
            }

            if ($handle = opendir($folder."/Reports")) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php')) {
                        unlink($folder."/Reports/$entry");
                    }
                }

                closedir($handle);
            }

            if ($handle = opendir($folder."/Shipments")) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != ".." && !strstr($entry,'.php')) {
                        unlink($folder."/Shipments/$entry");
                    }
                }

                closedir($handle);
            }

            rmdir($folder."/Shipments");
            rmdir($folder."/Reports");
            rmdir($folder);
        }

        $orderBackup->setNumberOfOrders($numOrder);
        $orderBackup->setBackupDate(new \DateTime());
        $orderBackup->setBackupFiles(implode(";", self::$fileDirList));
        $em->persist($orderBackup);

        rmdir($folderDir);
        return $zipname;
    }

    static public function getOrderList($controller, $id = null, $startDate = null, $endDate = null) {
        if ($startDate) {
            list($day, $month, $year) = explode('/', $startDate);
            $startDate = sprintf('%s-%s-%s', $year, $month, $day);
        }

        if ($endDate) {
            list($day, $month, $year) = explode('/', $endDate);
            $endDate = sprintf('%s-%s-%s', $year, $month, $day);
        }

        if ($id) {
            $orderBackup = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneById($id);
            if ($orderBackup) {
                $startDate = $orderBackup->getStartDate()->format('Y-m-d');
                $endDate = $orderBackup->getEndDate()->format('Y-m-d');
            }
        }

        if ($startDate && $endDate) {
            $orderQuery = $controller->get('doctrine')->getEntityManager()->createQueryBuilder();
            $orders = $orderQuery->select('
                                    p.ordernumber,
                                    p.dateordered,
                                    d.distributionchannel,
                                    ci.firstName,
                                    ci.surName
                                ')
                                ->from('AppDatabaseMainBundle:Customer', 'p')
                                ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'd', 'WITH', 'd.id = p.distributionchannelid')
                                ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'ci', 'WITH', 'ci.id = p.idCustomerInfo')
                                ->andWhere('p.dateordered >= :startDate AND p.dateordered <= :endDate')
                                ->setParameter('startDate', $startDate)
                                ->setParameter('endDate', $endDate)
                                ->getQuery()
                                ->getResult();

            return $orders;
        }

        return [];
    }
}