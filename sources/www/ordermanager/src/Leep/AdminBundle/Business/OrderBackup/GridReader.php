<?php
namespace Leep\AdminBundle\Business\OrderBackup;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

use Aws\S3\S3Client;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'period', 'numOrder', 'file', 'status', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',        'width' => '15%',   'sortable' => 'true'),
            array('title' => 'Period',      'width' => '20%',   'sortable' => 'true'),
            array('title' => 'Orders',      'width' => '7%',    'sortable' => 'true'),
            array('title' => 'File',        'width' => '30%',   'sortable' => 'false'),
            array('title' => 'Status',      'width' => '8%',    'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%',   'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_OrderBackup_GridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:OrderBackup', 'p');

        if ($this->filters->startDate) {
            $queryBuilder->andWhere('p.startDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }

        if ($this->filters->endDate) {
            $queryBuilder->andWhere('p.endDate >= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }


        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

    }

    public function buildCellPeriod($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $tpl = 'From %s to %s';
        $startDate = $formatter->format($row->getStartDate(), 'date');
        $endDate = $formatter->format($row->getEndDate(), 'date');

        return sprintf($tpl, $startDate, $endDate);
    }

    public function buildCellNumOrder($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '';

        $builder->addPopupButton('Matched Orders', $mgr->getUrl('leep_admin', 'order_backup', 'feature', 'getMatchedOrders', array('id' => $row->getId())), 'button-detail');
        $html = $builder->getHtml();

        if ($row->getNumberOfOrders()) {
            $html .= "  ".$row->getNumberOfOrders();
        }
        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'order_backup', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'order_backup', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');

            if ($row->getStatus() == Constant::ORDER_BACKUP_STATUS_READY) {
                $builder->addPopupButton('Verify', $mgr->getUrl('leep_admin', 'order_backup', 'feature', 'verifyBackup', array('id' => $row->getId())), 'button-copy');
            } else if ($row->getStatus() == Constant::ORDER_BACKUP_STATUS_ERROR) {
                $builder->addPopupButton('Verify', $mgr->getUrl('leep_admin', 'order_backup', 'feature', 'resetBackup', array('id' => $row->getId())), 'button-redo');
            }
        }

        return $builder->getHtml();
    }
}