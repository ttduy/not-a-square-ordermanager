<?php
namespace Leep\AdminBundle\Business\OrderBackup;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => true
        ));

        $builder->add('startDate', 'datepicker', array(
            'label' => 'Start Date',
            'required' => true
        ));

        $builder->add('endDate', 'datepicker', array(
            'label' => 'End Date',
            'required' => true
        ));

        $builder->add('description', 'textarea', array(
            'label' => 'Description',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 2
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $model = $this->getForm()->getData();

        $modelName = trim($model->name);
        if (empty($modelName)) {
            $this->errors[] = "Name must not be empty!";
        } else {
            $check = $em->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneByName($modelName);
            if ($check) {
                $this->errors[] = "Backup name: $modelName already exist!";
            }
        }

        if (empty($model->startDate)) {
            $this->errors[] = "Start date must not be empty!";
        }

        if (empty($model->endDate)) {
            $this->errors[] = "End date must not be empty!";
        }

        if ($model->startDate > $model->endDate) {
            $this->errors[] = "Start day must be smaller than end date!";
        }

        if (!empty($this->errors)) {
            return false;
        }

        $duration = $config->getOrderBackupDateDuration();
        $interval = date_diff($model->startDate, $model->endDate);
        $days = intval($interval->format('%R%a'));
        if ($duration && $days < $duration) {
            $this->getForm()->get('endDate')->addError(new FormError("Date duration must be equal or greater than: ".$duration));
        } else {
            $orderBackup = new Entity\OrderBackup();
            $orderBackup->setName($modelName);
            $orderBackup->setStartDate($model->startDate);
            $orderBackup->setEndDate($model->endDate);
            $orderBackup->setDescription($model->description ? $model->description : null);
            $orderBackup->setStatus(Constant::ORDER_BACKUP_STATUS_OPEN);
            $orderBackup->setFile('');
            $orderBackup->setMd5Hash('');

            $em->persist($orderBackup);
            $em->flush();

            parent::onSuccess();
        }
    }
}