<?php
namespace Leep\AdminBundle\Business\OrderBackup;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start Date',
            'required' => false
        ));

        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End Date',
            'required' => false
        ));

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        return $builder;
    }
}