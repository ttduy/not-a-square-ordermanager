<?php
namespace Leep\AdminBundle\Business\OrderBackup;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => true
        ));

        $builder->add('description', 'textarea', array(
            'label' => 'Description',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 2
            )
        ));

        $builder->add('isReOpen', 'checkbox', array(
            'label' => 'Re-Open?',
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $modelName = trim($model->name);
        if (empty($modelName)) {
            $this->errors[] = "Name must not be empty!";
        } else {
            $check = $em->getRepository('AppDatabaseMainBundle:OrderBackup')->findOneByName($modelName);
            if ($check && $check->getId() != $this->entity->getId()) {
                $this->errors[] = "Backup name: $modelName already exist!";
            }
        }

        if (!empty($this->errors)) {
            return false;
        }

        $this->entity->setName($model->name);
        $this->entity->setDescription($model->description);
        if ($model->isReOpen) {
            $this->entity->setStatus(Constant::ORDER_BACKUP_STATUS_OPEN);
        }

        parent::onSuccess();
    }
}