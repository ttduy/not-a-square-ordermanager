<?php
namespace Leep\AdminBundle\Business\Company;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');

        $imgPath = $this->container->getParameter('attachment_url').'/logo';

        $id = $this->container->get('request')->query->get('id', 0);
        $company = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($id);
        $data[] = $this->addSection('Company');
        $data[] = $this->add('Company Name', $company->getCompanyName());
        $data[] = $this->add('Street', $company->getStreet());
        $data[] = $this->add('Post Code', $company->getPostCode());
        $data[] = $this->add('City', $company->getCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $company->getCountryId()));
        $data[] = $this->add('VAT', $company->getVAT());
        $data[] = $this->add('Email', $company->getEmail());
        $data[] = $this->add('Telephone', $company->getTelephone());
        $data[] = $this->add('Website', $company->getWebsite());

        $data[] = $this->addSection('Bank Account');
        $data[] = $this->add('Bank', $company->getBank());
        $data[] = $this->add('Bank Account', $company->getBankAccount());
        $data[] = $this->add('Bank Code', $company->getBankCode());
        $data[] = $this->add('BIC', $company->getBIC());
        $data[] = $this->add('IBAN', $company->getIBAN());
        
        $data[] = $this->addSection('Others');
        $data[] = $this->add('Logo', '<img src="'.$imgPath.'/'.$company->getLogo().'"></img>&nbsp;');
        $data[] = $this->add('Trade Register', $company->getTradeRegister());
        $data[] = $this->add('Jurisdiction', $company->getJurisdiction());
        $data[] = $this->add('Manager', $company->getManager());
        $data[] = $this->add('Legal Form', $company->getLegalForm());
        $data[] = $this->add('Tax Number', $company->getTaxNumber());
        $data[] = $this->add('UID Number', $company->getUidNumber());

        return $data;
    }
}
