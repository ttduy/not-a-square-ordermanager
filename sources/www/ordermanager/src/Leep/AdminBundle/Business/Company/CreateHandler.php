<?php
namespace Leep\AdminBundle\Business\Company;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('companyName', 'text', array(
            'label'    => 'Company Name',
            'required' => true
        ));
        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',        
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('vat', 'text', array(
            'label'    => 'VAT',
            'required' => false
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('website', 'text', array(
            'label'    => 'Web Site',
            'required' => false
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank',
            'required' => false
        ));
        $builder->add('bank', 'text', array(
            'label'    => 'Bank',
            'required' => false
        ));
        $builder->add('bankAccount', 'text', array(
            'label'    => 'Bank Account',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));

        $builder->add('section3', 'section', array(
            'label'    => 'Others',
            'required' => false
        ));        
        $builder->add('logo',          'app_logo', array(
            'label'    => 'Logo',
            'required' => false
        ));
        $builder->add('tradeRegister', 'text', array(
            'label'    => 'Commercial Register Nr.',
            'required' => false
        ));
        $builder->add('jurisdiction', 'text', array(
            'label'    => 'Jurisdiction',
            'required' => false
        ));
        $builder->add('manager', 'text', array(
            'label'    => 'Manager',
            'required' => false
        ));
        $builder->add('legalForm', 'text', array(
            'label'    => 'Legal Form',
            'required' => false
        ));
        $builder->add('taxNumber', 'text', array(
            'label'    => 'Tax Number',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('billVariables', 'textarea', array(
            'label'    => 'Bill Variables',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $company = new Entity\Companies();
        $company->setCompanyName($model->companyName);
        $company->setStreet($model->street);
        $company->setPostCode($model->postCode);
        $company->setCity($model->city);
        $company->setCountryId($model->countryId);
        $company->setVat($model->vat);
        $company->setEmail($model->email);
        $company->setTelephone($model->telephone);
        $company->setWebsite($model->website);

        $company->setBank($model->bank);
        $company->setBankAccount($model->bankAccount);
        $company->setBankCode($model->bankCode);
        $company->setBIC($model->bic);
        $company->setIBAN($model->iban);
        
        $company->setTradeRegister($model->tradeRegister);
        $company->setJurisdiction($model->jurisdiction);
        $company->setManager($model->manager);
        $company->setLegalForm($model->legalForm);
        $company->setTaxNumber($model->taxNumber);
        $company->setUidNumber($model->uidNumber);

        $company->setBillVariables($model->billVariables);

        $em->persist($company);
        $em->flush();

        if (!empty($model->logo['attachment'])) {
            $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/logo';
            $name = $company->getId().'.'.pathinfo($model->logo['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model->logo['attachment']->move($attachmentDir, $name);
            $company->setLogo($name);
            $em->persist($company);
            $em->flush();
        }

        parent::onSuccess();
    }
}
