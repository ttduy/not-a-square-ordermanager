<?php
namespace Leep\AdminBundle\Business\Company\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppLogo extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'path' => 'logo'
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_logo';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $view->vars['imgPath'] = $this->container->getParameter('attachment_url').'/'.$options['path'];
        $today = new \DateTime();
        $view->vars['timestamp'] = $today->format('YmdHis');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('attachment', 'file', array(
            'label' => 'Logo',
            'required'  => false
        ));
        $builder->add('logo',       'hidden');
    }
}