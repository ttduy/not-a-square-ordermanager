<?php
namespace Leep\AdminBundle\Business\Company;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('companyName', 'countryId', 'city', 'email', 'telephone', 'website', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Company Name', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Country',      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'City',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Email',        'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Telephone',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Website',      'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',       'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CompanyGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Companies', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        if (trim($this->filters->companyName) != '') {
            $queryBuilder->andWhere('p.companyname LIKE :companyName')
                ->setParameter('companyName', '%'.trim($this->filters->companyName).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'company', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('companyModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'company', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'company', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
