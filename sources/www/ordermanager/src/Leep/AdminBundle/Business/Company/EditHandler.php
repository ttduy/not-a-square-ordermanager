<?php
namespace Leep\AdminBundle\Business\Company;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->companyName = $entity->getCompanyName();
        $model->street = $entity->getStreet();
        $model->postCode = $entity->getPostCode();
        $model->city = $entity->getCity();
        $model->countryId = $entity->getCountryId();
        $model->vat = $entity->getVat();
        $model->email = $entity->getEmail();
        $model->telephone = $entity->getTelephone();
        $model->website = $entity->getWebsite();
        
        $model->bank = $entity->getBank();
        $model->bankAccount = $entity->getBankAccount();
        $model->bankCode = $entity->getBankCode();
        $model->bic = $entity->getBIC();
        $model->iban = $entity->getIBAN();
        
        $model->logo = array('logo' => $entity->getLogo());
        $model->tradeRegister = $entity->getTradeRegister();
        $model->jurisdiction = $entity->getJurisdiction();
        $model->manager = $entity->getManager();
        $model->legalForm = $entity->getLegalForm();
        $model->taxNumber = $entity->getTaxNumber();
        $model->uidNumber = $entity->getUidNumber();

        $model->billVariables = $entity->getBillVariables();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('companyName', 'text', array(
            'label'    => 'Company Name',
            'required' => true
        ));
        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',        
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('vat', 'text', array(
            'label'    => 'VAT',
            'required' => false
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('website', 'text', array(
            'label'    => 'Web Site',
            'required' => false
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank',
            'required' => false
        ));
        $builder->add('bank', 'text', array(
            'label'    => 'Bank',
            'required' => false
        ));
        $builder->add('bankAccount', 'text', array(
            'label'    => 'Bank Account',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));

        $builder->add('section3', 'section', array(
            'label'    => 'Others',
            'required' => false
        ));        
        $builder->add('logo',          'app_logo', array(
            'label'    => 'Logo',
            'required' => false
        ));
        $builder->add('tradeRegister', 'text', array(
            'label'    => 'Commercial Register Nr.',
            'required' => false
        ));
        $builder->add('jurisdiction', 'text', array(
            'label'    => 'Jurisdiction',
            'required' => false
        ));
        $builder->add('manager', 'text', array(
            'label'    => 'Manager',
            'required' => false
        ));
        $builder->add('legalForm', 'text', array(
            'label'    => 'Legal Form',
            'required' => false
        ));
        $builder->add('taxNumber', 'text', array(
            'label'    => 'Tax Number',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('billVariables', 'textarea', array(
            'label'    => 'Bill Variables',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
            )
        ));
        
        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));

        return $builder;
    }

    public function onPreBind(DataEvent $event) {
        $model = $event->getData();

        $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/logo';
        if (isset($model['logo']['attachment']) && !empty($model['logo']['attachment'])) {
            $name = $this->id.'.'.pathinfo($model['logo']['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model['logo']['attachment']->move($attachmentDir, $name);
            $model['logo']['logo'] = $name;
        }
        else {
            $model['logo']['logo'] = $this->entity->getLogo();
        }

        $event->setData($model);
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setCompanyName($model->companyName);
        $this->entity->setStreet($model->street);
        $this->entity->setPostCode($model->postCode);
        $this->entity->setCity($model->city);
        $this->entity->setCountryId($model->countryId);
        $this->entity->setVat($model->vat);
        $this->entity->setEmail($model->email);
        $this->entity->setTelephone($model->telephone);
        $this->entity->setWebsite($model->website);

        $this->entity->setBank($model->bank);
        $this->entity->setBankAccount($model->bankAccount);
        $this->entity->setBankCode($model->bankCode);
        $this->entity->setBIC($model->bic);
        $this->entity->setIBAN($model->iban);
        
        if (!empty($model->logo['logo'])) {
            $this->entity->setLogo($model->logo['logo']);
        }
        $this->entity->setTradeRegister($model->tradeRegister);
        $this->entity->setJurisdiction($model->jurisdiction);
        $this->entity->setManager($model->manager);
        $this->entity->setLegalForm($model->legalForm);
        $this->entity->setTaxNumber($model->taxNumber);
        $this->entity->setUidNumber($model->uidNumber);

        $this->entity->setBillVariables($model->billVariables);

        parent::onSuccess();
    }
}
