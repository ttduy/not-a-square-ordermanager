<?php
namespace Leep\AdminBundle\Business\Company;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $companyName;
    public $street;
    public $postCode;
    public $city;
    public $countryId;
    public $vat;
    public $email;
    public $telephone;
    public $website;
    
    public $bank;
    public $bankAccount;
    public $bankCode;
    public $bic;
    public $iban;

    public $logo;
    public $tradeRegister;
    public $jurisdiction;
    public $manager;
    public $legalForm;
    public $taxNumber;
    public $uidNumber;
    
    public $billVariables;
}
