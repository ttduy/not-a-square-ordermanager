<?php
namespace Leep\AdminBundle\Business\Company;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('companyName', 'text', array(
            'label'       => 'Company Name',
            'required'    => false,
        ));
        
        return $builder;
    }
}