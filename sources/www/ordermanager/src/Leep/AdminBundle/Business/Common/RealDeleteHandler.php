<?php 
namespace Leep\AdminBundle\Business\Common;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class RealDeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $record = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:'.$this->repository)->findOneById($id);
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($record);
        $em->flush();
    }
}