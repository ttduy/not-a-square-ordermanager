<?php
namespace Leep\AdminBundle\Business\GenoPelletProduction;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\GenoPelletProduction\Constant;

class Utils {

    public static function clearAllStep($container, $productionId) {
        $steps = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findByProductionId($productionId);
        $em = $container->get('doctrine')->getEntityManager();
        foreach ($steps as $step) {
            $em->remove($step);
        }
        $em->flush();
    }

    public static function generateSteps($container, $content, $productionId) {
        $em = $container->get('doctrine')->getEntityManager();
        $steps = [];

        // First step is to scan user id card
        $steps[] = self::createStep($em, Constant::STEP_TYPE_SCAN_ID_CARD, Constant::STEP_STATUS_PROCESSING, [
            'id' => ''
        ], $productionId, 0);

        // For each substance, create an corresponding step
        $step = 1;
        foreach ($content['recipe'] as $substance => $amount) {
            $critical = in_array($substance, $content['critical']);

            // Scan substance lot number
            $steps[] = self::createStep($em, Constant::STEP_TYPE_SCAN_SUBSTANCE, Constant::STEP_STATUS_NEW, [
                'name' =>       $substance,
                'lots' =>       '',
            ], $productionId, $step++);

            // If critical substance then re autorize with another person
            if ($critical) {
                $steps[] = self::createStep($em, Constant::STEP_TYPE_RE_AUTHORIZE, Constant::STEP_STATUS_NEW, [
                    'id' => ''
                ], $productionId, $step++);
            }

            // Enter used amount
            $steps[] = self::createStep($em, Constant::STEP_TYPE_ENTER_AMOUNT, Constant::STEP_STATUS_NEW, [
                'substance' =>  $substance,
                'required' =>   intval($amount),
                'amounts' =>    0,
                'critical' =>   $critical
            ], $productionId, $step++);
        }

        $em->flush();
        return $steps;
    }

    public static function appendSubstanceStepAfter($container, $productionId, $afterStep, $substance, $required, $isCritical) {
        $em = $container->get('doctrine')->getEntityManager();
        $step = $afterStep + 1;

        $allSteps = $em->getRepository('AppDatabaseMainBundle:GenoPelletProductionSteps')->findByProductionId($productionId);
        foreach ($allSteps as $s) {
            if ($s->getStep() <= $afterStep) {
                continue;
            }

            if ($isCritical) {
                $s->setStep($s->getStep() + 3);
            } else {
                $s->setStep($s->getStep() + 2);
            }

            $em->persist($s);
        }

        // Scan substance lot number
        self::createStep($em, Constant::STEP_TYPE_SCAN_SUBSTANCE, Constant::STEP_STATUS_PROCESSING, [
            'name' =>       $substance,
            'lots' =>       '',
        ], $productionId, $step++);

        // If critical substance then re autorize with another person
        if ($isCritical) {
            self::createStep($em, Constant::STEP_TYPE_RE_AUTHORIZE, Constant::STEP_STATUS_NEW, [
                'id' => ''
            ], $productionId, $step++);
        }

        // Enter used amount
        self::createStep($em, Constant::STEP_TYPE_ENTER_AMOUNT, Constant::STEP_STATUS_NEW, [
            'substance' =>  $substance,
            'required' =>   $required,
            'amounts' =>    0,
            'critical' =>   $isCritical
        ], $productionId, $step++);

        $em->flush();
    }

    private static function createStep($em, $type, $status, $values, $productionId, $step) {
        $result = new Entity\GenoPelletProductionSteps();
        $result->setType($type);
        $result->setStatus($status);
        $result->setValue(json_encode($values));
        $result->setProductionId($productionId);
        $result->setStep($step);

        $em->persist($result);
    }

    public static function createStepPublic($em, $type, $status, $values, $productionId, $step)
    {
        self::createStep($em, $type, $status, $values, $productionId, $step);
    }
}