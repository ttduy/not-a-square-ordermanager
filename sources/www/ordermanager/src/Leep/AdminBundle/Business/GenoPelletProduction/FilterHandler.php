<?php
namespace Leep\AdminBundle\Business\GenoPelletProduction;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('formulaName', 'text', [
            'label' =>      'Formula\'s Name',
            'required' =>   false
        ]);

        $builder->add('startBy', 'searchable_box', array(
            'label'    => 'Started by',
            'required' => false,
            'choices'  => [0 => null] + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => [0 => 'All'] + Constant::getProductionStatus()
        ));

        return $builder;
    }
}