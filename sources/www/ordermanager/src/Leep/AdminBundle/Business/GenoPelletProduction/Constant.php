<?php
namespace Leep\AdminBundle\Business\GenoPelletProduction;

class Constant{
    const STEP_TYPE_SCAN_ID_CARD =      10;
    const STEP_TYPE_SCAN_SUBSTANCE =    20;
    const STEP_TYPE_ENTER_AMOUNT =      30;
    const STEP_TYPE_RE_AUTHORIZE =      40;

    public static function getProductionStepTypes() {
        return [
            self::STEP_TYPE_SCAN_ID_CARD =>     "Scan Id card",
            self::STEP_TYPE_SCAN_SUBSTANCE =>   "Scan substance",
            self::STEP_TYPE_ENTER_AMOUNT =>     "Enter used amount",
            self::STEP_TYPE_RE_AUTHORIZE =>     "Re-Authorize substance"
        ];
    }

    public static function getProductionStepTitle() {
        return [
            self::STEP_TYPE_SCAN_ID_CARD =>     'Please scan your ID Card!',
            self::STEP_TYPE_SCAN_SUBSTANCE =>   'Please get substance ',
            self::STEP_TYPE_ENTER_AMOUNT =>     'Enter used amount.',
            self::STEP_TYPE_RE_AUTHORIZE =>     'Critical substance! please find another to authorize this action!'
        ];
    }

    const STEP_STATUS_NEW =         10;
    const STEP_STATUS_PROCESSING =  20;
    const STEP_STATUS_SKIPED =      30;
    const STEP_STATUS_FINISHED =    40;

    public static function getProductionStepStatus() {
        return [
            self::STEP_STATUS_NEW =>            'New',
            self::STEP_STATUS_PROCESSING =>     'Processing',
            self::STEP_STATUS_SKIPED =>         'Skiped',
            self::STEP_STATUS_FINISHED =>       'Finished'
        ];
    }

    const PRODUCTION_STATUS_NEW =           10;
    const PRODUCTION_STATUS_PROCESSING =    20;
    const PRODUCTION_STATUS_FINISHED =      30;

    public static function getProductionStatus() {
        return [
            self::PRODUCTION_STATUS_NEW =>          'New',
            self::PRODUCTION_STATUS_PROCESSING =>   'Processing',
            self::PRODUCTION_STATUS_FINISHED =>     'Finished'
        ];
    }
}
