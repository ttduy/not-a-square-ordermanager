<?php
namespace Leep\AdminBundle\Business\GenoPelletProduction;

use Easy\CrudBundle\Form\BaseHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseHandler {
    public function execute() {
        $mgr = $this->container->get('easy_module.manager');
        $this->setRedirectUrl($mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'listFormula'));
    }
}
