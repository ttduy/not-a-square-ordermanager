<?php
namespace Leep\AdminBundle\Business\GenoPelletProduction;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('startDate', 'startBy', 'formulaName', 'status', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Start date',      'width' => '12%', 'sortable' => 'true'),
            array('title' => 'Start by',        'width' => '13%', 'sortable' => 'false'),
            array('title' => 'Formula\'s Name', 'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Status',          'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',          'width' => '20%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GenoPelletProductionGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoPelletProductions', 'p');

        if (trim($this->filters->formulaName) != '') {
            $queryBuilder->andWhere('p.formulaName LIKE :formulaName')
                ->setParameter('formulaName', '%'.trim($this->filters->formulaName).'%');
        }

        if (!empty($this->filters->startBy)) {
            $queryBuilder->andWhere('p.startBy = :startBy')
                ->setParameter('startBy', $this->filters->startBy);
        }

        if (!empty($this->filters->status)) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }
    }

    public function buildCellStatus($row) {
        return Constant::getProductionStatus()[$row->getStatus()];
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoPelletProductionModify')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_pellet_production', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
            if ($row->getStatus() != Constant::PRODUCTION_STATUS_FINISHED) {
                $builder->addButton('Restart', $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'restartProduction', array('id' => $row->getId())), 'button-redo');
            } else {
                $builder->addPopupButton('Log', $mgr->getUrl('leep_admin', 'geno_pellet_production', 'feature', 'viewStepStatus', array('id' => $row->getId())), 'button-detail');
            }
        }

        return $builder->getHtml();
    }
}
