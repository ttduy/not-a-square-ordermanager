<?php
namespace Leep\AdminBundle\Business\ProductGroup;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false,
        ));
        
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Customer_Status')
        ));

        return $builder;
    }
}