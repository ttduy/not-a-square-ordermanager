<?php
namespace Leep\AdminBundle\Business\ProductGroup;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductGroup', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        // set for related statuses
        $statuses = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductGroupStatus')->findByIdProductGroup($entity->getId());

        $arrStatuses = array();
        if ($statuses) {
            foreach ($statuses as $status) {
                $arrStatuses[] = $status->getStatus();
            }

            $model->statuses = $arrStatuses;
        }

        // get Products is being set with being-edited Group
        $model->products = $this->getRelatedProducts();

        return $model;
    }

    public function getRelatedProducts() {
        // get Products is being set with being-edited Group
        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findByIdProductGroup($this->id);

        $arrIdProducts = array();
        if ($products) {
            foreach ($products as $product) {
                $arrIdProducts[] = $product->getId();
            }
        }

        return $arrIdProducts;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'attr'     => array(
                'style' => 'height: 300px'
            )
        ));

        $builder->add('statuses', 'choice', array(
            'label'    => 'Statuses',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'attr'     => array(
                'style' => 'height: 300px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);

        // remove previously registered statuses
        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:ProductGroupStatus', 'p')
                ->andWhere('p.idProductGroup = :idProductGroup')
                ->setParameter('idProductGroup', $this->entity->getId())
                ->getQuery()
                ->execute();

        // register new statuses
        if ($model->statuses) {
            foreach ($model->statuses as $status) {
                $productGroupStatus = new Entity\ProductGroupStatus();
                $productGroupStatus->setIdProductGroup($this->entity->getId());
                $productGroupStatus->setStatus($status);
                $em->persist($productGroupStatus);
                $em->flush();
            }
        }

        // set being-edited Group for selected Products
        if ($model->products) {
            $queryUpdate = $em->createQueryBuilder();
            $queryUpdate->update('AppDatabaseMainBundle:ProductsDnaPlus', 'p')
                ->set('p.idProductGroup', ':idGroupProduct')
                ->setParameter('idGroupProduct', $this->entity->getId())
                ->where('p.id in (:idProducts)')
                ->setParameter('idProducts', $model->products)
                ->getQuery()
                ->execute();
        }

        $arrNoGroupIdProducts = array_diff($this->getRelatedProducts(), $model->products);
        $queryUpdate = $em->createQueryBuilder();
        $queryUpdate->update('AppDatabaseMainBundle:ProductsDnaPlus', 'p')
            ->set('p.idProductGroup', ':idGroupProduct')
            ->setParameter('idGroupProduct', null)
            ->where('p.id in (:idProducts)')
            ->setParameter('idProducts', $arrNoGroupIdProducts)
            ->getQuery()
            ->execute();

        parent::onSuccess();
    }
}
