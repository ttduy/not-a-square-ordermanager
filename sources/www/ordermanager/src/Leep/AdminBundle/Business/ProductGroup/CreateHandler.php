<?php
namespace Leep\AdminBundle\Business\ProductGroup;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'attr'     => array(
                'style' => 'height: 300px'
            )
        ));

        $builder->add('statuses', 'choice', array(
            'label'    => 'Statuses',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'attr'     => array(
                'style' => 'height: 300px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $productGroup = new Entity\ProductGroup();
        $productGroup->setName($model->name);
        $em->persist($productGroup);
        $em->flush();

        if ($model->statuses) {
            foreach ($model->statuses as $status) {
                $productGroupStatus = new Entity\ProductGroupStatus();
                $productGroupStatus->setIdProductGroup($productGroup->getId());
                $productGroupStatus->setStatus($status);
                $em->persist($productGroupStatus);
                $em->flush();
            }
        }

        // set new Group for selected Products
        if ($model->products) {
            $queryUpdate = $em->createQueryBuilder();
            $queryUpdate->update('AppDatabaseMainBundle:ProductsDnaPlus', 'p')
                ->set('p.idProductGroup', $productGroup->getId())
                ->where('p.id in (:idProducts)')
                ->setParameter('idProducts', $model->products)
                ->getQuery()
                ->execute();
        }

        parent::onSuccess();
    }
}
