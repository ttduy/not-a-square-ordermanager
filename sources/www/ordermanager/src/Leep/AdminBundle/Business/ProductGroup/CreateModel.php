<?php
namespace Leep\AdminBundle\Business\ProductGroup;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $statuses;
    public $products;
}
