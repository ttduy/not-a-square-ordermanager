<?php
namespace Leep\AdminBundle\Business\ProductGroup;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'product', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Product',      'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Status',      'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ProductGroup', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if ($this->filters->status) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:ProductGroupStatus', 'pgs', 'WITH', 'pgs.idProductGroup = p.id AND pgs.status = :status')
                ->setParameter('status', $this->filters->status);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productGroupModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'product_group', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'product_group', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellProduct($row) {
        $mapping = $this->container->get('easy_mapping');
        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findByIdProductGroup($row->getId());

        $html = '';
        if ($products) {
            foreach ($products as $p) {
                if ($html) {
                    $html .= '<br/>';
                }

                $html .= $p->getName();
            }
        }

        return $html;
    }

    public function buildCellStatus($row) {
        $mapping = $this->container->get('easy_mapping');
        $statuses = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductGroupStatus')->findByIdProductGroup($row->getId());

        $html = '';
        if ($statuses) {
            foreach ($statuses as $status) {
                if ($html) {
                    $html .= '<br/>';
                }

                $html .= $mapping->getMappingTitle('LeepAdmin_Customer_Status', $status->getStatus());
            }
        }

        return $html;
    }
}