<?php 
namespace Leep\AdminBundle\Business\ProductGroup;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $productGroup = $em->getRepository('AppDatabaseMainBundle:ProductGroup')->findOneById($id);
        
        if ($productGroup) {
            // remove related statuses
            $delQuery = $em->createQueryBuilder();
            $delQuery->delete('AppDatabaseMainBundle:ProductGroupStatus', 'p')
                    ->andWhere('p.idProductGroup = :idProductGroup')
                    ->setParameter('idProductGroup', $productGroup->getId())
                    ->getQuery()
                    ->execute();

            // delete Product Group itself
            $em->remove($productGroup);
            $em->flush();            
        }
    }
}