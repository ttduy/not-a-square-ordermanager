<?php
namespace Leep\AdminBundle\Business\CollectiveInvoiceForm;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridEditReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }

    public function getColumnMapping() {
        return array('orderNumber', 'dateOrdered', 'name', 'distributionChannelId', 'lastStatus', 'idPreferredPayment', 'reportDeliveryId', 'orderInfoText', 'total', 'priceOverride');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Order Number',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Date Ordered',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',           'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Last status',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Preferred payment',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Report delivery',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Order Info',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Total',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Price Override', 'width' => '10%', 'sortable' => 'false')
            
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CollectiveInvoiceFormGridReader');
    }

    public function buildCellTotal($row) {        
        if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
            $price = Business\Customer\Util::computePrice($this->container, $row->getId(), 'invoice_partner');
            return $price['total'];
        }
        else if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $price = Business\Customer\Util::computePrice($this->container, $row->getId(), 'invoice_dc');
            return $price['total'];
        }
        return '';
    }

    public function buildCellPriceOverride($row) {
        return '<input type="textbox" id="override_price_'.$row->getId().'" value="'.$row->getPriceOverride().'" />';
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellLastStatus($row) {
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());        
        $age = $this->getAge($row->getStatusDate());        

        return '('.$age.') '.$status;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')            
            ->andWhere('(p.collectiveinvoiceid IS NULL) or (p.collectiveinvoiceid = 0) or (p.collectiveinvoiceid = :currentId)')
            ->andWhere('(p.isinvoicecustomer IS NULL) or (p.isinvoicecustomer = 0)')
            ->setParameter('currentId', $this->container->get('request')->query->get('id', 0))
            ->andWhere('p.isdeleted = 0');

        if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
                ->andWhere('dc.partnerid = :partnerId')
                ->andWhere('p.invoicegoestoid = :idInvoiceGoesToPartner')
                ->setParameter('partnerId', $this->filters->partnerId)
                ->setParameter('idInvoiceGoesToPartner', Business\DistributionChannel\Constant::INVOICE_GOES_TO_PARTNER);
        }
        else if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
                ->andWhere('p.distributionchannelid = :distributionChannelId')
                ->andWhere('p.invoicegoestoid = :idInvoiceGoesToDc')
                ->setParameter('distributionChannelId', $this->filters->distributionChannelId)
                ->setParameter('idInvoiceGoesToDc', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL);
        }
        else {
            $queryBuilder->andWhere('p.id = -1');
        }        
        
        if ($this->filters->showId == Business\CollectiveInvoice\Constant::SHOW_REPORT_SUBMITTED_ONLY) {
            $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
            $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');            
            
            $reportSubmittedStatus = 8; // Fix me            
            $nutriMeSubmittedStatus = 55;
            $queryBuilder->andWhere('(FIND_IN_SET('.$reportSubmittedStatus.', p.statusList) > 0) OR (FIND_IN_SET('.$nutriMeSubmittedStatus.', p.statusList) > 0)');
        }

        if ($this->filters->idPreferredPayment != 0) {
            $queryBuilder->andWhere('p.idPreferredPayment = :idPreferredPayment')
                ->setParameter('idPreferredPayment', $this->filters->idPreferredPayment);
        }
    }
    
    public function buildCellOrderInfoText($row) {
        return nl2br($row->getOrderInfoText());
    }
}
