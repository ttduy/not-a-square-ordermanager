<?php
namespace Leep\AdminBundle\Business\CollectiveInvoiceForm;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class FilterEditHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterEditModel();

        $id = $this->container->get('request')->query->get('id', 0);
        $collectiveInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice', 'app_main')->findOneById($id);
        
        $model->name = $collectiveInvoice->getName();
        $model->invoiceNumber = $collectiveInvoice->getInvoiceNumber();
        $model->invoiceDate = $collectiveInvoice->getInvoiceDate();
        $model->invoiceFromCompanyId = $collectiveInvoice->getInvoiceFromCompanyId();
        $model->isWithTax = $collectiveInvoice->getIsWithTax();
        $model->taxValue = $collectiveInvoice->getTaxValue();
        $model->destinationId = $collectiveInvoice->getDestinationId();
        $model->partnerId = $collectiveInvoice->getPartnerId();
        $model->distributionChannelId = $collectiveInvoice->getDistributionChannelId();
        $model->postage = $collectiveInvoice->getPostage();
        $model->invoiceStatus = $collectiveInvoice->getInvoiceStatus();
        $model->showId = $collectiveInvoice->getIdShow();
        $model->idPreferredPayment = $collectiveInvoice->getIdPreferredPayment();
        $model->currencyRate = new Business\FormType\AppCurrencyRateModel();
        $model->currencyRate->idCurrency = $collectiveInvoice->getIdCurrency();
        $model->currencyRate->exchangeRate = $collectiveInvoice->getExchangeRate();
        
        $model->textAfterDescription = $collectiveInvoice->getTextAfterDescription();
        $model->textFooter = $collectiveInvoice->getTextFooter();

        $model->selectedItems = array();
        $items = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findBy(array('collectiveinvoiceid' => $id));
        foreach ($items as $customer) {
            $model->selectedItems[] = $customer->getId();
        }
        $model->selectedItems = implode(',', $model->selectedItems);

        // Load invoice status
        $model->statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CollectiveInvoiceStatus', 'p')
            ->andWhere('p.collectiveinvoiceid = :collectiveInvoiceId')
            ->setParameter('collectiveInvoiceId', $collectiveInvoice->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppInvoiceStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }        

        return $model;
    }

    public function prepareFormModel($defaultFormModel) {
        $request = $this->container->get('request');    
        if ($request->getMethod() == 'GET') {
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
        }                
        return $defaultFormModel;
    }


    public function buildForm($builder) {
        $id = $this->container->get('request')->query->get('id', 0);

        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId', 'choice', array(
            'label'    => 'Invoice From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => Util::getOpenPartnerList($this->container, $id)
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => Util::getOpenDistributionChannelList($this->container, $id)
        ));
        $builder->add('postage', 'money', array(
            'label'    => 'Postage',
            'required'  => false
        ));
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));
        $builder->add('showId', 'choice', array(
            'label'    => 'Show',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Show')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('selectedItems', 'hidden');
        $builder->add('actionMode', 'hidden');
        $builder->add('target', 'hidden');
        $builder->add('priceOverride', 'hidden');

        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppInvoiceStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',            
            'required' => false
        ));

        $builder->add('sectionExtraText',          'section', array(
            'label' => 'Extra Text',
            'property_path' => false
        ));
        $builder->add('textAfterDescription',      'textarea', array(
            'label' => 'After description',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));
        $builder->add('textFooter',      'textarea', array(
            'label' => 'Footer',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));

        $builder->addEventListener(FormEvents::PRE_BIND , array($this, 'onPreBind'));

        return $builder;
    }

    public $actionMode = '';
    public function onPreBind(DataEvent $event) {
        $filterData = $event->getData();
        $filterData['target'] = '';

        if (isset($filterData['actionMode'])) {
            if ($filterData['actionMode'] == 'save') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $event->setData($filterData);
            }
            else if ($filterData['actionMode'] == 'pdf') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $filterData['target'] = 'pdf';
                $event->setData($filterData);
            }
        }
    }
        
    public function onSuccess() {
        parent::onSuccess();

        if ($this->actionMode == 'save') {
            $model = $this->getCurrentFilter();
            $id = $this->container->get('request')->query->get('id', 0);
            $collectiveInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice', 'app_main')->findOneById($id);

            $collectiveInvoice->setName($model->name);
            $collectiveInvoice->setInvoiceNumber($model->invoiceNumber);
            $collectiveInvoice->setInvoiceDate($model->invoiceDate);
            $collectiveInvoice->setInvoiceFromCompanyId($model->invoiceFromCompanyId);
            $collectiveInvoice->setIsWithTax($model->isWithTax);
            $collectiveInvoice->setTaxValue($model->taxValue);
            $collectiveInvoice->setDestinationId($model->destinationId);
            $collectiveInvoice->setPartnerId($model->partnerId);
            $collectiveInvoice->setDistributionChannelId($model->distributionChannelId);
            $collectiveInvoice->setPostage($model->postage);
            $collectiveInvoice->setInvoiceStatus($model->invoiceStatus);
            $collectiveInvoice->setIdShow($model->showId);
            $collectiveInvoice->setIdPreferredPayment($model->idPreferredPayment);
            if ($model->currencyRate) {
                $collectiveInvoice->setIdCurrency($model->currencyRate->idCurrency);
                $collectiveInvoice->setExchangeRate($model->currencyRate->exchangeRate);
            }

            $collectiveInvoice->setTextAfterDescription($model->textAfterDescription);
            $collectiveInvoice->setTextFooter($model->textFooter);

            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($collectiveInvoice);
            $em->flush();
            
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:Customer', 'c')
                ->set('c.collectiveinvoiceid', 'null')
                ->set('c.priceoverride', 'null')
                ->andWhere('c.collectiveinvoiceid = :id')
                ->setParameter('id', $id)
                ->getQuery()->execute();

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);
                    $customer->setCollectiveInvoiceId($collectiveInvoice->getId());
                    $em->persist($customer);
                }
            }
            
            $em->flush();

            $overrideItems = explode('|', $model->priceOverride);
            foreach ($overrideItems as $priceOverrideItem) {
                $t = explode('=', $priceOverrideItem);
                $idCustomer = intval($t[0]);
                if (in_array($idCustomer, $items) && $idCustomer != 0)  {
                    $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);
                    $customer->setPriceOverride(floatval($t[1]));
                    if (floatval($customer->getPriceOverride()) !== null) {
                        $customer->setRevenueEstimation($customer->getPriceOverride());
                    }

                    $em->persist($customer);    
                }
            }
            $em->flush();

            // Update status
            $dbHelper = $this->container->get('leep_admin.helper.database');    
            $filters = array('collectiveinvoiceid' => $collectiveInvoice->getId());
            $dbHelper->delete($em, 'AppDatabaseMainBundle:CollectiveInvoiceStatus', $filters);

            $sortOrder = 1;
            $lastStatus = 0;
            $lastDate = new \DateTime();
            foreach ($model->statusList as $statusRow) {
                if (empty($statusRow)) continue;
                $status = new Entity\CollectiveInvoiceStatus();
                $status->setCollectiveInvoiceId($collectiveInvoice->getId());
                $status->setStatusDate($statusRow->statusDate);
                $status->setStatus($statusRow->status);
                $status->setNotes($statusRow->notes);
                $status->setSortOrder($sortOrder++);
                $em->persist($status);
                
                $lastStatus = $statusRow->status;
                $lastDate = $statusRow->statusDate;
            }
            $collectiveInvoice->setInvoiceStatus($lastStatus);
            $collectiveInvoice->setStatusDate($lastDate);
            $em->persist($collectiveInvoice);
            $em->flush();

            /*
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
            $this->builder->setData($this->getDefaultFormModel());
            $this->form = $this->builder->getForm();
            */
            $this->messages[] = "The records has been saved successfully"; 
        }
    }
}