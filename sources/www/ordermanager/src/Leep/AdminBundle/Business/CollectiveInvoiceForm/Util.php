<?php
namespace Leep\AdminBundle\Business\CollectiveInvoiceForm;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class Util {
    public static function getOpenDistributionChannelList($container, $selectedId = 0) {
        $doctrine = $container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('dc.id, dc.distributionchannel, COUNT(c) as noOpen')
            ->from('AppDatabaseMainBundle:Customer', 'c')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->andWhere('c.invoicegoestoid = :invoiceGoesToChannel')
            ->andWhere('(c.collectiveinvoiceid IS NULL) or (c.collectiveinvoiceid = 0) or (c.collectiveinvoiceid = :selectedId)')
            ->andWhere('(c.isinvoicecustomer IS NULL) or (c.isinvoicecustomer = 0)')
            ->andWhere('c.isdeleted = 0')
            ->setParameter('selectedId', $selectedId)
            ->setParameter('invoiceGoesToChannel', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL)
            ->orderBy('dc.distributionchannel', 'ASC')
            ->groupBy('dc.id');
        Util::filterByShow($query, $container);
        $result = $query->getQuery()->getResult();
        $arr = array();
        foreach ($result as $row) {
            $arr[$row['id']] = $row['distributionchannel'].' ('.$row['noOpen'].' open )';
        }
        return $arr;
    }

    public static function getOpenPartnerList($container, $selectedId = 0) {
        $doctrine = $container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('p.id, p.partner, COUNT(c) as noOpen')
            ->from('AppDatabaseMainBundle:Customer', 'c')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->innerJoin('AppDatabaseMainBundle:Partner', 'p', 'WITH', 'dc.partnerid = p.id')
            ->andWhere('c.invoicegoestoid = :invoiceGoesToPartner')
            ->andWhere('(c.collectiveinvoiceid IS NULL) or (c.collectiveinvoiceid = 0) or (c.collectiveinvoiceid = :selectedId)')
            ->andWhere('(c.isinvoicecustomer IS NULL) or (c.isinvoicecustomer = 0)')
            ->andWhere('c.isdeleted = 0')
            ->setParameter('selectedId', $selectedId)            
            ->setParameter('invoiceGoesToPartner', Business\DistributionChannel\Constant::INVOICE_GOES_TO_PARTNER)
            ->orderBy('p.partner', 'ASC')
            ->groupBy('p.id');
        Util::filterByShow($query, $container);
        $result = $query->getQuery()->getResult();
        $arr = array();
        foreach ($result as $row) {
            $arr[$row['id']] = $row['partner'].' ('.$row['noOpen'].' open )';
        }
        return $arr;
    }

    public static function filterByShow($query, $container) {
        $form = $container->get('request')->get('form');
        if (isset($form['showId']) && $form['showId'] == Business\CollectiveInvoice\Constant::SHOW_REPORT_SUBMITTED_ONLY) {            
            $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
            $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');            
            
            $reportSubmittedStatus = 8;
            $nutriMeSubmittedStatus = 55;
            $query->andWhere('(FIND_IN_SET('.$reportSubmittedStatus.', c.statusList) > 0) OR (FIND_IN_SET('.$nutriMeSubmittedStatus.', c.statusList) > 0)');
        }
    }
}