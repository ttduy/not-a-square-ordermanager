<?php
namespace Leep\AdminBundle\Business\CollectiveInvoiceForm;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $model = new FilterModel();
        $model->invoiceDate = new \DateTime();
        $model->taxValue = $config->getDefaultTax();

        return $model;
    }

    public function prepareFormModel($defaultFormModel) {
        $request = $this->container->get('request');    
        if ($request->getMethod() == 'GET') {
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
        }                
        return $defaultFormModel;
    }
    

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId', 'choice', array(
            'label'    => 'Invoice From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => Util::getOpenPartnerList($this->container)
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => Util::getOpenDistributionChannelList($this->container)
        ));
        $builder->add('postage', 'money', array(
            'label'    => 'Postage',
            'required'  => false
        ));
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));
        $builder->add('showId', 'choice', array(
            'label'    => 'Show',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Show')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('selectedItems', 'hidden');
        $builder->add('actionMode', 'hidden');
        $builder->add('priceOverride', 'hidden');
        
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppInvoiceStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',            
            'required' => false
        ));

        $builder->add('sectionExtraText',          'section', array(
            'label' => 'Extra Text',
            'property_path' => false
        ));
        $builder->add('textAfterDescription',      'textarea', array(
            'label' => 'After description',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));
        $builder->add('textFooter',      'textarea', array(
            'label' => 'Footer',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));

        $builder->addEventListener(FormEvents::PRE_BIND , array($this, 'onPreBind'));

        return $builder;
    }

    public $actionMode = '';
    public function onPreBind(DataEvent $event) {

        $filterData = $event->getData();
        if (isset($filterData['actionMode']) && $filterData['actionMode'] == 'save') {
            $this->actionMode = 'save';
            $filterData['actionMode'] = '';
            $event->setData($filterData);
        }

        if (isset($filterData['destinationId']) && ($filterData['destinationId'] != 0)) {
            $em = $this->container->get('doctrine')->getEntityManager();
            if ($filterData['destinationId'] == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
                $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($filterData['partnerId']);
                if ($partner && $partner->getIsInvoiceWithTax()) {
                    $filterData['isWithTax'] = true;
                    $event->setData($filterData);
                }
                else {
                    unset($filterData['isWithTax']);
                    $event->setData($filterData);
                }
            }
            else if ($filterData['destinationId'] == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
                $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($filterData['distributionChannelId']);
                if ($dc && $dc->getIsInvoiceWithTax()) {
                    $filterData['isWithTax'] = true;
                    $event->setData($filterData);
                }   
                else {
                    unset($filterData['isWithTax']);
                    $event->setData($filterData);
                }
            }
        }
    }
        
    public function onSuccess() {
        parent::onSuccess();
        if ($this->actionMode == 'save') {
            $model = $this->getCurrentFilter();
            $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
             $query->select('p')
                    ->from('AppDatabaseMainBundle:CollectiveInvoice', 'p')
                    ->andWhere('p.invoicenumber= :invoiceNumber')
                    ->setParameter('invoiceNumber', $model->invoiceNumber);
            $result = $query->getQuery()->getResult();
            if($result){
               $this->errors[] = "Error!";
                $this->getForm()->get('invoiceNumber')->addError(new FormError("invoice number already exist!"));
                return false;
            }
            
            $collectiveInvoice = new Entity\CollectiveInvoice();
            $collectiveInvoice->setName($model->name);
            $collectiveInvoice->setInvoiceNumber($model->invoiceNumber);
            $collectiveInvoice->setInvoiceDate($model->invoiceDate);
            $collectiveInvoice->setInvoiceFromCompanyId($model->invoiceFromCompanyId);
            $collectiveInvoice->setIsWithTax($model->isWithTax);
            $collectiveInvoice->setTaxValue($model->taxValue);
            $collectiveInvoice->setDestinationId($model->destinationId);
            $collectiveInvoice->setPartnerId($model->partnerId);
            $collectiveInvoice->setDistributionChannelId($model->distributionChannelId);
            $collectiveInvoice->setPostage($model->postage);
            $collectiveInvoice->setInvoiceStatus($model->invoiceStatus);
            $collectiveInvoice->setIdShow($model->showId);
            $collectiveInvoice->setIdPreferredPayment($model->idPreferredPayment);
            if ($model->currencyRate) {
                $collectiveInvoice->setIdCurrency($model->currencyRate->idCurrency);
                $collectiveInvoice->setExchangeRate($model->currencyRate->exchangeRate);
            }

            $collectiveInvoice->setTextAfterDescription($model->textAfterDescription);
            $collectiveInvoice->setTextFooter($model->textFooter);

            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($collectiveInvoice);
            $em->flush();

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);
                    $customer->setCollectiveInvoiceId($collectiveInvoice->getId());
                    $em->persist($customer);
                }
            }
            $em->flush();

            $overrideItems = explode('|', $model->priceOverride);
            foreach ($overrideItems as $priceOverrideItem) {
                $t = explode('=', $priceOverrideItem);
                  $idCustomer = intval($t[0]);
                   if (in_array($idCustomer, $items) && $idCustomer != 0)  {
                       $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);
                        $customer->setPriceOverride(floatval($t[1]));
                        $em->persist($customer);    
                    }
            }
            $em->flush();

            // Save status
            $sortOrder = 1;
            $lastStatus = 0;
            $lastDate = new \DateTime();
            foreach ($model->statusList as $statusRow) {
                if (empty($statusRow)) continue;
                $status = new Entity\CollectiveInvoiceStatus();
                $status->setCollectiveInvoiceId($collectiveInvoice->getId());
                $status->setStatusDate($statusRow->statusDate);
                $status->setStatus($statusRow->status);
                $status->setNotes($statusRow->notes);
                $status->setSortOrder($sortOrder++);
                $em->persist($status);
                $lastStatus = $statusRow->status;
                $lastDate = $statusRow->statusDate;
            }
            $collectiveInvoice->setInvoiceStatus($lastStatus);
            $collectiveInvoice->setStatusDate($lastDate);

            $em->flush();

            // Log activity
            $helperActivity = $this->container->get('leep_admin.helper.activity');
            $notes = 'Collective invoice generated';
            $helperActivity->addCollectiveInvoiceActivity($collectiveInvoice->getId(), Business\Customer\Constant::ACTIVITY_TYPE_INVOICE_CREATED, $notes);
                
            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $notes = 'Collective invoice no."'.$collectiveInvoice->getInvoiceNumber().'" is generated for this customer order';
                    $helperActivity->addCustomerActivity($idCustomer, Business\Customer\Constant::ACTIVITY_TYPE_INVOICE_CREATED, $notes);
                }
            }

            // Reload form
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
            $this->buildForm($this->builder);
            $this->builder->setData($this->getDefaultFormModel());
            $this->form = $this->builder->getForm();
                
            $mgr = $this->container->get('easy_module.manager');
            $url = $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_edit', 'list', array('id' => $collectiveInvoice->getId()));
            $this->messages[] = 'The collective invoice has been created. Click <a href="'.$url.'">here</a> to view the invoice';        

            
            
        }
    }
}