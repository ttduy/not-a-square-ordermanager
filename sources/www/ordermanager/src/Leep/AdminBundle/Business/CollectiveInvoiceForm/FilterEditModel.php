<?php
namespace Leep\AdminBundle\Business\CollectiveInvoiceForm;

use Symfony\Component\Validator\ExecutionContext;

class FilterEditModel {
    public $name;
    public $invoiceNumber;
    public $invoiceDate;
    public $invoiceFromCompanyId;
    public $isWithTax;
    public $destinationId;
    public $partnerId;
    public $distributionChannelId;
    public $postage;
    public $showId;
    public $idPreferredPayment;
    public $invoiceStatus;
    public $selectedItems;
    public $priceOverride;
    public $actionMode;
    public $target;
    public $currencyRate;
    
    public $statusList;

    public $textAfterDescription;
    public $textFooter;
}
