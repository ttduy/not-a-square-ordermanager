<?php
namespace Leep\AdminBundle\Business\CryosaveLaboratory;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveLaboratory', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->code = $entity->getCode();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));
        $builder->add('idSampleOrigin', 'choice', array(
            'label' => 'Sample origin',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_SampleOrigin'),
            'required' => false
        ));    

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setCode($model->code);
        $this->entity->setIdSampleOrigin($model->idSampleOrigin);

        parent::onSuccess();
    }
}
