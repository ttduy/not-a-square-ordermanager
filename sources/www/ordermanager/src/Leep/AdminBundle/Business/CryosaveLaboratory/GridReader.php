<?php
namespace Leep\AdminBundle\Business\CryosaveLaboratory;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'code', 'idSampleOrigin', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Code', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Sample origin', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CryosaveLaboratoryGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CryosaveLaboratory', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cryosaveUserModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cryosave_laboratory', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cryosave_laboratory', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
