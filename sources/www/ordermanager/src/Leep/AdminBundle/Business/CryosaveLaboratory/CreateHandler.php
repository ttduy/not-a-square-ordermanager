<?php
namespace Leep\AdminBundle\Business\CryosaveLaboratory;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));     
        $builder->add('idSampleOrigin', 'choice', array(
            'label' => 'Sample origin',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_SampleOrigin'),
            'required' => false
        ));    
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $pdfDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/third_party_pdf/Cryosave';
        @mkdir($pdfDir);
        @mkdir($pdfDir.'/'.$model->code);

        $laboratory = new Entity\CryosaveLaboratory();
        $laboratory->setName($model->name);
        $laboratory->setCode($model->code);
        $laboratory->setIdSampleOrigin($model->idSampleOrigin);

        $em->persist($laboratory);
        $em->flush();

        parent::onSuccess();
    }
}
