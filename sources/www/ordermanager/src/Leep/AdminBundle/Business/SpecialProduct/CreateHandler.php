<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->container = $this->container;
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution Channel',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Category_List')
        ));
        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $specialProduct = new Entity\SpecialProduct();
        $specialProduct->setName($model->name);
        $specialProduct->setCodename($model->codename);
        $specialProduct->setIdDistributionChannel($model->idDistributionChannel);
        $specialProduct->setIdCategory($model->idCategory);
        $specialProduct->setIdProduct($model->idProduct);

        $em->persist($specialProduct);
        $em->flush();

        parent::onSuccess();
    }
}
