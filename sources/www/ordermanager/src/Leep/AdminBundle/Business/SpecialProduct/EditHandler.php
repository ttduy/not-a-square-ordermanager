<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->container = $this->container;
        $model->entity = $entity;
        $model->name = $entity->getName();
        $model->codename = $entity->getCodename();
        $model->idDistributionChannel = $entity->getIdDistributionChannel();
        $model->idCategory = $entity->getIdCategory();
        $model->idProduct = $entity->getIdProduct();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution Channel',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Category_List')
        ));
        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setCodename($model->codename);
        $this->entity->setIdDistributionChannel($model->idDistributionChannel);
        $this->entity->setIdCategory($model->idCategory);
        $this->entity->setIdProduct($model->idProduct);
        
        parent::onSuccess();
    }
}
