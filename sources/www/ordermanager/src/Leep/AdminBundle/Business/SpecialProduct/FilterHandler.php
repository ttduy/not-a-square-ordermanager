<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idDistributionChannel = 0;
        $model->idProduct = 0;
        $model->idCategory = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'empty_value' => false
        ));
        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'empty_value' => false
        ));
        return $builder;
    }
}