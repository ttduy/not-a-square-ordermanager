<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $container;
    public $entity;

    public $idDistributionChannel;
    public $idCategory;
    public $idProduct;
    public $name;
    public $codename;

    public function isValid(ExecutionContext $context) {
        if ($this->idProduct != 0 && $this->idCategory != 0) {                
            $context->addViolationAtSubPath('idProduct', "Can't choose both product and category");                
            return false;
        }
        if ($this->idProduct == 0 && $this->idCategory == 0) {                
            $context->addViolationAtSubPath('idProduct', "Please choose either product or category");                
            return false;
        }
        
        // Validate product
        if ($this->idProduct != 0) {
            $criteria = array(
                'idDistributionChannel' => $this->idDistributionChannel,
                'idProduct' => $this->idProduct
            );
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findBy($criteria);
            foreach ($result as $p) {
                if ($p->getId() != $this->entity->getId()) {
                    $context->addViolationAtSubPath('idProduct', "Special product for the distribution/product is already existed ('".$p->getName()."')");                
                    return false;    
                }
            }
        }

        // Validate category
        if ($this->idCategory != 0) {
            $criteria = array(
                'idDistributionChannel' => $this->idDistributionChannel,
                'idCategory' => $this->idCategory
            );
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findBy($criteria);
            foreach ($result as $p) {
                if ($p->getId() != $this->entity->getId()) {
                    $context->addViolationAtSubPath('idCategory', "Special product for the distribution/category is already existed ('".$p->getName()."')");                
                    return false;    
                }
            }
        }
    }
}
