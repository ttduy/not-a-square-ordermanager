<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('idDistributionChannel', 'idCategory', 'idProduct', 'name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Distribution Channel', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Category', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Product', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Name', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_SpecialProductGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:SpecialProduct', 'p')
            ->andWhere('p.isDeleted = 0')
            ->orderBy('p.idDistributionChannel', 'ASC');


        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if ($this->filters->idDistributionChannel != 0) {
            $queryBuilder->andWhere('p.idDistributionChannel = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $this->filters->idDistributionChannel);
        }
        if ($this->filters->idCategory != 0) {
            $queryBuilder->andWhere('p.idCategory = :idCategory')
                ->setParameter('idCategory', $this->filters->idCategory);
        }        
        if ($this->filters->idProduct != 0) {
            $queryBuilder->andWhere('p.idProduct = :idProduct')
                ->setParameter('idProduct', $this->filters->idProduct);
        }        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'special_product', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'special_product', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
