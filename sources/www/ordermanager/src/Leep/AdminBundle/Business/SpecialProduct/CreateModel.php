<?php
namespace Leep\AdminBundle\Business\SpecialProduct;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;

    public $idDistributionChannel;
    public $idCategory;
    public $idProduct;
    public $name;
    public $codename;
    
    public function isValid(ExecutionContext $context) {
        if ($this->idProduct != 0 && $this->idCategory != 0) {                
            $context->addViolationAtSubPath('idProduct', "Can't choose both product and category");                
            return false;
        }
        if ($this->idProduct == 0 && $this->idCategory == 0) {                
            $context->addViolationAtSubPath('idProduct', "Please choose either product or category");                
            return false;
        }

        // Validate DC/Product
        if ($this->idProduct != 0) {
            $criteria = array(
                'idDistributionChannel' => $this->idDistributionChannel,
                'idProduct' => $this->idProduct
            );
            $p = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findOneBy($criteria);
            if ($p) {
                $context->addViolationAtSubPath('idProduct', "Special product for the distribution/product is already existed ('".$p->getName()."')");                
            }
        }

        // Validate DC/Category
        if ($this->idCategory != 0) {
            $criteria = array(
                'idDistributionChannel' => $this->idDistributionChannel,
                'idCategory' => $this->idCategory
            );
            $p = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findOneBy($criteria);
            if ($p) {
                $context->addViolationAtSubPath('idCategory', "Special product for the distribution/category is already existed ('".$p->getName()."')");                
            }
        }
    }
}
