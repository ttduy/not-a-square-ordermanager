<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class FamVicMappingRuleEditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        return null;
    }

    public static function getFamVicMappingCode($famVal, $vicVal) {
        return ($famVal ? '1' : '0' ).($vicVal ? '1' : '0');
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrDefaultData = array();

        $results = $em->getRepository('AppDatabaseMainBundle:FamVicMappingRule')->findAll();
        foreach ($results as $r) {
            $code = $this->getFamVicMappingCode($r->getResultFam(), $r->getResultVic());
            $arrDefaultData[$r->getIdResultGroup().'_'.$code] = $r->getResult();
        }

        return $arrDefaultData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $em = $this->container->get('doctrine')->getEntityManager();
        $resultGroups = $mapping->getMapping('LeepAdmin_Gene_ResultOptions');

        $resultGroupOptions = array();
        $results = $em->getRepository('AppDatabaseMainBundle:Results')->findAll();
        foreach ($results as $r) {
            if (!isset($resultGroupOptions[$r->getGroupId()])) {
                $resultGroupOptions[$r->getGroupId()] = array();
            }
            $resultGroupOptions[$r->getGroupId()][$r->getResult()] = $r->getResult();
        }

        foreach ($resultGroups as $key => $groupName) {
            $famVicCodes = $mapping->getMapping('LeepAdmin_Fam_Vic_Mapping_Code_List');
            foreach ($famVicCodes as $code => $title) {
                $builder->add($key.'_'.$code, 'choice', array(
                    'label'     => $title,
                    'required'  => false,
                    'choices'   => isset($resultGroupOptions[$key]) ? $resultGroupOptions[$key] : array(),
                    'attr'      => array('style' => 'width: 100px')
                ));                
            }
        }
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();      
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $mapping = $this->container->get('easy_mapping');
        $model = $this->getForm()->getData();        
        
        // remove all existing ones
        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:FamVicMappingRule', 'p')
                ->getQuery()
                ->execute();

        $mappingCode = $mapping->getMapping('LeepAdmin_Fam_Vic_Mapping_Code_List');
        $resultGroups = $mapping->getMapping('LeepAdmin_Gene_ResultOptions');
        $counter = 0;
        foreach ($resultGroups as $idResultGroup => $groupName) {
            foreach ($mappingCode as $code => $title) {
                list($fam, $vic) = str_split($code);
                $idInput = $idResultGroup.'_'.$code;

                if (array_key_exists($idInput, $model)) {
                    $newMapping = new Entity\FamVicMappingRule();
                    $newMapping->setIdResultGroup($idResultGroup);
                    $newMapping->setResultFam($fam);
                    $newMapping->setResultVic($vic);
                    $newMapping->setResult($model[$idInput]);
                    $em->persist($newMapping);
                    $counter++;
                }
            }
            
            if ($counter % 500 == 0) {
                $em->flush();
            }
        }

        $em->flush();
        
        $this->messages[] = 'The record has been updated successfully';
    }
}
