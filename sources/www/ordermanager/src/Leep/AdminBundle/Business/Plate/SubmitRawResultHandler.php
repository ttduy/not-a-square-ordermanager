<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class SubmitRawResultHandler extends BaseCreateHandler {
    public $requestedIdPlate = null;

    public function getDefaultFormModel() {
        $model = new SubmitRawResultModel();

        $this->requestedIdPlate = $this->container->get('request')->get('id');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('result', 'textarea', array(
            'label'    => 'Result',
            'required' => true,
            'attr'     => array(
                'style'   => 'width: 800px;height: 400px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();

        $parser = $this->container->get('leep_admin.plate.business.submit_raw_result_parser');
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');

        $doctrine = $this->container->get('doctrine');
        $model = $this->getForm()->getData();
        
        $parser->init();
        list($result, $errors) = $parser->parseResult($model->result);

        if (count($errors) > 0) {
            $cacheBox->putKey('parseErrors', $errors);
            return false;
        }

        // create new PLATE RAW RESULT
        $plateRawResult = new Entity\PlateRawResult();
        $plateRawResult->setIdPlate($this->requestedIdPlate);
        $plateRawResult->setCreationDateTime(new \DateTime());
        $plateRawResult->setRawData($model->result);
        $em->persist($plateRawResult);
        $em->flush();

        // number of records in $result > 0, we have data to process
        if (sizeof($result) > 0) {
            $counter = 0;
            foreach ($result as $i => $row) {
                $plateRawResultDetail = new Entity\PlateRawResultDetail();
                $plateRawResultDetail->setIdPlateRawResult($plateRawResult->getId());
                $plateRawResultDetail->setWellIndex($row['wellIndex']);
                $plateRawResultDetail->setCycle($row['cycle']);
                $plateRawResultDetail->setFam($row['fam']);
                $plateRawResultDetail->setRox($row['rox']);
                $plateRawResultDetail->setVic($row['vic']);
                $em->persist($plateRawResultDetail);

                $counter++;

                if ($counter % 2000 == 0) {
                    $em->flush();
                    $counter = 0;
                }
            }

            $em->flush();
        }

        $plate = $em->getRepository('AppDatabaseMainBundle:Plate')->findOneById($this->requestedIdPlate);
        $plate->setIdCurrentPlateRawResult($plateRawResult->getId());
        $em->flush();

        $this->messages = array();
        $this->messages[] = "Submited Raw Result successfully!";
    }
}
