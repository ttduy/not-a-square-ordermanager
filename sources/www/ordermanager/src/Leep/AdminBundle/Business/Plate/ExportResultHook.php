<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\ModuleBundle\Module\AbstractHook;

class ExportResultHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();        
        $mgr = $controller->get('easy_module.manager');
        $request = $controller->get('request');

        $plate = $em->getRepository('AppDatabaseMainBundle:Plate')->findOneById($request->get('id', 0));
        $exportData = Utils::buildExportData($controller, $plate);
        $data['data'] = $exportData;

        $data['editCustomerInfo'] = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit');
    }
}
