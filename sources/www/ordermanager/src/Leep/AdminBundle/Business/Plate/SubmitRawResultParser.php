<?php
namespace Leep\AdminBundle\Business\Plate;

class SubmitRawResultParser {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function init() {
    }

    public function parseResult($result) {
        $rowResult = array();
        $rowError = array();

        $rows = explode("\n", $result);
        foreach ($rows as $row) {
            if (trim($row) != '') {
                $rowContent = $this->parseRow($row);
                if (!empty($rowContent['error'])) {
                    $rowError[] = $rowContent;
                } else {
                    $rowResult[] = $rowContent;
                }
            }
        }

        return array($rowResult, $rowError);
    }

    /**
    * Well;Cycle;FAM;ROX;VIC
    * 1;1;71,408.547;116,432.648;32,715.486
    */
    public function parseRow($row) {
        $tokens = explode(Constant::RAW_RESULT_ENTRY_DATA_SEPARATOR, $row);

        $rowResult = array(
            'raw'         => $row,
            'tokens'      => $tokens,
            'error'       => '',
            'error_type'  => ''
        );

        /////////////////////////////////
        // x. Validate row formats
        if (count($tokens) != 5) {
            $rowResult['error'] = 'Invalid format, expecting 5 tokens, '.count($tokens).' found';
            $rowResult['error_type'] = Constant::TYPE_FORMAT_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Validate tokens
        $wellIndex = trim($tokens[0]);
        $cycle = trim($tokens[1]);
        $fam = str_replace(',', '', trim($tokens[2]));
        $rox = str_replace(',', '', trim($tokens[3]));
        $vic = str_replace(',', '', trim($tokens[4]));

        /////////////////////////////////
        // x. Check well index, must be int
        if (!is_numeric($wellIndex)) {
            $rowResult['error'] = "Invalid Well Index '".$wellIndex."'";
            $rowResult['error_type'] = Constant::TYPE_DATA_ERROR;
            return $rowResult;
        }
        $rowResult['wellIndex'] = intval($wellIndex);

        // x. Check cycle, must be int
        if (!is_numeric($cycle)) {
            $rowResult['error'] = "Invalid Cycle '".$cycle."'";
            $rowResult['error_type'] = Constant::TYPE_DATA_ERROR;
            return $rowResult;
        }
        $rowResult['cycle'] = intval($cycle);

        // x. Check fam, must be float
        $rowResult['fam'] = floatval($fam);

        // x. Check rox, must be float
        $rowResult['rox'] = floatval($rox);

        // x. Check vic, must be float
        $rowResult['vic'] = floatval($vic);

        return $rowResult;
    }
}
