<?php 
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeletePlateRawResultHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $plateRawResult = $em->getRepository('AppDatabaseMainBundle:PlateRawResult')->findOneById($id);
        
        if ($plateRawResult) {
            $this->idRedirect = $plateRawResult->getIdPlate();

            $delQuery = $em->createQueryBuilder();
            $delQuery->delete('AppDatabaseMainBundle:PlateRawResultDetail', 'p')
                    ->andWhere('p.idPlateRawResult = :idPlateRawResult')
                    ->setParameter('idPlateRawResult', $plateRawResult->getId())
                    ->getQuery()
                    ->execute();

            $em->remove($plateRawResult);
            $em->flush();
        }



    }
}