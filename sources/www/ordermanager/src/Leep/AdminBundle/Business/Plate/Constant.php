<?php 
namespace Leep\AdminBundle\Business\Plate;

class Constant {
	const FAM_CODE = '10';
	const VIC_CODE = '01';
	const FAM_AND_VIC_CODE = '11';

	public static function getFamVicMappingCodeList() {
		return array(
				self::FAM_CODE 				=> 'FAM',
				self::VIC_CODE 				=> 'VIC',
				self::FAM_AND_VIC_CODE 		=> 'FAM AND VIC'
			);
	}

    const TYPE_FORMAT_ERROR     = 10;
    const TYPE_DATA_ERROR       = 20;
    const TYPE_SUCCESS          = 30;

    const RAW_RESULT_ENTRY_DATA_SEPARATOR = ';';



    const ASSIGNMENT_FAM        = 1;
    const ASSIGNMENT_VIC        = 2;
    const ASSIGNMENT_FAM_VIC    = 3;
    const ASSIGNMENT_FAILED     = 99;
    public static function getAssignmentMap() {
        return array(
            self::ASSIGNMENT_FAM       => 'fam',
            self::ASSIGNMENT_VIC       => 'vic',
            self::ASSIGNMENT_FAM_VIC   => 'fam+vic',
            self::ASSIGNMENT_FAILED    => 'failed'
        );
    }

    public static function getAssignmentText() {
        return array(
            self::ASSIGNMENT_FAM       => '<b style="color: #0070C0">FAM</b>',
            self::ASSIGNMENT_VIC       => '<b style="color: #00B050">VIC</b>',
            self::ASSIGNMENT_FAM_VIC   => '<b style="color: #008E82">FAM+VIC</b>',
            self::ASSIGNMENT_FAILED    => '<b style="color: #000000">FAILED</b>'
        );
    }
}