<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        

        $today = new \DateTime();
        $todayStr = $today->format('d/m/Y');
        $model->placeBox = array();
        for ($i = 0; $i < 8; $i++) {
            for ($j = 0; $j < 12; $j++) {
                $model->plateBox['cell_'.$i.'_'.$j] = '##'.$todayStr;
            }
        }

        $model->creationDatetime = $today;
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('creationDatetime', 'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('idPlateType',     'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'     => $mapping->getMapping('LeepAdmin_Plate_Type'),
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('sectionPlateBox',          'section', array(
            'label' => 'Plate',
            'property_path' => false
        ));
        $builder->add('plateBox',      'app_plate_box', array(
            'label'    => 'Plate box',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $plate = new Entity\Plate();
        $plate->setName($model->name);
        $plate->setIdPlateType($model->idPlateType);
        $plate->setCreationDatetime($model->creationDatetime);
        $plate->setNotes($model->notes);
        $em->persist($plate);
        $em->flush();

        foreach ($model->plateBox as $wellId => $wellValue) {
            $arr = explode('_', $wellId);
            if (count($arr) == 3) {
                $row = intval($arr[1]);
                $col = intval($arr[2]);

                $arr = explode('##', $wellValue);
                if ((count($arr) == 2) && ($arr[0] != '')) {
                    $orderNumber = $arr[0];
                    $idOrder = null;
                    $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByOrdernumber($orderNumber);
                    if ($order) {
                        $idOrder = $order->getId();
                    }

                    $well = new Entity\PlateWell();
                    $well->setIdPlate($plate->getId());
                    $well->setRow($row);
                    $well->setCol($col);
                    $well->setOrderNumber($orderNumber);
                    $well->setIdOrder($idOrder);
                    if ($arr[1] != '') {
                        $well->setCreationDate(\DateTime::createFromFormat('d/m/Y', $arr[1]));
                    }
                    $em->persist($well);
                }
            }
        }
        $em->flush();

        $this->newId = $plate->getId();
        parent::onSuccess();
    }
}
