<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $request = $this->container->get('request');
        $idFromPlate = $request->get('idFromPlate', 0);

        $model = new CopyModel();        
        $model->idPlate = $idFromPlate;
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $request = $this->container->get('request');

        $idFromPlate = $request->get('idFromPlate', 0);
        $idToPlate = $request->get('id', 0);


        $builder->add('sectionFromPlateBox',          'section', array(
            'label' => 'Copy from this plate',
            'property_path' => false
        ));
        $builder->add('idPlate', 'searchable_box', array(
            'label'    => 'Plate',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Plate_List')
        ));
        $builder->add('fromPlateBox',      'app_plate_box_selector', array(
            'label'    => 'Plate box',
            'required' => false,
            'idPlate'  => $idFromPlate
        ));

        $builder->add('sectionToPlateBox',          'section', array(
            'label' => 'Copy to this plate',
            'property_path' => false
        ));
        $builder->add('targetPlateBox',      'app_plate_box_selector', array(
            'label'    => 'Plate box',
            'required' => false,
            'idPlate'  => $idToPlate
        ));
    }

    private function getSelectedColumns($plateBox) {
        $columns = array();
        for ($i = 0; $i < 12; $i++) {
            $varName = 'columnSelect_'.$i;
            if (isset($plateBox[$varName]) && ($plateBox[$varName] == '1')) {
                $columns[] = $i;
            }
        }
        return $columns;
    }

    private function deletePlateColumns($em, $idPlate, $col) {
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:PlateWell', 'p')
            ->andWhere('p.idPlate = :idPlate')
            ->andWhere('p.col = :col')
            ->setParameter('idPlate', $idPlate)
            ->setParameter('col', $col)
            ->getQuery()
            ->execute();
        $em->flush();
    }
    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $request = $this->container->get('request');

        $idFromPlate = $request->get('idFromPlate', 0);
        $idToPlate = $request->get('id', 0);

        if ($model->idPlate == 0) {
            $this->errors[] = 'Please select a plate to copy';
            return;
        }
        $fromColumns  = $this->getSelectedColumns($model->fromPlateBox);
        $toColumns = $this->getSelectedColumns($model->targetPlateBox);

        if (empty($fromColumns)) {
            $this->errors[] = 'Please select at least one column to copy';
            return;
        }

        if (count($fromColumns) != count($toColumns)) {
            $this->errors[] = 'From plate has '.count($fromColumns). ' columns selected, while target plate has '.count($toColumns).' columns selected';
            return;            
        }

        // Perform copy
        for ($i = 0; $i < count($fromColumns); $i++) {
            $fromCol = $fromColumns[$i];
            $toCol = $toColumns[$i];

            // Retrieve data
            $fromWells = array();
            $results = $em->getRepository('AppDatabaseMainBundle:PlateWell')->findBy(array(
                'idPlate' => $idFromPlate,
                'col'     => $fromCol
            ));
            foreach ($results as $r) {
                $fromWells[] = array(
                    'row' => $r->getRow(),
                    'orderNumber' => $r->getOrderNumber(),
                    'idOrder' => $r->getIdOrder(),
                    'creationDate' => $r->getCreationDate()
                );
            }

            // Delete column
            $this->deletePlateColumns($em, $idFromPlate, $fromCol);
            $this->deletePlateColumns($em, $idToPlate, $toCol);

            // Insert to new plate
            foreach ($fromWells as $well) {
                $newWell = new Entity\PlateWell();
                $newWell->setIdPlate($idToPlate);
                $newWell->setRow($well['row']);
                $newWell->setCol($toCol);
                $newWell->setOrderNumber($well['orderNumber']);
                $newWell->setIdOrder($well['idOrder']);
                $newWell->setCreationDate($well['creationDate']);
                $em->persist($newWell);
            }
            $em->flush();
        }
        parent::onSuccess();
    }
}
