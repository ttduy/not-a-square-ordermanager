<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Plate', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->idPlateType = $entity->getIdPlateType();
        $model->creationDatetime = $entity->getCreationDatetime();
        $model->notes = $entity->getNotes();

        $model->plateBox = array();
        $results = $em->getRepository('AppDatabaseMainBundle:PlateWell')->findByIdPlate($entity->getId());
        foreach ($results as $r) {
            $id = 'cell_'.$r->getRow().'_'.$r->getCol();

            $date = '';
            if ($r->getCreationDate() != '') {
                $date = $r->getCreationDate()->format('d/m/Y');
            }
            $value = $r->getOrderNumber().'##'.$date;
            $model->plateBox[$id] = $value;
        }

        $today = new \DateTime();
        $todayStr = $today->format('d/m/Y');
        for ($i = 0; $i < 8; $i++) {
            for ($j = 0; $j < 12; $j++) {
                if (!isset($model->plateBox['cell_'.$i.'_'.$j])) {
                    $model->plateBox['cell_'.$i.'_'.$j] = '##'.$todayStr;
                }
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('creationDatetime', 'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('idPlateType',     'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'     => $mapping->getMapping('LeepAdmin_Plate_Type'),
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('sectionPlateBox',          'section', array(
            'label' => 'Plate',
            'property_path' => false
        ));
        $builder->add('plateBox',      'app_plate_box', array(
            'label'    => 'Plate box',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();      
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $model = $this->getForm()->getData();        
        
        $this->entity->setName($model->name);
        $this->entity->setIdPlateType($model->idPlateType);
        $this->entity->setCreationDatetime($model->creationDatetime);
        $this->entity->setNotes($model->notes);

        $filters = array('idPlate' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:PlateWell', $filters);
        foreach ($model->plateBox as $wellId => $wellValue) {
            $arr = explode('_', $wellId);
            if (count($arr) == 3) {
                $row = intval($arr[1]);
                $col = intval($arr[2]);

                $arr = explode('##', $wellValue);
                if ((count($arr) == 2) && ($arr[0] != '')) {
                    $orderNumber = $arr[0];
                    $idOrder = null;

                    $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByOrdernumber($orderNumber);
                    if ($order) {
                        $idOrder = $order->getId();
                    }

                    $well = new Entity\PlateWell();
                    $well->setIdPlate($this->entity->getId());
                    $well->setRow($row);
                    $well->setCol($col);
                    $well->setOrderNumber($orderNumber);
                    $well->setIdOrder($idOrder);
                    if ($arr[1] != '') {
                        $well->setCreationDate(\DateTime::createFromFormat('d/m/Y', $arr[1]));
                    }
                    $em->persist($well);
                }
            }
        }
        $em->flush();

        parent::onSuccess();
    }
}
