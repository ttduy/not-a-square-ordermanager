<?php
namespace Leep\AdminBundle\Business\Plate;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class Utils {
    public static function buildExportData($controller, $plate) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $converter = $controller->get('leep_admin.plate.business.assignment_converter');
        $converter->load();
        
        $data = array();
        $rowMap = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');

        // Plate 
        $result = $em->getRepository('AppDatabaseMainBundle:PlateWell')->findByIdPlate($plate->getId());
        $plateMap = array();
        foreach ($result as $r) {
            $colIndex = $rowMap[$r->getRow()].$r->getCol();
            $plateMap[$colIndex] = array(
                'orderNumber' => $r->getOrderNumber(),
                'idOrder'     => $r->getIdOrder()
            );
        }

        // Plate assignment
        $plateAssignment = array();
        $result = $em->getRepository('AppDatabaseMainBundle:PlateResult')->findByIdPlateRawResult($plate->getIdCurrentPlateRawResult());
        foreach ($result as $r) {
            $plateAssignment[$r->getWellIndex()] = $r->getAssignment();
        }

        // Customer map
        $orderNumberMap = array();
        $query = $em->createQueryBuilder();
        $query->select('p.ordernumber, p.idCustomerInfo')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $orderNumberMap[$r['ordernumber']] = $r['idCustomerInfo'];
        }


        // Plate types
        $result = $em->getRepository('AppDatabaseMainBundle:PlateTypeWell')->findByIdPlateType($plate->getIdPlateType(), array('idGene' => 'ASC', 'row' => 'ASC', 'col' => 'ASC'));
        foreach ($result as $r) {
            $index = $r->getRow() * 24 + $r->getCol() + 1;
            $orderNumber = '';
            $idOrder = 0;
            $assignment = isset($plateAssignment[$index]) ? $plateAssignment[$index] : 0;
            $geneResult = $converter->convert($r->getIdGene(), $assignment);
            $currentResult = '';

            if (isset($plateMap[$r->getPlateWellIndex()])) {
                $orderNumber = $plateMap[$r->getPlateWellIndex()]['orderNumber'];
            }
            $idCustomerInfo = isset($orderNumberMap[$orderNumber]) ? $orderNumberMap[$orderNumber] : 0;        
            $c = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findOneBy(array('idCustomer' => $idCustomerInfo, 'idGene' => $r->getIdGene()));
            if ($c) {
                $currentResult = $c->getResult();
            }

            // Validation
            $errors = array();
            if (empty($orderNumber)) {
                $errors[] = "Don't have order number";
            }
            else if (empty($idCustomerInfo)) {
                $errors[] = "Can't find the order number: ".$orderNumber;
            }

            $isConflict = false;
            if ((trim($currentResult) != '') && 
                trim($currentResult) != trim($geneResult)) {
                $isConflict = true;
                $errors[] = 'Conflict result!';
            }

            $isSame = false;
            if ((trim($currentResult) != '') && 
                trim($currentResult) == trim($geneResult)) {
                $isSame = true;
            }

            $data[$index] = array(
                'wellIndex'       => $index,
                'orderNumber'     => $orderNumber,
                'idCustomerInfo'  => $idCustomerInfo,
                'idGene'          => $r->getIdGene(),
                'gene'            => $formatter->format($r->getIdGene(), 'mapping', 'LeepAdmin_Gene_List'),
                'assignment'      => $assignment,
                'assignmentText'  => $formatter->format($assignment, 'mapping', 'LeepAdmin_Plate_AssignmentText'),
                'geneResult'      => $geneResult,
                'currentResult'   => $currentResult,
                'errors'          => $errors,
                'isConflict'      => $isConflict,
                'isSame'          => $isSame
            );
        }

        usort($data, '\Leep\AdminBundle\Business\Plate\Utils::cmp');

        return $data;
    }
    public static function cmp($a, $b)
    {
        return strcasecmp($a['orderNumber'], $b['orderNumber'])*-1;
    }
}