<?php
namespace Leep\AdminBundle\Business\Plate;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $creationDatetime;
    public $idPlateType;
    public $notes;
    public $plateBox;
}
