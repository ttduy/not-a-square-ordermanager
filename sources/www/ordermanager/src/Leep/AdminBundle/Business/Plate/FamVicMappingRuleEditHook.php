<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\ModuleBundle\Module\AbstractHook;

class FamVicMappingRuleEditHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');

        $mapping = $controller->get('easy_mapping');
        $data['resultGroups'] = $mapping->getMapping('LeepAdmin_Gene_ResultOptions');
        $data['famVicMappingCodes'] = $mapping->getMapping('LeepAdmin_Fam_Vic_Mapping_Code_List');
    }
}
