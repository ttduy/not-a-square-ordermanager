<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class SubmitRawResultHistoryGridReader extends AppGridDataReader {
    public $filters;
    public $requestedIdPlate;

    public function __construct($container) {
        $request = $container->get('request');
        parent::__construct($container);
        $this->formatter = $this->container->get('leep_admin.helper.formatter');

        $this->requestedIdPlate = $request->get('id', null);
    }
    
    public function getColumnMapping() {
        return array('creationDatetime', 'rawData', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Date Time',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Data',        'width' => '55%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_Plate_SubmitRawResultHistoryGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.creationDatetime', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, p.creationDatetime, p.excelName')
            ->from('AppDatabaseMainBundle:PlateRawResult', 'p')
            ->andWhere('p.idPlate = :idPlate')
            ->setParameter('idPlate', $this->requestedIdPlate);

        if (!empty($this->filters->dateFrom)) {
            $queryBuilder->andWhere('p.creationDatetime >= :dateFrom')
                ->setParameter('dateFrom', $this->filters->dateFrom);
        }

        if (!empty($this->filters->dateTo)) {
            $queryBuilder->andWhere('p.creationDatetime <= :dateTo')
                ->setParameter('dateTo', $this->filters->dateTo);
        }
    }

    public function buildCellRawData($row) {
        $mgr = $this->getModuleManager();
        $html = '';
        $html .= '<a href="'.$mgr->getUrl('leep_admin', 'plate', 'feature', 'downloadHistory', array('id' => $row['id'])).'">Download</a>';

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('plateModify')) {
            //$builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'plate', 'delete_plate_raw_result', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }
}
