<?php
namespace Leep\AdminBundle\Business\Plate;

use Symfony\Component\Validator\ExecutionContext;

class CopyModel {
    public $idPlate;
    public $fromPlateBox;
    public $targetPlateBox;
}
