<?php
namespace Leep\AdminBundle\Business\Plate;

class AssignmentConverter {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $resultGroupMap = array();
    public $geneResultGroup = array();

    public function mapAssignment($fam, $vic) {
        if ($fam && $vic) {
            return Constant::ASSIGNMENT_FAM_VIC;
        }
        if ($fam) {
            return Constant::ASSIGNMENT_FAM;
        }
        if ($vic) {
            return Constant::ASSIGNMENT_VIC;
        }
        return Constant::ASSIGNMENT_FAILED;
    }

    public function load() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Result groups
        $this->resultGroupMap = array();
        $result = $em->getRepository('AppDatabaseMainBundle:FamVicMappingRule')->findAll();
        foreach ($result as $r) {
            if (!isset($this->resultGroupMap[$r->getIdResultGroup()])) {
                $this->resultGroupMap[$r->getIdResultGroup()] = array();
            }
            $assignment = $this->mapAssignment($r->getResultFam(), $r->getResultVic());
            $this->resultGroupMap[$r->getIdResultGroup()][$assignment] = $r->getResult();
        }

        // Gene map
        $this->geneResultGroup = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        foreach ($result as $r) {
            $this->geneResultGroup[$r->getId()] = $r->getGroupId();
        }
    }

    public function convert($idGene, $assignment) {
        $idResultGroup = isset($this->geneResultGroup[$idGene]) ? $this->geneResultGroup[$idGene] : 0;        
        if (isset($this->resultGroupMap[$idResultGroup])) {
            if (isset($this->resultGroupMap[$idResultGroup][$assignment])) {                
                return $this->resultGroupMap[$idResultGroup][$assignment];
            }
        }
        return '';
    }
}
