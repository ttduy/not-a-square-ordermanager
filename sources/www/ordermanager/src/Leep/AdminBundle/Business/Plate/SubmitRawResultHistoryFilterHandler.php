<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class SubmitRawResultHistoryFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new SubmitRawResultHistoryFilterModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));

        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));

        return $builder;
    }
}