<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('creationDatetime', 'idPlateType', 'name', 'result', 'analyse', 'export', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Datetime',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Type',       'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Name',       'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Result',     'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Analyse',    'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Export',     'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Action',     'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_PlateGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Plate', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:PlateWell', 'w', 'WITH', 'p.id = w.idPlate')
                ->andWhere('w.orderNumber = :orderNumber')
                ->setParameter('orderNumber', trim($this->filters->orderNumber));
        }
        if ($this->filters->idPlateType != 0) {
            $queryBuilder->andWhere('p.idPlateType = :idPlateType')
                ->setParameter('idPlateType', $this->filters->idPlateType);
        }
    }

    public function buildCellResult($row) {        
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('plateModify')) {
            $builder->addPopupButton('Submit Plate Raw Result', $mgr->getUrl('leep_admin', 'plate', 'submit_raw_result', 'create', array('id' => $row->getId())), 'button-redo');
            $builder->addPopupButton('View History', $mgr->getUrl('leep_admin', 'plate', 'view_history_raw_result_submission', 'list', array('id' => $row->getId())), 'button-detail');
        }

        return $builder->getHtml();
    }

    public function buildCellAnalyse($row) {        
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('plateModify')) {
            if ($row->getIdCurrentPlateRawResult() > 0) {
                $builder->addNewTabButton('Analyse', $mgr->getUrl('leep_admin', 'plate', 'feature', 'analyse', array('id' => $row->getId())), 'button-detail');
            }
        }

        return $builder->getHtml();
    }

    public function buildCellExport($row) {        
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('plateModify')) {
            if ($row->getIdCurrentPlateRawResult() > 0) {
                $builder->addButton('Export', $mgr->getUrl('leep_admin', 'plate', 'export_result', 'edit', array('id' => $row->getId())), 'button-export');
            }
        }

        return $builder->getHtml();
    }
    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('plateModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'plate', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'plate', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
