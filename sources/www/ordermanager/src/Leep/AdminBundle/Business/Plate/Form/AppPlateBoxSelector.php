<?php
namespace Leep\AdminBundle\Business\Plate\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPlateBoxSelector extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'typeMapping' => '',
            'idPlate'     => 0
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_plate_box_selector';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   

        $manager = $this->container->get('easy_module.manager');        
        $em = $this->container->get('doctrine')->getEntityManager();
        $today = new \DateTime();

        $idPlate = isset($options['idPlate']) ? intval($options['idPlate']) : 0;
        $plate = $em->getRepository('AppDatabaseMainBundle:Plate')->findOneById($idPlate);
        $results = $em->getRepository('AppDatabaseMainBundle:PlateWell')->findByIdPlate($idPlate);
        $wellData = array();
        foreach ($results as $r) {
            $row = $r->getRow();
            $col = $r->getCol();
            $orderNumber = $r->getOrderNumber();
            $dayStr = '';
            if ($r->getCreationDate() != '') {
                $diff = $today->getTimestamp() - $r->getCreationDate()->getTimestamp();
                $diff /= 24*60*60;
                $diff = floor($diff);
                if ($diff == 0) {
                    $dayStr = 'Today';
                }
                else {
                    $dayStr = $diff.' day(s)';
                }
            }

            $wellData[$row][$col] = $orderNumber.'<br/>'.$dayStr;
        }
        $view->vars['wellData'] = $wellData;

        $plateName = empty($plate) ? '(No plate selected)' : $plate->getName();
        $view->vars['plateName'] = $plateName;
    }



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        for ($i = 0; $i < 12; $i++) {
            $builder->add('columnSelect_'.$i, 'hidden');
        }
    }
}