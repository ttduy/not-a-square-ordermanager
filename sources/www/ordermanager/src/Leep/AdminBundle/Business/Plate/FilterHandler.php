<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false
        ));
        $builder->add('orderNumber', 'text', array(
            'label'       => 'Order Number',
            'required'    => false
        ));
        $builder->add('idPlateType', 'choice', array(
            'label'       => 'Type',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Plate_Type'),
            'empty_value' => false
        ));
        
        return $builder;
    }
}