<?php
namespace Leep\AdminBundle\Business\Plate;

use Easy\ModuleBundle\Module\AbstractHook;

class SubmitRawResultHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $cacheBox = $controller->get('leep_admin.helper.cache_box_data');

        $data['parseErrors'] = $cacheBox->getKey('parseErrors');
    }
}
