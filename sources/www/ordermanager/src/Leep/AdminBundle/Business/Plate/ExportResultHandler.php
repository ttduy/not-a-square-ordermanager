<?php
namespace Leep\AdminBundle\Business\Plate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class ExportResultHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Plate', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        return array();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAction', 'section', array(
            'label'    => 'Action',
            'required' => false
        ));
        $builder->add('isUpdateResult',     'checkbox', array(
            'label'       => 'Update Result?',
            'required'    => false
        ));
        $builder->add('sectionResult', 'section', array(
            'label'    => 'Result',
            'required' => false
        ));

    }
    public function onSuccess() {
        parent::onSuccess();            
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        if ($model['isUpdateResult']) {
            $exportData = Utils::buildExportData($this->container, $this->entity);
            foreach ($exportData as $d) {
                if ($d['isSame'] === false &&
                    count($d['errors']) == 0) {

                    $r = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findOneBy(array(
                        'idCustomer'     => $d['idCustomerInfo'],
                        'idGene'         => $d['idGene']
                    ));
                    if (!$r) {
                        $r = new Entity\CustomerInfoGene();
                        $r->setIdCustomer($d['idCustomerInfo']);
                        $r->setIdGene($d['idGene']);
                    }
                    $r->setResult($d['geneResult']);
                    $em->persist($r);
                }
            }
            $em->flush();
            $this->messages = array("Result has been exported successfully!");            
        }
    }
}
