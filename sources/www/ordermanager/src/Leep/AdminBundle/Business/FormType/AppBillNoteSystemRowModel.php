<?php
namespace Leep\AdminBundle\Business\FormType;

class AppBillNoteSystemRowModel {
    public $date;
    public $cost;
    public $notes;
    public $idTarget;
    public $text;
    public $idCustomer;
}
