<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppImagePicker extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'app_image_picker';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');
        $view->vars['imageHelperPath'] = $mgr->getUrl('leep_admin', 'report', 'report', 'imageHelper');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
    }
}