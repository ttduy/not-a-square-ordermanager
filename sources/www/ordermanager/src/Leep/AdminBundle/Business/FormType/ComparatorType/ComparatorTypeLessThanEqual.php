<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeLessThanEqual extends AbstractComparatorType {
	public function getLabel() {
		return 'Less than and equal to A';
	}

	public function getOperands() {
		return array('a');
	}

	public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
		$a = $data['a'];

		if ($a) {
	        $queryBuilder->$whereMethod($field . ' <= :comparatorTypeLessThanEqualVal')
	            ->setParameter('comparatorTypeLessThanEqualVal', $a);
		}
	}
}