<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPasswordRow extends AbstractType {
    protected $container;

    public function __construct($container, $defaultDate = '') {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        $options = is_array($options) ? $options : array();

        $options['url_generator'] = array_key_exists('url_generator', $options) ? $options['url_generator'] : '';
        $options['data_class'] = 'Leep\AdminBundle\Business\FormType\AppPasswordRowModel';

        return $options;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_password_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->set('urlGeneratePassword', $options['url_generator']);

        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm (FormBuilderInterface $builder, array $options) {
        $builder->add('password', 'text', array(
            'label'    => (array_key_exists('label', $options) ? $options['label'] : 'Password'),
            'required' => (array_key_exists('required', $options) ? $options['required'] : 'false'),
            'attr'     => (array_key_exists('attr', $options) && !empty($options['attr']) ? $options['attr'] : array('style' => 'width:150px'))
        ));
    }
}