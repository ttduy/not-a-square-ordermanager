<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFeedbackRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppFeedbackRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_feedback_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('fbTimestamp', 'datetimepicker', array(
            'required' => false
        ));
        $builder->add('feedbacker', 'text', array(
            'required' => false
        ));
        $builder->add('idWebUser', 'choice', array(
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('description', 'textarea', array(
            'required' => false,
            'attr'     => array('rows' => 10, 'cols' => 80)
        ));

        // Default Values
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();
        
        $model = new AppFeedbackRowModel();
        $model->idWebUser = $user->getId();
        $builder->setData($model);
    }
}