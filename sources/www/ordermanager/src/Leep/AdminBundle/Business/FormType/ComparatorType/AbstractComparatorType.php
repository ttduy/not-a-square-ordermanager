<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

abstract class AbstractComparatorType {
/*	protected $container;

	function __construct($container) {
		$this->container = $container;
	}
*/
	abstract function getLabel();
	abstract function getOperands();
	abstract function buildQuery(&$queryBuilder, $field, $data, $whereMethod);
}