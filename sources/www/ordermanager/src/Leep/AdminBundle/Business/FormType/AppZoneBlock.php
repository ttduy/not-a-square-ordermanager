<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppZoneBlock extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_zone_block';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('id', 'hidden');
        $builder->add('pageNumber', 'text', array(
            'label'     => 'Page Number',
            'required'  => false,
            'attr'      => array('style' => 'width: 70px')
        ));
        $builder->add('zonesDefinition', 'textarea', array(
            'label'     => 'Zone Definition',
            'required'  => false,
            'attr'      => array('rows' => 8, 'cols' => 150)
        ));
    }
}