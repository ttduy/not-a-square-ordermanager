<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppInvoiceStatusRow extends AbstractType {
    protected $container;
    public $defaultDate = '';
    public function __construct($container, $defaultDate = '') {
        $this->container = $container;
        $this->defaultDate = $defaultDate;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppInvoiceStatusRowModel',
            'statusMapping' => 'LeepAdmin_Invoice_Status'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_invoice_status_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('statusDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false,
            'default_date' => $this->defaultDate
        ));
        $builder->add('status',     'choice', array(
            'label'  => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping($options['statusMapping']),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 2
            )
        ));
    }
}