<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeEqual extends AbstractComparatorType {
	public function getLabel() {
		return 'Equal to A';
	}

	public function getOperands() {
		return array('a');
	}

	public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
		$a = $data['a'];

		if ($a) {
	        $queryBuilder->$whereMethod($field . ' = :comparatorTypeEqualVal')
	            ->setParameter('comparatorTypeEqualVal', $a);
		} else {
	        $queryBuilder->$whereMethod($field . ' IS NULL OR ' . $field . ' = ""');
		}
	}
}