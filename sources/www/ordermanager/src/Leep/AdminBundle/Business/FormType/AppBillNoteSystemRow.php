<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillNoteSystemRow extends AbstractType {
    protected $container;
    public $defaultDate = '';
    public $mapping = '';
    public $target = '';
    public $arrayTarget = [];
    public $customers = [];
    public function __construct($container, $defaultDate = '', $arrayTarget = [], $target = '', $arrayCustomer= []) {
        $this->container = $container;
        $this->defaultDate = $defaultDate;
        $this->arrayTarget = $arrayTarget;
        $this->target = $target;
        $this->customers = $arrayCustomer;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppBillNoteSystemRowModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_bill_note_system_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('date', 'datepicker', array(
            'label' => 'Date',
            'required'  => false,
            'default_date' => $this->defaultDate
        ));
        $builder->add('text', 'text', array(
            'label' => 'Text',
            'required'  => false,
            'attr'  => array(
                'style' => 'width:180px'
            )
        ));
        $builder->add('cost', 'text', array(
            'label' => 'cost',
            'required'  => false,
            'attr'  => array(
                'style' => 'width:150px'
            )
        ));
        $builder->add('idCustomer', 'choice', array(
            'label'  => 'Customer',
            'required' => false,
            'choices'  => $this->customers,
            'attr' => array(
                'style' => 'min-width: 160px; max-width: 160px'
            )
        ));
        $builder->add('idTarget', 'choice', array(
            'label'  => $this->target,
            'required' => false,
            'choices'  => $this->arrayTarget,
            'attr' => array(
                'style' => 'min-width: 220px; max-width: 220px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 30, 'rows' => 2
            )
        ));
    }
}
