<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeNotEqual extends AbstractComparatorType {
	public function getLabel() {
		return 'Not equal to A';
	}

	public function getOperands() {
		return array('a');
	}

	public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
		$a = $data['a'];

		if ($a) {
	        $queryBuilder->$whereMethod($field . ' <> :comparatorTypeNotEqualVal')
	            ->setParameter('comparatorTypeNotEqualVal', $a);
		} else {
	        $queryBuilder->$whereMethod($field . ' IS NOT NULL OR ' . $field . ' <> ""');
		}
	}
}