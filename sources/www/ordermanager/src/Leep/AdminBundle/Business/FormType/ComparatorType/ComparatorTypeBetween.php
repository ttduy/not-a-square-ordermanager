<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeBetween extends AbstractComparatorType {
	public function getLabel() {
		return 'Between A and B';
	}

	public function getOperands() {
		return array('a', 'b');
	}

	public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
		$a = $data['a'];
		$b = $data['b'];

		$whereConditions = array();
		$whereParameters = array();
		
		if ($a) {
			$whereConditions[] = $field . ' >= :comparatorTypeBetweenValA';
			$whereParameters['comparatorTypeBetweenValA'] = $a;
		}

		if ($b) {
			$whereConditions[] = $field . ' <= :comparatorTypeBetweenValB';
			$whereParameters['comparatorTypeBetweenValB'] = $b;
		}

		if (!empty($whereConditions)) {
	        $queryBuilder->$whereMethod(implode(' AND ', $whereConditions));
		}

		foreach ($whereParameters as $key => $val) {
	        $queryBuilder->setParameter($key, $val);
		}
	}
}