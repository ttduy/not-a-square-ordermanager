<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeFactory {
    private $container;

    const EQUAL               = 10;
    const NOT_EQUAL           = 20;
    const LESS_THAN           = 30;
    const LESS_THAN_EQUAL     = 40;
    const GREATER_THAN        = 50;
    const GREATER_THAN_EQUAL  = 60;
    const BETWEEN             = 70;

    function __construct($container) {
        $this->container = $container;
    }

    public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
        $selectedComparatorType = $data['comparatorType'];
        if (array_key_exists($selectedComparatorType, self::getComparatorTypes())) {
            $comparatorType = $this->getComparatorTypes()[$selectedComparatorType];
            $comparatorType->buildQuery($queryBuilder, $field, $data, $whereMethod);
        } 
    }

    public function hasSelectComparatorType($data) {
        return ($data['comparatorType']) ? true : false;
    }

    public static function getComparatorTypes() {
        return array(
            self::EQUAL                 => new ComparatorTypeEqual(),
            self::NOT_EQUAL             => new ComparatorTypeNotEqual(),
            self::LESS_THAN             => new ComparatorTypeLessThan(),
            self::LESS_THAN_EQUAL       => new ComparatorTypeLessThanEqual(),
            self::GREATER_THAN          => new ComparatorTypeGreaterThan(),
            self::GREATER_THAN_EQUAL    => new ComparatorTypeGreaterThanEqual(),
            self::BETWEEN               => new ComparatorTypeBetween()
        );
    }

    public static function getComparationTypeLabels() {
        $arr = array();
        foreach (self::getComparatorTypes() as $key => $val) {
            $arr[$key] = $val->getLabel();
        };

        return $arr;
    }

    public static function getComparationTypeOperands() {
        $arr = array();
        foreach (self::getComparatorTypes() as $key => $val) {
            $arr[$key] = $val->getOperands();
        };

        return $arr;
    }

    public static function getAvailableComparationOperands() {
        $arr = array();

        foreach (self::getComparatorTypes() as $val) {
            $a = array_diff($val->getOperands(), $arr);
            //print_r($a);
            $arr = array_merge($arr, $a);
        };

        usort($arr, array("self", "cmp"));

        return $arr;
    }

    public static function cmp($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }
    
}