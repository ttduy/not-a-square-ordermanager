<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppActivityRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppActivityRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_activity_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('activityDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false
        ));
        $builder->add('activityTimeId', 'choice', array(
            'label'  => 'Time',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Time_List'),
            'attr' => array(
                'style' => 'min-width: 80px; max-width: 80px'
            )
        ));
        $builder->add('activityTypeId',     'choice', array(
            'label'  => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping($options['typeMapping']),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 60, 'rows' => 3
            )
        ));
    }
}