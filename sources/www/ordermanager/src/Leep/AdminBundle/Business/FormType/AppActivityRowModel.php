<?php
namespace Leep\AdminBundle\Business\FormType;

class AppActivityRowModel {
    public $activityDate;
    public $activityTimeId;
    public $activityTypeId;
    public $notes;
}
