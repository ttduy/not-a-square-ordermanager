<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppComplaintRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppComplaintRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_complaint_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        // PROBLEM
        $builder->add('problemTimestamp', 'datetimepicker', array(
            'required' => false
        ));
        $builder->add('idTopic', 'choice', array(
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Complaint_Topic')
        ));
        $builder->add('problemComplaintant', 'text', array(
            'required' => false
        ));
        $builder->add('problemIdWebUser', 'choice', array(
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('problemDescription', 'textarea', array(
            'required' => false,
            'attr'     => array('rows' => 10, 'cols' => 80)
        ));

        // SOLUTION
        $builder->add('solutionTimestamp', 'datetimepicker', array(
            'required' => false
        ));
        $builder->add('solutionIdWebUser', 'choice', array(
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('solutionDescription', 'textarea', array(
            'required' => false,
            'attr'     => array('rows' => 14, 'cols' => 80)
        ));

        // Default Values
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();
        
        $model = new AppComplaintRowModel();
        $model->problemIdWebUser = $user->getId();
        $model->solutionIdWebUser = $user->getId();
        $builder->setData($model);
    }
}