<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppComparatorTypeRow extends AbstractType {
    protected $container;
    protected $arrOperands;

    public function __construct($container) {
        $this->container = $container;
        $this->arrOperands = Business\FormType\ComparatorType\ComparatorTypeFactory::getAvailableComparationOperands();
    }

    public function getDefaultOptions(array $options)
    {
        return array('selectLabel'    => (array_key_exists('selectLabel', $options) ? $options['selectLabel'] : 'Comparator Type'));
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_comparator_type_select';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['comparatorTypeOperands'] = json_encode(Business\FormType\ComparatorType\ComparatorTypeFactory::getComparationTypeOperands());
        $view->vars['availableOperands'] = $this->arrOperands;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('comparatorType', 'choice', array(
            'label' => $options['selectLabel'],
            'choices' => $mapping->getMappingFiltered('LeepAdmin_Comparation_Type_List'),
            'required'  => false,
            'error_bubbling' => false
        ));

        foreach ($this->arrOperands as $entry) {
            $builder->add($entry, 'text', array(
                'label' => strtoupper($entry),
                'required'  => false,
                'attr'  => array('style' => 'width:50px', 'placeholder' => strtoupper($entry))
            ));
        }

    }
}