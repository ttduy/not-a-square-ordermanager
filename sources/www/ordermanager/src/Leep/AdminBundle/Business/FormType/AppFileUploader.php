<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFileUploader extends AbstractType {
    protected $container;
    public $info = '';

    public function __construct($container, $info = null) {
        $this->container = $container;
        $this->info = $info;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'path' => 'logo',
            'isShowViewFile' => true,
            'info' => null
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_file_uploader';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {        
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');
        $view->vars['isShowViewFile'] = $options['isShowViewFile'];
        $view->vars['filePath'] = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'viewFile', array(
            'dir'   => $options['path']
        ));
        $today = new \DateTime();
        $view->vars['timestamp'] = $today->format('YmdHis');
        $view->vars['info'] = ($options['info'] ? $options['info'] : $this->info);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('attachment', 'file', array(
            'label' => 'File Upload',
            'required'  => false
        ));
        $builder->add('file',       'hidden');
        $builder->add('idFile',     'hidden');
        $builder->add('originalName',       'hidden');
    }
}