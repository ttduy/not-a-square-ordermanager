<?php
namespace Leep\AdminBundle\Business\FormType;

class AppBillManualProductRowModel {
    public $date;
    public $price;
    public $text;
    public $description;
    public $customer;
    public $orderNumber;
    public $taxPercentage;
}
