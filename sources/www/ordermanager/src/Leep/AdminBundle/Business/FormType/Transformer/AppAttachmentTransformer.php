<?php
namespace Leep\AdminBundle\Business\FormType\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class AppAttachmentTransformer implements DataTransformerInterface
{
    public $container;
    public $path;

    public function __construct($container, $path) {
        $this->container = $container;
        $this->path = $path;
    }

    // Loading
    public function transform($fileName)
    {
        $attachment = array(
            'attachmentName' => $fileName
        );

        return $attachment;
    }

    // Parsing
    public function reverseTransform($attachment)
    {
        if (!empty($attachment['attachment'])) {
            $attachmentDir = $this->container->getParameter('files_dir').'/'.$this->path;
            $randName = md5(uniqid());

            $name = $randName.'.'.pathinfo($attachment['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $attachment['attachment']->move($attachmentDir, $name);
            return $name;
        }

        return false;
    }
}
