<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppInvoiceTax extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppInvoiceTaxModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_invoice_tax';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   

        $manager = $this->container->get('easy_module.manager');        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('isTax', 'checkbox', array(
            'label'  => 'Is Tax',
            'required' => false
        ));
        $builder->add('taxPercentage', 'text', array(
            'label'  => 'Tax percentage',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
    }
}