<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillManualProductRow extends AbstractType {
    protected $container;
    public $defaultDate = '';
    public $mapping = '';
    public $target = '';
    public $arrayTarget = [];
    public $customers = [];
    public $taxPercentage = 0;
    public $hasTaxPercentage = false;

    public function __construct($container, $defaultDate = '', $hasTaxPercentage = false) {
        $this->container = $container;
        $this->defaultDate = $defaultDate;
        $this->hasTaxPercentage = $hasTaxPercentage;
    }

    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppBillManualProductRowModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_bill_manual_product_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['has_tax_percentage'] = $this->hasTaxPercentage;
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('date', 'datepicker', array(
            'label' => 'Date',
            'required'  => false,
            'default_date' => $this->defaultDate
        ));
        $builder->add('text', 'text', array(
            'label' => 'Product',
            'required'  => false,
            'attr'  => array(
                'style' => 'width:180px'
            )
        ));
        $builder->add('price', 'text', array(
            'label' => 'Price',
            'required'  => false,
            'attr'  => array(
                'style' => 'width:150px'
            )
        ));
        $builder->add('customer', 'text', array(
            'label'  => 'Customer',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 160px; max-width: 160px'
            )
        ));
        $builder->add('orderNumber', 'text', array(
            'label'  => 'Order Number',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 160px; max-width: 160px'
            )
        ));
        $builder->add('taxPercentage', 'text', array(
            'label'  => 'Tax percentage (Used when invoice is with tax)',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 160px; max-width: 160px'
            )
        ));
        $builder->add('description', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 30, 'rows' => 2
            )
        ));
    }
}
