<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppCategoryProductRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_category_product';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['categoryProductLinkJson'] = json_encode(Business\Customer\Util::getCategoryProductLink($this->container));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('categories', 'choice', array(
            'label' => 'Categories',
            'choices' => $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'multiple' => 'multiple',
            'required'  => false,
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px;height:150px',
                'onchange' => 'javascript:onChangeCategories()'
            ),            
        ));
        $builder->add('products', 'choice', array(
            'label' => 'Products',
            'choices' => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'required'  => false,
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px;height:200px',
                'onchange' => 'javascript:onChangeProducts()'
            )
        ));
    }
}