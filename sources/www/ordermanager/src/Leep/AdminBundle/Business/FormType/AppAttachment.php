<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\FormType\Transformer\AppAttachmentTransformer;

class AppAttachment extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'path' => 'logo'
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_attachment';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $attachmentDir = $this->container->getParameter('files_dir').'/'.$options['path'];
        if (!is_dir($attachmentDir)) {
            @mkdir($attachmentDir);
        }

        $mgr = $this->container->get('easy_module.manager');
        $view->vars['attachmentPath'] = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'viewAttachment');
        $view->vars['attachmentSubDir'] = $options['path'];
        $today = new \DateTime();
        $view->vars['timestamp'] = $today->format('YmdHis');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('attachment', 'file', array(
            'label' => 'Attachment',
            'required'  => false
        ));
        $builder->add('attachmentName',       'hidden');

        $builder->addModelTransformer(new AppAttachmentTransformer($this->container, $options['path']));
    }
}