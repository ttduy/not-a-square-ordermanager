<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppCurrencyRate extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppCurrencyRateModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_currency_rate';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $manager = $this->container->get('easy_module.manager');
        $view->vars['getCurrencyRateUrl'] = $manager->getUrl('leep_admin', 'currency', 'feature', 'getCurrencyRate');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('idCurrency', 'choice', array(
            'label'  => 'Currency',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Currency_List'),
            'attr' => array(
                'style' => 'min-width: 80px; max-width: 80px'
            )
        ));
        $builder->add('exchangeRate', 'text', array(
            'label'  => 'Exchange Rate',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 80px; max-width: 80px'
            )
        ));
    }
}
