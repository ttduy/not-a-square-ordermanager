<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppProductQuestionRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\FormType\AppProductQuestionRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_product_question_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('id', 'hidden');
        $builder->add('question', 'text', array(
            'label' => 'Question',
            'required'  => false
        ));
        $builder->add('controlTypeId', 'choice', array(
            'label'  => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_QuestionControlType'),
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('controlConfig',     'textarea', array(
            'label'  => 'Config',
            'required' => false,
            'attr' => array(
                'cols' => 60, 'rows' => 2
            )
        ));
        $builder->add('reportKey', 'text', array(
            'label' => 'Report Key',
            'required'  => false
        ));
    }
}