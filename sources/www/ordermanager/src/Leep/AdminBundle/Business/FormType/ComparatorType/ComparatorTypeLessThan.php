<?php
namespace Leep\AdminBundle\Business\FormType\ComparatorType;

use Leep\AdminBundle\Business;

class ComparatorTypeLessThan extends AbstractComparatorType {
	public function getLabel() {
		return 'Less than A';
	}

	public function getOperands() {
		return array('a');
	}

	public function buildQuery(&$queryBuilder, $field, $data, $whereMethod = "andWhere") {
		$a = $data['a'];

		if ($a) {
	        $queryBuilder->$whereMethod($field . ' < :comparatorTypeLessThanVal')
	            ->setParameter('comparatorTypeLessThanVal', $a);
		}
	}
}