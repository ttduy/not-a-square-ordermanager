<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppGeneResultOptionsRow extends AbstractType {
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        $options = is_array($options) ? $options : array();

        $options['label'] = array_key_exists('label', $options) ? $options['label'] : '';
        $options['idRequested'] = array_key_exists('idRequested', $options) ? $options['idRequested'] : null;
        
        return $options;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_gene_result_options_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->set('genes', Business\CustomerInfo\Utils::getGeneOptions($this->container));

        $view->set('isHasExtraGenes', false);
        $id = $options['idRequested'];
        if ($id) {
            $mgr = $this->container->get('easy_module.manager');
            $view->set('isHasExtraGenes', true);
            $view->set('viewAllGeneUrl', $mgr->getUrl('leep_admin', 'customer_info', 'gene_result_grid', 'list', array('id' => $id)));
        }

        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm (FormBuilderInterface $builder, array $options) {
        // Build gene options
        $geneOptions = Business\CustomerInfo\Utils::getGeneOptions($this->container);
        foreach ($geneOptions as $geneId => $data) {
            $builder->add('geneOption_'.$geneId, 'choice', array(
                'label' => $data['name'],
                'required' => false,
                'choices' => $data['options'],
                'empty_value' => false
            ));
        }
    }
}