<?php
namespace Leep\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppUsernameRow extends AbstractType {
    protected $container;

    public function __construct($container, $defaultDate = '') {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        $options = is_array($options) ? $options : array();

        $options['name_source_field_id'] = array_key_exists('name_source_field_id', $options) ? $options['name_source_field_id'] : '';
        $options['url_generator'] = array_key_exists('url_generator', $options) ? $options['url_generator'] : '';
        $options['data_class'] = 'Leep\AdminBundle\Business\FormType\AppUsernameRowModel';
        
        return $options;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_username_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->set('urlGenerateUsername', $options['url_generator']);
        $view->set('nameSourceFieldId', $options['name_source_field_id']);

        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm (FormBuilderInterface $builder, array $options) {
        
        $builder->add('username', 'text', array(
            'label'    => (array_key_exists('label', $options) ? $options['label'] : 'Username'),
            'required' => (array_key_exists('required', $options) ? $options['required'] : 'false'),
            'attr'     => (array_key_exists('attr', $options) && !empty($options['attr']) ? $options['attr'] : array('style' => 'width:150px'))
        ));
    }
}