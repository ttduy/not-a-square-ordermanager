<?php
namespace Leep\AdminBundle\Business\FormType;

class AppComplaintRowModel {
    public $problemTimestamp;
    public $idTopic;
    public $problemIdWebUser;
    public $problemComplaintant;
    public $problemDescription;

    public $solutionTimestamp;
    public $solutionIdWebUser;
    public $solutionDescription;

    const COMPLAINT_STATUS_RECORDED = 1;
    const COMPLAINT_STATUS_INPROGRESS = 2;
    const COMPLAINT_STATUS_SOLVED = 3;

    public static function getComplaintStatus() {
        return array(
            self::COMPLAINT_STATUS_RECORDED      => 'Recorded',
            self::COMPLAINT_STATUS_INPROGRESS    => 'In progress',
            self::COMPLAINT_STATUS_SOLVED        => 'Solved'
        );
    }
}
