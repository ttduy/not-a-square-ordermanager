<?php
namespace Leep\AdminBundle\Business\CustomerColorFilter;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return array();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $customerFilterBuilder = $this->container->get('leep_admin.customer.business.customer_filter_builder');
        
        $builder->add('sectionColorCode', 'section', array(
            'label'    => 'Color code',
            'property_path' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('colorCode', 'text', array(
            'label'    => 'Color code',
            'required' => false
        ));
        $builder->add('priority', 'text', array(
            'label'    => 'Priority',
            'required' => false
        ));

        $builder->add('sectionFilter', 'section', array(
            'label'    => 'Filter',
            'property_path' => false
        ));
        $customerFilterBuilder->buildForm($builder);

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $customerColorFilter = new Entity\CustomerColorFilter();
        $customerColorFilter->setName($model['name']);
        $customerColorFilter->setColorCode($model['colorCode']);
        $customerColorFilter->setPriority($model['priority']);
        $customerColorFilter->setFilter(json_encode($model));
        $em->persist($customerColorFilter);
        $em->flush();

        parent::onSuccess();
    }
}
