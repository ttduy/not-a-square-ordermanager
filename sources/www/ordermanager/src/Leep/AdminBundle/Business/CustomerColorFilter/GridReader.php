<?php
namespace Leep\AdminBundle\Business\CustomerColorFilter;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'colorCode', 'priority', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',               'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Color code',         'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Priority',      'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CustomerColorFilter', 'p');

    }

    public function buildCellColorCode($row) {
        return '<b style="color: '.$row->getColorCode().'">'.$row->getColorCode().'</b>';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerColorFilterModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer_color_filter', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'customer_color_filter', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}