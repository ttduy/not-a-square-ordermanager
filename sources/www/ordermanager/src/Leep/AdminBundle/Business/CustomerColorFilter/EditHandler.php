<?php
namespace Leep\AdminBundle\Business\CustomerColorFilter;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerColorFilter', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) { 
        $model = json_decode($entity->getFilter(), true);

        $model['name'] = $entity->getName();
        $model['colorCode'] = $entity->getColorCode();
        $model['priority'] = $entity->getPriority();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $customerFilterBuilder = $this->container->get('leep_admin.customer.business.customer_filter_builder');
        
        $builder->add('sectionColorCode', 'section', array(
            'label'    => 'Color code',
            'property_path' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('colorCode', 'text', array(
            'label'    => 'Color code',
            'required' => false
        ));
        $builder->add('priority', 'text', array(
            'label'    => 'Priority',
            'required' => false
        ));

        $builder->add('sectionFilter', 'section', array(
            'label'    => 'Filter',
            'property_path' => false
        ));
        $customerFilterBuilder->buildForm($builder);

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model['name']);
        $this->entity->setColorCode($model['colorCode']);
        $this->entity->setPriority($model['priority']);
        $this->entity->setFilter(json_encode($model));

        parent::onSuccess();
    }
}