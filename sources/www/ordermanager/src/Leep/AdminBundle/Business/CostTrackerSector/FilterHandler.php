<?php
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('txt', 'text', array(
            'label'       => 'Sector Name',
            'required'    => false,
        ));
        
        return $builder;
    }
}