<?php
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->txt = $entity->getTxt();
        $model->sortOrder = $entity->getSortOrder();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        
        $builder->add('txt', 'text', array(
            'label'    => 'Cost Tracker Sector',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
        
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setTxt($model->txt);
        $this->entity->setSortOrder($model->sortOrder);
        
        parent::onSuccess();
    }
}
