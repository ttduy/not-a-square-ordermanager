<?php
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('txt', 'text', array(
            'label'    => 'Cost Tracker Sector',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $sector = new Entity\CostTrackerSector();
        $sector->setTxt($model->txt);
        $sector->setSortOrder($model->sortOrder);

        $em->persist($sector);
        $em->flush();

        parent::onSuccess();
    }
}
