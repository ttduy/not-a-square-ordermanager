<?php
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('txt', 'sortOrder', 'considerFor', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Cost Tracker Sector', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Sort Order', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Consider For', 'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CostTrackerSector', 'p');

        if (trim($this->filters->txt) != '') {
            $queryBuilder->andWhere('p.txt LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->txt).'%');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');        
    }

    public function buildCellConsiderFor($row) {
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        $mgr = $this->getModuleManager();
        $html = "";

        if ($helperSecurity->hasPermission('financeTrackerView')) {
            $htmlChecker = '<input style="width:15px" type="checkbox" id="%s" onChange="%s" %s>&nbsp;%s';
            $arrHtml = array();

            // cost tracker
            $arrHtml[] = sprintf($htmlChecker, "costTrackerChk_" . $row->getId(), "updateIsFor('costTracker', " . $row->getId() . ")", $row->getIsForCostTracker() ? 'checked' : '' ,'Cost Tracker');

            // cash manager
            $arrHtml[] = sprintf($htmlChecker, "cashManagerChk_" . $row->getId(), "updateIsFor('cashManager', " . $row->getId() . ")", $row->getIsForCashManager() ? 'checked' : '' , 'Cash Manager');

            // book keeper
            $arrHtml[] = sprintf($htmlChecker, "bookKeeperChk_" . $row->getId(), "updateIsFor('bookKeeper', " . $row->getId() . ")", $row->getIsForBookKeeper() ? 'checked' : '' , 'Book Keeper');

            // included in total ?
            $arrHtml[] = sprintf($htmlChecker, "isInTotalChk_" . $row->getId(), "updateIsFor('isInTotal', " . $row->getId() . ")", $row->getIsInTotal() ? 'checked' : '' , 'included in total');

            $html = implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $arrHtml);
        }

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('financeTrackerView')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cost_tracker_sector', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cost_tracker_sector', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
