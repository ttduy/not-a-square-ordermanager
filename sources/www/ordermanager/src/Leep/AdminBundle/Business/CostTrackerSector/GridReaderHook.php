<?php
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Easy\ModuleBundle\Module\AbstractHook;

class GridReaderHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
		$mgr = $controller->get('easy_module.manager');
        $data['bulkDeleteUrl'] = $mgr->getUrl('leep_admin', 'cost_tracker_sector', 'feature', 'bulkDelete');
        $data['updateIsForUrl'] = $mgr->getUrl('leep_admin', 'cost_tracker_sector', 'feature', 'updateIsFor');
    }
}
