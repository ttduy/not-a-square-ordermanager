<?php 
namespace Leep\AdminBundle\Business\CostTrackerSector;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $record = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findOneById($id);
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($record);
        $em->flush();
    }
}