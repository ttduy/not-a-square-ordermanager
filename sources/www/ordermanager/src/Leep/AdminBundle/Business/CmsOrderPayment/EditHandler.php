<?php
namespace Leep\AdminBundle\Business\CmsOrderPayment;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CmsOrderPayment', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->timestamp = $entity->getTimestamp();
        $model->idMethod = $entity->getIdMethod();
        $model->amount = $entity->getAmount();
        $model->notes = $entity->getNotes();
        $model->data = $entity->getData();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('idMethod', 'choice', array(
            'label'    => 'Method',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsOrderPayment_Method')
        ));
        $builder->add('amount', 'text', array(
            'label'    => 'Amount',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));
        $builder->add('data', 'textarea', array(
            'label'    => 'Data',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();        
        
        $this->entity->setTimestamp($model->timestamp);
        $this->entity->setIdMethod($model->idMethod);
        $this->entity->setAmount($model->amount);
        $this->entity->setNotes($model->notes);
        $this->entity->setData($model->data);
        $em->persist($this->entity);
        $em->flush();

        Utils::updateCmsOrder($this->container, $this->entity->getIdCmsOrder());

        parent::onSuccess();
    }
}