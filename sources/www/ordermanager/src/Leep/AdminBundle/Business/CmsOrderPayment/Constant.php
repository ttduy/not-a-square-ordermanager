<?php 
namespace Leep\AdminBundle\Business\CmsOrderPayment;

class Constant {
    const CMS_PAYMENT_METHOD_CREDIT_CARD    = 1;
    const CMS_PAYMENT_METHOD_PAYPAL         = 2;

    public static function getCmsOrderPaymentMethod() {
        return array(
            self::CMS_PAYMENT_METHOD_CREDIT_CARD   => 'Credit Card',
            self::CMS_PAYMENT_METHOD_PAYPAL        => 'Paypal'
        );
    }
}