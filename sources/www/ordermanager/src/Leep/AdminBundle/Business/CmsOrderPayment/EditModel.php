<?php
namespace Leep\AdminBundle\Business\CmsOrderPayment;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $timestamp;
    public $idMethod;
    public $amount;
    public $notes;
    public $data;
}
