<?php 
namespace Leep\AdminBundle\Business\CmsOrderPayment;

class Utils {
    public static function updateCmsOrder($container, $idCmsOrder) {
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select("SUM(p.amount) as totalAmount")
            ->from('AppDatabaseMainBundle:CmsOrderPayment', 'p')
            ->andWhere('p.idCmsOrder = :idCmsOrder')
            ->setParameter('idCmsOrder', $idCmsOrder);
        $totalPayment = floatval($query->getQuery()->getSingleScalarResult());

        $cmsOrder = $em->getRepository('AppDatabaseMainBundle:CmsOrder')->findOneById($idCmsOrder);
        $cmsOrder->setTotalPayment($totalPayment);
        $em->persist($cmsOrder);
        $em->flush();
    }

}   