<?php 
namespace Leep\AdminBundle\Business\CmsOrderPayment;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $entry = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CmsOrderPayment')->findOneById($id);
        $idCmsOrder = $entry->getIdCmsOrder();
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($entry);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        Utils::updateCmsOrder($this->container, $idCmsOrder);

        return new RedirectResponse($mgr->getUrl('leep_admin', 'cms_order_payment', 'grid', 'list', array('idParent' => $idCmsOrder)));
    }
}