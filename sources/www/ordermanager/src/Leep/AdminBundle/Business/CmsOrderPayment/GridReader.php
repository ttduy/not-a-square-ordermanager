<?php
namespace Leep\AdminBundle\Business\CmsOrderPayment;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('timestamp', 'idMethod', 'amount', 'notes', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',      'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Method',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Amount',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Notes',          'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',         'width' => '15%', 'sortable' => 'false'),
        );
    }
    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.timestamp';
        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->addOrderBy('p.timestamp', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CmsOrderPaymentGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CmsOrderPayment', 'p');
        $idCmsOrder = $this->container->get('request')->get('idParent', 0);
        $queryBuilder->andWhere('p.idCmsOrder = :idCmsOrder')
            ->setParameter('idCmsOrder', $idCmsOrder);
            
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $idParent = $this->container->get('request')->get('idParent', 0);

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cmsOrderModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cms_order_payment', 'edit', 'edit', array('idParent' => $idParent, 'id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cms_order_payment', 'delete', 'delete', array('idParent' => $idParent, 'id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}