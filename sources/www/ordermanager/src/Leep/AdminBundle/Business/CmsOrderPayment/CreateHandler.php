<?php
namespace Leep\AdminBundle\Business\CmsOrderPayment;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('idMethod', 'choice', array(
            'label'    => 'Method',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsOrderPayment_Method')
        ));
        $builder->add('amount', 'text', array(
            'label'    => 'Amount',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));
        $builder->add('data', 'textarea', array(
            'label'    => 'Data',
            'required' => false,
            'attr'     => array(
                'rows' => 3, 'cols' => 70
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $idCmsOrder = $this->container->get('request')->get('idParent');
        
        $payment = new Entity\CmsOrderPayment();
        $payment->setIdCmsOrder($idCmsOrder);
        $payment->setTimestamp($model->timestamp);
        $payment->setIdMethod($model->idMethod);
        $payment->setAmount($model->amount);
        $payment->setNotes($model->notes);
        $payment->setData($model->data);
        $em->persist($payment);
        $em->flush();

        Utils::updateCmsOrder($this->container, $idCmsOrder);

        parent::onSuccess();
    }
}
