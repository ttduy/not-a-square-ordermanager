<?php
namespace Leep\AdminBundle\Business\ServiceCategory;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public $priceCategoryId;

    public function loadEntity($request) {
        /*$id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CategoryPrices', 'app_main')->findOneById($id);
        */        
        return null;
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        
        $builder->add('name',            'text', array(
            'label' => 'Name',
            'required' => true
        ));
        
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        
        parent::onSuccess();
    }
}
