<?php
namespace Leep\AdminBundle\Business\ProductCategoryPrice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('categoryid', 'price', 'dcmargin', 'partmargin');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Product Category',    'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Price',       'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel Margin',   'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Part Margin', 'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ProductCategoryPriceGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('cat.categoryname', 'ASC');        
    }
    
    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CategoryPrices', 'p')
            ->innerJoin('AppDatabaseMainBundle:ProductCategoriesDnaPlus', 'cat', 'WITH', 'p.categoryid = cat.id')
            ->andWhere('cat.isdeleted = 0')
            ->andWhere('p.pricecatid = :priceCatId');
        $queryBuilder->setParameter('priceCatId', $this->filters->priceCategoryId);
    }

    public function buildCellPrice($row) {
        $edit = $this->container->get('leep_admin.helper.inline_edit');
        $mgr = $this->getModuleManager();

        return $edit->getHtml(
            $row->getPrice(), 
            'price_'.$row->getId(),
            $mgr->getUrl('leep_admin', 'product_category_price', 'ajax_update', 'update', array('field' => 'price', 'categoryId' => $row->getCategoryId(), 'priceCatId' => $row->getPriceCatId()), 'button-edit')
        );
    }
    public function buildCellDcmargin($row) {
        $edit = $this->container->get('leep_admin.helper.inline_edit');
        $mgr = $this->getModuleManager();

        return $edit->getHtml(
            $row->getDCMargin(), 
            'dcmargin_'.$row->getId(),
            $mgr->getUrl('leep_admin', 'product_category_price', 'ajax_update', 'update', array('field' => 'dcmargin', 'categoryId' => $row->getCategoryId(), 'priceCatId' => $row->getPriceCatId()), 'button-edit')
        );
    }
    public function buildCellPartmargin($row) {
        $edit = $this->container->get('leep_admin.helper.inline_edit');
        $mgr = $this->getModuleManager();

        return $edit->getHtml(
            $row->getPartMargin(), 
            'partmargin_'.$row->getId(),
            $mgr->getUrl('leep_admin', 'product_category_price', 'ajax_update', 'update', array('field' => 'partmargin', 'categoryId' => $row->getCategoryId(), 'priceCatId' => $row->getPriceCatId()), 'button-edit')
        );
    }
    /*
    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'service_category', 'edit', 'edit', array('id' => $row->getId())));

        return $builder->getHtml();
    }
    */
}
