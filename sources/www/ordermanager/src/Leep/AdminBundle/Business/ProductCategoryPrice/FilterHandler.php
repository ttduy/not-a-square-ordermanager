<?php
namespace Leep\AdminBundle\Business\ProductCategoryPrice;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        $mapping = $this->container->get('easy_mapping');  
        $categories = $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List');
        foreach ($categories as $k => $v) {
            $model->priceCategoryId = $k;
            break;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('priceCategoryId', 'choice', array(
            'label'       => 'Price Category',
            'required'    => true,
            'choices'     => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List'),
            'empty_value' => false
        ));
        
        return $builder;
    }
}