<?php 
namespace Leep\AdminBundle\Business\ProductCategoryPrice;

use App\Database\MainBundle\Entity;

class AjaxUpdatePrice {
    public function update($controller) {
        $em = $controller->getDoctrine()->getEntityManager();

        $request = $controller->getRequest();
        $categoryId = $request->get('categoryId', 0);
        $priceCatId = $request->get('priceCatId', 0);
        $field = $request->get('field', '');

        $priceCat = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:CategoryPrices', 'app_main')->findOneBy(
            array(
                'categoryid' => $categoryId,
                'pricecatid' => $priceCatId
            )
        );

        // Make initial history
        $categoryPriceHistory = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:CategoryPricesHistory')->findOneBy(
            array(
                'categoryid' => $categoryId,
                'pricecatid' => $priceCatId
            )
        );
        if (empty($categoryPriceHistory)) {
            $categoryPriceHistory = new Entity\CategoryPricesHistory();
            $categoryPriceHistory->setCategoryId($categoryId);
            $categoryPriceHistory->setPriceCatId($priceCatId);
            $categoryPriceHistory->setPrice($priceCat->getPrice());
            $categoryPriceHistory->setDCMargin($priceCat->getDCMargin());
            $categoryPriceHistory->setPartMargin($priceCat->getPartMargin());
            $categoryPriceHistory->setPriceTimestamp(new \DateTime('2001-01-01'));
            $em->persist($categoryPriceHistory);
            $em->flush();
        }

        // Update new price
        $value = $request->get('fieldVal', 0);
        if ($field == 'price') {
            $priceCat->setPrice($value);
        }
        else if ($field == 'dcmargin') {
            $priceCat->setDCMargin($value);
        }
        else if ($field == 'partmargin') {
            $priceCat->setPartMargin($value);
        }
        $em->persist($priceCat);
        $em->flush();

        // Store price history
        $categoryPriceHistory = new Entity\CategoryPricesHistory();
        $categoryPriceHistory->setCategoryId($categoryId);
        $categoryPriceHistory->setPriceCatId($priceCatId);
        $categoryPriceHistory->setPrice($priceCat->getPrice());
        $categoryPriceHistory->setDCMargin($priceCat->getDCMargin());
        $categoryPriceHistory->setPartMargin($priceCat->getPartMargin());
        $categoryPriceHistory->setPriceTimestamp(new \DateTime());
        $em->persist($categoryPriceHistory);
        $em->flush();
    }
}