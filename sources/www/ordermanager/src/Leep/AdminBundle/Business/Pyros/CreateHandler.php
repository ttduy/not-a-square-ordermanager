<?php
namespace Leep\AdminBundle\Business\Pyros;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('genes', 'choice', array(
            'label'    => 'Genes',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Gene_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $pyros = new Entity\Pyros();
        $pyros->setName($model->name);

        $em->persist($pyros);
        $em->flush();

        foreach ($model->genes as $idGene) {
            $pyrosGene = new Entity\PyrosGeneLink();
            $pyrosGene->setPyrosId($pyros->getId());
            $pyrosGene->setGeneId($idGene);
            $em->persist($pyrosGene);
        }
        $em->flush();  

        parent::onSuccess();
    }
}
