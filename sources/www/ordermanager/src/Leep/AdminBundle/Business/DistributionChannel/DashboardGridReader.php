<?php
namespace Leep\AdminBundle\Business\DistributionChannel;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class DashboardGridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('reminderDate', 'idDistributionChannel', 'idMarketingStatus', 'idUser', 'statusDate', 'notes');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Reminder Date',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel',    'width' => '20%', 'sortable' => 'false'),
            array('title' => 'DC Info Status',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'User',                    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status Date',             'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Notes',                   'width' => '10%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_DistributionChannel_Dashboard_GridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $day = $config->getDcMarketingStatusReminderLimitPassedDays();
        if ($day == 0) { $day = 7; }

        $bdate = new \DateTime();
        $bdate->setTimestamp($bdate->getTimestamp() - $day*24*60*60);

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATEDIFF', 'Leep\AdminBundle\Business\Customer\DoctrineExt\DateDiff');

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannelMarketingStatus', 'p')
            ->andWhere('(p.reminderDate IS NOT NULL) AND (p.reminderDate >= :bday)')
            ->setParameter('bday', $bdate);
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.reminderDate', 'ASC');
    }

    public function buildCellReminderDate($row) {
        $html = '--not defined--';
        if ($row->getReminderDate()) {
            $today = new \DateTime();
            $today->setTime(0,0,0);
            $row->getReminderDate()->setTime(0,0,0);
            $interval = date_diff($today, $row->getReminderDate());
            $days = intval($interval->format('%R%a'));

            if ($days == 0) {
                $html = 'Today';
            } else if ($days > 0) {
                $html = 'In %s day(s)';
            } else {
                $html = '%s day(s) ago';
            }

            $html = sprintf($html, abs($days));
        }

        return $html;
    }

    public function buildCellNotes($row) {
        $infoText = $row->getNotes();
        if (!empty($infoText)) {
            return '<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        return '';
    }
}