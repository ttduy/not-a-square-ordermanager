<?php
namespace Leep\AdminBundle\Business\DistributionChannel;
use App\Database\MainBundle\Entity;

class Utils {
    public static function saveEmailTemplates($container, $channelId, $emailTemplates) {
        $em = $container->get('doctrine')->getEntityManager();
        $container->get('leep_admin.helper.database')->delete($em,
            'AppDatabaseMainBundle:DistributionChannelsEmailTemplate',
            array('distributionchannelid' => $channelId)
        );

        $mapping = $container->get('easy_mapping');
        $statusList = $mapping->getMapping('LeepAdmin_Customer_Status');

        foreach ($statusList as $statusId => $statusLabel) {
            $key = 'status_'.$statusId;
            if (isset($emailTemplates[$key])) {
                $dcTemplate = new Entity\DistributionChannelsEmailTemplate();
                $dcTemplate->setDistributionChannelId($channelId);
                $dcTemplate->setStatus($statusId);
                $dcTemplate->setEmailTemplateId($emailTemplates[$key]);
                $em->persist($dcTemplate);
            }
        }
        $em->flush();
    }

    public static function loadEmailTemplates($container, $channelId) {
        $arr = array();
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findBydistributionchannelid($channelId);
        foreach ($result as $r) {
            $arr['status_'.$r->getStatus()] = $r->getEmailTemplateId();
        }
        return $arr;
    }

    public static function copyDC($container, $dcSource) {
        $mgr = $container->get('easy_module.manager');
        $em = $container->get('doctrine')->getEntityManager();
        $distributionChannel = $dcSource;

        // Make copy
        $attachmentKey = $container->get('leep_admin.helper.common')->generateRandAttachmentDir();

        // not copy 'rating'
        $newDistributionChannel = new Entity\DistributionChannels();
        $newDistributionChannel->setCreationDate(new \DateTime());
        $newDistributionChannel->setDistributionChannel($distributionChannel->getDistributionchannel());
        $newDistributionChannel->setDomain($distributionChannel->getDomain());
        $newDistributionChannel->setUidNumber($distributionChannel->getUidNumber());
        $newDistributionChannel->setTypeId($distributionChannel->getTypeId());
        $newDistributionChannel->setType($distributionChannel->getType());
        $newDistributionChannel->setPriceCategoryId($distributionChannel->getPriceCategoryId());
        $newDistributionChannel->setGenderId($distributionChannel->getGenderId());
        $newDistributionChannel->setMale($distributionChannel->getMale());
        $newDistributionChannel->setFemale($distributionChannel->getFemale());
        $newDistributionChannel->setTitle($distributionChannel->getTitle());
        $newDistributionChannel->setFirstName($distributionChannel->getFirstName());
        $newDistributionChannel->setSurname($distributionChannel->getSurname());
        $newDistributionChannel->setInstitution($distributionChannel->getInstitution());
        $newDistributionChannel->setStreet($distributionChannel->getStreet());
        $newDistributionChannel->setPostCode($distributionChannel->getPostCode());
        $newDistributionChannel->setCity($distributionChannel->getCity());
        $newDistributionChannel->setCountryId($distributionChannel->getCountryId());
        $newDistributionChannel->setContactEmail($distributionChannel->getContactEmail());
        $newDistributionChannel->setTelephone($distributionChannel->getTelephone());
        $newDistributionChannel->setFax($distributionChannel->getFax());
        $newDistributionChannel->setStatusDeliveryEmail($distributionChannel->getStatusDeliveryEmail());
        $newDistributionChannel->setReportDeliveryEmail($distributionChannel->getReportDeliveryEmail());
        $newDistributionChannel->setInvoiceDeliveryEmail($distributionChannel->getInvoiceDeliveryEmail());
        $newDistributionChannel->setInvoiceDeliveryNote($distributionChannel->getInvoiceDeliveryNote());
        $newDistributionChannel->setInvoiceGoesToId($distributionChannel->getInvoiceGoesToId());
        $newDistributionChannel->setInvoiceGoesToPartnerId($distributionChannel->getInvoiceGoesToPartnerId());
        $newDistributionChannel->setReportGoesToId($distributionChannel->getReportGoesToId());
        $newDistributionChannel->setReportDeliveryId($distributionChannel->getReportDeliveryId());
        $newDistributionChannel->setFtpServerName($distributionChannel->getFtpServerName());
        $newDistributionChannel->setFtpUserName($distributionChannel->getFtpUserName());
        $newDistributionChannel->setFtpPassword($distributionChannel->getFtpPassword());
        $newDistributionChannel->setAccountName($distributionChannel->getAccountName());
        $newDistributionChannel->setAccountNumber($distributionChannel->getAccountNumber());
        $newDistributionChannel->setBankCode($distributionChannel->getBankCode());
        $newDistributionChannel->setBIC($distributionChannel->getBIC());
        $newDistributionChannel->setIBAN($distributionChannel->getIBAN());
        $newDistributionChannel->setOtherNotes($distributionChannel->getOtherNotes());
        $newDistributionChannel->setAcquisiteurId1($distributionChannel->getAcquisiteurId1());
        $newDistributionChannel->setCommission1($distributionChannel->getCommission1());
        $newDistributionChannel->setYearlyDecayPercentage1($distributionChannel->getYearlyDecayPercentage1());
        $newDistributionChannel->setAcquisiteurId2($distributionChannel->getAcquisiteurId2());
        $newDistributionChannel->setCommission2($distributionChannel->getCommission2());
        $newDistributionChannel->setYearlyDecayPercentage2($distributionChannel->getYearlyDecayPercentage2());
        $newDistributionChannel->setAcquisiteurId3($distributionChannel->getAcquisiteurId3());
        $newDistributionChannel->setCommission3($distributionChannel->getCommission3());
        $newDistributionChannel->setYearlyDecayPercentage3($distributionChannel->getYearlyDecayPercentage3());
        $newDistributionChannel->setWebUserId($distributionChannel->getWebUserId());
        $newDistributionChannel->setWebUsers($distributionChannel->getWebUsers());
        $newDistributionChannel->setPartnerId($distributionChannel->getPartnerId());
        $newDistributionChannel->setRebrandingNick($distributionChannel->getRebrandingNick());
        $newDistributionChannel->setHeaderFileName($distributionChannel->getHeaderFileName());
        $newDistributionChannel->setFooterFileName($distributionChannel->getFooterFileName());
        $newDistributionChannel->setTitleLogoFileName($distributionChannel->getTitleLogoFileName());
        $newDistributionChannel->setBoxLogoFileName($distributionChannel->getBoxLogoFileName());
        $newDistributionChannel->setCompanyInfo($distributionChannel->getCompanyInfo());
        $newDistributionChannel->setLaboratoryInfo($distributionChannel->getLaboratoryInfo());
        $newDistributionChannel->setContactInfo($distributionChannel->getContactInfo());
        $newDistributionChannel->setContactUs($distributionChannel->getContactUs());
        $newDistributionChannel->setLetterExtraText($distributionChannel->getLetterExtraText());
        $newDistributionChannel->setClinicianInfoText($distributionChannel->getClinicianInfoText());
        $newDistributionChannel->setText7($distributionChannel->getText7());
        $newDistributionChannel->setText8($distributionChannel->getText8());
        $newDistributionChannel->setText9($distributionChannel->getText9());
        $newDistributionChannel->setText10($distributionChannel->getText10());
        $newDistributionChannel->setText11($distributionChannel->getText11());
        $newDistributionChannel->setDoctorsReporting($distributionChannel->getDoctorsReporting());
        $newDistributionChannel->setAutomaticReporting($distributionChannel->getAutomaticReporting());
        $newDistributionChannel->setNoReporting($distributionChannel->getNoReporting());
        $newDistributionChannel->setInvoicingAndPaymentInfo($distributionChannel->getInvoicingAndPaymentInfo());
        $newDistributionChannel->setCardTypeId($distributionChannel->getCardTypeId());
        $newDistributionChannel->setCardNumber($distributionChannel->getCardNumber());
        $newDistributionChannel->setCardExpiryDate($distributionChannel->getCardExpiryDate());
        $newDistributionChannel->setCardCvcCode($distributionChannel->getCardCvcCode());
        $newDistributionChannel->setCardFirstName($distributionChannel->getCardFirstName());
        $newDistributionChannel->setCardSurName($distributionChannel->getCardSurName());
        $newDistributionChannel->setInvoiceAddressIsUsed($distributionChannel->getInvoiceAddressIsUsed());
        $newDistributionChannel->setInvoiceAddressStreet($distributionChannel->getInvoiceAddressStreet());
        $newDistributionChannel->setInvoiceAddressPostCode($distributionChannel->getInvoiceAddressPostCode());
        $newDistributionChannel->setInvoiceAddressIdCountry($distributionChannel->getInvoiceAddressIdCountry());
        $newDistributionChannel->setInvoiceAddressTelephone($distributionChannel->getInvoiceAddressTelephone());
        $newDistributionChannel->setInvoiceAddressFax($distributionChannel->getInvoiceAddressFax());
        $newDistributionChannel->setReportColor($distributionChannel->getReportColor());
        $newDistributionChannel->setReportLogo($distributionChannel->getReportLogo());
        $newDistributionChannel->setLatitude($distributionChannel->getLatitude());
        $newDistributionChannel->setLongitude($distributionChannel->getLongitude());
        $newDistributionChannel->setGeocodingStatusCode($distributionChannel->getGeocodingStatusCode());
        $newDistributionChannel->setAccountBalance($distributionChannel->getAccountBalance());
        $newDistributionChannel->setInvoiceClientName($distributionChannel->getInvoiceClientName());
        $newDistributionChannel->setInvoiceCompanyName($distributionChannel->getInvoiceCompanyName());
        $newDistributionChannel->setContactPersonName($distributionChannel->getContactPersonName());
        $newDistributionChannel->setContactPersonEmail($distributionChannel->getContactPersonEmail());
        $newDistributionChannel->setContactPersonTelephoneNumber($distributionChannel->getContactPersonTelephoneNumber());
        $newDistributionChannel->setIsStopShipping($distributionChannel->getIsStopShipping());
        $newDistributionChannel->setIdVisibleGroupReport($distributionChannel->getIdVisibleGroupReport());
        $newDistributionChannel->setIdVisibleGroupProduct($distributionChannel->getIdVisibleGroupProduct());
        $newDistributionChannel->setIdPreferredPayment($distributionChannel->getIdPreferredPayment());
        $newDistributionChannel->setIdDncReportType($distributionChannel->getIdDncReportType());
        $newDistributionChannel->setIdMarginGoesTo($distributionChannel->getIdMarginGoesTo());
        $newDistributionChannel->setIdMarginPaymentMode($distributionChannel->getIdMarginPaymentMode());
        $newDistributionChannel->setIdNutrimeGoesTo($distributionChannel->getIdNutrimeGoesTo());
        $newDistributionChannel->setIdRecipeBookDelivery($distributionChannel->getIdRecipeBookDelivery());
        $newDistributionChannel->setIdRecipeBookGoesTo($distributionChannel->getIdRecipeBookGoesTo());
        $newDistributionChannel->setIsFixedReportGoesTo($distributionChannel->getIsFixedReportGoesTo());
        $newDistributionChannel->setIsInvoiceWithTax($distributionChannel->getIsInvoiceWithTax());
        $newDistributionChannel->setIsPaymentWithTax($distributionChannel->getIsPaymentWithTax());
        $newDistributionChannel->setPaymentNoTaxText($distributionChannel->getPaymentNoTaxText());
        $newDistributionChannel->setIdCompleteStatus($distributionChannel->getIdCompleteStatus());
        $newDistributionChannel->setSupplementInfo($distributionChannel->getSupplementInfo());
        $newDistributionChannel->setAttachmentKey($attachmentKey);
        $newDistributionChannel->setIsCustomerBeContacted($distributionChannel->getIsCustomerBeContacted());
        $newDistributionChannel->setIsNutrimeOffered($distributionChannel->getIsNutrimeOffered());
        $newDistributionChannel->setIsNotContacted($distributionChannel->getIsNotContacted());
        $newDistributionChannel->setIsDestroySample($distributionChannel->getIsDestroySample());
        $newDistributionChannel->setIsFutureResearchParticipant($distributionChannel->getIsFutureResearchParticipant());
        $newDistributionChannel->setSampleCanBeUsedForScience($distributionChannel->getSampleCanBeUsedForScience());
        $newDistributionChannel->setIsShowBodCoverPage($distributionChannel->getIsShowBodCoverPage());
        $newDistributionChannel->setIdLanguage($distributionChannel->getIdLanguage());
        $newDistributionChannel->setCanCreateLoginAndSendStatusMessageToUser($distributionChannel->getCanCreateLoginAndSendStatusMessageToUser());
        $newDistributionChannel->setCanViewShop($distributionChannel->getCanViewShop());
        $newDistributionChannel->setCanCustomersDownloadReports($distributionChannel->getCanCustomersDownloadReports());
        $newDistributionChannel->setDestroyInNumDays($distributionChannel->getDestroyInNumDays());

        $newDistributionChannel->setIsCanViewYourOrder($distributionChannel->getIsCanViewYourOrder());
        $newDistributionChannel->setIsCanViewCustomerProtection($distributionChannel->getIsCanViewCustomerProtection());
        $newDistributionChannel->setIsCanViewRegisterNewCustomer($distributionChannel->getIsCanViewRegisterNewCustomer());
        $newDistributionChannel->setIsCanViewPendingOrder($distributionChannel->getIsCanViewPendingOrder());
        $newDistributionChannel->setIsCanViewSetting($distributionChannel->getIsCanViewSetting());

        $newDistributionChannel->setIsGenerateXmlVersion($distributionChannel->getIsGenerateXmlVersion());
        $newDistributionChannel->setIsDeliveryViaSftp($distributionChannel->getIsDeliveryViaSftp());
        $newDistributionChannel->setSftpHost($distributionChannel->getSftpHost());
        $newDistributionChannel->setSftpUsername($distributionChannel->getSftpUsername());
        $newDistributionChannel->setSftpPassword($distributionChannel->getSftpPassword());
        $newDistributionChannel->setSftpFolder($distributionChannel->getSftpFolder());

        $newDistributionChannel->setCanDownloadViaSftp($distributionChannel->getCanDownloadViaSftp());
        $newDistributionChannel->setSftpDownloadHost($distributionChannel->getSftpDownloadHost());
        $newDistributionChannel->setSftpDownloadUsername($distributionChannel->getSftpDownloadUsername());
        $newDistributionChannel->setSftpDownloadPassword($distributionChannel->getSftpDownloadPassword());
        $newDistributionChannel->setSftpDownloadFolder($distributionChannel->getSftpDownloadFolder());

        $newDistributionChannel->setIdRecallGoesTo($distributionChannel->getIdRecallGoesTo());

        $newDistributionChannel->setIsReportDeliveryDownloadAccess($distributionChannel->getIsReportDeliveryDownloadAccess());
        $newDistributionChannel->setIsReportDeliveryFtpServer($distributionChannel->getIsReportDeliveryFtpServer());
        $newDistributionChannel->setIsReportDeliveryPrintedBooklet($distributionChannel->getIsReportDeliveryPrintedBooklet());
        $newDistributionChannel->setIsReportDeliveryDontChargeBooklet($distributionChannel->getIsReportDeliveryDontChargeBooklet());

        $newDistributionChannel->setWebLoginGoesTo($distributionChannel->getWebLoginGoesTo());
        $newDistributionChannel->setIsWebLoginOptOut($distributionChannel->getIsWebLoginOptOut());

        $em->persist($newDistributionChannel);
        $em->flush();

        // Copy report
        if ($distributionChannel->getReportLogo() != '' ) {
            $logoDir = $container->getParameter('kernel.root_dir').'/../web/attachments/report_resource/DistributionChannels';

            $newLogo = $newDistributionChannel->getId().'.jpg';
            $newDistributionChannel->setReportLogo($newLogo);

            @copy($logoDir.'/'.$distributionChannel->getReportLogo(), $logoDir.'/'.$newLogo);
        }
        $em->flush();

        // Copy file
        $dir = $container->getParameter('attachment_dir').'/distribution_channels';
        @mkdir($dir.'/'.$newDistributionChannel->getAttachmentKey());
        if ($handle = @opendir($dir.'/'.$distributionChannel->getAttachmentKey())) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry != '.tmb') {
                    $originalFile = $dir.'/'.$distributionChannel->getAttachmentKey().'/'.$entry;
                    $targetFile = $dir.'/'.$newDistributionChannel->getAttachmentKey().'/'.$entry;

                    @copy($originalFile, $targetFile);
                }
            }
        }

        // Copy DC Email Template
        $dcEmailTemplates = $em->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findBydistributionchannelid($distributionChannel->getId());
        foreach ($dcEmailTemplates as $dcEmailTemplate) {
            $newDcEmailTemplate = new Entity\DistributionChannelsEmailTemplate();
            $newDcEmailTemplate->setDistributionChannelId($newDistributionChannel->getId());
            $newDcEmailTemplate->setStatus($dcEmailTemplate->getStatus());
            $newDcEmailTemplate->setEmailTemplateId($dcEmailTemplate->getEmailTemplateId());
            $em->persist($newDcEmailTemplate);
        }
        $em->flush();

        return $newDistributionChannel;
    }

    public static function applyFilter($queryBuilder, $filters) {
        if (trim($filters->domain) != '') {
            $queryBuilder->andWhere('p.domain LIKE :domain')
                ->setParameter('domain', '%'.trim($filters->domain).'%');
        }
        if (trim($filters->distributionChannel) != '') {
            $queryBuilder->andWhere('p.distributionchannel LIKE :distributionChannel')
                ->setParameter('distributionChannel', '%'.trim($filters->distributionChannel).'%');
        }
        if (trim($filters->uidNumber) != '') {
            $queryBuilder->andWhere('p.uidnumber LIKE :uidNumber')
                ->setParameter('uidNumber', '%'.trim($filters->uidNumber).'%');
        }
        if ($filters->typeId != 0) {
            $queryBuilder->andWhere('p.typeid = :typeId')
                ->setParameter('typeId', $filters->typeId);
        }
        if ($filters->priceCategoryId != 0) {
            $queryBuilder->andWhere('p.pricecategoryid = :priceCategoryId')
                ->setParameter('priceCategoryId', $filters->priceCategoryId);
        }
        if ($filters->genderId != 0) {
            $queryBuilder->andWhere('p.genderid = :genderId')
                ->setParameter('genderId', $filters->genderId);
        }
        if (trim($filters->name) != '') {
            $queryBuilder->andWhere('(p.firstname LIKE :name) OR (p.surname LIKE :name)')
                ->setParameter('name', '%'.trim($filters->name).'%');
        }
        if (trim($filters->idCode) != '') {
            $queryBuilder->andWhere('p.idCode LIKE :idCode')
                ->setParameter('idCode', '%'.trim($filters->idCode).'%');
        }
        if (trim($filters->accountingCode) != '') {
            $queryBuilder->andWhere('p.accountingCode = :accountingCode')
                ->setParameter('accountingCode',$filters->accountingCode);
        }
        if (trim($filters->institution) != '') {
            $queryBuilder->andWhere('p.institution LIKE :institution')
                ->setParameter('institution', '%'.trim($filters->institution).'%');
        }
        if ($filters->complaintStatus != 0) {
            if ($filters->complaintStatus == -1) {
                $queryBuilder->andWhere('p.complaintStatus > 0');
            }
            else {
                $queryBuilder->andWhere('p.complaintStatus = :complaintStatus')
                    ->setParameter('complaintStatus', $filters->complaintStatus);
            }
        }
        if ($filters->complaintTopic != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:DistributionChannelsComplaint', 'dcc', 'WITH', 'p.id = dcc.idDistributionChannel')
                ->andWhere('dcc.idTopic = '.$filters->complaintTopic);
        }
        if ($filters->positiveFeedback != 0) {
            $queryBuilder->andWhere('p.numberFeedback > 0');
        }
        if ($filters->idStopShipping != 0) {
            if ($filters->idStopShipping == 1) {
                $queryBuilder->andWhere('p.isStopShipping = 1');
            }
            else {
                $queryBuilder->andWhere('(p.isStopShipping IS NULL) OR (p.isStopShipping = 0)');
            }
        }

        if ($filters->idCompleteStatus != 0) {
            $queryBuilder->andWhere('p.idCompleteStatus = :idCompleteStatus')
                ->setParameter('idCompleteStatus', $filters->idCompleteStatus);
        }
        if ($filters->idInvoiceGoesTo != 0) {
            $queryBuilder->andWhere('p.invoicegoestoid = :idInvoiceGoesTo')
                ->setParameter('idInvoiceGoesTo', $filters->idInvoiceGoesTo);
        }

        if ($filters->idAcquisiteur != 0) {
            $queryBuilder->andWhere('(p.acquisiteurid1 = :idAcquisiteur) OR (p.acquisiteurid2 = :idAcquisiteur) OR (p.acquisiteurid3 = :idAcquisiteur)')
                ->setParameter('idAcquisiteur', $filters->idAcquisiteur);
        }
        if ($filters->idPartner) {
            $queryBuilder->andWhere('(p.partnerid = :idPartner)')
                ->setParameter('idPartner', $filters->idPartner);
        }
        if ($filters->rating != 0) {
            $queryBuilder->andWhere('p.rating = :rating')
                ->setParameter('rating', $filters->rating);
        }
        if ($filters->idCountry != 0) {
            $queryBuilder->andWhere('p.countryid = :idCountry')
                ->setParameter('idCountry', $filters->idCountry);
        }
        if ($filters->idLanguage != 0) {
            $queryBuilder->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('idLanguage', $filters->idLanguage);
        }
    }

    public static function getDistributionChannelsList($container)
    {
        $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p.id, p.distributionchannel')
              ->from("AppDatabaseMainBundle:DistributionChannels", 'p');
        $queryResult = $query->getQuery()->getResult();
        $dcList = [];
        foreach ($queryResult as $qr) {
            $dcList[$qr['id']] = $qr['distributionchannel'];
        }
        return $dcList;
    }
}
