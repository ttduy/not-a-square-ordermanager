<?php
namespace Leep\AdminBundle\Business\DistributionChannel\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppMarketingStatusRow extends AbstractType {
    protected $container;
    public $defaultDate = '';
    public function __construct($container, $defaultDate = '') {
        $this->container = $container;
        $this->defaultDate = $defaultDate;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\DistributionChannel\Form\AppMarketingStatusRowModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'marketing_status_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $view->vars['idUser'] = $userManager->getUser()->getId();
        
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('statusDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false,
            'default_date' => $this->defaultDate
        ));
        $builder->add('idMarketingStatus',     'choice', array(
            'label'  => 'DC Info Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarketingStatus'),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 80, 'rows' => 7
            )
        ));
        $builder->add('reminderDate', 'datepicker', array(
            'label'  => 'Reminder Date',
            'required' => false
        ));
        $builder->add('idUser', 'choice', array(
            'label'  => 'User',
            'choices'   => $mapping->getMapping('LeepAdmin_WebUser_List'),
            'required' => false
        ));  
    }
}