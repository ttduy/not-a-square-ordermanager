<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('distributionChannel', 'uidNumber', 'weblogin', 'domain', 'accountingCode', 'idCode', 'typeId', 'priceCategoryId', 'complaints', 'name', 'institution', 'partnerId', 'accountBalance', 'marketingStatus', 'rating', 'otherNotes', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Distribution Channel', 'width' => '8%', 'sortable' => 'true'),
            array('title' => 'UID Number',           'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Web Login',            'width' => '6%', 'sortable' => 'false'),
            array('title' => 'Domain',               'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Accounting Code',      'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Id Code',              'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Type',                 'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Price Category',       'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Complaints',           'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Name',                 'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Institution',          'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Partner',              'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Balance',              'width' => '8%', 'sortable' => 'true'),
            array('title' => 'DC Info Status',       'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Rating',               'width' => '4%', 'sortable' => 'true'),
            array('title' => 'Notes',                'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Action',               'width' => '8%', 'sortable' => 'false'),
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.distributionchannel';
        $this->columnSortMapping[] = 'p.uidnumber';
        $this->columnSortMapping[] = 'p.domain';
        $this->columnSortMapping[] = 'p.typeid';
        $this->columnSortMapping[] = 'p.pricecategoryid';
        $this->columnSortMapping[] = 'p.complaintStatus';
        $this->columnSortMapping[] = 'p.firstname';
        $this->columnSortMapping[] = 'p.institution';
        $this->columnSortMapping[] = 'p.partnerid';
        $this->columnSortMapping[] = 'p.accountBalance';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_DistributionChannelGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        Utils::applyFilter($queryBuilder, $this->filters);
    }

    public function buildCellWebLogin($row) {
        $formater = $this->container->get('leep_admin.helper.formatter');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '%s %s';

        // prepare status
        $strPwdStatus = '';
        if ($row->getWebLoginPassword() != '' && $row->getWebLoginUsername() != '') {
            $strPwdStatus = $row->getWebLoginUsername();
        }

        // button for display info
        $buttonHtml = '';
        $builder->addPopupButton('Send email', $mgr->getUrl('leep_admin', 'distribution_channel', 'email', 'create', array('id' => $row->getId())), 'button-email');

        // add button login to customer dashboard
        $builder->addNewTabButton('Login Customer Dashboard', $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'loginCustomerDashboard', array('id' => $row->getId())), 'button-login-cd');

        $buttonHtml = $builder->getHtml();

        return sprintf($html, $strPwdStatus, "<span>$buttonHtml</span>");
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'distribution_channel', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        // // add popup to statistics
        // $builder->addPopupButton('View Statistics', 
        //                             $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'StatisticsId', 
        //                             array('id' => $row->getId())), 'button-activity');

        $builder->addPopupButton('Manage genes',
                                    $mgr->getUrl('leep_admin', 'distribution_channel', 'feature', 'PopUpDC',
                                    array('id'  => $row->getId())), 'button-login-cd');
        // //               

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'distribution_channel', 'copy', 'edit', array('id' => $row->getId())), 'button-copy');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'distribution_channel', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellMarketingStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Update Marketing Status', $mgr->getUrl('leep_admin', 'distribution_channel', 'edit_marketing_status', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        // get the number of DC Info Status
        $count = 10;
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('count(p.id)')
            ->from('AppDatabaseMainBundle:DistributionChannelMarketingStatus', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $row->getId());

        $count = $query->getQuery()->getSingleScalarResult();
        return '('.$count.')'.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellOtherNotes($row) {
        $infoText = $row->getOtherNotes();
        if (!empty($infoText)) {
            return '<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        return '';
    }

    public function buildCellComplaints($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_ComplaintStatus', $row->getComplaintStatus());

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel', 'edit_complaint', 'edit', array('id' => $row->getId())), 'button-edit');

        $html = $builder->getHtml().'&nbsp;&nbsp;'.$title;

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel', 'edit_feedback', 'edit', array('id' => $row->getId())), 'button-checked');
        $html .= '<br/>'.$builder->getHtml().'&nbsp;&nbsp'.intval($row->getNumberFeedback()).' feedback(s)';

        return $html;
    }

    public function buildCellAccountBalance($row) {
        $html = '';
        $balance = $row->getAccountBalance();
        if ($balance < 0) {
            $html = 'MINUS &euro;'.number_format(-$balance, 2);
        }
        else if ($balance > 0) {
            $html .= 'PLUS &euro;'.number_format($balance, 2);
        }
        else {
            $html .= '&euro;0.00';
        }

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'distribution_channel_account_entry', 'grid', 'list', array('idParent' => $row->getId())), 'button-edit');

        $html .= '&nbsp;&nbsp;'.$builder->getHtml();

        return $html;
    }

    public function buildCellDistributionChannel($row) {
        $name = $row->getDistributionChannel();

        if ($row->getIsStopShipping()) {
            $name .= '&nbsp;&nbsp;<div class="button-delete"></div>';
        }

        return $name;
    }

    public function buildCellRating($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $mapping = $this->container->get('easy_mapping');

        $html = array();

        if ($row->getRating()) {
            $html[] = $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $row->getRating());
        }

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelView')) {
            $builder->addPopupButton('View History', $mgr->getUrl('leep_admin', 'rating', 'feature', 'dcRatingHistory', array('id' => $row->getId())), 'button-detail');
            $html[] = $builder->getHtml();
        }

        return implode('&nbsp;&nbsp;', $html);
    }
}
