<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

class Constant {
    const DC_RATING_A  = 10;
    const DC_RATING_B  = 20;
    const DC_RATING_C  = 30;
    const DC_RATING_D  = 40;
    const DC_RATING_E  = 50;
    public static function getDCRatings() {
        return array(
            self::DC_RATING_A => 'A',
            self::DC_RATING_B => 'B',
            self::DC_RATING_C => 'C',
            self::DC_RATING_D => 'D',
            self::DC_RATING_E => 'E'
        );
    }

    const INVOICE_GOES_TO_CUSTOMER = 1;
    const INVOICE_GOES_TO_CHANNEL  = 2;
    const INVOICE_GOES_TO_PARTNER  = 3;
    public static function getInvoiceGoesTo() {
        return array(
            self::INVOICE_GOES_TO_CUSTOMER => 'Customer',
            self::INVOICE_GOES_TO_CHANNEL  => 'Channel',
            self::INVOICE_GOES_TO_PARTNER  => 'Partner'
        );
    }

    const REPORT_GOES_TO_PARTNER  = 1;
    const REPORT_GOES_TO_CUSTOMER = 2;
    const REPORT_GOES_TO_CHANNEL  = 3;
    public static function getReportGoesTo() {
        return array(
            self::REPORT_GOES_TO_PARTNER    => 'Partner',
            self::REPORT_GOES_TO_CUSTOMER   => 'Customer',
            self::REPORT_GOES_TO_CHANNEL    => 'Channel',
        );
    }

    const REPORT_DELIVERY_EMAIL                     = 1;
    //const REPORT_DELIVERY_PRINT_AND_POST          = 2;  --> Convert to Print (4)
    const REPORT_DELIVERY_FTP_SERVER                = 3;
    const REPORT_DELIVERY_PRINT                     = 4;
    const REPORT_DELIVERY_PRINT_AND_EMAIL           = 5;
    const REPORT_DELIVERY_PRINT_NO_CHARGE           = 6;
    const REPORT_DELIVERY_PRINT_AND_EMAIL_NO_CHARGE = 7;


    public static function getReportDelivery() {
        return array(
            self::REPORT_DELIVERY_EMAIL                         => "Email",
            self::REPORT_DELIVERY_FTP_SERVER                    => "FTP Server",
            self::REPORT_DELIVERY_PRINT                         => "Print",
            self::REPORT_DELIVERY_PRINT_AND_EMAIL               => "Print and email",
            self::REPORT_DELIVERY_PRINT_NO_CHARGE               => "Print - No charge",
            self::REPORT_DELIVERY_PRINT_AND_EMAIL_NO_CHARGE     => "Print and email - No charge"
        );
    }
    public static function geyReportDeliveryTranslation() {
        return array(
            self::REPORT_DELIVERY_EMAIL                       => 'REPORT_DELIVERY_EMAIL',
            self::REPORT_DELIVERY_FTP_SERVER                  => 'REPORT_DELIVERY_FTP_SERVER',
            self::REPORT_DELIVERY_PRINT                       => 'REPORT_DELIVERY_PRINT',
            self::REPORT_DELIVERY_PRINT_AND_EMAIL             => 'REPORT_DELIVERY_PRINT_AND_EMAIL',
            self::REPORT_DELIVERY_PRINT_NO_CHARGE             => 'REPORT_DELIVERY_PRINT_NO_CHARGE',
            self::REPORT_DELIVERY_PRINT_AND_EMAIL_NO_CHARGE   => 'REPORT_DELIVERY_PRINT_AND_EMAIL_NO_CHARGE',
        );
    }

    const PREFERRED_PAYMENT_INVOICE        = 1;
    const PREFERRED_PAYMENT_CREDIT_CARD    = 2;
    const PREFERRED_PAYMENT_DIRECT_DEBIT   = 3;
    public static function getPreferredPayment() {
        return array(
            self::PREFERRED_PAYMENT_INVOICE        => 'Invoice (Bank transfer and paypal)',
            self::PREFERRED_PAYMENT_CREDIT_CARD    => 'Credit card',
            self::PREFERRED_PAYMENT_DIRECT_DEBIT   => 'Direct Debit (Lastschrift)'
        );
    }

    const REPORT_NAME_TYPE_DEFAULT              = 0;
    const REPORT_NAME_TYPE_WITH_CUSTOMER_NAME   = 1;
    public static function getReportNameTypes() {
        return array(
            self::REPORT_NAME_TYPE_WITH_CUSTOMER_NAME  => 'Extended (with customer name)'
        );
    }

    const MARGIN_GOES_TO_PARNER                        = 1;
    const MARGIN_GOES_TO_DC                            = 2;
    const MARGIN_GOES_TO_PARTNER_AND_DC                = 3;
    const MARGIN_GOES_TO_BOTH_TO_PARTNER               = 4;
    const MARGIN_GOES_TO_BOTH_TO_DC                    = 5;
    const MARGIN_GOES_TO_NO_MARGIN                     = 6;
    public static function getMarginGoesTo() {
        return array(
            self::MARGIN_GOES_TO_PARNER                => 'Margin goes to Partner',
            self::MARGIN_GOES_TO_DC                    => 'Margin goes to Distribution channel',
            self::MARGIN_GOES_TO_PARTNER_AND_DC        => 'Margin goes to Partner and Distribution channel',
            self::MARGIN_GOES_TO_BOTH_TO_PARTNER       => 'Both Margins go to Partner',
            self::MARGIN_GOES_TO_BOTH_TO_DC            => 'Both Margins go to DC',
            self::MARGIN_GOES_TO_NO_MARGIN             => 'No Margin'
        );
    }

    const NUTRIME_GOES_TO_CUSTOMER                      = 1;
    const NUTRIME_GOES_TO_DISTRIBUTION_CHANNEL          = 2;
    const NUTRIME_GOES_TO_PARTNER                       = 3;
    public static function getNutrimeGoesTo() {
        return array(
            self::NUTRIME_GOES_TO_CUSTOMER              => 'Customer',
            self::NUTRIME_GOES_TO_DISTRIBUTION_CHANNEL  => 'Channel',
            self::NUTRIME_GOES_TO_PARTNER               => 'Partner',
        );
    }

    const COMPLETE_STATUS_INCOMPLETE                   = 1;
    const COMPLETE_STATUS_COMPLETE                     = 2;
    public static function getCompleteStatus() {
        return array(
            self::COMPLETE_STATUS_INCOMPLETE           => 'Incomplete',
            self::COMPLETE_STATUS_COMPLETE             => 'Complete'
        );
    }


    const MARGIN_PAYMENT_MODE_NONE         = 1;
    const MARGIN_PAYMENT_MODE_CREDIT_NOTES = 2;
    const MARGIN_PAYMENT_MODE_INVOICE      = 3;
    public static function getMarginPaymentModes() {
        return array(
            self::MARGIN_PAYMENT_MODE_NONE             => 'None',
            self::MARGIN_PAYMENT_MODE_CREDIT_NOTES     => 'Credit notes',
            self::MARGIN_PAYMENT_MODE_INVOICE          => 'Invoice'
        );
    }


    const RECALL_GOES_TO_PARTNER              = 1;
    const RECALL_GOES_TO_DISTRIBUTION_CHANNEL = 2;
    const RECALL_GOES_TO_CUSTOMER             = 3;
    public static function getRecallGoesTo() {
        return array(
            self::RECALL_GOES_TO_PARTNER                => 'Partner',
            self::RECALL_GOES_TO_DISTRIBUTION_CHANNEL   => 'Distribution Channel',
            self::RECALL_GOES_TO_CUSTOMER               => 'Customer'
        );
    }
}
