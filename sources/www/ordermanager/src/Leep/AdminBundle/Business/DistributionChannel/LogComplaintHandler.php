<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;


class LogComplaintHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {         
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $model = new LogComplaintModel(); 
        $model->idUser = $userManager->getUser()->getId();
        $model->solutionIdUser = $userManager->getUser()->getId();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionProblem', 'section', array(
            'label'    => 'Problem',
            'property_path' => false
        ));
        $builder->add('idTopic', 'choice', array(
            'label'    => 'Topic',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Complaint_Topic')
        ));
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('complainant', 'text', array(
            'label'    => 'Complainant',
            'required' => false
        ));
        $builder->add('idUser', 'choice', array(
            'label'    => 'User',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array('rows' => 10, 'cols' => 80)
        ));


        $builder->add('sectionSolution', 'section', array(
            'label'    => 'Solution',
            'property_path' => false
        ));
        $builder->add('solutionTimestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('solutionIdUser', 'choice', array(
            'label'    => 'User',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('solutionDescription', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array('rows' => 10, 'cols' => 80)
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('MAX(p.sortOrder) as sortOrder')
            ->from('AppDatabaseMainBundle:DistributionChannelsComplaint', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $this->entity->getId());
        $currentSortOrder = intval($query->getQuery()->getSingleScalarResult());

        $complaint = new Entity\DistributionChannelsComplaint();
        $complaint->setIdDistributionChannel($this->entity->getId());
        $complaint->setIdTopic($model->idTopic);
        $complaint->setProblemTimestamp($model->timestamp);
        $complaint->setProblemIdWebUser($model->idUser);
        $complaint->setProblemComplaintant($model->complainant);
        $complaint->setProblemDescription($model->description);
        $complaint->setSolutionTimestamp($model->solutionTimestamp);
        $complaint->setSolutionIdWebUser($model->solutionIdUser);
        $complaint->setSolutionDescription($model->solutionDescription);
        $complaint->setSortOrder($currentSortOrder + 1);
        $em->persist($complaint);
        $em->flush();

        $this->entity->setComplaintStatus(Business\FormType\AppComplaintRowModel::COMPLAINT_STATUS_RECORDED);
        $em->flush();
        parent::onSuccess();
    }
}