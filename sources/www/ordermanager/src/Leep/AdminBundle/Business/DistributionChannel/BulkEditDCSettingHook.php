<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class BulkEditDCSettingHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idPartner = $controller->get('request')->get('id', 0);
        $session = $controller->get('session');
        $page = $session->get('page', 1);
        $idPartner = intval($idPartner);
        $arrDC = array();
        $query = $em->createQueryBuilder();
        $queryCount = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        $queryCount->select('count(p)')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);
            $queryCount->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);

            $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($idPartner);
            $data['partner'] = $partner;
        }
        $result = $query->getQuery()->setFirstResult(($page-1)*100)->setMaxResults(100)->getResult();
        $countAll = $queryCount->getQuery()->getSingleScalarResult();
        foreach ($result as $dc) {
            $arrDC[$dc->getId()] = $dc->getDistributionchannel();
        }
        $data['dcList'] = $arrDC;

        // Partners
        $partners = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Partner')->findBy(array(), array('partner' => 'ASC'));
        foreach ($result as $r) {
            $partners[$r->getId()] = $r->getPartner();
        }
        $data['partners'] = $partners;
        $data['idPartner'] = $idPartner;
        $data['curPage'] = $page;
        $data['totalPage'] = floor($countAll/100);
        $data['leftCurPage'] = ($page -2 > 0) ? $page -2  : 1;
        $data['rightCurPage'] = ($page + 3 <= floor($countAll/100)) ? $page + 3  : floor($countAll/100);
        $data['entriesNow'] = ($page*100 <= $countAll) ? $page*100 : $countAll;
        $data['entriesNow'] = ($page == 1) ? 1 : $data['entriesNow'];
        $data['entriesTo'] = (($page*100 +100) < $countAll) ? ($page*100 +100) : $countAll ;
        $data['allEntries'] = $countAll;
        if ($page == 1 && ($page + 4 <= floor($countAll/100))) {
            $data['rightCurPage'] = $page +4;
        }
        if (floor($countAll/100) == 0) {
            $data['rightCurPage'] = 1;
            $data['leftCurPage'] = 1;
        }
    }
}
