<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public $attachmentKey = false;
    public $id;
    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if ($this->attachmentKey == '') {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();

        if ($entity->getAttachmentKey() == '') {
            $this->generateAttachmentKey();
            $model->attachmentKey = $this->attachmentKey;
            $this->entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }

        $model->container = $this->container;

        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();

        $model->id = $entity->getId();

        $webLoginUsername = new Business\FormType\AppUsernameRowModel();
        $webLoginUsername->username = $entity->getWebLoginUsername();
        $model->webLoginUsername = $webLoginUsername;

        $webLoginPassword = new Business\FormType\AppPasswordRowModel();
        $webLoginPassword->password = $entity->getWebLoginPassword();
        $model->webLoginPassword = $webLoginPassword;

        $model->webLoginStatus = $entity->getWebLoginStatus();
        $model->webLoginGoesTo = $entity->getWebLoginGoesTo();
        $model->isWebLoginOptOut = $entity->getIsWebLoginOptOut();

        $model->isCustomerBeContacted = $entity->getIsCustomerBeContacted();
        $model->isNutrimeOffered = $entity->getIsNutrimeOffered();
        $model->isNotContacted = $entity->getIsNotContacted();
        $model->isDestroySample = $entity->getIsDestroySample();
        $model->isFutureResearchParticipant = $entity->getIsFutureResearchParticipant();
        $model->sampleCanBeUsedForScience = $entity->getSampleCanBeUsedForScience();

        $model->domain = $entity->getDomain();
        $model->distributionChannel = $entity->getDistributionChannel();
        $model->creationDate = $entity->getCreationDate();
        $model->uidNumber = $entity->getUidNumber();
        $model->genderId = $entity->getGenderId();
        $model->typeId = $entity->getTypeId();
        $model->priceCategoryId = $entity->getPriceCategoryId();
        $model->title = $entity->getTitle();
        $model->firstName = $entity->getFirstName();
        $model->surName = $entity->getSurName();
        $model->institution = $entity->getInstitution();
        $model->idCompleteStatus = $entity->getIdCompleteStatus();
        $model->supplementInfo = $entity->getSupplementInfo();
        $model->idCurrency = $entity->getIdCurrency();

        $model->street = $entity->getStreet();
        $model->postCode = $entity->getPostCode();
        $model->city = $entity->getCity();
        $model->countryId = $entity->getCountryId();
        $model->contactEmail = $entity->getContactEmail();
        $model->telephone = $entity->getTelephone();
        $model->fax = $entity->getFax();

        $model->invoiceAddressIsUsed = $entity->getInvoiceAddressIsUsed();
        $model->invoiceClientName = $entity->getInvoiceClientName();
        $model->invoiceCompanyName = $entity->getInvoiceCompanyName();
        $model->invoiceAddressStreet = $entity->getInvoiceAddressStreet();
        $model->invoiceAddressPostCode = $entity->getInvoiceAddressPostCode();
        $model->invoiceAddressCity = $entity->getInvoiceAddressCity();
        $model->invoiceAddressIdCountry = $entity->getInvoiceAddressIdCountry();
        $model->invoiceAddressTelephone = $entity->getInvoiceAddressTelephone();
        $model->invoiceAddressFax = $entity->getInvoiceAddressFax();
        $model->isInvoiceWithTax = $entity->getIsInvoiceWithTax();

        $model->contactPersonName = $entity->getContactPersonName();
        $model->contactPersonEmail = $entity->getContactPersonEmail();
        $model->contactPersonTelephoneNumber = $entity->getContactPersonTelephoneNumber();

        $model->statusDeliveryEmail = $entity->getStatusDeliveryEmail();
        $model->reportDeliveryEmail = $entity->getReportDeliveryEmail();
        $model->invoiceDeliveryEmail = $entity->getInvoiceDeliveryEmail();
        $model->invoiceDeliveryNote = $entity->getInvoiceDeliveryNote();

        $model->invoiceGoesToId = $entity->getInvoiceGoesToId();
        $model->reportGoesToId = $entity->getReportGoesToId();
        $model->isFixedReportGoesTo = $entity->getIsFixedReportGoesTo();
        $model->reportDeliveryId = $entity->getReportDeliveryId();

        $model->ftpServer = new Form\AppFtpServerModel();
        $model->ftpServer->server = $entity->getFtpServerName();
        $model->ftpServer->username = $entity->getFtpUsername();
        $model->ftpServer->password = $entity->getFtpPassword();

        $model->idMarginGoesTo = $entity->getIdMarginGoesTo();
        $model->idMarginPaymentMode = $entity->getIdMarginPaymentMode();
        $model->isPaymentWithTax = $entity->getIsPaymentWithTax();
        $model->paymentNoTaxText = $entity->getPaymentNoTaxText();
        $model->invoiceNoTaxText = $entity->getInvoiceNoTaxText();
        $model->commissionPaymentWithTax = $entity->getCommissionPaymentWithTax();

        $model->idNutrimeGoesTo = $entity->getIdNutrimeGoesTo();
        $model->idRecipeBookDelivery = $entity->getIdRecipeBookDelivery();
        $model->idRecipeBookGoesTo = $entity->getIdRecipeBookGoesTo();

        $model->isReportDeliveryDownloadAccess = $entity->getIsReportDeliveryDownloadAccess();
        $model->isReportDeliveryFtpServer = $entity->getIsReportDeliveryFtpServer();
        $model->isReportDeliveryPrintedBooklet = $entity->getIsReportDeliveryPrintedBooklet();
        $model->isReportDeliveryDontChargeBooklet = $entity->getIsReportDeliveryDontChargeBooklet();

        $model->acquisiteurId1 = $entity->getAcquisiteurId1();
        $model->commission1 = $entity->getCommission1();
        $model->yearlyDecayPercentage1 = $entity->getYearlyDecayPercentage1();
        $model->acquisiteurId2 = $entity->getAcquisiteurId2();
        $model->commission2 = $entity->getCommission2();
        $model->yearlyDecayPercentage2 = $entity->getYearlyDecayPercentage2();
        $model->acquisiteurId3 = $entity->getAcquisiteurId3();
        $model->commission3 = $entity->getCommission3();
        $model->yearlyDecayPercentage3 = $entity->getYearlyDecayPercentage3();

        $model->partnerId = $entity->getPartnerId();
        $model->webUserId = $entity->getWebUserId();

        $model->accountName = $entity->getAccountName();
        $model->accountNumber = $entity->getAccountNumber();
        $model->bankCode = $entity->getBankCode();
        $model->bic = $entity->getBic();
        $model->iban = $entity->getIban();
        $model->otherNotes = $entity->getOtherNotes();

        $model->rebrandingNick = $entity->getRebrandingNick();
        $model->headerFileName = $entity->getHeaderFileName();
        $model->footerFileName = $entity->getFooterFileName();
        $model->titleLogoFileName = $entity->getTitleLogoFileName();
        $model->boxLogoFileName = $entity->getBoxLogoFileName();

        $model->companyInfo = $entity->getCompanyInfo();
        $model->laboratoryInfo = $entity->getLaboratoryInfo();
        $model->contactInfo = $entity->getContactInfo();
        $model->contactUs = $entity->getContactUs();
        $model->letterExtraText = $entity->getLetterExtraText();
        $model->clinicianInfoText = $entity->getClinicianInfoText();
        $model->text7 = $entity->getText7();
        $model->text8 = $entity->getText8();
        $model->text9 = $entity->getText9();
        $model->text10 = $entity->getText10();
        $model->text11 = $entity->getText11();

        $model->doctorsReporting = $entity->getDoctorsReporting();
        $model->automaticReporting = $entity->getAutomaticReporting();
        $model->noReporting = $entity->getNoReporting();
        $model->invoicingAndPaymentInfo = $entity->getInvoicingAndPaymentInfo();

        $model->reportLogo = array('logo' => $entity->getReportLogo());
        $model->reportColor = $entity->getReportColor();

        $model->cardTypeId = $entity->getCardTypeId();
        $model->cardNumber = $entity->getCardNumber();
        $model->cardExpiryDate = $entity->getCardExpiryDate();
        $model->cardCvcCode = $entity->getCardCvcCode();
        $model->cardFirstName = $entity->getCardFirstName();
        $model->cardSurName = $entity->getCardSurName();

        $model->rating = $entity->getRating();
        $model->idVisibleGroupReport = $entity->getIdVisibleGroupReport();
        $model->idVisibleGroupProduct = $entity->getIdVisibleGroupProduct();
        $model->idPreferredPayment = $entity->getIdPreferredPayment();
        $model->isStopShipping = $entity->getIsStopShipping();
        $model->idDncReportType = $entity->getIdDncReportType();
        $model->idReportNameType = $entity->getIdReportNameType();
        $model->isShowBodCoverPage = $entity->getIsShowBodCoverPage();
        $model->expressShipment = $entity->getExpressShipment();

        $model->emailTemplates = Utils::loadEmailTemplates($this->container, $entity->getId());

        $model->idLanguage = $entity->getIdLanguage();
        // $model->canCreateLoginAndSendStatusMessageToUser = $entity->getCanCreateLoginAndSendStatusMessageToUser();

        $model->canCustomersDownloadReports = $entity->getCanCustomersDownloadReports();
        $model->onlyShowDistributionChannelAddress = $entity->getOnlyShowDistributionChannelAddress();
        $model->destroyInNumDays = $entity->getDestroyInNumDays();

        $model->canViewShop = $entity->getCanViewShop();
        $model->isCanViewYourOrder = $entity->getIsCanViewYourOrder();
        $model->isCanViewCustomerProtection = $entity->getIsCanViewCustomerProtection();
        $model->isCanViewRegisterNewCustomer = $entity->getIsCanViewRegisterNewCustomer();
        $model->isCanViewPendingOrder = $entity->getIsCanViewPendingOrder();
        $model->isCanViewSetting = $entity->getIsCanViewSetting();
        $model->isDelayDownloadReport = $entity->getIsDelayDownloadReport();
        $model->numberDateDelayDownloadReport = $entity->getNumberDateDelayDownloadReport();
        $model->isNotAccessReport = $entity->getIsNotAccessReport();
        $model->isNotSendEmail = $entity->getIsNotSendEmail();

        $model->isGenerateXmlVersion = $entity->getIsGenerateXmlVersion();
        $model->isDeliveryViaSftp = $entity->getIsDeliveryViaSftp();
        $model->sftpHost = $entity->getSftpHost();
        $model->sftpUsername = $entity->getSftpUsername();
        $model->sftpPassword = $entity->getSftpPassword();
        $model->sftpFolder = $entity->getSftpFolder();
        $model->accountingCode = $entity->getAccountingCode();
        $model->idCode = $entity->getIdCode();
        $model->idRecallGoesTo = $entity->getIdRecallGoesTo();

        $model->canDownloadViaSftp = $entity->getCanDownloadViaSftp();
        $model->sftpDownloadHost = $entity->getSftpDownloadHost();
        $model->sftpDownloadUsername = $entity->getSftpDownloadUsername();
        $model->sftpDownloadPassword = $entity->getSftpDownloadPassword();
        $model->sftpDownloadFolder = $entity->getSftpDownloadFolder();

        $model->customBodAccountNumber = $entity->getCustomBodAccountNumber();
        $model->customBodBarcodeNumber = $entity->getCustomBodBarcodeNumber();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'distribution_channels/'.$this->attachmentKey,
            'snapshot' => false
        ));

        $builder->add('sectionInfo', 'section', array(
            'label'    => 'Info',
            'property_path' => false
        ));
        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('distributionChannel', 'text', array(
            'label'    => 'Distribution Channel',
            'required' => true
        ));
        $builder->add('idCode', 'text', array(
            'label'    => 'Id Code',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('typeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_Type')
        ));
        $builder->add('priceCategoryId', 'choice', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));
        $builder->add('idCompleteStatus', 'choice', array(
            'label'    => 'Complete status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_CompleteStatus')
        ));
        $builder->add('supplementInfo', 'textarea', array(
            'label'    => 'Supplement Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('idCurrency', 'choice', array(
            'label'    => 'Currency',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Currency_List')
        ));

        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));


        $builder->add('sectionInvoiceAddress', 'section', array(
            'label'    => 'Invoice address',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('isInvoiceWithTax', 'checkbox', array(
            'label'    => 'Is invoice with tax?',
            'required' => false
        ));

        // Contact person
        $builder->add('sectionContactPerson', 'section', array(
            'label'    => 'Contact person',
            'property_path' => false
        ));
        $builder->add('contactPersonName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('contactPersonEmail', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('contactPersonTelephoneNumber', 'text', array(
            'label'    => 'Telephone number',
            'required' => false
        ));


        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login'
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generateUsername'),
            'name_source_field_id' => 'distributionChannel'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web Login email Opt-Out?',
            'required' => false
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));

        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));

        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));

        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));

        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));

        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));
        $builder->add('isNotAccessReport', 'checkbox', array(
            'label'    => "Can't access report?",
            'required' => false
        ));
        $builder->add('isNotSendEmail', 'checkbox', array(
            'label'    => "Can't send email?",
            'required' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));

        $builder->add('destroyInNumDays', 'text', array(
            'label'    => 'Destroy in x day(s) after completion',
            'required' => false
        ));

        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));

        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        // Partner
        $builder->add('section4', 'section', array(
            'label'    => 'Partner',
            'property_path' => false
        ));
        $builder->add('partnerId', 'choice', array(
            'label'    => 'Partner',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('webUserId', 'choice', array(
            'label'    => 'Web User',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));

        // Delivery
        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('reportDeliveryEmail', 'text', array(
            'label'    => 'Report Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryNote', 'choice', array(
            'label'    => 'Invoice Delivery Note',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Bill_DeliveyNoteTypes')
        ));

        $builder->add('invoiceGoesToId', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('reportGoesToId', 'choice', array(
            'label'    => 'Report Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));
        $builder->add('isFixedReportGoesTo', 'checkbox', array(
            'label'    => 'Fixed report goes to?',
            'required' => false
        ));
        $builder->add('reportDeliveryId', 'choice', array(
            'label'    => 'Report Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeReportDeliveryId()'
            )
        ));
        $builder->add('ftpServer', 'app_ftp_server', array(
            'label'    => 'FTP Server',
            'required' => false
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('invoiceNoTaxText', 'choice', array(
            'label'    => 'Invoice No Tax Text',
            'required' => true,
            'choices'  => Business\InvoiceNoTaxText\Constant::get($this->container)
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));
        // Report Delivery Options
        $builder->add('sectionReportDeliveryOptions', 'section', array(
            'label'    => 'Report Delivery Options',
            'property_path' => false
        ));
        $builder->add('isReportDeliveryDownloadAccess', 'checkbox', array(
            'label'    => 'Download access',
            'required' => false
        ));
        $builder->add('isReportDeliveryFtpServer', 'checkbox', array(
            'label'    => 'FTP server',
            'required' => false
        ));
        $builder->add('isReportDeliveryPrintedBooklet', 'checkbox', array(
            'label'    => 'Printed booklet',
            'required' => false
        ));
        $builder->add('isReportDeliveryDontChargeBooklet', 'checkbox', array(
            'label'    => 'Don\'t charge booklet',
            'required' => false
        ));

        $builder->add('idRecipeBookDelivery', 'choice', array(
            'label'    => 'Recipe Book Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery')
        ));
        $builder->add('idRecipeBookGoesTo', 'choice', array(
            'label'    => 'Recipe Book Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));

        // ACQUISITEUR
        $builder->add('section3', 'section', array(
            'label'    => 'Acquisiteur(s)',
            'property_path' => false
        ));
        $builder->add('acquisiteurId1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage1', 'text', array(
            'label'    => 'Yearly decay percentage 1',
            'required' => false
        ));
        $builder->add('acquisiteurId2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage2', 'text', array(
            'label'    => 'Yearly decay percentage 2',
            'required' => false
        ));
        $builder->add('acquisiteurId3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage3', 'text', array(
            'label'    => 'Yearly decay percentage 3',
            'required' => false
        ));


        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 10,  'cols' => 80
            )
        ));

        // Rebrander
        $builder->add('sectionRebrander', 'section', array(
            'label'    => 'Rebrander',
            'property_path' => false
        ));
        $builder->add('rebrandingNick', 'text', array(
            'label'    => 'Rebranding Nick',
            'required' => false
        ));
        $builder->add('headerFileName', 'text', array(
            'label'    => 'Header File Name',
            'required' => false
        ));
        $builder->add('footerFileName', 'text', array(
            'label'    => 'Footer File Name',
            'required' => false
        ));
        $builder->add('titleLogoFileName', 'text', array(
            'label'    => 'Title Logo File Name',
            'required' => false
        ));
        $builder->add('boxLogoFileName', 'text', array(
            'label'    => 'Box Logo File Name',
            'required' => false
        ));

        // Report details
        $builder->add('sectionReportDetails', 'section', array(
            'label'    => 'Report Details',
            'property_path' => false
        ));
        $builder->add('companyInfo', 'textarea', array(
            'label'    => 'Company Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('laboratoryInfo', 'textarea', array(
            'label'    => 'Laboratory Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactInfo', 'textarea', array(
            'label'    => 'Contact Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactUs', 'textarea', array(
            'label'    => 'Contact Us',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('letterExtraText', 'text', array(
            'label'    => 'Letter Extra Text',
            'required' => false
        ));
        $builder->add('clinicianInfoText', 'text', array(
            'label'    => 'Clinician Info Text',
            'required' => false
        ));
        $builder->add('text7', 'text', array(
            'label'    => 'Text 7',
            'required' => false
        ));
        $builder->add('text8', 'text', array(
            'label'    => 'Text 8',
            'required' => false
        ));
        $builder->add('text9', 'text', array(
            'label'    => 'Text 9',
            'required' => false
        ));
        $builder->add('text10', 'text', array(
            'label'    => 'Text 10',
            'required' => false
        ));
        $builder->add('text11', 'text', array(
            'label'    => 'Text 11',
            'required' => false
        ));

        // GS Setting
        $builder->add('sectionGsSetting', 'section', array(
            'label'    => 'GS Settings',
            'property_path' => false
        ));
        $builder->add('doctorsReporting', 'text', array(
            'label'    => 'Doctors Reporting',
            'required' => false
        ));
        $builder->add('automaticReporting', 'text', array(
            'label'    => 'Automatic Reporting',
            'required' => false
        ));
        $builder->add('noReporting', 'text', array(
            'label'    => 'No Reporting',
            'required' => false
        ));
        $builder->add('invoicingAndPaymentInfo', 'textarea', array(
            'label'    => 'Invoicing And Payment Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 10, 'cols' => 80
            )
        ));

        // Report
        $builder->add('sectionReportSection', 'section', array(
            'label'    => 'Report',
            'required' => false
        ));
        $builder->add('reportLogo',          'app_logo', array(
            'label'    => 'Report Logo',
            'required' => false,
            'path'     => 'report_resource/DistributionChannels'
        ));
        $builder->add('reportColor', 'text', array(
            'label'    => 'Report color',
            'required' => false
        ));

        // Credit card
        $builder->add('sectionCreditCardDetail', 'section', array(
            'label'    => 'Credit Card Detail',
            'property_path' => false
        ));
        $builder->add('cardTypeId', 'choice', array(
            'label'    => 'Card Type',
            'choices'  => $mapping->getMapping('LeepAdmin_CardType_List'),
            'required' => false
        ));
        $builder->add('cardNumber', 'text', array(
            'label'    => 'Card Number',
            'required' => false
        ));
        $builder->add('cardExpiryDate', 'datepicker', array(
            'label'    => 'Expiry Date',
            'required' => false
        ));
        $builder->add('cardCvcCode', 'text', array(
            'label'    => 'CVC Code',
            'required' => false
        ));
        $builder->add('cardFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('cardSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));

        // Others
        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'required' => false
        ));
        $builder->add('customBodAccountNumber', 'text', array(
            'label'    => 'Custom BOD Account number (For BOD shipment)',
            'required' => false
        ));
        $builder->add('customBodBarcodeNumber', 'text', array(
            'label'    => 'Custom BOD Barcode number (2 digit)',
            'required' => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));
        $builder->add('idVisibleGroupReport', 'choice', array(
            'label'    => 'Visible Group Report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Visible_Group_Report')
        ));
        $builder->add('idVisibleGroupProduct', 'choice', array(
            'label'    => 'Visible Group Product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Visible_Group_Product')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('isStopShipping', 'checkbox', array(
            'label'    => 'Stop shipping',
            'required' => false
        ));
        $builder->add('idDncReportType', 'choice', array(
            'label'    => 'DNC Report Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_DncReportType')
        ));
        $builder->add('idReportNameType', 'choice', array(
            'label'    => 'Report Name Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportNameTypes')
        ));
        $builder->add('isShowBodCoverPage', 'checkbox', array(
            'label'    => 'Show BOD Cover page?',
            'required' => false
        ));
        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));

        $builder->add('onlyShowDistributionChannelAddress', 'checkbox', array(
            'label'    => 'Only Show Distribution Channel Address',
            'required' => false
        ));

        $builder->add('isGenerateXmlVersion', 'checkbox', array(
            'label'    => 'Is generate XML Version?',
            'required' => false
        ));
        $builder->add('isDeliveryViaSftp', 'checkbox', array(
            'label'    => 'Is delivery via SFTP',
            'required' => false
        ));
        $builder->add('sftpHost', 'text', array(
            'label'    => 'SFTP Host',
            'required' => false
        ));
        $builder->add('sftpUsername', 'text', array(
            'label'    => 'SFTP Username',
            'required' => false
        ));
        $builder->add('sftpPassword', 'text', array(
            'label'    => 'SFTP Password',
            'required' => false
        ));
        $builder->add('sftpFolder', 'text', array(
            'label'    => 'SFTP Folder',
            'required' => false
        ));
        $builder->add('expressShipment', 'checkbox', array(
            'label'    => 'Express shipment',
            'required' => false
        ));


        $builder->add('canDownloadViaSftp', 'checkbox', array(
            'label'    => 'Can download via SFTP',
            'required' => false
        ));
        $builder->add('sftpDownloadHost', 'text', array(
            'label'    => 'SFTP download Host',
            'required' => false
        ));
        $builder->add('sftpDownloadUsername', 'text', array(
            'label'    => 'SFTP download Username',
            'required' => false
        ));
        $builder->add('sftpDownloadPassword', 'text', array(
            'label'    => 'SFTP download Password',
            'required' => false
        ));
        $builder->add('sftpDownloadFolder', 'text', array(
            'label'    => 'SFTP download Folder',
            'required' => false
        ));

        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));


        // Email templates
        $builder->add('sectionEmailTemplate', 'section', array(
            'label'    => 'Email Templates',
            'property_path' => false
        ));
        $builder->add('emailTemplates', 'app_email_templates');

        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));

        return $builder;
    }


    public function onPreBind(DataEvent $event) {
        $model = $event->getData();

        $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource/DistributionChannels';
        if (isset($model['reportLogo']['attachment']) && !empty($model['reportLogo']['attachment'])) {
            $name = $this->id.'.'.pathinfo($model['reportLogo']['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model['reportLogo']['attachment']->move($attachmentDir, $name);
            $model['reportLogo']['logo'] = $name;
        }
        else {
            $model['reportLogo']['logo'] = $this->entity->getReportLogo();
        }

        $event->setData($model);
    }


    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $this->entity->setAttachmentKey($model->attachmentKey);

        /// check
        $bodAccountNumber = trim($model->customBodAccountNumber);
        $bodBarcodeNumber= trim($model->customBodBarcodeNumber);
        if (!empty($bodAccountNumber) && strlen($bodBarcodeNumber) != 2) {
            $this->errors[] = "Bod barcode number must have two digit";
            return false;
        }

        if ($this->entity->getDistributionChannel() != $model->distributionChannel) {
            $query = $em->createQueryBuilder();
            $dcOld = $query->select('p')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere('p.distributionchannel =:name')
            ->setParameter('name', $model->distributionChannel)->getQuery()->getResult();
            if ($dcOld) {
                // Validation
                $this->errors += ['Distribution channel existed. Please try a diffrent name!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
            $this->entity->setDistributionChannel($model->distributionChannel);
        }
        else {
            $this->entity->setDistributionChannel($model->distributionChannel);
        }

        if ($model->webLoginUsername) {
            $this->entity->setWebLoginUsername($model->webLoginUsername->username);
        } else {
            $this->entity->setWebLoginUsername(null);
        }

        if ($model->webLoginPassword) {
            $this->entity->setWebLoginPassword($model->webLoginPassword->password);
        } else {
            $this->entity->setWebLoginPassword(null);
        }

        $this->entity->setWebLoginStatus($model->webLoginStatus);
        $this->entity->setWebLoginGoesTo($model->webLoginGoesTo);
        $this->entity->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $this->entity->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $this->entity->setIsNutrimeOffered($model->isNutrimeOffered);
        $this->entity->setIsNotContacted($model->isNotContacted);
        $this->entity->setIsDestroySample($model->isDestroySample);
        $this->entity->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $this->entity->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $this->entity->setDomain($model->domain);

        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setUidNumber($model->uidNumber);
        $this->entity->setTypeId($model->typeId);
        $this->entity->setPriceCategoryId($model->priceCategoryId);
        $this->entity->setGenderId($model->genderId);
        $this->entity->setTitle($model->title);
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setInstitution($model->institution);
        $this->entity->setIdCompleteStatus($model->idCompleteStatus);
        $this->entity->setSupplementInfo($model->supplementInfo);
        $this->entity->setIdCurrency($model->idCurrency);

        $this->entity->setStreet($model->street);
        $this->entity->setPostCode($model->postCode);
        $this->entity->setCity($model->city);
        $this->entity->setCountryId($model->countryId);
        $this->entity->setContactEmail($model->contactEmail);
        $this->entity->setTelephone($model->telephone);
        $this->entity->setFax($model->fax);

        $this->entity->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $this->entity->setInvoiceClientName($model->invoiceClientName);
        $this->entity->setInvoiceCompanyName($model->invoiceCompanyName);
        $this->entity->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $this->entity->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $this->entity->setInvoiceAddressCity($model->invoiceAddressCity);
        $this->entity->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $this->entity->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $this->entity->setInvoiceAddressFax($model->invoiceAddressFax);
        $this->entity->setIsInvoiceWithTax($model->isInvoiceWithTax);

        $this->entity->setContactPersonName($model->contactPersonName);
        $this->entity->setContactPersonEmail($model->contactPersonEmail);
        $this->entity->setContactPersonTelephoneNumber($model->contactPersonTelephoneNumber);

        $this->entity->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $this->entity->setReportDeliveryEmail($model->reportDeliveryEmail);
        $this->entity->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);
        $this->entity->setInvoiceDeliveryNote($model->invoiceDeliveryNote);

        $this->entity->setInvoiceGoesToId($model->invoiceGoesToId);
        $this->entity->setReportGoesToId($model->reportGoesToId);
        $this->entity->setIsFixedReportGoesTo($model->isFixedReportGoesTo);
        $this->entity->setReportDeliveryId($model->reportDeliveryId);
        if ($model->ftpServer != null) {
            $this->entity->setFtpServerName($model->ftpServer->server);
            $this->entity->setFtpUsername($model->ftpServer->username);
            $this->entity->setFtpPassword($model->ftpServer->password);
        }
        $this->entity->setIdMarginGoesTo($model->idMarginGoesTo);
        $this->entity->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $this->entity->setIsPaymentWithTax($model->isPaymentWithTax);
        $this->entity->setPaymentNoTaxText($model->paymentNoTaxText);
        $this->entity->setInvoiceNoTaxText($model->invoiceNoTaxText);
        $this->entity->setIdNutrimeGoesTo($model->idNutrimeGoesTo);
        $this->entity->setIdRecipeBookDelivery($model->idRecipeBookDelivery);
        $this->entity->setIdRecipeBookGoesTo($model->idRecipeBookGoesTo);
        $this->entity->setCommissionPaymentWithTax($model->commissionPaymentWithTax);

        $this->entity->setIsReportDeliveryDownloadAccess($model->isReportDeliveryDownloadAccess);
        $this->entity->setIsReportDeliveryFtpServer($model->isReportDeliveryFtpServer);
        $this->entity->setIsReportDeliveryPrintedBooklet($model->isReportDeliveryPrintedBooklet);
        $this->entity->setIsReportDeliveryDontChargeBooklet($model->isReportDeliveryDontChargeBooklet);

        $this->entity->setAcquisiteurId1($model->acquisiteurId1);
        $this->entity->setCommission1($model->commission1);
        $this->entity->setYearlyDecayPercentage1($model->yearlyDecayPercentage1);
        $this->entity->setAcquisiteurId2($model->acquisiteurId2);
        $this->entity->setCommission2($model->commission2);
        $this->entity->setYearlyDecayPercentage2($model->yearlyDecayPercentage2);
        $this->entity->setAcquisiteurId3($model->acquisiteurId3);
        $this->entity->setCommission3($model->commission3);
        $this->entity->setYearlyDecayPercentage3($model->yearlyDecayPercentage3);

        $this->entity->setPartnerId($model->partnerId);
        $this->entity->setWebUserId($model->webUserId);

        $this->entity->setAccountName($model->accountName);
        $this->entity->setAccountNumber($model->accountNumber);
        $this->entity->setBankCode($model->bankCode);
        $this->entity->setBIC($model->bic);
        $this->entity->setIBAN($model->iban);
        $this->entity->setOtherNotes($model->otherNotes);

        // Rebrander
        $this->entity->setRebrandingNick($model->rebrandingNick);
        $this->entity->setHeaderFileName($model->headerFileName);
        $this->entity->setFooterFileName($model->footerFileName);
        $this->entity->setTitleLogoFileName($model->titleLogoFileName);
        $this->entity->setBoxLogoFileName($model->boxLogoFileName);

        $this->entity->setCompanyInfo($model->companyInfo);
        $this->entity->setLaboratoryInfo($model->laboratoryInfo);
        $this->entity->setContactInfo($model->contactInfo);
        $this->entity->setContactUs($model->contactUs);
        $this->entity->setLetterExtraText($model->letterExtraText);
        $this->entity->setClinicianInfoText($model->clinicianInfoText);
        $this->entity->setText7($model->text7);
        $this->entity->setText8($model->text8);
        $this->entity->setText9($model->text9);
        $this->entity->setText10($model->text10);
        $this->entity->setText11($model->text11);

        $this->entity->setDoctorsReporting($model->doctorsReporting);
        $this->entity->setAutomaticReporting($model->automaticReporting);
        $this->entity->setNoReporting($model->noReporting);
        $this->entity->setInvoicingAndPaymentInfo($model->invoicingAndPaymentInfo);

        if (!empty($model->reportLogo['logo'])) {
            $this->entity->setReportLogo($model->reportLogo['logo']);
        }
        $this->entity->setReportColor($model->reportColor);

        $this->entity->setCardTypeId($model->cardTypeId);
        $this->entity->setCardNumber($model->cardNumber);
        $this->entity->setCardExpiryDate($model->cardExpiryDate);
        $this->entity->setCardCvcCode($model->cardCvcCode);
        $this->entity->setCardFirstName($model->cardFirstName);
        $this->entity->setCardSurName($model->cardSurName);

        $this->entity->setRating($model->rating);
        $this->entity->setIdVisibleGroupReport($model->idVisibleGroupReport);
        $this->entity->setIdVisibleGroupProduct($model->idVisibleGroupProduct);
        $this->entity->setIdPreferredPayment($model->idPreferredPayment);
        $this->entity->setIsStopShipping($model->isStopShipping);
        $this->entity->setIdDncReportType($model->idDncReportType);
        $this->entity->setIdReportNameType($model->idReportNameType);
        $this->entity->setIsShowBodCoverPage($model->isShowBodCoverPage);
        $this->entity->setExpressShipment($model->expressShipment);

        $this->entity->setIsRecheckGeocoding(1);

        $selectedLanguage = null;
        if ($model->idLanguage) {
            $selectedLanguage = $model->idLanguage;
        }
        $this->entity->setIdLanguage($selectedLanguage);
        // $this->entity->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
        $this->entity->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
        $this->entity->setOnlyShowDistributionChannelAddress($model->onlyShowDistributionChannelAddress);
        $this->entity->setDestroyInNumDays($model->destroyInNumDays);

        $this->entity->setCanViewShop($model->canViewShop);
        $this->entity->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $this->entity->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $this->entity->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $this->entity->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $this->entity->setIsCanViewSetting($model->isCanViewSetting);
        if ($model->isDelayDownloadReport == true) {
            $this->entity->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }
        else {
            $this->entity->setNumberDateDelayDownloadReport(null);
        }
        $this->entity->setIsDelayDownloadReport($model->isDelayDownloadReport);
        $this->entity->setIsNotAccessReport($model->isNotAccessReport);
        $this->entity->setIsNotSendEmail($model->isNotSendEmail);

        $this->entity->setIsGenerateXmlVersion($model->isGenerateXmlVersion);
        $this->entity->setIsDeliveryViaSftp($model->isDeliveryViaSftp);
        $this->entity->setSftpHost($model->sftpHost);
        $this->entity->setSftpUsername($model->sftpUsername);
        $this->entity->setSftpPassword($model->sftpPassword);
        $this->entity->setSftpFolder($model->sftpFolder);
        $this->entity->setCustomBodAccountNumber($bodAccountNumber);
        $this->entity->setCustomBodBarcodeNumber($bodBarcodeNumber);

        $this->entity->setCanDownloadViaSftp($model->canDownloadViaSftp);
        $this->entity->setSftpDownloadHost($model->sftpDownloadHost);
        $this->entity->setSftpDownloadUsername($model->sftpDownloadUsername);
        $this->entity->setSftpDownloadPassword($model->sftpDownloadPassword);
        $this->entity->setSftpDownloadFolder($model->sftpDownloadFolder);

        $this->entity->setIdRecallGoesTo($model->idRecallGoesTo);
        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->accountingCode != $this->entity->getAccountingCode()) {
            // validation accounting code
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
            $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
            $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

            if ($dc || $partner || $acquisiteur) {
                // Validation
                $this->errors += ['Accounting code existed. Please try a diffrent code!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
        }
        $this->entity->setAccountingCode($model->accountingCode);

        /// validation id code
        if (strlen($model->idCode) != 5) {
            $this->errors += ['Id code must be 5 char. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->idCode != $this->entity->getIdCode()) {
            // validation accounting code
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($model->idCode);
            $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($model->idCode);
            if ($dc || $partner) {
                // Validation
                $this->errors += ['Id code existed. Please try a diffrent code!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
        }
        $this->entity->setIdCode($model->idCode);
        // Save email template
        Utils::saveEmailTemplates($this->container, $this->entity->getId(), $model->emailTemplates);

        parent::onSuccess();
    }
}
