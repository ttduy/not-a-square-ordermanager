<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

class FilterModel {
    public $domain;
    public $distributionChannel;
    public $uidNumber;
    public $typeId;
    public $priceCategoryId;
    public $genderId;
    public $name;
    public $institution;
    public $complaintStatus;
    public $complaintTopic;
    public $positiveFeedback;
    public $idStopShipping;
    public $idCompleteStatus;
    public $idInvoiceGoesTo;
    public $idAcquisiteur;
    public $idPartner;
    public $rating;
    public $idCountry;
    public $idLanguage;
    public $idCode;
    public $accountingCode;
}
