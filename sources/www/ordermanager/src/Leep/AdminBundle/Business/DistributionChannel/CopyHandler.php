<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new CopyModel();
        $model->idDistributionChannel = $entity->getId();
        $model->distributionChannel = $entity->getDistributionChannel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idDistributionChannel', 'hidden');
        $builder->add('distributionChannel', 'label', array(
            'label'    => 'Distribution Channel',
            'required' => false
        ));
        $builder->add('newName', 'text', array(
            'label'    => 'New name',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $mgr = $this->container->get('easy_module.manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $distributionChannel = $this->entity;

        // copy DC
        $newDistributionChannel = Utils::copyDC($this->container, $distributionChannel);
        /// check
        $query = $em->createQueryBuilder();
        $dcOld = $query->select('p')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->andWhere('p.distributionchannel =:name')
        ->setParameter('name', $model->newName)->getQuery()->getResult();
        if ($dcOld) {
            // Validation
            $this->errors += ['Distribution channel existed. Please try a diffrent name!'];
            if (!empty($this->errors)) {
                return false;
            }
        }

        // accountingCode
        $query = $em->createQueryBuilder();
        $accountingCodeMaxDC = $query->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryPartner = $em->createQueryBuilder();
        $accountingCodeMaxPartner = $queryPartner->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Partner', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryAcquisiteur = $em->createQueryBuilder();
        $accountingCodeMaxAcqui = $queryAcquisiteur->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Acquisiteur', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();
        $max1 = ($accountingCodeMaxDC > $accountingCodeMaxPartner) ? $accountingCodeMaxDC : $accountingCodeMaxPartner;
        $maxAC = ($accountingCodeMaxAcqui > $max1) ? $accountingCodeMaxAcqui : $max1;

        $codeOld = [];
        $codeOldPart = [];
        $randomletter = '';
        do {
            $charset = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
            $randomletter = substr(str_shuffle($charset), 0, 5);
            $codeOldDC =  $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($randomletter);
            $codeOldPart =  $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($randomletter);
        }
        while ($codeOld || $codeOldPart);

        $newDistributionChannel->setIdCode($randomletter);
        $newDistributionChannel->setAccountingCode($maxAC+1);
        $newDistributionChannel->setDistributionchannel($model->newName);
        $newDistributionChannel->setDestroyInNumDays(30);

        $em->flush();

        // Done
        $url = $mgr->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit', array('id' => $newDistributionChannel->getId()));
        $this->messages[] = 'The distribution channel has been copied. Click <a href="'.$url.'">here</a> to view the copied distribution channel';
    }
}
