<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Partner\EditComplaintHandler as BaseEditComplaintHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends BaseEditComplaintHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannelsComplaint', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idDistributionChannel' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:DistributionChannelsComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\DistributionChannelsComplaint();
        $complaint->setIdDistributionChannel($id);
        return $complaint;
    }
}
