<?php
namespace Leep\AdminBundle\Business\DistributionChannel\Form;

class AppMarketingStatusRowModel {
    public $statusDate;
    public $idMarketingStatus;
    public $notes;
    public $reminderDate;
    public $idUser;
}
