<?php
namespace Leep\AdminBundle\Business\DistributionChannel\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFtpServer extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\DistributionChannel\Form\AppFtpServerModel'
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_ftp_server';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('server', 'text', array(
            'label' => 'Server',
            'required'  => false,
            'attr' => array(
                'style' => 'width: 150px'
            )
        ));
        $builder->add('username', 'text', array(
            'label' => 'Username',
            'required'  => false,
            'attr' => array(
                'style' => 'width: 100px'
            )
        ));
        $builder->add('password', 'text', array(
            'label' => 'Password',
            'required'  => false,
            'attr' => array(
                'style' => 'width: 100px'
            )
        ));
    }
}