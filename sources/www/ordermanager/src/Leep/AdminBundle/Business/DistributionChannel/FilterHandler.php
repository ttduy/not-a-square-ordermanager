<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('distributionChannel', 'text', array(
            'label'    => 'Distribution Channel',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('typeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
            'empty_value' => false
        ));
        $builder->add('priceCategoryId', 'choice', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List'),
            'empty_value' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Gender_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));
        $builder->add('complaintStatus', 'choice', array(
            'label'    => 'Complaint status',
            'required' => false,
            'choices'  => array(0 => 'All', -1 => 'Has complaints') + $mapping->getMapping('LeepAdmin_ComplaintStatus'),
            'empty_value' => false
        ));
        $builder->add('complaintTopic', 'choice', array(
            'label'    => 'Complaint topic',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Complaint_TopicFilter'),
            'empty_value' => false
        ));
        $builder->add('positiveFeedback', 'choice', array(
            'label'    => 'Positive Feedback',
            'required' => false,
            'choices'  => array(0 => 'Any', 1 => 'Has positive feedback'),
            'empty_value' => false
        ));
        $builder->add('idStopShipping', 'choice', array(
            'label'    => 'Stop shipping',
            'required' => false,
            'choices'  => array(0 => 'All', 1 => 'Yes', 2 => 'No'),
            'empty_value' => false
        ));
        $builder->add('idCompleteStatus', 'choice', array(
            'label'    => 'Complete status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_CompleteStatus'),
            'empty_value' => false
        ));
        $builder->add('idInvoiceGoesTo', 'choice', array(
            'label'    => 'Invoice goes to',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo'),
            'empty_value' => false
        ));
        $builder->add('idAcquisiteur', 'choice', array(
            'label'    => 'Acquisiteur',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
            'empty_value' => false
        ));
        $builder->add('idPartner', 'choice', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Partner_List'),
            'empty_value' => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Rating_Levels'),
            'empty_value' => false
        ));
        $builder->add('idCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Country_List'),
            'empty_value' => false
        ));
        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Language_List'),
            'empty_value' => false
        ));
        $builder->add('idCode', 'text', array(
            'label'    => 'Id Code',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        return $builder;
    }
}
