<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');

        $id = $this->container->get('request')->query->get('id', 0);
        $channel = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
        $data[] = $this->addSection('Channel');
        $data[] = $this->add('Domain', $channel->getDomain());
        $data[] = $this->add('Distribution Channel', $channel->getDistributionChannel());
        $data[] = $this->add('UID Number', $channel->getUidNumber());
        $data[] = $this->add('Type', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_Type', $channel->getTypeId()));
        $data[] = $this->add('Price Category', $mapping->getMappingTitle('LeepAdmin_PriceCategory_List', $channel->getPriceCategoryId()));
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $channel->getGenderId()));
        $data[] = $this->add('Title', $channel->getTitle());
        $data[] = $this->add('First Name', $channel->getFirstName());
        $data[] = $this->add('Surname', $channel->getSurName());
        $data[] = $this->add('Institution', $channel->getInstitution());
        $data[] = $this->add('Supplement Info', $channel->getSupplementInfo());

        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $channel->getStreet());
        $data[] = $this->add('Post Code', $channel->getPostCode());
        $data[] = $this->add('City', $channel->getCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $channel->getCountryId()));
        $data[] = $this->add('Contact Email', $channel->getContactEmail());
        $data[] = $this->add('Telephone', $channel->getTelephone());
        $data[] = $this->add('Fax', $channel->getFax());

        $data[] = $this->addSection('Delivery');
        $data[] = $this->add('Status Delivery Email', $channel->getStatusDeliveryEmail());
        $data[] = $this->add('Report Delivery Email', $channel->getReportDeliveryEmail());
        $data[] = $this->add('Invoice Delivery Email', $channel->getInvoiceDeliveryEmail());
        $data[] = $this->add('Invoice Delivery Note', $channel->getInvoiceDeliveryNote());
        $data[] = $this->add('Invoice Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_InvoiceGoesTo', $channel->getInvoiceGoesToId()));
        $data[] = $this->add('Report Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportGoesTo', $channel->getReportGoesToId()));
        $data[] = $this->add('Report Delivery', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportDelivery', $channel->getReportDeliveryId()));
        if ($channel->getReportDeliveryId() == Constant::REPORT_DELIVERY_FTP_SERVER) {
            $data[] = $this->add('FTP Server', $channel->getFtpServerName().' / '.$channel->getFtpUsername().' / '.$channel->getFtpPassword());
        }

        $data[] = $this->addSection('Acquisiteur (s)');
        $data[] = $this->add('Acquisiteur 1',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $channel->getAcquisiteurId1()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $channel->getAcquisiteurId1()))
            )
        );
        $data[] = $this->add('Commission 1', $channel->getCommission1());
        $data[] = $this->add('Acquisiteur 2',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $channel->getAcquisiteurId2()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $channel->getAcquisiteurId2()))
            )
        );
        $data[] = $this->add('Commission 2', $channel->getCommission2());
        $data[] = $this->add('Acquisiteur 3',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $channel->getAcquisiteurId3()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $channel->getAcquisiteurId3()))
            )
        );
        $data[] = $this->add('Commission 3', $channel->getCommission3());
        $data[] = $this->add('Partner', $mapping->getMappingTitle('LeepAdmin_Partner_List', $channel->getPartnerId()));
        $data[] = $this->add('Web User', $mapping->getMappingTitle('LeepAdmin_WebUser_List', $channel->getWebUserId()));


        $data[] = $this->addSection('Bank account');
        $data[] = $this->add('Account Name', $channel->getAccountName());
        $data[] = $this->add('Account Number', $channel->getAccountNumber());
        $data[] = $this->add('Bank Code', $channel->getBankCode());
        $data[] = $this->add('BIC', $channel->getBIC());
        $data[] = $this->add('IBAN', $channel->getIBAN());
        $data[] = $this->add('Other Notes', $channel->getOtherNotes());

        $data[] = $this->addSection('Rebrander');
        $data[] = $this->add('Rebranding Nick', $channel->getRebrandingNick());
        $data[] = $this->add('Header File Name', $channel->getHeaderFileName());
        $data[] = $this->add('Footer File Name', $channel->getFooterFileName());
        $data[] = $this->add('Title Logo File Name', $channel->getTitleLogoFileName());
        $data[] = $this->add('Box Logo File Name', $channel->getBoxLogoFileName());

        $data[] = $this->addSection('Report Details');
        $data[] = $this->add('Company Info', $channel->getCompanyInfo());
        $data[] = $this->add('Laboratory Info', $channel->getLaboratoryInfo());
        $data[] = $this->add('Contact Info', $channel->getContactInfo());
        $data[] = $this->add('Contact Us', $channel->getContactUs());
        $data[] = $this->add('Letter Extra Text', $channel->getLetterExtraText());
        $data[] = $this->add('Clinician Info Text', $channel->getClinicianInfoText());
        $data[] = $this->add('Text 7', $channel->getText7());
        $data[] = $this->add('Text 8', $channel->getText8());
        $data[] = $this->add('Text 9', $channel->getText9());
        $data[] = $this->add('Text 10', $channel->getText10());
        $data[] = $this->add('Text 11', $channel->getText11());

        $data[] = $this->addSection('GS Settings');
        $data[] = $this->add('Doctors Reporting', $channel->getDoctorsReporting());
        $data[] = $this->add('Automatic Reporting', $channel->getAutomaticReporting());
        $data[] = $this->add('No Reporting', $channel->getNoReporting());
        $data[] = $this->add('Invoicing And Payment Info', $channel->getInvoicingAndPaymentInfo());

        $data[] = $this->addSection('Credit Card Details');
        $data[] = $this->add('Card Type', $mapping->getMappingTitle('LeepAdmin_CardType_List', $channel->getCardTypeId()));
        $data[] = $this->add('Number', $channel->getCardNumber());
        $data[] = $this->add('Expiry Date', $formatter->format($channel->getCardExpiryDate(), 'date'));
        $data[] = $this->add('CVC Code', $channel->getCardCvcCode());
        $data[] = $this->add('First Name', $channel->getCardFirstName());
        $data[] = $this->add('Sur Name', $channel->getCardSurName());

        $data[] = $this->addSection('Others');
        $data[] = $this->add('Visible Group Report', $mapping->getMappingTitle('LeepAdmin_Visible_Group_Report', $channel->getIdVisibleGroupReport()));
        $data[] = $this->add('Visible Group Product', $mapping->getMappingTitle('LeepAdmin_Visible_Group_Product', $channel->getIdVisibleGroupProduct()));
        $data[] = $this->add('Can be contacted directly', ($channel->getIsCustomerBeContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('NutriMe Supplements are offered', ($channel->getIsNutrimeOffered()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer chose OPT OUT – don’t contact', ($channel->getIsNotContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('Destroy details and samples after completion', ($channel->getIsDestroySample()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer agreed to extended research participation', ($channel->getIsFutureResearchParticipant()) ? 'Yes' : 'No');
        $data[] = $this->add('Sample can be used for science', ($channel->getSampleCanBeUsedForScience()) ? 'Yes' : 'No');
        $data[] = $this->add('Language', $mapping->getMappingTitle('LeepAdmin_Customer_Language', $channel->getIdLanguage()));
        $data[] = $this->add('Web Login Opt-Out?', ($channel->getIsWebLoginOptOut()) ? 'Yes' : 'No');
        $data[] = $this->add('Can view Shop', ($channel->getCanViewShop()) ? 'Yes' : 'No');
        $data[] = $this->add('Is generate food table excel version', ($channel->getIsGenerateFoodTableExcelVersion()) ? 'Yes' : 'No');
        return $data;
    }
}
