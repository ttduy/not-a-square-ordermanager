<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class BulkEditDCWebLogin extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function getDistributionChannels() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $idPartner = $this->container->get('request')->get('id', 0);
        $idPartner = intval($idPartner);
        $arrDC = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);
        }
        return $query->getQuery()->getResult();
    }

    public function convertToFormModel($entity) {
        $result = $this->getDistributionChannels();
        $arrData = array();
        if ($result) {
            foreach ($result as $dc) {
                $arrData['isWebLoginOptOut_' . $dc->getId()] = $dc->getIsWebLoginOptOut();
                $arrData['canCustomersDownloadReports_' . $dc->getId()] = $dc->getCanCustomersDownloadReports();
                $arrData['isCustomerBeContacted_' . $dc->getId()] = $dc->getIsCustomerBeContacted();

                $arrData['canViewShop_' . $dc->getId()] = $dc->getCanViewShop();
                $arrData['isCanViewYourOrder_' . $dc->getId()] = $dc->getIsCanViewYourOrder();
                $arrData['isCanViewCustomerProtection_' . $dc->getId()] = $dc->getIsCanViewCustomerProtection();
                $arrData['isCanViewRegisterNewCustomer_' . $dc->getId()] = $dc->getIsCanViewRegisterNewCustomer();
                $arrData['isCanViewPendingOrder_' . $dc->getId()] = $dc->getIsCanViewPendingOrder();
                $arrData['isCanViewSetting_' . $dc->getId()] = $dc->getIsCanViewSetting();
            }
        }

        return $arrData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $result = $this->getDistributionChannels();
        if ($result) {
            foreach ($result as $dc) {
                $builder->add('isWebLoginOptOut_' . $dc->getId(), 'checkbox');
                $builder->add('canCustomersDownloadReports_' . $dc->getId(), 'checkbox');
                $builder->add('isCustomerBeContacted_' . $dc->getId(), 'checkbox');

                $builder->add('canViewShop_' . $dc->getId(), 'checkbox');
                $builder->add('isCanViewYourOrder_' . $dc->getId(), 'checkbox');
                $builder->add('isCanViewCustomerProtection_' . $dc->getId(), 'checkbox');
                $builder->add('isCanViewRegisterNewCustomer_' . $dc->getId(), 'checkbox');
                $builder->add('isCanViewPendingOrder_' . $dc->getId(), 'checkbox');
                $builder->add('isCanViewSetting_' . $dc->getId(), 'checkbox');
            }
        }

        return $builder;
    }

    public function onSuccess() {
        set_time_limit(300);
        $model = $this->getForm()->getData();

        $em = $this->container->get('doctrine')->getEntityManager();
        $result = $this->getDistributionChannels();
        foreach ($result as $dc) {
            if (isset($model['isWebLoginOptOut_'.$dc->getId()])) {
                $isWebLoginOptOut = $model['isWebLoginOptOut_'.$dc->getId()];
                $canCustomersDownloadReports = $model['canCustomersDownloadReports_'.$dc->getId()];
                $isCustomerBeContacted = $model['isCustomerBeContacted_'.$dc->getId()];

                $canViewShop = $model['canViewShop_'.$dc->getId()];
                $isCanViewYourOrder = $model['isCanViewYourOrder_'.$dc->getId()];
                $isCanViewCustomerProtection = $model['isCanViewCustomerProtection_'.$dc->getId()];
                $isCanViewRegisterNewCustomer = $model['isCanViewRegisterNewCustomer_'.$dc->getId()];
                $isCanViewPendingOrder = $model['isCanViewPendingOrder_'.$dc->getId()];
                $isCanViewSetting = $model['isCanViewSetting_'.$dc->getId()];

                $dc->setIsWebLoginOptuOut($isWebLoginOptOut);
                $dc->setCanCustomersDownloadReports($canCustomersDownloadReports);
                $dc->setIsCustomerBeContacted($isCustomerBeContacted);

                $dc->setCanViewShop($canViewShop);
                $dc->setIsCanViewYourOrder($isCanViewYourOrder);
                $dc->setIsCanViewCustomerProtection($isCanViewCustomerProtection);
                $dc->setIsCanViewRegisterNewCustomer($isCanViewRegisterNewCustomer);
                $dc->setIsCanViewPendingOrder($isCanViewPendingOrder);
                $dc->setIsCanViewSetting($isCanViewSetting);
            }
        }

        $em->flush();

        $this->messages = array();
        $this->messages[] = 'Saved';
    }
}
