<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditMarketingStatusHandler extends AppEditHandler {
    public $id;
    public $attachmentKey = false;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if ($this->attachmentKey === false) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannelMarketingStatus', 'p')
            ->leftJoin('AppDatabaseMainBundle:MarketingStatus', 'ms', 'WITH', 'p.idMarketingStatus = ms.id')
            ->andWhere('p.idDistributionChannel = :dcId')
            ->setParameter('dcId', $entity->getId())
            ->orderBy('ms.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Form\AppMarketingStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->idMarketingStatus = $status->getIdMarketingStatus();
            $m->notes = $status->getNotes();
            $m->idUser = $status->getIdUser();
            $m->reminderDate = $status->getReminderDate();
            $statusList[] = $m;
        }

        $model = new EditMarketingStatusModel();
        if ($entity->getAttachmentKey() == '') {
            $this->generateAttachmentKey();
            $this->entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }

        $this->attachmentKey = $entity->getAttachmentKey();

        $model->attachmentKey = $entity->getAttachmentKey();
        $model->distributionChannel = $entity->getDistributionchannel();
        $model->statusList = $statusList;
        $model->rating = $entity->getRating();
        $model->idDC = $entity->getId();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $today = new \DateTime();
        
        $builder->add('idDC', 'hidden');
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'distribution_channel_marketing_status/'.$this->attachmentKey,
            'snapshot' => false
        ));

        $builder->add('section1',          'section', array(
            'label' => 'Info',
            'property_path' => false
        ));
        $builder->add('distributionChannel',            'label', array(
            'label' => 'Distribution Channel',
            'required'      => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));

        $builder->add('section2',          'section', array(
            'label' => 'DC Info Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Form\AppMarketingStatusRow($this->container, $today->format('d/m/Y')),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'DC Info Status record',
                'required' => false
            )
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setAttachmentKey($model->attachmentKey);
        $this->entity->setRating($model->rating);

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();

        // delete previous registered DC Info Status
        $filters = array('idDistributionChannel' => $this->id);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:DistributionChannelMarketingStatus', $filters);

        // save DC Info Status
        $sortOrder = 1;
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\DistributionChannelMarketingStatus();
            $status->setIdDistributionChannel($this->id);
            $status->setStatusDate($statusRow->statusDate);
            $status->setIdMarketingStatus($statusRow->idMarketingStatus);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $status->setIdUser($statusRow->idUser);
            $status->setReminderDate($statusRow->reminderDate);
            $em->persist($status);
        }

        $em->flush();        

        parent::onSuccess();
    }
}
