<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class BulkEditDCReportSetting extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function getDistributionChannels() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $idPartner = $this->container->get('request')->get('id', 0);
        $idPartner = intval($idPartner);
        $arrDC = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);
        }
        return $query->getQuery()->getResult();
    }

    public function convertToFormModel($entity) {
        $result = $this->getDistributionChannels();
        $arrData = array();
        if ($result) {
            foreach ($result as $dc) {
                $arrData['isPrintedReportSentToPartner_' . $dc->getId()] = $dc->getIsPrintedReportSentToPartner();
                $arrData['isPrintedReportSentToDc_' . $dc->getId()] = $dc->getIsPrintedReportSentToDc();
                $arrData['isPrintedReportSentToCustomer_' . $dc->getId()] = $dc->getIsPrintedReportSentToCustomer();
                $arrData['isDigitalReportGoesToCustomer_' . $dc->getId()] = $dc->getIsDigitalReportGoesToCustomer();
                $arrData['idRecallGoesTo_' . $dc->getId()] = $dc->getIdRecallGoesTo();
            }
        }

        return $arrData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $result = $this->getDistributionChannels();
        if ($result) {
            foreach ($result as $dc) {
                $builder->add('isPrintedReportSentToPartner_' . $dc->getId(), 'checkbox');
                $builder->add('isPrintedReportSentToDc_' . $dc->getId(), 'checkbox');
                $builder->add('isPrintedReportSentToCustomer_' . $dc->getId(), 'checkbox');
                $builder->add('isDigitalReportGoesToCustomer_' . $dc->getId(), 'checkbox');
                $builder->add('idRecallGoesTo_' . $dc->getId(), 'choice', array(
                    'choices' => array('' => '') + $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
                ));

                $builder->add('isCopyToCustomer_' . $dc->getId(), 'checkbox');
            }
        }

        return $builder;
    }

    public function onSuccess() {
        set_time_limit(300);
        $model = $this->getForm()->getData();

        $em = $this->container->get('doctrine')->getEntityManager();
        $result = $this->getDistributionChannels();
        foreach ($result as $dc) {
            if (isset($model['isPrintedReportSentToPartner_'.$dc->getId()])) {
                $isPrintedReportSentToPartner = $model['isPrintedReportSentToPartner_'.$dc->getId()];
                $isPrintedReportSentToDc = $model['isPrintedReportSentToDc_'.$dc->getId()];
                $isPrintedReportSentToCustomer = $model['isPrintedReportSentToCustomer_'.$dc->getId()];
                $isDigitalReportGoesToCustomer = $model['isDigitalReportGoesToCustomer_'.$dc->getId()];
                $idRecallGoesTo = $model['idRecallGoesTo_'.$dc->getId()];
                $isCopyToCustomer = $model['isCopyToCustomer_'.$dc->getId()];

                $dc->setIsPrintedReportSentToPartner($isPrintedReportSentToPartner);
                $dc->setIsPrintedReportSentToDc($isPrintedReportSentToDc);
                $dc->setIsPrintedReportSentToCustomer($isPrintedReportSentToCustomer);
                $dc->setIsDigitalReportGoesToCustomer($isDigitalReportGoesToCustomer);
                $dc->setIdRecallGoesTo($idRecallGoesTo);

                if ($isCopyToCustomer) {
                    $query = $em->createQueryBuilder();
                    $query->update('AppDatabaseMainBundle:Customer', 'p')
                        ->set('p.isPrintedReportSentToPartner', intval($isPrintedReportSentToPartner))
                        ->set('p.isPrintedReportSentToDc', intval($isPrintedReportSentToDc))
                        ->set('p.isPrintedReportSentToCustomer', intval($isPrintedReportSentToCustomer))
                        ->set('p.isDigitalReportGoesToCustomer', intval($isDigitalReportGoesToCustomer))
                        ->set('p.idRecallGoesTo', intval($idRecallGoesTo))
                        ->andWhere('p.distributionchannelid = '.$dc->getId())
                        ->getQuery()
                        ->execute();
                }
            }
        }

        $em->flush();

        $this->messages = array();
        $this->messages[] = 'Saved';
    }
}
