<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Symfony\Component\Validator\ExecutionContext;

class LogComplaintModel {
    public $idTopic;
    public $timestamp;
    public $idUser;
    public $complainant;
    public $description;

    public $solutionTimestamp;
    public $solutionIdUser;
    public $solutionDescription;
}
