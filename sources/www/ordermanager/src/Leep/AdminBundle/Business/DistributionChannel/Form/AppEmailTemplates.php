<?php
namespace Leep\AdminBundle\Business\DistributionChannel\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppEmailTemplates extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_email_templates';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $mapping = $this->container->get('easy_mapping');  
        $statusList = $mapping->getMapping('LeepAdmin_Customer_Status');
        $view->vars['statusList'] = $statusList;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');          

        $statusList = $mapping->getMapping('LeepAdmin_Customer_Status');
        foreach ($statusList as $statusId => $statusLabel) {
            $builder->add('status_'.$statusId, 'choice', array(
                'label'   => $statusLabel,
                'required' => false,
                'choices' => $mapping->getMapping('LeepAdmin_EmailTemplate_List')
            ));
        }
    }
}