<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

class EmailModel {
    public $receiverId;
    public $sendTo;
    public $cc;
    public $subject;
    public $content;

    public $webLoginUsername;
    public $webLoginPassword;
    public $name;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $isFutureResearchParticipant;
    public $sampleCanBeUsedForScience;
    public $submitLoginInfo;
    public $submitContactSetting;

    public $idEmailMethod;
    public $isAutoRetry;
    public $isSendNow;
}