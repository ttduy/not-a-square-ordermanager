<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class BulkEditDCSetting extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function getDistributionChannels() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $idPartner = $this->container->get('request')->get('id', 0);
        $session = $this->container->get('session');
        $page = intval($this->container->get('request')->get('page', 0));
        if ($page != 0){
            $session->set('page', $page);
        }
        else {
            $page = $session->get('page', 1);
        }
        $idPartner = intval($idPartner);
        $arrDC = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);
        }
        return $query->getQuery()->setFirstResult(($page-1)*100)->setMaxResults(100)->getResult();
    }
    public function getDistributionChannelsByPage($page) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $idPartner = $this->container->get('request')->get('id', 0);
        $idPartner = intval($idPartner);
        $arrDC = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')

                ->setParameter('idPartner', $idPartner);
        }
        return $query->getQuery()->setFirstResult(($page-1)*100)->setMaxResults(100)->getResult();
    }

    public function convertToFormModel($entity) {
        $result = $this->getDistributionChannels();
        $arrData = array();
        if ($result) {
            foreach ($result as $dc) {
                $arrData['isCustomerBeContacted_' . $dc->getId()] = $dc->getIsCustomerBeContacted();
                $arrData['isNutrimeOffered_' . $dc->getId()] = $dc->getIsNutrimeOffered();
                $arrData['isNotContacted_' . $dc->getId()] = $dc->getIsNotContacted();
                $arrData['isDestroySample_' . $dc->getId()] = $dc->getIsDestroySample();
                $arrData['sampleCanBeUsedForScience_' . $dc->getId()] = $dc->getSampleCanBeUsedForScience();
                $arrData['isFutureResearchParticipant_' . $dc->getId()] = $dc->getIsFutureResearchParticipant();

                $arrData['canViewShop_' . $dc->getId()] = $dc->getCanViewShop();
                $arrData['canCustomersDownloadReports_' . $dc->getId()] = $dc->getCanCustomersDownloadReports();
                $arrData['isGenerateFoodTableExcelVersion_' . $dc->getId()] = $dc->getIsGenerateFoodTableExcelVersion();
            }
        }
        return $arrData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $result = $this->getDistributionChannels();
        if ($result) {
            foreach ($result as $dc) {
                $builder->add('isCustomerBeContacted_' . $dc->getId(), 'checkbox');
                $builder->add('isNutrimeOffered_' . $dc->getId(), 'checkbox');
                $builder->add('isNotContacted_' . $dc->getId(), 'checkbox');
                $builder->add('isDestroySample_' . $dc->getId(), 'checkbox');
                $builder->add('sampleCanBeUsedForScience_' . $dc->getId(), 'checkbox');
                $builder->add('isFutureResearchParticipant_' . $dc->getId(), 'checkbox');

                $builder->add('canViewShop_' . $dc->getId(), 'checkbox');
                $builder->add('canCustomersDownloadReports_' . $dc->getId(), 'checkbox');
                $builder->add('isGenerateFoodTableExcelVersion_' . $dc->getId(), 'checkbox');

                $builder->add('isCopyToCustomer_' . $dc->getId(), 'checkbox');
            }
        }
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $session = $this->container->get('session');
        $em = $this->container->get('doctrine')->getEntityManager();
        $result = $this->getDistributionChannels();
        foreach ($result as $dc) {
            if (isset($model['isCustomerBeContacted_'.$dc->getId()])) {
                $isCustomerBeContacted = $model['isCustomerBeContacted_'.$dc->getId()];
                $isNutrimeOffered = $model['isNutrimeOffered_'.$dc->getId()];
                $isNotContacted = $model['isNotContacted_'.$dc->getId()];
                $isDestroySample = $model['isDestroySample_'.$dc->getId()];
                $sampleCanBeUsedForScience = $model['sampleCanBeUsedForScience_'.$dc->getId()];
                $isFutureResearchParticipant = $model['isFutureResearchParticipant_'.$dc->getId()];

                $canViewShop = $model['canViewShop_'.$dc->getId()];
                $canCustomersDownloadReports = $model['canCustomersDownloadReports_'.$dc->getId()];
                $isGenerateFoodTableExcelVersion = $model['isGenerateFoodTableExcelVersion_'.$dc->getId()];

                $isCopyToCustomer = $model['isCopyToCustomer_'.$dc->getId()];

                $dc->setIsCustomerBeContacted($isCustomerBeContacted);
                $dc->setIsNutrimeOffered($isNutrimeOffered);
                $dc->setIsNotContacted($isNotContacted);
                $dc->setIsDestroySample($isDestroySample);
                $dc->setSampleCanBeUsedForScience($sampleCanBeUsedForScience);
                $dc->setIsFutureResearchParticipant($isFutureResearchParticipant);

                $dc->setCanViewShop($canViewShop);
                $dc->setCanCustomersDownloadReports($canCustomersDownloadReports);
                $dc->setIsGenerateFoodTableExcelVersion($isGenerateFoodTableExcelVersion);
                if ($isCopyToCustomer) {
                    $customers = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findByIdDistributionChannel($dc->getId());
                    foreach ($customers as $customer) {
                        $customer->setIsCustomerBeContacted($isCustomerBeContacted);
                        $customer->setIsNutrimeOffered($isNutrimeOffered);
                    }
                }
                $em->flush();
            }
        }
        $this->messages = array();
        $this->messages[] = 'Saved';
    }
}
