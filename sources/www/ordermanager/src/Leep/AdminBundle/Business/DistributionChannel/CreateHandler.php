<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if ($this->attachmentKey === false) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function getDefaultFormModel() {
        $em = $this->container->get('doctrine')->getEntityManager();
        // accountingCode
        $query = $em->createQueryBuilder();
        $accountingCodeMaxDC = $query->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryPartner = $em->createQueryBuilder();
        $accountingCodeMaxPartner = $queryPartner->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Partner', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryAcquisiteur = $em->createQueryBuilder();
        $accountingCodeMaxAcqui = $queryAcquisiteur->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Acquisiteur', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();
        $max1 = ($accountingCodeMaxDC > $accountingCodeMaxPartner) ? $accountingCodeMaxDC : $accountingCodeMaxPartner;
        $maxAC = ($accountingCodeMaxAcqui > $max1) ? $accountingCodeMaxAcqui : $max1;

        $codeOld = [];
        $codeOldPart = [];
        $randomletter = '';
        do {
            $charset = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
            $randomletter = substr(str_shuffle($charset), 0, 5);
            $codeOldDC =  $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($randomletter);
            $codeOldPart =  $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($randomletter);
        }
        while ($codeOld || $codeOldPart);

        $this->generateAttachmentKey();
        $model = new CreateModel();
        $model->creationDate = new \DateTime();
        $model->genderId = Business\Base\Constant::GENDER_MALE;
        $model->attachmentKey = $this->attachmentKey;
        $model->idCompleteStatus = Constant::COMPLETE_STATUS_INCOMPLETE;
        $model->container = $this->container;
        $model->webLoginStatus = Business\Base\Constant::WEB_LOGIN_STATUS_ACTIVE;

        $model->canViewShop = true;
        $model->isCanViewYourOrder = true;
        $model->isCanViewCustomerProtection = true;
        $model->isCanViewRegisterNewCustomer = true;
        $model->isCanViewPendingOrder = true;
        $model->isCanViewSetting = true;
        $model->accountingCode = $maxAC+1;
        $model->idCode = $randomletter;
        $model->destroyInNumDays = 30;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'distribution_channels/'.$this->attachmentKey,
            'snapshot' => false
        ));

        $builder->add('sectionInfo', 'section', array(
            'label'    => 'Info',
            'property_path' => false
        ));
        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('distributionChannel', 'text', array(
            'label'    => 'Distribution Channel',
            'required' => true
        ));
        $builder->add('idCode', 'text', array(
            'label'    => 'Id Code',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('typeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_Type')
        ));
        $builder->add('priceCategoryId', 'choice', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));
        $builder->add('idCompleteStatus', 'choice', array(
            'label'    => 'Complete status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_CompleteStatus')
        ));
        $builder->add('supplementInfo', 'textarea', array(
            'label'    => 'Supplement Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('idCurrency', 'choice', array(
            'label'    => 'Currency',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Currency_List')
        ));
        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionInvoiceAddress', 'section', array(
            'label'    => 'Invoice address',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('isInvoiceWithTax', 'checkbox', array(
            'label'    => 'Is invoice with tax?',
            'required' => false
        ));

        // Contact person
        $builder->add('sectionContactPerson', 'section', array(
            'label'    => 'Contact person',
            'property_path' => false
        ));
        $builder->add('contactPersonName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('contactPersonEmail', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('contactPersonTelephoneNumber', 'text', array(
            'label'    => 'Telephone number',
            'required' => false
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web Login email Opt-Out?',
            'required' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('destroyInNumDays', 'text', array(
            'label'    => 'Destroy in x day(s) after completion',
            'required' => false
        ));

        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));

        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        // Partner
        $builder->add('section4', 'section', array(
            'label'    => 'Partner',
            'property_path' => false
        ));
        $builder->add('partnerId', 'choice', array(
            'label'    => 'Partner',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('webUserId', 'choice', array(
            'label'    => 'Web User',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));

        // Delivery
        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('invoiceDeliveryNote', 'choice', array(
            'label'    => 'Invoice Delivery Note',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Bill_DeliveyNoteTypes')
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('reportDeliveryEmail', 'text', array(
            'label'    => 'Report Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceGoesToId', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('reportGoesToId', 'choice', array(
            'label'    => 'Report Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));
        $builder->add('isFixedReportGoesTo', 'checkbox', array(
            'label'    => 'Fixed report goes to?',
            'required' => false
        ));
        $builder->add('reportDeliveryId', 'choice', array(
            'label'    => 'Report Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeReportDeliveryId()'
            )
        ));
        $builder->add('ftpServer', 'app_ftp_server', array(
            'label'    => 'FTP Server',
            'required' => false
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('invoiceNoTaxText', 'choice', array(
            'label'    => 'Invoice No Tax Text',
            'required' => true,
            'choices'  => Business\InvoiceNoTaxText\Constant::get($this->container)
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));
        // Report Delivery Options
        $builder->add('sectionReportDeliveryOptions', 'section', array(
            'label'    => 'Report Delivery Options',
            'property_path' => false
        ));
        $builder->add('isReportDeliveryDownloadAccess', 'checkbox', array(
            'label'    => 'Download access',
            'required' => false
        ));
        $builder->add('isReportDeliveryFtpServer', 'checkbox', array(
            'label'    => 'FTP server',
            'required' => false
        ));
        $builder->add('isReportDeliveryPrintedBooklet', 'checkbox', array(
            'label'    => 'Printed booklet',
            'required' => false
        ));
        $builder->add('isReportDeliveryDontChargeBooklet', 'checkbox', array(
            'label'    => 'Don\'t charge booklet',
            'required' => false
        ));

        $builder->add('idRecipeBookDelivery', 'choice', array(
            'label'    => 'Recipe Book Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery')
        ));
        $builder->add('idRecipeBookGoesTo', 'choice', array(
            'label'    => 'Recipe Book Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));


        $builder->add('section3', 'section', array(
            'label'    => 'Acquisiteur(s)',
            'property_path' => false
        ));
        $builder->add('acquisiteurId1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage1', 'text', array(
            'label'    => 'Yearly decay percentage 1',
            'required' => false
        ));
        $builder->add('acquisiteurId2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage2', 'text', array(
            'label'    => 'Yearly decay percentage 2',
            'required' => false
        ));
        $builder->add('acquisiteurId3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage3', 'text', array(
            'label'    => 'Yearly decay percentage 3',
            'required' => false
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 10,  'cols' => 80
            )
        ));

        // Rebrander
        $builder->add('sectionRebrander', 'section', array(
            'label'    => 'Rebrander',
            'property_path' => false
        ));
        $builder->add('rebrandingNick', 'text', array(
            'label'    => 'Rebranding Nick',
            'required' => false
        ));
        $builder->add('headerFileName', 'text', array(
            'label'    => 'Header File Name',
            'required' => false
        ));
        $builder->add('footerFileName', 'text', array(
            'label'    => 'Footer File Name',
            'required' => false
        ));
        $builder->add('titleLogoFileName', 'text', array(
            'label'    => 'Title Logo File Name',
            'required' => false
        ));
        $builder->add('boxLogoFileName', 'text', array(
            'label'    => 'Box Logo File Name',
            'required' => false
        ));

        // Report details
        $builder->add('sectionReportDetails', 'section', array(
            'label'    => 'Report Details',
            'property_path' => false
        ));
        $builder->add('companyInfo', 'textarea', array(
            'label'    => 'Company Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('laboratoryInfo', 'textarea', array(
            'label'    => 'Laboratory Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactInfo', 'textarea', array(
            'label'    => 'Contact Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactUs', 'textarea', array(
            'label'    => 'Contact Us',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('letterExtraText', 'text', array(
            'label'    => 'Letter Extra Text',
            'required' => false
        ));
        $builder->add('clinicianInfoText', 'text', array(
            'label'    => 'Clinician Info Text',
            'required' => false
        ));
        $builder->add('text7', 'text', array(
            'label'    => 'Text 7',
            'required' => false
        ));
        $builder->add('text8', 'text', array(
            'label'    => 'Text 8',
            'required' => false
        ));
        $builder->add('text9', 'text', array(
            'label'    => 'Text 9',
            'required' => false
        ));
        $builder->add('text10', 'text', array(
            'label'    => 'Text 10',
            'required' => false
        ));
        $builder->add('text11', 'text', array(
            'label'    => 'Text 11',
            'required' => false
        ));

        // GS Setting
        $builder->add('sectionGsSetting', 'section', array(
            'label'    => 'GS Settings',
            'property_path' => false
        ));
        $builder->add('doctorsReporting', 'text', array(
            'label'    => 'Doctors Reporting',
            'required' => false
        ));
        $builder->add('automaticReporting', 'text', array(
            'label'    => 'Automatic Reporting',
            'required' => false
        ));
        $builder->add('noReporting', 'text', array(
            'label'    => 'No Reporting',
            'required' => false
        ));
        $builder->add('invoicingAndPaymentInfo', 'textarea', array(
            'label'    => 'Invoicing And Payment Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 10, 'cols' => 80
            )
        ));

        // Report
        $builder->add('sectionReportSection', 'section', array(
            'label'    => 'Report',
            'required' => false
        ));
        $builder->add('reportLogo',          'app_logo', array(
            'label'    => 'Report Logo',
            'required' => false,
            'path'     => 'report_resource/DistributionChannels'
        ));
        $builder->add('reportColor', 'text', array(
            'label'    => 'Report color',
            'required' => false
        ));

        // Others
        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'required' => false
        ));
        $builder->add('customBodAccountNumber', 'text', array(
            'label'    => 'Custom BOD Account number (For BOD shipment)',
            'required' => false
        ));
        $builder->add('customBodBarcodeNumber', 'text', array(
            'label'    => 'Custom BOD Barcode number (2 digit)',
            'required' => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));
        $builder->add('idVisibleGroupReport', 'choice', array(
            'label'    => 'Visible Group Report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Visible_Group_Report')
        ));
        $builder->add('idVisibleGroupProduct', 'choice', array(
            'label'    => 'Visible Group Product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Visible_Group_Product')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('isStopShipping', 'checkbox', array(
            'label'    => 'Stop shipping',
            'required' => false
        ));
        $builder->add('idDncReportType', 'choice', array(
            'label'    => 'DNC Report Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_DncReportType')
        ));
        $builder->add('idReportNameType', 'choice', array(
            'label'    => 'Report Name Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportNameTypes')
        ));
        $builder->add('isShowBodCoverPage', 'checkbox', array(
            'label'    => 'Show BOD Cover page?',
            'required' => false
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));

        $builder->add('onlyShowDistributionChannelAddress', 'checkbox', array(
            'label'    => 'Only Show Distribution Channel Address',
            'required' => false
        ));

        $builder->add('isGenerateXmlVersion', 'checkbox', array(
            'label'    => 'Is generate XML Version?',
            'required' => false
        ));
        $builder->add('isDeliveryViaSftp', 'checkbox', array(
            'label'    => 'Is delivery via SFTP',
            'required' => false
        ));
        $builder->add('sftpHost', 'text', array(
            'label'    => 'SFTP Host',
            'required' => false
        ));
        $builder->add('sftpUsername', 'text', array(
            'label'    => 'SFTP Username',
            'required' => false
        ));
        $builder->add('sftpPassword', 'text', array(
            'label'    => 'SFTP Password',
            'required' => false
        ));
        $builder->add('sftpFolder', 'text', array(
            'label'    => 'SFTP Folder',
            'required' => false
        ));
        $builder->add('canDownloadViaSftp', 'checkbox', array(
            'label'    => 'Can download via SFTP',
            'required' => false
        ));
        $builder->add('sftpDownloadHost', 'text', array(
            'label'    => 'SFTP download Host',
            'required' => false
        ));
        $builder->add('sftpDownloadUsername', 'text', array(
            'label'    => 'SFTP download Username',
            'required' => false
        ));
        $builder->add('sftpDownloadPassword', 'text', array(
            'label'    => 'SFTP download Password',
            'required' => false
        ));
        $builder->add('sftpDownloadFolder', 'text', array(
            'label'    => 'SFTP download Folder',
            'required' => false
        ));

        $builder->add('expressShipment', 'checkbox', array(
            'label'    => 'Express shipment',
            'required' => false
        ));

        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login',
            'property_path' => false
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generateUsername'),
            'name_source_field_id' => 'distributionChannel'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'distribution_channel', 'dc', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));

        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));

        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));

        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));

        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));
        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));
        $builder->add('isNotAccessReport', 'checkbox', array(
            'label'    => "Can't access report?",
            'required' => false
        ));
        $builder->add('isNotSendEmail', 'checkbox', array(
            'label'    => "Can't send email?",
            'required' => false
        ));

        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));

        // Credit card
        $builder->add('sectionCreditCardDetail', 'section', array(
            'label'    => 'Credit Card Detail',
            'property_path' => false
        ));
        $builder->add('cardTypeId', 'choice', array(
            'label'    => 'Card Type',
            'choices'  => $mapping->getMapping('LeepAdmin_CardType_List')
        ));
        $builder->add('cardNumber', 'text', array(
            'label'    => 'Card Number',
            'required' => false
        ));
        $builder->add('cardExpiryDate', 'datepicker', array(
            'label'    => 'Expiry Date',
            'required' => false
        ));
        $builder->add('cardCvcCode', 'text', array(
            'label'    => 'CVC Code',
            'required' => false
        ));
        $builder->add('cardFirstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('cardSurName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));

        $builder->add('sectionEmailTemplate', 'section', array(
            'label'    => 'Email Templates',
            'property_path' => false
        ));
        $builder->add('emailTemplates', 'app_email_templates');
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        /// check
        $query = $em->createQueryBuilder();
        $dcOld = $query->select('p')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->andWhere('p.distributionchannel =:name')
        ->setParameter('name', $model->distributionChannel)->getQuery()->getResult();
        if ($dcOld) {
            // Validation
            $this->errors += ['Distribution channel existed. Please try a diffrent name!'];
            if (!empty($this->errors)) {
                return false;
            }
        }

        $bodAccountNumber = trim($model->customBodAccountNumber);
        $bodBarcodeNumber= trim($model->customBodBarcodeNumber);
        if (!empty($bodAccountNumber) && strlen($bodBarcodeNumber) != 2) {
            $this->errors[] = "Bod barcode number must have two digit";
            return false;
        }

        $channel = new Entity\DistributionChannels();

        $channel->setAttachmentKey($model->attachmentKey);

        if ($model->webLoginUsername) {
            $channel->setWebLoginUserName($model->webLoginUsername->username);
        }

        if ($model->webLoginPassword) {
            $channel->setWebLoginPassword($model->webLoginPassword->password);
        }

        $channel->setWebLoginStatus($model->webLoginStatus);
        $channel->setWebLoginGoesTo($model->webLoginGoesTo);
        $channel->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $channel->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $channel->setIsNutrimeOffered($model->isNutrimeOffered);
        $channel->setIsNotContacted($model->isNotContacted);
        $channel->setIsDestroySample($model->isDestroySample);
        $channel->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $channel->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $channel->setDomain($model->domain);
        $channel->setDistributionChannel($model->distributionChannel);
        $channel->setCreationDate($model->creationDate);
        $channel->setUidNumber($model->uidNumber);
        $channel->setTypeId($model->typeId);
        $channel->setPriceCategoryId($model->priceCategoryId);
        $channel->setGenderId($model->genderId);
        $channel->setTitle($model->title);
        $channel->setFirstName($model->firstName);
        $channel->setSurName($model->surName);
        $channel->setInstitution($model->institution);
        $channel->setIdCompleteStatus($model->idCompleteStatus);
        $channel->setSupplementInfo($model->supplementInfo);
        $channel->setIdCurrency($model->idCurrency);

        $channel->setStreet($model->street);
        $channel->setPostCode($model->postCode);
        $channel->setCity($model->city);
        $channel->setCountryId($model->countryId);
        $channel->setContactEmail($model->contactEmail);
        $channel->setTelephone($model->telephone);
        $channel->setFax($model->fax);

        $channel->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $channel->setInvoiceClientName($model->invoiceClientName);
        $channel->setInvoiceCompanyName($model->invoiceCompanyName);
        $channel->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $channel->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $channel->setInvoiceAddressCity($model->invoiceAddressCity);
        $channel->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $channel->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $channel->setInvoiceAddressFax($model->invoiceAddressFax);
        $channel->setIsInvoiceWithTax($model->isInvoiceWithTax);

        $channel->setContactPersonName($model->contactPersonName);
        $channel->setContactPersonEmail($model->contactPersonEmail);
        $channel->setContactPersonTelephoneNumber($model->contactPersonTelephoneNumber);

        $channel->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $channel->setReportDeliveryEmail($model->reportDeliveryEmail);
        $channel->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);
        $channel->setInvoiceDeliveryNote($model->invoiceDeliveryNote);

        $channel->setInvoiceGoesToId($model->invoiceGoesToId);
        $channel->setReportGoesToId($model->reportGoesToId);
        $channel->setIsFixedReportGoesTo($model->isFixedReportGoesTo);
        $channel->setReportDeliveryId($model->reportDeliveryId);
        if ($model->ftpServer != null) {
            $channel->setFtpServerName($model->ftpServer->server);
            $channel->setFtpUsername($model->ftpServer->username);
            $channel->setFtpPassword($model->ftpServer->password);
        }
        $channel->setIdMarginGoesTo($model->idMarginGoesTo);
        $channel->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $channel->setIsPaymentWithTax($model->isPaymentWithTax);
        $channel->setPaymentNoTaxText($model->paymentNoTaxText);
        $channel->setInvoiceNoTaxText($model->invoiceNoTaxText);
        $channel->setIdNutrimeGoesTo($model->idNutrimeGoesTo);
        $channel->setIdRecipeBookDelivery($model->idRecipeBookDelivery);
        $channel->setIdRecipeBookGoesTo($model->idRecipeBookGoesTo);
        $channel->setCommissionPaymentWithTax($model->commissionPaymentWithTax);

        $channel->setIsReportDeliveryDownloadAccess($model->isReportDeliveryDownloadAccess);
        $channel->setIsReportDeliveryFtpServer($model->isReportDeliveryFtpServer);
        $channel->setIsReportDeliveryPrintedBooklet($model->isReportDeliveryPrintedBooklet);
        $channel->setIsReportDeliveryDontChargeBooklet($model->isReportDeliveryDontChargeBooklet);

        $channel->setAcquisiteurId1($model->acquisiteurId1);
        $channel->setCommission1($model->commission1);
        $channel->setYearlyDecayPercentage1($model->yearlyDecayPercentage1);
        $channel->setAcquisiteurId2($model->acquisiteurId2);
        $channel->setCommission2($model->commission2);
        $channel->setYearlyDecayPercentage2($model->yearlyDecayPercentage2);
        $channel->setAcquisiteurId3($model->acquisiteurId3);
        $channel->setCommission3($model->commission3);
        $channel->setYearlyDecayPercentage3($model->yearlyDecayPercentage3);

        $channel->setPartnerId($model->partnerId);
        $channel->setWebUserId($model->webUserId);

        $channel->setAccountName($model->accountName);
        $channel->setAccountNumber($model->accountNumber);
        $channel->setBankCode($model->bankCode);
        $channel->setBIC($model->bic);
        $channel->setIBAN($model->iban);
        $channel->setOtherNotes($model->otherNotes);

        // Rebrander
        $channel->setRebrandingNick($model->rebrandingNick);
        $channel->setHeaderFileName($model->headerFileName);
        $channel->setFooterFileName($model->footerFileName);
        $channel->setTitleLogoFileName($model->titleLogoFileName);
        $channel->setBoxLogoFileName($model->boxLogoFileName);

        $channel->setCompanyInfo($model->companyInfo);
        $channel->setLaboratoryInfo($model->laboratoryInfo);
        $channel->setContactInfo($model->contactInfo);
        $channel->setContactUs($model->contactUs);
        $channel->setLetterExtraText($model->letterExtraText);
        $channel->setClinicianInfoText($model->clinicianInfoText);
        $channel->setText7($model->text7);
        $channel->setText8($model->text8);
        $channel->setText9($model->text9);
        $channel->setText10($model->text10);
        $channel->setText11($model->text11);

        $channel->setDoctorsReporting($model->doctorsReporting);
        $channel->setAutomaticReporting($model->automaticReporting);
        $channel->setNoReporting($model->noReporting);
        $channel->setInvoicingAndPaymentInfo($model->invoicingAndPaymentInfo);

        $channel->setReportColor($model->reportColor);

        $channel->setCardTypeId($model->cardTypeId);
        $channel->setCardNumber($model->cardNumber);
        $channel->setCardExpiryDate($model->cardExpiryDate);
        $channel->setCardCvcCode($model->cardCvcCode);
        $channel->setCardFirstName($model->cardFirstName);
        $channel->setCardSurName($model->cardSurName);

        $channel->setRating($model->rating);
        $channel->setIdVisibleGroupReport($model->idVisibleGroupReport);
        $channel->setIdVisibleGroupProduct($model->idVisibleGroupProduct);
        $channel->setIdPreferredPayment($model->idPreferredPayment);
        $channel->setIsStopShipping($model->isStopShipping);
        $channel->setIdDncReportType($model->idDncReportType);
        $channel->setIdReportNameType($model->idReportNameType);
        $channel->setIsShowBodCoverPage($model->isShowBodCoverPage);
        $channel->setExpressShipment($model->expressShipment);

        $channel->setIsRecheckGeocoding(1);

        $channel->setIdLanguage($model->idLanguage);
        // $channel->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);

        $channel->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
        $channel->setOnlyShowDistributionChannelAddress($model->onlyShowDistributionChannelAddress);
        $channel->setDestroyInNumDays($model->destroyInNumDays);

        $channel->setCanViewShop($model->canViewShop);
        $channel->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $channel->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $channel->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $channel->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $channel->setIsCanViewSetting($model->isCanViewSetting);
        $channel->setIsDelayDownloadReport($model->isDelayDownloadReport);
        if ($model->isDelayDownloadReport == true) {
            $channel->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }
        $channel->setIsNotAccessReport($model->isNotAccessReport);
        $channel->setIsNotSendEmail($model->isNotAccessReport);

        $channel->setIsGenerateXmlVersion($model->isGenerateXmlVersion);
        $channel->setIsDeliveryViaSftp($model->isDeliveryViaSftp);
        $channel->setSftpHost($model->sftpHost);
        $channel->setSftpUsername($model->sftpUsername);
        $channel->setSftpPassword($model->sftpPassword);
        $channel->setSftpFolder($model->sftpFolder);
        $channel->setCustomBodAccountNumber($bodAccountNumber);
        $channel->setCustomBodBarcodeNumber($bodBarcodeNumber);

        $channel->setCanDownloadViaSftp($model->canDownloadViaSftp);
        $channel->setSftpDownloadHost($model->sftpDownloadHost);
        $channel->setSftpDownloadUsername($model->sftpDownloadUsername);
        $channel->setSftpDownloadPassword($model->sftpDownloadPassword);
        $channel->setSftpDownloadFolder($model->sftpDownloadFolder);

        $channel->setIdRecallGoesTo($model->idRecallGoesTo);
        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        // validation accounting code
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
        $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
        $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

        if ($dc || $partner || $acquisiteur) {
            // Validation
            $this->errors += ['Accounting code existed. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        /// validation id code
        if (strlen($model->idCode) != 5) {
            $this->errors += ['Id code must be 5 char. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($model->idCode);
        $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($model->idCode);
        if ($dc || $partner) {
            // Validation
            $this->errors += ['Id code existed. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }

        $channel->setIdCode($model->idCode);
        $channel->setAccountingCode($model->accountingCode);
        $em->persist($channel);
        $em->flush();

        // Save logo
        if (!empty($model->reportLogo['attachment'])) {
            $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource/DistributionChannels';
            $name = $channel->getId().'.'.pathinfo($model->reportLogo['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model->reportLogo['attachment']->move($attachmentDir, $name);
            $channel->setReportLogo($name);
            $em->persist($channel);
            $em->flush();
        }

        // Save email template
        Utils::saveEmailTemplates($this->container, $channel->getId(), $model->emailTemplates);

        parent::onSuccess();

        $defaultFormModel = $this->getDefaultFormModel();
        $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        $defaultFormModel->attachmentKey = $this->attachmentKey;
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $defaultFormModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
