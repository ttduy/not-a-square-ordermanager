<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Easy\ModuleBundle\Module\AbstractHook;

class EditMarketingStatusHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $controller->get('request')->get('id', 0);

        $data['linkDCComplaints'] = $manager->getUrl('leep_admin', 'distribution_channel', 'edit_complaint', 'edit', array('id' => $id));
        $data['linkDCFeedback'] = $manager->getUrl('leep_admin', 'distribution_channel', 'edit_feedback', 'edit', array('id' => $id));
        $data['linkCustomerComplaints'] = $manager->getUrl('leep_admin', 'customer_info', 'feature', 'viewComplaints', array('idDistributionChannel' => $id));
        $data['linkCustomerFeedback'] = $manager->getUrl('leep_admin', 'customer_info', 'feature', 'viewFeedback', array('idDistributionChannel' => $id));
        $data['linkCreateComplaint'] = $manager->getUrl('leep_admin', 'distribution_channel', 'log_complaint', 'edit', array('id' => $id));

        // DC Complaint
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as total')
            ->from('AppDatabaseMainBundle:DistributionChannelsComplaint', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id);
        $data['totalComplaint'] = intval($query->getQuery()->getSingleScalarResult());

        // DC Feedback
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as total')
            ->from('AppDatabaseMainBundle:DistributionChannelFeedback', 'p')
            ->andWhere('p.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id);
        $data['totalFeedback'] = intval($query->getQuery()->getSingleScalarResult());

        // Customer Complaints
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as total')
            ->from('AppDatabaseMainBundle:CustomerInfoComplaint', 'p')
            ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'c', 'WITH', 'p.idCustomerInfo = c.id')
            ->andWhere('c.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id);
        $data['totalCustomerComplaint'] = intval($query->getQuery()->getSingleScalarResult());

        // Customer feedback
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as total')
            ->from('AppDatabaseMainBundle:CustomerInfoFeedback', 'p')
            ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'c', 'WITH', 'p.idCustomerInfo = c.id')
            ->andWhere('c.idDistributionChannel = :idDistributionChannel')
            ->setParameter('idDistributionChannel', $id);
        $data['totalCustomerFeedback'] = intval($query->getQuery()->getSingleScalarResult());

    }
}
