<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

class EditMarketingStatusModel {
	public $idDC;
	public $attachment;
	public $attachmentKey;
	public $distributionChannel;
	public $statusList;
	public $rating;
}