<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Easy\ModuleBundle\Module\AbstractHook;

class DashboardHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $manager = $controller->get('easy_module.manager');

        $query = $em->createQueryBuilder();
        $query->select('p.complaintStatus, COUNT(p) as total')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere('p.complaintStatus > 0')
            ->groupBy('p.complaintStatus');
        $results = $query->getQuery()->getResult();

        $statistics = array();
        foreach ($results as $r) {
            $statistics[] = array(
                'id'    => $r['complaintStatus'],
                'name'  => $formatter->format($r['complaintStatus'], 'mapping', 'LeepAdmin_ComplaintStatus'),
                'total' => $r['total']
            );
        }
        $data['complaintStatistics'] = $statistics;

        $data['viewDcComplaint'] = $manager->getUrl('leep_admin', 'distribution_channel', 'dc', 'viewComplaint');

    }
}

