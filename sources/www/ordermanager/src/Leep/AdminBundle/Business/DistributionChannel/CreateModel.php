<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $rating;
    public $invoiceDeliveryNote;
    public $webLoginUsername;
    public $webLoginPassword;
    public $webLoginStatus;
    public $webLoginGoesTo;
    public $isWebLoginOptOut;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $sampleCanBeUsedForScience;
    public $isFutureResearchParticipant;

    public $attachment;
    public $attachmentKey;

    public $domain;
    public $distributionChannel;
    public $creationDate;
    public $uidNumber;
    public $typeId;
    public $priceCategoryId;
    public $genderId;
    public $title;
    public $firstName;
    public $surName;
    public $institution;
    public $idCompleteStatus;
    public $supplementInfo;
    public $idCurrency;

    public $street;
    public $postCode;
    public $city;
    public $countryId;
    public $contactEmail;
    public $telephone;
    public $fax;

    public $invoiceAddressIsUsed;
    public $invoiceCompanyName;
    public $invoiceClientName;
    public $invoiceAddressStreet;
    public $invoiceAddressPostCode;
    public $invoiceAddressCity;
    public $invoiceAddressIdCountry;
    public $invoiceAddressTelephone;
    public $invoiceAddressFax;
    public $isInvoiceWithTax;

    public $contactPersonName;
    public $contactPersonEmail;
    public $contactPersonTelephoneNumber;

    public $statusDeliveryEmail;
    public $reportDeliveryEmail;
    public $invoiceDeliveryEmail;

    public $invoiceGoesToId;
    public $reportGoesToId;
    public $isFixedReportGoesTo;
    public $reportDeliveryId;
    public $ftpServer;
    public $idMarginGoesTo;
    public $idMarginPaymentMode;
    public $isPaymentWithTax;
    public $paymentNoTaxText;
    public $invoiceNoTaxText;
    public $idNutrimeGoesTo;
    public $idRecipeBookDelivery;
    public $idRecipeBookGoesTo;
    public $commissionPaymentWithTax;

    public $acquisiteurId1;
    public $commission1;
    public $yearlyDecayPercentage1;
    public $acquisiteurId2;
    public $commission2;
    public $yearlyDecayPercentage2;
    public $acquisiteurId3;
    public $commission3;
    public $yearlyDecayPercentage3;

    public $partnerId;
    public $webUserId;

    public $accountName;
    public $accountNumber;
    public $bankCode;
    public $bic;
    public $iban;
    public $otherNotes;

    public $rebrandingNick;
    public $headerFileName;
    public $footerFileName;
    public $titleLogoFileName;
    public $boxLogoFileName;

    public $companyInfo;
    public $laboratoryInfo;
    public $contactInfo;
    public $contactUs;
    public $letterExtraText;
    public $clinicianInfoText;
    public $text7;
    public $text8;
    public $text9;
    public $text10;
    public $text11;

    public $doctorsReporting;
    public $automaticReporting;
    public $noReporting;
    public $invoicingAndPaymentInfo;

    public $reportLogo;
    public $reportColor;

    public $isReportDeliveryDownloadAccess;
    public $isReportDeliveryFtpServer;
    public $isReportDeliveryPrintedBooklet;
    public $isReportDeliveryDontChargeBooklet;

    public $cardTypeId;
    public $cardNumber;
    public $cardExpiryDate;
    public $cardCvcCode;
    public $cardFirstName;
    public $cardSurName;

    public $emailTemplates;

    public $idPreferredPayment;
    public $isStopShipping;
    public $idDncReportType;
    public $idReportNameType;
    public $isShowBodCoverPage;

    public $idVisibleGroupProduct;
    public $idVisibleGroupReport;

    public $idLanguage;
    // public $canCreateLoginAndSendStatusMessageToUser;

    public $canViewShop;
    public $canCustomersDownloadReports;
    public $destroyInNumDays;
    public $isCanViewYourOrder;
    public $isCanViewCustomerProtection;
    public $isCanViewRegisterNewCustomer;
    public $isCanViewPendingOrder;
    public $isCanViewSetting;
    public $onlyShowDistributionChannelAddress;
    public $isDelayDownloadReport;
    public $numberDateDelayDownloadReport;
    public $isNotAccessReport;
    public $isNotSendEmail;

    public $isGenerateXmlVersion;
    public $isDeliveryViaSftp;
    public $sftpHost;
    public $sftpUsername;
    public $sftpPassword;
    public $sftpFolder;

    public $canDownloadViaSftp;
    public $sftpDownloadHost;
    public $sftpDownloadUsername;
    public $sftpDownloadPassword;
    public $sftpDownloadFolder;

    public $idRecallGoesTo;
    public $accountingCode;
    public $idCode;
    // for validation
    public $container;

    public $expressShipment;
    public $customBodAccountNumber;
    public $customBodBarcodeNumber;

    public function isValid(ExecutionContext $context) {
        $helperCommon = $this->container->get('leep_admin.helper.common');

        if ($this->webLoginUsername != null) {
            $username = $this->webLoginUsername->username;
            if ($helperCommon->isWebLoginUsernameExisted($username)) {
                $context->addViolationAtSubPath('webLoginUsername', "This Web Login Username " . $username . " is already existed");
            }
        }
    }
}
