<?php 
	namespace Leep\AdminBundle\Business\DistributionChannel;

	use Easy\ModuleBundle\Module\AbstractHook;

	/**
	* 
	*/
	class EditHook extends AbstractHook
	{
		
		public function handle($controller, &$data, $options = array())
		{
			$mgr = $controller->get('easy_module.manager');
			$data['editDistributionChannelLink'] = $mgr->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit');
		}
	}
?>