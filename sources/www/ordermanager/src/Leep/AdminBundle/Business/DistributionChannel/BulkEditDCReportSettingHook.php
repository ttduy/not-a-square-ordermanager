<?php
namespace Leep\AdminBundle\Business\DistributionChannel;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class BulkEditDCReportSettingHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $idPartner = $controller->get('request')->get('id', 0);
        $idPartner = intval($idPartner);

        $arrDC = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->orderBy('p.distributionchannel', 'ASC');
        if ($idPartner) {
            $query->andWhere('p.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);

            $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($idPartner);
            $data['partner'] = $partner;
        }
        $result = $query->getQuery()->getResult();
        foreach ($result as $dc) {
            $arrDC[$dc->getId()] = $dc->getDistributionchannel();
        }
        $data['dcList'] = $arrDC;

        // Partners
        $partners = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Partner')->findBy(array(), array('partner' => 'ASC'));
        foreach ($result as $r) {
            $partners[$r->getId()] = $r->getPartner();
        }
        $data['partners'] = $partners;
        $data['idPartner'] = $idPartner;
    }
}
