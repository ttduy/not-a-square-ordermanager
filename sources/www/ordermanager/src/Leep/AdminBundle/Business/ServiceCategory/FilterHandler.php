<?php
namespace Leep\AdminBundle\Business\User;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        /*
        $builder->add('status', 'choice', array(
            'label'       => 'Status',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('AppAdmin_User_Status'),
            'empty_value' => false
        ));
        */
        return $builder;
    }
}