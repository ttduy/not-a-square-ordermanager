<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PriceCategories', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {        
        $model = new CopyModel();
        $model->targetPriceCategory = $entity->getPriceCategory();


        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $priceCategories = $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List');
        $idPriceCategory = $this->container->get('request')->query->get('id', 0);
        if (isset($priceCategories[$idPriceCategory])) {
            unset($priceCategories[$idPriceCategory]);
        }

        $builder->add('targetPriceCategory', 'label', array(
            'label'    => 'Target Price Category',
            'required' => false
        ));

        $builder->add('idPriceCategory', 'choice', array(
            'label'    => 'From Price Category',
            'required' => true,
            'choices'  => $priceCategories
        ));
        
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager('app_main');
        $idPriceCategory = $this->container->get('request')->query->get('id', 0);

        // Load price cat
        $categoryPrices = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CategoryPrices', 'app_main')->findBy(array(
            'pricecatid' => $model->idPriceCategory
        ));
        foreach ($result as $r) {
            $categoryPrices[$r->getCategoryId()] = $r;
        }

        $productPrices = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:Prices', 'app_main')->findBy(array(
            'pricecatid' => $model->idPriceCategory
        ));
        foreach ($result as $r) {
            $productPrices[$r->getProductId()] = $r;
        }

        // Update
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CategoryPrices', 'app_main')->findBy(array(
            'pricecatid' => $idPriceCategory
        ));
        foreach ($result as $r) {
            if (isset($categoryPrices[$r->getCategoryId()])) {
                $c = $categoryPrices[$r->getCategoryId()];
                $r->setPrice($c->getPrice());
                $r->setDCMargin($c->getDCMargin());
                $r->setPartMargin($c->getPartMargin());
            }
        }

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Prices', 'app_main')->findBy(array(
            'pricecatid' => $idPriceCategory
        ));
        foreach ($result as $r) {
            if (isset($productPrices[$r->getProductId()])) {
                $c = $productPrices[$r->getProductId()];
                $r->setPrice($c->getPrice());
                $r->setDCMargin($c->getDCMargin());
                $r->setPartMargin($c->getPartMargin());
            }
        }

        $em->flush();

        $this->messages[] = 'The price category has been copied';
    }
}
