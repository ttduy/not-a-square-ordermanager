<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->listOrder = 1;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('priceCategory', 'text', array(
            'label'    => 'Price Category',
            'required' => true
        ));
        $builder->add('listOrder', 'text', array(
            'label'    => 'List Order',
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr' => ['cols' => 50, 'rows' => 5]
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $priceCategory = new Entity\PriceCategories();
        $priceCategory->setPriceCategory($model->priceCategory);
        $priceCategory->setListOrder($model->listOrder);
        $priceCategory->setDescription($model->description);

        $em->persist($priceCategory);
        $em->flush();

        // Update category prices
        $categories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findAll();
        foreach ($categories as $category) {
            $categoryPrice = new Entity\CategoryPrices();
            $categoryPrice->setPriceCatId($priceCategory->getId());
            $categoryPrice->setCategoryId($category->getId());
            $categoryPrice->setPrice(0.0000);
            $categoryPrice->setDCMargin(0.0000);
            $categoryPrice->setPartMargin(0.0000);
            $em->persist($categoryPrice);
        }

        // Update product prices
        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findAll();
        foreach ($products as $product) {
            $productPrice = new Entity\Prices();
            $productPrice->setPriceCatId($priceCategory->getId());
            $productPrice->setProductId($product->getId());
            $productPrice->setPrice(0.0000);
            $productPrice->setDCMargin(0.0000);
            $productPrice->setPartMargin(0.0000);
            $em->persist($productPrice);
        }
        $em->flush();

        parent::onSuccess();
    }
}
