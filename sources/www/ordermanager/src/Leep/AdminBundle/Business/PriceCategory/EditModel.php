<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $priceCategory;
    public $listOrder;
    public $description;
}
