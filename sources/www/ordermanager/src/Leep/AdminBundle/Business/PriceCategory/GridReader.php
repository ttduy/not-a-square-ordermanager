<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('priceCategory', 'listOrder', 'description', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Price Category', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'List Order',     'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Description',    'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Action',         'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:PriceCategories', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.pricecategory', 'ASC');        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('priceCategoryModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'price_category', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'price_category', 'copy', 'edit', array('id' => $row->getId())), 'button-copy');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'price_category', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
