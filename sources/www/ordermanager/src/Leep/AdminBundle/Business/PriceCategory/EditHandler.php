<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PriceCategories', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->priceCategory = $entity->getPriceCategory();
        $model->listOrder = $entity->getListOrder();
        $model->description = $entity->getDescription();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('priceCategory', 'text', array(
            'label'    => 'Price Category',
            'required' => true
        ));
        $builder->add('listOrder', 'text', array(
            'label'    => 'List Order',
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr' => ['cols' => 50, 'rows' => 5]
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setPriceCategory($model->priceCategory);
        $this->entity->setListOrder($model->listOrder);
        $this->entity->setDescription($model->description);

        parent::onSuccess();
    }
}
