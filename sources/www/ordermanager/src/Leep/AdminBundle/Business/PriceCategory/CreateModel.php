<?php
namespace Leep\AdminBundle\Business\PriceCategory;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $priceCategory;
    public $listOrder;
    public $description;
}
