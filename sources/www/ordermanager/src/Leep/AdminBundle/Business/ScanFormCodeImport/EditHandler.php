<?php
namespace Leep\AdminBundle\Business\ScanFormCodeImport;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public $attachmentKey = 'a';
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormCodeImport', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $mgr = $this->container->get('easy_module.manager');

        $model = new EditModel();  
        $model->code = nl2br($entity->getCode());
        $url = $mgr->getUrl('leep_admin', 'scan_form', 'feature', 'downloadScanFormCodeImportPdf', array('id' => $entity->getId()));
        $model->code .= '<br/><br/><b><a target="_blank" href="'.$url.'">Download form</a></b>';

        $model->status = $formatter->format($entity->getStatus(), 'mapping', 'LeepAdmin_ScanFormCodeImport_Status');
        if ($entity->getStatus() == Constants::PENDING_FOR_SAMPLES_CAPTURING) {
            $model->status .= '<br/><b style="color: red">Please capture at least 10 images in the different poses</b>';
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('sectionCodeImport', 'section', array(
            'label'    => 'Code import',
            'property_path' => false
        ));
        $builder->add('code', 'label', array(
            'label'    => 'Code',
            'required' => false
        ));
        $builder->add('status', 'label', array(
            'label'    => 'Status',
            'required' => false
        ));

        $builder->add('sectionSamples', 'section', array(
            'label'    => 'Sample Images',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Sample images',
            'required' => false,
            'key'      => $this->entity->getAttachmentKey()
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        
        if ($this->entity->getStatus() == Constants::PENDING_FOR_SAMPLES_CAPTURING) {
            $numImages = 0;
            $directory = $this->container->getParameter('attachment_dir').'/'.$this->entity->getAttachmentKey();
            if ($handle = opendir($directory)) {
                while (($file = readdir($handle)) !== false){
                    if (!in_array($file, array('.', '..')) && !is_dir($directory.$file)) {
                        $numImages++;
                    } 
                }
            }

            if ($numImages < 10) {
                $this->errors[] = "Please capture at least 10 images";
                return false;
            }

            // If more than 10 images
            $this->entity->setStatus(Constants::PENDING_FOR_PROCESSING);
            $em->flush();
            $this->reloadForm();
        }
        
        
    }
}
