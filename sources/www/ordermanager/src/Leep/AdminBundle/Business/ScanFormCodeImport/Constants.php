<?php
namespace Leep\AdminBundle\Business\ScanFormCodeImport;

class Constants {
    const PENDING_FOR_SAMPLES_CAPTURING  = 1;
    const PENDING_FOR_PROCESSING     = 2;
    const PENDING_FOR_NOISE_REMOVAL  = 3;
    const PENDING_FOR_LEARNING       = 4;

    public static function getScanFormImportStatus() {
        return array(
            self::PENDING_FOR_SAMPLES_CAPTURING => 'Pending for sample capturing',
            self::PENDING_FOR_PROCESSING    => 'Pending for pre-processing',
            self::PENDING_FOR_NOISE_REMOVAL => 'Pending for noise removal',
            self::PENDING_FOR_LEARNING      => 'Pending for learning'
        );
    }
}