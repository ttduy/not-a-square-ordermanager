<?php
namespace Leep\AdminBundle\Business\ScanFormCodeImport;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('code', 'textarea', array(
            'label'    => 'Code',
            'required' => true,
            'attr'     => array(
                'rows'  => 10, 'cols' => 70
            )
        ));        
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $codes = explode("\n", $model->code);
        $sampleCodes = array();
        foreach ($codes as $code) {
            if (trim($code) != '') {
                $sampleCodes[] = trim($code);
            }
        }

        $this->validateCode($sampleCodes);

        if (empty($this->errors)) {
            $attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();

            $scanFormCodeImport = new Entity\ScanFormCodeImport();
            $scanFormCodeImport->setCode(implode("\n", $sampleCodes));
            $scanFormCodeImport->setAttachmentKey($attachmentKey);
            $scanFormCodeImport->setStatus(Constants::PENDING_FOR_SAMPLES_CAPTURING);
            $em->persist($scanFormCodeImport);
            $em->flush();

            $this->newId = $scanFormCodeImport->getId();

            parent::onSuccess();
        }        
    }

    public function validateCode($sampleCodes) {
        // Must have some code
        if (empty($sampleCodes)) {
            $this->errors[] = 'Please put some code';
            return;
        }

        // Code length should be 12
        foreach ($sampleCodes as $code) {
            if (strlen($code) != 10) {
                $this->errors[] = 'Code length must be 10, wrong: '.$code;
                return;
            }
        }
        
        // Maximum code
        $maxAllowedCode = 7;
        if (count($sampleCodes) > $maxAllowedCode) {
            $this->errors[] = 'Maximum number of code per batch: '.$maxAllowedCode;
            return;
        }

        // Code shouldn't be duplicated
        foreach ($sampleCodes as $i => $codeI) {
            foreach ($sampleCodes as $j => $codeJ) {
                if ($i != $j && strcmp($codeI, $codeJ) == 0) {
                    $this->errors[] = 'Code shouldn\'t be duplicated';
                    return;
                }
            }
        }

        
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        foreach ($sampleCodes as $code) {
            // Code shouldn't be existed
            $r = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormCode')->findOneByCode($code);
            if ($r) {
                $this->errors[] = "Code ".$code." is already existed";
                return;
            }

            // Code shouldn't be in another sample import 
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ScanFormCodeImport', 'p')
                ->andWhere('p.code LIKE :code')
                ->setParameter('code', '%'.$code.'%');
            $r = $query->getQuery()->getResult();
            if ($r) {
                $this->errors[] = "Code ".$code." is already in another code import";
                return;
            }
        }

    }
}
