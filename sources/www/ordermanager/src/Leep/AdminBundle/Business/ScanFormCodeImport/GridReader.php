<?php
namespace Leep\AdminBundle\Business\ScanFormCodeImport;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('code', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Code',   'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Status', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ScanFormCodeImportGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFormCodeImport', 'p');
        if (trim($this->filters->code) != '') {
            $queryBuilder->andWhere('p.code LIKE :code')
                ->setParameter('code', '%'.trim($this->filters->code).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'scan_form_code_import', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'scan_form_code_import', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellCode($row) {
        return nl2br($row->getCode());
    }
}
