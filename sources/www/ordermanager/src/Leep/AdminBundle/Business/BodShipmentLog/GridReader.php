<?php
namespace Leep\AdminBundle\Business\BodShipmentLog;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;
class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'idCustomer', 'fileName', 'logTime', 'logText', 'orderNumber', 'reportQueueDescription');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'id',            'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Customer',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'File',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Time',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Text',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Order Number',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Description',         'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_BodShipmentLogGridReader');
    }

    public function buildQuery($queryBuilder) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $queryBuilder
            ->select('p.id as id', 'p.idCustomer as idCustomer', 'p.idBodShipment as idBodShipment',
            'p.fileName as fileName', 'p.logTime as logTime',
            'p.logText as logText', 'p.idReportQueue as idReportQueue',
            'p.reportQueueDescription as reportQueueDescription', 'p.reportQueueInput as reportQueueInput',
            'p.ftpUploadStatus as ftpUploadStatus', 'p.orderNumber as orderNumber',
            'c.firstname as firstName', 'c.surname as surName')
            ->from('AppDatabaseMainBundle:ShipmentLog', 'p')
            ->innerJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'c.id = p.idCustomer');
            
        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.orderNumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber).'%');
        }
        if (trim($this->filters->fileName) != '')  {
            $queryBuilder->andWhere('p.fileName LIKE :fileName')
                ->setParameter('fileName', '%'.trim($this->filters->fileName).'%');
        }
        if (trim($this->filters->description) != '')  {
            $queryBuilder->andWhere('p.reportQueueDescription LIKE :description')
                ->setParameter('description', '%'.trim($this->filters->description).'%');
        }
        if (trim($this->filters->firstName) != '')  {
            $queryBuilder->andWhere('c.firstname LIKE :firstName')
                ->setParameter('firstName', '%'.trim($this->filters->firstName).'%');
        }
        if (trim($this->filters->surName) != '')  {
            $queryBuilder->andWhere('c.surname LIKE :surName')
                ->setParameter('surName', '%'.trim($this->filters->surName).'%');
        }
    }
    public function buildCellIdCustomer($row) {
        return $row['firstName']. ' '. $row['surName'];
    }
    public function buildCelllogText($row) {
        if ($row['ftpUploadStatus'] != Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_DONE) {
            return '<span style="color: red;">'. $row['logText']. '</span>';
        }
        return $row['logText'];
    }
    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.logTime', 'DESC');
    }
}
