<?php
namespace Leep\AdminBundle\Business\BodShipmentLog;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idWebUser = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));
        $builder->add('fileName', 'text', array(
            'label'    => 'File name',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => [
                'rows'     => 3,
                'style'    => 'width: 75%'
                ]
        ));
        return $builder;
    }
}
