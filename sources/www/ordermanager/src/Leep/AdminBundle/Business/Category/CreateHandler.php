<?php
namespace Leep\AdminBundle\Business\Category;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('categoryName', 'text', array(
            'label'    => 'Category Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
        $builder->add('materialCost', 'text', array(
            'label'    => 'Material Costs excl Reports',
            'required' => false
        ));
        $builder->add('productionCost', 'text', array(
            'label'    => 'Production Costs',
            'required' => false
        ));
        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));
        $builder->add('productTypeDE', 'choice', array(
            'label'    => 'Germany Product Type',
            'required' => false,
            'choices'  => \Leep\AdminBundle\Business\Product\Constant::getProductType($this->container)
        ));
        $builder->add('productTypeAT', 'choice', array(
            'label'    => 'Austria Product Type',
            'required' => false,
            'choices'  => \Leep\AdminBundle\Business\Product\Constant::getProductType($this->container)
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $category = new Entity\ProductCategoriesDnaPlus();
        $category->setCategoryName($model->categoryName);
        $category->setCodename($model->codename);
        $category->setMaterialCostsExclReports($model->materialCost);
        $category->setProductionCosts($model->productionCost);
        $category->setProductTypeGermany($model->productTypeDE);
        $category->setProductTypeAustria($model->productTypeAT);
        $em->persist($category);
        $em->flush();

        foreach ($model->products as $productId) {
            $categoryProd = new Entity\CatProdLink();
            $categoryProd->setCategoryId($category->getId());
            $categoryProd->setProductId($productId);
            $em->persist($categoryProd);
        }
        $em->flush();

        // Update category prices
        $priceCategories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PriceCategories')->findAll();
        foreach ($priceCategories as $priceCategory) {
            $categoryPrice = new Entity\CategoryPrices();
            $categoryPrice->setPriceCatId($priceCategory->getId());
            $categoryPrice->setCategoryId($category->getId());
            $categoryPrice->setPrice(0.0000);
            $categoryPrice->setDCMargin(0.0000);
            $categoryPrice->setPartMargin(0.0000);
            $em->persist($categoryPrice);
        }
        $em->flush();


        parent::onSuccess();
    }
}
