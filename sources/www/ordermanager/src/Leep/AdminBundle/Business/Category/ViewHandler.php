<?php
namespace Leep\AdminBundle\Business\Category;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $id = $this->container->get('request')->query->get('id', 0);
        $category = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findOneById($id);
        $data[] = $this->add('Category Name', $category->getCategoryName());        
        $data[] = $this->add('Material Cost (Excl reports)', $category->getMaterialCostsExclReports());
        $data[] = $this->add('Product Cost', $category->getProductionCosts());        
        
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $catProds = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CatProdLink')->findBy(array('categoryid' => $id));
        foreach ($catProds as $catProd) {
            $name = $mapping->getMappingTitle('LeepAdmin_Product_List', $catProd->getProductId());
            $builder->addLink($name, $mgr->getUrl('leep_admin', 'product', 'view', 'view', array('id' => $catProd->getProductId())));            
        }
        $data[] = $this->add('Products', $builder->getHtml());
        return $data;
    }
}
