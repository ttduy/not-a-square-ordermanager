<?php
namespace Leep\AdminBundle\Business\Category;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->categoryName = $entity->getCategoryName();
        $model->codename = $entity->getCodename();
        $model->materialCost = $entity->getMaterialCostsExclReports();
        $model->productionCost = $entity->getProductionCosts();
        $model->productTypeDE = $entity->getProductTypeGermany();
        $model->productTypeAT = $entity->getProductTypeAustria();
        $model->products = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from("AppDatabaseMainBundle:CatProdLink", 'p')
            ->andWhere('p.categoryid = '.$entity->getId());
        $products = $query->getQuery()->getResult();
        foreach ($products as $pLink) {
            $model->products[] = $pLink->getProductId();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('categoryName', 'text', array(
            'label'    => 'Category Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
        $builder->add('materialCost', 'text', array(
            'label'    => 'Material Costs excl Reports',
            'required' => false
        ));
        $builder->add('productionCost', 'text', array(
            'label'    => 'Production Costs',
            'required' => false
        ));
        $builder->add('productTypeDE', 'choice', array(
            'label'    => 'Germany Product Type',
            'required' => false,
            'choices'  => Business\Product\Constant::getProductType($this->container)
        ));
        $builder->add('productTypeAT', 'choice', array(
            'label'    => 'Austria Product Type',
            'required' => false,
            'choices'  => Business\Product\Constant::getProductType($this->container)
        ));
        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setCategoryName($model->categoryName);
        $this->entity->setCodename($model->codename);
        $this->entity->setMaterialCostsExclReports($model->materialCost);
        $this->entity->setProductionCosts($model->productionCost);
        $this->entity->setProductTypeGermany($model->productTypeDE);
        $this->entity->setProductTypeAustria($model->productTypeAT);

        // update product
        $filters = array('categoryid' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CatProdLink', $filters);
        foreach ($model->products as $productId) {
            $categoryProd = new Entity\CatProdLink();
            $categoryProd->setCategoryId($this->entity->getId());
            $categoryProd->setProductId($productId);
            $em->persist($categoryProd);
        }
        $em->flush();
        parent::onSuccess();
    }
}
