<?php
namespace Leep\AdminBundle\Business\Category;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('categoryName', 'materialCostsExclReports', 'productTypeDE', 'productTypeAT', 'productionCosts', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Category Name',               'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Material Costs excl Reports', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Product Type Germany',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Production Type Austria',     'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Production Costs',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }
    public function buildCellProductTypeDE($row) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $type = $em->getRepository('AppDatabaseMainBundle:ProductType', 'app_main')->findOneById($row->getProductTypeGermany());
        if ($type) {
            return $type->getName();
        }
        return '';

    }
    public function buildCellProductTypeAT($row) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $type = $em->getRepository('AppDatabaseMainBundle:ProductType', 'app_main')->findOneById($row->getProductTypeAustria());
        if ($type) {
            return $type->getName();
        }
        return '';

    }
    public function buildCellTaxGermany($row) {
        if ($row->getTaxGermany())
            return $row->getTaxGermany().' %';
        else
            return '0 %';
    }
    public function buildCellTaxAustria($row) {
        if ($row->getTaxAustria())
            return $row->getTaxAustria().' %';
        else
            return '0 %';
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ProductCategoriesDnaPlus', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.categoryname', 'ASC');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'category', 'view', 'view', array('id' => $row->getId())), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'category', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'category', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
