<?php
namespace Leep\AdminBundle\Business\Category;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $categoryName;
    public $codename;
    public $materialCost;
    public $productionCost;
    public $taxGermany;
    public $taxAustria;
    public $products;
    public $productTypeDE;
    public $productTypeAT;
}
