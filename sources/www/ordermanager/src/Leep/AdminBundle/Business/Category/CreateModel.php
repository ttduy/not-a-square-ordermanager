<?php
namespace Leep\AdminBundle\Business\Category;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $categoryName;
    public $codename;
    public $materialCost;
    public $productionCost;
    public $products;
    public $productTypeDE;
    public $productTypeAT;
}
