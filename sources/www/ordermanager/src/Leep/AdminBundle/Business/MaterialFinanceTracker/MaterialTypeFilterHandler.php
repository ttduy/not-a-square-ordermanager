<?php
namespace Leep\AdminBundle\Business\MaterialFinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class MaterialTypeFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new MaterialTypeFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));
        $model->sortDirection = 'asc';

        return $model;
    }



    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));
        $builder->add('materialTypes', 'choice', array(
            'label'    => 'Material types',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_MaterialType_List'),
            'empty_value' => false,
            'attr'     => array(
                'style'  => 'height: 130px'
            )
        ));
        $builder->add('sortBy', 'choice', array(
            'label'    => 'Sort by',
            'required' => false,
            'choices'  => array('name' => 'Material type')
        ));
        $builder->add('sortDirection', 'choice', array(
            'label'    => 'Sort direction',
            'required' => false,
            'choices'  => array('asc' => 'Ascending order', 'desc' => 'Descending order'),
            'empty_value' => false
        ));
        return $builder;
    }
}