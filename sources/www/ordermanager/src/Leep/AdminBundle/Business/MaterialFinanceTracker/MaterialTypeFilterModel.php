<?php
namespace Leep\AdminBundle\Business\MaterialFinanceTracker;

class MaterialTypeFilterModel {
    public $year;
    public $materialTypes;
    public $sortBy;
    public $sortDirection;
}