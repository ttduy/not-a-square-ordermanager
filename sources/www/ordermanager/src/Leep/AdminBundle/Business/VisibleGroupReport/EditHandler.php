<?php
namespace Leep\AdminBundle\Business\VisibleGroupReport;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupReport')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        // get related reports
        $model->reports = array();
        $reports = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupReportReport')->findBy(array('idVisibleGroupReport' => $entity->getId()));
        foreach ($reports as $report) {
            $model->reports[] = $report->getIdReport();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('reports', 'choice', array(
                'label'     => 'Reports',
                'required'  => false,
                'multiple'  => 'multiple',
                'choices'   => $mapping->getMapping('LeepAdmin_Report_List'),
                'attr'      => array(
                    'style'   => 'height: 400px; min-height: 400px'
                )
            )
        );
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);

        // clean reports
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:VisibleGroupReportReport', 'p')
                ->andWhere('p.idVisibleGroupReport = :idVisibleGroupReport')
                ->setParameter('idVisibleGroupReport', $this->entity->getId())
                ->getQuery()->execute();

        // create Visible Group - Report - Report
        if (!empty($model->reports)) {
            // check reports
            foreach($model->reports as $idReport) {
                $report = new Entity\VisibleGroupReportReport();
                $report->setIdVisibleGroupReport($this->entity->getId());
                $report->setIdReport($idReport);
                $em->persist($report);
                $em->flush();
            }
        }

        parent::onSuccess();
    }
}
