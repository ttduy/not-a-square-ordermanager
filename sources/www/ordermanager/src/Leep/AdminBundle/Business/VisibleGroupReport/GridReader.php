<?php
namespace Leep\AdminBundle\Business\VisibleGroupReport;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;

    public function getColumnMapping() {
        return array('name', 'reports', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Reports',     'width' => '80%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:VisibleGroupReport', 'p')
            ->orderBy('p.id', 'DESC');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'visible_group_report', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'visible_group_report', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellReports($row) {
        $mapping = $this->container->get('easy_mapping');
        $html = '';

        $reports = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupReportReport')->findBy(array('idVisibleGroupReport' => $row->getId()));

        foreach ($reports as $report) {
            if ($html) {
                $html .= '<br/>';
            }

            $html .= $mapping->getMappingTitle('LeepAdmin_Report_List', $report->getIdReport());
        }

        return $html;
    }
}
