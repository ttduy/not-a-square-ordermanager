<?php 
namespace Leep\AdminBundle\Business\VisibleGroupReport;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $vgr = $doctrine->getRepository('AppDatabaseMainBundle:VisibleGroupReport')->findOneById($id);
        $em->remove($vgr);
        $em->flush();

        $queryDeleteReport = $em->createQueryBuilder();
        $queryDeleteReport->delete('AppDatabaseMainBundle:VisibleGroupReportReport', 'p')
                                ->andWhere('p.idVisibleGroupReport = :idVisibleGroupReport')
                                ->setParameter('idVisibleGroupReport', $id)
                                ->getQuery()
                                ->execute();

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'visible_group_report', 'grid', 'list', array(
            'id' => $this->container->get('request')->query->get('id', 0)
        ));

        return new RedirectResponse($url);
    }
}