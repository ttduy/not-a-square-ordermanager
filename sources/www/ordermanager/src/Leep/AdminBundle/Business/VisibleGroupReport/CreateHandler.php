<?php
namespace Leep\AdminBundle\Business\VisibleGroupReport;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('reports', 'choice', array(
                'label'     => 'Reports',
                'required'  => false,
                'multiple'  => 'multiple',
                'choices'   => $mapping->getMapping('LeepAdmin_Report_List'),
                'attr'      => array(
                    'style'   => 'height: 400px; min-height: 400px'
                )
            )
        );
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // create Visible Group - Report
        $group = new Entity\VisibleGroupReport();
        $group->setName($model->name);

        $em->persist($group);
        $em->flush();

        // create Visible Group - Report - Report
        if (!empty($model->reports)) {
            foreach($model->reports as $idReport) {
                $report = new Entity\VisibleGroupReportReport();
                $report->setIdVisibleGroupReport($group->getId());
                $report->setIdReport($idReport);
                $em->persist($report);
                $em->flush();
            }
        }

        parent::onSuccess();
    }
}
