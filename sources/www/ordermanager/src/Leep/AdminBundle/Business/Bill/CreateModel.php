<?php
namespace Leep\AdminBundle\Business\Bill;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;
    public $billingTarget;
    public $billDate;
    public $name;
    public $number;
    public $idTarget;

    public function isValid(ExecutionContext $context) {
        $em = $this->container->get('doctrine')->getEntityManager();

        if ($this->billingTarget['idType'] == 0) {
            $context->addViolationAtSubPath('billingTarget[idType]', "Please select bill type");
        } else {
            $billTargetId = 'idTarget'.$this->billingTarget['idType'];
            if ($this->billingTarget[$billTargetId] == 0) {
                $context->addViolationAtSubPath('billingTarget['.$billTargetId.']', "Please select choose one");
            }
        }

        if (trim($this->name) != '') {
            $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneByName($this->name);
            if (!empty($bill)) {
                $context->addViolationAtSubPath('name', "Bill Name '".$this->name."' is already existed");
            }
        }

        if (trim($this->number) != '') {
            $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneByNumber($this->number);
            if (!empty($bill)) {
                $context->addViolationAtSubPath('number', "Bill Number '".$this->name."' is already existed");
            }
        }

    }
}
