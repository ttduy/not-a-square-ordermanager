<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $invoiceSubTypes;
    public $billingHandler;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        $subTypes = Constant::getSubBillTypes();
        $this->invoiceSubTypes = $subTypes[Constant::TYPE_INVOICE];

        $this->billingHandler = $container->get('leep_admin.billing.business.billing_handler');
    }
    public function getColumnMapping() {
        return array('billDate', 'idType', 'name', 'target', 'billAmount', 'currentStatus', 'reminder', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Date',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Type',             'width' => '13%', 'sortable' => 'true'),
            array('title' => 'Name / Number',    'width' => '18%', 'sortable' => 'false'),
            array('title' => 'Target',           'width' => '18%', 'sortable' => 'false'),
            array('title' => 'Amount',           'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Status',           'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Reminder',         'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Action',           'width' => '10%', 'sortable' => 'false')
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.billDate';
        $this->columnSortMapping[] = 'p.idType';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.billAmount';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        //$queryBuilder->orderBy('p.invoiceDate', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_BillReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Bill', 'p');

        if ($this->filters->idType != 0) {
            $subTypes = Constant::getSubBillTypes();
            if (isset($subTypes[$this->filters->idType])) {
                $queryBuilder->andWhere('p.idType in (:types)')
                    ->setParameter('types', $subTypes[$this->filters->idType]);
            }
            else {
                $queryBuilder->andWhere('p.idType = :idType')
                    ->setParameter('idType', $this->filters->idType);
            }
        }

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->number) != '') {
            $queryBuilder->andWhere('p.number LIKE :number')
                ->setParameter('number', '%'.trim($this->filters->number).'%');
        }

        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.billDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }

        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.billDate <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }

        if (!empty($this->filters->currentStatus)) {
            $queryBuilder->andWhere('p.status = :currentStatus')
                ->setParameter('currentStatus', $this->filters->currentStatus);
        }

        if ($this->filters->idDistributionChannel != 0) {
            $queryBuilder->andWhere('p.idTarget = :idDistributionChannel')
                ->andWhere('p.idType in (:billTypes)')
                ->setParameter('idDistributionChannel', $this->filters->idDistributionChannel)
                ->setParameter('billTypes', array(
                    Constant::TYPE_INVOICE_DISTRIBUTION_CHANNEL,
                    Constant::TYPE_PAYMENT_DISTRIBUTION_CHANNEL
                ));
        }

        if ($this->filters->idPartner != 0) {
            $queryBuilder->andWhere('p.idTarget = :idPartner')
                ->andWhere('p.idType in (:billTypes)')
                ->setParameter('idPartner', $this->filters->idPartner)
                ->setParameter('billTypes', array(
                    Constant::TYPE_INVOICE_PARTNER,
                    Constant::TYPE_PAYMENT_PARTNER
                ));
        }

        if ($this->filters->idAcquisiteur != 0) {
            $queryBuilder->andWhere('p.idTarget = :idAcquisiteur')
                ->andWhere('p.idType in (:billTypes)')
                ->setParameter('idAcquisiteur', $this->filters->idAcquisiteur)
                ->setParameter('billTypes', array(
                    Constant::TYPE_PAYMENT_ACQUISITEUR,
                    Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR,
                ));
        }

        if (!empty($this->filters->orderNumber)) {
            $queryBuilder
                ->leftJoin('AppDatabaseMainBundle:BillingInvoiceCustomer', 'bic', 'WITH', 'p.idTarget = bic.id')
                ->andWhere('bic.orderNumber = :orderNumber')
                ->andWhere('p.idType in (:billTypes)')
                ->setParameter('orderNumber', trim($this->filters->orderNumber))
                ->setParameter('billTypes', array(
                    Constant::TYPE_INVOICE_CUSTOMER
                ));
        }
    }

    public function buildCellName($row) {
        return $row->getName().'<br/>'.$row->getNumber();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($row->getIdActiveBillFile() != 0) {
            $builder->addNewTabButton('Download pdf', $mgr->getUrl('leep_admin', 'bill', 'feature', 'downloadBillFile', array('id' => $row->getIdActiveBillFile())), 'button-pdf');
        }
        if ($row->getIsCancel() == false) {
            $linkAfter = $mgr->getUrl('leep_admin', 'bill', 'feature', 'generateBillPdf', array('idBill' => $row->getId()));
            $linkDownload = $mgr->getUrl('leep_admin', 'bill', 'feature', 'downloadCancelBillFile', array('idBill' => $row->getId()));
            $builder->addConfirmButtonAction('Cancelation', $mgr->getUrl('leep_admin', 'bill', 'feature', 'cancelBill', array('id' => $row->getId())), 'button-cancel', 'Do you really want to cancel this item?\nThis process can not be undone!', 'Yes, do it!', 'No, cancel', $linkAfter, $linkDownload);
        }
        if ($helperSecurity->hasPermission('billingModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'bill', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'bill', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }


    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }

    public function buildCellBillAmount($row) {
        return "&euro;".number_format(floatval($row->getBillAmount()), 2);
    }


    public function buildCellReminder($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        if (in_array($row->getIdType(), $this->invoiceSubTypes)) {
            if ($row->getIdActiveBillFile() != 0) {
                $builder->addNewTabButton('ReminderNotice', $mgr->getUrl('leep_admin', 'bill', 'feature', 'generateReminderNotice', array('id' => $row->getId())), 'button-activity');
            }
            return intval($row->getNumberReminderCount()).'&nbsp;'.$builder->getHtml();
        }
        return '';
    }

    public function buildCellCurrentStatus($row) {
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Bill_Status', $row->getStatus());
        $age = $this->getAge($row->getStatusDate());

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('billingModify')) {
            $builder->addButton('Edit status', $mgr->getUrl('leep_admin', 'bill', 'edit_status', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        return $builder->getHtml().'&nbsp;'.(($age === '') ? '': "(${age}d) ").$status;
    }

    public function buildCellTarget($row) {
        return $this->billingHandler->getBillingHandlerByType($row->getIdType())->getTargetName($row->getIdType(), $row->getIdTarget());
    }
}
