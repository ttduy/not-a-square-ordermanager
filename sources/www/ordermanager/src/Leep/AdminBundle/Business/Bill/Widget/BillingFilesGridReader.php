<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class BillingFilesGridReader extends AppGridDataReader {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        return array('generateDate', 'name', 'billAmount', 'action');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';
        $this->columnSortMapping[] = 'p.generateDate';
        $this->columnSortMapping[] = 'p.name';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Generate time',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Name',                'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Amount',              'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',              'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_BillingFilesGridReader');
        return null;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.generateDate', 'DESC');
        $queryBuilder->setMaxResults(1);
    }

    public function buildQuery($queryBuilder) {
        $request = $this->container->get('request');
        $idBill = $request->get('idBill', 0);

        $queryBuilder->select('p')
            ->from('AppDatabaseMainBundle:BillFile', 'p')
            ->andWhere('p.idBill = :idBill')
            ->setParameter('idBill', $idBill);
    }

    public function buildCellBillAmount($row) {
        $html = '';
        if ($row->getIsActive()) {
            $html .= '<input type="hidden" class="row_color" color-code="#f1dbdb" />';
        }
        $html .= '€'.number_format($row->getBillAmount(), 2);
        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($row->getIdBill());

        $builder->addNewTabButton('Download pdf', $mgr->getUrl('leep_admin', 'bill', 'feature', 'downloadBillFile', array('id' => $row->getId())), 'button-pdf');

        // if ($bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_DISTRIBUTION_CHANNEL ||  $bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_PARTNER ||
        //     $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_PARTNER || $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_DISTRIBUTION_CHANNEL ) {
        // }
        $builder->addNewTabButton('Download csv', $mgr->getUrl('leep_admin', 'bill', 'feature', 'downloadCSVExport', array('id' => $row->getId())), 'button-csv');

        $builder->addNewTabButton('Set as active', "javascript:setAsActivePdf('".$row->getIdBill()."', '".$row->getId()."')", 'button-redo');

        return $builder->getHtml();
    }
}
