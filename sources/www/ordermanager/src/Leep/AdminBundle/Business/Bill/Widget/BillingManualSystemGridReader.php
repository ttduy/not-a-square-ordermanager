<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class BillingManualSystemGridReader extends AppGridDataReader {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        return array('orderDate', 'orderNumber', 'name', 'action');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';
        $this->columnSortMapping[] = 'p.generateDate';
        $this->columnSortMapping[] = 'p.name';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Date',                        'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Order Number',                'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Name',                        'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function postQueryBuilder($queryBuilder) {
        // $queryBuilder->orderBy('p.generateDate', 'DESC');
        $queryBuilder->setMaxResults(1);
    }

    public function buildQuery($queryBuilder) {
        $request = $this->container->get('request');
        $idBill = $request->get('idBill', 0);

        $queryBuilder->select('p')
            ->from('AppDatabaseMainBundle:Bill', 'p')
            ->andWhere('p.id = :idBill')
            ->setParameter('idBill', $idBill);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        return $builder->getHtml();
    }
}
