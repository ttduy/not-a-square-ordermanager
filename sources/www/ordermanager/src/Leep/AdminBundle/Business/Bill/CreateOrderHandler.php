<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateOrderHandler extends AppEditHandler {
    public $newId = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return [];
    }
    public function convertToFormModel($entity) {
        $model = new CreateOrderModel();
        $today = new \Datetime();
        $model->date = $today;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // new customer info
        $builder->add('sectionOrderInfo', 'section', array(
            'label'    => 'Manual System',
            'property_path' => false
        ));

        $builder->add('date', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));

        $builder->add('price', 'text', array(
            'label'    => 'Price',
            'required' => false
        ));

        $builder->add('text', 'text', array(
            'label'    => 'Product',
            'required' => false
        ));

        $builder->add('customer', 'text', array(
            'label'    => 'Customer',
            'required' => false
        ));

        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'  => [
                'rows'  => 5,
                'style' => 'width: 22.5%'
            ]
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        parent::onSuccess();
    }
}
