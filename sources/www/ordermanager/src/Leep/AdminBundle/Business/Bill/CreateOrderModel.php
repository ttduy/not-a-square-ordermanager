<?php
namespace Leep\AdminBundle\Business\Bill;

use Symfony\Component\Validator\ExecutionContext;

class CreateOrderModel {
    public $container;
    public $date;
    public $price;
    public $text;
    public $description;
    public $customer;
}
