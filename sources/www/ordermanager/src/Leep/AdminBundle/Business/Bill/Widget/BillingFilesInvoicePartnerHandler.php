<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesInvoicePartnerHandler extends BillingFilesInvoiceHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'invoice_partner';
        $this->idReport = $config->getIdBillingReportInvoicePartner();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingInvoicePartner';
        $this->tableEntriesLinkField = 'idBillingInvoicePartner';
        $this->tableEntriesCode = 'BillingInvoicePartnerEntry';
        $this->idBillGroup = 'invoice';
    }

    public function loadReportDataClient(&$data, $bill) {
        $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($bill->getIdTarget());
        $this->loadReportDataForDcOrPartner($data, $partner);
    }

    public function generateBillingName($bill) {
        $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$partner->getPartner().' - '.$bill->getNumber();
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingInvoicePartner = $this->em->getRepository('AppDatabaseMainBundle:BillingInvoicePartner')->findOneByIdBill($bill->getId());
        if ($billingInvoicePartner) {
            $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($billingInvoicePartner->getIdPartner());
            if ($partner) {
                $result['invoiceRestructure'] = $partner->getAccountingCode();
            }
        }

        return $result;
    }
}
