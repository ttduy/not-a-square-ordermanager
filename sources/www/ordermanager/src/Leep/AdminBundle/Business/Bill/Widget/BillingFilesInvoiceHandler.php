<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesInvoiceHandler extends BillingFilesHandler {
    public function generateCSVData($bill, $reportData) {
        $result = [
            'type' =>                   'invoice',
            'invoiceNumber' =>          $reportData['invoiceNumber'],
            'invoiceDate' =>            $reportData['billDateCsvFormat'],
            'totalAmountWithTax' =>     number_format($reportData['rawTotalAmountWithTax'], 2),
            'totalTaxAmount' =>         number_format($reportData['rawTotalTaxAmount'], 2),
            'invoiceNumber' =>          $reportData['billNumber'],
            'uid' =>                    $reportData['clientUid'],
            'invoiceRestructure' =>     '',
            'entries' =>                []
        ];

        foreach ($reportData['billEntries'] as $entry) {
            if ($entry['invoicingCode'] === false) {
                continue;
            }

            $result['entries'][] = [
                'invoicingCode' =>          $entry['invoicingCode'],
                'totalAmountWithoutTax' =>  number_format($entry['rawActualAmount'], 2),
                'taxAmount' =>              number_format($entry['rawTaxAmount'], 2),
                'VAT' =>                    number_format($entry['rawTaxPercentage'], 0)
            ];
        }

        return $result;
    }

    public function loadReportDataEntries(&$data, $bill) {
        $billEntries = array();
        $customers = $this->em->getRepository('AppDatabaseMainBundle:'.$this->tableCode)->findByIdBill($bill->getId());
        $isAustriaCompany = $data['companyCountry'] == 'AUSTRIA';
        $totalAmountWithTax = 0;
        $totalAmountWithoutTax = 0;
        $totalDiscount = 0;
        $totalTaxAmount = 0;
        foreach ($customers as $customer) {
            $query = $this->em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:'.$this->tableEntriesCode, 'p')
                ->andWhere('p.'.$this->tableEntriesLinkField.' = '.$customer->getId());
            $entries = $query->getQuery()->getResult();

            // Get bill amount and calculate tax
            foreach ($entries as $entry) {
                $overrideAmount = $customer->getOverrideAmount();
                $actualAmount = !empty($overrideAmount) ? $overrideAmount : $entry->getAmount();
                $taxPercentage = 0;
                $invoicingCode = '';

                // Currently we only have tax for the following coutries
                if (in_array($data['companyCountry'], ['AUSTRIA', 'GERMANY']) && $bill->getIsWithTax()) {
                    $targetEntity = null;
                    if (strpos($entry->getEntryId(), 'C') == true) {
                        $idCate = str_replace('C_', '', $entry->getEntryId());
                        $targetEntity = $this->em->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findOneById($idCate);
                    } else {
                        $idProduct = str_replace('P_', '', $entry->getEntryId());
                        $targetEntity = $this->em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findOneById($idProduct);
                    }

                    // Get tax base on company address
                    if ($targetEntity) {
                        if ($isAustriaCompany) {
                            $productType = $this->em->getRepository('AppDatabaseMainBundle:ProductType')->findOneById($targetEntity->getProductTypeAustria());
                            if ($productType) {
                                $taxPercentage = $productType->getTaxAustria();
                                $invoicingCode = $productType->getInvoicingCode();
                            }
                        } else {
                            $productType = $this->em->getRepository('AppDatabaseMainBundle:ProductType')->findOneById($targetEntity->getProductTypeGermany());
                            if ($productType) {
                                $taxPercentage = $productType->getTaxGermany();
                                $invoicingCode = $productType->getInvoicingCode();
                            }
                        }
                    }
                }

                // Add to actual entry
                $taxAmount = $actualAmount * ($taxPercentage / 100);
                $amountWithTax = $actualAmount + $taxAmount;
                $totalAmountWithTax += $amountWithTax;
                $totalAmountWithoutTax += $actualAmount;
                $totalTaxAmount += $taxAmount;
                $billEntries[] = array(
                    'orderNumber' =>        $customer->getOrderNumber(),
                    'orderDate' =>          $this->formatter->format($customer->getOrderDate(), 'date'),
                    'product' =>            $entry->getEntry(),
                    'rawTaxPercentage' =>   $taxPercentage,
                    'taxPercentage' =>      number_format($taxPercentage, 2).'%',
                    'name' =>               $customer->getName(),
                    'amount' =>             $this->formatter->format($entry->getAmount(), 'money'),
                    'actualAmount' =>       $this->formatter->format($actualAmount, 'money'),
                    'taxAmount' =>          $this->formatter->format($taxAmount, 'money'),
                    'amountWithTax' =>      $this->formatter->format($amountWithTax, 'money'),
                    'invoicingCode' =>      $invoicingCode,
                    'rawTaxAmount' =>       $taxAmount,
                    'rawActualAmount' =>    $actualAmount
                );
            }

            // Calculate discount value
            $overrideAmount = $customer->getOverrideAmount();
            if (!empty($overrideAmount)) {
                $diff = floatval($customer->getTotalAmount()) - floatval($customer->getOverrideAmount());
                $discount = $this->formatter->format(-$diff, 'money');
                $totalDiscount += $diff;
                if ($this->idBillGroup == 'invoice') {
                    $billEntries[] = array(
                        'orderNumber' =>        $customer->getOrderNumber(),
                        'orderDate' =>          $this->formatter->format($customer->getOrderDate(), 'date'),
                        'product' =>            'Discount',
                        'rawTaxPercentage' =>   0,
                        'taxPercentage' =>      '0.00%',
                        'name' =>               $customer->getName(),
                        'amount' =>             $this->formatter->format($discount, 'money'),
                        'actualAmount' =>       $this->formatter->format($discount, 'money'),
                        'taxAmount' =>          $this->formatter->format(0, 'money'),
                        'amountWithTax' =>      $this->formatter->format($discount, 'money'),
                        'invoicingCode' =>      false,
                        'rawTaxAmount' =>       0,
                        'rawActualAmount' =>    $discount
                    );
                }
            }
        }

        // get Product extend  Manual
        $manualProduct = $this->em->getRepository('AppDatabaseMainBundle:BillManualSystemText')->findByIdBill($bill->getId());
        foreach ($manualProduct as $key => $value) {
            $amount = $value->getPrice();
            $taxPercentage = $value->getTaxPercentage();
            if (empty($taxPercentage) || !$bill->getIsWithTax()) {
                $taxPercentage = 0;
            }

            $taxAmount = $amount * ($taxPercentage / 100);
            $amountWithTax = $amount + $taxAmount;
            $totalAmountWithTax += $amountWithTax;
            $totalAmountWithoutTax += $amount;
            $totalTaxAmount += $taxAmount;
            $billEntries[] = array(
                'orderNumber' =>        $value->getOrderNumber(),
                'orderDate' =>          $this->formatter->format($value->getDate(), 'date'),
                'product' =>            $value->getText(),
                'rawTaxPercentage' =>   $taxPercentage,
                'taxPercentage' =>      number_format($taxPercentage, 2).'%',
                'name' =>               $value->getCustomer(),
                'amount' =>             $this->formatter->format($amount, 'money'),
                'actualAmount' =>       $this->formatter->format($amount, 'money'),
                'taxAmount' =>          $this->formatter->format($taxAmount, 'money'),
                'amountWithTax' =>      $this->formatter->format($amountWithTax, 'money'),
                'invoicingCode' =>      '4999',
                'rawTaxAmount' =>       $taxAmount,
                'rawActualAmount' =>    $amount
            );
        }

        $data['billEntries'] = $billEntries;
        $data['isWithTax'] = $bill->getIsWithTax() ? 'YES' : 'NO';
        $data['totalAmountWithTax'] = $this->formatter->format($totalAmountWithTax, 'money');
        $data['totalAmountWithoutTax'] = $this->formatter->format($totalAmountWithoutTax, 'money');
        $data['totalDiscount'] = $this->formatter->format($totalDiscount, 'money');
        $data['totalTaxAmount'] = $this->formatter->format($totalTaxAmount, 'money');
        $data['rawTotalAmountWithTax'] = $totalAmountWithTax;
        $data['rawTotalTaxAmount'] = $totalTaxAmount;
        if ($bill->getIdCurrency() != 0) {
            $data['isCurrencyConversion'] = 'YES';
            $currency = $this->em->getRepository('AppDatabaseMainBundle:Currency')->findOneById($bill->getIdCurrency());
            $data['currencyExchangeRate'] = number_format($bill->getExchangeRate(), 2).' '.$currency->getName();
            $data['totalConvertedAmountWithTax'] = $currency->getSymbol().number_format($totalAmountWithTax * $bill->getExchangeRate(), 2);
        }

        $this->totalAmountWithTax = $totalAmountWithTax;
    }
}
