<?php
namespace Leep\AdminBundle\Business\Bill\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingOrderSelector extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'idBill' => 0,
            'idType' => 0
        );
    }

    public function getParent()
    {
        return 'field';
    }
    public function getName()
    {
        return 'app_billing_order_selector';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        $grid = $this->container->get('leep_admin.bill.business.order_selector_grid_reader');
        $view->vars['grid'] = array(
            'id' => 'OrderSelectorGrid',
            'header' => $grid->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'bill', 'feature', 'orderSelectorAjaxSource', array(
                'idBill' => $options['idBill'],
                'idType' => $options['idType']
            ))
        );

        $view->vars['getTargetBillingData'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'getTargetBillingData');
        $view->vars['idType'] = $options['idType'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');

        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandlerByType($options['idType'])->getOrderSelectorHandler()->buildOrderSelectorForm($builder, $options['idBill']);

        $builder->add('selectedEntries', 'hidden');
    }
}
