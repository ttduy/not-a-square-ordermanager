<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class EditStatusHandler extends AppEditHandler {       
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Bill', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($bill) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditStatusModel();
        $model->type = $formatter->format($bill->getIdType(), 'mapping', 'LeepAdmin_Bill_Type');
        $model->name = $bill->getName();
        $model->number = $bill->getNumber();

        // Status        
        $statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BillStatus', 'p')
            ->andWhere('p.idBill = :idBill')
            ->setParameter('idBill', $bill->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppBillStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $statusList[] = $m;
        }        
        $model->statusList = $statusList;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');    
        $today = new \DateTime();
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billStatusMapping = $billingHandler->getBillingHandlerByType($this->entity->getIdType())->getBillStatusMapping();

        // Status        
        $builder->add('type', 'label', array(
            'label'    => 'Type',
            'required' => false
        ));
        $builder->add('name', 'label', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('number', 'label', array(
            'label'    => 'Number',
            'required' => false
        ));
        $builder->add('sectionStatus', 'section', array(
            'label'    => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new Business\FormType\AppBillStatusRow($this->container, $today->format('d/m/Y'), $billStatusMapping),
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();       
        $em = $this->container->get('doctrine')->getEntityManager();       
        $dbHelper = $this->container->get('leep_admin.helper.database'); 

        // Update status
        $filters = array('idBill' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:BillStatus', $filters);                
        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\BillStatus();
            $status->setIdBill($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
                
            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }
        $this->entity->setStatus($lastStatus);
        $this->entity->setStatusDate($lastDate);        

        Utils::postUpdateStatusList($this->container, $this->entity->getId());

        parent::onSuccess();
    }
}
