<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesInvoiceCustomerHandler extends BillingFilesInvoiceHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'invoice_customer';
        $this->idReport = $config->getIdBillingReportInvoiceCustomer();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingInvoiceCustomer';
        $this->tableEntriesLinkField = 'idBillingInvoiceCustomer';
        $this->tableEntriesCode = 'BillingInvoiceCustomerEntry';
        $this->idBillGroup = 'invoice';
    }

    public function loadReportDataClient(&$data, $bill) {
        $invoiceCustomerRecord = $this->em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($bill->getIdTarget());
        $customer = $this->em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($invoiceCustomerRecord->getIdCustomer());

        $data['clientStreet'] = $bill->getInvoiceAddressStreet();
        $data['clientCity'] = $bill->getInvoiceAddressCity();
        $data['clientPostCode'] = $bill->getInvoiceAddressPostCode();
        $data['clientCountry'] = $this->formatter->format($bill->getInvoiceAddressIdCountry(), 'mapping', 'LeepAdmin_Country_List');
        $data['clientUid'] = $bill->getInvoiceAddressUid();

        if ($customer->getInvoiceAddressIsUsed()) {
            $data['clientName'] = $customer->getInvoiceAddressClientName();
            $data['clientCompany'] = $customer->getInvoiceAddressCompanyName();
        }
        else {
            $data['clientName'] = $customer->getFirstName().' '.$customer->getSurName();
            $data['clientCompany'] = '';
        }
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingInvoiceCustomer = $this->em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneByIdBill($bill->getId());
        if ($billingInvoiceCustomer) {
            $customer = $this->em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($billingInvoiceCustomer->getIdCustomer());
            if ($customer) {
                $dc = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionchannelid());
                if ($dc) {
                    $result['invoiceRestructure'] = $dc->getAccountingCode();
                }
            }
        }

        return $result;
    }

    public function generateBillingName($bill) {
        $billingInvoiceCustomer = $this->em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$billingInvoiceCustomer->getOrderNumber().' '.$billingInvoiceCustomer->getName().' - '.$bill->getNumber();
    }
}
