<?php
namespace Leep\AdminBundle\Business\Bill\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingTarget extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_billing_target';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $manager = $this->container->get('easy_module.manager');
        $billTypes = Business\Bill\Utils::getBillTypes($this->container);
        $view->vars['billTypes'] = $billTypes;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billTypes = Business\Bill\Utils::getBillTypes($this->container);
        uasort($billTypes, array($this,"vsort"));
        $builder->add('idType', 'choice', array(
            'label'    => 'Bill type',
            'required' => true,
            'choices'  => array(0 => '') + $billTypes
        ));
        foreach ($billTypes as $idBillType => $tmp) {
            $targetChoices = $billingHandler->getBillingHandlerByType($idBillType)->getBillingOpenTargets($idBillType);
            uasort($targetChoices, array($this,"vsort"));
            $builder->add('idTarget'.$idBillType, 'choice', array(
                'label'     => $billingHandler->getBillingHandlerByType($idBillType)->getBillingTargetName(),
                'required'  => true,
                'choices'   =>  array(0 => '') + $targetChoices,
                'attr'       => [
                    'onChange'  => 'changeTarget(this)'
                ]
            ));
        }
    }
    public function vsort($a,$b){
        return strtolower($a) > strtolower($b);
    }
}
