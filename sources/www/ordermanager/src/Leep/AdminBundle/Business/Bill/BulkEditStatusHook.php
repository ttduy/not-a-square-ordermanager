<?php
namespace Leep\AdminBundle\Business\Bill;

use Easy\ModuleBundle\Module\AbstractHook;

class BulkEditStatusHook extends AbstractHook {
    public $todayTs;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        $selectedBills = $controller->get('request')->get('selected', '');
        $data['selectedBills'] = $selectedBills;
        $selectedBills = explode(',', $selectedBills);

        $bills = array();
        foreach ($selectedBills as $selectedId) {
            if (trim($selectedId) != '') {
                $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById(intval($selectedId));
                if ($bill) {
                    $bills[] = array(
                        'date' => $formatter->format($bill->getBillDate(), 'date'),
                        'type' => $formatter->format($bill->getIdType(), 'mapping', 'LeepAdmin_Bill_Type'),
                        'name' => $bill->getName(),
                        'number' => $bill->getNumber(),
                        'status' => $formatter->format($bill->getStatus(), 'mapping', 'LeepAdmin_Bill_Status'),
                        'age'  => $this->computeAge($bill->getStatusDate()),
                        'amount' => $formatter->format($bill->getBillAmount(), 'money')
                    );
                }
            }
        }
        $data['bills'] = $bills;
    }

    private function computeAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }
}
