<?php 
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($id);
        
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandlerByType($bill->getIdType())->deleteBill($bill);

        $em->remove($bill);
        $em->flush();
    }
}