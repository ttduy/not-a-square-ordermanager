<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class OrderSelectorHandler {
    public $tableCode = '';
    public $idTargetField = '';
    public $mappingCode = '';
    public $targetLabel = '';
    public function __construct($container) {
        $this->container = $container;
    }

    /////////////////////////////////////////
    // x. BUILD BILL FORM
    public function buildBillToSection($builder, $bill) {
        $builder->add('sectionTo', 'section', array(
            'label'    => 'To',
            'property_path' => false
        ));
        $builder->add('orderSelector', 'app_billing_order_selector', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'idBill'   => $bill->getId(),
            'idType'   => $bill->getIdType()
        ));
    }

    /////////////////////////////////////////
    // x. ORDER SELECTOR - BUILD FORM
    public function buildOrderSelectorForm($builder, $idBill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($idBill);
        $mapping = $this->container->get('easy_mapping');
        $arrayDataMapping = [];
        if ($bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_DC || $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER ||
            $bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC || $bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_MANUAL_PARTNER || $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
            $arrayDataMapping = $mapping->getMapping($this->mappingCode);
        }
        else {
            $arrayDataMapping = $billingUtils->getOpenEntries($idBill, $this->tableCode, $this->idTargetField, $this->mappingCode);
        }
        $builder->add('idTarget', 'choice', array(
            'label'    => $this->targetLabel,
            'required' => false,
            'choices'  => $arrayDataMapping
        ));
        $builder->add('haveInvoiceAddress', 'text', array(
            'label'    => 'Have invoice address',
            'attr'     => [
                'readonly' => true,
                'style' => 'border: none;box-shadow: none;'
                ],
            'required' => false,
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('invoiceAddressUid', 'text', array(
            'label'    => 'UID',
            'required' => false
        ));
    }

    /////////////////////////////////////////
    // x. FORM - LOAD DATA
    public function formLoadData($bill) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $orderSelector = array();
        $orderSelector['idTarget'] = $bill->getIdTarget();
        $orderSelector['invoiceAddressStreet'] = $bill->getInvoiceAddressStreet();
        $orderSelector['invoiceAddressPostCode'] = $bill->getInvoiceAddressPostCode();
        $orderSelector['invoiceAddressCity'] = $bill->getInvoiceAddressCity();
        $orderSelector['invoiceAddressIdCountry'] = $bill->getInvoiceAddressIdCountry();
        $orderSelector['invoiceAddressTelephone'] = $bill->getInvoiceAddressTelephone();
        $orderSelector['invoiceAddressFax'] = $bill->getInvoiceAddressFax();
        $orderSelector['invoiceAddressUid'] = $bill->getInvoiceAddressUid();
        $orderSelector['haveInvoiceAddress'] = ($bill->getIsInvoiceAddress() == 1) ? 'Yes' : 'No';
        $billEntries = $em->getRepository('AppDatabaseMainBundle:'.$this->tableCode)->findByIdBill($bill->getId());
        if (!empty($billEntries)) {
            $selectedEntries = array();
            foreach ($billEntries as $entry) {
                $selectedEntries[] = $entry->getId();
                
       
            }

            $orderSelector['selectedEntries'] = implode(',', $selectedEntries);

        }
        return array('orderSelector' => $orderSelector);
    }

    /////////////////////////////////////////
    // x. FORM - ON SUCCESS
    public function formOnSuccess($handler, $bill, $data) {
        $request = $this->container->get('request');
        $em = $this->container->get('doctrine')->getEntityManager();

        $idTarget = $data['orderSelector']['idTarget'];
        $selectedEntries = $request->get('tableRows', array());
        $priceOverrides = $request->get('overridePrice');

        $bill->setIdTarget($idTarget);
        $bill->setInvoiceAddressStreet($data['orderSelector']['invoiceAddressStreet']);
        $bill->setInvoiceAddressPostCode($data['orderSelector']['invoiceAddressPostCode']);
        $bill->setInvoiceAddressCity($data['orderSelector']['invoiceAddressCity']);
        $bill->setInvoiceAddressIdCountry($data['orderSelector']['invoiceAddressIdCountry']);
        $bill->setInvoiceAddressTelephone($data['orderSelector']['invoiceAddressTelephone']);
        $bill->setInvoiceAddressFax($data['orderSelector']['invoiceAddressFax']);
        $bill->setInvoiceAddressUid($data['orderSelector']['invoiceAddressUid']);

        $handler->unhookBill($this->tableCode, $bill->getId());
        $handler->hookBill($this->tableCode, array_keys($selectedEntries), $bill->getId());
        $handler->overrideBill($this->tableCode, $priceOverrides);
    }

    /////////////////////////////////////////
    // x. BUILD QUERY
    public function buildQuery($queryBuilder, $idTarget, $idBill) {
        $queryBuilder
            ->select('p.orderNumber as orderNumber', 'p.overrideAmount as overrideAmount',
            'p.name as name', 'p.totalAmount as totalAmount', 'p.orderDate as orderDate',
            'p.id as id', 'p.idCustomer as idCustomer','cus.invoiceAddressIsUsed as haveInvoiceAddress' ,
            'cus.orderInfoText as orderInfoText')
            ->from('AppDatabaseMainBundle:'.$this->tableCode, 'p')
            ->innerJoin('AppDatabaseMainBundle:Customer', 'cus', 'WITH', 'cus.id = p.idCustomer');

        if ($idTarget != 0) {
            $queryBuilder->andWhere('p.'.$this->idTargetField.' = :idTarget')
                        ->setParameter('idTarget', $idTarget);
        }
        else {
            $queryBuilder->andWhere('p.id = 0');
        }

        $queryBuilder->andWhere('(p.idBill = 0) OR (p.idBill IS NULL) OR (p.idBill = :selectedIdBill)')
            ->setParameter('selectedIdBill', $idBill);
    }
}
