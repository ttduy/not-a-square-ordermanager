<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

abstract class BillingFilesHandler {
    public $filenameCode = 'file';
    public $tableCode = 'BillingInvoiceCustomer';
    public $idBillGroup = ''; // invoice, payment
    public $tableEntriesLinkField = 'idBillingInvoiceCustomer';
    public $tableEntriesCode = 'BillingInvoiceCustomerEntry';
    public $idReport = 0;
    public $idReminderNoticeReport = 0;
    public $container;
    public $em;
    public $formatter;
    public $totalAmountWithTax = 0;

    public function __construct($container) {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getEntityManager();
        $this->formatter = $container->get('leep_admin.helper.formatter');
        $this->presetConfig();
    }

    public function isInvoiceHandler() {
        return true;
    }

    protected function presetConfig() {}

    public function generateBillingFilename($bill) {
        $now = new \DateTime();
        return $this->filenameCode.'_'.$bill->getId().'_'.$now->format('YmdHis').'.pdf';
    }

    public function generateBillingName($bill) {
        return 'FIX ME';
    }

    public function generateReportData($bill) {
        $data = array();
        $this->loadReportDataCompany($data, $bill);
        $this->loadReportDataClient($data, $bill);
        $this->loadReportDataBill($data, $bill);
        $this->loadReportDataEntries($data, $bill);
        $this->loadReportDataExtra($data, $bill);
        return $data;
    }

    public function generateReportReminderData($bill) {
        $data = this-generateReportData($bill);
        $this->loadReportDataReminder($data, $bill);
        return $data;
    }

    public function loadReportDataClient(&$data, $bill) {}

    public function loadReportDataExtra(&$data, $bill) {}

    public abstract function loadReportDataEntries(&$data, $bill);
    public abstract function generateCSVData($bill, $reportData);

    public function loadReportDataBill(&$data, $bill) {
        $data['billNumber'] = $bill->getNumber();
        $data['billDate'] = $this->formatter->format($bill->getBillDate(), 'date');
        $data['billDateCsvFormat'] = $bill->getBillDate() ? $bill->getBillDate()->format('dmY') : '';
        $data['extraTextAfterDescription'] = $bill->getExtraTextAfterDescription();
        $data['extraTextFooter'] = $bill->getExtraTextFooter();
        $data['idNoTaxText'] = $bill->getIdNoTaxText();
        $data['idMarginPaymentMode'] = $bill->getIdMarginPaymentMode();
        if ($this->isInvoiceHandler()) {
            $data['noTaxText'] = Business\InvoiceNoTaxText\Utils::getText($this->container, $bill->getIdNoTaxText());
        } else {
            $data['noTaxText'] = Business\PaymentNoTaxText\Utils::getText($this->container, $bill->getIdNoTaxText());
        }
    }

    public function loadReportDataCompany(&$data, $bill) {
        $data['nameCancelation']      = $bill->getName();
        $data['invoiceNumber']        = $bill->getNumber();
        $data['isCancel']             = $bill->getIsCancel();
        $company = $this->em->getRepository('AppDatabaseMainBundle:Companies')->findOneById($bill->getIdCompany());
        if ($company) {
            $variablesRows = explode("\n", $company->getBillVariables());
            foreach ($variablesRows as $row) {
                $a = explode('=', $row);
                if (count($a) == 2) {
                    $data[trim($a[0])] = trim($a[1]);
                }
            }

            $data['companyName']          = $company->getCompanyName();
            $data['companyCity']          = $company->getCity();
            $data['companyStreet']        = $company->getStreet();
            $data['companyPostCode']      = $company->getPostCode();
            $data['companyCountry']       = $this->formatter->format($company->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
            $data['companyEmail']         = $company->getEmail();
            $data['companyPhone']         = $company->getTelephone();
            $data['companyWebsite']       = $company->getWebsite();
            $data['companyBank']          = $company->getBank();
            $data['companyBankAccount']   = $company->getBankAccount();
            $data['companyBankCode']      = $company->getBankCode();
            $data['companyBic']           = $company->getBic();
            $data['companyIban']          = $company->getIban();
            $data['companyTradeRegister'] = $company->getTradeRegister();
            $data['companyJurisdiction']  = $company->getJurisdiction();
            $data['companyUidNumber']     = $company->getUidNumber();
            $data['companyTaxNumber']     = $company->getTaxNumber();
            $data['companyLegalForm']     = $company->getLegalForm();
            $data['companyManager']       = $company->getManager();
        }

        $data['clientStreet'] = $bill->getInvoiceAddressStreet();
        $data['clientCity'] = $bill->getInvoiceAddressCity();
        $data['clientPostCode'] = $bill->getInvoiceAddressPostCode();
        $data['clientCountry'] = $this->formatter->format($bill->getInvoiceAddressIdCountry(), 'mapping', 'LeepAdmin_Country_List');
        $data['clientUid'] = $bill->getInvoiceAddressUid();
        $data['commissionPaymentWithTax'] = $bill->getCommissionPaymentWithTax();
    }

    public function loadReportDataReminder(&$data, $bill) {
        $today = new \DateTime();
        $datediff = $today->getTimestamp() - $bill->getBillDate()->getTimestamp();
        $data['billOverdueDays'] = intval($datediff / 86400);
        $data['billAmount'] = $this->formatter->format($bill->getBillAmount(), 'money');
    }

    ///////////////////////////////////////////////
    // x. SHARED FUNCTION
    public function loadReportDataForDcOrPartner(&$data, $client) {
        $data['clientName'] = $client->getFirstName().' '.$client->getSurName();
        $data['clientCompany'] = $client->getInstitution();

        if ($client->getInvoiceAddressIsUsed()) {
            $clientName = $client->getInvoiceClientName();
            if (trim($clientName) != '') {
                $data['clientName'] = $clientName;
                if ($clientName == '######') {
                    $data['clientName'] = '';
                }
            }
            $clientCompany = $client->getInvoiceCompanyName();
            if (trim($clientCompany) != '') {
                $data['clientCompany'] = $clientCompany;
                if ($clientCompany == '######') {
                    $data['clientCompany'] = '';
                }
            }
        }
    }
}
