<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesPaymentHandler extends BillingFilesHandler {
    public function isInvoiceHandler() {
        return false;
    }

    public function generateCSVData($bill, $reportData) {
        $result = [
            'type' =>                   'payment',
            'paymentDate' =>            $reportData['billDateCsvFormat'],
            'totalAmountWithoutTax' =>  number_format($reportData['rawTotalAmountWithoutTax'], 2),
            'totalTaxAmount' =>         number_format($reportData['rawTotalTaxAmount'], 2),
            'paymentNumber' =>          $reportData['billNumber'],
            'invoiceRestructure' =>     '',
            'VAT' =>                    $reportData['commissionPaymentWithTax'],
            'entries' =>                []
        ];

        foreach ($reportData['billEntries'] as $entry) {
            if ($entry['product'] === 'Extra') {
                continue;
            }

            $result['entries'][] = [
                'totalAmountWithTax' =>     number_format($entry['rawAmountWithTax'], 2),
            ];
        }

        return $result;
    }

    public function loadReportDataEntries(&$data, $bill) {
        $billEntries = array();
        $customers = $this->em->getRepository('AppDatabaseMainBundle:'.$this->tableCode)->findByIdBill($bill->getId());
        $totalAmountWithoutTax = 0;
        $totalExtra = 0;
        $taxPercentage = $bill->getCommissionPaymentWithTax();
        if (empty($taxPercentage)) {
            $taxPercentage = 0;
        }

        foreach ($customers as $customer) {
            $query = $this->em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:'.$this->tableEntriesCode, 'p')
                ->andWhere('p.'.$this->tableEntriesLinkField.' = '.$customer->getId());
            $entries = $query->getQuery()->getResult();

            // Get bill amount and calculate tax
            foreach ($entries as $entry) {
                $overrideAmount = $customer->getOverrideAmount();
                $actualAmount = !empty($overrideAmount) ? $overrideAmount : $entry->getAmount();

                // Add to actual entry
                $totalAmountWithoutTax += $actualAmount;
                $taxAmount = $actualAmount * ($taxPercentage / 100);
                $amountWithTax = $actualAmount + $taxAmount;
                $billEntries[] = array(
                    'orderNumber' =>        $customer->getOrderNumber(),
                    'orderDate' =>          $this->formatter->format($customer->getOrderDate(), 'date'),
                    'product' =>            $entry->getEntry(),
                    'name' =>               $customer->getName(),
                    'amount' =>             $this->formatter->format($entry->getAmount(), 'money'),
                    'actualAmount' =>       $this->formatter->format($actualAmount, 'money'),
                    'taxAmount' =>          $this->formatter->format($taxAmount, 'money'),
                    'amountWithTax' =>      $this->formatter->format($amountWithTax, 'money'),
                    'rawTaxAmount' =>       $taxAmount,
                    'rawAmountWithTax' =>   $amountWithTax
                );
            }

            // Calculate extra value
            $overrideAmount = $customer->getOverrideAmount();
            if (!empty($overrideAmount)) {
                $diff = floatval($customer->getTotalAmount()) - floatval($customer->getOverrideAmount());
                $extra = $this->formatter->format(-$diff, 'money');
                $totalExtra += $diff;
                if ($this->idBillGroup == 'invoice') {
                    $billEntries[] = array(
                        'orderNumber' =>        $customer->getOrderNumber(),
                        'orderDate' =>          $this->formatter->format($customer->getOrderDate(), 'date'),
                        'product' =>            'Extra',
                        'name' =>               $customer->getName(),
                        'amount' =>             $this->formatter->format($extra, 'money'),
                        'actualAmount' =>       $this->formatter->format($extra, 'money'),
                        'taxAmount' =>          $this->formatter->format(0, 'money'),
                        'amountWithTax' =>      $this->formatter->format($extra, 'money'),
                        'rawTaxAmount' =>       0,
                        'rawAmountWithTax' =>   $extra
                    );
                }
            }
        }

        // get Product extend  Manual
        $manualProduct = $this->em->getRepository('AppDatabaseMainBundle:BillManualSystemText')->findByIdBill($bill->getId());
        foreach ($manualProduct as $key => $value) {
            $amount = $value->getPrice();
            $totalAmountWithoutTax += $amount;
            $taxAmount = $actualAmount * ($taxPercentage / 100);
            $amountWithTax = $actualAmount + $taxAmount;
            $billEntries[] = array(
                'orderNumber' =>            $value->getOrderNumber(),
                'orderDate' =>              $this->formatter->format($value->getDate(), 'date'),
                'product' =>                $value->getText(),
                'name' =>                   $value->getCustomer(),
                'amount' =>                 $this->formatter->format($amount, 'money'),
                'actualAmount' =>           $this->formatter->format($amount, 'money'),
                'taxAmount' =>              $this->formatter->format($taxAmount, 'money'),
                'amountWithTax' =>          $this->formatter->format($amountWithTax, 'money'),
                'rawTaxAmount' =>           $taxAmount,
                'rawAmountWithTax' =>       $amountWithTax
            );
        }

        $totalTaxAmount = $totalAmountWithoutTax * ($taxPercentage / 100);
        $totalAmountWithTax = $totalAmountWithoutTax + $totalTaxAmount;
        $data['billEntries'] = $billEntries;
        $data['isWithTax'] = $bill->getIsWithTax() ? 'YES' : 'NO';
        $data['commissionPaymentWithTax'] = number_format($taxPercentage, 0);
        $data['totalAmountWithTax'] = $this->formatter->format($totalAmountWithTax, 'money');
        $data['totalAmountWithoutTax'] = $this->formatter->format($totalAmountWithoutTax, 'money');
        $data['totalExtra'] = $this->formatter->format($totalExtra, 'money');
        $data['totalTaxAmount'] = $this->formatter->format($totalTaxAmount, 'money');
        $data['rawTotalAmountWithoutTax'] = $totalAmountWithoutTax;
        $data['rawTotalTaxAmount'] = $totalTaxAmount;
        if ($bill->getIdCurrency() != 0) {
            $data['isCurrencyConversion'] = 'YES';
            $currency = $this->em->getRepository('AppDatabaseMainBundle:Currency')->findOneById($bill->getIdCurrency());
            $data['currencyExchangeRate'] = number_format($bill->getExchangeRate(), 2).' '.$currency->getName();
            $data['totalConvertedAmountWithTax'] = $currency->getSymbol().number_format($totalAmountWithTax * $bill->getExchangeRate(), 2);
        }

        $this->totalAmountWithTax = $totalAmountWithTax;
    }
}
