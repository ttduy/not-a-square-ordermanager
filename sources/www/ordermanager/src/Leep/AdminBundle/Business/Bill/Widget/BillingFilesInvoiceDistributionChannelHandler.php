<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesInvoiceDistributionChannelHandler extends BillingFilesInvoiceHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'invoice_distribution_channel';
        $this->idReport = $config->getIdBillingReportInvoiceDistributionChannel();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingInvoiceDistributionChannel';
        $this->tableEntriesLinkField = 'idBillingInvoiceDistributionChannel';
        $this->tableEntriesCode = 'BillingInvoiceDistributionChannelEntry';
        $this->idBillGroup = 'invoice';
    }

    public function loadReportDataClient(&$data, $bill) {
        $distributionChannel = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($bill->getIdTarget());
        $this->loadReportDataForDcOrPartner($data, $distributionChannel);
    }

    public function generateBillingName($bill) {
        $distributionChannel = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$distributionChannel->getDistributionChannel().' - '.$bill->getNumber();
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingInvoiceDistributionChannel = $this->em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneByIdBill($bill->getId());
        if ($billingInvoiceDistributionChannel) {
            $dc = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($billingInvoiceDistributionChannel->getIdDistributionChannel());
            if ($dc) {
                $result['invoiceRestructure'] = $dc->getAccountingCode();
            }
        }

        return $result;
    }
}
