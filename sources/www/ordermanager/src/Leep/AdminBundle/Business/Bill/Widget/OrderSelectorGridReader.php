<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class OrderSelectorGridReader extends AppGridDataReader {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        return array('orderNumber', 'orderDate', 'name', 'totalAmount', 'haveInvoiceAddress', 'orderInfoText', 'overrideAmount');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Order Number',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Order Date',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',                    'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Total Amount',            'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Have invoice address',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Order Information',       'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Override Amount',         'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_OrderSelectorGridReader');
        return null;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->setFirstResult(0);
        $queryBuilder->setMaxResults(500);
    }
    public function buildCellHaveInvoiceAddress($row) {
        $mgr = $this->getModuleManager();
        if($row['haveInvoiceAddress'] == 1)
            return 'Yes';
        return 'No';
    }
    public function buildCellOrderNumber($row) {
        $mgr = $this->getModuleManager();

        $href = $mgr->getUrl('leep_admin', 'bill', 'feature', 'viewOrderEntries', array('idCustomer' => $row['idCustomer']));
        return '<a href="'.$href.'" class="dataTablePopupButton">'.$row['orderNumber'].'</a>';
    }

    public function buildQuery($queryBuilder) {
        $request = $this->container->get('request');
        $idTarget = $request->get('idTarget', 0);
        $idBill = $request->get('idBill', 0);
        $idType = $request->get('idType', 0);

        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandlerByType($idType)->getOrderSelectorHandler()->buildQuery($queryBuilder, $idTarget, $idBill);
    }

    public function buildCellOverrideAmount($row) {
        return '<input type="textbox" name="overridePrice['.$row['id'].']" value="'.$row['overrideAmount'].'" />';
    }
}
