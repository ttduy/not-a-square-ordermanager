<?php
namespace Leep\AdminBundle\Business\Bill;

class FilterModel {
    public $idType;
    public $name;
    public $number;
    public $startDate;
    public $endDate;
    public $currentStatus;
    public $idDistributionChannel;
    public $idPartner;
    public $idAcquisiteur;
    public $orderNumber;
}