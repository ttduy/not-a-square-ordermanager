<?php
namespace Leep\AdminBundle\Business\Bill\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingFiles extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options) {
        return array(
            'idBill' => 0,
            'idReport' => 0
        );
    }

    public function getParent() {
        return 'field';
    }

    public function getName() {
        return 'app_billing_files';
    }

    public function finishView(FormView $view, FormInterface $form, array $options) {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        $grid = $this->container->get('leep_admin.bill.business.billing_files_grid_reader');
        $view->vars['grid'] = array(
            'id' => 'BillingFilesGrid',
            'header' => $grid->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'bill', 'feature', 'billingFilesAjaxSource', array(
                'idBill' => $options['idBill']
            ))
        );
        $view->vars['generatePdfUrl'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'generateBillPdf', array('idBill' => $options['idBill']));
        $view->vars['editPdfUrl'] = $mgr->getUrl('leep_admin', 'report', 'edit_section', 'edit', array('id' => $options['idReport']));
        $view->vars['setAsActivePdfUrl'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'setAsActivePdf');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
    }
}
