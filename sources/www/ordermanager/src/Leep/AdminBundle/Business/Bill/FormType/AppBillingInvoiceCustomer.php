<?php
namespace Leep\AdminBundle\Business\Bill\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingInvoiceCustomer extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'idBill' => 0
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_billing_invoice_customer';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        $view->vars['getBillingInvoiceCustomer'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'getBillingInvoiceCustomer');
        $view->vars['viewOrderEntriesLink'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'viewOrderEntries');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');

        $builder->add('nameCustomer', 'label', array(
            'label'    => 'Customer'
        ));
        $builder->add('idBillingInvoiceCustomer', 'hidden', array(
            'label'    => 'Customer Id'
        ));
        $builder->add('totalAmount', 'label', array(
            'label'    => 'Total amount'
        ));
        $builder->add('overrideAmount', 'text', array(
            'label'    => 'Override amount'
        ));
        $builder->add('orderInformation', 'textarea', array(
            'label'    => 'Order information',
            'required' => false,
            'attr'  => [
                'rows' => 5,
                'style' => 'width: 50%'
                ]
        ));
        $builder->add('haveInvoiceAddress', 'text', array(
            'label'    => 'Have invoice address',
            'attr'  => [
                'readonly' => true,
                'style' => 'border: none;box-shadow: none;'
                ],
            'required' => false,
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false,
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('invoiceAddressUid', 'text', array(
            'label'    => 'Uid',
            'required' => false
        ));
        // $em = $this->container->get('doctrine')->getEntityManager();
        // $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($options['idBill']);
        // if ($bill) {
        //     $billingInvoiceCustomer = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($bill->getIdTarget());
        //     if ($billingInvoiceCustomer) {
        //         $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($billingInvoiceCustomer->getIdCustomer());
        //         if ($customer && $customer->getInvoiceAddressIsUsed()) {
        //
        //         }
        //     }
        // }
    }
}
