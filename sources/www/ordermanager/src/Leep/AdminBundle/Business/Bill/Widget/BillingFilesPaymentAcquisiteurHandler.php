<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesPaymentAcquisiteurHandler extends BillingFilesPaymentHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'payment_acquisiteur';
        $this->idReport = $config->getIdBillingReportPaymentAcquisiteur();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingPaymentAcquisiteur';
        $this->tableEntriesLinkField = 'idBillingPaymentAcquisiteur';
        $this->tableEntriesCode = 'BillingPaymentAcquisiteurEntry';
        $this->idBillGroup = 'payment';
    }

    public function loadReportDataExtra(&$data, $bill) {
        if ($bill->getIdMarginPaymentMode() == Business\DistributionChannel\Constant::MARGIN_PAYMENT_MODE_INVOICE) {
            $data['headingTitle'] = 'Rechnung für Sonstiges/Invoice for miscellaneus';
            $config = $this->container->get('leep_admin.helper.common')->getConfig();
            $this->idReport = $config->getIdBillingReportPaymentAcquisiteurInvoice();
        } else {
            $data['headingTitle'] = 'Gutschrift für Sonstiges/Credit note for miscellaneus';
        }
    }


    public function loadReportDataClient(&$data, $bill) {
        $acquisiteur = $this->em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($bill->getIdTarget());
        $data['clientName'] = $acquisiteur->getFirstName().' '.$acquisiteur->getSurName();
        $data['clientCompany'] = $acquisiteur->getInstitution();
        $data['clientStreet'] = $acquisiteur->getStreet();
        $data['clientCity'] = $acquisiteur->getCity();
        $data['clientPostCode'] = $acquisiteur->getPostCode();
        $data['clientUid'] = $acquisiteur->getUidNumber();
        $data['clientCountry'] = $this->formatter->format($acquisiteur->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
        $data['clientAccountName'] = $acquisiteur->getAccountName();
        $data['clientAccountNumber'] = $acquisiteur->getAccountNumber();
        $data['clientBankCode'] = $acquisiteur->getBankCode();
        $data['clientBic'] = $acquisiteur->getBIC();
        $data['clientIban'] = $acquisiteur->getIBAN();
        $data['isAcquisiteur'] = 1;
    }

    public function generateBillingName($bill) {
        $acquisiteur = $this->em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$acquisiteur->getAcquisiteur().' - '.$bill->getNumber();
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingPaymentAcquisiteur = $this->em->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findOneByIdBill($bill->getId());
        if ($billingPaymentAcquisiteur) {
            $acquisiteur = $this->em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($billingPaymentAcquisiteur->getIdAcquisiteur());
            if ($acquisiteur) {
                $result['invoiceRestructure'] = $acquisiteur->getAccountingCode();
            }
        }

        return $result;
    }
}
