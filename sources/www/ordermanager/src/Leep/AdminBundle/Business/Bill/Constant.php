<?php
namespace Leep\AdminBundle\Business\Bill;

class Constant {
    const INVOICE_DELIVERY_NOTE_PRINT           = 10;
    const INVOICE_DELIVERY_NOTE_PRINT_AND_EMAIL = 20;
    const INVOICE_DELIVERY_NOTE_EMAIL           = 30;

    public static function getInvoiceDeliveryNoteTypes() {
        return array(
            self::INVOICE_DELIVERY_NOTE_PRINT               => 'Print',
            self::INVOICE_DELIVERY_NOTE_PRINT_AND_EMAIL     => 'Print & Email',
            self::INVOICE_DELIVERY_NOTE_EMAIL               => 'Email',
        );
    }

    // Invoice
    const TYPE_INVOICE                          = 100;
    const TYPE_INVOICE_CUSTOMER                 = 101;
    const TYPE_INVOICE_DISTRIBUTION_CHANNEL     = 102;
    const TYPE_INVOICE_PARTNER                  = 103;
    const TYPE_INVOICE_MANUAL_DC                = 104;
    const TYPE_INVOICE_MANUAL_PARTNER           = 105;

    // Payment
    const TYPE_PAYMENT                          = 200;
    const TYPE_PAYMENT_DISTRIBUTION_CHANNEL     = 201;
    const TYPE_PAYMENT_PARTNER                  = 202;
    const TYPE_PAYMENT_ACQUISITEUR              = 203;
    const TYPE_PAYMENT_MANUAL_DC                = 204;
    const TYPE_PAYMENT_MANUAL_PARTNER           = 205;
    const TYPE_PAYMENT_MANUAL_ACQUISITEUR       = 206;

    public static function getBillTypes() {
        return array(
            self::TYPE_INVOICE_CUSTOMER                => 'Invoice - Customer',
            self::TYPE_INVOICE_DISTRIBUTION_CHANNEL    => 'Invoice - Distribution Channel',
            self::TYPE_INVOICE_PARTNER                 => 'Invoice - Partner',
            self::TYPE_INVOICE_MANUAL_DC               => 'Manual Invoice - Any DC',
            self::TYPE_INVOICE_MANUAL_PARTNER          => 'Manual Invoice - Any Partner',
            self::TYPE_PAYMENT_DISTRIBUTION_CHANNEL    => 'Payment - Distribution Channel',
            self::TYPE_PAYMENT_PARTNER                 => 'Payment - Partner',
            self::TYPE_PAYMENT_ACQUISITEUR             => 'Payment - Acquisiteur',
            self::TYPE_PAYMENT_MANUAL_DC               => 'Manual Payment - Any DC',
            self::TYPE_PAYMENT_MANUAL_PARTNER          => 'Manual Payment - Any Partner',
            self::TYPE_PAYMENT_MANUAL_ACQUISITEUR      => 'Manual Payment - Any Acquisiteur',
        );
    }

    public static function getBillTypeExtended() {
        return array_merge(
        self::getBillTypes(), [
            self::TYPE_INVOICE                         => 'Invoice',
            self::TYPE_PAYMENT                         => 'Payment'
        ]);
    }

    public static function getManualTypes() {
        return [
            self::TYPE_INVOICE_MANUAL_DC,
            self::TYPE_INVOICE_MANUAL_PARTNER,
            self::TYPE_PAYMENT_MANUAL_DC,
            self::TYPE_PAYMENT_MANUAL_PARTNER,
            self::TYPE_PAYMENT_MANUAL_ACQUISITEUR,
        ];
    }

    public static function getBillTargets() {
        return [
            // Invoice
            self::TYPE_INVOICE_CUSTOMER                => 'Customer',
            self::TYPE_INVOICE_DISTRIBUTION_CHANNEL    => 'DistributionChannels',
            self::TYPE_INVOICE_PARTNER                 => 'Partner',
            self::TYPE_INVOICE_MANUAL_DC               => 'DistributionChannels',
            self::TYPE_INVOICE_MANUAL_PARTNER          => 'Partner',

            // Payment
            self::TYPE_PAYMENT_DISTRIBUTION_CHANNEL    => 'DistributionChannels',
            self::TYPE_PAYMENT_PARTNER                 => 'Partner',
            self::TYPE_PAYMENT_ACQUISITEUR             => 'Acquisiteur',
            self::TYPE_PAYMENT_MANUAL_DC               => 'DistributionChannels',
            self::TYPE_PAYMENT_MANUAL_PARTNER          => 'Partner',
            self::TYPE_PAYMENT_MANUAL_ACQUISITEUR      => 'Acquisiteur'
        ];
    }

    public static function getSubBillTypes() {
        return array(
            self::TYPE_INVOICE => array(
                self::TYPE_INVOICE_CUSTOMER,
                self::TYPE_INVOICE_DISTRIBUTION_CHANNEL,
                self::TYPE_INVOICE_PARTNER,
                self::TYPE_INVOICE_MANUAL_DC,
                self::TYPE_INVOICE_MANUAL_PARTNER
            ),
            self::TYPE_PAYMENT => array(
                self::TYPE_PAYMENT_DISTRIBUTION_CHANNEL,
                self::TYPE_PAYMENT_PARTNER,
                self::TYPE_PAYMENT_ACQUISITEUR,
                self::TYPE_PAYMENT_MANUAL_DC,
                self::TYPE_PAYMENT_MANUAL_PARTNER,
                self::TYPE_PAYMENT_MANUAL_ACQUISITEUR
            )
        );
    }

    // INVOICE
    const INVOICE_STATUS_TO_BE_DONE                       = 10;
    const INVOICE_STATUS_WAIT_WITH_INVOICE                = 20;
    const INVOICE_STATUS_INVOICE_SENT                     = 30;
    const INVOICE_STATUS_REMINDERS_SENT                   = 40;
    const INVOICE_STATUS_NOT_IN_BOOKEEPING                = 60;
    const INVOICE_STATUS_CANCELLED                        = 70;
    const INVOICE_STATUS_FREE_ANALYSIS                    = 80;
    const INVOICE_STATUS_WAIT_FOR_CREDIT_CARD_TRANSACTION = 90;
    const INVOICE_STATUS_WAIT_WITH_REMINDER               = 100;
    const INVOICE_STATUS_PAID                             = 110;
    const INVOICE_STATUS_PAID_WITH_CREDIT_CARD            = 111;
    const INVOICE_STATUS_PAID_BY_BANK_TRANSFER            = 112;
    const INVOICE_STATUS_PAID_BY_PAYPAL                   = 113;
    const INVOICE_STATUS_GIVEN_TO_DEBT_COLLECTION_AGENCY  = 114;

    public static function getBillInvoiceStatus() {
        return array(
            self::INVOICE_STATUS_TO_BE_DONE                        => 'To be done',
            self::INVOICE_STATUS_WAIT_WITH_INVOICE                 => 'Wait with invoice',
            self::INVOICE_STATUS_INVOICE_SENT                      => 'Invoice sent',
            self::INVOICE_STATUS_REMINDERS_SENT                    => 'Reminders sent',
            self::INVOICE_STATUS_PAID                              => 'Paid',
            self::INVOICE_STATUS_PAID_WITH_CREDIT_CARD             => 'Paid - By credit card',
            self::INVOICE_STATUS_PAID_BY_BANK_TRANSFER             => 'Paid - By bank transfer',
            self::INVOICE_STATUS_PAID_BY_PAYPAL                    => 'Paid - By paypal',
            self::INVOICE_STATUS_NOT_IN_BOOKEEPING                 => 'Not in bookeeping',
            self::INVOICE_STATUS_CANCELLED                         => 'Invoice cancelled',
            self::INVOICE_STATUS_FREE_ANALYSIS                     => 'Free analysis',
            self::INVOICE_STATUS_WAIT_FOR_CREDIT_CARD_TRANSACTION  => 'Wait for credit card transaction',
            self::INVOICE_STATUS_WAIT_WITH_REMINDER                => 'Wait with reminder',
            self::INVOICE_STATUS_GIVEN_TO_DEBT_COLLECTION_AGENCY   => 'Given to debt collection agency'
        );
    }

    // PAYMENT
    const PAYMENT_STATUS_TO_BE_DONE                      = 1000;
    const PAYMENT_STATUS_WAIT_WITH_PAYMENT               = 1010;
    const PAYMENT_STATUS_PAYMENT_SENT                    = 1020;
    //const PAYMENT_STATUS_REMINDER_SENT                   = 1030;
    const PAYMENT_STATUS_PAID                            = 1040;
    const PAYMENT_STATUS_PAYMENT_CANCELLED               = 1050;
    public static function getBillPaymentStatus() {
        return array(
            self::PAYMENT_STATUS_TO_BE_DONE        => 'To be done',
            self::PAYMENT_STATUS_WAIT_WITH_PAYMENT => 'Wait with payment',
            self::PAYMENT_STATUS_PAYMENT_SENT      => 'Payment sent',
            //self::PAYMENT_STATUS_REMINDER_SENT     => 'Reminders sent',
            self::PAYMENT_STATUS_PAID              => 'Paid',
            self::PAYMENT_STATUS_PAYMENT_CANCELLED => 'Payment cancelled'
        );
    }

    // BILL STATUS
    public static function getBillStatus() {
        return self::getBillInvoiceStatus() + self::getBillPaymentStatus();
    }


    const NO_TAX_TYPE_WITH_UID                 = 1;
    const NO_TAX_TYPE_NO_UID                   = 2;
    const NO_TAX_TYPE_NO_UID_AT_DOCTOR         = 3;
    public static function getNoTaxTypes() {
        return array(
            self::NO_TAX_TYPE_WITH_UID          => 'No Tax - With UID',
            self::NO_TAX_TYPE_NO_UID            => 'No Tax - No UID',
            self::NO_TAX_TYPE_NO_UID_AT_DOCTOR  => 'No Tax - No UID (AT Doctor)'
        );
    }
}
