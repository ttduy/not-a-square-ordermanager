<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesPaymentPartnerHandler extends BillingFilesPaymentHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'payment_partner';
        $this->idReport = $config->getIdBillingReportPaymentPartner();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingPaymentPartner';
        $this->tableEntriesLinkField = 'idBillingPaymentPartner';
        $this->tableEntriesCode = 'BillingPaymentPartnerEntry';
        $this->idBillGroup = 'payment';
    }

    public function loadReportDataExtra(&$data, $bill) {
        if ($bill->getIdMarginPaymentMode() == Business\DistributionChannel\Constant::MARGIN_PAYMENT_MODE_INVOICE) {
            $data['headingTitle'] = 'Rechnung für Beratung/Invoice for counselling';
            $config = $this->container->get('leep_admin.helper.common')->getConfig();
            $this->idReport = $config->getIdBillingReportPaymentPartnerInvoice();
        }
        else {
            $data['headingTitle'] = 'Gutschrift für Beratung/Credit note for counselling';
        }
    }

    public function loadReportDataClient(&$data, $bill) {
        $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($bill->getIdTarget());

        $this->loadReportDataForDcOrPartner($data, $partner);

        $data['clientAccountName'] = $partner->getAccountName();
        $data['clientAccountNumber'] = $partner->getAccountNumber();
        $data['clientBankCode'] = $partner->getBankCode();
        $data['clientBic'] = $partner->getBIC();
        $data['clientIban'] = $partner->getIBAN();
    }

    public function generateBillingName($bill) {
        $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$partner->getPartner().' - '.$bill->getNumber();
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingPaymentPartner = $this->em->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneByIdBill($bill->getId());
        if ($billingPaymentPartner) {
            $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($billingPaymentPartner->getIdPartner());
            if ($partner) {
                $result['invoiceRestructure'] = $partner->getAccountingCode();
            }
        }

        return $result;
    }
}
