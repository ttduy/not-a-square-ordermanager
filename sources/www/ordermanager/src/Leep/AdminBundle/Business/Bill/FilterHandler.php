<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Bill_TypeExtended')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => false
        ));        
        $builder->add('currentStatus', 'choice', array(
            'label'    => 'Current status',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + array(
                'Invoice' => $mapping->getMapping('LeepAdmin_Bill_InvoiceStatus'),
                'Payment' => $mapping->getMapping('LeepAdmin_Bill_PaymentStatus')
            )
        ));
        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start Date',
            'required' => false
        ));        
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End Date',
            'required' => false
        ));

        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List'),
            'empty_value' => false
        ));
        $builder->add('idPartner', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Partner_List'),
            'empty_value' => false
        ));
        $builder->add('idAcquisiteur', 'searchable_box', array(
            'label'    => 'Acquisiteur',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List'),
            'empty_value' => false
        ));
        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number (invoice to customer)',
            'required' => false
        ));
        return $builder;
    }
}