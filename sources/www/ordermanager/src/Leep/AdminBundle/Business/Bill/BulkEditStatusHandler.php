<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditStatusHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) { 
        $model = new BulkEditStatusModel(); 
        $model->selectedBills = $this->container->get('request')->get('selected', '');
        $model->statusDate = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedBills', 'hidden');
        $builder->add('statusDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false
        ));
        $builder->add('status',     'choice', array(
            'label'  => 'Status',
            'required' => false,
            'choices'  => array(
                'Invoice' => $mapping->getMapping('LeepAdmin_Bill_InvoiceStatus'),
                'Payment' => $mapping->getMapping('LeepAdmin_Bill_PaymentStatus'),
            ),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('notes', 'textarea', array(
            'label'  => 'Notes',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 2
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $selectedBills = explode(',', $model->selectedBills);
        foreach ($selectedBills as $selectedId) {
            if (trim($selectedId) != '') {                
                $selectedId = intval($selectedId);

                // Find max status
                $query = $em->createQueryBuilder();
                $query->select('MAX(p.sortOrder)')
                    ->from('AppDatabaseMainBundle:BillStatus', 'p')
                    ->andWhere('p.id = :idBill')
                    ->setParameter('idBill', $selectedId);
                $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;

                // New status
                $newStatus = new Entity\BillStatus();
                $newStatus->setIdBill($selectedId);
                $newStatus->setStatus($model->status);
                $newStatus->setStatusDate($model->statusDate);
                $newStatus->setSortOrder($sortOrder);
                $newStatus->setNotes($model->notes);
                $em->persist($newStatus);

                // Update bill status
                $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($selectedId);
                $bill->setStatus($model->status);
                $bill->setStatusDate($model->statusDate);
                $em->persist($bill);
                $em->flush();

                Utils::postUpdateStatusList($this->container, $bill->getId());
            }
        }
        $em->flush();

        $this->messages[] = 'Status has been changed successfully';
    }
}