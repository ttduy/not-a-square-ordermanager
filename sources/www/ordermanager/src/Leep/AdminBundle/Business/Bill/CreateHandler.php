<?php
namespace Leep\AdminBundle\Business\Bill;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->container = $this->container;
        $model->billDate = new \DateTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('billingTarget', 'app_billing_target', array(
        ));
        $builder->add('billDate', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => true
        ));


        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $model = $this->getForm()->getData();
        $billTargetId = 'idTarget'.$model->billingTarget['idType'];
        $idType = $model->billingTarget['idType'];
        $target = Utils::getBillTarget($idType, $model->billingTarget[$billTargetId], $this->container);

        $bill = new Entity\Bill();
        $bill->setBillDate($model->billDate);
        $bill->setName($model->name);
        $bill->setNumber($model->number);
        $bill->setIdType($model->billingTarget['idType']);
        $bill->setIdTarget($model->billingTarget[$billTargetId]);
        if ($target) {
            // Get invoice Address
            if ($idType == Business\Bill\Constant::TYPE_PAYMENT_ACQUISITEUR ||
                $idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR ||
                $target->getInvoiceAddressIsUsed() == 0) {
                $bill->setInvoiceAddressStreet($target->getStreet());
                $bill->setInvoiceAddressPostCode($target->getPostCode());
                $bill->setInvoiceAddressCity($target->getCity());
                $bill->setInvoiceAddressIdCountry($target->getCountryID());
                $bill->setInvoiceAddressTelephone($target->getTelephone());
                $bill->setInvoiceAddressFax($target->getFax());
            } else {
                $bill->setInvoiceAddressStreet($target->getInvoiceAddressStreet());
                $bill->setInvoiceAddressPostCode($target->getInvoiceAddressPostCode());
                $bill->setInvoiceAddressCity($target->getInvoiceAddressCity());
                $bill->setInvoiceAddressIdCountry($target->getInvoiceAddressIdCountry());
                $bill->setInvoiceAddressTelephone($target->getInvoiceAddressTelephone());
                $bill->setInvoiceAddressFax($target->getInvoiceAddressFax());
            }
            
            if ($idType != Business\Bill\Constant::TYPE_PAYMENT_ACQUISITEUR &&
                $idType != Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
                $bill->setDeliveryEmail($target->getInvoicedeliveryemail());
            } else {
                $bill->setDeliveryEmail($target->getInvoiceDeliveryEmail());
            }

            if ($idType != Business\Bill\Constant::TYPE_INVOICE_CUSTOMER) {
                if (Utils::isInvoice($idType)) {
                    $bill->setIsWithTax($target->getIsInvoiceWithTax());
                    $bill->setIdNoTaxText($target->getInvoiceNoTaxText());
                } else {
                    $bill->setIsWithTax($target->getIsPaymentWithTax());
                    $bill->setIdNoTaxText($target->getPaymentNoTaxText());
                }

                $bill->setInvoiceAddressUid($target->getUidNumber());
                $bill->setCommissionPaymentWithTax($target->getCommissionPaymentWithTax());
                if ($idType == Business\Bill\Constant::TYPE_PAYMENT_ACQUISITEUR || $idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
                    $bill->setIsInvoiceAddress(0);
                } else {
                    $bill->setIdCurrency($target->getIdCurrency());
                    $bill->setIsInvoiceAddress($target->getInvoiceAddressIsUsed());
                    $bill->setDeliveryNote($target->getInvoiceDeliveryNote());
                }
            } else if ($idType == Business\Bill\Constant::TYPE_INVOICE_CUSTOMER) {
                $bill->setInvoiceAddressUid($target->getInvoiceAddressUid());
                $bill->setIsInvoiceAddress($target->getInvoiceAddressIsUsed());
                $bill->setIsWithTax(true);
            }
        }

        $currentVAT = $bill->getCommissionPaymentWithTax();
        if (empty($currentVAT)) {
            // Default commission tax is 20
            $bill->setCommissionPaymentWithTax($config->getDefaultTax());
        }

        $em->persist($bill);
        $em->flush();

        $this->newId = $bill->getId();
        parent::onSuccess();
    }
}
