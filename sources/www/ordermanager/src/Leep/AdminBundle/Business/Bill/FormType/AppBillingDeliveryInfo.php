<?php
namespace Leep\AdminBundle\Business\Bill\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingDeliveryInfo extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'idBill' => 0,
            'idType' => 0,
            'idSource'  => '',
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_billing_delivery_info';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        $view->vars['idSource'] = $options['idSource'];
        $view->vars['idType'] = $options['idType'];
        $view->vars['getDeliveryInfo'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'getBillingDeliveryInfo');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('deliveryNote', 'choice', array(
            'label'    => 'Delivery Note',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Bill_DeliveyNoteTypes')
        ));
        $builder->add('deliveryEmail', 'text', array(
            'label'    => 'Delivery Email',
            'required'  => false
        ));
    }
}