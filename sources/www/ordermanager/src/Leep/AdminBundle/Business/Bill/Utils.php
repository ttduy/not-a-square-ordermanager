<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business;

class Utils {
    public static function postUpdateStatusList($container, $idBill) {
        $em = $container->get('doctrine')->getEntityManager();
        $em->flush();

        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($idBill);

        // Count reminders
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:BillStatus', 'p')
            ->andWhere('p.idBill = :idBill')
            ->andWhere('p.status = :reminderCountStatus')
            ->setParameter('reminderCountStatus', Constant::INVOICE_STATUS_REMINDERS_SENT)
            ->setParameter('idBill', $idBill);
        $total = intval($query->getQuery()->getSingleScalarResult());
        $bill->setNumberReminderCount($total);
        $em->persist($bill);
        $em->flush();
    }

    public static function computeTotalAmount($em, $table, $criteria) {
        $query = $em->createQueryBuilder();
        $query->select('SUM(IFNULL(p.overrideAmount, p.totalAmount))')
            ->from($table, 'p')
            ->andWhere($criteria);
        $totalAmount = $query->getQuery()->getSingleScalarResult();

        return $totalAmount;
    }

    public static function analysisAmount($em, $table) {
        $query = $em->createQueryBuilder();

        $query->select('b.status, SUM(IFNULL(p.overrideAmount, p.totalAmount)) totalAmount')
            ->from($table, 'p')
            ->innerJoin('AppDatabaseMainBundle:Bill', 'b', 'WITH', 'p.idBill = b.id')
            ->groupBy('b.status');

        $results = $query->getQuery()->getResult();
        $statistics = array();
        foreach ($results as $r) {
            $statistics[intval($r['status'])] = $r['totalAmount'];
        }

        return $statistics;
    }

    public static function getBillTypes($container) {
        $mapping = $container->get('easy_mapping');
        $billingHandler = $container->get('leep_admin.billing.business.billing_handler');
        $billingTypes = $mapping->getMapping('LeepAdmin_Bill_Type');
        foreach ($billingTypes as $idBillType => $name) {
            $totalOpen = $billingHandler->getBillingHandlerByType($idBillType)->getTotalOpen();
            if ($totalOpen > 0) {
                if (!in_array($idBillType, Business\Bill\Constant::getManualTypes())) {
                    $billingTypes[$idBillType] = $name.' ('.$totalOpen.' opens)';
                } else {
                    $billingTypes[$idBillType] = $name;
                }
            }
        }
        return $billingTypes;
    }

    public static function isInvoice($idType) {
        return in_array($idType, Constant::getSubBillTypes()[Constant::TYPE_INVOICE]);
    }

    public static function hasBillEntry($idType) {
        return !in_array($idType, Constant::getManualTypes());
    }

    public static function getTargetName($idType) {
        return Constant::getBillTargets()[$idType];
    }

    public static function getBillEntry($idType, $idEntry, $container) {
        if (!self::hasBillEntry($idType)) {
            // This type has no entry
            return null;
        }

        $targetName = self::getTargetName($idType);
        $billType = "Invoice";
        if (!self::isInvoice($idType)) {
            $billType = "Payment";
        }

        // Hack for DC
        if ($targetName == "DistributionChannels") {
            $targetName = "DistributionChannel";
        }

        $entryName = "Billing$billType$targetName";
        $em = $container->get('doctrine')->getEntityManager();
        return $em->getRepository("AppDatabaseMainBundle:$entryName")->findOneById($idEntry);
    }

    public static function getBillTarget($idType, $idTarget, $container) {
        $em = $container->get('doctrine')->getEntityManager();
        $targetName = self::getTargetName($idType);
        $entry = self::getBillEntry($idType, $idTarget, $container);
        if ($entry) {
            $getIdMethod = "getId$targetName";

            // Hack for DC
            if ($targetName == "DistributionChannels") {
                $getIdMethod = "getIdDistributionChannel";
            }

            return $em->getRepository("AppDatabaseMainBundle:$targetName")->findOneById($entry->$getIdMethod());
        }
        return $em->getRepository("AppDatabaseMainBundle:$targetName")->findOneById($idTarget);
    }
}
