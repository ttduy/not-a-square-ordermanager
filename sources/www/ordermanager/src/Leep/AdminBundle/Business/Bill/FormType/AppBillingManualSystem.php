<?php
namespace Leep\AdminBundle\Business\Bill\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppBillingManualSystem extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options) {
        return [
            'idBill' => 0
        ];
    }

    public function getParent() {
        return 'field';
    }

    public function getName() {
        return 'app_billing_manual_system';
    }

    public function finishView(FormView $view, FormInterface $form, array $options) {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');
        $grid = $this->container->get('leep_admin.bill.business.billing_manual_system_grid_reader');
        $view->vars['grid'] = array(
            'id' => 'manualOrderGrid',
            'header' => $grid->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'bill', 'feature', 'billingManualAjaxSource', array(
                'idBill' => $options['idBill']
            ))
        );

        $view->vars['addNewOrderUrl'] = $mgr->getUrl('leep_admin', 'bill', 'feature', 'addNewOrder', array('idBill' => $options['idBill']));
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $mapping = $this->container->get('easy_mapping');
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
    }
}
