<?php
namespace Leep\AdminBundle\Business\Bill\Widget;

use Leep\AdminBundle\Business;

class BillingFilesPaymentDistributionChannelHandler extends BillingFilesPaymentHandler {
    protected function presetConfig() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $this->filenameCode = 'payment_distribution_channel';
        $this->idReport = $config->getIdBillingReportPaymentDistributionChannel();
        $this->idReminderNoticeReport = $config->getIdBillingReportReminderNotice();
        $this->tableCode = 'BillingPaymentDistributionChannel';
        $this->tableEntriesLinkField = 'idBillingPaymentDistributionChannel';
        $this->tableEntriesCode = 'BillingPaymentDistributionChannelEntry';
        $this->idBillGroup = 'payment';
    }


    public function loadReportDataExtra(&$data, $bill) {
        if ($bill->getIdMarginPaymentMode() == Business\DistributionChannel\Constant::MARGIN_PAYMENT_MODE_INVOICE) {
            $data['headingTitle'] = 'Rechnung für Beratung/Invoice for counselling';
            $config = $this->container->get('leep_admin.helper.common')->getConfig();
            $this->idReport = $config->getIdBillingReportPaymentDistributionChannelInvoice();
        }
        else {
            $data['headingTitle'] = 'Gutschrift für Beratung/Credit note for counselling';
        }
    }

    public function loadReportDataClient(&$data, $bill) {
        $distributionChannel = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($bill->getIdTarget());

        $this->loadReportDataForDcOrPartner($data, $distributionChannel);

        $data['clientAccountName'] = $distributionChannel->getAccountName();
        $data['clientAccountNumber'] = $distributionChannel->getAccountNumber();
        $data['clientBankCode'] = $distributionChannel->getBankCode();
        $data['clientBic'] = $distributionChannel->getBIC();
        $data['clientIban'] = $distributionChannel->getIBAN();
    }

    public function generateBillingName($bill) {
        $distributionChannel = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($bill->getIdTarget());
        return $bill->getNumber().' - '.$distributionChannel->getDistributionChannel().' - '.$bill->getNumber();
    }

    public function generateCSVData($bill, $reportData) {
        $result = parent::generateCSVData($bill, $reportData);
        $billingPaymentDistributionChannel = $this->em->getRepository('AppDatabaseMainBundle:BillingPaymentDistributionChannel')->findOneByIdBill($bill->getId());
        if ($billingPaymentDistributionChannel) {
            $dc = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($billingPaymentDistributionChannel->getIdDistributionChannel());
            if ($dc) {
                $result['invoiceRestructure'] = $dc->getAccountingCode();
            }
        }

        return $result;
    }
}
