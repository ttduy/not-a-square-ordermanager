<?php
namespace Leep\AdminBundle\Business\Bill;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
class EditHandler extends AppEditHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->container = $this->container;
        $model->billDate = new \DateTime();

        return $model;
    }
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Bill', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($bill) {
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        return $billingHandler->getBillingHandlerByType($this->entity->getIdType())->billFormLoadData($bill);
    }

    public function buildForm($builder) {
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandlerByType($this->entity->getIdType())->billFormBuild($builder, $this->entity);

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        if($model['name']=='' || $model['number']==''){
            if($model['name']==''){
                $this->errors[] = "Error!";
                $this->getForm()->get('name')->addError(new FormError("This value should not be blank."));
            }
            if($model['number']==''){
                $this->errors[] = "Error!";
                $this->getForm()->get('number')->addError(new FormError("This value should not be blank."));
            }
            return false;
        }
        $em = $this->container->get('doctrine')->getEntityManager();
        $r =$em->getRepository('AppDatabaseMainBundle:Bill')->findOneByNumber($model['number']);
        if($r ){
            if($r->getId()!=$this->entity->getId()){
                $this->errors[] = "Error!";
                $this->getForm()->get('number')->addError(new FormError("number '" .$model['number']. "' already exist!"));
                return false;
            }
        }
            
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $billingHandler->getBillingHandlerByType($this->entity->getIdType())->billFormOnSuccess($this->entity, $model);
        parent::onSuccess();
        $this->reloadForm();
    }
}
