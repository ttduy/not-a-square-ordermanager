<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenlifeSampleRegistration', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->userName    = $entity->getUserName();
        $model->firstName   = $entity->getFirstName();
        $model->surName     = $entity->getSurName();
        $model->sex         = $entity->getSex();
        $model->barcode     = $entity->getBarcode();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('userName', 'text', array(
            'label'    => 'User Name',
            'required' => true
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => true
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => true
        ));
       
        $builder->add('sex', 'choice', array(
            'label'    => 'Sex',
            'required' => false,
            'choices'  => array(
                ''=> "None",
                'male'=> 'Male',
                'female'=>'Female',
            ),
        ));
        $builder->add('barcode', 'text', array(
            'label'    => 'Barcode',
            'required' => true
        ));
        
        return $builder;
    }

    public function onSuccess() {
        parent::onSuccess();
    }
}
