<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock;
use Leep\AdminBundle\Business\TextBlock\CreateHandler AS BaseTextBlockCreateHandler;

class CreateHandler extends BaseTextBlockCreateHandler {
   
    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');
        $group = Utils::getGenlifeTextBlockGroup($this->container);
        if ($group) {
            $builder->add('idGroup', 'choice', array(
                'label'    => 'Group',
                'required' => true,
                'choices'  => array($group->getId() => $group->getName()),
            ));
            $builder->add('name', 'text', array(
                'attr'     => array(
                    'placeholder' => 'Name'
                ),
                'required' => true
            ));                
            
            
            $builder->add('sectionConstraints', 'section', array(
                'label'    => 'Constraints',
                'property_path' => false
            ));
            $builder->add('maxLength', 'text', array(
                'label'    => 'Max Length',
                'required' => false
            ));
            $builder->add('isUppercase', 'checkbox', array(
                'label'    => 'Uppercase only?',
                'required' => false
            ));        
            $builder->add('numLineBreak', 'text', array(
                'label' => 'The number of line break',
                'required' => false
            ));
            $builder->add('keyWords', 'text', array(
                'label' => 'Key word',
                'required' => false
            ));
        
        
            $builder->add('sectionContent', 'section', array(
                'label'    => 'Text Blocks',
                'property_path' => false
            ));
        
            $languages = $mapping->getMapping('LeepAdmin_Language_List');
            foreach ($languages as $id => $name) {
                $builder->add('language_'.$id, 'textarea', array(
                    'label'    => $name,
                    'required' => false,
                    'attr'     => array('rows' => 5, 'cols' => 80)
                ));
            }
        
            return $builder;
        }
    }
    //
    //public function onSuccess() {
    //    $model = $this->getForm()->getData();
    //    $em = $this->container->get('doctrine')->getEntityManager();        
    //
    //    // Check
    //    $existingTextBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneByName(trim($model['name']));
    //    if ($existingTextBlock) {
    //        $this->errors = array("The text block '".trim($model['name'])."' is already existed");
    //        return;
    //    }
    //
    //    // Perform add
    //    $textBlock = new Entity\TextBlock();
    //    $textBlock->setIdGroup($model['idGroup']);
    //    $textBlock->setName($model['name']);
    //    $textBlock->setMaxLength($model['maxLength']);
    //    $textBlock->setIsUppercase($model['isUppercase']);
    //    $textBlock->setNumLineBreak($model['numLineBreak']);
    //    $textBlock->setKeyWords($model['keyWords']);
    //
    //    $em->persist($textBlock);
    //    $em->flush();
    //
    //    // Save text block languages
    //    $now = new \DateTime();
    //    foreach ($model as $k => $v) {
    //        if (strpos($k, 'language_') === 0) {
    //            $idLanguage = substr($k, 9);
    //            $textBlockLanguage = new Entity\TextBlockLanguage();
    //            $textBlockLanguage->setIdLanguage($idLanguage);
    //            $textBlockLanguage->setIdTextBlock($textBlock->getId());
    //            $textBlockLanguage->setBody($v);
    //            $em->persist($textBlockLanguage);
    //        }
    //    }
    //    $em->flush();
    //    
    //    parent::onSuccess();
    //}
}