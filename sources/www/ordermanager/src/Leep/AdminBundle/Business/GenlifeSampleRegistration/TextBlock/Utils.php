<?php 
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock;
    
class Utils {
    public static $group_name = 'Genlife Text Block';
    public static function getGenlifeTextBlockGroup($container) {
        $group = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlockGroup', 'app_main')->findOneByName(self::$group_name);
        return $group;

    }
}