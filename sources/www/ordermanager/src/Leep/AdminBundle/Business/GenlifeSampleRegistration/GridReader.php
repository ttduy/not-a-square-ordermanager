<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('userName', 'firstName','surName', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Username', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'First Name', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Sur Name', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenlifeSampleRegistration', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();


        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genlifeSampleRegistrationModify')) {
            $builder->addButton('View', $mgr->getUrl('leep_admin', 'genlife_sample_registration', 'edit', 'edit', array('id' => $row->getId())), 'button-detail');
        }

        return $builder->getHtml();
    }
}
