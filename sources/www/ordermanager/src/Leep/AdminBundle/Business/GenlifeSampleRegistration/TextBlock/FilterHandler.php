<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock;
use Leep\AdminBundle\Business\TextBlock\FilterHandler AS BaseTextBlockFilterHandler;

class FilterHandler extends BaseTextBlockFilterHandler {
    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('text', 'text', array(
            'label'    => 'Text',
            'required' => false
        ));
        return $builder;
    }
}