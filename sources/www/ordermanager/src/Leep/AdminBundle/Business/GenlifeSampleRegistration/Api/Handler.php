<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration\Api;

use Leep\AdminBundle\Business\ReportApi\BaseReportApi;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock\Utils;
use Leep\AdminBundle\Business;

class Handler extends BaseReportApi {
    public function getValue($data, $k, $default = '') {
        return isset($data[$k]) ? $data[$k] : $default;
    }
    public function execute() {
        $em = $this->get('doctrine')->getEntityManager();
        $request = $this->get('request');
        $data = @json_decode($request->getContent(), true);
        switch (@$data['action']){
            case "create";
                $entity = new Entity\GenlifeSampleRegistration();
                $entity->setCreated(new \DateTime());
                return $this->save($entity,$data);
                
            case "update";
                $entity = $this->get('doctrine')->getRepository('AppDatabaseMainBundle:GenlifeSampleRegistration')->findOneByCode('G_'.$data['id']);
                if (empty($entity)) {
                    $entity = new Entity\GenlifeSampleRegistration();
                    $entity->setCreated(new \DateTime());  
                }
                return  $this->save($entity,$data);
                
            case "delete";
                $entity = $this->get('doctrine')->getRepository('AppDatabaseMainBundle:GenlifeSampleRegistration')->findOneByCode('G_'.$data['id']);
                return $this->delete($entity,$data);
            case "update_translation";
                return $this->exportTranslation();
        } 
        
    }
 
    public function save($entity,$data) {
        $em = $this->get('doctrine')->getEntityManager();
        $entity->setCode('G_'.$data['id']);
        $entity->setUserName($data['user_name']);
        $entity->setPassword($data['password']);
        $entity->setFirstName($data['first_name']);
        $entity->setSurName($data['sur_name']);
        $entity->setAge($data['age']);
        $entity->setSex($data['sex']);
        $entity->setBarcode($data['barcode']);
        
        $em->persist($entity);
        $em->flush();
        return 'G_'.$data['id'];

   
    }
    public function delete($entity,$data) {
        $em = $this->get('doctrine')->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }
    
    public function exportTranslation(){
        $group = Utils::getGenlifeTextBlockGroup($this->container);
        
        if ($group) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $query = $em->createQueryBuilder();
            $query->select('p.id, tb.name, l.code, p.body')
                ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->leftJoin('AppDatabaseMainBundle:TextBlock', 'tb', 'WITH', 'tb.id = p.idTextBlock')
                ->leftJoin('AppDatabaseMainBundle:TextBlockGroup', 'tbg', 'WITH', 'tb.idGroup = tbg.id')
                ->leftJoin('AppDatabaseMainBundle:Language', 'l', 'WITH', 'l.id = p.idLanguage')
                ->andWhere('tbg.id = :idGroup')->setParameter('idGroup',$group->getId())
                ->andWhere('l.code IS NOT NULL');
        } 
        $result = $query->getQuery()->getResult();
        return $result;
    }
}
