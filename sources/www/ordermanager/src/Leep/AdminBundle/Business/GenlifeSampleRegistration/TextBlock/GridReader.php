<?php
namespace Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock;
use Leep\AdminBundle\Business\TextBlock\GridReader AS BaseTextBlockGridReader;
use Leep\AdminBundle\Business\GenlifeSampleRegistration\TextBlock\Utils;

class GridReader extends BaseTextBlockGridReader {

    public function buildQuery($queryBuilder) {
        parent::buildQuery($queryBuilder);
        $group = Utils::getGenlifeTextBlockGroup($this->container);
        if ($group) {
            $queryBuilder->andWhere('p.idGroup = :idGroup')
            ->setParameter('idGroup',$group->getId());
        } 
    }
    
    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
    
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genlifeTextBlockModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'genlife_text_block', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'genlife_text_block', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }
        return $builder->getHtml();
    }
    
}