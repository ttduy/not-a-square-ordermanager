<?php
namespace Leep\AdminBundle\Business\Meeting;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('title', 'text', array(
            'label'       => 'Title',
            'required'    => false
        ));

        return $builder;
    }
}