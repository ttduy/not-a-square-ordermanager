<?php
namespace Leep\AdminBundle\Business\Meeting;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('meetingDatetime', 'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('users', 'choice', array(
            'label'       => 'Participants',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('sectionEntry', 'section', array(
            'label'       => 'Entries',
            'property_path' => false
        ));
        $builder->add('entries', 'container_collection', array(
            'label'    => 'Entry',
            'required' => false,
            'type'     => new FormType\AppMeetingEntryRow($this->container)
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $meeting = new Entity\Meeting();
        $meeting->setTitle($model->title);
        $meeting->setMeetingDatetime($model->meetingDatetime);
        $em->persist($meeting);
        $em->flush();

        foreach ($model->users as $idUser) {
            $meetingUser = new Entity\MeetingUser();
            $meetingUser->setIdMeeting($meeting->getId());
            $meetingUser->setIdUser($idUser);
            $em->persist($meetingUser);
        }

        foreach ($model->entries as $entry) {
            $meetingEntry = new Entity\MeetingEntry();
            $meetingEntry->setIdMeeting($meeting->getId());
            $meetingEntry->setName($entry->name);
            $meetingEntry->setDescription($entry->description);
            $meetingEntry->setIdTodoTask(0);
            $em->persist($meetingEntry);
        }
        
        $em->flush();
        
        $this->newId = $meeting->getId();
        parent::onSuccess();
    }
}
