<?php
namespace Leep\AdminBundle\Business\Meeting;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateTaskHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $request = $this->container->get('request');
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $idMeeting = $request->get('idMeeting', 0);

        $model = new CreateTaskModel();        
        $user = $userManager->getUserSession()->getUser();

        $model->creationDate = new \DateTime();
        $model->idWebUser = $user->getId();
        $model->idCategory = $this->container->get('request')->get('idCategory', 0);
        $model->idType = Business\TodoTask\Constants::TYPE_GOAL;


        $meeting = $em->getRepository('AppDatabaseMainBundle:Meeting')->findOneById($idMeeting);
        $model->name = $meeting->getTitle();

        $entries = array();
        $meetingEntries = $em->getRepository('AppDatabaseMainBundle:MeetingEntry')->findByIdMeeting($idMeeting);
        foreach ($meetingEntries as $entry) {
            $entries[] = strtoupper($entry->getName())."\n".$entry->getDescription();
        }
        $model->description = implode("\n-----------------------\n", $entries);

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Goal type',
            'required' => true,
            'choices'  => Business\TodoTask\Constants::getTaskGoalTypes()
        ));
        $builder->add('idContinualImprovement', 'choice', array(
            'label'    => 'Continual improvement',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoContinualImprovement_List')
        ));
        $builder->add('idGoal', 'choice', array(
            'label'    => 'Goal',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_List')
        ));
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoCategory_List')
        ));
        $builder->add('idWebUser', 'choice', array(
            'label'    => 'Assign to',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'     => 15,
                'cols'     => 80
            )
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'empty_value' => false,
            'choices' => $mapping->getMapping('LeepAdmin_TodoTask_Status')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $task = new Entity\TodoTask();
        $task->setCreationDate($model->creationDate);
        $task->setIdCategory($model->idCategory);
        $task->setIdWebUser($model->idWebUser);
        $task->setName($model->name);
        $task->setDescription($model->description);
        $task->setStatus($model->status);
        if ($model->idType == Business\TodoTask\Constants::TYPE_GOAL) {
            $task->setIdGoal($model->idGoal);
            $task->setIdContinualImprovement(0);
        } 
        else {
            $task->setIdGoal(0);
            $task->setIdContinualImprovement($model->idContinualImprovement);
        }
        
        $em->persist($task);
        $em->flush();
        parent::onSuccess();
    }
}
