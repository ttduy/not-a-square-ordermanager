<?php
namespace Leep\AdminBundle\Business\Meeting;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $title;
    public $meetingDatetime;
    public $users;
    public $entries;
}
