<?php
namespace Leep\AdminBundle\Business\Meeting;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('title', 'meetingDatetime', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Title',     'width' => '50%', 'sortable' => 'true'),
            array('title' => 'Datetime', 'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Action',   'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_MeetingGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Meeting', 'p');
        if (trim($this->filters->title) != '') {
            $queryBuilder->andWhere('p.title LIKE :title')
                ->setParameter('title', '%'.trim($this->filters->title).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();


        $helperSecurity = $this->container->get('leep_admin.helper.security');            
        //$builder->addButton('CreateTask', $mgr->getUrl('leep_admin', 'meeting', 'create_task', 'create', array('idMeeting' => $row->getId())), 'button-export');

        if ($helperSecurity->hasPermission('meetingModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'meeting', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'meeting', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
