<?php
namespace Leep\AdminBundle\Business\Meeting;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Meeting', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($meeting) {
        $doctrine = $this->container->get('doctrine');

        $model = new EditModel();  
        $model->title = $meeting->getTitle();
        $model->meetingDatetime = $meeting->getMeetingDatetime();

        $model->users = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:MeetingUser')->findByIdMeeting($meeting->getId());
        foreach ($result as $r) {
            $model->users[] = $r->getIdUser();
        }

        $model->entries = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:MeetingEntry')->findByIdMeeting($meeting->getId());
        foreach ($result as $r) {
            $entry = new FormType\AppMeetingEntryRowModel();
            $entry->name = $r->getName();
            $entry->description = $r->getDescription();

            $model->entries[] = $entry;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('meetingDatetime', 'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('users', 'choice', array(
            'label'       => 'Participants',
            'required'    => false,
            'multiple'    => 'multiple',
            'attr'        => array('style' => 'height: 100px'),
            'choices'     => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('sectionEntry', 'section', array(
            'label'       => 'Entries',
            'property_path' => false
        ));
        $builder->add('entries', 'container_collection', array(
            'label'    => 'Entry',
            'required' => false,
            'type'     => new FormType\AppMeetingEntryRow($this->container)
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $filters = array('idMeeting' => $this->entity->getId());

        $this->entity->setTitle($model->title);
        $this->entity->setMeetingDatetime($model->meetingDatetime);
                
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MeetingUser', $filters);
        foreach ($model->users as $idUser) {
            $meetingUser = new Entity\MeetingUser();
            $meetingUser->setIdMeeting($this->entity->getId());
            $meetingUser->setIdUser($idUser);
            $em->persist($meetingUser);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:MeetingEntry', $filters);
        foreach ($model->entries as $entry) {
            $meetingEntry = new Entity\MeetingEntry();
            $meetingEntry->setIdMeeting($this->entity->getId());
            $meetingEntry->setName($entry->name);
            $meetingEntry->setDescription($entry->description);
            $meetingEntry->setIdTodoTask(0);
            $em->persist($meetingEntry);
        }
        
        $em->flush();
        parent::onSuccess();
    }
}
