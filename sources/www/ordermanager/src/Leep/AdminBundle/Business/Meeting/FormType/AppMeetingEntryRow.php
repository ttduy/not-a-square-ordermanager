<?php
namespace Leep\AdminBundle\Business\Meeting\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppMeetingEntryRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Meeting\FormType\AppMeetingEntryRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_meeting_entry_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        //$view->vars['createTaskUrl'] = $mgr->getUrl('leep_admin', 'todo_task', 'create', 'create');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'required' => false
        ));
        $builder->add('description', 'textarea', array(
            'required' => false,
            'attr'     => array('rows' => 7, 'cols' => 80)
        ));
    }
}