<?php
namespace Leep\AdminBundle\Business\Meeting;

use Symfony\Component\Validator\ExecutionContext;

class CreateTaskModel {
    public $creationDate;
    public $idCategory;
    public $idType;
    public $idContinualImprovement;
    public $idGoal;
    public $idWebUser;
    public $name;    
    public $description;    
    public $status;
}
