<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class BillingPaymentHandler extends BillingHandler {
    public function getBillStatusMapping() {
        return 'LeepAdmin_Bill_PaymentStatus';
    }

    public function isInvoiceHandler() {
        return false;
    }

    protected function billFormBuildBillPaymentModeSection($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionPaymentModeSection', 'section', array(
            'label'    => 'Payment mode',
            'property_path' => false
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'empty_value' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
    }
}
