<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Helper\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class BillingHandler {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function isInvoiceHandler() {
        return true;
    }

    protected $billingHandlers = array(
        'invoiceCustomer'               => 'leep_admin.billing.business.handler.billing_invoice_customer_handler',
        'invoiceDistributionChannel'    => 'leep_admin.billing.business.handler.billing_invoice_distribution_channel_handler',
        'invoicePartner'                => 'leep_admin.billing.business.handler.billing_invoice_partner_handler',
        'paymentAcquisiteur'            => 'leep_admin.billing.business.handler.billing_payment_acquisiteur_handler',
        'paymentDistributionChannel'    => 'leep_admin.billing.business.handler.billing_payment_distribution_channel_handler',
        'paymentPartner'                => 'leep_admin.billing.business.handler.billing_payment_partner_handler',
    );

    protected $billingHandlersByType = array(
        // Invoice
        Business\Bill\Constant::TYPE_INVOICE_CUSTOMER                => 'leep_admin.billing.business.handler.billing_invoice_customer_handler',
        Business\Bill\Constant::TYPE_INVOICE_DISTRIBUTION_CHANNEL    => 'leep_admin.billing.business.handler.billing_invoice_distribution_channel_handler',
        Business\Bill\Constant::TYPE_INVOICE_PARTNER                 => 'leep_admin.billing.business.handler.billing_invoice_partner_handler',
        Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC               => 'leep_admin.billing.business.handler.billing_invoice_distribution_channel_handler',
        Business\Bill\Constant::TYPE_INVOICE_MANUAL_PARTNER          => 'leep_admin.billing.business.handler.billing_invoice_partner_handler',

        // Payment
        Business\Bill\Constant::TYPE_PAYMENT_ACQUISITEUR             => 'leep_admin.billing.business.handler.billing_payment_acquisiteur_handler',
        Business\Bill\Constant::TYPE_PAYMENT_DISTRIBUTION_CHANNEL    => 'leep_admin.billing.business.handler.billing_payment_distribution_channel_handler',
        Business\Bill\Constant::TYPE_PAYMENT_PARTNER                 => 'leep_admin.billing.business.handler.billing_payment_partner_handler',
        Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER          => 'leep_admin.billing.business.handler.billing_payment_partner_handler',
        Business\Bill\Constant::TYPE_PAYMENT_MANUAL_DC               => 'leep_admin.billing.business.handler.billing_payment_distribution_channel_handler',
        Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR      => 'leep_admin.billing.business.handler.billing_payment_acquisiteur_handler',
    );


    public function getBillingHandler($id) {
        return $this->container->get($this->billingHandlers[$id]);
    }
    public function getBillingHandlerByType($idType) {
        return $this->container->get($this->billingHandlersByType[$idType]);
    }

    /////////////////////////////////////////////////////////
    // x. DELETE BILL
    public function deleteBill($bill) {}

    /////////////////////////////////////////////////////////
    // x. FORM - BUILD FORM
    protected function billFormBuildBillBasicInformation($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('sectionBillInformation', 'section', array(
            'label'    => 'Basic information',
            'property_path' => false
        ));
        $builder->add('idType', 'label', array(
            'label'    => 'Type',
            'required' => false
        ));
        $builder->add('billDate', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => true
        ));
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => true
        ));
        
        if ($this->isInvoiceHandler()) {
            $builder->add('isWithTax', 'checkbox', array(
                'label'  => 'Is Invoice with tax?',
                'required' => false
            ));
        } else {
            $builder->add('isWithTax', 'checkbox', array(
                'label'  => 'Is Payment with tax?',
                'required' => false
            ));
        }

        
        
        if (!Business\Bill\Utils::isInvoice($bill->getIdType())) {
            $builder->add('idNoTaxText', 'choice', array(
                'label'    => 'Payment No Tax Text',
                'required' => false,
                'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
            ));
        } else {
            $builder->add('idNoTaxText', 'choice', array(
                'label'    => 'Invoice No Tax Text',
                'required' => false,
                'choices'  => Business\InvoiceNoTaxText\Constant::get($this->container)
            ));
        }
        
        if (!$this->isInvoiceHandler()) {
            $builder->add('commissionPaymentWithTax', 'text', array(
                'label'  => 'Commission payment with tax(%)',
                'required' => false,
                'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
            ));
        }

        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',
            'required' => false
        ));

    }

    protected function billFormBuildBillFromSection($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionFrom', 'section', array(
            'label'    => 'From',
            'property_path' => false
        ));
        $builder->add('idCompany', 'choice', array(
            'label'    => 'Company',
            'required' => true,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
    }

    protected function billFormBuildBillToSection($builder, $bill) {}
    protected function billFormBuildBillPaymentModeSection($builder, $bill) {}

    protected function billFormBuildBillExtraTextSection($builder, $bill) {
        $builder->add('sectionText', 'section', array(
            'label'    => 'Extra text',
            'property_path' => false
        ));
        $builder->add('extraTextAfterDescription', 'textarea', array(
            'label'    => 'After description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 90
            )
        ));
        $builder->add('extraTextFooter', 'textarea', array(
            'label'    => 'Footer',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 90
            )
        ));
    }

    public function getBillStatusMapping() {
        return 'LeepAdmin_Bill_Status';
    }

    protected function billFormBuildBillStatusSection($builder, $bill) {
        $today = new \DateTime();
        // Status
        $builder->add('sectionStatus', 'section', array(
            'label'    => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new Business\FormType\AppBillStatusRow($this->container, $today->format('d/m/Y'), $this->getBillStatusMapping()),
        ));
    }

    protected function billFormBuildBillNoteSystemSection($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');
        if ($bill->getIdType() != Business\Bill\Constant::TYPE_INVOICE_CUSTOMER &&
            $bill->getIdType() != Business\Bill\Constant::TYPE_PAYMENT_ACQUISITEUR &&
            $bill->getIdType() != Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $today = new \DateTime();
            $tableTarget = '';
            $idTargetField = '';
            $mappingCode='';
            $targetLabel = '';
            $labelSection = '';
            if ($bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_DISTRIBUTION_CHANNEL || $bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC) {
                $tableTarget = 'BillingInvoiceDistributionChannel';
                $idTargetField= 'idDistributionChannel';
                $mappingCode= 'LeepAdmin_DistributionChannel_List';
                $targetLabel = 'DC';
            } else if ($bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_DISTRIBUTION_CHANNEL || $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_DC) {
                $tableTarget = 'BillingPaymentDistributionChannel';
                $idTargetField= 'idDistributionChannel';
                $mappingCode= 'LeepAdmin_DistributionChannel_List';
                $targetLabel = 'DC';
            } else if ($bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_PARTNER || $bill->getIdType() == Business\Bill\Constant::TYPE_INVOICE_MANUAL_PARTNER) {
                $tableTarget = 'BillingInvoicePartner';
                $idTargetField= 'idPartner';
                $mappingCode= 'LeepAdmin_Partner_List';
                $targetLabel = 'Partner';
            } else if ($bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_PARTNER || $bill->getIdType() == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER) {
                $tableTarget = 'BillingPaymentPartner';
                $idTargetField= 'idPartner';
                $targetLabel = 'Partner';
                $mappingCode= 'LeepAdmin_Partner_List';
            }

            $billingUtils = $this->container->get('leep_admin.billing.business.utils');
            $arrayTarget = [];
            if (!Business\Bill\Utils::hasBillEntry($bill->getIdType())) {
                $arrayTarget = $mapping->getMapping($mappingCode);
            } else {
                $arrayTarget = $billingUtils->getOpenEntries($bill->getId(), $tableTarget, $idTargetField, $mappingCode);
            }

            $targetRow = $em->getRepository('AppDatabaseMainBundle:'. $tableTarget)->findByIdBill($bill->getId());
            $customers = [];
            foreach ($targetRow as $key => $row) {
                if ($row) {
                    $customers [$row->getIdCustomer()] = $row->getname();
                }
            }

            // Status
            $labelSection= 'Note';

            $builder->add('sectionNoteSystem', 'section', array(
                'label'    => $labelSection,
                'property_path' => false
            ));
            $builder->add('noteList', 'container_collection', array(
                'label' =>      'Note',
                'required' =>   false,
                'type' =>       new Business\FormType\AppBillNoteSystemRow($this->container, $today->format('d/m/Y'), $arrayTarget, $targetLabel, $customers)
            ));
        }
    }

    protected function billFormBuildBillFilesSection($builder, $bill) {
        $builder->add('sectionInvoiceFile', 'section', array(
            'label'    => 'Files',
            'property_path' => false
        ));
        $builder->add('invoicePdf', 'app_billing_files', array(
            'label'    => 'PDF Files',
            'required' => false,
            'idBill'   => $bill->getId(),
            'idReport' => $this->getBillingFilesHandler()->idReport
        ));
    }

    protected function billFormBuildBillManualSystemSection($builder, $bill) {
        $today = new \DateTime();
        $builder->add('sectionManual', 'section', array(
            'label'    => 'Manual System',
            'property_path' => false
        ));
        $builder->add('manualProductSystem', 'container_collection', array(
            'label' =>      'Manual System',
            'required' =>   false,
            'type' =>       new Business\FormType\AppBillManualProductRow($this->container, $today->format('d/m/Y'), $this->isInvoiceHandler())
        ));
    }

    public function billFormBuild($builder, $bill) {
        $this->billFormBuildBillBasicInformation($builder, $bill);
        $this->billFormBuildBillFromSection($builder, $bill);
        $this->billFormBuildBillToSection($builder, $bill);
        $this->billFormBuildBillManualSystemSection($builder, $bill);
        $this->billFormBuildBillPaymentModeSection($builder, $bill);
        $this->billFormBuildBillExtraTextSection($builder, $bill);
        $this->billFormBuildBillStatusSection($builder, $bill);
        $this->billFormBuildBillNoteSystemSection($builder, $bill);
        $this->billFormBuildBillFilesSection($builder, $bill);
    }

    ///////////////////////////////////////////////////////
    // x. COMMON
    public function hookBill($tableCode, $entryList, $idBill) {
        if (empty($entryList)) {
            $entryList = array(0);
        }
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:'.$tableCode, 'p')
            ->set('p.idBill', $idBill)
            ->andWhere('p.id in (:entryList)')
            ->setParameter('entryList', $entryList);
        $query->getQuery()->execute();
    }

    public function unhookBill($tableCode, $idBill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:'.$tableCode, 'p')
            ->set('p.idBill', 'NULL')
            ->andWhere('p.idBill = :idBill')
            ->setParameter('idBill', $idBill);
        $query->getQuery()->execute();
    }

    public function overrideBill($tableCode, $overridePrices) {
        $entryList = empty($overridePrices) ? array() : array_keys($overridePrices);
        if (empty($entryList)) {
            $entryList = array(0);
        }

        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:'.$tableCode, 'p')
            ->andWhere('p.id in (:entryList)')
            ->setParameter('entryList', $entryList);
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $id = $r->getId();
            if (isset($overridePrices[$id])) {
                $r->setOverrideAmount($overridePrices[$id] == '' ? NULL : $overridePrices[$id]);
            }
        }
        $em->flush();
    }

    /////////////////////////////////////////////////////////
    // x. GET TOTAL OPEN
    protected function queryTotalOpen($table) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) as totalOpen')
            ->from('AppDatabaseMainBundle:'.$table, 'p')
            ->andWhere('(p.idBill = 0) OR (p.idBill IS NULL)');
        return intval($query->getQuery()->getSingleScalarResult());
    }

    ///////////////////////////////////////////////////////
    // x. FORM - LOAD DATA
    protected function billFormLoadDataExtra($bill) {
        return array();
    }

    public function billFormLoadData($bill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $currencyRate = new Business\FormType\AppCurrencyRateModel();
        $currencyRate->idCurrency = $bill->getIdCurrency();
        $currencyRate->exchangeRate = $bill->getExchangeRate();

        // Status
        $statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BillStatus', 'p')
            ->andWhere('p.idBill = :idBill')
            ->setParameter('idBill', $bill->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppBillStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $statusList[] = $m;
        }

        /// Manual System List
        $arraySystemNote = [];
        $noteList = $em->getRepository('AppDatabaseMainBundle:BillManualSystemNote')->findByIdBill($bill->getId());
        foreach ($noteList as $key => $value) {
            if ($value) {
                $temp = new Business\FormType\AppBillNoteSystemRowModel();
                $temp->date = $value->getDate();
                $temp->text = $value->getText();
                $temp->notes= $value->getNote();
                $temp->idCustomer = $value->getIdCustomer();
                $temp->idTarget = $value->getIdTarget();
                $temp->cost = $value->getCost();
                $arraySystemNote [] = $temp;
            }
        }

        /// Manual Product List
        $arraySystemProduct = [];
        $lst = $em->getRepository('AppDatabaseMainBundle:BillManualSystemText')->findByIdBill($bill->getId());
        foreach ($lst as $key => $value) {
            if ($value) {
                $temp = new Business\FormType\AppBillManualProductRowModel();
                $temp->date = $value->getDate();
                $temp->text = $value->getText();
                $temp->description= $value->getDescription();
                $temp->customer = $value->getCustomer();
                $temp->orderNumber = $value->getOrderNumber();
                $temp->price = $value->getPrice();
                $arraySystemProduct [] = $temp;
            }
        }

        Business\Bill\Utils::postUpdateStatusList($this->container, $bill->getId());

        // Return
        $data = array(
            'idType' =>                     $formatter->format($bill->getIdType(), 'mapping', 'LeepAdmin_Bill_Type'),
            'billDate' =>                   $bill->getBillDate(),
            'name' =>                       $bill->getName(),
            'number' =>                     $bill->getNumber(),
            'idNoTaxText' =>                $bill->getIdNoTaxText(),
            'idCompany' =>                  $bill->getIdCompany(),
            'isWithTax' =>                  $bill->getIsWithTax(),
            'commissionPaymentWithTax' =>   $bill->getCommissionPaymentWithTax(),
            'currencyRate' =>               $currencyRate,
            'extraTextAfterDescription' =>  $bill->getExtraTextAfterDescription(),
            'extraTextFooter' =>            $bill->getExtraTextFooter(),
            'idMarginPaymentMode' =>        $bill->getIdMarginPaymentMode(),
            'statusList' =>                 $statusList,
            'noteList' =>                   $arraySystemNote,
            'manualProductSystem' =>        $arraySystemProduct
        );

        $subData = $this->billFormLoadDataExtra($bill);
     
       
        if (!empty($subData) && is_array($subData)) {
            foreach ($subData as $k => $v) {
                $data[$k] = $v;

            }
        }
        return $data;
    }

    ///////////////////////////////////////////////////////
    // x. FORM - SAVE DATA
    protected function billFormOnSuccessExtra($bill, $data) {
    }

    public function billFormOnSuccess($bill, $data) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        // Update data
        $bill->setBillDate($data['billDate']);
        $bill->setName($data['name']);
        $bill->setNumber($data['number']);
        $bill->setIdNoTaxText($data['idNoTaxText']);
        $bill->setIdCompany($data['idCompany']);
        $bill->setIsWithTax($data['isWithTax']);
        $bill->setCommissionPaymentWithTax($data['commissionPaymentWithTax']);
        if ($data['currencyRate']) {
            $bill->setIdCurrency($data['currencyRate']->idCurrency);
            $bill->setExchangeRate($data['currencyRate']->exchangeRate);
        }

        $bill->setIdMarginPaymentMode(isset($data['idMarginPaymentMode']) ? $data['idMarginPaymentMode'] : null);
        $bill->setExtraTextAfterDescription($data['extraTextAfterDescription']);
        $bill->setExtraTextFooter($data['extraTextFooter']);

        // Update status
        $filters = array('idBill' => $bill->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:BillStatus', $filters);
        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($data['statusList'] as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\BillStatus();
            $status->setIdBill($bill->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }
        $bill->setStatus($lastStatus);
        $bill->setStatusDate($lastDate);
        /// Update Manual System
        $filters = array('idBill' => $bill->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:BillManualSystemNote', $filters);
        foreach ($data['noteList'] as $key => $system) {
            if ($system) {
                $manualSystem = new Entity\BillManualSystemNote();
                $manualSystem->setDate($system->date);
                $manualSystem->setIdCustomer($system->idCustomer);
                $manualSystem->setIdTarget($system->idTarget);
                $manualSystem->setIdBill($bill->getId());
                $manualSystem->setText($system->text);
                $manualSystem->setCost($system->cost);
                $manualSystem->setNote($system->notes);
                $em->persist($manualSystem);
            }
        }


        /// Update Manual Product System
        $filters = array('idBill' => $bill->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:BillManualSystemText', $filters);
        foreach ($data['manualProductSystem'] as $key => $system) {
            if ($system) {
                $manualSystem = new Entity\BillManualSystemText();
                $manualSystem->setDate($system->date);
                $manualSystem->setCustomer($system->customer);
                $manualSystem->setIdBill($bill->getId());
                $manualSystem->setText($system->text);
                $manualSystem->setPrice($system->price);
                $manualSystem->setDescription($system->description);
                $manualSystem->setOrderNumber($system->orderNumber);
                $manualSystem->setTaxPercentage($system->taxPercentage);
                $em->persist($manualSystem);
            }
        }
        // Extra modification
        $this->billFormOnSuccessExtra($bill, $data);

        // Save
        $em->flush();
    }

    //////////////////////////////////////////////
    // x. ORDER SELECTOR
    public function getOrderSelectorHandler() {

    }

    public function getTargetBillingData($idType, $id) {
        return array();
    }

    //////////////////////////////////////////////
    // x. GENERATE PDF
    public function getBillingFilesHandler() {
        return null;
    }

    public function generatePdf($bill) {
        $billingFilesHandler = $this->getBillingFilesHandler();
        $em = $this->container->get('doctrine')->getEntityManager();
        $builder = $this->container->get('leep_admin.report.business.builder');
        $builder->init();
        $builder->mode = 'full';
        $builder->applyLanguage(1);
        $builder->configData = $billingFilesHandler->generateReportData($bill);
        $dataCSV = $this->handleBuildCSV($billingFilesHandler->generateCSVData($bill, $builder->configData));

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $billingFilesHandler->idReport)
            ->orderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();
        foreach ($sections as $section) {
            if ($section->getIdType() == 2) {
                $idSection = $section->getIdReportSectionLink();
            } else {
                $idSection = $section->getId();
            }

            // Get text tools
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $idSection)
                ->orderBy('p.sortOrder', 'ASC');
            $result = $query->getQuery()->getResult();
            $textTools = array();
            foreach ($result as $row) {
                if ($row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER ||
                    $row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_LAYOUT) {
                    $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
                }
                else {
                    $textTools[] = array(
                        'type' => $row->getIdTextToolType(),
                        'data' => json_decode($row->getData(), true)
                    );
                }
            }

            // Build report
            $builder->prepareNextSection();
            $builder->resetLayout();
            $builder->moveNextPage();
            foreach ($textTools as $textTool) {
                $builder->addTextTool($textTool['type'], $textTool['data']);
            }

            $builder->endPage();
        }

        $filename = $billingFilesHandler->generateBillingFilename($bill);
        $name = $billingFilesHandler->generateBillingName($bill);
        $outputFile = $this->container->getParameter('kernel.root_dir').'/../files/bills/'.$filename;
        $builder->getPdf()->Output($outputFile, 'F');
        return array(
            'filename' =>       $filename,
            'name' =>           $name,
            'totalAmount' =>    $billingFilesHandler->totalAmountWithTax,
            'dataCSV' =>        $dataCSV
        );
    }


    public function generateReminderNoticePdf($bill) {
        $today = new \DateTime();
        $billingFilesHandler = $this->getBillingFilesHandler();
        $em = $this->container->get('doctrine')->getEntityManager();
        $builder = $this->container->get('leep_admin.report.business.builder');
        $builder->init();
        $builder->mode = 'full';
        $builder->applyLanguage(1);
        $builder->configData = $billingFilesHandler->generateReportReminderData($bill);

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $billingFilesHandler->idReminderNoticeReport)
            ->orderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();

        foreach ($sections as $section) {
            if ($section->getIdType() == 2) {
                $idSection = $section->getIdReportSectionLink();
            }
            else {
                $idSection = $section->getId();
            }

            // Get text tools
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $idSection)
                ->orderBy('p.sortOrder', 'ASC');
            $result = $query->getQuery()->getResult();
            $textTools = array();
            foreach ($result as $row) {
                if ($row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER ||
                    $row->getIdTextToolType() == Business\Report\Constants::TEXT_TOOL_TYPE_STRUCTURE_PAGE_LAYOUT) {
                    $builder->addTextTool($row->getIdTextToolType(), json_decode($row->getData(), true));
                }
                else {
                    $textTools[] = array(
                        'type' => $row->getIdTextToolType(),
                        'data' => json_decode($row->getData(), true)
                    );
                }
            }

            // Build report
            $builder->prepareNextSection();
            $builder->resetLayout();
            $builder->moveNextPage();
            foreach ($textTools as $textTool) {
                $builder->addTextTool($textTool['type'], $textTool['data']);
            }
            $builder->endPage();
        }

        $filename = 'reminder_notice_'.$bill->getId().'_'.$today->format('YmdHis').'.pdf';
        $outputFile = $this->container->getParameter('kernel.root_dir').'/../files/bills/'.$filename;
        $builder->getPdf()->Output($outputFile, 'F');
        return $filename;
    }

    protected function buildInvoiceCSV($data) {
        $result = [];

        // Only have one product
        if (count($data['entries']) == 1) {
            // Summary line
            $product = $data['entries'][0];
            $result[] = [
                $data['invoiceRestructure'],
                $product['invoicingCode'],
                '',
                $data['invoiceDate'],
                '', 'EUR',
                $data['totalAmountWithTax'],
                '',
                $data['totalTaxAmount'],
                '', '', '', '', '',
                $data['invoiceNumber'],
                '1',
                $product['VAT'],
                '2', '', '1', '', '', '', '', '',
                $data['uid'],
                '', '', '', '', '', '', '', '', '',
            ];

            // Second line
            $result[] = [
                $product['invoicingCode'],
                $data['invoiceRestructure'],
                '',
                $data['invoiceDate'],
                '', 'EUR', '',
                $product['totalAmountWithoutTax'],
                $product['taxAmount'],
                '', '', '', '', '',
                $data['invoiceNumber'],
                '1',
                $product['VAT'],
                '2', '', '1', '', '', '', '', '',
                $data['uid'],
                '', '', '', '', '', '', '', '', '',
            ];
        } else {
            // Summary line
            $result[] = [
                $data['invoiceRestructure'],
                '', '',
                $data['invoiceDate'],
                '', 'EUR',
                $data['totalAmountWithTax'],
                '', '', '', '', '', '', '',
                $data['invoiceNumber'],
                '1', '0', '0', '', '3', '', '', '', '', 'Splitbuchung mit', 'unterschiedl. USt',
                '', '', '', '', '', '', '', '', '', '',
            ];

            // Product information list
            foreach ($data['entries'] as $entry) {
                $result[] = [
                    $entry['invoicingCode'],
                    $data['invoiceRestructure'],
                    '',
                    $data['invoiceDate'],
                    '', 'EUR', '',
                    $entry['totalAmountWithoutTax'],
                    $entry['taxAmount'],
                    '', '', '', '', '',
                    $data['invoiceNumber'],
                    '1',
                    $entry['VAT'],
                    '2', '', '3', '', '', '', '', 'Splitbuchung mit', 'unterschiedl. USt',
                    '', '', '', '', '', '', '', '', '', '',
                ];
            }
        }

        return $result;
    }

    protected function buildPaymentCSV($data) {
        $result = [];

        // Summary line
        $result[] = [
            '5160',
            $data['invoiceRestructure'],
            '',
            $data['paymentDate'],
            '', 'EUR', '',
            $data['totalAmountWithoutTax'],
            '',
            $data['totalTaxAmount'],
            '', '', '', '', '',
            $data['paymentNumber'],
            '1',
            $data['VAT'],
            '2', '', '1', '', '', '', '', 'Gutschrift mit', 'Gegenbuchung',
            '', '', '', '', '', '', '', '', '', '',
        ];

        // For each product information
        foreach ($data['entries'] as $entry) {
            $result[] = [
                $data['invoiceRestructure'],
                '5160', '',
                $data['paymentDate'],
                '', 'EUR', '',
                $entry['totalAmountWithTax'],
                '', '', '', '', '', '',
                $data['paymentNumber'],
                '1',
                $data['VAT'],
                '2', '', '1', '', '', '', '', 'Gutschrift mit', 'Gegenbuchung',
                '', '', '', '', '', '', '', '', '', '',
            ];
        }

        return $result;
    }

    public function handleBuildCSV($data) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $result = [];
        if ($data['type'] == 'invoice') {
            $result = $this->buildInvoiceCSV($data);
        } else {
            $result = $this->buildPaymentCSV($data);
        }

        // Combine each line data
        $content = [];
        foreach ($result as $line) {
            $content[] = implode(';', $line);
        }

        return implode("\n", $content);
    }

    public function getBillingTargetName() {
        return 'Target';
    }

    public function getBillingOpenTargets($idType) {
        return array();
    }

    public function getTargetName($idType, $idTarget) {
        return '---';
    }

    public function getDeliveryInfo($bill, $idTarget) {
        return array(
            'deliveryNote'  => '',
            'deliveryEmail'  => ''
        );
    }
}
