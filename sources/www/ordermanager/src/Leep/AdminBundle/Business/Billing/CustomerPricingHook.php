<?php
namespace Leep\AdminBundle\Business\Billing;

use Easy\ModuleBundle\Module\AbstractHook;

class CustomerPricingHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $em = $controller->get('doctrine')->getEntityManager();

        $statsList = array();

        // Stats all
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p) totalNumber, SUM(p.revenueEstimation) totalAmount')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('(p.isAddedInvoiced IS NULL) OR (p.isAddedInvoiced = 0)')
            ->andWhere('p.dateordered >= :baselineDate')
            ->setParameter('baselineDate', $config->getBillingBaselineDate());
        $stat = $query->getQuery()->getSingleResult();

        $statsList[] = array(
            'name' => 'Pending for pricing (all)',
            'totalNumber' => intval($stat['totalNumber']),
            'totalAmount' => number_format($stat['totalAmount'], 2)
        );

        // Stats submitted
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        $reportSubmittedStatus = 8; // Fix me
        $nutriMeSubmittedStatus = 55;
        $query->andWhere('(FIND_IN_SET('.$reportSubmittedStatus.', p.statusList) > 0) OR (FIND_IN_SET('.$nutriMeSubmittedStatus.', p.statusList) > 0)');
        $stat = $query->getQuery()->getSingleResult();
        $statsList[] = array(
            'name' => 'Pending for pricing (submitted)',
            'totalNumber' => intval($stat['totalNumber']),
            'totalAmount' => number_format($stat['totalAmount'], 2)
        );

        $data['statsList'] = $statsList;
    }
}
