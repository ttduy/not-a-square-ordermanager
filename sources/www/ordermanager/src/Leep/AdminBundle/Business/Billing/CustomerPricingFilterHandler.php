<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class CustomerPricingFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new CustomerPricingFilterModel();
        $model->idShow = 1;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('orderNumber', 'text', array(
            'label'       => 'Order number',
            'required'    => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'       => 'Distribution channel',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'empty_value' => false
        ));
        $builder->add('idPartner', 'searchable_box', array(
            'label'       => 'Partner',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Partner_List'),
            'empty_value' => false
        ));
        $builder->add('isReadyForPricing', 'choice', array(
            'label'       => 'Ready for pricing?',
            'required'    => false,
            'choices'     => array(0 => 'All', 1 => 'Ready for pricing', 2 => 'Not ready for pricing'),
            'empty_value' => false
        ));

        $builder->add('idErrorType', 'choice', array(
            'label'       => 'Error Type',
            'required'    => false,
            'choices'     => array(0 => '') + Constant::getErrorTypes(),
            'empty_value' => false
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'       => 'Margin Goes To',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo'),
            'empty_value' => false
        ));
        $builder->add('idInvoiceGoesTo', 'choice', array(
            'label'       => 'Invoice Goes To',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo'),
            'empty_value' => false
        ));
        $builder->add('idPriceCategory', 'searchable_box', array(
            'label'       => 'Price Category',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_PriceCategory_List'),
            'empty_value' => false
        ));
        $builder->add('idReportDelivery', 'choice', array(
            'label'       => 'Report Delivery',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery'),
            'empty_value' => false
        ));
        $builder->add('idAcquisiteur', 'searchable_box', array(
            'label'       => 'Acquisiteur',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Acquisiteur_List'),
            'empty_value' => false
        ));
        $builder->add('notes', 'text', array(
            'label'       => 'Notes',
            'required'    => false
        ));

        return $builder;
    }
}