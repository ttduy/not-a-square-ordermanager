<?php
namespace Leep\AdminBundle\Business\Billing;

use Symfony\Component\Validator\ExecutionContext;

class CustomerPricingFilterModel {
    public $orderNumber;
    public $idDistributionChannel;
    public $idPartner;
    public $isReadyForPricing;

    public $idErrorType;
    public $idMarginGoesTo;
    public $idInvoiceGoesTo;
    public $idPriceCategory;
    public $idReportDelivery;
    public $idAcquisiteur;
    public $notes;
}
