<?php
namespace Leep\AdminBundle\Business\Billing;

class Constant {
    const BULK_PROCESS_ACTION_BULK_INVOICE                                 = 2;
    const BULK_PROCESS_ACTION_UPDATE_PRICE                                 = 3;

    const BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY                   = 4;
    const BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY_BY_DC_SETTING     = 5;
    const BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO                   = 6;
    const BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO_BY_DC_SETTING     = 7;
    const BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO                  = 8;
    const BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO_BY_DC_SETTING    = 9;

    public static function getBulkProcessActions() {
        return array(
            self::BULK_PROCESS_ACTION_BULK_INVOICE                                => "Bulk-pricing",
            self::BULK_PROCESS_ACTION_UPDATE_PRICE                                => "Bulk-update to latest price",
            self::BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY                  => "Bulk-fix 'Price category'",
            self::BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY_BY_DC_SETTING    => "Bulk-fix 'Price category' by DC Setting",
            self::BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO                  => "Bulk-fix 'Margin goes to'",
            self::BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO_BY_DC_SETTING    => "Bulk-fix 'Margin goes to' by DC Setting",
            self::BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO                 => "Bulk-fix 'Invoice goes to'",
            self::BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO_BY_DC_SETTING   => "Bulk-fix 'Invoice goes to' by DC Setting",
        );
    }


    const ERROR_TYPE_MISSING_PRICE_CATEGORY =       1;
    const ERROR_TYPE_MISSING_MARGIN_GOES_TO =       2;
    const ERROR_TYPE_MISSING_INVOICE_GOES_TO =      3;
    const ERROR_TYPE_MISSING_DISTRIBUTION_CHANNEL = 4;
    const ERROR_TYPE_MISSING_PRODUCT_OR_CATGORY_PRICE = 5;
    const ERROR_TYPE_ALL_ERROR =                    999;
    public static function getErrorTypes() {
        return array(
            self::ERROR_TYPE_ALL_ERROR =>                           'All error',
            self::ERROR_TYPE_MISSING_PRICE_CATEGORY =>              'Missing Price Category',
            self::ERROR_TYPE_MISSING_MARGIN_GOES_TO =>              'Missing Margin Goes To',
            self::ERROR_TYPE_MISSING_INVOICE_GOES_TO =>             'Missing Invoice Goes To',
            self::ERROR_TYPE_MISSING_DISTRIBUTION_CHANNEL =>        'Missing Distribution Channel',
            self::ERROR_TYPE_MISSING_PRODUCT_OR_CATGORY_PRICE =>    'Missing Product/Category Price',
        );
    }
}
