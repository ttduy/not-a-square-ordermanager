<?php
namespace Leep\AdminBundle\Business\Billing;
use App\Database\MainBundle\Entity;

use Leep\AdminBundle\Business;

class Utils {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    //////////////////////////////////////
    // x. INVOICE - CUSTOMER
    public function getOpenCustomers($selectedIdBill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BillingInvoiceCustomer', 'p')
            ->andWhere('(p.idBill = 0) OR (p.idBill IS NULL) OR (p.idBill = :selectedIdBill)')
            ->setParameter('selectedIdBill', $selectedIdBill);
        $results = $query->getQuery()->getResult();

        $map = array();
        foreach ($results as $r) {
            $name = $r->getOrderNumber();
            if (trim($r->getName()) != '') {
                $name .= ' ('.$r->getName().')';
            }
            $map[$r->getId()] = $name;
        }
        return $map;
    }

    //////////////////////////////////////
    // x. SHARED FUNCTION
    public function getOpenEntries($selectedIdBill, $tableCode, $idTargetField, $mappingCode) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $query = $em->createQueryBuilder();
        $query->select('p.'.$idTargetField.', COUNT(p) as numOpens, p.id')
            ->from('AppDatabaseMainBundle:'.$tableCode, 'p')
            ->andWhere('(p.idBill = 0) OR (p.idBill IS NULL) OR (p.idBill = :selectedIdBill)')
            ->groupBy('p.'.$idTargetField)
            ->setParameter('selectedIdBill', $selectedIdBill);
        $results = $query->getQuery()->getResult();

        $map = array();
        foreach ($results as $r) {
            $numOpens = intval($r['numOpens']);
            $idTarget = $r[$idTargetField];
            $name = $formatter->format($idTarget, 'mapping', $mappingCode);
            $map[$r['id']] = $name.' ('.$numOpens.' opens)';
        }
        return $map;
    }

    //////////////////////////////////////
    // x. OPEN ENTRIES
    public function getOpenInvoiceDistributionChannels($selectedIdBill) {
        return $this->getOpenEntries($selectedIdBill, 'BillingInvoiceDistributionChannel', 'idDistributionChannel', 'LeepAdmin_DistributionChannel_List');
    }
    public function getOpenInvoicePartners($selectedIdBill) {
        return $this->getOpenEntries($selectedIdBill, 'BillingInvoicePartner', 'idPartner', 'LeepAdmin_Partner_List');
    }
    public function getOpenPaymentDistributionChannels($selectedIdBill) {
        return $this->getOpenEntries($selectedIdBill, 'BillingPaymentDistributionChannel', 'idDistributionChannel', 'LeepAdmin_DistributionChannel_List');
    }
    public function getOpenPaymentPartners($selectedIdBill) {
        return $this->getOpenEntries($selectedIdBill, 'BillingPaymentPartner', 'idPartner', 'LeepAdmin_Partner_List');
    }
    public function getOpenPaymentAcquisiteurs($selectedIdBill) {
        return $this->getOpenEntries($selectedIdBill, 'BillingPaymentAcquisiteur', 'idAcquisiteur', 'LeepAdmin_Acquisiteur_List');
    }

    /////////////////////////////////////
    // x. BILLING FILE
    public function setBillFileAsActive($idBill, $idBillFile) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:BillFile', 'p')
            ->set('p.isActive', 0)
            ->andWhere('p.idBill = '.$idBill)
            ->getQuery()->execute();

        $billFile = $em->getRepository('AppDatabaseMainBundle:BillFile')->findOneById($idBillFile);
        $bill = $em->getRepository('AppDatabaseMainBundle:Bill')->findOneById($idBill);

        $billFile->setIsActive(1);
        $bill->setBillAmount($billFile->getBillAmount());
        $bill->setIdActiveBillFile($billFile->getId());
        $em->flush();
    }

    /////////////////////////////////////
    // x. UPDATE CUSTOMER PRICE
    public function updateCustomerPrice($container, $pricingHelper, $idCustomer) {
        $em = $container->get('doctrine')->getEntityManager();

        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if (!$customer) {
            return;
        }

        $pricing = $pricingHelper->computePricing($customer);
        $overridePrices = array();
        foreach ($pricing->entries as $entry) {
            if ($entry['id'][0] == 'P') {
                $idProduct = intval(substr($entry['id'], 2));
                $currentPrice = $pricingHelper->getProductPrice($customer->getPriceCategoryId(), $idProduct);
                $overridePrices[] = array(
                    'id'     => $entry['id'],
                    'price'  => $currentPrice
                );
            }
            else if ($entry['id'][0] == 'C') {
                $idCategory = intval(substr($entry['id'], 2));
                $currentPrice = $pricingHelper->getCategoryPrice($customer->getPriceCategoryId(), $idCategory);
                $overridePrices[] = array(
                    'id'     => $entry['id'],
                    'price'  => $currentPrice
                );
            }
        }

        // Remove old entries
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:OrderedOverridePrice', 'p')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $customer->getId())
            ->getQuery()
            ->execute();

        // New entries
        foreach ($overridePrices as $op) {
            $overridePrice = new Entity\OrderedOverridePrice();
            $overridePrice->setIdCustomer($customer->getId());
            $overridePrice->setIdEntry($op['id']);
            $overridePrice->setPrice($op['price']['price']);
            $overridePrice->setDcMargin($op['price']['dcMargin']);
            $overridePrice->setPartMargin($op['price']['partnerMargin']);
            $em->persist($overridePrice);
            $em->flush();
        }
    }

    /////////////////////////////////////
    // x. UPDATE CUSTOMER PRICE
    public function addNewStatus($bill, $status) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $today = new \DateTime();

        // Find max status
        $query = $em->createQueryBuilder();
        $query->select('MAX(p.sortOrder)')
            ->from('AppDatabaseMainBundle:BillStatus', 'p')
            ->andWhere('p.id = :idBill')
            ->setParameter('idBill', $bill->getId());
        $sortOrder = intval($query->getQuery()->getSingleScalarResult()) + 1;

        // New status
        $newStatus = new Entity\BillStatus();
        $newStatus->setIdBill($bill->getId());
        $newStatus->setStatus($status);
        $newStatus->setStatusDate($today);
        $newStatus->setSortOrder($sortOrder);
        $newStatus->setNotes("Reminder automatically added");
        $em->persist($newStatus);

        // Update bill status
        $bill->setStatus($status);
        $bill->setStatusDate($today);
        $em->persist($bill);
        $em->flush();

        Business\Bill\Utils::postUpdateStatusList($this->container, $bill->getId());
    }


    /////////////////////////////////////////////////////
    // x. Generate billing entries
    public function generateCustomerBillingEntries($customer, $pricingHelper, &$errors) {
        $billingHandler = $this->container->get('leep_admin.billing.business.billing_handler');
        $pricing = $pricingHelper->computePricing($customer);
        if (!empty($pricing->errors)) {
            $errors[] = "Can't invoice customer ".$customer->getOrderNumber()." errors = ".implode(", ", $pricing->errors);
            return false;
        }

        // Generate Invoice Customer Entries
        if ($pricing->hasBill('invoiceCustomer')) {
            $bill = $pricing->getBill('invoiceCustomer');
            $billingHandler->getBillingHandler('invoiceCustomer')->generateBillingEntries($pricing, $customer, $bill);
        }
        // Generate Invoice Distribution Channel Entries
        if ($pricing->hasBill('invoiceDistributionChannel')) {
            $bill = $pricing->getBill('invoiceDistributionChannel');
            $billingHandler->getBillingHandler('invoiceDistributionChannel')->generateBillingEntries($pricing, $customer, $bill);
        }
        // Generate Invoice Partner Entries
        if ($pricing->hasBill('invoicePartner')) {
            $bill = $pricing->getBill('invoicePartner');
            $billingHandler->getBillingHandler('invoicePartner')->generateBillingEntries($pricing, $customer, $bill);
        }
        // Generate Payment Distribution Channel
        if ($pricing->hasBill('paymentDistributionChannel')) {
            $bill = $pricing->getBill('paymentDistributionChannel');
            $billingHandler->getBillingHandler('paymentDistributionChannel')->generateBillingEntries($pricing, $customer, $bill);
        }
        // Generate Payment Partner
        if ($pricing->hasBill('paymentPartner')) {
            $bill = $pricing->getBill('paymentPartner');
            $billingHandler->getBillingHandler('paymentPartner')->generateBillingEntries($pricing, $customer, $bill);
        }
        // Generate Payment Acquisiteur
        if ($pricing->hasBill('paymentAcquisiteur1')) {
            $bill = $pricing->getBill('paymentAcquisiteur1');
            $billingHandler->getBillingHandler('paymentAcquisiteur')->generateBillingEntries($pricing, $customer, $bill, 1);
        }
        if ($pricing->hasBill('paymentAcquisiteur2')) {
            $bill = $pricing->getBill('paymentAcquisiteur2');
            $billingHandler->getBillingHandler('paymentAcquisiteur')->generateBillingEntries($pricing, $customer, $bill, 2);
        }
        if ($pricing->hasBill('paymentAcquisiteur3')) {
            $bill = $pricing->getBill('paymentAcquisiteur3');
            $billingHandler->getBillingHandler('paymentAcquisiteur')->generateBillingEntries($pricing, $customer, $bill, 3);
        }
    }
}
