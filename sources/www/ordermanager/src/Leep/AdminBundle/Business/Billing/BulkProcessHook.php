<?php
namespace Leep\AdminBundle\Business\Billing;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class BulkProcessHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $pricingHelper = $controller->get('leep_admin.billing.business.pricing_helper');

        $selectedCustomers = $controller->get('request')->get('selected', '');
        $data['selectedCustomers'] = $selectedCustomers;
        $selectedCustomers = explode(',', $selectedCustomers);

        $customers = array();
        foreach ($selectedCustomers as $selectedId) {
            if (trim($selectedId) != '') {
                $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($selectedId);
                if ($customer) {
                    $pricing = $pricingHelper->computePricing($customer);
                    $messages = array();
                    if (Business\PricingSetting\Utils::isReadyForPricing($controller, $customer)) {
                        $messages[] = '<b style="color: green">Ready for pricing</b><br/>';
                    }

                    foreach ($pricing->errors as $error) {
                        $messages[] = '<b style="color: red">'.$error."</b><br/>";
                    }

                    if (!empty($pricing->priceChanges)) {
                        $messages[] = '<b style="color: orange">Price has been updated recently</b><br/>';
                    }

                    $messages[]= "Price category '".$formatter->format($customer->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List')."'<br/>";
                    $messages[]= "Invoice goes to '".$formatter->format($customer->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo')."'<br/>";
                    $messages[]= $formatter->format($customer->getIdMarginGoesTo(), 'mapping', 'LeepAdmin_DistributionChannel_MarginGoesTo')."<br/>";
                    $messages[]= $formatter->format($customer->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery')." to '".
                                $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List')."'<br/>";

                    $messages[]= "<br/>";

                    $customers[] = array(
                        'orderNumber'     => $customer->getOrderNumber(),
                        'name'            => $customer->getFirstName().' '.$customer->getSurName(),
                        'messages'        => $messages
                    );
                }
            }
        }
        $data['customers'] = $customers;

    }
}
