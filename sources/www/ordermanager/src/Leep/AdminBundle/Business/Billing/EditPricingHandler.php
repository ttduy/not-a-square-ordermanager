<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditPricingHandler extends AppEditHandler {
    public $pricingHelper = null;
    public $pricing = null;
    public $customer = null;

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($customer) {
        $this->customer = $customer;
        $this->pricingHelper = $this->container->get('leep_admin.billing.business.pricing_helper');
        $this->pricing = $this->pricingHelper->computePricing($customer);

        $model = array(
            'priceOverride' => $customer->getPriceOverride(),
            'marginOverrideDc' => $customer->getMarginOverrideDc(),
            'marginOverridePartner' => $customer->getMarginOverridePartner(),
            'paymentOverrideAcquisiteur1' => $customer->getPaymentOverrideAcquisiteur1(),
            'paymentOverrideAcquisiteur2' => $customer->getPaymentOverrideAcquisiteur2(),
            'paymentOverrideAcquisiteur3' => $customer->getPaymentOverrideAcquisiteur3(),
            'priceCategoryId'             => $customer->getPriceCategoryId(),
            'invoiceGoesToId'             => $customer->getInvoiceGoesToId(),
            'reportDeliveryId'            => $customer->getReportDeliveryId(),
            'idMarginGoesTo'              => $customer->getIdMarginGoesTo(),
            'idAcquisiteur1'              => $customer->getIdAcquisiteur1(),
            'acquisiteurCommissionRate1'  => $customer->getAcquisiteurCommissionRate1(),
            'idAcquisiteur2'              => $customer->getIdAcquisiteur2(),
            'acquisiteurCommissionRate2'  => $customer->getAcquisiteurCommissionRate2(),
            'idAcquisiteur3'              => $customer->getIdAcquisiteur3(),
            'acquisiteurCommissionRate3'  => $customer->getAcquisiteurCommissionRate3()
        );

        foreach ($this->pricing->entries as $entry) {
            $model['price_'.$entry['id']] = $entry['price'];
            $model['dcMargin_'.$entry['id']] = $entry['dcMargin'];
            $model['partnerMargin_'.$entry['id']] = $entry['partnerMargin'];
        }

        $model['extraPriceReportDeliveryOverride'] = $this->pricing->extraEntryPostage;
        $model['extraPriceNutriMeDeliveryOverride'] = $this->pricing->extraNutriMeDeliveryPrice;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('priceCategoryId', 'searchable_box', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('invoiceGoesToId', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('reportDeliveryId', 'choice', array(
            'label'    => 'Report Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery')
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));


        $builder->add('idAcquisiteur1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List'),
            'attr'     => array(
                'style'   => 'width: 210px'
            )
        ));
        $builder->add('acquisiteurCommissionRate1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 70px'
            )
        ));
        $builder->add('idAcquisiteur2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List'),
            'attr'     => array(
                'style'   => 'width: 210px'
            )
        ));
        $builder->add('acquisiteurCommissionRate2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 70px'
            )
        ));
        $builder->add('idAcquisiteur3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List'),
            'attr'     => array(
                'style'   => 'width: 210px'
            )
        ));
        $builder->add('acquisiteurCommissionRate3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 70px'
            )
        ));

        foreach ($this->pricing->entries as $entry) {
            $builder->add('price_'.$entry['id'],   'text', array(
                'required' => false,
                'attr'     => array(
                    'style'   => 'width: 60%; height: 6px; margin-top: 5px'
                )
            ));
            $builder->add('dcMargin_'.$entry['id'],   'text', array(
                'required' => false,
                'attr'     => array(
                    'style'   => 'width: 60%; height: 6px; margin-top: 5px'
                )
            ));
            $builder->add('partnerMargin_'.$entry['id'],   'text', array(
                'required' => false,
                'attr'     => array(
                    'style'   => 'width: 60%; height: 6px; margin-top: 5px'
                )
            ));
        }
        $builder->add('sectionOverridePrice',          'section', array(
            'label' => 'Override Prices',
            'property_path' => false
        ));
        $builder->add('priceOverride', 'text', array(
            'label'    => 'Price Override (€)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('marginOverrideDc', 'text', array(
            'label'    => 'Margin Override DC (%)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('marginOverridePartner', 'text', array(
            'label'    => 'Margin Override Partner (%)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('paymentOverrideAcquisiteur1', 'text', array(
            'label'    => 'Commission Override Acquisiteur 1 (€)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('paymentOverrideAcquisiteur2', 'text', array(
            'label'    => 'Commission Override Acquisiteur 2 (€)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('paymentOverrideAcquisiteur3', 'text', array(
            'label'    => 'Commission Override Acquisiteur 3 (€)',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 60%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('extraPriceReportDeliveryOverride', 'text', array(
            'label'    => 'Extra price - Report delivery override',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 15%; height: 6px; margin-top: 5px'
            )
        ));
        $builder->add('extraPriceNutriMeDeliveryOverride', 'text', array(
            'label'    => 'Extra price - NutriME delivery override',
            'required' => false,
            'attr'     => array(
                'style'   => 'width: 15%; height: 6px; margin-top: 5px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');

        if ($this->entity->getIsAddedInvoiced() == 1) {
            $this->errors[] = "Error! Already invoiced, these information won't be updated!";
            $this->reloadForm();
            return;
        }

        $this->entity->setPriceCategoryId($model['priceCategoryId']);
        $this->entity->setInvoiceGoesToId($model['invoiceGoesToId']);
        $this->entity->setReportDeliveryId($model['reportDeliveryId']);
        $this->entity->setIdMarginGoesTo($model['idMarginGoesTo']);

        $this->entity->setPriceOverride($model['priceOverride']);
        $this->entity->setMarginOverrideDc($model['marginOverrideDc']);
        $this->entity->setMarginOverridePartner($model['marginOverridePartner']);
        $this->entity->setPaymentOverrideAcquisiteur1($model['paymentOverrideAcquisiteur1']);
        $this->entity->setPaymentOverrideAcquisiteur2($model['paymentOverrideAcquisiteur2']);
        $this->entity->setPaymentOverrideAcquisiteur3($model['paymentOverrideAcquisiteur3']);
        $this->entity->setExtraPriceReportDeliveryOverride($model['extraPriceReportDeliveryOverride']);
        $this->entity->setExtraPriceNutriMeDeliveryOverride($model['extraPriceNutriMeDeliveryOverride']);

        $this->entity->setIdAcquisiteur1($model['idAcquisiteur1']);
        $this->entity->setAcquisiteurCommissionRate1($model['acquisiteurCommissionRate1']);
        $this->entity->setIdAcquisiteur2($model['idAcquisiteur2']);
        $this->entity->setAcquisiteurCommissionRate2($model['acquisiteurCommissionRate2']);
        $this->entity->setIdAcquisiteur3($model['idAcquisiteur3']);
        $this->entity->setAcquisiteurCommissionRate3($model['acquisiteurCommissionRate3']);

        foreach ($this->pricing->entries as $entry) {
            $key = 'price_'.$entry['id'];
            $price = isset($model[$key]) ? $model[$key] : 0;
            $key = 'dcMargin_'.$entry['id'];
            $dcMargin = isset($model[$key]) ? $model[$key] : 0;
            $key = 'partnerMargin_'.$entry['id'];
            $partnerMargin = isset($model[$key]) ? $model[$key] : 0;
            $orderedOverridePrice = $em->getRepository('AppDatabaseMainBundle:OrderedOverridePrice')->findOneBy(array(
                'idCustomer'  => $this->customer->getId(),
                'idEntry'     => $entry['id']
            ));

            if (empty($orderedOverridePrice)) {
                $orderedOverridePrice = new Entity\OrderedOverridePrice();
                $orderedOverridePrice->setIdCustomer($this->customer->getId());
                $orderedOverridePrice->setIdEntry($entry['id']);
            }

            $orderedOverridePrice->setPrice($price);
            $orderedOverridePrice->setDcMargin($dcMargin);
            $orderedOverridePrice->setPartMargin($partnerMargin);
            $em->persist($orderedOverridePrice);
        }

        $em->flush();

        parent::onSuccess();
        $this->reloadForm();
    }
}
