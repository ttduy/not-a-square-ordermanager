<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
class BillingInvoiceDistributionChannelHandler extends BillingInvoiceHandler {
    public function generateBillingEntries($pricing, $customer, $bill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        // Make billing record
        $billingRecord = new Entity\BillingInvoiceDistributionChannel();
        $billingRecord->setIdCustomer($customer->getId());
        $billingRecord->setIdDistributionChannel($customer->getDistributionChannelId());
        $billingRecord->setOrderNumber($customer->getOrderNumber());
        $billingRecord->setOrderDate($customer->getDateOrdered());
        $billingRecord->setName($customer->getFirstName().' '.$customer->getSurName());
        $billingRecord->setTotalAmount($bill['total']);
        if ($bill['overrideTotal'] != '' && (floatval($bill['overrideTotal']) != floatval($bill['total']))) {
            $billingRecord->setOverrideAmount($bill['overrideTotal']);
        }
        $em->persist($billingRecord);
        $em->flush();

        // Make billing entry
        foreach ($bill['entries'] as $entry) {
            $billingEntry = new Entity\BillingInvoiceDistributionChannelEntry();
            $billingEntry->setIdBillingInvoiceDistributionChannel($billingRecord->getId());
            $billingEntry->setEntryId($entry['id']);
            $billingEntry->setEntry($entry['product']);
            $billingEntry->setAmount($entry['amount']);
            $em->persist($billingEntry);
        }

        // Make extra billing entry - Report delivery
        if ($pricing->extraEntryPostage != '') {
            $reportDeliveryEntry = $pricing->reportDeliveryBillingText;

            $billingEntry = new Entity\BillingInvoiceDistributionChannelEntry();
            $billingEntry->setIdBillingInvoiceDistributionChannel($billingRecord->getId());
            $billingEntry->setEntryId('REPORT_DELIVERY');
            $billingEntry->setEntry($reportDeliveryEntry);
            $billingEntry->setAmount($pricing->extraEntryPostage);
            $em->persist($billingEntry);
        }

        // Make extra billing entry - NutriME
        if ($pricing->extraNutriMeDeliveryPrice != '') {
            $reportDeliveryNutriMeEntry =
                sprintf("%s (x%s)",
                    $pricing->nutriMeBillingText,
                    $pricing->extraNutriMeNumProduct
                );
            $billingEntry = new Entity\BillingInvoiceDistributionChannelEntry();
            $billingEntry->setIdBillingInvoiceDistributionChannel($billingRecord->getId());
            $billingEntry->setEntryId('NUTRIME_DELIVERY');
            $billingEntry->setEntry($reportDeliveryNutriMeEntry);
            $billingEntry->setAmount($pricing->extraNutriMeDeliveryPrice);
            $em->persist($billingEntry);
        }

        $em->flush();
    }

    /////////////////////////////////////////////////////////
    // x. GET TOTAL OPEN
    public function getTotalOpen() {
        return $this->queryTotalOpen('BillingInvoiceDistributionChannel');
    }
    /////////////////////////////////////////////////////////
    // x. DELETE BILL
    public function deleteBill($bill) {
        $this->unhookBill('BillingInvoiceDistributionChannel', $bill->getId());
    }

    ///////////////////////////////////////////////////////
    // x. ORDER SELECTOR
    public function getOrderSelectorHandler() {
        $orderSelectorHandler = $this->container->get('leep_admin.bill.business.order_selector_handler');
        $orderSelectorHandler->tableCode = 'BillingInvoiceDistributionChannel';
        $orderSelectorHandler->idTargetField = 'idDistributionChannel';
        $orderSelectorHandler->mappingCode = 'LeepAdmin_DistributionChannel_List';
        $orderSelectorHandler->targetLabel = 'Distribution Channel';

        return $orderSelectorHandler;
    }

    public function getTargetBillingData($idType, $id) {
        $dc=array();
        $em = $this->container->get('doctrine')->getEntityManager();
        if ($idType != Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC) {
             $r =$em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneById($id);
             if($r){
                 $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($r->getidDistributionChannel());
             }
        }
        else{
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
        }
        
        $result = [
            'isWithTax' =>                  $dc->getIsInvoiceWithTax(),
            'idNoTaxText' =>                $dc->getInvoiceNoTaxText(),
            'idCurrency' =>                 $dc->getIdCurrency(),
            'commissionPaymentWithTax' =>   $dc->getCommissionPaymentWithTax(),
            'addressUid' =>                 $dc->getUidNumber(),
            'isInvoiceAddress' =>           $dc->getInvoiceAddressIsUsed(),
        ];

        if ($dc->getInvoiceAddressIsUsed()) {
            return $result + [
                'addressStreet' =>          $dc->getInvoiceAddressStreet(),
                'addressPostCode' =>        $dc->getInvoiceAddressPostCode(),
                'addressCity' =>            $dc->getInvoiceAddressCity(),
                'addressCountry' =>         $dc->getInvoiceAddressIdCountry(),
                'addressTelephone' =>       $dc->getInvoiceAddressTelephone(),
                'addressFax' =>             $dc->getInvoiceAddressFax(),
                'haveInvoiceAddress' =>     'Yes'
            ];
        } else {
            return $result + [
                'addressStreet' =>          $dc->getStreet(),
                'addressPostCode' =>        $dc->getPostCode(),
                'addressCity' =>            $dc->getCity(),
                'addressCountry' =>         $dc->getCountryID(),
                'addressTelephone' =>       $dc->getTelephone(),
                'addressFax' =>             $dc->getFax(),
                'haveInvoiceAddress' =>     'No'
            ];
        }

    }

    public function getDeliveryInfo($idType, $idTarget) {
        // $idTarget = $bill ? $bill->getIdTarget() : $idTarget;
        // $deliveryNote = $bill ? $bill->getDeliveryNote() : '';
        // $deliveryEmail = $bill ? $bill->getDeliveryEmail() : '';

        // if (!$deliveryNote || !$deliveryEmail) {
        //     $em = $this->container->get('doctrine')->getEntityManager();
        //     $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($idTarget);
        //     if ($dc) {
        //         if (!$deliveryNote) {
        //             $deliveryNote = $dc->getInvoiceDeliveryNote();
        //         }

        //         if (!$deliveryEmail) {
        //             $deliveryEmail = $dc->getInvoicedeliveryemail();
        //         }
        //     }
        // }
        $dc=array();
        $deliveryNote='';
        $deliveryEmail='';
        if ($idType == Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC) {
             $dc = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($idTarget);
        }
        else{
             $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneById($idTarget);
             if($m){
                $dc = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($m->getidDistributionChannel());
             }
            

        }
        if($dc){
                $deliveryNote=$dc->getInvoiceDeliveryNote();
                $deliveryEmail = $dc->getInvoicedeliveryemail();
        }
        return array(
            'deliveryNote' => $deliveryNote,
            'deliveryEmail' => $deliveryEmail
        );
    }

    /////////////////////////////////////////////////////////
    // x. FORM - BUILD FORM
    protected function billFormBuildBillToSection($builder, $bill) {
        $this->getOrderSelectorHandler()->buildBillToSection($builder, $bill);
        $this->billFormBuildBillDeliverySection($builder, $bill);
    }

    protected function billFormBuildBillDeliverySection($builder, $bill) {
        $builder->add('sectionDeliveryInfo', 'section', array(
            'label'    => 'Delivery Info'
        ));
        $builder->add('deliveryInfo', 'app_billing_delivery_info', array(
            'idType'      => $bill->getIdType()
        ));
    }

    ///////////////////////////////////////////////////////
    // x. FORM - LOAD DATA
    protected function billFormLoadDataExtra($bill) {
        $arrData = $this->getOrderSelectorHandler()->formLoadData($bill);
        $arrData += array('deliveryInfo' => self::getDeliveryInfo($bill->getIdType(),$bill->getIdTarget()));
        return $arrData;
    }

    ///////////////////////////////////////////////////////
    // x. FORM - SAVE DATA
    protected function billFormOnSuccessExtra($bill, $data) {
        $this->getOrderSelectorHandler()->formOnSuccess($this, $bill, $data);
        self::saveDeliveryInfo($bill, $data);
    }

    private function saveDeliveryInfo($bill, $data) {
        if (isset($data['deliveryInfo'])) {
            if (isset($data['deliveryInfo']['deliveryNote'])) {
                $bill->setDeliveryNote($data['deliveryInfo']['deliveryNote']);
            } else {
                $bill->setDeliveryNote(null);
            }

            if (isset($data['deliveryInfo']['deliveryEmail'])) {
                $bill->setDeliveryEmail($data['deliveryInfo']['deliveryEmail']);
            } else {
                $bill->setDeliveryEmail(null);
            }
        } else {
            $bill->setDeliveryNote(null);
            $bill->setDeliveryEmail(null);
        }
    }
    //////////////////////////////////////////////
    // x. GENERATE PDF
    public function getBillingFilesHandler() {
        $handler = $this->container->get('leep_admin.bill.business.billing_files_invoice_distribution_channel_handler');
        return $handler;
    }

    //////////////////////////////////////////////
    // x. BILLING FORM
    public function getBillingTargetName() {
        return 'Distribution Channel';
    }

    public function getBillingOpenTargets($idType) {
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
        if ($idType == Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC) {
            $mapping = $this->container->get('easy_mapping');
            return $mapping->getMapping('LeepAdmin_DistributionChannel_List');
        }
        return $billingUtils->getOpenInvoiceDistributionChannels(0);
    }

    public function getTargetName($idType, $idTarget) {
        if ($idType == Business\Bill\Constant::TYPE_INVOICE_MANUAL_DC) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($idTarget);
              if($r)
            return $r->getDistributionChannel();
        }
         $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneById($idTarget);
        if ($m) {
            $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($m->getidDistributionChannel());
            if($r)
            return $r->getDistributionChannel();
        }
        return '---';
    }


    public function getBillingEntries($idCustomer, &$billEntries) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $entry = array(
            'invoiceTo'       => 'DC',
            'totalAmount' => '',
            'overrideAmount' => '',
            'idBill' => 0,
            'entries' => array()
        );
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }
        $entry['totalAmount'] = $billRecord->getTotalAmount();
        $entry['overrideAmount'] = $billRecord->getOverrideAmount();
        $entry['idBill'] = intval($billRecord->getIdBill());

        $billingEntries = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannelEntry')->findByIdBillingInvoiceDistributionChannel($billRecord->getId());
        foreach ($billingEntries as $billingEntry) {
            $entry['entries'][$billingEntry->getEntryId()] = array(
                'entry'  => $billingEntry->getEntry(),
                'amount' => $billingEntry->getAmount()
            );
        }

        $billEntries['INVOICE'] = $entry;
    }


    public function removeBillingEntries($idCustomer) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:BillingInvoiceDistributionChannelEntry', 'p')
            ->andWhere('p.idBillingInvoiceDistributionChannel = '.$billRecord->getId())
            ->getQuery()
            ->execute();

        $em->remove($billRecord);
        $em->flush();
    }
}
