<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business;

class PricingModel {
    public $orderNumber;
    public $orderDate;
    public $customerName;

    public $errors = array();
    public $priceChanges = array();
    public $entries = array();
    public $extraEntryPostage = '';
    public $extraNutriMeDeliveryPrice = '';
    public $extraNutriMeNumProduct = 0;
    public $extraNutriMePricePerProduct = 0;
    public $bills = array();
    public $isMissingErrorOrProductPrice = false;

    public $reportDeliveryBillingText = '';
    public $nutriMeBillingText = '';

    public function setOrderInfo($orderNumber, $orderDate, $customerName) {
        $this->orderNumber = $orderNumber;
        $this->orderDate = $orderDate;
        $this->customerName = $customerName;
    }

    public function addError($error, $isMissingErrorOrProductPrice = false) {
        $this->errors[] = $error;
        $this->isMissingErrorOrProductPrice = $isMissingErrorOrProductPrice;
    }

    public function hasError() {
        return !empty($this->errors);
    }

    public function isMissingErrorOrProductPrice() {
        return $this->isMissingErrorOrProductPrice;
    }

    public function addPriceChange($message) {
        $this->priceChanges[] = $message;
    }

    public function addEntry($idEntry, $product, $price, $dcMargin, $partnerMargin, $isPriceChanges) {
        $this->entries[] = array(
            'id'              => $idEntry,
            'product'         => $product,
            'price'           => floatval($price),
            'dcMargin'        => floatval($dcMargin),
            'partnerMargin'   => floatval($partnerMargin),
            'isPriceChanges'  => $isPriceChanges
        );
    }

    public function setPostage($amount) {
        $this->extraEntryPostage = $amount;
    }

    public function addBillEntry($id, $entry, $amount) {
        if (!isset($this->bills[$id])) {
            $this->bills[$id] = array(
                'entries'         => array(),
                'total'           => 0,
                'overrideTotal'   => '',
                'finalTotal'      => ''
            );
        }

        $this->bills[$id]['entries'][] = array(
            'id'          => $entry['id'],
            'product'     => $entry['product'],
            'amount'      => $amount
        );
    }



    public function finalizeBills() {
        foreach ($this->bills as $id => $bill) {
            $total = 0;
            foreach ($bill['entries'] as $entry) {
                $total += $entry['amount'];
            }

            if (strpos($id, 'invoice') === 0) {
                $total += floatval($this->extraEntryPostage);
                $total += floatval($this->extraNutriMeDeliveryPrice);
            }

            $this->bills[$id]['total'] = $total;
            if ($this->bills[$id]['finalTotal'] === '') {
                $this->bills[$id]['finalTotal'] = $total;
            }
        }
    }

    public function hasBill($id) {
        return isset($this->bills[$id]);
    }

    public function getBill($id) {
        if (isset($this->bills[$id])) {
            return $this->bills[$id];
        }
        return null;
    }

    public function isBillOverrided($id) {
        if (isset($this->bills[$id])) {
            if (($this->bills[$id]['overrideTotal'] !== '') &&
                ($this->bills[$id]['overrideTotal'] != $this->bills[$id]['total'])) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function getBillFinalTotal($id) {
        if (isset($this->bills[$id])) {
            if ($this->bills[$id]['overrideTotal'] !== '') {
                return $this->bills[$id]['overrideTotal'];
            }
            return $this->bills[$id]['total'];
        }
        return 0;
    }

    public function removeBill($id) {
        if (isset($this->bills[$id])) {
            unset($this->bills[$id]);
        }
    }

    public function overrideBillTotal($id, $amount) {
        if (isset($this->bills[$id])) {
            $this->bills[$id]['overrideTotal'] = $amount;
            $this->bills[$id]['finalTotal'] = $amount;
        }
    }
}
