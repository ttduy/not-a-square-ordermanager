<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
class BillingPaymentPartnerHandler extends BillingPaymentHandler {
    public function generateBillingEntries($pricing, $customer, $bill) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());

        // Make billing record
        $billingRecord = new Entity\BillingPaymentPartner();
        $billingRecord->setIdCustomer($customer->getId());
        $billingRecord->setIdPartner($dc->getPartnerId());
        $billingRecord->setOrderNumber($customer->getOrderNumber());
        $billingRecord->setOrderDate($customer->getDateOrdered());
        $billingRecord->setName($customer->getFirstName().' '.$customer->getSurName());
        $billingRecord->setTotalAmount($bill['total']);
        if ($bill['overrideTotal'] != '' && (floatval($bill['overrideTotal']) != floatval($bill['total']))) {
            $billingRecord->setOverrideAmount($bill['overrideTotal']);
        }
        $em->persist($billingRecord);
        $em->flush();

        // Make billing entry
        foreach ($bill['entries'] as $entry) {
            $billingEntry = new Entity\BillingPaymentPartnerEntry();
            $billingEntry->setIdBillingPaymentPartner($billingRecord->getId());
            $billingEntry->setEntryId($entry['id']);
            $billingEntry->setEntry($entry['product']);
            $billingEntry->setAmount($entry['amount']);
            $em->persist($billingEntry);
        }
        $em->flush();
    }

    /////////////////////////////////////////////////////////
    // x. GET TOTAL OPEN
    public function getTotalOpen() {
        return $this->queryTotalOpen('BillingPaymentPartner');
    }

    /////////////////////////////////////////////////////////
    // x. DELETE BILL
    public function deleteBill($bill) {
        $this->unhookBill('BillingPaymentPartner', $bill->getId());
    }

    ///////////////////////////////////////////////////////
    // x. ORDER SELECTOR
    public function getOrderSelectorHandler() {
        $orderSelectorHandler = $this->container->get('leep_admin.bill.business.order_selector_handler');
        $orderSelectorHandler->tableCode = 'BillingPaymentPartner';
        $orderSelectorHandler->idTargetField = 'idPartner';
        $orderSelectorHandler->mappingCode = 'LeepAdmin_Partner_List';
        $orderSelectorHandler->targetLabel = 'Partner';

        return $orderSelectorHandler;
    }

    public function getTargetBillingData($idType, $id) {
        $partner=array();
         $em = $this->container->get('doctrine')->getEntityManager();
        if ($idType != Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneById($id);
            if($r)
                $partner=$em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($r->getidPartner());
        }
        else{
             $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);
        }
        $result = [
            'isWithTax' =>                  $partner->getIsPaymentWithTax(),
            'idMarginPaymentMode' =>        $partner->getIdMarginPaymentMode(),
            'idNoTaxText' =>                $partner->getInvoiceNoTaxText(),
            'idCurrency' =>                 $partner->getIdCurrency(),
            'commissionPaymentWithTax' =>   $partner->getCommissionPaymentWithTax(),
            'addressUid' =>                 $partner->getUidNumber(),
            'isInvoiceAddress' =>           $partner->getInvoiceAddressIsUsed(),
        ];

        if ($partner->getInvoiceAddressIsUsed() == 1) {
            return $result + [
                'addressStreet' =>          $partner->getInvoiceAddressStreet(),
                'addressPostCode' =>        $partner->getInvoiceAddressPostCode(),
                'addressCity' =>            $partner->getInvoiceAddressCity(),
                'addressCountry' =>         $partner->getInvoiceAddressIdCountry(),
                'addressTelephone' =>       $partner->getInvoiceAddressTelephone(),
                'addressFax' =>             $partner->getInvoiceAddressFax(),
                'haveInvoiceAddress' =>     'Yes'
            ];
        }
        else {
            return $result + [
                'addressStreet' =>          $partner->getStreet(),
                'addressPostCode' =>        $partner->getPostCode(),
                'addressCity' =>            $partner->getCity(),
                'addressCountry' =>         $partner->getCountryID(),
                'addressTelephone' =>       $partner->getTelephone(),
                'addressFax' =>             $partner->getFax(),
                'haveInvoiceAddress' =>     'No'
            ];
        }
    }

    public function getDeliveryInfo($idType, $idTarget) {
        // $idTarget = $bill ? $bill->getIdTarget() : $idTarget;

        // $deliveryNote = $bill ? $bill->getDeliveryNote() : '';
        // $deliveryEmail = $bill ? $bill->getDeliveryEmail() : '';

        // if (!$deliveryNote || !$deliveryEmail) {
        //     $em = $this->container->get('doctrine')->getEntityManager();
        //     $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($idTarget);
        //     if ($partner) {
        //         if (!$deliveryNote) {
        //             $deliveryNote = $partner->getInvoiceDeliveryNote();
        //         }

        //         if (!$deliveryEmail) {
        //             $deliveryEmail = $partner->getInvoicedeliveryemail();
        //         }
        //     }
        // }
        $partner=array();
        $deliveryNote='';
        $deliveryEmail='';
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER) {
             $partner = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($idTarget);
        }
        else{
             $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneById($idTarget);
             if($m){
                $partner = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($m->getidPartner());
             }
            

        }
        if($partner){
                $deliveryNote=$partner->getInvoiceDeliveryNote();
                $deliveryEmail = $partner->getInvoicedeliveryemail();
        }
        return array(
            'deliveryNote' => $deliveryNote,
            'deliveryEmail' => $deliveryEmail
        );
    }

    /////////////////////////////////////////////////////////
    // x. FORM - BUILD FORM
    protected function billFormBuildBillToSection($builder, $bill) {
        $this->getOrderSelectorHandler()->buildBillToSection($builder, $bill);
        $this->billFormBuildBillDeliverySection($builder, $bill);
    }

    protected function billFormBuildBillDeliverySection($builder, $bill) {
        $builder->add('sectionDeliveryInfo', 'section', array(
            'label'    => 'Delivery Info'
        ));
        $builder->add('deliveryInfo', 'app_billing_delivery_info', array(
            'idType'      => $bill->getIdType()
        ));
    }

    protected function billFormBuildBillInvoiceSection($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');
    }
    ///////////////////////////////////////////////////////
    // x. FORM - LOAD DATA
    protected function billFormLoadDataExtra($bill) {
        $arrData = $this->getOrderSelectorHandler()->formLoadData($bill);
        $arrData += array('deliveryInfo' => self::getDeliveryInfo($bill->getIdType(), $bill->getIdTarget()));

        return $arrData;
    }

    ///////////////////////////////////////////////////////
    // x. FORM - SAVE DATA
    protected function billFormOnSuccessExtra($bill, $data) {
        $this->getOrderSelectorHandler()->formOnSuccess($this, $bill, $data);
        self::saveDeliveryInfo($bill, $data);
    }

    private function saveDeliveryInfo($bill, $data) {
        if (isset($data['deliveryInfo'])) {
            if (isset($data['deliveryInfo']['deliveryNote'])) {
                $bill->setDeliveryNote($data['deliveryInfo']['deliveryNote']);
            } else {
                $bill->setDeliveryNote(null);
            }

            if (isset($data['deliveryInfo']['deliveryEmail'])) {
                $bill->setDeliveryEmail($data['deliveryInfo']['deliveryEmail']);
            } else {
                $bill->setDeliveryEmail(null);
            }
        } else {
            $bill->setDeliveryNote(null);
            $bill->setDeliveryEmail(null);
        }
    }

    //////////////////////////////////////////////
    // x. GENERATE PDF
    public function getBillingFilesHandler() {
        $handler = $this->container->get('leep_admin.bill.business.billing_files_payment_partner_handler');
        return $handler;
    }

    //////////////////////////////////////////////
    // x. BILLING FORM
    public function getBillingTargetName() {
        return 'Partner';
    }

    public function getBillingOpenTargets($idType) {
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER || $idType == Business\Bill\Constant::TYPE_INVOICE_MANUAL_PARTNER) {
            $mapping = $this->container->get('easy_mapping');
            return $mapping->getMapping('LeepAdmin_Partner_List');
        }
        return $billingUtils->getOpenPaymentPartners(0);
    }
    public function getTargetName($idType, $idTarget) {
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_PARTNER) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($idTarget);
            if($r)
            return $r->getPartner();
        }
        $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneById($idTarget);
        if ($m) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($m->getidPartner());
            if($r){
                 return $r->getPartner();
            }
            
        }
        return '---';
    }


    public function getBillingEntries($idCustomer, &$billEntries) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $entry = array(
            'totalAmount' => '',
            'overrideAmount' => '',
            'idBill' => 0,
            'entries' => array()
        );
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }
        $entry['totalAmount'] = $billRecord->getTotalAmount();
        $entry['overrideAmount'] = $billRecord->getOverrideAmount();
        $entry['idBill'] = intval($billRecord->getIdBill());

        $billingEntries = $em->getRepository('AppDatabaseMainBundle:BillingPaymentPartnerEntry')->findByIdBillingPaymentPartner($billRecord->getId());
        foreach ($billingEntries as $billingEntry) {
            $entry['entries'][$billingEntry->getEntryId()] = array(
                'entry'  => $billingEntry->getEntry(),
                'amount' => $billingEntry->getAmount()
            );
        }

        $billEntries['PAY_PARTNER'] = $entry;
    }

    public function removeBillingEntries($idCustomer) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingPaymentPartner')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:BillingPaymentPartnerEntry', 'p')
            ->andWhere('p.idBillingPaymentPartner = '.$billRecord->getId())
            ->getQuery()
            ->execute();

        $em->remove($billRecord);
        $em->flush();
    }
}
