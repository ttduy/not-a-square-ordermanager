<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class CustomerPricingGridReader extends AppGridDataReader {
    public $filters;
    public $currentTimestamp;
    public $pricingHelper;
    public $formatter;
    public $dcCell = [];
    public $dcPartner = [];
    public $dcAcquisiteurs = [];
    public $computedPricing = [];

    public function __construct($container) {
        parent::__construct($container);
        $this->pricingHelper = $container->get('leep_admin.billing.business.pricing_helper');
        $this->formatter = $this->container->get('leep_admin.helper.formatter');
        $today = new \DateTime();
        $this->currentTimestamp = $today->getTimestamp();
        $this->setIterateLargeData(true);

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            } else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            $this->dcAcquisiteurs[$r->getId()] = array(
                1 => $formatter->format($r->getAcquisiteurId1(), 'mapping', 'LeepAdmin_Acquisiteur_List'),
                2 => $formatter->format($r->getAcquisiteurId2(), 'mapping', 'LeepAdmin_Acquisiteur_List'),
                3 => $formatter->format($r->getAcquisiteurId3(), 'mapping', 'LeepAdmin_Acquisiteur_List'),
            );
        }
    }

    public function getColumnMapping() {
        return array('ordernumber', 'name', 'distributionChannel', 'age', 'status', 'pricing', 'orderInfoText', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Order Number',           'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Customer Name',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel/Partner',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Age',                    'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Status',                 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Pricing',                'width' => '32%', 'sortable' => 'false'),
            array('title' => 'Info',                   'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Action',                 'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CustomerBillingGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
            ->andWhere('(p.isAddedInvoiced IS NULL) OR (p.isAddedInvoiced = 0)');

        $queryBuilder->andWhere('p.dateordered >= :baselineDate')
            ->setParameter('baselineDate', $config->getBillingBaselineDate());

        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber).'%');
        }

        if ($this->filters->idDistributionChannel != 0) {
            $queryBuilder->andWhere('p.distributionchannelid = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $this->filters->idDistributionChannel);
        }

        if ($this->filters->idPartner != 0) {
            $queryBuilder->andWhere('dc.partnerid = :idPartner')
                ->setParameter('idPartner', $this->filters->idPartner);
        }

        $config = Business\PricingSetting\Utils::getPricingConfig($this->container);
        if ($this->filters->isReadyForPricing != 0 && count($config->status) > 0) {
            if ($this->filters->isReadyForPricing == 1) {
                $queryBuilder->andWhere('p.status in (:statusList)')
                            ->setParameter('statusList', $config->status);
            } else { // = 2 (NOT READY FOR INVOICED)
                $queryBuilder->andWhere('p.status not in (:statusList)')
                            ->setParameter('statusList', $config->status);
            }
        }

        if ($this->filters->idErrorType != 0) {
            if ($this->filters->idErrorType == Constant::ERROR_TYPE_MISSING_PRICE_CATEGORY) {
                $queryBuilder->andWhere('(p.pricecategoryid = 0) OR (p.pricecategoryid IS NULL)');
            } else if ($this->filters->idErrorType == Constant::ERROR_TYPE_MISSING_MARGIN_GOES_TO) {
                $queryBuilder->andWhere('(p.idMarginGoesTo = 0) OR (p.idMarginGoesTo IS NULL)');
            } else if ($this->filters->idErrorType == Constant::ERROR_TYPE_MISSING_INVOICE_GOES_TO) {
                $queryBuilder->andWhere('(p.invoicegoestoid = 0) OR (p.invoicegoestoid IS NULL)');
            } else if ($this->filters->idErrorType == Constant::ERROR_TYPE_MISSING_DISTRIBUTION_CHANNEL) {
                $queryBuilder->andWhere('(p.distributionchannelid = 0) OR (p.distributionchannelid IS NULL)');
            } else if ($this->filters->idErrorType == Constant::ERROR_TYPE_ALL_ERROR) {
                $queryBuilder->andWhere('(p.pricecategoryid = 0) OR (p.pricecategoryid IS NULL)'.
                    ' OR (p.idMarginGoesTo = 0) OR (p.idMarginGoesTo IS NULL)'.
                    ' OR (p.invoicegoestoid = 0) OR (p.invoicegoestoid IS NULL)'.
                    ' OR (p.distributionchannelid = 0) OR (p.distributionchannelid IS NULL)');
            }
        }

        if ($this->filters->idMarginGoesTo != 0) {
            $queryBuilder->andWhere('p.idMarginGoesTo = :idMarginGoesTo')
                ->setParameter('idMarginGoesTo', $this->filters->idMarginGoesTo);
        }
        if ($this->filters->idInvoiceGoesTo != 0) {
            $queryBuilder->andWhere('p.invoicegoestoid = :idInvoiceGoesTo')
                ->setParameter('idInvoiceGoesTo', $this->filters->idInvoiceGoesTo);
        }
        if ($this->filters->idPriceCategory != 0) {
            $queryBuilder->andWhere('p.pricecategoryid = :idPriceCategory')
                ->setParameter('idPriceCategory', $this->filters->idPriceCategory);
        }
        if ($this->filters->idReportDelivery != 0) {
            $queryBuilder->andWhere('p.reportdeliveryid = :idReportDelivery')
                ->setParameter('idReportDelivery', $this->filters->idReportDelivery);
        }
        if ($this->filters->idAcquisiteur != 0) {
            $queryBuilder->andWhere('(p.idAcquisiteur1 = :idAcquisiteur) OR (p.idAcquisiteur2 = :idAcquisiteur) OR (p.idAcquisiteur3 = :idAcquisiteur)')
                ->setParameter('idAcquisiteur', $this->filters->idAcquisiteur);
        }
        if (trim($this->filters->notes) != '') {
            $queryBuilder->andWhere('(p.orderInfoText LIKE :notes) OR (p.laboratoryDetails LIKE :notes)')
                ->setParameter('notes', '%'.trim($this->filters->notes).'%');
        }
    }

    protected function iterateLargeQueryResult($result, $start, $length) {
        $finalResult = [];
        $index = 0;
        $em = $this->container->get('doctrine')->getEntityManager();
        foreach ($result as $row) {
            $row = $row[0];
            $isAddToResult = true;
            $pricing = null;
            if ($this->filters->idErrorType == Constant::ERROR_TYPE_MISSING_PRODUCT_OR_CATGORY_PRICE ||
                $this->filters->idErrorType == Constant::ERROR_TYPE_ALL_ERROR) {
                $pricing = $this->pricingHelper->computePricing($row);
                if (!$pricing->isMissingErrorOrProductPrice() &&
                    ($this->filters->idErrorType != Constant::ERROR_TYPE_ALL_ERROR || !$pricing->hasError())) {
                    $isAddToResult = false;
                }
            }

            if ($isAddToResult && ($length <= 0 || $index >= $start)) {
                if (!$pricing) {
                    $pricing = $this->pricingHelper->computePricing($row);
                }

                $this->computedPricing[$row->getId()] = $pricing;
                $finalResult[] = $row;
                $index++;
                if ($index >= $start + $length) {
                    break;
                }
            } else {
                $em->detach($row);
            }
        }

        return $finalResult;
    }

    protected function postLoadData() {
    }

    public function buildCellName($row) {
        return $row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellDistributionChannel($row) {
        return isset($this->dcCell[$row->getDistributionChannelId()]) ? $this->dcCell[$row->getDistributionChannelId()] : '';
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->currentTimestamp - $date->getTimestamp()) / 86400);
        }
        return $age;
    }

    public function buildCellAge($row) {
        $age = $this->getAge($row->getDateOrdered());
        if ($age !== '') {
            return $age.'d';
        }
        return '';
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            $builder->addButton('Update status', $mgr->getUrl('leep_admin', 'customer', 'edit_status', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());

        $age = $this->getAge($row->getStatusDate());
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellOrderInfoText($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addNewTabButton('Edit', $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
        $html = $builder->getHtml().'&nbsp;';
        $infoText = $row->getOrderInfoText();
        if (!empty($infoText)) {
            $html .='<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        $laboratoryDetails = $row->getLaboratoryDetails();
        if (!empty($laboratoryDetails)) {
            $html .='<a class="simpletip"><div class="button-activity"></div><div class="hide-content">'.nl2br($laboratoryDetails).'</div></a>';
        }
        return $html;
    }

    private $patternInvoiceOverride = "<i style='color: green; text-decoration: line-through'>%s</i>&nbsp;&nbsp;<i style='color: green'>%s</i></br>";
    private $patternInvoice = "<i style='color: green'>%s</i></br>";
    private $patternPaymentOverride = "<i style='color: red; text-decoration: line-through'>%s</i>&nbsp;&nbsp;<i style='color: red'>%s</i></br>";
    private $patternPayment = "<i style='color: red'>%s</i></br>";

    public function buildCellPricing($row) {
        $pricing = $this->computedPricing[$row->getId()];
        $content = '';

        if (Business\PricingSetting\Utils::isReadyForPricing($this->container, $row)) {
            $content .= '<b style="color: green">Ready for pricing</b><br/>';
        }

        foreach ($pricing->errors as $error) {
            $content .= '<b style="color: red">'.$error."</b><br/>";
        }

        if (!empty($pricing->priceChanges)) {
            $content .= '<b style="color: orange">Price has been updated recently</b><br/>';
        }

        $content .= "Price category '".$this->formatter->format($row->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List')."'<br/>";
        $content .= "Invoice goes to '".$this->formatter->format($row->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo')."'<br/>";
        $content .= $this->formatter->format($row->getIdMarginGoesTo(), 'mapping', 'LeepAdmin_DistributionChannel_MarginGoesTo')."<br/>";
        $content .= $this->formatter->format($row->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery')." to '".
                    $this->formatter->format($row->getCountryId(), 'mapping', 'LeepAdmin_Country_List')."'<br/>";

        $content .= "<br/>";
        // Invoice
        if ($pricing->hasBill('invoiceCustomer')) {
            $bill = $pricing->getBill('invoiceCustomer');
            $content .= "Invoice to customer: ";
            if ($pricing->isBillOverrided('invoiceCustomer')) {
                $content .= sprintf($this->patternInvoiceOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
            }
            else {
                $content .= sprintf($this->patternInvoice, number_format($bill['total'], 2));
            }
        }

        if ($pricing->hasBill('invoiceDistributionChannel')) {
            $bill = $pricing->getBill('invoiceDistributionChannel');
            $content .= "Invoice to DC: ";
            if ($pricing->isBillOverrided('invoiceDistributionChannel')) {
                $content .= sprintf($this->patternInvoiceOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
            }
            else {
                $content .= sprintf($this->patternInvoice, number_format($bill['total'], 2));
            }
        }

        if ($pricing->hasBill('invoicePartner')) {
            $bill = $pricing->getBill('invoicePartner');
            $content .= "Invoice to partner: ";
            if ($pricing->isBillOverrided('invoicePartner')) {
                $content .= sprintf($this->patternInvoiceOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
            }
            else {
                $content .= sprintf($this->patternInvoice, number_format($bill['total'], 2));
            }
        }

        // Payment
        if ($pricing->hasBill('paymentDistributionChannel')) {
            $bill = $pricing->getBill('paymentDistributionChannel');
            $content .= "Payment to DC: ";
            if ($pricing->isBillOverrided('paymentDistributionChannel')) {
                $content .= sprintf($this->patternPaymentOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
            }
            else {
                $content .= sprintf($this->patternPayment, number_format($bill['total'], 2));
            }
        }

        if ($pricing->hasBill('paymentPartner')) {
            $bill = $pricing->getBill('paymentPartner');
            $content .= "Payment to partner: ";
            if ($pricing->isBillOverrided('paymentPartner')) {
                $content .= sprintf($this->patternPaymentOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
            }
            else {
                $content .= sprintf($this->patternPayment, number_format($bill['total'], 2));
            }
        }

        for ($i = 1; $i <= 3; $i++) {
            $getMethod = 'getIdAcquisiteur'.$i;
            $acquisiteurName = $this->formatter->format($row->$getMethod(), 'mapping', 'LeepAdmin_Acquisiteur_List');
            $billId = 'paymentAcquisiteur'.$i;
            if ($pricing->hasBill($billId)) {
                $bill = $pricing->getBill($billId);
                $content .= "Payment to acquisiteur '".$acquisiteurName."': ";
                if ($pricing->isBillOverrided($billId)) {
                    $content .= sprintf($this->patternPaymentOverride, number_format($bill['total'], 2), number_format($bill['overrideTotal'], 2));
                }
                else {
                    $content .= sprintf($this->patternPayment, number_format($bill['total'], 2));
                }
            }
        }

        return $content;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'billing', 'edit_pricing', 'edit', array('id' => $row->getId())), 'button-edit');
        return $builder->getHtml();
    }
}
