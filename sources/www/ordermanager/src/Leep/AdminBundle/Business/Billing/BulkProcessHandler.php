<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkProcessHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }

    public function convertToFormModel($entity) {
        $model = new BulkProcessModel();
        $model->selectedCustomers = $this->container->get('request')->get('selected', '');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedCustomers', 'hidden');

        $builder->add('idAction', 'choice', array(
            'label'  => 'Action',
            'required' => false,
            'choices'  => Constant::getBulkProcessActions(),
        ));
        $builder->add('idPriceCategory', 'choice', array(
            'label'     => 'Price Category',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'     => 'Margin Goes To',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('idInvoiceGoesTo', 'choice', array(
            'label'     => 'Invoice Goes To',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $customers = array(0);
        $selectedCustomers = explode(',', $model->selectedCustomers);
        foreach ($selectedCustomers as $selectedId) {
            if (trim($selectedId) != '') {
                $customers[] = intval($selectedId);
            }
        }
        // Process action
        switch ($model->idAction) {
            case Constant::BULK_PROCESS_ACTION_UPDATE_PRICE:
                $util = $this->container->get('leep_admin.billing.business.utils');
                $pricingHelper = $this->container->get('leep_admin.billing.business.pricing_helper');

                foreach ($customers as $idCustomer) {
                    $util->updateCustomerPrice($this->container, $pricingHelper, $idCustomer);
                }
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY:
                $query = $em->createQueryBuilder();
                $query->update('AppDatabaseMainBundle:Customer', 'p')
                    ->set('p.pricecategoryid', intval($model->idPriceCategory))
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers)
                    ->getQuery()
                    ->execute();
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_PRICE_CATEGORY_BY_DC_SETTING:
                $query = $em->createQueryBuilder();
                $query->select('p')
                    ->from('AppDatabaseMainBundle:Customer', 'p')
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers);
                $result = $query->getQuery()->getResult();
                foreach ($result as $r) {
                    $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($r->getDistributionChannelId());
                    if ($dc) {
                        $r->setPriceCategoryId($dc->getPriceCategoryId());
                    }
                }
                $em->flush();
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO:
                $query = $em->createQueryBuilder();
                $query->update('AppDatabaseMainBundle:Customer', 'p')
                    ->set('p.idMarginGoesTo', intval($model->idMarginGoesTo))
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers)
                    ->getQuery()
                    ->execute();
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_MARGIN_GOES_TO_BY_DC_SETTING:
                $query = $em->createQueryBuilder();
                $query->select('p')
                    ->from('AppDatabaseMainBundle:Customer', 'p')
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers);
                $result = $query->getQuery()->getResult();
                foreach ($result as $r) {
                    $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($r->getDistributionChannelId());
                    if ($dc) {
                        $r->setIdMarginGoesTo($dc->getIdMarginGoesTo());
                    }
                }
                $em->flush();
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO:
                $query = $em->createQueryBuilder();
                $query->update('AppDatabaseMainBundle:Customer', 'p')
                    ->set('p.invoicegoestoid', intval($model->idInvoiceGoesTo))
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers)
                    ->getQuery()
                    ->execute();
                break;
            case Constant::BULK_PROCESS_ACTION_FIX_MISSING_INVOICE_GOES_TO_BY_DC_SETTING:
                $query = $em->createQueryBuilder();
                $query->select('p')
                    ->from('AppDatabaseMainBundle:Customer', 'p')
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers);
                $result = $query->getQuery()->getResult();
                foreach ($result as $r) {
                    $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($r->getDistributionChannelId());
                    if ($dc) {
                        $r->setInvoiceGoesToId($dc->getInvoiceGoesToId());
                    }
                }
                $em->flush();
                break;
            case Constant::BULK_PROCESS_ACTION_BULK_INVOICE:
                $pricingHelper = $this->container->get('leep_admin.billing.business.pricing_helper');
                $billingUtils = $this->container->get('leep_admin.billing.business.utils');

                $query = $em->createQueryBuilder();
                $query->select('p')
                    ->from('AppDatabaseMainBundle:Customer', 'p')
                    ->andWhere('p.id in (:customers)')
                    ->setParameter('customers', $customers);
                $result = $query->getQuery()->getResult();
                foreach ($result as $r) {
                    if ($r->getIsAddedInvoiced() == 1) {
                        $this->errors[] = $r->getOrderNumber().' is already move to phase 2';
                        continue;
                    }
                    $billingUtils->generateCustomerBillingEntries($r, $pricingHelper, $this->errors);

                    // Mark 'done'
                    $r->setIsAddedInvoiced(1);
                    $em->flush();
                }

                break;
        }

        $this->messages = array('Done!');
    }
}
