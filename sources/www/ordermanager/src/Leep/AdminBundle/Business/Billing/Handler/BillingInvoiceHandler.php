<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;

class BillingInvoiceHandler extends BillingHandler {
    public function getBillStatusMapping() {
        return 'LeepAdmin_Bill_InvoiceStatus';
    }
}
