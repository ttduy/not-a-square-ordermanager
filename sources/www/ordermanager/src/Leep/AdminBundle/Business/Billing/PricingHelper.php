<?php
namespace Leep\AdminBundle\Business\Billing;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\DistributionChannel\Constant as DCConstant;

class PricingHelper {
    public $em;
    public $container;
    public $catProdLink = array();
    public $extraPriceReportDelivery = array();
    public $billingText = array();

    public function __construct($container) {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getEntityManager();
        $this->preloadPricing();
    }

    private function preloadPricing() {
        // Load category - product link
        $this->catProdLink = array();
        $results = $this->em->getRepository('AppDatabaseMainBundle:CatProdLink', 'app_main')->findAll();
        foreach ($results as $r) {
            if (!isset($this->catProdLink[$r->getProductId()])) {
                $this->catProdLink[$r->getProductId()] = array();
            }

            $this->catProdLink[$r->getProductId()][] = $r->getCategoryId();
        }

        // Load extra price - report delivery
        $results = $this->em->getRepository('AppDatabaseMainBundle:ExtraPriceReportDelivery')->findAll();
        foreach ($results as $r) {
            $this->extraPriceReportDelivery[$r->getIdCountry()] = $r->getPrice();
            $this->extraPriceNutriMeDelivery[$r->getIdCountry()] = $r->getNutriMePrice();
        }

        // Load billing text
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $billingText = $config->getBillingText();
        $rows = explode("\n", $billingText);
        foreach ($rows as $r) {
            $c = explode("=", $r);
            if (count($c) >= 2) {
                $this->billingText[trim($c[0])] = trim($c[1]);
            }
        }
    }

    public function getCategoryPrice($idPriceCategory, $idCategory) {
        $price = $this->em->getRepository('AppDatabaseMainBundle:CategoryPrices')->findOneBy(array(
            'pricecatid' => $idPriceCategory,
            'categoryid' => $idCategory
        ));

        if (!empty($price)) {
            return array(
                'price'          => floatval($price->getPrice()),
                'dcMargin'       => floatval($price->getDCMargin()),
                'partnerMargin'  => floatval($price->getPartMargin())
            );
        }

        return null;
    }

    public function getCategoryPriceHistory($idPriceCategory, $idCategory, $date) {
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CategoryPricesHistory', 'p')
            ->andWhere('p.categoryid = :idCategory')
            ->andWhere('p.pricecatid = :idPriceCategory')
            ->andWhere('p.priceTimestamp < :orderDate')
            ->orderBy('p.priceTimestamp', 'DESC')
            ->setMaxResults(1)
            ->setParameter('orderDate', $date)
            ->setParameter('idCategory', $idCategory)
            ->setParameter('idPriceCategory', $idPriceCategory);

        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            foreach ($results as $r) {
                return array(
                    'price'         => floatval($r->getPrice()),
                    'dcMargin'      => floatval($r->getDcMargin()),
                    'partnerMargin' => floatval($r->getPartMargin())
                );
            }
        }

        return null;
    }

    public function getProductPrice($idPriceCategory, $idProduct) {
        $price = $this->em->getRepository('AppDatabaseMainBundle:Prices')->findOneBy(array(
            'pricecatid' => $idPriceCategory,
            'productid'  => $idProduct
        ));

        if (!empty($price)) {
            return array(
                'price'          => floatval($price->getPrice()),
                'dcMargin'       => floatval($price->getDCMargin()),
                'partnerMargin'  => floatval($price->getPartMargin())
            );
        }

        return null;
    }
    public function getOrderOverridePrice($idCustomer, $idEntry) {
        $price = $this->em->getRepository('AppDatabaseMainBundle:OrderedOverridePrice')->findOneBy(array(
            'idCustomer' => $idCustomer,
            'idEntry'    => $idEntry
        ));

        if (!empty($price)) {
            return array(
                'price'          => floatval($price->getPrice()),
                'dcMargin'       => floatval($price->getDcMargin()),
                'partnerMargin'  => floatval($price->getPartMargin())
            );
        }

        return null;
    }

    public function getProductPriceHistory($idPriceCategory, $idProduct, $date) {
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:PricesHistory', 'p')
            ->andWhere('p.productid = :idProduct')
            ->andWhere('p.pricecatid = :idPriceCategory')
            ->andWhere('p.priceTimestamp < :orderDate')
            ->orderBy('p.priceTimestamp', 'DESC')
            ->setMaxResults(1)
            ->setParameter('orderDate', $date)
            ->setParameter('idProduct', $idProduct)
            ->setParameter('idPriceCategory', $idPriceCategory);

        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            foreach ($results as $r) {
                return array(
                    'price'         => floatval($r->getPrice()),
                    'dcMargin'      => floatval($r->getDcMargin()),
                    'partnerMargin' => floatval($r->getPartMargin())
                );
            }
        }

        return null;
    }

    public function computePricing($customer) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $pricing = new PricingModel();

        ////////////////////////////////////
        // x. Validation
        // Get customer
        if (is_integer($customer)) {
            $customer = $this->em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($customer);
        }

        // Get DC and partner (Required DC)
        $distributionChannel = $this->em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        $partner = null;
        if (empty($distributionChannel)) {
            $pricing->addError("Please set 'Distribution channel'");
        } else {
            $partner = $this->em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($distributionChannel->getPartnerId());
        }

        // Category price
        if ($customer->getPriceCategoryId() == 0) {
            $pricing->addError("Please select 'Price category'");
        }

        // Invoice goes to
        if ($customer->getInvoiceGoesToId() == 0) {
            $pricing->addError("Please select 'Invoice goes to'");
        }

        if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_PARTNER && empty($partner)) {
            $pricing->addError("Invoice goes to 'Partner' but there's no partner");
        }

        // Margin goes to
        if ($customer->getIdMarginGoesTo() == 0) {
            $pricing->addError("Please select 'Margin goes to'");
        }

        // Error
        if (!empty($pricing->errors)) {
            return $pricing;
        }

        ////////////////////////////////////
        // x. Order categories
        $orderedCategories = array();
        $results = $this->em->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBycustomerid($customer->getId());
        foreach ($results as $r) {
            $orderedCategories[$r->getCategoryId()] = 1;
        }

        ////////////////////////////////////
        // x. Order products
        $orderedProducts = array();
        $specialProducts = array();
        $results = $this->em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($customer->getId());
        foreach ($results as $r) {
            $isExists = false;
            if (isset($this->catProdLink[$r->getProductId()])) {
                foreach ($this->catProdLink[$r->getProductId()] as $categoryId) {
                    if (isset($orderedCategories[$categoryId])) {
                        $isExists = true;
                        break;
                    }
                }
            }
            if (!$isExists) {
                $orderedProducts[$r->getProductId()] = 1;
                $specialProducts[$r->getProductId()] = $r->getSpecialProductId();
            }
        }

        ////////////////////////////////////
        // x. Select prices
        // Category
        foreach ($orderedCategories as $idCategory => $t) {
            $categoryName = $formatter->format($idCategory, 'mapping', 'LeepAdmin_Category_List');

            // Get current price
            $appliedPrice = $this->getOrderOverridePrice($customer->getId(), 'C_'.$idCategory);
            $hasOverridePrice = true;
            if (empty($appliedPrice)) {
                $hasOverridePrice = false;
                $appliedPrice = $this->getCategoryPriceHistory($customer->getPriceCategoryId(), $idCategory, $customer->getDateOrdered());
            }

            $activePrice = $this->getCategoryPrice($customer->getPriceCategoryId(), $idCategory);
            if (empty($appliedPrice) || ($appliedPrice['price'] == 0 && !$hasOverridePrice)) {
                $appliedPrice = $activePrice;
            }

            // Check price
            if (empty($appliedPrice) || ($appliedPrice['price'] == 0 && !$hasOverridePrice)) {
                $pricing->addError("Can't find price for category ".$categoryName, true);
                continue;
            }

            // Check for price changes
            $isPriceChange = false;
            if ($appliedPrice['price'] != $activePrice['price'] ||
                $appliedPrice['dcMargin'] != $activePrice['dcMargin'] ||
                $appliedPrice['partnerMargin'] != $activePrice['partnerMargin']) {
                $pricing->addPriceChange($categoryName);
                $isPriceChange = true;
            }

            // Store prices
            $pricing->addEntry(
                'C_'.$idCategory,
                $categoryName,
                $appliedPrice['price'],
                $appliedPrice['dcMargin'],
                $appliedPrice['partnerMargin'],
                $isPriceChange
            );
        }

        // Products
        foreach ($orderedProducts as $idProduct => $t) {
            $productName = $formatter->format($idProduct, 'mapping', 'LeepAdmin_Product_List');
            if (isset($specialProducts[$idProduct])) {
                $productName = $formatter->format($specialProducts[$idProduct], 'mapping', 'LeepAdmin_SpecialProduct_List');
            }

            // Get current price
            $appliedPrice = $this->getOrderOverridePrice($customer->getId(), 'P_'.$idProduct);
            $hasOverridePrice = true;
            if (empty($appliedPrice)) {
                $hasOverridePrice = false;
                $appliedPrice = $this->getProductPriceHistory($customer->getPriceCategoryId(), $idProduct, $customer->getDateOrdered());
            }

            $activePrice = $this->getProductPrice($customer->getPriceCategoryId(), $idProduct);
            if (empty($appliedPrice) || ($appliedPrice['price'] == 0 && !$hasOverridePrice)) {
                $appliedPrice = $activePrice;
            }

            // Check price
            if (empty($appliedPrice) || ($appliedPrice['price'] == 0 && !$hasOverridePrice)) {
                $pricing->addError("Can't find price for product ".$productName, true);
                continue;
            }

            // Check for price changes
            $isPriceChange = false;
            if ($appliedPrice['price'] != $activePrice['price'] ||
                $appliedPrice['dcMargin'] != $activePrice['dcMargin'] ||
                $appliedPrice['partnerMargin'] != $activePrice['partnerMargin']) {
                $pricing->addPriceChange($productName);
                $isPriceChange = true;
            }

            // Store prices
            $pricing->addEntry(
                'P_'.$idProduct,
                $productName,
                $appliedPrice['price'],
                $appliedPrice['dcMargin'],
                $appliedPrice['partnerMargin'],
                $isPriceChange
            );
        }

        ////////////////////////////////////
        // x. Extra price
        // Report Delivery
        if ($customer->getReportDeliveryId() == DCConstant::REPORT_DELIVERY_PRINT ||
            $customer->getReportDeliveryId() == DCConstant::REPORT_DELIVERY_PRINT_AND_EMAIL) {
            if (isset($this->extraPriceReportDelivery[$customer->getCountryId()])) {
                $pricing->extraEntryPostage = floatval($this->extraPriceReportDelivery[$customer->getCountryId()]);
            }
            else {
                $pricing->addError("Can't find extra price - report delivery for the country");
            }

            if ($customer->getExtraPriceReportDeliveryOverride() != '') {
                $pricing->extraEntryPostage = floatval($customer->getExtraPriceReportDeliveryOverride());
            }

            if ($customer->getReportDeliveryId() == DCConstant::REPORT_DELIVERY_PRINT_AND_EMAIL) {
                $pricing->reportDeliveryBillingText = isset($this->billingText['REPORT_DELIVERY_PRINT_AND_EMAIL']) ? $this->billingText['REPORT_DELIVERY_PRINT_AND_EMAIL'] : 'Print and email';
            }
            else {
                $pricing->reportDeliveryBillingText = isset($this->billingText['REPORT_DELIVERY_PRINT']) ? $this->billingText['REPORT_DELIVERY_PRINT'] : 'Print';
            }
        }

        // Nutri Me
        $query = $this->em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->leftJoin('AppDatabaseMainBundle:ProductsDnaPlus', 'pro', 'WITH', 'p.productid = pro.id')
            ->andWhere('p.customerid = :idCustomer')
            ->andWhere('pro.isNutriMe = 1')
            ->setParameter('idCustomer', $customer->getId());
        $total = intval($query->getQuery()->getSingleScalarResult());
        if ($total > 0) {
            $pricing->extraNutriMeNumProduct = $total;
            if (isset($this->extraPriceNutriMeDelivery[$customer->getCountryId()])) {
                $pricing->extraNutriMePricePerProduct = floatval($this->extraPriceNutriMeDelivery[$customer->getCountryId()]);
                $pricing->extraNutriMeDeliveryPrice = $pricing->extraNutriMePricePerProduct * $total;
            }
            else {
                $pricing->addError("Can't find extra price - NutriMe Delivery for the country");
            }

            if ($customer->getExtraPriceNutriMeDeliveryOverride() != '') {
                $pricing->extraNutriMeDeliveryPrice = floatval($customer->getExtraPriceNutriMeDeliveryOverride());
            }
            $pricing->nutriMeBillingText = isset($this->billingText['NUTRIME_DELIVERY']) ? $this->billingText['NUTRIME_DELIVERY'] : 'NutriMe Delivery';
        }

        ////////////////////////////////////
        // x. Compute prices for parties
        foreach ($pricing->entries as $entry) {
            $pricing->addBillEntry('price_sum', $entry, $entry['price']);
            if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CUSTOMER) {
                // Invoice
                $pricing->addBillEntry('invoiceCustomer', $entry, $entry['price']);

                // Payment
                if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_DC) {
                    $pricing->addBillEntry('paymentDistributionChannel', $entry, $entry['dcMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_PARNER) {
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['partnerMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_PARTNER_AND_DC) {
                    $pricing->addBillEntry('paymentDistributionChannel', $entry, $entry['dcMargin']);
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['partnerMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_BOTH_TO_DC) {
                    $pricing->addBillEntry('paymentDistributionChannel', $entry, $entry['dcMargin'] + $entry['partnerMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_BOTH_TO_PARTNER) {
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['dcMargin'] + $entry['partnerMargin']);
                }
            } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CHANNEL) {
                // Invoice
                $pricing->addBillEntry('invoiceDistributionChannel', $entry, $entry['price'] - $entry['dcMargin']);

                // Payment
                if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_PARNER) {
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['partnerMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_PARTNER_AND_DC) {
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['partnerMargin']);
                } else if ($customer->getIdMarginGoesTo() == DCConstant::MARGIN_GOES_TO_BOTH_TO_PARTNER) {
                    $pricing->addBillEntry('paymentPartner', $entry, $entry['dcMargin'] + $entry['partnerMargin']);
                }
            } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_PARTNER) {
                $pricing->addBillEntry('invoicePartner', $entry, $entry['price'] - ($entry['dcMargin'] + $entry['partnerMargin']));
            }
        }

        if (empty($partner)) {
            $pricing->removeBill('invoicePartner');
            $pricing->removeBill('paymentPartner');
        }

        $pricing->finalizeBills();

        ////////////////////////////////////
        // x. Compute acquisiteur commission
        if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CUSTOMER) {
            $totalInvoice = $pricing->getBillFinalTotal('invoiceCustomer');
        } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CHANNEL) {
            $totalInvoice = $pricing->getBillFinalTotal('invoiceDistributionChannel');
        } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_PARTNER) {
            $totalInvoice = $pricing->getBillFinalTotal('invoicePartner');
        }

        $totalPayment = $pricing->getBillFinalTotal('paymentDistributionChannel') + $pricing->getBillFinalTotal('paymentPartner');
        $effectiveAmount = $totalInvoice - $totalPayment;

        if ($effectiveAmount < 0) {
            $effectiveAmount = 0;
        }

        if ($customer->getIdAcquisiteur1() != 0) {
            $pricing->addBillEntry('paymentAcquisiteur1', $pricing->orderNumber, $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate1()) / 100);
        }

        if ($customer->getIdAcquisiteur2() != 0) {
            $pricing->addBillEntry('paymentAcquisiteur2', $pricing->orderNumber, $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate2()) / 100);
        }

        if ($customer->getIdAcquisiteur3() != 0) {
            $pricing->addBillEntry('paymentAcquisiteur3', $pricing->orderNumber, $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate3()) / 100);
        }

        $pricing->finalizeBills();

        ////////////////////////////////////
        // x. Price override
        // Price override
        $invoicePriceOverride = floatval($customer->getPriceOverride());
        if ($invoicePriceOverride > 0) {
            $pricing->overrideBillTotal('invoiceCustomer', $invoicePriceOverride);
            $pricing->overrideBillTotal('invoiceDistributionChannel', $invoicePriceOverride);
            $pricing->overrideBillTotal('invoicePartner', $invoicePriceOverride);
        }

        // Margin override - DC
        $marginOverrideDc = $customer->getMarginOverrideDc();
        if (is_numeric($marginOverrideDc)) {
            $bill = $pricing->getBill('paymentDistributionChannel');
            if ($bill) {
                $overrideTotal = $bill['total'] * floatval($marginOverrideDc) / 100;
                $pricing->overrideBillTotal('paymentDistributionChannel', $overrideTotal);
            }
        }

        // Margin override - Partner
        $marginOverridePartner = $customer->getMarginOverridePartner();
        if (is_numeric($marginOverridePartner)) {
            $bill = $pricing->getBill('paymentPartner');
            if ($bill) {
                $overrideTotal = $bill['total'] * floatval($marginOverridePartner) / 100;
                $pricing->overrideBillTotal('paymentPartner', $overrideTotal);
            }
        }

        ////////////////////////////////////
        // x. Re-Compute acquisiteur commission after override
        if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CUSTOMER) {
            $totalInvoice = $pricing->getBillFinalTotal('invoiceCustomer');
        } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_CHANNEL) {
            $totalInvoice = $pricing->getBillFinalTotal('invoiceDistributionChannel');
        } else if ($customer->getInvoiceGoesToId() == DCConstant::INVOICE_GOES_TO_PARTNER) {
            $totalInvoice = $pricing->getBillFinalTotal('invoicePartner');
        }

        $totalPayment = $pricing->getBillFinalTotal('paymentDistributionChannel') + $pricing->getBillFinalTotal('paymentPartner');
        $effectiveAmount = $totalInvoice - $totalPayment;

        if ($effectiveAmount < 0) {
            $effectiveAmount = 0;
        }

        if ($customer->getIdAcquisiteur1() != 0) {
            $pricing->overrideBillTotal('paymentAcquisiteur1', $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate1()) / 100);
        }

        if ($customer->getIdAcquisiteur2() != 0) {
            $pricing->overrideBillTotal('paymentAcquisiteur2', $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate2()) / 100);
        }

        if ($customer->getIdAcquisiteur3() != 0) {
            $pricing->overrideBillTotal('paymentAcquisiteur3', $effectiveAmount * floatval($customer->getAcquisiteurCommissionRate3()) / 100);
        }

        $pricing->finalizeBills();

        ////////////////////////////////////
        // x. Override acquisiteur commission
        if (is_numeric($customer->getPaymentOverrideAcquisiteur1())) {
            $pricing->overrideBillTotal('paymentAcquisiteur1', $customer->getPaymentOverrideAcquisiteur1());
        }

        if (is_numeric($customer->getPaymentOverrideAcquisiteur2())) {
            $pricing->overrideBillTotal('paymentAcquisiteur2', $customer->getPaymentOverrideAcquisiteur2());
        }

        if (is_numeric($customer->getPaymentOverrideAcquisiteur3())) {
            $pricing->overrideBillTotal('paymentAcquisiteur3', $customer->getPaymentOverrideAcquisiteur3());
        }

        return $pricing;
    }
}