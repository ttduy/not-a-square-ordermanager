<?php
namespace Leep\AdminBundle\Business\Billing;

use Easy\ModuleBundle\Module\AbstractHook;

class EditPricingHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $idCustomer = $controller->get('request')->get('id', 0);
        $em = $controller->get('doctrine')->getEntityManager();
        $manager = $controller->get('easy_module.manager');
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);

        $pricingHelper = $controller->get('leep_admin.billing.business.pricing_helper');
        $pricing = $pricingHelper->computePricing($customer);

        $formatter = $controller->get('leep_admin.helper.formatter');

        $data['priceChangesUrl'] = $manager->getUrl('leep_admin', 'billing', 'feature', 'viewPriceChanges', array('idCustomer' => $customer->getId()));
        $data['loadLatestPriceUrl'] = $manager->getUrl('leep_admin', 'billing', 'feature', 'loadLatestPrice', array('idCustomer' => $customer->getId()));

        $data['pricing'] = $pricing;
        $data['customer'] = $customer;
        $data['customerOrderDate'] = $formatter->format($customer->getDateOrdered(), 'date');
        $data['customerStatus'] = $formatter->format($customer->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
        $data['customerPriceCategory'] = $formatter->format($customer->getPriceCategoryId(), 'mapping', 'LeepAdmin_PriceCategory_List');
        $data['customerInvoiceGoesTo'] = $formatter->format($customer->getInvoiceGoesToId(), 'mapping', 'LeepAdmin_DistributionChannel_InvoiceGoesTo');
        $data['customerMarginGoesTo'] = $formatter->format($customer->getIdMarginGoesTo(), 'mapping', 'LeepAdmin_DistributionChannel_MarginGoesTo');
        $data['distributionChannel'] = $formatter->format($customer->getDistributionChannelId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
        $data['reportDelivery'] = $formatter->format($customer->getReportDeliveryId(), 'mapping', 'LeepAdmin_DistributionChannel_ReportDelivery');
        $data['customerCountry'] = $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
        $data['editCustomerUrl'] = $manager->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $customer->getId()));
        $data['partner'] = '';
        $data['acquisiteurs'] = array();
        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        if ($dc) {
            $data['partner'] = $formatter->format($dc->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');
        }

        $acquisiteurs = array();
        if ($customer->getIdAcquisiteur1() != 0){
            $acquisiteurs[] = '1. '.$formatter->format($customer->getIdAcquisiteur1(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate1(), 2).'%)';
        }
        if ($customer->getIdAcquisiteur2() != 0){
            $acquisiteurs[] = '2. '.$formatter->format($customer->getIdAcquisiteur2(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate2(), 2).'%)';
        }
        if ($customer->getIdAcquisiteur3() != 0){
            $acquisiteurs[] = '3. '.$formatter->format($customer->getIdAcquisiteur3(), 'mapping', 'LeepAdmin_Acquisiteur_List').' ('.number_format($customer->getAcquisiteurCommissionRate3(), 2).'%)';
        }

        $data['acquisiteurs'] = implode('<br/>', $acquisiteurs);

        // Invoice
        $data['invoiceTo'] = '';
        $data['entries'] = $pricing->entries;
        $data['invoiceBill'] = [
            'entries'         => [],
            'total'           => 0,
            'overrideTotal'   => '',
            'finalTotal'      => ''
        ];

        if ($pricing->hasBill('invoiceCustomer')) {
            $data['invoiceTo'] = 'Customer';
            $data['invoiceBill'] = $pricing->getBill('invoiceCustomer');
        }
        if ($pricing->hasBill('invoiceDistributionChannel')) {
            $data['invoiceTo'] = 'DC';
            $data['invoiceBill'] = $pricing->getBill('invoiceDistributionChannel');
        }
        if ($pricing->hasBill('invoicePartner')) {
            $data['invoiceTo'] = 'Partner';
            $data['invoiceBill'] = $pricing->getBill('invoicePartner');
        }

        // Payment
        if ($pricing->hasBill('paymentDistributionChannel')) {
            $data['paymentDcBill'] = $pricing->getBill('paymentDistributionChannel');
        }
        if ($pricing->hasBill('paymentPartner')) {
            $data['paymentPartnerBill'] = $pricing->getBill('paymentPartner');
        }

        // Acquisiteur
        if ($pricing->hasBill('paymentAcquisiteur1')) {
            $data['paymentAcquisiteurBill1'] = $pricing->getBill('paymentAcquisiteur1');
        }
        if ($pricing->hasBill('paymentAcquisiteur2')) {
            $data['paymentAcquisiteurBill2'] = $pricing->getBill('paymentAcquisiteur2');
        }
        if ($pricing->hasBill('paymentAcquisiteur3')) {
            $data['paymentAcquisiteurBill3'] = $pricing->getBill('paymentAcquisiteur3');
        }

        // Extra entry postage
        if ($pricing->extraEntryPostage !== '') {
            $data['extraEntryPostage'] = number_format($pricing->extraEntryPostage, 2);
        }
        $data['reportDeliveryBillingText'] = $pricing->reportDeliveryBillingText;
        if ($pricing->extraNutriMeDeliveryPrice !== '') {
            $data['extraNutriMeDeliveryPrice'] = number_format($pricing->extraNutriMeDeliveryPrice, 2);
            $data['extraNutriMeNumProduct'] = $pricing->extraNutriMeNumProduct;
            $data['extraNutriMePricePerProduct'] = $pricing->extraNutriMePricePerProduct;
        }
        $data['nutriMeBillingText'] = $pricing->nutriMeBillingText;

        $data['isAddedInvoiced'] = $customer->getIsAddedInvoiced();
    }
}
