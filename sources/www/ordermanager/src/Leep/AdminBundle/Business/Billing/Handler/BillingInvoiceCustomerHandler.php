<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
class BillingInvoiceCustomerHandler extends BillingInvoiceHandler {
    public function generateBillingEntries($pricing, $customer, $bill) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        // Make billing record
        $billingRecord = new Entity\BillingInvoiceCustomer();
        $billingRecord->setIdCustomer($customer->getId());
        $billingRecord->setOrderNumber($customer->getOrderNumber());
        $billingRecord->setOrderDate($customer->getDateOrdered());
        $billingRecord->setName($customer->getFirstName().' '.$customer->getSurName());
        $billingRecord->setTotalAmount($bill['total']);
        if ($bill['overrideTotal'] != '' && (floatval($bill['overrideTotal']) != floatval($bill['total']))) {
            $billingRecord->setOverrideAmount($bill['overrideTotal']);
        }
        $em->persist($billingRecord);
        $em->flush();

        // Make billing entry
        foreach ($bill['entries'] as $entry) {
            $billingEntry = new Entity\BillingInvoiceCustomerEntry();
            $billingEntry->setIdBillingInvoiceCustomer($billingRecord->getId());
            $billingEntry->setEntryId($entry['id']);
            $billingEntry->setEntry($entry['product']);
            $billingEntry->setAmount($entry['amount']);
            $em->persist($billingEntry);
        }

        // Make extra billing entry - Report delivery
        if ($pricing->extraEntryPostage != '') {
            $reportDeliveryEntry = $pricing->reportDeliveryBillingText;
            $billingEntry = new Entity\BillingInvoiceCustomerEntry();
            $billingEntry->setIdBillingInvoiceCustomer($billingRecord->getId());
            $billingEntry->setEntryId('REPORT_DELIVERY');
            $billingEntry->setEntry($reportDeliveryEntry);
            $billingEntry->setAmount($pricing->extraEntryPostage);
            $em->persist($billingEntry);
        }

        // Make extra billing entry - NutriME
        if ($pricing->extraNutriMeDeliveryPrice != '') {
            $reportDeliveryNutriMeEntry =
                sprintf("%s (x%s)",
                    $pricing->nutriMeBillingText,
                    $pricing->extraNutriMeNumProduct
                );
            $billingEntry = new Entity\BillingInvoiceCustomerEntry();
            $billingEntry->setIdBillingInvoiceCustomer($billingRecord->getId());
            $billingEntry->setEntryId('NUTRIME_DELIVERY');
            $billingEntry->setEntry($reportDeliveryNutriMeEntry);
            $billingEntry->setAmount($pricing->extraNutriMeDeliveryPrice);
            $em->persist($billingEntry);
        }

        $em->flush();
    }

    /////////////////////////////////////////////////////////
    // x. GET TOTAL OPEN
    public function getTotalOpen() {
        return $this->queryTotalOpen('BillingInvoiceCustomer');
    }

    /////////////////////////////////////////////////////////
    // x. DELETE BILL
    public function deleteBill($bill) {
        $this->unhookBill('BillingInvoiceCustomer', $bill->getId());
    }

    /////////////////////////////////////////////////////////
    // x. FORM - BUILD FORM
    protected function billFormBuildBillToSection($builder, $bill) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('sectionTo', 'section', array(
            'label'    => 'To',
            'property_path' => false
        ));
         $builder->add('customer', 'app_billing_invoice_customer', array(
             'label'    => 'Customer',
             'required' => false,
             'idBill'   => $bill->getId()
         ));
    }

    public function getDeliveryInfo($bill, $idTarget) {
        // $idTarget = $bill ? $bill->getIdTarget() : $idTarget;
        // $deliveryNote = $bill ? $bill->getDeliveryNote() : '';
        // $deliveryEmail = $bill ? $bill->getDeliveryEmail() : '';
        // if (!$deliveryNote || !$deliveryEmail) {
        //     $em = $this->container->get('doctrine')->getEntityManager();
        //     $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idTarget);
        //     if ($customer) {
        //         // if (!$deliveryNote) {
        //         //     $deliveryNote = $customer->getInvoiceDeliveryNote();
        //         // }

        //         if (!$deliveryEmail) {
        //             $deliveryEmail = $customer->getInvoicedeliveryemail();
        //         }
        //     }
        // }
         $em = $this->container->get('doctrine')->getEntityManager();
            $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idTarget);
            $deliveryNote='';
            $deliveryEmail='';
            if ($customer) {
            
                $deliveryNote = $customer->getInvoiceDeliveryNote();
                $deliveryEmail = $customer->getInvoicedeliveryemail();
                
            }
        return array(
            'deliveryNote' => $deliveryNote,
            'deliveryEmail' => $deliveryEmail
        );
    }

    ///////////////////////////////////////////////////////
    // x. FORM - LOAD DATA
    protected function billFormLoadDataExtra($bill) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $customer = array();
        $customer['idBillingInvoiceCustomer'] = $bill->getIdTarget();

        $billingInvoiceCustomer = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($bill->getIdTarget());
        if (!empty($billingInvoiceCustomer)) {
            $customer['totalAmount'] = $billingInvoiceCustomer->getTotalAmount();
            $customer['overrideAmount'] = $billingInvoiceCustomer->getOverrideAmount();
            $customer['nameCustomer'] = $billingInvoiceCustomer->getOrderNumber(). ' ('. $billingInvoiceCustomer->getName(). ')';
            // get data From Bill
            $customer['idTarget'] = $bill->getIdTarget();
            $customer['invoiceAddressStreet'] = $bill->getInvoiceAddressStreet();
            $customer['invoiceAddressPostCode'] = $bill->getInvoiceAddressPostCode();
            $customer['invoiceAddressCity'] = $bill->getInvoiceAddressCity();
            $customer['invoiceAddressIdCountry'] = $bill->getInvoiceAddressIdCountry();
            $customer['invoiceAddressTelephone'] = $bill->getInvoiceAddressTelephone();
            $customer['invoiceAddressFax'] = $bill->getInvoiceAddressFax();
            $customer['invoiceAddressUid'] = $bill->getInvoiceAddressUid();
            $customer['haveInvoiceAddress'] = ($bill->getIsInvoiceAddress() == 1) ? 'Yes' : 'No';
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($billingInvoiceCustomer->getIdCustomer());
            if ($customerInfo) {
                if ($customerInfo->getInvoiceAddressIsUsed() == 1) {
                    $customer['invoiceAddressIsUsed'] = $customerInfo->getInvoiceAddressIsUsed();
                    $customer['invoiceAddressClientName'] = $customerInfo->getInvoiceAddressClientName();
                    $customer['invoiceAddressCompanyName'] = $customerInfo->getInvoiceAddressCompanyName();
                    // $customer['invoiceAddressStreet'] = $customerInfo->getInvoiceAddressStreet();
                    // $customer['invoiceAddressPostCode'] = $customerInfo->getInvoiceAddressPostCode();
                    // $customer['invoiceAddressCity'] = $customerInfo->getInvoiceAddressCity();
                    // $customer['invoiceAddressIdCountry'] = $customerInfo->getInvoiceAddressIdCountry();
                    // $customer['invoiceAddressTelephone'] = $customerInfo->getInvoiceAddressTelephone();
                    // $customer['invoiceAddressFax'] = $customerInfo->getInvoiceAddressFax();
                    // $customer['invoiceAddressUid'] = $customerInfo->getInvoiceAddressUid();
                    // $customer['haveInvoiceAddress']    = 'Yes';
                }
                else {
                    $customer['invoiceAddressIsUsed'] = $customerInfo->getInvoiceAddressIsUsed();
                    $customer['invoiceAddressClientName'] = '';
                    $customer['invoiceAddressCompanyName'] = '';
                    // $customer['invoiceAddressStreet'] = $customerInfo->getStreet();
                    // $customer['invoiceAddressPostCode'] = $customerInfo->getPostCode();
                    // $customer['invoiceAddressCity'] = $customerInfo->getCity();
                    // $customer['invoiceAddressIdCountry'] = $customerInfo->getCountryId();
                    // $customer['invoiceAddressTelephone'] = $customerInfo->getTelephone();
                    // $customer['invoiceAddressFax'] = $customerInfo->getFax();
                    // $customer['invoiceAddressUid'] = $customerInfo->getInvoiceAddressUid();
                    // $customer['haveInvoiceAddress']    = 'No';
                }
                $customer['orderInformation'] = $customerInfo->getOrderInfoText();
            }
        }

        return array(
            'customer' => $customer,
            'deliveryInfo' => self::getDeliveryInfo($bill->getIdType(),$bill->getIdTarget())
        );
    }

    ///////////////////////////////////////////////////////
    // x. FORM - SAVE DATA
    protected function billFormOnSuccessExtra($bill, $data) {
        $idBillingInvoiceCustomer = $data['customer']['idBillingInvoiceCustomer'];

        $em = $this->container->get('doctrine')->getEntityManager();
        $this->unhookBill('BillingInvoiceCustomer', $bill->getId());
        $this->hookBill('BillingInvoiceCustomer', array($idBillingInvoiceCustomer), $bill->getId());

        $bill->setIdTarget($idBillingInvoiceCustomer);
        $billingInvoiceCustomer = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($idBillingInvoiceCustomer);
        if (!empty($billingInvoiceCustomer)) {
            $billingInvoiceCustomer->setOverrideAmount($data['customer']['overrideAmount']);
        }
        // save address to Bill
        $bill->setInvoiceAddressStreet($data['customer']['invoiceAddressStreet']);
        $bill->setInvoiceAddressPostCode($data['customer']['invoiceAddressPostCode']);
        $bill->setInvoiceAddressCity($data['customer']['invoiceAddressCity']);
        $bill->setInvoiceAddressIdCountry($data['customer']['invoiceAddressIdCountry']);
        $bill->setInvoiceAddressTelephone($data['customer']['invoiceAddressTelephone']);
        $bill->setInvoiceAddressFax($data['customer']['invoiceAddressFax']);
        $bill->setInvoiceAddressUid($data['customer']['invoiceAddressUid']);
        if (isset($data['deliveryInfo']['deliveryNote'])) {
            $bill->setDeliveryNote($data['deliveryInfo']['deliveryNote']);
        } else {
            $bill->setDeliveryNote(null);
        }

        if (isset($data['deliveryInfo']['deliveryEmail'])) {
            $bill->setDeliveryEmail($data['deliveryInfo']['deliveryEmail']);
        } else {
            $bill->setDeliveryEmail(null);
        }
    }

    //////////////////////////////////////////////
    // x. GENERATE PDF
    public function getBillingFilesHandler() {
        $handler = $this->container->get('leep_admin.bill.business.billing_files_invoice_customer_handler');
        return $handler;
    }

    //////////////////////////////////////////////
    // x. BILLING FORM
    public function getBillingTargetName() {
        return 'Customer';
    }

    public function getBillingOpenTargets($idType) {
        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
        return $billingUtils->getOpenCustomers(0);
    }

    public function getTargetName($idType, $idTarget) {
        $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneById($idTarget);
        if ($r) {
            return $r->getOrderNumber().' - '.$r->getName();
        }
        return '---';
    }


    public function getBillingEntries($idCustomer, &$billEntries) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $entry = array(
            'invoiceTo'       => 'Customer',
            'totalAmount' => '',
            'overrideAmount' => '',
            'idBill' => 0,
            'entries' => array()
        );
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }
        $entry['totalAmount'] = $billRecord->getTotalAmount();
        $entry['overrideAmount'] = $billRecord->getOverrideAmount();
        $entry['idBill'] = intval($billRecord->getIdBill());

        $billingEntries = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomerEntry')->findByIdBillingInvoiceCustomer($billRecord->getId());
        foreach ($billingEntries as $billingEntry) {
            $entry['entries'][$billingEntry->getEntryId()] = array(
                'entry'  => $billingEntry->getEntry(),
                'amount' => $billingEntry->getAmount()
            );
        }

        $billEntries['INVOICE'] = $entry;
    }

    public function removeBillingEntries($idCustomer) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billRecord = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceCustomer')->findOneByIdCustomer($idCustomer);
        if (empty($billRecord)) {
            return false;
        }

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:BillingInvoiceCustomerEntry', 'p')
            ->andWhere('p.idBillingInvoiceCustomer = '.$billRecord->getId())
            ->getQuery()
            ->execute();

        $em->remove($billRecord);
        $em->flush();
    }

}
