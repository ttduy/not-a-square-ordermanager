<?php
namespace Leep\AdminBundle\Business\Billing\Handler;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class BillingPaymentAcquisiteurHandler extends BillingPaymentHandler {
    public function generateBillingEntries($pricing, $customer, $bill, $acquisiteurIndex) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $getMethod = 'getIdAcquisiteur'.$acquisiteurIndex;
        $idAcquisiteur = $customer->$getMethod();

        // Make billing record
        $billingRecord = new Entity\BillingPaymentAcquisiteur();
        $billingRecord->setIdCustomer($customer->getId());
        $billingRecord->setIdAcquisiteur($idAcquisiteur);
        $billingRecord->setOrderNumber($customer->getOrderNumber());
        $billingRecord->setOrderDate($customer->getDateOrdered());
        $billingRecord->setName($customer->getFirstName().' '.$customer->getSurName());
        $billingRecord->setTotalAmount($bill['total']);
        if ($bill['overrideTotal'] != '' && (floatval($bill['overrideTotal']) != floatval($bill['total']))) {
            $billingRecord->setOverrideAmount($bill['overrideTotal']);
        }

        $em->persist($billingRecord);
        $em->flush();

        // Make billing entry
        $billingEntry = new Entity\BillingPaymentAcquisiteurEntry();
        $billingEntry->setIdBillingPaymentAcquisiteur($billingRecord->getId());
        $billingEntry->setEntryId('ORDER_NUMBER');
        $billingEntry->setEntry($customer->getOrderNumber());
        $billingEntry->setAmount($bill['total']);
        $em->persist($billingEntry);

        $em->flush();
    }

    /////////////////////////////////////////////////////////
    // x. GET TOTAL OPEN
    public function getTotalOpen() {
        return $this->queryTotalOpen('BillingPaymentAcquisiteur');
    }

    /////////////////////////////////////////////////////////
    // x. DELETE BILL
    public function deleteBill($bill) {
        $this->unhookBill('BillingPaymentAcquisiteur', $bill->getId());
    }


    ///////////////////////////////////////////////////////
    // x. ORDER SELECTOR
    public function getOrderSelectorHandler() {
        $orderSelectorHandler = $this->container->get('leep_admin.bill.business.order_selector_handler');
        $orderSelectorHandler->tableCode = 'BillingPaymentAcquisiteur';
        $orderSelectorHandler->idTargetField = 'idAcquisiteur';
        $orderSelectorHandler->mappingCode = 'LeepAdmin_Acquisiteur_List';
        $orderSelectorHandler->targetLabel = 'Acquisiteur';
        return $orderSelectorHandler;
    }

    public function getTargetBillingData($idType, $id) {
        $acquisiteur=array();
         $em = $this->container->get('doctrine')->getEntityManager();
        if ($idType != Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findOneById($id);
            if($r)
                $acquisiteur=$em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($r->getidAcquisiteur());
        }
        else{
             $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($id);
        }
        return [
            'idNoTaxText' =>                $acquisiteur->getPaymentNoTaxText(),
            'idMarginPaymentMode' =>        $acquisiteur->getIdMarginPaymentMode(),
            'commissionPaymentWithTax' =>   $acquisiteur->getCommissionPaymentWithTax(),
            'addressStreet' =>              $acquisiteur->getStreet(),
            'addressPostCode' =>            $acquisiteur->getPostCode(),
            'addressCity' =>                $acquisiteur->getCity(),
            'addressCountry' =>             $acquisiteur->getCountryID(),
            'addressTelephone' =>           $acquisiteur->getTelephone(),
            'addressFax' =>                 $acquisiteur->getFax(),
            'addressUid' =>                 $acquisiteur->getUidNumber(),
            'isInvoiceAddress' =>           0,
            'haveInvoiceAddress' =>         'No'
        ];
    }

    public function getDeliveryInfo($idType, $idTarget) {
        // $idTarget = $bill ? $bill->getIdTarget() : $idTarget;
        // $deliveryNote = $bill ? $bill->getDeliveryNote() : '';
        // $deliveryEmail = $bill ? $bill->getDeliveryEmail() : '';
        // if (!$deliveryNote || !$deliveryEmail) {
        //     $em = $this->container->get('doctrine')->getEntityManager();
        //     $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($idTarget);
        //     if ($acquisiteur) {
        //         // if (!$deliveryNote) {
        //         //     $deliveryNote = $acquisiteur->getInvoiceDeliveryNote();
        //         // }

        //         if (!$deliveryEmail) {
        //             $deliveryEmail = $acquisiteur->getInvoiceDeliveryEmail();
        //         }
        //     }
        // }

        $acquisiteur=array();
        $deliveryNote='';
        $deliveryEmail='';
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
             $acquisiteur = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($idTarget);
        }
        else{
             $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findOneById($idTarget);
             if($m){
                $acquisiteur = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($m->getIdAcquisiteur());
             }
            

        }
        if($acquisiteur){
                //$deliveryNote=$acquisiteur->getInvoiceDeliveryNote();
                $deliveryEmail = $acquisiteur->getInvoicedeliveryemail();
        }
        return array(
            'deliveryNote' => $deliveryNote,
            'deliveryEmail' => $deliveryEmail
        );
    }

    /////////////////////////////////////////////////////////
    // x. FORM - BUILD FORM
    protected function billFormBuildBillToSection($builder, $bill) {
        $this->getOrderSelectorHandler()->buildBillToSection($builder, $bill);
    }

    ///////////////////////////////////////////////////////
    // x. FORM - LOAD DATA
    protected function billFormLoadDataExtra($bill) {
        $arrData = $this->getOrderSelectorHandler()->formLoadData($bill);
        $arrData += array('deliveryInfo' => self::getDeliveryInfo($bill->getIdType(), $bill->getIdTarget()));
        return $arrData;
    }

    ///////////////////////////////////////////////////////
    // x. FORM - SAVE DATA
    protected function billFormOnSuccessExtra($bill, $data) {
        $this->getOrderSelectorHandler()->formOnSuccess($this, $bill, $data);
        if (isset($data['deliveryInfo']['deliveryNote'])) {
            $bill->setDeliveryNote($data['deliveryInfo']['deliveryNote']);
        } else {
            $bill->setDeliveryNote(null);
        }

        if (isset($data['deliveryInfo']['deliveryEmail'])) {
            $bill->setDeliveryEmail($data['deliveryInfo']['deliveryEmail']);
        } else {
            $bill->setDeliveryEmail(null);
        }
    }

    //////////////////////////////////////////////
    // x. GENERATE PDF
    public function getBillingFilesHandler() {
        $handler = $this->container->get('leep_admin.bill.business.billing_files_payment_acquisiteur_handler');
        return $handler;
    }

    //////////////////////////////////////////////
    // x. BILLING FORM
    public function getBillingTargetName() {
        return 'Acquisiteur';
    }

    public function getBillingOpenTargets($idType) {
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
            $mapping = $this->container->get('easy_mapping');
            return $mapping->getMapping('LeepAdmin_Acquisiteur_List');
        }

        $billingUtils = $this->container->get('leep_admin.billing.business.utils');
        return $billingUtils->getOpenPaymentAcquisiteurs(0);
    }

    public function getTargetName($idType,$idTarget) {
        if ($idType == Business\Bill\Constant::TYPE_PAYMENT_MANUAL_ACQUISITEUR) {
             $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($idTarget);
              if($r)
            return $r->getAcquisiteur();
        }
        $m = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findOneById($idTarget);
        if ($m) {
            $r = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Acquisiteur')->findOneById($m->getidAcquisiteur());
            if($r)
            return $r->getAcquisiteur();
        }
        return '---';
    }


    public function getBillingEntries($idCustomer, &$billEntries) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billRecords = $em->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findByIdCustomer($idCustomer, array('id' => 'ASC'));
        if (empty($billRecords)) {
            return false;
        }

        $i = 1;
        foreach ($billRecords as $billRecord) {
            $entry = array(
                'totalAmount' => $billRecord->getTotalAmount(),
                'overrideAmount' => $billRecord->getOverrideAmount(),
                'idBill' => intval($billRecord->getIdBill()),
                'entries' => array()
            );

            $billingEntries = $em->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteurEntry')->findByIdBillingPaymentAcquisiteur($billRecord->getId());
            foreach ($billingEntries as $billingEntry) {
                $entry['entries'][$billingEntry->getEntryId()] = [
                    'entry'  => $billingEntry->getEntry(),
                    'amount' => $billingEntry->getAmount()
                ];
            }

            $billEntries['PAY_A'.$i] = $entry;
            $i++;
        }

    }

    public function removeBillingEntries($idCustomer) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $billRecords = $em->getRepository('AppDatabaseMainBundle:BillingPaymentAcquisiteur')->findByIdCustomer($idCustomer, array('id' => 'ASC'));
        if (empty($billRecords)) {
            return false;
        }

        foreach ($billRecords as $billRecord) {
            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:BillingPaymentAcquisiteurEntry', 'p')
                ->andWhere('p.idBillingPaymentAcquisiteur = '.$billRecord->getId())
                ->getQuery()
                ->execute();
        }
        $em->remove($billRecord);
        $em->flush();
    }
}
