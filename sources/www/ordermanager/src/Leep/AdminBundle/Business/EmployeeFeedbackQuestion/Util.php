<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;
         
class Util {
    public static function parseChoices($data) {
        $choices = array();

        $rows = explode("\n", $data);
        foreach ($rows as $r) {
            $a = explode('|', $r);
            $key = $a[0];
            $value = isset($a[1]) ? $a[1] : $a[0];

            $choices[trim($key)] = trim($value);
        }

        return $choices;
    }

    public static function loadEmployeeFeedbackQuestions($container) {
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findAll();
        $questions = array();
        foreach ($result as $r) {
            $questions[] = $r->getQuestion();
        }
        return $questions;
    }

    public static function updateStats($container) {
        $em = $container->get('doctrine')->getEntityManager();
        
        $numResponses = array();
        $query = $em->createQueryBuilder();
        $query->select('p.idEmployeeFeedbackQuestion, COUNT(p) total')
            ->from('AppDatabaseMainBundle:EmployeeFeedbackResponse', 'p')
            ->groupBy('p.idEmployeeFeedbackQuestion');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $numResponses[$r['idEmployeeFeedbackQuestion']] = intval($r['total']);
        }

        $questions = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion')->findAll();
        foreach ($questions as $question) {
            $idEmployeeFeedbackQuestion = $question->getId();
            $question->setNumResponses(isset($numResponses[$idEmployeeFeedbackQuestion]) ? $numResponses[$idEmployeeFeedbackQuestion] : 0);

            $stats = array();
            switch ($question->getIdType()) {
                case Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE:
                    $responses = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackResponse')->findByIdEmployeeFeedbackQuestion($idEmployeeFeedbackQuestion);
                    foreach ($responses as $response) {
                        $answer = json_decode($response->getAnswer());
                        $answer = intval($answer);
                        if (!isset($stats[$answer])) {
                            $stats[$answer] = 0;
                        }
                        $stats[$answer]++;
                    }
                    break;
                
                case Constant::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE:
                    $responses = $em->getRepository('AppDatabaseMainBundle:EmployeeFeedbackResponse')->findByIdEmployeeFeedbackQuestion($idEmployeeFeedbackQuestion);
                    foreach ($responses as $response) {
                        $answers = json_decode($response->getAnswer(), true);
                        foreach ($answers as $answer) {
                            if (!isset($stats[$answer])) {
                                $stats[$answer] = 0;
                            }
                            $stats[$answer]++;
                        }
                    }
                    break;
            }
            $question->setSummary(json_encode($stats));
        }
        $em->flush();
    }

    public static function computeStats($container, $idEmployeeFeedbackQuestion, $choices, $dateFrom, $dateTo, $isOverall = false) {
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Date');

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:EmployeeFeedbackResponse', 'p')
            ->andWhere('p.idEmployeeFeedbackQuestion = :idEmployeeFeedbackQuestion')
            ->setParameter('idEmployeeFeedbackQuestion', $idEmployeeFeedbackQuestion);
        if (!$isOverall) {
            if (!empty($dateFrom) && !empty($dateTo)) {
                $query->andWhere('DATE(p.responseTime) >= DATE(:dateFrom)')
                    ->andWhere('DATE(p.responseTime) <= DATE(:dateTo)')
                    ->setParameter('dateFrom', $dateFrom)
                    ->setParameter('dateTo', $dateTo);
            }
            else {
                return false;
            }
        }
        $query->orderBy('p.responseTime', 'DESC');
        $responses = $query->getQuery()->getResult();

        $stats = array();
        foreach ($choices as $k => $v) {
            $stats[$k] = array('total' => 0, 'percentage' => 0);
        }


        $userResponses = array();
        $totalScore = 0;
        $numResponses = 0;
        $numScore = 0;
        foreach ($responses as $response) {
            $answer = json_decode($response->getAnswer());
            if (is_array($answer)) {
                $answers = $answer;
                foreach ($answers as $a) {
                    $needToCountScore = false;
                    if (!in_array($response->getIdUser(), $userResponses)) {
                        $userResponses[] = $response->getIdUser();
                        $needToCountScore = true;
                        $stats[$a]['total']++;
                        $numResponses++;       
                    }
                }         
            }
            else {
                if (isset($stats[$answer])) {
                    $needToCountScore = false;
                    if (!in_array($response->getIdUser(), $userResponses)) {
                        $userResponses[] = $response->getIdUser();
                        $needToCountScore = true;
                        $stats[$answer]['total']++;
                    }

                    $numResponses++;
                    if ($answer != 99 && $needToCountScore) {
                        $numScore++;
                        $totalScore+=$answer;
                    }
                }      
            }      
        }

        foreach ($stats as $k => $v) {
            $stats[$k]['percentage'] = ($numResponses == 0) ? 0 : number_format($v['total'] * 100 / $numResponses, 0);
        }

        $averageScore = ($numScore == 0) ? 0 : ($totalScore / $numScore);
        $roundedScore = round($averageScore);
        $averageValue = isset($choices[$roundedScore]) ? $choices[$roundedScore] : 'Unknown';

        return array('stats' => $stats, 'averageScore' => $averageScore, 'averageValue' => $averageValue);
    }
}