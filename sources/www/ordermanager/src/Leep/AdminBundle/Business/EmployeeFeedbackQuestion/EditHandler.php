<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmployeeFeedbackQuestion', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->idType = $entity->getIdType();
        $model->question = $entity->getQuestion();
        $model->data = $entity->getData();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idType', 'choice', array(
            'label' => 'Type',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmployeeFeedbackQuestion_Type')
        ));        
        $builder->add('question', 'textarea', array(
            'label' => 'Question',
            'required' => false,
            'attr'    => array(
                'rows'  => 5, 'cols' => 70
            )
        ));        
        $builder->add('data', 'textarea', array(
            'label' => 'Choice(s)',
            'required' => false,
            'attr'    => array(
                'rows'  => 5, 'cols' => 70
            )
        ));    

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setIdType($model->idType);
        $this->entity->setQuestion($model->question);
        $this->entity->setData($model->data);

        parent::onSuccess();
    }
}