<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('question', 'idType', 'numResponses', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Question',            'width' => '40%', 'sortable' => 'true'),
            array('title' => 'Type',                'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Responses',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',              'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmployeeFeedbackQuestionGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmployeeFeedbackQuestion', 'p');
        if (!empty($this->filters->idType)) {
            $queryBuilder->andWhere('p.idType = :idType')
                ->setParameter('idType', $this->filters->idType);
        }
        if (trim($this->filters->question) != '') {
            $queryBuilder->andWhere('p.question LIKE :question')
                ->setParameter('question', '%'.trim($this->filters->question).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('employeeFeedbackQuestionModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'employee_feedback_question', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'employee_feedback_question', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
            
        return $builder->getHtml();
    }

    public function buildCellNumResponses($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('employeeFeedbackResponseModify')) {
            $builder->addPopupButton('View response', $mgr->getUrl('leep_admin', 'employee_feedback_response', 'grid', 'list', array('id' => $row->getId())), 'button-detail');

            $em = $this->container->get('doctrine')->getEntityManager();
            $query = $em->createQueryBuilder();
            $query->select('COUNT(p)')
                ->from('AppDatabaseMainBundle:EmployeeFeedbackResponse', 'p')
                ->andWhere('p.idEmployeeFeedbackQuestion = :idEmployeeFeedbackQuestion')
                ->setParameter('idEmployeeFeedbackQuestion', $row->getId());
            $numResponses = intval($query->getQuery()->getSingleScalarResult());

            return intval($numResponses).' response(s)&nbsp;&nbsp;&nbsp; '.$builder->getHtml();
        }
        return '';
    }
}