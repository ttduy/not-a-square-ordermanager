<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idType', 'choice', array(
            'label' => 'Type',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmployeeFeedbackQuestion_Type')
        ));        
        $builder->add('question', 'textarea', array(
            'label' => 'Question',
            'required' => false,
            'attr'    => array(
                'rows'  => 5, 'cols' => 70
            )
        ));        
        $builder->add('data', 'textarea', array(
            'label' => 'Choice(s)',
            'required' => false,
            'attr'    => array(
                'rows'  => 5, 'cols' => 70
            )
        ));        
        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $employeeFeedbackQuestion = new Entity\EmployeeFeedbackQuestion();
        $employeeFeedbackQuestion->setIdType($model->idType);
        $employeeFeedbackQuestion->setQuestion($model->question);
        $employeeFeedbackQuestion->setData($model->data);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($employeeFeedbackQuestion);
        $em->flush();
        
        parent::onSuccess();
    }
}