<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;

use Easy\ModuleBundle\Module\AbstractHook;

class GridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $helperSecurity = $controller->get('leep_admin.helper.security');
        $data['canModifyResponse'] = $helperSecurity->hasPermission('employeeFeedbackResponseModify');
        $data['canModifyEmployeeAnswer'] = $helperSecurity->hasPermission('employeeAnswerModify');
    }
}
