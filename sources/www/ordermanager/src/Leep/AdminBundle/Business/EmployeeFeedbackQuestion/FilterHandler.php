<?php
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_EmployeeFeedbackQuestion_Type')
        ));
        $builder->add('question', 'text', array(
            'label'    => 'Question',
            'required' => false
        ));

        return $builder;
    }
}