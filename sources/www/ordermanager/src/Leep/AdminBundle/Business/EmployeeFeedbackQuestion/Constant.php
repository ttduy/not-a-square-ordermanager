<?php 
namespace Leep\AdminBundle\Business\EmployeeFeedbackQuestion;

class Constant {
    const EMPLOYEE_FEEDBACK_QUESTION_TYPE_TEXT                = 1;
    const EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE              = 2;
    const EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE     = 3;

    public static function getEmployeeFeedbackQuestionTypes() {
        return array(
            self::EMPLOYEE_FEEDBACK_QUESTION_TYPE_TEXT              => 'Text',
            self::EMPLOYEE_FEEDBACK_QUESTION_TYPE_CHOICE            => 'Single choice',
            self::EMPLOYEE_FEEDBACK_QUESTION_TYPE_MULTIPLE_CHOICE   => 'Multiple choice'
        );
    }
}