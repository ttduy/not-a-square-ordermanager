<?php
namespace Leep\AdminBundle\Business\IpLog;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('logTime', 'ipAddress');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Time',         'width' => '45%', 'sortable' => 'false'),
            array('title' => 'IP Address',   'width' => '50%', 'sortable' => 'false')   
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_IpLogGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.ipAddress, p.id, p.logTime')
            ->from('AppDatabaseMainBundle:IpLog', 'p')
            ->groupBy('p.ipAddress');
        if (trim($this->filters->ipAddress) != '')  {
            $queryBuilder->andWhere('p.ipAddress LIKE :ipAddress')
                ->setParameter('ipAddress', '%'.trim($this->filters->ipAddress).'%');
        }
    }
    
    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.logTime', 'DESC');
    }    
}
