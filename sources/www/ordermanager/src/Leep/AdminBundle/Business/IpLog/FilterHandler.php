<?php
namespace Leep\AdminBundle\Business\IpLog;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('ipAddress', 'text', array(
            'label'    => 'IP Address',
            'required' => false
        ));
        return $builder;
    }
}