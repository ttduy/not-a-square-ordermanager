<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

class EditModel {
    public $idQueue;
    public $idEmailMethod;
    public $recipients;
    public $title;
    public $body;
    public $attachments;
    public $isSendNow;
    public $status;
    public $cc;
    public $isAutoRetry;
    public $description;
}
