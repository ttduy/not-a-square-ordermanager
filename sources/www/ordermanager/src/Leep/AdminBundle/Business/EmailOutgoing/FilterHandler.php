<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idQueue', 'choice', array(
            'label'    => 'Queue',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Queue')
        ));

        $builder->add('idEmailMethod', 'choice', array(
            'label'    => 'Email Method',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Status')
        ));

        $builder->add('recipients', 'text', array(
            'label'    => 'Recipient(s)',
            'required' => false
        ));

        $builder->add('massSender', 'choice', array(
            'label'    => 'Mass sender (#id)',
            'required' => false,
            'choices'  => $this->getMassSenderList($mapping)
        ));

        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr' => array(
                'cols' => 60, 'rows' => 2
            )
        ));

        return $builder;
    }

    public function getMassSenderList($mapping) {
        $massSenderList = $mapping->getMapping('LeepAdmin_EmailMassSender_List');
        $result = array();
        foreach ($massSenderList as $id => $title) {
            $result[$id] = "$title (#$id)";
        }

        return $result;
    }
}
