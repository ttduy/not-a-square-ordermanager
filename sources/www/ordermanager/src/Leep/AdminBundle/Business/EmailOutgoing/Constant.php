<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

class Constant {
    const EMAIL_ATTACHMENT_FOLDER = 'email';

    ## Email Queue
    const EMAIL_QUEUE_NOTIFICATION =        10;
    const EMAIL_QUEUE_MASS_EMAIL =          20;
    const EMAIL_QUEUE_WEB_LOGIN_DC =        30;
    const EMAIL_QUEUE_WEB_LOGIN_PN =        40;
    const EMAIL_QUEUE_AUTO_WEBLOGIN =       50;
    const EMAIL_QUEUE_DEMO_TEST =           60;
    const EMAIL_QUEUE_PRICING_DONE =        70;
    const EMAIL_QUEUE_CMS_ORDER =           80;
    const EMAIL_QUEUE_CUSTOMER_DASHBOARD =  90;
    const EMAIL_QUEUE_OTHER =               9999;
    public static function getQueues() {
        return array(
            self::EMAIL_QUEUE_NOTIFICATION =>       'Notification',
            self::EMAIL_QUEUE_MASS_EMAIL =>         'Mass Email',
            self::EMAIL_QUEUE_WEB_LOGIN_DC =>       'Weblogin - DC',
            self::EMAIL_QUEUE_WEB_LOGIN_PN =>       'Weblogin - Partner',
            self::EMAIL_QUEUE_AUTO_WEBLOGIN =>      'Auto Weblogin',
            self::EMAIL_QUEUE_DEMO_TEST =>          'Demo Test',
            self::EMAIL_QUEUE_PRICING_DONE =>       'Notification bill pricing done',
            self::EMAIL_QUEUE_CMS_ORDER =>          'Email CMS Order',
            self::EMAIL_QUEUE_CUSTOMER_DASHBOARD => 'Customer Dashboard',
            self::EMAIL_QUEUE_OTHER =>              'Other'
        );
    }

    public static function getWebLoginQueues() {
        return [
            self::EMAIL_QUEUE_WEB_LOGIN_DC,
            self::EMAIL_QUEUE_WEB_LOGIN_PN,
            self::EMAIL_QUEUE_AUTO_WEBLOGIN
        ];
    }

    ## Email Status
    const EMAIL_STATUS_OPEN          = 10;
    const EMAIL_STATUS_SENDING       = 20;
    const EMAIL_STATUS_SENT          = 30;
    const EMAIL_STATUS_ERROR         = 9999;
    public static function getStatus() {
        return array(
            self::EMAIL_STATUS_OPEN             => 'Open',
            self::EMAIL_STATUS_SENDING          => 'Sending',
            self::EMAIL_STATUS_SENT             => 'Sent',
            self::EMAIL_STATUS_ERROR            => 'Error'
        );
    }

    const BULK_SEND_TYPE_ALL =                  10;
    const BULK_SEND_TYPE_OPEN_ONLY =            20;
    const BULK_SEND_TYPE_ERROR_ONLY =           30;
    public static function getBulkSendingTypes() {
        return array(
            self::BULK_SEND_TYPE_ALL                    => 'All',
            self::BULK_SEND_TYPE_OPEN_ONLY              => 'Open only',
            self::BULK_SEND_TYPE_ERROR_ONLY             => 'Error only',
        );
    }
}
