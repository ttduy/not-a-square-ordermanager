<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

class FilterModel {
    public $idQueue;
    public $idEmailMethod;
    public $status;
    public $recipients;
    public $massSender;
    public $description;
}