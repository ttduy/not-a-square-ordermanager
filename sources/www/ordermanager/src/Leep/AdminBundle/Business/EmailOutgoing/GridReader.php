<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('creationDatetime', 'idQueue', 'idEmailMethod', 'email', 'description', 'isAutoRetry', 'isSendNow', 'status', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Creation Datetime', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Queue', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Email Method', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Email', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Description', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Auto retry on?', 'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Send now?', 'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Status', 'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Action', 'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_EmailOutgoingGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:EmailOutgoing', 'p');

        if (!empty($this->filters->idQueue)) {
            $queryBuilder->andWhere('p.idQueue = :idQueue')
                ->setParameter('idQueue', $this->filters->idQueue);
        }

        if (!empty($this->filters->idEmailMethod)) {
            $queryBuilder->andWhere('p.idEmailMethod = :idEmailMethod')
                ->setParameter('idEmailMethod', $this->filters->idEmailMethod);
        }

        if (!empty($this->filters->status)) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }

        if (trim($this->filters->recipients) != '') {
            $queryBuilder->andWhere('p.recipients LIKE :recipients')
                ->setParameter('recipients', '%'.trim($this->filters->recipients).'%');
        }

        if (!empty($this->filters->massSender)) {
            $queryBuilder->andWhere('p.idMassSender = :idMassSender')
                ->setParameter('idMassSender', intval($this->filters->massSender));
        }

        if (!empty($this->filters->description)) {
            $queryBuilder->andWhere('p.description LIKE :description')
                ->setParameter('description', '%'.$this->filters->description.'%');
        }
    }
    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.creationDatetime', 'DESC');
    }

    public function buildCellIsSendNow($row) {
        return $row->getIsSendNow() ? "Yes" : "";
    }

    public function buildCellDescription($row) {
        $text = str_replace("\n", "<br/>", $row->getDescription());
        return $text;
    }

    public function buildCellIsAutoRetry($row) {
        return $row->getIsAutoRetry() ? "Yes" : "";
    }

    public function buildCellEmail($row) {
        $recipients = $row->getRecipients();
        $recipients = str_replace('<', '&lt', $recipients);
        $recipients = str_replace('>', '&gt', $recipients);
        return sprintf("%s<br/>To: %s", $row->getTitle(), $recipients);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('emailOutgoingModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'email_outgoing', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'email_outgoing', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'email_outgoing', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        return $builder->getHtml();
    }
}
