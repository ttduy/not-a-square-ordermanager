<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Leep\AdminBundle\Business\EmailMassSender\Util;

class BulkProcessHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }

    public function convertToFormModel($entity) {
        $model = new BulkProcessModel();
        $model->selectedEmailList = $this->container->get('request')->get('selected', '');
        $model->isSendNow = true;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedEmailList', 'hidden');

        $builder->add('isSendNow', 'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));

        $builder->add('isAutoRetry', 'checkbox', array(
            'label'    => 'Auto retry when fail?',
            'required' => false
        ));

        $builder->add('sendingType', 'choice', array(
            'label'    => 'Send to',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_BulkSendType')
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $selectedEmailList =    array_map('intval',
                                array_map('trim',
                                    explode(',', $model->selectedEmailList)));

        $query = $em->createQueryBuilder();
        $emailList = $query->select('p')
            ->from('AppDatabaseMainBundle:EmailOutgoing', 'p')
            ->where('p.id in (:emailList)')
            ->setParameter('emailList', $selectedEmailList)
            ->getQuery()
            ->execute();

        $massSenderIdList = [];
        foreach ($emailList as $email) {
            if ($email->getStatus() == Constant::EMAIL_STATUS_SENDING ||
                ($model->sendingType == Constant::BULK_SEND_TYPE_OPEN_ONLY && $email->getStatus() != Constant::EMAIL_STATUS_OPEN) ||
                ($model->sendingType == Constant::BULK_SEND_TYPE_ERROR_ONLY && $email->getStatus() != Constant::EMAIL_STATUS_ERROR)) {
                continue;
            }

            $email->setStatus(Constant::EMAIL_STATUS_OPEN);
            $email->setIsSendNow($model->isSendNow);
            $email->setIsAutoRetry($model->isAutoRetry);
            $em->persist($email);
            $em->flush();

            $massSenderId = $email->getIdMassSender();
            if ($massSenderId && !in_array($massSenderId, $massSenderIdList)) {
                $massSenderIdList[] = $email->getIdMassSender();
            }
        }

        foreach ($massSenderIdList as $massSenderId) {
            $massSender = $em->getRepository('AppDatabaseMainBundle:EmailMassSender')->findOneById($massSenderId);
            Util::updateMassSenderStatus($em, $massSender);
        }

        $this->messages = array('Done!');
    }
}
