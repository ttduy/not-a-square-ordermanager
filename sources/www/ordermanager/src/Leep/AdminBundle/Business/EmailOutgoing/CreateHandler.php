<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionEmail', 'section', array(
            'label'    => 'Email',
            'property_path' => false
        ));

        $builder->add('idQueue', 'choice', array(
            'label'    => 'Queue',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Queue')
        ));

        $builder->add('idEmailMethod', 'choice', array(
            'label'    => 'Email Method',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('recipients', 'text', array(
            'label'    => 'Recipient(s)',
            'required' => false
        ));

        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));

        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('body', 'app_text_editor', array(
            'label'    => 'Content',
            'required' => false
        ));

        $builder->add('isAutoRetry', 'checkbox', array(
            'label'    => 'Auto retry when fail?',
            'required' => false
        ));

        $builder->add('isSendNow', 'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));

        $builder->add('description', 'textarea', array(
            'label'    => 'Desciption',
            'required' => false,
            'attr' => array(
                'cols' => 80, 'rows' => 6
            )
        ));

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment(s)',
            'property_path' => false
        ));

        $builder->add('attachments', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_file_uploader'),
            'label' => 'Attachments',
            'required' => false,
            'options'  => array(
                'path'     => Constant::EMAIL_ATTACHMENT_FOLDER
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $emailOutgoing = new Entity\EmailOutgoing();
        $emailOutgoing->setCreationDatetime(new \DateTime());
        $emailOutgoing->setIdQueue($model->idQueue);
        $emailOutgoing->setIdEmailMethod($model->idEmailMethod);
        $emailOutgoing->setRecipients($model->recipients);
        $emailOutgoing->setCc($model->cc);
        $emailOutgoing->setTitle($model->title);
        $emailOutgoing->setBody($model->body);
        $emailOutgoing->setIsSendNow($model->isSendNow);
        $emailOutgoing->setStatus(Constant::EMAIL_STATUS_OPEN);
        $emailOutgoing->setIsAutoRetry($model->isAutoRetry);
        $emailOutgoing->setDescription($model->description);
        $em->persist($emailOutgoing);
        $em->flush();

        // attachments
        if ($model->attachments) {
            foreach ($model->attachments as $fileUpload) {
                if ($fileUpload['attachment']) {
                    $attachmentDir = $this->container->getParameter('files_dir') . '/'. Constant::EMAIL_ATTACHMENT_FOLDER;
                    $extension = pathinfo($fileUpload['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);

                    // email pool attachment
                    $emailOutgoingAttachment = new Entity\EmailOutgoingAttachment();
                    $emailOutgoingAttachment->setIdEmailOutgoing($emailOutgoing->getId());
                    $em->persist($emailOutgoingAttachment);
                    $em->flush();

                    $name = $emailOutgoingAttachment->getId() . ($extension ? '.' . $extension : '');
                    $fileUpload['attachment']->move($attachmentDir, $name);

                    $emailOutgoingAttachment->setName($name);
                    $emailOutgoingAttachment->setOriginalName($fileUpload['attachment']->getClientOriginalName());
                    $em->flush();
                }
            }
        }

        parent::onSuccess();
    }
}
