<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\EmailWeblogin\Constant as WebloginConstant;

class Util {
    static public function createOutgoingEmail($container, $emailAddress, $data, $idQueue, $emailStatus, $emailMethodId, $sendNow=false, $autoRetry=false, $descriptions='') {
        $em = $container->get('doctrine')->getEntityManager();
        // Create Email Outgoing
        $email = new Entity\EmailOutgoing();
        $email->setTitle($data['subject']);
        $email->setBody($data['content']);
        $email->setRecipients($emailAddress);
        $email->setIdQueue($idQueue);
        $email->setIdEmailMethod($emailMethodId);
        $email->setStatus($emailStatus);
        $email->setCreationDatetime(new \DateTime());
        $email->setRecipientsType($data['recipe']['type']);
        $email->setRecipientsId($data['recipe']['id']);
        $email->setIsSendNow($sendNow);
        $email->setIsAutoRetry($autoRetry);
        $email->setDescription($descriptions);
        $em->persist($email);
        $em->flush();
    }

    static public function getPricingEmailDataFromTemplate($container, $arrayData) {
        $em = $container->get('doctrine')->getEntityManager();
        $result = [];
        $formatter = $container->get('leep_admin.helper.formatter');
        $mapping = $container->get('easy_mapping');

        // Twig environment is used to build template parameter
        $emailTemplateId = $arrayData['idTemplate'];
        $env = new \Twig_Environment(new \Twig_Loader_String(), ['autoescape' => false]);
        $helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $helperCommon = $container->get('leep_admin.helper.common');
        $emailTemplate = $em->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($emailTemplateId);
        $data['order'] = $arrayData['orderInfo'];

        // Render twig content
        $subject = $env->render($emailTemplate->getSubject(), $data);
        $content = $env->render($emailTemplate->getContentWithParagraph(), $data);

        // Add text block (Using multilanguage setting)
        // $language = Business\CustomerInfo\Utils::getCustomerLanguage($container, $customer->getId());
        $subject = Business\EmailTemplate\Utils::applyTextBlocks($container, $subject, 1);
        $content = Business\EmailTemplate\Utils::applyTextBlocks($container, $content, 1);
        $result = [
            'content' => $content,
            'subject' => $subject,
            'recipe'    => [
                'type'      => '',
                'id'        => 0
            ]
        ];

        return $result;
    }

    static public function getWebloginEmailDataFromTemplate($container, $orderId, $emailTemplateId) {
        $em = $container->get('doctrine')->getEntityManager();
        $results = [];
        $formatter = $container->get('leep_admin.helper.formatter');
        $mapping = $container->get('easy_mapping');

        // Twig environment is used to build template parameter
        $env = new \Twig_Environment(new \Twig_Loader_String(), ['autoescape' => false]);

        $helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $helperCommon = $container->get('leep_admin.helper.common');

        $webLoginOptOutUrl = $container->getParameter('application_url').'/web_user/feature/webLoginOptOut?';

        $emailTemplate = $em->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($emailTemplateId);
        $canSendToCustomer = $emailTemplate->getToCustomer();
        $canSendToPartner = $emailTemplate->getToPartner();
        $canSendToDistributionChannel = $emailTemplate->getToDistributionChannel();

        $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($orderId);
        $result = [];
        if ($order) {
            $customer = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());
            if (!$customer || $customer->getIsWebLoginOptOut()) {
                return null;
            }

            // Can send to customer?
            if (in_array($customer->getWebLoginGoesTo(), WebloginConstant::getReportGoesToGroup('customer')) && $canSendToCustomer) {
                $emailAddress = trim($order->getEmail());
                if (empty($emailAddress)) {
                    $emailAddress = trim($customer->getEmail());
                }

                if (!empty($emailAddress)) {
                    $result[] = [
                        'emailAddress' =>   $emailAddress,
                        'username' =>       $customer->getWebLoginUsername(),
                        'password' =>       $customer->getWebLoginPassword(),
                        'language' =>       $order->getLanguageid() ? $order->getLanguageid() : 1,
                        'recipe' => [
                            'type' =>       Business\EmailMassSender\Constant::RECEIVER_TYPE_CUSTOMER,
                            'id' =>         $order->getId()
                        ]
                    ];
                }
            }

            // Can send to DC?
            $distributionChannel = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getIdDistributionChannel());
            if ($distributionChannel && in_array($customer->getWebLoginGoesTo(), WebloginConstant::getReportGoesToGroup('dc')) && $canSendToDistributionChannel) {
                $dcEmail = trim($distributionChannel->getContactemail());
                if (!empty($dcEmail)) {
                    $result[] = [
                        'emailAddress' =>   $dcEmail,
                        'username' =>       $distributionChannel->getWebLoginUsername(),
                        'password' =>       $distributionChannel->getWebLoginPassword(),
                        'language' =>       $distributionChannel->getIdLanguage() ? $distributionChannel->getIdLanguage() : 1,
                        'recipe' => [
                            'type' =>       Business\EmailMassSender\Constant::RECEIVER_TYPE_DISTRIBUTION_CHANNEL,
                            'id' =>         $distributionChannel->getId()
                        ]
                    ];
                }
            }

            $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($distributionChannel->getPartnerid());
            if ($partner && in_array($customer->getWebLoginGoesTo(), WebloginConstant::getReportGoesToGroup('partner')) && $canSendToPartner) {
                $partnerEmail = trim($partner->getContactemail());
                if (!empty($partnerEmail)) {
                    $result[] = [
                        'emailAddress' =>   $partnerEmail,
                        'username' =>       $partner->getWebLoginUsername(),
                        'password' =>       $partner->getWebLoginPassword(),
                        'language' =>       $partner->getIdLanguage() ? $partner->getIdLanguage() : 1,
                        'recipe' => [
                            'type' =>       Business\EmailMassSender\Constant::RECEIVER_TYPE_PARTNER,
                            'id' =>         $partner->getId()
                        ]
                    ];
                }
            }

            // No matched email, quit!
            if (empty($result)) {
                return null;
            }

            // Get distribution channel information
            $distributionChannelData = '';
            if ($distributionChannel) {
                $distributionChannelData =
                    $distributionChannel->getFirstName().' '.$distributionChannel->getSurName().', '.$distributionChannel->getInstitution().', '.
                    $distributionChannel->getStreet().' '.$distributionChannel->getPostCode().' '.$distributionChannel->getCity().', '.$distributionChannel->getContactEmail();
            }

            // Get ordered products
            $orderedProducts = $em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($orderId);
            $productList = [];
            foreach ($orderedProducts as $p) {
                $productList[] = $formatter->format($p->getProductId(), 'mapping', 'LeepAdmin_Product_List');
            }

            // Ordered status
            $sampleArrivedDate = null;
            $sampleArrivedDateStatus = $em->getRepository('AppDatabaseMainBundle:OrderedStatus')->findOneBy([
                'customerid' => $orderId,
                'status' => 25 // Sample arrived
            ]);

            if ($sampleArrivedDateStatus) {
                $sampleArrivedDate = $sampleArrivedDateStatus->getStatusDate();
            }

            // Finallize email content
            foreach ($result as $key => $entry) {
                $language = $entry['language'];
                $webloginUserName = $entry['username'];
                $webloginPassword = $entry['password'];
                $emailAddress = $entry['emailAddress'];
                $recipientsType = $entry['recipe']['type'];
                $recipientsId = $entry['recipe']['id'];
                $arrParams = array(
                    'email' =>  $helperEncrypt->encrypt($emailAddress),
                    'type' =>   $helperEncrypt->encrypt($recipientsType),
                    'id' =>     $helperEncrypt->encrypt($recipientsId)
                );

                $webLoginOptOutUrl .= http_build_query($arrParams);

                // Genenral parameter for template
                $data = [
                    'orderNumber' =>            $order->getOrderNumber(),
                    'externalBarcode' =>        $customer->getExternalBarcode(),
                    'orderDate' =>              $formatter->format($order->getDateOrdered(), 'date'),
                    'name' =>                   $order->getFirstName().' '.$order->getSurName(),
                    'address' =>                $order->getStreet().' '.$order->getPostCode().' '.$order->getCity().', '.
                                                    $formatter->format($order->getCountryId(), 'mapping', 'LeepAdmin_Country_List'),
                    'productList' =>            implode(', ', $productList),
                    'distributionChannel' =>    $distributionChannelData,
                    'sampleArrivedDate' =>      empty($sampleArrivedDate) ? "(unknown)" : $sampleArrivedDate->format('d/m/Y'),
                    'reportLink'  =>            $container->getParameter('public_report_link').'?key='.$order->getShipmentKey(),
                    'reportAccessCode'  =>      $order->getShipmentCode(),
                    'feedbackQuestionLink' =>   $container->getParameter('application_url').'/feedback_form',
                    'DNCREPORTTYPE'  =>         $formatter->format($order->getIdDncReportType(), 'mapping', 'LeepAdmin_Report_DncReportType'),
                    'usernamelogin' =>          $webloginUserName,
                    'passwordlogin' =>          $webloginPassword,
                    'OptOutLink' =>             $webLoginOptOutUrl,
                    'OPTOUTLINK' =>             $webLoginOptOutUrl,
                    'OPTOUT_WEBLOGIN' =>        $webLoginOptOutUrl
                ];

                // Render twig content
                $subject = $env->render($emailTemplate->getSubject(), $data);
                $content = $env->render($emailTemplate->getContentWithParagraph(), $data);

                // Add text block (Using multilanguage setting)
                $result[$key]['subject'] = Business\EmailTemplate\Utils::applyTextBlocks($container, $subject, $language);
                $result[$key]['content'] = Business\EmailTemplate\Utils::applyTextBlocks($container, $content, $language);
            }
        }

        return $result;
    }
}
