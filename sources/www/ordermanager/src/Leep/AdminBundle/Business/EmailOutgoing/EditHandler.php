<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailOutgoing', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = new EditModel();

        $model->idQueue = $entity->getIdQueue();
        $model->idEmailMethod = $entity->getIdEmailMethod();
        $model->recipients = $entity->getRecipients();
        $model->title = $entity->getTitle();
        $model->body = $entity->getBody();
        $model->isSendNow = $entity->getIsSendNow();
        $model->status = $entity->getStatus();
        $model->cc = $entity->getCc();
        $model->attachments = array();
        $model->description = $entity->getDescription();

        $attachments = $em->getRepository('AppDatabaseMainBundle:EmailOutgoingAttachment')->findByIdEmailOutgoing($entity->getId());
        foreach ($attachments as $a) {
            $model->attachments[] = array(
                'idFile'       => $a->getId(),
                'originalName' => $a->getOriginalName(),
                'file'         => $a->getName()
            );
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionEmail', 'section', array(
            'label'    => 'Email',
            'property_path' => false
        ));

        $builder->add('idQueue', 'choice', array(
            'label'    => 'Queue',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Queue')
        ));

        $builder->add('idEmailMethod', 'choice', array(
            'label'    => 'Email Method',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailMethod_List')
        ));

        $builder->add('recipients', 'text', array(
            'label'    => 'Recipient(s)',
            'required' => false
        ));

        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));

        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('body', 'app_text_editor', array(
            'label'    => 'Content',
            'required' => false
        ));

        $builder->add('isAutoRetry', 'checkbox', array(
            'label'    => 'Auto retry when fail?',
            'required' => false
        ));

        $builder->add('isSendNow', 'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailOutgoing_Status')
        ));

        $builder->add('description', 'textarea', array(
            'label'    => 'Desciption',
            'required' => false,
            'attr' => array(
                'cols' => 80, 'rows' => 6
            )
        ));

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment(s)',
            'property_path' => false
        ));

        $builder->add('attachments', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_file_uploader'),
            'label' => 'Attachments',
            'required' => false,
            'options'  => array(
                'path'     => Constant::EMAIL_ATTACHMENT_FOLDER
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $this->entity->setIdQueue($model->idQueue);
        $this->entity->setIdEmailMethod($model->idEmailMethod);
        $this->entity->setRecipients($model->recipients);
        $this->entity->setCc($model->cc);
        $this->entity->setTitle($model->title);
        $this->entity->setBody($model->body);
        $this->entity->setIsSendNow($model->isSendNow);
        $this->entity->setStatus($model->status);
        $this->entity->setIsAutoRetry($model->isAutoRetry);
        $this->entity->setDescription($model->description);

        // Load existing attachments
        $files = array();
        $records = $em->getRepository('AppDatabaseMainBundle:EmailOutgoingAttachment')->findByIdEmailOutgoing($this->entity->getId());
        foreach ($records as $r) {
            $files[$r->getId()] = $r;
        }

        // Apply
        foreach ($model->attachments as $fileUpload) {
            $idFile = intval($fileUpload['idFile']);
            $attachment = $fileUpload['attachment'];

            if (empty($idFile)) {
                if ($attachment) {
                    $emailOutgoingAttachment = $this->upload($this->entity, $attachment);
                }
            }
            else {
                if ($attachment) {
                    $emailOutgoingAttachment = $this->upload($this->entity, $attachment);
                    $em->remove($files[$idFile]);
                    $em->flush();
                }
                else {
                    unset($files[$idFile]);
                }
            }
        }
        foreach ($files as $idFile => $file) {
            $em->remove($file);
            $em->flush();
        }

        $this->reloadForm();
        parent::onSuccess();
    }

    public function upload($emailOutgoing, $attachment) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $attachmentDir = $this->container->getParameter('files_dir') . '/'. Constant::EMAIL_ATTACHMENT_FOLDER;
        $extension = pathinfo($attachment->getClientOriginalName(), PATHINFO_EXTENSION);

        // email pool attachment
        $emailOutgoingAttachment = new Entity\EmailOutgoingAttachment();
        $emailOutgoingAttachment->setIdEmailOutgoing($emailOutgoing->getId());
        $em->persist($emailOutgoingAttachment);
        $em->flush();

        $name = $emailOutgoingAttachment->getId() . ($extension ? '.' . $extension : '');
        $attachment->move($attachmentDir, $name);
        $emailOutgoingAttachment->setName($name);
        $emailOutgoingAttachment->setOriginalName($attachment->getClientOriginalName());
        $em->flush();

        return $emailOutgoingAttachment;
    }
}
