<?php
namespace Leep\AdminBundle\Business\EmailOutgoing;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business\EmailOutgoing\Constant;

class BulkProcessHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $selectedEmailList = $controller->get('request')->get('selected', '');
        $data['selectedEmailList'] = $selectedEmailList;
        $selectedEmailList = explode(',', $selectedEmailList);

        $emailList = array();
        foreach ($selectedEmailList as $selectedId) {
            if (trim($selectedId) != '') {
                $email = $em->getRepository('AppDatabaseMainBundle:EmailOutgoing')->findOneById($selectedId);
                if ($email) {
                    $emailMethod = $em->getRepository('AppDatabaseMainBundle:EmailMethod')
                        ->findOneById($email->getIdEmailMethod());
                    if ($emailMethod)
                        $emailMethod = $emailMethod->getName();
                    else
                        $emailMethod = "N/A";

                    $status = '';
                    if (array_key_exists($email->getStatus(), Constant::getStatus())) {
                        $status = Constant::getStatus()[$email->getStatus()];
                    }

                    $queue = '';
                    if (array_key_exists($email->getIdQueue(), Constant::getQueues())) {
                        $queue = Constant::getQueues()[$email->getIdQueue()];
                    }

                    $emailList[] = array(
                        'timestamp' =>      $formatter->format($email->getCreationDatetime(), 'datetime'),
                        'queue'=>           $queue,
                        'emailMethod' =>    $emailMethod,
                        'title' =>          $email->getTitle(),
                        'recipients' =>     "To: ".$email->getRecipients(),
                        'showWarning' =>    $email->getStatus() == Constant::EMAIL_STATUS_SENT,
                        'status' =>         $status
                    );
                }
            }
        }

        $data['emailList'] = $emailList;
    }
}
