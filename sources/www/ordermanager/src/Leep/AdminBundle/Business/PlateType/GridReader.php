<?php
namespace Leep\AdminBundle\Business\PlateType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',       'width' => '75%', 'sortable' => 'false'),
            array('title' => 'Action',     'width' => '20%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:PlateType', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('plateTypeModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'plate_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'plate_type', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
