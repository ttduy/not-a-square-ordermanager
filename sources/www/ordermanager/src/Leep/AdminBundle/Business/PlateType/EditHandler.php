<?php
namespace Leep\AdminBundle\Business\PlateType;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PlateType', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();  
        $model->name = $entity->getName();

        $model->plateTypeBox = array();
        $results = $em->getRepository('AppDatabaseMainBundle:PlateTypeWell')->findByIdPlateType($entity->getId());
        foreach ($results as $r) {
            $id = 'cell_'.$r->getRow().'_'.$r->getCol();

            $plateWellIndex = '';
            if ($r->getPlateWellIndex() != '') {
                $plateWellIndex = $r->getPlateWellIndex();
            }

            $idGene = '';
            if ($r->getIdGene() != '') {
                $idGene = $r->getIdGene();
            }

            $value = $plateWellIndex.'!!'.$idGene.'!!'.$r->getColor();
            $model->plateTypeBox[$id] = $value;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('sectionPlateTypeBox', 'section', array(
            'label' => 'Plate Type',
            'property_path' => false
        ));
        $builder->add('plateTypeBox', 'app_plate_type_box', array(
            'label'    => 'Plate box',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();      
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $model = $this->getForm()->getData();        
        
        $this->entity->setName($model->name);

        $filters = array('idPlateType' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:PlateTypeWell', $filters);
        foreach ($model->plateTypeBox as $wellId => $wellValue) {
            $arr = explode('_', $wellId);
            if (count($arr) == 3) {
                $row = intval($arr[1]);
                $col = intval($arr[2]);

                $arr = explode('!!', $wellValue);
                if ((count($arr) == 3) && ($arr[0] != '')) {
                    $plateWellIndex = $arr[0];
                    $idGene = $arr[1];
                    $color = $arr[2];

                    $well = new Entity\PlateTypeWell();
                    $well->setIdPlateType($this->entity->getId());
                    $well->setRow($row);
                    $well->setCol($col);
                    $well->setPlateWellIndex($plateWellIndex);
                    $well->setIdGene($idGene);
                    $well->setColor($color);
                    $em->persist($well);
                }
            }
        }
        $em->flush();

        parent::onSuccess();
    }
}
