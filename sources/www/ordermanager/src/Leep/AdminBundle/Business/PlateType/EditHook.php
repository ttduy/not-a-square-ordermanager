<?php
namespace Leep\AdminBundle\Business\PlateType;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EditHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');

        $data['geneList'] = $mapping->getMapping('LeepAdmin_Gene_List');
    }
}
