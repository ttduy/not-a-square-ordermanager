<?php
namespace Leep\AdminBundle\Business\PlateType\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPlateTypeBox extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_plate_type_box';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   

        $mapping = $this->container->get('easy_mapping');
        
        $geneList = $mapping->getMapping('LeepAdmin_Gene_List');
        asort($geneList);

        // to keep the order of Genes after using JSON.parse JS
        $newGeneList = array();
        foreach ($geneList as $id => $name) {
            $object = new \stdClass();
            $object->id = $id;
            $object->name = $name;
            $newGeneList[] = $object;
        }

        $view->vars['geneListJSON'] = json_encode($newGeneList);
        $view->vars['geneRefJSON'] = json_encode($geneList);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        for ($i = 0; $i < 16; $i++) {
            for ($j = 0; $j < 24; $j++) {
                $builder->add('cell_'.$i.'_'.$j, 'hidden', array(
                    'required' => false
                ));
            }
        }
    }
}