<?php
namespace Leep\AdminBundle\Business\CryosaveUser;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idLaboratory', 'choice', array(
            'label'       => 'Laboratory',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_CryosaveSample_Laboratory'),
            'empty_value' => false
        ));
        $builder->add('username', 'text', array(
            'label'       => 'Username',
            'required'    => false
        ));

        return $builder;
    }
}