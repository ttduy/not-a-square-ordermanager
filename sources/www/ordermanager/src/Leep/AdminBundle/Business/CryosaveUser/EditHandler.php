<?php
namespace Leep\AdminBundle\Business\CryosaveUser;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveUser', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->idLaboratory = $entity->getIdLaboratory();
        $model->username = $entity->getUsername();
        $model->password = $entity->getPassword();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idLaboratory', 'choice', array(
            'label'       => 'Laboratory',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_CryosaveSample_Laboratory')
        ));
        $builder->add('username', 'label', array(
            'label'    => 'Username',
            'required' => false
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        

        $this->entity->setIdLaboratory($model->idLaboratory);
        $this->entity->setPassword($model->password);

        $em->flush();

        parent::onSuccess();
    }
}
