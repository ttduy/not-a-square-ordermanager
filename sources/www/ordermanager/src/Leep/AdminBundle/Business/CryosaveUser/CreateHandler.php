<?php
namespace Leep\AdminBundle\Business\CryosaveUser;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->container = $this->container;
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idLaboratory', 'choice', array(
            'label'       => 'Laboratory',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_CryosaveSample_Laboratory')
        ));
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));

    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $cryosaveUser = new Entity\CryosaveUser();
        $cryosaveUser->setIdLaboratory($model->idLaboratory);
        $cryosaveUser->setUsername($model->username);
        $cryosaveUser->setPassword($model->password);
        $em->persist($cryosaveUser);
        $em->flush();

        parent::onSuccess();
    }
}
