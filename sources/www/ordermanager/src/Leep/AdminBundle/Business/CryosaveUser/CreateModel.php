<?php
namespace Leep\AdminBundle\Business\CryosaveUser;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;
    public $idLaboratory;
    public $username;
    public $password;

    public function isValid(ExecutionContext $context) {  
        $em = $this->container->get('doctrine')->getEntityManager();

        $common = $this->container->get('leep_admin.helper.common');
        if ($common->isUsernameExisted($this->username)) {
            $context->addViolationAtSubPath('username', "Username is already existed");                    
        }
    }
}
