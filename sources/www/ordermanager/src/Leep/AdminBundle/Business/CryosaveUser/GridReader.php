<?php
namespace Leep\AdminBundle\Business\CryosaveUser;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idLaboratory', 'username', 'password', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Laboratory', 'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Username',   'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Password',   'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',     'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CryosaveUserGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CryosaveUser', 'p');
        if (trim($this->filters->username) != '') {
            $queryBuilder->andWhere('p.username LIKE :username')
                ->setParameter('username', '%'.trim($this->filters->username).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cryosaveUserModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cryosave_user', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cryosave_user', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
