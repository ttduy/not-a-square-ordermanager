<?php
namespace Leep\AdminBundle\Business\Gene;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idProduct = 0;
        $model->idResultOption = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('rsNumber', 'text', array(
            'label'    => 'Rs Number',
            'required' => false
        ));
        $builder->add('idResultOption', 'choice', array(
            'label'    => 'Results Option',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Gene_ResultOptions'),
            'empty_value' => false
        ));
        $builder->add('isRelevant', 'checkbox', array(
            'label'    => 'Relevant',
            'required' => false
        ));
        $builder->add('isRecheck', 'checkbox', array(
            'label'    => 'Recheck',
            'required' => false
        ));
        
        return $builder;
    }
}