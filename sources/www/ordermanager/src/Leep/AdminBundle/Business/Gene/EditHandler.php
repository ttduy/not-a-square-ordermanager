<?php
namespace Leep\AdminBundle\Business\Gene;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Genes', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        
        $model->geneName = $entity->getGeneName();
        $model->scientificName = $entity->getScientificName();
        $model->rsNumber = $entity->getRsNumber();
        $model->idResultOption = $entity->getGroupID();
        $model->notes = $entity->getNotes();
        $model->isRelevant = $entity->getRelevant();
        $model->isRecheck = $entity->getRecheck();
        $model->wtBase = $entity->getWTBase();
        $model->mutBase = $entity->getMutBase();
        $model->oddsRatioWt = $entity->getOddsRatioWt();
        $model->oddsRatioHet = $entity->getOddsRatioHet();
        $model->oddsRatioMut = $entity->getOddsRatioMut();
        $model->reference1 = $entity->getReference1();
        $model->reference2 = $entity->getReference2();
        $model->reference3 = $entity->getReference3();

        $model->amplicon1 = $entity->getAmplicon1();
        $model->primerFwd1 = $entity->getPrimerFwd1();
        $model->primerFwdSeq1 = $entity->getPrimerFwdSeq1();
        $model->primerRev1 = $entity->getPrimerRev1();
        $model->primerRevSeq1 = $entity->getPrimerRevSeq1();

        $model->amplicon2 = $entity->getAmplicon2();
        $model->primerFwd2 = $entity->getPrimerFwd2();
        $model->primerFwdSeq2 = $entity->getPrimerFwdSeq2();
        $model->primerRev2 = $entity->getPrimerRev2();
        $model->primerRevSeq2 = $entity->getPrimerRevSeq2();

        $model->amplicon3 = $entity->getAmplicon3();
        $model->primerFwd3 = $entity->getPrimerFwd3();
        $model->primerFwdSeq3 = $entity->getPrimerFwdSeq3();
        $model->primerRev3 = $entity->getPrimerRev3();
        $model->primerRevSeq3 = $entity->getPrimerRevSeq3();
        

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('geneName', 'text', array(
            'label'    => 'Gene Name',
            'required' => true
        ));
        $builder->add('scientificName', 'text', array(
            'label'    => 'Scientific Name',
            'required' => false
        ));
        $builder->add('rsNumber', 'text', array(
            'label'    => 'Rs Number',
            'required' => false
        ));
        $builder->add('idResultOption', 'choice', array(
            'label'    => 'Results Option',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Gene_ResultOptions')
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false
        ));
        $builder->add('isRelevant', 'checkbox', array(
            'label'    => 'Relevant',
            'required' => false
        ));
        $builder->add('isRecheck', 'checkbox', array(
            'label'    => 'Recheck',
            'required' => false
        ));

        $builder->add('wtBase', 'text', array(
            'label'    => 'WT Base',
            'required' => false
        ));
        $builder->add('mutBase', 'text', array(
            'label'    => 'Mut Base',
            'required' => false
        ));
        $builder->add('oddsRatioWt', 'text', array(
            'label'    => 'Odds Ratio Wt',
            'required' => false
        ));
        $builder->add('oddsRatioHet', 'text', array(
            'label'    => 'Odds Ratio Het',
            'required' => false
        ));
        $builder->add('oddsRatioMut', 'text', array(
            'label'    => 'Odds Ratio Mut',
            'required' => false
        ));

        $builder->add('reference1', 'textarea', array(
            'label'    => 'Reference 1',
            'required' => false
        ));
        $builder->add('reference2', 'textarea', array(
            'label'    => 'Reference 2',
            'required' => false
        ));
        $builder->add('reference3', 'textarea', array(
            'label'    => 'Reference 3',
            'required' => false
        ));

        // AMPLICON 1
        $builder->add('section1', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 1'
        ));
        $builder->add('amplicon1', 'text', array(
            'label'    => 'Amplicon 1',
            'required' => false
        ));
        $builder->add('primerFwd1', 'text', array(
            'label'    => 'Primer 1 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq1', 'text', array(
            'label'    => '1 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev1', 'text', array(
            'label'    => 'Primer 1 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq1', 'text', array(
            'label'    => '1 Rev Sequence',
            'required' => false
        ));

        // AMPLICON 2
        $builder->add('section2', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 2'
        ));
        $builder->add('amplicon2', 'text', array(
            'label'    => 'Amplicon 2',
            'required' => false
        ));
        $builder->add('primerFwd2', 'text', array(
            'label'    => 'Primer 2 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq2', 'text', array(
            'label'    => '2 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev2', 'text', array(
            'label'    => 'Primer 2 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq2', 'text', array(
            'label'    => '2 Rev Sequence',
            'required' => false
        ));

        // AMPLICON 3
        $builder->add('section3', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 3'
        ));
        $builder->add('amplicon3', 'text', array(
            'label'    => 'Amplicon 3',
            'required' => false
        ));
        $builder->add('primerFwd3', 'text', array(
            'label'    => 'Primer 3 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq3', 'text', array(
            'label'    => '3 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev3', 'text', array(
            'label'    => 'Primer 3 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq3', 'text', array(
            'label'    => '3 Rev Sequence',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        


        $this->entity->setGeneName($model->geneName);
        $this->entity->setScientificName($model->scientificName);
        $this->entity->setRsNumber($model->rsNumber);
        $this->entity->setGroupID($model->idResultOption);
        $this->entity->setNotes($model->notes);
        $this->entity->setRelevant($model->isRelevant);
        $this->entity->setRecheck($model->isRecheck);
        $this->entity->setWTBase($model->wtBase);
        $this->entity->setMutBase($model->mutBase);
        $this->entity->setOddsRatioWt($model->oddsRatioWt);
        $this->entity->setOddsRatioHet($model->oddsRatioHet);
        $this->entity->setOddsRatioMut($model->oddsRatioMut);
        $this->entity->setReference1($model->reference1);
        $this->entity->setReference2($model->reference2);
        $this->entity->setReference3($model->reference3);
        
        $this->entity->setAmplicon1($model->amplicon1);
        $this->entity->setPrimerFwd1($model->primerFwd1);
        $this->entity->setPrimerFwdSeq1($model->primerFwdSeq1);
        $this->entity->setPrimerRev1($model->primerRev1);
        $this->entity->setPrimerRevSeq1($model->primerRevSeq1);

        $this->entity->setAmplicon2($model->amplicon2);
        $this->entity->setPrimerFwd2($model->primerFwd2);
        $this->entity->setPrimerFwdSeq2($model->primerFwdSeq2);
        $this->entity->setPrimerRev2($model->primerRev2);
        $this->entity->setPrimerRevSeq2($model->primerRevSeq2);

        $this->entity->setAmplicon3($model->amplicon3);
        $this->entity->setPrimerFwd3($model->primerFwd3);
        $this->entity->setPrimerFwdSeq3($model->primerFwdSeq3);
        $this->entity->setPrimerRev3($model->primerRev3);
        $this->entity->setPrimerRevSeq3($model->primerRevSeq3);

        parent::onSuccess();
    }
}
