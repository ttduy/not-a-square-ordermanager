<?php
namespace Leep\AdminBundle\Business\Gene;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('geneName', 'scientificName', 'rsNumber', 'groupID', 'relevant', 'recheck', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Gene Name',               'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Scientific Name',         'width' => '15%', 'sortable' => 'true'),
            array('title' => 'RS Number',               'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Result Option',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Relevant',                'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Recheck',                 'width' => '10%', 'sortable' => 'false'),            
            array('title' => 'Action',                  'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GeneGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Genes', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        
        if ($this->filters->idProduct != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:ProdGeneLink', 'pg', 'WITH', 'p.id = pg.geneid')
                ->andWhere('pg.productid = :productId')
                ->setParameter('productId', $this->filters->idProduct);
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere("p.genename LIKE :name")
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if ($this->filters->idResultOption != 0) {
            $queryBuilder->andWhere('p.groupid = :groupId')
                ->setParameter('groupId', $this->filters->idResultOption);
        }
        if (trim($this->filters->rsNumber) != '') {
            $queryBuilder->andWhere('p.rsnumber LIKE :rsNumber')
                ->setParameter('rsNumber', '%'.trim($this->filters->rsNumber).'%');
        }
        if ($this->filters->isRelevant == 1) {
            $queryBuilder->andWhere('p.relevant = :isRelevant')
                ->setParameter('isRelevant', 1);
        }
        if ($this->filters->isRecheck == 1) {
            $queryBuilder->andWhere('p.recheck = :isRecheck')
                ->setParameter('isRecheck', 1);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'gene', 'view', 'view', array('id' => $row->getId())), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'gene', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');        
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'gene', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellRelevant($row) { return $row->getRelevant() ? "Yes" : "No"; }
    public function buildCellRecheck($row) { return $row->getRecheck() ? "Yes" : "No"; }
}
