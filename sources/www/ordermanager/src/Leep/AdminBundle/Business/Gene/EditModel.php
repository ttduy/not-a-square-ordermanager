<?php
namespace Leep\AdminBundle\Business\Gene;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $geneName;
    public $scientificName;
    public $rsNumber;
    public $idResultOption;
    public $notes;
    public $isRelevant;
    public $isRecheck;
    public $wtBase;
    public $mutBase;
    public $oddsRatioWt;
    public $oddsRatioHet;
    public $oddsRatioMut;
    public $reference1;
    public $reference2;
    public $reference3;

    public $amplicon1;
    public $primerFwd1;
    public $primerFwdSeq1;
    public $primerRev1;
    public $primerRevSeq1;

    public $amplicon2;
    public $primerFwd2;
    public $primerFwdSeq2;
    public $primerRev2;
    public $primerRevSeq2;

    public $amplicon3;
    public $primerFwd3;
    public $primerFwdSeq3;
    public $primerRev3;
    public $primerRevSeq3;
}
