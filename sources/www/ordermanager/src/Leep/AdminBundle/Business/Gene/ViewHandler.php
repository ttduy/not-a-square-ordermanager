<?php
namespace Leep\AdminBundle\Business\Gene;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');


        $id = $this->container->get('request')->query->get('id', 0);
        $gene = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Genes')->findOneById($id);
        
        $data[] = $this->addSection('Gene');
        $data[] = $this->add('Gene Name', $gene->getGeneName());        
        $data[] = $this->add('Scientific Name', $gene->getScientificName());
        $data[] = $this->add('Rs Number', $gene->getRsNumber());        
        $data[] = $this->add('Result Option', $mapping->getMappingTitle('LeepAdmin_Gene_ResultOption', $gene->getGroupID()));        
        $data[] = $this->add('Notes', $gene->getNotes());        
        $data[] = $this->add('Relevant', $gene->getRelevant() ? "Yes" : "No");        
        $data[] = $this->add('Recheck', $gene->getRecheck() ? "Yes" : "No");        
        $data[] = $this->add('Wt Base', $gene->getWTBase());        
        $data[] = $this->add('Mut Base', $gene->getMUTBase());        
        $data[] = $this->add('Odds Ratio Wt', $gene->getOddsRatioWt());        
        $data[] = $this->add('Odds Ratio Het', $gene->getOddsRatioHet());        
        $data[] = $this->add('Odds Ratio Mut', $gene->getOddsRatioMut());        
        $data[] = $this->add('Reference 1', $gene->getReference1());        
        $data[] = $this->add('Reference 2', $gene->getReference2());        
        $data[] = $this->add('Reference 3', $gene->getReference3());        
        
        $data[] = $this->addSection('Amplicon 1');
        $data[] = $this->add('Amplicon 1', $gene->getAmplicon1());        
        $data[] = $this->add('Primer 1 Fwd', $gene->getPrimerFwd1());        
        $data[] = $this->add('1 Fwd Sequence', $gene->getPrimerFwdSeq1());        
        $data[] = $this->add('Primer 1 Rev', $gene->getPrimerRev1());        
        $data[] = $this->add('1 Rev Sequence', $gene->getPrimerRevSeq1());     

        $data[] = $this->addSection('Amplicon 2');
        $data[] = $this->add('Amplicon 2', $gene->getAmplicon2());        
        $data[] = $this->add('Primer 2 Fwd', $gene->getPrimerFwd2());        
        $data[] = $this->add('2 Fwd Sequence', $gene->getPrimerFwdSeq2());        
        $data[] = $this->add('Primer 2 Rev', $gene->getPrimerRev2());        
        $data[] = $this->add('2 Rev Sequence', $gene->getPrimerRevSeq2());     

        $data[] = $this->addSection('Amplicon 3');
        $data[] = $this->add('Amplicon 3', $gene->getAmplicon3());        
        $data[] = $this->add('Primer 3 Fwd', $gene->getPrimerFwd3());        
        $data[] = $this->add('3 Fwd Sequence', $gene->getPrimerFwdSeq3());        
        $data[] = $this->add('Primer 3 Rev', $gene->getPrimerRev3());        
        $data[] = $this->add('3 Rev Sequence', $gene->getPrimerRevSeq3());     


        return $data;
    }
}
