<?php
namespace Leep\AdminBundle\Business\Gene;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('geneName', 'text', array(
            'label'    => 'Gene Name',
            'required' => true
        ));
        $builder->add('scientificName', 'text', array(
            'label'    => 'Scientific Name',
            'required' => false
        ));
        $builder->add('rsNumber', 'text', array(
            'label'    => 'Rs Number',
            'required' => false
        ));
        $builder->add('idResultOption', 'choice', array(
            'label'    => 'Results Option',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Gene_ResultOptions')
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false
        ));
        $builder->add('isRelevant', 'checkbox', array(
            'label'    => 'Relevant',
            'required' => false
        ));
        $builder->add('isRecheck', 'checkbox', array(
            'label'    => 'Recheck',
            'required' => false
        ));

        $builder->add('wtBase', 'text', array(
            'label'    => 'WT Base',
            'required' => false
        ));
        $builder->add('mutBase', 'text', array(
            'label'    => 'Mut Base',
            'required' => false
        ));
        $builder->add('oddsRatioWt', 'text', array(
            'label'    => 'Odds Ratio Wt',
            'required' => false
        ));
        $builder->add('oddsRatioHet', 'text', array(
            'label'    => 'Odds Ratio Het',
            'required' => false
        ));
        $builder->add('oddsRatioMut', 'text', array(
            'label'    => 'Odds Ratio Mut',
            'required' => false
        ));

        $builder->add('reference1', 'textarea', array(
            'label'    => 'Reference 1',
            'required' => false
        ));
        $builder->add('reference2', 'textarea', array(
            'label'    => 'Reference 2',
            'required' => false
        ));
        $builder->add('reference3', 'textarea', array(
            'label'    => 'Reference 3',
            'required' => false
        ));

        // AMPLICON 1
        $builder->add('section1', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 1'
        ));
        $builder->add('amplicon1', 'text', array(
            'label'    => 'Amplicon 1',
            'required' => false
        ));
        $builder->add('primerFwd1', 'text', array(
            'label'    => 'Primer 1 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq1', 'text', array(
            'label'    => '1 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev1', 'text', array(
            'label'    => 'Primer 1 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq1', 'text', array(
            'label'    => '1 Rev Sequence',
            'required' => false
        ));

        // AMPLICON 2
        $builder->add('section2', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 2'
        ));
        $builder->add('amplicon2', 'text', array(
            'label'    => 'Amplicon 2',
            'required' => false
        ));
        $builder->add('primerFwd2', 'text', array(
            'label'    => 'Primer 2 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq2', 'text', array(
            'label'    => '2 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev2', 'text', array(
            'label'    => 'Primer 2 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq2', 'text', array(
            'label'    => '2 Rev Sequence',
            'required' => false
        ));

        // AMPLICON 3
        $builder->add('section3', 'section', array(
            'property_path' => false,
            'label'    => 'Amplicon 3'
        ));
        $builder->add('amplicon3', 'text', array(
            'label'    => 'Amplicon 3',
            'required' => false
        ));
        $builder->add('primerFwd3', 'text', array(
            'label'    => 'Primer 3 Fwd',
            'required' => false
        ));
        $builder->add('primerFwdSeq3', 'text', array(
            'label'    => '3 Fwd Sequence',
            'required' => false
        ));
        $builder->add('primerRev3', 'text', array(
            'label'    => 'Primer 3 Rev',
            'required' => false
        ));
        $builder->add('primerRevSeq3', 'text', array(
            'label'    => '3 Rev Sequence',
            'required' => false
        ));

    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $gene = new Entity\Genes();
        $gene->setGeneName($model->geneName);
        $gene->setScientificName($model->scientificName);
        $gene->setRsNumber($model->rsNumber);
        $gene->setGroupID($model->idResultOption);
        $gene->setNotes($model->notes);
        $gene->setRelevant($model->isRelevant);
        $gene->setRecheck($model->isRecheck);
        $gene->setWTBase($model->wtBase);
        $gene->setMutBase($model->mutBase);
        $gene->setOddsRatioWt($model->oddsRatioWt);
        $gene->setOddsRatioHet($model->oddsRatioHet);
        $gene->setOddsRatioMut($model->oddsRatioMut);
        $gene->setReference1($model->reference1);
        $gene->setReference2($model->reference2);
        $gene->setReference3($model->reference3);
        
        $gene->setAmplicon1($model->amplicon1);
        $gene->setPrimerFwd1($model->primerFwd1);
        $gene->setPrimerFwdSeq1($model->primerFwdSeq1);
        $gene->setPrimerRev1($model->primerRev1);
        $gene->setPrimerRevSeq1($model->primerRevSeq1);

        $gene->setAmplicon2($model->amplicon2);
        $gene->setPrimerFwd2($model->primerFwd2);
        $gene->setPrimerFwdSeq2($model->primerFwdSeq2);
        $gene->setPrimerRev2($model->primerRev2);
        $gene->setPrimerRevSeq2($model->primerRevSeq2);

        $gene->setAmplicon3($model->amplicon3);
        $gene->setPrimerFwd3($model->primerFwd3);
        $gene->setPrimerFwdSeq3($model->primerFwdSeq3);
        $gene->setPrimerRev3($model->primerRev3);
        $gene->setPrimerRevSeq3($model->primerRevSeq3);


        $em->persist($gene);
        $em->flush();

        parent::onSuccess();
    }
}
