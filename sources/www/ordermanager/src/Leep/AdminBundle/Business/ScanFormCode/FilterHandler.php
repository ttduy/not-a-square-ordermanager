<?php
namespace Leep\AdminBundle\Business\ScanFormCode;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->status = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('code', 'text', array(
            'label'       => 'Code',
            'required'    => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_ScanFormCode_Status'),
            'empty_value' => false
        ));

        return $builder;
    }
}