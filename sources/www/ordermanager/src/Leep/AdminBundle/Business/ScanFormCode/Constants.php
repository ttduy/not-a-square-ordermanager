<?php
namespace Leep\AdminBundle\Business\ScanFormCode;

class Constants {
    const STATUS_UNUSED = 1;
    const STATUS_USED = 2;

    public static function getScanFormCodeStatus() {
        return array(
            self::STATUS_UNUSED    => 'Available',
            self::STATUS_USED      => 'Used'
        );
    }
}
