<?php 
namespace Leep\AdminBundle\Business\MwaInvoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'invoiceNumber', 'invoiceDate', 'postage', 'productPrice', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',     'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Number',   'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Date',     'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Postage',  'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Product Price', 'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Status',   'width' => '11%', 'sortable' => 'true'),
            array('title' => 'Action',   'width' => '11%', 'sortable' => 'false')
            
        );
    }


    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.invoiceNumber';
        $this->columnSortMapping[] = 'p.invoiceDate';
        $this->columnSortMapping[] = 'p.postage';
        $this->columnSortMapping[] = 'p.productPrice';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }
    

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_MwaInvoiceGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:MwaInvoice', 'p');
        
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->invoiceNumber) != '') {
            $queryBuilder->andWhere('p.invoicenumber LIKE :invoiceNumber')
                ->setParameter('invoiceNumber', '%'.trim($this->filters->invoiceNumber).'%');
        }
        if ($this->filters->invoiceDate != null) {
            $queryBuilder->andWhere('p.invoicedate = :invoiceDate')
                ->setParameter('invoiceDate', $this->filters->invoiceDate);
        }        
        if ($this->filters->invoiceStatus != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->invoiceStatus);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'mwa_invoice', 'invoice_form_edit',  'pdf',  array('id' => $row->getId())), 'button-pdf');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('mwaInvoiceModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'mwa_invoice', 'invoice_form_edit', 'list', array('id' => $row->getId())), 'button-edit');            
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'mwa_invoice', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
