<?php 
namespace Leep\AdminBundle\Business\MwaInvoice;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $mwaInvoice = $doctrine->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($id);
        $em->remove($mwaInvoice);
        $em->flush();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:MwaSample')->findByIdMwaInvoice($id);
        foreach ($result as $r) {
            $r->setIdMwaInvoice(NULL);
        }

        $em->flush();
    }
}