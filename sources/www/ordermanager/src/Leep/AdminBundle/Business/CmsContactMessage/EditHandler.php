<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CmsContactMessage', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->timestamp = $entity->getTimestamp();
        $model->idSource = $entity->getIdSource();
        $model->emailAddress = $entity->getEmailAddress();
        $model->name = $entity->getName();
        $model->message = $entity->getMessage();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('idSource', 'choice', array(
            'label'    => 'Source',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsSource_List')
        ));
        $builder->add('emailAddress', 'text', array(
            'label'    => 'Email Address',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('message', 'textarea', array(
            'label'    => 'Message',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();       
        $em = $this->container->get('doctrine')->getEntityManager();       
        $dbHelper = $this->container->get('leep_admin.helper.database'); 

        $this->entity->setTimestamp($model->timestamp);
        $this->entity->setIdSource($model->idSource);
        $this->entity->setEmailAddress($model->emailAddress);
        $this->entity->setName($model->name);
        $this->entity->setMessage($model->message);

        parent::onSuccess();
    }
}
