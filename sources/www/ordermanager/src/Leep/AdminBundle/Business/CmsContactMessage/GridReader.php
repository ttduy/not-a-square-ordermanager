<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('timestamp', 'idSource', 'emailAddress', 'name', 'message', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',                   'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Source',                      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Email Address',               'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',                        'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Message',                     'width' => '45%', 'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CmsContactMessageGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CmsContactMessage', 'p');
        if (trim($this->filters->emailAddress) != '') {
            $queryBuilder->andWhere('p.emailAddress LIKE :emailAddress')
                ->setParameter('emailAddress', '%'.trim($this->filters->emailAddress).'%');
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->text) != '') {
            $queryBuilder->andWhere('p.message LIKE :text')
                ->setParameter('text', '%'.trim($this->filters->text).'%');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.timestamp', 'DESC');        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cmsOrderModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cms_contact_message', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');    
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cms_contact_message', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellMessage($row) {
        return nl2br($row->getMessage());
    }
}
