<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('emailAddress', 'text', array(
            'label'    => 'Email Address',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('text', 'text', array(
            'label'    => 'Text',
            'required' => false
        ));

        return $builder;
    }
}