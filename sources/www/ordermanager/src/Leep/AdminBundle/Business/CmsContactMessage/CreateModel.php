<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $timestamp;
    public $idSource;
    public $emailAddress;
    public $name;
    public $message;
}
