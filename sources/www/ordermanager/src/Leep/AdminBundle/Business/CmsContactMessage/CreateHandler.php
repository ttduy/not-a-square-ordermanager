<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();   
        $model->timestamp = new \DateTime();     
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('timestamp', 'datetimepicker', array(
            'label'    => 'Timestamp',
            'required' => false
        ));
        $builder->add('idSource', 'choice', array(
            'label'    => 'Source',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CmsSource_List')
        ));
        $builder->add('emailAddress', 'text', array(
            'label'    => 'Email Address',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('message', 'textarea', array(
            'label'    => 'Message',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $contactMessage = new Entity\CmsContactMessage();
        $contactMessage->setTimestamp($model->timestamp);
        $contactMessage->setIdSource($model->idSource);
        $contactMessage->setEmailAddress($model->emailAddress);
        $contactMessage->setName($model->name);
        $contactMessage->setMessage($model->message);
        $em->persist($contactMessage);
        $em->flush();

        parent::onSuccess();
    }
}
