<?php
namespace Leep\AdminBundle\Business\CmsContactMessage;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $timestamp;
    public $idSource;
    public $emailAddress;
    public $name;
    public $message;
}
