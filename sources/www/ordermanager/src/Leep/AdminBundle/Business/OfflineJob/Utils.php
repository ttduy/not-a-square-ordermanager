<?php
namespace Leep\AdminBundle\Business\OfflineJob;

class Utils {
    public static function getCurrentActiveJobs($controller) {
        $userManager = $controller->get('leep_admin.web_user.business.user_manager');
        $idUser = $userManager->getUser()->getId();

        $activeStatus = array(
            Constants::OFFLINE_JOB_PENDING,
            Constants::OFFLINE_JOB_PROCESSING,
        );

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p')
            ->andWhere('p.idWebUser = :idWebUser') 
            ->andWhere('p.status in (:activeStatus)')
            ->setParameter('idWebUser', $idUser)
            ->setParameter('activeStatus', $activeStatus);
        return $query->getQuery()->getSingleScalarResult();
    }
}