<?php
namespace Leep\AdminBundle\Business\OfflineJob;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'idWebUser', 'inputTime', 'description', 'status',  'result');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'ID',            'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Web User',      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Input time',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Description',   'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Status',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Result',        'width' => '15%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_OfflineJobGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p');

        if ($this->filters->idWebUser != 0) {
            $queryBuilder->andWhere('p.idWebUser = :idWebUser')
                ->setParameter('idWebUser', $this->filters->idWebUser);
        }
        if ($this->filters->status != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }
        if (trim($this->filters->description) != '') {
            $queryBuilder->andWhere('p.description LIKE :description')
                ->setParameter('description', '%'.trim($this->filters->description).'%');
        }
    }

    public function buildCellResult($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        if ($row->getStatus() == Constants::OFFLINE_JOB_FINISHED) {
            $builder->addNewTabButton('Detail', $mgr->getUrl('leep_admin', 'offline_job', 'feature', 'viewResult', array('id' => $row->getId())), 'button-detail');      
        }

        return $builder->getHtml();
    }

    public function buildCellStatus($row) {
        $mapping = $this->container->get('easy_mapping');

        $status = $mapping->getMappingTitle('LeepAdmin_OfflineJob_Status', $row->getStatus());
        if ($row->getStatus() == Constants::OFFLINE_JOB_PROCESSING) {
            $status .= ' ('.number_format($row->getPercentage(), 2).'%)';
        }
        return $status;
    }
}