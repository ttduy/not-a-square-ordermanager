<?php
namespace Leep\AdminBundle\Business\OfflineJob\Handler;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TestReportHandler {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function process($offlineJob) {
        $jobData = json_decode($offlineJob->getParameters(), true);
        $idLanguage = $jobData['idLanguage'];
        $idReport = $jobData['idReport'];
        $reportData = $jobData['reportData'];

        $filename = 'test_report_'.$offlineJob->getId().'.pdf';

        $customerReport = new Entity\CustomerReport();
        $customerReport->setIdLanguage($idLanguage);
        $customerReport->setReportFinalData($reportData);
        $customerReport->setIdReport($idReport);
        $customerReport->setFilename($filename);

        $outputResult = array();
        try {
            $notifier = new Business\OfflineJob\Notifier($this->container);
            $notifier->setOfflineJob($offlineJob);

            Business\CustomerReport\Utils::generateReportPdf($this->container, $customerReport, true, $notifier);
            $outputResult['result'] = 'success';
            $outputResult['filename'] = $filename;
        }
        catch (\Exception $e) {
            $filename = 'exception_'.$offlineJob->getId().'.html';
            $outputFile = $this->container->getParameter('kernel.root_dir').'/../web/report/'.$filename;
            $outputResult['result'] = 'failed';
            $outputResult['filename'] = $filename;
                
            file_put_contents($outputFile, $e->getTraceAsString());
        }     
        return $outputResult;
    }

    public function view($offlineJob) {
        $output = json_decode($offlineJob->getOutputResult(), true);
        $filename = $output['filename'];

        $fileUrl = $this->container->getParameter('attachment_url').'/../report/'.$filename;
        $response = new RedirectResponse($fileUrl);
        return $response;
    }

    public function deleteJob($offlineJob) {
        $output = json_decode($offlineJob->getOutputResult(), true);
        $filePath = $this->container->getParameter('kernel.root_dir').'/../web/report/'.$output['filename'];        
        if (is_file($filePath)) {
            @unlink($filePath);            
        }
    }
}