<?php
namespace Leep\AdminBundle\Business\OfflineJob;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Notifier {
    public $container;
    public $em;
    public $offlineJob;
    public function __construct($container) {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getEntityManager();
    }

    public function setOfflineJob($offlineJob) {
        $this->offlineJob = $offlineJob;
    }
    public function updateProgress($numProcessed, $total) {
        if ($total == 0) {
            $percentage = '100';
        }
        else {
            $percentage = number_format($numProcessed * 100 / $total, 2);
        }

        $this->offlineJob->setPercentage($percentage);
        $this->em->flush();
    }
}
