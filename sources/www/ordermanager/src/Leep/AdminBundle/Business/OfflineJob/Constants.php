<?php
namespace Leep\AdminBundle\Business\OfflineJob;

class Constants {
    const OFFLINE_JOB_PENDING    = 1;
    const OFFLINE_JOB_PROCESSING = 2;
    const OFFLINE_JOB_FINISHED   = 3;
    public static function getOfflineJobStatus() {
        return array(
            self::OFFLINE_JOB_PENDING     => 'Pending',
            self::OFFLINE_JOB_PROCESSING  => 'Processing',
            self::OFFLINE_JOB_FINISHED    => 'Finished',
        );
    }


    const OFFLINE_JOB_HANDLER_TEST_REPORT = 1;
    const OFFLINE_JOB_HANDLER_CUSTOMER_REPORT = 2;
    public static function getHandler($idHandler, $container) {
        switch ($idHandler) {
            case self::OFFLINE_JOB_HANDLER_TEST_REPORT:
                return new Handler\TestReportHandler($container);
            case self::OFFLINE_JOB_HANDLER_CUSTOMER_REPORT:
                return new Handler\CustomerReportHandler($container);
        }
        return null;
    }
}