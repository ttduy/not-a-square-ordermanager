<?php
namespace Leep\AdminBundle\Business\NotificationSubscription;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();

        // get subscription info
        $subscription = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:NotificationSubscription')->findOneByIdUser($user->getId());

        if ($subscription) {
            $model->idTopics = json_decode($subscription->getIdTopics(), true);
            $model->subscribeAll = $subscription->getSubscribeAll();            
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        

        $builder->add('idTopics', 'choice', array(
            'label'    => 'Topic',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_Notification_Topic_List')
        ));

        $builder->add('subscribeAll', 'checkbox', array(
            'label'     => 'Subscribe All',
            'required'  => false,
            'attr'      => array('onChange' => 'selectAllTopics()')
        ));
    }

    public function onSuccess() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();

        $em = $this->container->get('doctrine')->getEntityManager();        
        $dbHelper = $this->container->get('leep_admin.helper.database'); 

        $model = $this->getForm()->getData();

        $subscription = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:NotificationSubscription')->findOneByIdUser($user->getId());

        if (!$subscription) {
            $subscription = new Entity\NotificationSubscription();
            $subscription->setIdUser($user->getId());
        }

        $subscription->setIdTopics(json_encode($model->idTopics));
        $subscription->setSubscribeAll($model->subscribeAll);

        $em->persist($subscription);
        $em->flush();        

        parent::onSuccess();
    }
}
