<?php 
namespace Leep\AdminBundle\Business\MwaInvoiceForm;

use Symfony\Component\Validator\ExecutionContext;

class FilterEditModel {
    public $name;
    public $invoiceNumber;
    public $invoiceDate;
    public $isWithTax;
    public $taxValue;
    public $postage;
    public $idCompany;
    public $productPrice;
    public $idGroup;
    public $isShowSummarySentence;

    public $uid;
    public $purchaseOrder;
    public $supplierNumber;
    public $product;

    public $selectedItems;
    public $priceOverride;
    public $actionMode;
    public $target;
    public $currencyRate;

    public $statusList;

    public $textAfterDescription;
    public $textFooter;
}
