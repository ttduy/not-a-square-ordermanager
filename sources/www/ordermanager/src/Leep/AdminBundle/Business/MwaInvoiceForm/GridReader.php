<?php 
namespace Leep\AdminBundle\Business\MwaInvoiceForm;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }
    
    public function getColumnMapping() {
        return array('sampleId', 'idGroup', 'currentStatus');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Sample Id',      'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Group',          'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Status',         'width' => '30%', 'sortable' => 'false')     
            
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_MwaInvoiceFormGridReader');
    }


    public function buildCellPriceOverride($row) {
        return '<input type="textbox" id="override_price_'.$row->getId().'" value="'.$row->getPriceOverride().'" />';
    }

    public function buildCellIdGroup($row) {
        if ($row->getIdGroup() == 1) {
            return 'Default';
        }
        return 'Russia';
    }
    
    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('(p.idMwaInvoice IS NULL) or (p.idMwaInvoice = 0)')
            ->andWhere('p.isDeleted = 0')
            ->andWhere('p.currentStatus = :statusDone')
            ->setParameter('statusDone', 9); // Fix me
        
        if ($this->filters->idGroup != 0) {
            $queryBuilder->andWhere('p.idGroup = :idGroup')
                ->setParameter('idGroup', $this->filters->idGroup);
        }
    }
}
