<?php 
namespace Leep\AdminBundle\Business\MwaInvoiceForm;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class FilterEditHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterEditModel();

        $id = $this->container->get('request')->query->get('id', 0);
        $mwaInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaInvoice', 'app_main')->findOneById($id);
        
        $model->name = $mwaInvoice->getName();
        $model->invoiceNumber = $mwaInvoice->getInvoiceNumber();
        $model->invoiceDate = $mwaInvoice->getInvoiceDate();
        $model->isWithTax = $mwaInvoice->getIsWithTax();
        $model->taxValue = $mwaInvoice->getTaxValue();
        $model->postage = $mwaInvoice->getPostage();
        $model->idCompany = $mwaInvoice->getIdCompany();
        $model->productPrice = $mwaInvoice->getProductPrice();
        $model->idGroup = $mwaInvoice->getIdGroup();
        $model->isShowSummarySentence = $mwaInvoice->getIsShowSummarySentence();
        
        $model->uid = $mwaInvoice->getUid();
        $model->purchaseOrder = $mwaInvoice->getPurchaseOrder();
        $model->supplierNumber = $mwaInvoice->getSupplierNumber();
        $model->product = $mwaInvoice->getProduct();

        $model->currencyRate = new Business\FormType\AppCurrencyRateModel();
        $model->currencyRate->idCurrency = $mwaInvoice->getIdCurrency();
        $model->currencyRate->exchangeRate = $mwaInvoice->getExchangeRate();

        $model->textAfterDescription = $mwaInvoice->getTextAfterDescription();
        $model->textFooter = $mwaInvoice->getTextFooter();

        $model->selectedItems = array();
        $items = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findBy(array('idMwaInvoice' => $id));
        foreach ($items as $mwaSample) {
            $model->selectedItems[] = $mwaSample->getId();
        }
        $model->selectedItems = implode(',', $model->selectedItems);

        // Load invoice status
        $model->statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MwaInvoiceStatus', 'p')
            ->andWhere('p.idMwaInvoice = :idMwaInvoice')
            ->setParameter('idMwaInvoice', $mwaInvoice->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppInvoiceStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }        

        return $model;
    }

    public function prepareFormModel($defaultFormModel) {
        $request = $this->container->get('request');    
        if ($request->getMethod() == 'GET') {
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
        }                
        return $defaultFormModel;
    }


    public function buildForm($builder) {
        $id = $this->container->get('request')->query->get('id', 0);

        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));        
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));        
        $builder->add('postage', 'money', array(
            'label'    => 'Postage',
            'required'  => false
        ));
        $builder->add('idCompany', 'choice', array(
            'label'    => 'Company',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('productPrice', 'text', array(
            'label'    => 'Product Price',
            'required' => false
        ));
        $builder->add('idGroup', 'choice', array(
            'label'    => 'Group',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All', 1 => 'Default', 2 => 'Russia')
        ));
        $builder->add('isShowSummarySentence', 'checkbox', array(
            'label'    => 'Is show summary sentence',
            'required' => false
        ));

        $builder->add('sectionTextFields',   'section', array(
            'label' => 'Text fields',
            'property_path' => false
        ));
        $builder->add('uid', 'text', array(
            'label'    => 'Your UID',
            'required' => false
        ));
        $builder->add('purchaseOrder', 'text', array(
            'label'    => 'Purchase Order',
            'required' => false
        ));
        $builder->add('supplierNumber', 'text', array(
            'label'    => 'Supplier Number',
            'required' => false
        ));        
        $builder->add('product', 'text', array(
            'label'    => 'Product',
            'required' => false
        ));

        $builder->add('selectedItems', 'hidden');
        $builder->add('actionMode', 'hidden');
        $builder->add('target', 'hidden');
        $builder->add('priceOverride', 'hidden');

        $builder->add('sectionExtraText',          'section', array(
            'label' => 'Extra Text',
            'property_path' => false
        ));
        $builder->add('textAfterDescription',      'textarea', array(
            'label' => 'After description',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));
        $builder->add('textFooter',      'textarea', array(
            'label' => 'Footer',
            'required' => false,
            'attr'  => array(
                'rows'  => 4, 
                'style' => 'width: 100%'
            )
        ));

        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppInvoiceStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',            
            'required' => false
        ));


        $builder->addEventListener(FormEvents::PRE_BIND , array($this, 'onPreBind'));

        return $builder;
    }

    public $actionMode = '';
    public function onPreBind(DataEvent $event) {
        $filterData = $event->getData();
        $filterData['target'] = '';

        if (isset($filterData['actionMode'])) {
            if ($filterData['actionMode'] == 'save') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $event->setData($filterData);
            }
            else if ($filterData['actionMode'] == 'pdf') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $filterData['target'] = 'pdf';
                $event->setData($filterData);
            }
        }
    }
        
    public function onSuccess() {
        parent::onSuccess();

        if ($this->actionMode == 'save') {
            $model = $this->getCurrentFilter();

            $id = $this->container->get('request')->query->get('id', 0);
            $mwaInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaInvoice', 'app_main')->findOneById($id);
        
            $mwaInvoice->setName($model->name);
            $mwaInvoice->setInvoiceNumber($model->invoiceNumber);
            $mwaInvoice->setInvoiceDate($model->invoiceDate);
            $mwaInvoice->setIsWithTax($model->isWithTax);
            $mwaInvoice->setTaxValue($model->taxValue);
            $mwaInvoice->setPostage($model->postage);
            $mwaInvoice->setIdCompany($model->idCompany);
            $mwaInvoice->setProductPrice($model->productPrice);
            $mwaInvoice->setIdGroup($model->idGroup);
            $mwaInvoice->setIsShowSummarySentence($model->isShowSummarySentence);

            $mwaInvoice->setUid($model->uid);
            $mwaInvoice->setPurchaseOrder($model->purchaseOrder);
            $mwaInvoice->setSupplierNumber($model->supplierNumber);
            $mwaInvoice->setProduct($model->product);

            if ($model->currencyRate) {
                $mwaInvoice->setIdCurrency($model->currencyRate->idCurrency);
                $mwaInvoice->setExchangeRate($model->currencyRate->exchangeRate);
            }

            $mwaInvoice->setTextAfterDescription($model->textAfterDescription);
            $mwaInvoice->setTextFooter($model->textFooter);
            
            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($mwaInvoice);
            $em->flush();
            
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:MwaSample', 'c')
                ->set('c.idMwaInvoice', 'null')
                ->set('c.priceOverride', 'null')
                ->andWhere('c.idMwaInvoice = :id')
                ->setParameter('id', $id)
                ->getQuery()->execute();

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idMwaSample) {
                $idMwaSample = intval($idMwaSample);
                if ($idMwaSample != 0) {
                    $mwaSample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findOneById($idMwaSample);
                    $mwaSample->setIdMwaInvoice($mwaInvoice->getId());
                    $em->persist($mwaSample);
                }
            }
            $em->flush();

            $overrideItems = explode('|', $model->priceOverride);
            foreach ($overrideItems as $priceOverrideItem) {
                $t = explode('=', $priceOverrideItem);
                $idMwaSample = intval($t[0]);

                if (in_array($idMwaSample, $items) && $idMwaSample != 0)  {
                    $mwaSample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findOneById($idMwaSample);
                    $mwaSample->setPriceOverride(floatval($t[1]));
                    $em->persist($mwaSample);    
                }
            }
            $em->flush();

            // Update status
            $dbHelper = $this->container->get('leep_admin.helper.database');    
            $filters = array('idMwaInvoice' => $mwaInvoice->getId());
            $dbHelper->delete($em, 'AppDatabaseMainBundle:MwaInvoiceStatus', $filters);

            $sortOrder = 1;
            $lastStatus = 0;
            $lastDate = new \DateTime();
            foreach ($model->statusList as $statusRow) {
                if (empty($statusRow)) continue;
                $status = new Entity\MwaInvoiceStatus();
                $status->setIdMwaInvoice($mwaInvoice->getId());
                $status->setStatusDate($statusRow->statusDate);
                $status->setStatus($statusRow->status);
                $status->setNotes($statusRow->notes);
                $status->setSortOrder($sortOrder++);
                $em->persist($status);
                
                $lastStatus = $statusRow->status;
                $lastDate = $statusRow->statusDate;
            }
            $mwaInvoice->setStatus($lastStatus);
            $mwaInvoice->setStatusDate($lastDate);
            $em->persist($mwaInvoice);
            $em->flush();

            $this->messages[] = "The records has been saved successfully"; 
        }
    }
}