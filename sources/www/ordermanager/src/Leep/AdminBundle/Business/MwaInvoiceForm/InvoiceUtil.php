<?php 
namespace Leep\AdminBundle\Business\MwaInvoiceForm;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;

class InvoiceUtil {
    // Build mwa invoice
    public static function buildPdf($controller, $idMwaInvoice, $isReturnDataOnly = false) {
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $mwaInvoice = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaInvoice')->findOneById($idMwaInvoice);
        $company = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Companies')->findOneById($mwaInvoice->getIdCompany());
        if (!$company) {
            return new Response("Please select company for this invoice");
        }

        $logo = $formatter->container->getParameter('kernel.root_dir').'/../logo.png';        
        if ($company->getLogo()) {
            $logo = $formatter->container->getParameter('kernel.root_dir').'/../web/attachments/logo/'.$company->getLogo();
        }

        $data = array();

        $data['yourUid'] = $mwaInvoice->getUid();
        $data['purchaseOrder'] = $mwaInvoice->getPurchaseOrder();
        $data['supplierNumber'] = $mwaInvoice->getSupplierNumber();

        $data['clientR1'] = 'Amway GmbH';
        $data['clientR2'] = 'Benzstraße 11 b-c';
        $data['clientR3'] = 'D-82178 Puchheim';
        $data['clientR4'] = 'Germany';
        
        $data['mwaInvoice'] = $mwaInvoice;
        $data['logoImagePath'] = $logo;
        $data['company'] = $company;
        $data['companyName'] = $company->getCompanyName();
        $data['companyStreet'] = $company->getStreet();
        $data['companyPostCode'] = $company->getPostCode();
        $data['companyCity'] = $company->getCity();
        $data['companyEmail'] = $company->getEmail();
        $data['companyTelephone'] = $company->getTelephone();
        $data['companyWebSite'] = $company->getWebSite();


        $today = new \DateTime();
        $data['invoiceDate'] = $formatter->format($mwaInvoice->getInvoiceDate(), 'date'); 
        $data['invoiceNumber'] = $mwaInvoice->getInvoiceNumber();

        if ($isReturnDataOnly) {
            return $data;
        }
        
        $order = array();
        $subTotal = 0;
        $result = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:MwaSample')->findBy(array('idMwaInvoice' => $idMwaInvoice));
        $order = array();
        $numSample = 0;
        $productPrice = floatval($mwaInvoice->getProductPrice());
        foreach ($result as $mwaSample) {            
            $order[] = array(
                'sampleId'  => $mwaSample->getSampleId(),
                'date'      => $formatter->format($mwaSample->getRegisteredDate(), 'date'),
                'product'   => $mwaInvoice->getProduct(),
                'price'     => $formatter->format($productPrice, 'money')
            );            

            $subTotal += $productPrice;
            $numSample++;
        } 
        $data['order'] = $order;
        $data['postage'] = $formatter->format($mwaInvoice->getPostage(), 'money');
        $data['subtotal'] = $formatter->format($subTotal, 'money');
        $data['total']    = $formatter->format($subTotal + floatval($mwaInvoice->getPostage()), 'money');

        $data['numSample'] = $numSample;

        $total = ($subTotal + floatval($mwaInvoice->getPostage()));
        $taxAmount = $total * floatval($mwaInvoice->getTaxValue()) / 100;
        $data['taxAmount'] = $formatter->format($taxAmount, 'money');
        $data['totalWithTax'] = $formatter->format($total + $taxAmount, 'money');

        // Update invoice amount
        if ($mwaInvoice->getIsWithTax() == 1) {
            $finalTotal = $total + $taxAmount;            
        }
        else {
            $finalTotal = $total;
        }
        $mwaInvoice->setInvoiceAmount($finalTotal);

        // Currency conversion
        if ($mwaInvoice->getIdCurrency() != 0) {
            $currency = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Currency')->findOneById($mwaInvoice->getIdCurrency());
            if ($currency) {
                $exchangeRate = floatval($mwaInvoice->getExchangeRate());
                $data['currency'] = $currency;
                $data['exchangeRate'] = $exchangeRate;
                $totalConverted = $exchangeRate * $finalTotal;
                $data['totalConverted'] = $formatter->format($totalConverted, 'money', $currency->getSymbol());
            }
        }

        // Extra text
        $data['textAfterDescription'] = nl2br($mwaInvoice->getTextAfterDescription());
        $data['textFooter'] = nl2br($mwaInvoice->getTextFooter());

        // Build PDF
        $html = $controller->get('templating')->render('LeepAdminBundle:MwaInvoiceForm:pdf.html.twig', $data);

        $dompdf = $controller->get('slik_dompdf');
        $dompdf->getpdf($html); 

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';
        $fileName = 'mwa_invoice_'.$mwaInvoice->getId().'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        $mwaInvoice->setInvoiceFile($fileName);

        $em = $controller->get('doctrine')->getEntityManager();
        $em->persist($mwaInvoice);
        $em->flush();
        return TRUE;        
    }
}
