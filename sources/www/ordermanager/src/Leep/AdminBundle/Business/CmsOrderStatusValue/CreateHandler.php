<?php
namespace Leep\AdminBundle\Business\CmsOrderStatusValue;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $orderStatusValue = new Entity\CmsOrderStatusValue();
        $orderStatusValue->setName($model->name);
        $orderStatusValue->setSortOrder($model->sortOrder);

        $em->persist($orderStatusValue);
        $em->flush();

        parent::onSuccess();
    }
}
