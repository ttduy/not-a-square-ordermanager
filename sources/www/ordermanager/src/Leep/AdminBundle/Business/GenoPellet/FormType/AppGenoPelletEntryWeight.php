<?php
namespace Leep\AdminBundle\Business\GenoPellet\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppGenoPelletEntryWeight extends AbstractType {
    protected $container;
    protected $hasTargetWeight;
    protected $isNewWeight;

    public function __construct($container, $hasTargetWeight = false, $isNewType = false) {
        $this->container = $container;
        $this->hasTargetWeight = $hasTargetWeight;
        $this->isNewType = $isNewType;
    }

    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeightModel'
        );
    }

    public function getParent() {
        return 'field';
    }

    public function getName() {
        return 'app_geno_pellet_entry_weight';
    }

    public function finishView(FormView $view, FormInterface $form, array $options) {
        $view->vars['hasTargetWeight'] = $this->hasTargetWeight;
        $view->vars['isNewType'] = $this->isNewType;
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('txt', 'text', array(
            'label' => 'Name',
            'required'  => false,
            'attr' => array('placeholder' => 'Micronutrient')
        ));

        $builder->add('weight', 'text', array(
            'label' => 'Weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));

        $builder->add('targetWeight', 'text', array(
            'label' => 'Target weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));

        $builder->add('activeIngredients', 'searchable_box', array(
            'label' => 'Active ingredients susbtance',
            'required'  => false,
            'choices'  => array('' => '') + Business\GenoMaterial\Utils::getAllGenoSubstance($this->container),
            'attr' => array('placeholder' => 'Active ingredients substance')
        ));

        $builder->add('actives', 'searchable_box', array(
            'label' => 'Active susbtance',
            'required'  => false,
            'choices'  => array('' => '') + Business\GenoMaterial\Utils::getAllGenoSubstance($this->container),
            'attr' => array('placeholder' => 'Active substance')
        ));

        $builder->add('additives', 'searchable_box', array(
            'label' => 'Additives susbtance',
            'required'  => false,
            'choices'  => array('' => '') + Business\GenoMaterial\Utils::getAllGenoSubstance($this->container),
            'attr' => array('placeholder' => 'Additives susbtance')
        ));

        $builder->add('weightActivesIngredients', 'text', array(
            'label' => 'Weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));

        $builder->add('weightActives', 'text', array(
            'label' => 'Weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));

        $builder->add('weightAdditives', 'text', array(
            'label' => 'Weight',
            'required'  => false,
            'attr' => array('placeholder' => 'w/w %')
        ));
    }
}