<?php
namespace Leep\AdminBundle\Business\GenoPellet;

class Utils {

    public static function clearAllEntries($container, $pelletId) {
        $em = $container->get('doctrine')->getEntityManager();

        // Remove entry history
        $history = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntryHistory')->findByIdGenoPellet($id);
        foreach ($history as $item) {
            $em->remove($item);
        }

        $em->flush();

        // Clear geno pellet entry
        $entryList = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findByIdPellet($pelletId);
        foreach ($entryList as $entry) {
            self::clearEntrySubData($container, $entry->getId());
            $em->remove($entry);
        }

        $em->flush();
    }

    public static function clearEntrySubData($container, $entryId) {
        $em = $container->get('doctrine')->getEntityManager();

        // Clear entry additives
        $resultList = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntryAdditives')->findByIdPelletEntry($entryId);
        foreach ($resultList as $data) {
            $em->remove($data);
        }

        $em->flush();

        // Clear entry weight
        $resultList = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntryWeight')->findByIdPelletEntry($entryId);
        foreach ($resultList as $data) {
            $em->remove($data);
        }

        $em->flush();
    }

    public static function getPelletUsableStock($container, $pelletId) {
        $em = $container->get('doctrine')->getEntityManager();

        $entryList = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findBy(
            ['idPellet' => $pelletId],
            ['expiryDate' => 'ASC']);

        $result = [
            'stock' => [],
            'total' => 0
        ];

        foreach ($entryList as $entry) {
            if ($entry->getSection() == Constant::SECTION_TESTED_AND_IN_USE) {
                $result['stock'][] = [
                    'id' =>         $entry->getId(),
                    'loading' =>    $entry->getLoading()
                ];

                $result['total'] += $entry->getLoading();
            }
        }

        return $result;
    }

    public static function customFormatFloat($number) {
        $decimal = strlen(substr(strrchr($number, "."), 1));
        return number_format($number, $decimal);
    }
}
