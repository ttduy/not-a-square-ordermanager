<?php
namespace Leep\AdminBundle\Business\GenoPellet;

use Leep\AdminBundle\Business\Common\RealDeleteHandler as BaseDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DeleteHandler extends BaseDeleteHandler {
    public function __construct($container, $repository) {
        parent::__construct($container, $repository);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        Utils::clearAllEntries($this->container, $id);
        parent::execute();
    }
}
