<?php
namespace Leep\AdminBundle\Business\GenoPellet;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters = array();

    // PRE GET RESULT
    public $stock = array();
    public $waiting = array();
    public $expiredIn = array();
    public $recivedNotTested = array();

    public function getColumnMapping() {
        return array('name', 'letter', 'substanceName', 'warningThreshold', 'stock', 'receivedButNotTested','waiting', 'warning', 'action');
    }

    public function getColumnSortMapping() {
        return array(
            'p.name',
            'p.letter'
        );
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',                        'width' => '11%',   'sortable' => 'true'),
            array('title' => 'Letter',                      'width' => '5%',    'sortable' => 'true'),
            array('title' => 'Substance Name',              'width' => '11%',   'sortable' => 'false'),
            array('title' => 'Warning Threshold',           'width' => '5%',   'sortable' => 'false'),
            array('title' => 'Stock',                       'width' => '13%',    'sortable' => 'false'),
            array('title' => 'Produced but not tested',     'width' => '11%',   'sortable' => 'false'),
            array('title' => 'Production',                  'width' => '10%',    'sortable' => 'false'),
            array('title' => 'Warning',                     'width' => '10%',    'sortable' => 'false'),
            array('title' => 'Action',                      'width' => '9%',    'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id,
                      p.name,
                      p.letter,
                      p.substanceName,
                      p.warningThreshold,
                      p.activeIngredientName,
                      p.expiryDateWarningThreshold')
            ->from('AppDatabaseMainBundle:GenoPellet', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if (trim($this->filters->letter) != '') {
            $queryBuilder->andWhere('p.letter LIKE :letter')
                ->setParameter('letter', '%'.trim($this->filters->letter).'%');
        }
    }

    public function buildCellReceivedButNotTested($row) {
        $mgr = $this->getModuleManager();
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('genoPelletModify')) {
            $builder->addButton('Stock management', $mgr->getUrl('leep_admin', 'geno_pellet', 'feature', 'manageStock', array('id' => $row['id'])), 'button-edit');
        }

        $itemId = $row['id'];
        $this->ensureStockWaiting($itemId);
        $html = $this->recivedNotTested[$itemId]['weight'] .' / €'.number_format($this->recivedNotTested[$itemId]['price'], 2);

        $threshold = intval($row['warningThreshold']);
        if ($this->recivedNotTested[$itemId]['weight'] < $threshold) {
            $html = '<b style="color: red">'.$html."</b>";
        }

        return $html.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoPelletModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_pellet', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_pellet', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellStock($row) {
        $mgr = $this->getModuleManager();
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($helperSecurity->hasPermission('genoPelletModify')) {
            $builder->addButton('Stock management', $mgr->getUrl('leep_admin', 'geno_pellet', 'feature', 'manageStock', array('id' => $row['id'])), 'button-edit');
        }

        $builder->addPopupButton('View history', $mgr->getUrl('leep_admin', 'geno_pellet', 'feature', 'viewStockEntryHistory', array('id' => $row['id'], 'entry' => 'idGenoPellet')), 'button-detail');

        $itemId = $row['id'];
        $this->ensureStockWaiting($itemId);
        $html = $this->stock[$itemId]['weight'] .' / €'.number_format($this->stock[$itemId]['price'], 2);

        $threshold = intval($row['warningThreshold']);
        if ($this->stock[$itemId]['weight'] < $threshold) {
            $html = '<b style="color: red">'.$html."</b>";
        }

        return $html.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellWaiting($row) {
        $itemId = $row['id'];
        $this->ensureStockWaiting($itemId);
        $html = $this->waiting[$itemId]['weight'] .' / €'.number_format($this->waiting[$itemId]['price'], 2);
        return $html;
    }


    private function ensureStockWaiting($idPellet) {
        if (!isset($this->stock[$idPellet])) {
            $this->stock[$idPellet] = [
                'weight' =>   0,
                'price' =>  0
            ];
        }
        if (!isset($this->waiting[$idPellet])) {
            $this->waiting[$idPellet] = [
                'weight' =>   0,
                'price' =>  0
            ];
        }
        if (!isset($this->recivedNotTested[$idPellet])) {
            $this->recivedNotTested[$idPellet] = [
                'weight' =>   0,
                'price' =>  0
            ];
        }
    }

    public function buildCellWarning($row) {
        $idPellet = $row['id'];
        $html = '';
        if (isset($this->expiryNumDays[$idPellet])) {
            $days = $this->expiryNumDays[$idPellet];
            if ($days < $row['expiryDateWarningThreshold']) {
                $htmlTpl = '<div style="color:%s">%s</div>';
                if ($days >= 0) {
                    $html .= sprintf($htmlTpl, 'orange', 'To be expired in ' . $days . ' day(s)');
                } else {
                    $html .= sprintf($htmlTpl, 'red', 'Expired '.abs($days).' day(s)');
                }
            }
        }
        return $html;
    }

    /*
    stock is about
     + recieved but not tested
     + tested and in stock
     - tested and in use
     - used up
     - expired
     - unusable
    waiting is about
     - order but not received
    */
    public function preGetResult() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Compute stocks
        $stockRules = array();
        $stockRules[] = '(p.received = 1)';
        $stockRules[] = '(p.section IN ('.implode(',', Constant::getStockSection()).'))';

        $waitingRules = array();
        $waitingRules[] = '(p.received = 0 OR p.received IS NULL)';
        $waitingRules[] = '(p.section IN ('.implode(',', Constant::getWaitingSection()).'))';

        $recivedNotTestedRules = array();
        $recivedNotTestedRules[] = '(p.received = 1)';
        $recivedNotTestedRules[] = '(p.section IN ('.implode(',', Constant::getRecivedNotTestedSection()).'))';

        $query = $em->createQueryBuilder();
        $query->select('
            p.idPellet,
            (SUM(CASE WHEN ('.implode(' AND ', $stockRules).') THEN p.loading ELSE 0 END)) AS stock,
            (SUM(CASE WHEN ('.implode(' AND ', $waitingRules).') THEN p.loading ELSE 0 END)) AS waiting,
            (SUM(CASE WHEN ('.implode(' AND ', $recivedNotTestedRules).') THEN p.loading ELSE 0 END)) AS recivedNotTested
            ')
            ->from('AppDatabaseMainBundle:GenoPelletEntry', 'p')
            ->addGroupBy('p.idPellet');

        $result = $query->getQuery()->getResult();

        foreach ($result as $pelletEntry) {
            $idPellet = $pelletEntry['idPellet'];
            $this->ensureStockWaiting($idPellet);

            $this->stock[$idPellet]['weight'] = $pelletEntry['stock'];
            $this->waiting[$idPellet]['weight'] = $pelletEntry['waiting'];
            $this->recivedNotTested[$idPellet]['weight'] = $pelletEntry['recivedNotTested'];
        }

        // Compute price
        $pelletPrices = [];
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPellet')->findAll();
        foreach ($result as $m) {
            $pelletPrices[$m->getId()] = floatval($m->getPricePerGram());
        }

        foreach ($this->stock as $pelletId => $v) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $this->stock[$pelletId]['price'] = $price * $this->stock[$pelletId]['weight'];
        }

        foreach ($this->waiting as $pelletId => $v) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $this->waiting[$pelletId]['price'] = $price * $this->waiting[$pelletId]['weight'];
        }

        foreach ($this->recivedNotTested as $pelletId => $v) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $this->recivedNotTested[$pelletId]['price'] = $price * $this->recivedNotTested[$pelletId]['weight'];
        }

        // Compute expiry
        $query = $em->createQueryBuilder();
        $query->select('p.idPellet, MIN(p.expiryDate) minExpiryDate')
            ->from('AppDatabaseMainBundle:GenoPelletEntry', 'p')
            ->andWhere('(p.expiryDateWarningDisabled = 0) OR (p.expiryDateWarningDisabled IS NULL)')
            ->andWhere('p.expiryDate IS NOT NULL')
            ->andWhere('p.section in (:warningSection)')
            ->setParameter('warningSection', Constant::expiryWarningSections())
            ->addGroupBy('p.idPellet');
        $result = $query->getQuery()->getResult();
        $today = new \DateTime();
        $today->setTime(0,0,0);
        foreach ($result as $r) {
            $expiryDate = \DateTime::createFromFormat('Y-m-d', $r['minExpiryDate']);
            $expiryDate->setTime(0,0,0);
            $idPellet = intval($r['idPellet']);
            $interval = date_diff($today, $expiryDate);
            $days = intval($interval->format('%R%a'));
            if (!isset($this->expiryNumDays[$idPellet]) || $this->expiryNumDays[$idPellet] < $days) {
                $this->expiryNumDays[$idPellet] = $days;
            }
        }
    }
}
