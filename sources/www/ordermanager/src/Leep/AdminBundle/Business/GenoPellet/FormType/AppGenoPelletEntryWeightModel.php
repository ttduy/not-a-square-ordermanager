<?php
namespace Leep\AdminBundle\Business\GenoPellet\FormType;

class AppGenoPelletEntryWeightModel {
    public $txt;
    public $weight;
    public $targetWeight;

    public $activeIngredients;
    public $actives;
    public $additives;
    public $weightActivesIngredients;
    public $weightActives;
    public $weightAdditives;
}
