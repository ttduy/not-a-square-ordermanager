<?php
namespace Leep\AdminBundle\Business\GenoPellet;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public $attachmentKey = null;
    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }

        if (empty($this->attachmentKey)) {
            $success = false;
            while (!$success) {
                $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
                $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
                if (!$duplicate) {
                    $success = true;
                }
            }
        }
    }

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPellet', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();

        // check if attached key is empty
        $entityAttachmentKey = $entity->getAttachmentKey();
        if(empty($entityAttachmentKey)){
            $this->generateAttachmentKey();
            $entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }

        $this->attachmentKey = $entity->getAttachmentKey();

        $model->name = $entity->getName();
        $model->letter = $entity->getLetter();
        $model->substanceName = $entity->getSubstanceName();
        $model->activeIngredientName = $entity->getActiveIngredientName();
        $model->sortOrderMixing = $entity->getSortOrderMixing();
        $model->warningThreshold = $entity->getWarningThreshold();
        $model->expiryDateWarningThreshold = $entity->getExpiryDateWarningThreshold();
        $model->notes = $entity->getNotes();
        $model->supplierDeclaration = $entity->getSupplierDeclaration();
        $model->pricePerGram = floatval($entity->getPricePerGram());

        // get the file
        $model->fileAttachment = array();
        $model->fileAttachment['file'] = $entity->getFileAttachment();

        return $model;
    }

    public function buildForm($builder) {
        // section attachment key
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genopellets/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Pellet Name',
            'required' => true
        ));

        $builder->add('letter', 'text', array(
            'label'    => 'Pellet Letter',
            'required' => true
        ));

        $builder->add('substanceName', 'text', array(
            'label'    => 'Substance Name',
            'required' => false
        ));

        $builder->add('activeIngredientName', 'text', array(
            'label'    => 'Active Ingredient Name',
            'required' => false
        ));

        $builder->add('sortOrderMixing', 'text', array(
            'label'    => 'Sort order for mixing',
            'required' => false
        ));

        $builder->add('warningThreshold', 'text', array(
            'label'    => 'Warning Threshold (g)',
            'required' => false
        ));

        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));

        $builder->add('pricePerGram', 'text', array(
            'label'    => 'Price (For every gram)',
            'required' => false
        ));

        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));

        $builder->add('supplierDeclaration', 'textarea', array(
            'label'    => 'Supplier Declaration',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get("doctrine")->getEntityManager();

        $name = trim($model->name);
        if (empty($name)) {
            $this->errors[] = "Empty name field";
            return false;
        }

        $letter = trim($model->letter);
        if (empty($letter)) {
            $this->errors[] = "Empty letter field";
            return false;
        }

        $check = $em->getRepository("AppDatabaseMainBundle:GenoPellet")->findOneByLetter($letter);
        if ($check && $check->getId() != $this->entity->getId()) {
            $this->errors[] = "This letter ($letter) is already exists!";
            return false;
        }

        // get user manager
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $this->entity->setName($name);
        $this->entity->setLetter($letter);
        $this->entity->setSubstanceName($model->substanceName);
        $this->entity->setActiveIngredientName($model->activeIngredientName);
        $this->entity->setSortOrderMixing($model->sortOrderMixing);
        $this->entity->setWarningThreshold($model->warningThreshold);
        $this->entity->setExpiryDateWarningThreshold(intval($model->expiryDateWarningThreshold));
        $this->entity->setNotes($model->notes);
        $this->entity->setSupplierDeclaration($model->supplierDeclaration);
        $this->entity->setPricePerGram(floatval($model->pricePerGram));

        /// Assign from system data: modified date, modified by, created date
        $this->entity->setModifiedDate(new \DateTime());
        $this->entity->setModifiedBy($userManager->getUser()->getId());

        // write to database
        $em->flush();

        parent::onSuccess();
    }
}
