<?php
namespace Leep\AdminBundle\Business\GenoPellet;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $letter;
    public $substanceName;
    public $activeIngredientName;
    public $sortOrderMixing;
    public $warningThreshold;
    public $expiryDateWarningThreshold;
    public $notes;
    public $supplierDeclaration;
    public $pricePerGram;

    public $attachmentKey;
    public $fileAttachment;
    public $attachment;
}
