<?php
namespace Leep\AdminBundle\Business\GenoPellet;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }

    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $model = new CreateModel();

        // generate attachment key
        $this->generateAttachmentKey();
        $model->attachmentKey = $this->attachmentKey;

        // 6 months
        $model->expiredIn = 180;
        $model->pricePerGram = 0;

        return $model;
    }

    public function buildForm($builder) {
        // process for attachment Key
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label' =>          'Attachments',
            'required' =>       false,
            'key' =>            'genopellets/'.$this->attachmentKey,
            'snapshot' =>       false,
            'allowCloneDir' =>  true
        ));

        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Pellet Name',
            'required' => true
        ));

        $builder->add('letter', 'text', array(
            'label'    => 'Pellet Letter',
            'required' => true
        ));

        $builder->add('substanceName', 'text', array(
            'label'    => 'Substance Name',
            'required' => false
        ));

        $builder->add('activeIngredientName', 'text', array(
            'label'    => 'Active Ingredient Name',
            'required' => false
        ));

        $builder->add('sortOrderMixing', 'text', array(
            'label'    => 'Sort order for mixing',
            'required' => false
        ));

        $builder->add('warningThreshold', 'text', array(
            'label'    => 'Warning Threshold (g)',
            'required' => false
        ));

        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));

        $builder->add('pricePerGram', 'text', array(
            'label'    => 'Price (for every gram)',
            'required' => false
        ));

        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));

        $builder->add('supplierDeclaration', 'textarea', array(
            'label'    => 'Supplier Declaration',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 80
            )
        ));
    }

    public function onSuccess() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $name = trim($model->name);
        if (empty($name)) {
            $this->errors[] = "Empty name field";
            return false;
        }

        $letter = trim($model->letter);
        if (empty($letter)) {
            $this->errors[] = "Empty letter field";
            return false;
        }

        $check = $em->getRepository("AppDatabaseMainBundle:GenoPellet")->findOneByLetter($letter);
        if ($check) {
            $this->errors[] = "This letter ($letter) is already exists!";
            return false;
        }

        $pellet = new Entity\GenoPellet();
        $pellet->setName($name);
        $pellet->setLetter($letter);
        $pellet->setSubstanceName($model->substanceName);
        $pellet->setActiveIngredientName($model->activeIngredientName);
        $pellet->setSortOrderMixing($model->sortOrderMixing);
        $pellet->setWarningThreshold($model->warningThreshold);
        $pellet->setExpiryDateWarningThreshold(intval($model->expiryDateWarningThreshold));
        $pellet->setNotes($model->notes);
        $pellet->setSupplierDeclaration($model->supplierDeclaration);
        $pellet->setPricePerGram(floatval($model->pricePerGram));
        $pellet->setCreatedDate(new \DateTime());
        $pellet->setModifiedDate(new \DateTime());
        $pellet->setCreatedBy($userManager->getUser()->getId());
        $pellet->setModifiedBy($userManager->getUser()->getId());
        $pellet->setAttachmentKey($model->attachmentKey);

        $em->persist($pellet);
        $em->flush();

        parent::onSuccess();
    }
}
