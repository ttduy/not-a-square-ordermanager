<?php
namespace Leep\AdminBundle\Business\PaymentNoTaxText;

class Constant {
    public static function get($container) {
        $em = $container->get('doctrine')->getEntityManager();
        $paymentNoTaxText = $em->getRepository('AppDatabaseMainBundle:PaymentNoTaxText')->findBy([], ['sortOrder' => 'DESC']);
        $arrayList = [0 => ''];
        foreach ($paymentNoTaxText as $key => $value) {
            $arrayList[$value->getId()] = $value->getName();
        }

        return $arrayList;
    }
}
