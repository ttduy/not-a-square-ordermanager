<?php
namespace Leep\AdminBundle\Business\PaymentNoTaxText;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'textGermany', 'textEnglish', 'sortOrder', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',            'width' => '20%', 'sortable' => 'true'),
            array('title' => 'German text',     'width' => '30%', 'sortable' => 'true'),
            array('title' => 'English text',    'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Sort order',      'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Action',          'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:PaymentNoTaxText', 'p');

        if (trim($this->filters->textGermany) != '') {
            $queryBuilder->andWhere('p.textGermany LIKE :textGermany')
                ->setParameter('textGermany', '%'.trim($this->filters->textGermany).'%');
        }

        if (trim($this->filters->textEnglish) != '') {
            $queryBuilder->andWhere('p.textEnglish LIKE :textEnglish')
                ->setParameter('textEnglish', '%'.trim($this->filters->textEnglish).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('billingModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'payment_no_tax_text', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'payment_no_tax_text', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        return $builder->getHtml();
    }
}
