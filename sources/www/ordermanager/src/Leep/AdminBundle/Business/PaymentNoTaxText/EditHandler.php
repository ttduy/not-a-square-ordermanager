<?php
namespace Leep\AdminBundle\Business\PaymentNoTaxText;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PaymentNoTaxText', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->textEnglish = $entity->getTextEnglish();
        $model->textGermany = $entity->getTextGermany();
        $model->name = $entity->getName();
        $model->sortOrder = $entity->getSortOrder();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('textGermany', 'text', array(
            'label'    => 'German text',
            'required' => true
        ));
        $builder->add('textEnglish', 'text', array(
            'label'    => 'English text',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $this->entity->setName($model->name);
        $this->entity->setSortOrder($model->sortOrder);
        $this->entity->setTextGermany($model->textGermany);
        $this->entity->setTextEnglish($model->textEnglish);
        parent::onSuccess();
    }
}
