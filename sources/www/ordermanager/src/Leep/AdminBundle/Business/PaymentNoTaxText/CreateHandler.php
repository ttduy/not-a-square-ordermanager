<?php
namespace Leep\AdminBundle\Business\PaymentNoTaxText;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('textGermany', 'text', array(
            'label'    => 'German text',
            'required' => true
        ));
        $builder->add('textEnglish', 'text', array(
            'label'    => 'English text',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $entity = new Entity\PaymentNoTaxText();
        $entity->setName($model->name);
        $entity->setTextGermany($model->textGermany);
        $entity->setTextEnglish($model->textEnglish);
        $entity->setSortOrder($model->sortOrder);
        $em->persist($entity);
        $em->flush();
        parent::onSuccess();
    }
}
