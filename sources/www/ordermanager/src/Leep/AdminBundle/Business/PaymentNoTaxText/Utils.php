<?php
namespace Leep\AdminBundle\Business\PaymentNoTaxText;

class Utils {
    public static function getText($container, $id) {
        $em = $container->get('doctrine')->getEntityManager();
        $paymentNoTaxText = $em->getRepository('AppDatabaseMainBundle:PaymentNoTaxText')->findOneById($id);
        if ($paymentNoTaxText) {
            return [
                'textGermany' => $paymentNoTaxText->getTextGermany(),
                'textEnglish' => $paymentNoTaxText->getTextEnglish()
            ];
        }

        return [
            'textGermany' => '',
            'textEnglish' => ''
        ];
    }
}
