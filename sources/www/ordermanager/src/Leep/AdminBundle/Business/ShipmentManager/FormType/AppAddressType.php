<?php
namespace Leep\AdminBundle\Business\ShipmentManager\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppAddressType extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_address_type';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

         $manager = $this->container->get('easy_module.manager');
        $view->vars['addressTypes'] = ['Partner', 'DistributionChannels'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $addressTypes = [0=>'Partner', 1=>'DistributionChannels'];
        $builder->add('addressType', 'choice', array(
            'label'    => 'Address Type',
            'required' => true,
            'choices'  => $addressTypes
        ));
        $em = $this->container->get('doctrine')->getEntityManager();
        $name='';
        $arrayAdresstmp=array();
        $arrayAdress[]=array();
         foreach ($addressTypes as $DC_partner=>$tmp) {

            if($DC_partner==0){
                $name='Partner';
                $arrayAdresstmp=$em->createQueryBuilder()
                                ->select('p.id, p.partner as name')
                                ->from('AppDatabaseMainBundle:Partner', 'p')
                                ->orderBy('p.partner', 'ASC')
                                ->getQuery()->getResult();
            }
            else{
                $name='Distribution Channels';
                $arrayAdresstmp=$em->createQueryBuilder()
                                ->select('p.id, p.distributionchannel as name')
                                ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
                                 ->orderBy('p.distributionchannel', 'ASC')
                                ->getQuery()->getResult();
            }
           foreach ($arrayAdresstmp as $address) {
                    $arrayAdress[$address['id']] = $address['name'];
            }
            $builder->add('DC_partner'.$DC_partner, 'choice', array(
                'label'     => $name,
                'required'  => false,
                'choices'   => $arrayAdress,
                'attr'       => [
                    'onChange'  => 'changeTarget(this)'
                ]
            ));
        }

    }

}
