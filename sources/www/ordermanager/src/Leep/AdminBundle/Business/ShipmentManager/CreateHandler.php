<?php
namespace Leep\AdminBundle\Business\ShipmentManager;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $container;
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->container = $this->container;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('addressType', 'app_address_type', array(
        ));
        $builder->add('address', 'text', array(
            'label'    => 'Address',
            'required' => true
        ));
        $builder->add('createBy', 'choice', array(
            'label'    => 'Create By',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'   =>array('Open'=>'Open', 'Rejected'=>'Rejected', 'Shipped'=>'Shipped')
        ));
        $builder->add('comment', 'textarea', array(
              'label'    => 'Comment',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
                )
        ));
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $idTarget = 'DC_partner'.$model->addressType['addressType'];
        if($model->address==null){
            $this->errors[] = "Error!";
            $this->getForm()->get('address')->addError(new FormError("not blank"));
            return false;
        }
        $shipmentManager = new Entity\ShipmentManager();
        $shipmentManager->setAddress($model->address);
        $shipmentManager->setCreateBy($model->createBy);
        $shipmentManager->setStatus($model->status);
        $shipmentManager->setComment($model->comment);
        $em->persist($shipmentManager);
        $em->flush();
         Business\ShipmentManager\Utils::updateEntryHistory($this->container, $shipmentManager);
        parent::onSuccess();

    }

}
