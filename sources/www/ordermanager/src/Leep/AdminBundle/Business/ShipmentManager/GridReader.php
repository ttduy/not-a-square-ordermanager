<?php
namespace Leep\AdminBundle\Business\ShipmentManager;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
 public $filters;
    public function getColumnMapping() {
        return array('address', 'createBy', 'status', 'comment', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Address',      'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Create By',     'width' => '15%', 'sortable' => 'false'),
            array('title' => 'status',       'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Comment',      'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',       'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
         return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ShipmentManagerReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ShipmentManager', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
         $builder->addPopupButton('View history', $mgr->getUrl('leep_admin', 'shipment_manager', 'feature', 'viewShipmentHistory', array('id' => $row->getId())), 'button-detail');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('companyModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'shipment_manager', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'shipment_manager', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
