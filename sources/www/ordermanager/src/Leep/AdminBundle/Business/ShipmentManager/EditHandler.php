<?php
namespace Leep\AdminBundle\Business\ShipmentManager;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ShipmentManager', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
    
        $model->addressType['addressType'] =  0;
        $model->addressType['DC_partner0']=0;
        $model->addressType['DC_partner1']=0;
        

        //$model->Dc_partner = $entity->getIdTarget();
        $model->address = $entity->getAddress();
        $model->createBy = $entity->getCreateBy();
        $model->status = $entity->getStatus();
        $model->comment = $entity->getComment();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('addressType', 'app_address_type', array(
        ));
        $builder->add('address', 'text', array(
            'label'    => 'Address',
            'required' => true
        ));
        $builder->add('createBy', 'choice', array(
            'label'    => 'Create By',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_List')
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'   =>array('Open'=>'Open', 'Rejected'=>'Rejected', 'Shipped'=>'Shipped')
        ));
        $builder->add('comment', 'textarea', array(
              'label'    => 'Comment',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
                )
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $idTarget = 'DC_partner'.$model->addressType['addressType'];
        if($model->address==null){
            $this->errors[] = "Error!";
            $this->getForm()->get('address')->addError(new FormError("not blank"));
            return false;
        }

        $this->entity->setAddress($model->address);
        $this->entity->setCreateBy($model->createBy);
        $this->entity->setStatus($model->status);
        $this->entity->setComment($model->comment);
        Utils::updateEntryHistory($this->container, $this->entity);
        parent::onSuccess();
    }
}
