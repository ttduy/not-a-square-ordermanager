<?php
    namespace Leep\AdminBundle\Business\ShipmentManager;

    use Leep\AdminBundle\Business\Base\AppFilterHandler;
    use Leep\AdminBundle\Business;

    /**
     *
     */
    class FilterHandler extends AppFilterHandler
    {
        public function getDefaultFormModel() {
            $model = new FilterModel();
            return $model;
        }

        public function  buildForm($builder){
            // create mapping to get more library
            $mapping = $this->container->get('easy_mapping');
            $builder->add('shipmentNumber', 'text', array(
                'label' => 'Shipment Number',
                'required' => true
            ));
            $builder->add('status', 'text', array(
                'label' => 'Status',
                'required' => true
            ));

            return $builder;
        }
    }

?>