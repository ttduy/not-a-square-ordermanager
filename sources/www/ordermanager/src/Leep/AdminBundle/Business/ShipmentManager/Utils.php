<?php
namespace Leep\AdminBundle\Business\ShipmentManager;

use App\Database\MainBundle\Entity;

class Utils {
    public static function updateEntryHistory($container, $entry) {

        $em = $container->get('doctrine')->getEntityManager();
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        $history = new Entity\ShipmentHistory();
        $history->setIdShipment($entry->getId());
        $history->setAddress($entry->getAddress());
        $history->setCreateBy($entry->getCreateBy());
        $history->setDate(new \DateTime());
        $history->setChangeBy($currentUserId);
        $history->setStatus($entry->getStatus());
        $history->setComment($entry->getComment());
        $em->persist($history);
        $em->flush();
    }
}