<?php
namespace Leep\AdminBundle\Business\Report;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class GenerateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new GenerateModel();        
        
        return $model;
    }

    public function buildForm($builder) {      
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idType', 'choice', array(
            'required' => true,
            'label'    => 'Task',
            'choices'  => $mapping->getMapping('LeepAdmin_Report_TaskType')
        ));                
        $builder->add('idReport', 'choice', array(
            'required' => true,
            'label'    => 'Report',
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('distributionChannels', 'choice', array(
            'required' => false,
            'label'    => 'Distribution channels',
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple' => 'multiple',
            'attr'     => array(
                'style'    => 'height: 400px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $mapping = $this->container->get('easy_mapping');  


        $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($model->idReport);
        $languages = array();
        $rows = $em->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($rows as $r) {
            $languages[$r->getId()] = $r->getName();
        }


        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere('p.id in (:dcArray)')
            ->setParameter('dcArray', $model->distributionChannels);
        $distributionChannels = $query->getQuery()->getResult();

        foreach ($distributionChannels as $dc) {
            $idLanguage = $dc->getIdLanguage();
            if (isset($languages[$idLanguage])) {
                $languageName = $languages[$idLanguage];
            }
            else {
                $idLanguage = 1;
                $languageName = 'GERMAN';
            }

            $description = array(
                'Report: '.$report->getName(),
                'Language: '.$languageName,
                'DC: '. $dc->getDistributionChannel()
            );  

            // Load price
            $reportData = array();
            $reportData['dcName'] = $dc->getDistributionChannel();
            $reportData['products'] = array();
            $prices = $em->getRepository('AppDatabaseMainBundle:Prices')->findBy(array(
                'pricecatid' => $dc->getPriceCategoryId()
            ));
            foreach ($prices as $p) {
                $pname = $mapping->getMappingTitle('LeepAdmin_Product_List', $p->getProductId());
                $reportData['products'][$pname] = number_format($p->getPrice(), 2);
            }
            $reportData['categories'] = array();
            $prices = $em->getRepository('AppDatabaseMainBundle:CategoryPrices')->findBy(array(
                'pricecatid' => $dc->getPriceCategoryId()
            ));
            foreach ($prices as $p) {
                $cname = $mapping->getMappingTitle('LeepAdmin_Category_List', $p->getCategoryId());
                $reportData['categories'][$cname] = number_format($p->getPrice(), 2);
            }

            // Build
            $input = array(
                'name'                  => $report->getName(),
                'idReport'              => $report->getId(),
                'idLanguage'            => $idLanguage,
                'idDistributionChannel' => $dc->getId(),
                'idFormulaTemplate'     => $report->getIdFormulaTemplate(),
                'reportData'            => $reportData
            );    

            // Add to report queue
            $reportQueue = new Entity\ReportQueue();
            $reportQueue->setIdWebUser($userManager->getUser()->getId());
            $reportQueue->setInputTime(new \DateTime());
            $reportQueue->setDescription(implode('<br/>', $description));
            $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
            $reportQueue->setInput(serialize($input));
            $reportQueue->setIdReport($report->getId());
            $reportQueue->setIdCustomer(0);
            $reportQueue->setIsSaved(true);
            $reportQueue->setIdJobType(Business\ReportQueue\Constant::JOB_TYPE_BUILD_DISTRIBUTION_CHANNEL_PRICE);
            $em->persist($reportQueue);
        }
        $em->flush();

        $this->messages = array('Report job has been placed!');
        parent::onSuccess();
    }
}