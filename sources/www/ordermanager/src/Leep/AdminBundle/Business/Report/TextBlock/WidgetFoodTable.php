<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business\Report\Constants;

class WidgetFoodTable {
    public $container;
    public $pdf;
    public $parameter;
    public $extraParameter = array();
    public $smileyConversion = array();
    public $style;
    public $overshootFactor = 1;
    public $builder;
    public $isDebug = false;
    public $isDisableSmileys = false;
    public $isDisableArticles = false;
    public $isDisableArticlesAmount = false;
    public $isDisableSmileysColumn = false;
    public $isDisableWarningColumn = array();
    public $isShowFooter = false;

    public $isHideArticles = false;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public $colorScales = array(
        0  => 'Ff8989',
        1  => 'Ff8989',
        2  => 'Ff8989',
        3  => 'Ff8989',
        4  => 'Ff8989',
        5  => 'Ff8989',
        6  => 'Ff8989',
        7  => 'Ff8989',
        8  => 'Ff8989',
        9  => 'Ff8989',
        10 => 'Ff8989',
        11 => 'Ff8989',
        12 => 'Ff8989',
        13 => 'Ff8989',
        14 => 'Ff8989',
        15 => 'Ff8989',
        16 => 'Ff8989',
        17 => 'Ff8989',
        18 => 'Ff8989',
        19 => 'Ff8989',
        20 => 'Ff8989',
        21 => 'Ff8989',
        22 => 'Fb8b8b',
        23 => 'Ee9191',
        24 => 'E29696',
        25 => 'caa1a1',
        26 => 'Bda7a7',
        27 => 'Afadad',
        28 => 'Afb6a6',
        29 => 'Afbf9e',
        30 => 'B0c896',
        31 => 'B1d18e',
        32 => 'B2da86',
        33 => 'B2de82',
        34 => 'B2da86',
        35 => 'B2da86',
        36 => 'B2da86',
        37 => 'B2da86',
        38 => 'B2da86',
        39 => 'B2da86',
        40 => 'B2da86',
        41 => 'B2da86',
        42 => 'B2da86',
        43 => 'B2da86',
        44 => 'B2da86'
    );

    public $TOP_MARGIN = 40;
    public $LEFT_MARGIN = 35;
    public $MAX_SLICE_PER_PAGE = 32;
    public $SLICE_HEIGHT = 17;

    private function parseIngredient($ingredientData) {
        $rows = explode("\n", $ingredientData);
        $ingredients = array();
        foreach ($rows as $r) {
            $arr = explode('=', $r);
            if (count($arr) == 2) {
                $ingredients[trim($arr[0])] = floatval($arr[1]);
            }
        }
        return $ingredients;
    }

    public function loadParameters() {
        $doctrine = $this->container->get('doctrine');

        // Load food parameter
        $config = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableParameter')->findAll();
        foreach ($config as $c) {
            $this->parameter = $c;
            break;
        }

        // Extra variables
        $arr = array('warnung', 'laktose', 'gluten', 'g', 'kcal', 'eiw', 'koh', 'fett');
        foreach ($arr as $key) {
            $getMethod = 'get'.ucfirst($key).'Id';

            $record = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findOneById($this->parameter->$getMethod());
            if ($record) {
                $this->extraParameter[$key] = $record->getVariableName();
            }
        }


        // Smiley conversion
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableSmileyConversion')->findAll();
        foreach ($results as $r) {
            $this->smileyConversion[] = array(
                'from' => $r->getFromValue(),
                'to'   => $r->getToValue(),
                'value' => $r->getNormalizedValue()
            );
        }
    }

    public function parseInput($arr, $input) {
        $rows = explode("\n", $input);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $arr[trim($k)] = trim($v);
            }
        }
        return $arr;
    }

    public function maxLength($str, $maxLength) {
        if (mb_strlen($str) > $maxLength) {
            return mb_substr($str, 0, $maxLength-2).'..';
        }
        return $str;
    }

    public function getWorkaroundMarginTop($pt) {
        return '<span style="font-size: 1px;">'.str_repeat('&nbsp;<br/>', $pt).'</span>';
    }

    public function breakFoodIntoPages($foods, $numPerPage) {
        return array_chunk($foods, $numPerPage, true);
    }

    public function roundTo5($value) {
        return ceil($value/5)*5;
        //return $value;
    }

    public function displayFooter() {
        if ($this->isShowFooter) {
            $this->pdf->StartTransform();

            $this->pdf->Rotate(-90, 0, 40);

            $pageNo = $this->pdf->getPageNumGroupAlias();
            $pageTotal = $this->pdf->getPageGroupAlias();
            if (!empty($this->pdf->footerData)) {
                $text = $this->pdf->footerData['textPage'].' '.$pageNo.' '.$this->pdf->footerData['textOf'].' '.$pageTotal;
                $text = '<p style="color: '.$this->pdf->footerData['lineColor'].'">'.$text.'</p>';
            }
            else {
                $text = '<p style="color: #000000">'.$pageNo.' /'.$pageTotal."</p>";
            }
            $this->pdf->setFont('pdfahelvetica', 'B', 10);
            $this->pdf->writeHTMLCell($w=0, $h=0, $x=$this->pdf->footerXPos, $y=0, $text, $border=0, $ln=1, $fill=0);
            $this->pdf->setFont($this->builder->fontMap['regular'], '', 10, '', true);

            $this->pdf->StopTransform();
        }
    }

    public function build($data) {
        mb_internal_encoding("UTF-8");
        $this->pdf->SetAutoPageBreak(FALSE);
        $this->pdf->setFont($this->builder->fontMap['regular'], '', 10, '', true);

        $doctrine = $this->container->get('doctrine');
        $this->loadParameters();

        ///////////////////////////////////////////////////////////
        // x. Parse INPUT
        // Check if show smiley
        if (isset($this->builder->configData['Smileys']) && ($this->builder->configData['Smileys'] == 'DISABLED')) {
            $this->isDisableSmileys = true;
        }

        if (isset($this->builder->configData['Articles']) && ($this->builder->configData['Articles'] == 'DISABLED')) {
            $this->isDisableArticles = true;
        }

        if (isset($this->builder->configData['Articles']) && ($this->builder->configData['Articles'] == 'HIDE')) {
            $this->isHideArticles = true;
        }

        if (isset($this->builder->configData['ArticlesAmount']) && ($this->builder->configData['ArticlesAmount'] == 'DISABLED')) {
            $this->isDisableArticlesAmount = true;
        }

        if (isset($this->builder->configData['SmileysColumn']) && ($this->builder->configData['SmileysColumn'] == 'DISABLED')) {
            $this->isDisableSmileysColumn = true;
        }
        if (isset($this->builder->configData['ArticlesPortionColumn']) && ($this->builder->configData['ArticlesPortionColumn'] == 'DISABLED')) {
            $this->isDisableWarningColumn['ArticlesPortion'] = true;
        }
        if (isset($this->builder->configData['GlutenColumn']) && ($this->builder->configData['GlutenColumn'] == 'DISABLED')) {
            $this->isDisableWarningColumn['Gluten'] = true;
        }
        if (isset($this->builder->configData['LactoseColumn']) && ($this->builder->configData['LactoseColumn'] == 'DISABLED')) {
            $this->isDisableWarningColumn['Lactose'] = true;
        }
        if (isset($this->builder->configData['WarningColumn']) && ($this->builder->configData['WarningColumn'] == 'DISABLED')) {
            $this->isDisableWarningColumn['Warning'] = true;
        }
        if (isset($this->builder->configData['AllergyColumn']) && ($this->builder->configData['AllergyColumn'] == 'DISABLED')) {
            $this->isDisableWarningColumn['Allergy'] = true;
        }


        // Parse debug
        if (isset($data['isDebug']) && $data['isDebug'] == 1) {
            $this->isDebug = true;
        }

        // Bar calculation
        $idBarCalculation = isset($data['idBarCalculation']) ? intval($data['idBarCalculation']) : 0;

        // Parse food list
        $foodDefinition = explode("\n", $data['foodDefinition']);

        // Parse label
        $labels = array(
            'scale' => 'Risiko fur Ubergewicht und Artikel',
            'smiley' => 'Gesund laut Ihren Genen',
            'label1' => 'Artikel/Prot',
            'label2' => 'Warnung',
            'label3' => 'Laktose',
            'label4' => 'Gluten',
            'label5' => 'Allegie',
            'foodItem' => 'Getranke (pro 100 g bzw. 100ml)',
            'pro'       => 'Pro typischer Portion',
            'proG'      => 'g',
            'proKcal'   => 'kcal',
            'proEiw'    => 'Eiw',
            'proKoh'    => 'Koh',
            'proFett'   => 'Fett',
            'gPerArtikel' => 'g / Artikel',
            'colorBarAnyAmount' => 'beliebig viel – kein Artikel'
        );
        $labels = $this->parseInput($labels, $data['label']);

        $labels['proG'] = $this->builder->localizator->config['food_table_amount.label'];

        // Parse style
        $style = array(
            'colorHead'    => '#203764',
            'bgcolorHead'  => '#FFFFFF',
            'colorBody'    => '#d9e1f2',
            'bgcolorBody'  => '#000000',
            'reduceProductNameWidth' => 0,
            'footer'       => 'Hide'
        );
        $style = $this->parseInput($style, $data['style']);
        $this->style = $style;

        if ($style['footer'] === 'Show') {
            $this->isShowFooter = true;
        }

        // Parse variables
        $variables = array(
            'kcal'    => 100,
            'eiw'    => 20,
            'koh'    => 30,
            'fett'     => 20,
            'colorBarMin' => 20,
            'colorBarMax' => 800,
            'colorBarThreshold' => 800,
            'sportCarbs' => 0,
            'sportFat'   => 0,
            'sportProtein' => 0
        );

        $variables = $this->parseInput($variables, $data['otherVariables']);

        // Parse risk
        $risks = array();
        $risks = $this->parseInput($risks, $data['risks']);

        // Parse warning
        $rawWarnings = array();
        $rawWarnings = $this->parseInput($rawWarnings, $data['warning']);
        $warnings = array();
        foreach ($rawWarnings as $k => $v) {
            if (strpos($k, '.') !== FALSE) {
                $key = explode('.', $k);

                if (!isset($warnings[$key[0]])) {
                    $warnings[$key[0]] = array();
                }
                $warnings[$key[0]][] = $v;
            }
            else {
                $warnings[$k] = $v;
            }
        }

        // Ingredient display
        $ingredientDisplay = array();
        $ingredientDisplayArr = explode("\n", $data['ingredientDisplay']);
        foreach ($ingredientDisplayArr as $k => $v) {
            if (trim($v) != '') {
                $ingredientDisplay[$k] = trim($v);
            }
        }

        // Overshoot factor
        $this->overshootFactor = floatval($data['overshootFactor']);

        // Scales
        $this->scales = array();
        $rows = explode("\n", $data['scales']);
        $i = 0;
        foreach ($rows as $r) {
            $a = explode(';', $r);
            if (count($a) == 2) {
                $k = intval($a[0]);
                $c = trim($a[1]);
                $this->scales[$i++] = array($k, $c);
            }
        }

        ////////////////////////////////////////////////
        // x. PROCESS
        // Disable
        /*if ($this->isDisableSmileys) {
            $labels['label2'] = '';
            $labels['label3'] = '';
            $labels['label4'] = '';
        }*/
        if ($this->isDisableArticles || $this->isHideArticles) {
            $labels['label1'] = '';
        }

        // Load food
        $foods = array();
        $foodIds = array();
        foreach ($foodDefinition as $r) {
            $r = trim($r);
            $a = explode('|', $r);
            if (count($a) == 3) {
                $r = trim($a[0]);
                $foods[] = array(
                    'id'          => $r,
                    'bgcolorBody' => trim($a[1]),
                    'overshootFactor' => floatval($a[2])
                );
            }
            else {
                $foods[] = array(
                    'id' => $r
                );
            }

            $foodIds[] = $r;
        }

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FoodTableFoodItem', 'p')
            ->andWhere('p.foodId in (:foodList)')
            ->setParameter('foodList', $foodIds);
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $foodId = $r->getFoodId();
            foreach ($foods as $id => $food) {
                if ($food['id'] == $foodId) {
                    $foods[$id]['name'] = $r->getName();
                    $foods[$id]['ingredients'] = $this->parseIngredient($r->getIngredientData());
                }
            }
        }

        // Clean up
        foreach ($foods as $id => $v) {
            unset($v['id']);

            if (empty($v)) {
                unset($foods[$id]);
            }
        }


        // Ingredients
        $ingredients = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
        foreach ($results as $r) {
            $ingredients[trim($r->getVariableName())] = array(
                'name' => $r->getName(),
                'riskAmountMax' => $r->getRiskAmountMax()
            );
        }

        // Check last tools
        if ($this->builder->lastTextTool['idTextToolType'] != Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE &&
            $this->builder->lastTextTool['idTextToolType'] != Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE_2) {
            // Force to create new page
            // Delete portrait page
            $this->pdf->deletePage($this->pdf->getPage());

            // Create new page
            $this->pdf->addPage('L');
            $this->pdf->setPageUnit('pt');

            // Define current position
            $this->builder->currentFoodTablePosition = 0;

            $this->displayFooter();
        }

        $foodList = $foods;
        while (!empty($foodList)) {
            // Add header
            $this->builder->currentFoodTablePosition += 4;

            // We don't have enough space
            if ($this->builder->currentFoodTablePosition >= $this->MAX_SLICE_PER_PAGE- 2) {
                // Make new page
                $this->pdf->addPage('L');
                //$this->pdf->setPageUnit('pt');
                $this->builder->currentFoodTablePosition = 4;

                $this->displayFooter();
            }

            // Compute startY
            $startY = $this->TOP_MARGIN + ($this->builder->currentFoodTablePosition - 4) * $this->SLICE_HEIGHT;

            // Populate foods
            $foods = array();
            while (!empty($foodList) && $this->builder->currentFoodTablePosition < $this->MAX_SLICE_PER_PAGE) {
                $foods[] = array_shift($foodList);
                $this->builder->currentFoodTablePosition++;
            }

            /////////////////////////////////////////////////////
            // x. Write header
            $extraProductWidth = 0;
            $curX = 5 + $this->LEFT_MARGIN;

            if (!$this->isHideArticles) {
                $html = '<table cellspacing="1pt">';
                $html .= '<tbody>';
                $html .= '<tr><td height="48pt" width="135pt" style="background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center;">'.$this->getWorkaroundMarginTop(5).$labels['scale'].'</td></tr>';
                $html .= '</tbody></table>';
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $curX += 1;

                $html = $this->getScale(45, 14);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY+50, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $curX += 137;
            } else {
                $extraProductWidth += 138;
            }

            if ($this->isDisableSmileysColumn) {
                $extraProductWidth += 63;
            }
            else {
                $html = '<table cellspacing="1">';
                $html .= '<tbody>';
                $html .= '<tr><td height="63pt" width="60pt" style="background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(5).'<font style="font-size: 8">'.$labels['smiley'].'</font></td></tr>';
                $html .= '</tbody></table>';
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $curX += 63;
            }

            if (!empty($this->isDisableWarningColumn)) {
                $html = '<table cellspacing="1">';
                $html .= '<tbody>';
                if (!isset($this->isDisableWarningColumn['ArticlesPortion'])) {
                    $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label1'].'</td></tr>';
                }
                if (!isset($this->isDisableWarningColumn['Warning'])) {
                    $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label2'].'</td></tr>';
                }
                if (!isset($this->isDisableWarningColumn['Lactose'])) {
                    $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label3'].'</td></tr>';
                }
                if (!isset($this->isDisableWarningColumn['Gluten'])) {
                    $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label4'].'</td></tr>';
                }
                if (!isset($this->isDisableWarningColumn['Allergy'])) {
                    $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label5'].'</td></tr>';
                }
                $html .= '</tbody></table>';
                if (count($this->isDisableWarningColumn) == 5) {
                    $html = '';
                    $curX -= 2;
                    $extraProductWidth += 2;
                }
                $this->pdf->StartTransform();
                $this->pdf->Translate(0, 65);
                $this->pdf->Rotate(90, $curX, $startY);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $this->pdf->StopTransform();
                $curX += 62 - count($this->isDisableWarningColumn) * 12;
                $extraProductWidth += count($this->isDisableWarningColumn) * 12;
            }
            else {
                $html = '<table cellspacing="1">';
                $html .= '<tbody>';
                $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label1'].'</td></tr>';
                $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label2'].'</td></tr>';
                $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label3'].'</td></tr>';
                $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label4'].'</td></tr>';
                $html .= '<tr><td width="63pt" height="11pt" style="font-size: 7; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left">'.$this->getWorkaroundMarginTop(1).$labels['label5'].'</td></tr>';
                $html .= '</tbody></table>';
                $this->pdf->StartTransform();
                $this->pdf->Translate(0, 65);
                $this->pdf->Rotate(90, $curX, $startY);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $this->pdf->StopTransform();
                $curX += 62;
            }

            // Product
            $productNameWidth = 415 - intval($this->style['reduceProductNameWidth']) + $extraProductWidth;
            foreach ($ingredientDisplay as $ingredientId) {
                if (isset($ingredients[$ingredientId])) {
                    $productNameWidth -= 9;
                }
            }

            $html = '<table cellspacing="1pt">';
            $html .= '<tbody>';
            $html .= '<tr><td height="63pt" width="'.$productNameWidth.'" style="background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(5).$labels['foodItem'].'</td></tr>';
            $html .= '</tbody></table>';
            $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
            $curX += 3 + $productNameWidth;

            $height1 = 45;
            $height2 = 17;
            if ($this->builder->idLanguage == 29) { // Hack Chinese
                $height1 = 35;
                $height2 = 27;
            }

            // Portion
            $html = '<table cellspacing="1pt">';
            $html .= '<tbody>';
            $html .= '<tr><td height="'.$height1.'pt" width="104pt" style="background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center" colspan=5>'.$this->getWorkaroundMarginTop(5).$labels['pro'].'</td></tr>';
            $html .= '<tr>';
            $html .= '<td height="'.$height2.'pt" width="20pt" style="font-size: 8; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$labels['proG'].'</td>';
            $html .= '<td height="'.$height2.'pt" width="20pt" style="font-size: 8; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$labels['proKcal'].'</td>';
            $html .= '<td height="'.$height2.'pt" width="20pt" style="font-size: 8; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$labels['proEiw'].'</td>';
            $html .= '<td height="'.$height2.'pt" width="20pt" style="font-size: 8; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$labels['proKoh'].'</td>';
            $html .= '<td height="'.$height2.'pt" width="20pt" style="font-size: 8; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$labels['proFett'].'</td>';
            $html .= '</tr>';
            $html .= '</tbody></table>';
            $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$startY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
            $curX += 42;


            // Ingredient header
            if (!empty($ingredientDisplay)) {
                $html = '<table cellspacing="1" width="63">';
                $html .= '<tbody>';
                foreach ($ingredientDisplay as $ingredientId) {
                    if (isset($ingredients[$ingredientId])) {
                        $ingredient = $ingredients[$ingredientId];
                        $name = $this->builder->translateBlock($ingredient['name']);
                        if (strpos($name, 'INVALID') !== FALSE) {
                            $name = $ingredientId;
                        }

                        $name = $this->maxLength($name, 17);
                        $html .= '<tr><td height="8" width="63" style="font-size: 6; background-color: '.$style['bgcolorHead'].'; color: '.$style['colorHead'].'; text-align: left"> '.$name.'</td></tr>';
                    }
                }
                $html .= '</tbody></table>';
                $this->pdf->StartTransform();
                $this->pdf->Translate($curX, $startY+35);
                $this->pdf->Rotate(90, 30, 30);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=30, $y=30+$this->LEFT_MARGIN, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $this->pdf->StopTransform();
            }

            //////////////////////////////////////////////////////////////////////
            // x. Body
            $curY = $startY + 66;
            foreach ($foods as $food) {
                $this->overrideFoodStyle($food);

                $curX = 6 + $this->LEFT_MARGIN;

                // Food scale
                $foodScale = $this->computeScale($food, $variables, 45);
                if (!$this->isHideArticles) {
                    if ($this->isDisableArticles) {
                        $barHtml = $this->getScale(0);
                        $articlesHtml = '<font face="sanukotb_0" style="color: #777777">?</font>';
                    } else {
                        $barHtml = $this->getScale($foodScale['numCell']);
                        if (round($foodScale['maxG']) > intval($variables['colorBarThreshold'])) {
                            $articlesHtml  = '<font style="font-size: 9">'.$labels['colorBarAnyAmount'].'</font>';
                        }
                        else {
                            $maxG = $foodScale['maxG'];
                            $maxG = $this->builder->localizator->translateFoodTableText($maxG);
                            $articlesHtml = '<font style="font-size: 9">'.$maxG.' '.$labels['proG'].'/'.$labels['gPerArtikel'].'</font>';
                        }

                        if ($this->isDisableArticlesAmount) {
                            $articlesHtml = '';
                        }
                    }

                    if ($idBarCalculation == 1) { // Sport
                        $numBar = $this->calculateCaloryBalance($food, $variables, 45);
                        $barHtml = $this->getScale($numBar);
                    }

                    $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$curY, $barHtml, $border=0, $ln=0, $fill=0, $reseth=true, 'L', $autopadding=false);
                    $curX += 28;

                    $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX+10, $y=$curY+2, $articlesHtml, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                    $curX += 108;
                }

                // Smiley
                if (!$this->isDisableSmileysColumn) {
                    $riskAmountSum = $this->computeRiskAmountSum($food, $ingredients, $risks);
                    $smiley = $this->computeSmiley($riskAmountSum);
                    $html = $this->getSmiley($smiley);
                    $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$curY+3, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                    $curX += 64;
                }
                else {
                    $curX += 1;
                }

                // Debug
                if ($this->isDebug) {
                    $html = round($riskAmountSum, 8);
                    $this->pdf->writeHTMLCell($w=0, $h=0, $x=100+$this->LEFT_MARGIN, $y=$curY+2, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                }

                // Warning
                $html = $this->getInfoCell($food, $foodScale['maxG'], $warnings);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$curY-1, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                if (!empty($this->isDisableWarningColumn)) {
                    $curX += 63 - count($this->isDisableWarningColumn) * 12;
                }
                else {
                    $curX += 63;
                }

                if (count($this->isDisableWarningColumn) == 5) {
                    $curX -= 2;
                }

                // Product
                $html = $this->getLabel($food['name'], $productNameWidth);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$curY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $curX += 3 + $productNameWidth;

                // Portion
                $html = $this->getPortion($food);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$curX, $y=$curY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                $curX += 41;

                // Ingredient
                if (!empty($ingredientDisplay)) {
                    $html = $this->getIngredients($ingredients, $food['ingredients'], $risks, $ingredientDisplay);
                    $this->pdf->StartTransform();
                    $this->pdf->Translate($curX, $curY-13);
                    $this->pdf->Rotate(90, 30, 30);
                    $this->pdf->writeHTMLCell($w=0, $h=0, $x=30, $y=30+$this->LEFT_MARGIN, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                    //$this->pdf->writeHTMLCell($w=0, $h=0, $x=200+$productNameWidth, $y=$curY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
                    $this->pdf->StopTransform();
                }

                $curY += 17;

                $this->reloadFoodStyle($food);
            }

        }


        $this->pdf->SetAutoPageBreak(TRUE, \Leep\AdminBundle\Business\Report\Builder::$PDF_MARGIN_BOTTOM + \Leep\AdminBundle\Business\Report\Builder::$PDF_MARGIN_FOOTER);
        //$this->pdf->addPage('P');
        //$this->pdf->setPageUnit('mm');
        //$this->pdf->deletePage($this->pdf->getPage());
    }

    public function computeScale($food, $variables, $maxCell) {
        // Kcal
        $smallValue = 0.00001;
        if ($food['ingredients'][$this->extraParameter['kcal']] == 0) {
            $maxKcal = $food['ingredients'][$this->extraParameter['g']] * $variables['kcal'] / $smallValue;
        }
        else {
            $maxKcal = $food['ingredients'][$this->extraParameter['g']] * $variables['kcal'] / $food['ingredients'][$this->extraParameter['kcal']];
        }

        // Eiw
        if ($food['ingredients'][$this->extraParameter['eiw']] == 0) {
            $maxEiw = $food['ingredients'][$this->extraParameter['g']] * $variables['eiw'] / $smallValue;
        }
        else {
            $maxEiw = $food['ingredients'][$this->extraParameter['g']] * $variables['eiw'] / $food['ingredients'][$this->extraParameter['eiw']];
        }

        // Koh
        if ($food['ingredients'][$this->extraParameter['koh']] == 0) {
            $maxKoh = $food['ingredients'][$this->extraParameter['g']] * $variables['koh'] / $smallValue;
        }
        else {
            $maxKoh = $food['ingredients'][$this->extraParameter['g']] * $variables['koh'] / $food['ingredients'][$this->extraParameter['koh']];
        }

        // Fett
        if ($food['ingredients'][$this->extraParameter['fett']] == 0) {
            $maxFett = $food['ingredients'][$this->extraParameter['g']] * $variables['fett'] / $smallValue;
        }
        else {
            $maxFett  = $food['ingredients'][$this->extraParameter['g']] * $variables['fett'] / $food['ingredients'][$this->extraParameter['fett']];
        }

        $maxGAllow = min($maxKcal, $maxEiw, $maxKoh, $maxFett);
        $maxGAllow = $maxGAllow * floatval($this->overshootFactor);

        // Compute scale
        /*
        $gPerCell = (intval($variables['colorBarMax']) - intval($variables['colorBarMin'])) / $maxCell;
        $numCell = ($maxGAllow - intval($variables['colorBarMin'])) / $gPerCell;
        if ($numCell <= 0) {
            $numCell = $maxCell;
        }
        else if ($numCell >= $maxCell) {
            $numCell = 1;
        }
        else {
            $numCell = $maxCell - ceil($numCell);
        }*/
        $index = 0;
        $minDiff = 99999;
        foreach ($this->scales as $i => $c) {
            $diff = abs($c[0] - $maxGAllow);
            if ($diff < $minDiff) {
                $minDiff = $diff;
                $index = $i;
            }
        }


        return array('maxG' => $maxGAllow, 'numCell' => $index);
    }

    public function calculateCaloryBalance($food, $variables, $maxCell) {
        $kcalAmount = floatval($food['ingredients'][$this->extraParameter['kcal']]);
        if ($kcalAmount == 0) {
            return 0;
        }

        $carbAmount = floatval($food['ingredients'][$this->extraParameter['koh']]);
        $fatAmount = floatval($food['ingredients'][$this->extraParameter['fett']]);
        $protAmount = floatval($food['ingredients'][$this->extraParameter['eiw']]);

        $protPercentage = ($protAmount * 4 * 100) / $kcalAmount;
        $carbPercentage = ($carbAmount * 4 * 100) / $kcalAmount;
        $fatPercentage = ($fatAmount * 9 * 100) / $kcalAmount;

        $protDiff = abs($protPercentage - floatval($variables['sportProtein']));
        $carbDiff = abs($carbPercentage - floatval($variables['sportCarbs']));
        $fatDiff = abs($fatPercentage - floatval($variables['sportFat']));

        $totalDiff = $protDiff + $carbDiff + $fatDiff;

        $index = 0;
        $minDiff = 99999999;
        foreach ($this->scales as $i => $c) {
            $diff = abs($c[0] - $totalDiff);
            if ($diff < $minDiff) {
                $minDiff = $diff;
                $index = $i;
            }
        }

        return $maxCell - $index;
    }

    public function computeSmiley($riskAmountSum) {
        foreach ($this->smileyConversion as $smileyConversion) {
            if ($smileyConversion['from'] <= $riskAmountSum && $riskAmountSum <= $smileyConversion['to']) {
                return $smileyConversion['value'];
            }
        }
        return 0;
    }

    public function computeRiskAmountSum($food, $ingredients, $risks) {
        $totalRiskAmount = 0;
        foreach ($ingredients as $ingredientId => $ingredient) {
            $riskAmountMax = $ingredient['riskAmountMax'];

            $risk = isset($risks[$ingredientId]) ? floatval($risks[$ingredientId]) : 0;
            $foodRiskAmount = isset($food['ingredients'][$ingredientId]) ? $food['ingredients'][$ingredientId] : 0;

            $riskAmount = $risk * $foodRiskAmount;
            $riskAmountPercentage = $riskAmount * 100 / $riskAmountMax;
            $totalRiskAmount += $riskAmountPercentage;
        }
        return $totalRiskAmount;
    }

    public function getScale($value, $height = 16) {
        $html = '<table><tbody><tr>';
        $totalCell = 45;
        for ($i = 0; $i < $totalCell - $value; $i++) {
            $html .= '<td height="'.$height.'pt" width="3pt"></td>';
        }
        for ($i = $totalCell - $value; $i < $totalCell; $i++) {
            $html .= '<td height="'.$height.'pt" width="3pt" style="background-color: #'.$this->scales[$i][1].'"></td>';
        }
        $html .= '</tr></tbody></table>';
        return $html;
    }

    public function getSmiley($value) {
        $smiley = '';
        if ($value > 0) {
            for ($i = 0; $i < $value; $i++) $smiley .= '<font face="wingdings" style="font-size: 10; color: green">J</font><b style="color: white; font-size: 7">_</b>';
        }
        else if ($value < 0) {
            $value = -$value;
            for ($i = 0; $i < $value; $i++) $smiley .= '<font face="wingdings" style="font-size: 10; color: red">L</font><b style="color: white; font-size: 7">_</b>';
        }
        else {
            $smiley .= '<font face="wingdings" style="font-size: 10; color: black">K</font><b style="color: white; font-size: 7">_</b>';
        }

        if ($this->isDisableSmileys) {
            $smiley = '<font style="color: #777777">?</font>';
        }
        $html = '<table><tbody><tr>';
        $html .= '<td width="60" style="text-align: center"><font face="sanukotb_0">'.$smiley.'</font></td>';
        $html .= '</tr></tbody></table>';
        return $html;
    }

    public function getInfoCell($food, $maxG, $warnings) {
        if ($maxG == 0) {
            $v1 = 0;
        }
        else {
            $v1 = round($food['ingredients'][$this->extraParameter['g']] / $maxG, 1);
            if ($v1 >= 10) {
                $v1 = round($v1, 0);
            }
        }

        // Compute allegie
        $p = array(
            'warnung' => false,
            'laktose' => false,
            'gluten'  => false,
            'allegie' => false
        );
        foreach ($warnings as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $warningV) {
                    $arr = explode(';', $warningV);
                    $ingredientId = trim($arr[0]);
                    $threshold = floatval($arr[1]);
                    if (isset($food['ingredients'][$ingredientId]) && ($food['ingredients'][$ingredientId] > $threshold)) {
                        $p[$k] = true;
                        break;
                    }
                }
            }
            else {
                $arr = explode(';', $v);
                $ingredientId = trim($arr[0]);
                $threshold = floatval($arr[1]);
                if ($food['ingredients'][$ingredientId] > $threshold) {
                    $p[$k] = true;
                }
            }
        }

        if ($p['laktose'] || $p['gluten']) {
            $p['warnung'] = true;
        }
        else {
            $p['warnung'] = false;
        }

        foreach ($p as $k => $v) {
            if ($v) {
                $p[$k] = '<b style="font-size: 3; color: '.$this->style['bgcolorBody'].'">_</b><font face="webdings">i</font>';
            }
        }

        if ($this->isDisableSmileys) {
            $p['warnung'] = '';
            $p['laktose'] = '';
            $p['gluten'] = '';
        }
        if ($this->isDisableArticles || $this->isHideArticles) {
            $v1 = '';
        }

        // Display
        $html = '<table cellspacing="1pt">';
        $html .= '<tbody>';
        $html .= '<tr>';
        if (!isset($this->isDisableWarningColumn['ArticlesPortion'])) {
            $html .= '<td width="11pt" height="16pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(4).$v1.'</td>';
        }
        if (!isset($this->isDisableWarningColumn['Warning'])) {
            $html .= '<td width="11pt" height="16pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: left">'.$this->getWorkaroundMarginTop(3).$p['warnung'].'</td>';
        }
        if (!isset($this->isDisableWarningColumn['Lactose'])) {
            $html .= '<td width="11pt" height="16pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: left">'.$this->getWorkaroundMarginTop(3).$p['laktose'].'</td>';
        }
        if (!isset($this->isDisableWarningColumn['Gluten'])) {
            $html .= '<td width="11pt" height="16pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: left">'.$this->getWorkaroundMarginTop(3).$p['gluten'].'</td>';
        }
        if (!isset($this->isDisableWarningColumn['Allergy'])) {
            $html .= '<td width="11pt" height="16pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: left">'.$this->getWorkaroundMarginTop(3).$p['allegie'].'</td>';
        }
        $html .= '</tr>';
        $html .= '</tbody></table>';
        if (count($this->isDisableWarningColumn) == 5) {
            return '';
        }
        return $html;
    }

    public function getLabel($label, $productNameWidth) {
        $label = $this->builder->translateBlock($label);
        $label = $this->maxLength($label, round($productNameWidth / 3.10));
        $label = htmlspecialchars ($label);
        $label = $this->getWorkaroundMarginTop(4).$label;

        $html = '<table><tbody><tr>';
        $html .= '<td width="'.$productNameWidth.'" height="16pt" style="font-size: 6; text-align: left; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].';">'.$label.'</td>';
        $html .= '</tr></tbody></table>';
        return $html;
    }

    public function getPortion($food) {
        $g    = $food['ingredients'][$this->extraParameter['g']];
        $kcal = $food['ingredients'][$this->extraParameter['kcal']];
        $eiw  = $food['ingredients'][$this->extraParameter['eiw']];
        $koh  = $food['ingredients'][$this->extraParameter['koh']];
        $fett = $food['ingredients'][$this->extraParameter['fett']];

        $g = $this->builder->localizator->translateFoodTableAmountText($g, 'decimal_col1');
        $eiw = $this->builder->localizator->translateFoodTableAmountText($eiw, 'decimal_col3');
        $koh = $this->builder->localizator->translateFoodTableAmountText($koh, 'decimal_col4');
        $fett = $this->builder->localizator->translateFoodTableAmountText($fett, 'decimal_col5');

        $html = '<table>';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td height="16pt" width="20pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).$g.'</td>';
        $html .= '<td width="1pt"></td>';
        $html .= '<td height="16pt" width="20pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).number_format($kcal, 0).'</td>';
        $html .= '<td width="1pt"></td>';
        $html .= '<td height="16pt" width="20pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).($eiw).'</td>';
        $html .= '<td width="1pt"></td>';
        $html .= '<td height="16pt" width="20pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).($koh).'</td>';
        $html .= '<td width="1pt"></td>';
        $html .= '<td height="16pt" width="20pt" style="font-size: 7; background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(3).($fett).'</td>';
        $html .= '</tr>';
        $html .= '</tbody></table>';
        return $html;
    }

    public function getIngredients($ingredients, $ingredientValue, $risks, $ingredientDisplay) {
        $html = '<table cellspacing="1">';
        $html .= '<tbody>';
        foreach ($ingredientDisplay as $ingredientId) {
            if (isset($ingredients[$ingredientId])) {
                $ingredient = $ingredients[$ingredientId];
                $value = isset($ingredientValue[$ingredientId]) ? $ingredientValue[$ingredientId] : '--';
                $cValue = floatval($value);

                if ($value != '--') {
                    $risk = isset($risks[$ingredientId]) ? floatval($risks[$ingredientId]) : 0;
                    $riskAmount = $risk * $value;
                    $riskPercentage = $riskAmount * 100 / $ingredient['riskAmountMax'];
                    if ($value < 1000) {
                        $value = round($value, 1);
                    }
                    else {
                        $value = round($value, 0);
                    }

                    if ($riskPercentage > $this->parameter->getColorGreenRiskamount()) {
                        $value = '<b style="color: green">'.$value.'</b>';
                    }
                    if ($riskPercentage < $this->parameter->getColorRedRiskamount()) {
                        $value = '<b style="color: red">'.$value.'</b>';
                    }
                }
                if ($cValue >= 100000) {
                    $html .= '<tr><td height="8pt" width="16pt" style="font-size: 4;background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(1).$value.'</td></tr>';
                }
                else {
                    $html .= '<tr><td height="8pt" width="16pt" style="font-size: 5;background-color: '.$this->style['bgcolorBody'].'; color: '.$this->style['colorBody'].'; text-align: center">'.$this->getWorkaroundMarginTop(1).$value.'</td></tr>';
                }
            }
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public $currentBgColorBody;
    public $currentOvershootFactor;
    public function overrideFoodStyle($food) {
        if (isset($food['bgcolorBody'])) {
            $this->currentBgColorBody = $this->style['bgcolorBody'];
            $this->currentOvershootFactor = $this->overshootFactor;

            $this->style['bgcolorBody'] = $food['bgcolorBody'];
            $this->overshootFactor = $food['overshootFactor'];
        }
    }

    public function reloadFoodStyle($food) {
        if (isset($food['bgcolorBody'])) {
            $this->style['bgcolorBody'] = $this->currentBgColorBody;
            $this->overshootFactor = $this->currentOvershootFactor;
        }
    }
}
