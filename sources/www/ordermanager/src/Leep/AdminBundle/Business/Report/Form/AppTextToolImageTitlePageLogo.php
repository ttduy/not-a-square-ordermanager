<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolImageTitlePageLogo extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_image_title_page_logo';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('image', 'app_image_picker', array(
            'label'  => 'Image',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('boxColor', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
        $builder->add('logo', 'app_image_picker', array(
            'label'  => 'Logo',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('overrideLogo', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
        $builder->add('alignment', 'choice', array(
            'required' => false,
            'choices'  => array('left' => 'Left', 'center' => 'Centered', 'right' => 'Right'),
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));        
    }
}
