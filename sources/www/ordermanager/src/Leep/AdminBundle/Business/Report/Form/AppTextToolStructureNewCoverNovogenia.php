<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureNewCoverNovogenia extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_new_cover_novogenia';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $mgr = $this->container->get('easy_module.manager');
        $view->vars['imageHelperPath'] = $mgr->getUrl('leep_admin', 'report', 'report', 'imageHelper');
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('numPage', 'text', array(
            'required' => false,
            'attr' => [
                'style' => 'min-width: 80px; max-width: 80px'
            ]
        ));

        $builder->add('background', 'textarea', [
            'required' => false,
            'attr' => [
                'rows'  => '3',
                'cols' => 100
            ]
        ]);

        $builder->add('rings', 'textarea', [
            'required' => false,
            'attr' => [
                'rows'  => '10',
                'cols' => 100
            ]
        ]);
        $builder->add('isBodCover',  'checkbox', array(
            'required' => false
        ));
    }
}
