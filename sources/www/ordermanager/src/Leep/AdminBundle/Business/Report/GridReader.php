<?php
namespace Leep\AdminBundle\Business\Report;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'sortOrder', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '60%', 'sortable' => 'false'),            
            array('title' => 'Order',       'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Report', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }   
        $queryBuilder->andWhere('p.isDeleted = 0');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');
        $queryBuilder->addOrderBy('p.name', 'ASC');        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('reportModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'report', 'edit_section', 'edit', array('id' => $row->getId())), 'button-edit');            
        }
        if ($helperSecurity->hasPermission('reportDelete')) {
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'report', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}