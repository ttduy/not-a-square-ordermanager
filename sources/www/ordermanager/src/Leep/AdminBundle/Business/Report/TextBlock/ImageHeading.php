<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class ImageHeading extends BaseTextBlock {
    private function getMargin($data, $field, $defaultValue) {
        if (isset($data[$field])) {
            if (trim($data[$field]) != '') {
                return intval($data[$field]);
            }
        }
        return $defaultValue;
    }
    public function build($data) { 
        $imageFile = $this->builder->resourceDir.$data['image'];       

        $imageSize = getimagesize($imageFile);
        $imageHeightResized = $data['overallHeight'];
        $imageWidthResized = $imageHeightResized * $imageSize[0] / $imageSize[1];

        $currentMargins = $this->pdf->getMargins();
        $margins = array(
            'top'  => $this->getMargin($data, 'marginTop', $currentMargins['top']),
            'left' => $this->getMargin($data, 'marginLeft', $currentMargins['left']),
            'right' => $this->getMargin($data, 'marginRight', $currentMargins['right']),
        );
        $this->pdf->setMargins($margins['left'], $margins['top'], $margins['right'], true);

        $this->pdf->myDrawImage(
            $file = $imageFile,
            $x = 0,
            $y = $margins['top'], 
            $w = $imageWidthResized,
            $h = $imageHeightResized,
            $type = '',
            $link = '',
            $align = 'R',
            $resize = true,
            $dpi = 300,
            $palign = 'L'
        );
        
        $boxHeightResized = $imageHeightResized;
        $boxWidthResized = $this->pdf->getPageWidth() - $imageWidthResized - $margins['right'] - $margins['left'] - 2;
        
        $fillColor = $this->builder->toColorArray($data['boxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $imageWidthResized + $margins['left'] + 2,
            $y = $margins['top'],
            $w = $boxWidthResized,
            $h = $boxHeightResized,
            'F',
            $style,
            $fillColor
        );

        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];
        $alignment = $this->builder->getAlignment($data);
        $textBlock = $data['textBlock'];
        $html = '<p style="color:'.$data['fontColor'].'; font-size: '.$fontSize.'"><font face="'.$this->builder->fontMap['bold'].'">'.$textBlock.'</font></p>';
        $estimatedHeight = $this->builder->estimateHeight($boxWidthResized-4, $html);
        
        $this->pdf->writeHTMLCell($w=$boxWidthResized-4, $h=0, 
            $x=$imageWidthResized + $margins['left'] +4, 
            $y=$boxHeightResized + $margins['top'] - $estimatedHeight-0.5,
            $html, $border=0, $ln=2, $fill=0, $reseth=true, $alignment, $autopadding=true);            

        $this->pdf->setX(0);
        $this->pdf->setY($imageHeightResized + 5 + $margins['top']);

        $this->pdf->setMargins($currentMargins['left'], $currentMargins['top'], $currentMargins['right'], true);        
    }
}
