<?php
namespace Leep\AdminBundle\Business\Report;

class Constants {
    // Images
    const TEXT_TOOL_TYPE_IMAGE_ACROSS_PAGE_TOOL   = 10;
    const TEXT_TOOL_TYPE_IMAGE_TITLE_PAGE_LOGO    = 11;
    const TEXT_TOOL_TYPE_IMAGE_HEADING            = 12;

    // Text
    const TEXT_TOOL_TYPE_TEXT_BARCODE             = 31;
    const TEXT_TOOL_TYPE_TEXT_PLAIN               = 32;
    const TEXT_TOOL_TYPE_TEXT_HTML                = 33;
    const TEXT_TOOL_TYPE_TEXT_LIST                = 34;
    const TEXT_TOOL_TYPE_TEXT_COLOR_TEXTBOX       = 35;
    const TEXT_TOOL_TYPE_TEXT_FLOATING_PLAIN      = 36;

    // Structure
    const TEXT_TOOL_TYPE_STRUCTURE_HORIZONTAL_LINE    = 51;
    const TEXT_TOOL_TYPE_STRUCTURE_SPACER             = 52;
    const TEXT_TOOL_TYPE_STRUCTURE_CHANGE_LAYOUT      = 53;
    const TEXT_TOOL_TYPE_STRUCTURE_BREAKLINE          = 54;
    //const TEXT_TOOL_TYPE_STRUCTURE_FOOTER             = 55;
    const TEXT_TOOL_TYPE_STRUCTURE_PAGE_LAYOUT        = 56;
    const TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER        = 57;
    const TEXT_TOOL_TYPE_STRUCTURE_CHANGE_FORMAT      = 58;
    const TEXT_TOOL_TYPE_STRUCTURE_COVER              = 59;
    const TEXT_TOOL_TYPE_STRUCTURE_PAGE_FOOTER_SIMPLIFIED = 62;

    // Widgets
    const TEXT_TOOL_TYPE_WIDGET_SCALE_TOOL               = 71;
    const TEXT_TOOL_TYPE_WIDGET_ARTICLE_TABLE            = 72;
    const TEXT_TOOL_TYPE_WIDGET_DAILY_DIET_TABLE         = 73;
    const TEXT_TOOL_TYPE_WIDGET_DAILY_SPORT_TABLE        = 74;
    const TEXT_TOOL_TYPE_WIDGET_SMALL_INFO_TABLE         = 75;
    const TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE               = 76;
    const TEXT_TOOL_TYPE_WIDGET_BAR_CHART                = 77;
    const TEXT_TOOL_TYPE_WIDGET_FAMILY_TREE_CALCULATION  = 78;
    const TEXT_TOOL_TYPE_WIDGET_IMAGE_WITH_LABEL         = 79;
    const TEXT_TOOL_TYPE_WIDGET_MEDICATION_TABLE         = 80;
    const TEXT_TOOL_TYPE_WIDGET_GENE_TABLE               = 81;
    const TEXT_TOOL_TYPE_WIDGET_SPORT_TABLE              = 82;
    const TEXT_TOOL_TYPE_WIDGET_TABLE                    = 83;
    const TEXT_TOOL_TYPE_WIDGET_RECIPE                   = 84;
    const TEXT_TOOL_TYPE_WIDGET_TABLE_NEW                = 85;
    const TEXT_TOOL_TYPE_WIDGET_RECIPE_SIMPLIFIED        = 86;
    const TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE_2             = 87;
    const TEXT_TOOL_TYPE_WIDGET_DRUG_LIST                = 88;

    // Utils
    const TEXT_TOOL_TYPE_UTIL_GRAB_TEXT_BLOCK            = 200;
    const TEXT_TOOL_TYPE_UTIL_EXTERNAL_PDF               = 201;
    const TEXT_TOOL_TYPE_UTIL_BACKGROUND_IMAGE           = 202;
    const TEXT_TOOL_TYPE_UTIL_MOVE_CURSOR                = 203;
    const TEXT_TOOL_TYPE_UTIL_TOC                        = 204;
    const TEXT_TOOL_TYPE_UTIL_TOC_ENTRY                  = 205;
    const TEXT_TOOL_TYPE_UTIL_ANNOTATOR                  = 210;

    // Marketing
    const TEXT_TOOL_TYPE_MARKETING_HEADER                = 300;
    const TEXT_TOOL_TYPE_MARKETING_HEADLINE              = 301;
    const TEXT_TOOL_TYPE_MARKETING_FOOTER                = 302;
    const TEXT_TOOL_TYPE_MARKETING_INFO_BOX              = 303;
    const TEXT_TOOL_TYPE_MARKETING_LOGO_RIBBON           = 305;

    // Chapter
    const TEXT_TOOL_TYPE_CHAPTER_TOC                     = 500;
    const TEXT_TOOL_TYPE_CHAPTER_PAGE                    = 501;

    public static function getBarCodeTypes() {
        return array(
            'qr_code' => 'QR Code'
        );
    }

    public static function getEffects() {
        return array(
            'regular_italic' => 'Regular italic',
            'bold'    => 'Bold',
            'light'   => 'Light',
            'light_italic' => 'Light italic',
            'medium'  => 'Medium',
            'thin'    => 'Thin',
            'palatino' => 'Special'
        );
    }

    public static function getLayouts() {
        return array(
            'page'         => 'Full page',
            '1left_1right' => '1/2 Left, 1/2 Right',
            '1left_2right' => '1/3 Left, 2/3 Right',
            '2left_1right' => '2/3 Left, 1/3 Right',
            '1left_3right' => '1/4 Left, 3/4 Right',
            '3left_1right' => '3/4 Left, 1/4 Right',
            '1left_4right' => '1/5 Left, 4/5 Right',
            '1left_5right' => '1/6 Left, 5/6 Right',
            '1left_1mid_1right' => '1/3 Left, 1/3 Middle, 1/3 Right',
            '4_columns'    => '1/4 1/4 1/4 1/4',
            'customs'      => 'Custom layout'
        );
    }
    public static function getBreaks() {
        return array(
            'column'       => 'Break column',
            'page'         => 'Break page',
            'skip_footer'       => 'Skip footer on this page',
            'skip_from_footer'  => 'Skip footer from this page',
            'unskip_from_footer'     => 'Un-skip footer from this page',
            'stop_auto_break'    => 'Stop auto-break',
            'start_auto_break'   => 'Start auto-break',
            'page_if'      => 'Break page if remaining height less than',
            'break_chapter'      => 'Skip chapter boxes from this page',
            'skip_chapter'       => 'Skip chapter boxes on this page'
        );
    }

    public static function getPageFormat() {
        return array(
            'A4' => 'A4',
            'A5' => 'A5',
            'CUSTOM_A5_1MMMARGIN' => 'A5 + 1mm margins',
            'CUSTOM_A5_3MMMARGIN' => 'A5 + 3mm margins',
            'CUSTOM_A5_3MMMARGIN_LC' => 'A5 + 3mm margins (landscape)',
            'CUSTOM_90_330'   => '90mm x 330mm',
            'CUSTOM_90_297'   => '90mm x 297mm',
            'A2_POSTER'       => 'A2 Poster (422 x 596 mm)',
            'A3_POSTER'       => 'A3 Poster (299 x 422 mm)',
            'CUSTOM'          => 'Custom format'
        );
    }

    public static function getPageOrientation() {
        return array(
            'P' => 'Portrait',
            'L' => 'Landscape'
        );
    }

    const BOD_BARCODE_TYPE_BOOKBLOCK        = 1;
    const BOD_BARCODE_TYPE_COVER            = 2;
    const BOD_BARCODE_TYPE_ORDER_NUMBER     = 3;
    public static function getBodBarcodeTypes() {
        return array(
            self::BOD_BARCODE_TYPE_BOOKBLOCK =>     'Bookblock',
            self::BOD_BARCODE_TYPE_COVER     =>     'Cover',
            self::BOD_BARCODE_TYPE_ORDER_NUMBER =>  'Order number'
        );
    }

    // Report type
    const DNC_REPORT_TYPE_MEDIC   = 1;
    const DNC_REPORT_TYPE_PHARMA  = 2;
    const DNC_REPORT_TYPE_FITNESS = 3;
    const DNC_REPORT_TYPE_HOME    = 4;
    public static function getDncReportTypes() {
        return array(
            self::DNC_REPORT_TYPE_MEDIC    => 'MEDIC',
            self::DNC_REPORT_TYPE_PHARMA   => 'PHARMA',
            self::DNC_REPORT_TYPE_FITNESS  => 'FITNESS',
            self::DNC_REPORT_TYPE_HOME     => 'HOME'
        );
    }


    // Task types
    const REPORT_TASK_TYPE_DISTRIBUTION_CHANNEL_PRICE         = 1;
    public static function getReportTaskTypes() {
        return array(
            self::REPORT_TASK_TYPE_DISTRIBUTION_CHANNEL_PRICE  => 'Generate distribution channel price list'
        );
    }
}
