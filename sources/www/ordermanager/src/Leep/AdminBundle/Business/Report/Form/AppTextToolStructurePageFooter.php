<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructurePageFooter extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_page_footer';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('lineColor',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px')
        ));
        $builder->add('textPage',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px')
        ));
        $builder->add('textOf',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px')
        ));
        $builder->add('isBreak',  'checkbox', array(
            'required' => false
        ));
        $builder->add('isCorrectPageGroup', 'checkbox', array(
            'required' => false
        ));
        $builder->add('logo', 'app_image_picker', array(
            'label'  => 'Image',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('overrideLogo', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            )
        ));
        $builder->add('orderNumber',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px')
        ));
    }
}
