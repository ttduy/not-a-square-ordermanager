<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class StructureStandardCover extends BaseTextBlock {
    public $paddingTop = 10;
    public $paddingBot = 10;
    public function process() {

        $resourceDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $background         = $this->getData('bgColor');
        $line               = $this->getData('lineBox');
        $imageContent       = $this->getData('imageContent');
        $logoBox            = $this->getData('logoBox');
        $icon               = $this->getData('iconBox');
        $numPage            = $this->getData('numPage');

        // Calculate extra size for cover with number of pages in report
        if ($this->builder->totalNumPage != 0) {
            $numPage = $this->builder->totalNumPage;
        } else if ($this->pdf->getNumPages() > 1) {
            $numPage = $this->pdf->getNumPages();
        }

        // Replace previous created page with an new, larger one
        $extraWidth = $numPage * 0.06;
        $this->pdf->deletePage($this->pdf->getPage());
        $this->pdf->addPage('L', 'A4_NOVO_COVER_'.$extraWidth);
        $this->pdf->setPageUnit('mm');

        // Cover setting (No margin, no page break)
        $bMargin = $this->pdf->getBreakMargin();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $this->pdf->SetAutoPageBreak(false, 0);

        // Draw background
        $this->drawBackground($background);

        /// Draw line
        $this->drawLine($line);

        // Draw Image Content
        $this->drawImageContent($imageContent);

        /// Draw Rect Logo
        $this->drawRectLogoBox($logoBox);
        $this->drawIcons($icon);

        // Reset setting after cover
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }

    private function drawImageContent($config) {
        $resourceDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $parameter = array_map('trim', explode(';', $config));
        if (count($parameter) == 11) {
            $x              = ($parameter[0] === '') ? 0 : $parameter[0];
            $y              = ($parameter[1] === '') ? 0 : $parameter[1];
            $width          = (!empty($parameter[2]) || $parameter[2] != 'auto') ? $parameter[2] : 0;
            $height         = (!empty($parameter[3]) || $parameter[3] != 'auto') ? $parameter[3] : 0;
            $urlImage       = !empty($parameter[4]) ? $parameter[4] : '';
            $widthColor     = !empty($parameter[5]) ? $parameter[5] : '100';
            $heightColor    = !empty($parameter[5]) ? $parameter[6] : '100';
            $colorBegin     = !empty($parameter[7]) ? $parameter[7] : '#FFFFFF';
            $colorEnd       = !empty($parameter[8]) ? $parameter[8] : '#FFFFFF';
            $direction      = !empty($parameter[9]) ? $parameter[9] : 'T';
            $opacity        = !empty($parameter[10]) ? $parameter[10] : '0.5';

            list($r, $g, $b) = sscanf($colorBegin, "#%02x%02x%02x");
            $colorBegin = [$r, $g, $b];

            list($r, $g, $b) = sscanf($colorEnd, "#%02x%02x%02x");
            $colorEnd = [$r, $g, $b];
            $coords = [0, -1, -1, 1, 1];
            if (!empty($urlImage)) {
                $imageFile = $resourceDir. $urlImage;
                $this->pdf->Image($imageFile, $x, $y, $width, $height, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array());
                $this->pdf->SetAlpha($opacity);
                $coords = array(0.5,0.5,0.5,0.5,1);
                if ($direction == 'T') {
                    $coords = [1, 0.5, 0.5, 1, 1.2];
                }
                else if ($direction == 'R') {
                    $coords = [0.5, 0.5, 1, 0.5, 1.2];
                }

                $this->pdf->RadialGradient($x, $y, $widthColor, $heightColor, $colorBegin, $colorEnd, $coords);
            }
        }

        $this->pdf->SetAlpha(1);
    }

    private function drawLine($config) {
        $arrayLine = explode("\n", $config);
        foreach($arrayLine as $key=> $value) {
            $parameter = array_map('trim', explode(';', $value));
            if (count($parameter) == 3 && $value[0] != '#') {
                $y              = ($parameter[0] === '') ? 10 : $parameter[0];
                $thickness      = !empty($parameter[1]) ? $parameter[1] : 0.5;
                $borderColor    = !empty($parameter[2]) ? $parameter[2] : '#ffffff';

                list($r, $g, $b) = sscanf($borderColor, "#%02x%02x%02x");
                $style = array('width' => $thickness, 'color' => array($r, $g, $b));
                $x = 0;

                $this->pdf->Line($x, $y, $this->pdf->getPageWidth(), $y, $style);
            }
        }
    }

    private function drawBackground($config) {
        $config = $this->parseInput([
            'gradient.color.begin' =>   '#FFFFFF',
            'gradient.color.end' =>     '#FFFFFF',
            'gradient.direction' =>     'bottom-to-top'
        ], $config);

        // Pdf's page parameter
        $width = $this->pdf->getPageWidth();
        $height = $this->pdf->getPageHeight();

        // Default is direction bottom to top
        $coords = [0, 0, 0, 1];
        $direction = $config['gradient.direction'];
        if ($direction == 'top-to-bottom' || $direction == 'top') {
            $coords = [0, 0, 0, -1];
        } else if ($direction == 'left-to-right' || $direction == 'left') {
            $coords = [0, 0, 1, 0];
        } else if ($direction == 'right-to-left' || $direction == 'right') {
            $coords = [0, 0, -1, 0];
        } else if ($direction == 'bottom-to-top' || $direction == 'bottom') {
            $coords = [0, -1, -1, 1];
        } else {
            $coords = explode(";", $direction);
        }

        // Draw linear gradient background
        $beginColor = $this->builder->toColorArray($config['gradient.color.begin']);
        $endColor = $this->builder->toColorArray($config['gradient.color.end']);
        $this->pdf->linearGradient(0, 0, $width, $height, $beginColor, $endColor, $coords);
    }


    private function drawIcons($config) {
        $resourceDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $arrayImage = array_map('trim', explode("\n", $config));
        foreach ($arrayImage as $key => $value) {
            $parameter = array_map('trim', explode(';', $value));
            // T;R;10;5;0;0;/Images/LOGOS/DNANutriControl_Logo_Transparency.gif
            if (count($parameter) == 5 && $value[0] != '#') {
                $x                        = ($parameter[0] === '') ? 10 : $parameter[0];
                $y                        = ($parameter[1] === '') ? 10 : $parameter[1];
                $widthImg                 = (!empty($parameter[2]) || $parameter[2] != 'auto')  ? $parameter[2] : 0;
                $heightImg                = (!empty($parameter[3]) || $parameter[3] != 'auto')  ? $parameter[3] : 0;
                $urlImage                 = !empty($parameter[4]) ? $parameter[4] : '';
                $fileImage = $resourceDir. $urlImage;

                $this->pdf->Image($fileImage, $x, $y, $widthImg, $heightImg, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array());
            }
        }
    }
    private function drawRectLogoBox($config) {
        $arrayLogo = array_map('trim', explode("\n", $config));
        foreach ($arrayLogo as $key => $value) {
            $parameter = array_map('trim', explode(';', $value));
            if (count($parameter) == 9 && $value[0] != '#') {
                $x                      = ($parameter[0] === '') ? 10 : $parameter[0];
                $y                      = ($parameter[1] === '') ? 10 : $parameter[1];
                $widthBox               = !empty($parameter[2]) ? $parameter[2] : '20';
                $heightBox              = !empty($parameter[3]) ? $parameter[3] : '20';
                $bgColor                = !empty($parameter[4]) ? $parameter[4] : '#FFFFFF';
                $hShadow                = !empty($parameter[5]) ? $parameter[5] : '0';
                $vShadow                = !empty($parameter[6]) ? $parameter[6] : '0';
                $blur                   = !empty($parameter[7]) ? $parameter[7] : '';
                $color                  = !empty($parameter[8]) ? $parameter[8] : '#000000';

                list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
                $colorShadow = [$r, $g, $b];

                ////
                list($r, $g, $b) = sscanf($bgColor, "#%02x%02x%02x");
                $bgColor = [$r, $g, $b];
                $this->pdf->boxWithShadow($x, $y, $widthBox, $heightBox, $bgColor, $hShadow, $vShadow, $blur, $colorShadow);
            }
        }
    }

}
