<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class UtilThirdPartyExternalPdf {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {       
        $pdfFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/third_party_pdf';

        $rawPdf = $data['pdf'];

        $pdfFile = $pdfFilePath.'/'.$rawPdf;

        $numPage = $this->pdf->setSourceFile($pdfFile);
        for ($i = 1; $i <= $numPage; $i++) {
            if ($i != 1) {
                $this->pdf->AddPage();
            }
            $tplId = $this->pdf->importPage($i);
            
            $tplSize = $this->pdf->getTemplateSize($tplId);
            $tplWidth = $tplSize['w'];
            $tplHeight = $tplSize['h'];
            $pageWidth = $this->pdf->getPageWidth();
            $pageHeight = $this->pdf->getPageHeight();

            $x = ($pageWidth - $tplWidth) / 2;
            $y = ($pageHeight - $tplHeight) / 2;

            $this->pdf->useTemplate($tplId, $x, $y);
        }        
    }
}
