<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class StructureNewCoverNovogenia extends BaseTextBlock {
    public function process() {
        $background = $this->getData('background');
        $rings = $this->getData('rings');
        $numPage = $this->getData('numPage');

        // Calculate extra size for cover with number of pages in report
        if ($this->builder->totalNumPage != 0) {
            $numPage = $this->builder->totalNumPage;
        } else if ($this->pdf->getNumPages() > 1) {
            $numPage = $this->pdf->getNumPages();
        }

        // Replace previous created page with an new, larger one
        $extraWidth = $numPage * 0.06;
        $this->pdf->deletePage($this->pdf->getPage());
        $this->pdf->addPage('L', 'A4_NOVO_COVER_'.$extraWidth);
        $this->pdf->setPageUnit('mm');

        // Cover setting (No margin, no page break)
        $bMargin = $this->pdf->getBreakMargin();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $this->pdf->SetAutoPageBreak(false, 0);

        // Draw background
        $this->drawBackground($background);

        // Draw rings
        $this->drawRings($rings);

        // Reset setting after cover
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }

    private function drawBackground($config) {
        $config = $this->parseInput([
            'gradient.color.begin' =>   '#FFFFFF',
            'gradient.color.end' =>     '#FFFFFF',
            'gradient.direction' =>     'bottom-to-top'
        ], $config);

        // Pdf's page parameter
        $width = $this->pdf->getPageWidth();
        $height = $this->pdf->getPageHeight();

        // Default is direction bottom to top
        $coords = [0, 0, 0, 1];
        $direction = $config['gradient.direction'];
        if ($direction == 'top-to-bottom' || $direction == 'top') {
            $coords = [0, 0, 0, -1];
        } else if ($direction == 'left-to-right' || $direction == 'left') {
            $coords = [0, 0, 1, 0];
        } else if ($direction == 'right-to-left' || $direction == 'right') {
            $coords = [0, 0, -1, 0];
        } else if ($direction == 'bottom-to-top' || $direction == 'bottom') {
            $coords = [0, 0, 0, 1];
        } else {
            $coords = explode(";", $direction);
        }

        // Draw linear gradient background
        $beginColor = $this->pdf->toColorArray($config['gradient.color.begin']);
        $endColor = $this->pdf->toColorArray($config['gradient.color.end']);
        $this->pdf->linearGradient(0, 0, $width, $height, $beginColor, $endColor, $coords);
    }

    private function drawRings($config) {
        $width = $this->pdf->getPageWidth();
        $height = $this->pdf->getPageHeight();
        $lines = array_map('trim', explode("\n", $config));
        $resourceDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $customClipping = [
            'x' => 0,
            'y' => 0,
            'r' => 0
        ];

        foreach ($lines as $line) {
            $setting = array_map('trim', explode(";", $line));
            if (count($setting) < 4 || $line[0] == '#') {
                continue;
            }

            if ($setting[0] == 'clipping') {
                if (count($setting) >= 4) {
                    $customClipping['x'] = !empty($setting[1]) ? floatval($setting[1]) : 0;
                    $customClipping['y'] = !empty($setting[2]) ? floatval($setting[2]) : 0;
                    $customClipping['r'] = !empty($setting[3]) ? floatval($setting[3]) : 0;
                } else {
                    $customClipping['r'] = 0;
                }

                continue;
            }

            // Get ring setting
            $x = !empty($setting[0]) ? floatval($setting[0]) : 0;
            $y = !empty($setting[1]) ? floatval($setting[1]) : 0;
            $r = !empty($setting[2]) ? floatval($setting[2]) : 0;
            $color = !empty($setting[3]) ? $setting[3] : "#ffffff";
            $img = !empty($setting[4]) ? trim($setting[4]) : "";

            // Begin
            $this->pdf->startTransform();
            if (!empty($img)) {
                // Set clipping area
                if ($customClipping['r'] != 0) {
                    $this->pdf->Circle($customClipping['x'], $customClipping['y'], $customClipping['r'], 0, 360, 'CNZ');
                }

                // Crop image
                $this->pdf->Circle($x, $y, $r, 0, 360, 'CNZ');

                // Draw image
                $img = $resourceDir.$img;
                $x = $x - $r;
                $y = $y - $r;

                // Translate image
                if (count($setting) >= 7) {
                    $tX = !empty($setting[5]) ? floatval($setting[5]) : 0;
                    $tY = !empty($setting[6]) ? floatval($setting[6]) : 0;
                    $x += $tX;
                    $y += $tY;
                }

                $w = 0;
                $h = 0;
                if (count($setting) >= 9) {
                    $w = !empty($setting[7]) ? floatval($setting[7]) : 0;
                    $h = !empty($setting[8]) ? floatval($setting[8]) : 0;
                }

                // Draw image
                $this->pdf->Image($img, $x, $y, $w, $h);
            } else {
                // Set clipping area
                if ($customClipping['r'] != 0) {
                    $this->pdf->Circle($customClipping['x'], $customClipping['y'], $customClipping['r'], 0, 360, 'CNZ');
                }

                // Clipping circle
                if (count($setting) >= 8) {
                    $cX = !empty($setting[5]) ? floatval($setting[5]) : 0;
                    $cY = !empty($setting[6]) ? floatval($setting[6]) : 0;
                    $cR = !empty($setting[7]) ? floatval($setting[7]) : 0;

                    // Draw clipping circle
                    if ($cR != 0) {
                        $this->pdf->Rect(0, 0, $width, $height, 'CNZ-C');
                        $this->pdf->Circle($cX, $cY, $cR, 0, 360, 'CNZ');
                    }
                }

                // Parse color
                $color = $this->pdf->toColorArray($color);

                // Draw ring
                $this->pdf->Circle($x, $y, $r, 0, 360, 'F', [], $color, 2);
            }

            // End
            $this->pdf->stopTransform();
        }
    }
}