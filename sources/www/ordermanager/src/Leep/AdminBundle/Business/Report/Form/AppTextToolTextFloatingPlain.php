<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolTextFloatingPlain extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_text_floating_plain';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idType', 'choice', array(
            'required' => false,
            'choices'  => array(
                'text'     => 'Text',
                'barcode'  => 'Bar code',
                'qrcode'   => 'QR code',
                'datamatrix'    => 'Data matrix'
            ),
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('textBlock', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '3', 'cols' => 80
            )
        ));
        $builder->add('fontColor',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('fontSize',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('alignment', 'choice', array(
            'required' => false,
            'choices'  => array('left' => 'Left', 'center' => 'Centered', 'right' => 'Right', 'justify' => 'Justify'),
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('alignmentVertical', 'choice', array(
            'required' => false,
            'choices'  => array('top' => 'Top', 'middle' => 'Middle', 'bottom' => 'Bottom'),
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('effects', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getEffects()
        ));
        $builder->add('posX',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('posY',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('textWidth',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('textHeight',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('backgroundColor', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('borderColor', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('visibility', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('rotation',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
    }
}
