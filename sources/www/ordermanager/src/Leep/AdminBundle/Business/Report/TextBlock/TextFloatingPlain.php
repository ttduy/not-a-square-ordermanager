<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class TextFloatingPlain extends BaseTextBlock {
    public function build($data) {
        $alignment = $this->builder->getAlignment($data);
        $idType = isset($data['idType']) ? $data['idType'] : 'text';
        $bgColor = isset($data['backgroundColor']) ? $data['backgroundColor'] : '';
        $borderColor = isset($data['borderColor']) ? $data['borderColor'] : '';
        $height = isset($data['textHeight']) ? $data['textHeight'] : '';
        $this->builder->applyFont($data);
        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];

        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();

        $text = $data['textBlock'];
        $text = trim($text);
        if (trim($text) != '') {
            $text = $this->builder->translateCustomTag($text);
            $text = '<p style="color: '.$data['fontColor'].'; font-size: '.$fontSize.';" >'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</p>';

            $textWidth = floatval($data['textWidth']);
            $posX = floatval($data['posX']);
            $posY = floatval($data['posY']);
            if ($posX < 0) {
                $posX += $pageWidth;
            }
            if ($posY < 0) {
                $posY += $pageHeight;
            }

            $rotation = intval(isset($data['rotation']) ? $data['rotation'] : 0);
            if ($rotation != 0) {
                $this->pdf->StartTransform();
                $this->pdf->Rotate($rotation, $posX, $posY);
            }

            $bMargin = $this->pdf->getBreakMargin();
            $auto_page_break = $this->pdf->getAutoPageBreak();
            $this->pdf->SetAutoPageBreak(false, 0);
            switch ($idType) {
                case 'text':
                    if ($bgColor != '') {
                        $this->pdf->setFillColorArray($this->builder->toColorArray($bgColor));
                        $isFill = true;
                    }
                    else {
                        $isFill = false;
                    }
                    if ($borderColor != '') {
                        $border = array(
                            'LTRB' => array(
                                'color' => $this->builder->toColorArray($borderColor)
                            )
                        );
                    }
                    else {
                        $border = array();
                    }
                    $topMargin = $this->getTopMargin($data['alignmentVertical'], $textWidth, $height, $data['textBlock'], $border);
                    $this->pdf->setCellPaddings(0, $topMargin, 0, 0);
                    $this->pdf->writeHTMLCell($w=$textWidth, $h=$height, $x=$posX, $y=$posY, $text, $border=$border, $ln=1, $fill=$isFill, $reseth=true, $alignment, $autopadding=true);
                    break;
                case 'barcode':
                    $barcodeStyle = array(
                        'stretch' => true,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'vpadding' => 'auto',
                        'fgcolor' => $this->builder->toColorArray($data['fontColor']),
                        'bgcolor' => $this->builder->toColorArray($bgColor == '' ? '#FFFFFF' : $bgColor),
                        'text' => true,
                        'font' => $this->builder->fontMap['regular'],
                        'fontsize' => $fontSize,
                        'stretchtext' => 1,
                    );
                    $this->pdf->write1DBarcode($data['textBlock'], 'EAN13', $x=$posX, $y=$posY, $textWidth, $height, 0.4, $barcodeStyle);
                    break;
                case 'qrcode':
                    $barcodeStyle = array(
                        'stretch' => true,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'vpadding' => 'auto',
                        'fgcolor' => $this->builder->toColorArray($data['fontColor']),
                        'bgcolor' => $this->builder->toColorArray($bgColor == '' ? '#FFFFFF' : $bgColor),
                        'text' => true,
                        'font' => $this->builder->fontMap['regular'],
                        'fontsize' => $fontSize,
                        'stretchtext' => 1,
                    );

                    $fromEncoding = mb_detect_encoding($data['textBlock']);
                    $text = mb_convert_encoding($data['textBlock'], "ISO-8859-1", $fromEncoding);
                    $this->pdf->write2DBarcode($text, 'QRCODE,H', $x=$posX, $y=$posY, $textWidth, $height, $barcodeStyle);
                    break;
                case 'datamatrix':
                    $barcodeStyle = array(
                        'stretch' => true,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'vpadding' => 'auto',
                        'fgcolor' => $this->builder->toColorArray($data['fontColor']),
                        'bgcolor' => $this->builder->toColorArray($bgColor == '' ? '#FFFFFF' : $bgColor),
                        'text' => true,
                        'font' => $this->builder->fontMap['regular'],
                        'fontsize' => $fontSize,
                        'stretchtext' => 1,
                    );

                    $fromEncoding = mb_detect_encoding($data['textBlock']);
                    $text = mb_convert_encoding($data['textBlock'], "ISO-8859-1", $fromEncoding);
                    $this->pdf->write2DBarcode($text, 'DATAMATRIX', $x=$posX, $y=$posY, $textWidth, $height, $barcodeStyle);
                    break;
            }
            $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            //$this->pdf->writeHTML($text, $ln=true, $fill=false, $reseth=true, $cell=false, $alignment);

            if ($rotation != 0) {
                $this->pdf->StopTransform();
            }
        }
    }

    public function getTopMargin($align, $width, $height, $text, $border) {
        $stringHeight = $this->pdf->getStringHeight($width, $text, true, true, '', $border);
        $freeHeight = $height - $stringHeight;
        $topMargin = 0;
        switch ($align) {
            case 'middle':
                $topMargin = $freeHeight / 2;
                break;
            case 'bottom':
                $topMargin = $freeHeight;
                break;
            default:
                $topMargin = 0;
                break;
        }
        return $topMargin;
    }
}
