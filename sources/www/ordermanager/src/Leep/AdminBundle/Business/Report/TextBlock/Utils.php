<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class Utils {
    public static function renderRingPattern($container, $builder, $pdf, $options, $prefix = '') {
        // Draw circle c0.r1
        $color2 = $builder->toColorArray($options['color.outer-ring']);
        $pdf->setX(0);
        $pdf->setY(0);

        $pdf->startTransform();
        $pdf->Circle(
            $x = $options[$prefix.'ring0.center.x'], 
            $y = $options[$prefix.'ring0.center.y'],
            $r = $options[$prefix.'ring0.c2.radius'], 
            0, 360,
            'F',
            array(),
            $color2
        );
        $pdf->stopTransform();

        /////////////////////////////////////
        // x. Draw circle C1
        $color = $builder->toColorArray($options['color.outer-ring']);
        $pdf->setX(0);
        $pdf->setY(0);

        $pdf->startTransform();
        $pdf->Circle(
            $x = $options[$prefix.'ring1.center.x'], 
            $y = $options[$prefix.'ring1.center.y'],
            $r = $options[$prefix.'ring1.radius'], 
            0, 360,
            'F',
            array(),
            $color
        );
        $pdf->stopTransform();

        // Bone R0-C1
        $pdf->setLineWidth($options[$prefix.'bone0-1.size']);
        $pdf->Line(
            $x0 = $options[$prefix.'ring0.center.x'],
            $y0 = $options[$prefix.'ring0.center.y'],
            $x1 = $options[$prefix.'ring1.center.x'],
            $y1 = $options[$prefix.'ring1.center.y'],
            array(
                'color' => $color
            )
        );

        /////////////////////////////////////
        // x. Draw circle C2
        $color = $builder->toColorArray($options['color.outer-ring']);
        $pdf->setX(0);
        $pdf->setY(0);

        $pdf->startTransform();
        $pdf->Circle(
            $x = $options[$prefix.'ring2.center.x'], 
            $y = $options[$prefix.'ring2.center.y'],
            $r = $options[$prefix.'ring2.radius'], 
            0, 360,
            'F',
            array(),
            $color
        );
        $pdf->stopTransform();

        // Bone R0-C2
        $pdf->setLineWidth($options[$prefix.'bone0-2.size']);
        $pdf->Line(
            $x0 = $options[$prefix.'ring0.center.x'],
            $y0 = $options[$prefix.'ring0.center.y'],
            $x1 = $options[$prefix.'ring2.center.x'],
            $y1 = $options[$prefix.'ring2.center.y'],
            array(
                'color' => $color
            )
        );

        /////////////////////////////////////
        // x. Draw circle C0.R0      
        $color1 = $builder->toColorArray($options['color.inner-ring']);
        $pdf->setX(0);
        $pdf->setY(0);

        $pdf->startTransform();
        $pdf->Circle(
            $x = $options[$prefix.'ring0.center.x'], 
            $y = $options[$prefix.'ring0.center.y'],
            $r = $options[$prefix.'ring0.c1.radius'], 
            0, 360,
            'F',
            array(),
            $color1
        );
        $pdf->stopTransform();

        /////////////////////////////////////
        // x. Draw image        
        $image = isset($options[$prefix.'ring0.c0.image']) ? $options[$prefix.'ring0.c0.image'] : '';
        $imageFilePath = $container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $imageFile = $imageFilePath.'/'.$image;

        $pdf->setX(0);
        $pdf->setY(0);

        if (trim($image) != '') {

            $pdf->startTransform();

            // Clipping masks
            $pdf->Circle(
                $x = $options[$prefix.'ring0.center.x'], 
                $y = $options[$prefix.'ring0.center.y'],
                $r = $options[$prefix.'ring0.c0.radius'], 
                0, 360, 'CNZ'
            );

            $pdf->translateX($options[$prefix.'ring0.c0.image.x']);
            $pdf->translateY($options[$prefix.'ring0.c0.image.y']);        
                
            $pdf->myDrawImage(
                $file = $imageFile,
                $x = $options[$prefix.'ring0.c0.image.x'],
                $y = $options[$prefix.'ring0.c0.image.y'],
                $w = intval($options[$prefix.'ring0.c0.image.width']),
                $h = intval($options[$prefix.'ring0.c0.image.height']),
                $type = '',
                $link = '',
                $align = 'N',
                $resize = true,
                $dpi = 300,
                $palign = 'L'
            );
            $pdf->stopTransform();
        }
        else {
            $color1 = $builder->toColorArray($options['color.center-ring']);
            $pdf->setX(0);
            $pdf->setY(0);

            $pdf->startTransform();
            $pdf->Circle(
                $x = $options[$prefix.'ring0.center.x'], 
                $y = $options[$prefix.'ring0.center.y'],
                $r = $options[$prefix.'ring0.c0.radius'], 
                0, 360,
                'F',
                array(),
                $color1
            );
            $pdf->stopTransform();
        }
    }
}