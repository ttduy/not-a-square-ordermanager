<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextTool extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $view->vars['bgColor'] = $this->container->get('leep_admin.report.business.util')->getTextToolColors();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $cacheBoxData = $this->container->get('leep_admin.helper.cache_box_data');
        $utils = $this->container->get('leep_admin.report.business.util');

        $builder->add('idTextToolType',     'choice', array(
            'label'  => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TextTool_Type'),
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('sortOrder',          'hidden');

        $ctrlMap = $mapping->getMapping('LeepAdmin_TextTool_CtrlMapping');
        foreach ($ctrlMap as $v) {
            $class = '\Leep\AdminBundle\Business\Report\Form\AppText'.ucfirst($v);
            $builder->add($v, new $class($this->container));
        }
    }
}