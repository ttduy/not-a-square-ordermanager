<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public $reportSections = array();
    public $idReport;
    public $idReportSection;
    public function loadEntity($request) {
        $doctrine = $this->container->get('doctrine');
        $this->idReportSection = $request->query->get('id');
        $reportSection = $doctrine->getRepository('AppDatabaseMainBundle:ReportSection')->findOneById($this->idReportSection);
        $this->idReport = $reportSection->getIdReport();

        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Report', 'app_main')->findOneById($reportSection->getIdReport());
    }
    public function convertToFormModel($entity) {        
        $mapping = $this->container->get('easy_mapping');

        $model = new EditModel();
        $model->name = $entity->getName();
        $model->shortName = $entity->getShortName();
        $model->reportCode = $entity->getReportCode();
        $model->idReportSection = $this->idReportSection;
        $model->testData = $entity->getTestData();
        $model->idLanguage = 1;

        // Load Text Tool List
        $ctrlMapping = $mapping->getMapping('LeepAdmin_TextTool_CtrlMapping');
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportItem', 'p')
            ->andWhere('p.idReport = :idReport')
            ->andWhere('p.idReportSection = :idReportSection')
            ->setParameter('idReport', $this->idReport)
            ->setParameter('idReportSection', $this->idReportSection)
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $textTool = array();
            $textTool['idTextToolType'] = $row->getIdTextToolType();
            $textTool[$ctrlMapping[$row->getIdTextToolType()]] = json_decode($row->getData(), true);
            $model->textToolList[] = $textTool;
        }

        return $model;
    }

    public function getReportSections() {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $this->idReport)
            ->orderBy('p.sortOrder', 'ASC');
        $mapping = array();
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $mapping[$r->getId()] = $r->getName();
        }
        return $mapping;
    }

    public function buildForm($builder) {
        ini_set("memory_limit", "2048M");
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));
        $builder->add('shortName', 'text', array(
            'attr'     => array(
                'placeholder' => 'Short name'
            ),
            'required' => false
        ));
        $builder->add('reportCode', 'text', array(
            'label'    => 'Report code',
            'attr'     => array(
                'placeholder' => 'Report code'
            ),
            'required' => true
        ));
        $builder->add('testData', 'textarea', array(
            'attr'     => array(
                'placeholder' => 'Test data',
                'rows'        => '10',
                'cols'        => '80'
            ),
            'required' => false
        ));
        $builder->add('idReportSection', 'choice', array(
            'label'    => 'Section',
            'choices'  => $this->getReportSections(),
            'required' => true,
        ));

        $builder->add('sectionDesign', 'section', array(
            'label'    => 'Report design',
            'property_path' => false
        ));

        $builder->add('textToolList', 'collection', array(
            'type' => new Form\AppTextTool($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Text Tool',
                'required' => false
            )
        ));

        $builder->add('sectionPreview', 'section', array(
            'label'    => 'Preview',
            'property_path' => false
        ));
        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'required' => true,
            'attr'     => array(
                'style' => 'min-width: 150px'
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $mapping = $this->container->get('easy_mapping');

        $this->entity->setName($model->name);
        $this->entity->setShortName($model->shortName);
        $this->entity->setReportCode($model->reportCode);
        $this->entity->setTestData($model->testData);

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ReportItem', array('idReportSection' => $this->idReportSection));
        $ctrlMapping = $mapping->getMapping('LeepAdmin_TextTool_CtrlMapping');
        foreach ($model->textToolList as $textTool) {
            $idTextToolType = $textTool['idTextToolType'];
            $configValue = $textTool[$ctrlMapping[$idTextToolType]];

            $item = new Entity\ReportItem();
            $item->setIdReport($this->idReport);
            $item->setIdReportSection($this->idReportSection);
            $item->setIdTextToolType($idTextToolType);
            $item->setData(json_encode($configValue));
            $item->setSortOrder($textTool['sortOrder']);
            $em->persist($item);
        }
        $em->flush();

        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
