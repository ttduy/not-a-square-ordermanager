<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business;

class StructureBodBarcode {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {
        $style = array(
            'xres' => '0.4',
            'height' => '10',
            'color' => '#FF0000',
            'bgcolor' => '#FFFFFF'
        );
        $style = $this->builder->parseInput($style, $data['style']);

        $barcode = '0000000000000';
        $idType = isset($data['idBarcodeType']) ? $data['idBarcodeType'] : Business\Report\Constants::BOD_BARCODE_TYPE_BOOKBLOCK;
        if (!empty($this->builder->bodBarcodeBook)) {
            if ($idType == Business\Report\Constants::BOD_BARCODE_TYPE_BOOKBLOCK) {
                $barcode = $this->builder->bodBarcodeBook;
            } else if ($idType == Business\Report\Constants::BOD_BARCODE_TYPE_ORDER_NUMBER) {
                $barcode = $this->builder->bodBarcodeOrderNumber;
            } else {
                $barcode = $this->builder->bodBarcodeCover;
            }
        }

        $barcodeStyle = array(
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'vpadding' => 'auto',
            'fgcolor' => $this->builder->toColorArray($style['color']),
            'bgcolor' => $this->builder->toColorArray($style['bgcolor']),
            'text' => true,
            'font' => $this->builder->fontMap['regular'],
            'fontsize' => 15,
            'stretchtext' => 2,
        );
        $x = intval($data['posX']);
        $y = intval($data['posY']);
        if ($x < 0) { $x += $this->pdf->getPageWidth(); }
        if ($y < 0) { $y += $this->pdf->getPageHeight(); }
        $this->pdf->write1DBarcode($barcode, 'EAN13', $x, $y, '', $style['height'], floatval($style['xres']), $barcodeStyle);
    }
}
