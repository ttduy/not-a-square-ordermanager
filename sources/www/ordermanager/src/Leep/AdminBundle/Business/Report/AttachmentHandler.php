<?php
namespace Leep\AdminBundle\Business\Report;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class AttachmentHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return array();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Report resources',
            'required' => false,
            'key'      => 'report_resource',
            'snapshot' => false,
            'mode'     => 'full'
        ));

        $builder->add('attachment2', 'file_attachment', array(
            'label'    => 'External PDF',
            'required' => false,
            'key'      => 'external_pdf',
            'snapshot' => false,
            'mode'     => 'full'
        ));

        $builder->add('attachment3', 'file_attachment', array(
            'label'    => 'Third party external PDF',
            'required' => false,
            'key'      => 'third_party_pdf',
            'snapshot' => false,
            'mode'     => 'full'
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();     
        parent::onSuccess();
    }
}
