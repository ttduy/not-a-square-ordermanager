<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureBodBarcode extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_bod_barcode';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('posX',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('posY',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('style', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            ) 
        ));
        $builder->add('idBarcodeType', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getBodBarcodeTypes(),
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
    }
}
