<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppReportSection extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_report_section';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $manager = $this->container->get('easy_module.manager');
        $view->vars['editId'] = $this->container->get('request')->get('id', 0);
        $view->vars['editUrl'] = $manager->getUrl('leep_admin', 'report', 'edit', 'edit');

        if ($data['idType'] == 2) {
            $view->vars['sectionId'] = $data['idReportSectionLink'];
        }
        else if ($data['idType'] == 3) {
            $view->vars['isReportLink'] = true;
        }
        else {
            $view->vars['sectionId'] = $data['id'];
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $utils = $this->container->get('leep_admin.report.business.util');
        $mgr = $this->container->get('easy_module.manager');
        $cacheBoxData = $this->container->get('leep_admin.helper.cache_box_data');

        $builder->add('id',                 'hidden');
        $builder->add('name',     'text', array(
            'label'    => 'Name',
            'required' => false,
            'attr'     => array('style' => 'min-width: 95%; max-width: 95%')
        ));
        $builder->add('alias',     'text', array(
            'label'    => 'Alias',
            'required' => false,
            'attr'     => array('style' => 'min-width: 10%; max-width: 10%')
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(2 => 'Link', 3 => 'Report link'),
            'attr'     => array('style' => 'min-width: 5%; max-width: 5%')
        ));

        $builder->add('idReportSectionLink', 'searchable_cache_box', array(
            'label'    => 'Report section',
            'required' => false,
            'width'    => '95%',
            'dataKey'  => 'reportSections'
        ));
        if (!$cacheBoxData->hasKey('reportSections')) {
            $cacheBoxData->putKey('reportSections', $utils->getReportSectionForSearchBox());
        }

        $builder->add('idReportLink', 'searchable_cache_box', array(
            'label'    => 'Report section',
            'required' => false,
            'width'    => '95%',
            'dataKey'  => 'reports'
        ));
        if (!$cacheBoxData->hasKey('reports')) {
            $cacheBoxData->putKey('reports', $utils->getReportForSearchBox());
        }

        $builder->add('isHide',   'checkbox', array(
            'label'    => 'Hide?',
            'required' => false
        ));
        $builder->add('sortOrder',          'hidden');
    }
}