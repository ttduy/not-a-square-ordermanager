<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business\Report\Constants;

class WidgetFoodTable2 {
    public $container;
    public $pdf;


    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {
        $this->loadModel($data);
        $this->render();
    }

    ////////////////////////////////////////////////////////
    // x. Render
    public $RISK_FOR_WEIGHT_WIDTH                   = 199;
    public $RISK_FOR_WEIGHT_X                       = 0;
    public $RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH     = 80;
    public $RISK_FOR_WEIGHT_G_PRO_ARTIKEL_X         = 0;
    public $RISK_FOR_WEIGHT_POSITIVE_WIDTH          = 59;
    public $RISK_FOR_WEIGHT_POSITIVE_X              = 80;
    public $RISK_FOR_WEIGHT_NEGATIVE_WIDTH          = 60;
    public $RISK_FOR_WEIGHT_NEGATIVE_X              = 140;
    public $HEALTHY_NUTRITION_WIDTH                 = 119;
    public $HEALTHY_NUTRITION_X                     = 200;
    public $HEALTHY_NUTRITION_POSITIVE_WIDTH        = 59;
    public $HEALTHY_NUTRITION_POSITIVE_X            = 200;
    public $HEALTHY_NUTRITION_NEGATIVE_WIDTH        = 60;
    public $HEALTHY_NUTRITION_NEGATIVE_X            = 260;
    public $ATHLETIC_PERFORMANCE_WIDTH              = 119;
    public $ATHLETIC_PERFORMANCE_X                  = 320;
    public $ATHLETIC_PERFORMANCE_POSITIVE_WIDTH     = 59;
    public $ATHLETIC_PERFORMANCE_POSITIVE_X         = 320;
    public $ATHLETIC_PERFORMANCE_NEGATIVE_WIDTH     = 60;
    public $ATHLETIC_PERFORMANCE_NEGATIVE_X         = 380;
    public $WARNING_COL_WIDTH                       = 86;
    public $WARNING_SUBCOL_WIDTH                    = 14;
    public $WARNING_COL_X                           = 440;
    public $WARNING_COL_IGE_ALLERGY_X               = 440;
    public $WARNING_COL_IGG_INTOLERANCE_X           = 454;
    public $WARNING_COL_GENE_INTOLERANCE_1_X        = 468;
    public $WARNING_COL_GENE_INTOLERANCE_2_X        = 482;
    public $WARNING_COL_FOOD_PREFERENCE_X           = 496;
    public $FOOD_ITEM_LABEL_WIDTH                   = 170;
    public $FOOD_ITEM_LABEL_X                       = 512;
    public $TYPICAL_PORTION_WIDTH                   = 90;
    public $TYPICAL_PORTION_SUBCOL_WIDTH            = 18;
    public $TYPICAL_PORTION_X                       = 682;
    public $INGREDIENT_DISPLAY_COL_X                = 690;
    public $INGREDIENT_DISPLAY_COL_WIDTH            = 16;
    public $INGREDIENT_DISPLAY_MAX                  = 6;

    public function render() {
        $pageRotate = 'L';
        if (isset($this->parameters['page.rotate']) && trim($this->parameters['page.rotate']) != '') {
            $pageRotate = $this->parameters['page.rotate'];
        }

        $prevPageUnit = $this->pdf->getPageUnit();
        $this->pdf->setPageUnit('pt');
        $prevCellPadding = $this->pdf->getCellPaddings();
        mb_internal_encoding("UTF-8");
        $this->pdf->SetAutoPageBreak(FALSE);
        $this->pdf->setFont($this->builder->fontMap['regular'], '', 10, '', true);

        // Check last tools
        if ($this->builder->lastTextTool['idTextToolType'] != Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE &&
            $this->builder->lastTextTool['idTextToolType'] != Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE_2) {
            // Force to create new page
            $this->pdf->deletePage($this->pdf->getPage());
            $this->pdf->addPage($pageRotate);
            $this->builder->currentFoodTablePosition = 0;
            $this->displayFooter();
        }

        ////////////////////////////////////////////////////
        // x. Calculate positions
        $this->RISK_FOR_WEIGHT_WIDTH                   = 200;
        $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH     = 80;
        $this->RISK_FOR_WEIGHT_POSITIVE_WIDTH          = 60;
        $this->RISK_FOR_WEIGHT_NEGATIVE_WIDTH          = 60;
        $this->HEALTHY_NUTRITION_WIDTH                 = 120;
        $this->HEALTHY_NUTRITION_POSITIVE_WIDTH        = 60;
        $this->HEALTHY_NUTRITION_NEGATIVE_WIDTH        = 60;
        $this->ATHLETIC_PERFORMANCE_WIDTH              = 120;
        $this->ATHLETIC_PERFORMANCE_POSITIVE_WIDTH     = 60;
        $this->ATHLETIC_PERFORMANCE_NEGATIVE_WIDTH     = 60;
        $this->WARNING_COL_WIDTH                       = 86;
        $this->WARNING_SUBCOL_WIDTH                    = 14;
        $this->FOOD_ITEM_LABEL_WIDTH                   = 175;
        $this->TYPICAL_PORTION_WIDTH                   = 90;
        $this->TYPICAL_PORTION_SUBCOL_WIDTH            = 18;
        if ($this->parameters['col-body-weight.visible'] == 'NO') {
            $this->RISK_FOR_WEIGHT_WIDTH = 0;
            $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH = 0;
            $this->FOOD_ITEM_LABEL_WIDTH += 200;
        } else {
            if ($this->parameters['col-body-weight.g-pro-artikel.visible'] == 'NO') {
                $this->RISK_FOR_WEIGHT_WIDTH -= 80;
                $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH = 0;
                $this->FOOD_ITEM_LABEL_WIDTH += 80;
            }
        }

        if ($this->parameters['col-healthy-nutrition.visible'] == 'NO') {
            $this->HEALTHY_NUTRITION_WIDTH = 0;
            $this->FOOD_ITEM_LABEL_WIDTH += 120;
        }

        if ($this->parameters['col-athletic-performance.visible'] == 'NO') {
            $this->ATHLETIC_PERFORMANCE_WIDTH = 0;
            $this->FOOD_ITEM_LABEL_WIDTH += 120;
        }

        if ($this->parameters['col-typical-portion.visible'] == 'NO') {
            $this->TYPICAL_PORTION_WIDTH = 0;
            $this->FOOD_ITEM_LABEL_WIDTH += 90;
        }

        $this->RISK_FOR_WEIGHT_X = 0;
        $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_X = 0;
        $this->RISK_FOR_WEIGHT_POSITIVE_X = $this->RISK_FOR_WEIGHT_X + $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH;
        $this->RISK_FOR_WEIGHT_NEGATIVE_X = $this->RISK_FOR_WEIGHT_POSITIVE_X + $this->RISK_FOR_WEIGHT_POSITIVE_WIDTH;
        $this->HEALTHY_NUTRITION_X = $this->RISK_FOR_WEIGHT_X + $this->RISK_FOR_WEIGHT_WIDTH;
        $this->HEALTHY_NUTRITION_POSITIVE_X = $this->HEALTHY_NUTRITION_X;
        $this->HEALTHY_NUTRITION_NEGATIVE_X = $this->HEALTHY_NUTRITION_POSITIVE_X + $this->HEALTHY_NUTRITION_POSITIVE_WIDTH;
        $this->ATHLETIC_PERFORMANCE_X = $this->HEALTHY_NUTRITION_X + $this->HEALTHY_NUTRITION_WIDTH;
        $this->ATHLETIC_PERFORMANCE_POSITIVE_X = $this->ATHLETIC_PERFORMANCE_X;
        $this->ATHLETIC_PERFORMANCE_NEGATIVE_X = $this->ATHLETIC_PERFORMANCE_POSITIVE_X + $this->ATHLETIC_PERFORMANCE_POSITIVE_WIDTH;
        $this->WARNING_COL_X = $this->ATHLETIC_PERFORMANCE_X + $this->ATHLETIC_PERFORMANCE_WIDTH;
        $warningColX = $this->WARNING_COL_X;
        if ($this->parameters['col-ige-allergy.visible'] == 'YES') {
            $this->WARNING_COL_IGE_ALLERGY_X = $warningColX;
            $warningColX += $this->WARNING_SUBCOL_WIDTH;
        } else {
            $this->FOOD_ITEM_LABEL_WIDTH += $this->WARNING_SUBCOL_WIDTH;
        }

        if ($this->parameters['col-igg-intolerance.visible'] == 'YES') {
            $this->WARNING_COL_IGG_INTOLERANCE_X = $warningColX;
            $warningColX += $this->WARNING_SUBCOL_WIDTH;
        } else {
            $this->FOOD_ITEM_LABEL_WIDTH += $this->WARNING_SUBCOL_WIDTH;
        }

        if ($this->parameters['col-gene-intolerance-1.visible'] == 'YES') {
            $this->WARNING_COL_GENE_INTOLERANCE_1_X = $warningColX;
            $warningColX += $this->WARNING_SUBCOL_WIDTH;
        } else {
            $this->FOOD_ITEM_LABEL_WIDTH += $this->WARNING_SUBCOL_WIDTH;
        }

        if ($this->parameters['col-gene-intolerance-2.visible'] == 'YES') {
            $this->WARNING_COL_GENE_INTOLERANCE_2_X = $warningColX;
            $warningColX += $this->WARNING_SUBCOL_WIDTH;
        } else {
            $this->FOOD_ITEM_LABEL_WIDTH += $this->WARNING_SUBCOL_WIDTH;
        }

        if ($this->parameters['col-food-preference.visible'] == 'YES') {
            $this->WARNING_COL_FOOD_PREFERENCE_X = $warningColX;
            $warningColX += $this->WARNING_SUBCOL_WIDTH;
        } else {
            $this->FOOD_ITEM_LABEL_WIDTH += $this->WARNING_SUBCOL_WIDTH;
        }

        $this->FOOD_ITEM_LABEL_X = $warningColX;
        if ($warningColX != $this->WARNING_COL_X) {
            $this->FOOD_ITEM_LABEL_X += 2;
        }

        $this->TYPICAL_PORTION_X = $this->FOOD_ITEM_LABEL_X + $this->FOOD_ITEM_LABEL_WIDTH;

        if ($this->ingredientDisplayEnabled) {
            $this->LEFT_MARGIN -= 12;
            $this->RISK_FOR_WEIGHT_WIDTH -= 30;
            $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH -= 30;

            if ($this->parameters['col-body-weight.g-pro-artikel.visible'] != "YES") {
                $this->RISK_FOR_WEIGHT_X -= 30;
                $this->RISK_FOR_WEIGHT_WIDTH += 30;
            }

            $this->RISK_FOR_WEIGHT_POSITIVE_X -= 30;
            $this->RISK_FOR_WEIGHT_NEGATIVE_X -= 30;

            $this->HEALTHY_NUTRITION_X -= 30;
            $this->HEALTHY_NUTRITION_POSITIVE_X -= 30;
            $this->HEALTHY_NUTRITION_NEGATIVE_X -= 30;

            $this->ATHLETIC_PERFORMANCE_X -= 30;
            $this->ATHLETIC_PERFORMANCE_POSITIVE_X -= 30;
            $this->ATHLETIC_PERFORMANCE_NEGATIVE_X -= 30;

            $this->WARNING_COL_X -= 30;
            $this->WARNING_COL_IGE_ALLERGY_X -= 30;
            $this->WARNING_COL_IGG_INTOLERANCE_X -= 30;
            $this->WARNING_COL_GENE_INTOLERANCE_1_X -= 30;
            $this->WARNING_COL_GENE_INTOLERANCE_2_X -= 30;
            $this->WARNING_COL_FOOD_PREFERENCE_X -= 30;

            $this->FOOD_ITEM_LABEL_WIDTH -= 20;
            $this->FOOD_ITEM_LABEL_X -= 30;
            $this->TYPICAL_PORTION_X -= 50;
            $this->INGREDIENT_DISPLAY_COL_X -= 50;

            $freeSpace = 96;
            $spaceNeed = count($this->ingredientDisplay) * $this->INGREDIENT_DISPLAY_COL_WIDTH;
            if ($spaceNeed > 96) {
                $spaceNeed = 96;
            }

            $this->LEFT_MARGIN += ($freeSpace - $spaceNeed) / 2;
        }

        ////////////////////////////////////////////////////
        // x. Render table
        while (!empty($this->foods)) {
            // Add header
            $this->builder->currentFoodTablePosition += 7;
            if ($this->builder->currentFoodTablePosition >= $this->MAX_SLICE_PER_PAGE- 2) {
                $this->pdf->addPage($pageRotate);
                $this->pdf->setPageUnit('pt');

                $this->builder->currentFoodTablePosition = 7;
                $this->displayFooter();
            }

            $startY = $this->TOP_MARGIN + ($this->builder->currentFoodTablePosition - 7) * $this->SLICE_HEIGHT;
            $foods = [];
            while (!empty($this->foods) && $this->builder->currentFoodTablePosition < $this->MAX_SLICE_PER_PAGE) {
                $foods[] = array_shift($this->foods);
                $this->builder->currentFoodTablePosition++;
            }

            $template_id = $this->pdf->startTemplate();

            ////////////////////////////////////////////////////////////////
            // x. Render header
            $extraProductWidth = 0;

            // Column #1: Risk for Weight
            if ($this->parameters['col-body-weight.visible'] == 'YES') {
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 28, 2, 2);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_X);
                $this->pdf->translateY($startY);
                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_WIDTH-3, 48, $this->parameters['col-body-weight.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->setCellPaddings(2, 0, 2, 0);
                $this->pdf->setFont('custom1', '', 28, '', false);
                $this->pdf->startTransform();

                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_X);
                $this->pdf->translateY($startY-2);
                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_WIDTH-3, 28, 'F', 0, 'C', false, 0);
                $this->pdf->stopTransform();

                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 14, 2, 2);
                if ($this->parameters['col-body-weight.g-pro-artikel.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_X);
                    $this->pdf->translateY($startY + 50);
                    $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH-2, 36, $this->parameters['col-body-weight.g-pro-artikel.label'], 0, 'C', true, 0);
                    $this->pdf->stopTransform();
                }

                $this->pdf->setCellPaddings(2, 4, 2, 2);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_POSITIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_NEGATIVE_WIDTH-2, 17, $this->parameters['labels.frequent'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_NEGATIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_POSITIVE_WIDTH-3, 17, $this->parameters['labels.rare'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_POSITIVE_X);
                $this->pdf->translateY($startY + 69);
                $this->printBars(10, 'F', true);
                $this->pdf->stopTransform();
            }

            // Column #2: Health Nutrition
            if ($this->parameters['col-healthy-nutrition.visible'] == 'YES') {
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 28, 2, 2);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_X);
                $this->pdf->translateY($startY);
                $this->pdf->MultiCell($this->HEALTHY_NUTRITION_WIDTH-3, 48, $this->parameters['col-healthy-nutrition.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->setCellPaddings(2, 0, 2, 0);
                $this->pdf->setFont('custom1', '', 24, '', false);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_X);
                $this->pdf->translateY($startY);
                $this->pdf->MultiCell($this->HEALTHY_NUTRITION_WIDTH-3, 28, 'E', 0, 'C', false, 0);
                $this->pdf->stopTransform();

                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 4, 2, 2);

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_POSITIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->HEALTHY_NUTRITION_NEGATIVE_WIDTH-2, 17, $this->parameters['labels.frequent'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_NEGATIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->HEALTHY_NUTRITION_POSITIVE_WIDTH-3, 17, $this->parameters['labels.rare'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_POSITIVE_X);
                $this->pdf->translateY($startY + 69);
                $this->printBars(10, 'E', true);
                $this->pdf->stopTransform();
            }

            // Column #3: Athletic performance
            if ($this->parameters['col-athletic-performance.visible'] == 'YES') {
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 28, 2, 2);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_X);
                $this->pdf->translateY($startY);
                $this->pdf->MultiCell($this->ATHLETIC_PERFORMANCE_WIDTH-3, 48, $this->parameters['col-athletic-performance.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->setCellPaddings(2, 0, 2, 0);
                $this->pdf->setFont('custom1', '', 24, '', false);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_X);
                $this->pdf->translateY($startY+1);
                $this->pdf->MultiCell($this->ATHLETIC_PERFORMANCE_WIDTH-3, 28, 'G', 0, 'C', false, 0);
                $this->pdf->stopTransform();

                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 4, 2, 2);

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_POSITIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->ATHLETIC_PERFORMANCE_NEGATIVE_WIDTH-2, 17, $this->parameters['labels.frequent'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_NEGATIVE_X);
                $this->pdf->translateY($startY + 50);
                $this->pdf->MultiCell($this->ATHLETIC_PERFORMANCE_POSITIVE_WIDTH-3, 17, $this->parameters['labels.rare'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_POSITIVE_X);
                $this->pdf->translateY($startY + 69);
                $this->printBars(10, 'G', true);
                $this->pdf->stopTransform();
            }

            // Column #4: Warnings
            $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
            $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
            $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
            $this->pdf->setCellPaddings(2, 2, 0, 0);
            $this->pdf->startTransform();
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->rotate(90);
            $this->pdf->translateX(-$startY);
            $this->pdf->translateY($this->LEFT_MARGIN + $this->WARNING_COL_X - 14);
            if ($this->parameters['col-ige-allergy.visible'] == 'YES') {
                $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                $this->pdf->translateY(14);
                $this->pdf->MultiCell($this->WARNING_COL_WIDTH, 13, $this->parameters['col-ige-allergy.label'], 0, 'L', true, 0);
            }

            if ($this->parameters['col-igg-intolerance.visible'] == 'YES') {
                $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                $this->pdf->translateY(14);
                $this->pdf->MultiCell($this->WARNING_COL_WIDTH, 13, $this->parameters['col-igg-intolerance.label'], 0, 'L', true, 0);
            }

            if ($this->parameters['col-gene-intolerance-1.visible'] == 'YES') {
                $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                $this->pdf->translateY(14);
                $this->pdf->MultiCell($this->WARNING_COL_WIDTH, 13, $this->parameters['col-gene-intolerance-1.label'], 0, 'L', true, 0);
            }

            if ($this->parameters['col-gene-intolerance-2.visible'] == 'YES') {
                $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                $this->pdf->translateY(14);
                $this->pdf->MultiCell($this->WARNING_COL_WIDTH, 13, $this->parameters['col-gene-intolerance-2.label'], 0, 'L', true, 0);
            }

            if ($this->parameters['col-food-preference.visible'] == 'YES') {
                $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                $this->pdf->translateY(14);
                $this->pdf->MultiCell($this->WARNING_COL_WIDTH, 13, $this->parameters['col-food-preference.label'], 0, 'L', true, 0);
            }

            $this->pdf->stopTransform();
            $this->pdf->setFont('custom1', '', 11, '', false);
            $this->pdf->setCellPaddings(0, 0, 0, 0);
            $this->pdf->startTransform();
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_X);
            $this->pdf->translateY($startY);

            if ($this->parameters['col-ige-allergy.visible'] == 'YES') {
                $this->pdf->setFont('custom1', '', 11, '', false);
                $this->pdf->setCellPaddings(0, 1, 0, 0);
                $this->pdf->MultiCell($this->WARNING_SUBCOL_WIDTH, 13, 'D', 0, 'C', false, 0);
            }

            if ($this->parameters['col-igg-intolerance.visible'] == 'YES') {
                $this->pdf->setFont('custom1', '', 11, '', false);
                $this->pdf->setCellPaddings(0, 1, 0, 0);
                $this->pdf->MultiCell($this->WARNING_SUBCOL_WIDTH, 13, 'D', 0, 'C', false, 0);
            }
            if ($this->parameters['col-gene-intolerance-1.visible'] == 'YES') {
                $this->pdf->setFont('custom1', '', 13, '', false);
                $this->pdf->setCellPaddings(0, 0, 0, 0);
                $this->pdf->translateX(3);
                $this->pdf->MultiCell($this->WARNING_SUBCOL_WIDTH, 13, 'C', 0, 'C', false, 0);
            }

            if ($this->parameters['col-gene-intolerance-2.visible'] == 'YES') {
                $this->pdf->setFont('custom1', '', 11, '', false);
                $this->pdf->setCellPaddings(0, 1, 0, 0);
                $this->pdf->translateX(-4);
                $this->pdf->MultiCell($this->WARNING_SUBCOL_WIDTH, 13, 'B', 0, 'C', false, 0);
            }

            if ($this->parameters['col-food-preference.visible'] == 'YES') {
                $this->pdf->setFont('custom1', '', 11, '', false);
                $this->pdf->setCellPaddings(0, 2, 0, 0);
                $this->pdf->MultiCell($this->WARNING_SUBCOL_WIDTH, 13, 'A', 0, 'C', false, 0);
            }
            $this->pdf->stopTransform();

            // Column #5: Food Label
            $fontSize = 7;
            if (!isset($this->parameters['col-food-name.fontSize']) || trim($this->parameters['col-food-name.fontSize']) === '') {
                $fontSize = 7;
            } else {
                $fontSize = $this->parameters['col-food-name.fontSize'];
            }


            $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
            $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
            $this->pdf->setCellPaddings(2, 12, 2, 0);
            $this->pdf->setFont($this->builder->fontMap['regular'], '', $fontSize, '', true);
            $this->pdf->startTransform();
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->translateX($this->LEFT_MARGIN + $this->FOOD_ITEM_LABEL_X);
            $this->pdf->translateY($startY);
            $this->pdf->MultiCell($this->FOOD_ITEM_LABEL_WIDTH-3, 86, $this->parameters['col-food-name.label'], 0, 'C', true, 0);
            $this->pdf->stopTransform();

            // Column #6: Typical Portion
            if ($this->parameters['col-typical-portion.visible'] == 'YES') {
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['header.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['header.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
                $this->pdf->setCellPaddings(2, 12, 2, 0);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X);
                $this->pdf->translateY($startY);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_WIDTH-1, 67, $this->parameters['col-typical-portion.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->setCellPaddings(0, 5, 0, 0);
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 6, '', true);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X);
                $this->pdf->translateY($startY + 69);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $this->parameters['col-typical-portion.g.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH);
                $this->pdf->translateY($startY + 69);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $this->parameters['col-typical-portion.kcal.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH * 2);
                $this->pdf->translateY($startY + 69);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $this->parameters['col-typical-portion.eiw.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH * 3);
                $this->pdf->translateY($startY + 69);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $this->parameters['col-typical-portion.koh.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();

                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH * 4);
                $this->pdf->translateY($startY + 69);
                $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $this->parameters['col-typical-portion.fett.label'], 0, 'C', true, 0);
                $this->pdf->stopTransform();
            }

            // Column #7: Ingredient display
            if ($this->ingredientDisplayEnabled) {
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->rotate(90);
                $this->pdf->translateX(-$startY);
                $this->pdf->translateY($this->LEFT_MARGIN + $this->INGREDIENT_DISPLAY_COL_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH * 4);
                foreach ($this->ingredientDisplay as $ingredient) {
                    $this->pdf->translateX(-($this->WARNING_COL_WIDTH));
                    $this->pdf->translateY($this->INGREDIENT_DISPLAY_COL_WIDTH);
                    $name = $this->builder->translateBlock($ingredient['name']);
                    $this->pdf->MultiCell($this->WARNING_COL_WIDTH, $this->INGREDIENT_DISPLAY_COL_WIDTH - 1, "  $name", 0, 'L', true, 0);
                }

                $this->pdf->stopTransform();
            }

            ////////////////////////////////////////////////////
            // x. Render content
            $this->pdf->setFillColor(230, 230, 230);
            $this->pdf->setTextColor(0, 0, 0);
            $curY = $startY + 88;
            foreach ($foods as $foodKey => $food) {
                // Column #1: risk for weight
                if ($this->parameters['col-body-weight.visible'] == 'YES') {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.color']));
                    $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-body-weight.g-pro-artikel.visible'] == 'YES') {
                        if ($this->parameters['col-body-weight.g-pro-artikel.active'] == 'NO') {
                            $this->pdf->setTextColorArray(array(119,119,119));
                            $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH-2, 17, '?', 0, 'C', true, 0);
                        }
                        else {
                            if ($food['maxG'] > $this->parameters['anyamount.threshold']) {
                                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH-2, 17, $this->parameters['labels.any-amount'], 0, 'C', true, 0);
                            }
                            else {
                                $weightUnit = $this->builder->localizator->config['food_table_amount.label'];
                                $weightValue =  $this->builder->localizator->translateFoodTableText($food['maxG']);
                                $this->pdf->MultiCell($this->RISK_FOR_WEIGHT_G_PRO_ARTIKEL_WIDTH-2, 17, $weightValue.' '.$weightUnit, 0, 'C', true, 0);
                                $riskWeightGProArtikel = $weightValue.' '.$weightUnit;
                            }
                        }
                    }
                    $this->pdf->stopTransform();

                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->RISK_FOR_WEIGHT_POSITIVE_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-body-weight.active'] == 'YES') {
                        $this->printBars($food['bodyWeightScale'], 'F');
                    }
                    else {
                        $this->printNotOrderedBars();
                    }
                    $this->pdf->stopTransform();
                }

                // Column #2: healthy nutrition
                if ($this->parameters['col-healthy-nutrition.visible'] == 'YES') {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.color']));
                    $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->HEALTHY_NUTRITION_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-healthy-nutrition.active'] == 'YES') {
                        $this->printBars($food['healthyNutritionScale'], 'E');
                    }
                    else {
                        $this->printNotOrderedBars();
                    }
                    $this->pdf->stopTransform();
                }

                // Column #3: Athletic performance
                if ($this->parameters['col-athletic-performance.visible'] == 'YES') {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.color']));
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->ATHLETIC_PERFORMANCE_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-athletic-performance.active'] == 'YES') {
                        $this->printBars($food['athleticPerformanceScale'], 'G');
                    }
                    else {
                        $this->printNotOrderedBars();
                    }
                    $this->pdf->stopTransform();
                }

                // Column #4: Allergy warnings
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);

                if ($this->parameters['col-ige-allergy.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_IGE_ALLERGY_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-ige-allergy.active'] == 'NO') {
                        $this->pdf->setTextColorArray(array(119,119,119));
                        $this->pdf->MultiCell(13, 17, '?', 0, 'C', true, 0);
                    } else {
                        if (isset($food['allergy']['IgE']) && $food['allergy']['IgE']) {
                            $this->pdf->setFont('custom1', '', 12, '', false);
                            $this->pdf->MultiCell(13, 17, 'H', 0, 'C', true, 0);
                            $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                        } else {
                            $this->pdf->MultiCell(13, 17, ' ', 0, 'C', true, 0);
                        }
                    }
                    $this->pdf->stopTransform();
                }

                if ($this->parameters['col-igg-intolerance.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_IGG_INTOLERANCE_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-igg-intolerance.active'] == 'NO') {
                        $this->pdf->setTextColorArray(array(119,119,119));
                        $this->pdf->MultiCell(13, 17, '?', 0, 'C', true, 0);
                    }
                    else {
                        if (isset($food['allergy']['IgG']) && $food['allergy']['IgG']) {
                            $this->pdf->setFont('custom1', '', 12, '', false);
                            $this->pdf->MultiCell(13, 17, 'H', 0, 'C', true, 0);
                            $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                        } else {
                            $this->pdf->MultiCell(13, 17, ' ', 0, 'C', true, 0);
                        }
                    }
                    $this->pdf->stopTransform();
                }

                if ($this->parameters['col-gene-intolerance-1.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_GENE_INTOLERANCE_1_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-gene-intolerance-1.active'] == 'NO') {
                        $this->pdf->setTextColorArray(array(119,119,119));
                        $this->pdf->MultiCell(13, 17, '?', 0, 'C', true, 0);
                    }
                    else {
                        if (isset($food['allergy']['intoleranz1']) && $food['allergy']['intoleranz1']) {
                            $this->pdf->setFont('custom1', '', 12, '', false);
                            $this->pdf->MultiCell(13, 17, 'H', 0, 'C', true, 0);
                            $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                        } else {
                            $this->pdf->MultiCell(13, 17, ' ', 0, 'C', true, 0);
                        }
                    }
                    $this->pdf->stopTransform();
                }

                if ($this->parameters['col-gene-intolerance-2.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_GENE_INTOLERANCE_2_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-gene-intolerance-2.active'] == 'NO') {
                        $this->pdf->setTextColorArray(array(119,119,119));
                        $this->pdf->MultiCell(13, 17, '?', 0, 'C', true, 0);
                    }
                    else {
                        if (isset($food['allergy']['intoleranz2']) && $food['allergy']['intoleranz2']) {
                            $this->pdf->setFont('custom1', '', 12, '', false);
                            $this->pdf->MultiCell(13, 17, 'H', 0, 'C', true, 0);
                            $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                        } else {
                            $this->pdf->MultiCell(13, 17, ' ', 0, 'C', true, 0);
                        }
                    }
                    $this->pdf->stopTransform();
                }

                if ($this->parameters['col-food-preference.visible'] == 'YES') {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->WARNING_COL_FOOD_PREFERENCE_X);
                    $this->pdf->translateY($curY);
                    if ($this->parameters['col-food-preference.active'] == 'NO') {
                        $this->pdf->setTextColorArray(array(119,119,119));
                        $this->pdf->MultiCell(13, 17, '?', 0, 'C', true, 0);
                    }
                    else {
                        if (isset($food['allergy']['eigene']) && $food['allergy']['eigene']) {
                            $this->pdf->setFont('custom1', '', 12, '', false);
                            $this->pdf->MultiCell(13, 17, 'H', 0, 'C', true, 0);
                            $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                        } else {
                            $this->pdf->MultiCell(13, 17, ' ', 0, 'C', true, 0);
                        }
                    }
                    $this->pdf->stopTransform();
                }

                // Column #5: Product label
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.food.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.food.color']));
                $this->pdf->setFont($this->builder->fontMap['regular'], '', 8, '', true);
                $this->pdf->setCellPaddings(3, 4, 0, 0);
                $this->pdf->startTransform();
                $this->pdf->setX(0);
                $this->pdf->setY(0);
                $this->pdf->translateX($this->LEFT_MARGIN + $this->FOOD_ITEM_LABEL_X);
                $this->pdf->translateY($curY);
                $productName = $this->builder->translateBlock($food['name']);
                $productName = $this->trimProductName($productName, $this->FOOD_ITEM_LABEL_WIDTH);
                $this->pdf->MultiCell($this->FOOD_ITEM_LABEL_WIDTH-3, 17, $productName, 0, 'L', true, 0);
                $this->pdf->stopTransform();

                // Column #6: Typical portion
                if ($this->parameters['col-typical-portion.visible'] == 'YES') {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.food.bgcolor']));
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.food.color']));
                    $this->pdf->setFont($this->builder->fontMap['regular'], '', 6, '', true);
                    $this->pdf->setCellPaddings(0, 5, 0, 0);
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X);
                    $this->pdf->translateY($curY);
                    $typical_g_newUnit = $this->builder->localizator->translateFoodTableText($food['typicalPortion']['g']);
                    $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $typical_g_newUnit, 0, 'C', true, 0);
                    $this->pdf->stopTransform();

                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH*1);
                    $this->pdf->translateY($curY);
                    $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $food['typicalPortion']['kcal'], 0, 'C', true, 0);
                    $this->pdf->stopTransform();

                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH*2);
                    $this->pdf->translateY($curY);
                    $typicalPortion_eiw_newUnit = $this->builder->localizator->translateFoodTableText($food['typicalPortion']['eiw']);
                    $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $typicalPortion_eiw_newUnit, 0, 'C', true, 0);
                    $this->pdf->stopTransform();

                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH*3);
                    $this->pdf->translateY($curY);
                    $typicalPortion_koh_newUnit = $this->builder->localizator->translateFoodTableText($food['typicalPortion']['koh']);
                    $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $typicalPortion_koh_newUnit, 0, 'C', true, 0);
                    $this->pdf->stopTransform();

                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->TYPICAL_PORTION_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH*4);
                    $this->pdf->translateY($curY);
                    $typicalPortion_fett_newUnit = $this->builder->localizator->translateFoodTableText($food['typicalPortion']['fett']);
                    $this->pdf->MultiCell($this->TYPICAL_PORTION_SUBCOL_WIDTH-1, 17, $typicalPortion_fett_newUnit, 0, 'C', true, 0);
                    $this->pdf->stopTransform();
                }

                // Column #7: Ingredient display
                if ($this->ingredientDisplayEnabled) {
                    $this->pdf->startTransform();
                    $this->pdf->setX(0);
                    $this->pdf->setY(0);
                    $this->pdf->translateX($this->LEFT_MARGIN + $this->INGREDIENT_DISPLAY_COL_X + $this->TYPICAL_PORTION_SUBCOL_WIDTH * 4 + $this->INGREDIENT_DISPLAY_COL_WIDTH);
                    $this->pdf->translateY($curY);
                    $this->pdf->setTextColorArray([0, 0, 0]);
                    foreach ($this->ingredientDisplay as $ingredient) {
                        $foodKey = $food['id'];
                        $riskPercentage = $ingredient['foods'][$foodKey]['riskPercentage'];
                        $value = $ingredient['foods'][$foodKey]['value'];
                        if ($riskPercentage > $this->foodTableParameter->getColorGreenRiskamount()) {
                            $this->pdf->setTextColorArray([137, 155, 60]);
                        } else if ($riskPercentage < $this->foodTableParameter->getColorRedRiskamount()) {
                            $this->pdf->setTextColorArray([253, 40, 46]);
                        }

                        $this->pdf->MultiCell($this->INGREDIENT_DISPLAY_COL_WIDTH - 1, 17, $value, 0, 'C', true, 0);
                        $this->pdf->translateX(1);
                    }
                    $this->pdf->stopTransform();
                }

                $curY += $this->SLICE_HEIGHT+1;
            }

            $this->pdf->endTemplate();
            $this->pdf->startTransform();
            if ($pageRotate == 'P') {
                $this->pdf->Rotate(90, 0, 0);
                $this->pdf->translateX(-$this->pdf->getPageHeight()+5);
            }
            $this->pdf->printTemplate($template_id);
            $this->pdf->stopTransform();
        }

        $this->pdf->setPageUnit($prevPageUnit);
        $this->pdf->setCellPaddings($prevCellPadding['L'], $prevCellPadding['T'], $prevCellPadding['R'], $prevCellPadding['B']);
    }

    public function roundTo5($value) {
        return ceil(round($value)/5)*5;
    }

    public function printNotOrderedBars() {
        $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
        $this->pdf->MultiCell(9*6, 17, '', 0, 'C', true, 0);

        $this->pdf->setFont($this->builder->fontMap['bold'], '', 8, '', true);
        $this->pdf->setCellPaddings(0, 5, 0, 0);
        $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar-center.bgcolor']));
        $this->pdf->setTextColorArray(array(255, 255, 255));
        $this->pdf->MultiCell(9, 17, '?', 0, 'C', true, 0);

        $this->pdf->setFont($this->builder->fontMap['regular'], '', 7, '', true);
        $this->pdf->setCellPaddings(3, 5, 0, 0);
        $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
        $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar-center.bgcolor']));
        $this->pdf->MultiCell(9*6, 17, $this->parameters['labels.not-ordered'], 0, 'L', true, 0);
    }

    public function printBars($value, $char = 'F', $isFull = false) {
        $L = 0;
        $R = 12;
        if (!$isFull) {
            if ($value == 0) {
                $L = $R = 6;
            }
            else if ($value > 0) {
                $L = 6 - $value;
                $R = 5;
            }
            else {
                $L = 7;
                $R = 6 - $value;
            }
        }

        if ($char == 'F') {
            $this->pdf->setCellPaddings(0, 2, 0, 0);
            $this->pdf->setFont('custom1', '', 12, '', false);
        }
        else if ($char == 'E') {
            $this->pdf->setCellPaddings(0, 3, 0, 0);
            $this->pdf->setFont('custom1', '', 10, '', false);
        }
        else if ($char == 'G') {
            $this->pdf->setCellPaddings(0, 3, 0, 0);
            $this->pdf->setFont('custom1', '', 10, '', false);
        }

        for ($i = 0; $i < 13; $i++) {
            if ($i == 6) {
                $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar-center.bgcolor']));
                $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.neutral.color']));
            }
            else {
                if ($isFull) {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.food.bgcolor']));
                }
                else {
                    $this->pdf->setFillColorArray($this->builder->toColorArray($this->parameters['body.scalebar.bgcolor']));
                }

                if ($i < 6) {
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.positive.color']));
                }
                else {
                    $this->pdf->setTextColorArray($this->builder->toColorArray($this->parameters['body.scalebar.negative.color']));
                }
            }
            $this->pdf->MultiCell(9, 17, ($L <= $i && $i <= $R) ? $char : '', 0, 'C', true, 0);
            #$this->pdf->setX($this->pdf->getX()-1);
        }
    }

    public function displayFooter() {
        if ($this->isShowFooter) {
            $this->pdf->StartTransform();

            $this->pdf->Rotate(-90, 0, 40);

            $pageNo = $this->pdf->getPageNumGroupAlias();
            $pageTotal = $this->pdf->getPageGroupAlias();
            if (!empty($this->pdf->footerData)) {
                $text = $this->pdf->footerData['textPage'].' '.$pageNo.' '.$this->pdf->footerData['textOf'].' '.$pageTotal;
                $text = '<p style="color: '.$this->pdf->footerData['lineColor'].'">'.$text.'</p>';
            }
            else {
                $text = '<p style="color: #000000">'.$pageNo.' /'.$pageTotal."</p>";
            }
            $this->pdf->setFont('pdfahelvetica', 'B', 10);
            $this->pdf->writeHTMLCell($w=0, $h=0, $x=$this->pdf->footerXPos, $y=0, $text, $border=0, $ln=1, $fill=0);
            $this->pdf->setFont($this->builder->fontMap['regular'], '', 10, '', true);

            $this->pdf->StopTransform();
        }
    }

    ////////////////////////////////////////////////////////
    // x. Load Model
    public $foods = [];
    public $warnings = [];
    public $TOP_MARGIN = 43;
    public $LEFT_MARGIN = 27;
    public $MAX_SLICE_PER_PAGE = 30;
    public $SLICE_HEIGHT = 17;
    public $isShowFooter = false;
    public $foodTableParameter = null;
    public $extraParameter = [];
    public $parameters = [];
    public $ingredients = [];
    public $risks = [];
    public $ingredientDisplay = [];
    public $ingredientDisplayEnabled = false;

    public function loadModel($data) {
        $this->loadParameters($data['parameters']);
        $this->loadExtraParameter();
        $this->loadIngredients();
        $this->warnings = $this->parseWarnings($data['warning']);
        $this->foods = $this->loadFoodList($data['foodDefinition']);

        // Ingredient display
        $ingredientDisplayArr = explode("\n", $data['ingredientDisplay']);
        $count = 0;
        foreach ($ingredientDisplayArr as $value) {
            if ($count++ >= $this->INGREDIENT_DISPLAY_MAX) {
                break;
            }

            $value = trim($value);
            if (!empty($value)) {
                $this->ingredientDisplay[] = [
                    'id' => trim($value),
                    'name' => isset($this->ingredients[trim($value)]) ? $this->ingredients[trim($value)]['name'] : "--",
                    'foods' => []
                ];
            }
        }

        $this->ingredientDisplayEnabled = !(empty($this->ingredientDisplay)) & $this->parameters['ingredients.display.enabled'] == 'YES';
        $this->calculateIngredientDisplay($this->ingredientDisplay);
    }

    public function parseWarnings($warning) {
        $data = [];
        $rows = explode("\n", $warning);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $data[trim($k)] = trim($v);
            }
        }

        $warnings = [];
        foreach ($data as $k => $v) {
            if (strpos($k, '.') !== FALSE) {
                $key = explode('.', $k);

                if (!isset($warnings[$key[0]])) {
                    $warnings[$key[0]] = [];
                }
                $warnings[$key[0]][] = $v;
            }
            else {
                $warnings[$k] = $v;
            }
        }

        return $warnings;
    }

    public function calculateAllergyWarning(&$food, $warnings) {
        // $maxG = $food['maxG'];
        // if ($maxG == 0) {
        //     $v1 = 0;
        // } else {
        //     $v1 = round($food['ingredients'][$this->extraParameter['g']] / $maxG, 1);
        //     if ($v1 >= 10) {
        //         $v1 = round($v1, 0);
        //     }
        // }

        // $food['ingredients']['IgE'] = $v1;

        // Compute allegie
        $food['allergy'] = [
            'IgE' =>            false,
            'IgG' =>            false,
            'intoleranz1' =>    false,
            'intoleranz2'  =>   false,
            'eigene' =>         false
        ];

        foreach ($warnings as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $warningV) {
                    $arr = explode(';', $warningV);
                    $ingredientId = trim($arr[0]);
                    $threshold = floatval($arr[1]);
                    if (isset($food['ingredients'][$ingredientId]) && ($food['ingredients'][$ingredientId] > $threshold)) {
                        $food['allergy'][$k] = true;
                        break;
                    }
                }
            } else {
                $arr = explode(';', $v);
                $ingredientId = trim($arr[0]);
                $threshold = floatval($arr[1]);
                if ($food['ingredients'][$ingredientId] > $threshold) {
                    $food['allergy'][$k] = true;
                }
            }
        }

        // if ($p['intoleranz1'] || $p['intoleranz2']) {
        //     $p['IgG'] = true;
        // } else {
        //     $p['IgG'] = false;
        // }
    }

    public function loadFoodList($foodDefinition) {
        $foodDefinition = explode("\n", $foodDefinition);

        // Parse food definition
        $foods = [];
        $foodIds = [];
        foreach ($foodDefinition as $r) {
            $r = trim($r);
            $a = explode('|', $r);
            if (count($a) == 3) {
                $r = trim($a[0]);
                $foods[] = array(
                    'id'               => $r,
                    'bgcolorBody'      => trim($a[1]),
                    'overshootFactor'  => floatval($a[2])
                );
            }
            else {
                $foods[] = array(
                    'id'               => $r
                );
            }

            $foodIds[] = $r;
        }

        // Load food information
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FoodTableFoodItem', 'p')
            ->andWhere('p.foodId in (:foodList)')
            ->setParameter('foodList', $foodIds);
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $foodId = $r->getFoodId();
            foreach ($foods as $id => $food) {
                if ($food['id'] == $foodId) {
                    $foods[$id]['name'] = $r->getName();
                    $foodIngredients = $this->parseIngredient($r->getIngredientData());

                    $foods[$id]['ingredients'] = $foodIngredients;
                    $maxG = $this->calculateMaxGAllowed($foodIngredients);

                    $foods[$id]['maxG'] = $maxG;

                    $foods[$id]['bodyWeightScale'] = $this->calculateBodyWeightScale($maxG);
                    $foods[$id]['healthyNutritionScale'] = $this->calculateHealthyNutritionScale($foods[$id]);
                    $foods[$id]['athleticPerformanceScale'] = $this->calculateAthleticPerformanceScale($foods[$id]);

                    $foods[$id]['typicalPortion'] = $this->getTypicalPortions($foodIngredients);
                    $this->calculateAllergyWarning($foods[$id], $this->warnings);
                }
            }
        }

        // Clean up
        foreach ($foods as $id => $v) {
            unset($v['id']);

            if (empty($v)) {
                unset($foods[$id]);
            }
        }


        return $foods;
    }

    public $athleticPerformanceScale = [];
    public function calculateAthleticPerformanceScale($food) {
        $kcalAmount = floatval($food['ingredients'][$this->extraParameter['kcal']]);
        if ($kcalAmount == 0) {
            return 0;
        }

        $carbAmount = floatval($food['ingredients'][$this->extraParameter['koh']]);
        $fatAmount = floatval($food['ingredients'][$this->extraParameter['fett']]);
        $protAmount = floatval($food['ingredients'][$this->extraParameter['eiw']]);

        $carbPercentage = ($carbAmount * 4 * 100) / $kcalAmount;
        $protPercentage = ($protAmount * 4 * 100) / $kcalAmount;
        $fatPercentage  = ($fatAmount  * 9 * 100) / $kcalAmount;

        $carbDiff = abs($carbPercentage - floatval($this->parameters['sport-calory-balance.carb']));
        $protDiff = abs($protPercentage - floatval($this->parameters['sport-calory-balance.prot']));
        $fatDiff  = abs($fatPercentage  - floatval($this->parameters['sport-calory-balance.fat']));
        $totalDiff = $protDiff + $carbDiff + $fatDiff;

        if (empty($this->athleticPerformanceScale)) {
            $this->athleticPerformanceScale = $this->loadScales('athletic-performance-scale');
        }
        return $this->convert($this->athleticPerformanceScale, $totalDiff);
    }

    public $healthyNutritionScale = [];
    public function calculateHealthyNutritionScale($food) {
        if (empty($this->risks)) {
            $this->risks = [];
            foreach ($this->parameters as $k => $v) {
                if (strpos($k, 'risks.') === 0) {
                    $tokens = explode('.', $k);
                    $ingredient = trim($tokens[1]);
                    $riskAmount = floatval($v);

                    $this->risks[$ingredient] = $riskAmount;
                }
            }
        }

        // Estimate risks
        $totalRiskAmount = 0;
        foreach ($this->ingredients as $ingredientId => $ingredient) {
            $foodRiskAmountMax = $ingredient['riskAmountMax'];

            $riskAmount = isset($this->risks[$ingredientId]) ? floatval($this->risks[$ingredientId]) : 0;
            $foodAmount = isset($food['ingredients'][$ingredientId]) ? floatval($food['ingredients'][$ingredientId]) : 0;
            $foodRiskAmount = $riskAmount * $foodAmount;
            $foodRiskAmountPercentage = $foodRiskAmount * 100 / $foodRiskAmountMax;
            $totalRiskAmount += $foodRiskAmountPercentage;
        }

        if (empty($this->healthyNutritionScale)) {
            $this->healthyNutritionScale = $this->loadScales('healthy-nutrition-scale');
        }

        return $this->convert($this->healthyNutritionScale, $totalRiskAmount);
    }

    public function loadIngredients() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // Ingredients
        $ingredients = [];
        $results = $em->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
        foreach ($results as $r) {
            $ingredients[trim($r->getVariableName())] = array(
                'name' => $r->getName(),
                'riskAmountMax' => $r->getRiskAmountMax()
            );
        }
        $this->ingredients = $ingredients;
        return $ingredients;
    }

    public function loadExtraParameter() {
        $doctrine = $this->container->get('doctrine');

        // Load food parameter
        $config = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableParameter')->findAll();
        foreach ($config as $c) {
            $this->foodTableParameter = $c;
            break;
        }

        $this->extraParameter = [];
        $arr = array('warnung', 'laktose', 'gluten', 'g', 'kcal', 'eiw', 'koh', 'fett');
        foreach ($arr as $key) {
            $getMethod = 'get'.ucfirst($key).'Id';

            $record = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findOneById($this->foodTableParameter->$getMethod());
            if ($record) {
                $this->extraParameter[$key] = $record->getVariableName();
            }
        }
    }

    public function trimProductName($productName, $avaiableWidth) {
        $maxLength = round($avaiableWidth / 4.40);
        if (mb_strlen($productName) > $maxLength) {
            return mb_substr($productName, 0, $maxLength-2).'..';
        }
        return $productName;
    }

    public function loadScales($code) {
        $scales = [];
        foreach ($this->parameters as $k => $v) {
            if (strpos($k, $code.'.') === 0) {
                $tokens = explode('.', $k);
                $scales[intval($tokens[1])] = intval($v);
            }
        }
        ksort($scales);
        return $scales;
    }

    public function convert($scales, $value) {
        $s = [];
        foreach ($scales as $k => $v) {
            $s[] = array('milestone' => $k, 'value' => $v);
        }

        for ($i = 0; $i < count($s); $i++) {
            if (($i == 0 || $s[$i-1]['milestone'] <= $value) && (($i == count($s) - 1) || $value <= $s[$i]['milestone'])) {
                return $s[$i]['value'];
            }
        }
        return 0;
    }

    public $bodyWeightScale = null;
    public function calculateBodyWeightScale($maxG) {
        if (empty($this->bodyWeightScale)) {
            $this->bodyWeightScale = $this->loadScales('body-weight-scale');
        }
        return $this->convert($this->bodyWeightScale, $this->roundTo5($maxG));
    }

    public function getTypicalPortions($ingredients) {
        $d = [];
        $d['g']    = round($ingredients[$this->extraParameter['g']]);
        $d['kcal'] = round($ingredients[$this->extraParameter['kcal']]);
        $d['eiw']  = round($ingredients[$this->extraParameter['eiw']]);
        $d['koh']  = round($ingredients[$this->extraParameter['koh']]);
        $d['fett'] = round($ingredients[$this->extraParameter['fett']]);
        return $d;
    }

    public function calculateMaxGAllowed($ingredients) {
        $smallValue = 0.00001;
        if ($ingredients[$this->extraParameter['kcal']] == 0) {
            $maxKcal = $ingredients[$this->extraParameter['g']] * $this->parameters['kcal'] / $smallValue;
        }
        else {
            $maxKcal = $ingredients[$this->extraParameter['g']] * $this->parameters['kcal'] / $ingredients[$this->extraParameter['kcal']];
        }
        // Eiw
        if ($ingredients[$this->extraParameter['eiw']] == 0) {
            $maxEiw = $ingredients[$this->extraParameter['g']] * $this->parameters['eiw'] / $smallValue;
        }
        else {
            $maxEiw = $ingredients[$this->extraParameter['g']] * $this->parameters['eiw'] / $ingredients[$this->extraParameter['eiw']];
        }
        // Koh
        if ($ingredients[$this->extraParameter['koh']] == 0) {
            $maxKoh = $ingredients[$this->extraParameter['g']] * $this->parameters['koh'] / $smallValue;
        }
        else {
            $maxKoh = $ingredients[$this->extraParameter['g']] * $this->parameters['koh'] / $ingredients[$this->extraParameter['koh']];
        }
        // Fett
        if ($ingredients[$this->extraParameter['fett']] == 0) {
            $maxFett = $ingredients[$this->extraParameter['g']] * $this->parameters['fett'] / $smallValue;
        }
        else {
            $maxFett  = $ingredients[$this->extraParameter['g']] * $this->parameters['fett'] / $ingredients[$this->extraParameter['fett']];
        }
        $maxGAllow = min($maxKcal, $maxEiw, $maxKoh, $maxFett);
        $maxGAllow = floatval($maxGAllow) * floatval($this->parameters['overshootFactor']);

        return $maxGAllow;
    }

    public function parseIngredient($ingredientData) {
        $rows = explode("\n", $ingredientData);
        $ingredients = [];
        foreach ($rows as $r) {
            $arr = explode('=', $r);
            if (count($arr) == 2) {
                $ingredients[trim($arr[0])] = floatval($arr[1]);
            }
        }
        return $ingredients;
    }

    public function loadParameters($parameters) {
        $p = [];
        $rows = explode("\n", $parameters);
        foreach ($rows as $row) {
            $tokens = explode('=', $row);
            if (count($tokens) == 2) {
                $p[trim($tokens[0])] = trim($tokens[1]);
            }
        }
        $this->parameters = $p;
        return $p;
    }

    public function calculateIngredientDisplay($ingredientDisplay) {
        foreach ($this->foods as $food) {
            $foodKey = $food['id'];
            foreach ($ingredientDisplay as $index => $ingredient) {
                $ingredientId = $ingredient['id'];
                $this->ingredientDisplay[$index]['foods'][$foodKey] = [
                    'value' => '--',
                    'cValue' => 0,
                    'riskPercentage' => 0
                ];

                if (isset($this->ingredients[$ingredientId]['riskAmountMax'])) {
                    $riskAmountMax = $this->ingredients[$ingredientId]['riskAmountMax'];
                    $value = isset($food['ingredients'][$ingredientId]) ? $food['ingredients'][$ingredientId] : '--';
                    $cValue = floatval($value);

                    if ($value != '--') {
                        $risk = isset($this->risks[$ingredientId]) ? $this->risks[$ingredientId] : 0;
                        $riskAmount = $risk * $value;
                        $riskPercentage = $riskAmount * 100 / $riskAmountMax;
                        if ($value < 1000) {
                            $value = round($value, 1);
                        } else {
                            $value = round($value, 0);
                        }

                        $this->ingredientDisplay[$index]['foods'][$foodKey]['riskPercentage'] = $riskPercentage;
                    }

                    $this->ingredientDisplay[$index]['foods'][$foodKey]['value'] = $value;
                    $this->ingredientDisplay[$index]['foods'][$foodKey]['cValue'] = $cValue;
                }
            }
        }
    }
}
