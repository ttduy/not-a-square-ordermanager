<?php
namespace Leep\AdminBundle\Business\Report\Util;

class SmileyCalculator {
    //////////////
    // VARIABLE
    private $smileyConversion;
    private $ingredients;


    ////////////
    // METHOD
    public function __construct($container) {
        $this->smileyConversion = [];
        $this->ingredients = [];

        // Get all food ingredients from database
        $doctrine = $container->get('doctrine');
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findAll();
        foreach ($results as $r) {
            $this->ingredients[trim($r->getVariableName())] = array(
                'name' => $r->getName(),
                'riskAmountMax' => $r->getRiskAmountMax()
            );
        }

        // Smiley conversion
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FoodTableSmileyConversion')->findAll();
        foreach ($results as $r) {
            $this->smileyConversion[] = [
                'from' =>   $r->getFromValue(),
                'to'   =>   $r->getToValue(),
                'value' =>  $r->getNormalizedValue(),
                'text' =>   $r->getText()
            ];
        }
    }

    public function translateText($builder) {
        foreach ($this->smileyConversion as $key => $conversion) {
            $this->smileyConversion[$key]['text'] = $builder->translateBlock($conversion['text']);
        }
    }

    public function compute($foodIngredients, $risks) {
        $risks = $this->parseRisks($risks);
        $foodIngredients = $this->parseFoodIngredients($foodIngredients);
        $riskAmountSum = $this->computeRiskAmountSum($foodIngredients, $risks);

        return $this->computeSmiley($riskAmountSum);
    }

    public function computeToHtml($foodIngredients, $risks, $style = []) {
        $value = $this->compute($foodIngredients, $risks);
        $text = $value['text'];
        $value = $value['value'];

        $smiley = '';
        if ($value > 0) {
            for ($i = 0; $i < $value; $i++) $smiley .= '<font face="wingdings" style="font-size: 10; color: green">J</font><b style="color: white; font-size: 7">_</b>';
        }
        else if ($value < 0) {
            $value = -$value;
            for ($i = 0; $i < $value; $i++) $smiley .= '<font face="wingdings" style="font-size: 10; color: red">L</font><b style="color: white; font-size: 7">_</b>';
        }
        else {
            $smiley .= '<font face="wingdings" style="font-size: 10; color: black">K</font><b style="color: white; font-size: 7">_</b>';
        }

        $textStyle = '';
        $textWidth = 150;
        if (isset($style['textWidth']) && $style['textWidth'] != '') {
            $textWidth = $style['textWidth'];
        }

        if (isset($style['fontType'])) {
            $textStyle .= ' font-weight: '.$style['fontType'].';';
        }

        if (isset($style['fontSize'])) {
            $textStyle .= ' font-size: '.$style['fontSize'].'px;';
        }

        if (isset($style['fontColor'])) {
            $textStyle .= ' color: '.$style['fontColor'].';';
        }

        $html = '<table style="margin: 0px; padding: 0px;"><tbody><tr>';
        $html .= '<td width="60" style="text-align: center; '.$textStyle.'"><font face="sanukotb_0">'.$smiley.'</font></td>';
        $html .= '<td width="'.$textWidth.'px" style="text-align: left;'.$textStyle.'">'.$text.'</td>';
        $html .= '</tr></tbody></table>';
        return $html;
    }

    private function computeRiskAmountSum($foodIngredients, $risks) {
        $totalRiskAmount = 0;
        foreach ($this->ingredients as $ingredientId => $ingredient) {
            $riskAmountMax = $ingredient['riskAmountMax'];

            $risk = isset($risks[$ingredientId]) ? floatval($risks[$ingredientId]) : 0;
            $foodRiskAmount = isset($foodIngredients[$ingredientId]) ? $foodIngredients[$ingredientId] : 0;

            $riskAmount = $risk * $foodRiskAmount;
            $riskAmountPercentage = $riskAmount * 100 / $riskAmountMax;
            $totalRiskAmount += $riskAmountPercentage;
        }

        return $totalRiskAmount;
    }

    private function computeSmiley($riskAmountSum) {
        foreach ($this->smileyConversion as $smileyConversion) {
            if ($smileyConversion['from'] <= $riskAmountSum && $riskAmountSum <= $smileyConversion['to']) {
                return ['value' => $smileyConversion['value'], 'text' => $smileyConversion['text']];
            }
        }

        return ['value' => 0, 'text' => ''];
    }

    private function parseFoodIngredients($input) {
        $rows = explode("\n", $input);
        $ingredients = [];
        foreach ($rows as $r) {
            $arr = explode('=', $r);
            if (count($arr) == 2) {
                $ingredients[trim($arr[0])] = floatval($arr[1]);
            }
        }

        return $ingredients;
    }

    private function parseRisks($input) {
        $rows = explode("\n", $input);
        $risks = [];
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $risks[trim($k)] = trim($v);
            }
        }

        return $risks;
    }
}