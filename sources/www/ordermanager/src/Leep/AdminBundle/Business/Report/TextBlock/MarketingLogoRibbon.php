<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class MarketingLogoRibbon extends BaseTextBlock {
    public function build($data) { 
        $toolStyle = array(
            'BoxColor' => '#173764',
            'BoxHeight' => '10',
            'TextColor' => '#FFFFFF',
            'TextSize' => '15',
            'TextMarginLeft' => '15',
            'LogoWidth' => '40',
            'LogoMarginLeft' => '5'
        ); 
        $toolStyle = $this->builder->parseInput($toolStyle, $data['style']);

        // Draw background box
        $curX = $this->pdf->getX();
        $curY = $this->pdf->getY();

        $margins = $this->pdf->getMargins();
        $pageWidth = $this->pdf->getPageWidth();
        $totalWidth = $pageWidth - $margins['left'] - $margins['right'];

        $fillColor = $this->builder->toColorArray($toolStyle['BoxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $curX,
            $y = $curY,
            $w = $totalWidth,
            $h = $toolStyle['BoxHeight'],
            'F',
            $style,
            $fillColor
        );

        // Draw logo
        $logoWidth = $toolStyle['LogoWidth'];
        $logoMarginLeft = $toolStyle['LogoMarginLeft'];
        $logoMarginTop = -3;
        $logoFile = $this->builder->resourceDir.$data['logo'];

        $this->pdf->StartTransform();
        $this->pdf->Rotate(4, 
            $curX + $logoMarginLeft, 
            $curY + $logoMarginTop
        );
        $this->pdf->myDrawImage(
            $logoFile, 
            $x = $curX + $logoMarginLeft, 
            $y=$curY + $logoMarginTop, 
            $w=$logoWidth, 
            $h=0, 
            $type='', 
            $link='', 
            $align='T', 
            $resized=true, 
            $dpi=800, 
            $palign='', 
            $ismask=false, 
            $imgmask=false, 
            $border=0, 
            $fitbox=false, 
            $hidden=false, 
            $fitonpage=false
        );        
        $this->pdf->StopTransform();

        // Write text
        $text = $data['text'];
        $text = $this->builder->translateCustomTag($text);
        $text = '<p style="color: #FFFFFF; font-size: '.$toolStyle['TextSize'].'"><font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</font></p>';

        $this->pdf->writeHTMLCell(
            $w=0, 
            $h=0, 
            $x=$curX + $logoWidth + $toolStyle['TextMarginLeft'], 
            $y=$curY + 2,
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='L', 
            $autopadding=true
        );

        $this->pdf->setX($curX);
        $this->pdf->setY($curY + 20);
    }
}
