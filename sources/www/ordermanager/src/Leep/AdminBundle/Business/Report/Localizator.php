<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business;

class Localizator {
    const MSG_ERROR = '!!ERROR!!';
    const MSG_UNSUPPORTED = '!!UNSUPPORTED!!';

    public $container;
    public $config = array();
    public function __constructor($container) {
        $this->container = $container;
        $this->init();
    }

    public function init() {
        $this->config = array(
            'date.format' => '$d$/$m$/$y$',
            'weight' => 'kilogram',
            'weight.format' => '$1$kg',
            'weight.format.raw' => '$1$',
            'weight2' => 'gram',
            'weight2.format' => '$1$g',
            'weight2.format.raw' => '$1$',
            'weight3' => 'milliliter',
            'weight3.format' => '$1$ml',
            'weight3.format.raw' => '$1$',
            'height' => 'centimeter',
            'height.format' => '$1$cm',
            'height.format.raw' => '$1$',
            'length' => 'kilometer',
            'length.format' => '$1$km',
            'length.format.raw' => '$1$',
            'food_table_amount' => 'gram',
            'food_table_amount.format.raw' => '$1$',
            'food_table_amount.label' => 'g',
            'food_table_amount.amount_rounder' => 5,
            'food_table_amount.decimal_col1' => 0,
            'food_table_amount.decimal_col3' => 0,
            'food_table_amount.decimal_col4' => 0,
            'food_table_amount.decimal_col5' => 0
        );
    }

    public function setLocalization($localization) {
        $this->init();
        $rows = explode("\n", $localization);
        foreach ($rows as $r) {
            $arr = explode('=', $r);
            if (count($arr) == 2) {
                $key = trim($arr[0]);
                $value = trim($arr[1]);
                $this->config[$key] = $value;
            }
        }
    }

    public function translate($block) {
        $arr = explode('|', $block);
        if (count($arr) < 2) {
            return self::MSG_ERROR;
        }

        $data = trim($arr[0]);
        $task = trim($arr[1]);
        $format = isset($arr[2]) ? trim($arr[2]) : '';

        switch ($task) {
            case 'date':
                return $this->translateDate($data, $format);
            case 'height':
                return $this->translateHeight($data, $format);
            case 'weight':
                return $this->translateWeight($data, $format);
            case 'weight2':
                return $this->translateWeight2($data, $format);
            case 'weight3':
                return $this->translateWeight3($data, $format);
            case 'length':
                return $this->translateLength($data, $format);

            default:
                return self::MSG_ERROR;
        }
        return self::MSG_ERROR;
    }

    protected function getDecimalPlaces($type, $format) {
        if (empty($format)) {
            $name = $type.'.format.decimal';
        }
        else {
            $name = $type.'.format.'.$format.'.decimal';
        }

        return isset($this->config[$name]) ? intval($this->config[$name]) : 2;
    }

    protected function replaceTokens($tokens, $type, $format) {
        $text = '';
        if (empty($format)) {
            $text = $this->config[$type.'.format'];
        }
        else {
            $text = $this->config[$type.'.format.'.$format];
        }

        foreach ($tokens as $index => $v) {
            $text = str_replace('$'.$index.'$', $v, $text);
        }
        return $text;
    }

    public function translateDate($data, $format) {
        $arr = explode('/', $data);
        if (count($arr) != 3) {
            return self::MSG_ERROR;
        }

        $tokens = array('d' => $arr[0], 'm' => $arr[1], 'y' => $arr[2]);
        return $this->replaceTokens($tokens, 'date', $format);
    }
    
    public function translateHeight($data, $format) {
        $decimalPlaces = $this->getDecimalPlaces('height', $format);

        if ($this->config['height'] == 'centimeter') {
            $tokens = array(1 => $data);
        }
        else if ($this->config['height'] == 'feet') {
            $feet = floatval($data) * 0.032808;

            $f = floor($feet);
            $i = round(($feet - $f)*12);    

            $tokens = array(1 => number_format($feet, $decimalPlaces), 'f' => $f, 'i' => $i);
        }
        else {
            return self::MSG_UNSUPPORTED;
        }

        return $this->replaceTokens($tokens, 'height', $format); 
    }
    
    public function translateWeight($data, $format) {
        $decimalPlaces = $this->getDecimalPlaces('weight', $format);

        if ($this->config['weight'] == 'kilogram') {
            $tokens = array(1 => $data);
        }
        else if ($this->config['weight'] == 'pound') {
            $oz = floatval($data) * 2.20462;
            $tokens = array(1 => number_format($oz, $decimalPlaces));
        }
        else if ($this->config['weight'] == 'ounce') {
            $oz = floatval($data) * 35.274;
            $tokens = array(1 => number_format($oz, $decimalPlaces));
        }
        else {
            return self::MSG_UNSUPPORTED;
        }

        return $this->replaceTokens($tokens, 'weight', $format); 
    }


    public function translateWeight2($data, $format) {
        $decimalPlaces = $this->getDecimalPlaces('weight2', $format);

        if ($this->config['weight2'] == 'gram') {
            $tokens = array(1 => $data);
        }
        else if ($this->config['weight2'] == 'pound') {
            $oz = floatval($data) * 2.20462 / 1000;
            $tokens = array(1 => number_format($oz, $decimalPlaces));
        }
        else if ($this->config['weight2'] == 'ounce') {
            $oz = floatval($data) * 35.274  / 1000;
            $tokens = array(1 => number_format($oz, $decimalPlaces));
        }
        else {
            return self::MSG_UNSUPPORTED;
        }

        return $this->replaceTokens($tokens, 'weight2', $format); 
    }


    public function translateWeight3($data, $format) {
        $decimalPlaces = $this->getDecimalPlaces('weight3', $format);

        if ($this->config['weight3'] == 'milliliter') {
            $tokens = array(1 => $data);
        }
        else if ($this->config['weight3'] == 'ounzes') {
            $oz = floatval($data) * 0.0338140225589;
            $tokens = array(1 => number_format($oz, $decimalPlaces));
        }
        else {
            return self::MSG_UNSUPPORTED;
        }

        return $this->replaceTokens($tokens, 'weight3', $format); 
    }

    public function translateFoodTableAmount($amount) {
        if ($this->config['food_table_amount'] == 'gram') {
            return $amount;
        }
        else if ($this->config['food_table_amount'] == 'pound') {
            return floatval($amount) * 2.20462 / 1000;
        }
        else if ($this->config['food_table_amount'] == 'ounce') {
            return floatval($amount) * 35.274 / 1000;
        }
        return self::MSG_UNSUPPORTED;        
    }

    public function translateFoodTableText($data) {
        $rounder = $this->config['food_table_amount.amount_rounder'];
        $arr = explode('.', $rounder);
        $decimalPlaces = (count($arr) == 2) ? strlen($arr[1]) : 0;

        $amount = $this->translateFoodTableAmount($data);
        if (is_numeric($amount)) {
            $amount = ceil($amount / $rounder) * $rounder;
            $tokens = array(1 => number_format($amount, $decimalPlaces));
        }
        else {
            return self::MSG_UNSUPPORTED;            
        }

        return $this->replaceTokens($tokens, 'food_table_amount', 'raw'); 
    }

    public function translateFoodTableAmountText($data, $decimalFormat) {
        $amount = $this->translateFoodTableAmount($data);
        $decimalPlace = isset($this->config['food_table_amount.'.$decimalFormat]) ? $this->config['food_table_amount.'.$decimalFormat] : 0;
        
        return number_format($amount, $decimalPlace);
    }

    protected function translateLength($data, $format) {
        $decimalPlaces = $this->getDecimalPlaces('length', $format);

        if ($this->config['length'] == 'kilometer') {
            $tokens = array(1 => $data);
        }
        else if ($this->config['length'] == 'mile') {
            $mile = floatval($data) * 0.621371;
            $tokens = array(1 => number_format($mile, $decimalPlaces));
        }
        else {
            return self::MSG_UNSUPPORTED;
        }

        return $this->replaceTokens($tokens, 'length', $format); 
    }
}
