<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class StructureWebCover extends BaseTextBlock {
    public function build($data) {       
        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';

        $imageFile = $imageFilePath.$data['coverImage'];
        // get the current page break margin
        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();

        $bMargin = $this->pdf->getBreakMargin();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->myDrawImage($imageFile, 0, 0, $pageWidth, $pageHeight, '', '', '', true, 300, '');
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }
}
