<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class MarketingHeadline extends BaseTextBlock {
    public function build($data) { 
        $toolStyle = array(
            'BoxColor' => '#173764',
            'BoxWidth' => '100',
            'BoxHeight' => '10',
            'TextColor' => '#FFFFFF',
            'TextSize' => '15'
        );
        $toolStyle = $this->builder->parseInput($toolStyle, $data['style']);

        // Draw background box
        $curX = $this->pdf->getX();
        $curY = $this->pdf->getY();

        $margins = $this->pdf->getMargins();

        $fillColor = $this->builder->toColorArray($toolStyle['BoxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $curX,
            $y = $curY,
            $w = $toolStyle['BoxWidth'],
            $h = $toolStyle['BoxHeight'],
            'F',
            $style,
            $fillColor
        );

        // Write text
        $text = $data['title'];
        $text = $this->builder->translateCustomTag($text);
        $text = '<p style="color: #FFFFFF; font-size: '.$toolStyle['TextSize'].'"><font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</font></p>';

        $this->pdf->writeHTMLCell(
            $w=0, 
            $h=0, 
            $x=$curX + 5, 
            $y=$curY + 2,
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='L', 
            $autopadding=true
        );

        $this->pdf->setX($curX);
        $this->pdf->setY($curY + 15);
    }
}
