<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetFoodTable extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_food_table';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('foodDefinition', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('ingredientDisplay', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('risks', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('otherVariables', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('label', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('style', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('warning', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
        $builder->add('overshootFactor', 'text', array(
            'required' => false
        ));
        $builder->add('isDebug', 'choice', array(
            'required' => false,
            'choices'  => array(
                1 => 'Turn debug on'
            )
        ));
        $builder->add('idBarCalculation', 'choice', array(
            'required' => false,
            'choices'  => array(
                1 => 'Calory balance'
            )
        ));
        $builder->add('scales', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
    }
}
