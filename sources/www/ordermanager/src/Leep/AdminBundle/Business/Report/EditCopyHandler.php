<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditCopyHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Report', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditCopyModel(); 
        $model->name = $entity->getName();       

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');   
        $utils = $this->container->get('leep_admin.report.business.util');
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));

        $builder->add('sections', 'choice', array(
            'label'    => 'Section to copy',
            'choices'  => $utils->getReportSections(),
            'required' => true,
            'multiple' => 'multiple',
            'attr'     => array(
                'style' => 'min-width: 800px; max-width: 800px; min-height: 400px'
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $mapping = $this->container->get('easy_mapping');   

        $this->entity->setName($model->name);
        $em->persist($this->entity);
        $em->flush();

        $query = $em->createQueryBuilder();
        $query->select('MAX(p.sortOrder) as curSortOrder')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $this->entity->getId());

        $sortOrder = $query->getQuery()->getSingleScalarResult();
        $sortOrder++;

        foreach ($model->sections as $idSection) {
            $section = $doctrine->getRepository('AppDatabaseMainBundle:ReportSection')->findOneById($idSection);

            $reportName = $mapping->getMappingTitle('LeepAdmin_Report_List', $section->getIdReport());

            $newSection = new Entity\ReportSection();
            $newSection->setIdReport($this->entity->getId());
            $newSection->setName($reportName.' - '.$section->getName());
            $newSection->setAlias($section->getAlias());
            $newSection->setSortOrder($sortOrder);
            $em->persist($newSection);
            $em->flush();
            $sortOrder++;

            $reportItems = $doctrine->getRepository('AppDatabaseMainBundle:ReportItem')->findByIdReportSection($section->getId());
            foreach ($reportItems as $reportItem) {
                $newItem = new Entity\ReportItem();
                $newItem->setIdReport($this->entity->getId());
                $newItem->setIdReportSection($newSection->getId());
                $newItem->setIdTextToolType($reportItem->getIdTextToolType());
                $newItem->setData($reportItem->getData());
                $newItem->setSortOrder($reportItem->getSortOrder());
                $em->persist($newItem);
            }
            $em->flush();
        }
        $em->flush();

        parent::onSuccess();
    }
}