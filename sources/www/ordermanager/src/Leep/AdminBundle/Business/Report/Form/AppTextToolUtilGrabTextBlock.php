<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolUtilGrabTextBlock extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_util_grab_text_block';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('textBlocks', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '10', 'cols' => 80
            )
        ));
        /*$builder->add('formulaTemplates', 'choice', array(
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_FormulaTemplate_List'),            
            'attr' => array(
                'style' => 'min-width: 200px; min-height: 150px'
            )
        ));*/
    }
}
