<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class WidgetTableNew extends BaseTextBlock {
    public function translateCustomTag($text) {
        $text = str_replace('##BR##', "\n", $text);
        return $text;
    }

    private function estimateRowHeight($row, $width) {
        $height = 0;
        $sumWidth = 0;

        end($row['cells']);
        $lastElementKey = key($row['cells']);
        foreach ($row['cells'] as $k => $cell) {
            if ($k == $lastElementKey) {
                $cellWidth = $width - $sumWidth;
            }
            else {
                $cellWidth = floatval(trim($cell['width'], '%')) * $width / 100;
            }
            $this->pdf->startTransaction();
            $curY = $this->pdf->getY();
            $this->pdf->setFont($cell['fontFace'], '', $cell['fontSize']);
            $this->pdf->multiCell($cellWidth, $height, $cell['text'], 0, $cell['alignment'], false, 0, $valign='M');
            $this->pdf->ln();
            $estimateHeight = $this->pdf->getY() - $curY;
            $this->pdf->rollbackTransaction(true);
            if ($estimateHeight > $height) {
                $height = $estimateHeight;
            }
            $sumWidth += $cellWidth + 0.5;
        }
        return $height;
    }

    private function drawRow($row, $width, $height = 0) {
        $sumWidth = 0;
        end($row['cells']);
        $lastElementKey = key($row['cells']);
        $curX = $this->pdf->getX();
        foreach ($row['cells'] as $k => $cell) {
            // Background
            if ($cell['bgcolor'] === '') {
                $isFill = false;
            }
            else {
                $bgcolor = $this->builder->toColorArray($cell['bgcolor']);
                $this->pdf->setFillColor($bgcolor[0], $bgcolor[1], $bgcolor[2]);
                $isFill = true;
            }

            // Font
            $color = $this->builder->toColorArray($cell['fontColor']);
            $this->pdf->setTextColor($color[0], $color[1], $color[2]);
            $this->pdf->setFont($cell['fontFace'], '', $cell['fontSize']);

            // Draw
            if ($k == $lastElementKey) {
                $cellWidth = $width - $sumWidth;
            }
            else {
                $cellWidth = floatval(floatval(trim($cell['width'], '%')) * $width / 100);
            }

            if ($cell['isWrapText']) {
                $x = $this->pdf->getX();
                $this->pdf->cell($cellWidth, $height, '', 0, 0, '', $isFill);
                $this->pdf->setX($x);
                $this->pdf->multiCell($cellWidth, $height, $cell['text'], 0, $cell['alignment'], $isFill, 0, $valign='M');
            }
            else {
                $this->pdf->cell($cellWidth, $height, $cell['text'], 0, 0, $cell['alignment'], $isFill, $valign='M');
            }

            $this->pdf->cell(0.5, $height, '', 0, 0, $cell['alignment'], false);
            $sumWidth += $cellWidth + 0.5;
        }
        $this->pdf->ln();
    }

    public function build($data) {
        $content = $data['content'];
        $table = $this->parseTableModel($content);
        $width = $this->builder->estimateCurrentWidth();

        $prevPaddings = $this->pdf->getCellPaddings();
        $this->pdf->setCellPaddings(1, 0.3, 1, 0.3);
        $curX = $this->pdf->getX();
        foreach ($table['rows'] as $row) {
            if ($row['height'] === 'auto') {
                $height = $this->estimateRowHeight($row, $width);
            }
            else {
                $height = floatval($row['height']);
            }
            $this->drawRow($row, $width, $height);
            $this->pdf->setY($this->pdf->getY() + $row['space']);
        }
        $this->pdf->setTextColor(0, 0, 0);
        $this->pdf->setCellPaddings($prevPaddings['L'], $prevPaddings['T'], $prevPaddings['R'], $prevPaddings['B']);
    }

    private function parseTableModel($content) {
        $table = array('rows' => array());
        $fontMap = $this->builder->getFontMap();

        $rows = explode("\n", $content);
        foreach ($rows as $r) {
            $r = trim($r);
            if (!empty($r)) {
                $rowData = array(
                    'height'  => 'auto',
                    'space'   => 0.3,
                    'cells'   => array()
                );

                $cells = explode('|', $r);
                foreach ($cells as $c) {
                    $tokens      = explode(';', $c);
                    $text        = trim($tokens[0]);
                    $bgcolor     = isset($tokens[1]) ? trim($tokens[1]) : '';
                    $width       = isset($tokens[2]) ? trim($tokens[2]) : '30%';
                    $alignment   = isset($tokens[3]) ? trim($tokens[3]) : 'left';
                    $fontSize    = isset($tokens[4]) ? trim($tokens[4]) : 11;
                    $fontColor   = isset($tokens[5]) ? trim($tokens[5]) : '#000000';
                    $fontFace    = isset($tokens[6]) ? trim($tokens[6]) : 'regular';
                    $fontFace    = isset($fontMap[$fontFace]) ? $fontMap[$fontFace] : $fontMap['regular'];
                    $rowSpace    = isset($tokens[7]) ? floatval($tokens[7]) : '';
                    $rowHeight   = isset($tokens[8]) ? trim($tokens[8]) : '';

                    if ($text == '#check#') {
                        $fontFace = 'customawsome';
                        $text = 'A';
                    } else if ($text == '#remove#') {
                        $fontFace = 'customawsome';
                        $text = 'B';
                    } else if ($text == '#warning#') {
                        $fontFace = 'customawsome';
                        $text = 'C';
                    }

                    // Alignment
                    switch ($alignment) {
                        case 'left': $alignment = 'L'; break;
                        case 'right': $alignment = 'R'; break;
                        case 'center': $alignment = 'C'; break;
                        default:
                            $alignment = 'L';
                    }

                    // Check break column
                    $isBreakColumn = false;
                    if (strpos($text, '@@@') === 0) {
                        $text = substr($text, 3);
                        $isBreakColumn = true;
                    }

                    $isWrapText = false;
                    if (strpos($text, '!!!') === 0) {
                        $text = substr($text, 3);
                        $isWrapText = true;
                    }

                    $text = $this->translateCustomTag($text);

                    // Add to row
                    if (!empty($rowHeight)) {
                        $rowData['height']   = $rowHeight;
                    }
                    if (!empty($rowSpace)) {
                        $rowData['space']    = $rowSpace;
                    }
                    $rowData['cells'][]  = array(
                        'text'              => $text,
                        'bgcolor'           => $bgcolor,
                        'width'             => $width,
                        'alignment'         => $alignment,
                        'fontSize'          => $fontSize,
                        'fontColor'         => $fontColor,
                        'fontFace'          => $fontFace,
                        'isBreakColumn'     => $isBreakColumn,
                        'isWrapText'        => $isWrapText
                    );
                }

                if ($rowData['height'] !== 'auto') {
                    foreach ($rowData['cells'] as $id => $cell) {
                        $rowData['cells'][$id]['isBreakColumn'] = true;
                    }
                }

                $table['rows'][] = $rowData;
            }
        }

        return $table;
    }
}
