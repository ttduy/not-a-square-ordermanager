<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridSearchSectionReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('nameReport', 'nameSection');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',               'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Name Section',       'width' => '50%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.name as nameSection', 'p.id as id', 'rp.id as idReport', 'rp.name as nameReport')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->innerJoin('AppDatabaseMainBundle:Report', 'rp', 'WITH', 'p.idReport = rp.id');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        $queryBuilder->andWhere('p.idType is NULL');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.name', 'ASC');
    }

    public function buildCellNameReport($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '%s %s';

        $builder->addNewTabButton('View report', $mgr->getUrl('leep_admin', 'report', 'edit_section', 'edit', array('id' => $row['idReport'])), 'button-login-cd');
        $buttonHtml = $builder->getHtml();

        return sprintf($html, $row['nameReport'], "<span>$buttonHtml</span>");
    }

    public function buildCellNameSection($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '%s %s';

        $builder->addNewTabButton('View report section', $mgr->getUrl('leep_admin', 'report', 'edit', 'edit', array('id' => $row['id'])), 'button-login-cd');
        $buttonHtml = $builder->getHtml();

        return sprintf($html, $row['nameSection'], "<span>$buttonHtml</span>");
    }
}
