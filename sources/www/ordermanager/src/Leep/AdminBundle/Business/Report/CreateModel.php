<?php
namespace Leep\AdminBundle\Business\Report;

class CreateModel {
    public $name;
    public $shortName;
    public $reportCode;
    public $sortOrder;
    public $idReport;
    public $idOption;
    public $isVisibleByDefault;
    public $isReleased;
}