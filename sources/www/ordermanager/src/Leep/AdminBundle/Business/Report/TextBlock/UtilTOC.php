<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class UtilTOC extends BaseTextBlock {
    public function appendFillerToString($title) {
        $params = \TCPDF_STATIC::serializeTCPDFtagParameters(array($title));
        return '<tcpdf method="appendFillerToString" params="'.$params.'"/>';
    }

    public function build($data) { 
        /*$margins = $this->pdf->getMargins();
        $tableWidth = round($this->pdf->getPageWidth() - $margins['left'] - $margins['right']);    
        $titleWidth = $tableWidth - 10; 

        $toolStyle = array(
            'font'       => 'regular',
            'size'       => '12',
            'color'      => '#FFFFFF',
            'bgcolor'    => '#EEEEEE'
        );
        $toolStyle = $this->builder->parseInput($toolStyle, $data['style']);


        $fontFace = $this->builder->getFontFace($toolStyle['font']);
        $fontSize = intval($toolStyle['size']);
        $fontColor = $toolStyle['color'];
        $fontBgColor = $toolStyle['bgcolor'];

        $tocTemplates = array();
        $tocTemplates[0] = '<table border="0" cellpadding="2" cellspacing="0" style="background-color:'.$fontBgColor.'">
                <tr>
                    <td width="'.$titleWidth.'mm">
                        <span style="font-family: '.$fontFace.'; font-size:'.$fontSize.';color:'.$fontColor.';">#TOC_DESCRIPTION#</span>
                    </td>
                    <td width="10mm" align="right">
                        <span style="font-family: '.$fontFace.'; font-size:'.$fontSize.';color:'.$fontColor.';">#TOC_PAGE_NUMBER#</span>
                    </td>
                </tr>
            </table>'.$this->builder->workaroundMarginTop('', 1);

        if ($this->builder->mode == 'preview') {
            $this->pdf->Bookmark(
                $title = "Example entry 1", 
                $level = 0,
                $y = 0,
                $page = '1'
                );
            $this->pdf->Bookmark(
                $title = "Example entry 2", 
                $level = 0,
                $y = 0,
                $page = '2'
                );
        
            $this->pdf->setFont($this->builder->getFontFace('regular'), '', $fontSize, '', true);        
            $this->pdf->addHTMLTOC('', 'INDEX', $tocTemplates, false);
        }
        else {
            $this->pdf->setFont($this->builder->getFontFace('regular'), '', $fontSize, '', true);        
            $this->pdf->addHTMLTOC('', 'INDEX', $tocTemplates, false);
        }*/
    }
}
