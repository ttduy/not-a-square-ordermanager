<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class StructureChangeFormat {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {        
        // Delete current page if blank
        $margins = $this->pdf->getMargins();
        if ($this->pdf->getX() == $margins['left'] && $this->pdf->getY() == $margins['top']) {
            $this->pdf->deletePage($this->pdf->getPage());            
        }

        // Create new page
        if ($data['idFormat'] == 'CUSTOM') {
            $width = isset($data['width']) ? $data['width'] : 0;
            $height = isset($data['height']) ? $data['height'] : 0;
            $this->pdf->addPage($data['idOrientation'], 'RCUSTOM_'.$width.'_'.$height);
        }
        else {
            $this->pdf->addPage($data['idOrientation'], $data['idFormat']);
        }
        $this->pdf->setPageUnit('mm');
        // Reset column
        $width = $this->pdf->getPageWidth() - $margins['left'] - $margins['right'];
        $columns = array(
            array('w' => $width, 's' => 0, 'y' => $margins['top'])
        );
        $this->pdf->setX($margins['left']);
        $this->pdf->resetColumns();
    }
}
