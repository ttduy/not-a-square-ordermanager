<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class ChapterToc extends BaseTextBlock {
    public function build($data) { 
        // Parse chapters
        $this->builder->toc = array();        
        $rows = explode("\n", $data['chapters']);
        foreach ($rows as $r) {
            $tokens = explode("|", $r);
            if (count($tokens) == 4) {
                $id = trim($tokens[0]);
                $name = trim($tokens[1]);
                $subtext = trim($tokens[2]);
                $available = trim(strtoupper($tokens[3])) == 'AVAILABLE' ? true : false;

                $this->builder->toc[$id] = array(
                    'id'           => $id,
                    'name'         => $name,
                    'subtext'      => $subtext,
                    'available'    => $available
                );
            }
        }

        // Parse styles
        $this->builder->tocOptions = array();        
        $this->builder->tocOptions = $this->parseInput($this->builder->tocOptions, $data['options']);

        $this->pdf->toc = $this->builder->toc;
        $this->pdf->tocOptions = $this->builder->tocOptions;
    }
}
