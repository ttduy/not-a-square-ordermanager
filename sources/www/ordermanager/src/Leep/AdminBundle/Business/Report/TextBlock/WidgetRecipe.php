<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business\Report\Constants;

class WidgetRecipe {
    public $container;
    public $pdf;
    public $builder;
    public $dpi = 300;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $smileyCalculator = $this->container->get('leep_admin.report.business.util.smiley_caculator');
        $smileyCalculator->translateText($this->builder);

        //////////////////////////////////////////////
        // x. Override layout
        $bookLayout = $this->builder->recipeBookLayout;
        $bookLayout->overrideLayoutData($data['style']);

        //////////////////////////////////////////////
        // x. List all recipes
        $recipeIdList = explode("\n", $data['recipeId']);

        $recipes = array();
        $recipeNames = array();
        foreach ($recipeIdList as $recipe) {
            $arr = explode("|", $recipe);
            $recipeName = trim($arr[0]);
            $recipeLayout = trim($arr[1]);
            $recipeNames[] = $recipeName;
            $recipes[] = array(
                'name'    => $recipeName,
                'layout'  => $recipeLayout,
            );
        }

        //////////////////////////////////////////////
        // x. Process recipes
        $bMargin = $this->pdf->getBreakMargin();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->setCellPaddings(0, 0, 0, 0);

        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $caloryAmount = $data['caloryAmount'];
        $caloryAmount2 = isset($data['caloryAmount2']) ? $data['caloryAmount2'] : 0;

        $i = 0;
        foreach ($recipes as $recipe) {
            $idLayout = $recipe['layout'];
            $idRecipe = $recipe['name'];

            $bookLayout->setIdLayout($idLayout);
            $this->dpi = intval($bookLayout->get('dpi'));
            if ($this->dpi == 0) {
                $this->dpi = 300;
            }

            $recipe = $em->getRepository('AppDatabaseMainBundle:Recipe')->findOneByRecipeId($idRecipe);

            // Write background
            $this->printBackground($bookLayout);

            // Write image
            $this->printRecipeImage($bookLayout, $recipe->getImageFile());

            // Write recipe header
            $this->printRecipeName($bookLayout, $recipe->getName());

            // Write label 1
            $this->printLabel1($bookLayout, $recipe->getPreparationTime(), $caloryAmount, $caloryAmount2);

            // Write cLabel
            $this->printCLabels($bookLayout);

            // Ingredient label
            $this->printIngredientLabel($bookLayout);

            // Ingredients
            $this->printIngredients($bookLayout, $recipe->getIngredient(), $caloryAmount, $caloryAmount2);

            // Instruction Label
            $this->printInstructionLabel($bookLayout);

            // Smiley
            $this->printSmiley($bookLayout, $smileyCalculator, $recipe->getFoodIngredients(), $data['risks']);

            // Instruction
            $this->printInstructions($bookLayout, $recipe->getInstruction());
            $i++;
            if ($i < count($recipes)) {
                $this->pdf->addPage();
            }
        }

        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }

    public function printSmiley($bookLayout, $smileyCalculator, $foodIngredients, $risks) {
        $isVisible = $bookLayout->get('smiley.visible');
        if ($isVisible != 'YES') {
            return;
        }

        $posX = $bookLayout->get('smiley.posX');
        $posY = $bookLayout->get('smiley.posY');

        $fontColor = $bookLayout->get('smiley.fontColor');
        $fontSize = $bookLayout->get('smiley.fontSize');
        $fontType = $bookLayout->get('smiley.fontType');
        $textWidth = $bookLayout->get('smiley.textWidth');

        $style = [];
        if (!empty($fontColor)) {
            $style['fontColor'] = $fontColor;
        }

        if (!empty($fontSize)) {
            $style['fontSize'] = $fontSize;
        }

        if (!empty($fontType)) {
            $style['fontType'] = $fontType;
        }

        if (!empty($textWidth)) {
            $style['textWidth'] = $textWidth;
        }

        $html = $smileyCalculator->computeToHtml($foodIngredients, $risks, $style);
        $this->pdf->writeHTMLCell($w=0, $h=0, $x=$posX, $y=$posY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
    }

    public function printBackground($bookLayout) {
        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();

        $imageFile = $imageFilePath.$bookLayout->get('background.image');
        $this->pdf->myDrawImage($imageFile, 0, 0, $pageWidth, $pageHeight, '', '', '', true, 300, '');
    }

    public function printRecipeImage($bookLayout, $recipeImage) {
        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';

        $imagePosX = $bookLayout->get('image.posX');
        $imagePosY = $bookLayout->get('image.posY');
        $imageWidth = $bookLayout->get('image.width');
        $imageHeight = $bookLayout->get('image.height');
        $resize = $bookLayout->get('image.resize');

        $this->pdf->StartTransform();
        $this->pdf->RoundedRect($imagePosX, $imagePosY, $imageWidth, $imageHeight, 0, '', 'CNZ');
        if ($resize == 'byHeight') {
            $this->pdf->myDrawImage($imageFilePath.'/Recipes/'.$recipeImage, $imagePosX, $imagePosY, '', $imageHeight, '', '', '', true, $this->dpi, '');
        }
        else {
            $this->pdf->myDrawImage($imageFilePath.'/Recipes/'.$recipeImage, $imagePosX, $imagePosY, $imageWidth, '', '', '', '', true, $this->dpi, '');
        }
        $this->pdf->StopTransform();
    }

    public function printInstructions($bookLayout, $instruction) {
        $instruction = $this->builder->translateBlock($instruction);

        $instructionPosX = $bookLayout->get('instruction.posX');
        $instructionPosY = $bookLayout->get('instruction.posY');
        $instructionWidth = $bookLayout->get('instruction.width');
        $instructionFontType = $bookLayout->get('instruction.fontType');
        $instructionFontSize = $bookLayout->get('instruction.fontSize');
        $instructionFontColor = $bookLayout->get('instruction.fontColor');
        $instructionNumberFontType = $bookLayout->get('instruction.numberFontType');
        $instructionNumberFontSize = $bookLayout->get('instruction.numberFontSize');
        $instructionNumberFontColor = $bookLayout->get('instruction.numberFontColor');

        $color = $this->builder->toColorArray($instructionFontColor);
        $numberColor = $this->builder->toColorArray($instructionNumberFontColor);

        $index = 1;
        $posY = $instructionPosY;
        $instructions = explode("\n", $instruction);
        foreach ($instructions as $instruction) {
            // print index current instruction
            $this->pdf->setTextColor($numberColor[0], $numberColor[1], $numberColor[2]);
            $this->pdf->setFont($this->builder->getFontFace($instructionNumberFontType), '', $instructionNumberFontSize, '', true);
            $this->pdf->multiCell(5, 0, $index.'.', 0, 'R', false, 2, $instructionPosX, $posY);

            // print current instruction content
            $this->pdf->setTextColor($color[0], $color[1], $color[2]);
            $this->pdf->setFont($this->builder->getFontFace($instructionFontType), '', $instructionFontSize, '', true);
            $this->pdf->multiCell($instructionWidth - 7, 0, $instruction, 0, 'L', false, 2, $instructionPosX + 7, $posY);

            $index++;
            if ($posY < $this->pdf->getY()) {
                $posY = $this->pdf->getY();
            }
            $posY += 1;
        }
    }

    public function printLabel($bookLayout, $type, $text) {
        $labelRot = $bookLayout->get($type.'.rot');
        $labelPosX = $bookLayout->get($type.'.posX');
        $labelPosY = $bookLayout->get($type.'.posY');
        $labelWidth = $bookLayout->get($type.'.width');
        $labelFontType = $bookLayout->get($type.'.fontType');
        $labelFontSize = $bookLayout->get($type.'.fontSize');
        $labelFontColor = $bookLayout->get($type.'.fontColor');

        //$ingredientLabelPosX = 160;
        //$ingredientLabelPosY = 5;

        $color = $this->builder->toColorArray($labelFontColor);
        $this->pdf->setTextColor($color[0], $color[1], $color[2]);
        $this->pdf->setFont($this->builder->getFontFace($labelFontType), '', $labelFontSize, '', true);
        $this->pdf->startTransform();
        if ($labelRot != 0) {
            $this->pdf->rotate($labelRot, 10, 10);
        }
        $this->pdf->multiCell($labelWidth, 0, $text, 0, 'L', false, 2, $labelPosX, $labelPosY);
        $this->pdf->stopTransform();
    }

    public function printIngredientLabel($bookLayout) {
        $label = $bookLayout->get('ingredientLabel.text');
        if ($label != '') {
            $label = $this->builder->translateBlock($label);
            $this->printLabel($bookLayout, 'ingredientLabel', $label);
        }
    }

    public function printLabel1($bookLayout, $preparationTime, $caloryAmount, $caloryAmount2) {
        $label1Text1 = $bookLayout->get('label1.text1');
        $label1Text1 = $this->builder->translateBlock($label1Text1);
        $label1Text2 = $bookLayout->get('label1.text2');
        $label1Text2 = $this->builder->translateBlock($label1Text2);
        $label1Text3 = $bookLayout->get('label1.text3');
        $label1Text3 = $this->builder->translateBlock($label1Text3);
        $label1Text4 = $bookLayout->get('label1.text4');
        $label1Text4 = $this->builder->translateBlock($label1Text4);

        $text = $label1Text1.': '.intval($preparationTime).' '.$label1Text2."\n".
                $label1Text3.': '.$caloryAmount.' '.$label1Text4.' / '.$caloryAmount2.' '.$label1Text4;

        $this->printLabel($bookLayout, 'label1', $text);
    }

    public function printCLabels($bookLayout) {
        for ($i = 1; $i <= 10; $i++) {
            $text = trim($bookLayout->get('cLabel'.$i.'.text'));
            if ($text != '') {
                $text = $this->builder->translateBlock($text);
                $this->printLabel($bookLayout, 'cLabel'.$i, $text);
            }
        }
    }

    public function printRecipeName($bookLayout, $recipeName) {
        $recipeNameText = $this->builder->translateBlock($recipeName);
        $this->printLabel($bookLayout, 'recipeName', $recipeNameText);
    }

    public function printInstructionLabel($bookLayout) {
        $instructionLabel = $bookLayout->get('instructionLabel.text');
        if ($instructionLabel != '') {
            $instructionLabel = $this->builder->translateBlock($instructionLabel);
            $this->printLabel($bookLayout, 'instructionLabel', $instructionLabel);
        }
    }

    private function parseIngredient($ingredient, $caloryAmount, $caloryAmount2) {
        $arr = explode('|', $ingredient);
        $i = array(
            'title'   => '',
            'value1'  => '',
            'value2'  => ''
        );

        if (count($arr) == 2) {
            $i['title'] = trim($arr[1]);

            // Compute amount 1
            if (trim($arr[0]) == '' || trim($arr[0]) == '0') {
                $value = '';
            }
            else {
                $value = floatval($caloryAmount * trim($arr[0]) / 100);
                $value = $this->adaptiveRounding($value);
            }
            $i['value1'] = $value;

            // Compute amount 2
            if (trim($arr[0]) == '' || trim($arr[0]) == '0') {
                $value = '';
            }
            else {
                $value = floatval($caloryAmount2 * trim($arr[0]) / 100);
                $value = $this->adaptiveRounding($value);
            }
            $i['value2'] = $value;

            return $i;
        }

        return false;
    }

    public function printIngredients($bookLayout, $ingredient, $caloryAmount, $caloryAmount2) {
        $ingredient = $this->builder->translateBlock($ingredient);

        $ingredientPosX = $bookLayout->get('ingredient.posX');
        $ingredientPosY = $bookLayout->get('ingredient.posY');
        $ingredientWidth = $bookLayout->get('ingredient.width');
        $ingredientFontType = $bookLayout->get('ingredient.fontType');
        $ingredientFontSize = $bookLayout->get('ingredient.fontSize');
        $ingredientFontColor = $bookLayout->get('ingredient.fontColor');

        $color = $this->builder->toColorArray($ingredientFontColor);
        $this->pdf->setTextColor($color[0], $color[1], $color[2]);
        $this->pdf->setFont($this->builder->getFontFace($ingredientFontType), '', $ingredientFontSize, '', true);

        $posY = $ingredientPosY;
        $ingredients = explode("\n", $ingredient);
        foreach ($ingredients as $ingredient) {
            $arr = $this->parseIngredient($ingredient, $caloryAmount, $caloryAmount2);

            if ($arr !== FALSE) {
                $this->pdf->multiCell(15, 0, $arr['value1'], 0, 'R', false, 2, $ingredientPosX-10, $posY);
                $this->pdf->multiCell(15, 0, $arr['value2'], 0, 'R', false, 2, $ingredientPosX, $posY);
                $this->pdf->multiCell($ingredientWidth - 34, 0, $arr['title'], 0, 'L', false, 2, $ingredientPosX + 20, $posY);

                if ($posY < $this->pdf->getY()) {
                    $posY = $this->pdf->getY();
                }
            }
        }
    }

    protected function adaptiveRounding($value) {
        $rounding = null;
        if ($value < 0.01) {
            return 0.01;
        }
        else if ($value < 0.5) {
            // 0-0.5 (to the nearest 0.1)
            $rounding = 0.1;
        } else if ($value >= 0.5 && $value < 4) {
            // 0.5 to 3 (to the nearest 0.5)‏
            $rounding = 0.5;
        } else if ($value >= 4 && $value < 21) {
            // 4 to 20 (to the nearest 1)‏
            $rounding = 1;
        } else if ($value >= 21) {
            // 21+ to the nearest 5‏
            $rounding = 5;
        }

        $result = $value;
        if ($rounding) {
            $result = round($value/$rounding) * $rounding;
        }

        return $result;
    }
}
