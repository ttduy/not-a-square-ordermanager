<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetScaleTool extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_sport_table';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('leftLabel', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('rightLabel', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('middleLabel', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('fontColor',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('fontSize',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('arrowSize',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('scaleDefinition', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '7', 'cols' => 80
            )
        ));
        $builder->add('selectedValue', 'text', array(
            'required' => false
        ));
        $builder->add('visibility', 'text', array(
            'required' => false
        ));
    }
}
