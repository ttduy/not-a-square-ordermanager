<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetRecipe extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_recipe';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('recipeId', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '9', 'cols' => 80
            )
        ));
        $builder->add('caloryAmount', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('caloryAmount2', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 250px; max-width: 250px'
            )
        ));
        $builder->add('risks', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 80
            )
        ));
        $builder->add('style', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '5', 'cols' => 80
            )
        ));
    }
}
