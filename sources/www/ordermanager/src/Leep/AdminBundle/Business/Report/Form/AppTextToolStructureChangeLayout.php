<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureChangeLayout extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_change_layout';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('idLayout', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getLayouts(),
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
        $builder->add('customLayout',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 250px; max-width: 250px') 
        ));
    }
}
