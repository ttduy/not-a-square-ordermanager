<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureHorizontalLine extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_horizontal_line';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('fontColor',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('lineHeight',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('marginTop',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('marginBottom',  'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 80px; max-width: 80px') 
        ));
        $builder->add('visibility', 'text', array(
            'required' => false
        ));   
    }
}
