<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class UtilMoveCursor extends BaseTextBlock {
    public function build($data) {     
        $this->pdf->setX($data['x']);
        $this->pdf->setY($data['y']);
    }
}
