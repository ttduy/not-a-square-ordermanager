<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class WidgetSportTable {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {
        $this->pdf->setFont($this->builder->fontMap['regular'], '', 10, '', true);

        $tableDefinition = $this->builder->translateBlock($data['tableDefinition']);
        $activityDefinition = $this->builder->translateBlock($data['activityDefinition']);
        $sessionDefinition = $this->builder->translateBlock($data['sessionDefinition']);
        $dailySportKcal = $this->builder->translateBlock($data['dailySportKcal']);

        $dataStyle = $this->builder->translateBlock(isset($data['style']) ? $data['style'] : '');
        $style = array(
            'header.font_size'    => '10',
            'header.font_type'    => 'regular',
            'activity.font_size'  => '10',
            'activity.font_type'  => 'regular',
            'session.font_size'   => '10',
            'session.font_type'   => 'regular',
            'column.sport_type.visible' => 'NO',
        );
        $style = $this->builder->parseInput($style, $dataStyle);

        $r = explode("\n", $tableDefinition);
        list($topLeftCellText, $topLeftCellColor) = explode('|', $r[0]);
        list($tableTitleText, $tableTitleColor) = explode('|', $r[1]);
        list($onAverageText, $onAverageColor) = explode('|', $r[2]);

        $activities = array();
        $r = explode("\n", $activityDefinition);
        list($activityOddColor, $activityEvenColor) = explode('|', $r[0]);
        unset($r[0]);
        foreach ($r as $row) {
            $rowData = explode('|', $row);
            $activities[] = array('title' => trim($rowData[0]), 'kcal' => trim($rowData[1]), 'type' => isset($rowData[2]) ? trim($rowData[2]) : "");
        }

        $sessions = array();
        $r = explode("\n", $sessionDefinition);
        foreach ($r as $row) {
            list($numberSession, $headerColor, $oddColor, $evenColor) = explode('|', $row);
            $sessions[] = array(
                'numberSession' => $numberSession,
                'headerColor'   => $headerColor,
                'oddColor'      => $oddColor,
                'evenColor'     => $evenColor
            );
        }

        $tableTitleText = $this->builder->makeHtmlText($tableTitleText, $style['header.font_type'], $style['header.font_size']);
        $topLeftCellText = $this->builder->makeHtmlText($topLeftCellText, $style['header.font_type'], $style['header.font_size']);
        $onAverageText = $this->builder->makeHtmlText($dailySportKcal.' '.$onAverageText, $style['header.font_type'], $style['header.font_size']);

        $isSportTypeVisible = $style['column.sport_type.visible'] == 'YES';
        $colSpan = 1;
        $leftColumWidth = '37%';
        $rightColumnWidth = '63%';
        if ($isSportTypeVisible) {
            $colSpan = 2;
            $leftColumWidth = '58%';
            $rightColumnWidth = '42%';
        }

        $html = '<table cellspacing="3">
            <thead>
                <tr>
                    <th width="'.$leftColumWidth.'" colspan="'.$colSpan.'" style="text-align: center; color: black; background-color: '.$topLeftCellColor.'">'.$topLeftCellText.'</th>
                    <th width="'.$rightColumnWidth.'" colspan="'.count($sessions).'" style="color: black; text-align: center;background-color: '.$tableTitleColor.'">'.$tableTitleText.'</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th width="'.$leftColumWidth.'" colspan="'.$colSpan.'" style="text-align: center; color: black; background-color: '.$onAverageColor.'">'.$onAverageText.'</th>';
        foreach ($sessions as $s) {
            $noSession = $this->builder->makeHtmlText($s['numberSession'].'X', $style['header.font_type'], $style['header.font_size']);

            $html .= '<th width="'.($isSportTypeVisible ? '6%' : '9%').'" style="text-align: center; color: white; background-color: '.$s['headerColor'].'">'.$noSession.'</th>';
        }

        $html.= '</tr>';

        $i = 0;
        foreach ($activities as $activity) {
            $i++;
            $activityColor = ($i % 2 == 0) ? $activityOddColor : $activityEvenColor;

            $activityTitle = $activity['title'].' ('.$activity['kcal'].' kcal/h)';
            $activityTitle = $this->builder->makeHtmlText($activityTitle, $style['activity.font_type'], $style['activity.font_size']);

            $icons = json_decode('{"circle": "\uf111", "rectangle": "\uf04d"}');
            $circle = "";
            $rectangle = "";

            $activityType = trim($activity['type']);
            if ($activityType == '0' || $activityType == '2') {
                $circle = $icons->circle;
            }

            if ($activityType == '1' || $activityType == '2') {
                $rectangle = $icons->rectangle;
            }

            $html .= '<tr>';
            if ($style['column.sport_type.visible'] == 'YES') {
                $html .= '  <td style="width: 7%; font-family: customawsome; background-color: '.$activityColor.'"><table><tr><td style="width: 10%"></td><td style="color: #00b0f0; width: 44%">'.$circle.'</td><td style="color: #00b050; width: 44%">'.$rectangle.'</td></tr></table></td>';
                $html .= '  <td style="width: 51%; color: black; background-color: '.$activityColor.'">'.$activityTitle.'</td>';
            } else {
                $html .= '  <td style="width: '.$leftColumWidth.'; color: black; background-color: '.$activityColor.'">'.$activityTitle.'</td>';
            }
            foreach ($sessions as $s) {
                $sessionColor = ($i %2  == 0) ? $s['oddColor'] : $s['evenColor'];
                $minute = ($dailySportKcal * 7 * 60) / ($s['numberSession'] * $activity['kcal']);

                $sessionTitle = round($minute).(!$isSportTypeVisible ? ' min' : '');
                $sessionTitle = $this->builder->makeHtmlText($sessionTitle, $style['session.font_type'], $style['session.font_size']);
                $html .= '<td style="text-align: center; color: black; background-color: '.$sessionColor.'">'.$sessionTitle.'</td>';
            }
            $html .= '</tr>';
        }


        $html.= '</tbody></table>';
        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);
    }
}
