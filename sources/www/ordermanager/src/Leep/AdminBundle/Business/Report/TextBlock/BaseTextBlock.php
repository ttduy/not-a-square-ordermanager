<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class BaseTextBlock {
    public $container;
    public $pdf;
    public $builder;
    public $data;

    public function __construct($container, $pdf, $builder, $data = []) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
        $this->data = $data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function parseInput($arr, $input) {
        $rows = explode("\n", $input);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $arr[trim($k)] = trim($v);
            }
        }
        return $arr;
    }

    public function getData($key, $default = '') {
        if (isset($this->data[$key]) && trim($this->data[$key]) != '') {
            return $this->data[$key];
        }

        return $default;
    }

    public function finalize($data) {

    }

    public function process() {}
}
