<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business\Report\Constants;

class WidgetDrugList {
    public $container;
    public $pdf;
    public $drugsInformation = [];
    public $parameters = [];

    const ENTER = 210;
    const SUB_ENTER = 70;
    const DIS_NAME_BACK = 25;
    const MM_TO_PT = 0.353;
    const LAST = "<--LAST-->";
    const LEFT_MARGIN_SEPCIAL_STRING = 22;
    const RIGHT_MARGIN_SEPCIAL_STRING = 27;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {
        $this->loadModel($data);
        $this->render();
    }

    private function getReturnData($passData, $drugKey) {
        $nameKey = 'DRUG_'.$drugKey.'_NAME';
        $effectKey = 'DRUG_'.$drugKey.'_EFFECT';
        $breakDownKey = 'DRUG_'.$drugKey.'_BREAK_DOWN';
        $dosisKey = 'DRUG_'.$drugKey.'_DOSIS';
        $bgColor = 'DRUG_'.$drugKey.'_BG_COLOR_DRUG_NAME';
        return $passData[$nameKey].'|'.$passData[$effectKey].'|'.$passData[$breakDownKey].'|'.$passData[$dosisKey].'|'.$passData[$bgColor];
    }

    private function getDrugInformation($drugKeys) {
        $drugKeys = explode("\n", $drugKeys);
        $drugKeys = array_map('trim', $drugKeys);
        $passData = $this->builder->configData;
        $returnData = [];
        foreach ($drugKeys as $drugKey) {
            $returnData[] = $this->getReturnData($passData, $drugKey);
        }
        return $returnData;
    }

    public function loadModel($data) {
        $parameters = explode("\n", $data['parameters']);
        foreach ($parameters as $parameter) {
            $tmp = explode("=", $parameter);
            if (count($tmp) == 2) {
                $key = trim($tmp[0]);
                $value = trim($tmp[1]);
                $this->parameters[$key] = $value;
            }
        }
        $this->drugsInformation = $this->getDrugInformation($data['drugsInformation']);
    }

    private function isNormalDrug($name) {
        $len = strlen($name) * $this->parameters['font-size'] - self::ENTER;
        if($len > $this->DRUG_NAME_TEXT_WIDTH) {
            return false;
        }
        return true;
    }

    private function applyColor($e1, $e2, $e3) {
        if ($e1 == '0' || $e2 == '0' || $e3 == '0') {
            return $this->parameters['bgcolor-x-existed'];
        }
        elseif ($e1 == '2' && $e2 == '2' && $e3 == '2') {
            return $this->parameters['bgcolor-all-v'];
        }
        else {
            return $this->parameters['bgcolor-updown-existed'];
        }
    }

    public function parseChunk($chunk) {
        $drugs = [];
        foreach ($chunk as $entry) {
            $tokens = explode('|', $entry);
            $drugs[] = array(
                'title' => trim($tokens[0]),
                'e1'    => $tokens[1],
                'e2'    => $tokens[2],
                'e3'    => $tokens[3],
                'color' => $tokens[4]
            );
        }
        return $drugs;
    }

    public function render() {
        $prevPageUnit = $this->pdf->getPageUnit();
        $this->pdf->setPageUnit('pt');
        $this->pdf->SetAutoPageBreak(FALSE);
        $this->pdf->deletePage($this->pdf->getPage());

        $lastMargins = $this->pdf->getMargins();
        $tmpTop = $this->parameters['top-margin'];
        $tmpLeft = $this->parameters['left-margin'];
        $tmpRight = $this->parameters['right-margin'];
        $this->parameters['top-margin'] = intval(($lastMargins['top'] + $tmpTop) / self::MM_TO_PT);
        $this->parameters['left-margin'] = intval(($lastMargins['left']  + $tmpLeft) / self::MM_TO_PT);
        $this->parameters['right-margin'] = intval(($lastMargins['right'] + $tmpRight) / self::MM_TO_PT);

        $effectivePageWidth = $this->pdf->getPageWidth() - $this->parameters['left-margin'] - $this->parameters['right-margin'];
        $this->START_Y = $this->parameters['top-margin'];
        $this->COLUMN_WIDTH = intval($effectivePageWidth/3);
        $this->DRUG_HEIGHT = $this->parameters['row-height'];
        $this->VARIABLE_WIDTH = $this->parameters['row-height'];
        $this->DRUG_NAME_TEXT_WIDTH = $this->COLUMN_WIDTH-($this->VARIABLE_WIDTH*3+6)-$this->parameters['space-column']-10;
        $this->HEADER_WIDTH = intval($this->DRUG_NAME_TEXT_WIDTH  / 2);
        $this->DRUG_NAME_X = 0;
        $this->DRUG_EFFECT_X = $this->DRUG_NAME_TEXT_WIDTH+2;
        $this->DRUG_BREAK_DOWN_X = $this->DRUG_NAME_TEXT_WIDTH + $this->VARIABLE_WIDTH + 4;
        $this->DRUG_DOSIS_X = $this->DRUG_NAME_TEXT_WIDTH + $this->VARIABLE_WIDTH*2 + 6;

        $maxNumberDrug = $this->parameters['max-number-drugs'];
        $chunks = array_chunk($this->drugsInformation, $maxNumberDrug * 3);
        foreach ($chunks as $chunk) {
            if (count($chunk) > 0) {
                $this->pdf->setPageUnit($prevPageUnit);
                $this->pdf->addPage('', '', false, true);
                $this->pdf->setPageUnit('pt');

                $sub_chunks = array_chunk($chunk, ceil(count($chunk)/3));

                $baseX = $this->parameters['left-margin'];
                foreach ($sub_chunks as $sub_chunk) {
                    $drugs = $this->parseChunk($sub_chunk);

                    $this->drawHeader($baseX + $this->DRUG_EFFECT_X+16, $this->START_Y+20, $this->parameters['label-effect']);
                    $this->drawHeader($baseX + $this->DRUG_BREAK_DOWN_X+16, $this->START_Y+20, $this->parameters['label-breakdown']);
                    $this->drawHeader($baseX + $this->DRUG_DOSIS_X+16, $this->START_Y+20, $this->parameters['label-dosis']);
                    $y = $this->START_Y;
                    foreach ($drugs as $drug) {
                        $height = $this->DRUG_HEIGHT;
                        if (!$this->isNormalDrug($drug['title'])) {
                            $height = $height * 2 + 3;
                        }
                        $this->printName($baseX + $this->DRUG_NAME_X, $y, $drug['title'], $this->DRUG_NAME_TEXT_WIDTH, $height, $drug['color']);
                        $this->printBar($baseX + $this->DRUG_EFFECT_X, $y, $drug['e1'], $this->VARIABLE_WIDTH, $height);
                        $this->printBar($baseX + $this->DRUG_BREAK_DOWN_X, $y, $drug['e2'], $this->VARIABLE_WIDTH, $height);
                        $this->printBar($baseX + $this->DRUG_DOSIS_X, $y, $drug['e3'], $this->VARIABLE_WIDTH, $height);
                        $y += $height+5;
                    }

                    // avoid break header in next column
                    $this->printName($baseX + $this->DRUG_NAME_X, $y, self::LAST, $this->DRUG_NAME_TEXT_WIDTH, $this->DRUG_HEIGHT, "#FFFFFF");
                    $this->printBar($baseX + $this->DRUG_EFFECT_X, $y, '', $this->VARIABLE_WIDTH, $this->DRUG_HEIGHT);
                    $this->printBar($baseX + $this->DRUG_BREAK_DOWN_X, $y, '', $this->VARIABLE_WIDTH, $this->DRUG_HEIGHT);
                    $this->printBar($baseX + $this->DRUG_DOSIS_X, $y, '', $this->VARIABLE_WIDTH, $this->DRUG_HEIGHT);

                    $baseX += $this->COLUMN_WIDTH;
                }
            }
        }

        $this->pdf->setPageUnit($prevPageUnit);
    }

    private function drawHeader($x, $y, $label) {
        $this->pdf->setFont($this->builder->fontMap['regular'], '', 7);
        $this->pdf->setFillColorArray(array(255, 255, 255));
        $this->pdf->setTextColorArray(array(0,0,0));
        $this->pdf->startTransform();
        $this->pdf->setX(0);
        $this->pdf->setY(0);
        $this->pdf->Rotate(90, 0, 0);
        $this->pdf->translateX(-$y);
        $this->pdf->translateY($x);
        $this->pdf->MultiCell($this->HEADER_WIDTH - 2, $this->DRUG_HEIGHT - 2, $label, 0, 'L', true, 1);
        $this->pdf->stopTransform();
    }

    private function isSpecialName($name) {
        $lenName = strlen($name);
        if ($lenName >= self::LEFT_MARGIN_SEPCIAL_STRING && $lenName <= self::RIGHT_MARGIN_SEPCIAL_STRING) {
            return true;
        }
        return false;
    }

    private function divideName($name, $numberParts) {
        $lenName = strlen($name);
        $seperatedName = [];
        if (($this->isSpecialName($name)) && (strpos($name, ' ') !== false)) {
            $mid = intval($lenName / 2);
            $i = $mid;
            $j = $mid;
            $pos = $mid;
            while(1){
                $splitChars = [' '];
                $currentChar = substr($name, $i, 1);
                if (in_array($currentChar, $splitChars) || $i == 0) {
                    $pos = $i;
                    break;
                }
                $currentChar = substr($name, $j, 1);
                if (in_array($currentChar, $splitChars) || $j == $lenName - 1) {
                    $pos = $j;
                    break;
                }
                $j++;
                $i--;
            }
            $part1 = substr($name, 0, $pos);
            $part2 = substr($name, $pos);
            $seperatedName[] = $part1;
            $seperatedName[] = $part2;
        }
        else {
            $chunkLen = intval(strlen($name) * (1 / $numberParts)) + 1;
            $seperatedName = str_split($name, $chunkLen);
        }
        return $seperatedName;
    }

    private function printName($x, $y, $name, $width = 100, $height = 10, $bgcolor='#FFFF00') {
        $len = strlen($name) * $this->parameters['font-size'] - self::ENTER;
        $lenSub = strlen($name) * $this->parameters['font-size'] - self::SUB_ENTER;
        $numberParts = intval($len / $this->DRUG_NAME_TEXT_WIDTH) + 1;
        $numberPartsSub = intval($lenSub / ($this->DRUG_NAME_TEXT_WIDTH + 28)) + 1;
        if ($this->isSpecialName($name)) {
            $numberPartsSub = 2;
        }
        $marginTop = ($height - $this->parameters['font-size'] * $numberPartsSub)/2;
        $bgColorArr = $this->builder->toColorArray($bgcolor);
        $this->pdf->setFont($this->builder->fontMap['regular'], '', $this->parameters['font-size']);
        $this->pdf->setCellPaddings(0, $marginTop, 0, 0);
        $this->pdf->setFillColorArray($bgColorArr);
        $this->pdf->setTextColorArray(array(0,0,0));

        $this->pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $bgColorArr));
        $heightFixed = $height == $this->DRUG_HEIGHT ? $height + 2 : $height;
        $this->pdf->RoundedRect($x, $y, $width+10, $heightFixed, 3.50, '1111', 'DF');
        $x_print = $x - self::DIS_NAME_BACK;
        if ((($height == $this->DRUG_HEIGHT || strpos($name, ' ') !== false) && ($numberParts <= 2) && (!$this->isSpecialName($name)))
            || (strcmp($name, self::LAST) === 0)) {
            $height = $this->DRUG_HEIGHT;
            $name = strcmp($name, self::LAST) === 0 ? "" : $name;
            $this->printString($x_print, $y, $width, $height, $name, 'R');
        }
        else {
            if ($this->isSpecialName($name)) {
                $height = $this->DRUG_HEIGHT;
                $numberPartsSub = 2;
            }
            $seperatedName = $this->divideName($name, $numberPartsSub);
            for ($i=0; $i < $numberPartsSub; $i++) {
                $y_print = $y + 9*$i;
                $this->printString($x_print, $y_print, $width, $height, $seperatedName[$i], 'R');
            }
        }
    }

    private function printString($x, $y, $width, $height, $printedString, $align) {
        $this->pdf->startTransform();
        $this->pdf->setX(0);
        $this->pdf->setY(0);
        $this->pdf->translateX($x);
        $this->pdf->translateY($y);
        $this->pdf->MultiCell($width, $height, $printedString, 0, $align, false, 0);
        $this->pdf->stopTransform();
    }

    private function getVariableColor($outcome) {
        switch ($outcome) {
            case '0':
                return ['fontColor' => $this->parameters['icon-color-x'],  'bgColor' => $this->parameters['bgcolor-x-existed'], 'text' => 'G'];
                break;
            case '1':
                return ['fontColor' => $this->parameters['icon-color-down'], 'bgColor' => $this->parameters['bgcolor-updown-existed'], 'text' => 'U'];
                break;
            case '2':
                return ['fontColor' => $this->parameters['icon-color-v'], 'bgColor' => $this->parameters['bgcolor-all-v'], 'text' => 'r'];
                break;
            case '3':
                return ['fontColor' => $this->parameters['icon-color-up'], 'bgColor' => $this->parameters['bgcolor-updown-existed'], 'text' => 'N'];
                break;
            default:
                return ['fontColor' => '#FFFFFF', 'bgColor' => '#FFFFFFF', 'text' => '-1'];
                break;
        }
    }

    private function printBar($x, $y, $outcome, $width = 17, $height = 17) {
        $printInfo = $this->getVariableColor($outcome);
        $this->pdf->setFont('pizzadudebullets', '', $this->parameters['variable-size'], '', false);
        $bgColorArr = $this->builder->toColorArray($printInfo['bgColor']);
        $this->pdf->setFillColorArray($bgColorArr);
        $this->pdf->setTextColorArray($this->builder->toColorArray($printInfo['fontColor']));
        $marginTop = 0;
        if ($height > $this->DRUG_HEIGHT) {
            $marginTop = ($height - $this->parameters['font-size'])/2-4;
        }
        $this->pdf->setCellPaddings(0, $marginTop, 0, 0);
        $this->pdf->startTransform();
        $this->pdf->setX(0);
        $this->pdf->setY(0);
        $this->pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $bgColorArr));
        $height = $height == $this->DRUG_HEIGHT ? $height + 2 : $height;
        $this->pdf->RoundedRect($x+10, $y, $width, $height, 4.50, '1111', 'DF');
        $this->pdf->translateX($x-13);
        $this->pdf->translateY($y+1);
        $this->pdf->MultiCell($width-4, $height-4, $printInfo['text'], 0, 'C', true, 0);
        $this->pdf->stopTransform();
    }
}
