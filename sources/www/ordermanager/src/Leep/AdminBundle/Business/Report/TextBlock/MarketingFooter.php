<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class MarketingFooter extends BaseTextBlock {
    public function build($data) { 
        $toolStyle = array(
            'BoxColor'   => '#173764',
            'BoxHeight'  => '35',
            'ImageWidth' => '80',
            'TextColor'  => '#FFFFFF',
            'TextSize'   => '11',
            'ImageMarginTop' => '5'
        );
        $toolStyle = $this->builder->parseInput($toolStyle, $data['style']);

        $curX = $this->pdf->getX();
        $curY = $this->pdf->getY();

        // Draw background box
        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();
        $margins = $this->pdf->getMargins();
        $totalWidth = $pageWidth - $margins['left'] - $margins['right'];

        $rootX = $margins['left'];
        $rootY = $pageHeight - $margins['bottom'] - $toolStyle['BoxHeight'];

        $fillColor = $this->builder->toColorArray($toolStyle['BoxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $rootX,
            $y = $rootY,
            $w = $pageWidth - $margins['left'] - $margins['right'],
            $h = $toolStyle['BoxHeight'],
            'F',
            $style,
            $fillColor
        );

        // Draw image
        $imageWidth = $toolStyle['ImageWidth'];
        $imageMarginLeft = 5;
        $imageMarginTop = -$toolStyle['ImageMarginTop'];
        $imageFile = $this->builder->resourceDir.$data['image'];
        $this->pdf->myDrawImage(
            $imageFile, 
            $x = $rootX + $imageMarginLeft, 
            $y = $rootY + $imageMarginTop,
            $w=$imageWidth, 
            $h=0, 
            $type='', 
            $link='', 
            $align='T', 
            $resized=true, 
            $dpi=300, 
            $palign='', 
            $ismask=false, 
            $imgmask=false, 
            $border=0, 
            $fitbox=false, 
            $hidden=false, 
            $fitonpage=false
        );  

        // Write text
        $text = $data['text'];
        $text = $this->builder->translateCustomTag($text);
        $text = '<p style="color: #FFFFFF; font-size: '.$toolStyle['TextSize'].'"><font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</font></p>';
        $titleWidth = $totalWidth - $imageWidth - 9;
        $titleMarginLeft = $imageWidth + $imageMarginLeft + 2;
        $titleMarginTop = 2;
        $this->pdf->writeHTMLCell(
            $w=$titleWidth, 
            $h=0, 
            $x=$rootX + $titleMarginLeft, 
            $y=$rootY + $titleMarginTop,
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='J', 
            $autopadding=true
        );

        $this->pdf->setX($curX);
        $this->pdf->setY($curY);
    }
}
