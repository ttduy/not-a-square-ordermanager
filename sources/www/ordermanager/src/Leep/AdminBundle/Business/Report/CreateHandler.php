<?php
namespace Leep\AdminBundle\Business\Report;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->sortOrder = 99;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));

        $builder->add('shortName', 'text', array(
            'attr'     => array(
                'placeholder' => 'Short name'
            ),
            'required' => false
        ));

        $builder->add('reportCode', 'text', array(
            'label'    => 'Report code',
            'attr'     => array(
                'placeholder' => 'Report code'
            ),
            'required' => true
        ));

        $builder->add('sortOrder', 'text', array(
            'attr'     => array(
                'placeholder' => 'Sort order'
            ),
            'required' => false
        ));

        $builder->add('idReport', 'choice', array(
            'label'    => 'Copy from report',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Report_List'),
        ));

        $builder->add('idOption', 'choice', array(
            'label'    => 'Option',
            'required' => false,
            'choices'  => array(
                'link'     => 'Copy link only'
            )
        ));

        $builder->add('isVisibleByDefault', 'checkbox', array(
            'label'    => 'Is visible to customers by default?',
            'required' => false
        ));

        $builder->add('isReleased', 'checkbox', array(
            'label'    => 'Released for report generation?',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $report = new Entity\Report();
        $report->setName($model->name);
        $report->setShortName($model->shortName);
        $report->setReportCode($model->reportCode);
        $report->setSortOrder($model->sortOrder);
        $report->setIsVisibleByDefault($model->isVisibleByDefault);
        $report->setIsReleased($model->isReleased);

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $em->persist($report);
        $em->flush();

        if ($model->idReport != 0) {
            $copyReport = $doctrine->getRepository('AppDatabaseMainBundle:Report')->findOneById($model->idReport);
            $report->setTestData($copyReport->getTestData());
            $em->persist($report);
            $em->flush();

            $sections = $doctrine->getRepository('AppDatabaseMainBundle:ReportSection')->findByIdReport($model->idReport);
            if ($model->idOption == 'link') {
                foreach ($sections as $section) {
                    $newSection = new Entity\ReportSection();
                    $newSection->setIdReport($report->getId());
                    $newSection->setName($section->getName());
                    $newSection->setIsHide($section->getIsHide());
                    $newSection->setIdType(2);
                    if ($section->getIdReportSectionLink() != 0) {
                        $newSection->setIdReportSectionLink($section->getIdReportSectionLink());
                    }
                    else {
                        $newSection->setIdReportSectionLink($section->getId());
                    }
                    $newSection->setAlias($section->getAlias());
                    $newSection->setSortOrder($section->getSortOrder());
                    $em->persist($newSection);
                    $em->flush();
                }

            }
            else {
                foreach ($sections as $section) {
                    $newSection = new Entity\ReportSection();
                    $newSection->setIdReport($report->getId());
                    $newSection->setName($section->getName());
                    $newSection->setIsHide($section->getIsHide());
                    $newSection->setIdReportSectionLink($section->getIdReportSectionLink());
                    $newSection->setIdType($section->getIdType());
                    $newSection->setAlias($section->getAlias());
                    $newSection->setSortOrder($section->getSortOrder());
                    $em->persist($newSection);
                    $em->flush();

                    $reportItems = $doctrine->getRepository('AppDatabaseMainBundle:ReportItem')->findByIdReportSection($section->getId());
                    foreach ($reportItems as $reportItem) {
                        $newItem = new Entity\ReportItem();
                        $newItem->setIdReport($report->getId());
                        $newItem->setIdReportSection($newSection->getId());
                        $newItem->setIdTextToolType($reportItem->getIdTextToolType());
                        $newItem->setData($reportItem->getData());
                        $newItem->setSortOrder($reportItem->getSortOrder());
                        $em->persist($newItem);
                    }
                    $em->flush();
                }
            }
        }

        $this->newId = $report->getId();

        parent::onSuccess();
    }
}