<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetImageWithLabel extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_image_with_label';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('image', 'app_image_picker', array(
            'label'  => 'Image',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('overrideImage', 'text', array(
            'label'  => 'Override image',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('height', 'text', array(
            'label'  => 'Height',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('rotation', 'text', array(
            'label'  => 'Rotation',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('labels', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '7', 'cols' => 80
            )
        ));
        $builder->add('posX',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('posY',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('alignment', 'choice', array(
            'required' => false,
            'choices'  => array('left' => 'Left', 'center' => 'Centered', 'right' => 'Right'),
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            )
        ));
        $builder->add('visibility', 'text', array(
            'required' => false
        ));
        $builder->add('link', 'text', array(
            'required' => false,
        ));
    }
}
