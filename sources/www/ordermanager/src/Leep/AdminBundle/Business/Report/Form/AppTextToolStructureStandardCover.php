<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureStandardCover extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_standard_cover';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('bgColor', 'textarea', array(
            'required' => true,
            'attr' => [
                'rows'  => '5',
                'cols' => 80
            ]
        ));

        $builder->add('lineBox', 'textarea', [
            'required' => true,
            'attr' => [
                'rows'  => '5',
                'cols' => 80
            ]
        ]);
        $builder->add('logoBox', 'textarea', [
            'required' => true,
            'attr' => [
                'rows'  => '8',
                'cols' => 100
            ]
        ]);
        $builder->add('iconBox', 'textarea', [
            'required' => true,
            'attr' => [
                'rows'  => '8',
                'cols' => 100
            ]
        ]);
        $builder->add('imageContent', 'app_image_picker', array(
            'label'  => 'Image',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 500px; max-width: 500px'
            )
        ));
        $builder->add('numPage', 'text', array(
            'required' => false,
            'attr' => [
                'style' => 'min-width: 80px; max-width: 80px'
            ]
        ));


    }
}
