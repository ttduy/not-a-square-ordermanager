<?php
namespace Leep\AdminBundle\Business\Report;

class Utils {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private $textToolColors = array();
    public function getTextToolColors() {
        if (empty($this->textToolColors)) {
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextToolType')->findAll();
            foreach ($result as $r) {
                $this->textToolColors[$r->getControlMapping()] = $r->getBgColor();
            }
        }
        return $this->textToolColors;
    }

    public function getReportSections() {
        $mapping = $this->container->get('easy_mapping');   
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('(p.idType IS NULL) OR (p.idType != 2)')
            ->addOrderBy('p.idReport', 'ASC')
            ->addOrderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();

        $map = array();
        foreach ($sections as $section) {
            $title = $mapping->getMappingTitle('LeepAdmin_Report_List', $section->getIdReport());
            if (empty($title)) {
                continue;
            }
            $title .=' - '.$section->getName();
            $map[$section->getId()] = $title;
        }
        return $map;
    }

    public function getReportSectionForSearchBox() {
        $mapping = $this->container->get('easy_mapping');   
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('(p.idType IS NULL) OR (p.idType != 2)')
            ->addOrderBy('p.idReport', 'ASC')
            ->addOrderBy('p.sortOrder', 'ASC');
        $sections = $query->getQuery()->getResult();

        $map = array();
        foreach ($sections as $section) {
            $title = $mapping->getMappingTitle('LeepAdmin_Report_List', $section->getIdReport());
            if (empty($title)) {
                continue;
            }
            $title .=' - '.$section->getName();
            $map[] = array(
                'id' => $section->getId(),
                'text' => $title
            );
        }
        return $map;
    }
    public function getReportForSearchBox() {
        $mapping = $this->container->get('easy_mapping');   
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Report', 'p')
            ->addOrderBy('p.sortOrder', 'ASC')
            ->addOrderBy('p.name', 'ASC');
        $reports = $query->getQuery()->getResult();

        $map = array();
        foreach ($reports as $report) {
            $map[] = array(
                'id' => $report->getId(),
                'text' => $report->getName()
            );
        }
        return $map;
    }
    private function scandirRecursive($directory, $rootPath) 
    { 
        $folderContents = array(); 
        $directory = realpath($directory).DIRECTORY_SEPARATOR; 

        foreach (scandir($directory) as $folderItem) 
        { 
            if ($folderItem != "." && $folderItem != ".." && $folderItem != '.tmb')  
            { 
                if (@is_dir($directory.$folderItem.DIRECTORY_SEPARATOR)) 
                { 
                    $childContents = $this->scandirRecursive( $directory.$folderItem."/", $folderItem); 
                    foreach ($childContents as $c) {
                        $folderContents[] = $rootPath.'/'.$c;
                    }
                } 
                else 
                { 
                    $folderContents[] = $rootPath.'/'.$folderItem; 
                } 
            } 
        } 

        return $folderContents; 
    }

    public function getResourceByDir($dirName) {
        $dir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/'.$dirName;

        $allDir = $this->scandirRecursive($dir, '');
        $dirs = array();
        foreach ($allDir as $c) {
            $dirs[$c] = $c;
        }

        return $dirs;
    }

    public function getResources() {
        return $this->getResourceByDir('report_resource');
    }   

    public function getExternalPdf() {
        return $this->getResourceByDir('external_pdf');
    }

    public function getThirdPartyPdf() {
        return $this->getResourceByDir('third_party_pdf');
    }
}