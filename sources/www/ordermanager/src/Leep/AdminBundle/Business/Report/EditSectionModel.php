<?php
namespace Leep\AdminBundle\Business\Report;

class EditSectionModel {
    public $name;
    public $shortName;
    public $reportCode;
    public $sectionList;
    public $idFormulaTemplate;
    public $sortOrder;
    public $products;
    public $specialProducts;
    public $isVisibleByDefault;
    public $isReleased;
    public $idLanguage;
}