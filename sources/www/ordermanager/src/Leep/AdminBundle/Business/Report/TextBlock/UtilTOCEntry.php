<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class UtilTOCEntry extends BaseTextBlock {
    public function build($data) { 
        $code = isset($data['code']) ? $data['code'] : '';

        /*
        $this->pdf->setDestination($title);

        $this->pdf->Bookmark(
            $title, 
            $level = 0,
            $y = 0,
            $page = $this->pdf->getGroupPageNo(),
            $style = '',
            $color = array(0,0,0),
            $x = 0,
            $link = '#'.$title
            );*/    
        $this->builder->variables[$code] = $this->pdf->getGroupPageNo();
    }
}
