<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolStructureChangeFormat extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_structure_change_format';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('idFormat', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getPageFormat(),
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
        $builder->add('idOrientation', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getPageOrientation(),
            'attr' => array(
                'style' => 'min-width: 150px; max-width: 150px'
            ) 
        ));
        $builder->add('width', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 90px; max-width: 90px'
            ) 
        ));
        $builder->add('height', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 90px; max-width: 90px'
            ) 
        ));
    }
}
