<?php
namespace Leep\AdminBundle\Business\Report;

class EditModel {
    public $name;
    public $shortName;
    public $reportCode;
    public $testData;
    public $idReportSection;
    public $textToolList;
    public $idLanguage;
}