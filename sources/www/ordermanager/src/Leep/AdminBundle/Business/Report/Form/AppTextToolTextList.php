<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolTextList extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_text_list';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('textBlock', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '3', 'cols' => 80
            )
        ));
        $builder->add('fontColor',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            ) 
        ));
        $builder->add('fontSize',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            ) 
        ));        
        $builder->add('effects', 'choice', array(
            'required' => false,
            'choices'  => Business\Report\Constants::getEffects()
        ));
        $builder->add('bulletColor',  'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 100px; max-width: 100px'
            ) 
        ));
        $builder->add('visibility', 'text', array(
            'required' => false
        ));   
    }
}
