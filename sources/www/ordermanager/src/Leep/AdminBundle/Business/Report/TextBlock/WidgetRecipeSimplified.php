<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

use Leep\AdminBundle\Business\Report\Constants;
use App\AdminBundle\Module\Report\TextTool\BaseTextTool;

class WidgetRecipeSimplified {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function parseInput($arr, $input) {
        $rows = explode("\n", $input);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $arr[trim($k)] = trim($v);
            }
        }
        return $arr;
    }

    public function build($data) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $smileyCalculator = $this->container->get('leep_admin.report.business.util.smiley_caculator');
        $smileyCalculator->translateText($this->builder);

        //////////////////////////////////////////////
        // x. List all recipes
        $recipeIdList = explode("\n", $data['recipeId']);

        $recipes = array();
        foreach ($recipeIdList as $recipe) { // check if current recipe has coordinate for smiley
            $arr = explode("|", $recipe);
            $recipes[] = array(
                'name'    => trim($arr[0]),
            );
        }

        //////////////////////////////////
        // x. Parse style
        $style = array(
            'layout.padding'          => '5',
            'layout.image.width'      => '50',
            'layout.ingredients.posX' => '100',
            'layout.instruction.posX' => '500',
            'layout.text.posY'        => 10,
            'layout.height'           => '200',
            'layout.background1'      => '#DDDDDD',
            'layout.background2'      => '#FFFFFF',
            'layout.kcal.visibility'  => '',
            'header.font'             => 'light',
            'header.font.size'        => '11',
            'header.color'            => '#443322',
            'text.font'               => 'light',
            'text.font.size'          => '11',
            'text.color'              => '#333333',
            'label.ca'                => '.ca',
            'label.kcal'              => 'kcal',
            'smiley.visible'          => 'NO',
            'smiley.textWidth'        => '300',
            'smiley.fontType'         => 'bold',
            'smiley.fontSize'         => '25',
            'smiley.fontColor'        => '#690054'
        );
        $style = $this->parseInput($style, $data['style']);

        //////////////////////////////////////////////
        // x. Process recipes
        $bMargin = $this->pdf->getBreakMargin();
        $margins = $this->pdf->getMargins();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $pageWidth = $this->pdf->getPageWidth() - $margins['left'] - $margins['right'];
        $pageHeight = $this->pdf->getPageHeight() - $bMargin;
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->setCellPaddings(0, 0, 0, 0);

        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $caloryAmount = $data['caloryAmount'];

        $padding = $style['layout.padding'];

        $i = 0;
        $curY = $this->pdf->getY();
        $curX = $this->pdf->getX();
        foreach ($recipes as $recipe) {
            $idRecipe = $recipe['name'];

            $recipe = $em->getRepository('AppDatabaseMainBundle:Recipe')->findOneByRecipeId($idRecipe);

            if ($curY + $style['layout.height'] > $pageHeight) {
                $this->pdf->addPage();
                $curY = $margins['top'];
            }

            $rootX = $curX;
            $rootY = $curY;

            // Background color
            $color = $this->builder->toColorArray($style['layout.background'.($i%2+1)]);
            $this->pdf->setFillColor($color[0], $color[1], $color[2]);
            $this->pdf->setFont($this->builder->fontMap[$style['text.font']], '', $style['text.font.size']);
            $this->pdf->setX($curX);
            $this->pdf->setY($curY);
            $this->pdf->MultiCell(
                $w = $pageWidth,
                $h = $style['layout.height'],
                $txt = '',
                $border = 0,
                $align = 'J',
                $fill = true,
                $ln = 1
            );
            $curX = $this->pdf->getX();
            $curY = $this->pdf->getY();

            // Draw image
            $imageFile = $imageFilePath.'/Recipes/'.$recipe->getImageFile();
            $this->pdf->myDrawImage($imageFile, $rootX + $padding, $rootY + $padding, $style['layout.image.width'], '', '', '', '', true, 300, '');

            // Draw header
            $this->pdf->setX($rootX);
            $this->pdf->setY($rootY);
            $color = $this->builder->toColorArray($style['header.color']);
            $this->pdf->setTextColor($color[0], $color[1], $color[2]);
            $this->pdf->setFont($this->builder->fontMap[$style['header.font']], '', $style['header.font.size']);

            $this->pdf->StartTransform();
            $this->pdf->translateX($style['layout.ingredients.posX']);
            $this->pdf->translateY($padding);
            $this->pdf->MultiCell(
                $w = $pageWidth,
                $h = $style['layout.height'],
                $txt = $this->builder->translateBlock($recipe->getName()),
                $border = 0,
                $align = 'L',
                $fill = false,
                $ln = 1
            );
            $this->pdf->StopTransform();

            if ($style['layout.kcal.visibility'] !== 'hide') {
                $this->pdf->setX($rootX);
                $this->pdf->setY($rootY);
                $this->pdf->StartTransform();
                $this->pdf->translateY($padding);
                $this->pdf->MultiCell(
                    $w = $pageWidth - $padding,
                    $h = $style['layout.height'],
                    $txt = $style['label.ca'].' '.$caloryAmount.' '.$style['label.kcal'],
                    $border = 0,
                    $align = 'R',
                    $fill = false,
                    $ln = 1
                );
                $this->pdf->StopTransform();
            }

            // Print ingredients
            $arr = explode("\n", $this->builder->translateBlock($recipe->getIngredient()));
            $ingredients = array();
            foreach ($arr as $r) {
                $row = $this->parseIngredient($r, $data['caloryAmount']);
                if (!empty($row)) {
                    $ingredients[] = $row['value'].' '.$row['title'];
                }

            }

            $color = $this->builder->toColorArray($style['text.color']);
            $this->pdf->setTextColor($color[0], $color[1], $color[2]);
            $this->pdf->setFont($this->builder->fontMap[$style['text.font']], '', $style['text.font.size']);
            $this->pdf->setX($rootX);
            $this->pdf->setY($rootY);
            $this->pdf->StartTransform();
            $this->pdf->translateX($style['layout.ingredients.posX']);
            $this->pdf->translateY($style['layout.text.posY']);
            $this->pdf->MultiCell(
                $w = $style['layout.instruction.posX'] - $style['layout.ingredients.posX'] - 10,
                $h = $style['layout.height'],
                $txt = implode("\n", $ingredients),
                $border = 0,
                $align = 'L',
                $fill = false,
                $ln = 1
            );
            $this->pdf->StopTransform();

            // Instruction
            $this->pdf->setX($rootX);
            $this->pdf->setY($rootY);
            $this->pdf->StartTransform();
            $this->pdf->translateX($style['layout.instruction.posX']);
            $this->pdf->translateY($style['layout.text.posY']);
            $this->pdf->MultiCell(
                $w = $pageWidth - $style['layout.instruction.posX'] - $padding,
                $h = $style['layout.height'],
                $txt = $this->builder->translateBlock($recipe->getInstruction())."\n",
                $border = 0,
                $align = 'J',
                $fill = false,
                $ln = 1
            );
            $this->pdf->StopTransform();

            // Smiley
            if ($style['smiley.visible'] == 'YES') {
                $posX = $style['smiley.posX'];
                $posY = $style['smiley.posY'];

                $fontColor = $style['smiley.fontColor'];
                $fontSize = $style['smiley.fontSize'];
                $fontType = $style['smiley.fontType'];
                $textWidth = $style['smiley.textWidth'];

                $style = [];
                if (!empty($fontColor)) {
                    $style['fontColor'] = $fontColor;
                }

                if (!empty($fontSize)) {
                    $style['fontSize'] = $fontSize;
                }

                if (!empty($fontType)) {
                    $style['fontType'] = $fontType;
                }

                if (!empty($textWidth)) {
                    $style['textWidth'] = $textWidth;
                }

                $html = $smileyCalculator->computeToHtml($recipe->getFoodIngredients(), $data['risks'], $style);
                $this->pdf->writeHTMLCell($w=0, $h=0, $x=$posX, $y=$posY, $html, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
            }

            $i++;
        }


        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }

    private function parseIngredient($ingredient, $caloryAmount) {
        $arr = explode('|', $ingredient);
        $i = array(
            'title'   => '',
            'value'  => ''
        );

        if (count($arr) == 2) {
            $i['title'] = trim($arr[1]);

            // Compute amount 1
            if (trim($arr[0]) == '' || trim($arr[0]) == '0') {
                $value = '';
            }
            else {
                $value = floatval($caloryAmount * trim($arr[0]) / 100);
                $value = $this->adaptiveRounding($value);
            }
            $i['value'] = $value;

            return $i;
        }

        return false;
    }

    protected function adaptiveRounding($value) {
        $rounding = null;
        if ($value < 0.01) {
            return 0.01;
        }
        else if ($value < 0.5) {
            // 0-0.5 (to the nearest 0.1)
            $rounding = 0.1;
        } else if ($value >= 0.5 && $value < 4) {
            // 0.5 to 3 (to the nearest 0.5)‏
            $rounding = 0.5;
        } else if ($value >= 4 && $value < 21) {
            // 4 to 20 (to the nearest 1)‏
            $rounding = 1;
        } else if ($value >= 21) {
            // 21+ to the nearest 5‏
            $rounding = 5;
        }

        $result = $value;
        if ($rounding) {
            $result = round($value/$rounding) * $rounding;
        }

        return $result;
    }
}
