<?php
namespace Leep\AdminBundle\Business\Report;

class MyTCPDF extends \Fpdi_Fpdi {
    public $container;
    public $isShowFooter = false;
    public $numSkip = 0;
    public $isSkipFooter = false;
    public $footerData = array();
    public $footerVisibility = array();
    public $currentLayout = 'page';
    public $iccCMYK = null;
    public $iccRGB = null;
    public $footerXPos = 350;
    public $fontMap = array();
    public $lastY = array();

    public $curPageChapter = '!###TEST###!';
    public $curPageChapterColor = '#000000';
    public $numSkipChapter = 0;
    public $toc = array();
    public $tocOptions = array();

    public function getPageUnit() {
        return $this->pdfunit;
    }

    public function getOrientation() {
        return $this->CurOrientation;
    }
    public function setOrientation($o) {
        $this->CurOrientation = $o;
    }

    public function init() {
        $this->isShowFooter = false;
        $this->footerData = array();
    }

    public function setMyFooterData($data) {
        $this->footerData = $data;
        $this->isShowFooter = true;
        $this->startPageGroup();
    }

    public function skipFooter() {
        $this->numSkip++;
    }
    public function skipFromFooter() {
        $this->isSkipFooter = true;
    }
    public function unskipFromFooter() {
        $this->isSkipFooter = false;
    }

    public function AcceptPageBreak() {
        $this->lastY = array();
        if ($this->num_columns > 1) {
            if ($this->currentLayout == '1left_1right' || $this->currentLayout == '1left_1mid_1right' || $this->currentLayout == '4_columns') {
                if ($this->current_column < ($this->num_columns - 1)) {
                    $this->selectColumn($this->current_column + 1);
                } elseif ($this->AutoPageBreak) {
                    $this->AddPage();
                    $this->selectColumn(0);
                }
                return false;
            }
        }
        return $this->AutoPageBreak;
    }

    public function breakFooter() {
        $this->isShowFooter = false;
        $this->startPageGroup();
    }

    public function workaroundMarginTop($pt) {
        $this->setY($this->getY() + $pt);
    }

    public function outputFloatingText($text) {
        $this->writeHTMLCell($w=10000, $h=0, $x='', $y='', $text, $border=0, $ln=0, $fill=0, $reseth=false, 'L', $autopadding=false);
    }

    public function appendFillerToString($string) {
        return $string.'FILLER';
    }

    public function toColorArray($fontColor) {
        $fontColor = trim($fontColor);
        if (strlen($fontColor) < 7) {
            return array(0, 0, 0);
        }
        $r = hexdec(substr($fontColor, 1, 2));
        $g = hexdec(substr($fontColor, 3, 2));
        $b = hexdec(substr($fontColor, 5, 2));
        return array($r, $g, $b);
    }

    public function printPageChapter() {
        if (!isset($this->tocOptions['box.index-width'])) {
            return false;
        }
        if ($this->numSkipChapter > 0) {
            $this->numSkipChapter--;
            return false;
        }

        if ($this->curPageChapter != '') {
            $bgcolor = $this->toColorArray($this->curPageChapterColor);

            $boxWidth = $this->tocOptions['box.index-width'];
            $boxHeight = $this->tocOptions['box.height'];
            $boxSpace = $this->tocOptions['box.space'];
            $flip = isset($this->tocOptions['flip']) ? intval($this->tocOptions['flip']) : 0;

            $posY = $this->tocOptions['position.y'];

            $this->setFillColorArray($bgcolor);

            // Print margin
            $i = 0;
            foreach ($this->toc as $chapter) {
                if ($this->pageNo() % 2 == $flip) {
                    $posX = $this->getPageWidth() - $boxWidth;
                    $mode = 'LTB';
                }
                else {
                    $posX = 0;
                    $mode = 'TRB';
                }

                $isActive = ($chapter['id'] == $this->curPageChapter);

                $this->setAbsX($posX);
                $this->setAbsY($posY + ($boxHeight + $boxSpace) * $i);
                $this->MultiCell(
                    $w = $boxWidth,
                    $h = $boxHeight,
                    $txt = "",
                    $border = array(
                        $mode => array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $bgcolor)
                    ),
                    $align = 'L',
                    $fill = $isActive,
                    $ln = 2,
                    $x = '',
                    $y = '',
                    $reseth = true
                );

                $i++;
            }
        }
    }

    public function Footer() {
        $this->printPageChapter();

        // Print footer
        if ($this->numSkip > 0) {
            $this->numSkip--;
            return;
        }

        if (!$this->isShowFooter) {
            return;
        }

        if ($this->isSkipFooter) {
            return;
        }

        if ($this->footerData['isSimplified']) {
            $prevPageUnit = $this->getPageUnit();
            $this->setPageUnit('mm');

            $posX = isset($this->footerData['posX']) ? intval($this->footerData['posX']) : 10;
            $posY = isset($this->footerData['posY']) ? intval($this->footerData['posY']) : 10;
            $posY = $this->getPageHeight() - $posY;
            $fontType = isset($this->footerData['fontType']) ? trim($this->footerData['fontType']) : 'regular';
            $fontSize = isset($this->footerData['fontSize']) ? intval($this->footerData['fontSize']) : 9;
            $fontColor = isset($this->footerData['fontColor']) ? trim($this->footerData['fontColor']) : '#000000';
            $textPage = isset($this->footerData['textPage']) ? trim($this->footerData['textPage']) : '';
            $textOf = isset($this->footerData['textOf']) ? trim($this->footerData['textOf']) : '/';
            $textFormat = isset($this->footerData['textFormat']) ? trim($this->footerData['textFormat']) : '%page% %number% %of% %total%';

            $textFormat = str_replace('%page%', $textPage, $textFormat);
            $textFormat = str_replace('%number%', '{'.$this->getPageNumGroupAlias().'}', $textFormat);
            $textFormat = str_replace('%of%', $textOf, $textFormat);
            $textFormat = str_replace('%total%', '{'.$this->getPageGroupAlias().'}', $textFormat);

            // Box
            $boxWidth = isset($this->footerData['boxWidth']) ? intval($this->footerData['boxWidth']) : 100;
            $boxHeight = isset($this->footerData['boxHeight']) ? intval($this->footerData['boxHeight']) : 20;
            $boxColor = isset($this->footerData['boxColor']) ? trim($this->footerData['boxColor']) : '';

            // Apply color
            $fontColor = $this->toColorArray($fontColor);
            $this->setTextColor($fontColor[0], $fontColor[1], $fontColor[2]);
            $isFill = false;
            if ($boxColor != '') {
                $boxColor = $this->toColorArray($boxColor);
                $this->setFillColor($boxColor[0], $boxColor[1], $boxColor[2]);
                $isFill = true;
            }

            $this->setFont($this->fontMap[$fontType], '', $fontSize);

            // Draw box
            $this->setAbsX($posX);
            $this->setAbsY($posY);

            $this->cell($boxWidth, $boxHeight, $textFormat, 0, 0, 'C', $isFill, '', 0);
            $this->setPageUnit($prevPageUnit);
        } else {
            $prevPageUnit = $this->getPageUnit();
            $this->setPageUnit('mm');

            $linestyle = array(
                'width' => 1,
                'cap' => 'butt',
                'join' => 'miter',
                'dash' => '',
                'phase' => 0,
                'color' => $this->footerData['lineColorArr']
            );

            $this->SetY(-$this->footerData['marginBottom']/2);

            $curY = $this->getY();
            $margins = $this->getMargins();

            $imageFile = $this->footerData['logo'];

            // Compute text width
            $imageWidthResized = 25;
            $imageSize = @getimagesize($imageFile);
            if ($imageSize[0] == 0) {
                $imageSize[0] = $imageWidthResized;
            }
            $imageHeightResized = $imageWidthResized * $imageSize[1] / $imageSize[0];

            $textWidth = 15;

            // Draw order number
            $x0 = $margins['left'];
            $y0 = $curY - 1;
            $orderNumberWidth = 0;
            if (!empty($this->footerData['orderNumber'])) {
                $this->setX($x0);
                $this->setY($y0);
                $orderNumber = !empty($this->footerData['orderNumber']) ? trim($this->footerData['orderNumber']) : '';
                $textOrderNumber = '<font face="'.$this->fontMap['regular'].'" style="font-size: 9; font-weight: bold; color: '.$this->footerData['lineColor'].'">'.$orderNumber.'</font>';
                $this->writeHTMLCell($w=0, $h=0, $x=$x0, $y=$y0, $textOrderNumber, $border=0, $ln=1, $fill=0);
                $orderNumberWidth = $this->GetStringWidth($orderNumber, $this->fontMap['regular'], 'B', 9, false) + 1;

            }

            // Draw line
            $this->Line(
                $x1=$x0 + $orderNumberWidth,
                $y1=$curY+1,
                $x2=$this->getPageWidth() - $textWidth - $imageWidthResized - 50,
                $y2=$curY+1,
                $linestyle
            );

            $this->SetY($curY);
            $this->SetX($x2);

            // Draw text
            $text = '<font face="'.$this->fontMap['regular'].'" style="font-size: 9; color: '.$this->footerData['lineColor'].'">'.
                $this->footerData['textPage'].' {'.$this->getPageNumGroupAlias().'} '.$this->footerData['textOf'].' {'.$this->getPageGroupAlias().'}'.
            '</font>';
            $this->writeHTMLCell($w=0, $h=0, $x=$x2 + 2, $y=$curY-1, $text, $border=0, $ln=1, $fill=0);
            $this->footerXPos = $x2 + 2;

            // Draw image
            $this->myDrawImage(
                $file = $imageFile,
                $x = 50,
                $y = $curY - $imageHeightResized + 4,
                $w = $imageWidthResized,
                $h = $imageHeightResized,
                $type = '',
                $link = '',
                $align = 'L',
                $resize = true,
                $dpi = 300,
                $palign = 'R'
            );

            $this->setPageUnit($prevPageUnit);
        }
    }

    public function myDrawImage($imageFile, $x, $y, $w, $h, $type, $link, $align = 'R', $resize = true, $dpi = 300, $palign = 'L') {
        /*if (empty($this->iccCMYK)) {
            $iccFile = $this->container->getParameter('iccFileCMYK');
            $this->iccCMYK = file_get_contents($iccFile);
            $iccFile = $this->container->getParameter('iccFileRGB');
            $this->iccRGB = file_get_contents($iccFile);
        }

        $imageLoaded = new \Imagick($imageFile);
        if ($imageLoaded->getImageColorspace() == \Imagick::COLORSPACE_SRGB ||
            $imageLoaded->getImageColorspace() == \Imagick::COLORSPACE_RGB) {

            $profiles = $imageLoaded->getImageProfiles('*', false);
            $hasIccProfile = (array_search('icc', $profiles) !== false);
            if (!$hasIccProfile) {
                $imageLoaded->profileImage('icc', $this->iccRGB);
            }
            $imageLoaded->profileImage('icc', $this->iccCMYK);
            $imageLoaded->setImageColorSpace(\Imagick::COLORSPACE_CMYK);
        }
        else if ($imageLoaded->getImageColorspace() == \Imagick::COLORSPACE_CMYK) {
            $imageLoaded->profileImage('icc', $this->iccCMYK);
        }
        else {
            throw new Exception("Invalid color space, only SRGB/RGB/CMYK accepted");
        }
        $imageLoaded->stripImage();
        */

        $this->Image(
            //'@'.$imageLoaded,
            $imageFile,
            $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign
        );
    }

    public function boxWithShadow($x, $y, $w, $h, $color, $shadowH, $shadowV, $blur, $shadowColor) {
        if (gettype($color) != 'array') {
            $color = $this->toColorArray($color);
        }

        if (gettype($shadowColor) != 'array') {
            $shadowColor = $this->toColorArray($shadowColor);
        }

        $lineWidth = 0.3;
        $lineStyle = [
            'all' => [
                'width' => $lineWidth,
                'color' => $shadowColor
            ]
        ];

        // First draw a shadow
        $lastAlpha = $this->getAlpha();
        $sX = $x + $shadowV + $blur * $lineWidth;
        $sY = $y + $shadowH + $blur * $lineWidth;
        $sW = $w - $blur * $lineWidth;
        $sH = $h - $blur * $lineWidth;
        $this->Rect($sX, $sY, $sW, $sH, 'F', [], $shadowColor);

        $alpha = 1;
        $step = 1/$blur;
        $r = 0;
        for ($i = 0; $i < $blur * 2; $i++) {
            if ($alpha <= 0) {
                break;
            }

            // Draw shadow box
            $this->setAlpha($alpha);
            $this->RoundedRect($sX, $sY, $sW, $sH, $r, '1111', 's', $lineStyle, $shadowColor);

            // Next shadow box
            $r += $lineWidth;
            $sX -= $lineWidth;
            $sY -= $lineWidth;
            $sW += 2 * $lineWidth;
            $sH += 2 * $lineWidth;
            $alpha -= $step;
        }

        // Reset preivous alpha
        $this->SetAlpha($lastAlpha);

        // Draw the rectangle
        $this->Rect($x, $y, $w, $h, 'F', [], $color);
    }
}
