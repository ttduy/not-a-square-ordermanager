<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditSectionHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Report', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {
        $model = new EditSectionModel();
        $model->name = $entity->getName();
        $model->shortName = $entity->getShortName();
        $model->reportCode = $entity->getReportCode();
        $model->idFormulaTemplate = $entity->getIdFormulaTemplate();
        $model->sortOrder = $entity->getSortOrder();
        $model->isVisibleByDefault = $entity->getIsVisibleByDefault();
        $model->isReleased = $entity->getIsReleased();
        $model->idLanguage = 1;
        $model->sectionList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $entity->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $model->sectionList[] = array(
                'id'   => $r->getId(),
                'name' => $r->getName(),
                'alias'  => $r->getAlias(),
                'isHide' => $r->getIsHide(),
                'idType' => $r->getIdType(),
                'idReportSectionLink' => $r->getIdReportSectionLink(),
                'idReportLink' => $r->getIdReportLink()
            );
        }

        // Load products
        $model->products = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from("AppDatabaseMainBundle:ReportProduct", 'p')
            ->andWhere('p.idReport = '.$entity->getId());
        $genes = $query->getQuery()->getResult();
        foreach ($genes as $pLink) {
            $model->products[] = $pLink->getIdProduct();
        }

        // Load special products
        $model->specialProducts = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from("AppDatabaseMainBundle:ReportSpecialProduct", 'p')
            ->andWhere('p.idReport = '.$entity->getId());
        $specialProducts = $query->getQuery()->getResult();
        foreach ($specialProducts as $sp) {
            $model->specialProducts[] = $sp->getIdSpecialProduct();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));

        $builder->add('shortName', 'text', array(
            'attr'     => array(
                'placeholder' => 'Short name'
            ),
            'required' => false
        ));

        $builder->add('reportCode', 'text', array(
            'label'    => 'Report code',
            'attr'     => array(
                'placeholder' => 'Report code'
            ),
            'required' => true
        ));

        $builder->add('idFormulaTemplate', 'choice', array(
            'label'    => 'Formula Template',
            'choices'  => $mapping->getMapping('LeepAdmin_FormulaTemplate_List'),
            'required' => false
        ));

        $builder->add('sortOrder', 'text', array(
            'attr'     => array(
                'placeholder' => 'Sort order'
            ),
            'required' => false
        ));

        $builder->add('products', 'choice', array(
            'label'    => 'Products',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style' => 'height: 150px'
            )
        ));

        $builder->add('specialProducts', 'choice', array(
            'label'    => 'Special products',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_SpecialProduct_List'),
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style' => 'height: 150px'
            )
        ));

        $builder->add('isVisibleByDefault', 'checkbox', array(
            'label'    => 'Is visible to customers by default?',
            'required' => false
        ));

        $builder->add('isReleased', 'checkbox', array(
            'label'    => 'Released for report generation?',
            'required' => false
        ));

        $builder->add('sectionDesign', 'section', array(
            'label'    => 'Report design',
            'property_path' => false
        ));

        $builder->add('sectionList', 'collection', array(
            'type' => new Form\AppReportSection($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Section',
                'required' => false
            )
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'required' => true,
            'attr'     => array(
                'style' => 'min-width: 150px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setName($model->name);
        $this->entity->setShortName($model->shortName);
        $this->entity->setReportCode($model->reportCode);
        $this->entity->setIdFormulaTemplate($model->idFormulaTemplate);
        $this->entity->setSortOrder($model->sortOrder);
        $this->entity->setIsVisibleByDefault($model->isVisibleByDefault);
        $this->entity->setIsReleased($model->isReleased);

        // Update report products
        $filters = array('idReport' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ReportProduct', $filters);
        foreach ($model->products as $idProduct) {
            $reportProduct = new Entity\ReportProduct();
            $reportProduct->setIdReport($this->entity->getId());
            $reportProduct->setIdProduct($idProduct);
            $em->persist($reportProduct);
        }
        $em->flush();

        // Update report special products
        $filters = array('idReport' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ReportSpecialProduct', $filters);
        foreach ($model->specialProducts as $idSpecialProduct) {
            $reportSpecialProduct = new Entity\ReportSpecialProduct();
            $reportSpecialProduct->setIdReport($this->entity->getId());
            $reportSpecialProduct->setIdSpecialProduct($idSpecialProduct);
            $em->persist($reportSpecialProduct);
        }
        $em->flush();

        // Unchange id
        $unchangedIdArr = array();
        foreach ($model->sectionList as $section) {
            $unchangedIdArr[$section['id']] = $section;
        }

        // Update
        $results = $doctrine->getRepository('AppDatabaseMainBundle:ReportSection')->findByIdReport($this->entity->getId());
        foreach ($results as $r) {
            if (isset($unchangedIdArr[$r->getId()])) {
                $section = $unchangedIdArr[$r->getId()];
                $r->setName($section['name']);
                $r->setAlias($section['alias']);
                $r->setIsHide($section['isHide']);
                $r->setSortOrder($section['sortOrder']);
                $r->setIdType($section['idType']);
                $r->setIdReportSectionLink($section['idReportSectionLink']);
                $r->setIdReportLink($section['idReportLink']);
            }
            else {
                $dbHelper->delete($em, 'AppDatabaseMainBundle:ReportItem', array('idReportSection' => $r->getId()));
                $em->remove($r);
            }
        }
        $em->flush();

        // Add new
        foreach ($model->sectionList as $section) {
            if (empty($section['id'])) {
                $newSection = new Entity\ReportSection();
                $newSection->setIdReport($this->entity->getId());
                $newSection->setName($section['name']);
                $newSection->setAlias($section['alias']);
                $newSection->setIsHide($section['isHide']);
                $newSection->setSortOrder($section['sortOrder']);
                $newSection->setIdType($section['idType']);
                $newSection->setIdReportSectionLink($section['idReportSectionLink']);
                $newSection->setIdReportLink($section['idReportLink']);
                $em->persist($newSection);
            }
        }
        $em->flush();

        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}