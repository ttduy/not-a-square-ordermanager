<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetBarChart extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_bar_chart';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('chartDefinition', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '5', 'cols' => 80
            ) 
        ));        
        $builder->add('barDefinition', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '9', 'cols' => 80
            ) 
        ));
        $builder->add('selectedValue', 'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 120px; max-width: 120px') 
        ));
        $builder->add('chartTitle', 'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 320px; max-width: 320px') 
        ));
        $builder->add('chartHeight', 'text', array(
            'required' => false,
            'attr' => array('style' => 'min-width: 120px; max-width: 120px') 
        ));
    }
}
