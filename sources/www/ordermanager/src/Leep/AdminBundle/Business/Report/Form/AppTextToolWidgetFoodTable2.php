<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolWidgetFoodTable2 extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_widget_food_table_2';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('foodDefinition', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '12', 'cols' => 50
            )
        ));

        $builder->add('parameters', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '12', 'cols' => 50
            )
        ));

        $builder->add('warning', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));

        $builder->add('ingredientDisplay', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '6', 'cols' => 50
            )
        ));
    }
}
