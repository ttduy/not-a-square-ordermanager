<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class MarketingHeader extends BaseTextBlock {
    public function build($data) { 
        $toolStyle = array(
            'BoxColor'   => '#173764',
            'TitleColor' => '#FFFFFF',
            'TitleSize'  => '30',
            'TextMarginTop' => '10',
            'TextSize'   => '11',
            'TextWidth'  => '80',
            'CornerWidth'  => '40',
            'CornerHeight' => '20',
            'LogoWidth'  => '20',
            'ImageWidth' => '120',
            'BoxHeight'  => '50'
        );
        $toolStyle = $this->builder->parseInput($toolStyle, $data['style']);

        $maxCurY = 0;

        // Draw background box
        $pageWidth = $this->pdf->getPageWidth();
        $margins = $this->pdf->getMargins();
        $boxHeight = $toolStyle['BoxHeight'];
        $totalWidth = $pageWidth - $margins['left'] - $margins['right'];

        $fillColor = $this->builder->toColorArray($toolStyle['BoxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $margins['left'],
            $y = $margins['top'],
            $w = $totalWidth,
            $h = $boxHeight,
            'F',
            $style,
            $fillColor
        );

        // Draw rotate rectangle
        $whiteBoxWidth = $toolStyle['CornerWidth'];
        $whiteBoxHeight = $toolStyle['CornerHeight'];
        $fillColor = $this->builder->toColorArray('#FFFFFF');
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->StartTransform();
        $this->pdf->Rotate(-4, 
            $pageWidth - $margins['right'] - $whiteBoxWidth, 
            $margins['top']
        );
        $this->pdf->Rect(
            $x = $pageWidth - $margins['right'] - $whiteBoxWidth + 3,
            $y = $margins['top'] - 5,
            $w = $whiteBoxWidth,
            $h = $whiteBoxHeight,
            'F',
            $style,
            $fillColor
        );
        $this->pdf->StopTransform();

        // Draw logo
        $logoWidth = $toolStyle['LogoWidth'];
        $logoFile = $this->builder->resourceDir.$data['logo'];
        $this->pdf->myDrawImage(
            $logoFile, 
            $x = $pageWidth - $margins['right'] - $logoWidth, 
            $y=$margins['top'], 
            $w=$logoWidth, 
            $h=0, 
            $type='', 
            $link='', 
            $align='T', 
            $resized=true, 
            $dpi=300, 
            $palign='', 
            $ismask=false, 
            $imgmask=false, 
            $border=0, 
            $fitbox=false, 
            $hidden=false, 
            $fitonpage=false
        );        
        $y = $this->pdf->getY();

        // Write text
        $imageWidth = $toolStyle['ImageWidth'];
        $imageMarginLeft = $totalWidth - $imageWidth - 5;

        $textWidth = $toolStyle['TextWidth'];
        $textMarginLeft = $imageMarginLeft;
        $textMarginTop = $toolStyle['TextMarginTop'];

        $text1 = $data['text1'];
        $text1 = $this->builder->translateCustomTag($text1);
        $text2 = $data['text2'];
        $text2 = $this->builder->translateCustomTag($text2);

        $text = '<p style="color: #FFFFFF; font-size: '.$toolStyle['TextSize'].'">'.
            '<font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text1, ENT_COMPAT, 'UTF-8')).'</font><br/>'.
            '<font face="'.$this->builder->fontMap['regular'].'">'.nl2br(htmlentities($text2, ENT_COMPAT, 'UTF-8')).'</font>'.
            '</p>';
        $this->pdf->writeHTMLCell(
            $w=$textWidth, 
            $h=0, 
            $x=$margins['left'] + $textMarginLeft, 
            $y=$margins['top'] + $textMarginTop, 
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='L', 
            $autopadding=true
        );

        // Draw image
        if ($data['image'] != '') {
            $imageFile = $this->builder->resourceDir.$data['image'];
            $imageHeightResized = $this->builder->computeImageHeight($imageFile, $imageWidth);
            $this->pdf->myDrawImage(
                $imageFile, 
                $x= $margins['left'] + $imageMarginLeft,
                $y= '', 
                $w= $imageWidth, 
                $h=0, 
                $type='', 
                $link='', 
                $align='T', 
                $resized=true, 
                $dpi=300, 
                $palign='', 
                $ismask=false, 
                $imgmask=false, 
                $border=0, 
                $fitbox=false, 
                $hidden=false, 
                $fitonpage=false
            );        
        }
        else {
            $imageHeightResized = $boxHeight - $imageMarginTop;
        }

        // Write title
        $text = $data['title'];
        $text = $this->builder->translateCustomTag($text);
        $text = '<p style="color: #FFFFFF; font-size: '.$toolStyle['TitleSize'].'"><font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</font></p>';
        $titleWidth = $totalWidth - $imageWidth - 10;
        $titleMarginLeft = 2;
        $height = $this->builder->estimateHeight($titleWidth, $text);
        $titleMarginTop = $toolStyle['BoxHeight'] - $height;
        $this->pdf->writeHTMLCell(
            $w=$titleWidth, 
            $h=0, 
            $x=$margins['left'] + $titleMarginLeft, 
            $y=$margins['top'] + $titleMarginTop, 
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='L', 
            $autopadding=true
        );

        // Set position
        $this->pdf->setX(0);
        $this->pdf->setY($margins['top'] + $imageHeightResized + 20);
    }
}
