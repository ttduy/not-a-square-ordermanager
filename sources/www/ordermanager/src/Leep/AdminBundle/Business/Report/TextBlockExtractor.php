<?php
namespace Leep\AdminBundle\Business\Report;

class TextBlockExtractor {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    private function serializeToText($arr) {
        $text = '';
        foreach ($arr as $a) {
            if (is_array($a)) {
                $text .= ' '.$this->serializeToText($a);
            }
            else {
                $text .= ' '.$a;
            }
        }
        return $text;
    }

    private function getFormulaConcatnated($formulaTemplateList) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.formula')
            ->from('AppDatabaseMainBundle:FormulaTemplate', 'p')
            ->andWhere('p.id in (:formulaTemplateList)')
            ->setParameter('formulaTemplateList', $formulaTemplateList);
        $formulaTemplates = $query->getQuery()->getResult();
        $text = '';
        foreach ($formulaTemplates as $formula) {
            $text .= ' '.$formula['formula'];
        }

        return $text;
    }

    private function trimArray($arr) {
        $a = array();
        foreach ($arr as $v) {
            $a[] = trim($v);
        }
        if (empty($a)) { $a = array(''); }
        return $a;
    }

    private function getFoodItemConcatnated($foodItems) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.name')
            ->from('AppDatabaseMainBundle:FoodTableFoodItem', 'p')
            ->andWhere('p.foodId in (:foodItems)')
            ->setParameter('foodItems', $this->trimArray($foodItems));
        $foodItems = $query->getQuery()->getResult();
        $text = '';
        foreach ($foodItems as $item) {
            $text .= ' '.$item['name'];
        }
        return $text;
    }

    private function getIngredientConcatnated($ingredients) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.name')
            ->from('AppDatabaseMainBundle:FoodTableIngredient', 'p')
            ->andWhere('p.variableName in (:ingredients)')
            ->setParameter('ingredients', $this->trimArray($ingredients));
        $ingredients = $query->getQuery()->getResult();
        $text = '';
        foreach ($ingredients as $ingredient) {
            $text .= ' '.$ingredient['name'];
        }
        return $text;
    }

    public function extractTextBlockFromReport($idReport) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Load report items
        $query = $em->createQueryBuilder();
        $query->select('p.idTextToolType, p.data, s.idReportLink')
            ->from('AppDatabaseMainBundle:ReportItem', 'p')
            ->innerJoin('AppDatabaseMainBundle:ReportSection', 's', 'WITH', '(p.idReportSection = s.id) OR (p.idReportSection = s.idReportSectionLink)')
            ->andWhere('s.idReport = :idReport')
            ->setParameter('idReport', $idReport);
        $result = $query->getQuery()->getResult();

        // Search text blocks
        $textBlocks = array();
        $pattern = '/\[\[(.*?)\]\]/ism';
        foreach ($result as $r) {
            $arr = json_decode($r['data'], true);
            $text = $this->serializeToText($arr);

            // Load more
            switch ($r['idTextToolType']) {
                case Constants::TEXT_TOOL_TYPE_UTIL_GRAB_TEXT_BLOCK:
                    $formulaTemplateList = $arr['formulaTemplates'];
                    if (empty($formulaTemplateList)) { $formulaTemplateList = array(0); }
                    $text .= ' '.$this->getFormulaConcatnated($formulaTemplateList);

                    break;
                case Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE:
                    $foodItems = explode("\n", $arr['foodDefinition']);
                    $text .= ' '.$this->getFoodItemConcatnated($foodItems);

                    $ingredients = explode("\n", $arr['ingredientDisplay']);
                    $text .= ' '.$this->getIngredientConcatnated($ingredients);
                    break;
                case Constants::TEXT_TOOL_TYPE_WIDGET_RECIPE:
                    $text .= ' '.$this->getRecipesConcatnated($arr);
                    break;
            }

            // Search text blocks
            $r = preg_match_all($pattern, $text, $matches);
            if (!empty($r)) {
                foreach ($matches[1] as $match) {
                    $textBlocks[trim($match)] = 0;
                }
            }
        }

        // Scan report link
        $sections = $em->getRepository('AppDatabaseMainBundle:ReportSection')->findByIdReport($idReport);
        foreach ($sections as $section) {
            if (!$section->getIdReportLink()) {
                continue;
            }

            $childReportId = $section->getIdReportLink();
            $textBlocks = array_merge($textBlocks, $this->extractTextBlockFromReport($childReportId));
        }

        $textBlockKeys = array_keys($textBlocks);
        if (empty($textBlockKeys)) { $textBlockKeys = array(''); }
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->andWhere('p.name IN (:textBlockKeys)')
            ->setParameter('textBlockKeys', $textBlockKeys);
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $textBlocks[$r->getName()] = $r->getId();
        }

        return $textBlocks;
    }

    public function getRecipesConcatnated($arr) {
        $recipesIds = array("");
        $recipeRows = explode("\n", $arr['recipeId']);
        foreach ($recipeRows as $r) {
            $a = explode("|", $r);
            if (isset($a[0])) {
                $recipeId = trim($a[0]);
                $recipesIds[] = $recipeId;
            }
        }

        $text = '';
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Recipe', 'p')
            ->andWhere('p.recipeId in (:recipesIds)')
            ->setParameter('recipesIds', $recipesIds);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $text.= ' '.$r->getName().' '.$r->getIngredient().' '.$r->getInstruction().' ';
        }
        return $text;
    }
}