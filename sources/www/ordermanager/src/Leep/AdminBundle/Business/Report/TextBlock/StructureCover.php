<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class StructureCover {
    public $container;
    public $pdf;
    public $builder;

    public function __construct($container, $pdf, $builder) {
        $this->container = $container;
        $this->pdf = $pdf;
        $this->builder = $builder;
    }

    public function build($data) {  
        $imageFilePath = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';

        $imageFile = $imageFilePath.$data['coverImage'];

        $numPage = intval($data['numPage']);
        if ($this->pdf->getNumPages() > 1) {
            $numPage = $this->pdf->getNumPages();
        }
        if ($this->builder->totalNumPage != 0) {
            $numPage = $this->builder->totalNumPage;
        }

        $this->pdf->deletePage($this->pdf->getPage());  
        $extraWidth = $numPage * 0.06;   
        $this->pdf->addPage('L', 'A4_NOVO_COVER_'.$extraWidth);
        $this->pdf->setPageUnit('mm');

        $pageWidth = $this->pdf->getPageWidth();
        $pageHeight = $this->pdf->getPageHeight();

        $resizedHeight = $pageHeight;
        $resizedWidth = $this->builder->computeImageWidth($imageFile, $pageHeight);

        $diffLeft = $pageWidth - $resizedWidth;

        $bMargin = $this->pdf->getBreakMargin();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->myDrawImage($imageFile, $diffLeft, 0, $resizedWidth, $resizedHeight, '', '', '', true, 300, '');
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setPageMark();
    }
}
