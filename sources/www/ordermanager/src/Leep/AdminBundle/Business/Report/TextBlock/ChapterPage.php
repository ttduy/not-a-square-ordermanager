<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class ChapterPage extends BaseTextBlock {
    public function build($data) {
        $style = array();
        $style = $this->parseInput($style, $data['options']);

        $width = $this->pdf->getPageWidth();
        $height = $this->pdf->getPageHeight();
        $bMargin = $this->pdf->getBreakMargin();
        $margins = $this->pdf->getMargins();
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $pageWidth = $this->pdf->getPageWidth() - $margins['left'] - $margins['right'];
        $pageHeight = $this->pdf->getPageHeight() - $bMargin;
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setMargins(0, 0, 0);

        /////////////////////////////////////
        // x. Draw gradient background
        $colorTop = $this->builder->toColorArray($style['bgcolor.gradient-top']);
        $colorBottom = $this->builder->toColorArray($style['bgcolor.gradient-bottom']);

        $coords = array(0.5, 1, 0.5, 0);

        $this->pdf->linearGradient(0, 0, $width, $height, $colorTop, $colorBottom, $coords);

        // Draw ring patterns
        Utils::renderRingPattern($this->container, $this->builder, $this->pdf, $style);
        Utils::renderRingPattern($this->container, $this->builder, $this->pdf, $style, 'small-');

        // Render chapter
        $boxHeight = $this->builder->tocOptions['box.height'];
        $boxWidth = $this->builder->tocOptions['box.width'];
        $boxActiveWidth = $this->builder->tocOptions['box.active-width'];
        $boxSpace = $this->builder->tocOptions['box.space'];
        $posY = $this->builder->tocOptions['position.y'];


        $style = $this->builder->tocOptions;
        $style = $this->parseInput($style, $data['options']);
        $i = 0;
        foreach ($this->builder->toc as $chapter) {
            // Load style
            if ($chapter['id'] == $data['chapterId']) {
                $code = 'box-active';
                $width = $boxActiveWidth;
            }
            else if ($chapter['available']) {
                $code = 'box-available';
                $width = $boxWidth;
            }
            else {
                $code = 'box-unavailable';
                $width = $boxWidth;
            }

            $bordercolor = $this->builder->toColorArray($style[$code.'.border.color']);
            $bgcolor = $this->builder->toColorArray($style[$code.'.background.color']);
            $textColor = $this->builder->toColorArray($style[$code.'.text.color']);
            $textPosX = $style[$code.'.text.position.x'];
            $textPosY = $style[$code.'.text.position.y'];
            $textFont = $style[$code.'.text.font'];
            $textSize = $style[$code.'.text.size'];
            $subtextPosY = $style[$code.'.subtext.position.y'];
            $subtextFont = $style[$code.'.subtext.font'];
            $subtextSize = $style[$code.'.subtext.size'];

            // Draw box
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->startTransform();
            $this->pdf->translateX($this->pdf->getPageWidth() - $width);
            $this->pdf->translateY($posY + ($boxHeight + $boxSpace) * $i);
            $this->pdf->setFillColorArray($bgcolor);
            $this->pdf->MultiCell(
                $w = $width,
                $h = $boxHeight,
                $txt = "",
                $border = array(
                    'LTRB' => array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $bordercolor)
                ),
                $align = 'L',
                $fill = true,
                $ln = 2,
                $x = '',
                $y = '',
                $reseth = true
            );
            $this->pdf->stopTransform();

            // Draw text
            $this->pdf->setFont($this->builder->fontMap[$textFont], '', $textSize, '', true);
            $this->pdf->setTextColorArray($textColor);
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->startTransform();
            $this->pdf->translateX($this->pdf->getPageWidth() - $boxWidth + $textPosX);
            $this->pdf->translateY($posY + ($boxHeight + $boxSpace) * $i + $textPosY);
            $this->pdf->cell(
                $w = $boxWidth,
                $h = 0,
                $txt = $chapter['name'],
                $border = 0,
                $ln = 0,
                $align = 'L'
            );
            $this->pdf->stopTransform();


            // Draw subtext
            $this->pdf->setFont($this->builder->fontMap[$subtextFont], '', $subtextSize, '', true);
            $this->pdf->setTextColorArray($textColor);
            $this->pdf->setX(0);
            $this->pdf->setY(0);
            $this->pdf->startTransform();
            $this->pdf->translateX($this->pdf->getPageWidth() - $boxWidth + $textPosX);
            $this->pdf->translateY($posY + ($boxHeight + $boxSpace) * $i + $subtextPosY);
            $this->pdf->cell(
                $w = $boxWidth,
                $h = 0,
                $txt = $chapter['subtext'],
                $border = 0,
                $ln = 0,
                $align = 'L'
            );
            $this->pdf->stopTransform();

            $i++;
        }

        // Close page
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setMargins($margins['left'], $margins['top'], $margins['right']);
        $this->pdf->setPageMark();

        // Set to page
        $this->pdf->curPageChapter = $data['chapterId'];
        $this->pdf->curPageChapterColor = $data['chapterColor'];
        $this->pdf->numSkipChapter++;
    }
}
