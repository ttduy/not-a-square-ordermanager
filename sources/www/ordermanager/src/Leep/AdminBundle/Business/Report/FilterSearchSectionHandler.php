<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterSearchSectionHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterSearchSectionModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name section',
            'required' => false
        ));

        return $builder;
    }
}
