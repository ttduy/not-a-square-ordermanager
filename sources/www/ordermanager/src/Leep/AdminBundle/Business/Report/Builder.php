<?php
namespace Leep\AdminBundle\Business\Report;

use Leep\AdminBundle\Business;

class Builder {
    const STD_SMILEY_WIDTH = 20;
    const FULL_PERCENT = 100;

    public static $PDF_MARGIN_HEADER = 5;
    public static $PDF_MARGIN_FOOTER = 0;
    public static $PDF_MARGIN_TOP    = 0;
    public static $PDF_MARGIN_BOTTOM = 0;
    public static $PDF_MARGIN_LEFT   = 10;
    public static $PDF_MARGIN_RIGHT  = 10;
    public static $PAGE_ORIENTATION  = '';

    public $fontMap = array();
    public function getFontMap() {
        return $this->fontMap;
    }
    // Data

    public $container;
    public $localizator;
    public $recipeBookLayout;
    public $idLanguage = 0;
    public $pdf;
    public $resourceDir;
    public $blocks;
    public $variables = array();
    public $env;
    public $configData = array();
    public $cache = null;
    public $lastTextTool = array(
        'idTextToolType' => 0,
        'data' => array()
    );
    public $currentFoodTablePosition = 0;
    public $mapping;
    public $bodBarcodeBook = null;
    public $bodBarcodeCover = null;
    public $bodBarcodeOrderNumber = null;
    public $totalNumPage = 0;
    public $mode = 'preview';
    public $isDisabledTextBlock = false;
    public $toc = array();
    public $tocOptions = array();

    public function __construct($container) {
        $this->container = $container;
    }

    public function workaroundMarginTop($title, $pt) {
        $params = \TCPDF_STATIC::serializeTCPDFtagParameters(array($pt));
        return '<tcpdf method="workaroundMarginTop" params="'.$params.'"/>'.$title;
    }

    public function outputFloatingText($text) {
        $params = \TCPDF_STATIC::serializeTCPDFtagParameters(array($text));
        return '<tcpdf method="outputFloatingText" params="'.$params.'"/>';
    }

    public function getEffectiveWidth() {
        return $this->pdf->getPageWidth() - self::$PDF_MARGIN_LEFT - self::$PDF_MARGIN_RIGHT;
    }

    public function init() {
        $this->resourceDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource';
        $rootDir = $this->container->getParameter('kernel.root_dir');

        $this->mapping = $this->container->get('easy_mapping');

        $this->pdf = $this->container->get('white_october.tcpdf')->create();
        $this->pdf->container = $this->container;
        $this->pdf->setFontSubsetting(false);
        $this->pdf->init();
        $this->pdf->breakFooter();

        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetHeaderMargin(0);

        $this->pdf->SetMargins(self::$PDF_MARGIN_LEFT, self::$PDF_MARGIN_TOP, self::$PDF_MARGIN_RIGHT, true);
        //die(self::$PDF_MARGIN_LEFT.' '.self::$PDF_MARGIN_TOP.' '.self::$PDF_MARGIN_RIGHT);
        $this->pdf->SetFooterMargin(self::$PDF_MARGIN_FOOTER);
        $this->pdf->SetAutoPageBreak(TRUE, self::$PDF_MARGIN_BOTTOM + self::$PDF_MARGIN_FOOTER);

        $this->pdf->setCellPadding(0);
        $this->pdf->setSRGBmode(true);

        $this->env = new \Twig_Environment(new \Twig_Loader_String(), array('autoescape' => false));
        $this->cache = $this->container->get('text_block_cache');

        $this->localizator = $this->container->get('leep_admin.report.business.localizator');
        $this->recipeBookLayout = $this->container->get('leep_admin.recipe_book_layout.business.recipe_book_layout_helper');
    }

    public function addSpecialData() {
        $this->configData['billEntries'] = array(
            array('orderNumber' => 'NA00000', 'orderDate' => '01/01/2011', 'product' => 'Product 1', 'name' => 'Demo user 1', 'amount' => '€100.00', 'actualAmount' => '€100.00', 'taxPercentage' => '7.00%', 'taxAmount' => '€7.00', 'amountWithTax' => '€107.00'),
            array('orderNumber' => 'NA00001', 'orderDate' => '02/02/2012', 'product' => 'Product 2', 'name' => 'Demo user 2', 'amount' => '€200.00', 'actualAmount' => '€200.00', 'taxPercentage' => '10.00%', 'taxAmount' => '€20.00', 'amountWithTax' => '€220.00'),
            array('orderNumber' => '(orderNumber)', 'orderDate' => '(orderDate)', 'product' => '(product)', 'name' => '(name)', 'amount' => '(amount)', 'actualAmount' => '(actual amount)', 'taxPercentage' => '(Tax percentage)', 'taxAmount' => '(Tax Amount)', 'amountWithTax' => '(Amount with tax)'),
        );

        $this->configData['noTaxText']['textGermany'] = 'No tax text in Germany!';
        $this->configData['noTaxText']['textEnglish'] = 'No tax text in English!';
    }

    public function begin() {
        $this->pdf->AddPage();
    }

    public function loadLocalizator($idLanguage) {
        $localization = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLocalization')->findOneByIdLanguage($idLanguage);
        if ($localization) {
            $this->localizator->setLocalization($localization->getLocalization());
        }
    }

    public function loadRecipeBookLayout() {
        $bookLayouts = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:RecipeBookLayout')->findAll();
        if (is_array($bookLayouts)) {
            foreach ($bookLayouts as $layout) {
                $this->recipeBookLayout->addLayout($layout->getName(), $layout->getData());
            }
        }
    }

    public function applyLanguage($idLanguage) {
        $this->loadBlock($idLanguage);
        $this->loadFontMap($idLanguage);

        $this->loadLocalizator($idLanguage);
        $this->loadRecipeBookLayout();

        $this->pdf->fontMap = $this->fontMap;
        if ($idLanguage == 29) { // Hack for Chinese
            $this->pdf->is_hack_justify = true;
        }
    }

    public function loadFontMap($idLanguage) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $fontLanguage = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ReportLanguageFont')->findOneByIdLanguage($idLanguage);
        if (empty($fontLanguage)) {
            die("Please set font language for this language");
        }

        $this->fontMap = array();
        $this->fontMap['regular'] = $formatter->format($fontLanguage->getIdRegular(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['regular_italic'] = $formatter->format($fontLanguage->getIdRegularItalic(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['bold'] = $formatter->format($fontLanguage->getIdBold(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['light'] = $formatter->format($fontLanguage->getIdLight(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['light_italic'] = $formatter->format($fontLanguage->getIdLightItalic(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['medium'] = $formatter->format($fontLanguage->getIdMedium(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['thin'] = $formatter->format($fontLanguage->getIdThin(), 'mapping', 'LeepAdmin_ApplicationFont_Code');
        $this->fontMap['palatino'] = $formatter->format($fontLanguage->getIdSpecial(), 'mapping', 'LeepAdmin_ApplicationFont_Code');

        foreach (array('regular', 'regular_italic', 'bold', 'light', 'light_italic', 'medium', 'thin', 'palatino') as $fontType) {
            if ($this->fontMap[$fontType] == '') {
                die("Missing some font type for this language, please fill all font style");
            }
        }

        $this->pdf->setFont($this->fontMap['regular'], '', 11, '', true);
    }

    public function loadBlock($idLanguage) {
        $this->idLanguage = $idLanguage;
        $this->cache->setLanguage($idLanguage);
        /*
        // Load all blocks??
        $this->blocks = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('t.name, l.body')
            ->from('AppDatabaseMainBundle:TextBlock', 't')
            ->innerJoin('AppDatabaseMainBundle:TextBlockLanguage', 'l', 'WITH', 't.id = l.idTextBlock')
            ->andWhere('l.idLanguage = :idLanguage')
            ->setParameter('idLanguage', $idLanguage);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $this->blocks[$r['name']] = $r['body'];
        }
        */
    }

    public function computeImageHeight($imageFile, $imageWidth) {
        @list($width, $height, $type, $attr) = getimagesize($imageFile);
        if (empty($width)) { $width = 1; }
        $resizedHeight = $imageWidth  * $height / $width;
        return $resizedHeight;
    }

    public function computeImageWidth($imageFile, $imageHeight) {
        @list($width, $height, $type, $attr) = getimagesize($imageFile);
        if (empty($height)) { $height = 1; }
        $resizedWidth = $imageHeight  * $width / $height;
        return $resizedWidth;
    }

    public function parseConfigData($configData) {
        $results = explode("\n", $configData);
        foreach ($results as $l) {
            $l = trim($l);
            if (!empty($l)) {
                $t = explode('=', $l);
                if (isset($t[0]) && isset($t[1])) {
                    $p = strpos($l, '=');
                    $v = substr($l, $p+1);
                    $this->configData[trim($t[0])] = trim($v);
                }
            }
        }

        // Parse array
        foreach ($this->configData as $k => $v) {
            $v = trim($v);
            if (strlen($v) >= 2) {
                if ($v[0] == '[' && $v[strlen($v)-1] == ']') {
                    if ($v[1] == '[' && $v[strlen($v)-2] == ']') {

                    }
                    else {
                        $arr = explode(',', substr($v, 1, strlen($v)-2));
                        $v = array();
                        foreach ($arr as $a) {
                            $v[] = trim($a);
                        }
                        $this->configData[$k] = $v;
                    }
                }
            }
        }

        // Check
        if (isset($this->configData['IS_DISABLED_TEXT_BLOCK']) && ($this->configData['IS_DISABLED_TEXT_BLOCK'] == 'DISABLED')) {
            $this->isDisabledTextBlock = true;
        }

        // Add some special variables
        $this->addSpecialData();
    }

    public function parseInput($arr, $input) {
        $rows = explode("\n", $input);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $arr[trim($k)] = trim($v);
            }
        }
        return $arr;
    }

    public function moveNextPage() {
        //$this->pdf->SetPrintFooter($this->pdf->isShowFooter);
        $this->pdf->startPage();
    }

    public function endPage() {
        $this->pdf->endPage();
    }

    public function addTextTool($idTextToolType, $data) {
        $ctrlMapping = $this->mapping->getMapping('LeepAdmin_TextTool_CtrlMapping');
        $map = $ctrlMapping[$idTextToolType];
        $method = 'add'.ucfirst($map);

        foreach ($data as $k => $v) {
            if (is_string($v)) {
                $data[$k] = $this->translateBlock($v);
            }
        }

        if ($idTextToolType == Constants::TEXT_TOOL_TYPE_UTIL_TOC_ENTRY) {
            $this->$method($data);
        } else {
            // Switch landscape --> portrait
            if ($this->lastTextTool['idTextToolType'] == Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE &&
                $idTextToolType != Constants::TEXT_TOOL_TYPE_WIDGET_FOOD_TABLE) {
                // Force to create new page
                $this->pdf->deletePage($this->pdf->getPage());

                // Create new page
                $this->pdf->addPage('P');
                $this->pdf->setPageUnit('mm');
            }


            // Visibility
            $visibility = isset($data['visibility']) ? $data['visibility'] : '';
            $visibility = $this->translateBlock($visibility);
            if (trim($visibility) == 'Hide') {
            }
            else {
                $this->$method($data);
            }

            $this->lastTextTool = array(
                'idTextToolType' => $idTextToolType,
                'data'           => $data
            );
        }
    }

    public function estimateHeight($width, $html) {
        $this->pdf->startTransaction();
        $this->pdf->writeHTMLCell($w=$width, $h=0,
            $x= 0,
            $y= 0,
            $html, $border=0, $ln=2, $fill=0, $reseth=true, $alignment='R', $autopadding=true);
        $height = $this->pdf->getY();
        $this->pdf->rollbackTransaction(true);

        return $height;
    }

    public function estimateCurrentWidth() {
        // Estimate current width
        $this->pdf->startTransaction();
        $curX = $this->pdf->getX();
        $this->pdf->writeHTMLCell(0, 0, 0, 0, '<b></b>');
        $width = round($this->pdf->getX() - $curX);
        $this->pdf->rollbackTransaction(true);

        return $width;
    }

    public function addToolStructureNewCoverNovogenia($data) {
        (new TextBlock\StructureNewCoverNovogenia($this->container, $this->pdf, $this, $data))->process();
    }

    public function addToolStructureStandardCover($data) {
        (new TextBlock\StructureStandardCover($this->container, $this->pdf, $this, $data))->process();
    }

    public function addToolStructureChangeFormat($data) {
        $changePageFormat = new TextBlock\StructureChangeFormat($this->container, $this->pdf, $this);
        $changePageFormat->build($data);
    }

    // HELPER
    public function toColorArray($fontColor) {
        $fontColor = trim($fontColor);
        if (strlen($fontColor) < 7) {
            return array(0, 0, 0);
        }
        $r = hexdec(substr($fontColor, 1, 2));
        $g = hexdec(substr($fontColor, 3, 2));
        $b = hexdec(substr($fontColor, 5, 2));
        return array($r, $g, $b);
    }

    public function translateAlignment($alignment) {
        $map = array(
            'left'    => 'L',
            'right'   => 'R',
            'center'  => 'C',
            'justify' => 'J'
        );
        return isset($map[$alignment]) ? $map[$alignment] : 'L';
    }

    public function getAlignment($data) {
        $alignment = 'L';
        if (!isset($data['alignment'])) {
            return $alignment;
        }

        switch ($data['alignment']) {
            case 'left': $alignment = 'L'; break;
            case 'right': $alignment = 'R'; break;
            case 'center': $alignment = 'C'; break;
            case 'justify': $alignment = 'J'; break;
        }
        return $alignment;
    }

    public function getFontId($data) {
        if (isset($data['effects'])) {
            if (is_array($data['effects'])) {
                $data['effects'] = array_pop($data['effects']);
            }

            return $data['effects'];
        }
        return 'regular';
    }

    public function getFontType($data) {
        $fontId = $this->getFontId($data);
        $fontMap = $this->getFontMap();
        if (isset($fontMap[$fontId])) {
            return $fontMap[$fontId];
        }
        return $this->fontMap['regular'];
    }

    public function applyFont($data) {
        $fontSize = isset($data['fontSize']) ? $data['fontSize'] : 11;

        $this->pdf->SetFont($this->fontMap['regular'], '', $fontSize);

        $fontType = $this->getFontType($data);
        $this->pdf->setFont($fontType, '', $fontSize);
    }

    public function translateBlock($textBlock) {
        $textBlock = $this->env->render($textBlock, $this->configData);

        // Text block
        if (!$this->isDisabledTextBlock) {
            $matches = array();
            $pattern = '/\[\[(.*?)\]\]/ism';
            $r = preg_match_all($pattern, $textBlock, $matches);
            if (!empty($r)) {
                foreach ($matches[0] as $match) {
                    $name = trim(substr($match, 2, strlen($match) - 4));

                    $newBlock = '***'.$name.'***';
                    if ($this->cache->contains($name)) {
                        $newBlock = $this->cache->getBlock($name);
                        $newBlock = trim($newBlock, " \n");
                    }

                    $textBlock = str_replace($match, $newBlock, $textBlock);
                }
            }
        }

        // Variable
        $matches = array();
        $pattern = '/\$\$(.*?)\$\$/ism';
        $r = preg_match_all($pattern, $textBlock, $matches);
        if (!empty($r)) {
            foreach ($matches[0] as $match) {
                $name = trim(substr($match, 2, strlen($match) - 4));

                if (isset($this->variables[$name])) {
                    $textBlock = str_replace($match, $this->variables[$name], $textBlock);
                }
                else {
                    // Metric
                    if (strpos($name, '|') !== FALSE) {
                        $metricTranslated = $this->localizator->translate($name);
                        $textBlock = str_replace($match, $metricTranslated, $textBlock);
                    }
                    else {
                        $textBlock = str_replace($match, '$*$', $textBlock);
                    }
                }
            }
        }

        return $textBlock;
    }

    public function translateCustomTag($text) {
        $text = str_replace('##BR##', "\n", $text);
        return $text;
    }

    // IMAGES
    public function addToolImageAcrossPage($data) {
        $imageFile = $this->resourceDir.$data['image'];
        @list($width, $height, $type, $attr) = getimagesize($imageFile);
        if (empty($width)) { $width = 1; }
        $resizedWidth = $this->pdf->getPageWidth() - (self::$PDF_MARGIN_LEFT + self::$PDF_MARGIN_RIGHT);
        $resizedHeight = $resizedWidth  * $height / $width;
        $x = self::$PDF_MARGIN_LEFT;

        $this->pdf->myDrawImage($imageFile, $x, $y=self::$PDF_MARGIN_TOP, $w=$resizedWidth, $h=$resizedHeight, $type='', $link='', $align='T', $resized=true, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false);
        $y = $this->pdf->getY();

        $this->pdf->setY($y + $resizedHeight);
    }

    public function addToolImageTitlePageLogo($data) {
        $imageFile = $this->resourceDir.$data['image'];
        $logoFile  = $this->resourceDir.$data['logo'];
        if (isset($data['overrideLogo']) && trim($data['overrideLogo']) != '') {
            if (is_file($this->resourceDir.$data['overrideLogo'])) {
                $logoFile = $this->resourceDir.$data['overrideLogo'];
            }
        }

        $imageSize = getimagesize($imageFile);
        $logoSize  = getimagesize($logoFile);

        $imageWidthResized = $this->pdf->getPageWidth() * 2 / 3;
        $logoWidthResized  = $this->pdf->getPageWidth() / 3;
        $boxWidthResized = $this->pdf->getPageWidth() / 3 - self::$PDF_MARGIN_RIGHT*2;

        $imageHeightResized = $imageWidthResized * $imageSize[1] / $imageSize[0];
        $logoHeightResized = $logoWidthResized * $logoSize[1] / $logoSize[0];
        $boxHeightResized = $imageHeightResized;

        $calculatedY = $this->pdf->getPageHeight() - $imageHeightResized - $logoHeightResized - self::$PDF_MARGIN_FOOTER - self::$PDF_MARGIN_BOTTOM ;

        $this->pdf->myDrawImage(
            $file = $logoFile,
            $x = '',
            $y = $calculatedY - 2,
            $w = $logoWidthResized,
            $h = $logoHeightResized,
            $type = '',
            $link = '',
            $align = 'R',
            $resize = true,
            $dpi = 300,
            $palign = 'R'
        );

        $this->pdf->myDrawImage(
            $file = $imageFile,
            $x = '',
            $y = $calculatedY + $logoHeightResized,
            $w = $imageWidthResized,
            $h = $imageHeightResized,
            $type = '',
            $link = '',
            $align = 'R',
            $resize = true,
            $dpi = 300,
            $palign = 'L'
        );

        $fillColor = $this->toColorArray($data['boxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->Rect(
            $x = $imageWidthResized + self::$PDF_MARGIN_RIGHT,
            $y = $calculatedY + $logoHeightResized,
            $w = $boxWidthResized,
            $h = $boxHeightResized,
            'F',
            $style,
            $fillColor
        );

    }

    public function addToolImageHeading($data) {
        $tool = new TextBlock\ImageHeading($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    // TEXT
    public function addToolTextBarcode($data) {
        $alignment = $this->getAlignment($data);
        $style = array(
            'position' => $alignment,
            'align' => $alignment,
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'vpadding' => 'auto',
            'fgcolor' => $this->toColorArray($data['fontColor']),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'pdfahelvetica',
            'fontsize' => 10,
            'stretchtext' => 2,
        );
        $height = isset($data['fontSize']) ? intval($data['fontSize']) : 9;
        $idCodeType = isset($data['idCodeType']) ? $data['idCodeType'] : '';
        if ($idCodeType == 'qr_code') {
            $fromEncoding = mb_detect_encoding($data['textBlock']);
            $text = mb_convert_encoding($data['textBlock'], "ISO-8859-1", $fromEncoding);
            $this->pdf->write2DBarcode($text, 'QRCODE', '', '', '', $height, $style, $alignment, false);
        }
        else { // bar code
            $this->pdf->write1DBarcode($data['textBlock'], 'C39', '', '', '', 9, 0.4, $style, $alignment);
        }
    }

    public function addToolTextPlain($data) {
        $alignment = $this->getAlignment($data);
        $this->applyFont($data);
        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];

        /*$this->pdf->AddSpotColor('quantest', 0, 42, 72, 0);
        $this->pdf->AddSpotColor('quantest2', 0, 100, 100, 20);
        $this->pdf->AddSpotColor('quantest3', 0, 100, 0, 40);*/

        $text = $data['textBlock'];
        $text = trim($text);
        if (trim($text) != '') {
            $text = $this->translateCustomTag($text);
            $text = '<p style="color: '.$data['fontColor'].'; font-size: '.$fontSize.';" >'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</p>';

            //$this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $text, $border=1, $ln=1, $fill=0, $reseth=true, $alignment, $autopadding=true);
            $this->pdf->writeHTML($text, $ln=true, $fill=false, $reseth=true, $cell=false, $alignment);
        }
    }

    public function addToolTextHtml($data) {
        $textBlock = $data['textBlock'];
        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $textBlock, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);
    }

    public function addToolTextList($data) {
        $lines = explode("\n", $data['textBlock']);

        $bullet = $this->pdf->unichr(228);

        $fontType = $this->getFontType($data);
        $bulletColor = isset($data['bulletColor']) ? $data['bulletColor'] : '#690059';

        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];
        $html = '<ul style="font-size: '.$fontSize.'; list-style-type: none">';
        foreach ($lines as $line) {
            if (trim($line) != '') {
                $html .= '<li style="color: '.$data['fontColor'].'; "><font face="pdfazapfdingbats" style="color: '.$bulletColor.'">'.$bullet.'</font>&nbsp;&nbsp;<font face="'.$fontType.'">'.$line.'</font></li>';
            }
        }
        $html .= '</ul>';
        $this->pdf->setFont($this->fontMap['regular'], '', 4, '', true);
        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);
        //die();
    }

    public function addToolTextColorTextbox($data) {
        $alignment = $this->getAlignment($data);
        $this->applyFont($data);
        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];
        $boxColor = empty($data['boxColor']) ? '#000000' : $data['boxColor'];
        $boxBackgroundColor = empty($data['boxBackgroundColor']) ? '#FFFFFF' : $data['boxBackgroundColor'];
        $boxSize = empty($data['boxSize']) ? 10 : $data['boxSize'];

        $text = $data['textBlock'];
        //$text = '<div style="height: 20px; color: '.$data['fontColor'].'; font-size: '.$fontSize.'; border: 1px solid '.$boxColor.';  background-color: '.$boxBackgroundColor.'; text-align: center;">&nbsp;'.$text.'&nbsp;</div>';
        $alignmentText = 'center';
        if ($data['alignment'] == 'left') {
            $alignmentText = 'left';
            $text = '<font style="color: '.$boxBackgroundColor.'">)</font>'.$text;
        }
        else if ($data['alignment'] == 'right') {
            $alignmentText = 'right';
            $text = $text.'<font style="color: '.$boxBackgroundColor.'">(</font>';
        }

        $text = '<table><tr><td align="'.$alignmentText.'" style="color: '.$data['fontColor'].'; font-size: '.$fontSize.'; border: 1px solid '.$boxColor.';  background-color: '.$boxBackgroundColor.';">'.$text.'</td></tr></table>';
        $this->pdf->writeHTMLCell($w=$boxSize, $h=10, $x='', $y='', $text, $border=0, $ln=1, $fill=0, $reseth=true, $alignment, $autopadding=false);
    }

    // Structure
    public function addToolStructureHorizontalLine($data) {
        $fontColor = !empty($data['fontColor']) ? $data['fontColor'] : '#000000';
        $lineHeight = empty($data['lineHeight']) ? 2 : $data['lineHeight'];
        $marginTop = empty($data['marginTop']) ? 0 : $data['marginTop'];
        $marginBottom = empty($data['marginBottom']) ? 0 : $data['marginBottom'];

        $linestyle = array(
            'width' => $lineHeight,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => '',
            'phase' => 0,
            'color' => $this->toColorArray($fontColor)
        );
        $curY = $this->pdf->getY();

        $this->pdf->Line(
            $x1=self::$PDF_MARGIN_LEFT,
            $y1=$curY + $marginTop,
            $x2=$this->pdf->getPageWidth() - self::$PDF_MARGIN_RIGHT,
            $y2=$curY + $marginTop,
            $linestyle
        );
        $this->pdf->setY($curY + $marginTop + $marginBottom);
    }

    public function addToolStructureSpacer($data) {
        $height = floatval($data['height']);

        $y = $this->pdf->getY() + $height;
        $this->pdf->setY($y);
    }

    public function resetLayout() {
        $this->pdf->currentLayout = 'page';
        $width = $this->pdf->getPageWidth() - self::$PDF_MARGIN_LEFT - self::$PDF_MARGIN_RIGHT;

        $currentY = $this->pdf->getY();
        $columns = array(
            array('w' => $width, 's' => 0, 'y' => $currentY)
        );
        $this->pdf->resetColumns();
        $this->pdf->setColumnsArray($columns);
    }

    public function prepareNextSection() {
        $this->currentFoodTablePosition = 0;
    }

    public function addToolStructureChangeLayout($data) {
        $idLayout = $data['idLayout'];
        $this->pdf->currentLayout = $idLayout;
        $width = $this->pdf->getPageWidth() - self::$PDF_MARGIN_LEFT - self::$PDF_MARGIN_RIGHT;

        $currentY = $this->pdf->getY();

        $spaceBetweenColumn = 6;
        $nWidth = $width - $spaceBetweenColumn;
        switch ($idLayout) {
            case '1left_1right':
                $col1Width = round($nWidth / 2, 0);
                break;
            case '1left_2right':
                $col1Width = round($width / 3, 0);
                break;
            case '2left_1right':
                $col1Width = round($width * 2/ 3, 0);
                break;
            case '1left_3right':
                $col1Width = round($width / 4, 0);
                break;
            case '3left_1right':
                $col1Width = round($width * 3/ 4, 0);
                break;
            case '1left_4right':
                $col1Width = round($width / 5, 0);
                break;
            case '1left_5right':
                $col1Width = round($width / 6, 0);
                break;
        }

        if ($idLayout == 'page') {
            $columns = array(
                array('w' => $width, 's' => 0, 'y' => $currentY)
            );
            $this->pdf->setX(self::$PDF_MARGIN_LEFT);
            $this->pdf->resetColumns();
        } else if ($idLayout == '1left_1mid_1right') {
            $nWidth = $width - 2*$spaceBetweenColumn;
            $colWidth = round($nWidth / 3);
            $columns = array(
                array('w' => round($colWidth, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($colWidth, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($colWidth, 0), 's' => 0, 'y' => $currentY),
            );
        } else if ($idLayout == '4_columns') {
            $nWidth = $width - 3*$spaceBetweenColumn;
            $colWidth = round($nWidth / 4);
            $columns = array(
                array('w' => round($colWidth, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($colWidth, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($colWidth, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($colWidth, 0), 's' => 0, 'y' => $currentY),
            );
        } else if ($idLayout == 'customs') {
            $arr = explode('|', $data['customLayout']);
            $columns = array();
            for ($i = 0; $i < count($arr); $i+=2) {
                $colWidth = round(floatval($arr[$i]) * $width / 100);
                $space = 0;
                if ($i != count($arr) - 1) {
                    $space = round(floatval($arr[$i+1]) * $width / 100);
                }

                $columns[] = array('w' => $colWidth, 's' => $space, 'y' => $currentY);
            }
        } else {
            $col2Width = $nWidth - $col1Width;
            $columns = array(
                array('w' => round($col1Width, 0), 's' => $spaceBetweenColumn, 'y' => $currentY),
                array('w' => round($col2Width, 0), 's' => 0, 'y' => $currentY),
            );
        }
        $this->pdf->setColumnsArray($columns);
    }

    public function addToolStructureBreak($data) {
        if ($data['idBreak'] == 'column') {
            $currentColumn = $this->pdf->getColumn();
            $this->pdf->lastY[$currentColumn] = $this->pdf->getY();

            $nextColumn = $currentColumn + 1;
            if ($nextColumn >= $this->pdf->getNumberOfColumns()) {
                $maxY = 0;
                foreach ($this->pdf->lastY as $y) { $maxY = $maxY < $y ? $y : $maxY; }
                $this->pdf->setY($maxY + 5);

                $this->addToolStructureChangeLayout(array('idLayout' => $this->pdf->currentLayout));
                $nextColumn = 0;
            }
            $this->pdf->selectColumn($nextColumn);
        }
        else if ($data['idBreak'] == 'page') {
            $this->pdf->AddPage();
        }
        else if ($data['idBreak'] == 'page_if') {
            $margins = $this->pdf->getMargins();
            $effectiveHeight = $this->pdf->getPageHeight() - $margins['bottom'];
            $remainingHeight = $effectiveHeight - $this->pdf->getY();
            if (isset($data['conditional'])) {
                if ($remainingHeight < floatval($data['conditional'])) {
                    $this->pdf->AddPage();
                }
            }
        }
        else if ($data['idBreak'] == 'footer') {
            $this->pdf->breakFooter();
        }
        else if ($data['idBreak'] == 'skip_footer') {
            $this->pdf->skipFooter();
        }
        else if ($data['idBreak'] == 'skip_from_footer') {
            $this->pdf->skipFromFooter();
        }
        else if ($data['idBreak'] == 'unskip_from_footer') {
            $this->pdf->unskipFromFooter();
        }
        else if ($data['idBreak'] == 'stop_auto_break') {
            $this->pdf->SetAutoPageBreak(false);
        }
        else if ($data['idBreak'] == 'start_auto_break') {
            $this->pdf->SetAutoPageBreak(true);
        }
        else if ($data['idBreak'] == 'break_chapter') {
            $this->pdf->curPageChapter = '';
        }
        else if ($data['idBreak'] == 'skip_chapter') {
            $this->pdf->numSkipChapter++;
        }
    }

    public function addToolStructurePageLayout($data) {
        self::$PDF_MARGIN_LEFT = $data['marginLeft'];
        self::$PDF_MARGIN_TOP = $data['marginTop'];
        self::$PDF_MARGIN_RIGHT = $data['marginRight'];
        self::$PDF_MARGIN_BOTTOM = $data['marginBottom'];

        $this->pdf->SetMargins(self::$PDF_MARGIN_LEFT, self::$PDF_MARGIN_TOP, self::$PDF_MARGIN_RIGHT, true);
        $this->pdf->SetFooterMargin(self::$PDF_MARGIN_FOOTER);
        $this->pdf->SetAutoPageBreak(TRUE, self::$PDF_MARGIN_BOTTOM + self::$PDF_MARGIN_FOOTER);
    }

    public function addToolStructurePageFooter($data) {
        if (isset($data['isBreak']) && $data['isBreak']) {
            $this->pdf->breakFooter();
        }
        else {
            $lineColor = !empty($data['lineColor']) ? $data['lineColor'] : '#000000';
            $data['lineColorArr'] = $this->toColorArray($lineColor);
            $data['marginBottom'] = self::$PDF_MARGIN_BOTTOM;
            $data['logo'] = $this->resourceDir.$data['logo'];
            if (isset($data['overrideLogo']) && trim($data['overrideLogo']) != '') {
                $data['logo'] = $this->resourceDir.$data['overrideLogo'];;
            }

            $data['isSimplified'] = false;

            $this->pdf->setMyFooterData($data);
        }
    }

    public function addToolStructurePageFooterSimplified($data) {
        $data['isSimplified'] = true;
        $config = isset($data['config']) ? $data['config'] : '';
        $arr = array('isSimplified' => true);
        $arr = $this->parseInput($arr, $config);
        $this->pdf->setMyFooterData($arr);
    }

    // WIDGETS
    public function addToolWidgetScaleTool($data) {
        // Def
        $tmp = explode("\n", $data['scaleDefinition']);
        $definition = array();
        foreach ($tmp as $r) {
            $r = trim($r);
            if (!empty($r)) {
                $r = $this->translateBlock($r);
                list($k, $label, $color) = explode(';', $r);
                $definition[] = array('key' => $k, 'label' => $label, 'color' => trim($color));
            }
        }

        // Round to nearest
        $matchedValue = 0;
        $minVal = 9999999;
        foreach ($definition as $k => $v) {
            $dis = abs(floatval($v['key']) - floatval($data['selectedValue']));
            if ($dis < $minVal) {
                $minVal = $dis;
                $matchedValue = $v['key'];
            }
        }

        $data['selectedValue'] = $matchedValue;

        // Build scale
        $fontColor = empty($data['fontColor']) ? '#000000' : $data['fontColor'];
        $fontSize = empty($data['fontSize']) ? 11 : $data['fontSize'];
        $width = 100 / count($definition);
        $arrowSize = (isset($data['arrowSize']) && !empty($data['arrowSize'])) ? $data['arrowSize'] : 11;
        $cellSpacing = 3;

        $middleLabel = isset($data['middleLabel']) ? $data['middleLabel'] : '';
        $html = '<table cellspacing="0">
            <tbody>
                <tr height="1">
                    <td align="left">
                        <div style="color:'.$fontColor.'; font-size: '.$fontSize.';">&nbsp;'.$data['leftLabel'].'</div>
                    </td>
                    <td align="center">
                        <div style="color:'.$fontColor.'; font-size: '.$fontSize.';">'.$middleLabel.'</div>
                    </td>
                    <td align="right">
                        <div style="color:'.$fontColor.'; font-size: '.$fontSize.';">'.$data['rightLabel'].'&nbsp;</div>
                    </td>
                </tr>
            </tbody>
            </table>
            <table cellspacing="'.$cellSpacing.'">
            <tbody>
                <tr>';
        foreach ($definition as $r) {
            $html .= '<td style="text-align: center; background-color: '.$r['color'].'; width: '.$width.'%">'.$r['label'].'</td>';
        }

        $html.='</tr><tr>';

        // Solve last-item case
        $lastItem = end($definition);
        if ($lastItem['key'] == $data['selectedValue']) {
            $html .= '<td style="text-align: right" colspan="'.count($definition).'"><b style="font-size: '.$arrowSize.'; color: #000000"><font face="pdfazapfdingbats">s</font></b></td>';
        } else { // Solve normal case
            $i=0;
            foreach ($definition as $r) {
                if ($r['key'] == $data['selectedValue']) {
                    $numCol = count($definition) - $i;
                    $html .= '<td style="text-align: left" colspan="'.$numCol.'"><b style="font-size: '.$arrowSize.'; color: #000000;"><font face="pdfazapfdingbats">s</font></b></td>';
                    break;
                } else {
                    $html .= '<td></td>';
                    $i++;
                }
            }
        }

        $html .='</tr></tbody></table>';
        $this->pdf->setY($this->pdf->getY() -4);
        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);
    }

    public function addToolWidgetSportTable($data) {
        $tool = new TextBlock\WidgetSportTable($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolWidgetTable($data) {
        $header = $data['header'];
        $content = $data['content'];
        $risks = $data['risks'];
        $fontMap = $this->getFontMap();

        $em = $this->container->get('doctrine')->getEntityManager();
        $smileyCalculator = $this->container->get('leep_admin.report.business.util.smiley_caculator');

        $html = '<table cellspacing="3">';

        $html .= '<thead>';
        $rows = explode("\n", $header);
        foreach ($rows as $r) {
            if (!empty($r)) {
                $html .= '<tr>';
                $cells = explode('|', $r);
                foreach ($cells as $c) {
                    $l = explode(';', $c);
                    $title = $l[0];
                    $color = $l[1];
                    $width = isset($l[2]) ? $l[2] : '20%';
                    $alignment = isset($l[3]) ? $l[3] : 'left';
                    $fontSize = isset($l[4]) ? $l[4]: 11;
                    $fontColor = isset($l[5]) ? $l[5] : '#000000';
                    $fontFace = isset($l[6]) ? $l[6]: 'regular';
                    $fontFace = trim($fontFace);
                    $fontFace = isset($fontMap[$fontFace]) ? $fontMap[$fontFace] : $this->fontMap['regular'];

                    if ($title == '#check#') {
                        $fontFace = 'customawsome';
                        $title = 'A';
                    } else if ($title == '#remove#') {
                        $fontFace = 'customawsome';
                        $title = 'B';
                    } else if ($title == '#warning#') {
                        $fontFace = 'customawsome';
                        $title = 'C';
                    }

                    $title = '<font face="'.$fontFace.'">'.trim($title)."</font>";
                    $title = $this->workaroundMarginTop($title, 1);

                    $html .= '<th style="color:'.$fontColor.'; background-color:'.$color.'; text-align: '.$alignment.'; font-size: '.$fontSize.'; " width="'.$width.'">'.$title.'</th>';
                }
                $html .= '</tr>';
            }
        }
        $html .= '</thead>';

        $backgroudSmiley = "#ffd480";
        $html .= '<tbody>';
        $rows = explode("\n", $content);
        foreach ($rows as $r) {
            if (!empty($r)) {
                $html .= '<tr>';
                $cells = explode('|', $r);
                foreach ($cells as $c) {
                    $l = explode(';', $c);
                    $title = $l[0];
                    $color = $l[1];
                    $width = isset($l[2]) ? $l[2] : '20%';
                    $alignment = isset($l[3]) ? $l[3] : 'left';
                    $fontSize = isset($l[4]) ? $l[4]: 11;
                    $fontColor = isset($l[5]) ? $l[5] : '#000000';
                    $fontFace = isset($l[6]) ? $l[6]: 'regular';
                    $fontType = isset($l[6]) ? $l[6]: 'regular';
                    $fontFace = trim($fontFace);
                    $fontFace = isset($fontMap[$fontFace]) ? $fontMap[$fontFace] : $this->fontMap['regular'];

                    $isBreakCell = false;
                    if (strpos($title, '@@@') === 0) {
                        $title = substr($title, 3);
                        $isBreakCell = true;
                    }

                    //$title = '&nbsp;&nbsp;<font face="'.$fontFace.'">'.trim($title)."</font>&nbsp;&nbsp;";
                    $isSmiley = false;
                    if (strpos($title, '##smiley') !== false) {
                        $smileyData = explode('#', $title);
                        if (count($smileyData) == 8) {
                            $smileyType = $smileyData[3];
                            $uid = $smileyData[4];
                            $width = $smileyData[5];
                            $width = $smileyData[5];

                            $foodIngredients = '';
                            $style = [
                                'fontColor' =>  $fontColor,
                                'fontSize' =>   $fontSize,
                                'fontType' =>   $fontType,
                                'textWidth' =>  $width
                            ];

                            if ($smileyType == 'recipe') {
                                $recipe = $em->getRepository('AppDatabaseMainBundle:Recipe')->findOneByRecipeId($uid);
                                $foodIngredients = $recipe->getFoodIngredients();
                            } else if ($smileyType == 'foodItem') {
                                $foodItem = $em->getRepository('AppDatabaseMainBundle:FoodTableFoodItem')->findOneByFoodId($uid);
                                $foodIngredients = $foodItem->getIngredientData();
                            }

                            $smileyCalculator->translateText($this);
                            $title = $smileyCalculator->computeToHtml($foodIngredients, $risks, $style);
                            $isSmiley = true;
                        }
                    }

                    if (!$isSmiley) {
                        if (trim($alignment) == 'left') {
                            $title = '&nbsp;&nbsp;<font face="'.$fontFace.'">'.trim($title).'</font>';
                        } else if (trim($alignment) == 'right') {
                            $title = '<font face="'.$fontFace.'">'.trim($title).'</font>&nbsp;&nbsp;';
                        } else {
                            $title = '<font face="'.$fontFace.'">'.trim($title).'</font>';
                        }

                        $title = $this->workaroundMarginTop($title, 1);
                    }

                    if ($isBreakCell) {
                        $title = $this->outputFloatingText($title);
                    }

                    $html .= '<td style="color:'.$fontColor.'; background-color:'.$color.'; text-align: '.$alignment.'; font-size: '.$fontSize.'; " width="'.$width.'">'.$title.'</td>';
                }
                $html .= '</tr>';
            }
        }
        $html .= '</tbody>';
        $html .= '</table>';

        $tagvs = array('b' => array(
            0 => array('h' => 10, 'n' => 5),
            1 => array('h' => 10, 'n' => 5)
        ));

        $this->pdf->setHtmlVSpace($tagvs);

        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=false);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
    }

    public function addToolWidgetDailySportTable($data) {
        $fromWeek = empty($data['fromWeek']) ? 1 : intval($data['fromWeek']);
        $toWeek = empty($data['toWeek']) ? 23 : intval($data['toWeek']);
        if ($fromWeek > $toWeek) $toWeek = $fromWeek;

        $rows = explode("\n", $data['definition']);
        $days = explode('|', $rows[0]);
        list($sport, $time) = explode('|', $rows[1]);
        list($color1, $color2, $color3, $color4) = explode('|', $rows[2]);

        $html = '<table cellspacing="2">';
        $html .= '<tbody>
            <tr><td width="30%" colspan="2"></td>';
        $width = (70 / ($toWeek - $fromWeek + 1)).'%';
        for ($w = $fromWeek; $w <= $toWeek; $w++) {
            $html .= '<td style="background-color: '.$color4.'" width="'.$width.'">W '.$w.'</td>';
        }
        $html .= '</tr>';

        foreach ($days as $day) {
            $html .= '<tr>
                <td colspan="2" style="background-color: '.$color1.'; text-align: center">'.$day.'</td>';
            for ($w = $fromWeek; $w <= $toWeek; $w++) {
                $html .= '<td style="background-color: '.$color4.'" width="'.$width.'" rowspan="3"></td>';
            }
            $html .= '</tr>';
            $html .= '<tr>
                <td width="12%" height="50px" style="background-color: '.$color2.'">'.$sport.'</td>
                <td width="18%" style="background-color: '.$color3.'"></td>
                </tr>
                <tr>
                <td style="background-color: '.$color2.'">'.$time.'</td>
                <td style="background-color: '.$color3.'"></td>
                </tr>';
        }
        $html .= '</tbody></table>';

        $this->pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, 'L', $autopadding=true);
    }


    public function addToolWidgetFoodTable($data) {
        $foodTable = new TextBlock\WidgetFoodTable($this->container, $this->pdf, $this);
        $foodTable->build($data, $this->idLanguage);
    }

    public function addToolWidgetFoodTable2($data) {
        $foodTable = new TextBlock\WidgetFoodTable2($this->container, $this->pdf, $this);
        $foodTable->build($data, $this->idLanguage);
    }

    public function addToolWidgetRecipe($data) {
        $foodTable = new TextBlock\WidgetRecipe($this->container, $this->pdf, $this);
        $foodTable->build($data);
    }

    public function addToolUtilExternalPdf($data) {
        $externalPdf = new TextBlock\UtilExternalPdf($this->container, $this->pdf, $this);
        $externalPdf->build($data);
    }

    public function addToolUtilThirdPartyExternalPdf($data) {
        $externalPdf = new TextBlock\UtilThirdPartyExternalPdf($this->container, $this->pdf, $this);
        $externalPdf->build($data);
    }

    public function addToolWidgetBarChart($data) {
        $chartDefinition = explode("\n", $data['chartDefinition']);
        $barDefinition = explode("\n", $data['barDefinition']);

        $x = $this->pdf->getX();
        $y = $this->pdf->getY();

        $chartHeight = intval($data['chartHeight']);
        if ($y + $chartHeight + self::$PDF_MARGIN_BOTTOM > $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            $x = $this->pdf->getX();
            $y = $this->pdf->getY();
        }

        $x = $x + 10;

        // Draw title
        list($chartTitle, $yourRiskTitle, $titleColor) = explode(';', $data['chartTitle']);
        $chartTitle = '<b style="color: '.$titleColor.'; font-size: 12"><font face="'.$this->fontMap['bold'].'">'.$chartTitle."</font></b>";
        $this->pdf->StartTransform();
        $this->pdf->Rotate(90);
        $this->pdf->writeHTMLCell(0, 0, -$chartHeight/2-10, $x-10, $chartTitle);
        $this->pdf->StopTransform();

        // Draw scales
        $minValue = 999999;
        $scaleHeightArr = array();
        $chartWidth = $this->pdf->getPageWidth() - self::$PDF_MARGIN_LEFT - self::$PDF_MARGIN_RIGHT - 10;
        foreach ($chartDefinition as $scale) {
            $scale = explode(';', $scale);
            $size = trim($scale[1]);
            $size = trim($size, '%');

            $scaleHeight = $chartHeight * $size / 100;
            $scaleHeightArr[$scale[0]] = $size;
            $minValue = min($minValue, $scale[0]);

            $style = array(
                'width' => 0,
                'cap' => 'butt',
                'join' => 'miter',
                'dash' => 0,
                'phase' => 0,
                'color' => $this->toColorArray($scale[2])
            );
            $this->pdf->Rect($x, $y, $chartWidth, $scaleHeight, 'F', $style, $this->toColorArray($scale[2]));
            $this->pdf->writeHTMLCell(0, 0, $x+2, $y+$scaleHeight/2-2, $scale[0].'%');
            $y = $y + $scaleHeight;
        }

        // Draw bars
        if (count($barDefinition) == 0) {
            return ;
        }

        ksort($scaleHeightArr);
        $barWidth = ($chartWidth - 30) / count($barDefinition);
        $curX = $x + 30;

        $minDiff = 999999;
        $selectedValue = intval($data['selectedValue']);
        $posX = 0;

        foreach ($barDefinition as $bar) {
            $bar = explode(';', $bar);
            if (!isset($bar[1])) {
                continue;
            }

            $barValue = $bar[0];
            $diff = abs($barValue - $selectedValue);
            if ($minDiff > $diff) {
                $minDiff = $diff;
                $posX = $curX;
            }
            $barSize = 0;
            $previousValue = $minValue - 50;
            foreach ($scaleHeightArr as $scaleValue => $scaleHeight) {
                if ($barValue >= $scaleValue) {
                    $previousValue = $scaleValue;
                    $barSize += $scaleHeight;
                }
                else {
                    $ratio = ($barValue - $previousValue) / ($scaleValue - $previousValue);
                    $barSize += $ratio * $scaleHeight;
                    break;
                }
            }
            $barSize = $barSize * $chartHeight / 100;

            $style = array(
                'width' => 0.5,
                'cap' => 'square',
                'join' => 'bevel',
                'dash' => 0,
                'phase' => 0,
                'color' => array(255, 255, 255)
            );
            $this->pdf->Rect($curX, $y - $barSize, $barWidth - 3, $barSize, 'F', array('LTRB' => $style), $this->toColorArray($bar[1]));

            // Draw text
            if (isset($bar[2])) {
                $barText = $bar[2];
                $barText = '<b style="color: '.$titleColor.'; font-size: 12"><font face="'.$this->fontMap['bold'].'">'.$barText."</font></b>";

                $this->pdf->writeHTMLCell(30, 0, $curX-10, $y-$barSize - 15, $barText, 0, false, false, 'C', true);
                $barText = '<b style="color: '.$titleColor.'"><font face="pdfazapfdingbats">t</font></b>';
                $this->pdf->writeHTMLCell(0, 0, $curX+($barWidth-8)/2, $y-$barSize - 5, $barText);
            }

            $curX += $barWidth;
        }

        // Draw selected text
        $barText = '<b style="color: '.$titleColor.'; font-size: 12"><font face="'.$this->fontMap['bold'].'">'.$yourRiskTitle."</font></b>";

        $this->pdf->writeHTMLCell(30, 0, $posX-10, $y + 5, $barText, 0, false, false, 'C', true);
        $barText = '<b style="color: '.$titleColor.'"><font face="pdfazapfdingbats">s</font></b>';
        $this->pdf->writeHTMLCell(0, 0, $posX+($barWidth-8)/2, $y, $barText);

        $this->pdf->setX(0);
        $this->pdf->setY($chartHeight + 20);
    }

    public function addToolUtilGrabTextBlock($data) {

    }

    public function addToolWidgetImageWithLabel($data) {
        $height = empty($data['height']) ? 20 : $data['height'];
        $labels = $data['labels'];

        $overrideImage = isset($data['overrideImage']) ? $data['overrideImage'] : '';
        $overrideImage = $this->translateBlock($overrideImage);
        if (trim($overrideImage) != '') {
            $imageFile = $this->resourceDir.$overrideImage;
        }
        else {
            $imageFile = $this->resourceDir.$data['image'];
        }

        $imageSize = getimagesize($imageFile);
        $imageHeightResized = $height;
        $imageWidthResized = $imageHeightResized * $imageSize[0] / $imageSize[1];

        $imageLink = isset($data['link']) ? $data['link'] : '';
        $imageLink = $this->translateBlock($imageLink);

        $posX = isset($data['posX']) ? $data['posX'] : '';
        $posY = isset($data['posY']) ? $data['posY'] : '';
        $rotation = isset($data['rotation']) ? $data['rotation'] : 0;
        if ($posX != '' || $posY != '') {
            $posX = floatval($posX);
            $posY = floatval($posY);
            if ($posX < 0) {
                $posX += $this->pdf->getPageWidth();
            }
            if ($posY < 0) {
                $posY += $this->pdf->getPageHeight();
            }

            $this->pdf->setX($posX);
            $this->pdf->setY($posY);

            $this->pdf->myDrawImage(
                $file = $imageFile,
                //'@'.$imageLoaded,
                $x = $posX,
                $y = $posY,
                $w = $imageWidthResized,
                $h = $imageHeightResized,
                $type = '',
                $link = $imageLink,
                $align = '',
                $resize = true,
                $dpi = 300,
                $palign = ''
            );
            $cX = $posX;
            $cY = $posY;
        }
        else {
            $alignment = $this->getAlignment($data);

            $this->pdf->myDrawImage(
                $file = $imageFile,
                //'@'.$imageLoaded,
                $x = '',
                $y = '',
                $w = $imageWidthResized,
                $h = $imageHeightResized,
                $type = '',
                $link = $imageLink,
                $align = 'T',
                $resize = true,
                $dpi = 300,
                $palign = $alignment
            );
            $cX = $this->pdf->getX() - $imageWidthResized;
            $cY = $this->pdf->getY();
        }

        $rows = explode("\n", $data['labels']);
        foreach ($rows as $r) {
            $t = explode('|', $r);
            if (count($t) < 2) continue;
            $mX = $t[0]; $mY = $t[1]; $label = $t[2];
            $color = isset($t[3]) ? $t[3] : '#FFFFFF';
            $size = isset($t[4]) ? $t[4] : 11;
            $fontCode = isset($t[5]) ? trim($t[5]) : 'regular';
            $width = isset($t[6]) ? intval($t[6]) : 0;
            $alignment = isset($t[7]) ? $this->translateAlignment(trim($t[7])) : 'L';

            $x = $cX + $mX;
            $y = $cY + $mY;

            $text = '<p style="color: '.$color.'; font-size: '.$size.';"><font face="'.$this->fontMap[$fontCode].'">'.$label.'</font></p>';

            $this->pdf->startTransform();
            $this->pdf->Rotate($rotation, $x + $width / 2, $y);
            $this->pdf->writeHTMLCell($w=$width, $h=0, $x, $y, $text, $border=0, $ln=1, $fill=0, $reseth=true, $alignment, $autopadding=true);
            $this->pdf->stopTransform();
        }

        $this->pdf->setX($cX);
        $this->pdf->setY($cY + $height);
    }


    public function addToolUtilBackgroundImage($data) {
        $tool = new TextBlock\UtilBackgroundImage($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolStructureWebCover($data) {
        $tool = new TextBlock\StructureWebCover($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolUtilMoveCursor($data) {
        $tool = new TextBlock\UtilMoveCursor($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolTextFloatingPlain($data) {
        $tool = new TextBlock\TextFloatingPlain($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolMarketingHeader($data) {
        $tool = new TextBlock\MarketingHeader($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolMarketingFooter($data) {
        $tool = new TextBlock\MarketingFooter($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolMarketingHeadline($data) {
        $tool = new TextBlock\MarketingHeadline($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolMarketingInfoBox($data) {
        $tool = new TextBlock\MarketingInfoBox($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolMarketingLogoRibbon($data) {
        $tool = new TextBlock\MarketingLogoRibbon($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolStructureCover($data) {
        $tool = new TextBlock\StructureCover($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolStructureBodBarcode($data) {
        $tool = new TextBlock\StructureBodBarcode($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolUtilTOC($data) {
        $tool = new TextBlock\UtilTOC($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolUtilTOCEntry($data) {
        $tool = new TextBlock\UtilTOCEntry($this->container, $this->pdf, $this);
        $tool->build($data);
    }
    public function addToolUtilAnnotator($data) {
    }

    public function addToolWidgetTableNew($data) {
        $tool = new TextBlock\WidgetTableNew($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolWidgetRecipeSimplified($data) {
        $tool = new TextBlock\WidgetRecipeSimplified($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolPageIntroPage($data) {
        $tool = new TextBlock\PageIntroPage($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolChapterToc($data) {
        $tool = new TextBlock\ChapterToc($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolChapterPage($data) {
        $tool = new TextBlock\ChapterPage($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    public function addToolWidgetDrugList($data)
    {
        $tool = new TextBlock\WidgetDrugList($this->container, $this->pdf, $this);
        $tool->build($data);
    }

    ////////////////////////////////////////////////////
    // x.

    public function getPdf() {
        return $this->pdf;
    }


    public function makeHtmlText($text, $fontType, $fontSize) {
        $fontMap = $this->getFontMap();
        $fontFace = isset($fontMap[$fontType]) ? $fontMap[$fontType] : $fontMap['regular'];

        return '<font face="'.$fontFace.'" style="font-size: '.$fontSize.'">'.$text.'</font>';
    }

    public function getFontFace($fontId) {
        $fontMap = $this->getFontMap();
        $fontFace = isset($fontMap[$fontId]) ? $fontMap[$fontId] : $fontMap['regular'];

        return $fontFace;
    }
}
