<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class PageIntroPage extends BaseTextBlock {
    public function parseInput($arr, $input) {
        $rows = explode("\n", $input);
        foreach ($rows as $r) {
            $index = strpos($r, '=');
            if ($index != 0) {            
                $k = substr($r, 0, $index);
                $v = substr($r, $index+1);
                $arr[trim($k)] = trim($v);
            }
        }
        return $arr;
    }

    public function build($data) { 
        $style = array(
            'bgcolor.gradient-top'    => '#5F0248',
            'bgcolor.gradient-bottom' => '#A47491',
            'color.inner-ring'        => '#C7D536',
            'color.outer-ring'        => '#AD7DA1',
            'color.center-ring'        => '#FFFFFF',
            'ring0.center.x'          => 120,
            'ring0.center.y'          => 70,
            'ring0.c0.radius'         => 60,
            'ring0.c0.image'          => '',
            'ring0.c0.image.width'    => 178,
            'ring0.c0.image.height'   => 248,
            'ring0.c0.image.x'        => 30,
            'ring0.c0.image.y'        => 0,
            'ring0.c1.radius'         => 70,
            'ring0.c2.radius'         => 80,
            'ring1.center.x'          => 50,
            'ring1.center.y'          => 200,
            'ring1.radius'            => 45,
            'ring2.center.x'          => 210,
            'ring2.center.y'          => 170,
            'ring2.radius'            => 25,
            'bone0-1.size'            => 30,
            'bone0-2.size'            => 20
        );
        $style = $this->parseInput($style, $data['style']);

        $width = $this->pdf->getPageWidth();
        $height = $this->pdf->getPageHeight();
        $bMargin = $this->pdf->getBreakMargin();
        $margins = $this->pdf->getMargins();        
        $auto_page_break = $this->pdf->getAutoPageBreak();
        $pageWidth = $this->pdf->getPageWidth() - $margins['left'] - $margins['right'];
        $pageHeight = $this->pdf->getPageHeight() - $bMargin;
        $this->pdf->SetAutoPageBreak(false, 0);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setMargins(0, 0, 0);

        /////////////////////////////////////
        // x. Draw gradient background
        $colorTop = $this->builder->toColorArray($style['bgcolor.gradient-top']);
        $colorBottom = $this->builder->toColorArray($style['bgcolor.gradient-bottom']);

        $coords = array(0.5, 1, 0.5, 0);

        $this->pdf->linearGradient(0, 0, $width, $height, $colorTop, $colorBottom, $coords);

        // Draw ring patterns
        Utils::renderRingPattern($this->container, $this->builder, $this->pdf, $style);

        // Close page
        $this->pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->pdf->setMargins($margins['left'], $margins['top'], $margins['right']);
        $this->pdf->setPageMark();
    }
}
