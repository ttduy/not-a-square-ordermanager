<?php
namespace Leep\AdminBundle\Business\Report\TextBlock;

class MarketingInfoBox extends BaseTextBlock {
    public function parseList($content) {
        $arr = array();

        $rows = explode("\n", $content);
        foreach ($rows as $r) {
            if (!empty($r)) {
                $arr[] = explode("|", $r);
            }
        }
        return $arr;
    }

    public function build($data) { 
        $curX = $this->pdf->getX();
        $curY = $this->pdf->getY();

        // Parse style
        $infoBoxStyle = array(
            'BoxWidth'          => '150',
            'BoxColor'          => '#123234',
            'TitleColor'        => '#000000',
            'BoldTextColor'     => '#173764',
            'NormalTextColor'   => '#000000'
        );
        $infoBoxStyle = $this->builder->parseInput($infoBoxStyle, $data['style']);

        // Write title
        $text = $data['title'];
        $text = $this->builder->translateCustomTag($text);
        $text = '<p style="color: '.$infoBoxStyle['TitleColor'].'; font-size: 12"><font face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($text, ENT_COMPAT, 'UTF-8')).'</font></p>';
        $this->pdf->writeHTMLCell(
            $w=0, 
            $h=0, 
            $x=$curX + 3, 
            $y=$curY,
            $text, 
            $border=0, 
            $ln=1, 
            $fill=0, 
            $reseth=true, 
            $alignment='L', 
            $autopadding=true
        );

        // Draw list
        $this->pdf->startTransaction();
        $this->drawList($data, $infoBoxStyle, $curX, $curY);
        $boxHeight = $this->pdf->getY() - $curY - 5;
        $this->pdf->rollbackTransaction(true);

        // Draw background box
        $fillColor = $this->builder->toColorArray($infoBoxStyle['BoxColor']);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $this->pdf->RoundedRect(
            $x = $curX,
            $y = $curY + 6,
            $w = $infoBoxStyle['BoxWidth'],
            $h = $boxHeight,
            $r = 3,
            $roundCorner = '1111',
            'F',
            $style,
            $fillColor
        );

        // Actually draw list
        $this->drawList($data, $infoBoxStyle, $curX, $curY);

    }
    public function drawList($data, $infoBoxStyle, $curX, $curY) {
        $this->pdf->setY($curY + 8);
        $textList = $this->parseList($data['boxContent']);
        foreach ($textList as $textRow) {
            $text = '<p>'.
                '<font style="color: '.$infoBoxStyle['BoldTextColor'].'; font-size: 12" face="'.$this->builder->fontMap['bold'].'">'.nl2br(htmlentities($textRow[0], ENT_COMPAT, 'UTF-8')).'</font>';
            if (isset($textRow[1])) {
                $text .= '<font style="color: '.$infoBoxStyle['NormalTextColor'].'; font-size: 10" face="'.$this->builder->fontMap['regular'].'">&nbsp;»&nbsp;'.nl2br(htmlentities($textRow[1], ENT_COMPAT, 'UTF-8')).'</font>';
            }
            $text .='</p>';
            $this->pdf->writeHTMLCell(
                $w=0, 
                $h=0, 
                $x=$curX + 3, 
                $y='',
                $text, 
                $border=0, 
                $ln=1, 
                $fill=0, 
                $reseth=true, 
                $alignment='L', 
                $autopadding=true
            );
        }
    }
}
