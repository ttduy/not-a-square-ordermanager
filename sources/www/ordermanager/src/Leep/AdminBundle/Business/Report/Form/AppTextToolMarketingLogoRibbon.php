<?php
namespace Leep\AdminBundle\Business\Report\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppTextToolMarketingLogoRibbon extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_text_tool_marketing_logo_ribbon';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 

        $builder->add('logo', 'app_image_picker', array(
            'label'  => 'Logo',
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 280px; max-width: 280px'
            )
        ));
        $builder->add('text', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 400px; max-width: 400px'
            ) 
        ));
        $builder->add('style', 'textarea', array(
            'required' => false,
            'attr' => array(
                'rows'  => '5', 'cols' => 80
            ) 
        )); 
    }
}
