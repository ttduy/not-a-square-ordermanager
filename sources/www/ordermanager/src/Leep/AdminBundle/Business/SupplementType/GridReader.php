<?php
namespace Leep\AdminBundle\Business\SupplementType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'letter', 'lotInUse', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',       'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Letter',     'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Lot in use', 'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',     'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_SpecialProductGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:SupplementType', 'p');            

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->letter) != '') {
            $queryBuilder->andWhere('p.letter LIKE :letter')
                ->setParameter('letter', '%'.trim($this->filters->letter).'%');
        }
        if (trim($this->filters->lotInUse) != '') {
            $queryBuilder->andWhere('p.lotInUse LIKE :lotInUse')
                ->setParameter('lotInUse', '%'.trim($this->filters->lotInUse).'%');
        }

    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');            
        $builder->addNewTabButton('Detail', $mgr->getUrl('leep_admin', 'supplement_type', 'feature', 'viewChanges', array('id' => $row->getId())), 'button-detail');

        if ($helperSecurity->hasPermission('nutrimeModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'supplement_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'supplement_type', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
