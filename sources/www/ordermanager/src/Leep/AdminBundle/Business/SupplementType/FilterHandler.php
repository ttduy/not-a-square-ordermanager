<?php
namespace Leep\AdminBundle\Business\SupplementType;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('letter', 'text', array(
            'label'    => 'Letter',
            'required' => false
        ));
        $builder->add('lotInUse', 'text', array(
            'label'    => 'Lot in use',
            'required' => false
        ));
        
        return $builder;
    }
}