<?php
namespace Leep\AdminBundle\Business\SupplementType;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:SupplementType', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->letter = $entity->getLetter();
        $model->lotInUse = $entity->getLotInUse();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('letter', 'text', array(
            'label'    => 'Letter',
            'required' => false
        ));
        $builder->add('lotInUse', 'text', array(
            'label'    => 'Lot in use',
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setLetter($model->letter);
        $this->entity->setLotInUse($model->lotInUse);

        $supplementTypeLog = new Entity\SupplementTypeLog();
        $supplementTypeLog->setIdWebUser($userManager->getUser()->getId());
        $supplementTypeLog->setIdSupplementType($this->entity->getId());
        $supplementTypeLog->setTimestamp(new \DateTime());
        $supplementTypeLog->setName($model->name);
        $supplementTypeLog->setLetter($model->letter);
        $supplementTypeLog->setLotInUse($model->lotInUse);
        $em->persist($supplementTypeLog);
        $em->flush();

        parent::onSuccess();
    }
}
