<?php
namespace Leep\AdminBundle\Business\SupplementType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('letter', 'text', array(
            'label'    => 'Letter',
            'required' => false
        ));
        $builder->add('lotInUse', 'text', array(
            'label'    => 'Lot in use',
            'required' => false
        ));
    }

    public function onSuccess() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $supplementType = new Entity\SupplementType();
        $supplementType->setName($model->name);
        $supplementType->setLetter($model->letter);
        $supplementType->setLotInUse($model->lotInUse);
        $em->persist($supplementType);
        $em->flush();

        $supplementTypeLog = new Entity\SupplementTypeLog();
        $supplementTypeLog->setIdWebUser($userManager->getUser()->getId());
        $supplementTypeLog->setIdSupplementType($supplementType->getId());
        $supplementTypeLog->setTimestamp(new \DateTime());
        $supplementTypeLog->setName($model->name);
        $supplementTypeLog->setLetter($model->letter);
        $supplementTypeLog->setLotInUse($model->lotInUse);
        $em->persist($supplementTypeLog);
        $em->flush();

        parent::onSuccess();
    }
}

