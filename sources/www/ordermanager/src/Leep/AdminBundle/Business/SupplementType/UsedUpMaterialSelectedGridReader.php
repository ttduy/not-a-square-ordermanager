<?php
namespace Leep\AdminBundle\Business\SupplementType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class UsedUpMaterialSelectedGridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('name', 'quantity', 'usedfor', 'datetaken', 'takenby');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Material', 'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Quantity in units', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Used for', 'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Date taken', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Taken by', 'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_SupplementTypeUsedUpMaterialSelectedGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.quantity, p.usedfor, p.datetaken, p.takenby, p.id, m.name')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('p.isSelected = :isSelected')
            ->setParameter('isSelected', 1)
            ->orderBy('p.datetaken', 'DESC');
    }
}
