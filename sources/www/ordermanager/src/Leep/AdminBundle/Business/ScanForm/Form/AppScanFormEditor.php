<?php
namespace Leep\AdminBundle\Business\ScanForm\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppScanFormEditor extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_scan_form_editor';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $data = $form->getData();   
        $view->vars['zones'] = $data['zones'];

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('formImage', 'hidden');
        $builder->add('zones', 'hidden');
    }
}