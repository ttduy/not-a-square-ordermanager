<?php
namespace Leep\AdminBundle\Business\ScanForm;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanForm', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $mgr = $this->container->get('easy_module.manager');
        
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->formImgFile = $entity->getFormImgFile();        

        // Load zones
        $zones = array();
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormField')->findByIdScanForm($entity->getId());
        foreach ($results as $r) {
            $zones[] = array(
                'idFieldType' => $r->getIdFieldType(),
                'title'       => $r->getName(),
                'x'           => $r->getZoneX(),
                'y'           => $r->getZoneY(),
                'width'       => $r->getZoneWidth(),
                'height'      => $r->getZoneHeight(),
            );
        }
        $model->scanFormEditor = array(
            'formImage' => $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'viewAttachment', array('dir' => 'scan_form', 'name' => $entity->getFormImgFile())),
            'zones' => $zones
        );

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        

        $builder->add('sectionInfo', 'section', array(
            'label'    => 'Scan form',
            'property_path' => false
        ));        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('formImgFile',          'app_attachment', array(
            'label'    => 'Form Image',
            'required' => false,
            'path'     => 'scan_form'
        ));

        $builder->add('sectionDesign', 'section', array(
            'label'    => 'Form Design',
            'property_path' => false
        ));        
        $builder->add('scanFormEditor',          'app_scan_form_editor', array(
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');
        
        $this->entity->setName($model->name);
        if ($model->formImgFile !== FALSE) {
            $this->entity->setFormImgFile($model->formImgFile);
        }

        // Save zones
        $filters = array('idScanForm' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ScanFormField', $filters);
        $zones = json_decode($model->scanFormEditor['zones'], true);
        foreach ($zones as $zone) {
            $scanFormField = new Entity\ScanFormField();
            $scanFormField->setIdScanForm($this->entity->getId());
            $scanFormField->setName($zone['title']);
            $scanFormField->setIdFieldType($zone['type']);
            $scanFormField->setZoneX($zone['x']);
            $scanFormField->setZoneY($zone['y']);
            $scanFormField->setZoneWidth($zone['width']);
            $scanFormField->setZoneHeight($zone['height']);
            $em->persist($scanFormField);            
        }
        $em->flush();

        parent::onSuccess();

        $this->reloadForm();
    }
}
