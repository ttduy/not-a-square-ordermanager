<?php
namespace Leep\AdminBundle\Business\ScanForm;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('formImgFile',          'app_attachment', array(
            'label'    => 'Form Image',
            'required' => false,
            'path'     => 'scan_form'
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $code = $this->getUnusedCode();
        if (empty($code)) {
            $this->errors[] = "Don't have enough code, please add new code";
            return;
        }

        $scanForm = new Entity\ScanForm();
        $scanForm->setName($model->name);
        $scanForm->setCode($code->getCode());
        if ($model->formImgFile != FALSE) {
            $scanForm->setFormImgFile($model->formImgFile);
        }
        $em->persist($scanForm);
        
        $code->setStatus(Business\ScanFormCode\Constants::STATUS_USED);
        $em->persist($code);
        $em->flush();

        $this->newId = $scanForm->getId();

        parent::onSuccess();
    }

    private function getUnusedCode() {
        $doctrine = $this->container->get('doctrine');
        return $doctrine->getRepository('AppDatabaseMainBundle:ScanFormCode')->findOneByStatus(Business\ScanFormCode\Constants::STATUS_UNUSED);
    }
}
