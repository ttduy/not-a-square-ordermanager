<?php
namespace Leep\AdminBundle\Business\Recipe;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('recipeId', 'name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Recipe ID', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Name',      'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',    'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Recipe', 'p');     
        if (trim($this->filters->recipeId) != '') {
            $queryBuilder->andWhere('p.recipeId LIKE :recipeId')
                ->setParameter('recipeId', '%'.trim($this->filters->recipeId).'%');
        }   
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }   
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('recipeModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'recipe', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'recipe', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
