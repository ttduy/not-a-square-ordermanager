<?php
namespace Leep\AdminBundle\Business\Recipe;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return new CreateModel();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('recipeId', 'text', array(
            'label'    => 'Recipe ID',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('ingredient', 'textarea', array(
            'label'    => 'Ingredient',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 90
            )
        ));
        $builder->add('foodIngredients', 'textarea', array(
            'label'    => 'Food ingredients',
            'attr'     => array(
                'rows'        => 20,
                'cols'        => 90
            ),
            'required' => false
        ));
        $builder->add('instruction', 'textarea', array(
            'label'    => 'Instruction',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 90
            )
        ));
        $builder->add('imageFile',          'app_logo', array(
            'label'    => 'Image',
            'required' => false,
            'path'     => 'report_resource/Recipes'
        ));
        $builder->add('isVegan', 'checkbox', array(
            'label'    => 'Is Vegan',
            'required' => false
        ));
        $builder->add('preparationTime', 'text', array(
            'label'    => 'Preparation time',
            'required' => false
        ));
        $builder->add('carb', 'text', array(
            'label'    => 'Carbohydrates (g/Kcal)',
            'required' => false
        ));
        $builder->add('prot', 'text', array(
            'label'    => 'Protein (g/Kcal)',
            'required' => false
        ));
        $builder->add('fat', 'text', array(
            'label'    => 'Fat (g/Kcal)',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $recipe = new Entity\Recipe();
        $recipe->setRecipeId($model->recipeId);
        $recipe->setName($model->name);
        $recipe->setIngredient($model->ingredient);
        $recipe->setFoodIngredients($model->foodIngredients);
        $recipe->setInstruction($model->instruction);

        // Save logo
        if (!empty($model->imageFile['attachment'])) {
            $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource/Recipes';
            $name = $recipe->getRecipeId().'.'.pathinfo($model->imageFile['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model->imageFile['attachment']->move($attachmentDir, $name);
            $recipe->setImageFile($name);
        }

        $recipe->setPreparationTime($model->preparationTime);
        $recipe->setIsVegan($model->isVegan);
        $recipe->setCarb($model->carb);
        $recipe->setProt($model->prot);
        $recipe->setFat($model->fat);
        $em->persist($recipe);
        $em->flush();

        parent::onSuccess();
    }
}
