<?php
namespace Leep\AdminBundle\Business\Recipe;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Recipe', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->recipeId = $entity->getRecipeId();
        $model->name = $entity->getName();
        $model->ingredient = $entity->getIngredient();
        $model->foodIngredients = $entity->getFoodIngredients();
        $model->instruction = $entity->getInstruction();
        $model->imageFile = array('logo' => $entity->getImageFile());
        $model->isVegan = $entity->getIsVegan();
        $model->carb = $entity->getCarb();
        $model->prot = $entity->getProt();
        $model->fat = $entity->getFat();
        $model->preparationTime = $entity->getPreparationTime();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('recipeId', 'text', array(
            'label'    => 'Recipe ID',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('ingredient', 'textarea', array(
            'label'    => 'Ingredient',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 90
            )
        ));
        $builder->add('foodIngredients', 'textarea', array(
            'label'    => 'Food ingredients',
            'attr'     => array(
                'rows'        => 20,
                'cols'        => 90
            ),
            'required' => false
        ));
        $builder->add('instruction', 'textarea', array(
            'label'    => 'Instruction',
            'required' => false,
            'attr'     => array(
                'rows' => 10, 'cols' => 90
            )
        ));
        $builder->add('imageFile',          'app_logo', array(
            'label'    => 'Image',
            'required' => false,
            'path'     => 'report_resource/Recipes'
        ));
        $builder->add('isVegan', 'checkbox', array(
            'label'    => 'Is Vegan',
            'required' => false
        ));
        $builder->add('preparationTime', 'text', array(
            'label'    => 'Preparation time',
            'required' => false
        ));
        $builder->add('carb', 'text', array(
            'label'    => 'Carbohydrates (g/Kcal)',
            'required' => false
        ));
        $builder->add('prot', 'text', array(
            'label'    => 'Protein (g/Kcal)',
            'required' => false
        ));
        $builder->add('fat', 'text', array(
            'label'    => 'Fat (g/Kcal)',
            'required' => false
        ));
        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));

        return $builder;
    }


    public function onPreBind(DataEvent $event) {
        $model = $event->getData();

        $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/attachments/report_resource/Recipes';
        if (isset($model['imageFile']['attachment']) && !empty($model['imageFile']['attachment'])) {
            $name = $this->entity->getRecipeId().'.'.pathinfo($model['imageFile']['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
            $model['imageFile']['attachment']->move($attachmentDir, $name);
            $model['imageFile']['logo'] = $name;
        }
        else {
            $model['imageFile']['logo'] = $this->entity->getImageFile();
        }

        $event->setData($model);
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->entity->setRecipeId($model->recipeId);
        $this->entity->setName($model->name);
        $this->entity->setIngredient($model->ingredient);
        $this->entity->setFoodIngredients($model->foodIngredients);
        $this->entity->setInstruction($model->instruction);
        if (!empty($model->imageFile['logo'])) {
            $this->entity->setImageFile($model->imageFile['logo']);
        }
        $this->entity->setPreparationTime($model->preparationTime);
        $this->entity->setIsVegan($model->isVegan);
        $this->entity->setCarb($model->carb);
        $this->entity->setProt($model->prot);
        $this->entity->setFat($model->fat);

        parent::onSuccess();
    }
}
