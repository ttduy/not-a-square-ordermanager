<?php
namespace Leep\AdminBundle\Business\Recipe;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $recipeId;
    public $name;
    public $ingredient;
    public $foodIngredients;
    public $instruction;
    public $imageFile;

    public $isVegan;
    public $preparationTime;
    public $carb;
    public $prot;
    public $fat;
}
