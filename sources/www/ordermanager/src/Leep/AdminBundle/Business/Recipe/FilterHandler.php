<?php
namespace Leep\AdminBundle\Business\Recipe;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('recipeId', 'text', array(
            'label'       => 'Recipe ID',
            'required'    => false
        ));
        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false
        ));

        return $builder;
    }
}