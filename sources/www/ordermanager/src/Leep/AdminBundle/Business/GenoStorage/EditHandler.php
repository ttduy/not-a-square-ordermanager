<?php
namespace Leep\AdminBundle\Business\GenoStorage;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoStorages', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();
        $model->warningThreshold = $entity->getWarningThreshold();
        $model->storageCells = [];

        $cells = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findBy(
                ['storageId' =>  $entity->getId()],
                ['level' =>      'ASC']
            );

        foreach ($cells as $cell) {
            $model->storageCells[] = [
                'id' =>                 $cell->getId(),
                'name' =>               $cell->getName(),
                'warningThreshold' =>   $cell->getWarningThreshold(),
                'level' =>              $cell->getLevel() + 1,
                'loading' =>            $cell->getLoading()
            ];
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // Common
        $builder->add('storageSection', 'section', [
            'label' =>          'Storage information',
            'property_path' =>  false
        ]);

        $builder->add('name', 'text', [
            'label' =>      'Name',
            'required' =>   true,
        ]);

        $builder->add('warningThreshold', 'text', [
            'label' =>      'Default warning threshold',
            'required' =>   true
        ]);

        $builder->add('description', 'textarea', [
            'label' =>      'Notes',
            'required' =>   false,
            'attr' => [
                'style' =>  'width:600px',
                'rows' =>   10
            ]
        ]);

        // Cell
        $builder->add('storageLevelSection', 'section', [
            'label' =>          'Storage\'s level',
            'property_path' =>  false
        ]);

        $builder->add('storageCells', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_storage_cell'),
            'label' => 'Level',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // Validate unique name
        $modelName = trim($model->name);
        $check = $em->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneByName($modelName);
        if (empty($modelName)) {
            $this->errors[] = "Storage's name can not be empty!";
            return false;
        }

        if ($check && $check->getId() != $this->entity->getId()) {
            $this->errors[] = "Storage's name $modelName already exist!";
            return false;
        }

        if (intval($model->warningThreshold) <= 0) {
            $this->errors[] = "Default warning threshold must be greater than zero!";
            return false;
        }

        // Update storage information
        $this->entity->setName($modelName);
        $this->entity->setDescription($model->description);
        $this->entity->setWarningThreshold(intval($model->warningThreshold));

        // Get previous storage cells
        $storageCells = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findByStorageId($this->entity->getId());

        // Add new storage cell
        $updateCells = [];
        if ($model->storageCells) {
            $cellNameList = [];
            foreach ($model->storageCells as $cellData) {
                $cellName = trim($cellData['name']);

                if (empty($cellName)) {
                    $this->errors[] = "Storage level's name can not be empty!";
                    return false;
                }

                // validation
                if (in_array($cellName, $cellNameList)) {
                    $this->errors[] = "Level name $cellName already exist!";
                    return false;
                }

                $cellNameList[] = $cellName;

                // Insert or update record
                $cell = new Entity\GenoStorageCell();
                $cell->setStorageId($this->entity->getId());
                $cell->setLoading(0);

                // Check if this is an update cell
                if (!empty($cellData['id'])) {
                    $updateCells[] = $cellData['id'];
                    $cell = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findOneById($cellData['id']);
                }

                $cell->setLevel($cellData['level'] - 1);
                $cell->setWarningThreshold(intval($cellData['warningThreshold']));
                $cell->setName($cellData['name']);
                $em->persist($cell);
            }

            $em->flush();
        }

        // Remove all un-updated storage cell
        foreach ($storageCells as $cell) {
            // Only delete cell that is not updated
            if (!in_array($cell->getId(), $updateCells)) {
                // If try to delete not empty cell. Return error.
                if ($cell->getLoading() > 0) {
                    $this->errors[] = "Unable to delete level ".$cell->getName()." (#".$cell->getId()."). Storage level not empty.";
                    return false;
                }

                $em->remove($cell);
            }
        }

        $em->flush();

        $this->reloadForm();
        parent::onSuccess();
    }
}
