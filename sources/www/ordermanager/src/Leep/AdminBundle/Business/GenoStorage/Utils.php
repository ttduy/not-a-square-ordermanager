<?php
namespace Leep\AdminBundle\Business\GenoStorage;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\GenoBatch\Utils as GenoBatchUtils;

class Utils {
    const FIRST_INDEX = 0;

    private static function getFirstNumber($str){
        $number = $str[self::FIRST_INDEX];
        if (!is_numeric(intval($number))) {
            return 0;
        }
        $i = self::FIRST_INDEX + 1;
        while ($i < strlen($str) && is_numeric($str[$i])) {
            $number = $number."{$str[$i]}";
            $i++;
        }
        $numberRes = intval($number);
        return $numberRes;
    }

    static public function getLocationList($container) {
        $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p.id, s.name as storage_name, p.name as cell_name, p.level, s.id as idStorage')
            ->from('AppDatabaseMainBundle:GenoStorageCell', 'p')
            ->innerJoin('AppDatabaseMainBundle:GenoStorages', 's', 'WITH', 'p.storageId = s.id')
            ->addGroupBy('p.id')
            ->addOrderBy('s.name', 'ASC')
            ->addOrderBy('p.level', 'ASC');

        $queryResult = $query->getQuery()->getResult();

        $query2 = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query2->select('g.id, g.name as storage_name')
              ->from('AppDatabaseMainBundle:GenoStorages', 'g');

        $genoStoragesInfo = $query2->getQuery()->getResult();

        $storageNameArr = [];
        $tmp = [];
        foreach ($genoStoragesInfo as $rowStorage) {
            $firstNumberCurrent = Utils::getFirstNumber($rowStorage['storage_name']);
            $tmp[$rowStorage['id']] = $firstNumberCurrent;
            $storageNameArr[$rowStorage['id']] = $rowStorage['storage_name'];
        }
        asort($tmp);
        $storageNameSorted = [];
        foreach ($tmp as $tmpKey => $tmpValue) {
            $storageNameSorted[$tmpKey] = $storageNameArr[$tmpKey];
        }

        // assign value
        $result = [];
        foreach ($storageNameSorted as $idStorageKey => $storageName) {
            $i = 0;
            $flag = False;
            foreach ($queryResult as $row) {
                if ($row['idStorage'] == $idStorageKey) {
                    $result[$row['id']] = $row['storage_name']." => ".$row['cell_name']." (".($row['level'] + 1).")";
                    $flag = True;
                }
                if ($flag == True && $row['idStorage'] != $idStorageKey) {
                    break;
                }
            }
        }

        return $result;
    }

    static public function changeBatchLocation($container, $batch, $newLocation) {
        if ($batch->getLocation() == $newLocation) {
            return;
        }

        $em = $container->get('doctrine')->getEntityManager();
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        $newStorageCell = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findOneById($newLocation);

        $moveToStr = "Unknown!";
        if ($newStorageCell) {
            $newStorage = $em->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneById($newStorageCell->getStorageId());
            $moveToStr = "#$newLocation - ".$newStorage->getName()." - ".$newStorageCell->getName();
        }

        $currentStorageCell = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findOneById($batch->getLocation());
        if ($currentStorageCell) {
            $currentStorage = $em->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneById($currentStorageCell->getStorageId());
            $moveFromStr = "#".$currentStorageCell->getId()." - ".$currentStorage->getName()." - ".$currentStorageCell->getName();

            // Remove from current storage
            $log = new Entity\GenoStorageCellLogs();
            $log->setCellId($currentStorageCell->getId());
            $log->setAmount(-$batch->getAmountInStock());
            $log->setHelper("Batch: ".$batch->getId()."; Change location (Move to $moveToStr).");
            $log->setUser($currentUserId);
            $log->setDate(new \DateTime());
            $em->persist($log);

            $newLoading = $currentStorageCell->getLoading() - $batch->getAmountInStock();
            if ($newLoading < 0) {
                $newLoading = 0;
            }

            $currentStorageCell->setLoading($newLoading);
            $em->persist($currentStorageCell);

            $newLoading = $currentStorage->getLoading() - $batch->getAmountInStock();
            if ($newLoading < 0) {
                $newLoading = 0;
            }

            $currentStorage->setLoading($newLoading);
            $em->persist($currentStorage);

            $em->flush();
        }
        else {
            $moveFromStr = "No location";
        }

        // Add batch log
        $log = new Entity\GenoBatchLogs();
        $log->setBatchId($batch->getId());
        $log->setAmount($batch->getAmountInStock());
        $log->setHelper("Move from $moveFromStr to $moveToStr.");
        $log->setUser($currentUserId);
        $log->setDate(new \DateTime());
        $em->persist($log);

        // Add to new Storage
        if ($newStorageCell) {
            $log = new Entity\GenoStorageCellLogs();
            $log->setCellId($newStorageCell->getId());
            $log->setAmount($batch->getAmountInStock());
            $log->setHelper("Batch: ".$batch->getId()."; Change location (Move from $moveFromStr).");
            $log->setUser($currentUserId);
            $log->setDate(new \DateTime());
            $em->persist($log);

            $newLoading = $newStorageCell->getLoading() + $batch->getAmountInStock();
            if ($newLoading < 0) {
                $newLoading = 0;
            }

            $newStorageCell->setLoading($newLoading);
            $em->persist($newStorageCell);

            $newLoading = $newStorage->getLoading() + $batch->getAmountInStock();
            if ($newLoading < 0) {
                $newLoading = 0;
            }

            $newStorage->setLoading($newLoading);
            $em->persist($newStorage);

            $em->flush();
        }

        // Update batch
        $batch->setLocation($newLocation);
        $em->persist($batch);
        $em->flush();
    }

    static public function updateStorage($container, $batch, $amount, $helper) {
        // Making sure amount is number
        $amount = intval($amount);

        // If no amount was used. Skip!
        if ($amount == 0) {
            return 0;
        }

        $em = $container->get('doctrine')->getEntityManager();
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        // Create log
        if ($helper != "Delete") {
            // Update batch
            $newAmountInstock = intval($batch->getAmountInStock()) + $amount;
            if ($newAmountInstock <= 0) {
                $newAmountInstock = 0;
                GenoBatchUtils::updateBatchStatus($container, $batch, Business\GenoBatch\Constant::STATUS_USED_UP, $helper);
            }

            $batch->setAmountInStock($newAmountInstock);
            $em->persist($batch);

            $log = new Entity\GenoBatchLogs();
            $log->setBatchId($batch->getId());
            $log->setAmount($amount);
            $log->setHelper($helper);
            $log->setUser($currentUserId);
            $log->setDate(new \DateTime());
            $em->persist($log);
        }

        if (!$batch->getLocation()) {
            $em->flush();
            return 0;
        }

        $storageCell = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findOneById($batch->getLocation());
        $storage = $em->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneById($storageCell->getStorageId());

        $log = new Entity\GenoStorageCellLogs();
        $log->setCellId($storageCell->getId());
        $log->setAmount($amount);
        $log->setHelper("Batch: ".$batch->getId()."; $helper");
        $log->setUser($currentUserId);
        $log->setDate(new \DateTime());
        $em->persist($log);

        // Update storage
        $newLoading = $storageCell->getLoading() + $amount;
        if ($newLoading < 0) {
            $newLoading = 0;
        }

        $storageCell->setLoading($newLoading);
        $em->persist($storageCell);

        $newLoading = $storage->getLoading() + $amount;
        if ($newLoading < 0) {
            $newLoading = 0;
        }

        $storage->setLoading($newLoading);
        $em->persist($storage);
        $em->flush();
    }

    static public function moveGenoBatch($container, $originalBatchId, $movedAmount, $destinationCellId){
        $em = $container->get('doctrine')->getEntityManager();
        $currentBatch = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findOneBy(array('id' => $originalBatchId));

        // update storage
        $passedAmount = $movedAmount * (-1);
        Business\GenoStorage\Utils::updateStorage($container, $currentBatch, $passedAmount, "Batch updated when move batch storage (Geno form > move batch)");

        // get User id: determine who move stock
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        // create new batch
        $genoBatchNew = new Entity\GenoBatches();

        // Common
        $genoBatchNew->setCreatedDate(new \DateTime());
        $genoBatchNew->setCreatedBy($currentUserId);
        $genoBatchNew->setModifiedDate(new \DateTime());
        $genoBatchNew->setModifiedBy($currentUserId);
        $genoBatchNew->setIdGenoMaterial($currentBatch->getIdGenoMaterial());
        $genoBatchNew->setLocation($destinationCellId);

        // Order information
        $genoBatchNew->setOrderedBy($currentBatch->getOrderedBy());
        $genoBatchNew->setOrderedDate($currentBatch->getOrderedDate());
        $genoBatchNew->setOrderedPrice($currentBatch->getOrderedPrice());
        $genoBatchNew->setUnitPrice($currentBatch->getUnitPrice());
        $genoBatchNew->setUnitQuantity($currentBatch->getUnitQuantity());
        $genoBatchNew->setSupplierName($currentBatch->getSupplierName());
        $genoBatchNew->setFillers($currentBatch->getFillers());

        // receive Information
        $genoBatchNew->setRecievedBy($currentBatch->getRecievedBy());
        $genoBatchNew->setRecievedDate($currentBatch->getRecievedDate());
        $genoBatchNew->setExpiryDate($currentBatch->getExpiryDate());
        $genoBatchNew->setLoading($currentBatch->getLoading());
        $genoBatchNew->setExternalLot($currentBatch->getExternalLot());
        $genoBatchNew->setIsShipmentOk($genoBatchNew->getIsShipmentOk());
        $genoBatchNew->setNotes($currentBatch->getNotes());
        $em->persist($genoBatchNew);
        $em->flush();

        // Update status
        GenoBatchUtils::updateBatchStatus($container, $genoBatchNew, $currentBatch->getStatus(), "Create batch when move older batch #".$currentBatch->getId()." (Geno form > move batch)", true);

        // Update storage
        Business\GenoStorage\Utils::updateStorage($container, $genoBatchNew, $movedAmount, "Create batch when move older batch #".$currentBatch->getId()." (Geno form > move batch)");

        return "1";
    }

    static public function removeGenoBatch($container, $batchIdPass, $removedAmount){
        $em = $container->get('doctrine')->getEntityManager();
        $currentBatch = $em->getRepository('AppDatabaseMainBundle:GenoBatches')->findOneBy(array('id' => $batchIdPass));

        $removedAmountPass = -floatval($removedAmount);

        $rs = Business\GenoStorage\Utils::updateStorage($container, $currentBatch, $removedAmountPass, "Update Batch (Geno form > Remove stock)");

        if ($rs == 0) {
            return 0;
        }
        return 1;
    }
}
?>
