<?php
namespace Leep\AdminBundle\Business\GenoStorage;

class Constant {
    // Status
    const STATUS_EMPTY =        10;
    const STATUS_MEDIUM =       20;
    const STATUS_HIGH =         30;

    public static function getStatus() {
        return array(
            self::STATUS_EMPTY =>       'Empty',
            self::STATUS_MEDIUM =>      'Medium',
            self::STATUS_HIGH =>        'High'
        );
    }
}
