<?php
namespace Leep\AdminBundle\Business\GenoStorage;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('createdDate', 'name', 'status', 'loading', 'description', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Created date', 'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Name', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Total load', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Description', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action', 'width' => '20%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GenoStorageGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoStorages', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellStatus($row) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $result = [
            'empty' =>  0,
            'medium' => 0,
            'high' =>   0
        ];

        $defaultWarningThreshold = $row->getWarningThreshold();
        $cells = $em->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findByStorageId($row->getId());

        foreach ($cells as $cell) {
            $warningThreshold = $defaultWarningThreshold;
            $cellThreshold = $cell->getWarningThreshold();
            if (!empty($cellThreshold)) {
                $warningThreshold = $cellThreshold;
            }

            if ($cell->getLoading() <= 0) {
                $result['empty']++;
            } else if ($cell->getLoading() > $warningThreshold) {
                $result['high']++;
            } else {
                $result['medium']++;
            }
        }

        return sprintf("<font color='green'>%s empty</font><br/><font color='blue'>%s medium</font><br/><font color='red'>%s high</font>",
            $result['empty'], $result['medium'], $result['high']);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoStorageModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_storage', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');

            // Check loading before delete
            if ($row->getLoading() <= 0) {
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_storage', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
            }
        }

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'geno_storage', 'feature', 'viewLog', array('id' => $row->getId())), 'button-detail');
        return $builder->getHtml();
    }
}
