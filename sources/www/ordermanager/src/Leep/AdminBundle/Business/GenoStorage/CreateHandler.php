<?php
namespace Leep\AdminBundle\Business\GenoStorage;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // Common
        $builder->add('storageSection', 'section', [
            'label' =>          'Storage information',
            'property_path' =>  false
        ]);

        $builder->add('name', 'text', [
            'label' =>      'Location',
            'required' =>   true,
        ]);

        $builder->add('warningThreshold', 'text', [
            'label' =>      'Default warning threshold',
        ]);

        $builder->add('description', 'textarea', [
            'label' =>      'Notes',
            'required' =>   false,
            'attr' => [
                'style' =>  'width:600px',
                'rows' =>   10
            ]
        ]);

        // Cell
        $builder->add('storageLevelSection', 'section', [
            'label' =>          'Storage\'s level',
            'property_path' =>  false
        ]);

        $builder->add('storageCells', 'container_collection', array(
            'type' => $this->container->get('leep_admin.form_types.app_storage_cell'),
            'label' => 'Level',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // Validate unique name
        $modelName = trim($model->name);
        $check = $em->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneByName($modelName);
        if ($check) {
            $this->errors[] = "Error in field Location";
            $this->getForm()->get('name')->addError(new FormError("Storage name $modelName already exist!"));
            return false;
        }

        // Create storage
        $storage = new Entity\GenoStorages();
        $storage->setCreatedDate(new \DateTime());
        $storage->setName($modelName);
        $storage->setWarningThreshold(intval($model->warningThreshold));
        $storage->setDescription($model->description);
        $storage->setLoading(0);

        $em->persist($storage);
        $em->flush();

        // Create storage cell
        $newStorageId = $storage->getId();

        if ($model->storageCells) {
            $cellNameList = [];
            foreach ($model->storageCells as $cellData) {
                $cellName = trim($cellData['name']);

                // validation
                if (in_array($cellName, $cellNameList)) {
                    $this->errors[] = "Level name $cellName already exist!";
                    return false;
                }

                $cellNameList[] = $cellName;

                // Insert into table
                $cell = new Entity\GenoStorageCell();
                $cell->setLevel($cellData['level'] - 1);
                $cell->setWarningThreshold(intval($cellData['warningThreshold']));
                $cell->setName($cellData['name']);
                $cell->setStorageId($newStorageId);
                $cell->setLoading(0);

                $em->persist($cell);
            }

            $em->flush();
        }

        parent::onSuccess();
    }
}
