<?php
namespace Leep\AdminBundle\Business\GenoStorage\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppStorageCell extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_storage_cell';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        if ($data['loading'] > 0) {
            $view->vars['warning'] = "Warning: This level is in use and cannot be deleted!";
            $view->vars['disable-delete'] = true;
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');

        $builder->add('level', 'text', [
            'label'    => 'Level',
            'required' => false,
            'attr' => [
                'class' =>      'level',
                'style' =>      'width: 25px; opacity: 0.7',
                'readonly' =>   'readonly'
            ]
        ]);

        $builder->add('loading', 'text', [
            'label'    => 'Loading',
            'required' => false,
            'attr' => [
                'class' =>      'level',
                'style' =>      'width: 40px; opacity: 0.7',
                'readonly' =>   'readonly'
            ]
        ]);

        $builder->add('name', 'text', [
            'label'    => 'Name',
            'required' => true,
            'attr' => [
                'style' =>      'width: 100px;'
            ]
        ]);

        $builder->add('warningThreshold', 'text', [
            'label'    => 'Warning threshold',
            'required' => false,
            'attr' => [
                'style' =>      'width: 100px;'
            ]
        ]);
    }
}