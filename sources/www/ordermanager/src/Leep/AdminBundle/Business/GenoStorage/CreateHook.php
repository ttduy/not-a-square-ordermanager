<?php
    namespace Leep\AdminBundle\Business\GenoStorage;

    use Easy\ModuleBundle\Module\AbstractHook;

    class CreateHook extends AbstractHook{
        public function handle($controller, &$data, $options = array()){
            $mgr = $controller->get('easy_module.manager');
            $data['visualizedLoadPage'] = $mgr->getUrl('leep_admin', 'geno_storage', 'feature', 'visualizeLoad');
        }
    }
 ?>
