<?php
namespace Leep\AdminBundle\Business\GenoStorage;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', [
            'label' =>      'Name',
            'required' =>   false
        ]);

        return $builder;
    }
}