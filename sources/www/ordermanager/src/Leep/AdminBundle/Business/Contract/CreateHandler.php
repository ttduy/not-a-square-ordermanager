<?php
// create handler for contract tracker
namespace Leep\AdminBundle\Business\LeadType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
	public function getDefaultFormModel() {
		$model = new CreateModel();
		return $model;
	}

	public function buildForm($builder) {
		
	}
}