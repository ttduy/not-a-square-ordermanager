<?php
namespace Leep\AdminBundle\Business\CmsOrderApi;

use Leep\AdminBundle\Business\ReportApi\BaseReportApi;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateCmsContactMessageApi extends BaseReportApi {
    public function execute() {        
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $config = $this->get('leep_admin.helper.common')->getConfig();

        $data = @json_decode($request->getContent(), true);
        if (!empty($data)) {
            $contactMessage = new Entity\CmsContactMessage();
            $contactMessage->setTimestamp(new \DateTime());
            $contactMessage->setIdSource($data['idSource']);
            $contactMessage->setEmailAddress($data['email']);
            $contactMessage->setName($data['name']);
            $contactMessage->setMessage($data['message']);
            $em->persist($contactMessage);
            $em->flush();

            // Send notification
            $emails = $config->getCmsContactMessageNotificationEmailList();        
            $emailArr = explode(';', $emails);

            $subject = "[CMS] New contact messages from [".$data['email'].']';
            $this->sendNotification($subject, $data['message'], $emailArr);
        }

        return 'OK';
    }

    public function sendNotification($subject, $content, $toList) {
        try {
            $senderEmail = $this->container->getParameter('sender_email');
            $senderName  = $this->container->getParameter('sender_name');
            $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($senderEmail, $senderName)
            ->setTo($toList)
            ->setBody($content, 'text/html');

            $this->get('mailer')->send($message);
        }
        catch (\Exception $e) {
            $e->getMessage();
        }
    }
}