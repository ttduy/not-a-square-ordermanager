<?php
namespace Leep\AdminBundle\Business\CmsOrderApi;

use Leep\AdminBundle\Business\ReportApi\BaseReportApi;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateCmsOrderApi extends BaseReportApi {
    public function getValue($data, $k, $default = '') {
        return isset($data[$k]) ? $data[$k] : $default;
    }
    public function execute() {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();

        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $data = @json_decode($request->getContent(), true);

        // Register new CMS Order
        $cmsOrder = new Entity\CmsOrder();
        $cmsOrder->setTimestamp(new \DateTime());
        $cmsOrder->setIdSource($data['idSource']);
        $cmsOrder->setWebsiteName($data['websiteName']);
        $cmsOrder->setCurrencyCode($data['currencyCode']);
        $cmsOrder->setLanguageCode($data['languageCode']);
        $cmsOrder->setUserIp($data['userIp']);
        $cmsOrder->setFullname($data['fullname']);
        $cmsOrder->setBirthday($data['birthday']);
        $cmsOrder->setTelephoneNumber($data['telephoneNumber']);
        $cmsOrder->setPreviousOrderNumber($data['previousOrderNumber']);
        $cmsOrder->setEmailAddress($data['emailAddress']);
        $cmsOrder->setInvoiceFirstName($data['invoiceFirstName']);
        $cmsOrder->setInvoiceSurName($data['invoiceSurName']);
        $cmsOrder->setInvoiceStreet($data['invoiceStreet']);
        $cmsOrder->setInvoiceCity($data['invoiceCity']);
        $cmsOrder->setInvoicePostCode($data['invoicePostCode']);
        $cmsOrder->setInvoiceCountryCode($data['invoiceCountryCode']);
        $cmsOrder->setShippingIsSameInvoice($data['shippingIsSameInvoice']);
        $cmsOrder->setShippingFirstName($data['shippingFirstName']);
        $cmsOrder->setShippingSurName($data['shippingSurName']);
        $cmsOrder->setShippingStreet($data['shippingStreet']);
        $cmsOrder->setShippingCity($data['shippingCity']);
        $cmsOrder->setShippingPostCode($data['shippingPostCode']);
        $cmsOrder->setShippingCountryCode($data['shippingCountryCode']);
        $cmsOrder->setNotes($data['notes']);
        $cmsOrder->setCouponCode($data['couponCode']);
        $cmsOrder->setSubtotalAmount($data['subtotalAmount']);
        $cmsOrder->setDiscountAmount($data['discountAmount']);
        $cmsOrder->setTotalAmount($data['totalAmount']);
        $cmsOrder->setTaxAmount($data['taxAmount']);
        $cmsOrder->setGrandTotalAmount($data['grandTotalAmount']);

        $websiteName = isset($data['websiteName']) ? $data['websiteName'] : "";
        $cmsOrder->setWebsiteName($websiteName);

        $em->persist($cmsOrder);
        $em->flush();

        $orderCode = 'N-'.$cmsOrder->getId();
        $cmsOrder->setOrderCode($orderCode);
        $em->flush();

        // Products
        if (isset($data['products']) && is_array($data['products'])) {
            foreach ($data['products'] as $p) {
                $product = new Entity\CmsOrderProduct();
                $product->setIdCmsOrder($cmsOrder->getId());
                $product->setProductCode($p['code']);
                $product->setExtraData(json_encode($p['extraData']));
                $product->setPriceWithoutTax($p['priceWithoutTax']);
                $product->setPriceWithTax($p['priceWithTax']);

                $productKey = isset($data['productKey']) ? $data['productKey'] : "";
                $product->setProductKey($productKey);

                $em->persist($product);
            }
        }

        // Discounts
        if (isset($data['discounts']) && is_array($data['discounts'])) {
            foreach ($data['discounts'] as $d) {
                $discount = new Entity\CmsOrderDiscount();
                $discount->setIdCmsOrder($cmsOrder->getId());
                $discount->setTitle($d['title']);
                $discount->setDiscountCode($d['code']);
                $discount->setAmount($d['amount']);
                $em->persist($discount);
            }
        }
        $em->flush();

        // Send
        if (!isset($data['isDisableNotification'])) {
            try {
                $content = Business\CmsOrder\Utils::renderView($this->container, $cmsOrder->getId());

                $emails = $config->getCmsContactMessageNotificationEmailList();
                $emailArr = explode(';', $emails);
                $receiverEmails = array();
                $receiverEmails[] = $cmsOrder->getEmailAddress();
                foreach ($emailArr as $e) {
                    $receiverEmails[] = trim($e);
                }
                $emailAddress = implode(';', $receiverEmails);
                $senderEmail = $this->container->getParameter('sender_email');
                $senderName  = $this->container->getParameter('sender_name');

                // get email method

                $email = new Entity\EmailOutgoing();
                $email->setTitle("Your order registration (".$cmsOrder->getOrderCode().')');
                $email->setBody($content);
                $email->setRecipients($emailAddress);
                $email->setIdQueue(Business\EmailOutgoing\Constant::EMAIL_QUEUE_CMS_ORDER);
                $email->setIdEmailMethod($config->getEmailMethodCmsOrder());
                $email->setStatus(Business\EmailOutgoing\Constant::EMAIL_STATUS_OPEN);
                $email->setCreationDatetime(new \DateTime());
                // $email->setRecipientsType($data['recipe']['type']);
                // $email->setRecipientsId($data['recipe']['id']);
                $email->setIsSendNow(true);
                $email->setIsAutoRetry(true);
                $em->persist($email);
                $em->flush();
            }
            catch (\Exception $e) {

            }
        }

        return $orderCode;
    }
}
