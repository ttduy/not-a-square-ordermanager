<?php
namespace Leep\AdminBundle\Business\ResultGroup;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('options', 'textarea', array(
            'label'    => 'Options',
            'required' => true,
            'attr'     => array(
                'style'   => 'height:200px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $group = new Entity\ResultGroup();
        $group->setName($model->name);

        $em->persist($group);
        $em->flush();

        $options = explode("\n", $model->options);
        foreach ($options as $row) {
            $arr = explode(' ', $row);

            if (isset($arr[0]) && isset($arr[1])) {
                $resultOption = new Entity\Results();
                $resultOption->setGroupId($group->getId());
                $resultOption->setResult(trim($arr[0]));
                $resultOption->setCallResult(trim($arr[1]));
                $em->persist($resultOption);
            }
        }
        $em->flush();  

        parent::onSuccess();
    }
}
