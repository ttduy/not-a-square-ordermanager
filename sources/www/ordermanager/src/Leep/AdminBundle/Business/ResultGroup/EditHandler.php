<?php
namespace Leep\AdminBundle\Business\ResultGroup;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ResultGroup', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        $options = array();
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Results', 'app_main')->findBygroupid($entity->getId());
        foreach ($results as $r) {
            $options[] = $r->getResult().' '.$r->getCallResult();
        }
        $model->options = implode("\n", $options);

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('options', 'textarea', array(
            'label'    => 'Options',
            'required' => true,            
            'attr'     => array(
                'style'   => 'height:200px'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();       
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setName($model->name);
        
        // update results
        $filters = array('groupid' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:Results', $filters);
        $options = explode("\n", $model->options);
        foreach ($options as $row) {
            $arr = explode(' ', $row);

            if (isset($arr[0]) && isset($arr[1])) {
                $resultOption = new Entity\Results();
                $resultOption->setGroupId($this->entity->getId());
                $resultOption->setResult(trim($arr[0]));
                $resultOption->setCallResult(trim($arr[1]));
                $em->persist($resultOption);
            }
        }
        $em->flush();  

        parent::onSuccess();
    }
}
