<?php
namespace Leep\AdminBundle\Business\ResultGroup;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('name', 'options', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',               'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Options',            'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ResultGroup', 'p');
       
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellOptions($row) {
        $options = array();
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Results')->findBygroupid($row->getId());
        foreach ($results as $row) {
            $options[] = $row->getResult().'&nbsp;&nbsp;'.$row->getCallResult();
        }

        return implode('<br/>', $options);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('resultGroupModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'result_group', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'result_group', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }
}
