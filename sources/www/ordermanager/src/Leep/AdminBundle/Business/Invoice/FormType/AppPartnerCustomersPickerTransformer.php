<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class AppPartnerCustomersPickerTransformer implements DataTransformerInterface
{
    public $container;    

    public function __construct($container) {
        $this->container = $container;
    }

    // Loading
    public function transform($data)
    {
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');
        $customerDataSessionKey = $tempDataHelper->generateKey();
        $idPartner = empty($data) ? 0 : $data['idPartner'];

        if (!empty($data)) {
            $idInvoice = $data['idInvoice'];
            $customerSelected = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:InvoiceCustomer')->findByIdInvoice($idInvoice);
            $customerSelectedId = array();
            foreach ($customerSelected as $c) {
                $customerSelectedId[$c->getIdCustomer()] = array(
                    'idCustomer' => $c->getIdCustomer(),
                    'overridePrice' => $c->getOverridePrice(),
                    'price'         => $c->getPrice(),
                    'priceDetail'   => json_decode($c->getPriceDetail(), true),
                    'note'          => $c->getNotes()
                );
            }
            $tempDataHelper->save($customerDataSessionKey, $customerSelectedId);
        }        
        
        return array(
            'idPartner' => $idPartner,
            'customerDataSessionKey' => $customerDataSessionKey
        );        
    }

    // Parsing
    public function reverseTransform($partnerPicker)
    {        
        $customerDataSessionKey = $partnerPicker['customerDataSessionKey'];
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');

        return array(
            'idPartner'   => $partnerPicker['idPartner'],
            'customerDataSessionKey'  => $partnerPicker['customerDataSessionKey'],
            'customers'               => $tempDataHelper->get($customerDataSessionKey)
        );
    }
}
