<?php 
namespace Leep\AdminBundle\Business\Invoice;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class InvoicePartnerBuilder extends InvoiceBuilder {
    public function buildInvoiceTo($builder) {
        $builder->add('partner', 'app_partner_customers_picker', array(
            'label'    => 'Customers',
            'required' => false
        ));
        return $builder;
    }

    public function convertToFormModel($model, $invoice) {
        $model->partner = array(
            'idPartner'      => $invoice->getIdPartner(),
            'idInvoice'      => $invoice->getId()
        );
    }
    
    public function copyModelToEntity($model, $invoice) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $invoice->setIdPartner($model->partner['idPartner']);

        $filters = array('idInvoice' => $invoice->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:InvoiceCustomer', $filters);       
        foreach ($model->partner['customers'] as $idCustomer => $customer) {
            $invoiceCustomer = new Entity\InvoiceCustomer();
            $invoiceCustomer->setIdInvoice($invoice->getId());
            $invoiceCustomer->setIdCustomer($idCustomer);
            if (isset($customer['overridePrice']) && trim($customer['overridePrice']) !== '') {
                $invoiceCustomer->setOverridePrice($customer['overridePrice']);
            }
            $invoiceCustomer->setNotes(isset($customer['note']) ? $customer['note'] : '');
            $invoiceCustomer->setPrice(isset($customer['price']) ? $customer['price'] : '');            
            $invoiceCustomer->setPriceDetail(isset($customer['priceDetail']) ? $customer['priceDetail'] : array());
            $invoiceCustomer->setPriceDetail(json_encode($invoiceCustomer->getPriceDetail()));
            $em->persist($invoiceCustomer);
        }
        $em->flush();
    }

    public function buildPdfClientSection(&$data, $invoice) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $partner = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($invoice->getIdPartner());

        if ($partner) {
            $data['clientName'] = $partner->getFirstName().' '.$partner->getSurName();        
            $data['clientCompanyName'] = $partner->getInstitution();
            $data['clientUidNumber'] = $partner->getUIDNumber();

            if ($partner->getInvoiceAddressIsUsed()) {
                $data['clientStreet'] = $partner->getInvoiceAddressStreet();
                $data['clientStreet2'] = '';
                $data['clientCity'] = $partner->getInvoiceAddressCity();
                $data['clientPostCode'] = $partner->getInvoiceAddressPostCode();
                $data['clientCountry'] = $formatter->format($partner->getInvoiceAddressIdCountry(), 'mapping', 'LeepAdmin_Country_List');

                $clientName = $partner->getInvoiceClientName();
                $companyName = $partner->getInvoiceCompanyName();
                if (trim($clientName) != '') {
                    $data['clientName'] = $clientName;
                    if ($clientName == '######') {
                        $data['clientName'] = '';
                    }
                }
                if (trim($companyName) != '') {
                    $data['clientCompanyName'] = $companyName;
                    if ($companyName == '######') {
                        $data['clientCompanyName'] = '';
                    }
                }
            }
            else {
                $data['clientStreet'] = $partner->getStreet();
                $data['clientStreet2'] = '';
                $data['clientCity'] = $partner->getCity();
                $data['clientPostCode'] = $partner->getPostCode();
                $data['clientCountry'] = $formatter->format($partner->getCountryId(), 'mapping', 'LeepAdmin_Country_List');
            }
        }        
    }

    public function buildPdfOrderSection(&$data, $invoice) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $doctrine = $this->container->get('doctrine');

        $order = array(
            'orders' => array(),
            'discount' => 0
        );

        $invoiceCustomers = $doctrine->getRepository('AppDatabaseMainBundle:InvoiceCustomer')->findByIdInvoice($invoice->getId());
        foreach ($invoiceCustomers as $invoiceCustomer) {
            $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($invoiceCustomer->getIdCustomer());
            
            $products = json_decode($invoiceCustomer->getPriceDetail(), true);
            $total = 0;
            foreach ($products as $product) {                
                $order['orders'][] = array(
                    'orderNumber' => $customer->getOrderNumber(),
                    'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                    'product'     => $product['product'],
                    'firstName'   => $customer->getFirstName(),
                    'lastName'    => $customer->getSurName(),
                    'amount'      => $product['amount'],
                    'sum'         => $formatter->format($product['amount'], 'money')
                );
                $total += floatval($product['amount']);
            }

            $overridePrice = floatval($invoiceCustomer->getOverridePrice());
            if (!empty($overridePrice)) {
                $discount = $total - $overridePrice;
                $order['discount'] += $discount;
            }
        }

        return $order;
    }    
}
