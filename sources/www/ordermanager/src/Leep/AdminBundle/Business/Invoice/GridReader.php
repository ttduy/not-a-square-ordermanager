<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }
    public function getColumnMapping() {
        return array('invoiceDate', 'idInvoiceType', 'name', 'number', 'totalAmount', 'currentStatus', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Date',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Type',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Name',             'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Number',           'width' => '15%', 'sortable' => 'true'),            
            array('title' => 'Amount',           'width' => '10%', 'sortable' => 'true'),    
            array('title' => 'Status',           'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Action',           'width' => '15%', 'sortable' => 'false')            
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.invoiceDate';
        $this->columnSortMapping[] = 'p.idInvoiceType';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.number';
        $this->columnSortMapping[] = 'p.totalAmount';
        $this->columnSortMapping[] = 'p.currentStatus';

        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        //$queryBuilder->orderBy('p.invoiceDate', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_InvoiceReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Invoice', 'p');
        
        if ($this->filters->idInvoiceType != 0) {
            $queryBuilder->andWhere('p.idInvoiceType = :idInvoiceType')
                ->setParameter('idInvoiceType', $this->filters->idInvoiceType);
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->number) != '') {
            $queryBuilder->andWhere('p.number LIKE :number')
                ->setParameter('number', '%'.trim($this->filters->number).'%');
        }
        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.invoiceDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }        
        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.invoiceDate <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }        
        if (!empty($this->filters->currentStatus)) {
            $queryBuilder->andWhere('p.currentStatus in (:currentStatus)')
                ->setParameter('currentStatus', $this->filters->currentStatus);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('integratedInvoiceModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'invoice', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');        
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'invoice', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }


    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellTotalAmount($row) {
        return "&euro;".number_format(floatval($row->getTotalAmount()), 2);
    }

    public function buildCellCurrentStatus($row) {        
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Invoice_Status', $row->getCurrentStatus());        
        $age = $this->getAge($row->getCurrentStatusDate()); 

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();             

        return $builder->getHtml().'&nbsp;'.(($age === '') ? '': "(${age}d) ").$status;
    }
}
