<?php
namespace Leep\AdminBundle\Business\Invoice;

class InvoiceGenerator {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getInvoiceBuilder($idInvoiceType) {
        $invoiceBuilders = Constant::getInvoiceBuilders();
        if (isset($invoiceBuilders[$idInvoiceType])) {
            return $this->container->get($invoiceBuilders[$idInvoiceType]);
        }
        return null;
    }

    public function generateInvoice($invoice) {
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $doctrine = $this->container->get('doctrine');
        $today = new \DateTime();
        $data = array();

        // Load invoice
        $invoiceBuilder = $this->getInvoiceBuilder($invoice->getIdInvoiceType());

        /////////////////////////////////
        // x. Company        
        $data['companyLogo'] = $this->container->getParameter('kernel.root_dir').'/../logo.png';        
        $data['companyName'] = '';
        $data['companyStreet'] = '';
        $data['companyPostCode'] = '';
        $data['companyCity'] = '';
        $data['companyEmail'] = '';
        $data['companyTelephone'] = '';
        $data['companyWebSite'] = '';

        $data['companyBank'] = '';
        $data['companyBankAccount'] = '';
        $data['companyBankCode'] = '';
        $data['companyBic'] = '';
        $data['companyIban'] = '';
        $data['companyTradeRegister'] = '';
        $data['companyJurisdiction'] = '';
        $data['companyUidNumber'] = '';
        $data['companyTaxNumber'] = '';
        $data['companyLegalForm'] = '';
        $data['companyManager'] = '';


        $company = $doctrine->getRepository('AppDatabaseMainBundle:Companies')->findOneById($invoice->getIdFromCompany());
        $data['company'] = $company;
        if ($company) {
            if ($company->getLogo()) {
                $data['companyLogo'] = $this->container->getParameter('kernel.root_dir').'/../web/attachments/logo/'.$company->getLogo();
            }
            $data['companyName'] = $company->getCompanyName();
            $data['companyStreet'] = $company->getStreet();
            $data['companyPostCode'] = $company->getPostCode();
            $data['companyCity'] = $company->getCity();
            $data['companyEmail'] = $company->getEmail();
            $data['companyTelephone'] = $company->getTelephone();
            $data['companyWebSite'] = $company->getWebSite();

            $data['companyBank'] = $company->getBank();
            $data['companyBankAccount'] = $company->getBankAccount();
            $data['companyBankCode'] = $company->getBankCode();
            $data['companyBic'] = $company->getBic();
            $data['companyIban'] = $company->getIban();
            $data['companyTradeRegister'] = $company->getTradeRegister();
            $data['companyJurisdiction'] = $company->getJurisdiction();
            $data['companyUidNumber'] = $company->getUidNumber();
            $data['companyTaxNumber'] = $company->getTaxNumber();
            $data['companyLegalForm'] = $company->getLegalForm();
            $data['companyManager'] = $company->getManager();
        }
        
        /////////////////////////////////
        // x. Client
        $data['clientName'] = '';
        $data['clientCompanyName'] = '';
        $data['clientStreet'] = '';
        $data['clientStreet2'] = '';
        $data['clientCity'] = '';
        $data['clientPostCode'] = '';
        $data['clientCountry'] = '';
        $data['clientUidNumber'] = '';

        $invoiceBuilder->buildPdfClientSection($data, $invoice);

        /////////////////////////////////
        // x. Invoice information        
        $data['invoiceDate'] = $formatter->format($invoice->getInvoiceDate(), 'date'); 
        $data['invoiceNumber'] = $invoice->getNumber();

        /////////////////////////////////
        // x. Order information
        $order = $invoiceBuilder->buildPdfOrderSection($data, $invoice);
        $data['order'] = $order['orders'];

        $subTotal = 0;
        foreach ($order['orders'] as $row) {
            $subTotal += floatval($row['amount']);
        }
        $postage = floatval($invoice->getPostage());
        $discount = floatval($order['discount']);
        $total = $subTotal + $postage - $discount;

        $isTax = $invoice->getIsTax();
        $tax = 0;
        $grandTotal = $total;
        if ($invoice->getIsTax()) {
            $tax = $total * floatval($invoice->getTaxPercentage()) / 100;
            $grandTotal = $total + $tax;
        }        
        
        $data['subtotal']      = $formatter->format($subTotal, 'money');
        $data['postage']       = $formatter->format($postage, 'money');
        $data['discount']      = $formatter->format($discount, 'money');
        $data['total']         = $formatter->format($total, 'money');
        $data['isTax']         = $isTax;
        $data['taxPercentage'] = $invoice->getTaxPercentage();
        $data['tax']           = $formatter->format($tax, 'money');
        $data['totalWithTax']  = $formatter->format($grandTotal, 'money');

        // Currency conversion
        if ($invoice->getIdCurrency()) {
            $currency = $doctrine->getRepository('AppDatabaseMainBundle:Currency')->findOneById($invoice->getIdCurrency());
            if ($currency) {
                $exchangeRate = floatval($invoice->getExchangeRate());
                $data['currency'] = $currency;
                $data['exchangeRate'] = $exchangeRate;
                $totalConverted = $exchangeRate * $grandTotal;
                $data['totalConverted'] = $formatter->format($totalConverted, 'money', $currency->getSymbol());
            }
        }

        /////////////////////////////////
        // x. Extra text
        $data['textAfterDescription'] = '';
        $data['textFooter'] = '';


        /////////////////////////////////
        // x. Generate PDF
        $invoicePdfTemplate = $this->container->get('templating')->render('LeepAdminBundle:Invoice:pdf.html.twig', $data);
        $dompdf = $this->container->get('slik_dompdf');
        $dompdf->getpdf($invoicePdfTemplate);

        $filePath = $this->container->getParameter('files_dir').'/invoices';
        $fileName = 'invoice_'.$invoice->getId().'_'.$today->format('YmdHis').'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        return array(
            'file' => $fileName,
            'grandTotal' => $grandTotal
        );
    }
}