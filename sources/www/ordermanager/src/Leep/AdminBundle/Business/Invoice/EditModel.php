<?php
namespace Leep\AdminBundle\Business\Invoice;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    // Invoice information    
    public $invoiceDate;
    public $name;
    public $number;
    public $idInvoiceType;
    
    // From
    public $idFromCompany;

    // To
    public $distributionChannel;
    
    public $idPartner;
    public $idDistributionChannel;
    public $idMwaGroup;
    
    public $idCustomer;
    public $customerList;
    public $mwaSampleList;

    // Extra amount
    public $postage;
    public $tax;
    public $currencyRate;

    // Extra text
    public $extraTextList;

    // Status
    public $statusList;

    // Invoice pdf
    public $invoicePdf;
}
