<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppCustomerPicker extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'path' => 'logo'
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_customer_picker';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');

        $idCustomer = $form->getData();

        $doctrine = $this->container->get('doctrine');
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if ($customer) {
            $view->vars['customer'] = array(                
                'id'          => $idCustomer,
                'orderNumber' => $customer->getOrderNumber(),
            );
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idCustomer',    'hidden');

        $builder->addModelTransformer(new AppCustomerPickerTransformer($this->container));
    }
}