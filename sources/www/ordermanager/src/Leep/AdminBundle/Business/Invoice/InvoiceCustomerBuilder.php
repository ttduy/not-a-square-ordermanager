<?php 
namespace Leep\AdminBundle\Business\Invoice;

class InvoiceCustomerBuilder extends InvoiceBuilder {
    public function buildInvoiceTo($builder) {        
        $builder->add('idCustomer', 'app_customer_picker', array(
            'label'    => 'Customer',
            'required' => false
        ));                
    }

    public function convertToFormModel($model, $invoice) {
        $model->idCustomer = $invoice->getIdCustomer();
    }
    
    public function copyModelToEntity($model, $invoice) {
    	$invoice->setIdCustomer($model->idCustomer);
    }
}
