<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class AppDistributionChannelCustomersPickerTransformer implements DataTransformerInterface
{
    public $container;    

    public function __construct($container) {
        $this->container = $container;
    }

    // Loading
    public function transform($data)
    {
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');
        $customerDataSessionKey = $tempDataHelper->generateKey();
        $idDistributionChannel = empty($data) ? 0 : $data['idDistributionChannel'];

        if (!empty($data)) {
            $idInvoice = $data['idInvoice'];
            $customerSelected = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:InvoiceCustomer')->findByIdInvoice($idInvoice);
            $customerSelectedId = array();
            foreach ($customerSelected as $c) {
                $customerSelectedId[$c->getIdCustomer()] = array(
                    'idCustomer' => $c->getIdCustomer(),
                    'overridePrice' => $c->getOverridePrice(),
                    'price'         => $c->getPrice(),
                    'priceDetail'   => json_decode($c->getPriceDetail(), true),
                    'note'          => $c->getNotes()
                );
            }
            $tempDataHelper->save($customerDataSessionKey, $customerSelectedId);
        }        
        
        return array(
            'idDistributionChannel' => $idDistributionChannel,
            'customerDataSessionKey' => $customerDataSessionKey
        );        
    }

    // Parsing
    public function reverseTransform($distributionChannelPickers)
    {        
        $customerDataSessionKey = $distributionChannelPickers['customerDataSessionKey'];
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');

        return array(
            'idDistributionChannel'   => $distributionChannelPickers['idDistributionChannel'],
            'customerDataSessionKey'  => $distributionChannelPickers['customerDataSessionKey'],
            'customers'               => $tempDataHelper->get($customerDataSessionKey)
        );
    }
}
