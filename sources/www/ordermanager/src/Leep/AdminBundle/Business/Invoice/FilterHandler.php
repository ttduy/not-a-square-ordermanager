<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->currentStatus = array();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idInvoiceType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Invoice_Type')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => false
        ));        
        $builder->add('currentStatus', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'empty_value' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'   => 'height: 80px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));
        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start Date',
            'required' => false
        ));        
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End Date',
            'required' => false
        ));        

        return $builder;
    }
}