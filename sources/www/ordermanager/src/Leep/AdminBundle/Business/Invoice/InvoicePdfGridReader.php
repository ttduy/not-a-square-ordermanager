<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class InvoicePdfGridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('generateDate', 'totalAmount', 'action');
    }
    public function getTableHeader() {
        return array(       
            array('title' => 'Generate date',      'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Total amount',       'width' => '25%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '40%', 'sortable' => 'false')                            
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_InvoicePdfGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.generateDate', 'DESC');
        $queryBuilder->setFirstResult(0);        
        $queryBuilder->setMaxResults(5000);
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:InvoicePdf', 'p');

        // Already selected customer filters
        $request = $this->container->get('request');
        $idInvoice = $request->get('idInvoice', '');        
        $queryBuilder->andWhere('p.idInvoice = :idInvoice')
            ->setParameter('idInvoice', $idInvoice);
    }

    public function buildCellTotalAmount($row) {
        return '&euro;'.number_format($row->getTotalAmount(), 2);
    }

    public function buildCellAction($row) {
        $mgr = $this->getModuleManager();
        $url = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'viewInvoice', array('id' => $row->getId()));

        return "<a target='_blank' href='".$url."'>Download</a>";
    }
}
