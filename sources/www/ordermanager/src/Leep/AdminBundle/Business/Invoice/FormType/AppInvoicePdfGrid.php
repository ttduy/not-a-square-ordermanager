<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppInvoicePdfGrid extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_invoice_pdf_grid';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mgr = $this->container->get('easy_module.manager');        

        $request = $this->container->get('request');

        $view->vars['generateInvoiceUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'generateInvoice', array('id' => $request->get('id')));

        $invoicePdfGrid = $this->container->get('leep_admin.invoice.business.invoice_pdf_grid_reader');
        $view->vars['invoicePdfGrid'] = array(
            'id' => 'InvoicePdfGrid',
            'title' => 'Invoice files',
            'header' => $invoicePdfGrid->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'invoice', 'feature', 'invoicePdfAjaxSource', array(
                'idInvoice' => $request->get('id', 0)
            ))
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        
    }
}