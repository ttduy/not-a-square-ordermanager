<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class CustomerPickerFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new CustomerPickerFilterModel();
        $model->isShowReportSubmittedOnly = true;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));        
        $builder->add('isShowReportSubmittedOnly', 'checkbox', array(
            'label'    => 'Show report submitted only',
            'required' => false
        ));

        $builder->add('firstname', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surname', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        
        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));
        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));
        return $builder;
    }
}