<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class CustomerViewerGridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $dcCell = array();
    public $dcPartners = array();
    public $customerReportInfo = array();
    public $customers = array();
    public $formatter;

    public function __construct($container) {
        parent::__construct($container);

        $this->formatter = $this->container->get('leep_admin.helper.formatter');

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            }
            else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }
    }

    public function getColumnMapping() {
        return array('orderNumber', 'dateordered', 'name', 'distributionChannelId', 'status', 'price', 'overridePrice', 'notes');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';

        return $this->columnSortMapping;
    }
    
    public function getTableHeader() {
        return array(       
            array('title' => 'Order Number',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Date ordered',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',                 'width' => '15%', 'sortable' => 'false'),     
            array('title' => 'Distribution Channel', 'width' => '10%', 'sortable' => 'false'),     
            array('title' => 'Status',               'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Price',                'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Override price',       'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Notes',                'width' => '20%', 'sortable' => 'false'),                             
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CustomerViewerGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->setFirstResult(0);        
        $queryBuilder->setMaxResults(5000);
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        // Already selected customer filters
        $request = $this->container->get('request');
        $customerDataSessionKey = $request->get('customerDataSessionKey', '');        
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');
        $this->customers = $tempDataHelper->get($customerDataSessionKey, array());
        if (empty($this->customers)) {
            $this->customers = array(0 => 0);
        }        
        $queryBuilder->andWhere('p.id IN (:selectedCustomersId)')
            ->setParameter('selectedCustomersId', array_keys($this->customers));
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            //$builder->addButton('Update status', $mgr->getUrl('leep_admin', 'customer', 'edit_status', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());
        
        $age = $this->getAge($row->getStatusDate());        
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellDistributionChannelId($row) {
        return isset($this->dcCell[$row->getDistributionChannelId()]) ? $this->dcCell[$row->getDistributionChannelId()] : '';
    }


    public function buildCellPrice($row) {
        $amount = 0;
        $idCustomer = $row->getId();
        if (isset($this->customers[$idCustomer]['price'])) {
            $amount = $this->customers[$idCustomer]['price'];
        }
        $amountHtml = $this->formatter->format($amount, 'money');

        $infoText = '';
        if (isset($this->customers[$idCustomer]['priceDetail'])) {
            $tpl =   "
            <div>
                <div style='float:left; width: 250px'>%s:</div>
                <div style='float:left;'>%s</div>
                <div style='clear:both'></div>
            </div>";
            
            foreach ($this->customers[$idCustomer]['priceDetail'] as $row) {
                $amount = $this->formatter->format($row['amount'], 'money');
                $infoText .= sprintf($tpl, $row['product'], $amount);
            }
        }
        $html = '<a class="simpletip" href="#" onclick="return false">'.$amountHtml.'<div class="hide-content">'.$infoText.'</div></a>';
        return $html;
    }
    public function buildCellOverridePrice($row) {
        $idCustomer = $row->getId();
        $value = '';
        if (isset($this->customers[$idCustomer])) {
            if (isset($this->customers[$idCustomer]['overridePrice']))
            $value = $this->customers[$idCustomer]['overridePrice'];
        }
        return '<input type="textbox" class="invoiceCustomerGridField" field="overridePrice" idCustomer="'.$row->getId().'" style="max-width: 100px" id="override_price_'.$row->getId().'" value="'.$value.'" />';
    }
    public function buildCellNotes($row) {
        $idCustomer = $row->getId();
        $value = '';
        if (isset($this->customers[$idCustomer])) {
            if (isset($this->customers[$idCustomer]['note']))
            $value = $this->customers[$idCustomer]['note'];
        }
        return '<input type="textbox" class="invoiceCustomerGridField" field="note" idCustomer="'.$row->getId().'" id="notes_'.$row->getId().'" value="'.$value.'" />';
    }
}
