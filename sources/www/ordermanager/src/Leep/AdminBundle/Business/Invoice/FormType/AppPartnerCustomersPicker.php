<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppPartnerCustomersPicker extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_partner_customers_picker';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $request = $this->container->get('request');
        $mgr = $this->container->get('easy_module.manager');        

        $customerDataSessionKey = $form['customerDataSessionKey']->getData();

        $view->vars['customersPickerUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'pickCustomers', array(
            'customerDataSessionKey' => $customerDataSessionKey,
            'idInvoice'              => $request->get('id', 0),
            'mode'                   => 'partner'
        ));        
        $view->vars['removeCustomersUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'removeCustomers', array(
            'customerDataSessionKey' => $customerDataSessionKey
        ));
        $view->vars['updateCustomerDataUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'updateCustomerData', array(
            'customerDataSessionKey' => $customerDataSessionKey
        ));
        
        $customerGridViewer = $this->container->get('leep_admin.invoice.business.customer_viewer_grid_reader');
        $view->vars['customerGridViewer'] = array(
            'id' => 'CustomerGridViewer',
            'title' => 'Customer',
            'header' => $customerGridViewer->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'invoice', 'feature', 'viewCustomersAjaxSource', array(
                'customerDataSessionKey' => $customerDataSessionKey
            ))
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idPartner', 'choice', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => $this->getOpenPartners()
        ));
        $builder->add('customerDataSessionKey',    'hidden');

        $builder->addModelTransformer(new AppPartnerCustomersPickerTransformer($this->container));
    }

    protected function getOpenPartners() {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $this->container->get('request');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        $idInvoice = $request->get('id', 0);

        $arr = array();
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.partner, COUNT(c.id) as number')
            ->from('AppDatabaseMainBundle:Customer', 'c')            
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->innerJoin('AppDatabaseMainBundle:Partner', 'p', 'WITH', 'dc.partnerid = p.id')
            ->leftJoin('AppDatabaseMainBundle:InvoiceCustomer', 'ic', 'WITH', 'ic.idCustomer = c.id')
            ->andWhere('(ic.id IS NULL) or (ic.idInvoice = :idInvoice)')            
            ->andWhere('c.invoicegoestoid = :invoiceGoesToPartner')
            ->setParameter('idInvoice', $idInvoice)
            ->setParameter('invoiceGoesToPartner', Business\DistributionChannel\Constant::INVOICE_GOES_TO_PARTNER)
            ->orderBy('p.partner', 'ASC')
            ->groupBy('p.id');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $arr[$row['id']] = array(
                'name' => $row['partner'],
                'open' => $row['number'],
                'submit' => 0
            );
        }

        $reportSubmittedStatus = 8; // Fix me
        $query->andWhere('FIND_IN_SET('.$reportSubmittedStatus.', c.statusList) > 0');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $idPartner = $row['id'];
            if (isset($arr[$idPartner])) {
                $arr[$idPartner]['submit'] = $row['number'];
            }            
        }

        $partners = array();
        foreach ($arr as $idPartner => $v) {
            $partners[$idPartner] = $v['name'].' ('.$v['open'].' open / '.$v['submit'].' submitted)';
        }
        return $partners;
    }
}