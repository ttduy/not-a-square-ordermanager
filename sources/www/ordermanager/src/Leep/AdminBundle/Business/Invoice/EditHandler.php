<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {   
    public $invoiceBuilder = null;

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Invoice', 'app_main')->findOneById($id);
    }

    public function getInvoiceBuilder($idInvoiceType) {
        $invoiceBuilders = Constant::getInvoiceBuilders();
        if (isset($invoiceBuilders[$idInvoiceType])) {
            return $this->container->get($invoiceBuilders[$idInvoiceType]);
        }
        return null;
    }

    public function convertToFormModel($invoice) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $model = new EditModel();  
        // Invoice information
        $model->name = $invoice->getName();
        $model->number = $invoice->getNumber();
        $model->idInvoiceType = $formatter->format($invoice->getIdInvoiceType(), 'mapping', 'LeepAdmin_Invoice_Type');
        $model->invoiceDate = $invoice->getInvoiceDate();

        $this->invoiceBuilder = $this->getInvoiceBuilder($invoice->getIdInvoiceType());

        // From
        $model->idFromCompany = $invoice->getIdFromCompany();

        // To
        $this->invoiceBuilder->convertToFormModel($model, $invoice);     
        $model->idCustomer = 12103;   

        // Extra amount
        $model->postage = $invoice->getPostage();
        $model->tax = new Business\FormType\AppInvoiceTaxModel();
        $model->tax->isTax = $invoice->getIsTax();
        $model->tax->taxPercentage = $invoice->getTaxPercentage();
        $model->currencyRate = new Business\FormType\AppCurrencyRateModel();
        $model->currencyRate->idCurrency = $invoice->getIdCurrency();
        $model->currencyRate->exchangeRate = $invoice->getExchangeRate();             

        // Extra text
        //$model->extraTextList = array();


        // Status        
        $model->statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:InvoiceChangeStatus', 'p')
            ->andWhere('p.idInvoice = :idInvoice')
            ->setParameter('idInvoice', $invoice->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppInvoiceStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }        

        // Invoice pdf
        $model->pdfList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:InvoicePdf', 'p')
            ->andWhere('p.idInvoice = :idInvoice')
            ->setParameter('idInvoice', $invoice->getId())
            ->orderBy('p.sortOrder', 'ASC');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');          

        // Invoice information        
        $builder->add('sectionInvoiceInformation', 'section', array(
            'label'    => 'Invoice information',
            'property_path' => false
        ));
        $builder->add('idInvoiceType', 'label', array(
            'label'    => 'Type',
            'required' => false
        )); 
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));                
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));                
        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => true
        )); 
        

        // From
        $builder->add('sectionFrom', 'section', array(
            'label'    => 'Invoice from',
            'property_path' => false
        ));
        $builder->add('idFromCompany', 'choice', array(
            'label'    => 'Company',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));

        // To
        $builder->add('sectionTo', 'section', array(
            'label'    => 'Invoice to',
            'property_path' => false
        ));
        $builder = $this->invoiceBuilder->buildInvoiceTo($builder);

        // Extra amount
        $builder->add('sectionExtraAmount', 'section', array(
            'label'    => 'Extra amount',
            'property_path' => false
        ));
        $builder->add('postage', 'text', array(
            'label'    => 'Postage',
            'required' => false
        ));
        $builder->add('tax', 'app_invoice_tax', array(
            'label'    => 'Tax',
            'required' => false
        ));
        $builder->add('currencyRate', 'app_currency_rate', array(
            'label' => 'Currency convert',            
            'required' => false
        ));

        // Extra text

        // Status        
        $builder->add('sectionStatus', 'section', array(
            'label'    => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new Business\FormType\AppInvoiceStatusRow($this->container)
        ));

        // PDF        
        $builder->add('sectionInvoiceFile', 'section', array(
            'label'    => 'Files',
            'property_path' => false
        ));
        $builder->add('invoicePdf', 'app_invoice_pdf_grid', array(
            'label'    => 'Invoice PDF',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');

        ////////////////////////////////////////
        // x. EXTRA VALIDATION

        ////////////////////////////////////////
        // x. SAVE INVOICE
        // Invoice information
        $this->entity->setInvoiceDate($model->invoiceDate);
        $this->entity->setName($model->name);
        $this->entity->setNumber($model->number);
        
        // From
        $this->entity->setIdFromCompany($model->idFromCompany);
        
        // To        
        $this->invoiceBuilder->copyModelToEntity($model, $this->entity);   

        // Extra amount
        $this->entity->setPostage($model->postage);
        if ($model->tax) {
        	$this->entity->setIsTax($model->tax->isTax);
        	$this->entity->setTaxPercentage($model->tax->taxPercentage);
        }
        if ($model->currencyRate) {
        	$this->entity->setIdCurrency($model->currencyRate->idCurrency);
        	$this->entity->setExchangeRate($model->currencyRate->exchangeRate);
        }
        
        // Extra text
        
        
        // Status              
        $filters = array('idInvoice' => $this->entity->getId());
        
        $dbHelper->delete($em, 'AppDatabaseMainBundle:InvoiceChangeStatus', $filters);                
        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\InvoiceChangeStatus();
            $status->setIdInvoice($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
                
            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }
        $this->entity->setCurrentStatus($lastStatus);
        $this->entity->setCurrentStatusDate($lastDate);
        
        parent::onSuccess();
    }
}
