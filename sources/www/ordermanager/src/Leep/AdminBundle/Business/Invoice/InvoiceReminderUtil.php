<?php 
namespace Leep\AdminBundle\Business\Invoice;

use Symfony\Component\HttpFoundation\Response;
use Leep\AdminBundle\Business;

class InvoiceReminderUtil {
    public static function buildPdf($controller, $invoice, $type) {
        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';
        $em = $controller->get('doctrine')->getEntityManager();        
        $formatter = $controller->get('leep_admin.helper.formatter');

        $invoiceFile = $filePath.'/'.$invoice->getInvoiceFile();
        $invoiceDate = $invoice->getInvoiceDate();

        if (!is_file($invoiceFile)) {
            die("Please generate invoice first");
        }
        if (empty($invoiceDate)) {
            die("Please choose invoice date");
        }

        switch ($type) {
            case 'collective_invoice':
                $data = Business\CollectiveInvoice\Utils::buildPdf($controller, $invoice->getId(), true);
                $data['companyLogo'] = $data['logoImagePath'];
                $data['clientR1'] = $data['clientName'];
                $data['clientR2'] = $data['clientCompanyName'];
                $data['clientR3'] = $data['clientStreet'];
                $data['clientR4'] = $data['clientPostCode'].' '.$data['clientCity'];
                $data['clientR5'] = $data['clientCountry'];

                $data['invoiceAmount'] = $formatter->format($invoice->getInvoiceAmount(), 'money');
                break;
            case 'customer_invoice':
                $data = Business\Customer\InvoiceUtil::buildPdf($controller, $invoice->getId(), true);
                $data['companyLogo'] = $data['logoImagePath'];
                $data['clientR1'] = '';
                $data['clientR2'] = $data['clientName'];
                $data['clientR3'] = $data['clientStreet'];
                $data['clientR4'] = $data['clientPostCode'].' '.$data['clientCity'];
                $data['clientR5'] = $data['clientCountry'];

                $data['invoiceAmount'] = $formatter->format($invoice->getInvoiceAmount(), 'money');
                break;
            case 'mwa_invoice':
                $data = Business\MwaInvoiceForm\InvoiceUtil::buildPdf($controller, $invoice->getId(), true);
                $data['companyLogo'] = $data['logoImagePath'];

                $data['invoiceAmount'] = $formatter->format($invoice->getInvoiceAmount(), 'money');
                break;
        }
        $today = new \DateTime();
        $ts = $today->getTimestamp() - $invoiceDate->getTimestamp();
        $data['invoiceDays'] = intval($ts / (24*60*60));

        $invoicePdfTemplate = $controller->get('templating')->render('LeepAdminBundle:Invoice:reminder_pdf.html.twig', $data);
        $dompdf = $controller->get('slik_dompdf');
        $dompdf->getpdf($invoicePdfTemplate);

        // Save to file
        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';
        $fileName = 'reminder_'.$type.'_'.$invoice->getId().'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        // Combine two pdf
        $pdf = $controller->get('white_october.tcpdf')->create();   
        $pdf->setPrintFooter(false);
        $pdf->setPrintHeader(false);
        $numPage = $pdf->setSourceFile($outputFile);
        for ($i = 1; $i <= $numPage; $i++) {
            $pdf->AddPage();
            $tplId = $pdf->importPage($i);
            $pdf->useTemplate($tplId);
        }        
        $numPage = $pdf->setSourceFile($invoiceFile);
        for ($i = 1; $i <= $numPage; $i++) {
            $pdf->AddPage();
            $tplId = $pdf->importPage($i);
            $pdf->useTemplate($tplId);
        }        


        $resp = new Response();
        $resp->headers->set('Content-Type', 'application/pdf; charset=UTF-8');
        $resp->headers->set('Content-Disposition', 'attachment; filename=reminder_'.$invoice->getInvoiceNumber().'.pdf');
        $resp->setContent($pdf->Output('', 'S'));

        return $resp;
    }

}