<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppDistributionChannelCustomersPicker extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_distribution_channel_customers_picker';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $request = $this->container->get('request');
        $mgr = $this->container->get('easy_module.manager');        

        $customerDataSessionKey = $form['customerDataSessionKey']->getData();

        $view->vars['customersPickerUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'pickCustomers', array(
            'customerDataSessionKey' => $customerDataSessionKey,
            'idInvoice'              => $request->get('id', 0),
            'mode'                   => 'distributionChannel'
        ));        
        $view->vars['removeCustomersUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'removeCustomers', array(
            'customerDataSessionKey' => $customerDataSessionKey
        ));
        $view->vars['updateCustomerDataUrl'] = $mgr->getUrl('leep_admin', 'invoice', 'feature', 'updateCustomerData', array(
            'customerDataSessionKey' => $customerDataSessionKey
        ));
        
        $customerGridViewer = $this->container->get('leep_admin.invoice.business.customer_viewer_grid_reader');
        $view->vars['customerGridViewer'] = array(
            'id' => 'CustomerGridViewer',
            'title' => 'Customer',
            'header' => $customerGridViewer->getTableHeader(),
            'ajaxSource' => $mgr->getUrl('leep_admin', 'invoice', 'feature', 'viewCustomersAjaxSource', array(
                'customerDataSessionKey' => $customerDataSessionKey
            ))
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution channel',
            'required' => false,
            'choices'  => $this->getOpenDistributionChannels()
        ));
        $builder->add('customerDataSessionKey',    'hidden');

        $builder->addModelTransformer(new AppDistributionChannelCustomersPickerTransformer($this->container));
    }

    protected function getOpenDistributionChannels() {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $this->container->get('request');
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        $idInvoice = $request->get('id', 0);

        $arr = array();
        $query = $em->createQueryBuilder();
        $query->select('dc.id, dc.distributionchannel, COUNT(c.id) as number')
            ->from('AppDatabaseMainBundle:Customer', 'c')            
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->leftJoin('AppDatabaseMainBundle:InvoiceCustomer', 'ic', 'WITH', 'ic.idCustomer = c.id')
            ->andWhere('(ic.id IS NULL) or (ic.idInvoice = :idInvoice)')            
            ->andWhere('c.invoicegoestoid = :invoiceGoesToChannel')
            ->setParameter('idInvoice', $idInvoice)
            ->setParameter('invoiceGoesToChannel', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL)
            ->orderBy('dc.distributionchannel', 'ASC')
            ->groupBy('dc.id');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $arr[$row['id']] = array(
                'name' => $row['distributionchannel'],
                'open' => $row['number'],
                'submit' => 0
            );
        }

        $reportSubmittedStatus = 8; // Fix me
        $query->andWhere('FIND_IN_SET('.$reportSubmittedStatus.', c.statusList) > 0');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $idDistributionChannel = $row['id'];
            if (isset($arr[$idDistributionChannel])) {
                $arr[$idDistributionChannel]['submit'] = $row['number'];
            }            
        }

        $dc = array();
        foreach ($arr as $idDistributionChannel => $v) {
            $dc[$idDistributionChannel] = $v['name'].' ('.$v['open'].' open / '.$v['submit'].' submitted)';
        }
        return $dc;
    }
}