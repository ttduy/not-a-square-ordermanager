<?php 
namespace Leep\AdminBundle\Business\Invoice;

class Constant {
    const INVOICE_STATUS_TO_BE_DONE                         = 1;
    const INVOICE_STATUS_WAIT_WITH_INVOICE                  = 2;
    const INVOICE_STATUS_INVOICE_SENT                       = 3;
    const INVOICE_STATUS_REMINDERS_SENT                     = 4;
    const INVOICE_STATUS_PAID                               = 5;
    const INVOICE_STATUS_NOT_IN_BOOKEEPING                  = 6;
    const INVOICE_STATUS_CANCELLED                          = 7;
    const INVOICE_STATUS_FREE_ANALYSIS                      = 8;
    const INVOICE_STATUS_WAIT_FOR_CREDIT_CARD_TRANSACTION   = 20;
    const INVOICE_STATUS_PAID_WITH_CREDIT_CARD              = 21;
    const INVOICE_STATUS_PAID_BY_BANK_TRANSFER              = 22;
    const INVOICE_STATUS_PAID_BY_PAYPAL                     = 23;
    const INVOICE_STATUS_WAIT_WITH_REMINDER                 = 26;
    const INVOICE_STATUS_GIVEN_TO_DEBT_COLLECTION_AGENCY    = 27;
    const INVOICE_STATUS_PAYMENT_BY_INSTALLMENTS            = 28;
    const INVOICE_STATUS_WAITING_FOR_SETTLEMENT             = 29;

    public static function getInvoiceStatus() {
        return array(
            self::INVOICE_STATUS_TO_BE_DONE                         => 'To be done',
            self::INVOICE_STATUS_WAIT_WITH_INVOICE                  => 'Wait with invoice',
            self::INVOICE_STATUS_INVOICE_SENT                       => 'Invoice sent',
            self::INVOICE_STATUS_REMINDERS_SENT                     => 'Reminders sent',
            self::INVOICE_STATUS_PAID_WITH_CREDIT_CARD              => 'Paid - By credit card',
            self::INVOICE_STATUS_PAID_BY_BANK_TRANSFER              => 'Paid - By bank transfer',
            self::INVOICE_STATUS_PAID_BY_PAYPAL                     => 'Paid - By paypal',
            self::INVOICE_STATUS_NOT_IN_BOOKEEPING                  => 'Not in bookeeping',
            self::INVOICE_STATUS_CANCELLED                          => 'Invoice cancelled',
            self::INVOICE_STATUS_FREE_ANALYSIS                      => 'Free analysis',
            self::INVOICE_STATUS_WAIT_WITH_REMINDER                 => 'Wait with reminder',
            self::INVOICE_STATUS_GIVEN_TO_DEBT_COLLECTION_AGENCY    => 'Given to debt collection agency',
            self::INVOICE_STATUS_PAYMENT_BY_INSTALLMENTS            => 'Payment by installments',
            self::INVOICE_STATUS_WAITING_FOR_SETTLEMENT             => 'Waiting for Settlement'
        );
    }

    public static function getCustomerInvoiceStatus() {
        return array(
            self::INVOICE_STATUS_TO_BE_DONE                         => 'To be done',
            self::INVOICE_STATUS_WAIT_WITH_INVOICE                  => 'Wait with invoice',
            self::INVOICE_STATUS_INVOICE_SENT                       => 'Invoice sent',
            self::INVOICE_STATUS_REMINDERS_SENT                     => 'Reminders sent',
            self::INVOICE_STATUS_PAID                               => 'Paid',
            self::INVOICE_STATUS_PAID_WITH_CREDIT_CARD              => 'Paid - By credit card',
            self::INVOICE_STATUS_PAID_BY_BANK_TRANSFER              => 'Paid - By bank transfer',
            self::INVOICE_STATUS_PAID_BY_PAYPAL                     => 'Paid - By paypal',
            self::INVOICE_STATUS_NOT_IN_BOOKEEPING                  => 'Not in bookeeping',
            self::INVOICE_STATUS_CANCELLED                          => 'Invoice cancelled',
            self::INVOICE_STATUS_FREE_ANALYSIS                      => 'Free analysis',
            self::INVOICE_STATUS_WAIT_FOR_CREDIT_CARD_TRANSACTION   => 'Wait for credit card transaction',
            self::INVOICE_STATUS_WAIT_WITH_REMINDER                 => 'Wait with reminder',
            self::INVOICE_STATUS_GIVEN_TO_DEBT_COLLECTION_AGENCY    => 'Given to debt collection agency',
            self::INVOICE_STATUS_PAYMENT_BY_INSTALLMENTS            => 'Payment by installments',
            self::INVOICE_STATUS_WAITING_FOR_SETTLEMENT             => 'Waiting for Settlement'
        );
    }

    const INVOICE_TYPE_CUSTOMER     = 1;
    const INVOICE_TYPE_DISTRIBUTION_CHANNEL = 2;
    const INVOICE_TYPE_PARTNER      = 3;
    const INVOICE_TYPE_MWA_GROUP    = 4;
    public static function getInvoiceTypes() {
        return array(
            self::INVOICE_TYPE_CUSTOMER                   => 'Customer',
            self::INVOICE_TYPE_DISTRIBUTION_CHANNEL       => 'Distribution channel',
            self::INVOICE_TYPE_PARTNER                    => 'Partner',
            self::INVOICE_TYPE_MWA_GROUP                  => 'Mwa group'
        );
    }

    public static function getInvoiceBuilders() {
        return array(
            self::INVOICE_TYPE_CUSTOMER                   => 'leep_admin.invoice.business.invoice_customer_builder',
            self::INVOICE_TYPE_DISTRIBUTION_CHANNEL       => 'leep_admin.invoice.business.invoice_distribution_channel_builder',
            self::INVOICE_TYPE_PARTNER                    => 'leep_admin.invoice.business.invoice_partner_builder',
            self::INVOICE_TYPE_MWA_GROUP                  => 'leep_admin.invoice.business.invoice_mwa_group_builder'
        );
    }
}