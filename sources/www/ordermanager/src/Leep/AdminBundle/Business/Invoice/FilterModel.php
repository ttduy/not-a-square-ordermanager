<?php
namespace Leep\AdminBundle\Business\Invoice;

class FilterModel {
    public $idInvoiceType;
    public $name;
    public $number;
    public $startDate;
    public $endDate;
    public $currentStatus;
}