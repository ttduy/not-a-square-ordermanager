<?php
namespace Leep\AdminBundle\Business\Invoice;

class CustomerPickerFilterModel {
    public $orderNumber;
    public $isShowReportSubmittedOnly;

    public $firstname;
    public $surname;
    
    public $dateFrom;
    public $dateTo;
}