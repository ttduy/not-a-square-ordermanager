<?php
namespace Leep\AdminBundle\Business\Invoice;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->invoiceDate = new \DateTime();
        
        return $model;
    }

    public function buildForm($builder) {    
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idInvoiceType', 'choice', array(
            'label'    => 'Invoice type',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Type')
        )); 
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));                
        $builder->add('name', 'text', array(
            'label'    => 'Invoice name',
            'required' => true
        ));                
        $builder->add('number', 'text', array(
            'label'    => 'Invoice number',
            'required' => true
        )); 
        

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $model = $this->getForm()->getData();

        $invoice = new Entity\Invoice();
        $invoice->setInvoiceDate($model->invoiceDate);
        $invoice->setName($model->name);
        $invoice->setNumber($model->number);
        $invoice->setIdInvoiceType($model->idInvoiceType);            
        $invoice->setTaxPercentage($config->getDefaultTax());

        $em->persist($invoice);
        $em->flush();
        
        $this->newId = $invoice->getId();
        
        parent::onSuccess();
    }
}