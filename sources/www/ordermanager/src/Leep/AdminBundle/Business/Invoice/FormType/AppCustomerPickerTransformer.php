<?php
namespace Leep\AdminBundle\Business\Invoice\FormType;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class AppCustomerPickerTransformer implements DataTransformerInterface
{
    public $container;    

    public function __construct($container) {
        $this->container = $container;
    }

    // Loading
    public function transform($idCustomer)
    {
    	return array(
    		'idCustomer' => $idCustomer
    	);        
    }

    // Parsing
    public function reverseTransform($customerPicker)
    {
    	return $customerPicker['idCustomer'];    	
    }
}
