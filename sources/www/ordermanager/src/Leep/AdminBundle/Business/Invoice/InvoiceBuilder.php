<?php 
namespace Leep\AdminBundle\Business\Invoice;

abstract class InvoiceBuilder {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
    
    abstract function buildInvoiceTo($builder);
    abstract function convertToFormModel($model, $invoice);
    abstract function copyModelToEntity($model, $invoice);
}
