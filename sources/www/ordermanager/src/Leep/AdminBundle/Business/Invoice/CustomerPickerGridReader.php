<?php
namespace Leep\AdminBundle\Business\Invoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class CustomerPickerGridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $dcCell = array();
    public $dcPartners = array();
    public $customerReportInfo = array();

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            }
            else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }
    }

    public function getColumnMapping() {
        return array('orderNumber', 'name', 'distributionChannelId', 'age', 'status');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';
        $this->columnSortMapping[] = 'p.ordernumber';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.dateordered';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }
    
    public function getTableHeader() {
        return array(       
            array('title' => 'Order Number',         'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Customer Name',        'width' => '9%', 'sortable' => 'false'),     
            array('title' => 'Distribution Channel', 'width' => '9%', 'sortable' => 'true'),     
            array('title' => 'Age',                  'width' => '4%', 'sortable' => 'true'),
            array('title' => 'Status',               'width' => '8%', 'sortable' => 'true')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CustomerPickerGridReader');
    }

    public function buildQuery($queryBuilder) {
        $request = $this->container->get('request');
        $mode = $request->get('mode', '');

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
            ->leftJoin('AppDatabaseMainBundle:InvoiceCustomer', 'ic', 'WITH', 'p.id = ic.idCustomer')
            ->andWhere('(ic.id IS NULL) or (ic.idInvoice = :idInvoice)')
            ->setParameter('idInvoice', $request->get('idInvoice', 0));

        if ($mode == 'distributionChannel') {
            $queryBuilder
                ->andWhere('p.invoicegoestoid = :invoiceGoesToChannel')
                ->setParameter('invoiceGoesToChannel', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL);

            $idDistributionChannel = $request->get('idDistributionChannel', 0);        
            $queryBuilder->andWhere('p.distributionchannelid = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $idDistributionChannel);
        }
        else if ($mode == 'partner') {
            $queryBuilder
                ->andWhere('p.invoicegoestoid = :invoiceGoesToPartner')
                ->setParameter('invoiceGoesToPartner', Business\DistributionChannel\Constant::INVOICE_GOES_TO_PARTNER);   
            
            $idPartner = $request->get('idPartner', 0);        
            $queryBuilder->andWhere('dc.partnerid = :idPartner')
                ->setParameter('idPartner', $idPartner);                
        }

        $queryBuilder->andWhere('p.isdeleted = 0');
        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        // Filters
        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber).'%');
        }
        if ($this->filters->isShowReportSubmittedOnly) {            
            $reportSubmittedStatus = 8; // Fix me
            $queryBuilder->andWhere('FIND_IN_SET('.$reportSubmittedStatus.', p.statusList) > 0');
        }
        if (trim($this->filters->firstname) != '') {
            $queryBuilder->andWhere('p.firstname LIKE :firstname')
                ->setParameter('firstname', '%'.trim($this->filters->firstname).'%');
        }
        if (trim($this->filters->surname) != '') {
            $queryBuilder->andWhere('p.surname LIKE :surname')
                ->setParameter('surname', '%'.trim($this->filters->surname).'%');
        }
        if (!empty($this->filters->dateFrom)) {
            $queryBuilder->andWhere('p.dateordered >= :dateFrom')
                ->setParameter('dateFrom', $this->filters->dateFrom);
        }
        if (!empty($this->filters->dateTo)) {
            $queryBuilder->andWhere('p.dateordered <= :dateTo')
                ->setParameter('dateTo', $this->filters->dateTo);
        }                                

        // Already selected customer filters
        $customerDataSessionKey = $request->get('customerDataSessionKey', '');        
        $tempDataHelper = $this->container->get('leep_admin.helper.temp_data');
        $currentSelected = $tempDataHelper->get($customerDataSessionKey, array());
        if (empty($currentSelected)) {
            $currentSelected[] = 0;
        }
        $queryBuilder->andWhere('p.id NOT IN (:selectedCustomersId)')
            ->setParameter('selectedCustomersId', array_keys($currentSelected));
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('customerModify')) {
            //$builder->addButton('Update status', $mgr->getUrl('leep_admin', 'customer', 'edit_status', 'edit', array('id' => $row->getId())), 'button-edit');
        }

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());
        
        $age = $this->getAge($row->getStatusDate());        
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellAge($row) {
        $age = $this->getAge($row->getDateOrdered());
        if ($age !== '') {
            return $age.'d';
        }
        return '';
    }

    public function buildCellDistributionChannelId($row) {
        return isset($this->dcCell[$row->getDistributionChannelId()]) ? $this->dcCell[$row->getDistributionChannelId()] : '';
    }
}
