<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

class BulkEditModel {
    public $selectedSamples;
    public $currentStatus;
    public $pushStatus;
    public $pushDate;
    public $isSendNow;
    public $isOverrideSending;
}