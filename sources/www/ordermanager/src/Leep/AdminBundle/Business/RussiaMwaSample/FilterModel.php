<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

class FilterModel {
    public $sampleId;
    public $group;
    public $age;
    public $resultPattern;
    public $currentStatus;
    public $isSendNow;
    public $sendingStatus;
    public $sampleList;
    public $complaintStatus;
}