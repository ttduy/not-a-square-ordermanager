<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

class Utils {
    public static function getNextStatus($container, $currentStatus) {
        $mapping = $container->get('easy_mapping');
        $nextStatus = array();
        $results = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findByIdMwaStatus($currentStatus);
        foreach ($results as $r) {
            $nextStatus[$r->getIdMwaNextStatus()] = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $r->getIdMwaNextStatus());
        }
        return $nextStatus;
    }
}