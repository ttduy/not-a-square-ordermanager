<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

use Leep\AdminBundle\Business\Partner\EditComplaintHandler as BaseEditComplaintHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends BaseEditComplaintHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample', 'app_main')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:RussiaMwaSampleComplaint', 'p')
            ->andWhere('p.idRussiaMwaSample = :idRussiaMwaSample')
            ->setParameter('idRussiaMwaSample', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idRussiaMwaSample' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:RussiaMwaSampleComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\RussiaMwaSampleComplaint();
        $complaint->setIdRussiaMwaSample($id);
        return $complaint;
    }
}
