<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $mapping = $this->container->get('easy_mapping');
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $model = new CreateModel();        
        $model->container = $this->container;
        $model->currentStatus = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $config->getMwaInitialStatus());
        
        return $model;
    }

    public function buildForm($builder) {     
        $mapping = $this->container->get('easy_mapping');
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $builder->add('sectionSampleInfo', 'section', array(
            'label'    => 'Sample Info',
            'property_path' => false
        ));
        $builder->add('sampleId', 'textarea', array(
            'label'    => 'Sample ID (s)',
            'required' => true,
            'attr'     => array(
                'rows'  => 10, 'cols' => 80
            )
        ));
        $builder->add('group', 'text', array(
            'label'    => 'Group',
            'required' => false
        ));
        $builder->add('resultPattern', 'choice', array(
            'label'    => 'Result Pattern',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_MwaSample_ResultPattern')
        ));
        $builder->add('currentStatus', 'label', array(
            'label'    => 'Current status',
            'required' => false
        ));
        $builder->add('infoText', 'textarea', array(
            'label'    => 'Info',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('gew', 'app_sample_gew_result', array(
            'label'    => 'GEW Result',
            'required' => false
        ));

        $builder->add('sectionSendPush', 'section', array(
            'label'    => 'Send Push',
            'property_path' => false
        ));
        $builder->add('pushStatus',   'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => Utils::getNextStatus($this->container, $config->getMwaInitialStatus())
        ));
        $builder->add('pushDatetime',     'datetimepicker', array(
            'label'    => 'Datetime',
            'required' => false
        ));
        $builder->add('isSendNow',    'checkbox', array(
            'label'    => 'Send now?',
            'required' => false
        ));
        $builder->add('isOverrideSending',    'checkbox', array(
            'label'    => 'Override sending?',
            'required' => false
        ));
        /*
        $builder->add('sendingStatus',     'choice', array(
            'label'    => 'Sending status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_MwaSample_SendingStatus')
        ));
        */

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $em = $this->container->get('doctrine')->getEntityManager();        
        $sampleIds = explode("\n", $model->sampleId);
        foreach ($sampleIds as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $mwaSample = new Entity\MwaSample();
                $mwaSample->setSampleId($sampleId);
                $mwaSample->setSampleGroup($model->group);
                $mwaSample->setResultPattern($model->resultPattern);
                $mwaSample->setCurrentStatus($config->getMwaInitialStatus());
                $mwaSample->setInfoText($model->infoText);
                $mwaSample->setIdGroup(Business\mwaSample\Constants::GROUP_RUSSIA);
                
                $mwaSample->setPushDate(new \DateTime());
                $mwaSample->setPushStatus($model->pushStatus);
                $mwaSample->setPushDatetime($model->pushDatetime);
                $mwaSample->setIsSendNow($model->isSendNow);
                $mwaSample->setIsOverrideSending($model->isOverrideSending);
                $mwaSample->setSendingStatus(0);

                $mwaSample->setGew1($model->gew['gew1']);
                $mwaSample->setGew2($model->gew['gew2']);
                $mwaSample->setGew3($model->gew['gew3']);
                $mwaSample->setGew4($model->gew['gew4']);
                $mwaSample->setGew5($model->gew['gew5']);
                
                $mwaSample->setRegisteredDate(new \Datetime());
                
                $em->persist($mwaSample);
            }
        }
        $em->flush();        
        
        parent::onSuccess();
    }
}