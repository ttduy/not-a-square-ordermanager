<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }

    public function getColumnMapping() {
        return array('sampleId', 'sampleGroup', 'age', 'tat', 'currentStatus', 'resultPattern', 'complaints', 'isSendNow', 'isOverrideSending', 'pushStatus', 'sendingStatus', 'idMwaInvoice', 'infoText', 'action');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $colMap = $this->getColumnMapping();
        foreach ($colMap as $map) {
            $this->columnSortMapping[] = $map;
        }
        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Sample ID',      'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Group',          'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Age',            'width' => '6%', 'sortable' => 'true'),
            array('title' => 'TAT',            'width' => '6%', 'sortable' => 'true'),
            array('title' => 'Current Status', 'width' => '7%', 'sortable' => 'true'),
            array('title' => 'Result Pattern', 'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Complaints',     'width' => '9%', 'sortable' => 'false'),
            array('title' => 'Send now',       'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Override sending',       'width' => '6%', 'sortable' => 'false'),
            array('title' => 'Next status',    'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Last Sending status', 'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Invoice No',     'width' => '6%', 'sortable' => 'false'),
            array('title' => 'Info',           'width' => '6%', 'sortable' => 'false'),
            array('title' => 'Action',         'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_RussiaMwaSampleGridReader');
        //return null;
    }

    public function buildQuery($queryBuilder) {
        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATEDIFF', 'Leep\AdminBundle\Business\MwaSample\DoctrineExt\Datediff');

        $today = new \DateTime();

        $queryBuilder
            ->select('p.id, p.sampleId, p.sampleGroup, p.resultPattern, p.currentStatus, p.isSendNow, p.sendingStatus, p.pushStatus, p.infoText, p.isOverrideSending,
                p.idMwaInvoice, p.complaintStatus,
                DATEDIFF(\''.$today->format('Y-m-d').'\', p.registeredDate) as age,
                DATEDIFF(p.finishedDate, p.registeredDate) as tat')
            ->from('AppDatabaseMainBundle:MwaSample', 'p');
        $queryBuilder->andWhere('p.idGroup = '.Business\MwaSample\Constants::GROUP_RUSSIA);
        $queryBuilder->andWhere('p.isDeleted = 0');
        if (trim($this->filters->sampleId) != '') {
            $queryBuilder->andWhere('p.sampleId LIKE :sampleId')
                ->setParameter('sampleId', '%'.trim($this->filters->sampleId).'%');
        }
        if (trim($this->filters->group) != '') {
            $queryBuilder->andWhere('p.sampleGroup LIKE :group')
                ->setParameter('group', '%'.trim($this->filters->group).'%');
        }
        if (trim($this->filters->age) != '') {
            $date = new \DateTime();
            $date->setTimestamp($date->getTimestamp() - 24*60*60*intval($this->filters->age));
            $queryBuilder->andWhere('p.registeredDate <= :date')
                ->setParameter('date', $date);
        }
        if ($this->filters->resultPattern !== 0) {
            if (strcmp($this->filters->resultPattern, 'NONE') === 0) {
                $queryBuilder->andWhere('p.resultPattern IS NULL OR p.resultPattern = :resultPattern')
                    ->setParameter('resultPattern', '');
            }
            else if (strcmp($this->filters->resultPattern, 'ANY') === 0) {
                $queryBuilder->andWhere('p.resultPattern != :resultPattern')
                    ->setParameter('resultPattern', '');
            }
            else {
                $queryBuilder->andWhere('p.resultPattern LIKE :resultPattern')
                    ->setParameter('resultPattern', '%'.trim($this->filters->resultPattern).'%');
            }
        }
        if ($this->filters->currentStatus != 0) {
            $queryBuilder->andWhere('p.currentStatus = :currentStatus')
                ->setParameter('currentStatus', $this->filters->currentStatus);
        }
        if ($this->filters->isSendNow != 0) {
            $queryBuilder->andWhere('p.isSendNow = :isSendNow')
                ->setParameter('isSendNow', $this->filters->isSendNow == 1 ? true : false);
        }
        if ($this->filters->sendingStatus != 0) {
            $queryBuilder->andWhere('p.sendingStatus = :sendingStatus')
                ->setParameter('sendingStatus', $this->filters->sendingStatus);
        }
        if ($this->filters->idMwaInvoice != 0) {
            if ($this->filters->idMwaInvoice == -1) {
                $queryBuilder->andWhere('p.idMwaInvoice = 0 OR p.idMwaInvoice IS NULL');
            }
            else {
                $queryBuilder->andWhere('p.idMwaInvoice = :idMwaInvoice')
                    ->setParameter('idMwaInvoice', $this->filters->idMwaInvoice);
            }
        }
        if (trim($this->filters->sampleList) != '') {
            $list = explode("\n", $this->filters->sampleList);
            foreach ($list as $k => $v) {
                $list[$k] = trim($v);
            }
            $queryBuilder->andWhere('p.sampleId in (:sampleList)')
                ->setParameter('sampleList', $list);
        }
        if ($this->filters->complaintStatus != 0) {
            $queryBuilder->andWhere('p.complaintStatus = :complaintStatus')
                ->setParameter('complaintStatus', $this->filters->complaintStatus);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('mwaProcessorModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'mwa_sample', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'mwa_sample', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellSendingStatus($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_MwaSample_SendingStatus', $row['sendingStatus']);
        if ($row['sendingStatus'] == Constants::SENDING_STATUS_ERROR) {
            $builder = $this->container->get('leep_admin.helper.button_list_builder');
            $mgr = $this->getModuleManager();

            $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'mwa_sample', 'feature', 'viewLastError', array('id' => $row['id'])), 'button-detail');
            return $title.'&nbsp;&nbsp;'.$builder->getHtml();
        }
        return $title;
    }

    public function buildCellIsSendNow($row) {
        return $row['isSendNow'] ? 'Yes' : '';
    }

    public function buildCellIsOverrideSending($row) {
        return $row['isOverrideSending'] ? 'Yes' : '';
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }

    public function buildCellAge($row) {
        $age = $row['age'];
        if ($age != '') {
            return $age.'d';
        }
        return '';
    }

    public function buildCellTat($row) {
        $tat = $row['tat'];
        if ($tat != '') {
            return $tat.'d';
        }
        return '';
    }

    public function buildCellInfoText($row) {
        $infoText = $row['infoText'];
        if (!empty($infoText)) {
            return '<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.$infoText.'</div></a>';
        }
        return '';
    }


    public function buildCellComplaints($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_ComplaintStatus', $row['complaintStatus']);

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'russia_mwa_sample', 'edit_complaint', 'edit', array('id' => $row['id'])), 'button-edit');

        return $builder->getHtml().'&nbsp;&nbsp;'.$title;
    }
}