<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

class EditModel {
    public $sampleId;
    public $group;
    public $age;
    public $resultPattern;
    public $infoText;
    
    public $pushStatus;
    public $pushDatetime;
    public $isSendNow;
    public $isOverrideSending;
    public $sendingStatus;

    public $gew;

    public $pushList;
}