<?php
namespace Leep\AdminBundle\Business\RussiaMwaSample;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;

    public $sampleId;
    public $group;
    public $age;
    public $resultPattern;
    public $currentStatus;
    public $infoText;
    
    public $pushStatus;
    public $pushDatetime;
    public $isSendNow;
    public $isOverrideSending;
    public $sendingStatus;
    
    public $gew;

    public function isValid(ExecutionContext $context) {  
        $sampleIds = explode("\n", $this->sampleId);
        $error = array();
        foreach ($sampleIds as $sampleId) {
            $sampleId = trim($sampleId);
            if (!empty($sampleId)) {
                $p = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBySampleId($sampleId);
                if ($p) {
                    $error[]= $sampleId;
                }
            }
        }
        if (!empty($error)) {
            $context->addViolationAtSubPath('sampleId', 'The following sample id are already existed: '.implode(', ', $error));
        }
    }
}