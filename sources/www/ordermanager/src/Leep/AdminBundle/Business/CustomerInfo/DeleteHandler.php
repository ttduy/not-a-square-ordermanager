<?php 
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);
        $orders = $em->getRepository('AppDatabaseMainBundle:Customer')->findByIdCustomerInfo($id);
        foreach ($orders as $order) {
            $em->remove($order);
        }
        $em->remove($customerInfo);
        $em->flush();
    }
}
