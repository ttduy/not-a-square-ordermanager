<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Partner\EditComplaintHandler as BaseEditComplaintHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends BaseEditComplaintHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfoComplaint', 'p')
            ->andWhere('p.idCustomerInfo = :idCustomerInfo')
            ->setParameter('idCustomerInfo', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idCustomerInfo' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerInfoComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\CustomerInfoComplaint();
        $complaint->setIdCustomerInfo($id);
        return $complaint;
    }
}
