<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();

        $model->container = $this->container;
        $model->id = $entity->getId();

        $webLoginUsername = new Business\FormType\AppUsernameRowModel();
        $webLoginUsername->username = $entity->getWebLoginUsername();
        $model->webLoginUsername = $webLoginUsername;

        $webLoginPassword = new Business\FormType\AppPasswordRowModel();
        $webLoginPassword->password = $entity->getWebLoginPassword();
        $model->webLoginPassword = $webLoginPassword;

        $model->webLoginStatus = $entity->getWebLoginStatus();
        $model->webLoginOptOut = $entity->getIsWebLoginOptOut();

        $model->isCustomerBeContacted = $entity->getIsCustomerBeContacted();
        $model->isNutrimeOffered = $entity->getIsNutrimeOffered();
        $model->isNotContacted = $entity->getIsNotContacted();
        $model->sampleCanBeUsedForScience = $entity->getSampleCanBeUsedForScience();

        $model->customerNumber = $entity->getCustomerNumber();
        $model->externalBarcode = $entity->getExternalBarcode();
        $model->idDistributionChannel = $entity->getIdDistributionChannel();
        $model->firstName = $entity->getFirstName();
        $model->surName = $entity->getSurName();
        $model->dateOfBirth = $entity->getDateOfBirth();
        $model->email = $entity->getEmail();
        $model->postCode = $entity->getPostCode();
        $model->genderId = $entity->getGenderId();
        $model->title = $entity->getTitle();
        $model->isDestroySample = $entity->getIsDestroySample();
        $model->isFutureResearchParticipant = $entity->getIsFutureResearchParticipant();
        $model->detailInformation = $entity->getDetailInformation();

        $model->canViewShop = $entity->getCanViewShop();
        $model->isCanViewYourOrder = $entity->getIsCanViewYourOrder();
        $model->isCanViewCustomerProtection = $entity->getIsCanViewCustomerProtection();
        $model->isCanViewRegisterNewCustomer = $entity->getIsCanViewRegisterNewCustomer();
        $model->isCanViewPendingOrder = $entity->getIsCanViewPendingOrder();
        $model->isCanViewSetting = $entity->getIsCanViewSetting();
        $model->webLoginGoesTo = $entity->getWebLoginGoesTo();

        // used for generating username
        $model->customerNumberHidden = $entity->getCustomerNumber();

        // get existing gene result
        $geneResults = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findBy(array('idCustomer' => $entity->getId()));

        $model->geneResult = array();
        if ($geneResults && sizeof($geneResults) > 0) {
            foreach ($geneResults as $result) {
                $geneOption = 'geneOption_' . $result->getIdGene();
                $model->geneResult[$geneOption] = $result->getResult();
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        // BASIC INFO
        $builder->add('sectionBasicInfo', 'section', array(
            'label'    => 'Basic Information',
            'property_path' => false
        ));

        $builder->add('customerNumber', 'label', array(
            'label'    => 'Customer number',
            'required' => false
        ));

        $builder->add('customerNumberHidden', 'hidden');

        $builder->add('externalBarcode', 'text', array(
            'label'    => 'External barcode',
            'required' => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution channel',
            'required' => true,
            'choices'  => [0 => ''] + $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('dateOfBirth', 'datepicker', array(
            'label'    => 'Birthday',
            'required' => false
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List'),
            'expanded' => 'expanded'
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('detailInformation', 'textarea', array(
            'label'    => 'Detail information',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
            )
        ));

        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login'
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generateUsername'),
            'name_source_field_id' => 'customerNumberHidden'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web login email Opt-Out?',
            'required' => false
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));

        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));

        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));
        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));

        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));

        // BASIC INFO
        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        $builder->add('sectionGeneResult', 'section', array(
            'label'    => 'Gene Result',
            'property_path' => false
        ));

        // Build gene options
        $builder->add('geneResult', 'app_gene_result_options_row', array(
            'label'    => 'Genes to be analyzed',
            'required' => false,
            'idRequested' => $this->container->get('request')->get('id', null)
        ));
    }

    public function onSuccess() {
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        if ($model->webLoginUsername) {
            $this->entity->setWebLoginUsername($model->webLoginUsername->username);
        } else {
            $this->entity->setWebLoginUsername(null);
        }

        if ($model->webLoginPassword) {
            $this->entity->setWebLoginPassword($model->webLoginPassword->password);
        } else {
            $this->entity->setWebLoginPassword(null);
        }

        if(!$this->isUniqueCode($em, $model->externalBarcode, $this->entity->getId(), $this->entity->getCustomerNumber())) {
            $this->errors[] = "Error! External barcode already exist";
            return false;
        }

        $this->entity->setWebLoginStatus($model->webLoginStatus);
        $this->entity->setWebLoginGoesTo($model->webLoginGoesTo);
        $this->entity->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $this->entity->setCanViewShop($model->canViewShop);
        $this->entity->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $this->entity->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $this->entity->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $this->entity->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $this->entity->setIsCanViewSetting($model->isCanViewSetting);

        $this->entity->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $this->entity->setIsNutrimeOffered($model->isNutrimeOffered);
        $this->entity->setIsNotContacted($model->isNotContacted);
        $this->entity->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $this->entity->setExternalBarcode($model->externalBarcode);
        $this->entity->setIdDistributionChannel($model->idDistributionChannel);
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setDateOfBirth($model->dateOfBirth);
        $this->entity->setEmail($model->email);
        $this->entity->setPostCode($model->postCode);
        $this->entity->setGenderId($model->genderId);
        $this->entity->setTitle($model->title);
        $this->entity->setIsDestroySample($model->isDestroySample);
        $this->entity->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $this->entity->setDetailInformation($model->detailInformation);

        // gene result
        // remove if existing
        $filter = array('idCustomer' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerInfoGene', $filter);

        // save one by one
        $geneOptions = Business\CustomerInfo\Utils::getGeneOptions($this->container);
        foreach ($geneOptions as $geneId => $data) {
            $geneOptionId = 'geneOption_'.$geneId;
            if (array_key_exists($geneOptionId, $model->geneResult) && $model->geneResult[$geneOptionId]) {
                $newGeneResult = new Entity\CustomerInfoGene();
                $newGeneResult->setIdCustomer($this->entity->getId());
                $newGeneResult->setIdGene($geneId);
                $newGeneResult->setResult($model->geneResult[$geneOptionId]);
                $em->persist($newGeneResult);
            }
        }

        $em->flush();

        // Copy to children for DUPLICATION
        Utils::ensureDuplication($em, $this->entity);

        parent::onSuccess();
    }

    private function isUniqueCode($em, $externalBarcode, $customerInfoId, $orderNumber)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->andX(
                    $query->expr()->eq('p.externalBarcode', ':externalBarcode'),
                    $query->expr()->neq('p.id', ':customerInfoId')))
                ->setParameter('externalBarcode', $externalBarcode)
                ->setParameter('customerInfoId', $customerInfoId);
        $results = $query->getQuery()->getResult();
        if(!empty($results)){
            return false;
        }

        return true;
    }
}
