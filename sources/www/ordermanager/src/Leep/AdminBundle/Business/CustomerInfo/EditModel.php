<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $id;

    public $webLoginUsername;
    public $webLoginPassword;
    public $webLoginStatus;
    public $webLoginGoesTo;
    public $customerNumberHidden;
    public $customerNumber;
    public $externalBarcode;
    public $idDistributionChannel;
    public $firstName;
    public $surName;
    public $dateOfBirth;
    public $email;
    public $postCode;
    public $genderId;
    public $title;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $sampleCanBeUsedForScience;
    public $isDestroySample;
    public $isFutureResearchParticipant;
    public $detailInformation;
    public $isWebLoginOptOut;
    public $canViewShop;
    public $isCanViewYourOrder;
    public $isCanViewCustomerProtection;
    public $isCanViewRegisterNewCustomer;
    public $isCanViewPendingOrder;
    public $isCanViewSetting;

    public $container;

    public function isValid(ExecutionContext $context) {
        $helperCommon = $this->container->get('leep_admin.helper.common');

        if ($this->webLoginUsername != null) {
            $username = $this->webLoginUsername->username;
            if ($helperCommon->isWebLoginUsernameExisted($username, 'CustomerInfo', $this->id)) {
                $context->addViolationAtSubPath('webLoginUsername', "This Web Login Username " . $username . " is already existed");
            }
        }
    }
}
