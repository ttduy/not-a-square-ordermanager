<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class Utils {
    const ID_ENGLISH = 2;

    public static function getDefaultLanguage() {
        return self::ID_ENGLISH;
    }

    public static function ensureDuplication($em, $customerInfo) {
        $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findByIdCustomerInfo($customerInfo->getId());
        foreach ($customers as $customer) {
            $customer->setDistributionChannelId($customerInfo->getIdDistributionChannel());
            $customer->setFirstName($customerInfo->getFirstName());
            $customer->setSurName($customerInfo->getSurName());
            $customer->setEmail($customerInfo->getEmail());
            $customer->setDateOfBirth($customerInfo->getDateOfBirth());
            $customer->setPostCode($customerInfo->getPostCode());
            $customer->setGenderId($customerInfo->getGenderId());
            $customer->setTitle($customerInfo->getTitle());
        }
        $em->flush();
    }

    public static function getGeneOptions($container) {
        $formatter = $container->get('leep_admin.helper.formatter');

        // Result options
        $resultOptions = array();
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:Results')->findAll();
        foreach ($result as $row) {
            if (!isset($resultOptions[$row->getGroupId()])) {
                $resultOptions[$row->getGroupId()] = array(0 => '');
            }
            $resultOptions[$row->getGroupId()][$row->getResult()] = $row->getResult();
        }

        $doctrine = $container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('g.id as gene_id,
                        g.genename as gene_name,
                        g.rsnumber as gene_rsnumber,
                        g.scientificname as gene_scientific_name,
                        g.groupid gene_group_id,
                        m.name as massarray_name
                        ')
                ->from('AppDatabaseMainBundle:Genes', 'g')
                ->leftJoin('AppDatabaseMainBundle:MassarrayGeneLink', 'mg', 'WITH', 'g.id = mg.geneid')
                ->leftJoin('AppDatabaseMainBundle:Massarray', 'm', 'WITH', 'm.id = mg.massarrayid')
                ->andWhere('g.isdeleted = 0')
                ->orderBy('g.genename', 'ASC')
                ->groupBy('g.id');

        $result = $query->getQuery()->getResult();

        // Gene options
        $genes = array();

        foreach ($result as $row) {
            $genes[$row['gene_id']] = array(
                'name' => $row['gene_name'],
                'massarray' => $row['massarray_name'],
                'rsNumber' => $row['gene_rsnumber'],
                'scientificName' => $row['gene_scientific_name'],
                'rsOption' => $formatter->format($row['gene_group_id'], 'mapping', 'LeepAdmin_Gene_ResultOptions'),
                'options' => isset($resultOptions[$row['gene_group_id']]) ? $resultOptions[$row['gene_group_id']] : array(0 => '')
            );
        }

        return $genes;
    }


    public static function getCustomerLanguage($container, $idCustomerInfo) {
        $query = $container->get('doctrine')->getManager()->createQueryBuilder();
        $order = $query->select('l.id, l.name')
                ->from('AppDatabaseMainBundle:Customer', 'p')
                ->innerJoin('AppDatabaseMainBundle:Language', 'l', 'WITH', 'l.id = p.languageid')
                ->andWhere('p.idCustomerInfo = :idCustomerInfo')
                ->andWhere('p.languageid IS NOT NULL OR p.languageid <> 0')
                ->setParameter('idCustomerInfo', $idCustomerInfo)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

        $lang = array(
                    'id'    => null,
                    'name'  => null
                );

        if ($order) {
            $lang = array(
                    'id'    => $order[0]['id'],
                    'name'  => $order[0]['name']
                );
        } else {
            // use English as default
            $language = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language')->findOneById(self::getDefaultLanguage());
            if ($language) {
                $lang = array(
                            'id'        => $language->getId(),
                            'name'      => $language->getName()
                        );
            }
        }

        return $lang;
    }

    public static function unlockCustomerInfo($container, $unlockPassword, $idCustomerInfo) {
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $errors = array();
        $unlockValue = 0;

        // get unlock password
        if ($config->getUnlockPassword() == $unlockPassword) {
            $customer = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($idCustomerInfo);

            if ($customer) {
                $tomorrow = new \DateTime();
                $tomorrow->add(new \DateInterval('P1D'));

                // unlock it
                $customer->setIsLocked(0);
                $customer->setLockDate($tomorrow);
                $em->persist($customer);

                $em->flush();
            } else {
                $errors[] = "Invalid Customer";
            }
        } else {
            $errors[] = "Invalid Unlock Password";
        }

        return $errors;
    }

    public static function unlockOrder($container, $unlockPassword, $idOrder) {
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $errors = array();
        $unlockValue = 0;

        // get unlock password
        if ($config->getUnlockPassword() == $unlockPassword) {
            $order = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOrder);

            if ($order) {
                $tomorrow = new \DateTime();
                $tomorrow->add(new \DateInterval('P1D'));

                // unlock it
                $order->setIsLocked(0);
                $order->setLockDate($tomorrow);

                $orderStatus = Business\Customer\Constant::ORDER_STATUS_UNLOCKED;
                Business\Customer\Util::addNewStatus($container, $order->getId(), $orderStatus);

                $em->flush();
            } else {
                $errors[] = "Invalid Customer";
            }
        } else {
            $errors[] = "Invalid Unlock Password";
        }

        return $errors;
    }

    static public function applyFilter($queryBuilder, $filters)
    {
        // filter on distribution channel
        if (trim($filters->idDistributionChannel) != '') {
            $queryBuilder->andWhere('p.idDistributionChannel = :idDC')
                ->setParameter('idDC', $filters->idDistributionChannel);
        }

        // filter on customer name
        if (trim($filters->customerName) != '') {
            $queryBuilder->andWhere('p.firstName LIKE :name OR p.surName LIKE :name')
                ->setParameter('name', '%'.trim($filters->customerName).'%');
        }

        // filter on customer number
        if (trim($filters->customerNumber) != '') {
            $queryBuilder->andWhere('p.customerNumber LIKE :customerNumber')
                ->setParameter('customerNumber', '%' . trim($filters->customerNumber) . '%');
        }

        // filter on order number
        if (trim($filters->orderNumber) != '') {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:Customer', 'o', 'WITH', 'o.idCustomerInfo = p.id AND o.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%' . trim($filters->orderNumber) . '%');
        }

        // filter on complaint status
        if ($filters->complaintStatus != 0) {
            if ($filters->complaintStatus == -1) {
                $queryBuilder->andWhere('p.complaintStatus > 0');
            }
            else {
                $queryBuilder->andWhere('p.complaintStatus = :complaintStatus')
                    ->setParameter('complaintStatus', $filters->complaintStatus);
            }
        }

        // filter on complaint topic
        if ($filters->complaintTopic != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:CustomerInfoComplaint', 'cic', 'WITH', 'p.id = cic.idCustomerInfo')
                ->andWhere('cic.idTopic = '.$filters->complaintTopic);
        }

        // filter on feedback
        if ($filters->positiveFeedback != 0) {
            $queryBuilder->andWhere('p.numberFeedback > 0');
        }
    }
}