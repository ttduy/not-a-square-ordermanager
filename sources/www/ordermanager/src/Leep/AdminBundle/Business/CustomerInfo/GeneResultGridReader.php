<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GeneResultGridReader extends AppGridDataReader {
    public $idCustomer = 0;
    public $filters;

    public function __construct($container) {
        parent::__construct($container);        

        $this->idCustomer = $container->get('request')->get('id', 0);
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'g.genename';
        $this->columnSortMapping[] = 'g.scientificname';
        $this->columnSortMapping[] = 'g.rsnumber';
        $this->columnSortMapping[] = 'p.result';

        return $this->columnSortMapping;
    }

    public function getColumnMapping() {
        return array('genename', 'scientificname', 'rsnumber', 'result');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Gene Name',       'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Scientific Name', 'width' => '30%', 'sortable' => 'true'),
            array('title' => 'RS Number',       'width' => '25%', 'sortable' => 'true'),
            array('title' => 'Result',          'width' => '20%', 'sortable' => 'true'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.idGene as id, g.genename, g.scientificname, g.rsnumber, p.result')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
            ->leftJoin('AppDatabaseMainBundle:Genes', 'g', 'WITH', 'p.idGene = g.id');

        $queryBuilder->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $this->idCustomer);

        if (trim($this->filters->geneName) != '') {
            $queryBuilder->andWhere('g.genename LIKE :genename')
                ->setParameter('genename', '%'.trim($this->filters->geneName).'%');
        }
        if (trim($this->filters->scientificName) != '') {
            $queryBuilder->andWhere('g.scientificname LIKE :scientificname')
                ->setParameter('scientificname', '%'.trim($this->filters->scientificName).'%');
        }
        if (trim($this->filters->rsNumber) != '') {
            $queryBuilder->andWhere('g.rsnumber LIKE :rsnumber')
                ->setParameter('rsnumber', '%'.trim($this->filters->rsNumber).'%');
        }
    }

    public function buildCellResult($row) {
        if (empty($row['result'])) {
            return '-';
        }
        return $row['result'];
    }

}
