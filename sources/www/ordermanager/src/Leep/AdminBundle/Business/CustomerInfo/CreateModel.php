<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $webLoginUsername;
    public $webLoginPassword;
    public $webLoginStatus;
    public $webLoginGoesTo;
    public $customerNumber;
    public $externalBarcode;
    public $idDistributionChannel;
    public $firstName;
    public $surName;
    public $dateOfBirth;
    public $email;
    public $postCode;
    public $genderId;
    public $title;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $sampleCanBeUsedForScience;
    public $isFutureResearchParticipant;
    public $detailInformation;
    public $geneResult;
    public $isWebLoginOptOut;
    public $canViewShop;
    public $isCanViewYourOrder;
    public $isCanViewCustomerProtection;
    public $isCanViewRegisterNewCustomer;
    public $isCanViewPendingOrder;
    public $isCanViewSetting;

    // for validation
    public $container;

    public function isValid(ExecutionContext $context) {
        $helperCommon = $this->container->get('leep_admin.helper.common');

        if ($this->webLoginUsername != null) {
            $username = $this->webLoginUsername->username;
            if ($helperCommon->isWebLoginUsernameExisted($username)) {
                $context->addViolationAtSubPath('webLoginUsername', "This Web Login Username " . $username . " is already existed");
            }
        }
    }
}
