<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class GeneResultFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new GeneResultFilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('geneName', 'text', array(
            'label'       => 'Gene Name',
            'required'    => false,
        ));
        $builder->add('scientificName', 'text', array(
            'label'       => 'Scientific Name',
            'required'    => false,
        ));
        $builder->add('rsNumber', 'text', array(
            'label'       => 'RS Number',
            'required'    => false,
        ));
        return $builder;
    }
}