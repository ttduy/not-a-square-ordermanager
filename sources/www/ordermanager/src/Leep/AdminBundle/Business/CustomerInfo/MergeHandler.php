<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class MergeHandler extends BaseEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $model = new MergeModel();
        $model->container = $this->container;
        $model->currentOrder = '...';

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        // BASIC INFO
        $builder->add('sectionBasicInfo', 'section', array(
            'label'    => 'Merge customers',
            'property_path' => false
        ));
        $builder->add('customerNumbers', 'textarea', array(
            'label'    => 'Customer numbers to be merged',
            'required' => true,
            'attr'     => array(
                'rows'   => 10,
                'cols'   => 90
            )
        ));
        $builder->add('currentOrder', 'label', array(
            'label'    => 'Selected orders',
            'required' => false,
        ));
        $builder->add('customerNumber', 'text', array(
            'label'    => 'Merge to customer number',
            'required' => true
        ));
        $builder->add('idAction', 'choice', array(
            'label'    => 'Action',
            'required' => false,
            'choices'  => array(
                1 => 'Merge now'
            )
        ));

        $builder->add('viewDuplicationFrom', 'datepicker', array(
            'label' =>      'View dupcliation from',
            'required' =>   false,
            'property_path' => false
        ));

        $builder->add('viewDuplicationTo', 'datepicker', array(
            'label' =>      'View dupcliation to',
            'required' =>   false,
            'property_path' => false
        ));

        $builder->add('mustBeProduct', 'choice', array(
            'label'    => 'Must be product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));

        $builder->add('mustNotBeProduct', 'choice', array(
            'label'    => 'Must not be product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        ));
    }

    public function onSuccess() {
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $arr = [];
        $selectedIdRaw = $model->customerNumbers;
        $tmp = explode("\n", $selectedIdRaw);
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $foundOrders = array();
        $query = $em->createQueryBuilder();
        $result = $query->select('d, e.customerNumber')
            ->from('AppDatabaseMainBundle:Customer', 'd')
            ->leftJoin('AppDatabaseMainBundle:CustomerInfo', 'e', 'WITH', 'd.idCustomerInfo = e.id')
            ->andWhere('e.customerNumber in (:arr)')
            ->andWhere('d.isdeleted = 0')
            ->setParameter('arr', $arr)
            ->getQuery()->getResult();

        $customerNumbers = $arr;
        foreach ($result as $r) {
            $foundOrders[] = array(
                'order' => $r[0],
                'customerNumber' => $r['customerNumber']
            );
        }

        $conflictResult = Business\Customer\Util::checkForConflict($this->container, $customerNumbers);
        if ($conflictResult[0] > 0) {
            $this->errors[] = "Error: unresolved conflict.";
            return;
        }

        // Customer number must in the list
        $idCustomerInfo = 0;
        foreach ($foundOrders as $record) {
            if ($record['customerNumber'] == trim($model->customerNumber)) {
                $idCustomerInfo = $record['order']->getIdCustomerInfo();
                break;
            }
        }

        if ($idCustomerInfo == 0) {
            $this->errors = array('Invalid customer number!');
            return false;
        }

        if ($model->idAction == 1) {
            // Merge gene data
            $geneDatas = $conflictResult[2];
            foreach ($geneDatas as $geneId => $data) {
                if (empty($geneId)) {
                    continue;
                }

                $result = $data['initResult'];

                // Check if customer gene info exist
                $gene = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findBy([
                    'idCustomer' => $idCustomerInfo,
                    'idGene' =>     $geneId
                ]);

                if (!$gene) {
                    $newGeneInfo = new Entity\CustomerInfoGene();
                    $newGeneInfo->setIdCustomer($idCustomerInfo);
                    $newGeneInfo->setIdGene($geneId);
                    $newGeneInfo->setResult($result);
                    $em->persist($newGeneInfo);
                }
            }

            $em->flush();

            // Merge customer info
            foreach ($foundOrders as $record) {
                if ($record['order']->getIdCustomerInfo() != $idCustomerInfo) {
                    $record['order']->setIdCustomerInfo($idCustomerInfo);
                }
            }

            $em->flush();

            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($idCustomerInfo);
            Business\CustomerInfo\Utils::ensureDuplication($em, $customerInfo);

            $this->messages[] = 'Merged';
        }
    }
}