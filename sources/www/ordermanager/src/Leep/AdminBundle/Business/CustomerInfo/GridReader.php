<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $dcCell = array();
    public $dcPartners = array();

    public function __construct($container) {
        parent::__construct($container);

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            }
            else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }
    }

    public function getColumnMapping() {
        return array('number', 'name', 'idDistributionChannel', 'weblogin', 'complaints', 'customerInfoText', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Number',                  'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Customer Name',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Web Login',               'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Complaints',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Info',                    'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Action',                  'width' => '15%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfo', 'p');

        // filter on distribution channel
        if (trim($this->filters->idDistributionChannel) != '') {
            $queryBuilder->andWhere('p.idDistributionChannel = :idDC')
                ->setParameter('idDC', $this->filters->idDistributionChannel);
        }

        // filter on customer name
        if (trim($this->filters->customerName) != '') {
            $queryBuilder->andWhere('p.firstName LIKE :name OR p.surName LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->customerName).'%');
        }

        // filter on customer number
        if (trim($this->filters->customerNumber) != '') {
            $queryBuilder->andWhere('p.customerNumber LIKE :customerNumber')
                ->setParameter('customerNumber', '%' . trim($this->filters->customerNumber) . '%');
        }

        // filter on order number
        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:Customer', 'o', 'WITH', 'o.idCustomerInfo = p.id AND o.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%' . trim($this->filters->orderNumber) . '%');
        }

        // filter on complaint status
        if ($this->filters->complaintStatus != 0) {
            if ($this->filters->complaintStatus == -1) {
                $queryBuilder->andWhere('p.complaintStatus > 0');
            }
            else {
                $queryBuilder->andWhere('p.complaintStatus = :complaintStatus')
                    ->setParameter('complaintStatus', $this->filters->complaintStatus);
            }
        }

        // filter on complaint topic
        if ($this->filters->complaintTopic != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:CustomerInfoComplaint', 'cic', 'WITH', 'p.id = cic.idCustomerInfo')
                ->andWhere('cic.idTopic = '.$this->filters->complaintTopic);
        }

        // filter on feedback
        if ($this->filters->positiveFeedback != 0) {
            $queryBuilder->andWhere('p.numberFeedback > 0');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');
    }

    public function buildCellIdDistributionChannel($row) {
        return isset($this->dcCell[$row->getIdDistributionChannel()]) ? $this->dcCell[$row->getIdDistributionChannel()] : '';
    }

    public function buildCellCustomerInfoText($row) {
        $html = '';
        $infoText = $row->getDetailInformation();
        if (!empty($infoText)) {
            $html .='<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        return $html;
    }

    public function buildCellWebLogin($row) {
        $formater = $this->container->get('leep_admin.helper.formatter');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '%s %s';

        // prepare status
        $strPwdStatus = '';
        if ($row->getWebLoginPassword() != '' && $row->getWebLoginUsername() != '') {
            $strPwdStatus = $row->getWebLoginUsername();
        }

        // button for display info
        $buttonHtml = '';
        if (!$this->isLocked($row)) {
            $builder->addPopupButton('Send email', $mgr->getUrl('leep_admin', 'customer_info', 'email', 'create', array('id' => $row->getId())), 'button-email');
            // add button login to customer dashboard
            $builder->addNewTabButton('Login Customer Dashboard', $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'loginCustomerDashboard', array('id' => $row->getId())), 'button-login-cd');
            $buttonHtml = $builder->getHtml();
        }

        return sprintf($html, $strPwdStatus, "<span>$buttonHtml</span>");
    }


    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $mgr = $this->getModuleManager();

        if (!$this->isLocked($row)) {
            $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'customer_info', 'view', 'view', array('id' => $row->getId())), 'button-detail');
            if ($helperSecurity->hasPermission('customerInfoModify')) {
                $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'customer_info', 'delete', 'delete', array('id' => $row->getId())), 'button-delete',
                        "Are you really want to delete this customer? All orders of this customers will be deleted too!");
            }
        } else {
            if ($helperSecurity->hasPermission('customerInfoModify')) {
                $builder->addPopupButton('Unlock', $mgr->getUrl('leep_admin', 'customer_info', 'unlock', 'edit', array('id' => $row->getId())), 'button-unlock');
            }
        }

        return $builder->getHtml();
    }

    public function buildCellName($row) {
        if ($row->getIsLocked()) {
            return '-- Deleted --';
        }
        return sprintf('%s %s', $row->getFirstName(), $row->getSurName());
    }

    public function buildCellNumber($row) {
        return $row->getCustomerNumber()."<br/><i>".$row->getExternalBarcode()."</i>";
    }

    public function buildCellComplaints($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_ComplaintStatus', $row->getComplaintStatus());

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builderHtml = '';

        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer_info', 'edit_complaint', 'edit', array('id' => $row->getId())), 'button-edit');

        $builderHtml = $builder->getHtml().'&nbsp;&nbsp;'.$title;

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer_info', 'edit_feedback', 'edit', array('id' => $row->getId())), 'button-checked');

        $builderHtml .= '<br/>'.$builder->getHtml() . '&nbsp;&nbsp';


        return ($builderHtml ? $builderHtml : '').intval($row->getNumberFeedback()).' feedback(s)';
    }

    private function isLocked($row) {
        $blnLocked = false;
        if ($row->getIsLocked()) {
            $blnLocked = true;
        }

        return $blnLocked;
    }
}
