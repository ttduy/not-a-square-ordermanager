<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');

        $id = $this->container->get('request')->query->get('id', 0);
        $customerInfo = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);
        $data[] = $this->add('Customer number', $customerInfo->getCustomerNumber());
        $data[] = $this->add('External barcode', $customerInfo->getExternalBarcode());
        $data[] = $this->add('Distribution channel', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_List', $customerInfo->getIdDistributionChannel()));
        $data[] = $this->add('First name', $customerInfo->getFirstName());
        $data[] = $this->add('Sur name', $customerInfo->getSurName());
        $data[] = $this->add('Date of birth', ($customerInfo->getDateOfBirth() ? $customerInfo->getDateOfBirth()->format('d/m/Y') : ''));
        $data[] = $this->add('Email', $customerInfo->getEmail());
        $data[] = $this->add('Post code', $customerInfo->getPostCode());
        $data[] = $this->add('Detail information', $customerInfo->getDetailInformation());
        $data[] = $this->add('Can be contacted directly', ($customerInfo->getIsCustomerBeContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('NutriMe Supplements are offered', ($customerInfo->getIsNutrimeOffered()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer chose OPT OUT – don’t contact', ($customerInfo->getIsNotContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('Destroy details and samples after completion', ($customerInfo->getIsDestroySample()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer agreed to extended research participation', ($customerInfo->getIsFutureResearchParticipant()) ? 'Yes' : 'No');
        $data[] = $this->add('Sample can be used for science', ($customerInfo->getSampleCanBeUsedForScience()) ? 'Yes' : 'No');

        return $data;
    }
}
