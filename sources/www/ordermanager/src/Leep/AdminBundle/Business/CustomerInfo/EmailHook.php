<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EmailHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $receiverInfo = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);

        $data['receiverInfo'] = array(
            'name' => sprintf('%s - %s %s', $receiverInfo->getCustomerNumber(), $receiverInfo->getFirstName(), $receiverInfo->getSurName())
        );
    }
}
