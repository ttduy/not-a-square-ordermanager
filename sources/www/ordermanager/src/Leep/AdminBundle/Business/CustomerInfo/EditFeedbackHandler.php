<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Partner\EditFeedbackHandler as BaseEditFeedbackHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditFeedbackHandler extends BaseEditFeedbackHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($id);
    }

    public function searchFeedback($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfoFeedback', 'p')
            ->andWhere('p.idCustomerInfo = :idCustomerInfo')
            ->setParameter('idCustomerInfo', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldFeedbacks($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idCustomerInfo' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerInfoFeedback', $filters);
    }

    public function createNewFeedback($id) {
        $feedback = new Entity\CustomerInfoFeedback();
        $feedback->setIdCustomerInfo($id);
        return $feedback;
    }
}
