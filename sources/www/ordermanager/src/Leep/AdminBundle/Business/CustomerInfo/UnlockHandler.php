<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class UnlockHandler extends AppEditHandler {
    private $order = null;

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $arrData = array(
                'infoFirstName'     =>      $entity->getFirstName(),
                'infoSurName'       =>      $entity->getSurName(),
                'unlockWarning'     =>      'This customer has had his information destroyed. Do you want to recover the details from achive?',
            );

        $idOrder = $this->container->get('request')->get('idOrder', null);
        if ($idOrder) {
            $order = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idOrder);

            if ($order) {
                $this->order = $order;
                $arrData['infoOrderNumber'] = $order->getOrdernumber();
            }
        }

        $arrData['idOrder'] = $this->container->get('request')->get('idOrder', 0);

        return $arrData;
    }

    public function buildForm($builder) {
        $builder->add('idOrder', 'hidden');
        $builder->add('infoSection', 'section', array(
            'label' => 'Basic Information'
        ));

        if ($this->order) {
            $builder->add('infoOrderNumber', 'label', array(
                'label' => 'Order Number'
            ));            
        }

        $builder->add('infoFirstName', 'label', array(
            'label' => 'First Name',
            'required'  => false
        ));
        $builder->add('infoSurName', 'label', array(
            'label' => 'Surname',
            'required'  => false
        ));

        $builder->add('unlockSection', 'section', array(
            'label' => 'Unlock'

        ));
        $builder->add('unlockWarning', 'label', array(
            'label' => 'Warning',
            'required'  => false
        ));
        $builder->add('unlockPassword', 'text', array(
            'label' => 'Unlock Password',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        if ($model['idOrder'] != 0) {
            $errors = Utils::unlockOrder($this->container, $model['unlockPassword'], $model['idOrder']);
        }
        else {
            $errors = Utils::unlockCustomerInfo($this->container, $model['unlockPassword'], $this->entity->getId());
        }

        if (!empty($errors)) {
            $this->errors = $errors;
        } else {
            $this->messages[] = "The record is unlocked!";
        }
    }
}