<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->container = $this->container;
        $model->webLoginStatus = Business\Base\Constant::WEB_LOGIN_STATUS_ACTIVE;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        // BASIC INFO
        $builder->add('sectionBasicInfo', 'section', array(
            'label'    => 'Basic Information',
            'property_path' => false
        ));
        $builder->add('customerNumber', 'text', array(
            'label'    => 'Customer number',
            'required' => true
        ));
        $builder->add('externalBarcode', 'text', array(
            'label'    => 'External barcode',
            'required' => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution channel',
            'required' => true,
            'choices'  => [0 => ''] + $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('dateOfBirth', 'datepicker', array(
            'label'    => 'Birthday',
            'required' => false
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List'),
            'expanded' => 'expanded'
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));

        $builder->add('detailInformation', 'textarea', array(
            'label'    => 'Detail information',
            'required' => false,
            'attr'     => array(
                'rows'   => 6,
                'cols'   => 60
            )
        ));

        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login',
            'property_path' => false
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generateUsername'),
            'name_source_field_id' => 'customerNumber'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web login email Opt-Out?',
            'required' => false
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));

        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));

        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));
        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));

        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));

        // BASIC INFO
        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly?',
            'required' => false,
            'attr'     => array('style' => 'padding:3px 20px 5px 20px')
        ));

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        $builder->add('sectionGeneResult', 'section', array(
            'label'    => 'Gene Result',
            'property_path' => false
        ));

        // Build gene options
        $builder->add('geneResult', 'app_gene_result_options_row', array(
            'label'    => 'Genes to be analyzed',
            'required' => false
        ));
    }

    public function onSuccess() {
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $customerInfo = new Entity\CustomerInfo();

        if ($model->webLoginUsername) {
            $customerInfo->setWebLoginUsername($model->webLoginUsername->username);
        }
        else{
            $helperCommon = $this->container->get('leep_admin.helper.common');
            $inputName = $model->customerNumber;
            $customerInfo->setWebLoginUsername($helperCommon->generateUsername($inputName));
        }

        if ($model->webLoginPassword) {
            $customerInfo->setWebLoginPassword($model->webLoginPassword->password);
        }
        else {
            # code...
            $helperCommon = $this->container->get('leep_admin.helper.common');
            $customerInfo->setWebLoginPassword($helperCommon->generateRandPasscode(8));
        }

        if(!$this->isUniqueCode($em, $model->externalBarcode)) {
            $this->errors[] = "Error! External barcode already exist";
            return false;
        }

        $customerInfo->setWebLoginStatus($model->webLoginStatus);
        $customerInfo->setWebLoginGoesTo($model->webLoginGoesTo);
        $customerInfo->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $customerInfo->setCanViewShop($model->canViewShop);
        $customerInfo->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $customerInfo->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $customerInfo->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $customerInfo->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $customerInfo->setIsCanViewSetting($model->isCanViewSetting);

        $customerInfo->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $customerInfo->setIsNutrimeOffered($model->isNutrimeOffered);
        $customerInfo->setIsNotContacted($model->isNotContacted);
        $customerInfo->setIsDestroySample($model->isDestroySample);
        $customerInfo->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $customerInfo->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $customerInfo->setCustomerNumber($model->customerNumber);
        $customerInfo->setExternalBarcode($model->externalBarcode);
        $customerInfo->setIdDistributionChannel($model->idDistributionChannel);
        $customerInfo->setFirstName($model->firstName);
        $customerInfo->setSurName($model->surName);
        $customerInfo->setDateOfBirth($model->dateOfBirth);
        $customerInfo->setEmail($model->email);
        $customerInfo->setPostCode($model->postCode);
        $customerInfo->setGenderId($model->genderId);
        $customerInfo->setTitle($model->title);
        $customerInfo->setDetailInformation($model->detailInformation);
        $customerInfo->setAccountingCode(230000000);
        $em->persist($customerInfo);
        $em->flush();

        // gene result
        // save one by one
        $geneOptions = Business\CustomerInfo\Utils::getGeneOptions($this->container);
        foreach ($geneOptions as $geneId => $data) {
            $geneOptionId = 'geneOption_'.$geneId;
            if (array_key_exists($geneOptionId, $model->geneResult) && $model->geneResult[$geneOptionId]) {
                $newGeneResult = new Entity\CustomerInfoGene();
                $newGeneResult->setIdCustomer($customerInfo->getId());
                $newGeneResult->setIdGene($geneId);
                $newGeneResult->setResult($model->geneResult[$geneOptionId]);
                $em->persist($newGeneResult);
                $em->flush();
            }
        }

        parent::onSuccess();
    }

    private function isUniqueCode($em, $externalBarcode) {
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->eq('p.externalBarcode', ':externalBarcode'))
                ->setParameter('externalBarcode', $externalBarcode);
        $results = $query->getQuery()->getResult();
        if(!empty($results)){
            return false;
        }

        return true;
    }
}
