<?php
namespace Leep\AdminBundle\Business\CustomerInfo;

use Symfony\Component\Validator\ExecutionContext;

class MergeModel {
    public $customerNumbers;
    public $currentOrder;
    public $customerNumber;
    public $idAction;
    public $mustBeProduct;
    public $mustNotBeProduct;
}
