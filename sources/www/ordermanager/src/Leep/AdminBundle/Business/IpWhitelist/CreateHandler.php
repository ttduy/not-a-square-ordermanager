<?php
namespace Leep\AdminBundle\Business\IpWhitelist;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('ipAddress', 'text', array(
            'label'    => 'IP Address',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $ipWhitelist = new Entity\IpWhitelist();
        $ipWhitelist->setIpAddress($model->ipAddress);
        $ipWhitelist->setName($model->name);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($ipWhitelist);
        $em->flush();
        
        parent::onSuccess();
    }
}