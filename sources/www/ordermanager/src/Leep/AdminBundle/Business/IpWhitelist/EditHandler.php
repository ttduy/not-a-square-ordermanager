<?php
namespace Leep\AdminBundle\Business\IpWhitelist;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:IpWhitelist', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->ipAddress = $entity->getIpAddress();
        $model->name = $entity->getName();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('ipAddress', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setIpAddress($model->ipAddress);
        $this->entity->setName($model->name);

        parent::onSuccess();
    }
}