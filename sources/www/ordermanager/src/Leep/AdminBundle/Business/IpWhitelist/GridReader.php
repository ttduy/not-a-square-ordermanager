<?php
namespace Leep\AdminBundle\Business\IpWhitelist;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('ipAddress', 'name', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Ip Address',  'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Name',        'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '35%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:IpWhitelist', 'p');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('ipLogModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'ip_whitelist', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'ip_whitelist', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}