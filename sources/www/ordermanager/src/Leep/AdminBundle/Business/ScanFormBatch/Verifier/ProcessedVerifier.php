<?php
namespace Leep\AdminBundle\Business\ScanFormBatch\Verifier;

use Leep\AdminBundle\Business;

class ProcessedVerifier extends BaseVerifier {
    public function view(&$data) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $this->container->get('request');

        $id = $request->get('id');
        $scanFormBatch = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormBatch')->findOneById($id);

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormRecord', 'p')
            ->andWhere('p.idScanFormBatch = :idScanFormBatch')
            ->andWhere('p.verifyProcessedStatus = 0 OR p.verifyProcessedStatus IS NULL')
            ->setParameter('idScanFormBatch', $id)
            ->setMaxResults(20);
        $result = $query->getQuery()->getResult();

        $records = array();
        foreach ($result as $r) {
            $records[$r->getId()] = array(
                'imageUrl'  => $this->getScanImageUrl('form_parse', $scanFormBatch->getName(), $r->getId(), 'marked')
            );
        }

        $forms = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:ScanForm')->findAll();
        foreach ($result as $r) {
            $forms[$r->getId()] = $r->getCode().' - '.$r->getName();
        }
        $data['forms'] = $forms;

        $data['recordsId'] = implode(',', array_keys($records));
        $data['records'] = $records;
    }

    public function submit() {
        $recordProcessorFunction = function($verifier, $scanFormRecord, &$data) {
            $request = $verifier->container->get('request');
            $idScanFormCorrected = $request->get('record_'.$scanFormRecord->getId());
            if ($idScanFormCorrected == 0) {
                $scanFormRecord->setVerifyProcessedStatus(1);
            }
            else {
                $scanFormRecord->setStatus(Business\ScanFormRecord\Constants::STATUS_ERROR);
                $scanFormRecord->setIdScanFormRecognized($idScanFormCorrected);
                $scanFormRecord->setVerifyProcessedStatus(1);
            }
        };
        $this->doSubmit($recordProcessorFunction);
    }
}