<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

class Constants {
    const STATUS_CAPTURING                = 1;
    const STATUS_READY_FOR_PROCESSING     = 2;
    const STATUS_READY_FOR_MANUAL_VERIFY  = 3;
    const STATUS_DONE                     = 4;
    const STATUS_ERROR                    = 99;

    public static function getScanFormBatchStatus() {
        return array(
            self::STATUS_CAPTURING                 => 'Capturing',
            self::STATUS_READY_FOR_PROCESSING      => 'Ready for processing',
            self::STATUS_READY_FOR_MANUAL_VERIFY   => 'Ready for manual verification',
            self::STATUS_DONE                      => 'Done',
            self::STATUS_ERROR                     => 'Error',
        );
    }

    const VERIFY_STATUS_VERIFY_FORM      = 0;
    const VERIFY_STATUS_VERIFY_TEXTBOX   = 1;
    const VERIFY_STATUS_VERIFY_CHECKBOX  = 2;
    const VERIFY_STATUS_VERIFY_TEXTAREA  = 3;
}
