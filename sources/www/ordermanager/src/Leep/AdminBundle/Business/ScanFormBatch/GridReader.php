<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('batchDate', 'name', 'status', 'verify', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Batch Date',  'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Name',        'width' => '20%', 'sortable' => 'false'),            
            array('title' => 'Status',      'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Verify',      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.batchDate';
        return $this->columnSortMapping;
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->addOrderBy('p.id', 'DESC');
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ScanFormBatchGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFormBatch', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->status) != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }        
        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.batchDate >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }
        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.batchDate <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormDataModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'scan_form_batch', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'scan_form_batch', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellVerify($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormDataModify') && $row->getStatus() == Constants::STATUS_READY_FOR_MANUAL_VERIFY) {
            $builder->addButton('Review', $mgr->getUrl('leep_admin', 'scan_form_batch', 'feature', 'verify', array('id' => $row->getId())), 'button-edit');
        }
        return $builder->getHtml();
    }    
}
