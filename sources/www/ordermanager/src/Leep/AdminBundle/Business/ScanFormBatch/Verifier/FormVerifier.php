<?php
namespace Leep\AdminBundle\Business\ScanFormBatch\Verifier;

use Leep\AdminBundle\Business;

class FormVerifier extends BaseVerifier {
    public function getScanFormAllFields($idScanForm) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormField', 'p')
            ->andWhere('p.idScanForm = :idScanForm')
            ->setParameter('idScanForm', $idScanForm);
        $result = $query->getQuery()->getResult();

        $fields = array();
        foreach ($result as $r) {
            $fields[] = array(
                'name' => $r->getName(),
                'type' => $r->getIdFieldType(),
                'x'    => $r->getZoneX(),
                'y'    => $r->getZoneY(),
                'width' => $r->getZoneWidth(),
                'height' => $r->getZoneHeight()
            );
        }
        return $fields;
    }   

    public function getScanFormImageUrl($idScanForm) {
        $doctrine = $this->container->get('doctrine');
        $scanForm = $doctrine->getRepository('AppDatabaseMainBundle:ScanForm')->findOneById($idScanForm);

        $mgr = $this->container->get('easy_module.manager');
        return $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'viewAttachment', array(
            'dir'   => 'scan_form',
            'name'  => $scanForm->getFormImgFile()
        ));
    }

    public function getScanFormImageFileDir($idScanForm) {
        $doctrine = $this->container->get('doctrine');
        $scanForm = $doctrine->getRepository('AppDatabaseMainBundle:ScanForm')->findOneById($idScanForm);

        $dir = $this->container->getParameter('files_dir').'/scan_form/'.$scanForm->getFormImgFile();
        return $dir;
    }

    public function view(&$data) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $this->container->get('request');

        $id = $request->get('id');
        $scanFormBatch = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormBatch')->findOneById($id);

        // Don't verify success form
        $query = $this->getScanFormRecordQuery($id);
        $query->andWhere('p.verifyFormStatus = 0 OR p.verifyFormStatus IS NULL');            
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $r->setVerifyFormStatus(1);
        }
        $em->flush();

        // Get error form
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormRecord', 'p')
            ->andWhere('p.idScanFormBatch = :idScanFormBatch')
            ->andWhere('p.status = :statusError')
            ->setParameter('idScanFormBatch', $id)
            ->setParameter('statusError', Business\ScanFormRecord\Constants::STATUS_ERROR)
            ->setMaxResults(5);
        $result = $query->getQuery()->getResult();

        $records = array();
        $recordsId = array();
        foreach ($result as $r) {
            $recordsId[] = $r->getId();
            $records[] = array(
                'id'         => $r->getId(),
                'imageUrl'   => $this->getScanImageUrl('form_parse', $scanFormBatch->getName(), $r->getId(), 'marked'),
                'formImage'  => $this->normalizeForm(array(
                    'width'      => 600,
                    'recordId'   => $r->getId(),
                    'imageFile'  => $this->getScanFormImageFileDir($r->getIdScanFormRecognized()),
                    'imageUrl'   => $this->getScanFormImageUrl($r->getIdScanFormRecognized()),
                    'fields'     => $this->getScanFormAllFields($r->getIdScanFormRecognized())
                ))
            );
        }

        $data['recordsId'] = implode(',', $recordsId);
        $data['records'] = $records;
    }

    public function normalizeForm($form) {
        $size = getimagesize($form['imageFile']);
        $width = $size[0];
        $height = $size[1];

        $ratio = $form['width'] / $width;
        foreach ($form['fields'] as $id => $field) {
            $form['fields'][$id]['x'] = intval($field['x'] * $ratio);
            $form['fields'][$id]['y'] = intval($field['y'] * $ratio);
            $form['fields'][$id]['width'] = intval($field['width'] * $ratio);
            $form['fields'][$id]['height'] = intval($field['height'] * $ratio);

            if ($field['type'] == 'TB') {
                $form['fields'][$id]['x'] += 5;
                $form['fields'][$id]['width'] -= 10;
            }
            else if ($field['type'] == 'CB') {
                $form['fields'][$id]['x'] += $form['fields'][$id]['width'] / 2 - 3;
                $form['fields'][$id]['y'] += $form['fields'][$id]['height'] / 2 - 3;
            }
        }

        return $form;        
    }

    public function submit() {
        $recordProcessorFunction = function($verifier, $scanFormRecord, &$data) {
            $request = $verifier->container->get('request');
            $fields = $verifier->getScanFormAllFields($scanFormRecord->getIdScanFormRecognized());
            foreach ($fields as $field) {
                $fieldName = $field['name'];
                $text = $request->get('record_'.$scanFormRecord->getId().'_'.$fieldName, NULL);
                if ($field['type'] == 'CB') {
                    $data[$fieldName] = ($text === NULL) ? 0 : 1;
                }
                else {
                    if ($text !== NULL) {
                        $data[$fieldName] = $text;
                    }
                }
            }            
            $scanFormRecord->setVerifyTextboxStatus(1);
            $scanFormRecord->setVerifyTextareaStatus(1);
            $scanFormRecord->setVerifyCheckboxStatus(1);            
            $scanFormRecord->setVerifyFormStatus(1);
            $scanFormRecord->setStatus(Business\ScanFormRecord\Constants::STATUS_DONE);
        };        
        $this->doSubmit($recordProcessorFunction);        
    }
}