<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public $attachmentKey = 'a';
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormBatch', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $mgr = $this->container->get('easy_module.manager');

        $model = new EditModel();  
        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();
        $model->name = $entity->getName();
        $model->batchDate = $entity->getBatchDate();
        $model->status = $formatter->format($entity->getStatus(), 'mapping', 'LeepAdmin_ScanFormBatch_Status');

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        
        $builder->add('batchDate', 'datepicker', array(
            'label'    => 'Batch date',
            'required' => false
        ));        
        $builder->add('status', 'label', array(
            'label'    => 'Status',
            'required' => false
        ));        
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => $this->attachmentKey
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        
        $this->entity->setName($model->name);
        $this->entity->setBatchDate($model->batchDate);
        parent::onSuccess();            
        
    }
}
