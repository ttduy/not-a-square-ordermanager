<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;
    public $name;
    public $batchDate;
    public $status;
    public $attachmentKey;
    public $attachment;
}
