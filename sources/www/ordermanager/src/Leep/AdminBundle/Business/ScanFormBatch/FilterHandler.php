<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'       => 'Name',
            'required'    => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_ScanFormBatch_Status'),
            'empty_value' => false
        ));
        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start date',
            'required' => false
        ));
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End date',
            'required' => false
        ));

        return $builder;
    }
}