<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if ($this->attachmentKey === false) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        }
    }

    public function getDefaultFormModel() {
        $this->generateAttachmentKey();
        $model = new CreateModel();       
        $model->container = $this->container;
        $model->batchDate = new \DateTime();
        $model->attachmentKey = $this->attachmentKey; 
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));        
        $builder->add('batchDate', 'datepicker', array(
            'label'    => 'Batch date',
            'required' => false
        ));        
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_ScanFormBatch_Status')
        ));        
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => $this->attachmentKey
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $scanFormBatch = new Entity\ScanFormBatch();
        $scanFormBatch->setName($model->name);
        $scanFormBatch->setBatchDate($model->batchDate);
        $scanFormBatch->setStatus($model->status);
        $scanFormBatch->setAttachmentKey($model->attachmentKey);
        $em->persist($scanFormBatch);
        $em->flush();
        
        parent::onSuccess();            
    }
}
