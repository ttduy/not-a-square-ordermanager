<?php
namespace Leep\AdminBundle\Business\ScanFormBatch\Verifier;

use Leep\AdminBundle\Business;

class CheckboxVerifier extends BaseVerifier {
    public function view(&$data) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $request = $this->container->get('request');

        $id = $request->get('id');
        $scanFormBatch = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormBatch')->findOneById($id);

        $query = $this->getScanFormRecordQuery($id);
        $query->andWhere('p.verifyCheckboxStatus = 0 OR p.verifyCheckboxStatus IS NULL')    
            ->setMaxResults(50);
        $result = $query->getQuery()->getResult();

        $records = array();
        $recordsId = array();
        foreach ($result as $r) {
            $recordsId[] = $r->getId();
            $fields = $this->getScanFormFields($r->getIdScanFormRecognized(), 'CB');
            foreach ($fields as $fieldName) {
                $records[] = array(
                    'id'       => $r->getId(),
                    'name'     => $fieldName,
                    'imageUrl' => $this->getScanImageUrl('form_parse_extracted', $scanFormBatch->getName(), $r->getId(), $fieldName)
                );
            }
        }

        if (empty($records)) {
            foreach ($result as $r) {
                $r->setVerifyCheckboxStatus(1);
            }
            $em->flush();
            return false;
        }

        $data['recordsId'] = implode(',', $recordsId);
        $data['records'] = $records;
    }    

    public function submit() {
        $recordProcessorFunction = function($verifier, $scanFormRecord, &$data) {
            $request = $verifier->container->get('request');
            $fields = $verifier->getScanFormFields($scanFormRecord->getIdScanFormRecognized(), 'CB');
            foreach ($fields as $fieldName) {
                $text = $request->get('record_'.$scanFormRecord->getId().'_'.$fieldName, NULL);
                if ($text !== NULL) {
                    $data[$fieldName] = $text;
                }
            }
            $scanFormRecord->setVerifyCheckboxStatus(1);
        };        
        $this->doSubmit($recordProcessorFunction);
    }
}