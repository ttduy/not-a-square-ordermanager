<?php
namespace Leep\AdminBundle\Business\ScanFormBatch;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class ManualReviewHandler extends AppEditHandler {   
    const MODE_VERIFY_ZONE     = 1;
    const MODE_VERIFY_TEXTBOX  = 2;
    const MODE_VERIFY_CHECKBOX = 3;
    const MODE_VERIFY_TEXTAREA = 4;

    public $idMode = self::MODE_VERIFY_ZONE;

    public function detectMode() {

    }

    public function loadEntity($request) {
        $this->detectMode();
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormBatch', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        return array(
            
        );
    }

    public function getFormRecords() {
        $mgr = $this->container->get('easy_module.manager');

        $id = $this->container->get('request')->get('id');
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormRecord')->findByIdScanFormBatch($id);
        $records = array();

        $viewAttachmentUrl = $mgr->getUrl('leep_admin', 'scan_form', 'feature', 'viewScanFormImage');

        foreach ($result as $r) {
            $records[$r->getId()] = array(
                'id'              => $r->getId(),
                'status'          => $r->getStatus(), 
                'markedImageUrl'  => $viewAttachmentUrl.'?dir=form_parse&batch='.$this->entity->getName().'&id='.$r->getId().'&name=marked'
            );
        }
        return $records;
    }

    public function getTextBoxRecords() {
        $mgr = $this->container->get('easy_module.manager');
        $id = $this->container->get('request')->get('id');

        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        if ($this->idMode == self::MODE_VERIFY_ZONE) {
            $records = $this->getFormRecords();
            $builder->add('sectionVerifyZone', 'section', array(
                'label'    => 'Verify: code detection, zone extraction',
                'required' => false
            ));
            foreach ($records as $r) {
                $builder->add('verify_'.$r['id'], 'app_scan_form_verify_mark', array(
                    'image' => $r['markedImageUrl']
                ));
            }            
        }   
        else if ($this->idMode == self::MODE_VERIFY_TEXTBOX) {

        }

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        
        print_r($model);

        parent::onSuccess();            
        
    }
}
