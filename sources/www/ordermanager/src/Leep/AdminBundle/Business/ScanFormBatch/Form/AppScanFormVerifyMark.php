<?php
namespace Leep\AdminBundle\Business\ScanFormBatch\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\FormType\Transformer\AppAttachmentTransformer;

class AppScanFormVerifyMark extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'image' => ''
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_scan_form_verify_mark';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $view->vars['imageUrl'] = $options['image'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idResult', 'choice', array(
            'label' => 'Result',
            'required'  => false,
            'choices'   => array(
                'Wrong - Code recognition',
                'Wrong - Zone extraction',
                'Wrong'
            ),
            'attr' => array(
                'style' => 'min-width: 180x; max-width: 180px'
            )
        ));
    }
}