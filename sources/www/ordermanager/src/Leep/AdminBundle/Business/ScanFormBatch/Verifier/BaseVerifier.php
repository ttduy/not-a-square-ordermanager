<?php
namespace Leep\AdminBundle\Business\ScanFormBatch\Verifier;

use Leep\AdminBundle\Business;

class BaseVerifier {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getScanImageUrl($dir, $batchName, $id, $name) {             
        $webUrl = $this->container->getParameter('app_web_url');
        return $webUrl.'/files/scan_form_images/'.$dir.'/'.$batchName.'/'.$id.'/'.$name.'.jpg';
    }    


    public function getScanFormFields($idScanForm, $idFieldType = 'TB') {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormField', 'p')
            ->andWhere('p.idScanForm = :idScanForm')
            ->setParameter('idScanForm', $idScanForm);
        if ($idFieldType !== NULL) {
            $query
                ->andWhere('p.idFieldType = :idFieldType')
                ->setParameter('idFieldType', $idFieldType);
        }
        $result = $query->getQuery()->getResult();

        $fields = array();
        foreach ($result as $r) {
            $fields[] = $r->getName();
        }
        return $fields;
    }    

    public function getScanFormRecordQuery($idScanFormBatch) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormRecord', 'p')
            ->andWhere('p.idScanFormBatch = :idScanFormBatch')
            ->andWhere('p.status = :statusProcessed')
            ->setParameter('idScanFormBatch', $idScanFormBatch)
            ->setParameter('statusProcessed', Business\ScanFormRecord\Constants::STATUS_DONE);
        return $query;      
    }

    public function doSubmit($recordParseFunction) {
        $request = $this->container->get('request');
        $em = $this->container->get('doctrine')->getEntityManager();

        $scanFormRecords = $this->getScanFormRecordFromRequest();
        foreach ($scanFormRecords as $scanFormRecord) {
            $data = $this->loadScanFormData($scanFormRecord);

            $recordParseFunction($this, $scanFormRecord, $data);

            $scanFormRecord->setFormData(json_encode($data));
        }
        $em->flush();
    }

    public function getScanFormRecordFromRequest() {
        $request = $this->container->get('request');
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $scanFormRecords = array();
        $recordsId = explode(',', $request->get('recordsId'));
        foreach ($recordsId as $recordId) {
            $recordId = intval($recordId);
            if ($recordId != 0) {
                $scanFormRecord = $doctrine->getRepository('AppDatabaseMainBundle:ScanFormRecord')->findOneById($recordId);
                if ($scanFormRecord) {
                    $scanFormRecords[] = $scanFormRecord;
                }
            }
        }

        return $scanFormRecords;
    }

    public function loadScanFormData($scanFormRecord) {        
        $data = json_decode($scanFormRecord->getFormData(), true);
        if (!is_array($data)) {
            $data = array();
        }
        return $data;
    }
}