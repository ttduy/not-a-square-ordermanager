<?php 
namespace Leep\AdminBundle\Business\VisibleGroupProduct;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $vgp = $doctrine->getRepository('AppDatabaseMainBundle:VisibleGroupProduct')->findOneById($id);
        $em->remove($vgp);
        $em->flush();

        $queryDeleteCategories = $em->createQueryBuilder();
        $queryDeleteCategories->delete('AppDatabaseMainBundle:VisibleGroupProductCategory', 'p')
                                ->andWhere('p.idVisibleGroupProduct = :idVisibleGroupProduct')
                                ->setParameter('idVisibleGroupProduct', $id)
                                ->getQuery()
                                ->execute();

        $queryDeleteProducts = $em->createQueryBuilder();
        $queryDeleteProducts->delete('AppDatabaseMainBundle:VisibleGroupProductProduct', 'p')
                                ->andWhere('p.idVisibleGroupProduct = :idVisibleGroupProduct')
                                ->setParameter('idVisibleGroupProduct', $id)
                                ->getQuery()
                                ->execute();

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'visible_group_product', 'grid', 'list', array(
            'id' => $this->container->get('request')->query->get('id', 0)
        ));

        return new RedirectResponse($url);
    }
}