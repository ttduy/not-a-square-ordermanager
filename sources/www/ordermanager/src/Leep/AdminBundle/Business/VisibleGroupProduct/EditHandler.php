<?php
namespace Leep\AdminBundle\Business\VisibleGroupProduct;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {   
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProduct')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();

        // get related categories
        $model->categoryProductRow = array('categories' => array(), 'products' => array());
        $categories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductCategory')->findBy(array('idVisibleGroupProduct' => $entity->getId()));
        foreach ($categories as $category) {
            $model->categoryProductRow['categories'][] = $category->getIdCategory();
        }
        
        // get related products
        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductProduct')->findBy(array('idVisibleGroupProduct' => $entity->getId()));
        foreach ($products as $product) {
            $model->categoryProductRow['products'][] = $product->getIdProduct();
        }

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('categoryProductRow', 'app_category_product');
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);

        // clean categories
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:VisibleGroupProductCategory', 'p')
                ->andWhere('p.idVisibleGroupProduct = :idVisibleGroupProduct')
                ->setParameter('idVisibleGroupProduct', $this->entity->getId())
                ->getQuery()->execute();
        $query->delete('AppDatabaseMainBundle:VisibleGroupProductProduct', 'p')
                ->andWhere('p.idVisibleGroupProduct = :idVisibleGroupProduct')
                ->setParameter('idVisibleGroupProduct', $this->entity->getId())
                ->getQuery()->execute();

        // create Visible Group - Product - Category
        if (!empty($model->categoryProductRow)) {
            // check categories
            foreach($model->categoryProductRow['categories'] as $idCategory) {
                $category = new Entity\VisibleGroupProductCategory();
                $category->setIdVisibleGroupProduct($this->entity->getId());
                $category->setIdCategory($idCategory);
                $em->persist($category);
                $em->flush();
            }

            // check products
            foreach($model->categoryProductRow['products'] as $idProduct) {
                $product = new Entity\VisibleGroupProductProduct();
                $product->setIdVisibleGroupProduct($this->entity->getId());
                $product->setIdProduct($idProduct);
                $em->persist($product);
                $em->flush();
            }            
        }

        parent::onSuccess();
    }
}
