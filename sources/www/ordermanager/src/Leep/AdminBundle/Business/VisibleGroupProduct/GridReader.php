<?php
namespace Leep\AdminBundle\Business\VisibleGroupProduct;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;

    public function getColumnMapping() {
        return array('name', 'categories', 'products', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Categories',  'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Products',    'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:VisibleGroupProduct', 'p')
            ->orderBy('p.id', 'DESC');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('distributionChannelModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'visible_group_product', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'visible_group_product', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellCategories($row) {
        $mapping = $this->container->get('easy_mapping');
        $html = '';

        $categories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductCategory')->findBy(array('idVisibleGroupProduct' => $row->getId()));

        foreach ($categories as $category) {
            if ($html) {
                $html .= '<br/>';
            }

            $html .= $mapping->getMappingTitle('LeepAdmin_Category_List', $category->getIdCategory());
        }

        return $html;
    }

    public function buildCellProducts($row) {
        $mapping = $this->container->get('easy_mapping');
        $html = '';

        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductProduct')->findBy(array('idVisibleGroupProduct' => $row->getId()));

        foreach ($products as $product) {
            if ($html) {
                $html .= '<br/>';
            }

            $html .= $mapping->getMappingTitle('LeepAdmin_Product_List', $product->getIdProduct());
        }

        return $html;
    }
}
