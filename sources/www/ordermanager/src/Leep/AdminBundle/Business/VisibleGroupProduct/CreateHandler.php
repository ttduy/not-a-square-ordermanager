<?php
namespace Leep\AdminBundle\Business\VisibleGroupProduct;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));

        $builder->add('categoryProductRow', 'app_category_product');
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        // create Visible Group - Product
        $group = new Entity\VisibleGroupProduct();
        $group->setName($model->name);

        $em->persist($group);
        $em->flush();

        // create Visible Group - Product - Category
        if (!empty($model->categoryProductRow)) {
            // check categories
            foreach($model->categoryProductRow['categories'] as $idCategory) {
                $category = new Entity\VisibleGroupProductCategory();
                $category->setIdVisibleGroupProduct($group->getId());
                $category->setIdCategory($idCategory);
                $em->persist($category);
                $em->flush();
            }

            // check products
            foreach($model->categoryProductRow['products'] as $idProduct) {
                $product = new Entity\VisibleGroupProductProduct();
                $product->setIdVisibleGroupProduct($group->getId());
                $product->setIdProduct($idProduct);
                $em->persist($product);
                $em->flush();
            }            
        }

        parent::onSuccess();
    }
}
