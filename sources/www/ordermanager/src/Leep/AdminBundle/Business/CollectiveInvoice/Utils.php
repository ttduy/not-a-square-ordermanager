<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;

class Utils {
    public static function buildPdf($controller, $idCollectiveInvoice, $isReturnDataOnly = false) {
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');

        $collectiveInvoice = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($idCollectiveInvoice);
        $company = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($collectiveInvoice->getInvoiceFromCompanyId());
        if (!$company) {
            return new Response("Please select company for this invoice");
        }

        $data = array();
        if ($collectiveInvoice->getPartnerId() != 0) {
            $client = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($collectiveInvoice->getPartnerId());
            $applied = 'invoice_partner';
        }
        else {
            $client = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($collectiveInvoice->getDistributionChannelId());
            $applied = 'invoice_dc';
            $data['distributionChannel'] = $client->getDistributionChannel();
        }

        $logo = $formatter->container->getParameter('kernel.root_dir').'/../logo.png';
        if ($company->getLogo()) {
            $logo = $formatter->container->getParameter('kernel.root_dir').'/../web/attachments/logo/'.$company->getLogo();
        }

        $data['collectiveInvoice'] = $collectiveInvoice;
        $data['logoImagePath'] = $logo;
        $data['company'] = $company;
        $data['companyName'] = $company->getCompanyName();
        $data['companyStreet'] = $company->getStreet();
        $data['companyPostCode'] = $company->getPostCode();
        $data['companyCity'] = $company->getCity();
        $data['companyEmail'] = $company->getEmail();
        $data['companyTelephone'] = $company->getTelephone();
        $data['companyWebSite'] = $company->getWebSite();

        $data['client'] = $client;
        $data['clientName'] = $client->getFirstName().' '.$client->getSurName();
        $data['clientCompanyName'] = $client->getInstitution();

        if ($client->getInvoiceAddressIsUsed()) {
            $data['clientStreet'] = $client->getInvoiceAddressStreet();
            $data['clientStreet2'] = '';
            $data['clientCity'] = $client->getInvoiceAddressCity();
            $data['clientPostCode'] = $client->getInvoiceAddressPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getInvoiceAddressIdCountry());

            $clientName = $client->getInvoiceClientName();
            $companyName = $client->getInvoiceCompanyName();
            if (trim($clientName) != '') {
                $data['clientName'] = $clientName;
                if ($clientName == '######') {
                    $data['clientName'] = '';
                }
            }
            if (trim($companyName) != '') {
                $data['clientCompanyName'] = $companyName;
                if ($companyName == '######') {
                    $data['clientCompanyName'] = '';
                }
            }
        }
        else {
            $data['clientStreet'] = $client->getStreet();
            $data['clientStreet2'] = method_exists($client, 'getStreet2') ? $client->getStreet2() : '';
            $data['clientCity'] = $client->getCity();
            $data['clientPostCode'] = $client->getPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getCountryId());
        }

        $data['clientUidNumber'] = $client->getUIDNumber();

        $today = new \DateTime();
        $data['invoiceDate'] = $formatter->format($collectiveInvoice->getInvoiceDate(), 'date');
        $data['invoiceNumber'] = $collectiveInvoice->getInvoiceNumber();

        if ($isReturnDataOnly) {
            return $data;
        }

        $order = array();
        $subTotal = 0;
        $discount = 0;
        $result = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findBy(array('collectiveinvoiceid' => $idCollectiveInvoice));
        foreach ($result as $customer) {
            $orderPriceTotal = 0;
            $price = Business\Customer\Util::computePrice($controller, $customer->getId(), $applied);
            foreach ($price['items']['categories'] as $idCategory => $v) {
                $order[] = array(
                    'orderNumber' => $customer->getOrderNumber(),
                    'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                    'product'     => $mapping->getMappingTitle('LeepAdmin_Category_List', $idCategory),
                    'firstName'   => $customer->getFirstName(),
                    'lastName'    => $customer->getSurName(),
                    'sum'         => $formatter->format($v, 'money')
                );
                $orderPriceTotal += $v;
            }
            foreach ($price['items']['products'] as $idProduct => $v) {
                $row = array(
                    'orderNumber' => $customer->getOrderNumber(),
                    'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                    'product'     => $mapping->getMappingTitle('LeepAdmin_Product_List', $idProduct),
                    'firstName'   => $customer->getFirstName(),
                    'lastName'    => $customer->getSurName(),
                    'sum'         => $formatter->format($v, 'money')
                );
                $orderPriceTotal += $v;

                if (isset($price['specialProducts'][$idProduct])) {
                    $row['product'] = $mapping->getMappingTitle('LeepAdmin_SpecialProduct_List', $price['specialProducts'][$idProduct]);
                }

                $order[] = $row;
            }

            $overridePrice = $customer->getPriceOverride();
            if ($overridePrice != null && $overridePrice != 0) {
                $discount += $orderPriceTotal - $overridePrice;
            }

            $subTotal += $orderPriceTotal;
        }
        $data['order'] = $order;
        $data['postage'] = $formatter->format($collectiveInvoice->getPostage(), 'money');
        $data['subtotal'] = $formatter->format($subTotal, 'money');
        $data['discount'] = $formatter->format($discount, 'money');
        $data['total']    = $formatter->format($subTotal - $discount + floatval($collectiveInvoice->getPostage()), 'money');

        $total = ($subTotal - $discount + floatval($collectiveInvoice->getPostage()));
        $taxAmount = $total * floatval($collectiveInvoice->getTaxValue()) / 100;
        $data['taxAmount'] = $formatter->format($taxAmount, 'money');
        $data['totalWithTax'] = $formatter->format($total + $taxAmount, 'money');

        // Update invoice amount
        if ($collectiveInvoice->getIsWithTax() == 1) {
            $finalTotal = $total + $taxAmount;
        }
        else {
            $finalTotal = $total;

        }
        $collectiveInvoice->setInvoiceAmount($finalTotal);

        // Currency conversion
        if ($collectiveInvoice->getIdCurrency() != 0) {
            $currency = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Currency')->findOneById($collectiveInvoice->getIdCurrency());
            if ($currency) {
                $exchangeRate = floatval($collectiveInvoice->getExchangeRate());
                $data['currency'] = $currency;
                $data['exchangeRate'] = $exchangeRate;
                $totalConverted = $exchangeRate * $finalTotal;
                $data['totalConverted'] = $formatter->format($totalConverted, 'money', $currency->getSymbol());
            }
        }

        // Extra text
        $data['textAfterDescription'] = nl2br($collectiveInvoice->getTextAfterDescription());
        $data['textFooter'] = nl2br($collectiveInvoice->getTextFooter());

        // Build

        $html = $controller->get('templating')->render('LeepAdminBundle:CollectiveInvoiceForm:pdf.html.twig', $data);

        $dompdf = $controller->get('slik_dompdf');
        $dompdf->getpdf($html);

        // Save to file
        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';
        $fileName = 'collective_invoice_'.$collectiveInvoice->getId().'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        $collectiveInvoice->setInvoiceFile($fileName);
        $em = $controller->get('doctrine')->getEntityManager();
        $em->persist($collectiveInvoice);
        $em->flush();

        return TRUE;
    }
}
