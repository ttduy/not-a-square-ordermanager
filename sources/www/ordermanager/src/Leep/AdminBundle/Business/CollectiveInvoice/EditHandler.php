<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUsers', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->invoiceNumber = $entity->getInvoiceNumber();
        $model->invoiceDate = $entity->getInvoiceDate();
        $model->invoiceFromCompanyId = $entity->getInvoiceFromCompanyId();
        $model->isWithTax = $entity->getIsWithTax();
        $model->destinationId = $entity->getDestinationId();
        $model->partnerId = $entity->getPartnerId();
        $model->distributionChannelId = $entity->getDistributionChannelId();
        $model->postage = $entity->getPostage();
        $model->invoiceStatus = $entity->getInvoiceStatus();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('section1', 'section', array(
            'property_path' => false,
            'label'         => 'Collective Invoice'
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId', 'choice', array(
            'label'    => 'Invoice From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('postage', 'money', array(
            'label'    => 'Postage',
            'required'  => false
        ));
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setName($model->name);
        $this->entity->setInvoiceNumber($model->invoiceNumber);
        $this->entity->setInvoiceDate($model->invoiceDate);
        $this->entity->setInvoiceFromCompanyId($model->invoiceFromCompanyId);
        $this->entity->setIsWithTax($model->isWithTax);
        $this->entity->setDestinationId($model->destinationId);
        $this->entity->setPartnerId($model->partnerId);
        $this->entity->setDistributionChannelId($model->distributionChannelId);
        $this->entity->setPostage($model->postage);
        $this->entity->setInvoiceStatus($model->invoiceStatus);
        
        parent::onSuccess();
    }
}
