<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'invoiceNumber', 'invoiceDate', 'invoiceFromCompanyId', 'partnerId', 'distributionChannelId', 'postage', 'invoiceStatus', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',     'width' => '15%',  'sortable' => 'true'),
            array('title' => 'Number',   'width' => '9%',   'sortable' => 'true'),
            array('title' => 'Date',     'width' => '9%',   'sortable' => 'true'),
            array('title' => 'From',     'width' => '9%',   'sortable' => 'true'),
            array('title' => 'Partner',  'width' => '10%',  'sortable' => 'true'),
            array('title' => 'Distribution Channel', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Postage',  'width' => '9%',   'sortable' => 'true'),
            array('title' => 'Status',   'width' => '9%',   'sortable' => 'true'),
            array('title' => 'Action',   'width' => '15%',  'sortable' => 'false')
            
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.invoicenumber';
        $this->columnSortMapping[] = 'p.invoicedate';
        $this->columnSortMapping[] = 'p.invoicefromcompanyid';
        $this->columnSortMapping[] = 'p.partnerid';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.postage';
        $this->columnSortMapping[] = 'p.invoicestatus';

        return $this->columnSortMapping;
    }
    
    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CollectiveInvoiceGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CollectiveInvoice', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->invoiceNumber) != '') {
            $queryBuilder->andWhere('p.invoicenumber LIKE :invoiceNumber')
                ->setParameter('invoiceNumber', '%'.trim($this->filters->invoiceNumber).'%');
        }
        if ($this->filters->invoiceDate != null) {
            $queryBuilder->andWhere('p.invoicedate = :invoiceDate')
                ->setParameter('invoiceDate', $this->filters->invoiceDate);
        }
        if ($this->filters->invoiceFromCompanyId != 0) {
            $queryBuilder->andWhere('p.invoicefromcompanyid = :invoiceFromCompanyId')
                ->setParameter('invoiceFromCompanyId', $this->filters->invoiceFromCompanyId);
        }
        if ($this->filters->partnerId != 0) {
            $queryBuilder->andWhere('p.partnerid = :partnerId')
                ->setParameter('partnerId', $this->filters->partnerId);
        }
        if ($this->filters->distributionChannelId != 0) {
            $queryBuilder->andWhere('p.distributionchannelid = :distributionChannelId')
                ->setParameter('distributionChannelId', $this->filters->distributionChannelId);
        }
        if ($this->filters->invoiceStatus != 0) {
            $queryBuilder->andWhere('p.invoicestatus = :invoiceStatus')
                ->setParameter('invoiceStatus', $this->filters->invoiceStatus);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_pdf',  'pdf',  array('id' => $row->getId())), 'button-pdf');
        $builder->addPopupButton('Email', $mgr->getUrl('leep_admin', 'collective_invoice', 'email',  'create', array('id' => $row->getId(), 'mode' => 'collective_invoice')), 'button-email');
        $builder->addPopupButton('Activity', $mgr->getUrl('leep_admin', 'collective_invoice', 'edit_activity',  'edit', array('id' => $row->getId())), 'button-activity');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('collectiveInvoiceModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_edit', 'list', array('id' => $row->getId())), 'button-edit');            
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'collective_invoice', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
