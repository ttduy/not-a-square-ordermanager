<?php 
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $collectiveInvoice = $doctrine->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($id);
        $collectiveInvoice->setIsDeleted(1);

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findBy(array(
            'collectiveinvoiceid' => $id
        ));
        foreach ($result as $r) {
            $r->setCollectiveInvoiceId(NULL);
        }

        $em->flush();
    }
}