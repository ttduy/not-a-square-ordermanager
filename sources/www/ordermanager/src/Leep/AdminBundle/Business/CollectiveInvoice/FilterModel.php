<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

class FilterModel {
    public $name;
    public $invoiceNumber;
    public $invoiceDate;
    public $invoiceFromCompanyId;
    public $partnerId;
    public $distributionChannelId;
    public $invoiceStatus;
}