<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Symfony\Component\Validator\ExecutionContext;

class EmailModel {
    public $subject;
    public $sendTo;
    public $cc;
    public $content;
}
