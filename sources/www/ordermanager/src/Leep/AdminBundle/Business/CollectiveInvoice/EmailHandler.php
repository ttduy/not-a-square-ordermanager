<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;

class EmailHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $id = $this->container->get('request')->query->get('id', 0);
        $collectiveInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice', 'app_main')->findOneById($id);
        $company = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($collectiveInvoice->getInvoiceFromCompanyId());
        $client = 'null';
        if ($collectiveInvoice->getPartnerId() != 0) {
            $client = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($collectiveInvoice->getPartnerId());
        }
        else {
            $client = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($collectiveInvoice->getDistributionChannelId());
        }
        if (empty($company)) {
            die("Please select a company");
        }

        $model = new EmailModel();        
        $model->subject = 'Collective Invoice - No. '.$collectiveInvoice->getInvoiceNumber();
        $model->sendTo = $client != null ? $client->getInvoiceDeliveryEmail() : '';
        $model->content = "Dear ".$client->getFirstName().' '.$client->getSurName().", \nPlease check the attachment for the invoice \n\nRegards, \n".$company->getCompanyName().".";

        /*$mgr = $this->container->get('easy_module.manager');
        $link = 'http://'.$_SERVER['HTTP_HOST'].$mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_pdf',  'pdf',  array('id' => $id));
        $model->content = str_replace('{{invoicePdfLink}}', $link, $model->content);
        */

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('sendTo', 'text', array(
            'label'    => 'Send To',
            'required' => true
        ));
        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'   => array(
                'rows'  => 6, 'cols' => 70
            )
        ));
    }

    public function onSuccess() {
        $id = $this->container->get('request')->query->get('id', 0);
        $collectiveInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice', 'app_main')->findOneById($id);
        $company = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($collectiveInvoice->getInvoiceFromCompanyId());
        $client = 'null';
        if ($collectiveInvoice->getPartnerId() != 0) {
            $client = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($collectiveInvoice->getPartnerId());
        }
        else {
            $client = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($collectiveInvoice->getDistributionChannelId());
        }

        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        
        $isSuccess = false;
        try {
            $senderEmail = $this->container->getParameter('sender_email');
            $senderName  = $this->container->getParameter('sender_name');

            if (trim($collectiveInvoice->getInvoiceFile()) == '') {
                $result = self::generatePdf($id);
                if ($result instanceof Response) {
                    throw new \Exception($result->getContent());
                }
            }
            $filePath = $this->container->getParameter('files_dir').'/invoices';
            $pdfData = file_get_contents($filePath.'/'.$collectiveInvoice->getInvoiceFile());

            $message = \Swift_Message::newInstance()
                ->setSubject($model->subject)
                ->setFrom($senderEmail, $senderName)
                ->setTo($model->sendTo)
                ->setBody($model->content, 'text/plain')
                ->attach(\Swift_Attachment::newInstance($pdfData, 'invoice.pdf', 'application/pdf'));                
            $this->container->get('mailer')->send($message);            
            $isSuccess = true;

            parent::onSuccess();
            $this->messages = array();
            $this->messages[] = 'The email has been sent';

            $helperActivity = $this->container->get('leep_admin.helper.activity');
            $notes = 'Send email "'.$model->subject.'" to '.$model->sendTo;
            if (!empty($model->cc)) { $notes.= ' (cc: '.$model->cc.') '; }
            $helperActivity->addCollectiveInvoiceActivity($id, Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT, $notes);
        }
        catch (\Exception $e) {
            $isSuccess = false;
            $this->errors[] = $e->getMessage();
        }        
    }

    private function generatePdf($id) {
        $dompdf = Utils::buildPdf($this->container, $id);
        return $dompdf;
    }
}
