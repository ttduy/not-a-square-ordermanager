<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $invoiceNumber;
    public $invoiceDate;
    public $invoiceFromCompanyId;
    public $isWithTax;
    public $destinationId;
    public $partnerId;
    public $distributionChannelId;
    public $postage;
    public $invoiceStatus;
}
