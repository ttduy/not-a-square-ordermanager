<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('section1', 'section', array(
            'property_path' => false,
            'label'         => 'Collective Invoice'
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => true
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId', 'choice', array(
            'label'    => 'Invoice From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('postage', 'money', array(
            'label'    => 'Postage',
            'required'  => false
        ));
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));

        $builder->add('section2', 'section', array(
            'property_path' => false,
            'label'         => 'Invoices'
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $collectiveInvoice = new Entity\CollectiveInvoice();
        $collectiveInvoice->setName($model->name);
        $collectiveInvoice->setInvoiceNumber($model->invoiceNumber);
        $collectiveInvoice->setInvoiceDate($model->invoiceDate);
        $collectiveInvoice->setInvoiceFromCompanyId($model->invoiceFromCompanyId);
        $collectiveInvoice->setIsWithTax($model->isWithTax);
        $collectiveInvoice->setDestinationId($model->destinationId);
        $collectiveInvoice->setPartnerId($model->partnerId);
        $collectiveInvoice->setDistributionChannelId($model->distributionChannelId);
        $collectiveInvoice->setPostage($model->postage);
        $collectiveInvoice->setInvoiceStatus($model->invoiceStatus);

        $em->persist($collectiveInvoice);
        $em->flush();

        $this->newId = $collectiveInvoice->getId();

        parent::onSuccess();        
    }
}
