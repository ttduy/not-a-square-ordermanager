<?php 
namespace Leep\AdminBundle\Business\CollectiveInvoice;

class Constant {
    const DESTINATION_PARTNER = 1;
    const DESTINATION_DISTRIBUTION_CHANNEL = 2;

    public static function getDestination() {
        return array(
            self::DESTINATION_PARTNER     => 'Partner',
            self::DESTINATION_DISTRIBUTION_CHANNEL  => 'Distribution Channel'
        );
    }

    const SHOW_ALL = 1;
    const SHOW_REPORT_SUBMITTED_ONLY = 2;
    public static function getShow() {
        return array(
            self::SHOW_ALL                     => 'All',
            self::SHOW_REPORT_SUBMITTED_ONLY   => 'Show report submitted / NutriMe submitted only',
        );
    }
}