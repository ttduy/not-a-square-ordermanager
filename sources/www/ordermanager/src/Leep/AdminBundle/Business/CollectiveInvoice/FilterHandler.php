<?php
namespace Leep\AdminBundle\Business\CollectiveInvoice;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('invoiceNumber', 'text', array(
            'label'    => 'Invoice Number',
            'required' => false
        ));
        $builder->add('invoiceDate', 'datepicker', array(
            'label'    => 'Invoice Date',
            'required' => false
        ));
        $builder->add('invoiceFromCompanyId', 'choice', array(
            'label'    => 'Invoice From',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));                
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));

        return $builder;
    }
}