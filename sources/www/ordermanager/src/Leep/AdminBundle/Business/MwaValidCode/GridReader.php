<?php
namespace Leep\AdminBundle\Business\MwaValidCode;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('code', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Code',        'width' => '60%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:MwaValidCode', 'p');

        if (trim($this->filters->code) != '') {
            $queryBuilder->andWhere('p.code LIKE :code')
                ->setParameter('code', '%'.trim($this->filters->code).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('mwaProcessorModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'mwa_valid_code', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'mwa_valid_code', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }
}