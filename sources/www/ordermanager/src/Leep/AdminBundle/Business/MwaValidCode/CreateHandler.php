<?php
namespace Leep\AdminBundle\Business\MwaValidCode;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('code', 'textarea', array(
            'attr'     => array(
                'placeholder' => 'Codes',
                'rows'        => 30,
                'cols'        => 70
            ),
            'required' => true,
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        set_time_limit(300);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $codes = explode("\n", $model->code);
        foreach ($codes as $code) {
            $code = trim($code);
            if (!empty($code)) {
                $validCode = new Entity\MwaValidCode();
                $validCode->setCode($code);
                $em->persist($validCode);
            }
        }
        $em->flush();

        parent::onSuccess();
    }
}