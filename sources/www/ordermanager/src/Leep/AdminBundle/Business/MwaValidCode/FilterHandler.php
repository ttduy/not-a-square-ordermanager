<?php
namespace Leep\AdminBundle\Business\MwaValidCode;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 

         $builder->add('code', 'text', array(
            'label'    => 'Code',
            'required' => false
        ));
        return $builder;
    }
}