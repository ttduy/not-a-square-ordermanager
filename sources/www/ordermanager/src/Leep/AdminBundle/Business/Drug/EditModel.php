<?php
namespace Leep\AdminBundle\Business\Drug;

class EditModel {
    public $drugName;
    public $cardioDrugName;
    public $pubchemId;
    public $drugBankId;
    public $mainPathwayLink;
    public $level1;
    public $level2;
    public $level3;
    public $level4;
    public $cypInteraction;
    public $cypBreakDown;
    public $halfLifeH;
    public $referencesCombined;
    public $ingredients;
}