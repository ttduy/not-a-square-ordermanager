<?php
    namespace Leep\AdminBundle\Business\Drug;

    use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
    use App\Database\MainBundle\Entity;
    use Leep\AdminBundle\Business;

    class CreateHandler extends BaseCreateHandler {
        public function getDefaultFormModel() {
            $model = new CreateModel();
            $model->ingredients = Utils::loadIngredientData($this->container, '');

            return $model;
        }

        public function buildForm($builder)
        {
            $builder->add('drugName', 'text', array(
            'label'    => "Drug name (drugName)",
            'required' => true
            ));
            $builder->add('cardioDrugName', 'text', array(
                'label'    => 'Cardio drug name (cardioDrugName)',
                'required' => false
            ));
            $builder->add('pubchemId', 'text', array(
                'label'    => 'Pubchem Id (pubchemId)',
                'required' => false
            ));
            $builder->add('drugBankId', 'text', array(
                'label'    => 'Drug bank id (drugBankId)',
                'required' => false
            ));
            $builder->add('mainPathwayLink', 'text', array(
                'label'    => 'Main path way link (mainPathwayLink)',
                'required' => false
            ));
            $builder->add('level1', 'text', array(
                'label'    => 'Level 1 (level1)',
                'required' => false
            ));
            $builder->add('level2', 'text', array(
                'label'    => 'Level 2 (level2)',
                'required' => false
            ));
            $builder->add('level3', 'text', array(
                'label'    => 'Level 3 (level3)',
                'required' => false
            ));
            $builder->add('level4', 'text', array(
                'label'    => 'Level 4 (level4)',
                'required' => false
            ));
            $builder->add('cypInteraction', 'checkbox', array(
                'label'    => 'CYP Interaction (cypInteraction)',
                'required' => false
            ));
            $builder->add('cypBreakDown', 'text', array(
                'label'    => 'CYP break down (cypBreakDown)',
                'required' => false
            ));
            $builder->add('halfLifeH', 'text', array(
                'label'    => 'Half life H (halfLifeH)',
                'required' => false
            ));
            $builder->add('referencesCombined', 'text', array(
                'label'    => 'References combined (referencesCombined)',
                'required' => false
            ));
            $builder->add('ingredients', 'textarea', array(
                'label'    => 'Ingredients',
                'attr'     => array(
                    'rows'        => 20,
                    'cols'        => 70
                ),
                'required' => false
            ));
        }

        public function onSuccess() {
            $model = $this->getForm()->getData();
            $em = $this->container->get('doctrine')->getEntityManager();

            $drugName = trim($model->drugName);
            if (empty($drugName)) {
                $this->errors[] = "Error!";
                $this->getForm()->get('drugName')->addError(new FormError("Drug name can not be empty"));
            }

            $drug = new Entity\Drug();
            $drug->setDrugName(trim($drugName));
            $drug->setCardioDrugName(trim($model->cardioDrugName));
            $drug->setPubchemId(trim($model->pubchemId));
            $drug->setDrugBankId(trim($model->drugBankId));
            $drug->setMainPathwayLink(trim($model->mainPathwayLink));
            $drug->setLevel1($model->level1);
            $drug->setLevel2($model->level2);
            $drug->setLevel3($model->level3);
            $drug->setLevel4($model->level4);
            $drug->setCypInteraction($model->cypInteraction);
            $drug->setCypBreakDown(floatval($model->cypBreakDown));
            $drug->setHalfLifeH(floatval($model->halfLifeH));
            $drug->setReferencesCombined($model->referencesCombined);
            $drug->setIngredients($model->ingredients);

            $em->persist($drug);
            $em->flush();

            parent::onSuccess();
        }
    }
?>