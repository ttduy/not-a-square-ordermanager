<?php
namespace Leep\AdminBundle\Business\Drug;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class Edithandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Drug', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->drugName = $entity->getDrugName();
        $model->cardioDrugName = $entity->getCardioDrugName();
        $model->pubchemId = $entity->getPubchemId();
        $model->drugBankId = $entity->getDrugBankId();
        $model->mainPathwayLink = $entity->getMainPathwayLink();
        $model->level1 = $entity->getLevel1();
        $model->level2 = $entity->getLevel2();
        $model->level3 = $entity->getLevel3();
        $model->level4 = $entity->getLevel4();
        $model->cypInteraction = $entity->getCypInteraction();
        $model->cypBreakDown = $entity->getCypBreakDown();
        $model->halfLifeH = $entity->getHalfLifeH();
        $model->referencesCombined = $entity->getReferencesCombined();
        $model->ingredients = $entity->getIngredients();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('drugName', 'text', array(
            'label'    => "Drug name (drugName)",
            'required' => true
        ));
        $builder->add('cardioDrugName', 'text', array(
            'label'    => 'Cardio drug name (cardioDrugName)',
            'required' => false
        ));
        $builder->add('pubchemId', 'text', array(
            'label'    => 'Pubchem Id (pubchemId)',
            'required' => false
        ));
        $builder->add('drugBankId', 'text', array(
            'label'    => 'Drug bank id (drugBankId)',
            'required' => false
        ));
        $builder->add('mainPathwayLink', 'text', array(
            'label'    => 'Main path way link (mainPathwayLink)',
            'required' => false
        ));
        $builder->add('level1', 'text', array(
            'label'    => 'Level 1 (level1)',
            'required' => false
        ));
        $builder->add('level2', 'text', array(
            'label'    => 'Level 2 (level2)',
            'required' => false
        ));
        $builder->add('level3', 'text', array(
            'label'    => 'Level 3 (level3)',
            'required' => false
        ));
        $builder->add('level4', 'text', array(
            'label'    => 'Level 4 (level4)',
            'required' => false
        ));
        $builder->add('cypInteraction', 'checkbox', array(
            'label'    => 'CYP Interaction (cypInteraction)',
            'required' => false
        ));
        $builder->add('cypBreakDown', 'text', array(
            'label'    => 'CYP break down (cypBreakDown)',
            'required' => false
        ));
        $builder->add('halfLifeH', 'text', array(
            'label'    => 'Half life H (halfLifeH)',
            'required' => false
        ));
        $builder->add('referencesCombined', 'text', array(
            'label'    => 'References combined (referencesCombined)',
            'required' => false
        ));
        $builder->add('ingredients', 'textarea', array(
            'label'    => 'Ingredients',
            'attr'     => array(
                'rows'        => 20,
                'cols'        => 70
            ),
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getManager();

        $this->entity->setDrugName(trim($model->drugName));
        $this->entity->setCardioDrugName(trim($model->cardioDrugName));
        $this->entity->setPubchemId(trim($model->pubchemId));
        $this->entity->setDrugBankId($model->drugBankId);
        $this->entity->setMainPathwayLink($model->mainPathwayLink);
        $this->entity->setLevel1($model->level1);
        $this->entity->setLevel2($model->level2);
        $this->entity->setLevel3($model->level3);
        $this->entity->setLevel4($model->level4);
        $this->entity->setCypInteraction($model->cypInteraction);
        $this->entity->setCypBreakDown($model->cypBreakDown);
        $this->entity->setHalfLifeH($model->halfLifeH);
        $this->entity->setReferencesCombined($model->referencesCombined);
        $this->entity->setIngredients($model->ingredients);

        $em->flush();
        parent::onSuccess();
    }
}
