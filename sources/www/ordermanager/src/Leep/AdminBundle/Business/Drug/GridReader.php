<?php
namespace Leep\AdminBundle\Business\Drug;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        return ['id', 'drugName', 'cardioDrugName', 'action'];
    }

    public function getTableHeader() {
        return array(
            array('title' => 'ID',                     'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Drug Name',              'width' => '40%', 'sortable' => 'true'),
            array('title' => 'Cardio Drug Name',       'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Action',                 'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Drug', 'p');

        if (trim($this->filters->drugName) != '') {
            $queryBuilder->andWhere('p.drugName LIKE :drugName')
                ->setParameter('drugName', '%'.trim($this->filters->drugName).'%');
        }
        if (!empty($this->filters->cardioDrugName)) {
            $queryBuilder->andWhere('p.cardioDrugName = :cardioDrugName')
                ->setParameter('cardioDrugName', $this->filters->cardioDrugName);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('drugModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'drug', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'drug', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
