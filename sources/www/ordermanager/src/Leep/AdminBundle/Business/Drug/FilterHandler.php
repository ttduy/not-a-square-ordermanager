<?php
namespace Leep\AdminBundle\Business\Drug;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Base\AppFilterHandler;
use Leep\AdminBundle\Business\Partner;
use Leep\AdminBundle\Business\Acquisiteur;
use Leep\AdminBundle\Business\DistributionChannel;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('drugName', 'text', array(
            'label'    => 'Drug name',
            'required' => true
        ));
        $builder->add('cardioDrugName', 'text', array(
            'label'    => 'Cardio drug name',
            'required' => true
        ));
        $builder->add('pubchemId', 'text', array(
            'label'    => 'Pubchem Id',
            'required' => true
        ));
        $builder->add('drugBankId', 'text', array(
            'label'    => 'Drug bank id',
            'required' => true
        ));

        return $builder;
    }
}
