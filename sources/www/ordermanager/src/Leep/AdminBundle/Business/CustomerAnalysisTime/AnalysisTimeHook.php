<?php
namespace Leep\AdminBundle\Business\CustomerAnalysisTime;

use Easy\ModuleBundle\Module\AbstractHook;

class AnalysisTimeHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $data['formTargetUrl'] = $mgr->getUrl('leep_admin', 'customer_analysis_time', 'feature', 'generateReport');
    }   
}
