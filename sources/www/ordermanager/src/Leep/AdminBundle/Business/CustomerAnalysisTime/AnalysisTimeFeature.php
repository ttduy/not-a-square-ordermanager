<?php
namespace Leep\AdminBundle\Business\CustomerAnalysisTime;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class AnalysisTimeFeature extends AbstractFeature {
    public function getActions() { return array('update'); }
    public function getDefaultOptions() {
        return array(
        );
    }

    public function handleGenerateReport($controller, &$data, $options = array()) {
        $form = $controller->get('request')->get('form');
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $hideOrders = array("ERROR");
        if (isset($form['hideOrders'])) {
            $list = explode("\n", $form['hideOrders']);
            foreach ($list as $orderNumber) {
                $hideOrders[] = trim($orderNumber);
            }
        }

        // Search customers
        $query = $em->createQueryBuilder();
        $query->select('DISTINCT p.id, p.ordernumber, p.firstname, p.surname, p.dateordered, p.distributionchannelid')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:OrderedProduct', 'op', 'WITH', 'p.id = op.customerid')
            ->leftJoin('AppDatabaseMainBundle:OrderedCategory', 'oc', 'WITH', 'p.id = oc.customerid')
            ->orderBy('p.id', 'DESC')
            ->andWhere('p.ordernumber NOT IN (:blackList)')
            ->setParameter('blackList', $hideOrders);

        if ($form['idDistributionChannel'] != 0) {
            $query->andWhere('p.distributionchannelid = :idDistributionChannel')
                ->setParameter('idDistributionChannel', intval($form['idDistributionChannel']));
        }

        if (isset($form['products'])) {
            $query->andWhere('op.productid IN (:products)')
                ->setParameter('products', $form['products']);
        }
        if (isset($form['categories'])) {
            $query->andWhere('oc.categoryid IN (:categories)')
                ->setParameter('categories', $form['categories']);
        }

        if ($form['startDate'] != '') {
            $query->andWhere('p.dateordered >= :startDate')
                ->setParameter('startDate', \DateTime::createFromFormat('d/m/Y', $form['startDate']));
        }
        if ($form['endDate'] != '') {
            $query->andWhere('p.dateordered <= :endDate')
                ->setParameter('endDate', \DateTime::createFromFormat('d/m/Y', $form['endDate']));
        } 

        // Set data
        $data['startDate'] = $form['startDate'];
        $data['endDate'] = $form['endDate'];
        
        $products = array();
        if (isset($form['products'])) {
            foreach ($form['products'] as $idProduct) {
                $products[] = $formatter->format($idProduct, 'mapping', 'LeepAdmin_Product_List');
            }
        }
        $data['products'] = implode(', ', $products);
        
        $categories = array();
        if (isset($form['categories'])) {
            foreach ($form['categories'] as $idCategory) {
                $categories[] = $formatter->format($idCategory, 'mapping', 'LeepAdmin_Category_List');
            }
        }
        $data['categories'] = implode(', ', $categories);

        $data['idDistributionChannel'] = $formatter->format($form['idDistributionChannel'], 'mapping', 'LeepAdmin_DistributionChannel_List');
        $data['fromStatus'] = $formatter->format($form['fromStatus'], 'mapping', 'LeepAdmin_Customer_Status');
        $data['toStatus'] = $formatter->format($form['toStatus'], 'mapping', 'LeepAdmin_Customer_Status');

        // Search customer
        $analysisTime = array();
        $result = $query->getQuery()->getResult();

        $cId = array(-1);
        foreach ($result as $r) {
            $cId[] = $r['id'];
            $analysisTime[$r['id']] = array(
                'dateOrdered'   => $formatter->format($r['dateordered'], 'date'),
                'dc'            => $formatter->format($r['distributionchannelid'], 'mapping', 'LeepAdmin_DistributionChannel_List'),
                'orderNumber'   => $r['ordernumber'],
                'name'          => $r['firstname'].' '.$r['surname'],
                'analysisTime'  => "UNDEFINED"
            );
        }

        // Compute analysis time
        $fromStatusDate = array();
        $query = $em->createQueryBuilder();
        $query->select('p.customerid, p.statusdate')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid in (:customerList)')
            ->andWhere('p.status = :fromStatus')
            ->andWhere('p.statusdate IS NOT NULL')
            ->setParameter('fromStatus', $form['fromStatus'])
            ->setParameter('customerList', $cId)
            ->orderBy('p.statusdate', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $fromStatusDate[$r['customerid']] = $r['statusdate']->getTimestamp();
        }

        $toStatusDate = array();
        $query = $em->createQueryBuilder();
        $query->select('p.customerid, p.statusdate')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid in (:customerList)')
            ->andWhere('p.status = :toStatus')
            ->andWhere('p.statusdate IS NOT NULL')
            ->setParameter('toStatus', $form['toStatus'])
            ->setParameter('customerList', $cId)
            ->orderBy('p.statusdate', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $toStatusDate[$r['customerid']] = $r['statusdate']->getTimestamp();
        }        

        // Compute
        $sumDay = 0;
        $countOrder = 0;
        $totalOrder = 0;
        $isHideUndefined = intval($form['isHideUndefined']);
        foreach ($analysisTime as $idCustomer => $d) {
            if (isset($fromStatusDate[$idCustomer]) && isset($toStatusDate[$idCustomer])) {
                $datediff = ($toStatusDate[$idCustomer] - $fromStatusDate[$idCustomer]) / 86400;
                $datediff++;
                $analysisTime[$idCustomer]['analysisTime'] = $datediff;

                $sumDay += $datediff;
                $countOrder ++;                
            }
            else {
                if ($isHideUndefined) {
                    unset($analysisTime[$idCustomer]);
                    $totalOrder--;   
                }
            }
            $totalOrder++;
        }

        $data['analysisTime'] = $analysisTime;
        $data['averageTime'] = ($countOrder != 0) ? number_format($sumDay / $countOrder, 2) : '0.00';
        $data['countOrder'] = $countOrder;
        $data['totalOrder'] = $totalOrder;

        return $controller->render('LeepAdminBundle:CustomerAnalysisTime:generate_report.html.twig', $data);
    }
}
