<?php
namespace Leep\AdminBundle\Business\CustomerAnalysisTime;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class AnalysisTimeHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new AnalysisTimeModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start Date',
            'required' => false
        ));
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'End Date',
            'required' => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('categories', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'multiple' => 'multiple',
            'attr'     => array(
                'style'   => 'height: 250px'
            )
        ));
        $builder->add('products', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array(
                'style'   => 'height: 250px'
            )
        ));
        $builder->add('fromStatus',     'choice', array(
            'label'  => 'From status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status')            
        ));
        $builder->add('toStatus',     'choice', array(
            'label'  => 'To status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status')            
        ));
        $builder->add('hideOrders', 'textarea', array(
            'label'  => 'Hide these orders',
            'required' => false,
            'attr'    => array(
                'rows'  => 15, 'cols' => 50
            )
        ));
        $builder->add('isHideUndefined', 'checkbox', array(
            'label'  => "Hide 'UNDEFINED'",
            'required' => false,
        ));
    }

    public function onSuccess() {

    }
}
