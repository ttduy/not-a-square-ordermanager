<?php
namespace Leep\AdminBundle\Business\CustomerAnalysisTime;

use Symfony\Component\Validator\ExecutionContext;

class AnalysisTimeModel {
    public $startDate;
    public $endDate;
    public $idDistributionChannel;
    public $categories;
    public $products;
    public $fromStatus;
    public $toStatus;
    public $hideOrders;
    public $isHideUndefined;
}
