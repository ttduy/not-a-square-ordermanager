<?php  
	namespace Leep\AdminBundle\Business\GenoMaterialType;

	use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
	use App\Database\MainBundle\Entity;
	use Symfony\Component\Form\FormError;

	class CreateHandler extends BaseCreateHandler{
		public function getDefaultFormModel()
		{
			$model = new CreateModel();
			$model->createdDate = new \DateTime();
			return $model;
		}

		public function buildForm($builder)
		{
			$mapping = $this->container->get('easy_mapping');
			$builder->add('typeName', 'text', array(
				'label' 	=> 'Type name',
				'required' 	=> true
			));
		}

		public function onSuccess()
		{
			$em = $this->container->get('doctrine')->getEntityManager();
			$model = $this->getForm()->getData();
			$genoMaterialType = new Entity\GenoMaterialTypes();
			$typeName = trim($model->typeName);
			if (empty($typeName)) {
				$this->errors[] = "Error!";
				$this->getForm()->get("typeName")->addError(new FormError("Geno Material type name can not be empty"));
				return false;
			}
			$typeNameCheck = $em->getRepository('AppDatabaseMainBundle:GenoMaterialTypes')->findOneByTypeName($typeName);
			if ($typeNameCheck) {
				$this->errors[] = "Error!";
	            $this->getForm()->get('typeName')->addError(new FormError("Type name already exist!"));
	            return false;
			}
			$genoMaterialType->setTypeName($typeName);
			$genoMaterialType->setCreatedDate($model->createdDate);

			$em->persist($genoMaterialType);
			$em->flush();
			parent::onSuccess();
		}
	}
?>