<?php
	namespace Leep\AdminBundle\Business\GenoMaterialType;

	use Leep\AdminBundle\Business\Base\AppFilterHandler;

	class FilterHandler extends AppFilterHandler 
	{
		
		public function getDefaultFormModel()
		{
			$model = new FilterModel();
			return $model;
		}

		public function buildForm($builder)
		{
			$mapping = $this->container->get('easy_mapping');
			$builder->add('typeName', 'text', array(
				'label'		=> 	'Type name',
				'required'	=>	false
			));
			return $builder;
		}
	}
?>