<?php  
	namespace Leep\AdminBundle\Business\GenoMaterialType;

	use Leep\AdminBundle\Business\Base\AppGridDataReader;

	class GridReader extends AppGridDataReader
	{
		public $filters;
		public function getColumnMapping()
		{
			return array('typeName', 'action');
		}

		public function getTableHeader()
		{
			return array(
				array('title' => 'Type name', 'width'=> '50%', 'sortable'	=> 'true'),
				array('title' => 'Action', 'width'	=> '50%', 'sortable'	=> 'true')
			);
		}

		public function getFormatter()
		{
			return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GenoMaterialType');
		}

		public function buildQuery($query)
		{
			$query->select('p')
				  ->from('AppDatabaseMainBundle:GenoMaterialTypes', 'p');
			if (trim($this->filters->typeName) != '') {
				$query->andWhere('p.typeName LIKE :typeName')
						->setParameter('typeName', '%'.trim($this->filters->typeName).'%');
			}
		}

		public function buildCellAction($row)
		{
	        $builder = $this->container->get('leep_admin.helper.button_list_builder');
	        $mgr = $this->getModuleManager();
	        $security = $this->container->get('leep_admin.helper.security');

	        if ($security->hasPermission('genoMaterialTypeModify')) {
	            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_material_type', 'edit', 'edit', array(
	            	'id' => $row->getId())), 'button-edit');
	            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_material_type', 'delete', 'delete', array(
	            	'id' => $row->getId())), 'button-delete');
			}
			
	        return $builder->getHtml();
		}
	}
?>