<?php
    namespace Leep\AdminBundle\Business\GenoMaterialType;

    /**
    *
    */
    class Utils
    {
        static public function getGenoMaterialTypeList($container)
        {
            $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $query->select('p.id, p.typeName')
                    ->from('AppDatabaseMainBundle:GenoMaterialTypes', 'p')
                    ->addOrderBy('p.typeName', 'ASC');
            $queryResult = $query->getQuery()->getResult();

            $typeNames = [];
            foreach ($queryResult as $qr){
                $typeNames[$qr['id']] = $qr['typeName'];
            }

            return $typeNames;
        }
    }

?>