<?php  
	namespace Leep\AdminBundle\Business\GenoMaterialType;

	use Leep\AdminBundle\Business\Base\AppEditHandler;
	use App\Database\MainBundle\Entity;
	use Symfony\Component\Form\FormError;

	/**
	* 
	*/
	class EditHandler extends AppEditHandler
	{
		public function loadEntity($request)
		{
			$id = $request->query->get('id');
			$this->id = $id;
			return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoMaterialTypes')->findOneById($id);
		}
		
		public function convertToFormModel($entity)
		{
			$model = new EditModel();
			$model->typeName = $entity->getTypeName();
			return $model;
		}

		public function buildForm($builder)
		{
			$mapping = $this->container->get('easy_mapping');
			$builder->add('typeName', 'text', array(
				'label'		=> 'Type name',
				'required'	=> true
			));

			return $builder;
		}

		public function onSuccess()
		{
			$model = $this->getForm()->getData();
			$em = $this->container->get('doctrine')->getEntityManager();

			$typeName = trim($model->typeName);
			if (empty($typeName)) {
				$this->errors[] = "Error!";
				$this->getForm()->get("typeName")->addError(new FormError("Geno Material type name can not be empty"));
				return false;
			}
			$typeNameCheck = $em->getRepository('AppDatabaseMainBundle:GenoMaterialTypes')->findOneByTypeName($typeName);
			if ($typeNameCheck) {
				$this->errors[] = "Error!";
	            $this->getForm()->get('typeName')->addError(new FormError("Type name already exist!"));
	            return false;
			}
			$this->entity->setTypeName(trim($typeName));
			$this->entity->setCreatedDate(new \DateTime());
			$em->flush();
			parent::onSuccess();
		}
	}
?>