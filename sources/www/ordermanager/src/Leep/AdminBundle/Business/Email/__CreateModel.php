<?php
namespace Leep\AdminBundle\Business\Email;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $subject;
    public $sendTo;
    public $cc;
    public $content;
}
