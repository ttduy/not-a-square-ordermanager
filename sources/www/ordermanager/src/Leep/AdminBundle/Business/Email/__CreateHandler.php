<?php
namespace Leep\AdminBundle\Business\Email;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $id = $this->container->get('request')->query->get('id', 0);
        $mode = $this->container->get('request')->query->get('mode', 'customer');

        $model = new CreateModel();        
        if ($mode == 'customer') {
            $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
            $company = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($customer->getInvoiceFromCompanyId());

            $model->subject = 'Invoice - No. '.$customer->getInvoiceNumber();
            $model->sendTo = $customer->getEmail();
            $model->content = "Dear ".$customer->getFirstName().' '.$customer->getSurName().", \nPlease check the following link for the invoice {{invoicePdfLink}}\n\nRegards, \n".$company->getCompanyName().".";

            $mgr = $this->container->get('easy_module.manager');
            $link = 'http://'.$_SERVER['HTTP_HOST'].$mgr->getUrl('leep_admin', 'invoice', 'customer_form_pdf',  'pdf',  array('id' => $id));
            $model->content = str_replace('{{invoicePdfLink}}', $link, $model->content);
        }
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('sendTo', 'text', array(
            'label'    => 'Send To',
            'required' => true
        ));
        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'   => array(
                'rows'  => 6, 'cols' => 70
            )
        ));
    }

    public function onSuccess() {
        $id = $this->container->get('request')->query->get('id', 0);
        $mode = $this->container->get('request')->query->get('mode', 'customer');

        if ($mode == 'customer') {
            $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
            $company = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Companies')->findOneById($customer->getInvoiceFromCompanyId());
        }

        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        
        $isSuccess = false;
        try {
            $senderEmail = $this->container->getParameter('sender_email');
            $senderName  = $this->container->getParameter('sender_name');
            $message = \Swift_Message::newInstance()
                ->setSubject($model->subject)
                ->setFrom($senderEmail, $senderName)
                ->setTo($model->sendTo)
                ->setCc($model->cc)
                ->setBody($model->content, 'text/plain');
            $this->container->get('mailer')->send($message);            
            $isSuccess = true;

            parent::onSuccess();
            $this->messages = array();
            $this->messages[] = 'The email has been sent';

            $helperActivity = $this->container->get('leep_admin.helper.activity');
            $notes = 'Send email "'.$model->subject.'" to '.$model->sendTo;
            if (!empty($model->cc)) { $notes.= ' (cc: '.$model->cc.') '; }
            $helperActivity->addCustomerActivity($id, Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT, $notes);
        }
        catch (\Exception $e) {
            $isSuccess = false;
            $this->errors[] = $e->getMessage();
        }        
    }
}
