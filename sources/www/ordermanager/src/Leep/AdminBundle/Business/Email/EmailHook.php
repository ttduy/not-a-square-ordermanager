<?php
namespace Leep\AdminBundle\Business\Email;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EmailHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mode = $controller->getRequest()->query->get('mode', '');
        $id   = $controller->getRequest()->query->get('id', 0);

        $formatter = $controller->get('leep_admin.helper.formatter');
        $mapping = $controller->get('easy_mapping');

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        // MODE
        if ($mode == 'customer') {
            $query->select('a')
                ->from('AppDatabaseMainBundle:CustomerActivity', 'a')
                ->andWhere('a.customerid = :customerId')
                ->setParameter('customerId', $id);
        }
        else if ($mode == 'collective_invoice') {
            $query->select('a')
                ->from('AppDatabaseMainBundle:CollectiveInvoiceActivity', 'a')
                ->andWhere('a.collectiveinvoiceid = :collectiveInvoiceId')
                ->setParameter('collectiveInvoiceId', $id);   
        }
        else {
            return false;
        }

        // DISTRIBUTION EMAIL TEMPLATE
        if ($mode == 'customer') {
            $customer = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
            if ($customer) {
                $status = $controller->getRequest()->query->get('status', 0);
                $filters = array(
                    'status' => $controller->getRequest()->query->get('status', 0),
                    'distributionchannelid' => $customer->getDistributionChannelId()
                );
                $r = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findOneBy($filters);
                if ($r) {
                    $data['preSelectTemplateId'] = $r->getEmailTemplateId();
                }
            }
            
        }


        // ACTIVITIES
        $query
            ->andWhere('a.activitytypeid = :activityTypeEmailId')
            ->setParameter('activityTypeEmailId', Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT)
            ->orderBy('a.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
            
        $activities = array();
        foreach ($result as $row) {
            $activities[] = array(
                'time' => $formatter->format($row->getActivityDate(), 'date').' '.$mapping->getMappingTitle('LeepAdmin_Time_List', $row->getActivityTimeId()),
                'notes' => $row->getNotes()
            );
        }
        $data['activities'] = $activities;
    }
}
