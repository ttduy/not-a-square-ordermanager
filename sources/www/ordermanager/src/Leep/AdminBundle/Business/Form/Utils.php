<?php 
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business;

class Utils {
    public static function getFormSteps($container, $idFormVersion) {
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('p')
            ->from('AppDatabaseMainBundle:FormStep', 'p')
            ->andWhere('p.idFormVersion = :idFormVersion')
            ->setParameter('idFormVersion', $idFormVersion)
            ->orderBy('p.sortOrder', 'ASC');

        $mapping = array();
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $mapping[$r->getId()] = $r->getName();
        }

        return $mapping;
    }

    public static function getFormVersions($container, $idForm) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('p')
            ->from('AppDatabaseMainBundle:FormVersion', 'p')
            ->andWhere('p.idForm = :idForm')
            ->setParameter('idForm', $idForm)
            ->orderBy('p.version', 'ASC');

        $mapping = array();
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $mapping[$r->getId()] = 'Version '.$r->getVersion().' at '.$formatter->format($r->getCreationDatetime(), 'datetime');
        }

        return $mapping;
    }

    /*
    * recompute the note
    * return an array of step and its inputs
    * for example:
    * [
    *   [
    *       'step': 'step 1',
    *       'value': [
    *                   [  
    *                       'label': 'input one',
    *                       'value': 'input value'
    *                   ]
    *                ]
    *   ]
    * ]
    */
    public static function recomputeAdditionalInfo($container, $idFormSession, $idFormVersion, $idActiveFormStep) {
        $em = $container->get('doctrine')->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');
        $query = $em->createQueryBuilder();
        $formStepData = "Steps:";

        // get all steps data
        $itemNames = array();
        $query
            ->select('
                fi.id,
                fi.idType,
                fi.data
                ')
            ->from('AppDatabaseMainBundle:FormStep', 'fs')
            ->innerJoin('AppDatabaseMainBundle:FormItem', 'fi', 'WITH', 'fs.id = fi.idFormStep')
            ->andWhere('fs.idFormVersion = :idFormVersion')
            ->setParameter('idFormVersion', $idFormVersion);

        // if current active form step is not summary step
        if ($idActiveFormStep > 0) {
            $query
                ->andWhere('fs.id <= :idActiveFormStep')
                ->setParameter('idActiveFormStep', $idActiveFormStep);
        }


        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            // process each form item
            $itemData = json_decode($r['data'], true);
            $itemNames[$r['id']] = (array_key_exists('text', $itemData)) ? array($itemData['text'], $r['idType']) : array(null, $r['idType']);
        }

        // get inputed data
        $queryTwo = $em->createQueryBuilder();
        $queryTwo
            ->select('
                fss.data as stepData,
                fs.name as stepName
                ')
            ->from('AppDatabaseMainBundle:FormSessionStep', 'fss')
            ->innerJoin('AppDatabaseMainBundle:FormStep', 'fs', 'WITH', 'fss.idFormStep = fs.id')
            ->andWhere('fss.idFormSession = :idFormSession')
            ->setParameter('idFormSession', $idFormSession);
        $results = $queryTwo->getQuery()->getResult();

        $arrData = array();
        foreach ($results as $r) {
            // process step data
            // sample: {"65":{"value":"cool"},"66":{"value":"08\/02\/2015"},"67":{"value":"1"}}
            $decodedData = json_decode($r['stepData'], true);

            // collect input data of current Step
            $curStepInput = array();
            foreach ($decodedData as $itemId => $itemValue) {
                // check if label for item available - FormItem.data['text']
                $itemLabel = null;
                $value = '';
                if (array_key_exists($itemId, $itemNames)) {
                    if ($itemNames[$itemId][0]) {
                        $itemLabel = $itemNames[$itemId][0];
                    } else if (($itemNames[$itemId][1]) === Constant::FORM_ITEM_TYPE_DATE) {
                        $itemLabel = 'Date';
                    }


                    if ($itemNames[$itemId][1] === Constant::FORM_ITEM_TYPE_INSTRUCTION) {
                        continue;
                    }

                    $value = (($itemValue['value']) ? $itemValue['value'] : '');
                    if ($itemNames[$itemId][1] === Constant::FORM_ITEM_TYPE_PERSON ||
                        $itemNames[$itemId][1] === Constant::FORM_ITEM_TYPE_AUTHORIZED_PERSONNEL) {
                        $value = $formatter->format($value, 'mapping', 'LeepAdmin_WebUser_List');
                    }
                }

                $curStepInput[] = array(
                    'label' => $itemLabel,
                    'value' => $value
                );
            }

            // finalize data for current Step
            $arrData[] = array(
                'step'  => $r['stepName'],
                'value' => $curStepInput
            );
        }

        return $arrData;
    }

    public static function generateNoteWebInfo($encodedNoteData) {
        $formStepData = array();
            
        $i = 1;
        if ($encodedNoteData) {
            //$formStepData = $encodedNoteData;

            // get note, refer Utils::recomputeAdditionalInfo()
            $decodedNote = json_decode($encodedNoteData, true);
            foreach ($decodedNote as $eachStep) {
                $stepData = array();
                foreach ($eachStep['value'] as $eachInput) {
                    if ($eachInput['label']) {
                        $stepData []= sprintf("- %s: <i style='color: red'>%s</i>",
                            $eachInput['label'],
                            ($eachInput['value']) ? $eachInput['value'] : '(?)'
                        );
                    } else {
                        $stepData []= sprintf("- %s",
                            ($eachInput['value']) ? $eachInput['value'] : '(?)'
                        );
                    }
                }

                // build template in case there's no data necessary to be input into a Step if it's only about Instruction
                if (empty($stepData)) {
                    $tplStepData = "%s. %s %s";
                    $stepDataHtml = '';
                }
                else {
                    $tplStepData = "%s. %s <br/>%s";
                    $stepDataHtml = implode("<br/>", $stepData);
                }

                $formStepData []= sprintf($tplStepData,
                    $i++,
                    $eachStep['step'],
                    $stepDataHtml
                );
            }
        }

        return implode("<br/>", $formStepData);
    }


    public static function captureCurrentLots($container) {
        $em = $container->get('doctrine')->getEntityManager();
        $currentLots = array();

        // get supplement type
        $currentLots['supplementType'] = array();
        $result = $em->getRepository('AppDatabaseMainBundle:SupplementType')->findAll();
        foreach ($result as $r) {
            $currentLots['supplementType'][] = array(
                'name'     => $r->getName(),
                'letter'   => $r->getLetter(),
                'lotInUse' => $r->getLotInUse()
            );
        }

        // get tested and in-use Pellet
        $currentLots['testedAndInUsePellet'] = array();
        $query = $em->createQueryBuilder();
        $query->select('
                pe.idPellet as id,
                pe.cups, pe.section, p.notes,
                p.letter, p.name as pelletName
            ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'pe')
            ->leftJoin('AppDatabaseMainBundle:Pellet', 'p', 'WITH', 'pe.idPellet = p.id')
            ->andWhere('pe.section = :sectionInd')
            ->setParameter('sectionInd', Business\Pellet\Constant::SECTION_TESTED_AND_IN_USE);

        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $currentLots['testedAndInUsePellet'][] = array(
                'name'     => $r['pelletName'],
                'letter'   => $r['letter'],
                'cups' => $r['cups'],
                'ordered' => 0,
                'activeIngredient' => 0,
                'shipment' => 0,
                'expiryDate' => null,
                'notes' => $r['notes'],
                'status' => array_key_exists($r['section'], Business\Pellet\Constant::getSections()) ? Business\Pellet\Constant::getSections()[$r['section']] : null
            );
        }

        // get selected used-up-Material
        $currentLots['selectedUsedUpMaterial'] = array();
        $query = $em->createQueryBuilder();
        $query->select('p.quantity, p.usedfor, p.datetaken, p.takenby, m.name as materialName')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('p.isSelected = :isSelected')
            ->setParameter('isSelected', 1)
            ->orderBy('p.datetaken', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $currentLots['selectedUsedUpMaterial'][] = array(
                'materialName'  => $r['materialName'],
                'quantity'      => $r['quantity'],
                'usedFor'       => $r['usedfor'],
                'dateTaken'     => ($r['datetaken'] ? $r['datetaken']->format('d/m/Y') : ''),
                'takenBy'       => $r['takenby']
            );
        }

        return $currentLots;
    }
}
