<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class InstructionItemHandler extends BaseItemHandler {
    public function render($index, $item, $itemData) {
        $data = array();

        $itemInfo = @json_decode($item->getData(), true);
        $data['index'] = $index;
        $data['instruction'] = isset($itemInfo['text']) ? $itemInfo['text'] : '(no instruction)';
        return $this->templating->render('LeepAdminBundle:Form:instruction_item.html.twig', $data);
    }
}
