<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class BaseItemHandler {
    public $container;
    public $templating;
    public function __construct($container) {
        $this->container = $container;
        $this->templating = $container->get('templating');
    }
}