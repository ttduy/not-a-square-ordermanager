<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class AuthorizedPersonnelItemHandler extends BaseItemHandler {
    public function render($index, $item, $itemData) {
        $stepInfo = @json_decode($item->getData(), true);

        // exclude current logged-in User by default
        $excludeMe = (array_key_exists('excludeMe', $stepInfo)) ? $stepInfo['excludeMe'] : true;

        $authorizedPersonnelIds = (array_key_exists('authorizedPersonnel', $stepInfo)) ? $stepInfo['authorizedPersonnel'] : null;

        $data = array();
        $data['index'] = $index;
        $data['id'] = $item->getId();
        $data['value'] = isset($itemData['value']) ? $itemData['value'] : '';
        $data['instruction'] = isset($stepInfo['text']) ? $stepInfo['text'] : '(no instruction)';
        $data['choices'] = $this->getFormPersons($excludeMe, $authorizedPersonnelIds);

        return $this->templating->render('LeepAdminBundle:Form:authorized_personnel_item.html.twig', $data);
    }

    // current logged-in User won't be inlcuded in the list
    public function getFormPersons($excludeMe, $authorizedPersonnelIds) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');            
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $map = array();
        if ($authorizedPersonnelIds) {
            $query = $em->createQueryBuilder();
            $query->select('wu.id, wu.username')
                ->from('AppDatabaseMainBundle:AuthorizedPersonnel', 'p')
                ->innerJoin('AppDatabaseMainBundle:WebUserPermission', 'm', 'WITH', 'FIND_IN_SET(m.idWebUser, p.webUsers) > 0')
                ->innerJoin('AppDatabaseMainBundle:WebUsers', 'wu', 'WITH', 'wu.id = m.idWebUser')
                ->andWhere('m.permissionKey = :formViewPermission')
                ->setParameter('formViewPermission', 'formSessionView')
                ->andWhere('p.id IN (:authorizedPersonnelIds)')
                ->setParameter('authorizedPersonnelIds', $authorizedPersonnelIds)
                ->orderBy('wu.username', 'ASC');

            // if current logged-in User is excluded from this list
            if ($excludeMe) {
                    $query->andWhere('m.idWebUser <> :currentLoggedInUserId')
                            ->setParameter('currentLoggedInUserId', $userManager->getUser()->getId());
            }

            $results = $query->getQuery()->getResult();

            foreach ($results as $r) {
                $map[$r['id']] = $r['username'];
            }
        }

        return $map;
    }
}
