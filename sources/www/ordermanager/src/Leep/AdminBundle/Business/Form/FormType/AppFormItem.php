<?php
namespace Leep\AdminBundle\Business\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFormItem extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_form_item';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        
        $manager = $this->container->get('easy_module.manager');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $utils = $this->container->get('leep_admin.report.business.util');
        $mgr = $this->container->get('easy_module.manager');
        $cacheBoxData = $this->container->get('leep_admin.helper.cache_box_data');

        $builder->add('id',                 'hidden');
        $builder->add('idType',             'choice', array(
            'label'   => 'Type',
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_FormItem_Type')
        ));
        $builder->add('sortOrder',          'hidden');

        $ctrlMap = $mapping->getMapping('LeepAdmin_FormItem_CtrlMapping');
        foreach ($ctrlMap as $v) {
            $class = '\Leep\AdminBundle\Business\Form\FormType\AppFormItem'.ucfirst($v);
            $builder->add($v, new $class($this->container));
        }
    }
}