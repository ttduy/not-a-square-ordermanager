<?php
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditStepHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormVersion', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = new EditStepModel();

        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($this->entity->getIdForm());

        $model->name = $form->getName();
        $model->formType = $form->getFormType();
        $model->formNumber = $form->getFormNumber();
        $model->description = $form->getDescription();
        $model->sortOrder = $form->getSortOrder();
        $model->colorCode = $form->getColorCode();
        $model->idFormVersion = $entity->getId();
        $model->alertOldVersion = $form->getAlertOldVersion();
        $model->idActiveFormVersion = $form->getIdActiveFormVersion();
        $model->idLanguage = 1;
        $model->stepList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FormStep', 'p')
            ->andWhere('p.idFormVersion = :idFormVersion')
            ->setParameter('idFormVersion', $entity->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $model->stepList[] = array(
                'id'   => $r->getId(),
                'name' => $r->getName()
            );
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('formType', 'text', array(
            'label'    => 'Form Type',
            'required' => true
        ));
        $builder->add('formNumber', 'text', array(
            'label'    => 'Form Number',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 10, 'cols' => 70
            )
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => false
        ));
        $builder->add('colorCode', 'text', array(
            'label'     => 'Color Code',
            'required'  => false
        ));


        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $attr = [];
        if (!$helperSecurity->hasPermission('formActiveVersionModify')) {
            $attr = [
                'readonly' =>   'true',
                'class' =>      'disabled-field'
            ];
        }

        $builder->add('idActiveFormVersion', 'choice', array(
            'label'    => 'Active version',
            'required' => true,
            'empty_value' => false,
            'attr'     => $attr,
            'choices'  => Utils::getFormVersions($this->container, $this->entity->getIdForm())
        ));

        $builder->add('sectionFormVersion', 'section', array(
            'label'    => 'Form Version',
            'property_path' => false
        ));
        $builder->add('idFormVersion', 'choice', array(
            'label'    => 'Version',
            'required' => false,
            'empty_value' => false,
            'choices'  => Utils::getFormVersions($this->container, $this->entity->getIdForm())
        ));
        $builder->add('alertOldVersion', 'checkbox', array(
            'label'    => 'Alert',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes (your reason to update this version)',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 90
            )
        ));

        $builder->add('sectionSteps', 'section', array(
            'label'    => 'Steps',
            'property_path' => false
        ));
        $builder->add('stepList', 'collection', array(
            'type' => new FormType\AppFormStep($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Section',
                'required' => false
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($this->entity->getIdForm());

        $form->setName($model->name);
        $form->setFormType($model->formType);
        $form->setFormNumber($model->formNumber);
        $form->setDescription($model->description);
        $form->setSortOrder($model->sortOrder);
        $form->setColorCode($model->colorCode);
        $form->setAlertOldVersion($model->alertOldVersion);
        $form->setIdActiveFormVersion($model->idActiveFormVersion);

        // Unchange id
        $unchangedIdArr = array();
        foreach ($model->stepList as $step) {
            $unchangedIdArr[$step['id']] = $step;
        }

        // Update
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FormStep')->findByIdFormVersion($this->entity->getId());
        foreach ($results as $r) {
            if (isset($unchangedIdArr[$r->getId()])) {
                $step = $unchangedIdArr[$r->getId()];
                $r->setName($step['name']);
                $r->setSortOrder($step['sortOrder']);
            }
            else {
                $dbHelper->delete($em, 'AppDatabaseMainBundle:FormItem', array('idFormStep' => $r->getId()));
                $em->remove($r);
            }
        }
        $em->flush();

        // Add new
        foreach ($model->stepList as $step) {
            if (empty($step['id'])) {
                $newStep = new Entity\FormStep();
                $newStep->setIdFormVersion($this->entity->getId());
                $newStep->setName($step['name']);
                $newStep->setSortOrder($step['sortOrder']);
                $em->persist($newStep);
            }
        }
        $em->flush();

        // Update log
        $log = new Entity\FormVersionLogs();
        $log->setNotes($model->notes);
        $log->setFormVersionId($this->entity->getId());
        $log->setCreatedDate(new \DateTime());
        $log->setCreatedBy($currentUserId);

        $em->persist($log);
        $em->flush();

        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}