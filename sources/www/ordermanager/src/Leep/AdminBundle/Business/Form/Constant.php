<?php 
namespace Leep\AdminBundle\Business\Form;

class Constant {
    const FORM_ITEM_TYPE_INSTRUCTION            = 11;
    const FORM_ITEM_TYPE_TEXT_BOX               = 12;
    const FORM_ITEM_TYPE_DATE                   = 13;
    const FORM_ITEM_TYPE_PERSON                 = 14;
    const FORM_ITEM_TYPE_AUTHORIZED_PERSONNEL   = 15;
    //const FORM_ITEM_TYPE_BARCODE         = 15;

    public static function getItemHandler() {
        return array(
            self::FORM_ITEM_TYPE_INSTRUCTION                    => 'leep_admin.form.business.form_item_handler.instruction_item_handler',
            self::FORM_ITEM_TYPE_TEXT_BOX                       => 'leep_admin.form.business.form_item_handler.text_box_item_handler',
            self::FORM_ITEM_TYPE_DATE                           => 'leep_admin.form.business.form_item_handler.date_item_handler',
            self::FORM_ITEM_TYPE_PERSON                         => 'leep_admin.form.business.form_item_handler.person_item_handler',
            self::FORM_ITEM_TYPE_AUTHORIZED_PERSONNEL           => 'leep_admin.form.business.form_item_handler.authorized_personnel_item_handler',
            //self::FORM_ITEM_TYPE_BARCODE         => 'leep_admin.form.business.form_item_handler.barcode_item_handler',
        );
    }

    const STATUS_OPEN          = 1;
    const STATUS_FINISHED      = 10;
    public static function getFormStatus() {
        return array(
            self::STATUS_OPEN      => 'Open',
            self::STATUS_FINISHED  => 'Closed'
        );
    }
    public static function getFormStatusColor() {
        return array(
            self::STATUS_OPEN      => '#CCCCCC',
            self::STATUS_FINISHED  => '#009900'
        );
    }


    const SUMMARY_STEP = -2;

}