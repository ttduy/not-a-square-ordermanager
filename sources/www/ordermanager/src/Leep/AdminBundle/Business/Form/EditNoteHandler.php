<?php
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditNoteHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormSession', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = new EditNoteModel();
        $model->note = $this->entity->getNote();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');   

        $builder->add('note', 'textarea', array(
            'label'    => 'Note',
            'required' => false,
            'attr'     => array(
                'rows'   => 10, 'cols' => 70
            )
        )); 

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $form = $em->getRepository('AppDatabaseMainBundle:FormSession')->findOneById($this->entity->getId());

        $form->setNote($model->note);
        $em->flush();

        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}