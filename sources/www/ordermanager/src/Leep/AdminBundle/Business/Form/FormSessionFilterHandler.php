<?php
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FormSessionFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FormSessionFilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idWebUser', 'choice', array(
            'label'    => 'User',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Form_Status'),
            'empty_value' => false
        ));
        $builder->add('idForm', 'searchable_box', array(
            'label'    => 'Form',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Form_List'),
            'empty_value' => false
        ));
        $builder->add('search', 'text', array(
            'label'    => 'Search',
            'required' => false
        ));

        return $builder;
    }
}