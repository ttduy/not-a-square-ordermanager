<?php
namespace Leep\AdminBundle\Business\Form;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $newId = 0;
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->sortOrder = 99;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('formType', 'text', array(
            'label'    => 'Form Type',
            'required' => true
        ));
        $builder->add('formNumber', 'text', array(
            'label'    => 'Form Number',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 10, 'cols' => 70
            )
        ));
        $builder->add('sortOrder', 'text', array(
            'label'     => 'Sort Order',
            'attr'      => array(
                'placeholder' => 'Sort order'
            ),
            'required'  => false
        ));

        $builder->add('colorCode', 'text', array(
            'label'     => 'Color Code',
            'required'  => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $form = new Entity\Form();
        $form->setName($model->name);
        $form->setFormType($model->formType);
        $form->setFormNumber($model->formNumber);
        $form->setDescription($model->description);
        $form->setSortOrder($model->sortOrder);
        $form->setColorCode($model->colorCode);
        $em->persist($form);
        $em->flush();

        $formVersion = new Entity\FormVersion();
        $formVersion->setIdForm($form->getId());
        $formVersion->setCreationDatetime(new \DateTime());
        $formVersion->setVersion(1);
        $em->persist($formVersion);
        $em->flush();

        // Update log
        $log = new Entity\FormVersionLogs();
        $log->setNotes($model->description);
        $log->setFormVersionId($formVersion->getId());
        $log->setCreatedDate(new \DateTime());
        $log->setCreatedBy($currentUserId);

        $em->persist($log);
        $em->flush();

        $form->setIdActiveFormVersion($formVersion->getId());
        $em->flush();

        $this->newId = $formVersion->getId();

        parent::onSuccess();
    }
}