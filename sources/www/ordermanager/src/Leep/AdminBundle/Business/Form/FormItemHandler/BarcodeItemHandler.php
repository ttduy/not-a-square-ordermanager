<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class BarcodeItemHandler extends BaseItemHandler {
    public function render($index, $item, $itemData) {
        $stepInfo = @json_decode($item->getData(), true);

        $data = array();
        $data['index'] = $index;
        $data['id'] = $item->getId();
        $data['value'] = isset($itemData['value']) ? $itemData['value'] : '';
        $data['instruction'] = isset($stepInfo['text']) ? $stepInfo['text'] : '(no instruction)';
        return $this->templating->render('LeepAdminBundle:Form:barcode_item.html.twig', $data);
    }
}
