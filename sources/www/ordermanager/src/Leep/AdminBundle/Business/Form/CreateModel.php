<?php
namespace Leep\AdminBundle\Business\Form;

class CreateModel {
    public $name;
    public $formType;
    public $formNumber;
    public $description;
    public $sortOrder;
    public $colorCode;
}