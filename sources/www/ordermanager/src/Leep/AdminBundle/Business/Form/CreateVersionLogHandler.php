<?php
namespace Leep\AdminBundle\Business\Form;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateVersionLogHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $id = $this->container->get('request')->query->get('id', 0);

        $model = new CreateVersionLogModel();
        $model->formVersionId = $id;
        $model->resetUrl = null;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('formVersionId', 'hidden');
        $builder->add('resetUrl', 'hidden');

        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes (your reason to create new version)',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 90
            )
        ));
    }

    public function onSuccess() {
        $currentUserId = $this->container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $data = [];
        $formFeature = new \Leep\AdminBundle\Feature\Form($this->container);
        $formFeature->handleMakeVersion($this->container, $data);

        // Get new active form version
        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($model->formVersionId);
        if (!$formVersion) {
            $this->errors[] = "Error: Form Version #".$model->formVersionId." not found!";
            return false;
        }

        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());
        if (!$form) {
            $this->errors[] = "Error: Form #".$form->getId()." not found!";
            return false;
        }

        $newVersionId = $form->getIdActiveFormVersion();

        $log = new Entity\FormVersionLogs();
        $log->setNotes($model->notes);
        $log->setFormVersionId($newVersionId);
        $log->setCreatedDate(new \DateTime());
        $log->setCreatedBy($currentUserId);

        $em->persist($log);
        $em->flush();

        $this->messages = "Success! Reloading page, please wait.";

        $mgr = $this->container->get('easy_module.manager');
        $model->resetUrl = $mgr->getUrl('leep_admin', 'form', 'edit_step', 'edit', array('id' => $newVersionId));
        $this->builder->setData($model);
        $this->form = $this->builder->getForm();
    }
}
