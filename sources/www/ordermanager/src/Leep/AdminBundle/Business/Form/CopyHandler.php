<?php
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Form')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new CopyModel();
        $model->id = $entity->getId();
        $model->name = $entity->getName();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
    
        $builder->add('id', 'hidden');
        $builder->add('name', 'label', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('newName', 'text', array(
            'label'    => 'New name',
            'required' => true
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $mgr = $this->container->get('easy_module.manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $form = $this->entity;
        $today = new \DateTime();

        // make copy
        $newForm = new Entity\Form();
        $newForm->setName($model->newName);
        $newForm->setFormType($form->getFormType());
        $newForm->setFormNumber($form->getFormNumber());
        $newForm->setDescription($form->getDescription());
        $newForm->setSortOrder($form->getSortOrder());
        $newForm->setColorCode($form->getColorCode());
        $em->persist($newForm);
        $em->flush();

        // initialize 1st version
        $formVersion = new Entity\FormVersion();
        $formVersion->setIdForm($newForm->getId());
        $formVersion->setCreationDatetime(new \DateTime());
        $formVersion->setVersion(1);
        $em->persist($formVersion);
        $em->flush();

        // update active form version
        $newForm->setIdActiveFormVersion($formVersion->getId());
        $em->persist($newForm);
        $em->flush();

        // clone form steps
        $formSteps = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormStep')->findByIdFormVersion($form->getIdActiveFormVersion());

        if ($formSteps && sizeof($formSteps) > 0) {
            foreach ($formSteps as $formStep) {
                $step = new Entity\FormStep();
                $step->setIdFormVersion($formVersion->getId());
                $step->setName($formStep->getName());
                $step->setSortOrder($formStep->getSortOrder());
                $em->persist($step);
                $em->flush();

                // clone form step - items
                $formStepItems = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormItem')->findByIdFormStep($formStep->getId());

                if ($formStepItems && sizeof($formStepItems) > 0) {
                    foreach ($formStepItems as $formStepItem) {
                        $item = new Entity\FormItem();
                        $item->setIdFormStep($step->getId());
                        $item->setIdType($formStepItem->getIdType());
                        $item->setData($formStepItem->getData());
                        $item->setSortOrder($formStepItem->getSortOrder());
                        $em->persist($item);
                        $em->flush();
                    }
                }
            }
        }

        // Done
        $url = $mgr->getUrl('leep_admin', 'form', 'edit_step', 'edit', array('id' => $formVersion->getId()));
        $this->messages[] = 'The Form has been copied. Click <a href="'.$url.'">here</a> to view the copied Form';
    }
}
