<?php
namespace Leep\AdminBundle\Business\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFormItemStructureAuthorizedPersonnel extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_form_item_structure_authorized_personnel';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('text', 'text', array(
            'required' => false,
            'attr' => array(
                'style' => 'min-width: 600px; max-width: 600px'
            )
        ));

        // it's possible to exclude current logged-in User by default
        $builder->add('excludeMe', 'checkbox', array(
            'required' => false
        ));

        $builder->add('authorizedPersonnel', 'choice', array(
            'required' 	=> false,
            'choices'	=> $mapping->getMapping('LeepAdmin_Authorized_Personnel_List'),
            'multiple'	=> 'multiple',
            'attr' 		=> array(
                'style' => 'height: 300px'
            )
        ));
    }
}
