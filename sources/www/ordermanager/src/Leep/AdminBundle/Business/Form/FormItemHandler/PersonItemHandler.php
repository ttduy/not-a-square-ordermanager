<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class PersonItemHandler extends BaseItemHandler {
    public function render($index, $item, $itemData) {
        $stepInfo = @json_decode($item->getData(), true);

        // exclude current logged-in User by default
        $excludeMe = (array_key_exists('excludeMe', $stepInfo)) ? $stepInfo['excludeMe'] : true;

        $data = array();
        $data['index'] = $index;
        $data['id'] = $item->getId();
        $data['value'] = isset($itemData['value']) ? $itemData['value'] : '';
        $data['instruction'] = isset($stepInfo['text']) ? $stepInfo['text'] : '(no instruction)';
        $data['choices'] = $this->getFormPersons($excludeMe);

        return $this->templating->render('LeepAdminBundle:Form:person_item.html.twig', $data);
    }

    // current logged-in User won't be inlcuded in the list
    public function getFormPersons($excludeMe) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $map = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:WebUsers', 'p')
            ->innerJoin('AppDatabaseMainBundle:WebUserPermission', 'm', 'WITH', 'p.id = m.idWebUser')
            ->andWhere('m.permissionKey = :formViewPermission')
            ->setParameter('formViewPermission', 'formSessionView')
            ->orderBy('p.username', 'ASC');

        // if current logged-in User is excluded from this list
        if ($excludeMe) {
            $query->select('p')
                ->andWhere('p.id <> :currentLoggedInUserId')
                ->setParameter('currentLoggedInUserId', $userManager->getUser()->getId());
        }

        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $map[$r->getId()] = $r->getUsername();
        }
        return $map;
    }
}
