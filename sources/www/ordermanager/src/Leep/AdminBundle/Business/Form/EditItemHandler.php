<?php
namespace Leep\AdminBundle\Business\Form;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditItemHandler extends AppEditHandler {
    public $idFormStep;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->idFormStep = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FormStep', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $mapping = $this->container->get('easy_mapping');   
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = new EditStepModel(); 

        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($this->entity->getIdFormVersion());
        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());

        $model->name = $form->getName();
        $model->activeVersion = "Version ".$formVersion->getVersion().' at '.$formatter->format($formVersion->getCreationDatetime(), 'datetime');
        $model->idFormStep = $entity->getId();

        // Load Item List
        $model->itemList = array();
        $ctrlMapping = $mapping->getMapping('LeepAdmin_FormItem_CtrlMapping');
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FormItem', 'p')
            ->andWhere('p.idFormStep = :idFormStep')
            ->setParameter('idFormStep', $this->idFormStep)
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $item = array();
            $item['id'] = $row->getId();
            $item['idType'] = $row->getIdType();
            $item['sortOrder'] = $row->getSortOrder();
            $item[$ctrlMapping[$row->getIdType()]] = json_decode($row->getData(), true);
            $model->itemList[] = $item;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');   
        $builder->add('name', 'label', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('activeVersion', 'label', array(
            'label'    => 'Version',
            'required' => false
        ));
        $builder->add('idFormStep', 'choice', array(
            'label'    => 'Step',
            'required' => false,
            'empty_value' => false,
            'choices'  => Utils::getFormSteps($this->container, $this->entity->getIdFormVersion())
        ));

        $builder->add('sectionItems', 'section', array(
            'label'    => 'Step: '.$this->entity->getName(),
            'property_path' => false
        ));
        $builder->add('itemList', 'collection', array(
            'type' => new FormType\AppFormItem($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Section',
                'required' => false
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $mapping = $this->container->get('easy_mapping');   
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($this->entity->getIdFormVersion());
        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());

        $ctrlMapping = $mapping->getMapping('LeepAdmin_FormItem_CtrlMapping');

        // Unchange id
        $unchangedIdArr = array();        
        foreach ($model->itemList as $item) {
            $unchangedIdArr[$item['id']] = $item;
        }

        // Update
        $results = $doctrine->getRepository('AppDatabaseMainBundle:FormItem')->findByIdFormStep($this->idFormStep);
        foreach ($results as $r) {
            if (isset($unchangedIdArr[$r->getId()])) {
                $item = $unchangedIdArr[$r->getId()];

                $idType = $item['idType'];
                $configValue = $item[$ctrlMapping[$idType]];
                $r->setIdType($idType);
                $r->setData(json_encode($configValue));
                $r->setSortOrder($item['sortOrder']);
            }
            else {
                $em->remove($r);
            }
        }
        $em->flush();

        // Add new
        foreach ($model->itemList as $item) {
            if (empty($item['id'])) {
                $idType = $item['idType'];
                $configValue = $item[$ctrlMapping[$idType]];

                $newFormItem = new Entity\FormItem();
                $newFormItem->setIdFormStep($this->idFormStep);
                $newFormItem->setIdType($idType);
                $newFormItem->setData(json_encode($configValue));
                $newFormItem->setSortOrder($item['sortOrder']);

                $em->persist($newFormItem);
            }
        }
        $em->flush();


        parent::onSuccess();

        // Reload form
        $formModel = $this->convertToFormModel($this->entity);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}