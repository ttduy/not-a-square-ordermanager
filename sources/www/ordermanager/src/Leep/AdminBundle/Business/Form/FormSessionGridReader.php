<?php
namespace Leep\AdminBundle\Business\Form;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class FormSessionGridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('creationDatetime', 'idWebUser', 'additionalInformation', 'status', 'note', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',               'width' => '15%', 'sortable' => 'false'),
            array('title' => 'User',                    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Additional Information',  'width' => '45%', 'sortable' => 'false'),
            array('title' => 'Status',                  'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Notes',                   'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',                  'width' => '5%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FormSessionGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.creationDatetime', 'DESC');        
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('
                p.id,
                p.creationDatetime, 
                p.idWebUser,
                f.name, 
                v.version,
                f.formType,
                f.formNumber,
                v.creationDatetime as versionDatetime,
                p.status,
                p.idActiveFormStep,
                p.note,
                p.additionalInformation,
                v.id as formVersionId
                ')
            ->from('AppDatabaseMainBundle:FormSession', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormVersion', 'v', 'WITH', 'p.idFormVersion = v.id')
            ->innerJoin('AppDatabaseMainBundle:Form', 'f', 'WITH', 'v.idForm = f.id');

        if (intval($this->filters->idWebUser) != 0) {
            $queryBuilder->andWhere('p.idWebUser = :idWebUser')
                ->setParameter('idWebUser', intval($this->filters->idWebUser));
        }
        if (intval($this->filters->status) != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', intval($this->filters->status));
        }
        if (intval($this->filters->idForm) != 0) {
            $queryBuilder->andWhere('v.idForm = :idForm')
                ->setParameter('idForm', intval($this->filters->idForm));
        }
    
        // Search by text / date / user in form
        if (trim($this->filters->search) != '') {
            $queryBuilder->andWhere('p.note LIKE :search OR p.additionalInformation LIKE :search')
                ->setParameter('search', '%'.trim($this->filters->search).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('formSessionModify')) {
            $builder->addNewTabButton('Edit', $mgr->getUrl('leep_admin', 'form', 'feature', 'viewForm', array('id' => $row['id'])), 'button-detail');

            // available only if the form is in open
            if ($row['status'] == Constant::STATUS_OPEN) {
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'form', 'feature', 'deleteFormSession', array('id' => $row['id'])), 'button-delete');    
            }
        }
            
        return $builder->getHtml();
    }

    public function buildCellAdditionalInformation($row) {
        $html = sprintf("%s<br/><i>Form Number: %s / Form Type: %s / Version: %s</i><br/>%s",
            $row['name'],
            $row['formNumber'],
            $row['formType'],
            $row['version'],
            Utils::generateNoteWebInfo($row['additionalInformation'])
        );

        return $html;
    }

    public function buildCellNote($row) {
        $html = nl2br($row['note']);

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('formSessionModify')) {
            $builder->addPopupButton('Edit Note', $mgr->getUrl('leep_admin', 'form', 'edit_note', 'edit', array('id' => $row['id'])), 'button-activity');

            $html .= (($html) ? '&nbsp;' : '') . $builder->getHtml();
        }

        return $html;
    }
}