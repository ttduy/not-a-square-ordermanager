<?php
    namespace Leep\AdminBundle\Business\Form\FormType;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilder;
    use Symfony\Component\Form\FormInterface;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormView;

    use Leep\AdminBundle\Business;

    /**
     *
     */
    class AppPrintBarcode extends AbstractType
    {

        function __construct($container)
        {
            $this->container = $container;
        }

        public function finishView(FormView $view, FormInterface $form, array $pptions){
            parent::finishView($view, $form, $options);
            $mgr = $this->container->get('easy_module.manager');
            $view->vars['printBarcodePage'] = $mgr->getUrl('leep_admin', 'geno_form', 'feature', 'printBarcode');
        }
    }


 ?>
