<?php 
namespace Leep\AdminBundle\Business\Form;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FormHandler {
    public $container;
    public function __construct($container) {
        $this->container = $container;        
    }

    private function getFormSteps($formSession) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formSteps = array();

        // get all text field id
        // this data will be used to collect info for what called 'barcode'
        $queryOne = $em->createQueryBuilder();
        $queryOne->select('fi.id, fi.idFormStep')
            ->from('AppDatabaseMainBundle:FormStep', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormItem', 'fi', 'WITH', 'fi.idFormStep = p.id AND fi.idType = :idTextFieldType')
            ->andWhere('p.idFormVersion = :idFormVersion')
            ->setParameter('idFormVersion', $formSession->getIdFormVersion())
            ->setParameter('idTextFieldType', Constant::FORM_ITEM_TYPE_TEXT_BOX);
        $resultOne = $queryOne->getQuery()->getResult();
        $arrTextFieldId = array();
        foreach ($resultOne as $r) {
            if (!array_key_exists($r['idFormStep'], $arrTextFieldId)) {
                $arrTextFieldId[$r['idFormStep']] = array();
            }
            $arrTextFieldId[$r['idFormStep']][] = $r['id'];
        }

        // get form steps
        $i = 1;
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.name, fs.id as isDone, fs.data')
            ->from('AppDatabaseMainBundle:FormStep', 'p')
            ->leftJoin('AppDatabaseMainBundle:FormSessionStep', 'fs', 'WITH', '(fs.idFormSession = :idFormSession) AND (fs.idFormStep = p.id)')
            ->andWhere('p.idFormVersion = :idFormVersion')
            ->orderBy('p.sortOrder', 'ASC')
            ->setParameter('idFormSession', $formSession->getId())
            ->setParameter('idFormVersion', $formSession->getIdFormVersion());
        $result = $query->getQuery()->getResult();

        // barcode data (actually, it's now a collection of all textifield data)
        $barcodeData = "";
        foreach ($result as $r) {
            $formSteps[$r['id']] = array(
                'no'       => $i++,
                'name'     => $r['name'],
                'data'     => $r['data'],
                'isDone'   => ($r['isDone'] == 0) ? 0 : 1,
                'isActive' => 0
            );

            // prevent the case there's no 'data'
            if ($r['data']) {
                // check and get text field info if exists
                $tmpStepData = json_decode($r['data'], true);

                // if this form step has at least 1 text field
                if (array_key_exists($r['id'], $arrTextFieldId)) {
                    // check and get data of text field
                    foreach ($arrTextFieldId[$r['id']] as $textFieldId) {
                        if (array_key_exists($textFieldId, $tmpStepData) && $tmpStepData[$textFieldId]['value']) {
                            if ($barcodeData) {
                                $barcodeData .= ", ";
                            }
                            $barcodeData .= $tmpStepData[$textFieldId]['value'];
                        }
                    }
                }                
            }
        }

        return array($barcodeData, $formSteps);
    }

    private function getActiveFormStep($formSession, $formSteps) {        
        $idActiveFormStep = $formSession->getIdActiveFormStep();
        if ($idActiveFormStep == Constant::SUMMARY_STEP) {
            return Constant::SUMMARY_STEP;
        }

        if (!isset($formSteps[$idActiveFormStep])) {
            foreach ($formSteps as $k => $v) {
                $idActiveFormStep = $k;
                break;
            }
        }
        return $idActiveFormStep;
    }

    private function findNextStep($formSteps, $idActiveFormStep) {
        $found = false;
        foreach ($formSteps as $idStep => $v) {
            if ($found) {
                return $idStep;
            }
            if ($idStep == $idActiveFormStep) {
                $found = true;
            }
        }

        return -1;
    }

    private function findLastStep($formSteps, $idActiveFormStep) {
        $idLastStep = -1;
        foreach ($formSteps as $idStep => $v) {
            if ($idStep == $idActiveFormStep) {
                return $idLastStep;
            }
            $idLastStep = $idStep;
        }
        return - 1;
    }

    private function captureCurrentLots() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $currentLots = array();

        // get supplement type
        $currentLots['supplementType'] = array();
        $result = $em->getRepository('AppDatabaseMainBundle:SupplementType')->findAll();
        foreach ($result as $r) {
            $currentLots['supplementType'][] = array(
                'name'     => $r->getName(),
                'letter'   => $r->getLetter(),
                'lotInUse' => $r->getLotInUse()
            );
        }

        // get tested and in-use Pellet
        $currentLots['testedAndInUsePellet'] = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('
                pe.idPellet as id,
                pe.cups, pe.section, p.notes,
                p.letter, p.name as pelletName
            ')
            ->from('AppDatabaseMainBundle:PelletEntry', 'pe')
            ->leftJoin('AppDatabaseMainBundle:Pellet', 'p', 'WITH', 'pe.idPellet = p.id')
            ->andWhere('pe.section = :sectionInd')
            ->setParameter('sectionInd', Business\Pellet\Constant::SECTION_TESTED_AND_IN_USE);

        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $currentLots['testedAndInUsePellet'][] = array(
                'name'     => $r['pelletName'],
                'letter'   => $r['letter'],
                'cups' => $r['cups'],
                'ordered' => 0,
                'activeIngredient' => 0,
                'shipment' => 0,
                'expiryDate' => null,
                'notes' => $r['notes'],
                'status' => array_key_exists($r['section'], Business\Pellet\Constant::getSections()) ? Business\Pellet\Constant::getSections()[$r['section']] : null
            );
        }

        // get selected used-up-Material
        $currentLots['selectedUsedUpMaterial'] = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p.quantity, p.usedfor, p.datetaken, p.takenby, m.name as materialName')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->leftJoin('AppDatabaseMainBundle:Materials', 'm', 'WITH', 'p.materialid = m.id')
            ->andWhere('p.isSelected = :isSelected')
            ->setParameter('isSelected', 1)
            ->orderBy('p.datetaken', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $currentLots['selectedUsedUpMaterial'][] = array(
                'materialName'  => $r['materialName'],
                'quantity'      => $r['quantity'],
                'usedFor'       => $r['usedfor'],
                'dateTaken'     => ($r['datetaken'] ? $r['datetaken']->format('d/m/Y') : ''),
                'takenBy'       => $r['takenby']
            );
        }

        return $currentLots;
    }

    public function viewFormSession(&$data, $idFormSession) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $mgr = $this->container->get('easy_module.manager');

        // Load form session
        $formSession = $em->getRepository('AppDatabaseMainBundle:FormSession')->findOneById($idFormSession);
        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($formSession->getIdFormVersion());
        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());

        // Load form steps
        $tmpData = $this->getFormSteps($formSession);
        $barcodeData = $tmpData[0];
        $formSteps = $tmpData[1];

        // Find active form step
        $idActiveFormStep = $this->getActiveFormStep($formSession, $formSteps);
        if ($idActiveFormStep == Constant::SUMMARY_STEP) {
            $data['isSummaryStep'] = true;

            if ($formSession->getCurrentLots() == '') {
                $currentLots = $this->captureCurrentLots();
                $currentLotsTimestamp = new \DateTime();
            }
            else {
                $currentLots = unserialize($formSession->getCurrentLots());
                $currentLotsTimestamp = $formSession->getCurrentLotsTimestamp();
            }
            $data['currentLots'] = $currentLots;
            $data['currentLotsTimestamp'] = $formatter->format($currentLotsTimestamp, 'datetime');
        }   
        else {
            $data['isSummaryStep'] = false;
            $formSteps[$idActiveFormStep]['isActive'] = 1;

            //////////////////////////////////
            // x. Get active step items
            $stepData = $formSteps[$idActiveFormStep]['data'];
            $stepData = json_decode($stepData, true);

            $itemHtml = array();

            $stepItems = array();
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:FormItem', 'p')
                ->andWhere('p.idFormStep = :idFormStep')
                ->setParameter('idFormStep', $idActiveFormStep)
                ->orderBy('p.sortOrder', 'ASC');
            $results = $query->getQuery()->getResult();
            $index = 1;
            foreach ($results as $r) {
                $itemHtml[] = $this->renderItem(
                    $index++,
                    $r->getIdType(),
                    $r,
                    isset($stepData[$r->getId()]) ? $stepData[$r->getId()] : array()
                );
            }
            $data['formItemHtml'] = $itemHtml;
        }

        // Step position
        $idNextStep = $this->findNextStep($formSteps, $idActiveFormStep);
        $data['hasNextStep'] = ($idNextStep != -1);
        $idLastStep = $this->findLastStep($formSteps, $idActiveFormStep);
        $data['hasLastStep'] = ($idLastStep != -1);

        $data['isOpen'] = ($formSession->getStatus() == Constant::STATUS_OPEN);
        $data['status'] = $formatter->format($formSession->getStatus(), 'mapping', 'LeepAdmin_Form_Status');
        $data['statusColor'] = $formatter->format($formSession->getStatus(), 'mapping', 'LeepAdmin_Form_StatusColor');

        //////////////////////////////////
        // x. Set display data
        $data['note'] = $formSession->getNote();
        $data['additionalInformation'] = Utils::generateNoteWebInfo($formSession->getAdditionalInformation());
        $data['formNumber'] = $form->getFormNumber();
        $data['alertOldVersion'] = $form->getAlertOldVersion();
        $data['formDescription'] = $form->getDescription();
        $data['formName'] = $form->getName();
        $data['formVersion'] = $formVersion->getVersion();
        $data['formUser'] = $formatter->format($formSession->getIdWebUser(), 'mapping', 'LeepAdmin_WebUser_List');
        $data['formCreationDatetime'] = $formatter->format($formSession->getCreationDatetime(), 'datetime');
        $data['idActiveFormStep'] = $idActiveFormStep;
        $data['formSteps'] = $formSteps;
        $data['barcodeData'] = $barcodeData;
    }

    
    private function renderItem($index, $idType, $formItem, $itemData) {
        $handlers = Constant::getItemHandler();
        $handler = $this->container->get($handlers[$idType]);

        return $handler->render($index, $formItem, $itemData);
    }

    public function saveFormStep(&$data, $idFormSession, $formAction) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $request = $this->container->get('request');

        // Load form session
        $formSession = $em->getRepository('AppDatabaseMainBundle:FormSession')->findOneById($idFormSession);
        $formVersion = $em->getRepository('AppDatabaseMainBundle:FormVersion')->findOneById($formSession->getIdFormVersion());
        $form = $em->getRepository('AppDatabaseMainBundle:Form')->findOneById($formVersion->getIdForm());
        $action = $request->get('formAction', 'save');

        // Load form steps
        $tmpData = $this->getFormSteps($formSession);
        $barcodeData = $tmpData[0];
        $formSteps = $tmpData[1];

        // Find active form step
        $idActiveFormStep = $this->getActiveFormStep($formSession, $formSteps);
        $formSteps[$idActiveFormStep]['isActive'] = 1;

        // Save data
        $post = $request->request->all();
        $step_note = null;
        $formStepData = array();
        foreach ($post as $k => $v) {
            if (strpos($k, 'field_') === 0) {
                $idFormItem = intval(substr($k, 6));
                $formStepData[$idFormItem] = array(
                    'value' => $v
                );
            } else if ($k == 'step_note') {
                $step_note = $v;
            }
        }

        // Save form session step
        if ($formSession->getStatus() == Constant::STATUS_OPEN) {
            $formSessionStep = null;
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:FormSessionStep', 'p')
                ->andWhere('p.idFormSession = :idFormSession')
                ->andWhere('p.idFormStep = :idFormStep')
                ->setParameter('idFormSession', $formSession->getId())
                ->setParameter('idFormStep', $idActiveFormStep);
            $result = $query->getQuery()->getResult();
            if ($result) {
                foreach ($result as $r) {
                    $formSessionStep = $r;
                }
            }

            if (!$formSessionStep) {
                $formSessionStep = new Entity\FormSessionStep();
                $formSessionStep->setIdFormSession($formSession->getId());
                $formSessionStep->setIdFormStep($idActiveFormStep);
                $em->persist($formSessionStep);
                $em->flush();
            }

            $formSessionStep->setCreationDatetime(new \DateTime());
            $formSessionStep->setIdWebUser($userManager->getUser()->getId());
            $formSessionStep->setData(json_encode($formStepData));
            $em->flush();
        }

        // Perform action
        // set note
        $formSession->setNote($step_note);

        // re-compute note
        $encodedAddInfo = json_encode(Utils::recomputeAdditionalInfo($this->container, $formSession->getId(), $formSession->getIdFormVersion(), $idActiveFormStep));
        $formSession->setAdditionalInformation($encodedAddInfo);
        switch ($formAction) {
            case 'save':
                break;
            case 'next':
                $idNextStep = $this->findNextStep($formSteps, $idActiveFormStep);
                if ($idNextStep != -1) {
                    $formSession->setIdActiveFormStep($idNextStep);
                    $em->flush();
                }
                break;
            case 'previous':
                $idLastStep = $this->findLastStep($formSteps, $idActiveFormStep);
                if ($idLastStep != -1) {
                    $formSession->setIdActiveFormStep($idLastStep);
                    $em->flush();
                }
                break;
            case 'finish':
                $formSession->setIdActiveFormStep(Constant::SUMMARY_STEP);
                $em->flush();
                break;
            case 'complete':
                $currentLots = $this->captureCurrentLots();
                $formSession->setStatus(Constant::STATUS_FINISHED);
                $formSession->setCurrentLots(serialize($currentLots));
                $formSession->setCurrentLotsTimestamp(new \DateTime());
                $em->flush();

                break;
        }
    }
}