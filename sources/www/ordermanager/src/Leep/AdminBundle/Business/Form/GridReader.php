<?php
namespace Leep\AdminBundle\Business\Form;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'formType', 'formNumber', 'sortOrder', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',               'width' => '50%', 'sortable' => 'false'),
            array('title' => 'Type',               'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Number',             'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Sort Order',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',             'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_FormGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.sortOrder', 'ASC');        
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.name, p.formType, p.formNumber, p.description, fv.id as idFormVersion, fv.version, fv.creationDatetime, p.sortOrder')
            ->from('AppDatabaseMainBundle:Form', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormVersion', 'fv', 'WITH', 'p.idActiveFormVersion = fv.id');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $appURL = $this->container->getParameter('application_url');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('formModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'form', 'edit_step', 'edit', array('id' => $row['idFormVersion'])), 'button-edit');
            $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'form', 'copy', 'edit', array('id' => $row['id'])), 'button-copy');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'form', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
            $builder->addPromptButton('View Link', 'Link to start Form', $appURL . $mgr->getUrl('leep_admin', 'form', 'feature', 'startForm', array('id' => $row['idFormVersion'])), 'button-export');
            
        }
            
        return $builder->getHtml();
    }

    public function buildCellName($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $html = sprintf("%s<br/><i>Version %s at %s</i>",
            $row['name'],
            $row['version'],
            $formatter->format($row['creationDatetime'], 'datetime')
        );

        return $html;
    }
}