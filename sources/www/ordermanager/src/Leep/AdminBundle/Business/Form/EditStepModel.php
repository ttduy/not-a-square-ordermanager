<?php
namespace Leep\AdminBundle\Business\Form;

class EditStepModel {
    public $name;
    public $formType;
    public $formNumber;
    public $description;
    public $sortOrder;
    public $colorCode;
    public $idActiveFormVersion;
    public $idFormVersion;
    public $alertOldVersion;
    public $stepList;
    public $notes;
}