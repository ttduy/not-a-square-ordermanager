<?php
namespace Leep\AdminBundle\Business\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppFormStep extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_form_step';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        
        $manager = $this->container->get('easy_module.manager');
        $view->vars['stepId'] = $data['id'];
        $view->vars['editUrl'] = $manager->getUrl('leep_admin', 'form', 'edit_item', 'edit');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $utils = $this->container->get('leep_admin.report.business.util');
        $mgr = $this->container->get('easy_module.manager');
        $cacheBoxData = $this->container->get('leep_admin.helper.cache_box_data');

        $builder->add('id',                 'hidden');
        $builder->add('name',     'text', array(
            'label'    => 'Name',
            'required' => false,
            'attr'     => array('style' => 'min-width: 95%; max-width: 95%')
        ));                
        $builder->add('sortOrder',          'hidden');
    }
}