<?php 
namespace Leep\AdminBundle\Business\Form\FormItemHandler;

class DateItemHandler extends BaseItemHandler {
    public function render($index, $item, $itemData) {
        $stepInfo = @json_decode($item->getData(), true);
        $today = new \DateTime();

        $data = array();
        $data['index'] = $index;
        $data['id'] = $item->getId();
        $data['value'] = isset($itemData['value']) ? $itemData['value'] : $today->format('d/m/Y');
        return $this->templating->render('LeepAdminBundle:Form:date_item.html.twig', $data);
    }
}
