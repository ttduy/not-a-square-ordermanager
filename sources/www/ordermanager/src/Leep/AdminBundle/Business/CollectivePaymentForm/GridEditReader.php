<?php
namespace Leep\AdminBundle\Business\CollectivePaymentForm;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridEditReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }
    
    public function getColumnMapping() {
        return array('ordernumber', 'dateordered', 'lastStatus', 'partnerid', 'distributionchannelid', 'total', 'marginOverride', 'priceoverride');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Order Number',             'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Date Ordered',             'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Last status',              'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Partner',                  'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel',     'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Total',                    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Margin override',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Price Override',           'width' => '10%', 'sortable' => 'false'),            
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CollectivePaymentFormGridReader');
    }
    
    public function buildCellTotal($row) {        
        $applied = 'payment_no_margin';
        $idMarginGoesTo = intval($row['idMarginGoesTo']);
        if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
            $applied = Business\CollectivePayment\Utils::getPaymentApplied($idMarginGoesTo, 'partner');
        }
        else if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $applied = Business\CollectivePayment\Utils::getPaymentApplied($idMarginGoesTo, 'dc');
        }

        $price = Business\Customer\Util::computePrice($this->container, $row['id'], $applied);
        return $price['total'];
    }


    public function buildCellMarginOverride($row) {
        $total = floatval($this->buildCellTotal($row));
        if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
            if (is_numeric($row['marginOverridePartner'])) {
                $percentage = floatval($row['marginOverridePartner']);
                $total = $total * $percentage / 100;
            }
        }
        else if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            if (is_numeric($row['marginOverrideDc'])) {
                $percentage = floatval($row['marginOverrideDc']);
                $total = $total * $percentage / 100;
            }
        }
        return number_format($total, 2);
    }
    
    public function buildCellPriceOverride($row) {      
        $priceOverride = $row['priceoverride'];
        
        
        return $priceOverride;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, p.ordernumber, p.dateordered, p.status, p.statusDate, dc.partnerid, p.distributionchannelid, p.priceoverride, p.idMarginGoesTo, p.marginOverrideDc, p.marginOverridePartner')
            ->from('AppDatabaseMainBundle:Customer', 'p')            
            ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id');

        if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
            $queryBuilder
                ->andWhere('dc.partnerid = :partnerId')
                ->andWhere('(p.idCollectivePaymentPartner IS NULL) or (p.idCollectivePaymentPartner = 0) or (p.idCollectivePaymentPartner = :currentId)')
                ->andWhere('(p.invoicegoestoid = :idInvoiceGoesToCustomer) OR (p.invoicegoestoid = :invoiceGoesToChannel)')
                ->andWhere('(p.idMarginGoesTo = :toPartner) OR (p.idMarginGoesTo = :toDcAndPartner) OR (p.idMarginGoesTo = :bothPartner)')                                
                ->setParameter('partnerId', $this->filters->partnerId)
                ->setParameter('idInvoiceGoesToCustomer', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER)
                ->setParameter('invoiceGoesToChannel', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL)
                ->setParameter('currentId', $this->container->get('request')->query->get('id', 0))
                ->setParameter('toPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARNER)
                ->setParameter('toDcAndPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC)
                ->setParameter('bothPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_PARTNER);                
        }
        else if ($this->filters->destinationId == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $queryBuilder
                ->andWhere('p.distributionchannelid = :distributionChannelId')
                ->andWhere('(p.idCollectivePaymentDc IS NULL) or (p.idCollectivePaymentDc = 0) or (p.idCollectivePaymentDc = :currentId)')
                ->andWhere('p.invoicegoestoid = :idInvoiceGoesToCustomer')
                ->andWhere('(p.idMarginGoesTo = :toDc) OR (p.idMarginGoesTo = :toDcAndPartner) OR (p.idMarginGoesTo = :bothDc)')                                
                ->setParameter('distributionChannelId', $this->filters->distributionChannelId)
                ->setParameter('idInvoiceGoesToCustomer', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER)
                ->setParameter('currentId', $this->container->get('request')->query->get('id', 0))
                ->setParameter('toDc', Business\DistributionChannel\Constant::MARGIN_GOES_TO_DC)
                ->setParameter('toDcAndPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC)
                ->setParameter('bothDc', Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_DC);                                  
        }
        else {
            $queryBuilder->andWhere('p.id = -1');
        }        
    }

    
    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellLastStatus($row) {
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row['status']);        
        $age = $this->getAge($row['statusDate']);        

        return '('.$age.') '.$status;
    }

}
