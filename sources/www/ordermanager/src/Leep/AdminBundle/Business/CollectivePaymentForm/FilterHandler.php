<?php
namespace Leep\AdminBundle\Business\CollectivePaymentForm;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $model = new FilterModel();
        $model->paymentDate = new \DateTime();
        $model->taxValue = $config->getDefaultTax();

        return $model;
    }

    public function prepareFormModel($defaultFormModel) {
        $request = $this->container->get('request');
        if ($request->getMethod() == 'GET') {
            $session = $this->container->get('request')->getSession();
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);
        }
        return $defaultFormModel;
    }


    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('paymentNumber', 'text', array(
            'label'    => 'Payment Number',
            'required' => true
        ));
        $builder->add('paymentDate', 'datepicker', array(
            'label'    => 'Payment Date',
            'required' => false
        ));
        $builder->add('paymentFromCompanyId', 'choice', array(
            'label'    => 'Payment From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => Util::getOpenPartnerList($this->container)
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => Util::getOpenDistributionChannelList($this->container)
        ));
        $builder->add('paymentStatus', 'choice', array(
            'label'    => 'Payment Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectivePayment_PaymentStatus')
        ));
        $builder->add('overrideAmount', 'text', array(
            'label'    => 'Override amount',
            'required' => false
        ));
        $builder->add('selectedItems', 'hidden');
        $builder->add('actionMode', 'hidden');

        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppPaymentStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        $builder->addEventListener(FormEvents::PRE_BIND , array($this, 'onPreBind'));

        return $builder;
    }

    public $actionMode = '';
    public function onPreBind(DataEvent $event) {
        $filterData = $event->getData();

        if (isset($filterData['actionMode']) && $filterData['actionMode'] == 'save') {
            $this->actionMode = 'save';
            $filterData['actionMode'] = '';
            $event->setData($filterData);
        }

        if (isset($filterData['destinationId']) && ($filterData['destinationId'] != 0)) {
            $em = $this->container->get('doctrine')->getEntityManager();
            if ($filterData['destinationId'] == Business\CollectiveInvoice\Constant::DESTINATION_PARTNER) {
                $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById($filterData['partnerId']);
                if ($partner && $partner->getIsPaymentWithTax()) {
                    $filterData['isWithTax'] = true;
                    $event->setData($filterData);
                }
                else {
                    unset($filterData['isWithTax']);
                    $event->setData($filterData);
                }
            }
            else if ($filterData['destinationId'] == Business\CollectiveInvoice\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
                $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($filterData['distributionChannelId']);
                if ($dc && $dc->getIsPaymentWithTax()) {
                    $filterData['isWithTax'] = true;
                    $event->setData($filterData);
                }
                else {
                    unset($filterData['isWithTax']);
                    $event->setData($filterData);
                }
            }
        }
    }

    public function onSuccess() {
        parent::onSuccess();

        if ($this->actionMode == 'save') {
            $model = $this->getCurrentFilter();

            $collectivePayment = new Entity\CollectivePayment();
            $collectivePayment->setName($model->name);
            $collectivePayment->setPaymentNumber($model->paymentNumber);
            $collectivePayment->setPaymentDate($model->paymentDate);
            $collectivePayment->setPaymentFromCompanyId($model->paymentFromCompanyId);
            $collectivePayment->setIsWithTax($model->isWithTax);
            $collectivePayment->setTaxValue($model->taxValue);
            $collectivePayment->setDestinationId($model->destinationId);
            $collectivePayment->setPartnerId($model->partnerId);
            $collectivePayment->setDistributionChannelId($model->distributionChannelId);
            $collectivePayment->setPaymentStatus($model->paymentStatus);
            $collectivePayment->setOverrideAmount($model->overrideAmount);

            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($collectivePayment);
            $em->flush();

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);

                    if ($model->destinationId == Business\CollectivePayment\Constant::DESTINATION_PARTNER) {
                        $customer->setIdCollectivePaymentPartner($collectivePayment->getId());
                    }
                    else if ($model->destinationId == Business\CollectivePayment\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
                        $customer->setIdCollectivePaymentDc($collectivePayment->getId());
                    }
                    $em->persist($customer);
                }
            }
            $em->flush();

            // Save status
            $sortOrder = 1;
            $lastStatus = 0;
            foreach ($model->statusList as $statusRow) {
                if (empty($statusRow)) continue;
                $status = new Entity\CollectivePaymentStatus();
                $status->setCollectivePaymentId($collectivePayment->getId());
                $status->setStatusDate($statusRow->statusDate);
                $status->setStatus($statusRow->status);
                $status->setNotes($statusRow->notes);
                $status->setSortOrder($sortOrder++);
                $em->persist($status);

                $lastStatus = $statusRow->status;
            }
            $collectivePayment->setPaymentStatus($lastStatus);

            $em->flush();

            // Log activity
            $helperActivity = $this->container->get('leep_admin.helper.activity');
            $notes = 'Collective payment generated';
            $helperActivity->addCollectivePaymentActivity($collectivePayment->getId(), Business\Customer\Constant::ACTIVITY_TYPE_INVOICE_CREATED, $notes);

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $notes = 'Collective payment no."'.$collectivePayment->getPaymentNumber().'" is generated for this customer order';
                    $helperActivity->addCustomerActivity($idCustomer, Business\Customer\Constant::ACTIVITY_TYPE_INVOICE_CREATED, $notes);
                }
            }

            // Reload form
            $session = $this->container->get('request')->getSession();
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);
            $this->builder->setData($this->getDefaultFormModel());
            $this->form = $this->builder->getForm();

            $mgr = $this->container->get('easy_module.manager');
            $url = $mgr->getUrl('leep_admin', 'collective_payment', 'payment_form_edit', 'list', array('id' => $collectivePayment->getId()));
            $this->messages[] = 'The collective payment has been created. Click <a href="'.$url.'">here</a> to view the payment';
        }
    }
}