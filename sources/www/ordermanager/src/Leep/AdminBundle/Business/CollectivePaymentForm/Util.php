<?php
namespace Leep\AdminBundle\Business\CollectivePaymentForm;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class Util {
    public static function getOpenDistributionChannelList($container, $selectedId = 0) {
        $doctrine = $container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('dc.id, dc.distributionchannel, COUNT(c) as noOpen')
            ->from('AppDatabaseMainBundle:Customer', 'c')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->andWhere('c.invoicegoestoid = :invoiceGoesToCustomer')
            ->andWhere('(c.idCollectivePaymentDc IS NULL) or (c.idCollectivePaymentDc = 0) or (c.idCollectivePaymentDc = :selectedId)')
            ->andWhere('(c.idMarginGoesTo = :toDc) OR (c.idMarginGoesTo = :toDcAndPartner) OR (c.idMarginGoesTo = :bothDc)')
            ->setParameter('selectedId', $selectedId)
            ->setParameter('invoiceGoesToCustomer', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER)
            ->setParameter('toDc', Business\DistributionChannel\Constant::MARGIN_GOES_TO_DC)
            ->setParameter('toDcAndPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC)
            ->setParameter('bothDc', Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_DC)
            ->orderBy('dc.distributionchannel', 'ASC')
            ->groupBy('dc.id');
        $result = $query->getQuery()->getResult();
        $arr = array();
        foreach ($result as $row) {
            $arr[$row['id']] = $row['distributionchannel'].' ('.$row['noOpen'].' open )';
        }
        return $arr;
    }

    public static function getOpenPartnerList($container, $selectedId = 0) {
        $doctrine = $container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('p.id, p.partner, COUNT(c) as noOpen')
            ->from('AppDatabaseMainBundle:Customer', 'c')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'c.distributionchannelid = dc.id')
            ->innerJoin('AppDatabaseMainBundle:Partner', 'p', 'WITH', 'dc.partnerid = p.id')
            ->andWhere('(c.invoicegoestoid = :invoiceGoesToCustomer) OR (c.invoicegoestoid = :invoiceGoesToChannel)')
            ->andWhere('(c.idCollectivePaymentPartner IS NULL) or (c.idCollectivePaymentPartner = 0) or (c.idCollectivePaymentPartner = :selectedId)')
            ->andWhere('(c.idMarginGoesTo = :toPartner) OR (c.idMarginGoesTo = :toDcAndPartner) OR (c.idMarginGoesTo = :bothPartner)')
            ->setParameter('selectedId', $selectedId)
            ->setParameter('invoiceGoesToCustomer', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER)
            ->setParameter('invoiceGoesToChannel', Business\DistributionChannel\Constant::INVOICE_GOES_TO_CHANNEL)
            ->setParameter('toPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARNER)
            ->setParameter('toDcAndPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC)
            ->setParameter('bothPartner', Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_PARTNER)
            ->orderBy('p.partner', 'ASC')
            ->groupBy('p.id');
        $result = $query->getQuery()->getResult();
        $arr = array();
        foreach ($result as $row) {
            $arr[$row['id']] = $row['partner'].' ('.$row['noOpen'].' open )';
        }
        return $arr;
    }
}