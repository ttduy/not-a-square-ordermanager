<?php
namespace Leep\AdminBundle\Business\CollectivePaymentForm;

use Symfony\Component\Validator\ExecutionContext;

class FilterEditModel {
    public $name;
    public $paymentNumber;
    public $paymentDate;
    public $paymentFromCompanyId;
    public $isWithTax;
    public $taxValue;
    public $destinationId;
    public $partnerId;
    public $distributionChannelId;
    public $overrideAmount;
    
    public $paymentStatus;
    public $selectedItems;
    public $actionMode;
    public $target;
    
    public $statusList;
}
