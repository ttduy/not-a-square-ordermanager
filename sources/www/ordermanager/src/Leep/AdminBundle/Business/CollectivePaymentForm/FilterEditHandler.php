<?php
namespace Leep\AdminBundle\Business\CollectivePaymentForm;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Leep\AdminBundle\Business;

class FilterEditHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterEditModel();

        $id = $this->container->get('request')->query->get('id', 0);
        $collectivePayment = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectivePayment', 'app_main')->findOneById($id);
        
        $model->name = $collectivePayment->getName();
        $model->paymentNumber = $collectivePayment->getPaymentNumber();
        $model->paymentDate = $collectivePayment->getPaymentDate();
        $model->paymentFromCompanyId = $collectivePayment->getPaymentFromCompanyId();
        $model->isWithTax = $collectivePayment->getIsWithTax();
        $model->taxValue = $collectivePayment->getTaxValue();
        $model->destinationId = $collectivePayment->getDestinationId();
        $model->partnerId = $collectivePayment->getPartnerId();
        $model->distributionChannelId = $collectivePayment->getDistributionChannelId();
        $model->paymentStatus = $collectivePayment->getPaymentStatus();
        $model->overrideAmount = $collectivePayment->getOverrideAmount();
        
        $criteria = array('id' => -1);
        if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_PARTNER) {
            $criteria = array('idCollectivePaymentPartner' => $id);
        }
        else if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $criteria = array('idCollectivePaymentDc' => $id);
        }

        $model->selectedItems = array();
        $items = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findBy($criteria);
        foreach ($items as $customer) {
            $model->selectedItems[] = $customer->getId();
        }
        $model->selectedItems = implode(',', $model->selectedItems);
        
        // Load payment status
        $model->statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CollectivePaymentStatus', 'p')
            ->andWhere('p.collectivepaymentid = :collectivePaymentId')
            ->setParameter('collectivePaymentId', $collectivePayment->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Business\FormType\AppPaymentStatusRowModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }        
        return $model;
    }

    public function prepareFormModel($defaultFormModel) {
        $request = $this->container->get('request');    
        if ($request->getMethod() == 'GET') {
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
        }                
        return $defaultFormModel;
    }


    public function buildForm($builder) {
        $id = $this->container->get('request')->query->get('id', 0);
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('paymentNumber', 'text', array(
            'label'    => 'Payment Number',
            'required' => true
        ));
        $builder->add('paymentDate', 'datepicker', array(
            'label'    => 'Payment Date',
            'required' => false
        ));
        $builder->add('paymentFromCompanyId', 'choice', array(
            'label'    => 'Payment From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('taxValue', 'text', array(
            'label'    => 'Tax value',
            'required' => false,
            'attr'     => array('style' => 'min-width: 100px; max-width: 100px')
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => Util::getOpenPartnerList($this->container, $id)
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => Util::getOpenDistributionChannelList($this->container, $id)
        ));
        $builder->add('paymentStatus', 'choice', array(
            'label'    => 'Payment Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectivePayment_PaymentStatus')
        ));
        $builder->add('overrideAmount', 'text', array(
            'label'    => 'Override amount',
            'required' => false
        ));
        $builder->add('selectedItems', 'hidden');
        $builder->add('actionMode', 'hidden');
        $builder->add('target', 'hidden');

        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Business\FormType\AppPaymentStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        $builder->addEventListener(FormEvents::PRE_BIND , array($this, 'onPreBind'));

        return $builder;
    }

    public $actionMode = '';
    public function onPreBind(DataEvent $event) {
        $filterData = $event->getData();
        $filterData['target'] = '';

        if (isset($filterData['actionMode'])) {
            if ($filterData['actionMode'] == 'save') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $event->setData($filterData);
            }
            else if ($filterData['actionMode'] == 'pdf') {
                $this->actionMode = 'save';
                $filterData['actionMode'] = '';
                $filterData['target'] = 'pdf';
                $event->setData($filterData);
            }
        }
    }
        
    public function onSuccess() {
        parent::onSuccess();

        if ($this->actionMode == 'save') {
            $model = $this->getCurrentFilter();


            $id = $this->container->get('request')->query->get('id', 0);
            $collectivePayment = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectivePayment', 'app_main')->findOneById($id);
        
            $collectivePayment->setName($model->name);
            $collectivePayment->setPaymentNumber($model->paymentNumber);
            $collectivePayment->setPaymentDate($model->paymentDate);
            $collectivePayment->setPaymentFromCompanyId($model->paymentFromCompanyId);
            $collectivePayment->setIsWithTax($model->isWithTax);
            $collectivePayment->setTaxValue($model->taxValue);
            $collectivePayment->setDestinationId($model->destinationId);
            $collectivePayment->setPartnerId($model->partnerId);
            $collectivePayment->setDistributionChannelId($model->distributionChannelId);
            $collectivePayment->setPaymentStatus($model->paymentStatus);
            $collectivePayment->setOverrideAmount($model->overrideAmount);

            $em = $this->container->get('doctrine')->getEntityManager();
            $em->persist($collectivePayment);
            $em->flush();
            
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:Customer', 'c')
                ->set('c.idCollectivePaymentPartner', 'null')
                ->andWhere('c.idCollectivePaymentPartner = :id')
                ->setParameter('id', $id)
                ->getQuery()->execute();
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:Customer', 'c')
                ->set('c.idCollectivePaymentDc', 'null')
                ->andWhere('c.idCollectivePaymentDc = :id')
                ->setParameter('id', $id)
                ->getQuery()->execute();

            $items = explode(',', $model->selectedItems);
            foreach ($items as $idCustomer) {
                $idCustomer = intval($idCustomer);
                if ($idCustomer != 0) {
                    $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($idCustomer);
                    if ($model->destinationId == Business\CollectivePayment\Constant::DESTINATION_PARTNER) {
                        $customer->setIdCollectivePaymentPartner($collectivePayment->getId());
                    }
                    else if ($model->destinationId == Business\CollectivePayment\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
                        $customer->setIdCollectivePaymentDc($collectivePayment->getId());
                    }    
                    $em->persist($customer);
                }
            }
            $em->flush();

            // Update status
            $dbHelper = $this->container->get('leep_admin.helper.database');    
            $filters = array('collectivepaymentid' => $collectivePayment->getId());
            $dbHelper->delete($em, 'AppDatabaseMainBundle:CollectivePaymentStatus', $filters);

            $sortOrder = 1;
            $lastStatus = 0;
            foreach ($model->statusList as $statusRow) {
                if (empty($statusRow)) continue;
                $status = new Entity\CollectivePaymentStatus();
                $status->setCollectivePaymentId($collectivePayment->getId());
                $status->setStatusDate($statusRow->statusDate);
                $status->setStatus($statusRow->status);
                $status->setNotes($statusRow->notes);
                $status->setSortOrder($sortOrder++);
                $em->persist($status);
                
                $lastStatus = $statusRow->status;
            }
            $collectivePayment->setPaymentStatus($lastStatus);
            $em->persist($collectivePayment);
            $em->flush();

            /*
            $session = $this->container->get('request')->getSession();   
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);     
            $this->builder->setData($this->getDefaultFormModel());
            $this->form = $this->builder->getForm();
            */
            $this->messages[] = "The records has been saved successfully"; 
        }
    }
}