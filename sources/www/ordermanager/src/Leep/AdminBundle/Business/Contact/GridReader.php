<?php
namespace Leep\AdminBundle\Business\Contact;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;
class GridReader extends AppGridDataReader {
    public $filters;
    protected $result = [];
    protected $columnMapping = array();
    protected $columnSortMapping = array();
    protected $idArray = array();
    protected $totalRow = 0;

    public function __construct($container) {
        parent::__construct($container);
    }

    public function getColumnMapping() {
        $this->columnMapping = array();
        $this->columnMapping[] = 'id';
        $this->columnMapping[] = 'name';
        $this->columnMapping[] = 'city';
        $this->columnMapping[] = 'type';
        $this->columnMapping[] = 'action';
        return $this->columnMapping;
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'id';
        $this->columnSortMapping[] = 'name';
        $this->columnSortMapping[] = 'city';
        $this->columnSortMapping[] = 'type';
        $this->columnSortMapping[] = 'action';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Id',        'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Name',      'width' => '40%', 'sortable' => 'false'),
            array('title' => 'City',      'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Type',      'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',    'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function checkFilterFirst() {
        if (trim($this->filters->firstName) != '') {
            return true;
        }

        if (trim($this->filters->surName) != '') {
            return true;
        }

        if (trim($this->filters->city) != '') {
            return true;
        }

        if (trim($this->filters->company) != '') {
            return true;
        }

        return false;
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_ContactGridReader');
    }

    ///// raw data of DC
    public function rawDataDC($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')->from('AppDatabaseMainBundle:DistributionChannels', 'p');
        if (trim($this->filters->firstName) != '') {
            $query->andWhere('p.firstname LIKE :firstName')
            ->setParameter('firstName', '%'. trim($this->filters->firstName). '%');
        }
        if (trim($this->filters->surName) != '') {
            $query->andWhere('p.surname LIKE :surName')
            ->setParameter('surName', '%'. trim($this->filters->surName). '%');
        }
        if (trim($this->filters->city) != '') {
            $query->andWhere('(p.city LIKE :city) or (p.invoiceAddressCity LIKE :city)')
            ->setParameter('city', '%'. trim($this->filters->city). '%');
        }
        if (trim($this->filters->company) != '') {
            $query->andWhere('p.invoiceCompanyName LIKE :company')
            ->setParameter('company', '%'. trim($this->filters->company). '%');
        }
        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();

        $rows = $query->select('p')->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getDistributionChannel(),
                'city'      => ($value->getCity() != '') ? $value->getCity() : $value->getInvoiceAddressCity(),
                'type'      => 'Distribution Channels',
                'link'      => $mgr->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    //// raw data of Partner
    public function rawDataPartner($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')->from('AppDatabaseMainBundle:Partner', 'p');
        if (trim($this->filters->firstName) != '') {
            $query->andWhere('p.firstname LIKE :firstName')
            ->setParameter('firstName', '%'. trim($this->filters->firstName). '%');
        }
        if (trim($this->filters->surName) != '') {
            $query->andWhere('p.surname LIKE :surName')
            ->setParameter('surName', '%'. trim($this->filters->surName). '%');
        }
        if (trim($this->filters->city) != '') {
            $query->andWhere('(p.city LIKE :city) or (p.invoiceAddressCity LIKE :city)')
            ->setParameter('city', '%'. trim($this->filters->city). '%');
        }
        if (trim($this->filters->company) != '') {
            $query->andWhere('p.invoiceCompanyName LIKE :company')
            ->setParameter('company', '%'. trim($this->filters->company). '%');
        }
        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();
        $rows = $query->select('p')->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getPartner(),
                'city'      => ($value->getCity() != '') ? $value->getCity() : $value->getInvoiceAddressCity(),
                'type'      => 'Partner',
                'link'      => $mgr->getUrl('leep_admin', 'partner', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    //// raw data of Customer
    public function rawDataCustomer($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')->from('AppDatabaseMainBundle:Customer', 'p');
        if (trim($this->filters->firstName) != '') {
            $query->andWhere('p.firstname LIKE :firstName')
            ->setParameter('firstName', '%'. trim($this->filters->firstName). '%');
        }
        if (trim($this->filters->surName) != '') {
            $query->andWhere('p.surname LIKE :surName')
            ->setParameter('surName', '%'. trim($this->filters->surName). '%');
        }
        if (trim($this->filters->city) != '') {
            $query->andWhere('(p.city LIKE :city) or (p.invoiceAddressCity LIKE :city)')
            ->setParameter('city', '%'. trim($this->filters->city). '%');
        }
        if (trim($this->filters->company) != '') {
            $query->andWhere('p.invoiceAddressCompanyName LIKE :company')
            ->setParameter('company', '%'. trim($this->filters->company). '%');
        }
        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();
        $rows = $query->select('p')->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getFirstName(). ' '. $value->getSurName(),
                'city'      => ($value->getCity() != '') ? $value->getCity() : $value->getInvoiceAddressCity(),
                'type'      => 'Customer',
                'link'      => $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    //// raw data of Lead
    public function rawDataLead($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')->from('AppDatabaseMainBundle:Lead', 'p');
        if (trim($this->filters->firstName) != '') {
            $query->andWhere('p.firstName LIKE :firstName')
            ->setParameter('firstName', '%'. trim($this->filters->firstName). '%');
        }
        if (trim($this->filters->surName) != '') {
            $query->andWhere('p.surName LIKE :surName')
            ->setParameter('surName', '%'. trim($this->filters->surName). '%');
        }
        if (trim($this->filters->city) != '') {
            $query->andWhere('p.city LIKE :city')
            ->setParameter('city', '%'. trim($this->filters->city). '%');
        }
        if (trim($this->filters->company) != '') {
            $query->andWhere('p.company LIKE :company')
            ->setParameter('company', '%'. trim($this->filters->company). '%');
        }
        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();
        $rows = $query->select('p')->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getName(),
                'city'      => $value->getCity(),
                'type'      => 'Lead',
                'link'      => $mgr->getUrl('leep_admin', 'lead', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    //// raw data of Acquisiteur
    public function rawDataAcquisiteur($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')->from('AppDatabaseMainBundle:Acquisiteur', 'p');
        if (trim($this->filters->firstName) != '') {
            $query->andWhere('p.firstname LIKE :firstName')
            ->setParameter('firstName', '%'. trim($this->filters->firstName). '%');
        }
        if (trim($this->filters->surName) != '') {
            $query->andWhere('p.surname LIKE :surName')
            ->setParameter('surName', '%'. trim($this->filters->surName). '%');
        }
        if (trim($this->filters->city) != '') {
            $query->andWhere('p.city LIKE :city')
            ->setParameter('city', '%'. trim($this->filters->city). '%');
        }
        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();
        $rows = $query->select('p')->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getAcquisiteur(),
                'city'      => $value->getCity(),
                'type'      => 'Acquisiteur',
                'link'      => $mgr->getUrl('leep_admin', 'acquisiteur', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    public function buildQuery($queryBuilder) {}
    public function postQueryBuilder($queryBuilder) {}
    public function getCountSQL() {
        return count($this->result);
    }
    public function getTotalRecordsMatched() {
        return $this->totalRow;
    }
    public function loadData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $em = $this->container->get('doctrine')->getEntityManager();
        if ($this->checkFilterFirst() === true) {
            if (count($this->filters->type) > 0) {
                if (in_array(Constant::DISTRIBUTION_CHANNELS, $this->filters->type)) {
                    $this->rawDataDC($displayStart, $displayLength);
                }
                if (in_array(Constant::PARTNER, $this->filters->type)) {
                    $this->rawDataPartner($displayStart, $displayLength);
                }
                if (in_array(Constant::CUSTOMER, $this->filters->type)) {
                    $this->rawDataCustomer($displayStart, $displayLength);
                }
                if (in_array(Constant::LEAD, $this->filters->type)) {
                    $this->rawDataLead($displayStart, $displayLength);
                }
                if (in_array(Constant::ACQUISITEUR, $this->filters->type)) {
                    $this->rawDataAcquisiteur($displayStart, $displayLength);
                }
            }

        }
    }
    public function preGetResult() {
    }

    public function getResult() {
        $data = array();
        $formatter = $this->getFormatter();
        $this->preGetResult();
        foreach ($this->result as $row) {
            $this->rowData = array();
            foreach ($this->columnMapping as $columnId) {
                if ($formatter != null && $formatter->contains($columnId)) {
                    $this->rowData[] = $formatter->format($columnId, $row);
                }
                else {
                    $readMethod = 'buildCell'.ucfirst($columnId);
                    $getMethod = 'get'.ucfirst($columnId);

                    if (method_exists($this, $readMethod)) {
                        $this->rowData[] = $this->$readMethod($row);
                    }
                    else {
                        if (is_array($row) && isset($row[$columnId])) {
                            $this->rowData[] = $row[$columnId]    ;
                        }
                        else if (method_exists($row, $getMethod)) {
                            $this->rowData[] = $row->$getMethod();
                        }
                        else {
                            $this->rowData[] = '';
                        }
                    }
                }
            }
            $rowId = is_array($row) ? $row['id'] : $row->getId();
            $data[$rowId] = $this->rowData;
            //$data[] = $this->rowData;
        }
        return $data;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('View', $row['link'], 'button-detail');
        return $builder->getHtml();
    }

}
