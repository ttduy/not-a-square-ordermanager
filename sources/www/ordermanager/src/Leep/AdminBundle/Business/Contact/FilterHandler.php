<?php
namespace Leep\AdminBundle\Business\Contact;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idWebUser = 0;
        $model->type = array_keys(Constant::getFindIn());
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));

        $builder->add('company', 'text', array(
            'label'    => 'Company name',
            'required' => false
        ));

        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));

        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));

        $builder->add('type', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'attr'     => [
                'style' => 'height: 100px'
                ],
            'multiple' => true,
            'choices'  => Constant::getFindIn(),
            'empty_value' => false
        ));

        return $builder;
    }
}
