<?php
namespace Leep\AdminBundle\Business\Contact;

class Constant {
    const DISTRIBUTION_CHANNELS         = 1;
    const PARTNER                       = 2;
    const CUSTOMER                      = 3;
    const LEAD                          = 4;
    const ACQUISITEUR                   = 5;

    public static function getFindIn() {
        return array(
            self::PARTNER                       => 'Partner',
            self::DISTRIBUTION_CHANNELS         => 'Distribution Channel',
            self::CUSTOMER                      => 'Customer',
            self::LEAD                          => 'Lead',
            self::ACQUISITEUR                   => 'Acquisiteur'
        );
    }
}
