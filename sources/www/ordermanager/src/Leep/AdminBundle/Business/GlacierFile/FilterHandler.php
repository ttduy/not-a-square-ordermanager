<?php
namespace Leep\AdminBundle\Business\GlacierFile;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idGlacierVault = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('idGlacierVault', 'choice', array(
            'label'    => 'Vault',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_GlacierVault_List'),
            'empty_value' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
    }
}
