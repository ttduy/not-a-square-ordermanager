<?php
namespace Leep\AdminBundle\Business\GlacierFile;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('idGlacierVault', 'archiveDatetime', 'name', 'status', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Vault',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Timestamp',     'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Name',          'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Status',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_GlacierFileReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GlacierFile', 'p');

        if (!empty($this->filters->idGlacierVault)) {
            $queryBuilder->andWhere('p.idGlacierVault = :idGlacierVault')
                ->setParameter('idGlacierVault', $this->filters->idGlacierVault);
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.archiveDatetime', 'ASC');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('glacierModify')) {

        }

        return $builder->getHtml();
    }
}