<?php
namespace Leep\AdminBundle\Business\GlacierFile;

class Constant {
    const STATUS_PENDING_FOR_ARCHIVING       = 10;
    const STATUS_UPLOADING                   = 20;
    const STATUS_FREEZING                    = 30;
    const STATUS_READY                       = 40;
    const STATUS_REQUESTING                  = 50;
    const STATUS_READY_FOR_DOWNLOAD          = 60;

    public static function getStatus() {
        return array(
            self::STATUS_PENDING_FOR_ARCHIVING           => 'Pending for archiving',
            self::STATUS_UPLOADING                       => 'Uploading',
            self::STATUS_FREEZING                        => 'Freezing',
            self::STATUS_READY                           => 'Ready',
            self::STATUS_WAITING_FOR_DOWNLOAD            => 'Requesting',
            self::STATUS_READY_FOR_DOWNLOAD              => 'Ready for download'
        );
    }
}