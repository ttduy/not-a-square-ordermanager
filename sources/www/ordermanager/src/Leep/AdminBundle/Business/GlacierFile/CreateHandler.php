<?php
namespace Leep\AdminBundle\Business\GlacierFile;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        


        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

    }
    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        parent::onSuccess();
    }
}
