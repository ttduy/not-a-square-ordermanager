<?php
namespace Leep\AdminBundle\Business\WidgetStatistic;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WidgetStatistic', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->name = $entity->getName();
        $model->sortOrder = $entity->getSortOrder();

        $doctrine = $this->container->get('doctrine');
        
        $model->timePeriods = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticTimePeriod')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->timePeriods[] = $r->getIdTimePeriod();
        }

        $model->calculates = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCalculate')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->calculates[] = $r->getIdCalculate();
        }

        $model->mustBeStatus = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatus')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->mustBeStatus[] = $r->getStatus();
        }

        $model->mustNotBeStatus = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatusNot')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->mustNotBeStatus[] = $r->getStatus();
        }

        $model->mustBeProduct = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticProduct')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->mustBeProduct[] = $r->getIdProduct();
        }

        $model->mustBeCategory = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCategory')->findByIdWidgetStatistic($entity->getId());
        foreach ($results as $r) {
            $model->mustBeCategory[] = $r->getIdCategory();
        }
        return $model;
    }

    public function buildForm($builder) { 
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => true
        ));
        $builder->add('timePeriods', 'choice', array(
            'label'    => 'Time periods',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Widget_TimePeriod')
        ));
        $builder->add('calculates', 'choice', array(
            'label'    => 'Calculate',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Widget_Calculate')
        ));
        $builder->add('mustBeStatus', 'choice', array(
            'label'    => 'Must be status',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status') 
        ));
        $builder->add('mustNotBeStatus', 'choice', array(
            'label'    => 'Must not be status',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status') 
        ));
        $builder->add('mustBeProduct', 'choice', array(
            'label'    => 'Must be product',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List') 
        ));
        $builder->add('mustBeCategory', 'choice', array(
            'label'    => 'Must be category',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Category_List') 
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setName($model->name);
        $this->entity->setSortOrder($model->sortOrder);
        
        $filter = array('idWidgetStatistic' => $this->entity->getId());

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticTimePeriod', $filter);
        foreach ($model->timePeriods as $idTimePeriod) {
            $widgetTimePeriod = new Entity\WidgetStatisticTimePeriod();
            $widgetTimePeriod->setIdWidgetStatistic($this->entity->getId());
            $widgetTimePeriod->setIdTimePeriod($idTimePeriod);
            $em->persist($widgetTimePeriod);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticCalculate', $filter);
        foreach ($model->calculates as $idCalculate) {
            $widgetCalculate = new Entity\WidgetStatisticCalculate();
            $widgetCalculate->setIdWidgetStatistic($this->entity->getId());
            $widgetCalculate->setIdCalculate($idCalculate);
            $em->persist($widgetCalculate);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticStatus', $filter);
        foreach ($model->mustBeStatus as $status) {
            $widgetStatus = new Entity\WidgetStatisticStatus();
            $widgetStatus->setIdWidgetStatistic($this->entity->getId());
            $widgetStatus->setStatus($status);
            $em->persist($widgetStatus);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticStatusNot', $filter);
        foreach ($model->mustNotBeStatus as $status) {
            $widgetStatusNot = new Entity\WidgetStatisticStatusNot();
            $widgetStatusNot->setIdWidgetStatistic($this->entity->getId());
            $widgetStatusNot->setStatus($status);
            $em->persist($widgetStatusNot);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticProduct', $filter);
        foreach ($model->mustBeProduct as $idProduct) {
            $widgetProduct = new Entity\WidgetStatisticProduct();
            $widgetProduct->setIdWidgetStatistic($this->entity->getId());
            $widgetProduct->setIdProduct($idProduct);
            $em->persist($widgetProduct);
        }

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticCategory', $filter);
        foreach ($model->mustBeCategory as $idCategory) {
            $widgetCategory = new Entity\WidgetStatisticCategory();
            $widgetCategory->setIdWidgetStatistic($this->entity->getId());
            $widgetCategory->setIdCategory($idCategory);
            $em->persist($widgetCategory);
        }

        $em->flush();
        parent::onSuccess();
    }
}
