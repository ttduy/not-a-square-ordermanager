<?php
namespace Leep\AdminBundle\Business\WidgetStatistic;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $sortOrder;
    public $timePeriods;
    public $calculates;
    public $mustBeStatus;
    public $mustNotBeStatus;
    public $mustBeProduct;
    public $mustBeCategory;
}
