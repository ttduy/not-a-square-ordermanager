<?php
namespace Leep\AdminBundle\Business\WidgetStatistic;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $doctrine;
    public $formatter;
    public function getColumnMapping() {
        return array('name', 'timePeriod', 'calculate', 'mustBeStatus', 'mustNotBeStatus', 'mustBeProduct', 'mustBeCategory', 'sortOrder', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',            'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Time Period',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Calculates',      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Must be',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Must not be',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Must product',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Must category',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Sort order',      'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Action',          'width' => '5%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {
        $this->doctrine = $this->container->get('doctrine');
        $this->formatter = $this->container->get('leep_admin.helper.formatter');

        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:WidgetStatistic', 'p');
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellTimePeriod($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticTimePeriod')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getIdTimePeriod(), 'mapping', 'LeepAdmin_Widget_TimePeriod');
        }
        return implode(', ', $array);
    }

    public function buildCellCalculate($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCalculate')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getIdCalculate(), 'mapping', 'LeepAdmin_Widget_Calculate');
        }
        return implode(', ', $array);
    }

    public function buildCellMustBeStatus($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatus')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
        }
        return implode(', ', $array);
    }

    public function buildCellMustNotBeStatus($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticStatusNot')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
        }
        return implode(', ', $array);
    }

    public function buildCellMustBeProduct($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticProduct')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getIdProduct(), 'mapping', 'LeepAdmin_Product_List');
        }
        return implode(', ', $array);
    }

    public function buildCellMustBeCategory($row) {
        $array = array();
        $results = $this->doctrine->getRepository('AppDatabaseMainBundle:WidgetStatisticCategory')->findByIdWidgetStatistic($row->getId());        
        foreach ($results as $r) {
            $array[] = $this->formatter->format($r->getIdCategory(), 'mapping', 'LeepAdmin_Category_List');
        }
        return implode(', ', $array);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('dashboardWidgetModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'widget_statistic', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'widget_statistic', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
