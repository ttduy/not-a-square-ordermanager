<?php 
namespace Leep\AdminBundle\Business\WidgetStatistic;

class Constant {
    const TIME_PERIOD_ALL_TIME             = 1;
    const TIME_PERIOD_LAST_7_DAYS          = 2;
    const TIME_PERIOD_LAST_30_DAYS         = 3;
    const TIME_PERIOD_THIS_CALENDAR_MONTH  = 4;
    const TIME_PERIOD_LAST_CALENDAR_MONTH  = 5;
    const TIME_PERIOD_THIS_YEAR            = 6;
    const TIME_PERIOD_LAST_YEAR            = 7;    
    public static function getTimePeriods() {
        return array(
            self::TIME_PERIOD_ALL_TIME             => 'All time',
            self::TIME_PERIOD_LAST_7_DAYS          => 'Last 7 days',
            self::TIME_PERIOD_LAST_30_DAYS         => 'Last 30 days',
            self::TIME_PERIOD_THIS_CALENDAR_MONTH  => 'This calendar month',
            self::TIME_PERIOD_LAST_CALENDAR_MONTH  => 'Last calendar month',
            self::TIME_PERIOD_THIS_YEAR            => 'This year',
            self::TIME_PERIOD_LAST_YEAR            => 'Last year'
        );
    }


    const CALCULATE_NUMBER_OF_ORDERS       = 1;
    const CALCULATE_ESTIMATED_REVENUE      = 2;
    const CALCULATE_LARGEST_AGE_OF_ORDER   = 3;
    const CALCULATE_AVERAGE_AGE_OF_ORDER   = 4;
    const CALCULATE_SHORTEST_AGE_OF_ORDER  = 5;
    public static function getCalculates() {
        return array(
            self::CALCULATE_NUMBER_OF_ORDERS         => 'Number of orders',
            self::CALCULATE_ESTIMATED_REVENUE        => 'Estimated revenue',
            self::CALCULATE_LARGEST_AGE_OF_ORDER     => 'Largest age of order',
            self::CALCULATE_AVERAGE_AGE_OF_ORDER     => 'Average age of order',
            self::CALCULATE_SHORTEST_AGE_OF_ORDER    => 'Shortest age of order',
        );
    }
}