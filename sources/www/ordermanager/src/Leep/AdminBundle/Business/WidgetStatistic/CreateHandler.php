<?php
namespace Leep\AdminBundle\Business\WidgetStatistic;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('sortOrder', 'text', array(
            'label'    => 'Sort Order',
            'required' => true
        ));
        $builder->add('timePeriods', 'choice', array(
            'label'    => 'Time periods',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Widget_TimePeriod')
        ));
        $builder->add('calculates', 'choice', array(
            'label'    => 'Calculate',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Widget_Calculate')
        ));
        $builder->add('mustBeStatus', 'choice', array(
            'label'    => 'Must be status',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status') 
        ));
        $builder->add('mustNotBeStatus', 'choice', array(
            'label'    => 'Must not be status',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status') 
        ));
        $builder->add('mustBeProduct', 'choice', array(
            'label'    => 'Must be product',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List') 
        ));
        $builder->add('mustBeCategory', 'choice', array(
            'label'    => 'Must be category',
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px'),
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Category_List') 
        ));
        
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $widget = new Entity\WidgetStatistic();
        $widget->setName($model->name);
        $widget->setSortOrder($model->sortOrder);
        $em->persist($widget);
        $em->flush();

        foreach ($model->timePeriods as $idTimePeriod) {
            $widgetTimePeriod = new Entity\WidgetStatisticTimePeriod();
            $widgetTimePeriod->setIdWidgetStatistic($widget->getId());
            $widgetTimePeriod->setIdTimePeriod($idTimePeriod);
            $em->persist($widgetTimePeriod);
        }

        foreach ($model->calculates as $idCalculate) {
            $widgetCalculate = new Entity\WidgetStatisticCalculate();
            $widgetCalculate->setIdWidgetStatistic($widget->getId());
            $widgetCalculate->setIdCalculate($idCalculate);
            $em->persist($widgetCalculate);
        }

        foreach ($model->mustBeStatus as $status) {
            $widgetStatus = new Entity\WidgetStatisticStatus();
            $widgetStatus->setIdWidgetStatistic($widget->getId());
            $widgetStatus->setStatus($status);
            $em->persist($widgetStatus);
        }

        foreach ($model->mustNotBeStatus as $status) {
            $widgetStatusNot = new Entity\WidgetStatisticStatusNot();
            $widgetStatusNot->setIdWidgetStatistic($widget->getId());
            $widgetStatusNot->setStatus($status);
            $em->persist($widgetStatusNot);
        }

        foreach ($model->mustBeProduct as $idProduct) {
            $widgetProduct = new Entity\WidgetStatisticProduct();
            $widgetProduct->setIdWidgetStatistic($widget->getId());
            $widgetProduct->setIdProduct($idProduct);
            $em->persist($widgetProduct);
        }

        foreach ($model->mustBeCategory as $idCategory) {
            $widgetCategory = new Entity\WidgetStatisticCategory();
            $widgetCategory->setIdWidgetStatistic($widget->getId());
            $widgetCategory->setIdCategory($idCategory);
            $em->persist($widgetCategory);
        }

        $em->flush();

        parent::onSuccess();
    }
}
