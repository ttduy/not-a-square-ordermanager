<?php
namespace Leep\AdminBundle\Business\WidgetStatistic;
use App\Database\MainBundle\Entity;

class Utils {
    public static function getList($doctrine, $idWidget, $table, $field) {
        $array = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:'.$table)->findByIdWidgetStatistic($idWidget);
        foreach ($result as $r) {
            $method = 'get'.ucfirst($field);
            $array[] = $r->$method();
        }
        return $array;
    }
    public static function updateWidget($container, $idWidget) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $dbHelper = $container->get('leep_admin.helper.database');
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATEDIFF', 'Leep\AdminBundle\Business\WidgetStatistic\DoctrineExt\DateDiff');    
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');       

        $widget = $doctrine->getRepository('AppDatabaseMainBundle:WidgetStatistic')->findOneById($idWidget);
        $timePeriods = Utils::getList($doctrine, $idWidget, 'WidgetStatisticTimePeriod', 'idTimePeriod');
        $calculates = Utils::getList($doctrine, $idWidget, 'WidgetStatisticCalculate', 'idCalculate');
        $mustBeStatus = Utils::getList($doctrine, $idWidget, 'WidgetStatisticStatus', 'status');
        $mustNotBeStatus = Utils::getList($doctrine, $idWidget, 'WidgetStatisticStatusNot', 'status');
        $mustBeProduct = Utils::getList($doctrine, $idWidget, 'WidgetStatisticProduct', 'idProduct');
        $mustBeCategory = Utils::getList($doctrine, $idWidget, 'WidgetStatisticCategory', 'idCategory');

        $dbHelper->delete($em, 'AppDatabaseMainBundle:WidgetStatisticResult', array('idWidgetStatistic' => $idWidget));
        foreach ($timePeriods as $idTimePeriod) {
            $r = Utils::getTimePeriod($idTimePeriod);
            foreach ($calculates as $idCalculate) {
                $result = Utils::compute($container, $idCalculate, $r[0], $r[1], $mustBeStatus, $mustNotBeStatus, $mustBeProduct, $mustBeCategory);
                $widgetResult = new Entity\WidgetStatisticResult();
                $widgetResult->setIdWidgetStatistic($idWidget);
                $widgetResult->setIdTimePeriod($idTimePeriod);
                $widgetResult->setIdCalculate($idCalculate);
                $widgetResult->setResult($result);
                $em->persist($widgetResult);
            }
        }
        $widget->setLastUpdate(new \DateTime());
        $em->persist($widget);
        $em->flush();
    }

    public static function compute($container, $idCalculate, $from, $to, $mustBeStatus, $mustNotBeStatus, $mustBeProduct, $mustBeCategory) {
        $doctrine = $container->get('doctrine');
        
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->from('AppDatabaseMainBundle:Customer', 'p');
        $query->andWhere('p.isdeleted = 0');
        foreach ($mustBeStatus as $status) {
            $query->andWhere('FIND_IN_SET('.$status.', p.statusList) > 0');
        }
        foreach ($mustNotBeStatus as $status) {
            $query->andWhere('FIND_IN_SET('.$status.', p.statusList) = 0');
        }
        if (!empty($mustBeProduct)) {        
            $orCond = array();
            foreach ($mustBeProduct as $idProduct) {
                $orCond[] = '(FIND_IN_SET('.$idProduct.', p.productList) > 0)';                
            }
            $query->andWhere('('.implode(' OR ', $orCond).')');
        }
        if (!empty($mustBeCategory)) {        
            $orCond = array();
            foreach ($mustBeCategory as $idCategory) {
                $orCond[] = '(FIND_IN_SET('.$idCategory.', p.categoryList) > 0)';                
            }
            $query->andWhere('('.implode(' OR ', $orCond).')');
        }
        if (!empty($from)) {
            $query->andWhere('p.dateordered >= :dateFrom')
                ->setParameter('dateFrom', $from);
        }
        if (!empty($to)) {
            $query->andWhere('p.dateordered <= :dateTo')
                ->setParameter('dateTo', $to);
        }

        switch ($idCalculate) {
            case Constant::CALCULATE_NUMBER_OF_ORDERS:
                $query->select('COUNT(p) as data');
                $result = $query->getQuery()->getSingleScalarResult();
                return $result;
            case Constant::CALCULATE_ESTIMATED_REVENUE:
                $query->select('SUM(p.revenueEstimation) as data');
                $result = $query->getQuery()->getSingleScalarResult();
                return $result;
            case Constant::CALCULATE_LARGEST_AGE_OF_ORDER:
                $query->select('MAX(DATEDIFF(:currentDate, p.dateordered)) as data');
                $query->setParameter('currentDate', new \DateTime());
                $result = $query->getQuery()->getSingleScalarResult();
                return $result;
            case Constant::CALCULATE_AVERAGE_AGE_OF_ORDER:
                $query->select('AVG(DATEDIFF(:currentDate, p.dateordered)) as data');
                $query->setParameter('currentDate', new \DateTime());
                $result = $query->getQuery()->getSingleScalarResult();
                return number_format($result, 2);
            case Constant::CALCULATE_SHORTEST_AGE_OF_ORDER:
                $query->select('MIN(DATEDIFF(:currentDate, p.dateordered)) as data');
                $query->setParameter('currentDate', new \DateTime());
                $result = $query->getQuery()->getSingleScalarResult();
                return $result;
        }
        return '';
    }

    public static function getTimePeriod($idTimePeriod) {
        $from = new \DateTime();
        $to = new \DateTime();
        switch ($idTimePeriod) {
            case Constant::TIME_PERIOD_ALL_TIME:
                $from = $to = null;
                break;
            case Constant::TIME_PERIOD_LAST_7_DAYS:
                $from->setTimeStamp($to->getTimeStamp() - 24*60*60*7);
                break;
            case Constant::TIME_PERIOD_LAST_30_DAYS:
                $from->setTimeStamp($to->getTimeStamp() - 24*60*60*30);
                break;
            case Constant::TIME_PERIOD_THIS_CALENDAR_MONTH:
                $year = $to->format('Y');
                $month = $to->format('n');
                $day = $to->format('t');

                $from->setDate($year, $month, 1);
                $to->setDate($year, $month, $day);
                break;
            case Constant::TIME_PERIOD_LAST_CALENDAR_MONTH:            
                $year = $to->format('Y');
                $month = $to->format('n');
                $to->setDate($year, $month, 1);
                $to->setTimestamp($to->getTimeStamp() - 24*60*60-2);

                $year = $to->format('Y');
                $month = $to->format('n');
                $day = $to->format('t');
                $from->setDate($year, $month, 1);
                $to->setDate($year, $month, $day);
                break;
            case Constant::TIME_PERIOD_THIS_YEAR:
                $year = $to->format('Y');
                $from->setDate($year, 1, 1);
                $to->setDate($year, 12, 31);
                break;
            case Constant::TIME_PERIOD_LAST_YEAR:
                $year = $to->format('Y') - 1;
                $from->setDate($year, 1, 1);
                $to->setDate($year, 12, 31);
                break;
        }

        if ($from != null && $to != null) {
            $from->setTime(0, 0, 0);
            $to->setTime(23, 59, 59);
        }

        return array($from, $to);
    }
}