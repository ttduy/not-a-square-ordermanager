<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

use App\Database\MainBundle\Entity;

class Utils {
    public static function updateEntryHistory($container, $entry, $what, $from, $to) {
        if ($from == $to) {
            return;
        }

        $em = $container->get('doctrine')->getEntityManager();
        $currentUserId = $container->get('leep_admin.web_user.business.user_manager')->getUser()->getId();

        $history = new Entity\GenoPelletEntryHistory();
        $history->setIdGenoPelletEntry($entry->getId());
        $history->setIdGenoPellet($entry->getIdPellet());
        $history->setDate(new \DateTime());
        $history->setUser($currentUserId);
        $history->setWhatChange($what);
        $history->setFromValue($from);
        $history->setToValue($to);
        $em->persist($history);
        $em->flush();
    }
}
