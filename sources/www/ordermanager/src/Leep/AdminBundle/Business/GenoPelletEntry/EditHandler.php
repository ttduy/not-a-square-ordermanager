<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

use Leep\AdminBundle\Business\GenoPellet\Utils;

class EditHandler extends AppEditHandler {
    public $attachmentKey = false;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntry', 'app_main')->findOneById($id);
    }

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }

        if (empty($this->attachmentKey)) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir('genoPelletEntry');
        }
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel();

        $model->loading = $entity->getLoading();
        $model->section = $entity->getSection();
        $model->note = $entity->getNote();

        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();
        if (empty($model->attachmentKey)) {
            $this->generateAttachmentKey();
            $model->attachmentKey = $this->attachmentKey;
        }

        $model->weightList = [];
        $result = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryWeight')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\GenoPellet\FormType\AppGenoPelletEntryWeightModel();
                $weight->txt = $entry->getTxt();
                $weight->weight = Utils::customFormatFloat($entry->getWeight());
                $weight->targetWeight = Utils::customFormatFloat($entry->getTargetWeight());
                $model->weightList[] = $weight;
            }
        }

        $model->weightListNew = [];
        $result = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryWeightNew')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\GenoPellet\FormType\AppGenoPelletEntryWeightModel();
                $weight->activeIngredients = $entry->getActiveIngredientsSubstance();
                $weight->actives = $entry->getActiveSubstance();
                $weight->weightActivesIngredients = $entry->getActiveIngredientsSubstanceWeight();
                $weight->weightActives = $entry->getActiveSubstanceWeight();
                $model->weightListNew[] = $weight;
            }
        }

        $model->additivesList = [];
        $result = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryAdditives')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\GenoPellet\FormType\AppGenoPelletEntryWeightModel();
                $weight->txt = $entry->getName();
                $weight->weight = $entry->getWeight();
                $model->additivesList[] = $weight;
            }
        }

        $model->additivesListNew = [];
        $result = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryAdditivesNew')->findByIdPelletEntry($entity->getId());
        if ($result) {
            foreach ($result as $entry) {
                $weight = new Business\GenoPellet\FormType\AppGenoPelletEntryWeightModel();
                $weight->additives = $entry->getName();
                $weight->weightAdditives = $entry->getWeight();
                $model->additivesListNew[] = $weight;
            }
        }

        $model->shipment = $entity->getShipment();
        $model->exterialLot = $entity->getExterialLot();
        $model->ordered = $entity->getOrdered();
        $model->orderedDate = $entity->getOrderedDate();
        $model->orderedBy = $entity->getOrderedBy();
        $model->received = $entity->getReceived();
        $model->receivedDate = $entity->getReceivedDate();
        $model->receivedBy = $entity->getReceivedBy();

        $model->expiryDate = $entity->getExpiryDate();
        $model->expiryDateWarningDisabled = $entity->getExpiryDateWarningDisabled();

        $model->dateTaken = $entity->getDateTaken();
        $model->usedFor = $entity->getUsedFor();
        $model->takenBy = $entity->getTakenBy();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));

        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genoPelletEntry/'.$this->attachmentKey
        ));

        $builder->add('sectionPelletInfo', 'section', array(
            'label'         => 'Pellet Info',
            'property_path' => false
        ));

        $builder->add('loading', 'text', array(
            'label'    => 'Loading (g)',
            'required' => false
        ));

        $builder->add('section', 'choice', array(
            'label'    => 'Section',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_GenoPellet_Section')
        ));

        $builder->add('note', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 3, 'cols' => 70
            )
        ));

        $builder->add('sectionWeight', 'section', array(
            'label'         => 'Weight per Weight (%) according to recipe / after testing',
            'property_path' => false
        ));

        $builder->add('weightList', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, true, false),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionWeightNew', 'section', array(
            'label'         => 'Weight per Weight (%) according to recipe / after testing New',
            'property_path' => false
        ));

        $builder->add('weightListNew', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, true, true),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditives', 'section', array(
            'label'         => 'ADDITIVES',
            'property_path' => false
        ));

        $builder->add('additivesList', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, false, false),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditivesNew', 'section', array(
            'label'         => 'ADDITIVES NEW',
            'property_path' => false
        ));

        $builder->add('additivesListNew', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, false, true),
            'label' => 'W/W',
            'required' => false
        ));


        $builder->add('sectionOrdering', 'section', array(
            'label'         => 'Order information',
            'property_path' => false
        ));

        $builder->add('shipment', 'text', array(
            'label'    => 'Interial Lot',
            'required' => false
        ));

        $builder->add('exterialLot', 'text', array(
            'label'    => 'Exterial Lot',
            'required' => false
        ));

        $builder->add('ordered', 'text', array(
            'label'    => 'Ordered (k)',
            'required' => false
        ));

        $builder->add('orderedDate', 'datepicker', array(
            'label'    => 'Order date',
            'required' => false
        ));

        $builder->add('orderedBy', 'text', array(
            'label'    => 'Order by',
            'required' => false
        ));

        $builder->add('received', 'checkbox', array(
            'label'    => 'Received?',
            'required' => false
        ));

        $builder->add('receivedDate', 'datepicker', array(
            'label'    => 'Received date',
            'required' => false
        ));

        $builder->add('receivedBy', 'text', array(
            'label'    => 'Received by',
            'required' => false
        ));


        $builder->add('sectionExpiracy', 'section', array(
            'label'         => 'Expiration',
            'property_path' => false
        ));

        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));

        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Expiry warning disabled?',
            'required' => false
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));

        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));

        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));

        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $sections = \Leep\AdminBundle\Business\GenoPellet\Constant::getSections();
        $sectionFrom = $sections[$this->entity->getSection()];
        $sectionTo = $sections[$model->section];
        \Leep\AdminBundle\Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $this->entity, "Status (Section) [In edit entry]", $sectionFrom, $sectionTo);

        $loadingFrom = $this->entity->getLoading();
        $loadingTo = $model->loading;
        \Leep\AdminBundle\Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $this->entity, "Loading (g) [In edit entry]", $loadingFrom, $loadingTo);

        $this->entity->setLoading($model->loading);
        $this->entity->setSection($model->section);
        $this->entity->setNote($model->note);

        $this->entity->setShipment($model->shipment);
        $this->entity->setExterialLot($model->exterialLot);
        $this->entity->setOrdered($model->ordered);
        $this->entity->setOrderedDate($model->orderedDate);
        $this->entity->setOrderedBy($model->orderedBy);
        $this->entity->setReceived($model->received);
        $this->entity->setReceivedDate($model->receivedDate);
        $this->entity->setReceivedBy($model->receivedBy);

        $this->entity->setExpiryDate($model->expiryDate);
        $this->entity->setExpiryDateWarningDisabled($model->expiryDateWarningDisabled);

        $this->entity->setDateTaken($model->dateTaken);
        $this->entity->setUsedFor($model->usedFor);
        $this->entity->setTakenBy($model->takenBy);
        $this->entity->setAttachmentKey($model->attachmentKey);

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:GenoPelletEntryWeight', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->weightList) {
            foreach ($model->weightList as $eachWeight) {
                $newWeight = new Entity\GenoPelletEntryWeight();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setTxt($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $newWeight->setTargetWeight($eachWeight->targetWeight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:GenoPelletEntryWeightNew', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->weightListNew) {
            foreach ($model->weightListNew as $eachWeightNew) {
                $newWeight = new Entity\GenoPelletEntryWeightNew();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setActiveIngredientsSubstance($eachWeightNew->activeIngredients);
                $newWeight->setActiveIngredientsSubstanceWeight($eachWeightNew->weightActivesIngredients);
                $newWeight->setActiveSubstance($eachWeightNew->actives);
                $newWeight->setActiveSubstanceWeight($eachWeightNew->weightActives);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:GenoPelletEntryAdditives', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->additivesList) {
            foreach ($model->additivesList as $eachWeight) {
                $newWeight = new Entity\GenoPelletEntryAdditives();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setName($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        $delQuery = $em->createQueryBuilder();
        $delQuery->delete('AppDatabaseMainBundle:GenoPelletEntryAdditivesNew', 'p')
                ->andWhere('p.idPelletEntry = :idPelletEntry')
                ->setParameter('idPelletEntry', $this->entity->getId())
                ->getQuery()
                ->execute();

        if ($model->additivesListNew) {
            foreach ($model->additivesListNew as $eachWeightNew) {
                $newWeight = new Entity\GenoPelletEntryAdditivesNew();
                $newWeight->setIdPelletEntry($this->entity->getId());
                $newWeight->setName($eachWeightNew->additives);
                $newWeight->setWeight($eachWeightNew->weightAdditives);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        parent::onSuccess();
    }
}
