<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }

        if (empty($this->attachmentKey)) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir('genoPelletEntry');
        }
    }

    public function getDefaultFormModel() {
        $this->generateAttachmentKey();

        $model = new CreateModel();
        $model->created = new \DateTime();
        $model->section = $this->container->get('request')->get('idSection', 0);
        $model->attachmentKey = $this->attachmentKey;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));

        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'genoPelletEntry/'.$this->attachmentKey
        ));

        $builder->add('sectionPelletInfo', 'section', array(
            'label'         => 'Pellet Info ',
            'property_path' => false
        ));

        $builder->add('loading', 'text', array(
            'label'    => 'Loading (g)',
            'required' => false
        ));

        $builder->add('section', 'choice', array(
            'label'    => 'Section',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_GenoPellet_Section')
        ));

        $builder->add('note', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 3, 'cols' => 70
            )
        ));

        $builder->add('sectionWeight', 'section', array(
            'label'         => 'Weight per Weight (%) according to recipe / after testing',
            'property_path' => false
        ));

        $builder->add('weightList', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, true, false),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionWeightNew', 'section', array(
            'label'         => 'Weight per Weight (%) according to recipe / after testing New',
            'property_path' => false
        ));

        $builder->add('weightListNew', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, true, true),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditives', 'section', array(
            'label'         => 'ADDITIVES',
            'property_path' => false
        ));

        $builder->add('additivesList', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, false, false),
            'label' => 'W/W',
            'required' => false
        ));

        $builder->add('sectionAdditivesNew', 'section', array(
            'label'         => 'ADDITIVES NEW',
            'property_path' => false
        ));

        $builder->add('additivesListNew', 'container_collection', array(
            'type' => new \Leep\AdminBundle\Business\GenoPellet\FormType\AppGenoPelletEntryWeight($this->container, false, true),
            'label' => 'W/W',
            'required' => false
        ));


        $builder->add('sectionOrdering', 'section', array(
            'label'         => 'Order information',
            'property_path' => false
        ));

        $builder->add('shipment', 'text', array(
            'label'    => 'Interial Lot',
            'required' => false
        ));

        $builder->add('exterialLot', 'text', array(
            'label'    => 'Exterial Lot',
            'required' => false
        ));

        $builder->add('ordered', 'text', array(
            'label'    => 'Ordered (k)',
            'required' => false
        ));

        $builder->add('orderedDate', 'datepicker', array(
            'label'    => 'Order date',
            'required' => false
        ));

        $builder->add('orderedBy', 'text', array(
            'label'    => 'Order by',
            'required' => false
        ));

        $builder->add('received', 'checkbox', array(
            'label'    => 'Received?',
            'required' => false
        ));

        $builder->add('receivedDate', 'datepicker', array(
            'label'    => 'Received date',
            'required' => false
        ));

        $builder->add('receivedBy', 'text', array(
            'label'    => 'Received by',
            'required' => false
        ));


        $builder->add('sectionExpiracy', 'section', array(
            'label'         => 'Expiration',
            'property_path' => false
        ));

        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));

        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Expiry warning disabled?',
            'required' => false
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));


        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));

        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));

        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $idPellet = $this->container->get('request')->get('idPellet', 0);

        $entry = new Entity\GenoPelletEntry();
        $entry->setIdPellet($idPellet);
        $entry->setLoading(intval($model->loading));
        $entry->setSection($model->section);
        $entry->setNote($model->note);

        $entry->setShipment($model->shipment);
        $entry->setExterialLot($model->exterialLot);
        $entry->setOrdered($model->ordered);
        $entry->setOrderedDate($model->orderedDate);
        $entry->setOrderedBy($model->orderedBy);
        $entry->setReceived($model->received);
        $entry->setReceivedDate($model->receivedDate);
        $entry->setReceivedBy($model->receivedBy);

        $entry->setExpiryDate($model->expiryDate);
        $entry->setExpiryDateWarningDisabled($model->expiryDateWarningDisabled);

        $entry->setDateTaken($model->dateTaken);
        $entry->setUsedFor($model->usedFor);
        $entry->setTakenBy($model->takenBy);
        $entry->setAttachmentKey($model->attachmentKey);

        $em->persist($entry);
        $em->flush();

        $sectionTo = Business\GenoPellet\Constant::getSections()[$model->section];
        Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $entry, "Status (Section) [In create entry]", "", $sectionTo);
        Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $entry, "Loading (g) [In create entry]", "", $model->loading);

        if ($model->weightList) {
            foreach ($model->weightList as $eachWeight) {
                $newWeight = new Entity\GenoPelletEntryWeight();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setTxt($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $newWeight->setTargetWeight($eachWeight->targetWeight);
                $em->persist($newWeight);
            }

            $em->flush();
        }

        if ($model->weightListNew) {
            foreach ($model->weightListNew as $eachWeightNew) {
                $newWeight = new Entity\GenoPelletEntryWeightNew();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setActiveIngredientsSubstance($eachWeightNew->activeIngredients);
                $newWeight->setActiveIngredientsSubstanceWeight($eachWeightNew->weightActivesIngredients);
                $newWeight->setActiveSubstance($eachWeightNew->actives);
                $newWeight->setActiveSubstanceWeight($eachWeightNew->weightActives);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        if ($model->additivesList) {
            foreach ($model->additivesList as $eachWeight) {
                $newWeight = new Entity\GenoPelletEntryAdditives();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setName($eachWeight->txt);
                $newWeight->setWeight($eachWeight->weight);
                $em->persist($newWeight);
            }

            $em->flush();
        }

        if ($model->additivesListNew) {
            foreach ($model->additivesListNew as $eachWeightNew) {
                $newWeight = new Entity\GenoPelletEntryAdditivesNew();
                $newWeight->setIdPelletEntry($entry->getId());
                $newWeight->setName($eachWeightNew->additives);
                $newWeight->setWeight($eachWeightNew->weightAdditives);
                $em->persist($newWeight);
            }
            $em->flush();
        }

        parent::onSuccess();
    }
}
