<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormError;

class UsageHandler extends AppEditHandler {
    public $availableLoading = null;
    public function loadEntity($request) {
        $id = $request->query->get('id');

        $entity = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoPelletEntry', 'app_main')->findOneById($id);
        if ($entity) {
            $this->availableLoading = $entity->getLoading();
        }

        return $entity;
    }

    public function convertToFormModel($entity) {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        return [
            'amounts' =>    null,
            'dateTaken' =>  new \DateTime(),
            'usedFor' =>    null,
            'takenBy' =>    $userManager->getUser()->getUsername()
        ];
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionUseCup', 'section', array(
            'label'         => 'Use Cup(s)',
            'property_path' => false
        ));

        $builder->add('amounts', 'text', array(
            'label'    => 'Amounts(g)',
            'required' => false,
            'attr'      => array('placeholder'  => $this->availableLoading)
        ));

        $builder->add('sectionUsage', 'section', array(
            'label'         => 'Usage',
            'property_path' => false
        ));

        $builder->add('dateTaken', 'datepicker', array(
            'label'    => 'Date taken',
            'required' => false
        ));

        $builder->add('usedFor', 'text', array(
            'label'    => 'Used for',
            'required' => false
        ));

        $builder->add('takenBy', 'text', array(
            'label'    => 'Taken by',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        if ($model['amounts'] && $model['amounts'] <= $this->availableLoading) {
            $usedAmount = $model['amounts'];
            $dateTaken = ($model['dateTaken'] ? $model['dateTaken'] : null);
            $usedFor = ($model['usedFor'] ? $model['usedFor'] : null);
            $takenBy = ($model['takenBy'] ? $model['takenBy'] : null);
            $isSelected = 1;

            // create new pellet entity with section of "USED UP"
            $newPelletEntry = new Entity\GenoPelletEntry();
            $newPelletEntry->setIdPellet($this->entity->getIdPellet());
            $newPelletEntry->setLoading($usedAmount);
            $newPelletEntry->setSection(Business\Pellet\Constant::SECTION_USED_UP);
            $newPelletEntry->setNote($this->entity->getNote());

            $newPelletEntry->setShipment($this->entity->getShipment());
            $newPelletEntry->setOrdered($this->entity->getOrdered());
            $newPelletEntry->setOrderedDate($this->entity->getOrderedDate());
            $newPelletEntry->setOrderedBy($this->entity->getOrderedBy());
            $newPelletEntry->setReceived($this->entity->getReceived());
            $newPelletEntry->setReceivedDate($this->entity->getReceivedDate());
            $newPelletEntry->setReceivedBy($this->entity->getReceivedBy());

            $newPelletEntry->setExpiryDate($this->entity->getExpiryDate());
            $newPelletEntry->setExpiryDateWarningDisabled($this->entity->getExpiryDateWarningDisabled());

            $newPelletEntry->setIsSelected($isSelected);
            $newPelletEntry->setDateTaken($dateTaken);
            $newPelletEntry->setUsedFor($usedFor);
            $newPelletEntry->setTakenBy($takenBy);
            $newPelletEntry->setIdSuperEntry($this->entity->getId());

            $em->persist($newPelletEntry);
            $em->flush();

            $sectionTo = Business\GenoPellet\Constant::getSections()[Business\Pellet\Constant::SECTION_USED_UP];
            Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $newPelletEntry, "Status (Section) [In entry usage]", "", $sectionTo);
            Business\GenoPelletEntry\Utils::updateEntryHistory($this->container, $newPelletEntry, "Loading (g) [In entry usage]", "", $usedAmount);

            $loadingFrom = $this->entity->getLoading();
            $loadingTo = $this->entity->getLoading() - $usedAmount;
            if ($loadingTo < 0) {
                $loadingTo = 0;
            }

            Utils::updateEntryHistory($this->container, $this->entity, "Loading (g) [In entry usage]", $loadingFrom, $loadingTo);
            $this->entity->setLoading($loadingTo);

            // copy W/W
            $weights = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryWeight')->findByIdPelletEntry($this->entity->getId());
            if ($weights) {
                foreach ($weights as $eachWeight) {
                    $newWeight = new Entity\GenoPelletEntryWeight();
                    $newWeight->setIdPelletEntry($newPelletEntry->getId());
                    $newWeight->setTxt($eachWeight->getTxt());
                    $newWeight->setWeight($eachWeight->getWeight());
                    $newWeight->setSortOrder($eachWeight->getSortOrder());
                    $newWeight->setTargetWeight($eachWeight->getTargetWeight());
                    $em->persist($newWeight);
                }
                $em->flush();
            }

            // copy W/W
            $weights = $em->getRepository('AppDatabaseMainBundle:GenoPelletEntryAdditives')->findByIdPelletEntry($this->entity->getId());
            if ($weights) {
                foreach ($weights as $eachWeight) {
                    $newWeight = new Entity\GenoPelletEntryAdditives();
                    $newWeight->setIdPelletEntry($newPelletEntry->getId());
                    $newWeight->setName($eachWeight->getName());
                    $newWeight->setWeight($eachWeight->getWeight());
                    $em->persist($newWeight);
                }
                $em->flush();
            }

            parent::onSuccess();
        } else {
            $this->getForm()->get('amounts')->addError(new FormError("You used more than there are available in stock (".$this->availableLoading.")"));
        }
    }
}
