<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Leep\AdminBundle\Business;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Clear subdata
        Business\GenoPellet\Utils::clearEntrySubData($this->container, $id);

        // Remove entry history
        $history = $doctrine->getRepository('AppDatabaseMainBundle:GenoPelletEntryHistory')->findByIdGenoPelletEntry($id);
        foreach ($history as $item) {
            $em->remove($item);
        }

        $em->flush();

        // Remove entry
        $pelletEntry = $doctrine->getRepository('AppDatabaseMainBundle:GenoPelletEntry')->findOneById($id);
        $idPellet = $pelletEntry->getIdPellet();
        $em->remove($pelletEntry);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'geno_pellet', 'feature', 'manageStock', array('id' => $idPellet));

        return new RedirectResponse($url);
    }
}