<?php
namespace Leep\AdminBundle\Business\GenoPelletEntry;

class EditModel {
    public $loading;
    public $section;
    public $note;
    public $weightList;
    public $additivesList;
    public $weightListNew;
    public $additivesListNew;

    public $shipment;
    public $exterialLot;
    public $ordered;
    public $orderedDate;
    public $orderedBy;
    public $received;
    public $receivedDate;
    public $receivedBy;

    public $expiryDate;
    public $expiryDateWarningDisabled;

    public $dateTaken;
    public $usedFor;
    public $takenBy;

    public $attachment;
    public $attachmentKey;
}
