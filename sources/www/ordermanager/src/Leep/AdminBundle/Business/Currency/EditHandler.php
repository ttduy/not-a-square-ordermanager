<?php
namespace Leep\AdminBundle\Business\Currency;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Currency', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        $model->symbol = $entity->getSymbol();
        $model->exchangeRate = $entity->getExchangeRate();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => false
        ));        
        $builder->add('symbol', 'text', array(
            'label' => 'Symbol',
            'required' => false
        ));        
        $builder->add('exchangeRate', 'text', array(
            'label' => 'Exchange Rate',
            'required' => false
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model->name);
        $this->entity->setSymbol($model->symbol);
        $this->entity->setExchangeRate($model->exchangeRate);

        parent::onSuccess();
    }
}