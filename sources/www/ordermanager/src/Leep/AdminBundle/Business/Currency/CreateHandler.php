<?php
namespace Leep\AdminBundle\Business\Currency;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => false
        ));        
        $builder->add('symbol', 'text', array(
            'label' => 'Symbol',
            'required' => false
        ));        
        $builder->add('exchangeRate', 'text', array(
            'label' => 'Exchange Rate',
            'required' => false
        ));        

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $currency = new Entity\Currency();
        $currency->setName($model->name);
        $currency->setSymbol($model->symbol);
        $currency->setExchangeRate($model->exchangeRate);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($currency);
        $em->flush();
        
        parent::onSuccess();
    }
}