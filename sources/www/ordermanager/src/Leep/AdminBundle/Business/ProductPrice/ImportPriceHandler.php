<?php
namespace Leep\AdminBundle\Business\ProductPrice;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ImportPriceHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PriceCategories', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new ImportPriceModel();  
        $model->priceCategoryLabel = $entity->getPriceCategory();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('importPriceSection', 'section', array(
            'label'    => 'Import price',
            'required' => false,
            'property_path' => false
        ));
        $builder->add('priceCategoryLabel', 'label', array(
            'label'    => 'Price Category',
            'required' => false
        ));
        $builder->add('importData', 'textarea', array(
            'label'    => 'CSV Text',
            'required' => false,
            'attr'     => array(
                'rows' => 20, 'cols' => 90
            )
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();      
        $currentTime = new \DateTime(); 

        $lines = explode("\n", $model->importData);
        $changes = array();
        foreach ($lines as $line) {
            if (trim($line) == '') {
                continue;
            }

            // Validate
            $arr = explode(';', $line);
            if (count($arr) != 4) {
                $this->errors[] = $line.': should have 4 tokens';
                continue;
            }

            // Make job
            if (strpos($arr[0], '&') !== false) {
                $arr2 = explode('&', $arr[0]);
                foreach ($arr2 as $name) {
                    $changes[] = array(
                        trim($name),
                        floatval($arr[1]),
                        floatval($arr[2]),
                        floatval($arr[3])
                    );
                }
            }
            else {
                $changes[] = array(
                    trim($arr[0]),
                    floatval($arr[1]),
                    floatval($arr[2]),
                    floatval($arr[3])
                );
            }
        }

        foreach ($changes as $arr) {
            $product = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findOneByName(trim($arr[0]));
            $category = $em->getRepository('AppDatabaseMainBundle:ProductCategoriesDnaPlus')->findOneBycategoryname(trim($arr[0]));

            if (empty($product) && empty($category)) {
                $this->errors[] = 'Can\'t find the product or category name '.$arr[0];
                continue;
            }

            if (!empty($product) && !empty($category)) {
                $this->errors[] = "WARNING: naming conflict ".$arr[0];
                continue;
            }

            if (!empty($product)) {
                // Make record
                $price = $em->getRepository('AppDatabaseMainBundle:Prices')->findOneBy(array(
                    'productid'   => $product->getId(),
                    'pricecatid'  => $this->entity->getId()
                ));

                if ($price->getPrice() != floatval($arr[1]) || 
                    $price->getDCMargin() != floatval($arr[2]) ||
                    $price->getPartMargin() != floatval($arr[3])) {
                    $priceHistory = new Entity\PricesHistory();
                    $priceHistory->setProductId($price->getProductId());
                    $priceHistory->setPriceCatId($price->getPriceCatId());
                    $priceHistory->setPrice($price->getPrice());
                    $priceHistory->setDCMargin($price->getDCMargin());
                    $priceHistory->setPartMargin($price->getPartMargin());
                    $priceHistory->setPriceTimestamp($currentTime);
                    $em->persist($priceHistory);
                }
                
                $price->setPrice(floatval($arr[1]));
                $price->setDCMargin(floatval($arr[2]));
                $price->setPartMargin(floatval($arr[3]));
                $em->persist($price);
            }

            if (!empty($category)) {
                // Make record
                $price = $em->getRepository('AppDatabaseMainBundle:CategoryPrices')->findOneBy(array(
                    'categoryid'   => $category->getId(),
                    'pricecatid'   => $this->entity->getId()
                ));

                if ($price->getPrice() != floatval($arr[1]) || 
                    $price->getDCMargin() != floatval($arr[2]) ||
                    $price->getPartMargin() != floatval($arr[3])) {
                    $priceHistory = new Entity\CategoryPricesHistory();
                    $priceHistory->setCategoryId($price->getCategoryId());
                    $priceHistory->setPriceCatId($price->getPriceCatId());
                    $priceHistory->setPrice($price->getPrice());
                    $priceHistory->setDCMargin($price->getDCMargin());
                    $priceHistory->setPartMargin($price->getPartMargin());
                    $priceHistory->setPriceTimestamp($currentTime);
                    $em->persist($priceHistory);
                }
                
                $price->setPrice(floatval($arr[1]));
                $price->setDCMargin(floatval($arr[2]));
                $price->setPartMargin(floatval($arr[3]));
                $em->persist($price);
            }
        }
        $em->flush();


        parent::onSuccess();

        $this->messages = array('Done');
    }
}
