<?php 
namespace Leep\AdminBundle\Business\ProductPrice;

use App\Database\MainBundle\Entity;

class AjaxUpdatePrice {
    public function update($controller) {
        $em = $controller->getDoctrine()->getEntityManager();
        $request = $controller->getRequest();
        $productId = $request->get('productId', 0);
        $priceCatId = $request->get('priceCatId', 0);
        $field = $request->get('field', '');

        $price = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Prices', 'app_main')->findOneBy(
            array(
                'productid' => $productId,
                'pricecatid' => $priceCatId
            )
        );

        // Make initial history
        $priceHistory = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:PricesHistory')->findOneBy(
            array(
                'productid' => $productId,
                'pricecatid' => $priceCatId
            )
        );
        if (empty($priceHistory)) {
            $priceHistory = new Entity\PricesHistory();
            $priceHistory->setProductId($productId);
            $priceHistory->setPriceCatId($priceCatId);
            $priceHistory->setPrice($price->getPrice());
            $priceHistory->setDCMargin($price->getDCMargin());
            $priceHistory->setPartMargin($price->getPartMargin());
            $priceHistory->setPriceTimestamp(new \DateTime('2001-01-01'));
            $em->persist($priceHistory);
            $em->flush();
        }

        // Update price
        $value = $request->get('fieldVal', 0);
        if ($field == 'price') {
            $price->setPrice($value);
        }
        else if ($field == 'dcmargin') {
            $price->setDCMargin($value);
        }
        else if ($field == 'partmargin') {
            $price->setPartMargin($value);
        }
        $em->persist($price);
        $em->flush();

        // Store price history
        $priceHistory = new Entity\PricesHistory();
        $priceHistory->setProductId($productId);
        $priceHistory->setPriceCatId($priceCatId);
        $priceHistory->setPrice($price->getPrice());
        $priceHistory->setDCMargin($price->getDCMargin());
        $priceHistory->setPartMargin($price->getPartMargin());
        $priceHistory->setPriceTimestamp(new \DateTime());
        $em->persist($priceHistory);
        $em->flush();
    }
}