<?php
namespace Leep\AdminBundle\Business\Product;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $container = $this->container;

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
        $builder->add('materialCost', 'text', array(
            'label'    => 'Material Costs',
            'required' => false
        ));
        $builder->add('productionCost', 'text', array(
            'label'    => 'Production Costs (no reports)',
            'required' => false
        ));
        $builder->add('productTypeDE', 'choice', array(
            'label'    => 'Germany Product Type',
            'required' => false,
            'choices'  => Business\Product\Constant::getProductType($this->container)
        ));
        $builder->add('productTypeAT', 'choice', array(
            'label'    => 'Austria Product Type',
            'required' => false,
            'choices'  => Business\Product\Constant::getProductType($this->container)
        ));
        $builder->add('genes', 'choice', array(
            'label'    => 'Genes',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Gene_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));

        $builder->add('isNutriMe', 'checkbox', array(
            'label'    => 'Is NutriMe?',
            'required' => false
        ));

        $builder->add('sectionQuestion',          'section', array(
            'label' => 'Question',
            'property_path' => false
        ));
        $builder->add('questionList', 'collection', array(
            'type' => new Business\FormType\AppProductQuestionRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $product = new Entity\ProductsDnaPlus();
        $product->setName($model->name);
        $product->setCodename($model->codename);
        $product->setMaterialCosts($model->materialCost);
        $product->setProductionCostsNoReports($model->productionCost);
        $product->setIsNutriMe($model->isNutriMe);
        $product->setProductTypeGermany($model->productTypeDE);
        $product->setProductTypeAustria($model->productTypeAT);

        $em->persist($product);
        $em->flush();

        foreach ($model->genes as $idGene) {
            $prodGene = new Entity\ProdGeneLink();
            $prodGene->setProductId($product->getId());
            $prodGene->setGeneId($idGene);
            $em->persist($prodGene);
        }
        $em->flush();

        // Update product prices
        $priceCategories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:PriceCategories')->findAll();
        foreach ($priceCategories as $priceCategory) {
            $productPrice = new Entity\Prices();
            $productPrice->setPriceCatId($priceCategory->getId());
            $productPrice->setProductId($product->getId());
            $productPrice->setPrice(0.0000);
            $productPrice->setDCMargin(0.0000);
            $productPrice->setPartMargin(0.0000);
            $em->persist($productPrice);
        }
        $em->flush();

        // Update question
        $sortOrder = 1;
        $lastStatus = 0;
        foreach ($model->questionList as $questionRow) {
            if (empty($questionRow)) continue;
            $question = new Entity\ProductQuestion();
            $question->setProductId($product->getId());
            $question->setQuestion($questionRow->question);
            $question->setControlTypeId($questionRow->controlTypeId);
            $question->setControlConfig($questionRow->controlConfig);
            $question->setReportKey($questionRow->reportKey);
            $question->setSortOrder($sortOrder++);
            $em->persist($question);
        }

        $em->flush();

        parent::onSuccess();
    }
}
