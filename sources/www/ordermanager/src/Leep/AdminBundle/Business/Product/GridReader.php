<?php
namespace Leep\AdminBundle\Business\Product;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters = array();
    public function getColumnMapping() {
        return array('name', 'materialCosts', 'productTypeDE', 'productTypeAT', 'productionCostsNoReports', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',                            'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Material Costs',                  'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Product Type Germany',            'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Production Type Austria',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Production Costs (no reports)',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',                          'width' => '20%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ProductGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.name', 'ASC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ProductsDnaPlus', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        if ($this->filters->categoryId != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:CatProdLink', 'cp', 'WITH', 'p.id = cp.productid')
                ->andWhere('cp.categoryid = :categoryId')
                ->setParameter('categoryId', $this->filters->categoryId);
        }

        if ($this->filters->isNutriMe == 1) {
            $queryBuilder->andWhere('p.isNutriMe = 1');
        }

    }
    public function buildCellProductTypeDE($row) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $type = $em->getRepository('AppDatabaseMainBundle:ProductType', 'app_main')->findOneById($row->getProductTypeGermany());
        if ($type) {
            return $type->getName();
        }
        return '';

    }
    public function buildCellProductTypeAT($row) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $type = $em->getRepository('AppDatabaseMainBundle:ProductType', 'app_main')->findOneById($row->getProductTypeAustria());
        if ($type) {
            return $type->getName();
        }
        return '';

    }
    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'product', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'product', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'product', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        $builder->addNewTabButton('Correct genes', $mgr->getUrl('leep_admin', 'product', 'product', 'correctProductGenes', array('idProduct' => $row->getId())), 'button-double-check');
        $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'product', 'copy', 'edit', array('id' => $row->getId())), 'button-copy');

        return $builder->getHtml();
    }
}
