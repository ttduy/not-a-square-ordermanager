<?php
namespace Leep\AdminBundle\Business\Product;

class Constant {
    const PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX         = 1;
    const PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO    = 2;
    const PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX = 3;
    const PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE    = 4;

    public static function getProductQuestionControls() {
        return array(
            self::PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX          => 'Textbox',
            self::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO     => 'Choice - Radio button',
            self::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX  => 'Choice - Combobox',
            self::PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE     => 'Multiple choices'
        );
    }

    public static function getProductQuestionControlHelper() {
        return array(
            self::PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX          => 'Type textbox footer here, e.g (kg) / USD / ..',
            self::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO     => 'List all options here (separated by ;) E.g: YES;NO',
            self::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX  => 'List all options here (separated by ;) E.g: YES;NO',
            self::PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE     => 'List all options here (separated by ;) E.g: YES;NO'
        );
    }

    public static function getProductType($container){
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.name')
                ->from('AppDatabaseMainBundle:ProductType', 'p');
        $results = $query->getQuery()->getResult();
        $productType = [];
        foreach ($results as $result) {
            $productType[$result['id']] = $result['name'];
        }
        return $productType;
    }
}
