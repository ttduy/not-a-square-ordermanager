<?php
namespace Leep\AdminBundle\Business\Product;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->categoryId = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('categoryId', 'choice', array(
            'label'       => 'Category',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'empty_value' => false
        ));
        $builder->add('isNutriMe', 'choice', array(
            'label'       => 'Is Nutri Me',
            'required'    => false,
            'choices'     => array(0 => 'All', 1 => 'Show Nutri Me only'),
            'empty_value' => false
        ));


        return $builder;
    }
}
