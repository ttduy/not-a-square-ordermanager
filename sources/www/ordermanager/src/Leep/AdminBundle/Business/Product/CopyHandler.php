<?php
namespace Leep\AdminBundle\Business\Product;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {        
        $model = new CopyModel();
        $model->idProduct = $entity->getId();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
    
        $builder->add('sectionCopy', 'section', array(
            'label'    => 'Copy product',
            'required' => false
        ));

        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => true,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'New name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'New codename',
            'required' => true
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();

        // Copy product
        $product = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findOneById($model->idProduct);

        $newProduct = new Entity\ProductsDnaPlus();
        $newProduct->setName($model->name);
        $newProduct->setCodename($model->codename);
        $newProduct->setStandardPriceExclVat($product->getStandardPriceExclVat());
        $newProduct->setMaterialCosts($product->getMaterialCosts());
        $newProduct->setProductionCostsNoReports($product->getProductionCostsNoReports());
        $newProduct->setDoctorsMargin($product->getDoctorsMargin());
        $newProduct->setDistributionChannelMargin($product->getDistributionChannelMargin());
        $newProduct->setIsNutriMe($product->getIsNutriMe());
        $newProduct->setIdProductGroup($product->getIdProductGroup());
        $newProduct->setIsDeleted(0);
        $em->persist($newProduct);
        $em->flush();

        // Copy question
        $productQuestions = $em->getRepository('AppDatabaseMainBundle:ProductQuestion')->findByproductid($product->getId());
        if (!empty($productQuestions)) {
            foreach ($productQuestions as $question) {
                $newQuestion = new Entity\ProductQuestion();
                $newQuestion->setProductId($newProduct->getId());
                $newQuestion->setQuestion($question->getQuestion());
                $newQuestion->setControlTypeId($question->getControlTypeId());
                $newQuestion->setControlConfig($question->getControlConfig());
                $newQuestion->setSortOrder($question->getSortOrder());
                $newQuestion->setReportKey($question->getReportKey());
                $em->persist($newQuestion);
            }
            $em->flush();
        }

        // Copy product gene
        $productGeneLinks = $em->getRepository('AppDatabaseMainBundle:ProdGeneLink')->findByproductid($product->getId());
        if (!empty($productGeneLinks)) {
            foreach ($productGeneLinks as $link) {
                $newProdGeneLink = new Entity\ProdGeneLink();
                $newProdGeneLink->setProductId($newProduct->getId());
                $newProdGeneLink->setGeneId($link->getGeneId());
                $em->persist($newProdGeneLink);
            }
            $em->flush();
        }

        // Copy product prices
        $prices = $em->getRepository('AppDatabaseMainBundle:Prices')->findByproductid($product->getId());
        if (!empty($prices)) {
            foreach ($prices as $price) {
                $newPrice = new Entity\Prices();
                $newPrice->setProductId($newProduct->getId());
                $newPrice->setPriceCatId($price->getPriceCatId());
                $newPrice->setPrice($price->getPrice());
                $newPrice->setDCMargin($price->getDCMargin());
                $newPrice->setPartMargin($price->getPartMargin());
                $em->persist($newPrice);
            }
            $em->flush();
        }

        $this->messages[] = 'The product has been copied';
    }
}
