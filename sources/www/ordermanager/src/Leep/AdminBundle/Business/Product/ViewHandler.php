<?php
namespace Leep\AdminBundle\Business\Product;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $id = $this->container->get('request')->query->get('id', 0);
        $product = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findOneById($id);
        $data[] = $this->add('Product Name', $product->getName());        
        $data[] = $this->add('Material Costs', $product->getMaterialCosts());
        $data[] = $this->add('Product Costs (No reports)', $product->getProductionCostsNoReports());
        $data[] = $this->add('Product Group', $mapping->getMappingTitle('LeepAdmin_Product_Group', $product->getIdProductGroup()));
        
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $prodGenes = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProdGeneLink')->findBy(array('productid' => $id));
        foreach ($prodGenes as $prodGene) {
            $name = $mapping->getMappingTitle('LeepAdmin_Gene_List', $prodGene->getGeneId());
            $builder->addLink($name, $mgr->getUrl('leep_admin', 'gene', 'view', 'view', array('id' => $prodGene->getGeneId())));            
        }
        $data[] = $this->add('Genes', $builder->getHtml());
        return $data;
    }
}
