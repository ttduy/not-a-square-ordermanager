<?php
namespace Leep\AdminBundle\Business\Product;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $name;
    public $codename;
    public $materialCost;
    public $productionCost;
    public $productTypeDE;
    public $productTypeAT;
    public $genes;
    public $questionList;

    public $isNutriMe;
}
