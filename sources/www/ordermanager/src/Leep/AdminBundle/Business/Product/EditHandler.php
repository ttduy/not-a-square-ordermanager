<?php
namespace Leep\AdminBundle\Business\Product;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductsDnaPlus', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->codename = $entity->getCodename();
        $model->materialCost = $entity->getMaterialCosts();
        $model->productionCost = $entity->getProductionCostsNoReports();
        $model->isNutriMe = $entity->getIsNutriMe();
        $model->productTypeDE = $entity->getProductTypeGermany();
        $model->productTypeAT = $entity->getProductTypeAustria();

        $model->genes = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from("AppDatabaseMainBundle:ProdGeneLink", 'p')
            ->andWhere('p.productid = '.$entity->getId());
        $genes = $query->getQuery()->getResult();
        foreach ($genes as $pLink) {
            $model->genes[] = $pLink->getGeneId();
        }

        // Load question
        $model->questionList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ProductQuestion', 'p')
            ->andWhere('p.productid = :productId')
            ->setParameter('productId', $entity->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $question) {
            $m = new Business\FormType\AppProductQuestionRowModel();
            $m->question = $question->getQuestion();
            $m->controlTypeId = $question->getControlTypeId();
            $m->controlConfig = $question->getControlConfig();
            $m->id = $question->getId();
            $m->reportKey = $question->getReportKey();
            $model->questionList[] = $m;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $container = $this->container;

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('codename', 'text', array(
            'label'    => 'Codename',
            'required' => true
        ));
        $builder->add('materialCost', 'text', array(
            'label'    => 'Material Costs',
            'required' => false
        ));
        $builder->add('productionCost', 'text', array(
            'label'    => 'Production Costs (no reports)',
            'required' => false
        ));
        $builder->add('isNutriMe', 'checkbox', array(
            'label'    => 'Is NutriMe?',
            'required' => false
        ));

        $builder->add('productTypeDE', 'choice', array(
            'label'    => 'Germany Product Type',
            'required' => false,
            'choices'  => Constant::getProductType($container)
        ));
        $builder->add('productTypeAT', 'choice', array(
            'label'    => 'Austria Product Type',
            'required' => false,
            'choices'  => Constant::getProductType($container)
        ));
        $builder->add('genes', 'choice', array(
            'label'    => 'Genes',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Gene_List'),
            'attr'    => array(
                'style'  => 'height: 300px'
            )
        ));

        $builder->add('sectionQuestion',          'section', array(
            'label' => 'Question',
            'property_path' => false
        ));
        $builder->add('questionList', 'collection', array(
            'type' => new Business\FormType\AppProductQuestionRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setName($model->name);
        $this->entity->setCodename($model->codename);
        $this->entity->setMaterialCosts($model->materialCost);
        $this->entity->setProductionCostsNoReports($model->productionCost);
        $this->entity->setIsNutriMe($model->isNutriMe);
        $this->entity->setProductTypeGermany($model->productTypeDE);
        $this->entity->setProductTypeAustria($model->productTypeAT);

        // update genes
        $filters = array('productid' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ProdGeneLink', $filters);
        foreach ($model->genes as $idGene) {
            $prodGene = new Entity\ProdGeneLink();
            $prodGene->setProductId($this->entity->getId());
            $prodGene->setGeneId($idGene);
            $em->persist($prodGene);
        }
        $em->flush();

        // Save question
        $existingQuestions = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductQuestion')->findByproductid($this->entity->getId());
        foreach ($result as $r) {
            $existingQuestions[$r->getId()] = $r;
        }

        $sortOrder = 1;
        $lastStatus = 0;
        foreach ($model->questionList as $questionRow) {
            if (empty($questionRow)) continue;
            if ($questionRow->id == 0) {
                $question = new Entity\ProductQuestion();
                $question->setProductId($this->entity->getId());
                $question->setQuestion($questionRow->question);
                $question->setControlTypeId($questionRow->controlTypeId);
                $question->setControlConfig($questionRow->controlConfig);
                $question->setReportKey($questionRow->reportKey);
                $question->setSortOrder($sortOrder++);
                $em->persist($question);
            }
            else {
                if (isset($existingQuestions[$questionRow->id])) {
                    $question = $existingQuestions[$questionRow->id];
                    $question->setProductId($this->entity->getId());
                    $question->setQuestion($questionRow->question);
                    $question->setControlTypeId($questionRow->controlTypeId);
                    $question->setControlConfig($questionRow->controlConfig);
                    $question->setReportKey($questionRow->reportKey);
                    $question->setSortOrder($sortOrder++);
                    $em->persist($question);

                    unset($existingQuestions[$questionRow->id]);
                }
            }
        }

        foreach ($existingQuestions as $deletedQuestion) {
            $em->remove($deletedQuestion);
        }

        $em->flush();

        parent::onSuccess();

        // Reload form
        $request = $this->container->get('request');
        $this->entity = $this->loadEntity($request);
        $formModel = $this->convertToFormModel($this->entity);

        $this->builder = $this->container->get('form.factory')->createBuilder('form', $formModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
