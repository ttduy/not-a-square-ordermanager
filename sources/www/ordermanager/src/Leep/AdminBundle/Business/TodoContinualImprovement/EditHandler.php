<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TodoContinualImprovement', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->idUser = $entity->getIdUser();
        $model->creationDate = $entity->getCreationDate();
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();
        $model->milestones = $entity->getMilestones();
        $model->status = $entity->getStatus();
        $model->idFinishIn = $entity->getIdFinishIn();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idUser', 'choice', array(
            'label'    => 'Creator',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('milestones', 'textarea', array(
            'label'    => 'Milestones',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_Status')
        ));
        $builder->add('idFinishIn', 'choice', array(
            'label'    => 'Finish in',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_FinishIn')
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        

        $this->entity->setIdUser($model->idUser);
        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setName($model->name);
        $this->entity->setDescription($model->description);
        $this->entity->setMilestones($model->milestones);
        $this->entity->setStatus($model->status);
        $this->entity->setIdFinishIn($model->idFinishIn);
        
        parent::onSuccess();
    }
}
