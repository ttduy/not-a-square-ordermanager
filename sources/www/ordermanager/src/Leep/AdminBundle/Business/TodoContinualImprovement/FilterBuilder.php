<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Leep\AdminBundle\Business;

class FilterBuilder {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));

        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_TodoGoal_Status'),
            'empty_value' => false
        )); 
    }

    public function buildQuery($queryBuilder, $filters) {
        if (!empty($filters->dateFrom)) {
            $queryBuilder->andWhere('p.creationDate >= :dateFrom')
                ->setParameter('dateFrom', $filters->dateFrom);
        }
        if (!empty($filters->dateTo)) {
            $queryBuilder->andWhere('p.creationDate <= :dateTo')
                ->setParameter('dateTo', $filters->dateTo);
        }
        if ($filters->status != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $filters->status);
        }
    }
}