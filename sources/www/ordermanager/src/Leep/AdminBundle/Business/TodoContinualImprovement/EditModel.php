<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $idUser;
    public $creationDate;
    public $name;
    public $description;
    public $milestones;
    public $status;
    public $idFinishIn;
}
