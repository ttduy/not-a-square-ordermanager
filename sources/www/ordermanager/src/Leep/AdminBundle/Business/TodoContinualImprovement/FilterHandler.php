<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        
        return $model;
    }

    public function buildForm($builder) {
        $filterBuilder = $this->container->get('leep_admin.todo_continual_improvement.business.filter_builder');
        $filterBuilder->buildForm($builder);
        
        return $builder;
    }
}