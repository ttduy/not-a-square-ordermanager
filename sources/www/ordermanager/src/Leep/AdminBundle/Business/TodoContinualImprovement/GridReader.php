<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'idUser', 'creationDate', 'status', 'idFinishIn', 'statistics', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',          'width' => '25%', 'sortable' => 'true'),
            array('title' => 'User',          'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Creation date', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Status',        'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Finish in',     'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Statistics',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_TodoContinualImprovementGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:TodoContinualImprovement', 'p');
        
        $filterBuilder = $this->container->get('leep_admin.todo_continual_improvement.business.filter_builder');
        $filterBuilder->buildQuery($queryBuilder, $this->filters);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');        
        $builder->addButton('Detail', $mgr->getUrl('leep_admin', 'todo_task', 'feature', 'applyTaskFilter', array('idContinualImprovement' => $row->getId())), 'button-detail');

        if ($helperSecurity->hasPermission('strategyPlanningToolModify')) {
            // button : edit
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'todo_continual_improvement', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');

            // button : delete
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'todo_continual_improvement', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellStatus($row) {
        $mapping = $this->container->get('easy_mapping');
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $mgr = $this->getModuleManager();       

        $statusSelectBox = "";
        if ($helperSecurity->hasPermission('strategyPlanningToolModify')) {
            $arrStatus = $mapping->getMapping('LeepAdmin_TodoGoal_Status');

            //$href = "alert('hello ' + this.value)";
            $href = $mgr->getUrl('leep_admin', 'todo_continual_improvement', 'feature', 'setStatus', array('id' => $row->getId()));
            $href = "javascript:callTaskAjax('".$href."&status='+this.value)";
            $statusSelectBox = '<select onchange="' . $href . '">';
            foreach ($arrStatus as $statusKey => $status) {
                $statusSelectBox .= '<option value="' . $statusKey . '"' . (($row->getStatus() === $statusKey) ? "selected" : "") . '>' . $status . '</option>';
            }
            $statusSelectBox .= '</select>';
            $statusSelectBox .= '<br/><br/>';
        }

        return $statusSelectBox;
    }

    public function buildCellStatistics($row) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p.status, COUNT(p) total')
            ->from('AppDatabaseMainBundle:TodoTask', 'p')
            ->andWhere('p.idContinualImprovement = :idContinualImprovement')
            ->setParameter('idContinualImprovement', $row->getId())
            ->groupBy('p.status');
        $results = $query->getQuery()->getResult();
        $status = array();
        foreach ($results as $r) {
            $status[$r['status']] = $r['total'];
        }

        $numActive = isset($status[Business\TodoTask\Constants::STATUS_ACTIVE]) ? $status[Business\TodoTask\Constants::STATUS_ACTIVE] : 0;
        $numCompleted = isset($status[Business\TodoTask\Constants::STATUS_COMPLETED]) ? $status[Business\TodoTask\Constants::STATUS_COMPLETED] : 0;

        return sprintf("%s active task(s)<br/>%s completed task(s)", $numActive, $numCompleted);
    }

    public function buildCellName($row) {
        $html = '<b>'.$row->getName().'</b><br/>';
        $html .= nl2br($row->getDescription())."<br/>";

        if ($row->getMilestones() != '') {
            $html .= "<br/>Milestones: <br/>".nl2br($row->getMilestones());
        }
        return $html;
    }
}
