<?php
namespace Leep\AdminBundle\Business\TodoContinualImprovement;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $user = $userManager->getUserSession()->getUser();

        $model = new CreateModel();        
        $model->idUser = $user->getId();
        $model->creationDate = new \DateTime();
        $model->status = Business\TodoGoal\Constants::STATUS_FOR_DISCUSSION;
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('idUser', 'choice', array(
            'label'    => 'Creator',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('milestones', 'textarea', array(
            'label'    => 'Milestones',
            'required' => false,
            'attr'     => array(
                'rows'   => 5,
                'cols'   => 70
            )
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_Status')
        ));
        $builder->add('idFinishIn', 'choice', array(
            'label'    => 'Finish in',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_TodoGoal_FinishIn')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $improvement = new Entity\TodoContinualImprovement();
        $improvement->setIdUser($model->idUser);
        $improvement->setCreationDate($model->creationDate);
        $improvement->setName($model->name);
        $improvement->setDescription($model->description);
        $improvement->setMilestones($model->milestones);
        $improvement->setStatus($model->status);
        $improvement->setIdFinishIn($model->idFinishIn);
        $em->persist($improvement);
        $em->flush();

        parent::onSuccess();
    }
}
