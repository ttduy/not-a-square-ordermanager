<?php
namespace Leep\AdminBundle\Business\ApplicationFont;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('fontFile',          'app_attachment', array(
            'label'    => 'Font file (.TTF)',
            'required' => false,
            'path'     => 'fonts'
        ));

        $builder->add('sectionExtra', 'section', array(
            'label'    => 'Extra',
            'property_path' => false
        ));
        $builder->add('idSpecialLanguage', 'choice', array(
            'label'    => 'Special language',
            'required' => false,
            'choices'  => Utils::getSpecialLanguages()
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        // Validation
        $result = Utils::addFont($this->container, $model->fontFile, $model->idSpecialLanguage);
        if ($result['status'] == FALSE) {
            $this->errors[] = $result['error'];
            return false;
        }

        // Insert
        $font = new Entity\ApplicationFont();
        $font->setName($model->name);
        $font->setFontFile($model->fontFile);
        $font->setTcpdfCode($result['code']);
        $em->persist($font);
        $em->flush();

        parent::onSuccess();
    }
}
