<?php
namespace Leep\AdminBundle\Business\ApplicationFont;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {
    public $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ApplicationFont', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->fontFile = $entity->getFontFile();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('fontFile',          'app_attachment', array(
            'label'    => 'Font file (.TTF)',
            'required' => false,
            'path'     => 'fonts'
        ));

        $builder->add('sectionExtra', 'section', array(
            'label'    => 'Extra',
            'property_path' => false
        ));
        $builder->add('idSpecialLanguage', 'choice', array(
            'label'    => 'Special language',
            'required' => false,
            'choices'  => Utils::getSpecialLanguages()
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setName($model->name);
        if ($model->fontFile !== FALSE) {
            // Validation
            $result = Utils::addFont($this->container, $model->fontFile, $model->idSpecialLanguage);
            if ($result['status'] == FALSE) {
                $this->errors[] = $result['error'];
                return false;
            }

            $this->entity->setTcpdfCode($result['code']);
            $this->entity->setFontFile($model->fontFile);
        }

        parent::onSuccess();

        $this->reloadForm();
    }
}
