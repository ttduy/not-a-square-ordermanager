<?php
namespace Leep\AdminBundle\Business\ApplicationFont;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'fontFile', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',        'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Font file',   'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ApplicationFont', 'p');
        if (!empty($this->filters->name)) {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellFontFile($row) {
        $mgr = $this->getModuleManager();
        $url = $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'viewAttachment', array('dir' => 'fonts', 'name' => $row->getFontFile()));
        return '<a href="'.$url.'">Download</a>';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('reportModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'application_font', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'application_font', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}
