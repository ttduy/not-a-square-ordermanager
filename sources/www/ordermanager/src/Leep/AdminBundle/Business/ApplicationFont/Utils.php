<?php
namespace Leep\AdminBundle\Business\ApplicationFont;

class Utils {
    const SPECIAL_LANGUAGE_JAPANSE   = 1;
    const SPECIAL_LANGUAGE_CHINESE   = 2;
    const SPECIAL_LANGUAGE_CHINESE_SIMPLIFIED = 3;
    const SPECIAL_LANGUAGE_KOREAN    = 4;

    const SPECIAL_LANGUAGE_TRUE_TYPE_UNICODE   = 99;

    public static function getSpecialLanguages() {
        return array(
            self::SPECIAL_LANGUAGE_JAPANSE => 'Japansese',
            self::SPECIAL_LANGUAGE_CHINESE => 'Chinese',
            self::SPECIAL_LANGUAGE_CHINESE_SIMPLIFIED => 'Chinese-simplified',
            self::SPECIAL_LANGUAGE_KOREAN  => 'Korean',
            
            self::SPECIAL_LANGUAGE_TRUE_TYPE_UNICODE  => 'TrueTypeUnicode'
        );
    }

    public static function getSpecialLanguageEncoder() {
        return array(
            self::SPECIAL_LANGUAGE_JAPANSE            => array('CID0JP', 'UniJIS-UTF16-H', 32),
            self::SPECIAL_LANGUAGE_CHINESE            => array('CID0CT', 'UniCNS-UTF16-H', 32),
            self::SPECIAL_LANGUAGE_CHINESE_SIMPLIFIED => array('CID0CS', 'UniGB-UTF16-H', 32),
            self::SPECIAL_LANGUAGE_KOREAN             => array('CID0KR', 'UniKS-UTF16-H', 32),
            self::SPECIAL_LANGUAGE_TRUE_TYPE_UNICODE  => array('TrueTypeUnicode', '', 32)
        );
    }

    public static function addFont($container, $fontFile, $idSpecialLanguage) {
        if ($fontFile == FALSE) {
            return array('status' => FALSE, 'error' => 'Please attachment the font');
        }
        
        // Add font
        $fontFile = $container->getParameter('files_dir').'/fonts/'.$fontFile;
        $pdf = $container->get('white_october.tcpdf')->create();    
        try {
            $type = 'TrueType';
            $enc = '';
            $flag = 32;

            $specialEncoders = self::getSpecialLanguageEncoder();
            if (isset($specialEncoders[$idSpecialLanguage])) {
                $encoder = $specialEncoders[$idSpecialLanguage];
                $type = $encoder[0];
                $enc = $encoder[1];
                $flag = $encoder[2];
            }
                
            $code = $pdf->addTTFFont($fontFile, $type, $enc, $flag);  
            if (empty($code)) {
                throw new \Exception("Can't add font");
            }            
        }
        catch (\Exception $e) {
            return array('status' => FALSE, 'error' => $e->getMessage());
        }

        return array('status' => TRUE, 'code' => $code);
    }
}