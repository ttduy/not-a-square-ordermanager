<?php
namespace Leep\AdminBundle\Business\MwaStatus;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'isSendPush', 'nextStatus', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',           'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Is Send Push',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Next status',    'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:MwaStatus', 'p');
        $queryBuilder->andWhere('p.isDeleted = 0');        
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('mwaProcessorModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'mwa_status', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'mwa_status', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }


    public function buildCellIsSendPush($row) {
        return $row->getIsSendPush() ? 'Yes' : 'No';
    }

    public function buildCellNextStatus($row) {
        $mapping = $this->container->get('easy_mapping');

        $nextStatus = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findByIdMwaStatus($row->getId());
        foreach ($result as $r) {
            $nextStatus[] = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $r->getIdMwaNextStatus());
        }
        return implode(', ', $nextStatus);
    }
}