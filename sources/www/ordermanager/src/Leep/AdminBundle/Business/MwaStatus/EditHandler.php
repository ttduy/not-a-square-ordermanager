<?php
namespace Leep\AdminBundle\Business\MwaStatus;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaStatus', 'app_main')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        
        $model->nextStatus = array();
        $model->isSendPush = $entity->getIsSendPush();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaNextStatus')->findByIdMwaStatus($entity->getId());
        foreach ($result as $r) {
            $model->nextStatus[] = $r->getIdMwaNextStatus();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));        
        $builder->add('nextStatus', 'choice', array(
            'attr'     => array(
                'placeholder' => 'Next status',
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'multiple' => 'multiple',
            'required' => false
        ));
        $builder->add('isSendPush', 'checkbox', array(
            'label'    => 'Is send push',
            'required' => false
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $this->entity->setName($model->name);
        $this->entity->setIsSendPush($model->isSendPush);

        $em = $this->container->get('doctrine')->getEntityManager();     
        $dbHelper = $this->container->get('leep_admin.helper.database');        
        $filters = array('idMwaStatus' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MwaNextStatus', $filters);
        foreach ($model->nextStatus as $nextStatusId) {
            $nStatus = new Entity\MwaNextStatus();
            $nStatus->setIdMwaStatus($this->entity->getId());
            $nStatus->setIdMwaNextStatus($nextStatusId);
            $em->persist($nStatus);
        }
        $em->flush();

        parent::onSuccess();
    }
}