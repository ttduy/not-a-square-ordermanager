<?php
namespace Leep\AdminBundle\Business\MwaStatus;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'attr'     => array(
                'placeholder' => 'Name'
            ),
            'required' => true
        ));        
        $builder->add('nextStatus', 'choice', array(
            'attr'     => array(
                'placeholder' => 'Next status',
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_MwaStatus_List'),
            'multiple' => 'multiple',
            'required' => false
        ));
        $builder->add('isSendPush', 'checkbox', array(
            'label'    => 'Is send push',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $status = new Entity\MwaStatus();
        $status->setName($model->name);
        $status->setIsSendPush($model->isSendPush);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $em->persist($status);
        $em->flush();

        foreach ($model->nextStatus as $nextStatusId) {
            $nStatus = new Entity\MwaNextStatus();
            $nStatus->setIdMwaStatus($status->getId());
            $nStatus->setIdMwaNextStatus($nextStatusId);
            $em->persist($nStatus);
        }
        $em->flush();
        
        parent::onSuccess();
    }
}