<?php
namespace Leep\AdminBundle\Business\Search;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->search = array_keys(Constant::getFindIn());
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('number', 'text', array(
            'label'    => 'Number',
            'required' => false
        ));

        $builder->add('search', 'choice', array(
            'label'    => 'Search',
            'required' => false,
            'attr'     => [
                'style' => 'height: 100px'
                ],
            'multiple' => true,
            'choices'  => Constant::getFindIn(),
            'empty_value' => false
        ));
        return $builder;
    }
}
