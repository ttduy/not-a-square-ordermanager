<?php
namespace Leep\AdminBundle\Business\Search;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;
class GridReader extends AppGridDataReader {
    public $filters;
    protected $result = [];
    protected $columnMapping = array();
    protected $columnSortMapping = array();
    protected $idArray = array();
    protected $totalRow = 0;
    public function getColumnMapping() {
        $this->columnMapping = array();
        $this->columnMapping[] = 'id';
        $this->columnMapping[] = 'name';
        $this->columnMapping[] = 'number';
        $this->columnMapping[] = 'action';

        return $this->columnMapping;
    }
    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'id';
        $this->columnSortMapping[] = 'name';
        $this->columnSortMapping[] = 'number';
        $this->columnSortMapping[] = 'action';

        return $this->columnSortMapping;
    }
    public function getTableHeader() {
        return array(
            array('title' => 'Id',        'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Name',      'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Number',    'width' => '40%', 'sortable' => 'false'),
            array('title' => 'Action',    'width' => '15%', 'sortable' => 'false'),
        );
    }
    public function checkFilterFirst() {
        if (trim($this->filters->number) != '') {
            return true;
        }
        return false;
    }
    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_ContactGridReader');
    }

    ///// raw data of GENO BATCH
    public function rawGenoBatch($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:GenoBatches', 'p')
            ->innerJoin('AppDatabaseMainBundle:GenoLotNumbers', 'lot', 'WITH', 'p.id = lot.batchId');
        if (trim($this->filters->number) != '') {
            $query->andWhere('lot.value LIKE :number')
            ->setParameter('number', '%'. trim($this->filters->number). '%');
        }

        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();

        $rows = $query->select('p.id as id', 'p.supplierName as name', 'lot.value as number', 'lot.id as idLot')
                ->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value['id'],
                'name'      => $value['name'],
                'number'    => $value['number'],
                'link'      => $mgr->getUrl('leep_admin', 'geno_batch', 'edit', 'edit', array('id' => $value['id']))
            ];
        }
    }

    ///// raw data of ORDER
    public function rawOrder($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        if (trim($this->filters->number) != '') {
            $query->andWhere('p.ordernumber LIKE :number')
            ->setParameter('number', '%'. trim($this->filters->number). '%');
        }

        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();

        $rows = $query->select('p')
                ->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getFirstName().' '. $value->getSurName(),
                'number'    => $value->getOrderNumber(),
                'link'      => $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    ///// raw data of Customer
    public function rawCustomerInfo($displayStart, $displayLength) {
        $mgr = $this->getModuleManager();
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfo', 'p');
        if (trim($this->filters->number) != '') {
            $query->andWhere('p.customerNumber LIKE :number')
            ->setParameter('number', '%'. trim($this->filters->number). '%');
        }

        $this->totalRow+= $query->select('COUNT(p)')->getQuery()->getSingleScalarResult();

        $rows = $query->select('p')
                ->setFirstResult($displayStart)->setMaxResults($displayLength)->getQuery()->getResult();
        foreach ($rows as $key => $value) {
            $this->result [] = [
                'id'        => $value->getId(),
                'name'      => $value->getFirstName().' '. $value->getSurName(),
                'number'    => $value->getCustomerNumber(),
                'link'      => $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit', array('id' => $value->getId()))
            ];
        }
    }

    public function buildQuery($queryBuilder) {}
    public function postQueryBuilder($queryBuilder) {}
    public function getCountSQL() {
        return count($this->result);
    }
    public function getTotalRecordsMatched() {
        return $this->totalRow;
    }
    public function loadData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $search = $this->container->get('request')->query->get('search');
        $em = $this->container->get('doctrine')->getEntityManager();
        if ($this->checkFilterFirst() === true) {
            if (count($this->filters->search) > 0) {
                if (in_array(Constant::GENO_BATCH, $this->filters->search) && $search == Constant::GENO_BATCH) {
                    $this->rawGenoBatch($displayStart, $displayLength);
                }
                if (in_array(Constant::ORDER, $this->filters->search) && $search == Constant::ORDER) {
                    $this->rawOrder($displayStart, $displayLength);
                }
                if (in_array(Constant::CUSTOMER, $this->filters->search) && $search == Constant::CUSTOMER) {
                    $this->rawCustomerInfo($displayStart, $displayLength);
                }
            }
        }
    }
    public function preGetResult() {
    }

    public function getResult() {
        $data = array();
        $formatter = $this->getFormatter();
        $this->preGetResult();
        foreach ($this->result as $row) {
            $this->rowData = array();
            foreach ($this->columnMapping as $columnId) {
                if ($formatter != null && $formatter->contains($columnId)) {
                    $this->rowData[] = $formatter->format($columnId, $row);
                }
                else {
                    $readMethod = 'buildCell'.ucfirst($columnId);
                    $getMethod = 'get'.ucfirst($columnId);

                    if (method_exists($this, $readMethod)) {
                        $this->rowData[] = $this->$readMethod($row);
                    }
                    else {
                        if (is_array($row) && isset($row[$columnId])) {
                            $this->rowData[] = $row[$columnId]    ;
                        }
                        else if (method_exists($row, $getMethod)) {
                            $this->rowData[] = $row->$getMethod();
                        }
                        else {
                            $this->rowData[] = '';
                        }
                    }
                }
            }
            $rowId = is_array($row) ? $row['id'] : $row->getId();
            $data[$rowId] = $this->rowData;
            //$data[] = $this->rowData;
        }
        return $data;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('View', $row['link'], 'button-detail');
        return $builder->getHtml();
    }

}
