<?php
namespace Leep\AdminBundle\Business\Search;

class Constant {
    const GENO_BATCH                    = 1;
    const ORDER                         = 2;
    const CUSTOMER                      = 3;

    public static function getFindIn() {
        return array(
            self::GENO_BATCH                    => 'Geno Batch',
            self::ORDER                         => 'Order',
            self::CUSTOMER                      => 'Customer',
        );
    }
}
