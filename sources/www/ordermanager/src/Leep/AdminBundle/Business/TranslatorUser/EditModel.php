<?php
namespace Leep\AdminBundle\Business\TranslatorUser;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $username;
    public $password;
    public $sourceLanguages;
    public $targetLanguages;
    public $reports;
}
