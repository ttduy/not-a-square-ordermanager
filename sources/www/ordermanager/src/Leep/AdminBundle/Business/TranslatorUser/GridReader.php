<?php
namespace Leep\AdminBundle\Business\TranslatorUser;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('username', 'password', 'sourceLanguages', 'targetLanguages', 'reports', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Username', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Password', 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Source languages', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Target languages', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Reports', 'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:TranslatorUser', 'p');
        if (trim($this->filters->username) != '') {
            $queryBuilder->andWhere('p.username LIKE :username')
                ->setParameter('username', '%'.trim($this->filters->username).'%');
        }

        // target language
        if ($this->filters->targetLanguage) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:TranslatorUserTargetLanguage', 'tl', 'WITH', 'p.id = tl.idTranslatorUser')
                        ->andWhere('tl.idLanguage = :targetLanguage')
                        ->setParameter('targetLanguage', $this->filters->targetLanguage);
        }

        // reports
        if ($this->filters->report) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:TranslatorUserReport', 'r', 'WITH', 'p.id = r.idTranslatorUser')
                        ->andWhere('r.idReport = :report')
                        ->setParameter('report', $this->filters->report);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('translatorUserModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'translator_user', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'translator_user', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        $builder->addButton('Switch', $mgr->getUrl('leep_admin', 'app_utils', 'app_utils', 'switchTranslator', array('_switch_user' => $row->getUsername())), 'button-detail');

        return $builder->getHtml();
    }

    public function buildCellSourceLanguages($row) {
        $mapping = $this->container->get('easy_mapping');

        $languages = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserSourceLanguage')->findByIdTranslatorUser($row->getId());
        foreach ($result as $r) {
            $languages[] = $mapping->getMappingTitle('LeepAdmin_Language_List', $r->getIdLanguage());
        }
        return implode('<br/>', $languages);
    }

    public function buildCellTargetLanguages($row) {
        $mapping = $this->container->get('easy_mapping');

        $languages = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserTargetLanguage')->findByIdTranslatorUser($row->getId());
        foreach ($result as $r) {
            $languages[] = $mapping->getMappingTitle('LeepAdmin_Language_List', $r->getIdLanguage());
        }
        return implode('<br/>', $languages);
    }

    public function buildCellReports($row) {
        $mapping = $this->container->get('easy_mapping');

        $reports = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserReport')->findByIdTranslatorUser($row->getId());
        foreach ($result as $r) {
            $reports[] = $mapping->getMappingTitle('LeepAdmin_Report_List', $r->getIdReport());
        }
        return implode('<br/>', $reports);
    }
}
