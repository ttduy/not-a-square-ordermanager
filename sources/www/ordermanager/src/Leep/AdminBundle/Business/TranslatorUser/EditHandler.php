<?php
namespace Leep\AdminBundle\Business\TranslatorUser;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUser', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->username = $entity->getUsername();
        $model->password = $entity->getPassword();

        // Source languages
        $model->sourceLanguages = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserSourceLanguage')->findByIdTranslatorUser($entity->getId());
        foreach ($result as $r) {
            $model->sourceLanguages[] = $r->getIdLanguage();
        }

        // Target languages
        $model->targetLanguages = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserTargetLanguage')->findByIdTranslatorUser($entity->getId());
        foreach ($result as $r) {
            $model->targetLanguages[] = $r->getIdLanguage();
        }

        // Reports
        $model->reports = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserReport')->findByIdTranslatorUser($entity->getId());
        foreach ($result as $r) {
            $model->reports[] = $r->getIdReport();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('username', 'label', array(
            'label'    => 'Username',
            'required' => false
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));
        $builder->add('sourceLanguages', 'choice', array(
            'label'    => 'Source languages',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('targetLanguages', 'choice', array(
            'label'    => 'Target languages',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('reports', 'choice', array(
            'label'    => 'Reports',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Report_List')
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setPassword($model->password);
        
        $filters = array('idTranslatorUser' => $this->entity->getId());
        
        // Source languages
        $dbHelper->delete($em, 'AppDatabaseMainBundle:TranslatorUserSourceLanguage', $filters);        
        foreach ($model->sourceLanguages as $idSourceLanguage) {
            $sourceLanguage = new Entity\TranslatorUserSourceLanguage();
            $sourceLanguage->setIdTranslatorUser($this->entity->getId());
            $sourceLanguage->setIdLanguage($idSourceLanguage);
            $em->persist($sourceLanguage);
        }

        // Target languages
        $dbHelper->delete($em, 'AppDatabaseMainBundle:TranslatorUserTargetLanguage', $filters);        
        foreach ($model->targetLanguages as $idTargetLanguage) {
            $targetLanguage = new Entity\TranslatorUserTargetLanguage();
            $targetLanguage->setIdTranslatorUser($this->entity->getId());
            $targetLanguage->setIdLanguage($idTargetLanguage);
            $em->persist($targetLanguage);
        }

        // Reports
        $dbHelper->delete($em, 'AppDatabaseMainBundle:TranslatorUserReport', $filters);        
        foreach ($model->reports as $idReport) {
            $report = new Entity\TranslatorUserReport();
            $report->setIdTranslatorUser($this->entity->getId());
            $report->setIdReport($idReport);
            $em->persist($report);
        }

        $em->flush();

        parent::onSuccess();
    }
}
