<?php
namespace Leep\AdminBundle\Business\TranslatorUser;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->container = $this->container;
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));
        $builder->add('sourceLanguages', 'choice', array(
            'label'    => 'Source languages',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('targetLanguages', 'choice', array(
            'label'    => 'Target languages',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));
        $builder->add('reports', 'choice', array(
            'label'    => 'Reports',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 200px'
            ),
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Report_List')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $translatorUser = new Entity\TranslatorUser();
        $translatorUser->setUsername($model->username);
        $translatorUser->setPassword($model->password);

        $em->persist($translatorUser);
        $em->flush();

        // Save source languages
        foreach ($model->sourceLanguages as $idSourceLanguage) {
            $sourceLanguage = new Entity\TranslatorUserSourceLanguage();
            $sourceLanguage->setIdTranslatorUser($translatorUser->getId());
            $sourceLanguage->setIdLanguage($idSourceLanguage);
            $em->persist($sourceLanguage);
        }

        // Save target languages
        foreach ($model->targetLanguages as $idTargetLanguage) {
            $targetLanguage = new Entity\TranslatorUserTargetLanguage();
            $targetLanguage->setIdTranslatorUser($translatorUser->getId());
            $targetLanguage->setIdLanguage($idTargetLanguage);
            $em->persist($targetLanguage);
        }

        // Save reports
        foreach ($model->reports as $idReport) {
            $report = new Entity\TranslatorUserReport();
            $report->setIdTranslatorUser($translatorUser->getId());
            $report->setIdReport($idReport);
            $em->persist($report);
        }

        $em->flush();

        parent::onSuccess();
    }
}
