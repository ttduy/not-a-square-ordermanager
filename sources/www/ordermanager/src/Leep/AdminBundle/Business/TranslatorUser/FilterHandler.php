<?php
namespace Leep\AdminBundle\Business\TranslatorUser;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('username', 'text', array(
            'label'       => 'Username',
            'required'    => false
        ));

        $builder->add('targetLanguage', 'choice', array(
            'label'       => 'Target language',
            'required'    => false,
            'choices'     => $mapping->getMapping('LeepAdmin_Language_List')
        ));

        $builder->add('report', 'choice', array(
            'label'       => 'Report',
            'required'    => false,
            'choices'     => $mapping->getMappingFiltered('LeepAdmin_Report_List')
        ));

        return $builder;
    }
}