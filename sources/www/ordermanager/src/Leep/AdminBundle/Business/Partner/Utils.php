<?php
    namespace Leep\AdminBundle\Business\Partner;

    class Utils{
        static public function getPartnerList($container)
        {
            $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();
            $query->select('p.id, p.partner')
                  ->from('AppDatabaseMainBundle:Partner', 'p');
            $queryResult = $query->getQuery()->getResult();
            $partnerList = [];
            foreach ($queryResult as $qr) {
                $partnerList[$qr['id']] = $qr['partner'];
            }

            return $partnerList;
        }

        static public function applyFilter($queryBuilder, $filters)
        {
            if (trim($filters->domain) != '') {
                $queryBuilder->andWhere('pa.domain LIKE :domain')
                    ->setParameter('domain', '%'.trim($filters->domain).'%');
            }
            if (trim($filters->partner) != '') {
                $queryBuilder->andWhere('pa.partner LIKE :partner')
                    ->setParameter('partner', '%'.trim($filters->partner).'%');
            }
            if (trim($filters->uidNumber) != '') {
                $queryBuilder->andWhere('pa.uidnumber LIKE :uidNumber')
                    ->setParameter('uidNumber', '%'.trim($filters->uidNumber).'%');
            }
            if ($filters->genderId != 0) {
                $queryBuilder->andWhere('pa.genderid = :genderId')
                    ->setParameter('genderId', $filters->genderId);
            }
            if (trim($filters->name) != '') {
                $queryBuilder->andWhere('(pa.firstname LIKE :name) OR (pa.surname LIKE :name)')
                    ->setParameter('name', '%'.trim($filters->name).'%');
            }
            if (trim($filters->institution) != '') {
                $queryBuilder->andWhere('pa.institution LIKE :institution')
                    ->setParameter('institution', '%'.trim($filters->institution).'%');
            }
            if (trim($filters->idCode) != '') {
                $queryBuilder->andWhere('p.idCode LIKE :idCode')
                    ->setParameter('idCode', '%'.trim($filters->idCode).'%');
            }
            if ($filters->complaintStatus != 0) {
                if ($filters->complaintStatus == -1) {
                    $queryBuilder->andWhere('pa.complaintStatus > 0');
                }
                else {
                    $queryBuilder->andWhere('pa.complaintStatus = :complaintStatus')
                        ->setParameter('complaintStatus', $filters->complaintStatus);
                }
            }
            if ($filters->complaintTopic != 0) {
                $queryBuilder->innerJoin('AppDatabaseMainBundle:PartnerComplaint', 'pc', 'WITH', 'pa.id = pc.idPartner')
                    ->andWhere('pc.idTopic = '.$filters->complaintTopic);
            }
            if ($filters->positiveFeedback != 0) {
                $queryBuilder->andWhere('pa.numberFeedback > 0');
            }
            if ($filters->idStopShipping != 0) {
                if ($filters->idStopShipping == 1) {
                    $queryBuilder->andWhere('pa.isStopShipping = 1');
                }
                else {
                    $queryBuilder->andWhere('(pa.isStopShipping IS NULL) OR (pa.isStopShipping = 0)');
                }
            }
            if ($filters->idInvoiceGoesTo != 0) {
                $queryBuilder->andWhere('pa.idInvoicesGoesTo = :idInvoiceGoesTo')
                    ->setParameter('idInvoiceGoesTo', $filters->idInvoiceGoesTo);
            }

            if ($filters->idAcquisiteur != 0) {
                $queryBuilder->andWhere('(pa.acquisiteurid1 = :idAcquisiteur) OR (pa.acquisiteurid2 = :idAcquisiteur) OR (pa.acquisiteurid3 = :idAcquisiteur)')
                    ->setParameter('idAcquisiteur', $filters->idAcquisiteur);
            }

            if ($filters->rating != 0) {
                $queryBuilder->andWhere('pa.rating = :rating')
                    ->setParameter('rating', $filters->rating);
            }
        }
    }
 ?>
