<?php
namespace Leep\AdminBundle\Business\Partner;

class FilterModel {
    public $domain;
    public $partner;
    public $uidNumber;
    public $genderId;
    public $name;
    public $institution;
    public $complaintStatus;
    public $complaintTopic;
    public $positiveFeedback;
    public $idStopShipping;
    public $idInvoiceGoesTo;
    public $idAcquisiteur;
    public $idCode;
    public $accountingCode;
    public $rating;
}
