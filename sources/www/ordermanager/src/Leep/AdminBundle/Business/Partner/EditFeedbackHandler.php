<?php
namespace Leep\AdminBundle\Business\Partner;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditFeedbackHandler extends AppEditHandler {   
    public $idObject = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findOneById($id);
    }

    public function searchFeedback($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:PartnerFeedback', 'p')
            ->andWhere('p.idPartner = :idPartner')
            ->setParameter('idPartner', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function convertToFormModel($entity) {
        $this->idObject = $entity->getId();

        $feedbackList = array();
        $result = $this->searchFeedback($entity->getId());
        foreach ($result as $feedback) {
            $row = new FormType\AppFeedbackRowModel();
            $row->fbTimestamp = $feedback->getFbTimestamp();
            $row->idWebUser = $feedback->getIdWebUser();
            $row->feedbacker = $feedback->getFeedbacker();
            $row->description = $feedback->getDescription();

            $feedbackList[] = $row;
        }

        return array('feedbackList' => $feedbackList);
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionFeedback',          'section', array(
            'label' => 'Positive feedback',
            'property_path' => false
        ));        
        $builder->add('feedbackList', 'container_collection', array(
            'label'    => 'Feedback',
            'required' => false,
            'type'     => new FormType\AppFeedbackRow($this->container)
        ));

        return $builder;
    }

    public function clearOldFeedbacks($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idPartner' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:PartnerFeedback', $filters);
    }

    public function createNewFeedback($id) {
        $feedback = new Entity\PartnerFeedback();
        $feedback->setIdPartner($id);
        return $feedback;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->clearOldFeedbacks($this->idObject);
        
        $sortOrder = 1;
        foreach ($model['feedbackList'] as $row) {
            if (empty($row)) continue;
            $feedback = $this->createNewFeedback($this->idObject);            
            $feedback->setFbTimestamp($row->fbTimestamp);
            $feedback->setIdWebUser($row->idWebUser);
            $feedback->setFeedbacker($row->feedbacker);
            $feedback->setDescription($row->description);
            $feedback->setSortOrder($sortOrder);

            $sortOrder++;
            $em->persist($feedback);
        }

        $this->entity->setNumberFeedback($sortOrder-1);

        $em->flush();        

        parent::onSuccess();        
    }
}
