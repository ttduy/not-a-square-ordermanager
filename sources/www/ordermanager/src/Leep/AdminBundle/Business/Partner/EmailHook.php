<?php
namespace Leep\AdminBundle\Business\Partner;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EmailHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $receiverInfo = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);

        $data['receiverInfo'] = array(
            'name' => $receiverInfo->getPartner()
        );
    }
}
