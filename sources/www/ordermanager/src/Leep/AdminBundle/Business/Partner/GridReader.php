<?php
namespace Leep\AdminBundle\Business\Partner;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {

    public $filters;
    public function getColumnMapping() {
        return array('partner', 'uidNumber', 'weblogin', 'domain', 'accountingCode', 'idCode', 'complaints', 'name', 'institution', 'accountBalance', 'rating', 'otherNotes', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Partner',         'width' => '10%', 'sortable' => 'true'),
            array('title' => 'UID Number',      'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Web Login',       'width' => '6%', 'sortable' => 'false'),
            array('title' => 'Domain',          'width' => '7%', 'sortable' => 'true'),
            array('title' => 'Accounting Code', 'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Id Code',         'width' => '5%', 'sortable' => 'true'),
            array('title' => 'Complaints',      'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Name',            'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Institution',     'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Balance',         'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Rating',          'width' => '7%',  'sortable' => 'false'),
            array('title' => 'Notes',           'width' => '5%',  'sortable' => 'false'),
            array('title' => 'Action',          'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.partner';
        $this->columnSortMapping[] = 'p.uidnumber';
        $this->columnSortMapping[] = 'p.domain';
        $this->columnSortMapping[] = 'p.complaintStatus';
        $this->columnSortMapping[] = 'p.firstname';
        $this->columnSortMapping[] = 'p.institution';
        $this->columnSortMapping[] = 'p.accountBalance';

        return $this->columnSortMapping;
    }
    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_PartnerGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Partner', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        if (trim($this->filters->domain) != '') {
            $queryBuilder->andWhere('p.domain LIKE :domain')
                ->setParameter('domain', '%'.trim($this->filters->domain).'%');
        }
        if (trim($this->filters->partner) != '') {
            $queryBuilder->andWhere('p.partner LIKE :partner')
                ->setParameter('partner', '%'.trim($this->filters->partner).'%');
        }
        if (trim($this->filters->uidNumber) != '') {
            $queryBuilder->andWhere('p.uidnumber LIKE :uidNumber')
                ->setParameter('uidNumber', '%'.trim($this->filters->uidNumber).'%');
        }
        if ($this->filters->genderId != 0) {
            $queryBuilder->andWhere('p.genderid = :genderId')
                ->setParameter('genderId', $this->filters->genderId);
        }
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('(p.firstname LIKE :name) OR (p.surname LIKE :name)')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->institution) != '') {
            $queryBuilder->andWhere('p.institution LIKE :institution')
                ->setParameter('institution', '%'.trim($this->filters->institution).'%');
        }
        if (trim($this->filters->idCode) != '') {
            $queryBuilder->andWhere('p.idCode LIKE :idCode')
                ->setParameter('idCode', '%'.trim($this->filters->idCode).'%');
        }
        if (trim($this->filters->accountingCode) != '') {
            $queryBuilder->andWhere('p.accountingCode = :accountingCode')
                ->setParameter('accountingCode',$this->filters->accountingCode);
        }
        if ($this->filters->complaintStatus != 0) {
            if ($this->filters->complaintStatus == -1) {
                $queryBuilder->andWhere('p.complaintStatus > 0');
            }
            else {
                $queryBuilder->andWhere('p.complaintStatus = :complaintStatus')
                    ->setParameter('complaintStatus', $this->filters->complaintStatus);
            }
        }
        if ($this->filters->complaintTopic != 0) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:PartnerComplaint', 'pc', 'WITH', 'p.id = pc.idPartner')
                ->andWhere('pc.idTopic = '.$this->filters->complaintTopic);
        }
        if ($this->filters->positiveFeedback != 0) {
            $queryBuilder->andWhere('p.numberFeedback > 0');
        }
        if ($this->filters->idStopShipping != 0) {
            if ($this->filters->idStopShipping == 1) {
                $queryBuilder->andWhere('p.isStopShipping = 1');
            }
            else {
                $queryBuilder->andWhere('(p.isStopShipping IS NULL) OR (p.isStopShipping = 0)');
            }
        }
        if ($this->filters->idInvoiceGoesTo != 0) {
            $queryBuilder->andWhere('p.idInvoicesGoesTo = :idInvoiceGoesTo')
                ->setParameter('idInvoiceGoesTo', $this->filters->idInvoiceGoesTo);
        }

        if ($this->filters->idAcquisiteur != 0) {
            $queryBuilder->andWhere('(p.acquisiteurid1 = :idAcquisiteur) OR (p.acquisiteurid2 = :idAcquisiteur) OR (p.acquisiteurid3 = :idAcquisiteur)')
                ->setParameter('idAcquisiteur', $this->filters->idAcquisiteur);
        }

        if ($this->filters->rating != 0) {
            $queryBuilder->andWhere('p.rating = :rating')
                ->setParameter('rating', $this->filters->rating);
        }
    }

    public function buildCellWebLogin($row) {
        $formater = $this->container->get('leep_admin.helper.formatter');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $html = '%s %s';

        // prepare status
        $strPwdStatus = '';
        if ($row->getWebLoginPassword() != '' && $row->getWebLoginUsername() != '') {
            $strPwdStatus = $row->getWebLoginUsername();
        }

        // button for display info
        $buttonHtml = '';
        $builder->addPopupButton('Send email', $mgr->getUrl('leep_admin', 'partner', 'email', 'create', array('id' => $row->getId())), 'button-email');

        // add button login to customer dashboard
        $builder->addNewTabButton('Login Customer Dashboard', $mgr->getUrl('leep_admin', 'partner', 'feature', 'loginCustomerDashboard', array('id' => $row->getId())), 'button-login-cd');
        $buttonHtml = $builder->getHtml();

        return sprintf($html, $strPwdStatus, "<span>$buttonHtml</span>");
    }

    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'partner', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('partnerModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'partner', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'partner', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellOtherNotes($row) {
        $infoText = $row->getOtherNotes();
        if (!empty($infoText)) {
            return '<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
        }
        return '';
    }

    public function buildCellComplaints($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_ComplaintStatus', $row->getComplaintStatus());

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'partner', 'edit_complaint', 'edit', array('id' => $row->getId())), 'button-edit');

        $html = $builder->getHtml().'&nbsp;&nbsp;'.$title;

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'partner', 'edit_feedback', 'edit', array('id' => $row->getId())), 'button-checked');
        $html .= '<br/>'.$builder->getHtml().'&nbsp;&nbsp'.intval($row->getNumberFeedback()).' feedback(s)';

        return $html;
    }

    public function buildCellAccountBalance($row) {
        $html = '';
        $balance = $row->getAccountBalance();
        if ($balance < 0) {
            $html = 'MINUS &euro;'.number_format(-$balance, 2);
        }
        else if ($balance > 0) {
            $html .= 'PLUS &euro;'.number_format($balance, 2);
        }
        else {
            $html .= '&euro;0.00';
        }

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addPopupButton('Edit', $mgr->getUrl('leep_admin', 'partner_account_entry', 'grid', 'list', array('idParent' => $row->getId())), 'button-edit');

        $html .= '&nbsp;&nbsp;'.$builder->getHtml();

        return $html;
    }

    public function buildCellPartner($row) {
        $name = $row->getPartner();

        if ($row->getIsStopShipping()) {
            $name .= '&nbsp;&nbsp;<div class="button-delete"></div>';
        }

        return $name;
    }

    public function buildCellRating($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $mapping = $this->container->get('easy_mapping');

        $html = array();

        if ($row->getRating()) {
            $html[] = $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $row->getRating());
        }

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('partnerView')) {
            $builder->addPopupButton('View History', $mgr->getUrl('leep_admin', 'rating', 'feature', 'partnerRatingHistory', array('id' => $row->getId())), 'button-detail');
            $html[] = $builder->getHtml();
        }

        return implode('&nbsp;&nbsp;', $html);
    }
}
