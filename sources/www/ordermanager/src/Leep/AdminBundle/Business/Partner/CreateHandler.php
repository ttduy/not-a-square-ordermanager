<?php
namespace Leep\AdminBundle\Business\Partner;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $em = $this->container->get('doctrine')->getEntityManager();
        // accountingCode
        $query = $em->createQueryBuilder();
        $accountingCodeMaxDC = $query->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:DistributionChannels', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryPartner = $em->createQueryBuilder();
        $accountingCodeMaxPartner = $queryPartner->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Partner', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();

        $queryAcquisiteur = $em->createQueryBuilder();
        $accountingCodeMaxAcqui = $queryAcquisiteur->select('MAX(p.accountingCode)')->from('AppDatabaseMainBundle:Acquisiteur', 'p')
        ->orderBy('p.accountingCode', 'DESC')
        ->getQuery()
        ->getSingleScalarResult();
        $max1 = ($accountingCodeMaxDC > $accountingCodeMaxPartner) ? $accountingCodeMaxDC : $accountingCodeMaxPartner;
        $maxAC = ($accountingCodeMaxAcqui > $max1) ? $accountingCodeMaxAcqui : $max1;

        $codeOld = [];
        $codeOldPart = [];
        $randomletter = '';
        do {
            $charset = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
            $randomletter = substr(str_shuffle($charset), 0, 5);
            $codeOldDC =  $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($randomletter);
            $codeOldPart =  $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($randomletter);
        }
        while ($codeOld || $codeOldPart);

        $model = new CreateModel();
        $model->genderId = Business\Base\Constant::GENDER_MALE;
        $model->creationDate = new \DateTime();
        $model->container = $this->container;
        $model->webLoginStatus = Business\Base\Constant::WEB_LOGIN_STATUS_ACTIVE;
        $model->accountingCode = $maxAC+1;
        $model->idCode = $randomletter;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('sectionBasicInformation', 'section', array(
            'label'    => 'Basic Information',
            'property_path' => false
        ));

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('partner', 'text', array(
            'label'    => 'Partner Name',
            'required' => true
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('idCode', 'text', array(
            'label'    => 'Id Code',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));
        $builder->add('idCurrency', 'choice', array(
            'label'    => 'Currency',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Currency_List')
        ));

        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionInvoiceAddress', 'section', array(
            'label'    => 'Invoice address',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('isInvoiceWithTax', 'checkbox', array(
            'label'    => 'Is invoice with tax?',
            'required' => false
        ));

        // Contact person
        $builder->add('sectionContactPerson', 'section', array(
            'label'    => 'Contact person',
            'property_path' => false
        ));
        $builder->add('contactPersonName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('contactPersonEmail', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('contactPersonTelephoneNumber', 'text', array(
            'label'    => 'Telephone number',
            'required' => false
        ));


        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login',
            'property_path' => false
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'partner', 'partner', 'generateUsername'),
            'name_source_field_id' => 'partner'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'partner', 'partner', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web Login email Opt-Out?',
            'required' => false
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));
        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));
        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));
        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));
        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));
        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));
        $builder->add('isNotAccessReport', 'checkbox', array(
            'label'    => "Can't access report?",
            'required' => false
        ));
        $builder->add('isNotSendEmail', 'checkbox', array(
            'label'    => "Can't send email?",
            'required' => false
        ));


        // Delivery
        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryNote', 'choice', array(
            'label'    => 'Invoice Delivery Note',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Bill_DeliveyNoteTypes')
        ));
        $builder->add('idInvoicesGoesTo', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('invoiceNoTaxText', 'choice', array(
            'label'    => 'Invoice No Tax Text',
            'required' => true,
            'choices'  => Business\InvoiceNoTaxText\Constant::get($this->container)
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));

        $builder->add('section3', 'section', array(
            'label'    => 'Acquisiteur(s)',
            'property_path' => false
        ));
        $builder->add('acquisiteurId1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage1', 'text', array(
            'label'    => 'Yearly decay percentage 1',
            'required' => false
        ));
        $builder->add('acquisiteurId2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage2', 'text', array(
            'label'    => 'Yearly decay percentage 2',
            'required' => false
        ));
        $builder->add('acquisiteurId3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage3', 'text', array(
            'label'    => 'Yearly decay percentage 3',
            'required' => false
        ));

        $builder->add('section4', 'section', array(
            'label'    => 'Web User',
            'property_path' => false
        ));
        $builder->add('webUserId', 'choice', array(
            'label'    => 'Web User',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));

        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 10,  'cols' => 80
            )
        ));

        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('isStopShipping', 'checkbox', array(
            'label'    => 'Stop shipping',
            'required' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));

        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));

        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));

        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));
        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
        $builder->add('isGenerateFoodTableExcelVersion', 'checkbox', array(
            'label'    => 'Is generate food table excel version?',
            'required' => false
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));


        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));

    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        /// check
        $query = $em->createQueryBuilder();
        $partnerOld = $query->select('p')->from('AppDatabaseMainBundle:Partner', 'p')
        ->andWhere('p.partner =:name')
        ->setParameter('name', $model->partner)->getQuery()->getResult();
        if ($partnerOld) {
            // Validation
            $this->errors += ['Partner existed. Please try a diffrent name!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        // validation accounting code
        $dcOld = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
        $partnerOld = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
        $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

        if ($dcOld || $partnerOld || $acquisiteur) {
            // Validation
            $this->errors += ['Accounting code existed. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }

        /// Id Code
        /// validation id code char == 5
        if (strlen($model->idCode) != 5) {
            $this->errors += ['Id code must be 5 char. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        $dcOld = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($model->idCode);
        $partnerOld = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($model->idCode);
        if ($dcOld || $partnerOld) {
            // Validation
            $this->errors += ['Id code existed. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }

        $partner = new Entity\Partner();

        if ($model->webLoginUsername) {
            $partner->setWebLoginUserName($model->webLoginUsername->username);
        }

        if ($model->webLoginPassword) {
            $partner->setWebLoginPassword($model->webLoginPassword->password);
        }

        $partner->setWebLoginStatus($model->webLoginStatus);
        $partner->setWebLoginGoesTo($model->webLoginGoesTo);
        $partner->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $partner->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $partner->setIsNutrimeOffered($model->isNutrimeOffered);
        $partner->setIsNotContacted($model->isNotContacted);
        $partner->setIsDestroySample($model->isDestroySample);
        $partner->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $partner->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $partner->setCanViewShop($model->canViewShop);
        $partner->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
        $partner->setIsGenerateFoodTableExcelVersion($model->isGenerateFoodTableExcelVersion);

        $partner->setCreationDate($model->creationDate);
        $partner->setUidNumber($model->uidNumber);
        $partner->setGenderId($model->genderId);

        $partner->setDomain($model->domain);
        $partner->setPartner($model->partner);
        $partner->setCreationDate($model->creationDate);
        $partner->setUidNumber($model->uidNumber);
        $partner->setGenderId($model->genderId);
        $partner->setTitle($model->title);
        $partner->setFirstName($model->firstName);
        $partner->setSurName($model->surName);
        $partner->setInstitution($model->institution);
        $partner->setIdCurrency($model->idCurrency);

        $partner->setStreet($model->street);
        $partner->setPostCode($model->postCode);
        $partner->setCity($model->city);
        $partner->setCountryId($model->countryId);
        $partner->setContactEmail($model->contactEmail);
        $partner->setTelephone($model->telephone);
        $partner->setFax($model->fax);

        $partner->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $partner->setInvoiceClientName($model->invoiceClientName);
        $partner->setInvoiceCompanyName($model->invoiceCompanyName);
        $partner->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $partner->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $partner->setInvoiceAddressCity($model->invoiceAddressCity);
        $partner->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $partner->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $partner->setInvoiceAddressFax($model->invoiceAddressFax);
        $partner->setIsInvoiceWithTax($model->isInvoiceWithTax);

        $partner->setContactPersonName($model->contactPersonName);
        $partner->setContactPersonEmail($model->contactPersonEmail);
        $partner->setContactPersonTelephoneNumber($model->contactPersonTelephoneNumber);

        $partner->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $partner->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);
        $partner->setInvoiceDeliveryNote($model->invoiceDeliveryNote);
        $partner->setIdInvoicesGoesTo($model->idInvoicesGoesTo);
        $partner->setIdMarginGoesTo($model->idMarginGoesTo);
        $partner->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $partner->setIsPaymentWithTax($model->isPaymentWithTax);
        $partner->setIdNutrimeGoesTo($model->idNutrimeGoesTo);
        $partner->setCommissionPaymentWithTax($model->commissionPaymentWithTax);


        $partner->setAcquisiteurId1($model->acquisiteurId1);
        $partner->setCommission1($model->commission1);
        $partner->setYearlyDecayPercentage1($model->yearlyDecayPercentage1);
        $partner->setAcquisiteurId2($model->acquisiteurId2);
        $partner->setCommission2($model->commission2);
        $partner->setYearlyDecayPercentage2($model->yearlyDecayPercentage2);
        $partner->setAcquisiteurId3($model->acquisiteurId3);
        $partner->setCommission3($model->commission3);
        $partner->setYearlyDecayPercentage3($model->yearlyDecayPercentage3);

        $partner->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $partner->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $partner->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $partner->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $partner->setIsCanViewSetting($model->isCanViewSetting);
        $partner->setIsDelayDownloadReport($model->isDelayDownloadReport);
        if ($model->isDelayDownloadReport == true) {
            $partner->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }
        $partner->setIsNotAccessReport($model->isNotAccessReport);
        $partner->setIsNotSendEmail($model->isNotAccessReport);

        $partner->setWebUserId($model->webUserId);

        $partner->setAccountName($model->accountName);
        $partner->setAccountNumber($model->accountNumber);
        $partner->setBankCode($model->bankCode);
        $partner->setBIC($model->bic);
        $partner->setIBAN($model->iban);
        $partner->setOtherNotes($model->otherNotes);

        $partner->setIdPreferredPayment($model->idPreferredPayment);
        $partner->setIsStopShipping($model->isStopShipping);
        /// set Tax
        $partner->setPaymentNoTaxText($model->paymentNoTaxText);
        $partner->setInvoiceNoTaxText($model->invoiceNoTaxText);
        $selectedLanguage = null;
        if ($model->idLanguage) {
            $selectedLanguage = $model->idLanguage;
        }
        $partner->setIdLanguage($selectedLanguage);
        // $partner->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
        $partner->setRating($model->rating);

        $partner->setIdRecallGoesTo($model->idRecallGoesTo);
        $partner->setIdCode($model->idCode);
        $partner->setAccountingCode($model->accountingCode);
        $em->persist($partner);
        $em->flush();

        parent::onSuccess();
    }
}
