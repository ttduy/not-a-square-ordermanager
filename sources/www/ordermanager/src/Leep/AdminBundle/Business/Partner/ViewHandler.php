<?php
namespace Leep\AdminBundle\Business\Partner;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');

        $id = $this->container->get('request')->query->get('id', 0);
        $partner = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner')->findOneById($id);
        $data[] = $this->addSection('Partner');
        $data[] = $this->add('Domain', $partner->getDomain());
        $data[] = $this->add('Partner', $partner->getPartner());
        $data[] = $this->add('UID Number', $partner->getUidNumber());
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $partner->getGenderId()));
        $data[] = $this->add('Title', $partner->getTitle());
        $data[] = $this->add('First Name', $partner->getFirstName());
        $data[] = $this->add('Surname', $partner->getSurName());
        $data[] = $this->add('Institution', $partner->getInstitution());

        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $partner->getStreet());
        $data[] = $this->add('Post Code', $partner->getPostCode());
        $data[] = $this->add('City', $partner->getCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $partner->getCountryId()));
        $data[] = $this->add('Contact Email', $partner->getContactEmail());
        $data[] = $this->add('Telephone', $partner->getTelephone());
        $data[] = $this->add('Fax', $partner->getFax());

        $data[] = $this->addSection('Delivery');
        $data[] = $this->add('Status Delivery Email', $partner->getStatusDeliveryEmail());
        $data[] = $this->add('Invoice Delivery Email', $partner->getInvoiceDeliveryEmail());
        $data[] = $this->add('Invoice Delivery Note', $partner->getInvoiceDeliveryNote());

        $data[] = $this->addSection('Acquisiteur (s)');
        $data[] = $this->add('Acquisiteur 1',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $partner->getAcquisiteurId1()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $partner->getAcquisiteurId1()))
            )
        );
        $data[] = $this->add('Commission 1', $partner->getCommission1());
        $data[] = $this->add('Acquisiteur 2',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $partner->getAcquisiteurId2()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $partner->getAcquisiteurId2()))
            )
        );
        $data[] = $this->add('Commission 2', $partner->getCommission2());
        $data[] = $this->add('Acquisiteur 3',
            $builder->getLink(
                $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $partner->getAcquisiteurId3()),
                $mgr->getUrl('leep_admin', 'acquisiteur', 'view', 'view', array('id' => $partner->getAcquisiteurId3()))
            )
        );
        $data[] = $this->add('Commission 3', $partner->getCommission3());
        $data[] = $this->add('Web User', $mapping->getMappingTitle('LeepAdmin_WebUser_List', $partner->getWebUserId()));

        $data[] = $this->addSection('Bank account');
        $data[] = $this->add('Account Name', $partner->getAccountName());
        $data[] = $this->add('Account Number', $partner->getAccountNumber());
        $data[] = $this->add('Bank Code', $partner->getBankCode());
        $data[] = $this->add('BIC', $partner->getBIC());
        $data[] = $this->add('IBAN', $partner->getIBAN());
        $data[] = $this->add('Other Notes', $partner->getOtherNotes());

        $data[] = $this->addSection('Others');
        $data[] = $this->add('Rating', $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $partner->getRating()));
        $data[] = $this->add('Can be contacted directly', ($partner->getIsCustomerBeContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('NutriMe Supplements are offered', ($partner->getIsNutrimeOffered()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer chose OPT OUT – don’t contact', ($partner->getIsNotContacted()) ? 'Yes' : 'No');
        $data[] = $this->add('Destroy details and samples after completion', ($partner->getIsDestroySample()) ? 'Yes' : 'No');
        $data[] = $this->add('Customer agreed to extended research participation', ($partner->getIsFutureResearchParticipant()) ? 'Yes' : 'No');
        $data[] = $this->add('Sample can be used for science', ($partner->getSampleCanBeUsedForScience()) ? 'Yes' : 'No');
        $data[] = $this->add('Language', $mapping->getMappingTitle('LeepAdmin_Customer_Language', $partner->getIdLanguage()));
        $data[] = $this->add('Web Login Opt-Out?', ($partner->getIsWebLoginOptOut()) ? 'Yes' : 'No');

        return $data;
    }
}
