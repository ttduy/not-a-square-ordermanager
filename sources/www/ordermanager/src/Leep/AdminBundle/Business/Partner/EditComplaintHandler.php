<?php
namespace Leep\AdminBundle\Business\Partner;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends AppEditHandler {   
    public $idObject = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:PartnerComplaint', 'p')
            ->andWhere('p.idPartner = :idPartner')
            ->setParameter('idPartner', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function convertToFormModel($entity) {
        $this->idObject = $entity->getId();

        $complaintList = array();
        $result = $this->searchComplaint($entity->getId());
        foreach ($result as $complaint) {
            $row = new FormType\AppComplaintRowModel();
            $row->problemTimestamp = $complaint->getProblemTimestamp();
            $row->idTopic = $complaint->getIdTopic();
            $row->problemIdWebUser = $complaint->getProblemIdWebUser();
            $row->problemComplaintant = $complaint->getProblemComplaintant();
            $row->problemDescription = $complaint->getProblemDescription();
            $row->solutionTimestamp = $complaint->getSolutionTimestamp();
            $row->solutionIdWebUser = $complaint->getSolutionIdWebUser();
            $row->solutionDescription = $complaint->getSolutionDescription();

            $complaintList[] = $row;
        }

        return array('complaintList' => $complaintList, 'complaintStatus' => $entity->getComplaintStatus());
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionComplaints',          'section', array(
            'label' => 'Complaint(s)',
            'property_path' => false
        ));        
        $builder->add('complaintList', 'container_collection', array(
            'label'    => 'Complaints',
            'required' => false,
            'type'     => new FormType\AppComplaintRow($this->container)
        ));
        
        $builder->add('sectionCurrentStatus',          'section', array(
            'label' => 'Current Status',
            'property_path' => false
        ));
        $builder->add('complaintStatus',            'choice', array(
            'label' => 'Status',
            'required'      => false,
            'choices' => $mapping->getMapping('LeepAdmin_ComplaintStatus')
        ));

        return $builder;
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idPartner' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:PartnerComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\PartnerComplaint();
        $complaint->setIdPartner($id);
        return $complaint;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->clearOldComplaints($this->idObject);        
        $this->entity->setComplaintStatus($model['complaintStatus']);
        $em->persist($this->entity);
        
        $sortOrder = 1;
        foreach ($model['complaintList'] as $row) {
            if (empty($row)) continue;
            $complaint = $this->createNewComplaint($this->idObject);            
            $complaint->setProblemTimestamp($row->problemTimestamp);
            $complaint->setIdTopic($row->idTopic);
            $complaint->setProblemIdWebUser($row->problemIdWebUser);
            $complaint->setProblemComplaintant($row->problemComplaintant);
            $complaint->setProblemDescription($row->problemDescription);
            $complaint->setSolutionTimestamp($row->solutionTimestamp);
            $complaint->setSolutionIdWebUser($row->solutionIdWebUser);
            $complaint->setSolutionDescription($row->solutionDescription);
            $complaint->setSortOrder($sortOrder);

            $sortOrder++;
            $em->persist($complaint);
        }

        $em->flush();        

        parent::onSuccess();        
    }
}
