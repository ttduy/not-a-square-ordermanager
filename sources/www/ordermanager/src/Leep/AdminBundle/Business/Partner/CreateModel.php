<?php
namespace Leep\AdminBundle\Business\Partner;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $webLoginUsername;
    public $webLoginPassword;
    public $webLoginStatus;
    public $webLoginGoesTo;
    public $isWebLoginOptOut;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $sampleCanBeUsedForScience;
    public $isFutureResearchParticipant;
    public $idCurrency;

    public $canViewShop;
    public $canCustomersDownloadReports;
    public $isGenerateFoodTableExcelVersion;

    public $domain;
    public $partner;
    public $creationDate;
    public $uidNumber;
    public $genderId;
    public $title;
    public $firstName;
    public $surName;
    public $institution;

    public $street;
    public $postCode;
    public $city;
    public $countryId;
    public $contactEmail;
    public $telephone;
    public $fax;

    public $invoiceAddressIsUsed;
    public $invoiceCompanyName;
    public $invoiceClientName;
    public $invoiceAddressStreet;
    public $invoiceAddressPostCode;
    public $invoiceAddressCity;
    public $invoiceAddressIdCountry;
    public $invoiceAddressTelephone;
    public $invoiceAddressFax;
    public $isInvoiceWithTax;

    public $contactPersonName;
    public $contactPersonEmail;
    public $contactPersonTelephoneNumber;

    public $statusDeliveryEmail;
    public $invoiceDeliveryEmail;
    public $invoiceDeliveryNote;
    public $idInvoicesGoesTo;
    public $idMarginGoesTo;
    public $idMarginPaymentMode;
    public $isPaymentWithTax;
    public $paymentNoTaxText;
    public $invoiceNoTaxText;
    public $idNutrimeGoesTo;
    public $commissionPaymentWithTax;

    public $acquisiteurId1;
    public $commission1;
    public $yearlyDecayPercentage1;
    public $acquisiteurId2;
    public $commission2;
    public $yearlyDecayPercentage2;
    public $acquisiteurId3;
    public $commission3;
    public $yearlyDecayPercentage3;

    public $webUserId;

    public $accountName;
    public $accountNumber;
    public $bankCode;
    public $bic;
    public $iban;
    public $otherNotes;
    public $idPreferredPayment;
    public $isStopShipping;
    public $idLanguage;
    // public $canCreateLoginAndSendStatusMessageToUser;
    public $rating;

    public $idRecallGoesTo;

    public $isCanViewYourOrder;
    public $isCanViewCustomerProtection;
    public $isCanViewRegisterNewCustomer;
    public $isCanViewPendingOrder;
    public $isCanViewSetting;
    public $accountingCode;
    public $idCode;
    // for validation
    public $container;
    public $isDelayDownloadReport;
    public $numberDateDelayDownloadReport;
    public $isNotAccessReport;
    public $isNotSendEmail;

    public function isValid(ExecutionContext $context) {
        $helperCommon = $this->container->get('leep_admin.helper.common');

        if ($this->webLoginUsername != null) {
            $username = $this->webLoginUsername->username;
            if ($helperCommon->isWebLoginUsernameExisted($username)) {
                $context->addViolationAtSubPath('webLoginUsername', "This Web Login Username " . $username . " is already existed");
            }
        }
    }
}
