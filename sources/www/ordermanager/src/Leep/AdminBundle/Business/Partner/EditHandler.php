<?php
namespace Leep\AdminBundle\Business\Partner;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();

        $model->container = $this->container;

        $model->id = $entity->getId();

        $webLoginUsername = new Business\FormType\AppUsernameRowModel();
        $webLoginUsername->username = $entity->getWebLoginUsername();
        $model->webLoginUsername = $webLoginUsername;

        $webLoginPassword = new Business\FormType\AppPasswordRowModel();
        $webLoginPassword->password = $entity->getWebLoginPassword();
        $model->webLoginPassword = $webLoginPassword;

        $model->webLoginStatus = $entity->getWebLoginStatus();
        $model->webLoginGoesTo = $entity->getWebLoginGoesTo();
        $model->isWebLoginOptOut = $entity->getIsWebLoginOptOut();

        $model->isCustomerBeContacted = $entity->getIsCustomerBeContacted();
        $model->isNutrimeOffered = $entity->getIsNutrimeOffered();
        $model->isNotContacted = $entity->getIsNotContacted();
        $model->isDestroySample = $entity->getIsDestroySample();
        $model->isFutureResearchParticipant = $entity->getIsFutureResearchParticipant();
        $model->sampleCanBeUsedForScience = $entity->getSampleCanBeUsedForScience();

        $model->canViewShop = $entity->getCanViewShop();
        $model->canCustomersDownloadReports = $entity->getCanCustomersDownloadReports();
        $model->isGenerateFoodTableExcelVersion = $entity->getIsGenerateFoodTableExcelVersion();

        $model->domain = $entity->getDomain();
        $model->partner = $entity->getPartner();
        $model->creationDate = $entity->getCreationDate();
        $model->uidNumber = $entity->getUidNumber();
        $model->genderId = $entity->getGenderId();
        $model->title = $entity->getTitle();
        $model->firstName = $entity->getFirstName();
        $model->surName = $entity->getSurName();
        $model->institution = $entity->getInstitution();
        $model->idCurrency = $entity->getIdCurrency();

        $model->street = $entity->getStreet();
        $model->postCode = $entity->getPostCode();
        $model->city = $entity->getCity();
        $model->countryId = $entity->getCountryId();
        $model->contactEmail = $entity->getContactEmail();
        $model->telephone = $entity->getTelephone();
        $model->fax = $entity->getFax();

        $model->invoiceAddressIsUsed = $entity->getInvoiceAddressIsUsed();
        $model->invoiceClientName = $entity->getInvoiceClientName();
        $model->invoiceCompanyName = $entity->getInvoiceCompanyName();
        $model->invoiceAddressStreet = $entity->getInvoiceAddressStreet();
        $model->invoiceAddressPostCode = $entity->getInvoiceAddressPostCode();
        $model->invoiceAddressCity = $entity->getInvoiceAddressCity();
        $model->invoiceAddressIdCountry = $entity->getInvoiceAddressIdCountry();
        $model->invoiceAddressTelephone = $entity->getInvoiceAddressTelephone();
        $model->invoiceAddressFax = $entity->getInvoiceAddressFax();
        $model->isInvoiceWithTax = $entity->getIsInvoiceWithTax();
        $model->commissionPaymentWithTax = $entity->getCommissionPaymentWithTax();

        $model->contactPersonName = $entity->getContactPersonName();
        $model->contactPersonEmail = $entity->getContactPersonEmail();
        $model->contactPersonTelephoneNumber = $entity->getContactPersonTelephoneNumber();

        $model->statusDeliveryEmail = $entity->getStatusDeliveryEmail();
        $model->invoiceDeliveryEmail = $entity->getInvoiceDeliveryEmail();
        $model->invoiceDeliveryNote = $entity->getInvoiceDeliveryNote();
        $model->idInvoicesGoesTo = $entity->getIdInvoicesGoesTo();
        $model->idMarginGoesTo = $entity->getIdMarginGoesTo();
        $model->idMarginPaymentMode = $entity->getIdMarginPaymentMode();
        $model->isPaymentWithTax = $entity->getIsPaymentWithTax();
        $model->paymentNoTaxText = $entity->getPaymentNoTaxText();
        $model->invoiceNoTaxText = $entity->getInvoiceNoTaxText();
        $model->idNutrimeGoesTo = $entity->getIdNutrimeGoesTo();

        $model->acquisiteurId1 = $entity->getAcquisiteurId1();
        $model->commission1 = $entity->getCommission1();
        $model->yearlyDecayPercentage1 = $entity->getYearlyDecayPercentage1();
        $model->acquisiteurId2 = $entity->getAcquisiteurId2();
        $model->commission2 = $entity->getCommission2();
        $model->yearlyDecayPercentage2 = $entity->getYearlyDecayPercentage2();
        $model->acquisiteurId3 = $entity->getAcquisiteurId3();
        $model->commission3 = $entity->getCommission3();
        $model->yearlyDecayPercentage3 = $entity->getYearlyDecayPercentage3();

        $model->webUserId = $entity->getWebUserId();

        $model->accountName = $entity->getAccountName();
        $model->accountNumber = $entity->getAccountNumber();
        $model->bankCode = $entity->getBankCode();
        $model->bic = $entity->getBic();
        $model->iban = $entity->getIban();
        $model->otherNotes = $entity->getOtherNotes();

        $model->idPreferredPayment = $entity->getIdPreferredPayment();
        $model->isStopShipping = $entity->getIsStopShipping();

        $model->idLanguage = $entity->getIdLanguage();
        // $model->canCreateLoginAndSendStatusMessageToUser = $entity->getCanCreateLoginAndSendStatusMessageToUser();
        $model->rating = $entity->getRating();

        $model->idRecallGoesTo = $entity->getIdRecallGoesTo();

        $model->isCanViewYourOrder = $entity->getIsCanViewYourOrder();
        $model->isCanViewCustomerProtection = $entity->getIsCanViewCustomerProtection();
        $model->isCanViewRegisterNewCustomer = $entity->getIsCanViewRegisterNewCustomer();
        $model->isCanViewPendingOrder = $entity->getIsCanViewPendingOrder();
        $model->isCanViewSetting = $entity->getIsCanViewSetting();
        $model->accountingCode = $entity->getAccountingCode();
        $model->idCode = $entity->getIdCode();
        $model->isDelayDownloadReport = $entity->getIsDelayDownloadReport();
        $model->numberDateDelayDownloadReport = $entity->getNumberDateDelayDownloadReport();
        $model->isNotAccessReport = $entity->getIsNotAccessReport();
        $model->isNotSendEmail = $entity->getIsNotSendEmail();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('sectionBasicInformation', 'section', array(
            'label'    => 'Basic Information',
            'property_path' => false
        ));

        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('partner', 'text', array(
            'label'    => 'Partner Name',
            'required' => true
        ));
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Date of creation',
            'required' => false
        ));
        $builder->add('idCode', 'text', array(
            'label'    => 'Id Code',
            'required' => false
        ));
        $builder->add('accountingCode', 'text', array(
            'label'    => 'Accounting Code',
            'required' => false
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));
        $builder->add('idCurrency', 'choice', array(
            'label'    => 'Currency',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Currency_List')
        ));

        $builder->add('section1', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));

        $builder->add('sectionInvoiceAddress', 'section', array(
            'label'    => 'Invoice address',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('isInvoiceWithTax', 'checkbox', array(
            'label'    => 'Is invoice with tax?',
            'required' => false
        ));

        // Contact person
        $builder->add('sectionContactPerson', 'section', array(
            'label'    => 'Contact person',
            'property_path' => false
        ));
        $builder->add('contactPersonName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('contactPersonEmail', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('contactPersonTelephoneNumber', 'text', array(
            'label'    => 'Telephone number',
            'required' => false
        ));

        // Web Login
        $builder->add('sectionWebLogin', 'section', array(
            'label'    => 'Web Login'
        ));

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Web login username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'partner', 'partner', 'generateUsername'),
            'name_source_field_id' => 'partner'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Web login password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'partner', 'partner', 'generatePassword')
        ));

        $builder->add('webLoginStatus', 'choice', array(
            'label'    => 'Web login status',
            'choices'  => $mapping->getMapping('LeepAdmin_WebLogin_Status_List'),
            'required' => false
        ));

        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isWebLoginOptOut', 'checkbox', array(
            'label'    => 'Web Login email Opt-Out?',
            'required' => false
        ));

        $builder->add('canViewShop', 'checkbox', array(
            'label'    => 'Can view Shop',
            'required' => false
        ));

        $builder->add('isCanViewYourOrder', 'checkbox', array(
            'label'    => 'Can view [Your order]?',
            'required' => false
        ));
        $builder->add('isCanViewCustomerProtection', 'checkbox', array(
            'label'    => 'Can view [Customer protection]?',
            'required' => false
        ));
        $builder->add('isCanViewRegisterNewCustomer', 'checkbox', array(
            'label'    => 'Can view [Register new customer]?',
            'required' => false
        ));
        $builder->add('isCanViewPendingOrder', 'checkbox', array(
            'label'    => 'Can view [Pending order]?',
            'required' => false
        ));
        $builder->add('isCanViewSetting', 'checkbox', array(
            'label'    => 'Can view [Setting]?',
            'required' => false
        ));
        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));
        $builder->add('isNotAccessReport', 'checkbox', array(
            'label'    => "Can't access report?",
            'required' => false
        ));
        $builder->add('isNotSendEmail', 'checkbox', array(
            'label'    => "Can't send email?",
            'required' => false
        ));

        // Delivery
        $builder->add('section5', 'section', array(
            'label'    => 'Delivery',
            'property_path' => false
        ));
        $builder->add('statusDeliveryEmail', 'text', array(
            'label'    => 'Status Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryNote', 'choice', array(
            'label'    => 'Invoice Delivery Note',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Bill_DeliveyNoteTypes')
        ));
        $builder->add('idInvoicesGoesTo', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('idMarginPaymentMode', 'choice', array(
            'label'    => 'Margin payment mode',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginPaymentMode')
        ));
        $builder->add('isPaymentWithTax', 'checkbox', array(
            'label'    => 'Is payment with tax?',
            'required' => false
        ));
        $builder->add('paymentNoTaxText', 'choice', array(
            'label'    => 'Payment No Tax Text',
            'required' => true,
            'choices'  => Business\PaymentNoTaxText\Constant::get($this->container)
        ));
        $builder->add('invoiceNoTaxText', 'choice', array(
            'label'    => 'Invoice No Tax Text',
            'required' => true,
            'choices'  => Business\InvoiceNoTaxText\Constant::get($this->container)
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));
        $builder->add('commissionPaymentWithTax', 'text', array(
            'label'    => 'Commission payment with tax (%)',
            'required' => false
        ));

        $builder->add('section3', 'section', array(
            'label'    => 'Acquisiteur(s)',
            'property_path' => false
        ));
        $builder->add('acquisiteurId1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage1', 'text', array(
            'label'    => 'Yearly decay percentage 1',
            'required' => false
        ));
        $builder->add('acquisiteurId2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage2', 'text', array(
            'label'    => 'Yearly decay percentage 2',
            'required' => false
        ));
        $builder->add('acquisiteurId3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('commission3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));
        $builder->add('yearlyDecayPercentage3', 'text', array(
            'label'    => 'Yearly decay percentage 3',
            'required' => false
        ));

        $builder->add('section4', 'section', array(
            'label'    => 'Web User',
            'property_path' => false
        ));
        $builder->add('webUserId', 'choice', array(
            'label'    => 'Web User',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_WebUser_List')
        ));


        $builder->add('section2', 'section', array(
            'label'    => 'Bank Account Information',
            'property_path' => false
        ));
        $builder->add('accountName', 'text', array(
            'label'    => 'Account Name',
            'required' => false
        ));
        $builder->add('accountNumber', 'text', array(
            'label'    => 'Account Number',
            'required' => false
        ));
        $builder->add('bankCode', 'text', array(
            'label'    => 'Bank Code',
            'required' => false
        ));
        $builder->add('bic', 'text', array(
            'label'    => 'BIC',
            'required' => false
        ));
        $builder->add('iban', 'text', array(
            'label'    => 'IBAN',
            'required' => false
        ));
        $builder->add('otherNotes', 'textarea', array(
            'label'    => 'Other Notes',
            'required' => false,
            'attr'     => array(
                'rows'  => 10,  'cols' => 80
            )
        ));

        $builder->add('sectionOthers', 'section', array(
            'label'    => 'Others',
            'property_path' => false
        ));
        $builder->add('rating', 'choice', array(
            'label'    => 'Rating',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Rating_Levels')
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('isStopShipping', 'checkbox', array(
            'label'    => 'Stop shipping',
            'required' => false
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));
        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));
        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));
        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
        $builder->add('isGenerateFoodTableExcelVersion', 'checkbox', array(
            'label'    => 'Is generate food table excel version?',
            'required' => false
        ));

        $builder->add('idLanguage', 'choice', array(
            'label'    => 'Language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        if ($model->webLoginUsername) {
            $this->entity->setWebLoginUsername($model->webLoginUsername->username);
        } else {
            $this->entity->setWebLoginUsername(null);
        }

        if ($model->webLoginPassword) {
            $this->entity->setWebLoginPassword($model->webLoginPassword->password);
        } else {
            $this->entity->setWebLoginPassword(null);
        }
        if ($model->accountingCode < 230000000) {
            $this->errors += ['Accounting code must be than 230000000. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->accountingCode != $this->entity->getAccountingCode()) {
            // validation accounting code
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByAccountingCode($model->accountingCode);
            $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByAccountingCode($model->accountingCode);
            $acquisiteur = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findByAccountingCode($model->accountingCode);

            if ($dc || $partner || $acquisiteur) {
                // Validation
                $this->errors += ['Accounting code existed. Please try a diffrent code!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
        }
        $this->entity->setAccountingCode($model->accountingCode);

        /// validation id code
        if (strlen($model->idCode) != 5) {
            $this->errors += ['Id code must be 5 char. Please try a diffrent code!'];
            if (!empty($this->errors)) {
                return false;
            }
        }
        if ($model->idCode != $this->entity->getIdCode()) {
            // validation accounting code
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findByIdCode($model->idCode);
            $partner = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findByIdCode($model->idCode);
            if ($dc || $partner) {
                // Validation
                $this->errors += ['Id code existed. Please try a diffrent code!'];
                if (!empty($this->errors)) {
                    return false;
                }
            }
        }
        $this->entity->setIdCode($model->idCode);
        $this->entity->setWebLoginStatus($model->webLoginStatus);
        $this->entity->setWebLoginGoesTo($model->webLoginGoesTo);
        $this->entity->setIsWebLoginOptOut($model->isWebLoginOptOut);

        $this->entity->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $this->entity->setIsNutrimeOffered($model->isNutrimeOffered);
        $this->entity->setIsNotContacted($model->isNotContacted);
        $this->entity->setIsDestroySample($model->isDestroySample);
        $this->entity->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $this->entity->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);

        $this->entity->setDomain($model->domain);
        $this->entity->setPartner($model->partner);
        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setUidNumber($model->uidNumber);
        $this->entity->setGenderId($model->genderId);
        $this->entity->setTitle($model->title);
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setInstitution($model->institution);
        $this->entity->setIdCurrency($model->idCurrency);

        $this->entity->setStreet($model->street);
        $this->entity->setPostCode($model->postCode);
        $this->entity->setCity($model->city);
        $this->entity->setCountryId($model->countryId);
        $this->entity->setContactEmail($model->contactEmail);
        $this->entity->setTelephone($model->telephone);
        $this->entity->setFax($model->fax);

        $this->entity->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $this->entity->setInvoiceClientName($model->invoiceClientName);
        $this->entity->setInvoiceCompanyName($model->invoiceCompanyName);
        $this->entity->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $this->entity->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $this->entity->setInvoiceAddressCity($model->invoiceAddressCity);
        $this->entity->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $this->entity->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $this->entity->setInvoiceAddressFax($model->invoiceAddressFax);
        $this->entity->setIsInvoiceWithTax($model->isInvoiceWithTax);

        $this->entity->setContactPersonName($model->contactPersonName);
        $this->entity->setContactPersonEmail($model->contactPersonEmail);
        $this->entity->setContactPersonTelephoneNumber($model->contactPersonTelephoneNumber);

        $this->entity->setStatusDeliveryEmail($model->statusDeliveryEmail);
        $this->entity->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);
        $this->entity->setInvoiceDeliveryNote($model->invoiceDeliveryNote);
        $this->entity->setIdInvoicesGoesTo($model->idInvoicesGoesTo);
        $this->entity->setIdMarginGoesTo($model->idMarginGoesTo);
        $this->entity->setIdMarginPaymentMode($model->idMarginPaymentMode);
        $this->entity->setIsPaymentWithTax($model->isPaymentWithTax);
        $this->entity->setPaymentNoTaxText($model->paymentNoTaxText);
        $this->entity->setInvoiceNoTaxText($model->invoiceNoTaxText);
        $this->entity->setCommissionPaymentWithTax($model->commissionPaymentWithTax);
        $this->entity->setIdNutrimeGoesTo($model->idNutrimeGoesTo);

        $this->entity->setAcquisiteurId1($model->acquisiteurId1);
        $this->entity->setCommission1($model->commission1);
        $this->entity->setYearlyDecayPercentage1($model->yearlyDecayPercentage1);
        $this->entity->setAcquisiteurId2($model->acquisiteurId2);
        $this->entity->setCommission2($model->commission2);
        $this->entity->setYearlyDecayPercentage2($model->yearlyDecayPercentage2);
        $this->entity->setAcquisiteurId3($model->acquisiteurId3);
        $this->entity->setCommission3($model->commission3);
        $this->entity->setYearlyDecayPercentage3($model->yearlyDecayPercentage3);

        $this->entity->setWebUserId($model->webUserId);

        $this->entity->setAccountName($model->accountName);
        $this->entity->setAccountNumber($model->accountNumber);
        $this->entity->setBankCode($model->bankCode);
        $this->entity->setBIC($model->bic);
        $this->entity->setIBAN($model->iban);
        $this->entity->setOtherNotes($model->otherNotes);

        $this->entity->setIdPreferredPayment($model->idPreferredPayment);
        $this->entity->setIsStopShipping($model->isStopShipping);

        $this->entity->setIsCanViewYourOrder($model->isCanViewYourOrder);
        $this->entity->setIsCanViewCustomerProtection($model->isCanViewCustomerProtection);
        $this->entity->setIsCanViewRegisterNewCustomer($model->isCanViewRegisterNewCustomer);
        $this->entity->setIsCanViewPendingOrder($model->isCanViewPendingOrder);
        $this->entity->setIsCanViewSetting($model->isCanViewSetting);
        if ($model->isDelayDownloadReport == true) {
            $this->entity->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }
        else {
            $this->entity->setNumberDateDelayDownloadReport(null);
        }
        $this->entity->setIsDelayDownloadReport($model->isDelayDownloadReport);
        $this->entity->setIsNotAccessReport($model->isNotAccessReport);
        $this->entity->setIsNotSendEmail($model->isNotSendEmail);

        $selectedLanguage = null;
        if ($model->idLanguage) {
            $selectedLanguage = $model->idLanguage;
        }
        $this->entity->setIdLanguage($selectedLanguage);

        // $this->entity->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
        $this->entity->setRating($model->rating);

        $this->entity->setIdRecallGoesTo($model->idRecallGoesTo);

        parent::onSuccess();
    }
}
