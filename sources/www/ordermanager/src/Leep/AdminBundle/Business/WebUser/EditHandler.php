<?php
namespace Leep\AdminBundle\Business\WebUser;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUsers', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();  
        $model->username = $entity->getUsername();
        $model->password = $entity->getPassword();

        $model->permissions = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUserPermission')->findByIdWebUser($entity->getId());
        foreach ($result as $r) {
            $model->permissions[] = $r->getPermissionKey();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        
        $builder->add('username',            'label', array(
            'label' => 'Username',
            'required' => false
        ));
        $builder->add('password',            'text', array(
            'label' => 'Password',
            'required' => true
        ));
        
        $builder->add('permissions', 'choice', array(
            'label'    => 'Permissions',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 300px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_Permission')
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();   
        $model = $this->getForm()->getData();        
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->entity->setPassword($model->password);
        
        $filters = array('idWebUser' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:WebUserPermission', $filters);
        foreach ($model->permissions as $permission) {
            $webUserPermission = new Entity\WebUserPermission();
            $webUserPermission->setIdWebUser($this->entity->getId());
            $webUserPermission->setPermissionKey($permission);
            $em->persist($webUserPermission);
        }
        $em->flush();

        parent::onSuccess();
    }
}
