<?php
namespace Leep\AdminBundle\Business\WebUser;

class UserManager {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function encodePassword($password, $salt) {
        $factory = $this->container->get('security.encoder_factory');
        $userSession = new UserSession();
        $encoder = $factory->getEncoder($userSession);

        return $encoder->encodePassword($password, $salt);
    }

    public function generateSalt() {
        return uniqid(null, true);
    }

    public function checkPassword($password, $user) {
        $encodePassword = $this->encodePassword($password, $user->getSalt());
        if (strcmp($encodePassword, $user->getPassword()) != 0) {
            return false;
        }
        return true;
    }

    public function hasRole($role) {
        $userSession = $this->getUserSession();
        if ($userSession != null) {
            if (in_array($role, $userSession->getRoles())) {
                return true;
            }
        }
        return false;
    }

    public function getUserSession() {
        $token = $this->container->get('security.context')->getToken();
        if ($token != null)  {
            return $token->getUser();
        }
        return null;
    }

    public function getUser() {
        $userSession = $this->getUserSession();
        if ($userSession != null) {
            return $userSession->getUser();
        }
        return null;
    }
}
