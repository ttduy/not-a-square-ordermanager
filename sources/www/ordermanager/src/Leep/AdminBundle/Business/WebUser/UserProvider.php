<?php
namespace Leep\AdminBundle\Business\WebUser;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Leep\AdminBundle\Business;

class UserProvider implements UserProviderInterface
{
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function loadUserByUsername($username)
    {
        $user = null;
        $userSession = new UserSession();

        /////////////////////////////////////////////
        // x. Search web users
        $q = $this->container->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:WebUsers')
            ->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->andWhere('u.isdeleted = 0')
            ->getQuery();
        try {
            $user = $q->getSingleResult();
            $userSession->setUser($user);
            $userSession->setRole('ROLE_ADMIN');

            // Load permission
            $userSession->permissions = array();
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUserPermission')->findByIdWebUser($user->getId());
            foreach ($result as $r) {
                $userSession->permissions[$r->getPermissionKey()] = 1;
            }

            if (isset($userSession->permissions['webUserView'])) {
                $userSession->setCanSwitchUser(true);
            }

            return $userSession;
        } catch (\Exception $e) {
        }

        /////////////////////////////////////////////
        // x. Search translator
        $q = $this->container->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:TranslatorUser')
            ->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery();
        try {
            $user = $q->getSingleResult();
            $userSession->setUser($user);
            $userSession->setRole('ROLE_ADMIN');

            $userSession->permissions = array();
            $userSession->permissions['translatorView'] = 1;
            $userSession->permissions['translatorModify'] = 1;

            return $userSession;
        }
        catch (\Exception $e) {
        }

        /////////////////////////////////////////////
        // x. Search laboratory user
        $q = $this->container->get('doctrine')
            ->getRepository('AppDatabaseMainBundle:CryosaveUser')
            ->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery();
        try {
            $user = $q->getSingleResult();
            $userSession->setUser($user);
            $userSession->setRole('ROLE_ADMIN');

            $userSession->permissions = array();
            $userSession->permissions['cryosaveExternalLab'] = 1;
            $userSession->permissions['cryosaveSampleView'] = 1;
            $userSession->permissions['cryosaveSampleModify'] = 1;

            return $userSession;
        }
        catch (\Exception $e) {
        }

        throw new UsernameNotFoundException(sprintf('Unable to find an active admin AcmeUserBundle:User object identified by "%s".', $username), null, 0, $e);
    }

    public function refreshUser(UserInterface $userSession)
    {
        if (!$userSession instanceof UserSession) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($userSession)));
        }

        return $this->loadUserByUsername($userSession->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'AppCourierTracker\AdminBundle\Business\UserSession';
    }
}