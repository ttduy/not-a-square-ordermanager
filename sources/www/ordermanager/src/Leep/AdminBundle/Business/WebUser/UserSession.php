<?php
namespace Leep\AdminBundle\Business\WebUser;

use Symfony\Component\Security\Core\User\UserInterface;

class UserSession implements UserInterface {
    private $user;
    private $role;
    private $canSwitchUser = false;
    public $permissions = array();

    public function setRole($role) { $this->role = $role; }
    public function setUser($user) { $this->user = $user; }
    public function getUser() { return $this->user; }
    public function setCanSwitchUser($canSwitchUser) { $this->canSwitchUser = $canSwitchUser; }

    function getPassword() {
        return $this->user->getPassword();
    }

    function getSalt() {
        return '';//$this->user->getSalt();
    }

    function getUsername() {
        return $this->user->getUsername();
    }

    function getRoles() {
        $roles = array($this->role);
        if ($this->canSwitchUser) {
            $roles[] = 'ROLE_ALLOWED_TO_SWITCH';
        }
        return $roles;
    }

    function eraseCredentials() {

    }
    function equals(UserInterface $user) {
        return $this->getUsername() === $user->getUsername();
    }
}