<?php
namespace Leep\AdminBundle\Business\WebUser;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('username', 'text', array(
            'label'       => 'Username',
            'required'    => false
        ));

        return $builder;
    }
}