<?php
namespace Leep\AdminBundle\Business\WebUser;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;
    public $username;
    public $password;
    public $permissions;

    public function isValid(ExecutionContext $context) {  
        if ($this->container->get('leep_admin.helper.common')->isUsernameExisted($this->username)) {
            $context->addViolationAtSubPath('username', "Username is already existed");                    
        }
    }
}
