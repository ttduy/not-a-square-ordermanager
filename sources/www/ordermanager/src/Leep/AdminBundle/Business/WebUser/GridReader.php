<?php
namespace Leep\AdminBundle\Business\WebUser;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('username', 'password', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Username', 'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Password', 'width' => '35%', 'sortable' => 'false'),
            array('title' => 'Action',   'width' => '25%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:WebUsers', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        if (trim($this->filters->username) != '') {
            $queryBuilder->andWhere('p.username LIKE :username')
                ->setParameter('username', '%'.trim($this->filters->username).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();


        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('webUserModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'web_user', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'web_user', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
            $builder->addPopupButton('Copy', $mgr->getUrl('leep_admin', 'web_user', 'copy', 'create', array('id' => $row->getId())), 'button-copy');
        }

        return $builder->getHtml();
    }
}
