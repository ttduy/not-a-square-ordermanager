<?php
namespace Leep\AdminBundle\Business\WebUser;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends BaseCreateHandler {
    public $idWebUser = null;

    public function getDefaultFormModel() {
        $request = $this->container->get('request');
        $this->idWebUser = $request->get('id', null);

        $arrData = array();
        if ($this->idWebUser) {
            $webUser = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:WebUsers')->findOneById($this->idWebUser);

            $arrData['password'] = $webUser->getPassword();
        }

        return $arrData;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $webUser = new Entity\WebUsers();
        $webUser->setUsername($model['username']);
        $webUser->setPassword($model['password']);

        $em->persist($webUser);
        $em->flush();

        $queryPermission = $em->createQueryBuilder();
        $originalPermissions = $queryPermission->select('p')
                                                ->from('AppDatabaseMainBundle:WebUserPermission', 'p')
                                                ->andWhere('p.idWebUser = :idWebUserSource')
                                                ->setParameter('idWebUserSource', $this->idWebUser)
                                                ->getQuery()
                                                ->getResult();

        if ($originalPermissions) {
            foreach ($originalPermissions as $permission) {
                $webUserPermission = new Entity\WebUserPermission();
                $webUserPermission->setIdWebUser($webUser->getId());
                $webUserPermission->setPermissionKey($permission->getPermissionKey());
                $em->persist($webUserPermission);
            }
        }

        $em->flush();

        parent::onSuccess();
    }
}
