<?php
namespace Leep\AdminBundle\Business\WebUser;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->container = $this->container;
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('username', 'text', array(
            'label'    => 'Username',
            'required' => true
        ));
        $builder->add('password', 'text', array(
            'label'    => 'Password',
            'required' => true
        ));
        $builder->add('permissions', 'choice', array(
            'label'    => 'Permissions',
            'required' => false,
            'multiple' => 'multiple',
            'attr'     => array(
                'style'  => 'height: 300px'
            ),
            'choices'  => $mapping->getMapping('LeepAdmin_WebUser_Permission')
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $webUser = new Entity\WebUsers();
        $webUser->setUsername($model->username);
        $webUser->setPassword($model->password);

        $em->persist($webUser);
        $em->flush();

        foreach ($model->permissions as $permission) {
            $webUserPermission = new Entity\WebUserPermission();
            $webUserPermission->setIdWebUser($webUser->getId());
            $webUserPermission->setPermissionKey($permission);
            $em->persist($webUserPermission);
        }
        $em->flush();

        parent::onSuccess();
    }
}
