<?php
namespace Leep\AdminBundle\Business\Lead;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }

    public function getColumnMapping() {
        return array('name', 'company', 'idPartner', 'idDistributionChannel', 'idAcquisiteur', 'currentStatus', 'creationDate', 'reminderDate', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',          'width' => '18%', 'sortable' => 'true'),
            array('title' => 'Company',       'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Partner',       'width' => '8%', 'sortable' => 'true'),
            array('title' => 'DC',            'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Acquisiteur',   'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Status',        'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Creation date', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Reminder date', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Action',        'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_LeadGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Lead', 'p');
        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (!empty($this->filters->idType)) {
            $queryBuilder->andWhere('p.idType = :idType')
                ->setParameter('idType', $this->filters->idType);
        }
        if (!empty($this->filters->currentStatus)) {
            $queryBuilder->andWhere('p.currentStatus = :currentStatus')
                ->setParameter('currentStatus', $this->filters->currentStatus);
        }
        if (!empty($this->filters->status)) {
            $queryBuilder->andWhere('FIND_IN_SET('.$this->filters->status.', p.statusList) > 0');
        }

        if (!empty($this->filters->idPartner)) {
            $queryBuilder->andWhere('p.idPartner = :idPartner')
                ->setParameter('idPartner', $this->filters->idPartner);
        }

        if (!empty($this->filters->idAcquisiteur)) {
            $queryBuilder->andWhere('p.idAcquisiteur = :idAcquisiteur')
                ->setParameter('idAcquisiteur', $this->filters->idAcquisiteur);
        }

        if (!empty($this->filters->idDistributionChannel)) {
            $queryBuilder->andWhere('p.idDistributionChannel = :idDistributionChannel')
                ->setParameter('idDistributionChannel', $this->filters->idDistributionChannel);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('leadModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'lead', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'lead', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }
    public function buildCellCurrentStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Lead_Status', $row->getCurrentStatus());

        $age = $this->getAge($row->getCurrentStatusDate());
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }
}
