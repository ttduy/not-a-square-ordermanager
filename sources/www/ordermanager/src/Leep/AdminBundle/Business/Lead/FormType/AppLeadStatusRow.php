<?php
namespace Leep\AdminBundle\Business\Lead\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppLeadStatusRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Lead\FormType\AppLeadStatusRowModel',
            'typeMapping' => ''
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_lead_status_row';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('statusDate', 'datepicker', array(
            'required' => false
        ));
        $builder->add('status', 'choice', array(
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_Status'),
            'attr'     => array(
                'style'   => 'max-width: 150px; min-width: 150px'
            )
        ));
    }
}