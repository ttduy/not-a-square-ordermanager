<?php
namespace Leep\AdminBundle\Business\Lead;

class Constants {
    const REMINDER_IN_SPECIFIC_DATE     = 1;
    const REMINDER_IN_3_DAYS            = 2;
    const REMINDER_IN_1_WEEK            = 3;
    const REMINDER_IN_2_WEEKS           = 4;
    const REMINDER_IN_1_MONTH           = 5;
    const REMINDER_IN_3_MONTHS          = 6;
    const REMINDER_IN_6_MONTHS          = 7;
    const REMINDER_IN_1_YEAR            = 8;
    const REMINDER_IN_NO_REMINDER       = 9;

    public static function getReminderIn() {
        return array(
            self::REMINDER_IN_SPECIFIC_DATE    => 'Specific date',
            self::REMINDER_IN_3_DAYS           => '3 days',
            self::REMINDER_IN_1_WEEK           => '1 week',
            self::REMINDER_IN_2_WEEKS          => '2 weeks',
            self::REMINDER_IN_1_MONTH          => '1 month',
            self::REMINDER_IN_3_MONTHS         => '3 months',
            self::REMINDER_IN_6_MONTHS         => '6 months',
            self::REMINDER_IN_1_YEAR           => '1 year',
            self::REMINDER_IN_NO_REMINDER      => 'No reminder',
        );
    }

    public static function getDayMaps() {
        return array(
            self::REMINDER_IN_3_DAYS    => 3,
            self::REMINDER_IN_1_WEEK    => 7,
            self::REMINDER_IN_2_WEEKS   => 14,
            self::REMINDER_IN_1_MONTH   => 30,
            self::REMINDER_IN_3_MONTHS  => 90,
            self::REMINDER_IN_6_MONTHS  => 180,
            self::REMINDER_IN_1_YEAR    => 365
        );
    }
}

