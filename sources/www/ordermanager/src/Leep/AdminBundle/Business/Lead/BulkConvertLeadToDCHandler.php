<?php
namespace Leep\AdminBundle\Business\Lead;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\DistributionChannel\Utils as DCUtils;

class BulkConvertLeadToDCHandler extends AppEditHandler {
    public $arrLeads = array();

    public function loadEntity($request) {
        return false;
    }

    public function convertToFormModel($entity) { 
        $em = $this->container->get('doctrine')->getEntityManager();
        $selectedIdRaw = $this->container->get('request')->get('id', '');

        $tmp = explode(",", $selectedIdRaw);
        $arr = array();
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        $modelData = array();
        if (sizeOf($arr) > 0) {
            // find some useful info for each entry
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:Lead', 'p')
                ->andWhere('p.id in (:ids)')
                ->setParameter('ids', $arr);
            $result = $query->getQuery()->getResult();
            foreach ($result as $entry) {
                $modelData['leadInfoSection_' . $entry->getId()] = null;
                $modelData['targetDC_' . $entry->getId()] = null;

                $this->arrLeads[$entry->getId()] = array(
                                                        'id'   => $entry->getId(),
                                                        'info' => $entry->getName()
                                                    );


            }
        }

        $modelData['selectedId'] = implode(',', array_keys($this->arrLeads));

        return $modelData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        foreach ($this->arrLeads as $key => $entry) {
            $builder->add('leadInfoSection_' . $key, 'section', array(
                'label'    => 'Lead ' . $entry['info']
            ));

            $builder->add('targetDC_' . $key, 'searchable_box', array(
                'label'     => 'Distribution Channel',
                'required'  => true,
                'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List')
            ));
        }

        $builder->add('selectedId', 'hidden');

        return $builder;
    }

    public function onSuccess() {
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');
        $mgr = $this->container->get('easy_module.manager');
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $doctrine = $this->container->get('doctrine');

        // get selected entries
        $arrOfEntries = explode(',', $model['selectedId']);

        $errors = array();

        $arrMsgNewDCs = array();
        $arrLeadHasNoDC = array();
        $arrLeadHasSimilarDC = array();
        $arrToBeRemovedLeads = array();

        if (sizeof($arrOfEntries) > 0) {
            $query = $em->createQueryBuilder();
            $result = $query->select('p')
                            ->from('AppDatabaseMainBundle:Lead', 'p')
                            ->andWhere('p.id in (:ids)')
                            ->setParameter('ids', $arrOfEntries)
                            ->getQuery()
                            ->getResult();

            if ($result) {
                foreach ($result as $lead) {
                    $targetDC = 'targetDC_' . $lead->getId();
                    if ($model[$targetDC]) {
                        $dc = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($model[$targetDC]);
                        if ($dc) {
                            // check similar DC  
                            if (!$this->hasSimilarDC($lead)) {
                                $newDC = $this->buildNewDC($dc, $lead);
                                $em->persist($newDC);
                                $em->flush();

                                // mark to be removed Leads
                                $arrToBeRemovedLeads[] = $lead->getId();

                                // message for accessing edition page
                                $msgTpl = "> %s - <a href='%s' target='_blank'>EDIT</a>";
                                $arrMsgNewDCs[] = sprintf($msgTpl, 
                                                                $newDC->getDistributionchannel(), 
                                                                $mgr->getUrl('leep_admin', 'distribution_channel', 'edit', 'edit', array('id' => $newDC->getId()))
                                                        );
                            } else {
                                $arrLeadHasSimilarDC[] = '> ' . $lead->getName();
                            }
                        } else {
                            // not found required DC
                            $arrLeadHasNoDC[] = '> ' . $lead->getName();
                        }
                    }

                }
            }
        }

        // remove Leads
        if (!empty($arrToBeRemovedLeads)) {
            $queryDelLead = $em->createQueryBuilder();
            $queryDelLead->delete('AppDatabaseMainBundle:Lead', 'p')
                        ->andWhere('p.id IN (:ids)')
                        ->setParameter('ids', $arrToBeRemovedLeads)
                        ->getQuery()
                        ->execute();
        }

        $msg = array('No DC is created');
        if (sizeOf($arrMsgNewDCs) > 0) {
            $msg = array();
            $msg[] = sprintf('Created %s new Distribution Channel(s) successfully<br/>%s', 
                                                sizeof($arrMsgNewDCs),
                                                implode('<br/>', $arrMsgNewDCs)
                                            );
        }

        if (sizeOf($arrLeadHasNoDC) > 0) {
            $msg[] = sprintf('%s Lead has not been specified target Distribution Channel!<br/>%s', 
                                                sizeof($arrLeadHasNoDC),
                                                implode('<br/>', $arrLeadHasNoDC)
                                            );
        }

        if (sizeOf($arrLeadHasSimilarDC) > 0) {
            $msg[] = sprintf('%s Lead has similar name to target Distribution Channel!<br/>%s', 
                                    sizeof($arrLeadHasSimilarDC),
                                    implode('<br/>', $arrLeadHasSimilarDC)
                            );
        }

        $this->messages = $msg;
    }

    private function hasSimilarDC($lead) {
        $result = false;

        // lead vs dc
        $arrMapping = array(
                            'name'  => 'distributionchannel'
                        );

        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('count(p.id)')
                    ->from('AppDatabaseMainBundle:DistributionChannels', 'p');

        foreach($arrMapping as $leadProperty => $dcProperty) {
            $leadGetPropertyMethod = 'get' . ucfirst($leadProperty);
            $query->andWhere('p.' . $dcProperty . ' = :' . $leadProperty)
                    ->setParameter($leadProperty, $lead->$leadGetPropertyMethod());
        }

        $total = $query->getQuery()
                    ->getSingleScalarResult();

        if ($total > 0) {
            $result = true;
        }

        return $result;
    }

    private function buildNewDC($dc, $lead) {
        // copy DC
        $newDC = DCUtils::copyDC($this->container, $dc);

        // overwrite by Lead info
        $newDC->setDistributionchannel($lead->getName());
        $newDC->setUidnumber(($lead->getUidNumber() ? $lead->getUidNumber() : $newDC->getUidnumber()));
        $newDC->setTypeid(($lead->getIdType() ? $lead->getIdType() : $newDC->getTypeid()));
        $newDC->setGenderid(($lead->getIdGender() ? $lead->getIdGender() : $newDC->getGenderid()));
        $newDC->setTitle(($lead->getTitle() ? $lead->getTitle() : $newDC->getTitle()));
        $newDC->setFirstname(($lead->getFirstName() ? $lead->getFirstName() : $newDC->getFirstname()));
        $newDC->setSurname(($lead->getSurName() ? $lead->getSurName() : $newDC->getSurname()));
        $newDC->setInstitution(($lead->getInstitution() ? $lead->getInstitution() : $newDC->getInstitution()));
        $newDC->setStreet(($lead->getStreet() ? $lead->getStreet() : $newDC->getStreet()));
        $newDC->setPostcode(($lead->getPostcode() ? $lead->getPostcode() : $newDC->getPostcode()));
        $newDC->setCity(($lead->getCity() ? $lead->getCity() : $newDC->getCity()));
        $newDC->setCountryId(($lead->getIdCountry() ? $lead->getIdCountry() : $newDC->getCountryId()));
        $newDC->setContactemail(($lead->getContactEmail() ? $lead->getContactEmail() : $newDC->getContactemail()));
        $newDC->setTelephone(($lead->getTelephone() ? $lead->getTelephone() : $newDC->getTelephone()));
        $newDC->setAcquisiteurid1(($lead->getIdAcquisiteur() ? $lead->getIdAcquisiteur() : $newDC->getAcquisiteurid1()));
        $newDC->setPartnerid(($lead->getIdPartner() ? $lead->getIdPartner() : $newDC->getPartnerId()));
        $newDC->setCompanyinfo(($lead->getCompany() ? $lead->getCompany() : $newDC->getCompanyInfo()));

        return $newDC;
    }
}