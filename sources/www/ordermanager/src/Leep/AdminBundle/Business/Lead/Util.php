<?php
namespace Leep\AdminBundle\Business\Lead;

class Util {
    public static function updateStatusList($container, $idLead) {
        $em = $container->get('doctrine')->getEntityManager();
        $status = array();
        $results = $em->getRepository('AppDatabaseMainBundle:LeadStatus')->findByIdLead($idLead);
        foreach ($results as $r) {
            $status[] = $r->getStatus();
        }

        $lead = $em->getRepository('AppDatabaseMainBundle:Lead')->findOneById($idLead);   
        $lead->setStatusList(implode(',', $status));
        $em->persist($lead);
        $em->flush();
    }

    public static function updateReminderDate($container, $lead, $idReminderIn, $reminderDate) {
        if ($idReminderIn == Constants::REMINDER_IN_SPECIFIC_DATE) {
            $lead->setReminderDate($reminderDate);
        }
        else {
            $date = $lead->getCreationDate();
            if (empty($date) || $idReminderIn == Constants::REMINDER_IN_NO_REMINDER || !isset($dayMap[$idReminderIn])) {
                $lead->setReminderDate(null);
            }
            else {
                $timestamp = $date->getTimestamp();
                $dayMap = Constants::getDayMaps();
                $numDays = $dayMap[$idReminderIn];
                $timestamp += 24*60*60*$numDays;
                $date->setTimestamp($timestamp);
                $lead->setReminderDate($date);
            }
        }

        $em = $container->get('doctrine')->getEntityManager();
        $em->persist($lead);
        $em->flush();
    }   
}