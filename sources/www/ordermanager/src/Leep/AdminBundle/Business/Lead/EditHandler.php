<?php
namespace Leep\AdminBundle\Business\Lead;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Lead', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($lead) {
        $doctrine = $this->container->get('doctrine');

        $model = new EditModel();  
        $model->creationDate = $lead->getCreationDate();
        $model->name = $lead->getName();
        $model->uidNumber = $lead->getUidNumber();
        $model->idReminderIn = $lead->getIdReminderIn();
        $model->reminderDate = $lead->getReminderDate();
        $model->source = $lead->getSource();
        $model->company = $lead->getCompany();
        $model->idPartner = $lead->getIdPartner();
        $model->idDistributionChannel = $lead->getIdDistributionChannel();
        $model->idAcquisiteur = $lead->getIdAcquisiteur();
        
        $model->idType = $lead->getIdType();
        $model->idGender = $lead->getIdGender();
        $model->title = $lead->getTitle();
        $model->firstName = $lead->getFirstName();
        $model->surName = $lead->getSurName();
        $model->institution = $lead->getInstitution();
        
        $model->street = $lead->getStreet();
        $model->postcode = $lead->getPostcode();
        $model->city = $lead->getCity();
        $model->idCountry = $lead->getIdCountry();
        $model->contactEmail = $lead->getContactEmail();
        $model->telephone = $lead->getTelephone();

        $model->category = $lead->getIdCategory();

        $model->statusList = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:LeadStatus')->findByIdLead($lead->getId(), array('sortOrder' => 'ASC'));
        foreach ($result as $status) {
            $statusEntry = new FormType\AppLeadStatusRowModel();
            $statusEntry->status = $status->getStatus();
            $statusEntry->statusDate = $status->getStatusDate();

            $model->statusList[] = $statusEntry;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('idPartner', 'choice', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Partner_List')
        ));
        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('idAcquisiteur', 'choice', array(
            'label'    => 'Acquisiteur',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_Type')
        ));

        $builder->add('category', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_Type')
        ));

        $builder->add('idReminderIn', 'choice', array(
            'label'    => 'Reminder in',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_ReminderIn')
        ));
        $builder->add('reminderDate', 'datepicker', array(
            'label'    => 'Reminder date',
            'required' => false
        ));
        $builder->add('company', 'text', array(
            'label'    => 'Company',
            'required' => false
        ));
        $builder->add('source', 'textarea', array(
            'label'    => 'Source/Info',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));

        // Basic information
        $builder->add('sectionBasicInformation', 'section', array(
            'label'    => 'Basic information',
            'property_path' => false
        ));
        $builder->add('idGender', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));

        // Street
        $builder->add('sectionStreet', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postcode', 'text', array(
            'label'    => 'Post code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));

        //edit lead category --------------------------------------
        $builder->add('idCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        //---------------------------------------------------------

        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));

        // Status list
        $builder->add('sectionStatusList', 'section', array(
            'label'    => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppLeadStatusRow($this->container)
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('idLead' => $this->entity->getId());

        $this->entity->setCreationDate($model->creationDate);
        $this->entity->setName($model->name);
        $this->entity->setUidNumber($model->uidNumber);
        $this->entity->setIdType($model->idType);
        $this->entity->setIdReminderIn($model->idReminderIn);
        $this->entity->setSource($model->source);
        $this->entity->setCompany($model->company);
        $this->entity->setIdPartner($model->idPartner);
        $this->entity->setIdDistributionChannel($model->idDistributionChannel);
        $this->entity->setIdAcquisiteur($model->idAcquisiteur);

        $this->entity->setIdGender($model->idGender);
        $this->entity->setTitle($model->title);
        $this->entity->setFirstName($model->firstName);
        $this->entity->setSurName($model->surName);
        $this->entity->setInstitution($model->institution);

        $this->entity->setStreet($model->street);
        $this->entity->setPostcode($model->postcode);
        $this->entity->setCity($model->city);
        $this->entity->setIdCountry($model->idCountry);
        $this->entity->setContactEmail($model->contactEmail);
        $this->entity->setTelephone($model->telephone);

        //onSuccess for category---------------------------
        $this->entity->setIdCategory($model->category);
        //-------------------------------------------------

        // Status
        $dbHelper->delete($em, 'AppDatabaseMainBundle:LeadStatus', $filters);
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\LeadStatus();
            $status->setIdLead($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
            
            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $this->entity->setCurrentStatus($currentStatus);
        $this->entity->setCurrentStatusDate($currentStatusDate);
        $em->flush();

        Util::updateReminderDate($this->container, $this->entity, $model->idReminderIn, $model->reminderDate);
        Util::updateStatusList($this->container, $this->entity->getId());

        parent::onSuccess();
    }
}
