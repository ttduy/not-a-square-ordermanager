<?php
namespace Leep\AdminBundle\Business\Lead;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
// use Symfony\Component\Form\FormError;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        $model->creationDate = new \DateTime();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('creationDate', 'datepicker', array(
            'label'    => 'Creation date',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true          //change from false
        ));
        $builder->add('idPartner', 'choice', array(
            'label'    => 'Partner',
            'required' => false,                //change from false
            'choices'  => $mapping->getMapping('LeepAdmin_Partner_List')
        ));
        $builder->add('idDistributionChannel', 'choice', array(
            'label'    => 'Distribution Channel',
            'required' => false,                //change from false
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('idAcquisiteur', 'choice', array(
            'label'    => 'Acquisiteur',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('uidNumber', 'text', array(
            'label'    => 'UID Number',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_Type')
        ));

        $builder->add('category', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_Type'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeCategoryId()'
            )
        ));
        
        $builder->add('idReminderIn', 'choice', array(
            'label'    => 'Reminder in',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Lead_ReminderIn')
        ));
        $builder->add('reminderDate', 'datepicker', array(
            'label'    => 'Reminder date',
            'required' => false
        ));
        $builder->add('company', 'text', array(
            'label'    => 'Company',
            'required' => false
        ));
        $builder->add('source', 'textarea', array(
            'label'    => 'Source/Info',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));

        // Basic information
        $builder->add('sectionBasicInformation', 'section', array(
            'label'    => 'Basic information',
            'property_path' => false
        ));
        $builder->add('idGender', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List')
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('institution', 'text', array(
            'label'    => 'Institution',
            'required' => false
        ));

        // Street
        $builder->add('sectionStreet', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('postcode', 'text', array(
            'label'    => 'Post code',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('idCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('contactEmail', 'text', array(
            'label'    => 'Contact email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));

        // Status list
        $builder->add('sectionStatusList', 'section', array(
            'label'    => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'label'    => 'Status',
            'required' => false,
            'type'     => new FormType\AppLeadStatusRowTarget($this->container)
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $lead = new Entity\Lead();
        $lead->setCreationDate($model->creationDate);
        $lead->setName($model->name);
        $lead->setUidNumber($model->uidNumber);
        $lead->setIdType($model->idType);
        $lead->setIdReminderIn($model->idReminderIn);
        $lead->setSource($model->source);
        $lead->setCompany($model->company);
        $lead->setIdPartner($model->idPartner);
        $lead->setIdDistributionChannel($model->idDistributionChannel);
        $lead->setIdAcquisiteur($model->idAcquisiteur);

        $lead->setIdGender($model->idGender);
        $lead->setTitle($model->title);
        $lead->setFirstName($model->firstName);
        $lead->setSurName($model->surName);
        $lead->setInstitution($model->institution);

        $lead->setStreet($model->street);
        $lead->setPostcode($model->postcode);
        $lead->setCity($model->city);
        $lead->setIdCountry($model->idCountry);
        $lead->setContactEmail($model->contactEmail);
        $lead->setTelephone($model->telephone);

        //save category in database
        $lead->setIdCategory($model->category);
        //

        $em->persist($lead);
        $em->flush();

        // Status
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\LeadStatus();
            $status->setIdLead($lead->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setSortOrder($sortOrder++);

            $interval = $statusRow->statusDate->diff($model->creationDate);
            $dateDiff = $interval->format('%R%a');
            // if ($dateDiff < 0) {
            $dateDiff = -($dateDiff);
            // }

            // print_r($dateDiff);
            // die;

            $status->setDateDiff($dateDiff);
            
            $em->persist($status);
            
            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $lead->setCurrentStatus($currentStatus);
        $lead->setCurrentStatusDate($currentStatusDate);
        $em->flush();

        Util::updateReminderDate($this->container, $lead, $model->idReminderIn, $model->reminderDate);
        Util::updateStatusList($this->container, $lead->getId());

        parent::onSuccess();
    }
}