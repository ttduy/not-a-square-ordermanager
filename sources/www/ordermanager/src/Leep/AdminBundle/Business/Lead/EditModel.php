<?php
namespace Leep\AdminBundle\Business\Lead;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $company;
    public $idPartner;
    public $idDistributionChannel;
    public $idAcquisiteur;

    public $creationDate;
    public $name;
    public $uidNumber;
    public $idReminderIn;
    public $reminderDate;
    public $source;

    public $idType;
    public $idGender;
    public $title;
    public $firstName;
    public $surName;
    public $institution;

    public $street;
    public $postcode;
    public $city;
    public $idCountry;
    public $contactEmail;
    public $telephone;

    public $statusList;

    public $category;
}
