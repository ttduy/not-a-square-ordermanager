<?php
namespace Leep\AdminBundle\Business\Lead;

use App\Database\MainBundle\Entity;
use Easy\ModuleBundle\Module\AbstractHook;

use Leep\AdminBundle\Business;

class CreateHook extends AbstractHook {

    public function handle($controller, &$data, $options = array()) {
		$leadTypes = $this->getLeadTypes($controller);
		
		$targetChoices = $this->getStatusList($controller);
		$data['myData'] = json_encode(['data'	=> $targetChoices]);
    }

    public function getLeadTypes($container) {
        $mapping = $container->get('easy_mapping');
        $leadTypes = $mapping->getMapping('LeepAdmin_Lead_Type');
        return array_keys($leadTypes);
    }

    public function getStatusList($controller){

		$em = $controller->get('doctrine')->getEntityManager();

		$query = $em->createQueryBuilder();
		$query->select('q.id as LSVid, q.name, p.idLeadCategory')
				->from('AppDatabaseMainBundle:LeadCategoryStatus', 'p')
				->innerJoin('AppDatabaseMainBundle:LeadType', 'lt', 'WITH', 'p.idLeadCategory = lt.id')
				->leftJoin("AppDatabaseMainBundle:LeadStatusValue", 'q', 'WITH', 'p.idLeadStatus = q.id');

		$logs = $query->getQuery()->getResult();

		$returnRes = [];
		foreach ($logs as $log) {
			if (!isset($log['idLeadCategory'])) {
				$returnRes[$log['idLeadCategory']] = [];
			}
			$returnRes[$log['idLeadCategory']][$log['LSVid']] = $log['name'];
		}

		return $returnRes;
	}
}

