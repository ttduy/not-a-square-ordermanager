<?php
namespace Leep\AdminBundle\Business\Lead;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Base\AppFilterHandler;
use Leep\AdminBundle\Business\Partner;
use Leep\AdminBundle\Business\Acquisiteur;
use Leep\AdminBundle\Business\DistributionChannel;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_Type'),
            'empty_value' => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Lead_Status'),
            'empty_value' => false
        ));
        $builder->add('currentStatus', 'choice', array(
            'label'    => 'Current status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Lead_Status'),
            'empty_value' => false
        ));

        $builder->add('idPartner', 'searchable_box', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => [0 => 'ALL'] + Business\Partner\Utils::getPartnerList($this->container),
            'empty_value' => false
        ));

        $builder->add('idAcquisiteur', 'searchable_box', array(
            'label'     => 'Acquisiteur',
            'required'  => false,
            'choices'   => [0 => 'ALL'] + Business\Acquisiteur\Utils::getAcquisiteurList($this->container),
            'empty_value' => false
        ));

        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   =>[0 => 'ALL'] + Business\DistributionChannel\Utils::getDistributionChannelsList($this->container),
            'empty_value' => false
        ));

        return $builder;
    }
}
