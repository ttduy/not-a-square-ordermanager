<?php
namespace Leep\AdminBundle\Business\Lead;

use Easy\ModuleBundle\Module\AbstractHook;

class GridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
		$data['bulkConvertLeadToDC'] = $mgr->getUrl('leep_admin', 'lead', 'bulk_convert_lead_to_dc', 'edit');
    }
}
