<?php
namespace Leep\AdminBundle\Business\ReportQueue;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTimestamp = 0;
    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTimestamp = $today->getTimestamp();
    }
    public function getColumnMapping() {
        return array('id', 'inputTime', 'idWebUser', 'description', 'status',  'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'ID',            'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Input time',    'width' => '10%', 'sortable' => 'false'),
            array('title' => 'User',          'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Description',   'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Status',        'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',        'width' => '10%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ReportQueueGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p');

        if ($this->filters->idWebUser != 0) {
            $queryBuilder->andWhere('p.idWebUser = :idWebUser')
                ->setParameter('idWebUser', $this->filters->idWebUser);
        }
        if ($this->filters->status != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $this->filters->status);
        }
        if (trim($this->filters->description) != '') {
            $queryBuilder->andWhere('p.description LIKE :description')
                ->setParameter('description', '%'.trim($this->filters->description).'%');
        }
    }

    public function buildCellStatus($row) {
        $mapping = $this->container->get('easy_mapping');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addNewTabButton('Detail', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewLog', array('id' => $row->getId())), 'button-detail');

        $status = $mapping->getMappingTitle('LeepAdmin_ReportQueue_Status', $row->getStatus());
        if ($row->getStatus() == Constant::REPORT_QUEUE_STATUS_ERROR) {
            $status = '<span style="color:red; font-weight:bold">'.$status.'</span>';
        } else if ($row->getStatus() == Constant::REPORT_QUEUE_STATUS_PROCESSING || $row->getStatus() == Constant::REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD) {
            $status .= ' ('.number_format($row->getProgress(), 2).'%) ';
        }

        $output = @unserialize($row->getOutput());
        if (isset($output['filename_low_resolution'])) {
            $builder->addNewTabButton('Download', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewResult', array('id' => $row->getId(), 'isLowResolution' => 1)), 'button-download-gray');
        }
        if (isset($output['filename_food_table_excel'])) {
            $builder->addNewTabButton('Download', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewResult', array('id' => $row->getId(), 'isExcel' => 1)), 'button-excel');
        }
        if (isset($output['filename_food_table_excel_2'])) {
            $builder->addNewTabButton('Download', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewResult', array('id' => $row->getId(), 'isExcel2' => 1)), 'button-excel');
        }
        if (isset($output['filename_report_xml'])) {
            $builder->addNewTabButton('Download', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewResult', array('id' => $row->getId(), 'isXml' => 1)), 'button-export');
        }

        if ($row->getStatus() == Constant::REPORT_QUEUE_STATUS_FINISHED || $row->getStatus() == Constant::REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD) {
            $builder->addNewTabButton('Download', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'viewResult', array('id' => $row->getId())), 'button-down');
        }
        $status .= '&nbsp;&nbsp;'.$builder->getHtml();

        // Last updated
        if ($row->getStatus() == Constant::REPORT_QUEUE_STATUS_PROCESSING) {
            $lastUpdated = $row->getLastUpdated();
            if (!empty($lastUpdated)) {
                $timeElapse = $this->todayTimestamp - $lastUpdated->getTimestamp();

                $status .= "<br/>Last update: ".$timeElapse.'s ago';
            }
        }

        // Total time
        if ($row->getStatus() == Constant::REPORT_QUEUE_STATUS_FINISHED) {
            $startTime = $row->getStartTime();
            $endTime = $row->getEndTime();
            if (!empty($startTime) && !empty($endTime)) {
                $totalTime = $endTime->getTimestamp() - $startTime->getTimestamp();
                $totalMin = intval($totalTime/60);
                $totalSec = $totalTime - $totalMin*60;
                $status .= "<br/>Total time: ".$totalMin.'m'.$totalSec;
            }
        }

        // FTP Upload
        if ($row->getIdJobType() == Constant::JOB_TYPE_BUILD_SHIPMENT) {
            if ($row->getFtpUploadStatus() != 0) {
                $ftpUploadStatus = $mapping->getMappingTitle('LeepAdmin_ReportQueue_FtpUploadStatus', $row->getFtpUploadStatus());
                if ($row->getFtpUploadStatus() == Constant::FTP_UPLOAD_STATUS_ERROR) {
                    $status .= '</br><span style="color:red; font-weight:bold">FTP Upload: '.$ftpUploadStatus.'</span>';
                } else {
                    $status .= '</br>FTP Upload: '.$ftpUploadStatus;
                }
            }
        }

        if ($row->getIsWarning()) {
            $status .= '<br/><text style="color: orange">Warning!</text>';
        }

        return $status;
    }


    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        if ($row->getIdPriority() == 1) {
            $builder->addButton('MarkUnimportant', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'unmarkImportant', array('id' => $row->getId())), 'button-green-flag');
        }
        else {
            $builder->addButton('MarkImportant', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'markImportant', array('id' => $row->getId())), 'button-red-flag');
        }

        $builder->addButton('Reset', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'reset', array('id' => $row->getId())), 'button-redo');
        $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'report_queue', 'feature', 'delete', array('id' => $row->getId())), 'button-delete', 'Are you really want to delete this job?');

        return $builder->getHtml();
    }

    public function buildCellDescription($row) {
        $mapping = $this->container->get('easy_mapping');
        $description = '';
        $jobType = $mapping->getMappingTitle('LeepAdmin_ReportQueue_JobType', $row->getIdJobType());

        $description = '<b>'.$jobType."</b><br/>".$row->getDescription();
        if ($row->getIdPriority() == 1) {
            $description .= '<br/><b style="color:red">Important</b>';
        }
        return $description;
    }
}