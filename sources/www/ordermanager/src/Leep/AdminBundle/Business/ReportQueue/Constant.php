<?php
namespace Leep\AdminBundle\Business\ReportQueue;

class Constant {
    const REPORT_QUEUE_STATUS_PENDING    = 1;
    const REPORT_QUEUE_STATUS_PROCESSING = 2;
    const REPORT_QUEUE_STATUS_FINISHED   = 3;
    const REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD = 4;
    const REPORT_QUEUE_STATUS_ERROR      = 99;
    public static function getReportQueueStatus() {
        return array(
            self::REPORT_QUEUE_STATUS_PENDING     => 'Pending',
            self::REPORT_QUEUE_STATUS_PROCESSING  => 'Processing',
            self::REPORT_QUEUE_STATUS_FINISHED    => 'Finished',
            self::REPORT_QUEUE_STATUS_READY_FOR_DOWNLOAD => 'Ready',
            self::REPORT_QUEUE_STATUS_ERROR       => 'Error'
        );
    }

    const JOB_TYPE_BUILD_DEMO_REPORT           = 1;
    const JOB_TYPE_BUILD_CUSTOMER_REPORT       = 2;
    const JOB_TYPE_BUILD_SHIPMENT              = 3;
    const JOB_TYPE_BUILD_CRYOSAVE_REPORT       = 4;
    const JOB_TYPE_BUILD_DISTRIBUTION_CHANNEL_PRICE    = 5;
    public static function getReportQueueJobType() {
        return array(
            self::JOB_TYPE_BUILD_DEMO_REPORT      => 'Build demo report',
            self::JOB_TYPE_BUILD_CUSTOMER_REPORT  => 'Build customer report',
            self::JOB_TYPE_BUILD_SHIPMENT         => 'Build shipment',
            self::JOB_TYPE_BUILD_CRYOSAVE_REPORT  => 'Build cryosave report',
            self::JOB_TYPE_BUILD_DISTRIBUTION_CHANNEL_PRICE  => 'Build distribution channel price report'
        );
    }

    const FTP_UPLOAD_STATUS_PENDING       = 1;
    const FTP_UPLOAD_STATUS_INPROGRESS    = 2;
    const FTP_UPLOAD_STATUS_DONE          = 3;
    const FTP_UPLOAD_STATUS_ERROR         = 99;
    public static function getFtpUploadStatus() {
        return array(
            self::FTP_UPLOAD_STATUS_PENDING        => 'Pending',
            self::FTP_UPLOAD_STATUS_INPROGRESS     => 'In progress',
            self::FTP_UPLOAD_STATUS_DONE           => 'Done',
            self::FTP_UPLOAD_STATUS_ERROR          => 'Error',
        );
    }
}
