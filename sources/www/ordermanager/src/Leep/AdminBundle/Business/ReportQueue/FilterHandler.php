<?php
namespace Leep\AdminBundle\Business\ReportQueue;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');

        $model = new FilterModel();
        $model->idWebUser = $userManager->getUser()->getId();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 
        
        $builder->add('idWebUser', 'choice', array(
            'label'    => 'Web User',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_WebUser_List'),
            'empty_value' => false
        ));
        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_ReportQueue_Status'),
            'empty_value' => false
        ));
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => false
        ));
        return $builder;
    }
}