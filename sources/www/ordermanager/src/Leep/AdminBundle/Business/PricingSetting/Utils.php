<?php
namespace Leep\AdminBundle\Business\PricingSetting;

class Utils {
    public static function getPricingConfig($container) {
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $pricingConfig = trim($config->getConfigCustomerPricing());
        if(empty($pricingConfig)) {
            return (object) [
                'status' => [],
                'email' => '',
                'idTemplate' => 0,
                'emailMethod' => 0
            ];
        }

        return json_decode($pricingConfig);
    }

    public static function savePricingConfig($container, $pricingConfig) {
        $em = $container->get('doctrine')->getEntityManager();
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $config->setConfigCustomerPricing(json_encode($pricingConfig));
        $em->persist($config);
        $em->flush();
    }

    public static function isReadyForPricing($container, $customer) {
        $config = self::getPricingConfig($container);
        $status = $customer->getStatus();
        return in_array($status, $config->status);
    }
}