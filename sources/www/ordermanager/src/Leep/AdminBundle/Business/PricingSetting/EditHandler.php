<?php
namespace Leep\AdminBundle\Business\PricingSetting;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        return Utils::getPricingConfig($this->container);
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('status', 'choice', array(
            'label' =>      'Status',
            'required' =>   false,
            'multiple' =>   'multiple',
            'attr' =>       ['style' => 'height: 500px'],
            'choices' =>    $mapping->getMapping('LeepAdmin_Customer_Status'),
        ));

        $builder->add('email', 'text', array(
            'label' =>      'Email',
            'required' =>   false,
            'attr' =>       ['style' => 'width: 40%']
        ));

        $builder->add('idTemplate', 'choice', array(
            'label' =>      'Email Template',
            'required' =>   true,
            'choices' =>    $mapping->getMapping('LeepAdmin_EmailTemplate_List'),
        ));

        $builder->add('emailMethod', 'choice', array(
            'label' =>      'Email Method',
            'required' =>   true,
            'choices' =>    $mapping->getMapping('LeepAdmin_EmailMethod_List'),
        ));
    }

    public function onSuccess() {
        Utils::savePricingConfig($this->container, $this->getForm()->getData());
        $this->messages = ["SUCCESS"];
    }
}
