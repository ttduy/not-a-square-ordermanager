<?php
namespace Leep\AdminBundle\Business\ScanFile;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'timestamp', 'file', 'status', 'idOrder', 'action');
    } 
    
    public function getTableHeader() {
        return array(
            array('title' => 'Id',                  'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Creation Time',       'width' => '20%', 'sortable' => 'false'),
            array('title' => 'File',                'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status',              'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Order',               'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Action',              'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ScanFileGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFile', 'p');
        
        if ($this->filters->status) {
            if ($this->filters->status == 1) {
                $queryBuilder->andWhere('(p.status = '.Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING.') OR (p.status = '.Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING.')');
            }
            else if ($this->filters->status == 2) {
                $queryBuilder->andWhere('p.status = '.Business\ScanFile\Constant::SCAN_FILE_STATUS_ERROR);
            }
            else if ($this->filters->status == 3) {
                $queryBuilder->andWhere('p.status = '.Business\ScanFile\Constant::SCAN_FILE_STATUS_CLOSED);
            }
        }
    }

    public function buildCellFile($row) {
        $mgr = $this->getModuleManager();
        $url = $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'downloadFile', array('id' => $row->getId()));
        return '<a target="_blank" href="'.$url.'">'.$row->getFilename().'</a>';
    }

    public function buildCellIdOrder($row) {
        if ($row->getIdOrder() != 0) {
            $mgr = $this->getModuleManager();
            return '<a target="_blank" href="'.$mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $row->getIdOrder())).'">View</a>';
        }
        return '';
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormModify')) {            
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'scan_file', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }
}