<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

abstract class AbstractZoneType implements ZoneTypeInterface {
    public $container;
    public $em;
    public function __construct($container) {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getEntityManager();
    }

    public function getToken($idScanFile, $id) {
        return $this->em->getRepository('AppDatabaseMainBundle:ScanFileToken')->findOneBy(array(
            'idScanFile' => $idScanFile,
            'tokenKey'   => $id
        ));
    }
    
    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            $r->setData($data);
        }
    }
}