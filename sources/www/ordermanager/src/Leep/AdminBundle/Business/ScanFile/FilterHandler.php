<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->status = 1;
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('status', 'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => array(
                0 => 'All',
                1 => "Pending for creation + updation",
                2 => 'Error',
                3 => 'Closed'
            )
        ));

        return $builder;
    }
}