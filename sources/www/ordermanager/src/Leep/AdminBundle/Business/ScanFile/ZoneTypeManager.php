<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\ScanFormTemplate\Constant;

class ZoneTypeManager {
    public $container;
    public $typeHandlers = array();
    public function __construct($container) {
        $this->container = $container;

        $handlers = Constant::getZoneTypeHandlers();
        foreach ($handlers as $idType => $service) {
            $this->typeHandlers[$idType] = $container->get($service);
        }
    }

    public function buildFormToken($builder, $token) {
        $idType = $token['idType'];
        $id = $token['id'];

        $this->typeHandlers[$idType]->buildForm($builder, $id, $token);
    }

    public function updateTokenData($idScanFile, $token, $data) {
        $idType = $token['idType'];
        $id = $token['id'];

        $this->typeHandlers[$idType]->updateTokenData($idScanFile, $id, $data);
    }
}