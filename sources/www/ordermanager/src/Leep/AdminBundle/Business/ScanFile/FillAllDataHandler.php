<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FillAllDataHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFile', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $data = array();
        $data['idScanFormTemplate'] = $entity->getIdScanFormTemplate();


        return $data;
    }

    public function buildForm($builder) {
        $helper = $this->container->get('leep_admin.scan_file.business.helper');
        $zoneTypeManager = $this->container->get('leep_admin.scan_file.business.zone_type_manager');

        $builder->add('idScanFormTemplate', 'hidden');
        $tokens = Utils::getTokens($this->container, $this->entity->getId(), false);
        foreach ($tokens as $token) {
            $zoneTypeManager->buildFormToken($builder, $token);
        }
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $model = $this->getForm()->getData();
        $zoneTypeManager = $this->container->get('leep_admin.scan_file.business.zone_type_manager');


        $tokens = Utils::getTokens($this->container, $this->entity->getId(), false);
        foreach ($tokens as $token) {
            $id = $token['id'];
            if (isset($model[$id])) {
                $data = $model[$id];

                $zoneTypeManager->updateTokenData($this->entity->getId(), $token, $data);
            }
        }
        $em->flush();

        // Redirect
        $this->setRedirectUrl($mgr->getUrl('leep_admin', 'scan_file', 'update_order', 'edit', array('id' => $this->entity->getId())));

        parent::onSuccess();
    }
}