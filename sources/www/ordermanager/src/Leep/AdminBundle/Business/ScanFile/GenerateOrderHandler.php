<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class GenerateOrderHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFile', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $data = array();
        $data['idScanFormTemplate'] = $entity->getIdScanFormTemplate();


        return $data;
    }

    public function buildForm($builder) {
        $helper = $this->container->get('leep_admin.scan_file.business.helper');
        $zoneTypeManager = $this->container->get('leep_admin.scan_file.business.zone_type_manager');

        $builder->add('idScanFormTemplate', 'hidden');

        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $model = $this->getForm()->getData();
        $zoneTypeManager = $this->container->get('leep_admin.scan_file.business.zone_type_manager');

        if ($this->entity->getIdOrder() == 0) {
            $this->entity->setStatus(Constant::SCAN_FILE_STATUS_ERROR);
        }
        else {
            $this->entity->setStatus(Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING);
        }
        $em->flush();

        // Redirect
        $this->setRedirectUrl($mgr->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForCreation'));

        parent::onSuccess();
    }
}