<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class GeneralCheckBoxZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $builder->add($id, 'checkbox', array(
            'attr' => array(
                'style' => 'width: 40px; height: 40px'
            )
        ));
    }
}
