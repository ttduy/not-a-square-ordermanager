<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class LanguageZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add($id, 'searchable_box', array(
            'choices' => array(0 => '') + $mapping->getMapping('LeepAdmin_Customer_Language')
        ));
    }
    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            $mapping = $this->container->get('easy_mapping');
            $data = $mapping->getMappingTitle('LeepAdmin_Customer_Language', $data);
            $r->setData($data);
        }
    }
}
