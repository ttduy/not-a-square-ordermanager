<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class GeneralTextBoxZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $builder->add($id, 'text', array(
            'attr' => array(
                'style' => 'width: 95%; height: 40px; font-size: 25px'
            )
        ));
    }

    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            $r->setData($data);
        }
    }
}