<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\ModuleBundle\Module\AbstractHook;

class RealignTemplateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $id = $controller->get('request')->get('id', 0);

        // Scan file
        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $data['scanFile'] = array(
            'id'               => $id,
            'name'             => $scanFile->getFilename(),
            'downloadPdfUrl'   => $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'downloadFile', array('id' => $id))
        );

        $data['numPages'] = $helper->getNumberPages($scanFile);
        $data['viewScanFileUrl'] = $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'viewScanFile');
    }
}
