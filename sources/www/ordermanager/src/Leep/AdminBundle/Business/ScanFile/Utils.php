<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business;

class Utils {

    public static function getTokens($container, $idScanFile, $isCreationRequired = false) {
        $filter = array();
        $filter['idScanFile'] = $idScanFile;
        if ($isCreationRequired === 'all') {

        }
        else {
            if ($isCreationRequired) {
                $filter['isCreationRequired'] = 1;
            }
            else {
                $filter['isCreationRequired'] = 0;   
            }
        }

        $results = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFileToken')
            ->findBy($filter, array('id' => 'ASC'));

        $tokens = array();
        foreach ($results as $r) {
            if ($r->getIdTokenType() == Business\ScanFormTemplate\Constant::TEMPLATE_DETECTION_ZONE) {
                continue;
            }

            $tokens[] = array(
                'id'           => $r->getTokenKey(),
                'idType'       => $r->getIdTokenType(),
                'image'        => $r->getTokenImage(),
                'data'         => $r->getData(),
                'params'       => @json_decode($r->getParams(), true) 
            );
        }
        return $tokens;
    }
}