<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\ModuleBundle\Module\AbstractHook;

class UpdateOrderHook extends AbstractHook {
    private $idScanFormFile;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        // Scan file
        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $data['scanFile'] = array(
            'id'               => $id,
            'name'             => $scanFile->getFilename(),
            'downloadPdfUrl'   => $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'downloadFile', array('id' => $id))
        );

        $data['viewOrderUrl'] = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $scanFile->getIdOrder()));

        // Scan Template
        $scanTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($scanFile->getIdScanFormTemplate());

        $data['formulaTemplate'] = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($scanTemplate->getIdFormulaTemplateUpdate());

        // Raw data
        $tokens = Utils::getTokens($controller, $id, 'all');
        $rawData = array();
        foreach ($tokens as $t) {
            $rawData[] = $t['id'].' = '.$t['data'];
        }
        $data['rawData'] = implode("\n", $rawData);
    }
}
