<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class GeneralDateTimeZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $builder->add($id, 'datepicker', array(
            'attr' => array(
                'style' => 'height: 40px; font-size: 25px'
            )
        ));
    }
    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            if ($data) {
                $r->setData($data->format('d/m/Y'));
                return true;
            }
            $r->setData('');
        }
    }
}
