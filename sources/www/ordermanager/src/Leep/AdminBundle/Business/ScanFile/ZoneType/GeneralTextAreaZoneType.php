<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class GeneralTextAreaZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $builder->add($id, 'textarea', array(
            'attr' => array(
                'rows' => 5,
                'cols' => 70,
                'style' => 'width: 95%;  font-size: 25px'
            )
        ));
    }
}
