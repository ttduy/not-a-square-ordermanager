<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class GridReaderHook extends AbstractHook {
    public $todayTs;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $formatter = $controller->get('leep_admin.helper.formatter');

        $query = $em->createQueryBuilder();
        $query->select('p.status, COUNT(p) as total')
            ->from('AppDatabaseMainBundle:ScanFile', 'p')
            ->groupBy('p.status');
        $rows = $query->getQuery()->getResult();

        $stats = array();
        foreach ($rows as $r) {
            $stats[$r['status']] = intval($r['total']);
        }
        $data['stats'] = array(
            'numPendingForCreation' => isset($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING]) ? intval($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING]) : 0,
            'numPendingForUpdation' => isset($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING]) ? intval($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING]) : 0,
            'numClosed' => isset($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_CLOSED]) ? intval($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_CLOSED]) : 0,
            'numError' => isset($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ERROR]) ? intval($stats[Business\ScanFile\Constant::SCAN_FILE_STATUS_ERROR]) : 0,
        );
    }

}
