<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DetectTemplateHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFile', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $data = array(
            'idScanFormTemplate' => $entity->getIdScanFormTemplate()
        );

        return $data;
    }

    public function buildForm($builder) {
        $builder->add('idScanFormTemplate', 'hidden');
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $model = $this->getForm()->getData();

        if ($model['idScanFormTemplate'] == -1) {
            $this->entity->setStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_ERROR);
            $this->setRedirectUrl($mgr->getUrl('leep_admin', 'scan_file', 'feature', 'processPendingForCreation'));
        }
        else {
            $this->entity->setIdScanFormTemplate($model['idScanFormTemplate']);
            $this->setRedirectUrl($mgr->getUrl('leep_admin', 'scan_file', 'realign_template', 'edit', array('id' => $this->entity->getId())));
        }
        $em->flush();

        parent::onSuccess();
    }
}