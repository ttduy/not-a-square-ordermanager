<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UploadHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new UploadModel();        
        
        $currentFiles = array();
        $helper = $this->container->get('leep_admin.scan_file.business.helper');
        $dir = $helper->getFileRawDir();
        $files = scandir($dir);
        foreach ($files as $f) {
            if ($f != '.' && $f != '..') {
                $currentFiles[] = $f;
            }
        }
        $model->currentFiles = implode("<br/>", $currentFiles);
        return $model;
    }

    public function buildForm($builder) {        
        $helper = $this->container->get('leep_admin.scan_file.business.helper');
        $builder->add('file', 'app_file_uploader', array(
            'label' => 'File',
            'required' => true,
            'path'     => $helper->getFileRawDir()
        ));

        $builder->add('currentFiles', 'label', array(
            'label' => 'Pending files',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $helper = $this->container->get('leep_admin.scan_file.business.helper');

        // check file upload
        $fileUpload = $model->file['attachment'];
        if ($fileUpload) {
            $attachmentDir = $helper->getFileRawDir();
            $name = $fileUpload->getClientOriginalName();
            $fileUpload->move($attachmentDir, $name);
        }
        else {
            $this->errors[] = "Please select a file";
            return false;
        }

        parent::onSuccess();
    }
}