<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

interface ZoneTypeInterface {
    public function buildForm($builder, $id, $token);
    public function updateTokenData($idScanFile, $id, $data);
}