<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class ComboboxZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $builder->add($id, 'choice', array(
            'choices' => array('A', 'B', 'C')
        ));
    }
}
