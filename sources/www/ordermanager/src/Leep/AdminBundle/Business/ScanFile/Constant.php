<?php 
namespace Leep\AdminBundle\Business\ScanFile;

class Constant {
    const SCAN_FILE_STATUS_ORDER_CREATE_PENDING = 10;
    const SCAN_FILE_STATUS_ORDER_UPDATE_PENDING = 20;
    const SCAN_FILE_STATUS_CLOSED    = 90;
    const SCAN_FILE_STATUS_ERROR     = 99;

    public static function getStatus() {
        return array(
            self::SCAN_FILE_STATUS_ORDER_CREATE_PENDING   => 'Pending for creating order',
            self::SCAN_FILE_STATUS_ORDER_UPDATE_PENDING   => 'Pending for updating order',
            self::SCAN_FILE_STATUS_ERROR                  => 'Error',
            self::SCAN_FILE_STATUS_CLOSED                 => 'Closed'
        );
    }
}
