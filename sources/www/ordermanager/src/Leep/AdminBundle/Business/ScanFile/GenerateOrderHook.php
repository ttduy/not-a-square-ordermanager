<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\ModuleBundle\Module\AbstractHook;

class GenerateOrderHook extends AbstractHook {
    private $idScanFormFile;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $mgr = $controller->get('easy_module.manager');
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $id = $controller->get('request')->get('id', 0);

        // Scan file
        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $data['scanFile'] = array(
            'id'               => $id,
            'name'             => $scanFile->getFilename(),
            'downloadPdfUrl'   => $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'downloadFile', array('id' => $id))
        );

        // Scan Template
        $scanTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($scanFile->getIdScanFormTemplate());

        $data['formulaTemplate'] = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($scanTemplate->getIdFormulaTemplateCreate());

        // Raw data
        $tokens = Utils::getTokens($controller, $id, true);
        $rawData = array();
        foreach ($tokens as $t) {
            $rawData[] = $t['id'].' = '.$t['data'];
        }
        $data['rawData'] = implode("\n", $rawData);
    }
}
