<?php 
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business;
use App\Database\MainBundle\Entity;


class Helper {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getFileRawDir() {
        return $this->container->getParameter('files_dir').'/scan_form/files_raw';
    }
    public function getFileDir() {
        return $this->container->getParameter('files_dir').'/scan_form/files';
    }
    public function getFileTemplatesDir() {
        return $this->container->getParameter('files_dir').'/scan_form/files_templates';
    }
    public function getFileTokensDir() {
        return $this->container->getParameter('files_dir').'/scan_form/files_tokens';
    }
    public function getFile($idScanFile) {
        return $this->getFileDir().'/'.$idScanFile.'.pdf';
    }

    public function getNumberPages($scanFile) {
        $pdfFile = $this->getFile($scanFile->getId());

        $pdf = $this->container->get('white_october.tcpdf')->create();   
        $numPage = $pdf->setSourceFile($pdfFile);
        return $numPage;
    }


    public function getImageUrl($image) {
        $mgr = $this->container->get('easy_module.manager');
        return $mgr->getUrl('leep_admin', 'app_utils', 'app_utils',  'viewImage', array('file' => '/scan_form/'.$image));        
    }

    public function parseZones($inputFile, $outputDir, $zonesArgs) {
        $helper = $this->container->get('leep_admin.helper.common');
        $jarFile = $this->container->getParameter('kernel.root_dir').'/../scripts/pdfImageExtractor/build/jar/pdfImageExtractor.jar';

        $cmd = sprintf("java -jar %s %s %s %s",
            '"'.$jarFile.'"',
            '"'.$inputFile.'"',
            '"'.$outputDir.'"',
            '"'.implode(';', $zonesArgs).'"'
        );
        return $helper->executeCommand($cmd);       
    }

    public function parseScanFileAgainstTemplates($scanFile) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $utils = $this->container->get('leep_admin.scan_form_template.business.utils');

        // Load all template detection zones
        $templateZones = array();
        $i = 0;
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormTemplateZone', 'p')
            ->innerJoin('AppDatabaseMainBundle:ScanFormTemplate', 'j', 'WITH', 'p.idScanFormTemplate = j.id');
        $zones = $query->getQuery()->getResult();
        foreach ($zones as $zone) {            
            $zonesDef = $utils->parseZonesDefinition($zone->getZonesDefinition());
            foreach ($zonesDef['zones'] as $zoneDef) {
                if ($zoneDef['idType'] == Business\ScanFormTemplate\Constant::TEMPLATE_DETECTION_ZONE) {
                    $i++;

                    if (!isset($templateZones[$zone->getIdScanFormTemplate()])) {
                        $templateZones[$zone->getIdScanFormTemplate()] = array(
                            'zones' => array(),
                            'files' => array()
                        );
                    }
            
                    $templateZones[$zone->getIdScanFormTemplate()]['zones'][] = sprintf("%s,%s,%s,%s,%s,%s", 
                        $zone->getPageNumber(),
                        $zoneDef['x'],
                        $zoneDef['y'],
                        $zoneDef['width'],
                        $zoneDef['height'],
                        $i
                    );   
                    $templateZones[$zone->getIdScanFormTemplate()]['files'][] = $i;
                }
            }
        }

        // Process all
        $pdfFile = $this->getFile($scanFile->getId());
        $imgDir = $this->getFileTemplatesDir().'/'.$scanFile->getId();
        @mkdir($imgDir, 0777);

        $allZones = array();
        foreach ($templateZones as $idScanFormTemplate => $z) {
            foreach ($z['zones'] as $zoneArg) {
                $allZones[] = $zoneArg;
            }
        }

        $this->parseZones($pdfFile, $imgDir, $allZones);

        return $templateZones;
    }

    public function generateScanFileTokens($scanFile, $pagesRealign) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $utils = $this->container->get('leep_admin.scan_form_template.business.utils');

        $expanderX = 20;
        $expanderY = 10;
        $allZones = array();
        $allZonesTokens = array();
        $templateZones = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplateZone')->findByIdScanFormTemplate($scanFile->getIdScanFormTemplate());
        foreach ($templateZones as $zone) {
            $zonesDef = $utils->parseZonesDefinition($zone->getZonesDefinition());
            foreach ($zonesDef['zones'] as $zoneDef) {
                $alignX = isset($pagesRealign[$zone->getPageNumber()]) ? intval($pagesRealign[$zone->getPageNumber()]['x']) : 0;
                $alignY = isset($pagesRealign[$zone->getPageNumber()]) ? intval($pagesRealign[$zone->getPageNumber()]['y']) : 0;

                $x = $zoneDef['x'] + $alignX - $expanderX;
                $y = $zoneDef['y'] + $alignY - $expanderY;
                $width = $zoneDef['width'] + $expanderX*2;
                $height = $zoneDef['height'] + $expanderY*2;

                $allZones[] = sprintf("%s,%s,%s,%s,%s,%s", 
                    $zone->getPageNumber(),
                    $x, $y, $width, $height,
                    $zoneDef['key']
                );   

                $allZonesTokens[] = array(
                    'idType'  => $zoneDef['idType'],
                    'isCreationRequired' => isset($zoneDef['isCreationRequired']) ? true : false,
                    'key' => $zoneDef['key'],
                    'params' => isset($zoneDef['params']) ? $zoneDef['params'] : array()
                );
            }
        }

        $pdfFile = $this->getFile($scanFile->getId());
        $tokensDir = $this->getFileTokensDir().'/'.$scanFile->getId();
        @mkdir($tokensDir);
        $this->parseZones($pdfFile, $tokensDir, $allZones);

        // Make entries
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ScanFileToken', 'p')
            ->andWhere('p.idScanFile = '.$scanFile->getId())
            ->getQuery()
            ->execute();
        foreach ($allZonesTokens as $t) {
            $fileToken = new Entity\ScanFileToken();
            $fileToken->setIdScanFile($scanFile->getId());
            $fileToken->setTokenKey($t['key']);
            $fileToken->setIsCreationRequired($t['isCreationRequired']);
            $fileToken->setIdTokenType($t['idType']);
            $fileToken->setTokenImage('/files_tokens/'.$scanFile->getId().'/'.$t['key'].'.jpg');
            $fileToken->setParams(json_encode($t['params']));
            $em->persist($fileToken);
        }
        $em->flush();
    }
}
