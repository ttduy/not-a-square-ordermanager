<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Easy\ModuleBundle\Module\AbstractHook;

class DetectTemplateHook extends AbstractHook {
    private $idScanFormFile;
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $helper = $controller->get('leep_admin.scan_file.business.helper');
        $id = $controller->get('request')->get('id', 0);
        $mgr = $controller->get('easy_module.manager');

        // Scan file
        $scanFile = $em->getRepository('AppDatabaseMainBundle:ScanFile')->findOneById($id);
        $data['scanFile'] = array(
            'id'               => $id,
            'name'             => $scanFile->getFilename(),
            'downloadPdfUrl'   => $mgr->getUrl('leep_admin', 'scan_file', 'feature', 'downloadFile', array('id' => $id))
        );

        // Scan Form Template 
        $templates = array();
        $scanFormTemplates = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findAll();
        foreach ($scanFormTemplates as $template) {
            $templates[$template->getId()] = array(
                'name'              => $template->getName(),
                'expectedTokens'    => array(),
                'actualTokens'      => array()
            );            
        }

        // Load expected tokens
        $expectedTokens = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplateExpectedTokens')->findAll();
        foreach ($expectedTokens as $t) {
            $idScanFormTemplate = $t->getIdScanFormTemplate();
            if (isset($templates[$idScanFormTemplate])) {
                $templates[$idScanFormTemplate]['expectedTokens'][] = $helper->getImageUrl($t->getImage());
            }
        }

        // Parse tokens
        $actualTokens = $helper->parseScanFileAgainstTemplates($scanFile);
        foreach ($actualTokens as $idScanFormTemplate => $indexes) {
            if (isset($templates[$idScanFormTemplate])) {
                foreach ($indexes['files'] as $i) {
                    $templates[$idScanFormTemplate]['actualTokens'][] = $helper->getImageUrl('/files_templates/'.$scanFile->getId().'/'.$i.'.jpg');
                }
            }
        }
        $data['templates'] = $templates;
    }
}
