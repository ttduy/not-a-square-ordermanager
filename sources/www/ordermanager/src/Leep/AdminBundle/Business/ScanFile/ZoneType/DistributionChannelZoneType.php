<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class DistributionChannelZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add($id, 'searchable_box', array(
            'choices' => array(0 => '') + $mapping->getMapping('LeepAdmin_DistributionChannel_List')
        ));
    }
    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            $mapping = $this->container->get('easy_mapping');
            $data = $mapping->getMappingTitle('LeepAdmin_DistributionChannel_List', $data);
            $r->setData($data);
        }
    }
}
