<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

use Leep\AdminBundle\Business;

class FormQuestionZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $key = isset($token['params']['Key']) ? $token['params']['Key'] : '';
        $em = $this->container->get('doctrine')->getEntityManager();
        $formQuestion = $em->getRepository('AppDatabaseMainBundle:FormQuestion')->findOneByReportKey(trim($key));

        if ($formQuestion) {            
            $formQuestionData = Business\Customer\Util::prepareFormQuestion($formQuestion);

            Business\Customer\Util::buildFormQuestion($builder, $id, $formQuestionData);
        }
        else {
            $builder->add($id, 'label');
        }
    }
    public function updateTokenData($idScanFile, $id, $data) {
        if (is_array($data)) {
            $data = '['.implode(',', $data).']';
        }
        $r = $this->getToken($idScanFile, $id);
        $r->setData($data);
    }
}
