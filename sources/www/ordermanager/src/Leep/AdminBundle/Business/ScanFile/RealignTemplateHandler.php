<?php
namespace Leep\AdminBundle\Business\ScanFile;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class RealignTemplateHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFile', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $data = array(
            'idScanFormTemplate' => $entity->getIdScanFormTemplate()
        );

        $aligns = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFileRealign')->findByIdScanFile($entity->getId());
        foreach ($aligns as $a) {
            $data['alignX_'.$a->getPageNumber()] = $a->getX();
            $data['alignY_'.$a->getPageNumber()] = $a->getY();
        }

        return $data;
    }

    public function buildForm($builder) {
        $helper = $this->container->get('leep_admin.scan_file.business.helper');
        $numPage = $helper->getNumberPages($this->entity);

        $builder->add('idScanFormTemplate', 'hidden');
        for ($i = 1; $i <= $numPage; $i++) {
            $builder->add('alignX_'.$i, 'text', array(
                'attr' => array('style' => 'width: 90px')
            ));
            $builder->add('alignY_'.$i, 'text', array(
                'attr' => array('style' => 'width: 90px')
            ));
        }
        
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $model = $this->getForm()->getData();
        $helper = $this->container->get('leep_admin.scan_file.business.helper');

        // Update realignment
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('idScanFile' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ScanFileRealign', $filters);

        $pagesRealign = array();
        foreach ($model as $k => $v) {
            if (strpos($k, "alignX_") === 0) {
                $pageNumber = intval(substr($k, 7));
                if (!isset($pagesRealign)) { $pagesRealign[$pageNumber] = array('x' => 0, 'y' => 0); }
                $pagesRealign[$pageNumber]['x'] = intval($v);
            }
            else if (strpos($k, "alignY_") === 0) {
                $pageNumber = intval(substr($k, 7));
                if (!isset($pagesRealign)) { $pagesRealign[$pageNumber] = array('x' => 0, 'y' => 0); }
                $pagesRealign[$pageNumber]['y'] = intval($v);
            }
        }

        foreach ($pagesRealign as $pageNumber => $r) {
            $realign = new Entity\ScanFileRealign();
            $realign->setIdScanFile($this->entity->getId());
            $realign->setPageNumber($pageNumber);
            $realign->setX($r['x']);
            $realign->setY($r['y']);
            $em->persist($realign);
        }
        $em->flush();

        // Regenerate tokens
        $helper->generateScanFileTokens($this->entity, $pagesRealign);

        // Redirect
        $this->setRedirectUrl($mgr->getUrl('leep_admin', 'scan_file', 'fill_basic_data', 'edit', array('id' => $this->entity->getId())));

        parent::onSuccess();
    }
}