<?php 
namespace Leep\AdminBundle\Business\ScanFile\ZoneType;

class ReportTypeZoneType extends AbstractZoneType {
    public function buildForm($builder, $id, $token) {
        $mapping = $this->container->get('easy_mapping');
        $builder->add($id, 'choice', array(
            'choices' => array(0 => '') + $mapping->getMapping('LeepAdmin_Report_DncReportType')
        ));
    }
    public function updateTokenData($idScanFile, $id, $data) {
        $r = $this->getToken($idScanFile, $id);
        if ($r) {
            $mapping = $this->container->get('easy_mapping');
            $data = $mapping->getMappingTitle('LeepAdmin_Report_DncReportType', $data);
            $r->setData($data);
        }
    }
}
