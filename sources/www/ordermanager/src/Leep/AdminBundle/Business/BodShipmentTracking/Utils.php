<?php
namespace Leep\AdminBundle\Business\BodShipmentTracking;
use App\Database\MainBundle\Entity;

class Utils {
    public static function syncShipment($container) {
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $em = $container->get('doctrine')->getEntityManager();

        $existingFiles = array();
        $rows = $em->getRepository('AppDatabaseMainBundle:BodShipmentTracking')->findAll();
        foreach ($rows as $r) {
            $existingFiles[$r->getFilename()] = 1;
        }

        $ftpServer = $config->getBodFtpServer();
        $ftpConn = ftp_connect($ftpServer) or die("Could not connect to $ftpServer");
        $login = ftp_login($ftpConn, $config->getBodFtpUsername(), $config->getBodFtpPassword());
        ftp_pasv($ftpConn,true);

        $newFiles = 0;
        $files = ftp_nlist($ftpConn, '/shipconf');
        foreach ($files as $remoteFile) {
            $filename = trim(substr($remoteFile, 10));
            if (!isset($existingFiles[$filename])) {
                ob_start();
                ftp_get($ftpConn, 'php://output', $remoteFile, FTP_ASCII);
                $data = ob_get_contents();
                ob_end_clean();

                $xml = simplexml_load_string($data);
                foreach ($xml->Order as $order) {
                    $newFiles++;

                    $orderNumber = @$order->OrderNumber;
                    $customerNumber = @$order->CustomerOrderNumber;
                    $shippingDate = @$order->ShippingDate;
                    $shippingTime = @$order->ShippingTime;
                    $trackingNumber = @$order->TrackingInfo->TrackingNo;
                    $trackingLink = @$order->TrackingInfo->TrackingLink;

                    $bodShipmentTracking = new Entity\BodShipmentTracking();
                    $bodShipmentTracking->setTimestamp(new \DateTime());
                    $bodShipmentTracking->setOrderNumber($orderNumber);
                    $bodShipmentTracking->setCustomerNumber($customerNumber);
                    $bodShipmentTracking->setShipmentDatetime($shippingDate.' '.$shippingTime);
                    $bodShipmentTracking->setTrackingNo($trackingNumber);
                    $bodShipmentTracking->setTrackingLink($trackingLink);
                    $bodShipmentTracking->setXml($data);
                    $bodShipmentTracking->setFilename($filename);
                    $em->persist($bodShipmentTracking);
                }
                $em->flush();
            }
        }

        ftp_close($ftpConn);

        return $newFiles;
    }
}