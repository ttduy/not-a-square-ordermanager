<?php
namespace Leep\AdminBundle\Business\BodShipmentTracking;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));
        $builder->add('customerNumber', 'text', array(
            'label'    => 'Customer Order Number',
            'required' => false
        ));
        
        return $builder;
    }
}