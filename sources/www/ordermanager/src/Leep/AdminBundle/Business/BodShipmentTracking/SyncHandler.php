<?php
namespace Leep\AdminBundle\Business\BodShipmentTracking;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class SyncHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SyncModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('isSyncNow', 'checkbox', array(
            'label'    => 'Sync now?',
            'required' => false
        ));
    }

    public function onSuccess() {
        set_time_limit(300);

        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        if ($model->isSyncNow) {
            $newFiles = Utils::syncShipment($this->container);
            $this->messages = array('Done! There\'re '.$newFiles.' new order(s) tracking number');
        }
    }
}
