<?php
namespace Leep\AdminBundle\Business\BodShipmentTracking;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('timestamp', 'orderNumber', 'customerNumber', 'shipmentDatetime', 'trackingNo', 'trackingLink');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Order Number',      'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Customer Order Number',   'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Shipment Time',     'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Tracking Number',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Tracking Link',     'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_BodShipmentTrackingGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:BodShipmentTracking', 'p');

        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder
                ->andWhere('p.orderNumber = :orderNumber')
                ->setParameter('orderNumber', trim($this->filters->orderNumber));
        }
        if (trim($this->filters->customerNumber) != '') {
            $queryBuilder
                ->andWhere('p.customerNumber = :customerNumber')
                ->setParameter('customerNumber', trim($this->filters->customerNumber));
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.id', 'DESC');        
    }

    public function buildCellTrackingLink($row) {
        return '<a target="_blank" href="'.$row->getTrackingLink().'">'.$row->getTrackingLink().'</a>';
    }
}
