<?php
namespace Leep\AdminBundle\Business\ProductType;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $productType = $em->getRepository('AppDatabaseMainBundle:ProductType')->findOneById($id);
        // delete Product Group itself
        $em->remove($productType);
        $em->flush();
    }
}
