<?php
namespace Leep\AdminBundle\Business\ProductType;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoicingCode', 'text', array(
            'label'    => 'Product Invoicing code (Numbers, 4 digits)',
            'required' => false
        ));
        $builder->add('taxGermany', 'number', array(
            'label'    => 'Tax Germany',
            'required' => false
        ));
        $builder->add('taxAustria', 'text', array(
            'label'    => 'Tax Austria',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $productType = new Entity\ProductType();
        $productType->setName($model->name);
        $productType->setInvoicingCode($model->invoicingCode);
        $productType->setTaxGermany($model->taxGermany);
        $productType->setTaxAustria($model->taxAustria);
        $em->persist($productType);
        $em->flush();

        parent::onSuccess();
    }
}
