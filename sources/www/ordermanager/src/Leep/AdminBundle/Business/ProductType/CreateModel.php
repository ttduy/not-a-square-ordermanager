<?php
namespace Leep\AdminBundle\Business\ProductType;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $invoicingCode;
    public $taxGermany;
    public $taxAustria;
}
