<?php
namespace Leep\AdminBundle\Business\ProductType;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name','taxGermany', 'taxAustria', 'invoicingCode', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name',            'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Tax Germany',     'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Tax Austria',     'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Invoicing code',  'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Action',          'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ProductType', 'p');

        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }

        if ($this->filters->country) {
            $queryBuilder->innerJoin('AppDatabaseMainBundle:ProductTypeCountry', 'pgs', 'WITH', 'pgs.idProductType = p.id AND pgs.idCountry = :country')
                ->setParameter('country', $this->filters->country);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('productGroupModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'product_type', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'product_type', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        return $builder->getHtml();
    }
}
