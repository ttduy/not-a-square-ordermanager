<?php
namespace Leep\AdminBundle\Business\ProductType;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class EditHandler extends AppEditHandler {
    private $id;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $this->id = $id;
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductType', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->invoicingCode = $entity->getInvoicingCode();
        $model->taxGermany    = $entity->getTaxGermany();
        $model->taxAustria    = $entity->getTaxAustria();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('invoicingCode', 'text', array(
            'label'    => 'Product Invoicing code (Numbers, 4 digits)',
            'required' => false
        ));
        $builder->add('taxGermany', 'number', array(
            'label'    => 'Tax Germany',
            'required' => false
        ));
        $builder->add('taxAustria', 'text', array(
            'label'    => 'Tax Austria',
            'required' => false
        ));
        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $this->entity->setName($model->name);
        $this->entity->setInvoicingCode($model->invoicingCode);
        $this->entity->setTaxGermany($model->taxGermany);
        $this->entity->setTaxAustria($model->taxAustria);

        parent::onSuccess();
    }
}
