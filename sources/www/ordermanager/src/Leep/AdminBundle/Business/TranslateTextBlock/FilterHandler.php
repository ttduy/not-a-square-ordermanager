<?php
namespace Leep\AdminBundle\Business\TranslateTextBlock;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idReport = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping'); 

        $utils = $this->container->get('leep_admin.translate_text_block.business.utils');

        $builder->add('idReport', 'choice', array(
            'label'    => 'Report',            
            'choices'  => array(0 => 'All') + $utils->getReportMapping(),
            'required' => false,
            'empty_value' => false
        ));
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('text', 'text', array(
            'label'    => 'Text',
            'required' => false
        ));
        $builder->add('isShowUntranslatedOnly', 'checkbox', array(
            'label'    => 'Show only untranslated',
            'required' => false
        ));

        return $builder;
    }
}