<?php
namespace Leep\AdminBundle\Business\TranslateTextBlock;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class TranslateHandler extends BaseCreateHandler {
    public $textBlockLanguage = null;

    public function getDefaultFormModel() {
        $request = $this->container->get('request');

        $model = array();
        if ($this->textBlockLanguage) {
            $model['translation'] = $this->textBlockLanguage->getBody();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('translation', 'textarea', array(
            'label' => 'Translation',
            'attr'  => array(
                'style'  => 'width: 95%',
                'rows'   => 10
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $request = $this->container->get('request');
        $validator = $this->container->get('leep_admin.text_block.business.constraint_validator');

        $textBlock = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($this->textBlockLanguage->getIdTextBlock());

        $errors = $validator->validate($textBlock, $model['translation']);
        if (!empty($errors)) {
            $this->errors = $errors;
            return false;
        }

        $idLanguage = $request->get('idLanguage', 0);
        $this->textBlockLanguage->setBody($model['translation']);
        $this->textBlockLanguage->setIsTranslated(1);
        $this->textBlockLanguage->setIsCached(0);
        $em->flush();

        // Remove duplication
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:TextBlock', 'p')
            ->andWhere('(p.name = :name) AND (p.id != :idTextBlock)')
            ->setParameter('name', $textBlock->getName())
            ->setParameter('idTextBlock', $textBlock->getId())
            ->getQuery()
            ->execute();

        parent::onSuccess();
        $this->messages = array('Saved!');
    }
}
