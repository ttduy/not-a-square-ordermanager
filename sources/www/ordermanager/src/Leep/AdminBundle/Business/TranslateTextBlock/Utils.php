<?php
namespace Leep\AdminBundle\Business\TranslateTextBlock;
         
class Utils {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }


    public function getSourceLanguages() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $id = $userManager->getUser()->getId();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserSourceLanguage')->findByIdTranslatorUser($id);
        $languages = array();
        foreach ($result as $r) {
            $languages[] = $r->getIdLanguage();
        }
        return $languages;
    }

    public function getTargetLanguages() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $id = $userManager->getUser()->getId();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserTargetLanguage')->findByIdTranslatorUser($id);
        $languages = array();
        foreach ($result as $r) {
            $languages[] = $r->getIdLanguage();
        }
        return $languages;
    }

    public function getReports() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $id = $userManager->getUser()->getId();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserReport')->findByIdTranslatorUser($id);
        $reports = array();
        foreach ($result as $r) {
            $reports[] = $r->getIdReport();
        }
        return $reports;
    }

    public function getReportMapping() {
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $mapping = $this->container->get('easy_mapping');
        $id = $userManager->getUser()->getId();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:TranslatorUserReport')->findByIdTranslatorUser($id);
        $reports = array();
        foreach ($result as $r) {
            $reports[$r->getIdReport()] = $mapping->getMappingTitle('LeepAdmin_Report_List', $r->getIdReport());
        }
        return $reports;
    }


}