<?php
namespace Leep\AdminBundle\Business\TranslateTextBlock;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $utils;
    public $sourceLanguages = array();
    public $targetLanguages = array();

    public function __construct($container) {
        $this->utils = $container->get('leep_admin.translate_text_block.business.utils');
        $this->sourceLanguages = $this->utils->getSourceLanguages();
        $this->targetLanguages = $this->utils->getTargetLanguages();

        parent::__construct($container);
    }

    public function getColumnMapping() {
        $headers = array();
        $headers[] = 'name';
        foreach ($this->sourceLanguages as $idLanguage) {
            $headers[] = 'name';
        }
        foreach ($this->targetLanguages as $idLanguage) {
            $headers[] = 'name';
        }

        return $headers;
    }

    public function getTableHeader() {
        $total = count($this->sourceLanguages) + count($this->targetLanguages);
        $percentage = round(80/$total);
        $mapping = $this->container->get('easy_mapping');
        $headers = array();
        $headers[] = array('title' => 'Name', 'width' => '15%', 'sortable' => 'false');
        foreach ($this->sourceLanguages as $idLanguage) {
            $headers[] = array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage),
                'width' => $percentage.'%',
                'sortable' => 'false'
            );
        }
        foreach ($this->targetLanguages as $idLanguage) {
            $headers[] = array(
                'title' => $mapping->getMappingTitle('LeepAdmin_Language_List', $idLanguage),
                'width' => $percentage.'%',
                'sortable' => 'false'
            );
        }

        return $headers;
    }

    public function getFormatter() {
        return null;
    }

    public function getCountSQL() {
        return 'COUNT(DISTINCT p.id)';
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('DISTINCT p.id, p.name, p.maxLength, p.isUppercase, p.numLineBreak, p.keyWords')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->innerJoin('AppDatabaseMainBundle:TextBlockLanguage', 'l', 'WITH', 'l.idTextBlock = p.id')
            ->innerJoin('AppDatabaseMainBundle:ReportTextBlock', 'r', 'WITH', 'p.id = r.idTextBlock');

        $languages = array_merge($this->sourceLanguages, $this->targetLanguages);
        $queryBuilder->andWhere('l.idLanguage IN (:languages)')
            ->setParameter('languages', $languages);

        if ($this->filters->idReport != 0) {
            $queryBuilder->andWhere('r.idReport = :idReport')
                ->setParameter('idReport', intval($this->filters->idReport));
        }
        else {
            $reports = $this->utils->getReports();
            if (empty($reports)) { $reports[] = 0; }
            $queryBuilder->andWhere('r.idReport IN (:reports)')
                ->setParameter('reports', $reports);
        }
        if (!empty($this->filters->name)) {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (!empty($this->filters->text)) {
            $queryBuilder->andWhere('l.body LIKE :text')
                ->setParameter('text', '%'.trim($this->filters->text).'%');
        }
        if (!empty($this->filters->isShowUntranslatedOnly)) {
            $queryList = array();
            $queryList[] = "(l.body IS NULL)";
            $queryList[] = "(TRIM(l.body) = '')";
            foreach ($this->untranslatedPattern as $pattern) {
                $queryList[] = "(l.body = '".$pattern."')";
            }
            $queryBuilder->andWhere(implode(" OR ", $queryList));
        }
    }

    public function getResult() {
        $data = array();

        $em = $this->container->get('doctrine')->getEntityManager();
        $languagesToLoad = $this->sourceLanguages;
        foreach ($this->targetLanguages as $idLanguage) {
            $languagesToLoad[] = $idLanguage;
        }

        $textBlockToLoad = array(0);
        foreach ($this->result as $row) {
            $textBlockToLoad[] = $row['id'];
        }

        // Load text blocks
        $textBlocks = array();
        $query = $em->createQueryBuilder();
        $result = $query->select('p.idTextBlock, p.idLanguage, p.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->andWhere('p.idLanguage in (:languages)')
            ->andWhere('p.idTextBlock in (:textBlocks)')
            ->setParameter('languages', $languagesToLoad)
            ->setParameter('textBlocks', $textBlockToLoad)
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $textBlocks[$r['idTextBlock'].'_'.$r['idLanguage']] = $r['body'];
        }

        foreach ($this->result as $row) {
            $rowData = array();

            // Text block name
            $name = $row['name'];
            if ($row['maxLength'] !== NULL) {
                $name .= '<br/><i style="color: red">Max Length: '.$row['maxLength']."</i>";
            }
            if ($row['isUppercase']) {
                $name .= '<br/><i style="color: red">Uppercase: YES </i>';
            }
            if ($row['numLineBreak'] !== NULL) {
                $name .= '<br/><i style="color: red">Num line break: '.$row['numLineBreak']."</i>";
            }
            if ($row['keyWords'] != '') {
                $name .= '<br/><i style="color: red">Keywords: '.$row['keyWords']."</i>";
            }


            // Prepare source
            $rowData[] = $name;
            $sourceText = array();
            foreach ($this->sourceLanguages as $idLanguage) {
                $id = $row['id'].'_'.$idLanguage;
                $body = isset($textBlocks[$id]) ? $textBlocks[$id] : '';
                $sourceText[] = $body;
                $rowData[] = str_replace("\n", "<br/><br/>", htmlspecialchars($body));
            }

            foreach ($this->targetLanguages as $idLanguage) {
                $id = $row['id'].'_'.$idLanguage;
                $body = isset($textBlocks[$id]) ? $textBlocks[$id] : '';

                $htmlId = 'tb_'.$row['id'].'_'.$idLanguage;
                $htmlIdInfo = $htmlId . '_info';
                $rowData[] = '<textarea id="'.$htmlId.'" style="width: 100%; height: 80%" onkeyup="javascript:updateStats('.$row['id'].', '.$idLanguage.')" onchange="javascript:saveTextBlock('.$row['id'].', '.$idLanguage.')">'.$body.'</textarea>'.
                    '<div id="'.$htmlId.'_stats_info" style="display: inline-block">&nbsp;</div><br/>'.
                    '<div id="'.$htmlIdInfo.'" style="display: inline-block">&nbsp;</div><br/>';
            }

            $data[$row['id']] = $rowData;
        }
        return $data;
    }

    public $untranslatedPattern = array(
        "###",
        "-- No translation yet --",
        "????",
        "***MISSING TEXT***",
        "--- TRANSLATE ME ---",
        "***MISSING TEXT****",
        "--- TRANSLATE ME ---",
        "####"
    );
}