<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Partner\EditComplaintHandler as BaseEditComplaintHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditComplaintHandler extends BaseEditComplaintHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function searchComplaint($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerComplaint', 'p')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldComplaints($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idCustomer' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerComplaint', $filters);
    }

    public function createNewComplaint($id) {
        $complaint = new Entity\CustomerComplaint();
        $complaint->setIdCustomer($id);
        return $complaint;
    }
}
