<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditColorHandler extends AppEditHandler {
    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) { 
        $model = new EditColorModel();

        return $model;
    }

    public function getCustomerColorCodes() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $colorCodes = array();
        $rows = explode("\n", $config->getCustomerColorCode());
        foreach ($rows as $row) {
            $a = explode("|", $row);
            if (count($a) == 2) {
                $colorCodes[trim($a[0])] = trim($a[1]);
            }
        }
        return $colorCodes;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('colorCode',   'choice', array(
            'label'    => 'Color code',
            'required' => false,
            'choices'  => $this->getCustomerColorCodes()
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $customerFilterBuilder = $this->container->get('leep_admin.customer.business.customer_filter_builder');

        $filterHandler = $this->container->get('leep_admin.customer.business.filter_handler');
        $filters = $filterHandler->getCurrentFilter();

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->update('AppDatabaseMainBundle:Customer', 'p')
            ->set('p.colorCode', "'".$model->colorCode."'");
        $customerFilterBuilder->buildQuery($queryBuilder, $filters);

        $queryBuilder->getQuery()->execute();

        $em->flush();

        $this->messages[] = 'Color has been updated successfully';
    }
}