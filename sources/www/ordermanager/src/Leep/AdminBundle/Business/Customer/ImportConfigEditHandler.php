<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class ImportConfigEditHandler extends AppEditHandler {   
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $model = array();

        $this->configProducts = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ImportConfigProduct')->findAll();
        foreach ($this->configProducts as $row) {
            $model[$row->getCode()] = $row->getProductId();
        }
        $this->configCategories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ImportConfigCategory')->findAll();
        foreach ($this->configCategories as $row) {
            $model[$row->getCode()] = $row->getCategoryId();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('section1', 'section', array(
            'label'    => 'Product Mapping Config',
            'property_path' => false
        ));
        foreach ($this->configProducts as $row) {
            $builder->add($row->getCode(),  'choice', array(
                'label' => $row->getCode(),
                'required' => false,
                'choices' => $mapping->getMappingFiltered('LeepAdmin_Product_List')
            ));
        }

        $builder->add('section2', 'section', array(
            'label'    => 'Category Mapping Config',
            'property_path' => false
        ));
        foreach ($this->configCategories as $row) {
            $builder->add($row->getCode(),  'choice', array(
                'label' => $row->getCode(),
                'required' => false,
                'choices' => $mapping->getMappingFiltered('LeepAdmin_Category_List')
            ));
        }
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();


        foreach ($this->configProducts as $row) {
            $v = isset($model[$row->getCode()]) ? $model[$row->getCode()] : 0;
            $row->setProductId($v);
            $em->persist($row);
        }

        foreach ($this->configCategories as $row) {
            $v = isset($model[$row->getCode()]) ? $model[$row->getCode()] : 0;
            $row->setCategoryId($v);
            $em->persist($row);
        }
        $em->flush();
        //parent::onSuccess();
        $this->messages[] = 'Updated!';
    }
}
