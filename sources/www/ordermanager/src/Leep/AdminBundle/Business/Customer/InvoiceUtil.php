<?php
namespace Leep\AdminBundle\Business\Customer;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;

class InvoiceUtil {
    // Build customer invoice
    public static function buildPdf($controller, $idCustomer, $isReturnDataOnly = false) {
        $mapping = $controller->get('easy_mapping');

        $formatter = $controller->get('leep_admin.helper.formatter');

        $customer = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $company = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Companies')->findOneById($customer->getInvoiceFromCompanyId());
        if (!$company) {
            return new Response("Please select company for this invoice");
        }
        $client = $customer;
        $applied = 'invoice_customer';

        $logo = $formatter->container->getParameter('kernel.root_dir').'/../logo.png';
        if ($company->getLogo()) {
            $logo = $formatter->container->getParameter('kernel.root_dir').'/../web/attachments/logo/'.$company->getLogo();
        }

        $distributionChannel = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());

        $data = array();
        $data['customer'] = $customer;
        if ($distributionChannel) {
            $data['distributionChannel'] = $distributionChannel->getDistributionChannel();
        }
        $data['logoImagePath'] = $logo;
        $data['company'] = $company;
        $data['companyName'] = $company->getCompanyName();
        $data['companyStreet'] = $company->getStreet();
        $data['companyPostCode'] = $company->getPostCode();
        $data['companyCity'] = $company->getCity();
        $data['companyEmail'] = $company->getEmail();
        $data['companyTelephone'] = $company->getTelephone();
        $data['companyWebSite'] = $company->getWebSite();

        if ($client->getInvoiceAddressIsUsed()) {
            $data['clientStreet'] = $client->getInvoiceAddressStreet();
            $data['clientStreet2'] = '';
            $data['clientCity'] = $client->getInvoiceAddressCity();
            $data['clientPostCode'] = $client->getInvoiceAddressPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getInvoiceAddressIdCountry());

            $clientName = $client->getInvoiceAddressClientName();
            $companyName = $client->getInvoiceAddressCompanyName();
            if (trim($clientName) != '') {
                $data['clientName'] = $clientName;
                if ($clientName == '######') {
                    $data['clientName'] = '';
                }
            }
            if (trim($companyName) != '') {
                $data['clientCompanyName'] = $companyName;
                if ($companyName == '######') {
                    $data['clientCompanyName'] = '';
                }
            }
        }
        else {
            $data['clientName'] = $client->getFirstName().' '.$client->getSurName();
            $data['clientStreet'] = $client->getStreet();
            $data['clientStreet2'] = method_exists($client, 'getStreet2') ? $client->getStreet2() : '';
            $data['clientCity'] = $client->getCity();
            $data['clientPostCode'] = $client->getPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getCountryId());
        }

        $today = new \DateTime();
        $data['invoiceDate'] = $formatter->format($customer->getInvoiceDate(), 'date');
        $data['invoiceNumber'] = $customer->getInvoiceNumber();
        if ($isReturnDataOnly) {
            return $data;
        }

        $order = array();
        $subTotal = 0;
        $discount = 0;

        $price = Business\Customer\Util::computePrice($controller, $customer->getId(), $applied);
        foreach ($price['items']['categories'] as $idCategory => $v) {
            $order[] = array(
                'orderNumber' => $customer->getOrderNumber(),
                'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                'product'     => $mapping->getMappingTitle('LeepAdmin_Category_List', $idCategory),
                'firstName'   => $customer->getFirstName(),
                'lastName'    => $customer->getSurName(),
                'sum'         => $formatter->format($v, 'money')
            );    
             $subTotal += $v;
        }
        foreach ($price['items']['products'] as $idProduct => $v) {
            $row = array(
                'orderNumber' => $customer->getOrderNumber(),
                'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                'product'     => $mapping->getMappingTitle('LeepAdmin_Product_List', $idProduct),
                'firstName'   => $customer->getFirstName(),
                'lastName'    => $customer->getSurName(),
                'sum'         => $formatter->format($v, 'money')
            );    
            $subTotal += $v;

            if (isset($price['specialProducts'][$idProduct])) {
                $row['product'] = $mapping->getMappingTitle('LeepAdmin_SpecialProduct_List', $price['specialProducts'][$idProduct]);
            }
            $order[] = $row;
        }     
        $overridePrice = $customer->getPriceOverride();
        if ($overridePrice != null && $overridePrice != 0) {
            $discount += $subTotal - $overridePrice;
        }

        $data['order'] = $order;
        $data['postage'] = $formatter->format($customer->getPostage(), 'money');
        $data['subtotal'] = $formatter->format($subTotal, 'money');
        $data['discount'] = $formatter->format($discount, 'money');
        $data['total']    = $formatter->format($subTotal - $discount + floatval($customer->getPostage()), 'money');

        $total = ($subTotal - $discount + floatval($customer->getPostage()));
        $taxAmount = $total * floatval($customer->getTaxValue()) / 100;
        $data['taxAmount'] = $formatter->format($taxAmount, 'money');
        $data['totalWithTax'] = $formatter->format($total + $taxAmount, 'money');

        // Update invoice amount
        if ($customer->getIsWithTax() == 1) {
            $finalTotal = $total + $taxAmount;            
        }
        else {
            $finalTotal = $total;
        }
        $customer->setInvoiceAmount($finalTotal);

        // Currency conversion
        if ($customer->getIdCurrency() != 0) {
            $currency = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Currency')->findOneById($customer->getIdCurrency());
            if ($currency) {
                $exchangeRate = floatval($customer->getExchangeRate());
                $data['currency'] = $currency;
                $data['exchangeRate'] = $exchangeRate;
                $totalConverted = $exchangeRate * $finalTotal;
                $data['totalConverted'] = $formatter->format($totalConverted, 'money', $currency->getSymbol());
            }
        }

        // Extra text
        $data['textAfterDescription'] = nl2br($customer->getTextAfterDescription());
        $data['textFooter'] = nl2br($customer->getTextFooter());

        // Build PDF
        $html = $controller->get('templating')->render('LeepAdminBundle:CustomerInvoiceForm:pdf.html.twig', $data);
        

        $dompdf = $controller->get('slik_dompdf');
        $dompdf->getpdf($html);

        $filePath = $controller->get('service_container')->getParameter('files_dir').'/invoices';
        $fileName = 'customer_invoice_'.$customer->getId().'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        $customer->setInvoiceFile($fileName);
        $em = $controller->get('doctrine')->getEntityManager();
        $em->persist($customer);
        $em->flush();

        return TRUE;
    }
}
