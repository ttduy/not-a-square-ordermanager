<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Form\FormError;

class EditHandler extends AppEditHandler {
    public $attachmentKey = false;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        $order = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);

        return $order;
    }

    public function convertToFormModel($entity) {
        $mapping = $this->container->get('easy_mapping');
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $model = new EditModel();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        if ($entity->getIdOriginalCustomer() != 0) {
            $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($entity->getIdOriginalCustomer());
            if ($customer) {
                $model->oldBarcode = $customer->getOrderNumber();
                $originalExternalBarcode = $this->findExternalBarcode($em, $customer->getIdCustomerInfo());
                if ($originalExternalBarcode != '') {
                    $model->oldBarcode .= ' / '.$originalExternalBarcode;
                }
            }
        }

        // get customer info
        if ($entity->getIdCustomerInfo() != 0) {
            $customerInfo = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($entity->getIdCustomerInfo());
            if ($customerInfo) {
                $model->idCustomerInfo = $entity->getIdCustomerInfo();
                $model->customerNumber = $customerInfo->getCustomerNumber();
                $model->externalBarcode = $customerInfo->getExternalBarcode();
                $model->idDistributionChannel = $customerInfo->getIdDistributionChannel();
                $model->firstName = $customerInfo->getFirstName();
                $model->surName = $customerInfo->getSurName();
                $model->dateOfBirth = $customerInfo->getDateOfBirth();
                $model->email = $customerInfo->getEmail();
                $model->genderId = $customerInfo->getGenderId();
                $model->title = $customerInfo->getTitle();
                $model->postCode = $customerInfo->getPostCode();
                $model->isCustomerBeContacted = $customerInfo->getIsCustomerBeContacted();
                $model->isNutrimeOffered = $customerInfo->getIsNutrimeOffered();
                $model->isNotContacted = $customerInfo->getIsNotContacted();
                $model->isDestroySample = $customerInfo->getIsDestroySample();
                $model->sampleCanBeUsedForScience = $customerInfo->getSampleCanBeUsedForScience();
                $model->isFutureResearchParticipant = $customerInfo->getIsFutureResearchParticipant();
                $model->webLoginGoesTo = $customerInfo->getWebLoginGoesTo();

                $model->detailInformation = $customerInfo->getDetailInformation();

                $orders = $em->getRepository('AppDatabaseMainBundle:Customer')->findByIdCustomerInfo($customerInfo->getId());
                $customerInfoUrl = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit', array('id' => $customerInfo->getId()));
                $customerInfoLink = "<a target='_blank' href='".$customerInfoUrl."'>".$customerInfo->getCustomerNumber()."</a>";
                $links = array();
                foreach ($orders as $order) {
                    $url = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $order->getId()));
                    $links[] = "<a target='_blank' href='".$url."'>".$order->getOrderNumber()."</a>";
                }
                $model->otherOrders = $customerInfoLink.": ".implode("&nbsp;&nbsp;", $links);

                $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customerInfo->getIdDistributionChannel());

                if ($dc) {
                    $model->idVisibleGroupProduct = $dc->getIdVisibleGroupProduct();
                }
            }
        }

        // other data
        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();

        $model->id = $entity->getId();

        $model->previousOrderNumber = $entity->getPreviousOrderNumber();
        $model->orderNumber = $entity->getOrderNumber();
        $model->trackingCode = $entity->getTrackingCode();
        $model->dnaSampleOrderNumberVario = $entity->getDnaSampleOrderNumberVario();
        $model->orderInfoText = $entity->getOrderInfoText();
        $model->domain = $entity->getDomain();
        $model->dateOrdered = $entity->getDateOrdered();
        $model->orderAge = $entity->getOrderAge();
        $model->laboratoryDetails = $entity->getLaboratoryDetails();

        $model->street = $entity->getStreet();
        $model->street2 = $entity->getStreet2();
        $model->postCode = $entity->getPostCode();
        $model->city = $entity->getCity();
        $model->countryId = $entity->getCountryId();
        $model->telephone = $entity->getTelephone();
        $model->fax = $entity->getFax();
        $model->notes = $entity->getNotes();

        $model->invoiceAddressIsUsed = $entity->getInvoiceAddressIsUsed();
        $model->invoiceAddressClientName = $entity->getInvoiceAddressClientName();
        $model->invoiceAddressCompanyName = $entity->getInvoiceAddressCompanyName();
        $model->invoiceAddressStreet = $entity->getInvoiceAddressStreet();
        $model->invoiceAddressPostCode = $entity->getInvoiceAddressPostCode();
        $model->invoiceAddressCity = $entity->getInvoiceAddressCity();
        $model->invoiceAddressIdCountry = $entity->getInvoiceAddressIdCountry();
        $model->invoiceAddressTelephone = $entity->getInvoiceAddressTelephone();
        $model->invoiceAddressFax = $entity->getInvoiceAddressFax();
        $model->invoiceAddressUid = $entity->getInvoiceAddressUid();
        $model->priceOverride = $entity->getPriceOverride();

        $model->languageId = $entity->getLanguageId();

        $model->companyInfo = $entity->getCompanyInfo();
        $model->laboratoryInfo = $entity->getLaboratoryInfo();
        $model->contactInfo = $entity->getContactInfo();
        $model->contactUs = $entity->getContactUs();
        $model->letterExtraText = $entity->getLetterExtraText();
        $model->clinicianInfoText = $entity->getClinicianInfoText();
        $model->text7 = $entity->getText7();
        $model->text8 = $entity->getText8();
        $model->text9 = $entity->getText9();
        $model->text10 = $entity->getText10();
        $model->text11 = $entity->getText11();

        $model->doctorsReporting = $entity->getDoctorsReporting();
        $model->automaticReporting = $entity->getAutomaticReporting();
        $model->noReporting = $entity->getNoReporting();
        $model->invoicingAndPaymentInfo = $entity->getInvoicingAndPaymentInfo();
        $model->idDncReportType = $entity->getIdDncReportType();

        $model->priceCategoryId = $entity->getPriceCategoryId();
        $model->reportDeliveryEmail = $entity->getReportDeliveryEmail();
        $model->invoiceDeliveryEmail = $entity->getInvoiceDeliveryEmail();

        $model->invoiceGoesToId = $entity->getInvoiceGoesToId();
        $model->reportGoesToId = $entity->getReportGoesToId();
        $model->reportDeliveryId = $entity->getReportDeliveryId();

        $model->ftpServer = new Business\DistributionChannel\Form\AppFtpServerModel();
        $model->ftpServer->server = $entity->getFtpServerName();
        $model->ftpServer->username = $entity->getFtpUsername();
        $model->ftpServer->password = $entity->getFtpPassword();

        $model->idPreferredPayment = $entity->getIdPreferredPayment();
        $model->idMarginGoesTo = $entity->getIdMarginGoesTo();
        $model->marginOverrideDc = $entity->getMarginOverrideDc();
        $model->marginOverridePartner = $entity->getMarginOverridePartner();

        $model->idNutrimeGoesTo = $entity->getIdNutrimeGoesTo();

        $model->isReportDeliveryDownloadAccess = $entity->getIsReportDeliveryDownloadAccess();
        $model->isReportDeliveryFtpServer = $entity->getIsReportDeliveryFtpServer();
        $model->isReportDeliveryPrintedBooklet = $entity->getIsReportDeliveryPrintedBooklet();
        $model->isReportDeliveryDontChargeBooklet = $entity->getIsReportDeliveryDontChargeBooklet();

        $model->idRecallGoesTo = $entity->getIdRecallGoesTo();
        // $model->canCreateLoginAndSendStatusMessageToUser = $entity->getCanCreateLoginAndSendStatusMessageToUser();
        $model->canCustomersDownloadReports = $entity->getCanCustomersDownloadReports();
        $model->isDelayDownloadReport = $entity->getIsDelayDownloadReport();
        $model->numberDateDelayDownloadReport = $entity->getNumberDateDelayDownloadReport();
        $model->isDisplayInCustomerDashboard = $entity->getIsDisplayInCustomerDashboard();

        $model->idAcquisiteur1 = $entity->getIdAcquisiteur1();
        $model->acquisiteurCommissionRate1 = $entity->getAcquisiteurCommissionRate1();
        $model->idAcquisiteur2 = $entity->getIdAcquisiteur2();
        $model->acquisiteurCommissionRate2 = $entity->getAcquisiteurCommissionRate2();
        $model->idAcquisiteur3 = $entity->getIdAcquisiteur3();
        $model->acquisiteurCommissionRate3 = $entity->getAcquisiteurCommissionRate3();


        $model->rebrandingNick = $entity->getRebrandingNick();
        $model->headerFileName = $entity->getHeaderFileName();
        $model->footerFileName = $entity->getFooterFileName();
        $model->titleLogoFileName = $entity->getTitleLogoFileName();
        $model->boxLogoFileName = $entity->getBoxLogoFileName();

        // Status
        $model->statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid = :customerId')
            ->setParameter('customerId', $entity->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Form\AppStatusRowModel();
            $m->id = $status->getId();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->isNotFilterable = $status->getIsNotFilterable();
            $model->statusList[] = $m;
        }

        // Order info
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $customerFilters = array('customerid' => $entity->getId());
        $model->orderInfo = array();
        $model->orderInfo['categories'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedCategory', $customerFilters, 'CategoryId', 'CategoryId');
        $model->orderInfo['products'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedProduct', $customerFilters, 'ProductId', 'ProductId');
        $model->orderInfo['specialProducts'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedSpecialProduct', array('idCustomer' => $entity->getId()), 'idSpecialProduct', 'idSpecialProduct');

        $questionProductId = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductQuestion')->findAll();
        foreach ($result as $row) {
            $questionProductId[$row->getId()] = $row->getProductId();
        }
        $filters = array('customerid' => $entity->getId());
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerQuestion')->findBy($filters);
        foreach ($result as $row) {
			if (isset($questionProductId[$row->getQuestionId()])) {
                $answer = $row->getAnswer();
                if (json_decode($answer, true) !== NULL) {
                    $answer = json_decode($answer, true);
                }
				$model->orderInfo['product_question_'.$questionProductId[$row->getQuestionId()].'_'.$row->getQuestionId()] = $answer;
			}
        }

        // Form Question
        $model->questions = Util::loadFormQuestions($this->container, $em, $entity);

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('id', 'hidden');
        $builder->add('idVisibleGroupProduct', 'hidden');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'customers/'.$this->attachmentKey
        ));

        // new customer info
        $builder->add('sectionCustomerInfo', 'section', array(
            'label'    => 'Customer info',
            'property_path' => false
        ));
        $builder->add('customerNumber', 'text', array(
            'label'    => 'Customer number',
            'required' => true
        ));
        $builder->add('externalBarcode', 'text', array(
            'label'    => 'External barcode',
            'required' => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => true,
            'empty_value' => false,
            'choices'  => array('' => '') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeDistributionChannel()'
            )
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('previousOrderNumber', 'text', array(
            'label'    => 'Previous Order Number',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List'),
            'expanded' => 'expanded'
        ));
        $builder->add('dateOfBirth', 'datepicker', array(
            'label'    => 'Date of birth',
            'required' => false
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));
        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));
        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));
        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        $builder->add('detailInformation', 'textarea', array(
            'label'    => 'Customer information',
            'required' => false,
            'attr'     => array(
                'rows'   => 10,
                'style'  => 'width: 95%'
            )
        ));
        $builder->add('otherOrders', 'label', array(
            'label'    => 'Orders',
            'required' => false
        ));

        // other data
        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order',
            'property_path' => false
        ));
        if ($this->entity->getIdOriginalCustomer() != 0) {
            $builder->add('oldBarcode', 'label', array(
                'label'    => 'Copied from',
                'required' => false
            ));
            $builder->add('copyHistory', 'app_copy_history', array(
                'label'    => 'Copy history',
                'required' => false,
                'idOriginalCustomer' => $this->entity->getIdOriginalCustomer()
            ));
        }
        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => true
        ));
        $builder->add('trackingCode', 'text', array(
            'label'    => 'Tracking Code',
            'required' => false
        ));
        $builder->add('dnaSampleOrderNumberVario', 'text', array(
            'label'    => 'DNA sample Order Number for VARIO',
            'required' => false
        ));
        $builder->add('orderInfoText', 'textarea', array(
            'label'    => 'Order information',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('dateOrdered', 'datepicker', array(
            'label'    => 'Date Ordered',
            'required' => false
        ));
        $builder->add('orderAge', 'text', array(
            'label'    => 'Order Age',
            'required' => false
        ));
        $builder->add('laboratoryDetails', 'textarea', array(
            'label'    => 'Laboratory Details',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
        $builder->add('isDisplayInCustomerDashboard', 'checkbox', array(
            'label'    => "Don't show in Weblogin",
            'required' => false
        ));
        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));
        $builder->add('isDisplayInCustomerDashboard', 'checkbox', array(
            'label'    => "Don't show in Weblogin",
            'required' => false
        ));

        // SECTION Report Delivery Options
        $builder->add('sectionReportDeliveryOptions', 'section', array(
            'label'    => 'Report Delivery Options',
            'property_path' => false
        ));
        $builder->add('isReportDeliveryDownloadAccess', 'checkbox', array(
            'label'    => 'Download access',
            'required' => false
        ));
        $builder->add('isReportDeliveryFtpServer', 'checkbox', array(
            'label'    => 'FTP server',
            'required' => false
        ));
        $builder->add('isReportDeliveryPrintedBooklet', 'checkbox', array(
            'label'    => 'Printed booklet',
            'required' => false
        ));
        $builder->add('isReportDeliveryDontChargeBooklet', 'checkbox', array(
            'label'    => 'Don\'t charge booklet',
            'required' => false
        ));

        $builder->add('sectionAddress', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('street2', 'text', array(
            'label'    => 'Street 2',
            'required' => false
        ));
        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'cols' => 80, 'rows' => 10
            )
        ));

        // Invoice
        $builder->add('sectionInvoice', 'section', array(
            'label'    => 'Invoice',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceAddressClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceAddressCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('invoiceAddressUid', 'text', array(
            'label'    => 'UID',
            'required' => false
        ));

        // Setting
        $builder->add('sectionSetting',         'section', array(
            'label'    => 'Setting',
            'property_path' => false
        ));
        $builder->add('languageId',             'choice', array(
            'label'    => 'Language',
            'expanded' => 'expanded',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // Report details
        $builder->add('sectionReportDetails', 'section', array(
            'label'    => 'Report Details',
            'property_path' => false
        ));
        $builder->add('companyInfo', 'textarea', array(
            'label'    => 'Company Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('laboratoryInfo', 'textarea', array(
            'label'    => 'Laboratory Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactInfo', 'textarea', array(
            'label'    => 'Contact Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactUs', 'textarea', array(
            'label'    => 'Contact Us',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('letterExtraText', 'text', array(
            'label'    => 'Letter Extra Text',
            'required' => false
        ));
        $builder->add('clinicianInfoText', 'text', array(
            'label'    => 'Clinician Info Text',
            'required' => false
        ));
        $builder->add('text7', 'text', array(
            'label'    => 'Text 7',
            'required' => false
        ));
        $builder->add('text8', 'text', array(
            'label'    => 'Text 8',
            'required' => false
        ));
        $builder->add('text9', 'text', array(
            'label'    => 'Text 9',
            'required' => false
        ));
        $builder->add('text10', 'text', array(
            'label'    => 'Text 10',
            'required' => false
        ));
        $builder->add('text11', 'text', array(
            'label'    => 'Text 11',
            'required' => false
        ));


        // GS Setting
        $builder->add('sectionGsSetting', 'section', array(
            'label'    => 'GS Settings',
            'property_path' => false
        ));
        $builder->add('doctorsReporting', 'text', array(
            'label'    => 'Doctors Reporting',
            'required' => false
        ));
        $builder->add('automaticReporting', 'text', array(
            'label'    => 'Automatic Reporting',
            'required' => false
        ));
        $builder->add('noReporting', 'text', array(
            'label'    => 'No Reporting',
            'required' => false
        ));
        $builder->add('invoicingAndPaymentInfo', 'textarea', array(
            'label'    => 'Invoicing And Payment Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 10, 'cols' => 80
            )
        ));
        $builder->add('idDncReportType', 'choice', array(
            'label'    => 'DNC Report Type',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_DncReportType')
        ));

        // DELIVERY
        $builder->add('sectionDelivery',        'section', array(
            'label'    => 'Delivery',
            'property_path'  => false
        ));
        $builder->add('priceCategoryId', 'searchable_box', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('reportDeliveryEmail', 'text', array(
            'label'    => 'Report Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceGoesToId', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('reportGoesToId', 'choice', array(
            'label'    => 'Report Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));
        $builder->add('reportDeliveryId', 'choice', array(
            'label'    => 'Report Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeReportDeliveryId()'
            )
        ));
        $builder->add('ftpServer', 'app_ftp_server', array(
            'label'    => 'FTP Server',
            'required' => false
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe goes to',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));

        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));

        // PRICING
        $builder->add('sectionPricing',        'section', array(
            'label'    => 'Pricing',
            'property_path'  => false
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin goes to',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('marginOverrideDc', 'text', array(
            'label'    => 'Margin override DC (%)',
            'required' => false
        ));
        $builder->add('marginOverridePartner', 'text', array(
            'label'    => 'Margin override partner (%)',
            'required' => false
        ));
        $builder->add('priceOverride',          'text', array(
            'label'    => 'Price-override',
            'required' => false
        ));
        $builder->add('idAcquisiteur1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('idAcquisiteur2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('idAcquisiteur3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));


        // Rebrander
        $builder->add('sectionRebrander', 'section', array(
            'label'    => 'Rebrander',
            'property_path' => false
        ));
        $builder->add('rebrandingNick', 'text', array(
            'label'    => 'Rebranding Nick',
            'required' => false
        ));
        $builder->add('headerFileName', 'text', array(
            'label'    => 'Header File Name',
            'required' => false
        ));
        $builder->add('footerFileName', 'text', array(
            'label'    => 'Footer File Name',
            'required' => false
        ));
        $builder->add('titleLogoFileName', 'text', array(
            'label'    => 'Title Logo File Name',
            'required' => false
        ));
        $builder->add('boxLogoFileName', 'text', array(
            'label'    => 'Box Logo File Name',
            'required' => false
        ));

        // Status
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Form\AppStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        $builder->add('sectionOrderInformation', 'section', array(
            'label'    => 'Order information',
            'property_path' => false
        ));
        $builder->add('orderInfo', 'app_gene_select');

        $builder->add('questions', 'app_question_select');

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $em = $this->container->get('doctrine')->getEntityManager();

        ////////////////////////////////////
        // x. Update Customer Info
        $isValid = true;
        $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($this->entity->getIdCustomerInfo());
        if (strcmp($customerInfo->getCustomerNumber(), $model->customerNumber) != 0) {
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneByCustomerNumber($model->customerNumber);
            if (empty($customerInfo)) {
                $this->getForm()->get('customerNumber')->addError(new FormError("Can't find Customer '".$model->customerNumber."'!"));
                $isValid = false;
            }
        }

        $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findByordernumber($model->orderNumber);
        if (count($customers) > 1 || (isset($customers[0]) && $customers[0]->getId() != $this->entity->getId())) {
            $this->getForm()->get('orderNumber')->addError(new FormError("Order number already existed!"));
            $isValid = false;
        }

        if (!$isValid) {
            $this->errors[] = "Error!";
            return false;
        }

        $customerInfo->setExternalBarcode($model->externalBarcode);
        $customerInfo->setIdDistributionChannel($model->idDistributionChannel);
        $customerInfo->setFirstName($model->firstName);
        $customerInfo->setSurName($model->surName);
        $customerInfo->setEmail($model->email);
        $customerInfo->setDateOfBirth($model->dateOfBirth);
        $customerInfo->setPostCode($model->postCode);
        $customerInfo->setGenderId($model->genderId);
        $customerInfo->setTitle($model->title);
        $customerInfo->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $customerInfo->setIsNutrimeOffered($model->isNutrimeOffered);
        $customerInfo->setIsNotContacted($model->isNotContacted);
        $customerInfo->setIsDestroySample($model->isDestroySample);
        $customerInfo->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);
        $customerInfo->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $customerInfo->setWebLoginGoesTo($model->webLoginGoesTo);

        $customerInfo->setDetailInformation($model->detailInformation);
        $em->persist($customerInfo);
        $em->flush();

        ////////////////////////////////////
        // x. Update Order
        $this->entity->setIdCustomerInfo($customerInfo->getId());
        $this->entity->setOrderNumber($model->orderNumber);
        $this->entity->setTrackingCode($model->trackingCode);
        $this->entity->setPreviousOrderNumber($model->previousOrderNumber);
        // $this->entity->setExternalBarcode($model->externalBarcode);
        $this->entity->setDnaSampleOrderNumberVario($model->dnaSampleOrderNumberVario);
        $this->entity->setOrderInfoText($model->orderInfoText);
        $this->entity->setDomain($model->domain);
        $this->entity->setDateOrdered($model->dateOrdered);
        $this->entity->setOrderAge($model->orderAge);
        $this->entity->setLaboratoryDetails($model->laboratoryDetails);
        $this->entity->setDistributionChannelId($customerInfo->getIdDistributionChannel());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE

        // Report delivery options
        $this->entity->setIsReportDeliveryDownloadAccess($model->isReportDeliveryDownloadAccess);
        $this->entity->setIsReportDeliveryFtpServer($model->isReportDeliveryFtpServer);
        $this->entity->setIsReportDeliveryPrintedBooklet($model->isReportDeliveryPrintedBooklet);
        $this->entity->setIsReportDeliveryDontChargeBooklet($model->isReportDeliveryDontChargeBooklet);

        $this->entity->setStreet($model->street);
        $this->entity->setStreet2($model->street2);
        $this->entity->setCity($model->city);
        $this->entity->setCountryId($model->countryId);
        $this->entity->setTelephone($model->telephone);
        $this->entity->setFax($model->fax);
        $this->entity->setNotes($model->notes);

        $this->entity->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $this->entity->setInvoiceAddressClientName($model->invoiceAddressClientName);
        $this->entity->setInvoiceAddressCompanyName($model->invoiceAddressCompanyName);
        $this->entity->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $this->entity->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $this->entity->setInvoiceAddressCity($model->invoiceAddressCity);
        $this->entity->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $this->entity->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $this->entity->setInvoiceAddressFax($model->invoiceAddressFax);
        $this->entity->setInvoiceAddressUid($model->invoiceAddressUid);
        $this->entity->setPriceOverride($model->priceOverride);

        $this->entity->setLanguageId($model->languageId);

        $this->entity->setCompanyInfo($model->companyInfo);
        $this->entity->setLaboratoryInfo($model->laboratoryInfo);
        $this->entity->setContactInfo($model->contactInfo);
        $this->entity->setContactUs($model->contactUs);
        $this->entity->setLetterExtraText($model->letterExtraText);
        $this->entity->setClinicianInfoText($model->clinicianInfoText);
        $this->entity->setText7($model->text7);
        $this->entity->setText8($model->text8);
        $this->entity->setText9($model->text9);
        $this->entity->setText10($model->text10);
        $this->entity->setText11($model->text11);

        $this->entity->setDoctorsReporting($model->doctorsReporting);
        $this->entity->setAutomaticReporting($model->automaticReporting);
        $this->entity->setNoReporting($model->noReporting);
        $this->entity->setInvoicingAndPaymentInfo($model->invoicingAndPaymentInfo);
        $this->entity->setIdDncReportType($model->idDncReportType);

        $this->entity->setPriceCategoryId($model->priceCategoryId);
        $this->entity->setReportDeliveryEmail($model->reportDeliveryEmail);
        $this->entity->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);

        $this->entity->setInvoiceGoesToId($model->invoiceGoesToId);
        $this->entity->setReportGoesToId($model->reportGoesToId);
        $this->entity->setReportDeliveryId($model->reportDeliveryId);

        if ($model->ftpServer != null) {
            $this->entity->setFtpServerName($model->ftpServer->server);
            $this->entity->setFtpUsername($model->ftpServer->username);
            $this->entity->setFtpPassword($model->ftpServer->password);
        }

        $this->entity->setIdPreferredPayment($model->idPreferredPayment);
        $this->entity->setIdMarginGoesTo($model->idMarginGoesTo);
        $this->entity->setMarginOverrideDc($model->marginOverrideDc);
        $this->entity->setMarginOverridePartner($model->marginOverridePartner);

        $this->entity->setIdNutrimeGoesTo($model->idNutrimeGoesTo);
        // $this->entity->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
        $this->entity->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
        if ($model->isDelayDownloadReport == true) {
            if ($this->entity->getIsDelayDownloadReport() != true) {
                $this->entity->setDateSetDelayDownloadReport(new \DateTime('now'));
            }
            $this->entity->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }
        else {
            $this->entity->setDateSetDelayDownloadReport(null);
            $this->entity->setNumberDateDelayDownloadReport(null);
        }
        $this->entity->setIsDelayDownloadReport($model->isDelayDownloadReport);
        $this->entity->setIsDisplayInCustomerDashboard($model->isDisplayInCustomerDashboard);

        $this->entity->setIdRecallGoesTo($model->idRecallGoesTo);


        $this->entity->setIdAcquisiteur1($model->idAcquisiteur1);
        $this->entity->setAcquisiteurCommissionRate1($model->acquisiteurCommissionRate1);
        $this->entity->setIdAcquisiteur2($model->idAcquisiteur2);
        $this->entity->setAcquisiteurCommissionRate2($model->acquisiteurCommissionRate2);
        $this->entity->setIdAcquisiteur3($model->idAcquisiteur3);
        $this->entity->setAcquisiteurCommissionRate3($model->acquisiteurCommissionRate3);

        // Rebrander
        $this->entity->setRebrandingNick($model->rebrandingNick);
        $this->entity->setHeaderFileName($model->headerFileName);
        $this->entity->setFooterFileName($model->footerFileName);
        $this->entity->setTitleLogoFileName($model->titleLogoFileName);
        $this->entity->setBoxLogoFileName($model->boxLogoFileName);

        $this->entity->setIsRecheckGeocoding(1);

        // Save status
        $result = $em->getRepository('AppDatabaseMainBundle:OrderedStatus')->findByCustomerid($this->entity->getId());
        $lastStatusList = [];
        foreach ($result as $item) {
            $lastStatusList[$item->getId()] = $item;
        }

        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            if (isset($lastStatusList[$statusRow->id])) {
                $status = $lastStatusList[$statusRow->id];
                unset($lastStatusList[$statusRow->id]);
            } else {
                $status = new Entity\OrderedStatus();
            }

            $status->setCustomerId($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setIsNotFilterable($statusRow->isNotFilterable);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }
        $this->entity->setStatus($lastStatus);
        $this->entity->setStatusDate($lastDate);
        $em->flush();

        foreach ($lastStatusList as $status) {
            $em->remove($status);
            $em->flush();
        }

        ////////////////////////////////////
        // x. Update other information
        Business\CustomerInfo\Utils::ensureDuplication($em, $customerInfo);

        Util::updateStatusList($this->container, $this->entity->getId());
        // Save order information
        Util::removeOrderInformation($this->container, $em, $this->entity);
        Util::saveOrderInformation($this->container, $em, $this->entity, $model->orderInfo);
        Util::saveFormQuestions($this->container, $em, $this->entity, $model->questions);
        Util::updateRevenueEstimation($this->container, $this->entity->getId());
        parent::onSuccess();

        $this->reloadForm();
    }

    private function isUniqueCode($em, $externalBarcode, $customerInfoId, $customerId)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from('AppDatabaseMainBundle:Customer','p')
                ->where($query->expr()->andX(
                    $query->expr()->eq('p.externalBarcode', ':externalBarcode'),
                    $query->expr()->neq('p.id', ':customerId')))
                ->setParameter('externalBarcode', $externalBarcode)
                ->setParameter('customerId', $customerId);
        $results = $query->getQuery()->getResult();

        if(!empty($results)){
            return false;
        }

        return true;
    }

    private function findExternalBarcode($em, $customerInfoId)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.externalBarcode')
            ->from('AppDatabaseMainBundle:CustomerInfo','p')
            ->where($query->expr()->eq('p.id', ':idCustomerInfo'))
            ->setParameter('idCustomerInfo', $customerInfoId);
        $externalBarcode = $query->getQuery()->getSingleScalarResult();
        return $externalBarcode;
    }
}
