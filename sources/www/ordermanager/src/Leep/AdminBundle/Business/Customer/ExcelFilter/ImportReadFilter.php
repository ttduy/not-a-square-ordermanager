<?php
namespace Leep\AdminBundle\Business\Customer\ExcelFilter;

class ImportReadFilter implements \PHPExcel_Reader_IReadFilter
{
    public $start = 1;
    public $end = 10;
    public function __construct($start, $end) {
        $this->start = $start;
        $this->end = $end;
    }
    public function readCell($column, $row, $worksheetName = '') {
        if ($row >= $this->start && $row <= $this->end) {
            if (in_array($column,range('A', 'D'))) {
                return true;
            }
        }
        return false;
    }
}
