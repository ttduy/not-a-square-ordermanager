<?php
namespace Leep\AdminBundle\Business\Customer;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class OrderProcessor {
    private $container = null;
    private $inputData = null;

    private $processedData = null;

    private $errors = null;
    private $messages = null;

    private $fields = array(
                                'idDistributionChannel' => array(
                                                                'required'  => true,
                                                                'dataType'  => 'integer',
                                                                'dbCheck'  => array(
                                                                                    'entity'   => 'DistributionChannels',
                                                                                    'field'     => 'id'
                                                                                )
                                                            ),
                                'idCustomerInfo'        => array(
                                                                'required'  => true,
                                                                'dataType'  => 'integer',
                                                                'dbCheck'  => array(
                                                                                    'entity'   => 'CustomerInfo',
                                                                                    'field'     => 'id'
                                                                                )
                                                            ),
                                'orderNumber'           => array(
                                                                'required'  => true,
                                                                'dataType'  => 'string'
                                                            ),
                                'products'              => array(
                                                                'required'  => true,
                                                                'dataType'  => 'array',
                                                                'dbCheck'  => array(
                                                                                    'entity'   => 'ProductDnaPlus',
                                                                                    'field'     => 'id'
                                                                                )
                                                            ),
                                'specialProducts'       => array(
                                                                'required'  => true,
                                                                'dataType'  => 'array',
                                                                'dbCheck'  => array(
                                                                                    'entity'    => 'SpecialProduct',
                                                                                    'field'     => 'id'
                                                                                )
                                                            )
                            );

    public function __construct($container) {
        $this->container = $container;
    }

    public function setData($data) {
        $this->inputData = $data;
    }

    public function createOrderFromData() {
        self::validateInputData();
        self::createCustomerInfo();
        self::createOrder();
        self::createReturnInfo();
    }

    /*
    * PRIVATE FUNCTIONs
    */
    private function validateInputData() {
        $blnOk = true;
        foreach ($this->fields as $field => $fieldConstraints) {
            $inputFieldValue = null;
            if (array_key_exists($field, $this->inputData)) {
                $inputFieldValue = $this->inputData[$field];
            }
            
            // validation flow
            $arrValidations = array(
                                    'isRequired',
                                    'isValidDataType',
                                    'isRequiredInDB'
                                );

            // loop through validations
            foreach ($arrValidations as $eachValidation) {
                if (!$eachValidation($inputFieldValue, $fieldConstraints)) {
                    $blnOk = false;
                    break;
                }                
            }
        }
    }

    private function isRequiredInDB($inputFieldValue, $fieldConstraints) {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $blnOk = true;
        if (array_key_exists('dbCheck', $fieldConstraints)) {
            $dbCheckConstraints = $fieldConstraints['dbCheck'];
            
            $separator = ',';
            if (array_key_exists('separator', $dbCheckConstraints)) {
                $separator = $dbCheckConstraints['separator'];
            }            

            $field = 'id';
            if (array_key_exists('id', $dbCheckConstraints)) {
                $field = $dbCheckConstraints['id'];
            }

            $arrInputValues = explode($separator, $inputFieldValue);

            $query = $em->createQueryBuilder();
            $total = $query->select('count(p.id)')
                            ->from('AppDatabaseMainBundle:' . $dbCheckConstraints['entity'], 'p')
                            ->andWhere(sprintf('p.%s IN (:%s)')
                            ->setParameter($field, $arrInputValues)
                            ->getQuery()
                            ->getSingleScalaResult();

            if ($total != sizeof($arrInputValues)) {
                $blnOk = false;
            }
        }
        return $blnOk;
    }

    private function isRequired($inputFieldValue, $fieldConstraints) {
        $blnOk = true;
        if (array_key_exists('required', $fieldConstraints) &&  $fieldConstraints['required'] === true) {
            if (!$inputFieldValue) {
                $blnOk = false;
                break;
            }
        }
        return $blnOk;
    }

    private function isValidDataType($inputFieldValue, $fieldConstraints) {
        $blnOk = true;
        if (array_key_exists('dataType', $fieldConstraints)) {
            if ($inputFieldValue) {
                switch ($fieldConstraints['dataType']) {
                    case 'integer':
                        if (!is_numeric($inputFieldValue) || !is_int($inputFieldValue)) {
                            $blnOk = false;
                        }
                        break;
                    case 'array':
                        if (!is_array($inputFieldValue)) {
                            $blnOk = false;
                        }
                        break;
                    case 'string':
                        break;
                }
            }            
        }

        return $blnOk;
    }

    public function createCustomerInfo() {

    }

    public function createOrder() {
        
    }

    public function createReturnInfo() {
        
    }
}