<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class GridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $dcCell = array();
    public $dcPartners = array();
    public $stopShippingDC = array();
    public $customerReportInfo = array();

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            }
            else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }

        // Check stop shipping
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('dc.id')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'dc')
            ->leftJoin('AppDatabaseMainBundle:Partner', 'p', 'WITH', 'dc.partnerid = p.id')
            ->andWhere('dc.isStopShipping = 1 OR p.isStopShipping = 1');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $this->stopShippingDC[$r['id']] = 1;
        }
    }

    public function getColumnMapping() {
        //return array('doubleChecked', 'orderNumber', 'name', 'distributionChannelId', 'age', 'status', 'complaints', 'invoice', 'report', 'orderInfoText', 'action');
        return array('doubleChecked', 'orderNumber', 'name', 'distributionChannelId', 'age', 'status', 'invoice', 'report', 'orderInfoText', 'action');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.id';
        $this->columnSortMapping[] = 'p.ordernumber';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.dateordered';
        $this->columnSortMapping[] = 'p.status';

        return $this->columnSortMapping;
    }

    public function getTableHeader() {
        return array(
            array('title' => 'DC',                   'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Order Number',         'width' => '8%', 'sortable' => 'true'),
            array('title' => 'Customer Name',        'width' => '9%', 'sortable' => 'false'),
            array('title' => 'Distribution Channel', 'width' => '9%', 'sortable' => 'true'),
            array('title' => 'Age',                  'width' => '4%', 'sortable' => 'true'),
            array('title' => 'Status',               'width' => '8%', 'sortable' => 'true'),
            //array('title' => 'Complaints',           'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Invoice',              'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Report',               'width' => '8%', 'sortable' => 'false'),
            array('title' => 'Info',                 'width' => '4%', 'sortable' => 'false'),
            array('title' => 'Action',               'width' => '9%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CustomerGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');

        $customerFilterBuilder = $this->container->get('leep_admin.customer.business.customer_filter_builder');
        $customerFilterBuilder->buildQuery($queryBuilder, $this->filters);
    }



    public function buildCellAction($row) {
        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();


        if (!$this->isLocked($row)) {
            $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'customer', 'view', 'view', array('id' => $row->getId())), 'button-detail');
            $builder->addPopupButton('Notes', $mgr->getUrl('leep_admin', 'customer', 'edit_activity', 'edit', array('id' => $row->getId())), 'button-activity');
            $builder->addButton('Export', $mgr->getUrl('leep_admin', 'customer', 'customer_export', 'export', array('orderNumber' => $row->getOrderNumber())), 'button-export');

            if ($helperSecurity->hasPermission('customerModify')) {
                $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
                //$builder->addPopupButton('Attachment', $mgr->getUrl('leep_admin', 'customer', 'attachment', 'show', array('id' => $row->getId())), 'button-attachment');
                $builder->addButton('Copy', $mgr->getUrl('leep_admin', 'customer', 'copy', 'edit', array('id' => $row->getId())), 'button-copy');
                $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'customer', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
            }
        } else {
            if ($row->getIdCustomerInfo()) {
                if ($helperSecurity->hasPermission('customerInfoModify')) {
                    $builder->addPopupButton('Unlock', $mgr->getUrl('leep_admin', 'customer_info', 'unlock', 'edit', array('id' => $row->getIdCustomerInfo(), 'idOrder' => $row->getId())), 'button-unlock');
                }
            }
        }

        return $builder->getHtml();
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }
    public function buildCellInvoice($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        if ($row->getCollectiveInvoiceId() != 0) {
            $collectiveInvoice = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findOneById($row->getCollectiveInvoiceId());

            $collectiveInvoiceStatus = $collectiveInvoice->getInvoiceStatus();
            if ($helperSecurity->hasPermission('collectiveInvoiceModify')) {
                $builder->addButton('View collective invoice', $mgr->getUrl('leep_admin', 'collective_invoice', 'invoice_form_edit', 'list', array('id' => $row->getCollectiveInvoiceId())), 'button-detail');
            }

            $collectiveInvoiceStatus = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Invoice_Status', $collectiveInvoiceStatus);
            if ($collectiveInvoiceStatus != '') $collectiveInvoiceStatus .= '&nbsp;&nbsp;';

            $age = $this->getAge($collectiveInvoice->getStatusDate());

            return (($age === '') ? '': "(${age}d) ").$collectiveInvoiceStatus.$builder->getHtml();
        }
        else {
            if ($row->getInvoiceGoesToId() == Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER) {
                if ($helperSecurity->hasPermission('customerModify')) {
                    $builder->addButton('Update invoice', $mgr->getUrl('leep_admin', 'customer_invoice', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
                }


                if ($row->getInvoiceNumber() != '') {
                    $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'customer_invoice', 'customer_form_pdf',  'pdf',  array('id' => $row->getId())), 'button-pdf');
                    //$builder->addPopupButton('Email', $mgr->getUrl('leep_admin', 'email', 'email',  'create', array('id' => $row->getId(), 'mode' => 'customer')), 'button-email');
                }

                $status = $formatter->format($row->getInvoiceStatus(), 'mapping', 'LeepAdmin_CustomerInvoice_Status');
                if ($status != '') $status.= '&nbsp;&nbsp;';

                $age = $this->getAge($row->getInvoiceStatusDate());

                return (($age === '') ? '': "(${age}d) ").$status.$builder->getHtml();
            }
        }
        return '';
    }

    public function buildCellEmail($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $html = '';

        if (!$this->isLocked($row)) {
            $builder->addPopupButton('Email', $mgr->getUrl('leep_admin', 'email', 'email',  'create', array('id' => $row->getId(), 'mode' => 'customer')), 'button-email');
            $html = $builder->getHtml();
        }

        return $html;
    }

    public function buildCellStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builderHtml = '';

        if (!$this->isLocked($row)) {
            $helperSecurity = $this->container->get('leep_admin.helper.security');
            if ($helperSecurity->hasPermission('customerModify')) {
                $builder->addButton('Update status', $mgr->getUrl('leep_admin', 'customer', 'edit_status', 'edit', array('id' => $row->getId())), 'button-edit');
            }

            $builderHtml = $builder->getHtml();
        }

        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_Customer_Status', $row->getStatus());

        $age = $this->getAge($row->getStatusDate());
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builderHtml;
    }

    public function buildCellName($row) {
        $name = '';
        $name .= '<input type="hidden" class="customer_color" color-code="'.$row->getColorCode().'" />';
        if ($this->isLocked($row)) {
            return $name.'-- Deleted --';
        }
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

    public function buildCellDoubleChecked($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $helperSecurity = $this->container->get('leep_admin.helper.security');

        $html = '';

        if (!$this->isLocked($row)) {
            if ($row->getIsDoubleChecked()) {
                $builder->addImage('Double checked', 'button-checked');
            }
            else {
                if ($helperSecurity->hasPermission('customerModify')) {
                    $builder->addButton('Perform double check', $mgr->getUrl('leep_admin', 'customer', 'double_check', 'create', array('id' => $row->getId())), 'button-double-check');
                }
            }

            $html = $builder->getHtml();
        }

        return $html;
    }

    public function buildCellAge($row) {
        $age = $this->getAge($row->getDateOrdered());
        if ($age !== '') {
            return $age.'d';
        }
        return '';
    }

    public function buildCellDistributionChannelId($row) {
        return isset($this->dcCell[$row->getDistributionChannelId()]) ? $this->dcCell[$row->getDistributionChannelId()] : '';
    }

    public function buildCellOrderInfoText($row) {
        $html = '';
        if (!$this->isLocked($row)) {
            $infoText = $row->getOrderInfoText();
            if (!empty($infoText)) {
                $html .='<a class="simpletip"><div class="button-detail"></div><div class="hide-content">'.nl2br($infoText).'</div></a>';
            }
            $laboratoryDetails = $row->getLaboratoryDetails();
            if (!empty($laboratoryDetails)) {
                $html .='<a class="simpletip"><div class="button-activity"></div><div class="hide-content">'.nl2br($laboratoryDetails).'</div></a>';
            }
            if ($row->getLockDate() != null) {
                $today = new \DateTime();
                $today->setTime(0, 0, 0);
                $lockDate = $row->getLockDate();
                $lockDate->setTime(0, 0, 0);
                $day = round(($lockDate->getTimestamp() - $today->getTimestamp()) / 86400);
                $html .='<a class="simpletip"><div class="button-unlock"></div><div class="hide-content">Locked in '.$day.' day(s)</div></a>';
            }
        }
        return $html;
    }
/*
    public function buildCellComplaints($row) {
        $mapping = $this->container->get('easy_mapping');
        $title = $mapping->getMappingTitle('LeepAdmin_ComplaintStatus', $row->getComplaintStatus());

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer', 'edit_complaint', 'edit', array('id' => $row->getId())), 'button-edit');

        $html = $builder->getHtml().'&nbsp;&nbsp;'.$title;

        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'customer', 'edit_feedback', 'edit', array('id' => $row->getId())), 'button-checked');
        $html .= '<br/>'.$builder->getHtml().'&nbsp;&nbsp'.intval($row->getNumberFeedback()).' feedback(s)';

        return $html;
    }
*/

    public function buildCellReport($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $html = '';
        if (!$this->isLocked($row)) {
            $builder->addPopupButton('Add all report', $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'buildAllReport', array('id' => $row->getId())), 'button-export');

            $builder->addPopupButton('View reports', $mgr->getUrl('leep_admin', 'customer_report', 'grid', 'list', array('id' => $row->getId())), 'button-pdf');
            $html = $builder->getHtml();

            $info = $this->customerReportInfo[$row->getId()];
            $html .= '&nbsp;&nbsp;'.$info['currentReport'].' (of '.$info['availableReport'].')';
            if ($info['isInprogress']) {
                $html .= '&nbsp;&nbsp;<div class="button-clock"></div>';
            }

            $idDc = $row->getDistributionChannelId();
            if (isset($this->stopShippingDC[$idDc])) {
                $html .= '&nbsp;&nbsp;<div class="button-delete"></div>';
            }
        }

        return $html;
    }

    public function preLoading() {
        if (empty($this->idArray)) {
            $this->idArray = array(0);
        }

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $customerAvailableReports = array();

        // Find available special reports
        $customerHasSpecialReports = array();
        $query = $em->createQueryBuilder();
        $query->select('op.idCustomer, r.id')
            ->from('AppDatabaseMainBundle:OrderedSpecialProduct', 'op')
            ->innerJoin('AppDatabaseMainBundle:ReportSpecialProduct', 'rp', 'WITH', 'op.idSpecialProduct = rp.idSpecialProduct')
            ->innerJoin('AppDatabaseMainBundle:Report', 'r', 'WITH', 'rp.idReport = r.id')
            ->andWhere('op.idCustomer in (:customerArr)')
            ->setParameter('customerArr', $this->idArray);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idCustomer = $r['idCustomer'];
            $idReport = $r['id'];

            if (!isset($customerAvailableReports[$idCustomer])) {
                $customerAvailableReports[$idCustomer] = array();
            }
            $customerAvailableReports[$idCustomer][$idReport] = 1;

            // Mark as "have special reports"
            $customerHasSpecialReports[$idCustomer] = 1;
        }

        // Find available reports
        $query = $em->createQueryBuilder();
        $query->select('op.customerid, r.id')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'op')
            ->innerJoin('AppDatabaseMainBundle:ReportProduct', 'rp', 'WITH', 'op.productid = rp.idProduct')
            ->innerJoin('AppDatabaseMainBundle:Report', 'r', 'WITH', 'rp.idReport = r.id')
            ->andWhere('op.customerid in (:customerArr)')
            ->setParameter('customerArr', $this->idArray);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idCustomer = $r['customerid'];
            $idReport = $r['id'];

            if (!isset($customerAvailableReports[$idCustomer])) {
                $customerAvailableReports[$idCustomer] = array();
            }

            if (!isset($customerHasSpecialReports[$idCustomer])) {
                $customerAvailableReports[$idCustomer][$idReport] = 1;
            }
        }

        // Find reports relating to selected DC
        $query = $em->createQueryBuilder();
        $query->select('p.id as idCustomer, rp.id as idReport')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->innerJoin('AppDatabaseMainBundle:DistributionChannels', 'dc', 'WITH', 'p.distributionchannelid = dc.id')
            ->innerJoin('AppDatabaseMainBundle:VisibleGroupReportReport', 'vgr', 'WITH', 'vgr.idVisibleGroupReport = dc.idVisibleGroupReport')
            ->innerJoin('AppDatabaseMainBundle:Report', 'rp', 'WITH', 'rp.id = vgr.idReport')
            ->andWhere('p.id in (:customerArr)')
            ->setParameter('customerArr', $this->idArray);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idCustomer = $r['idCustomer'];
            $idReport = $r['idReport'];

            if (!isset($customerAvailableReports[$idCustomer])) {
                $customerAvailableReports[$idCustomer] = array();
            }

            $customerAvailableReports[$idCustomer][$idReport] = 1;
        }

        // Find current reports
        $customerCurrentReports = array();
        $query = $em->createQueryBuilder();
        $query->select('p.idCustomer, p.idReport')
            ->from('AppDatabaseMainBundle:CustomerReport', 'p')
            ->andWhere('p.idCustomer in (:customerArr)')
            ->setParameter('customerArr', $this->idArray)
            ->groupBy('p.idCustomer, p.idReport');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idCustomer = $r['idCustomer'];
            $idReport = $r['idReport'];
            if (!isset($customerCurrentReports[$idCustomer])) {
                $customerCurrentReports[$idCustomer] = 0;
            }

            if (isset($customerAvailableReports[$idCustomer])) {
                if (isset($customerAvailableReports[$idCustomer][$idReport])) {
                    $customerCurrentReports[$idCustomer]++;
                }
            }
        }

        // Find inprogress reports
        $inprogressReports = array();
        $query = $em->createQueryBuilder();
        $query->select('p.idCustomer')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->andWhere('p.idCustomer in (:customerArr)')
            //->andWhere('p.isSaved = 1')
            ->andWhere('(p.status = :statusInprogress) OR (p.status = :statusPending)')
            ->setParameter('statusPending', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING)
            ->setParameter('statusInprogress', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING)
            ->setParameter('customerArr', $this->idArray);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idCustomer = $r['idCustomer'];
            $inprogressReports[$idCustomer] = 1;
        }
        // Integrate
        foreach ($this->idArray as $idCustomer) {
            $this->customerReportInfo[$idCustomer] = array(
                'availableReport' => isset($customerAvailableReports[$idCustomer]) ? count($customerAvailableReports[$idCustomer]) : 0,
                'currentReport'   => isset($customerCurrentReports[$idCustomer]) ? $customerCurrentReports[$idCustomer] : 0,
                'isInprogress'    => isset($inprogressReports[$idCustomer]) ? true : false
            );
        }
    }

    private function isLocked($row) {
        $blnLocked = false;
        if ($row->getIsLocked()) {
            $blnLocked = true;
        }

        return $blnLocked;
    }
}
