<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\CrudBundle\Grid\StaticGridDataReader;
use App\Database\MainBundle\Entity;

class ImportGridReader extends StaticGridDataReader {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getTableHeader() {
        return array(        
            array('title' => 'Order Number',    'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Domain',          'width' => '7%', 'sortable' => 'false'),            
            array('title' => 'Date Ordered',    'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Title',           'width' => '7%', 'sortable' => 'false'),            
            array('title' => 'First Name',      'width' => '7%', 'sortable' => 'false'),            
            array('title' => 'Sur Name',        'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Street',          'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Street 2',        'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Post Code',       'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Country',         'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Email',           'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Telephone',       'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Birthday',        'width' => '7%', 'sortable' => 'false'),
            array('title' => 'Gender',          'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Products',        'width' => '20%', 'sortable' => 'false'),
            array('title' => 'Categories',      'width' => '20%', 'sortable' => 'false')
        );
    }

    public function setData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $file = Util::getImportFile($this->container);

        if ($file != '') {
            @ini_set('memory_limit', '1024M');        
            @ini_set('max_execution_time', '120');

            $validator = $this->container->get('leep_admin.customer.business.import_validator');
            $validator->initialize();

            $raw = array();
            $result = Util::parseCvs($this->container, $file, $raw);
            $this->data = array();
            foreach ($result as $row) {
                $validator->validate($row);

                $this->data[] = array(
                    $row['orderNumber'],
                    $row['domain'],
                    $row['dateOrdered'],
                    $row['title'],
                    $row['firstName'],
                    $row['surName'],
                    $row['street'],
                    $row['street2'],
                    $row['postCode'],
                    $row['country'],
                    $row['email'],
                    $row['telephone'],
                    $row['dateOfBirth'],
                    $row['gender'],
                    $row['products'],
                    $row['categories']
                );
            }
        }
    }

}