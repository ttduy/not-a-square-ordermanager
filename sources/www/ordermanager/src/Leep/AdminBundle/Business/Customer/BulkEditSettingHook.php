<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\ModuleBundle\Module\AbstractHook;

class BulkEditSettingHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $data['selectedId'] = $controller->get('request')->get('selectedId', '');
    }
}
