<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\ModuleBundle\Module\AbstractHook;

class DoubleCheckHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $idOrder = $controller->get('request')->get('id', null);
        
        $data['subOrderInfo'] = $idOrder ? Util::getSubOrderInfo($controller, $idOrder) : null;
    }
}
