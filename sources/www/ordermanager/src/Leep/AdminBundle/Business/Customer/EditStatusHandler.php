<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditStatusHandler extends AppEditHandler {
    public $customerId = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $this->customerId = $entity->getId();

        $statusList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid = :customerId')
            ->setParameter('customerId', $entity->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new Form\AppStatusRowModel();
            $m->id = $status->getId();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->isNotFilterable = $status->getIsNotFilterable();
            $statusList[] = $m;
        }

        return array('statusList' => $statusList, 'orderNumber' => $entity->getOrderNumber());
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('section1',          'section', array(
            'label' => 'Customer Order',
            'property_path' => false
        ));
        $builder->add('orderNumber',            'label', array(
            'label' => 'Order Number',
            'required'      => false
        ));

        $builder->add('section2',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Form\AppStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $result = $em->getRepository('AppDatabaseMainBundle:OrderedStatus')->findByCustomerid($this->entity->getId());
        $lastStatusList = [];
        foreach ($result as $item) {
            $lastStatusList[$item->getId()] = $item;
        }

        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model['statusList'] as $statusRow) {
            if (empty($statusRow)) continue;
            if (isset($lastStatusList[$statusRow->id])) {
                $status = $lastStatusList[$statusRow->id];
                unset($lastStatusList[$statusRow->id]);
            } else {
                $status = new Entity\OrderedStatus();
            }

            $status->setCustomerId($this->customerId);
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setIsNotFilterable($statusRow->isNotFilterable);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }

        $this->entity->setStatus($lastStatus);
        $this->entity->setStatusDate($lastDate);
        $em->flush();

        foreach ($lastStatusList as $status) {
            $em->remove($status);
        }
        $em->flush();

        Util::updateStatusList($this->container, $this->customerId);
        //Util::updateArrivalDate($this->container, $this->customerId);

        parent::onSuccess();
    }
}
