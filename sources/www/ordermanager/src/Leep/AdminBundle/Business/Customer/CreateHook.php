<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\ModuleBundle\Module\AbstractHook;

class CreateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');

        $cacheBox = $controller->get('leep_admin.helper.cache_box_data');
        $data['urlCheckCustomerInfo'] = $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'existCustomerInfo');
        $data['urlViewCustomerInfo'] = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit');
        $data['isCreateHandler'] = '1';
        $data['customerQuickAccesskInformation'] = "";
    }
}
