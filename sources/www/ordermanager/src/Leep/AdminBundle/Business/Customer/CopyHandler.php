<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class CopyHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new CopyModel();
        $model->idCustomer = $entity->getId();
        $model->orderInformation = $entity->getOrderNumber().' ('.$entity->getFirstName().' '.$entity->getSurName().')';

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idCustomer', 'hidden');
        $builder->add('orderInformation', 'label', array(
            'label'    => 'Order Information',
            'required' => false
        ));
        $builder->add('newOrderNumber', 'text', array(
            'label'    => 'New Order Number',
            'required' => true
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $mgr = $this->container->get('easy_module.manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $customer = $this->entity;
        $today = new \DateTime();
        $statusCustomerCopied = 56;

        // Check if new order number existed
        $newCustomer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber(trim($model->newOrderNumber));
        if ($newCustomer) {
            $this->errors[] = $model->newOrderNumber.' is already existed';
            return;
        }

        // Create new order
        $attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();

        $newCustomer = new Entity\Customer();
        $newCustomer->setAttachmentKey($attachmentKey);
        $newCustomer->setOrderNumber(trim($model->newOrderNumber));
        $newCustomer->setTrackingCode(trim($customer->getTrackingCode()));
        $newCustomer->setIdCustomerInfo($customer->getIdCustomerInfo());
        $newCustomer->setDnaSampleOrderNumberVario($customer->getDnaSampleOrderNumberVario());
        $newCustomer->setDistributionChannelId($customer->getDistributionChannelId());
        $newCustomer->setDomain($customer->getDomain());
        $newCustomer->setDateOrdered($today);
        $newCustomer->setOrderAge($customer->getOrderAge());
        $newCustomer->setGenderId($customer->getGenderId());
        $newCustomer->setDateOfBirth($customer->getDateOfBirth());
        $newCustomer->setTitle($customer->getTitle());
        $newCustomer->setFirstName($customer->getFirstName());
        $newCustomer->setSurName($customer->getSurName());
        $newCustomer->setStreet($customer->getStreet());
        $newCustomer->setStreet2($customer->getStreet2());
        $newCustomer->setPostCode($customer->getPostCode());
        $newCustomer->setCity($customer->getCity());
        $newCustomer->setCountryId($customer->getCountryId());
        $newCustomer->setEmail($customer->getEmail());
        $newCustomer->setTelephone($customer->getTelephone());
        $newCustomer->setFax($customer->getFax());
        $newCustomer->setNotes($customer->getNotes());
        $newCustomer->setLanguageId($customer->getLanguageId());
        $newCustomer->setCompanyInfo($customer->getCompanyInfo());
        $newCustomer->setLaboratoryInfo($customer->getLaboratoryInfo());
        $newCustomer->setContactInfo($customer->getContactInfo());
        $newCustomer->setContactUs($customer->getContactUs());
        $newCustomer->setLetterExtraText($customer->getLetterExtraText());
        $newCustomer->setClinicianInfoText($customer->getClinicianInfoText());
        $newCustomer->setText7($customer->getText7());
        $newCustomer->setText8($customer->getText8());
        $newCustomer->setText9($customer->getText9());
        $newCustomer->setText10($customer->getText10());
        $newCustomer->setText11($customer->getText11());
        $newCustomer->setDoctorsReporting($customer->getDoctorsReporting());
        $newCustomer->setAutomaticReporting($customer->getAutomaticReporting());
        $newCustomer->setNoReporting($customer->getNoReporting());
        $newCustomer->setInvoicingAndPaymentInfo($customer->getInvoicingAndPaymentInfo());
        $newCustomer->setPriceCategoryId($customer->getPriceCategoryId());
        $newCustomer->setReportDeliveryEmail($customer->getReportDeliveryEmail());
        $newCustomer->setInvoiceDeliveryEmail($customer->getInvoiceDeliveryEmail());
        $newCustomer->setInvoiceGoesToId($customer->getInvoiceGoesToId());
        $newCustomer->setReportGoesToId($customer->getReportGoesToId());
        $newCustomer->setReportDeliveryId($customer->getReportDeliveryId());
        $newCustomer->setFtpServerName($customer->getFtpServerName());
        $newCustomer->setFtpUserName($customer->getFtpUserName());
        $newCustomer->setFtpPassword($customer->getFtpPassword());
        $newCustomer->setRebrandingNick($customer->getRebrandingNick());
        $newCustomer->setHeaderFileName($customer->getHeaderFileName());
        $newCustomer->setFooterFileName($customer->getFooterFileName());
        $newCustomer->setTitleLogoFileName($customer->getTitleLogoFileName());
        $newCustomer->setBoxLogoFileName($customer->getBoxLogoFileName());
        $newCustomer->setIsDoubleChecked(0);
        //$newCustomer->setOrderInfoText($customer->getOrderInfoText());
        $newCustomer->setArrivalDate($customer->getArrivalDate());
        $newCustomer->setLaboratoryDetails($customer->getLaboratoryDetails());
        $newCustomer->setIsRecheckGeocoding($customer->getIsRecheckGeocoding());
        $newCustomer->setLatitude($customer->getLatitude());
        $newCustomer->setLongitude($customer->getLongitude());
        $newCustomer->setGeocodingStatusCode($customer->getGeocodingStatusCode());
        $newCustomer->setTextAfterDescription($customer->getTextAfterDescription());
        $newCustomer->setTextFooter($customer->getTextFooter());
        $newCustomer->setIdPreferredPayment($customer->getIdPreferredPayment());
        $newCustomer->setColorCode($customer->getColorCode());
        $newCustomer->setIdDncReportType($customer->getIdDncReportType());
        $newCustomer->setIdMarginGoesTo($customer->getIdMarginGoesTo());
        $newCustomer->setIdNutrimeGoesTo($customer->getIdNutrimeGoesTo());
        $newCustomer->setMarginOverrideDc($customer->getMarginOverrideDc());
        $newCustomer->setMarginOverridePartner($customer->getMarginOverridePartner());
        $newCustomer->setPaymentOverrideAcquisiteur1($customer->getPaymentOverrideAcquisiteur1());
        $newCustomer->setPaymentOverrideAcquisiteur2($customer->getPaymentOverrideAcquisiteur2());
        $newCustomer->setPaymentOverrideAcquisiteur3($customer->getPaymentOverrideAcquisiteur3());
        $newCustomer->setExtraPriceReportDeliveryOverride($customer->getExtraPriceReportDeliveryOverride());
        $newCustomer->setStatus($statusCustomerCopied);
        $newCustomer->setStatusList($statusCustomerCopied);
        $newCustomer->setStatusDate($today);

        $newCustomer->setInvoiceAddressIsUsed($customer->getInvoiceAddressIsUsed());
        $newCustomer->setInvoiceAddressCompanyName($customer->getInvoiceAddressCompanyName());
        $newCustomer->setInvoiceAddressClientName($customer->getInvoiceAddressClientName());
        $newCustomer->setInvoiceAddressStreet($customer->getInvoiceAddressStreet());
        $newCustomer->setInvoiceAddressPostCode($customer->getInvoiceAddressPostCode());
        $newCustomer->setInvoiceAddressCity($customer->getInvoiceAddressCity());
        $newCustomer->setInvoiceAddressIdCountry($customer->getInvoiceAddressIdCountry());
        $newCustomer->setInvoiceAddressTelephone($customer->getInvoiceAddressTelephone());
        $newCustomer->setInvoiceAddressFax($customer->getInvoiceAddressFax());

        $newCustomer->setIsReportDeliveryDownloadAccess($customer->getIsReportDeliveryDownloadAccess());
        $newCustomer->setIsReportDeliveryFtpServer($customer->getIsReportDeliveryFtpServer());
        $newCustomer->setIsReportDeliveryPrintedBooklet($customer->getIsReportDeliveryPrintedBooklet());
        $newCustomer->setIsReportDeliveryDontChargeBooklet($customer->getIsReportDeliveryDontChargeBooklet());
        $newCustomer->setIsDelayDownloadReport($customer->getIsDelayDownloadReport());
        $newCustomer->setNumberDateDelayDownloadReport($customer->getNumberDateDelayDownloadReport());

        if ($customer->getIdOriginalCustomer() != 0) {
            $newCustomer->setIdOriginalCustomer($customer->getIdOriginalCustomer());
        }
        else {
            $newCustomer->setIdOriginalCustomer($customer->getId());
        }
        $em->persist($newCustomer);
        $em->flush();

        // Copy file
        $dir = $this->container->getParameter('attachment_dir').'/customers';
        @mkdir($dir.'/'.$newCustomer->getAttachmentKey());
        if ($handle = opendir($dir.'/'.$customer->getAttachmentKey())) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry != '.tmb') {
                    $originalFile = $dir.'/'.$customer->getAttachmentKey().'/'.$entry;
                    $targetFile = $dir.'/'.$newCustomer->getAttachmentKey().'/'.$entry;

                    @copy($originalFile, $targetFile);
                }
            }
        }

        // Make copy history
        $customerCopyHistory = new Entity\CustomerCopyHistory();
        $customerCopyHistory->setIdOriginalCustomer($newCustomer->getIdOriginalCustomer());
        $customerCopyHistory->setCopyDatetime(new \DateTime());
        $customerCopyHistory->setIdFromCustomer($customer->getId());
        $customerCopyHistory->setIdToCustomer($newCustomer->getId());
        $em->persist($customerCopyHistory);
        $em->flush();

        // Make status entry
        $newStatusEntry = new Entity\OrderedStatus();
        $newStatusEntry->setCustomerId($newCustomer->getId());
        $newStatusEntry->setStatusDate($today);
        $newStatusEntry->setStatus($statusCustomerCopied);
        $newStatusEntry->setSortOrder(1);
        $em->persist($newStatusEntry);
        $em->flush();

        $customerStatus = array();
        $result = $em->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBycustomerid($customer->getId());
        foreach ($result as $r) {
            $customerStatus[$r->getStatus()] = 1;
        }

        /*$latestStatus = $statusCustomerCopied;
        $newCustomerStatusList = array($statusCustomerCopied);
        $copyStatusList = array(
            18,   // First details available
            1,    // All details fully registered
            33,   // Double check done
            3,    // Registration complete
            7,    // Results ready
        );
        $sortOrder = 2;
        foreach ($copyStatusList as $status) {
            if (isset($customerStatus[$status])) {
                $newStatusEntry = new Entity\OrderedStatus();
                $newStatusEntry->setCustomerId($newCustomer->getId());
                $newStatusEntry->setStatusDate($today);
                $newStatusEntry->setStatus($status);
                $newStatusEntry->setSortOrder($sortOrder);
                $em->persist($newStatusEntry);
                $sortOrder++;

                $latestStatus = $status;
                $newCustomerStatusList[] = $status;
            }
        }
        $newCustomer->setStatus($latestStatus);
        $newCustomer->setStatusList(implode(',', $newCustomerStatusList));
        $em->flush();*/


        // Customer questions
        $questions = $em->getRepository('AppDatabaseMainBundle:CustomerQuestion')->findBycustomerid($customer->getId());
        foreach ($questions as $question) {
            $newCustomerQuestion = new Entity\CustomerQuestion();
            $newCustomerQuestion->setCustomerId($newCustomer->getId());
            $newCustomerQuestion->setQuestionId($question->getQuestionId());
            $newCustomerQuestion->setAnswer($question->getAnswer());
            $em->persist($newCustomerQuestion);
        }
        $em->flush();

        // New customer questions
        $questions = $em->getRepository('AppDatabaseMainBundle:OrderedQuestion')->findByIdCustomer($customer->getId());
        foreach ($questions as $question) {
            $newQuestion = new Entity\OrderedQuestion();
            $newQuestion->setIdCustomer($newCustomer->getId());
            $newQuestion->setIdFormQuestion($question->getIdFormQuestion());
            $newQuestion->setAnswer($question->getAnswer());
            $em->persist($newQuestion);
        }
        $em->flush();

        // Done
        $url = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $newCustomer->getId()));
        $this->messages[] = 'The customer has been copied. Click <a href="'.$url.'">here</a> to view the copied customer';
    }
}
