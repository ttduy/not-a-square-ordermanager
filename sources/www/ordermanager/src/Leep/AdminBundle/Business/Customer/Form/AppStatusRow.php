<?php
namespace Leep\AdminBundle\Business\Customer\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppStatusRow extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Customer\Form\AppStatusRowModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_status_row';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('id', 'hidden');
        $builder->add('statusDate', 'datepicker', array(
            'label' => 'Date',
            'required'  => false
        ));
        $builder->add('status',     'choice', array(
            'label'  => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'attr' => array(
                'style' => 'min-width: 180px; max-width: 180px'
            )
        ));
        $builder->add('isNotFilterable', 'checkbox', array(
            'label'  => 'Not filterable?',
            'required' => false
        ));
    }
}