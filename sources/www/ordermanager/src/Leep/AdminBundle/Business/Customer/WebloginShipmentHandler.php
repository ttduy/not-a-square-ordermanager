<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class WebloginShipmentHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $id = $this->container->get('request')->query->get('id', 0);
        $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $customer = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());

        $model = new WebloginShipmentModel();
        $model->name = $customer->getCustomerNumber();
        $model->sendTo = $customer->getEmail();

        $webLoginUsername = new Business\FormType\AppUsernameRowModel();
        $webLoginUsername->username = $customer->getWebLoginUsername();
        $model->webLoginUsername = $webLoginUsername;

        $webLoginPassword = new Business\FormType\AppPasswordRowModel();
        $webLoginPassword->password = $customer->getWebLoginPassword();
        $model->webLoginPassword = $webLoginPassword;

        $model->receiverId = $customer->getId();
        $model->isCustomerBeContacted = $customer->getIsCustomerBeContacted();
        $model->isNutrimeOffered = $customer->getIsNutrimeOffered();
        $model->isNotContacted = $customer->getIsNotContacted();
        $model->isDestroySample = $customer->getIsDestroySample();
        $model->sampleCanBeUsedForScience = $customer->getSampleCanBeUsedForScience();
        $model->isFutureResearchParticipant = $customer->getIsFutureResearchParticipant();

        // build email template
        $customerLanguage = Business\CustomerInfo\Utils::getCustomerLanguage($this->container, $customer->getId());
        $arrInputData = array(
            'name'          =>  $customer->getFirstName() . ' ' . $customer->getSurName(),
            'usernamelogin' =>  $customer->getWebLoginUsername(),
            'passwordlogin' =>  $customer->getWebLoginPassword(),
            'idLanguage'    =>  $customerLanguage['id'],
            'type'          =>  1,
            'id'            =>  $customer->getId()
        );

        $emailTemplate = Business\EmailTemplate\Utils::getEmailTemplate($this->container, $arrInputData);
        $model->subject = $emailTemplate['subject'];
        $model->content = $emailTemplate['content'];

        // get previous submitted data
        if ($cacheBox->hasKey('submittedData')) { 
            $submittedData = $cacheBox->getKey('submittedData');
            $model->sendTo = $submittedData->sendTo;
            $model->cc = $submittedData->cc;
            $model->subject = $submittedData->subject;
            $model->content = $submittedData->content;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $mgr = $this->container->get('easy_module.manager');

        $builder->add('name', 'hidden');
        $builder->add('submitLoginInfo', 'hidden');
        $builder->add('submitContactSetting', 'hidden');

        $builder->add('webLoginUsername', 'app_username_row', array(
            'label'    => 'Username',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generateUsername'),
            'name_source_field_id' => 'name'
        ));

        $builder->add('webLoginPassword', 'app_password_row', array(
            'label'    => 'Password',
            'required' => false,
            'url_generator' => $mgr->getUrl('leep_admin', 'customer_info', 'feature', 'generatePassword')
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'     => 'Can be contacted directly',
            'required'  => false
            )
        );

        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'     => 'NutriMe Supplements are offered',
            'required'  => false
            )
        );

        $builder->add('isNotContacted', 'checkbox', array(
            'label'     => 'Customer chose OPT OUT – don’t contact',
            'required'  => false
            )
        );

        $builder->add('isDestroySample', 'checkbox', array(
            'label'     => 'Destroy details and samples after completion',
            'required'  => false
            )
        );

        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'     => 'Sample can be used for science',
            'required'  => false
            )
        );

        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'     => 'Customer agreed to extended research participation',
            'required'  => false
            )
        );

        $builder->add('sectionContent', 'section', array(
            'label'    => 'Send email',
            'property_path' => false
        ));

        $builder->add('sendTo', 'text', array(
            'label'    => 'Send To',
            'required' => true
        ));
        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => true,
            'attr'     => array('cols' => 100, 'rows' => 10)
        ));
    }

    public function onSuccess() {
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $helperCommon = $this->container->get('leep_admin.helper.common');
        $helperActivity = $this->container->get('leep_admin.helper.activity');
        $config = $helperCommon->getConfig();

        // save model
        $cacheBox->putKey('submittedData', $model);

        // save new username and password
        $receiver = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($model->receiverId);

        // get submit type
        if ($model->submitLoginInfo || $model->submitContactSetting) {
            if ($receiver) {
                if ($model->submitLoginInfo) {
                    // check username exist
                    if ($model->webLoginUsername) {
                        $username = $model->webLoginUsername->username;
                        if ($helperCommon->isWebLoginUsernameExisted($username, 'CustomerInfo', $receiver->getId())) {
                            $this->errors[] = "This Web Login Username " . $username . " is already existed";
                            return false;
                        }
                        $receiver->setWebLoginUsername($model->webLoginUsername->username);                        
                    } else {
                        $receiver->setWebLoginUsername(null);
                    }

                    if ($model->webLoginPassword) {
                        $receiver->setWebLoginPassword($model->webLoginPassword->password);    
                    } else {
                        $receiver->setWebLoginPassword(null);
                    }
                    
                }

                if ($model->submitContactSetting) {
                    $receiver->setIsCustomerBeContacted($model->isCustomerBeContacted);
                    $receiver->setIsNutrimeOffered($model->isNutrimeOffered);
                    $receiver->setIsNotContacted($model->isNotContacted);
                    $receiver->setIsDestroySample($model->isDestroySample);
                    $receiver->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
                    $receiver->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);
                }

                $em->persist($receiver);
                $em->flush();

                parent::onSuccess();
                $this->messages = array();
                $this->messages[] = 'The information has been updated';
            }
        } else {
            $isSuccess = false;

            $sendToEmails = $helperCommon->parseEmaiLList($model->sendTo);
            $ccEmails = $helperCommon->parseEmaiLList($model->cc);

            $arrEmails = array_merge($sendToEmails, $ccEmails);
            if (Business\EmailTemplate\Utils::canSendEmail($this->container, $receiver, $arrEmails)) {
                try {
                    $senderEmail = $this->container->getParameter('sender_email');
                    $senderName  = $this->container->getParameter('sender_name');
                    $message = \Swift_Message::newInstance()
                        ->setSubject($model->subject)
                        ->setFrom($senderEmail, $senderName)
                        ->setTo($sendToEmails)
                        ->setCc($ccEmails)
                        ->setBody($model->content)
                        ->addPart(Business\EmailTemplate\Utils::convertToHtmlContent($model->content), 'text/html');
                    $this->container->get('mailer')->send($message);
                    $isSuccess = true;
                    
                    parent::onSuccess();
                    $this->messages = array();
                    $this->messages[] = 'The email has been sent';
                }
                catch (\Exception $e) {
                    $this->errors[] = $e->getMessage();
                    return false;
                }
            } else {
                $this->errors[] = 'Receiver was opted out';
                return false;
            }
        }
    }
}
