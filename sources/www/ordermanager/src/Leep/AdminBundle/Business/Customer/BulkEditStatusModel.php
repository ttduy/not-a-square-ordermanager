<?php
namespace Leep\AdminBundle\Business\Customer;

class BulkEditStatusModel {
    public $selectedOrderNumbers;
    public $currentOrder;
    public $status;
    public $statusDate;
    public $isSendEmail;
}
