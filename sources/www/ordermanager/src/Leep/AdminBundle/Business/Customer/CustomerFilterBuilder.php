<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business;

class CustomerFilterBuilder {
    public $container;
    public $dcPartner = array();
    public function __construct($container) {
        $this->container = $container;
    }

    public function loadDcPartners() {
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        foreach ($results as $r) {
            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => false
        ));        
        $builder->add('dateOrdered', 'datepicker', array(
            'label'    => 'Date Ordered',
            'required' => false
        ));

        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List'),
            'empty_value' => false
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Partner_List'),
            'empty_value' => false
        ));
        $builder->add('acquisiteurId', 'searchable_box', array(
            'label'    => 'Acquisiteur',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List'),
            'empty_value' => false
        ));
        $builder->add('lockedId', 'choice', array(
            'label'    => 'Locked status',
            'required' => false,
            'choices'  => array(0 => 'All', 1 => 'Locked', 2 => 'Not Locked'),
            'empty_value' => false
        ));
        $builder->add('firstname', 'text', array(
            'label'    => 'First Name',
            'required' => false
        ));
        $builder->add('surname', 'text', array(
            'label'    => 'Sur Name',
            'required' => false
        ));
        

        $builder->add('status', 'choice', array(
            'label'    => 'Last Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Customer_Status'),
            'empty_value' => false
        )); 
        $builder->add('invoiceStatus', 'choice', array(
            'label'    => 'Invoice Status',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_Invoice_Status'),
            'empty_value' => false
        )); 

        $builder->add('isDoubleChecked', 'choice', array(
            'label'    => 'Is Double Checked',
            'required' => false,
            'choices'  => array(0 => 'All', '1' => 'Yes', '2' => 'No'),
            'empty_value' => false
        ));         
        $builder->add('showTobeInvoicedCustomers', 'checkbox', array(
            'label'    => 'Show to-be invoiced?',
            'required' => false
        ));

        $builder->add('mustBeStatus', 'choice', array(
            'label'    => 'Must be status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        )); 
        $builder->add('mustNotBeStatus', 'choice', array(
            'label'    => 'Must not be status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        )); 
        $builder->add('mustBeProduct', 'choice', array(
            'label'    => 'Must be product',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        )); 
        $builder->add('mustBeCategory', 'choice', array(
            'label'    => 'Must be category',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Category_List'),
            'multiple' => 'multiple',
            'attr'     => array('style' => 'min-height:100px; max-height: 100px')
        )); 
        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));
        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));
        $builder->add('orderNumberList', 'textarea', array(
            'label'    => 'Order number list',
            'required' => false,
            'attr'     => array('rows' => 5, 'cols' => 50)
        ));
        $builder->add('externalBarcode', 'text', array(
            'label'    => 'External Barcode',
            'required' => false
        )); 
        $builder->add('showMissingInvoiceGoesTo', 'checkbox', array(
            'label'    => 'Show missing "invoice goes to"?',
            'required' => false
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment'),
            'empty_value' => false
        ));
    }

    public function buildQuery($queryBuilder, $filters) {
        $this->loadDcPartners();

        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        if (trim($filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.ordernumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($filters->orderNumber).'%');
        }
        if ($filters->distributionChannelId != 0) {
            $queryBuilder->andWhere('p.distributionchannelid = :distributionChannelId')
                ->setParameter('distributionChannelId', $filters->distributionChannelId);
        }
        if ($filters->partnerId != 0) {
            $dcArr = isset($this->dcPartner[$filters->partnerId]) ? $this->dcPartner[$filters->partnerId] : array();
            $dcArr[] = -1;
            $queryBuilder->andWhere('p.distributionchannelid in (:dcPartnerArr)')
                ->setParameter('dcPartnerArr', $dcArr);
        }
        if ($filters->acquisiteurId != 0) {
            $queryBuilder->andWhere('(p.idAcquisiteur1 = :acquisiteurId OR p.idAcquisiteur2 = :acquisiteurId OR p.idAcquisiteur3 = :acquisiteurId)')
                ->setParameter('acquisiteurId', intval($filters->acquisiteurId));
        }
        if ($filters->lockedId != 0) {
            if ($filters->lockedId == 1) {
                $queryBuilder->andWhere('p.isLocked = 1');
            }
            else {
                $queryBuilder->andWhere('(p.isLocked = 0 OR p.isLocked IS NULL)');
            }
        }
        if ($filters->dateOrdered != null) {
            $queryBuilder->andWhere('p.dateordered = :dateOrdered')
                ->setParameter('dateOrdered', $filters->dateOrdered);
        }
        if ($filters->status != 0) {
            $queryBuilder->andWhere('p.status = :status')
                ->setParameter('status', $filters->status);
        }
        if ($filters->invoiceStatus != 0) {
            $queryBuilder->andWhere('p.invoicestatus = :invoiceStatus')
                ->setParameter('invoiceStatus', $filters->invoiceStatus);
        }
        if ($filters->isDoubleChecked != 0) {
            $isDoubleChecked = 0;
            if ($filters->isDoubleChecked == 1) {
                $isDoubleChecked = 1;
            }
            $queryBuilder->andWhere('p.isdoublechecked = :isDoubleChecked')
                ->setParameter('isDoubleChecked', $isDoubleChecked);
        }
        if (trim($filters->firstname) != '') {
            $queryBuilder->andWhere('p.firstname LIKE :firstname')
                ->setParameter('firstname', '%'.trim($filters->firstname).'%');
        }
        if (trim($filters->surname) != '') {
            $queryBuilder->andWhere('p.surname LIKE :surname')
                ->setParameter('surname', '%'.trim($filters->surname).'%');
        }

        if (!empty($filters->mustBeStatus) || !empty($filters->mustNotBeStatus)) {        
            foreach ($filters->mustBeStatus as $mustBeStatus) {
                $queryBuilder->andWhere('FIND_IN_SET('.$mustBeStatus.', p.statusList) > 0');
            }
            foreach ($filters->mustNotBeStatus as $mustNotBeStatus) {
                $queryBuilder->andWhere('FIND_IN_SET('.$mustNotBeStatus.', p.statusList) = 0');
            }
        }
        if (!empty($filters->mustBeProduct)) {        
            $orCond = array();
            foreach ($filters->mustBeProduct as $idProduct) {
                $orCond[] = '(FIND_IN_SET('.$idProduct.', p.productList) > 0)';                
            }
            $queryBuilder->andWhere('('.implode(' OR ', $orCond).')');
        }
        if (!empty($filters->mustBeCategory)) {        
            $orCond = array();
            foreach ($filters->mustBeCategory as $idCategory) {
                $orCond[] = '(FIND_IN_SET('.$idCategory.', p.categoryList) > 0)';                
            }
            $queryBuilder->andWhere('('.implode(' OR ', $orCond).')');
        }
        if (!empty($filters->dateFrom)) {
            $queryBuilder->andWhere('p.dateordered >= :dateFrom')
                ->setParameter('dateFrom', $filters->dateFrom);
        }
        if (!empty($filters->dateTo)) {
            $queryBuilder->andWhere('p.dateordered <= :dateTo')
                ->setParameter('dateTo', $filters->dateTo);
        }        
        if ($filters->showTobeInvoicedCustomers != 0) {
            $queryBuilder->andWhere('(FIND_IN_SET(8, p.statusList) > 0) OR (FIND_IN_SET(55, p.statusList) > 0)')            
                ->andWhere('(p.invoicegoestoid != '.Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER.' AND (p.collectiveinvoiceid = 0 OR p.collectiveinvoiceid IS NULL)) OR 
                            (p.invoicegoestoid = '.Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER.' AND (p.invoicestatus = 0 OR p.invoicestatus IS NULL))');                            
        }
        if (trim($filters->orderNumberList) != '') {
            $list = explode("\n", $filters->orderNumberList);
            foreach ($list as $k => $v) {
                $list[$k] = trim($v);
            }
            $queryBuilder->andWhere('p.ordernumber in (:orderNumberList)')
                ->setParameter('orderNumberList', $list);
        }
        if (trim($filters->externalBarcode) != '') {
            $queryBuilder->andWhere('p.externalBarcode LIKE :externalBarcode')
                ->setParameter('externalBarcode', '%'.trim($filters->externalBarcode).'%');
        }
        if ($filters->showMissingInvoiceGoesTo != 0) {
            $queryBuilder->andWhere('(p.invoicegoestoid = 0) OR (p.invoicegoestoid IS NULL)');
        }
        if ($filters->idPreferredPayment != 0) {
            $queryBuilder->andWhere('p.idPreferredPayment = :idPreferredPayment')
                ->setParameter('idPreferredPayment', $filters->idPreferredPayment);
        }
    }
}