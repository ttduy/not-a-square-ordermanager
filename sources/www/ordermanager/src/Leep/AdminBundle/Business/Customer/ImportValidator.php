<?php
namespace Leep\AdminBundle\Business\Customer;
use Leep\AdminBundle\Business;

class ImportValidator {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $existingOrderNumber;
    public $existingCountry;
    public $existingGender;
    public $dateFormat;
    public $existingProducts;
    public $existingCategories;
    public $mapping;

    public function initialize() {
        $this->existingOrderNumber = $this->getExistingMapping('AppDatabaseMainBundle:Customer', 'ordernumber');
        $this->existingCountry     = $this->getExistingMapping('AppDatabaseMainBundle:Countries', 'country', 'id', false);
        $this->existingGender      = array('Male' => Business\Base\Constant::GENDER_MALE, 'Female' => Business\Base\Constant::GENDER_FEMALE);
        $this->dateFormat          = 'd.m.Y';
        $this->existingProducts    = $this->getExistingMapping('AppDatabaseMainBundle:ImportConfigProduct', 'code', 'productid', false, 'p.productid != 0 AND p.productid IS NOT NULL');
        $this->existingCategories  = $this->getExistingMapping('AppDatabaseMainBundle:ImportConfigCategory', 'code', 'categoryid', false, 'p.categoryid != 0 AND p.categoryid IS NOT NULL');
        $this->mapping = $this->container->get('easy_mapping');
    }

    public function validate(&$row) {
        $isValid = true;
        if (isset($this->existingOrderNumber[$row['orderNumber']])) {
            $row['orderNumber'] = $this->highlight($row['orderNumber'], 'Order number is already existed');
            $isValid = false;
        }
        if (!empty($row['country'])) {
            if (!isset($this->existingCountry[$row['country']])) {
                $row['country'] = $this->highlight($row['country'], 'This country does not exist');
                $isValid = false;
            }
        }
        if (!isset($this->existingGender[$row['gender']])) {
            $row['gender'] = $this->highlight($row['gender'], 'Invalid gender, expected: "Male" or "Female"');
            $isValid = false;
        }
        if (!empty($row['dateOrdered'])) {
            $dateOrdered = \DateTime::createFromFormat($this->dateFormat, $row['dateOrdered']);
            if ($dateOrdered === false) {
                $row['dateOrdered'] = $this->highlight($row['dateOrdered'], 'Incorrect format, expected: month/date/year');
                $isValid = false;
            }
        }
        if (!empty($row['dateOfBirth'])) {
            $birthday    = \DateTime::createFromFormat($this->dateFormat, $row['dateOfBirth']);
            if ($birthday === false) {
                $row['dateOfBirth'] = $this->highlight($row['dateOfBirth'], 'Incorrect format, expected: month/date/year');
                $isValid = false;
            }
        }
        
        // Validate products
        $products = array();
        $i = 0;
        foreach ($row['products'] as $productCode) {
            if (!isset($this->existingProducts[$productCode])) {
                $productName = $this->highlight($productCode, 'Invalid code');
                $isValid = false;
            }
            else {
                $productId = $this->existingProducts[$productCode];
                $productName = $this->mapping->getMappingTitle('LeepAdmin_Product_List', $productId);
            }
            $i++;
            if ($i % 3 == 0) {
                $productName .= '&nbsp;&nbsp;<br/>';
            }
            else {
                $productName .= '&nbsp;&nbsp;';
            }
            $products[] = $productName;
        }
        $row['products'] = implode('', $products);

        // Validate categories
        $categories = array();
        $i = 0;
        foreach ($row['categories'] as $categoryCode) {
            if (!isset($this->existingCategories[$categoryCode])) {
                $categoryName = $this->highlight($categoryCode, 'Invalid code');
                $isValid = false;
            }
            else {
                $categoryId = $this->existingCategories[$categoryCode];
                $categoryName = $this->mapping->getMappingTitle('LeepAdmin_Category_List', $categoryId);
            }
            $i++;
            if ($i % 3 == 0) {
                $categoryName .= '&nbsp;&nbsp;<br/>';
            }
            else {
                $categoryName .= '&nbsp;&nbsp;';
            }
            $categories[] = $categoryName;
        }
        $row['categories'] = implode('', $categories);
        return $isValid;
    }

    public function process($row) {
        $data = array();
        $data['orderNumber'] = $row['orderNumber'];
        $data['domain']      = $row['domain'];
        if (!empty($row['dateOrdered'])) {
            $data['dateOrdered'] = \DateTime::createFromFormat($this->dateFormat, $row['dateOrdered']);
        }
        else {
            $data['dateOrdered'] = null;
        }
        $data['gender']      = $this->existingGender[$row['gender']];
        if (!empty($row['dateOfBirth'])) {
            $data['dateOfBirth'] = \DateTime::createFromFormat($this->dateFormat, $row['dateOfBirth']);
        }
        else {
            $data['dateOfBirth'] = null;
        }
        $data['title']       = $row['title'];
        $data['firstName']   = $row['firstName'];
        $data['surName']     = $row['surName'];
        $data['street']      = $row['street'];
        $data['street2']     = $row['street2'];
        $data['postCode']    = $row['postCode'];
        $data['city']        = $row['city'];
        if (!empty($data['country'])) {
            $data['country']     = $this->existingCountry[$row['country']];
        }
        else {
            $data['country'] = 0;
        }
        $data['email']       = $row['email'];        
        $data['telephone']   = $row['telephone'];

        $data['products']    = array();
        foreach ($row['products'] as $productCode) {
            $productId = $this->existingProducts[$productCode];
            $data['products'][] = $productId;
        }

        $data['categories']    = array();
        foreach ($row['categories'] as $categoryCode) {
            $categoryId = $this->existingCategories[$categoryCode];
            $data['categories'][] = $categoryId;
        }

        return $data;
    }


    private function highlight($value, $reason) {
        return '<b style="color: red">'.$value."&nbsp;[".$reason.']</b>';
    }

    private function getExistingMapping($repository, $field, $mapField = '', $isDeleted = true, $extraAnd = '') {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select(('p.'.$field.' as field').($mapField != '' ? (', p.'.$mapField.' as map') : ', 1 as map'))
            ->from($repository, 'p');
        if ($isDeleted) {
            $query->andWhere('p.isdeleted = 0');
        }

        if (!empty($extraAnd)) {
            $query->andWhere($extraAnd);
        }

        $mapping = array();
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $mapping[$row['field']] = $row['map'];
        }

        return $mapping;
    }
}