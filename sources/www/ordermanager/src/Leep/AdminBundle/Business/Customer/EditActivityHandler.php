<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditActivityHandler extends AppEditHandler {   
    public $customerId = 0;
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $this->customerId = $entity->getId();

        $model = new EditActivityModel();

        $model->activityList = array();
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerActivity', 'p')
            ->andWhere('p.customerid = :customerId')
            ->setParameter('customerId', $entity->getId())
            ->orderBy('p.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $activity) {
            $m = new Business\FormType\AppActivityRowModel();
            $m->activityDate = $activity->getActivityDate();
            $m->activityTimeId = $activity->getActivityTimeId();
            $m->activityTypeId = $activity->getActivityTypeId();
            $m->notes = $activity->getNotes();
            $model->activityList[] = $m;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionActivity',          'section', array(
            'label' => 'Activities',
            'property_path' => false
        ));
        $builder->add('activityList', 'collection', array(
            'type' => new Business\FormType\AppActivityRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'typeMapping' => 'LeepAdmin_Customer_Activity',
                'required' => false
            )
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();        
        $em = $this->container->get('doctrine')->getEntityManager();

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('customerid' => $this->customerId);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerActivity', $filters);

        $sortOrder = 1;
        
        foreach ($model->activityList as $activityRow) {
            if (empty($activityRow)) continue;
            $activity = new Entity\CustomerActivity();
            $activity->setCustomerId($this->customerId);
            $activity->setActivityDate($activityRow->activityDate);
            $activity->setActivityTimeId($activityRow->activityTimeId);
            $activity->setActivityTypeId($activityRow->activityTypeId);
            $activity->setNotes($activityRow->notes);
            $activity->setSortOrder($sortOrder++);
            $em->persist($activity);            
        }
        $em->flush();

        parent::onSuccess();
    }
}
