<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class AttachmentFeature extends AbstractFeature {
    public function getActions() { return array('create'); }
    public function getDefaultOptions() {
        return array(
            'handler' => ''
        );
    }

    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function handleElfinder($controller, &$data, $options = array()) {
        $key = $controller->getRequest()->get('key', 'default');
        $data['processAttachmentUrl'] = $this->getModule()->getUrl('attachment', 'process', array('key' => $key));
        $data['mode'] = $controller->getRequest()->get('mode', 'normal');
        $data['allowCloneDir'] = $controller->getRequest()->get('allowCloneDir', false);
        $data['processGetAttachmentUrl'] = $this->getModule()->getUrl('attachment', 'getAttachmentUrl', array('key' => $key));
        $data['cloneDirUrl'] = $this->getModule()->getUrl('attachment', 'cloneDir', array('key' => $key));
    }

    public function handleShow($controller, &$data, $options = array()) {
        $id = $controller->getRequest()->get('id', 0);
        $customer = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $key = 'customers/'.$customer->getAttachmentKey();

        $data['orderNumber'] = $customer->getOrderNumber();
        $data['attachmentUrl'] = $this->getModule()->getUrl('attachment', 'elfinder', array('key' => $key));
    }

    public function handleGetAttachmentUrl($controller, &$data, $options = array()) {
        $key = $controller->getRequest()->get('key', 'default');
        return new Response($key);
    }

    public function handleCloneDir($controller, &$data, $options = array()) {
        $key = $controller->getRequest()->get('key', 'default');
        $srcKey = $controller->getRequest()->get('srcKey');

        $dir = $this->container->getParameter('attachment_dir').DIRECTORY_SEPARATOR.$key;
        $srcDir = $this->container->getParameter('attachment_dir').DIRECTORY_SEPARATOR.$srcKey;

        if (!is_dir($srcDir)) {
            return new Response("Error: Invalid key!");
        }

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $files = scandir($srcDir);
        foreach ($files as $file) {
            if($file === '.' || $file === '..') {
                continue;
            }

            if(is_file("$srcDir/$file")) {
                $success = false;
                $index = 0;
                while (!$success) {
                    if (!is_file("$dir/$file")) {
                        copy("$srcDir/$file", "$dir/$file");
                        $success = true;
                    } else if (is_file("$dir/copy_$file"."_$index")) {
                        $index++;
                    } else {
                        copy("$srcDir/$file", "$dir/copy_$file"."_$index");
                        $success = true;
                    }
                }
            }
        }

        return new Response("Success");
    }

    public function handleProcess($controller, &$data, $options = array()) {
        $key = $controller->getRequest()->get('key', 'default');
        $dir = $this->container->getParameter('attachment_dir').DIRECTORY_SEPARATOR.$key;

        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $opts = array(
            'root'            => $dir,
            'URL'             => $this->container->getParameter('attachment_url').'/'.$key.'/',
            'rootAlias'       => 'Attachment',
        );
        $fm = new \Leep\AdminBundle\Business\Base\elFinder($opts);

        ob_start();
        $fm->run();
        $content = ob_get_contents();

        return new Response($content);
    }
}
