<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    private $idPendingOrder = null;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }

        if ($this->attachmentKey === false) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir('customers');
        }
    }

    public function getDefaultFormModel() {
        // check if previous Order is created by using a Pending Order
        $fmsg =  $this->container->get('request')->getSession()->getFlashBag()->get('msgOrderCreation');
        if ($fmsg) {
            $this->messages = $fmsg;
        }

        $this->generateAttachmentKey();
        $model = new CreateModel();
        $model->container = $this->container;
        $model->attachmentKey = $this->attachmentKey;

        $this->idPendingOrder = $this->container->get('request')->get('id_pending_order', null);

        if ($this->idPendingOrder) {
            $pendingOrder = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderPending')->findOneById($this->idPendingOrder);

            if ($pendingOrder) {
                $model->externalBarcode = $pendingOrder->getOrderExternalBarcode();
                $model->idDistributionChannel = $pendingOrder->getIdDistributionChannel();

                $dc = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($model->idDistributionChannel);

                $model->firstName = $pendingOrder->getContactFirstName();
                $model->surName = $pendingOrder->getContactSurName();
                $model->previousOrderNumber = $pendingOrder->getPreviousOrderNumber();
                $model->postCode = $pendingOrder->getAddressPostCode();
                $model->genderId = $pendingOrder->getContactidGender();
                $model->dateOfBirth = $pendingOrder->getContactDateOfBirth();
                $model->title = $pendingOrder->getContactTitle();

                $model->orderInfoText = $pendingOrder->getOrderInfo();
                $model->street = $pendingOrder->getAddressStreet();
                $model->street2 = $pendingOrder->getAddressStreet2();
                $model->city = $pendingOrder->getAddressCity();
                $model->countryId = $pendingOrder->getAddressIdCountry();
                $model->email = $pendingOrder->getAddressEmail();
                $model->telephone = $pendingOrder->getAddressTelephone();
                $model->fax = $pendingOrder->getAddressFax();
                $model->notes = $pendingOrder->getAddressNote();

                $model->languageId = $pendingOrder->getSettingIdLanguage();
                $model->reportGoesToId = $pendingOrder->getSettingReportGoesTo();
                $model->reportDeliveryId = $pendingOrder->getSettingReportDelivery();
                $model->idNutrimeGoesTo = $pendingOrder->getSettingNutrimeGoesTo();
                $model->isReportDeliveryPrintedBooklet = $pendingOrder->getIsReportDeliveryPrintedBooklet();
                $model->isReportDeliveryDownloadAccess = $pendingOrder->getIsReportDeliveryDownloadAccess();
                $model->idDncReportType = $pendingOrder->getIdDncReportType();

                $model->isCustomerBeContacted = $dc->getIsCustomerBeContacted();
                $model->isNutrimeOffered = $dc->getIsNutrimeOffered();
                $model->isNotContacted = $dc->getIsNotContacted();
                $model->isDestroySample = $dc->getIsDestroySample();
                $model->sampleCanBeUsedForScience = $dc->getSampleCanBeUsedForScience();
                $model->isFutureResearchParticipant = $dc->getIsFutureResearchParticipant();

                $model->companyInfo = $dc->getCompanyInfo();
                $model->laboratoryInfo = $dc->getLaboratoryInfo();
                $model->contactInfo = $dc->getContactInfo();
                $model->contactUs = $dc->getContactUs();
                $model->letterExtraText = $dc->getLetterExtraText();
                $model->clinicianInfoText = $dc->getClinicianInfoText();
                $model->text7 = $dc->getText7();
                $model->text8 = $dc->getText8();
                $model->text9 = $dc->getText9();
                $model->text10 = $dc->getText10();
                $model->text11 = $dc->getText11();

                $model->doctorsReporting = $dc->getDoctorsReporting();
                $model->automaticReporting = $dc->getAutomaticReporting();
                $model->noReporting = $dc->getNoReporting();
                $model->invoicingAndPaymentInfo = $dc->getInvoicingAndPaymentInfo();

                $model->priceCategoryId = $dc->getPriceCategoryId();
                $model->reportDeliveryEmail = $dc->getReportDeliveryEmail();
                $model->invoiceDeliveryEmail = $dc->getInvoiceDeliveryEmail();
                $model->invoiceGoesToId = $dc->getInvoiceGoesToId();
                $model->reportGoesToId = $dc->getReportGoesToId();
                $model->reportDeliveryId = $dc->getReportDeliveryId();
                $model->ftpServerName = $dc->getFtpServerName();
                $model->ftpUserName = $dc->getFtpUserName();
                $model->ftpPassword = $dc->getFtpPassword();
                $model->idPreferredPayment = $dc->getIdPreferredPayment();
                $model->idMarginGoesTo = $dc->getIdMarginGoesTo();
                $model->idNutrimeGoesTo = $dc->getIdNutrimeGoesTo();
                // $model->canCreateLoginAndSendStatusMessageToUser = $dc->getCanCreateLoginAndSendStatusMessageToUser();

                $model->isReportDeliveryFtpServer = $dc->getIsReportDeliveryFtpServer();
                $model->isReportDeliveryDontChargeBooklet = $dc->getIsReportDeliveryDontChargeBooklet();
                $model->idAcquisiteur1 = $dc->getAcquisiteurId1();
                $model->acquisiteurCommissionRate1 = $dc->getCommission1();
                $model->idAcquisiteur2 = $dc->getAcquisiteurId2();
                $model->acquisiteurCommissionRate2 = $dc->getCommission2();
                $model->idAcquisiteur3 = $dc->getAcquisiteurId3();
                $model->acquisiteurCommissionRate3 = $dc->getCommission3();
                $model->canCustomersDownloadReports = $dc->getCanCustomersDownloadReports();

                $model->rebrandingNick = $dc->getRebrandingNick();
                $model->headerFileName = $dc->getHeaderFileName();
                $model->footerFileName = $dc->getFooterFileName();
                $model->titleLogoFileName = $dc->getTitleLogoFileName();
                $model->boxLogoFileName = $dc->getBoxLogoFileName();

                $model->idVisibleGroupProduct = $dc->getIdVisibleGroupProduct();
                $model->idRecallGoesTo = $dc->getIdRecallGoesTo();

                $model->orderInfo = array();
                // ordered categories
                $model->orderInfo['categories'] = explode(',', $pendingOrder->getOrderedCategories());

                // ordered products
                $model->orderInfo['products'] = explode(',', $pendingOrder->getOrderedProducts());

                // ordered special products
                $model->orderInfo['specialProducts'] = explode(',', $pendingOrder->getOrderedSpecialProducts());

                // question
                $arrFormQuestions = Business\Customer\Util::getFormQuestions($this->container);
                $formQuestions = array();
                $questions = json_decode($pendingOrder->getOrderedQuestions(), true);
                foreach ($questions as $questionAnswerPair) {
                    $formQuestion = $arrFormQuestions[$questionAnswerPair['id']];
                    $answer = Business\FormQuestion\Util::decodeAnswer($formQuestion['idType'], $questionAnswerPair['answer']);
                    $formQuestions['form_question_'.$questionAnswerPair['id']] = $answer;
                }

                $model->questions = $formQuestions;
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('idVisibleGroupProduct', 'hidden');

        // new customer info
        $builder->add('sectionCustomerInfo', 'section', array(
            'label'    => 'Customer info',
            'property_path' => false
        ));
        $builder->add('customerNumber', 'text', array(
            'label'    => 'Customer number',
            'required' => true
        ));
        $builder->add('externalBarcode', 'text', array(
            'label'    => 'External barcode',
            'required' => false
        ));
        $builder->add('idDistributionChannel', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => true,
            'empty_value' => false,
            'choices'  => array('' => '') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeDistributionChannel()'
            )
        ));
        $builder->add('firstName', 'text', array(
            'label'    => 'First name',
            'required' => false
        ));
        $builder->add('surName', 'text', array(
            'label'    => 'Sur name',
            'required' => false
        ));
        $builder->add('previousOrderNumber', 'text', array(
            'label'    => 'Previous Order Number',
            'required' => false
        ));
        $builder->add('postCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('genderId', 'choice', array(
            'label'    => 'Gender',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Gender_List'),
            'expanded' => 'expanded'
        ));
        $builder->add('dateOfBirth', 'datepicker', array(
            'label'    => 'Date of birth',
            'required' => false
        ));
        $builder->add('title', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('webLoginGoesTo', 'choice', array(
            'label'    => 'Digital Report Goes To (Via Web Login)',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_EmailWeblogin_ReportGoesTo')
        ));

        $builder->add('isCustomerBeContacted', 'checkbox', array(
            'label'    => 'Can be contacted directly',
            'required' => false
        ));
        $builder->add('isNutrimeOffered', 'checkbox', array(
            'label'    => 'NutriMe Supplements are offered',
            'required' => false
        ));
        $builder->add('isNotContacted', 'checkbox', array(
            'label'    => 'Customer chose OPT OUT – don’t contact',
            'required' => false
        ));
        $builder->add('isDestroySample', 'checkbox', array(
            'label'    => 'Destroy details and samples after completion',
            'required' => false
        ));
        $builder->add('isFutureResearchParticipant', 'checkbox', array(
            'label'    => 'Customer agreed to extended research participation',
            'required' => false
        ));
        $builder->add('sampleCanBeUsedForScience', 'checkbox', array(
            'label'    => 'Sample can be used for science',
            'required' => false
        ));

        $builder->add('detailInformation', 'textarea', array(
            'label'    => 'Customer information',
            'required' => false,
            'attr'     => array(
                'rows'   => 10,
                'style'  => 'width: 95%'
            )
        ));

        // other data
        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'customers/'.$this->attachmentKey
        ));

        // SECTION ORDER
        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order',
            'property_path' => false
        ));
        $builder->add('orderNumber', 'text', array(
            'label'    => 'Order Number',
            'required' => true
        ));
        $builder->add('trackingCode', 'text', array(
            'label'    => 'Tracking Code',
            'required' => false
        ));
        $builder->add('dnaSampleOrderNumberVario', 'text', array(
            'label'    => 'DNA sample Order Number for VARIO',
            'required' => false
        ));
        $builder->add('orderInfoText', 'textarea', array(
            'label'    => 'Order information',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('domain', 'text', array(
            'label'    => 'Domain',
            'required' => false
        ));
        $builder->add('dateOrdered', 'datepicker', array(
            'label'    => 'Date Ordered',
            'required' => false
        ));
        $builder->add('orderAge', 'text', array(
            'label'    => 'Order Age',
            'required' => false
        ));
        $builder->add('laboratoryDetails', 'textarea', array(
            'label'    => 'Laboratory Details',
            'required' => false,
            'attr'     => array(
                'rows'   => 5, 'cols' => 70
            )
        ));
        $builder->add('isDelayDownloadReport', 'checkbox', array(
            'label'    => 'Delay download report?',
            'required' => false
        ));
        $builder->add('numberDateDelayDownloadReport', 'text', array(
            'label'    => 'Number date delay download report',
            'required' => false
        ));

        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
        $builder->add('isDisplayInCustomerDashboard', 'checkbox', array(
            'label'    => "Don't show in Weblogin",
            'required' => false
        ));

        // SECTION Report Delivery Options
        $builder->add('sectionReportDeliveryOptions', 'section', array(
            'label'    => 'Report Delivery Options',
            'property_path' => false
        ));
        $builder->add('isReportDeliveryDownloadAccess', 'checkbox', array(
            'label'    => 'Download access',
            'required' => false
        ));
        $builder->add('isReportDeliveryFtpServer', 'checkbox', array(
            'label'    => 'FTP server',
            'required' => false
        ));
        $builder->add('isReportDeliveryPrintedBooklet', 'checkbox', array(
            'label'    => 'Printed booklet',
            'required' => false
        ));
        $builder->add('isReportDeliveryDontChargeBooklet', 'checkbox', array(
            'label'    => 'Don\'t charge booklet',
            'required' => false
        ));

        // SECTION ADDRESS
        $builder->add('sectionAddress', 'section', array(
            'label'    => 'Address',
            'property_path' => false
        ));
        $builder->add('street', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('street2', 'text', array(
            'label'    => 'Street 2',
            'required' => false
        ));

        $builder->add('city', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('countryId', 'searchable_box', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => false
        ));
        $builder->add('telephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('fax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false,
            'attr'     => array(
                'cols' => 80, 'rows' => 10
            )
        ));

        // Invoice
        $builder->add('sectionInvoice',         'section', array(
            'label'    => 'Invoice',
            'property_path' => false
        ));
        $builder->add('invoiceAddressIsUsed', 'checkbox', array(
            'label'    => 'Have invoice address?',
            'required' => false
        ));
        $builder->add('invoiceAddressClientName', 'text', array(
            'label'    => 'Client Name',
            'required' => false
        ));
        $builder->add('invoiceAddressCompanyName', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('invoiceAddressStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('invoiceAddressPostCode', 'text', array(
            'label'    => 'Post Code',
            'required' => false
        ));
        $builder->add('invoiceAddressCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('invoiceAddressIdCountry', 'choice', array(
            'label'    => 'Country',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Country_List')
        ));
        $builder->add('invoiceAddressTelephone', 'text', array(
            'label'    => 'Telephone',
            'required' => false
        ));
        $builder->add('invoiceAddressFax', 'text', array(
            'label'    => 'Fax',
            'required' => false
        ));
        $builder->add('invoiceAddressUid', 'text', array(
            'label'    => 'UID',
            'required' => false
        ));

        // Setting
        $builder->add('sectionSetting',         'section', array(
            'label'    => 'Setting',
            'property_path' => false
        ));
        $builder->add('languageId',             'choice', array(
            'label'    => 'Language',
            'expanded' => 'expanded',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Language')
        ));

        // Report details
        $builder->add('sectionReportDetails', 'section', array(
            'label'    => 'Report Details',
            'property_path' => false
        ));
        $builder->add('companyInfo', 'textarea', array(
            'label'    => 'Company Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('laboratoryInfo', 'textarea', array(
            'label'    => 'Laboratory Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactInfo', 'textarea', array(
            'label'    => 'Contact Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('contactUs', 'textarea', array(
            'label'    => 'Contact Us',
            'required' => false,
            'attr'     => array(
                'rows'  => 5,  'cols' => 80
            )
        ));
        $builder->add('letterExtraText', 'text', array(
            'label'    => 'Letter Extra Text',
            'required' => false
        ));
        $builder->add('clinicianInfoText', 'text', array(
            'label'    => 'Clinician Info Text',
            'required' => false
        ));
        $builder->add('text7', 'text', array(
            'label'    => 'Text 7',
            'required' => false
        ));
        $builder->add('text8', 'text', array(
            'label'    => 'Text 8',
            'required' => false
        ));
        $builder->add('text9', 'text', array(
            'label'    => 'Text 9',
            'required' => false
        ));
        $builder->add('text10', 'text', array(
            'label'    => 'Text 10',
            'required' => false
        ));
        $builder->add('text11', 'text', array(
            'label'    => 'Text 11',
            'required' => false
        ));


        // GS Setting
        $builder->add('sectionGsSetting', 'section', array(
            'label'    => 'GS Settings',
            'property_path' => false
        ));
        $builder->add('doctorsReporting', 'text', array(
            'label'    => 'Doctors Reporting',
            'required' => false
        ));
        $builder->add('automaticReporting', 'text', array(
            'label'    => 'Automatic Reporting',
            'required' => false
        ));
        $builder->add('noReporting', 'text', array(
            'label'    => 'No Reporting',
            'required' => false
        ));
        $builder->add('invoicingAndPaymentInfo', 'textarea', array(
            'label'    => 'Invoicing And Payment Info',
            'required' => false,
            'attr'     => array(
                'rows'  => 10, 'cols' => 80
            )
        ));
        $builder->add('idDncReportType', 'choice', array(
            'label'    => 'DNC Report Type',
            'required' => false,
            'choices'  => [0 => ''] + $mapping->getMapping('LeepAdmin_Report_DncReportType'),
            'empty_value' => false
        ));

        // DELIVERY
        $builder->add('sectionDelivery',        'section', array(
            'label'    => 'Delivery',
            'property_path'  => false
        ));
        $builder->add('priceCategoryId', 'searchable_box', array(
            'label'    => 'Price Category',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_PriceCategory_List')
        ));
        $builder->add('reportDeliveryEmail', 'text', array(
            'label'    => 'Report Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceDeliveryEmail', 'text', array(
            'label'    => 'Invoice Delivery Email',
            'required' => false
        ));
        $builder->add('invoiceGoesToId', 'choice', array(
            'label'    => 'Invoice Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_InvoiceGoesTo')
        ));
        $builder->add('reportGoesToId', 'choice', array(
            'label'    => 'Report Goes To',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportGoesTo')
        ));
        $builder->add('reportDeliveryId', 'choice', array(
            'label'    => 'Report Delivery',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_ReportDelivery'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeReportDeliveryId()'
            )
        ));
        $builder->add('ftpServer', 'app_ftp_server', array(
            'label'    => 'FTP Server',
            'required' => false
        ));
        $builder->add('idPreferredPayment', 'choice', array(
            'label'    => 'Preferred payment',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_PreferredPayment')
        ));
        $builder->add('idNutrimeGoesTo', 'choice', array(
            'label'    => 'NutriMe goes to',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_NutrimeGoesTo')
        ));

        // PRICING
        $builder->add('sectionPricing',        'section', array(
            'label'    => 'Pricing',
            'property_path'  => false
        ));
        $builder->add('idMarginGoesTo', 'choice', array(
            'label'    => 'Margin goes to',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_MarginGoesTo')
        ));
        $builder->add('marginOverrideDc', 'text', array(
            'label'    => 'Margin override DC (%)',
            'required' => false
        ));
        $builder->add('marginOverridePartner', 'text', array(
            'label'    => 'Margin override partner (%)',
            'required' => false
        ));
        $builder->add('priceOverride',          'text', array(
            'label'    => 'Price-override',
            'required' => false
        ));
        $builder->add('idAcquisiteur1', 'choice', array(
            'label'    => 'Acquisiteur 1',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate1', 'text', array(
            'label'    => 'Acquisiteur commission 1',
            'required' => false
        ));
        $builder->add('idAcquisiteur2', 'choice', array(
            'label'    => 'Acquisiteur 2',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate2', 'text', array(
            'label'    => 'Acquisiteur commission 2',
            'required' => false
        ));
        $builder->add('idAcquisiteur3', 'choice', array(
            'label'    => 'Acquisiteur 3',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Acquisiteur_List')
        ));
        $builder->add('acquisiteurCommissionRate3', 'text', array(
            'label'    => 'Acquisiteur commission 3',
            'required' => false
        ));

        // Report setting
        $builder->add('sectionReportSetting', 'section', array(
            'label'    => 'Report setting',
            'property_path' => false
        ));
        $builder->add('idRecallGoesTo', 'choice', array(
            'label' => "Recall Goes to",
            'required' => false,
            'choices' => $mapping->getMapping('LeepAdmin_DistributionChannel_RecallGoesTo')
        ));

        // Rebrander
        $builder->add('sectionRebrander', 'section', array(
            'label'    => 'Rebrander',
            'property_path' => false
        ));
        $builder->add('rebrandingNick', 'text', array(
            'label'    => 'Rebranding Nick',
            'required' => false
        ));
        $builder->add('headerFileName', 'text', array(
            'label'    => 'Header File Name',
            'required' => false
        ));
        $builder->add('footerFileName', 'text', array(
            'label'    => 'Footer File Name',
            'required' => false
        ));
        $builder->add('titleLogoFileName', 'text', array(
            'label'    => 'Title Logo File Name',
            'required' => false
        ));
        $builder->add('boxLogoFileName', 'text', array(
            'label'    => 'Box Logo File Name',
            'required' => false
        ));

        // Status
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'collection', array(
            'type' => new Form\AppStatusRow($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
        $builder->add('sectionOrderInformation', 'section', array(
            'label'    => 'Order information',
            'property_path' => false
        ));
        $builder->add('orderInfo', 'app_gene_select');

        $builder->add('questions', 'app_question_select');
    }

    public function onSuccess() {
        $config = $this->container->get('leep_admin.helper.common')->getConfig();

        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        // create new Customer info
        $customerInfo = new Entity\CustomerInfo();
        $customerInfo->setCustomerNumber($model->customerNumber);
        $customerInfo->setExternalBarcode($model->externalBarcode);
        $customerInfo->setIdDistributionChannel($model->idDistributionChannel);
        $customerInfo->setFirstName($model->firstName);
        $customerInfo->setSurName($model->surName);
        $customerInfo->setDateOfBirth($model->dateOfBirth);
        $customerInfo->setEmail($model->email);
        $customerInfo->setPostCode($model->postCode);
        $customerInfo->setGenderId($model->genderId);
        $customerInfo->setTitle($model->title);
        $customerInfo->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $customerInfo->setIsNutrimeOffered($model->isNutrimeOffered);
        $customerInfo->setIsNotContacted($model->isNotContacted);
        $customerInfo->setIsDestroySample($model->isDestroySample);
        $customerInfo->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);
        $customerInfo->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $customerInfo->setWebLoginGoesTo($model->webLoginGoesTo);

        $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($model->idDistributionChannel);
        if ($dc) {
            $customerInfo->setCanViewShop($dc->getCanViewShop());
            $customerInfo->setIsCanViewYourOrder($dc->getIsCanViewYourOrder());
            $customerInfo->setIsCanViewCustomerProtection($dc->getIsCanViewCustomerProtection());
            $customerInfo->setIsCanViewRegisterNewCustomer($dc->getIsCanViewRegisterNewCustomer());
            $customerInfo->setIsCanViewPendingOrder($dc->getIsCanViewPendingOrder());
            $customerInfo->setIsCanViewSetting($dc->getIsCanViewSetting());
        }

        $customerInfo->setDetailInformation($model->detailInformation);
        $em->persist($customerInfo);
        $em->flush();


        $customer = new Entity\Customer();

        // set Id of Customer info
        $customer->setIdCustomerInfo($customerInfo->getId());

        // set other data
        $customer->setOrderNumber($model->orderNumber);
        $customer->setTrackingCode($model->trackingCode);
        // $customer->setExternalBarcode($customerInfo->getExternalBarcode()); // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setDnaSampleOrderNumberVario($model->dnaSampleOrderNumberVario);
        $customer->setOrderInfoText($model->orderInfoText);
        $customer->setDistributionChannelId($customerInfo->getIdDistributionChannel());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setDomain($model->domain);
        $customer->setDateOrdered($model->dateOrdered);
        $customer->setOrderAge($model->orderAge);
        $customer->setLaboratoryDetails($model->laboratoryDetails);

        // Report delivery options
        $customer->setIsReportDeliveryDownloadAccess($model->isReportDeliveryDownloadAccess);
        $customer->setIsReportDeliveryFtpServer($model->isReportDeliveryFtpServer);
        $customer->setIsReportDeliveryPrintedBooklet($model->isReportDeliveryPrintedBooklet);
        $customer->setIsReportDeliveryDontChargeBooklet($model->isReportDeliveryDontChargeBooklet);

        $customer->setFirstName($customerInfo->getFirstName());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setSurName($customerInfo->getSurName());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setPreviousOrderNumber($model->previousOrderNumber);
        $customer->setDateOfBirth($customerInfo->getDateOfBirth());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setPostCode($customerInfo->getPostCode());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setGenderId($customerInfo->getGenderId());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setTitle($customerInfo->getTitle());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE

        $customer->setStreet($model->street);
        $customer->setStreet2($model->street2);
        $customer->setPostCode($customerInfo->getPostCode());  // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setCity($model->city);
        $customer->setCountryId($model->countryId);
        $customer->setEmail($customerInfo->getEmail());   // !! MAKE DUPLICATION HERE in MIGRATION PHASE
        $customer->setTelephone($model->telephone);
        $customer->setFax($model->fax);
        $customer->setNotes($model->notes);

        // invoice
        $customer->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $customer->setInvoiceAddressClientName($model->invoiceAddressClientName);
        $customer->setInvoiceAddressCompanyName($model->invoiceAddressCompanyName);
        $customer->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $customer->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $customer->setInvoiceAddressCity($model->invoiceAddressCity);
        $customer->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $customer->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $customer->setInvoiceAddressFax($model->invoiceAddressFax);
        $customer->setInvoiceAddressUid($model->invoiceAddressUid);
        $customer->setPriceOverride($model->priceOverride);

        $customer->setAttachmentKey($model->attachmentKey);

        $customer->setLanguageId($model->languageId);

        $customer->setTaxValue($config->getDefaultTax());

        // Report details
        $customer->setCompanyInfo($model->companyInfo);
        $customer->setLaboratoryInfo($model->laboratoryInfo);
        $customer->setContactInfo($model->contactInfo);
        $customer->setContactUs($model->contactUs);
        $customer->setLetterExtraText($model->letterExtraText);
        $customer->setClinicianInfoText($model->clinicianInfoText);
        $customer->setText7($model->text7);
        $customer->setText8($model->text8);
        $customer->setText9($model->text9);
        $customer->setText10($model->text10);
        $customer->setText11($model->text11);

        // Gs setting
        $customer->setDoctorsReporting($model->doctorsReporting);
        $customer->setAutomaticReporting($model->automaticReporting);
        $customer->setNoReporting($model->noReporting);
        $customer->setInvoicingAndPaymentInfo($model->invoicingAndPaymentInfo);
        $customer->setIdDncReportType($model->idDncReportType);

        // Delivery
        $customer->setPriceCategoryId($model->priceCategoryId);
        $customer->setReportDeliveryEmail($model->reportDeliveryEmail);
        $customer->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);

        $customer->setInvoiceGoesToId($model->invoiceGoesToId);
        $customer->setReportGoesToId($model->reportGoesToId);
        $customer->setReportDeliveryId($model->reportDeliveryId);

        if ($model->ftpServer != null) {
            $customer->setFtpServerName($model->ftpServer->server);
            $customer->setFtpUsername($model->ftpServer->username);
            $customer->setFtpPassword($model->ftpServer->password);
        }

        $customer->setIdPreferredPayment($model->idPreferredPayment);
        $customer->setIdMarginGoesTo($model->idMarginGoesTo);
        $customer->setMarginOverrideDc($model->marginOverrideDc);
        $customer->setMarginOverridePartner($model->marginOverridePartner);

        $customer->setIdAcquisiteur1($model->idAcquisiteur1);
        $customer->setAcquisiteurCommissionRate1($model->acquisiteurCommissionRate1);
        $customer->setIdAcquisiteur2($model->idAcquisiteur2);
        $customer->setAcquisiteurCommissionRate2($model->acquisiteurCommissionRate2);
        $customer->setIdAcquisiteur3($model->idAcquisiteur3);
        $customer->setAcquisiteurCommissionRate3($model->acquisiteurCommissionRate3);

        $customer->setIdNutrimeGoesTo($model->idNutrimeGoesTo);
        // $customer->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
        $customer->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
        $customer->setIsDisplayInCustomerDashboard($model->isDisplayInCustomerDashboard);
        $customer->setIsDelayDownloadReport($model->isDelayDownloadReport);
        if ($model->isDelayDownloadReport == true) {
            $customer->setDateSetDelayDownloadReport(new \DateTime('now'));
            $customer->setNumberDateDelayDownloadReport($model->numberDateDelayDownloadReport);
        }

        // Report settings
        $customer->setIdRecallGoesTo($model->idRecallGoesTo);

        // Rebrander
        $customer->setRebrandingNick($model->rebrandingNick);
        $customer->setHeaderFileName($model->headerFileName);
        $customer->setFooterFileName($model->footerFileName);
        $customer->setTitleLogoFileName($model->titleLogoFileName);
        $customer->setBoxLogoFileName($model->boxLogoFileName);

        $customer->setIsRecheckGeocoding(1);

        $em->persist($customer);
        $em->flush();


        // Default invoice status
        if ($model->invoiceGoesToId == Business\DistributionChannel\Constant::INVOICE_GOES_TO_CUSTOMER) {
            $customer->setInvoiceStatus(Business\Invoice\Constant::INVOICE_STATUS_TO_BE_DONE);

            $invoiceStatusRow = new Entity\InvoiceStatus();
            $invoiceStatusRow->setCustomerId($customer->getId());
            $invoiceStatusRow->setStatusDate(new \DateTime());
            $invoiceStatusRow->setStatus(Business\Invoice\Constant::INVOICE_STATUS_TO_BE_DONE);
            $invoiceStatusRow->setSortOrder(1);
            $em->persist($invoiceStatusRow);
        }


        // Update status
        $sortOrder = 1;
        $lastStatus = 0;
        $lastDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\OrderedStatus();
            $status->setCustomerId($customer->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setIsNotFilterable($statusRow->isNotFilterable);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $lastStatus = $statusRow->status;
            $lastDate = $statusRow->statusDate;
        }
        $customer->setStatus($lastStatus);
        $customer->setStatusDate($lastDate);

        $em->flush();


        Util::updateStatusList($this->container, $customer->getId());
        //Util::updateArrivalDate($this->container, $customer->getId());
        // Save order information
        Util::saveOrderInformation($this->container, $em, $customer, $model->orderInfo);
        Util::saveFormQuestions($this->container, $em, $customer, $model->questions);
        Util::updateRevenueEstimation($this->container, $customer->getId());

        parent::onSuccess();

        $defaultFormModel = $this->getDefaultFormModel();
        $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
        $defaultFormModel->attachmentKey = $this->attachmentKey;
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $defaultFormModel);
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();

        $mgr = $this->container->get('easy_module.manager');
        $emailUrl = $mgr->getUrl('leep_admin', 'email', 'email', 'create', array('id' => $customer->getId(), 'mode' => 'customer'));

        $this->messages[] = '<a href="javascript:openEmail(\''.$emailUrl.'\')">Send email</a> to newly created customer</a>';

        $this->container->get('request')->getSession()->getFlashBag()->set('msgOrderCreation', $this->messages);
    }

    private function isUniqueCode($em, $externalBarcode)
    {
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from('AppDatabaseMainBundle:Customer','p')
                ->where($query->expr()->eq('p.externalBarcode', ':externalBarcode'))
                ->setParameter('externalBarcode', $externalBarcode);
        $results = $query->getQuery()->getResult();
        if(!empty($results)){
            return false;
        }
        return true;
    }
}
