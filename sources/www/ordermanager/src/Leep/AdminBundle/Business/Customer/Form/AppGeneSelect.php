<?php
namespace Leep\AdminBundle\Business\Customer\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppGeneSelect extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array();
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_gene_select';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['categoryProductLinkJson'] = json_encode(Business\Customer\Util::getCategoryProductLink($this->container));
        $view->vars['productQuestionLinkJson'] = json_encode(Business\Customer\Util::getProductQuestionLink($this->container));
        $view->vars['productQuestions'] = Business\Customer\Util::getProductQuestions($this->container);

        $view->vars['specialProductLinkJson'] = json_encode(Business\Customer\Util::getSpecialProductLink($this->container));
        $view->vars['specialProductCategoryLinkJson'] = json_encode(Business\Customer\Util::getSpecialProductCategoryLink($this->container));

        $view->vars['visibleGroupProductCategoryLinkJson'] = json_encode(Business\Customer\Util::getVisibleGroupProductCategoryLink($this->container));
        $view->vars['visibleGroupProductProductLinkJson'] = json_encode(Business\Customer\Util::getVisibleGroupProductProductLink($this->container));
            
        $id = $this->container->get('request')->get('id');
        $mgr = $this->container->get('easy_module.manager');
        $view->vars['id'] = $id;
        $view->vars['viewAllGeneUrl'] = $mgr->getUrl('leep_admin', 'customer', 'grid_result', 'list', array('id' => $id));
        $view->vars['viewProductGeneUrl'] = $mgr->getUrl('leep_admin', 'customer', 'customer', 'viewProductGene').'?id='.$id;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('categories', 'choice', array(
            'label' => 'Categories',
            'choices' => $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'multiple' => 'multiple',
            'required'  => false,
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px;height:150px',
                'onchange' => 'javascript:onChangeCategories()'
            ),            
        ));
        $builder->add('products', 'choice', array(
            'label' => 'Products',
            'choices' => $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'multiple' => 'multiple',
            'required'  => false,
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px;height:200px',
                'onchange' => 'javascript:onChangeProducts()'
            )
        ));
        $builder->add('specialProducts', 'choice', array(
            'label' => 'Special Products',
            'choices' => $mapping->getMappingFiltered('LeepAdmin_SpecialProduct_List'),
            'multiple' => 'multiple',
            'required'  => false,
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px;height:200px'
            )
        ));

        // Build question
        $productQuestions = Business\Customer\Util::getProductQuestions($this->container);     
        foreach ($productQuestions as $productId => $questions) {
            $builder->add('section_product_'.$productId, 'section', array(
                'label'    => $mapping->getMappingTitle('LeepAdmin_Product_List', $productId),
                'property_path' => false
            ));
            foreach ($questions as $question) {
                $fieldId = 'product_question_'.$productId.'_'.$question->getId();
                switch ($question->getControlTypeId()) {
                    case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX:
                        $builder->add($fieldId, 'text', array(
                            'label' => $question->getQuestion(),
                            'required' => false
                        ));
                        break;
                    case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO:
                        $options = explode(';', $question->getControlConfig());
                        $builder->add($fieldId, 'choice', array(
                            'label' => $question->getQuestion(),
                            'required' => false,
                            'expanded' => 'expanded',
                            'choices'  => $options
                        ));
                        break;
                    case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX:
                        $options = explode(';', $question->getControlConfig());
                        $builder->add($fieldId, 'choice', array(
                            'label' => $question->getQuestion(),
                            'required' => false,
                            'choices'  => $options
                        ));
                        break;
                    case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE:
                        $options = explode(';', $question->getControlConfig());
                        $builder->add($fieldId, 'choice', array(
                            'label' => $question->getQuestion(),
                            'required' => false,
                            'multiple' => 'multiple',
                            'expanded' => 'expanded',
                            'choices'  => $options
                        ));
                        break;
                }
            }
        }

    }
}