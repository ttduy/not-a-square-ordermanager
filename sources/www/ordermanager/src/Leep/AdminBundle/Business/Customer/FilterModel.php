<?php
namespace Leep\AdminBundle\Business\Customer;

class FilterModel {
    public $orderNumber;
    public $dateOrdered;

    public $distributionChannelId;
    public $partnerId;

    public $acquisiteurId;
    public $lockedId;

    public $firstname;
    public $surname;
    
    public $status;
    public $invoiceStatus;
    
    public $isDoubleChecked;
    public $reversed;

    public $mustBeStatus;
    public $mustNotBeStatus;
    public $mustBeProduct;
    public $mustBeCategory;
    public $dateFrom;
    public $dateTo;

    public $showTobeInvoicedCustomers;

    public $orderNumberList;
    public $externalBarcode;
    public $showMissingInvoiceGoesTo;
    public $idPreferredPayment;
}