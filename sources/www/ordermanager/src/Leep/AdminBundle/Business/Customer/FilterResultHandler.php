<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterResultHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterResultModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('geneName', 'text', array(
            'label'       => 'Gene Name',
            'required'    => false,
        ));
        $builder->add('scientificName', 'text', array(
            'label'       => 'Scientific Name',
            'required'    => false,
        ));
        $builder->add('rsNumber', 'text', array(
            'label'       => 'RS Number',
            'required'    => false,
        ));
        $builder->add('idRequireType', 'choice', array(
            'label'       => 'Is required',
            'required'    => false,
            'empty_value' => false,
            'choices'     => array(0 => 'All', 1 => 'Required genes', 2 => 'Extra genes')
        ));
        
        return $builder;
    }
}