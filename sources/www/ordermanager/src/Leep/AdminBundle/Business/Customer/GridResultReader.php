<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridResultReader extends AppGridDataReader {
    public $idCustomer = 0;
    public $requiredGenes = array();
    public $filters;

    public function __construct($container) {
        parent::__construct($container);

        // Load required genes
        $this->requiredGenes = array(0);
        $this->idCustomer = $container->get('request')->get('id', 0);
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('pg.geneid')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'od')
            ->innerJoin('AppDatabaseMainBundle:ProdGeneLink', 'pg', 'WITH', 'od.productid = pg.productid')
            ->andWhere('od.customerid = :idCustomer')
            ->setParameter('idCustomer', $this->idCustomer);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $this->requiredGenes[] = $r['geneid'];
        }

        $this->customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($this->idCustomer);
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'g.genename';
        $this->columnSortMapping[] = 'g.scientificname';
        $this->columnSortMapping[] = 'g.rsnumber';
        $this->columnSortMapping[] = 'p.result';

        return $this->columnSortMapping;
    }

    public function getColumnMapping() {
        return array('genename', 'scientificname', 'rsnumber', 'result', 'idRequireType');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Gene Name',       'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Scientific Name', 'width' => '30%', 'sortable' => 'true'),
            array('title' => 'RS Number',       'width' => '20%', 'sortable' => 'true'),
            array('title' => 'Result',          'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Is required',     'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return null;
    }

    public function buildQuery($queryBuilder) {

        $queryBuilder
            ->select('p.idGene as id, g.genename, g.scientificname, g.rsnumber, p.result')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
            ->leftJoin('AppDatabaseMainBundle:Genes', 'g', 'WITH', 'p.idGene = g.id');
        $queryBuilder->andWhere('p.idCustomer = :idCustomerInfo')
            ->setParameter('idCustomerInfo', $this->customer->getIdCustomerInfo());
        if (trim($this->filters->geneName) != '') {
            $queryBuilder->andWhere('g.genename LIKE :genename')
                ->setParameter('genename', '%'.trim($this->filters->geneName).'%');
        }
        if (trim($this->filters->scientificName) != '') {
            $queryBuilder->andWhere('g.scientificname LIKE :scientificname')
                ->setParameter('scientificname', '%'.trim($this->filters->scientificName).'%');
        }
        if (trim($this->filters->rsNumber) != '') {
            $queryBuilder->andWhere('g.rsnumber LIKE :rsnumber')
                ->setParameter('rsnumber', '%'.trim($this->filters->rsNumber).'%');
        }
        if ($this->filters->idRequireType == 1) { // Required
            $queryBuilder->andWhere('p.idGene in (:requiredGenes)')
                ->setParameter('requiredGenes', $this->requiredGenes);
        }
        else if ($this->filters->idRequireType == 2) { // Extra
            $queryBuilder->andWhere('p.idGene not in (:requiredGenes)')
                ->setParameter('requiredGenes', $this->requiredGenes);
        }
    }

    public function buildCellResult($row) {
        if (empty($row['result'])) {
            return '-';
        }
        return $row['result'];
    }

    public function buildCellIdRequireType($row) {
        if (in_array($row['id'], $this->requiredGenes)) {
            return 'Required';
        }
        return 'Extra';
    }
}
