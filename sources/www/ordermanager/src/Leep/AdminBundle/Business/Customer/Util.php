<?php
namespace Leep\AdminBundle\Business\Customer;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class Util {
    public static function getSubOrderInfo($container, $idOrder) {
        $doctrine = $container->get('doctrine');

        // get sub order list
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $result = $query->select('
                        p.idProductGroup,
                        pg.name AS productGroupName,
                        p.barcode
                    ')
                    ->from('AppDatabaseMainBundle:SubOrder', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ProductGroup', 'pg', 'WITH', 'pg.id = p.idProductGroup')
                    ->andWhere('p.idOrder = :idOrder')
                    ->setParameter('idOrder', $idOrder)
                    ->getQuery()
                    ->getResult();

        $arrSubOrders = array();
        foreach ($result as $row) {
            $arrSubOrders[$row['idProductGroup']] = array(
                                                            'barcode'   => $row['barcode'],
                                                            'groupName' => $row['productGroupName'],
                                                            'products'  => array()
                                                        );
        }

        // get product list
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $products = $query->select('pdp.name, pdp.idProductGroup')
                            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
                            ->innerJoin('AppDatabaseMainBundle:ProductsDnaPlus', 'pdp', 'WITH', 'pdp.id = p.productid')
                            ->andWhere('p.customerid = :idOrder')
                            ->setParameter('idOrder', $idOrder)
                            ->getQuery()
                            ->getResult();

        foreach ($products as $product) {
            $idProductGroup = $product['idProductGroup'];
            if (array_key_exists($idProductGroup, $arrSubOrders)) {
                $arrSubOrders[$idProductGroup]['products'][] = $product['name'];
            }
        }
        return $arrSubOrders;
    }

    public static function getVisibleGroupProductCategoryLink($container) {
        $links = array();

        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductCategory')->findAll();
        foreach ($result as $row) {
            $key = $row->getIdVisibleGroupProduct();
            $value = $row->getIdCategory();

            if (!isset($links[$key])) {
                $links[$key] = array();
            }
            $links[$key][$value] = 1;
        }
        return $links;
    }

    public static function getVisibleGroupProductProductLink($container) {
        $links = array();

        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:VisibleGroupProductProduct')->findAll();
        foreach ($result as $row) {
            $key = $row->getIdVisibleGroupProduct();
            $value = $row->getIdProduct();

            if (!isset($links[$key])) {
                $links[$key] = array();
            }
            $links[$key][$value] = 1;
        }
        return $links;
    }

    public static function getCategoryProductLink($container) {
        $catProdLink = array();

        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:CatProdLink')->findAll();
        foreach ($result as $row) {
            if (!isset($catProdLink[$row->getCategoryId()])) {
                $catProdLink[$row->getCategoryId()] = array();
            }
            $catProdLink[$row->getCategoryId()][] = $row->getProductId();
        }
        return $catProdLink;
    }

    public static function getProductQuestionLink($container) {
        $prodQuestionLink = array();

        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductQuestion')->findAll();
        foreach ($result as $row) {
            if (!isset($prodQuestionLink[$row->getProductId()])) {
                $prodQuestionLink[$row->getProductId()] = array();
            }
            $prodQuestionLink[$row->getProductId()][] = $row->getId();
        }
        return $prodQuestionLink;
    }

    public static function getProductQuestions($container) {
        $prodQuestions = array();
        /*
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductQuestion')->findAll();
        foreach ($result as $row) {
            if (!isset($prodQuestions[$row->getProductId()])) {
                $prodQuestions[$row->getProductId()] = array();
            }
            $prodQuestions[$row->getProductId()][] = $row;
        }*/
        return $prodQuestions;
    }

    public static function getSpecialProductLink($container) {
        $specialProductLink = array();
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findAll();
        foreach ($result as $row) {
            if ($row->getIsDeleted() != 1) {
                if (!isset($specialProductLink[$row->getIdDistributionChannel()])) {
                    $specialProductLink[$row->getIdDistributionChannel()] = array();
                }
                $specialProductLink[$row->getIdDistributionChannel()][$row->getIdProduct()] = array('id' => $row->getId(), 'name' => $row->getName());
            }
        }
        return $specialProductLink;
    }

    public static function getSpecialProductCategoryLink($container) {
        $specialProductCategoryLink = array();
        $result = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:SpecialProduct')->findAll();
        foreach ($result as $row) {
            if ($row->getIsDeleted() != 1) {
                if (!isset($specialProductCategoryLink[$row->getIdDistributionChannel()])) {
                    $specialProductCategoryLink[$row->getIdDistributionChannel()] = array();
                }
                $specialProductCategoryLink[$row->getIdDistributionChannel()][$row->getIdCategory()] = array('id' => $row->getId(), 'name' => $row->getName());
            }
        }
        return $specialProductCategoryLink;
    }

    public static function saveOrderInformation($container, $em, $customer, $orderInfo) {
        $em = $container->get('doctrine')->getEntityManager();

        $orderedCategoriesId = array();
        foreach ($orderInfo['categories'] as $categoryId) {
            $orderCategory = new Entity\OrderedCategory();
            $orderCategory->setCustomerid($customer->getId());
            $orderCategory->setCategoryId($categoryId);
            $orderedCategoriesId[] = $categoryId;
            $em->persist($orderCategory);
        }
        $customer->setCategoryList(implode(',', $orderedCategoriesId));

        $specialProductLink = Util::getSpecialProductLink($container);
        $specialProducts = isset($specialProductLink[$customer->getDistributionChannelId()]) ? $specialProductLink[$customer->getDistributionChannelId()] : array();

        $orderedProductsId = array();
        foreach ($orderInfo['products'] as $productId) {
            $orderProduct = new Entity\OrderedProduct();
            $orderProduct->setCustomerid($customer->getId());
            $orderProduct->setProductId($productId);

            foreach ($specialProducts as $idProduct => $p) {
                if ($productId == $idProduct) {
                    $orderProduct->setSpecialProductId($p['id']);
                }
            }
            $orderedProductsId[] = $productId;
            $em->persist($orderProduct);
        }

        $customer->setProductList(implode(',', $orderedProductsId));

        // save SubOrder
        self::saveSubOrderInformation($container, $em, $customer, $orderInfo);

        foreach ($orderInfo['specialProducts'] as $idSpecialProduct) {
            $orderSpecialProduct = new Entity\OrderedSpecialProduct();
            $orderSpecialProduct->setIdCustomer($customer->getId());
            $orderSpecialProduct->setIdSpecialProduct($idSpecialProduct);
            $em->persist($orderSpecialProduct);
        }

        // Save question
        /*$productQuestions = Util::getProductQuestions($container);
        foreach ($orderInfo['products'] as $productId) {
            if (isset($productQuestions[$productId])) {
                foreach ($productQuestions[$productId] as $question) {
                    $fieldId = 'product_question_'.$productId.'_'.$question->getId();
                    if (isset($orderInfo[$fieldId])) {
                        $value = $orderInfo[$fieldId];

                        $customerQuestion = new Entity\CustomerQuestion();
                        $customerQuestion->setCustomerId($customer->getId());
                        $customerQuestion->setQuestionId($question->getId());
                        if (is_array($value)) {
                            $customerQuestion->setAnswer(json_encode($value));
                        }
                        else {
                            $customerQuestion->setAnswer($value);
                        }
                        $em->persist($customerQuestion);
                    }
                }
            }
        }*/
        $em->flush();
    }

    public static function saveSubOrderInformation($container, $em, $customer, $orderInfo) {
        // save Product Group
        if (sizeOf($orderInfo['products']) > 0) {
            // get avaialble sub orders
            $existingSubOrders = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:SubOrder')->findByIdOrder($customer->getId());

            $arrExistingSubOrders = array();
            foreach ($existingSubOrders as $entry) {
                $arrExistingSubOrders[$entry->getIdProductGroup()] = $entry->getId();
            }

            $queryProductGroups = $em->createQueryBuilder();
            $productGroups = $queryProductGroups->select('p.idProductGroup')
                                            ->from('AppDatabaseMainBundle:ProductsDnaPlus', 'p')
                                            ->andWhere('p.id in (:idProducts)')
                                            ->setParameter('idProducts', $orderInfo['products'])
                                            ->groupBy('p.idProductGroup')
                                            ->getQuery()
                                            ->getResult();

            $arrFinalSubOrders = array();
            foreach ($productGroups as $product) {
                if (!array_key_exists($product['idProductGroup'], $arrExistingSubOrders)) {
                    $newSubOrder = new Entity\SubOrder();
                    $newSubOrder->setIdOrder($customer->getId());
                    $newSubOrder->setIdProductGroup($product['idProductGroup']);
                    $em->persist($newSubOrder);
                }

                $arrFinalSubOrders[$product['idProductGroup']] = 1;
            }

            // remove not used group
            $toBeRemovedProductGroups = array_diff(array_keys($arrExistingSubOrders), array_keys($arrFinalSubOrders));

            if (sizeof($toBeRemovedProductGroups) > 0) {
                $queryDel = $em->createQueryBuilder();

                $queryDel->delete('AppDatabaseMainBundle:SubOrder', 'p')
                        ->andWhere('p.idProductGroup IN (:idProductGroups)')
                        ->setParameter('idProductGroups', $toBeRemovedProductGroups)
                        ->andWhere('p.idOrder = :idOrder')
                        ->setParameter('idOrder', $customer->getId())
                        ->getQuery()
                        ->execute();
            }
        }
    }

    public static function removeOrderInformation($container, $em, $customer) {
        $dbHelper = $container->get('leep_admin.helper.database');
        $filters = array('customerid' => $customer->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:OrderedCategory', $filters);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:OrderedProduct', $filters);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:OrderedSpecialProduct', array('idCustomer' => $customer->getId()));
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerQuestion', $filters);
    }

    public static function computePrice($container, $customerId, $applied = 'invoice_customer') {
        $doctrine = $container->get('doctrine');

        // Load category - product link
        $catProdLink = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CatProdLink', 'app_main')->findAll();
        foreach ($result as $r) {
            if (!isset($catProdLink[$r->getProductId()])) {
                $catProdLink[$r->getProductId()] = array();
            }
            $catProdLink[$r->getProductId()][] = $r->getCategoryId();
        }

        // Load order information
        $orderCategory = array();
        $orderProduct = array();
        $specialProduct = array();

        $categories = $doctrine->getRepository('AppDatabaseMainBundle:OrderedCategory', 'app_main')->findBy(array('customerid' => $customerId));
        foreach ($categories as $category) {
            $orderCategory[$category->getCategoryId()] = 1;
        }

        $products = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct', 'app_main')->findBy(array('customerid' => $customerId));
        foreach ($products as $product) {
            $isExists = false;
            if (isset($catProdLink[$product->getProductId()])) {
                foreach ($catProdLink[$product->getProductId()] as $categoryId) {
                    if (isset($orderCategory[$categoryId])) {
                        $isExists = true;
                        break;
                    }
                }
            }

            if (!$isExists) {
                $orderProduct[$product->getProductId()] = 1;
                $specialProduct[$product->getProductId()] = $product->getSpecialProductId();
            }
        }

        // Load price category id
        $priceCategoryId = -1;
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('c.pricecategoryid')
            ->from('AppDatabaseMainBundle:Customer', 'c')
            ->andWhere('c.id = :customerId')
            ->setParameter('customerId', $customerId);
        $result = $query->getQuery()->getResult();
        if ($result != null) {
            foreach ($result as $row) {
                $priceCategoryId = $row['pricecategoryid'];
                break;
            }
        }

        $categoryPrices = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CategoryPrices', 'app_main')->findBy(array('pricecatid' => $priceCategoryId));
        foreach ($result as $r) {
            $categoryPrices[$r->getCategoryId()] = $r;
        }

        $productPrices = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:Prices', 'app_main')->findBy(array('pricecatid' => $priceCategoryId));
        foreach ($result as $r) {
            $productPrices[$r->getProductId()] = $r;
        }

        // Compute prices
        $price = array(
            'items' => array(
                'categories' => array(),
                'products'   => array()
            ),
            'total' => 0
        );
        foreach ($orderCategory as $idCategory => $t) {
            $p = 0;
            if (isset($categoryPrices[$idCategory])) {
                $p = Util::getAppliedPrice($categoryPrices[$idCategory], $applied);
                //$getMethod = 'get'.ucfirst($applied);
                //$p = $categoryPrices[$idCategory]->$getMethod();
            }

            $price['items']['categories'][$idCategory] = $p;
            $price['total'] += $p;
        }
        foreach ($orderProduct as $idProduct => $t) {
            $p = 0;
            if (isset($productPrices[$idProduct])) {
                $p = Util::getAppliedPrice($productPrices[$idProduct], $applied);

                //$getMethod = 'get'.ucfirst($applied);
                //$p = $productPrices[$idProduct]->$getMethod();
            }

            $price['items']['products'][$idProduct] = $p;
            $price['total'] += $p;
        }

        $price['specialProducts'] = $specialProduct;

        return $price;
    }

    public static function getAppliedPrice($price, $applied) {
        $v = 0;
        switch ($applied) {
            case 'invoice_partner':
                $v = floatval($price->getPrice()) - floatval($price->getDCMargin()) - floatval($price->getPartMargin());
                break;
            case 'invoice_dc':
                $v = floatval($price->getPrice()) - floatval($price->getDCMargin());
                break;
            case 'payment_partner':
                $v = floatval($price->getPartMargin());
                break;
            case 'payment_dc':
                $v = floatval($price->getDCMargin());
                break;
            case 'payment_both':
                $v = floatval($price->getPartMargin() + $price->getDCMargin());
                break;
            case 'payment_no_margin':
                $v = 0;
                break;
            case 'invoice_customer':
                $v = floatval($price->getPrice());
                break;
            default:
                break;
        }
        return $v;
    }

    public static function getImportFile($container) {
        $file = '';
        $path = $container->getParameter('kernel.root_dir').'/../web/attachments/import';
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry[0] != '.') {
                    $file = $path.'/'.$entry;
                }
            }
        }
        closedir($handle);

        return $file;
    }

    public static function parseCvs($container, $fileName, &$raw) {
        $delimiter = ';';
        $file = fopen($fileName, 'r');

        $data = array();

        $lineHeader = fgets($file);
        $raw[0] = $lineHeader;
        $r = explode($delimiter, $lineHeader);
        $productCodes = array();
        $categoryCodes = array();
        $i = 15;
        while (isset($r[$i][0]) && ($r[$i][0] == 'P')) {
            $productCodes[$i] = $r[$i];
            $i++;
        }
        while (isset($r[$i][0]) && ($r[$i][0] == 'C')) {
            $categoryCodes[$i] = $r[$i];
            $i++;
        }

        $j = 1;
        while (($line = fgets($file)) !== false) {
            $raw[$j] = $line;
            $j++;

            $r = explode($delimiter, $line);
            if (!empty($r[0])) {
                $products = array();
                $categories   = array();
                $i = 15;
                foreach ($productCodes as $pos => $code) {
                    if (isset($r[$pos]) && $r[$pos] == 1) {
                        $products[] = $code;
                    }
                }
                foreach ($categoryCodes as $pos => $code) {
                    if (isset($r[$pos]) && $r[$pos] == 1) {
                        $categories[] = $code;
                    }
                }

                $data[] = array(
                    'orderNumber'    => $r[0],
                    'domain'         => $r[1],
                    'dateOrdered'    => $r[2],
                    'title'          => $r[3],
                    'firstName'      => $r[4],
                    'surName'        => $r[5],
                    'street'         => $r[6],
                    'street2'        => $r[7],
                    'postCode'       => $r[8],
                    'city'           => $r[9],
                    'country'        => $r[10],
                    'email'          => $r[11],
                    'telephone'      => $r[12],
                    'dateOfBirth'    => $r[13],
                    'gender'         => $r[14],
                    'products'       => $products,
                    'categories'     => $categories
                );

            }
        }

        return $data;
    }

    public static function getOrderInformation($container, $customerId) {
        $doctrine = $container->get('doctrine');

        // Load category - product link
        $catProdLink = array();
        $result = $doctrine->getRepository('AppDatabaseMainBundle:CatProdLink', 'app_main')->findAll();
        foreach ($result as $r) {
            if (!isset($catProdLink[$r->getProductId()])) {
                $catProdLink[$r->getProductId()] = array();
            }
            $catProdLink[$r->getProductId()][] = $r->getCategoryId();
        }

        // Load order information
        $orderCategory = array();
        $orderProduct = array();

        $categories = $doctrine->getRepository('AppDatabaseMainBundle:OrderedCategory', 'app_main')->findBy(array('customerid' => $customerId));
        foreach ($categories as $category) {
            $orderCategory[$category->getCategoryId()] = 1;
        }

        $products = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct', 'app_main')->findBy(array('customerid' => $customerId));
        foreach ($products as $product) {
            $isExists = false;
            if (isset($catProdLink[$product->getProductId()])) {
                foreach ($catProdLink[$product->getProductId()] as $categoryId) {
                    if (isset($orderCategory[$categoryId])) {
                        $isExists = true;
                        break;
                    }
                }
            }

            if (!$isExists) {
                $orderProduct[$product->getProductId()] = 0;
            }
            else {
                $orderProduct[$product->getProductId()] = 1;
            }
        }

        return array(
            'orderCategory' => $orderCategory,
            'orderProduct'  => $orderProduct
        );
    }

    public static function addNewStatus($container, $orderId, $orderStatus) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $order = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($orderId);

        if ($order) {
            $orderStatusDate = new \DateTime();

            // get max order of ordered status
            $query = $em->createQueryBuilder();
            $maxSortOrder = $query->select('MAX(p.sortorder)')
                                ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
                                ->andWhere('p.customerid = :idOrder')
                                ->setParameter('idOrder', $orderId)
                                ->getQuery()
                                ->getSingleScalarResult();

            $maxSortOrder++;

            // unlock order
            $order->setStatus($orderStatus);
            $order->setStatusDate($orderStatusDate);
            $em->persist($order);

            $orderedStatus = new Entity\OrderedStatus();
            $orderedStatus->setCustomerid($orderId);
            $orderedStatus->setStatusdate($orderStatusDate);
            $orderedStatus->setStatus($orderStatus);
            $orderedStatus->setSortorder($maxSortOrder);
            $em->persist($orderedStatus);

            $em->flush();

            // update status list
            self::updateStatusList($container, $order->getId());
        }

        return $order;
    }

    public static function updateStatusList($container, $customerId) {
        $doctrine = $container->get('doctrine');
        $status = array();
        $results = $doctrine->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBycustomerid($customerId);
        foreach ($results as $r) {
            if ($r->getIsNotFilterable() == 1) {
                continue;
            }

            $status[] = $r->getStatus();
        }

        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($customerId);
        $customer->setStatusList(implode(',', $status));
        $doctrine->getEntityManager()->persist($customer);
        $doctrine->getEntityManager()->flush();
    }

    public static function getProducts($container, $customerId) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $prices = Util::computePrice($container, $customerId, 'invoice_customer');
        $products = array();
        foreach ($prices['items']['categories'] as $idCategory => $v) {
            $products[] = $formatter->format($idCategory, 'mapping', 'LeepAdmin_Category_List');
        }
        foreach ($prices['items']['products'] as $idProduct => $v) {
            $products[] = $formatter->format($idProduct, 'mapping', 'LeepAdmin_Product_List');
        }
        return $products;
    }

    public static function getCurrentStatus($container, $customerId) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.customerid = :customerId')
            ->setParameter('customerId', $customerId)
            ->orderBy('p.sortorder', 'DESC')
            ->setMaxResults(1);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            return $formatter->format($r->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
        }
        return '';
    }

    public static function sendEmailTemplate($container, $emailTemplateId, $customer) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $doctrine = $container->get('doctrine');
        $helperCommon = $container->get('leep_admin.helper.common');
        $helperActivity = $container->get('leep_admin.helper.activity');
        $em = $doctrine->getEntityManager();

        $emailTemplate = $doctrine->getRepository('AppDatabaseMainBundle:EmailTemplate')->findOneById($emailTemplateId);
        if ($emailTemplate) {
            $env = new \Twig_Environment(new \Twig_Loader_String(), array('autoescape' => false));

            $orderedProducts = $doctrine->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($customer->getId());
            $productList = array();
            foreach ($orderedProducts as $p) {
                $productList[] = $formatter->format($p->getProductId(), 'mapping', 'LeepAdmin_Product_List');
            }

            $sampleArrivedDate = null;
            $sampleArrivedDateStatus = $doctrine->getRepository('AppDatabaseMainBundle:OrderedStatus')->findOneBy(array(
                'customerid' => $customer->getId(),
                'status' => 25 // Sample arrived
            ));
            if ($sampleArrivedDateStatus) {
                $sampleArrivedDate = $sampleArrivedDateStatus->getStatusDate();
            }

            $distributionChannelData = '';
            $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
            if ($dc) {
                $distributionChannelData = $dc->getFirstName().' '.$dc->getSurName().', '.$dc->getInstitution().', '.$dc->getStreet().' '.$dc->getPostCode().' '.$dc->getCity().', '.$dc->getContactEmail();
            }

            $query = $em->createQueryBuilder();
            $query->select('p.externalBarcode')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->eq('p.id', ':idCustomerInfo'))
                ->setParameter('idCustomerInfo', $customer->getIdCustomerInfo());
            $externalBarcode = $query->getQuery()->getSingleScalarResult();

            $data = array(
                'orderNumber' => $customer->getOrderNumber(),
                'externalBarcode' => $externalBarcode,
                'orderDate'   => $formatter->format($customer->getDateOrdered(), 'date'),
                'name'        => $customer->getFirstName().' '.$customer->getSurName(),
                'address'     => $customer->getStreet().' '.$customer->getPostCode().' '.$customer->getCity().', '.
                                $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List'),
                'productList' => implode(', ', $productList),
                'distributionChannel' => $distributionChannelData,
                'sampleArrivedDate' => empty($sampleArrivedDate) ? "(unknown)" : $sampleArrivedDate->format('d/m/Y'),
                'reportLink'  => $container->getParameter('public_report_link').'?key='.$customer->getShipmentKey(),
                'reportAccessCode'  => $customer->getShipmentCode(),
                'feedbackQuestionLink' => $container->getParameter('application_url').'/feedback_form',
                'DNCREPORTTYPE'  => $formatter->format($customer->getIdDncReportType(), 'mapping', 'LeepAdmin_Report_DncReportType')
            );

            $subject = $env->render($emailTemplate->getSubject(), $data);
            $content = $env->render($emailTemplate->getContent(), $data);
            $receivers = array();
            if ($emailTemplate->getToCustomer()) {
                $receivers[] = $customer->getEmail();
            }
            if ($emailTemplate->getToOthers()) {
                $receivers[] = $emailTemplate->getToOthers();
            }
            if ($emailTemplate->getToDistributionChannel()) {
                if ($dc) {
                    $receivers[] = $dc->getStatusDeliveryEmail();
                }
            }
            if ($emailTemplate->getToPartner()) {
                if ($dc) {
                    $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
                    if ($partner) {
                        $receivers[] = $partner->getStatusDeliveryEmail();
                    }
                }
            }

            $sendTo = $helperCommon->parseEmaiLList(implode(';', $receivers));

            try {
                $senderEmail = $container->getParameter('sender_email');
                $senderName  = $container->getParameter('sender_name');
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($senderEmail, $senderName)
                    ->setTo($sendTo)
                    ->setBody($content, 'text/plain');
                $container->get('mailer')->send($message);
                $notes = 'Send email "'.$subject.'" to '.implode(';', $sendTo);
                $helperActivity->addCustomerActivity($customer->getId(), Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SENT, $notes);
            }
            catch (\Exception $e) {
                print_r($e);
            }
        }
    }

    public static function updateRevenueEstimation($container, $idCustomer, $catProdLinkPreload = null, $flushNow = true) {
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Get customer
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        if (!$customer) {
            return false;
        }

        if ($customer->getPriceCategoryId() == 0) {
            $customer->setRevenueEstimation(0);
            $em->persist($customer);
            if ($flushNow) {
                $em->flush();
            }
            return true;
        }

        // Load category - product link
        if ($catProdLinkPreload == null) {
            $catProdLink = array();
            $result = $doctrine->getRepository('AppDatabaseMainBundle:CatProdLink', 'app_main')->findAll();
            foreach ($result as $r) {
                if (!isset($catProdLink[$r->getProductId()])) {
                    $catProdLink[$r->getProductId()] = array();
                }
                $catProdLink[$r->getProductId()][] = $r->getCategoryId();
            }
        }
        else {
            $catProdLink = $catProdLinkPreload;
        }

        $totalPrice = 0;

        // Load category price
        $orderedCategory  = array();
        $query = $em->createQueryBuilder();
        $query->select('p.categoryid, cp.price')
            ->from('AppDatabaseMainBundle:OrderedCategory', 'p')
            ->innerJoin('AppDatabaseMainBundle:CategoryPrices', 'cp', 'WITH', 'cp.categoryid = p.categoryid AND cp.pricecatid = :idPriceCategory')
            ->andWhere('p.customerid = :idCustomer')
            ->setParameter('idCustomer', $customer->getId())
            ->setParameter('idPriceCategory', $customer->getPriceCategoryId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idCategory = $r['categoryid'];
            $price = $r['price'];
            $totalPrice += floatval($price);
            $orderedCategory[$idCategory] = 1;
        }

        // Load product price
        $query = $em->createQueryBuilder();
        $query->select('p.productid, cp.price')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->innerJoin('AppDatabaseMainBundle:Prices', 'cp', 'WITH', 'cp.productid = p.productid AND cp.pricecatid = :idPriceCategory')
            ->andWhere('p.customerid = :idCustomer')
            ->setParameter('idCustomer', $customer->getId())
            ->setParameter('idPriceCategory', $customer->getPriceCategoryId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $idProduct = $r['productid'];
            $price = $r['price'];
            $isExists = false;
            if (isset($catProdLink[$idProduct])) {
                foreach ($catProdLink[$idProduct] as $categoryId) {
                    if (isset($orderedCategory[$categoryId])) {
                        $isExists = true;
                        break;
                    }
                }
            }

            if (!$isExists) {
                $totalPrice += $price;
            }
        }

        $customer->setRevenueEstimation($totalPrice);
        if ($customer->getPriceOverride() !== null) {
            $customer->setRevenueEstimation($customer->getPriceOverride());
        }
        $em->persist($customer);
        if ($flushNow) {
            $em->flush();
        }

        return true;
    }


    public static function updateArrivalDate($container, $idCustomer) {
        $sampleArrivalStatus = 25; // Fix me
        $orderStatus = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedStatus')
            ->findOneBy(array(
                'customerid' => $idCustomer,
                'status'     => $sampleArrivalStatus
            ));
        if ($orderStatus) {
            $customer = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
            if ($customer) {
                $customer->setArrivalDate($orderStatus->getStatusDate());
                $em = $container->get('doctrine')->getEntityManager();
                $em->persist($customer);
                $em->flush();
            }
        }
    }

    public static function prepareFormQuestion($r) {
        $choices = array();
        if ($r->getIdType() == Business\FormQuestion\Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE ||
            $r->getIdType() == Business\FormQuestion\Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE) {
            $rows = explode("\n", $r->getQuestionData());
            foreach ($rows as $row) {
                $arr = explode('|', $row);
                if (count($arr) == 2) {
                    $choices[trim($arr[0])] = trim($arr[1]);
                }
            }
        }

        return array(
            'id'          => $r->getId(),
            'idType'      => $r->getIdType(),
            'question'    => $r->getQuestion(),
            'reportKey'   => $r->getReportKey(),
            'choices'     => $choices
        );
    }

    public static function getFormQuestions($container) {
        $em = $container->get('doctrine')->getEntityManager();

        $formQuestions = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:FormQuestion', 'p')
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $formQuestions[$r->getId()] = Util::prepareFormQuestion($r);
        }
        return $formQuestions;
    }

    public static function saveFormQuestions($container, $em, $customer, $questions) {
        $formQuestions = Util::getFormQuestions($container);

        $formQuestionTypes = array();
        foreach ($formQuestions as $formQuestion) {
            $formQuestionTypes[$formQuestion['id']] = $formQuestion['idType'];
        }

        $dbHelper = $container->get('leep_admin.helper.database');
        $filters = array('idCustomer' => $customer->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:OrderedQuestion', $filters);

        foreach ($questions as $questionKey => $answer) {
            $id = intval(substr($questionKey, 14));

            if (isset($formQuestionTypes[$id])) {
                $question = new Entity\OrderedQuestion();
                $question->setIdCustomer($customer->getId());
                $question->setIdFormQuestion($id);
                $question->setAnswer(Business\FormQuestion\Util::encodeAnswer($formQuestionTypes[$id], $answer));

                $em->persist($question);
            }
        }
        $em->flush();
    }

    public static function loadFormQuestions($container, $em, $customer, $includeNoAnswer = true, $decodeMethod = 'decodeAnswer') {
        $formQuestions = array();

        $query = $em->createQueryBuilder();
        $query->select('f.id, p.answer, f.idType')
            ->from('AppDatabaseMainBundle:OrderedQuestion', 'p')
            ->innerJoin('AppDatabaseMainBundle:FormQuestion', 'f', 'WITH', 'p.idFormQuestion = f.id')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $customer->getId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            if ($r['answer'] || $includeNoAnswer) {
                $formQuestions['form_question_'.$r['id']] = Business\FormQuestion\Util::$decodeMethod($r['idType'], $r['answer']);
            }
        }
        return $formQuestions;
    }

    public static function getOrderAge($date) {
        $today = new \DateTime();
        $todayTs = $today->getTimestamp();

        $age = '';
        if ($date) {
            $age = intval(($todayTs - $date->getTimestamp()) / 86400);
        }

        return $age;
    }


    public static function buildFormQuestion($builder, $key, $formQuestion) {
        switch ($formQuestion['idType']) {
            case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_SECTION:
                $builder->add($key, 'section', array(
                    'label'         => $formQuestion['question'],
                    'required'      => false,
                       'property_path' => false
                ));

                break;
            case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_TEXTBOX:
                $builder->add($key, 'text', array(
                    'label'         => $formQuestion['question'],
                    'required'      => false,
                ));

                break;
            case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE:
                $builder->add($key, 'choice', array(
                    'label'         => $formQuestion['question'],
                    'required'      => false,
                    'choices'       => $formQuestion['choices']
                ));

                break;
            case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE:
                $builder->add($key, 'choice', array(
                    'label'         => $formQuestion['question'],
                    'required'      => false,
                    'choices'       => $formQuestion['choices'],
                    'multiple'      => 'multiple'
                ));
                break;
            }
    }

    public static function checkForConflict($container, $customerNumbers) {
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $customerNumbers = array_unique(array_map('trim', $customerNumbers));

        $result = $query->select('x.customerNumber, y.idGene, y.result, z.genename, z.scientificname')
            ->from('AppDatabaseMainBundle:CustomerInfo', 'x')
            ->leftJoin('AppDatabaseMainBundle:CustomerInfoGene', 'y', 'WITH', 'y.idCustomer = x.id')
            ->leftJoin('AppDatabaseMainBundle:Genes', 'z', 'WITH', 'z.id = y.idGene')
            ->andWhere('x.customerNumber in (:customerNumbers)')
            ->setParameter('customerNumbers', $customerNumbers)
            ->getQuery()->getResult();

        $geneDatas = [];
        $numConflict = 0;
        $numSoftConflict = 0;
        foreach ($result as $data) {
            $customerNumber = $data['customerNumber'];
            $geneId = $data['idGene'];
            $result = trim($data['result']);
            $geneName = $data['genename'];
            $scientificName = $data['scientificname'];

            if (empty($geneId)) {
                continue;
            }

            if (!isset($geneDatas[$geneId])) {
                $geneDatas[$geneId] = [
                    'name' =>           $geneName,
                    'scientificName' => $scientificName,
                    'customers' =>      [],
                    'conflict' =>       false,
                    'softConflict' =>   false,
                    'initResult' =>     $result
                ];
            }

            $geneDatas[$geneId]['customers'][] = [
                'number' => $customerNumber,
                'result' => $result
            ];

            if (!empty($result)) {
                if ($result != $geneDatas[$geneId]['initResult']) {
                    if (empty($result) || empty($geneDatas[$geneId]['initResult'])) {
                        // $geneDatas[$geneId]['softConflict'] = true;
                        // $numSoftConflict++;
                    } else {
                        $geneDatas[$geneId]['conflict'] = true;
                        $numConflict++;
                    }
                }

                if (empty($geneDatas[$geneId]['initResult'])) {
                    $geneDatas[$geneId]['initResult'] = $result;
                }
            }
        }

        // // Recheck soft conflict
        // foreach ($geneDatas as $geneId => $data) {
        //     if ($data['softConflict'] || count($data['customers']) == count($customerNumbers)) {
        //         continue;
        //     }

        //     $geneDatas[$geneId]['softConflict'] = true;
        //     $numSoftConflict++;
        //     foreach ($customerNumbers as $customerNumber) {
        //         $existed = false;
        //         foreach ($data['customers'] as $customer) {
        //             if ($customer['number'] == $customerNumber) {
        //                 $existed = true;
        //                 break;
        //             }
        //         }

        //         if (!$existed) {
        //             $geneDatas[$geneId]['customers'][] = [
        //                 'number' => $customerNumber,
        //                 'result' => null
        //             ];
        //         }
        //     }
        // }

        return [$numConflict, $numSoftConflict, $geneDatas];
    }
}