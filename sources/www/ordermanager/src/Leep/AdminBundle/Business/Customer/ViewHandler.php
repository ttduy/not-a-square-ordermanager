<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');


        $id = $this->container->get('request')->query->get('id', 0);
        $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $data[] = $this->addSection('Customer order');
        $data[] = $this->add('Order Number', $customer->getOrderNumber());
        $data[] = $this->add('Tracking Code', $customer->getTrackingCode());
        $data[] = $this->add('Distribution Channel', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_List', $customer->getDistributionChannelId()));
        $data[] = $this->add('Domain', $customer->getDomain());
        $data[] = $this->add('Date Ordered', $formatter->format($customer->getDateOrdered(), 'date'));
        $data[] = $this->add('Order Age', $customer->getOrderAge());
        $data[] = $this->add('Status', $mapping->getMappingTitle('LeepAdmin_Customer_Status', $customer->getStatus()));
        
        $data[] = $this->addSection('Contact');
        $data[] = $this->add('Gender', $mapping->getMappingTitle('LeepAdmin_Gender_List', $customer->getGenderId()));
        $data[] = $this->add('Date Of Birth', $formatter->format($customer->getDateOfBirth(), 'date'));
        $data[] = $this->add('Title', $customer->getTitle());
        $data[] = $this->add('First Name', $customer->getFirstName());
        $data[] = $this->add('Surname', $customer->getSurName());

        $data[] = $this->addSection('Address');
        $data[] = $this->add('Street', $customer->getStreet());
        $data[] = $this->add('Street 2', $customer->getStreet2());
        $data[] = $this->add('Post Code', $customer->getPostCode());
        $data[] = $this->add('City', $customer->getCity());
        $data[] = $this->add('Country', $mapping->getMappingTitle('LeepAdmin_Country_List', $customer->getCountryId()));
        $data[] = $this->add('Email', $customer->getEmail());
        $data[] = $this->add('Telephone', $customer->getTelephone());
        $data[] = $this->add('Fax', $customer->getFax());
        $data[] = $this->add('Notes', $customer->getNotes());

        $data[] = $this->addSection('Report Details');
        $data[] = $this->add('Company Info', $customer->getCompanyInfo());
        $data[] = $this->add('Laboratory Info', $customer->getLaboratoryInfo());
        $data[] = $this->add('Contact Info', $customer->getContactInfo());
        $data[] = $this->add('Contact Us', $customer->getContactUs());
        $data[] = $this->add('Letter Extra Text', $customer->getLetterExtraText());
        $data[] = $this->add('Clinician Info Text', $customer->getClinicianInfoText());
        $data[] = $this->add('Text 7', $customer->getText7());
        $data[] = $this->add('Text 8', $customer->getText8());
        $data[] = $this->add('Text 9', $customer->getText9());
        $data[] = $this->add('Text 10', $customer->getText10());
        $data[] = $this->add('Text 11', $customer->getText11());

        $data[] = $this->addSection('GS Settings');
        $data[] = $this->add('Doctors Reporting', $customer->getDoctorsReporting());
        $data[] = $this->add('Automatic Reporting', $customer->getAutomaticReporting());
        $data[] = $this->add('No Reporting', $customer->getNoReporting());
        $data[] = $this->add('Invoicing And Payment Info', $customer->getInvoicingAndPaymentInfo());
        
        $data[] = $this->addSection('Delivery');   
        $data[] = $this->add('Report Delivery Email', $customer->getReportDeliveryEmail());        
        $data[] = $this->add('Invoice Delivery Email', $customer->getInvoiceDeliveryEmail());        
        $data[] = $this->add('Invoice Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_InvoiceGoesTo', $customer->getInvoiceGoesToId()));
        $data[] = $this->add('Report Goes To', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportGoesTo', $customer->getReportGoesToId()));
        $data[] = $this->add('Report Delivery', $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportDelivery', $customer->getReportDeliveryId()));
        if ($customer->getReportDeliveryId() == Business\DistributionChannel\Constant::REPORT_DELIVERY_FTP_SERVER) {
            $data[] = $this->add('FTP Server', $customer->getFtpServerName().' / '.$customer->getFtpUsername().' / '.$customer->getFtpPassword());
        }

        $data[] = $this->addSection('Rebrander');
        $data[] = $this->add('Rebranding Nick', $customer->getRebrandingNick());
        $data[] = $this->add('Header File Name', $customer->getHeaderFileName());
        $data[] = $this->add('Footer File Name', $customer->getFooterFileName());
        $data[] = $this->add('Title Logo File Name', $customer->getTitleLogoFileName());
        $data[] = $this->add('Box Logo File Name', $customer->getBoxLogoFileName());

        return $data;
    }
}
