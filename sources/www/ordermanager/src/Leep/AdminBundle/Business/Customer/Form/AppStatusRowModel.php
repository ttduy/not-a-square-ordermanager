<?php
namespace Leep\AdminBundle\Business\Customer\Form;

class AppStatusRowModel {
    public $id;
    public $statusDate;
    public $status;
    public $isNotFilterable;
}
