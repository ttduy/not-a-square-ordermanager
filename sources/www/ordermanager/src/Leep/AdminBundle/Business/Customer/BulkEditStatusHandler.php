<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditStatusHandler extends AppEditHandler {
    public $selectedId;
    public $selectedStatus;
    public $status;

    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) {
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new BulkEditStatusModel();
        $model->selectedOrderNumbers = array();
        $result = $this->getSelectedOrderNumbers();
        foreach ($result as $r) {
            $model->selectedOrderNumbers[] = $r->getOrderNumber();
        }
        $model->selectedOrderNumbers = implode("\n", $model->selectedOrderNumbers);

        if (!empty($this->selectedStatus)) {
            $model->status = $this->selectedStatus;
        }
        $model->statusDate = new \DateTime();
        $model->isSendEmail = true;

        return $model;
    }

    protected function getSelectedOrderNumbers() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:Customer', 'd')
            ->andWhere('d.id in (:arr)')
            ->andWhere('d.isdeleted = 0')
            ->setParameter('arr', $this->selectedId)
            ->getQuery()->getResult();
        return $result;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedOrderNumbers', 'textarea', array(
            'label'    => 'Order numbers',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));
        $builder->add('currentOrder', 'label', array(
            'label'    => 'Selected orders',
            'required' => false
        ));
        $builder->add('status',   'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Customer_Status')
        ));
        $builder->add('statusDate',     'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));
        $builder->add('isSendEmail',    'checkbox', array(
            'label'    => 'Send email template?',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $doctrine = $this->container->get('doctrine');
        $orderNumbers = explode("\n", $model->selectedOrderNumbers);
        $errors = array();
        $status = array();
        $updateOrders = array();
        foreach ($orderNumbers as $orderNumber) {
            $orderNumber = trim($orderNumber);
            if (!empty($orderNumber)) {
                $customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneBy(array('ordernumber' => $orderNumber, 'isdeleted' => 0));
                if (!empty($customer)) {
                    $updateOrders[] = $orderNumber;

                    // Max order
                    $query = $em->createQueryBuilder();
                    $query->select('MAX(p.sortorder)')
                        ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
                        ->andWhere('p.customerid = :customerId')
                        ->setParameter('customerId', $customer->getId());
                    $maxSortOrder = $query->getQuery()->getSingleScalarResult();

                    // Add status record
                    $status = new Entity\OrderedStatus();
                    $status->setCustomerId($customer->getId());
                    $status->setStatusDate($model->statusDate);
                    $status->setStatus($model->status);
                    $status->setIsCompleted(0);
                    $status->setSortOrder($maxSortOrder+1);
                    $em->persist($status);

                    $customer->setStatus($model->status);
                    $customer->setStatusDate($model->statusDate);

                    // Update status list
                    $status = explode(',', $customer->getStatusList());
                    foreach ($status as $k => $v) {
                        if (empty($v)) {
                            unset($status[$k]);
                        }
                    }
                    $status[] = $model->status;
                    $customer->setStatusList(implode(',', $status));

                    // Send email
                    if ($model->isSendEmail) {
                        $criteria = array(
                            'distributionchannelid' => $customer->getDistributionChannelId(),
                            'status'                => $model->status
                        );
                        $emailTemplate = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findOneBy($criteria);
                        if ($emailTemplate) {
                            Util::sendEmailTemplate($this->container, $emailTemplate->getEmailTemplateId(), $customer);
                        }
                    }
                }
            }
        }
        $em->flush();

        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $userSession = $userManager->getUserSession();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $log = new Entity\Log();
        $log->setIdWebUser($userSession->getUser()->getId());
        $log->setTitle('Bulk update order status (New status: '.$formatter->format($model->status, 'mapping', 'LeepAdmin_Customer_Status').')');
        $log->setLogTime(new \DateTime());
        $log->setLogData(json_encode(array('data' => array('orderList' => $updateOrders))));
        $em->persist($log);
        $em->flush();

        $this->messages[] = 'Status has been changed successfully';
    }
}