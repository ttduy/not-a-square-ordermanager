<?php
namespace Leep\AdminBundle\Business\Customer\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class AppCopyHistory extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'idOriginalCustomer' => 0
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_copy_history';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $idOriginalCustomer = intval($options['idOriginalCustomer']);

        // Original customer
        $originalCustomer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idOriginalCustomer);
        $query = $em->createQueryBuilder();
        $query->select('p.externalBarcode')
                ->from('AppDatabaseMainBundle:CustomerInfo','p')
                ->where($query->expr()->eq('p.id', ':idCustomerInfo'))
                ->setParameter('idCustomerInfo', $originalCustomer->getIdCustomerInfo());
        $externalBarcode = $query->getQuery()->getSingleScalarResult();
        if ($originalCustomer) {
            $view->vars['originalCustomerOrder'] = $originalCustomer->getOrderNumber();
            if ($externalBarcode != '') {
                $view->vars['originalCustomerOrder'] .= ' / '.$externalBarcode;
            }
        }
        else {
            $view->vars['originalCustomerOrder'] = '<deleted>';
        }

        // Copy history
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerCopyHistory', 'p')
            ->andWhere('p.idOriginalCustomer = :idOriginalCustomer')
            ->setParameter('idOriginalCustomer', $idOriginalCustomer)
            ->orderBy('p.copyDatetime', 'ASC');
        $results = $query->getQuery()->getResult();

        $copyHistory = array();
        foreach ($results as $r) {
            $fromCustomer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($r->getIdFromCustomer());
            $toCustomer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($r->getIdToCustomer());

            $products = array();
            if ($fromCustomer) {
                $orderedProducts = $em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBycustomerid($fromCustomer->getId());
                foreach ($orderedProducts as $op) {
                    $products[] = $formatter->format($op->getProductId(), 'mapping', 'LeepAdmin_Product_List');
                }
            }
            else {
                $products[] = '<deleted>';
            }

            $copyHistory[] = array(
                'datetime' => $formatter->format($r->getCopyDatetime(), 'datetime'),
                'from'     => empty($fromCustomer) ? '<deleted>' : sprintf("%s (%s)", $fromCustomer->getOrderNumber(), $fromCustomer->getSurname()),
                'to'       => empty($toCustomer) ? '<deleted>' : sprintf("%s (%s)", $toCustomer->getOrderNumber(), $toCustomer->getSurname()),
                'products' => implode(', ', $products)
            );
        }
        $view->vars['copyHistory'] = $copyHistory;

    }
}