<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->mustBeStatus = array();
        $model->mustNotBeStatus = array();

        return $model;
    }

    public function buildForm($builder) {
        $customerFilterBuilder = $this->container->get('leep_admin.customer.business.customer_filter_builder');
        $customerFilterBuilder->buildForm($builder);

        return $builder;
    }
}
