<?php
namespace Leep\AdminBundle\Business\Customer;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class WebloginShipmentHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $formatter = $controller->get('leep_admin.helper.formatter');
        $em = $controller->get('doctrine')->getEntityManager();

        $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $receiverInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());

        $data['receiverInfo'] = array(
            'name' => sprintf('%s - %s %s', $receiverInfo->getCustomerNumber(), $receiverInfo->getFirstName(), $receiverInfo->getSurName())
        );
    }
}
