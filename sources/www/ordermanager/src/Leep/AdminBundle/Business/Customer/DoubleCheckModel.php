<?php
namespace Leep\AdminBundle\Business\Customer;

use Symfony\Component\Validator\ExecutionContext;

class DoubleCheckModel {
    public $attachment;
    public $attachmentKey;

    public $idVisibleGroupProduct;
    public $trackingCode;
    public $webLoginGoesTo;

    // customer info
    public $idCustomerInfo;
    public $customerNumber;
    public $externalBarcode;
    public $idDistributionChannel;
    public $firstName;
    public $surName;
    public $previousOrderNumber;
    public $birthday;
    public $postCode;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $sampleCanBeUsedForScience;
    public $isFutureResearchParticipant;
    public $detailInformation;
    public $otherOrders;
    public $isDisplayInCustomerDashboard;

    // other data
    public $orderNumber;
    //public $externalBarcode;
    public $dnaSampleOrderNumberVario;
    public $orderInfoText;
    //public $distributionChannelId;
    public $domain;
    public $dateOrdered;
    public $orderAge;
    public $status;
    public $laboratoryDetails;

    public $genderId;
    public $dateOfBirth;
    public $title;
/*    public $firstName;
    public $surName;*/

    public $street;
    public $street2;
    //public $postCode;
    public $city;
    public $countryId;
    public $email;
    public $telephone;
    public $fax;
    public $notes;

    public $invoiceAddressIsUsed;
    public $invoiceAddressCompanyName;
    public $invoiceAddressClientName;
    public $invoiceAddressStreet;
    public $invoiceAddressPostCode;
    public $invoiceAddressCity;
    public $invoiceAddressIdCountry;
    public $invoiceAddressTelephone;
    public $invoiceAddressFax;
    public $invoiceAddressUid;
    public $priceOverride;

    public $statusList;

    public $languageId;

    public $companyInfo;
    public $laboratoryInfo;
    public $contactInfo;
    public $contactUs;
    public $letterExtraText;
    public $clinicianInfoText;
    public $text7;
    public $text8;
    public $text9;
    public $text10;
    public $text11;

    public $doctorsReporting;
    public $automaticReporting;
    public $noReporting;
    public $invoicingAndPaymentInfo;
    public $idDncReportType;

    public $priceCategoryId;
    public $reportDeliveryEmail;
    public $invoiceDeliveryEmail;
    public $invoiceGoesToId;
    public $reportGoesToId;
    public $reportDeliveryId;
    public $ftpServer;
    public $idPreferredPayment;
    public $idMarginGoesTo;
    public $idNutrimeGoesTo;
    public $marginOverrideDc;
    public $marginOverridePartner;

    public $idRecallGoesTo;

    public $idAcquisiteur1;
    public $acquisiteurCommissionRate1;
    public $idAcquisiteur2;
    public $acquisiteurCommissionRate2;
    public $idAcquisiteur3;
    public $acquisiteurCommissionRate3;

    public $rebrandingNick;
    public $headerFileName;
    public $footerFileName;
    public $titleLogoFileName;
    public $boxLogoFileName;

    public $orderInfo;
    public $questions;

    public $act;

    public $isReportDeliveryDownloadAccess;
    public $isReportDeliveryFtpServer;
    public $isReportDeliveryPrintedBooklet;
    public $isReportDeliveryDontChargeBooklet;

    public $canCreateLoginAndSendStatusMessageToUser;
    public $canCustomersDownloadReports;

    public $isDelayDownloadReport;
    public $numberDateDelayDownloadReport;
}
