<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class ProductFilterGridReader extends AppGridDataReader {
    public $filters;
    public $todayTs;
    public $dcCell = array();
    public $dcPartners = array();
    public $formatter;

    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();

        // Load dcName && dcPartner
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $this->formatter = $this->container->get('leep_admin.helper.formatter');
        foreach ($results as $r) {
            $dcName = $this->formatter->format($r->getId(), 'mapping', 'LeepAdmin_DistributionChannel_List');
            $partnerName = $this->formatter->format($r->getPartnerId(), 'mapping', 'LeepAdmin_Partner_List');

            if (trim($partnerName) != '') {
                $this->dcCell[$r->getId()] = $dcName.' / '.$partnerName;
            }
            else {
                $this->dcCell[$r->getId()] = $dcName;
            }

            if (!isset($this->dcPartner[$r->getPartnerId()])) {
                $this->dcPartner[$r->getPartnerId()] = array();
            }
            $this->dcPartner[$r->getPartnerId()][] = $r->getId();
        }
    }

    public function getColumnMapping() {
        return array('dateordered', 'orderNumber', 'name', 'distributionChannelId', 'currentStatus', 'categoriesOrdered');
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.dateordered';
        $this->columnSortMapping[] = 'p.ordernumber';
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.status';
        $this->columnSortMapping[] = 'p.distributionchannelid';

        return $this->columnSortMapping;
    }
    
    public function getTableHeader() {
        return array(       
            array('title' => 'Date ordered',         'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Order Number',         'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Customer Name',        'width' => '15%', 'sortable' => 'false'),     
            array('title' => 'Distribution Channel', 'width' => '15%', 'sortable' => 'true'),   
            array('title' => 'Latest Status', 'width' => '10%', 'sortable' => 'true'),   
            array('title' => 'Categories ordered', 'width' => '30%', 'sortable' => 'false'),  
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_ProductFilterGridReader');
    }

    public function getCountSQL() {
        return 'COUNT(DISTINCT p)';
    }
    
    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('DISTINCT p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:OrderedProduct', 'op', 'WITH', 'p.id = op.customerid')
            ->leftJoin('AppDatabaseMainBundle:OrderedCategory', 'oc', 'WITH', 'p.id = oc.customerid');
        if ($this->filters->idProduct == 0 && $this->filters->idCategory == 0) {
            $queryBuilder->andWhere('p.id = -1');
        }
        else {
            if ($this->filters->idProduct != 0) {
                $queryBuilder->andWhere('op.productid = :productId')
                    ->setParameter('productId', $this->filters->idProduct);
            }
            if ($this->filters->idCategory != 0) {
                $queryBuilder->andWhere('oc.categoryid = :categoryId')
                    ->setParameter('categoryId', $this->filters->idCategory);
            }
        }

        if (!empty($this->filters->startDate)) {
            $queryBuilder->andWhere('p.dateordered >= :startDate')
                ->setParameter('startDate', $this->filters->startDate);
        }
        if (!empty($this->filters->endDate)) {
            $queryBuilder->andWhere('p.dateordered <= :endDate')
                ->setParameter('endDate', $this->filters->endDate);
        }        
    }

    public function buildCellDistributionChannelId($row) {
        return isset($this->dcCell[$row->getDistributionChannelId()]) ? $this->dcCell[$row->getDistributionChannelId()] : '';
    }

    public function buildCellCurrentStatus($row) {
        return $this->formatter->format($row->getStatus(), 'mapping', 'LeepAdmin_Customer_Status');
    }

    public function buildCellCategoriesOrdered($row) {
        $categories = array();
        $results = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBycustomerid($row->getId());
        foreach ($results as $r) {
            $categoryName = $this->formatter->format($r->getCategoryId(), 'mapping', 'LeepAdmin_Category_List');
            if (!empty($categoryName)) {
                $categories[] = $categoryName;
            }
        }

        return implode(', ', $categories);
    }
    
    public function buildCellName($row) {
        $name = '';
        if (trim($row->getTitle()) != '') {
            $name .= $row->getTitle().' ';
        }
        return $name.$row->getFirstName().' '.$row->getSurName();
    }

}
