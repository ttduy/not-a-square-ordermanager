<?php
namespace Leep\AdminBundle\Business\Customer;

class WebloginShipmentModel {
    public $receiverId;
    public $sendTo;
    public $cc;
    public $subject;
    public $content;
    
    public $webLoginUsername;
    public $webLoginPassword;
    public $name;
    public $isCustomerBeContacted;
    public $isNutrimeOffered;
    public $isNotContacted;
    public $isDestroySample;
    public $allowDonateToScience;
    public $isFutureResearchParticipant;
    public $submitLoginInfo;
    public $submitContactSetting;
}