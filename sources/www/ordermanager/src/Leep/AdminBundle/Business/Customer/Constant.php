<?php 
namespace Leep\AdminBundle\Business\Customer;

class Constant {
    const ORDER_STATUS_DELETE_DETAIL_AFTER_ANALYSIS = 45;
    const ORDER_STATUS_UNLOCKED = 70;
    const ORDER_STATUS_REPORTED_SUBMITTED = 8;


    const ACTIVITY_TYPE_INVOICE_CREATED = 1;
    const ACTIVITY_TYPE_INVOICE_UPDATED = 2;
    const ACTIVITY_TYPE_EMAIL_SENT      = 3;
    const ACTIVITY_TYPE_OTHER           = 4;
    const ACTIVITY_TYPE_EMAIL_SHIPMENT_SENT = 5;
    public static function getCustomerActivityTypes() {
        return array(
            self::ACTIVITY_TYPE_INVOICE_CREATED     => 'Invoice generated',
            //self::ACTIVITY_TYPE_INVOICE_UPDATED     => 'Invoice updated',
            self::ACTIVITY_TYPE_EMAIL_SENT          => 'Email sent',
            self::ACTIVITY_TYPE_EMAIL_SHIPMENT_SENT => 'Email shipment sent',
            self::ACTIVITY_TYPE_OTHER               => 'Other',
        );
    }


    const LANGUAGE_GERMAN     = 1;
    const LANGUAGE_ENGLISH    = 2;
    const LANGUAGE_ITALIAN    = 3;
    const LANGUAGE_SPANISH    = 4;
    const LANGUAGE_FRENCH     = 5;
    const LANGUAGE_POLISH     = 6;
    const LANGUAGE_RUSSIAN    = 7;
    const LANGUAGE_TURKISH    = 8;
    const LANGUAGE_HUNGARIAN  = 9;
    const LANGUAGE_FINNISH    = 10;
    const LANGUAGE_SWEDISH    = 11;
    public static function getLanguages() {
        return array(
            self::LANGUAGE_GERMAN         => 'German',
            self::LANGUAGE_ENGLISH        => 'English',
            self::LANGUAGE_ITALIAN        => 'Italian',
            self::LANGUAGE_SPANISH        => 'Spanish',
            self::LANGUAGE_FRENCH         => 'French',
            self::LANGUAGE_POLISH         => 'Polish',
            self::LANGUAGE_RUSSIAN        => 'Russian',
            self::LANGUAGE_TURKISH        => 'Turkish',
            self::LANGUAGE_HUNGARIAN      => 'Hungarian',
            self::LANGUAGE_FINNISH        => 'Finnish',
            self::LANGUAGE_SWEDISH        => 'Swedish'
        );
    }
}
