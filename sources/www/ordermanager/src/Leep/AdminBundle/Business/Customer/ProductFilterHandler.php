<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class ProductFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new ProductFilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('startDate', 'datepicker', array(
            'label'    => 'Start date',
            'required' => false
        ));
        $builder->add('endDate', 'datepicker', array(
            'label'    => 'Start date',
            'required' => false
        ));

        $builder->add('idProduct', 'choice', array(
            'label'    => 'Product',
            'required' => false,
            'choices'  => array(0 => '') + $mapping->getMappingFiltered('LeepAdmin_Product_List'),
            'empty_value' => false
        ));
                
        $builder->add('idCategory', 'choice', array(
            'label'    => 'Category',
            'required' => false,
            'choices'  => array(0 => '') + $mapping->getMappingFiltered('LeepAdmin_Category_List'),
            'empty_value' => false
        ));
                
        return $builder;
    }
}