<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Partner\EditFeedbackHandler as BaseEditFeedbackHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\FormType;

class EditFeedbackHandler extends BaseEditFeedbackHandler {  
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($id);
    }

    public function searchFeedback($id) {
        $query = $this->container->get('doctrine')->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerFeedback', 'p')
            ->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $id)
            ->orderBy('p.sortOrder', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function clearOldFeedbacks($id) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $filters = array('idCustomer' => $id);
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerFeedback', $filters);
    }

    public function createNewFeedback($id) {
        $feedback = new Entity\CustomerFeedback();
        $feedback->setIdCustomer($id);
        return $feedback;
    }
}
