<?php
namespace Leep\AdminBundle\Business\Customer\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppQuestionSelect extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_question_select';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  

        $formQuestions = Business\Customer\Util::getFormQuestions($this->container);   

        foreach ($formQuestions as $formQuestion) {
            Business\Customer\Util::buildFormQuestion($builder, 'form_question_'.$formQuestion['id'], $formQuestion);
        } 

    }
}
