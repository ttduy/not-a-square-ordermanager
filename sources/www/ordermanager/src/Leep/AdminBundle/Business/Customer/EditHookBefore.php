<?php
    namespace Leep\AdminBundle\Business\Customer;

    use App\Database\MainBundle\Entity;
    use Easy\ModuleBundle\Module\AbstractHook;
    use Leep\AdminBundle\Business;

    class EditHookBefore extends AbstractHook {
        public function handle($controller, &$data, $options = array()) {
            $mgr = $controller->get('easy_module.manager');
            $idOrder = $controller->get('request')->get('id', null);
            $em = $controller->get('doctrine')->getEntityManager();

            $query = $em->createQueryBuilder();
            $query->select('p.idAcquisiteur1 as idAc1, p.idAcquisiteur2 as idAc2, p.idAcquisiteur3 as idAc3, p.distributionchannelid as dcId')
                    ->from('AppDatabaseMainBundle:Customer','p')
                    ->andWhere('p.id = :idOrder')
                    ->setParameter('idOrder', $idOrder);
            $queryResult = $query->getQuery()->getResult();
            $info = $queryResult[0];

            $acquisituerId1 = $info['idAc1'];
            $acquisituerId2 = $info['idAc2'];
            $acquisituerId3 = $info['idAc3'];
            $distributionChannelId = $info['dcId'];

            $query2 = $em->createQueryBuilder();
            $query2->select('dc.partnerid as partner_Id')
                    ->from('AppDatabaseMainBundle:DistributionChannels','dc')
                    ->andWhere('dc.id = :distributionChannelId')
                    ->setParameter('distributionChannelId', $distributionChannelId);
            $queryResult2 = $query2->getQuery()->getResult();
            $infoDC = $queryResult2[0];
            $partnerId = $infoDC['partner_Id'];
            $customerQuickInfo_json = json_encode([$acquisituerId1, $acquisituerId2, $acquisituerId3, $partnerId]);
            $data['customerQuickAccesskInformation'] = $customerQuickInfo_json;
            $data['isCreateHandler'] = '0';
        }
    }
?>