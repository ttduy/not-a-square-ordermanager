<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditSettingHandler extends AppEditHandler {
    public $selectedId;

    public function loadEntity($request) {
        return false;
    }

    public function convertToFormModel($entity) {
        return new BulkEditSettingModel();
    }

    public function __construct($container) {
        $this->container = $container;
    }

    public function buildForm($builder) {
        // $builder->add('canCreateLoginAndSendStatusMessageToUser', 'checkbox', array(
        //     'label'    => 'Give users Login and send them Status message',
        //     'required' => false
        // ));

        $builder->add('canCustomersDownloadReports', 'checkbox', array(
            'label'    => 'Can customers download reports',
            'required' => false
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $selectedId = explode(',', $this->container->get('request')->get('selectedId', ''));
        $this->selectedId = array_map('intval', array_map('trim', $selectedId));

        $doctrine = $this->container->get('doctrine');
        $customerList = $em->getRepository('AppDatabaseMainBundle:Customer')->findById($this->selectedId);
        foreach ($customerList as $customer) {
            // $customer->setCanCreateLoginAndSendStatusMessageToUser($model->canCreateLoginAndSendStatusMessageToUser);
            $customer->setCanCustomersDownloadReports($model->canCustomersDownloadReports);
            $em->persist($customer);
        }

        $em->flush();
        $this->messages[] = 'Status has been changed successfully';
    }
}