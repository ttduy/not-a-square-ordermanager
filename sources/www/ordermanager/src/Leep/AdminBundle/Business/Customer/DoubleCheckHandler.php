<?php
namespace Leep\AdminBundle\Business\Customer;

use Leep\AdminBundle\Business;
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\Form\FormError;

class DoubleCheckHandler extends CreateHandler {
    public $idCustomer = 0;
    public function getDefaultFormModel() {
        $mapping = $this->container->get('easy_mapping');
        $em = $this->container->get('doctrine')->getEntityManager();
        $mgr = $this->container->get('easy_module.manager');
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $this->idCustomer = $this->container->get('request')->query->get('id', 0);
        $this->customer = $em->getRepository('AppDatabaseMainBundle:Customer', 'app_main')->findOneById($this->idCustomer);
        $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($this->customer->getIdCustomerInfo());

        $model = new DoubleCheckModel();
        $model->orderNumber = $this->customer->getOrderNumber();
        $model->idDistributionChannel = $this->customer->getDistributionChannelId();
        $model->detailInformation = $customerInfo->getDetailInformation();
        $model->orderInfoText = $this->customer->getOrderInfoText();
        $model->laboratoryDetails = $this->customer->getLaboratoryDetails();
        $model->dateOrdered = $this->customer->getDateOrdered();
        $model->attachmentKey = $this->customer->getAttachmentKey();
        $model->previousOrderNumber = $this->customer->getPreviousOrderNumber();
        $model->isDisplayInCustomerDashboard = $this->customer->getIsDisplayInCustomerDashboard();

        $this->attachmentKey = $model->attachmentKey;

        // $model->trackingCode = $this->customer->getTrackingCode();
        // $model->dnaSampleOrderNumberVario = $this->customer->getDnaSampleOrderNumberVario();
        // $model->domain = $this->customer->getDomain();
        // $model->street = $this->customer->getStreet();
        // $model->street2 = $this->customer->getStreet2();
        // $model->city = $this->customer->getCity();
        // $model->countryId = $this->customer->getCountryId();
        // $model->telephone = $this->customer->getTelephone();
        // $model->fax = $this->customer->getFax();
        // $model->notes = $this->customer->getNotes();

        // $model->invoiceAddressIsUsed = $this->customer->getInvoiceAddressIsUsed();
        // $model->invoiceAddressClientName = $this->customer->getInvoiceAddressClientName();
        // $model->invoiceAddressCompanyName = $this->customer->getInvoiceAddressCompanyName();
        // $model->invoiceAddressStreet = $this->customer->getInvoiceAddressStreet();
        // $model->invoiceAddressPostCode = $this->customer->getInvoiceAddressPostCode();
        // $model->invoiceAddressCity = $this->customer->getInvoiceAddressCity();
        // $model->invoiceAddressIdCountry = $this->customer->getInvoiceAddressIdCountry();
        // $model->invoiceAddressTelephone = $this->customer->getInvoiceAddressTelephone();
        // $model->invoiceAddressFax = $this->customer->getInvoiceAddressFax();
        // $model->invoiceAddressUid = $this->customer->getInvoiceAddressUid();
        // $model->priceOverride = $this->customer->getPriceOverride();

        // $model->languageId = $this->customer->getLanguageId();

        // $model->companyInfo = $this->customer->getCompanyInfo();
        // $model->laboratoryInfo = $this->customer->getLaboratoryInfo();
        // $model->contactInfo = $this->customer->getContactInfo();
        // $model->contactUs = $this->customer->getContactUs();
        // $model->letterExtraText = $this->customer->getLetterExtraText();
        // $model->clinicianInfoText = $this->customer->getClinicianInfoText();
        // $model->text7 = $this->customer->getText7();
        // $model->text8 = $this->customer->getText8();
        // $model->text9 = $this->customer->getText9();
        // $model->text10 = $this->customer->getText10();
        // $model->text11 = $this->customer->getText11();

        // $model->doctorsReporting = $this->customer->getDoctorsReporting();
        // $model->automaticReporting = $this->customer->getAutomaticReporting();
        // $model->noReporting = $this->customer->getNoReporting();
        // $model->invoicingAndPaymentInfo = $this->customer->getInvoicingAndPaymentInfo();
        // $model->idDncReportType = $this->customer->getIdDncReportType();

        // $model->priceCategoryId = $this->customer->getPriceCategoryId();
        // $model->reportDeliveryEmail = $this->customer->getReportDeliveryEmail();
        // $model->invoiceDeliveryEmail = $this->customer->getInvoiceDeliveryEmail();

        // $model->invoiceGoesToId = $this->customer->getInvoiceGoesToId();
        // $model->reportGoesToId = $this->customer->getReportGoesToId();
        // $model->reportDeliveryId = $this->customer->getReportDeliveryId();

        // $model->ftpServer = new Business\DistributionChannel\Form\AppFtpServerModel();
        // $model->ftpServer->server = $this->customer->getFtpServerName();
        // $model->ftpServer->username = $this->customer->getFtpUsername();
        // $model->ftpServer->password = $this->customer->getFtpPassword();

        // $model->idPreferredPayment = $this->customer->getIdPreferredPayment();
        // $model->idMarginGoesTo = $this->customer->getIdMarginGoesTo();
        // $model->marginOverrideDc = $this->customer->getMarginOverrideDc();
        // $model->marginOverridePartner = $this->customer->getMarginOverridePartner();

        // $model->idNutrimeGoesTo = $this->customer->getIdNutrimeGoesTo();

        // $model->isReportDeliveryDownloadAccess = $this->customer->getIsReportDeliveryDownloadAccess();
        // $model->isReportDeliveryFtpServer = $this->customer->getIsReportDeliveryFtpServer();
        // $model->isReportDeliveryPrintedBooklet = $this->customer->getIsReportDeliveryPrintedBooklet();
        // $model->isReportDeliveryDontChargeBooklet = $this->customer->getIsReportDeliveryDontChargeBooklet();

        // $model->idRecallGoesTo = $this->customer->getIdRecallGoesTo();
        // // $model->canCreateLoginAndSendStatusMessageToUser = $this->customer->getCanCreateLoginAndSendStatusMessageToUser();
        // $model->canCustomersDownloadReports = $this->customer->getCanCustomersDownloadReports();

        // $model->idAcquisiteur1 = $this->customer->getIdAcquisiteur1();
        // $model->acquisiteurCommissionRate1 = $this->customer->getAcquisiteurCommissionRate1();
        // $model->idAcquisiteur2 = $this->customer->getIdAcquisiteur2();
        // $model->acquisiteurCommissionRate2 = $this->customer->getAcquisiteurCommissionRate2();
        // $model->idAcquisiteur3 = $this->customer->getIdAcquisiteur3();
        // $model->acquisiteurCommissionRate3 = $this->customer->getAcquisiteurCommissionRate3();


        // $model->rebrandingNick = $this->customer->getRebrandingNick();
        // $model->headerFileName = $this->customer->getHeaderFileName();
        // $model->footerFileName = $this->customer->getFooterFileName();
        // $model->titleLogoFileName = $this->customer->getTitleLogoFileName();
        // $model->boxLogoFileName = $this->customer->getBoxLogoFileName();

        $customerFilters = array('customerid' => $this->idCustomer);
        $model->orderInfo = array();
        $model->orderInfo['categories'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedCategory', $customerFilters, 'CategoryId', 'CategoryId');
        $model->orderInfo['products'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedProduct', $customerFilters, 'ProductId', 'ProductId');
        $model->orderInfo['specialProducts'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedSpecialProduct', array('idCustomer' => $this->idCustomer), 'idSpecialProduct', 'idSpecialProduct');

        // get customer info
        if ($this->customer->getIdCustomerInfo() != 0) {
            $customerInfo = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($this->customer->getIdCustomerInfo());
            if ($customerInfo) {
                $url = $mgr->getUrl('leep_admin', 'customer_info', 'edit', 'edit', array('id' => $this->customer->getIdCustomerInfo()));

                //$model->idCustomerInfo = $this->customer->getIdCustomerInfo();
                $model->customerNumber = "<a href='".$url."'>".$customerInfo->getCustomerNumber()."</a>";
                // $model->externalBarcode = $customerInfo->getExternalBarcode();
                // $model->idDistributionChannel = $customerInfo->getIdDistributionChannel();
                // $model->firstName = $customerInfo->getFirstName();
                // $model->surName = $customerInfo->getSurName();
                // $model->dateOfBirth = $customerInfo->getDateOfBirth();
                // $model->genderId =  $customerInfo->getGenderId();
                // $model->title = $customerInfo->getTitle();
                // $model->postCode = $customerInfo->getPostCode();
                // $model->isDestroySample = $customerInfo->getIsDestroySample();
                // $model->isFutureResearchParticipant = $customerInfo->getIsFutureResearchParticipant();
                // $model->isNotContacted = $customerInfo->getIsNotContacted();
                // $model->detailInformation = $customerInfo->getDetailInformation();
                // $model->isNutrimeOffered = $customerInfo->getIsNutrimeOffered();
                // $model->sampleCanBeUsedForScience = $customerInfo->getSampleCanBeUsedForScience();
                // $model->isCustomerBeContacted = $customerInfo->getIsCustomerBeContacted();
                // $model->email = $customerInfo->getEmail();

                $orders = $em->getRepository('AppDatabaseMainBundle:Customer')->findByIdCustomerInfo($this->customer->getIdCustomerInfo());
                $links = array();
                foreach ($orders as $order) {
                    $url = $mgr->getUrl('leep_admin', 'customer', 'edit', 'edit', array('id' => $order->getId()));
                    $links[] = "<a href='".$url."'>".$order->getOrderNumber()."</a>";
                }
                $model->otherOrders = implode("&nbsp;&nbsp;", $links);
            }
        }

        // Order info
        // $dbHelper = $this->container->get('leep_admin.helper.database');
        // $customerFilters = array('customerid' => $this->customer->getId());
        // $model->orderInfo = array();
        // $model->orderInfo['categories'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedCategory', $customerFilters, 'CategoryId', 'CategoryId');
        // $model->orderInfo['products'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedProduct', $customerFilters, 'ProductId', 'ProductId');
        // $model->orderInfo['specialProducts'] = $dbHelper->loadList('AppDatabaseMainBundle:OrderedSpecialProduct', array('idCustomer' => $this->customer->getId()), 'idSpecialProduct', 'idSpecialProduct');

        // $questionProductId = array();
        // $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ProductQuestion')->findAll();
        // foreach ($result as $row) {
        //     $questionProductId[$row->getId()] = $row->getProductId();
        // }
        // $filters = array('customerid' => $this->customer->getId());
        // $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerQuestion')->findBy($filters);
        // foreach ($result as $row) {
        //     if (isset($questionProductId[$row->getQuestionId()])) {
        //         $answer = $row->getAnswer();
        //         if (json_decode($answer, true) !== NULL) {
        //             $answer = json_decode($answer, true);
        //         }
        //         $model->orderInfo['product_question_'.$questionProductId[$row->getQuestionId()].'_'.$row->getQuestionId()] = $answer;
        //     }
        // }

        // // Form Question
        // $model->questions = Util::loadFormQuestions($this->container, $em, $this->customer);

        return $model;
    }

    public function buildForm($builder) {
        parent::buildForm($builder);
        $builder->add('act', 'hidden');


        $builder->add('customerNumber', 'label', array(
            'label'    => 'Customer Number',
            'required' => false
        ));

        $builder->add('otherOrders', 'label', array(
            'label'    => 'All Orders',
            'required' => false
        ));
    }

    public function onSuccess() {
        $mapping = $this->container->get('easy_mapping');
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        if ($model->act == 'check') {
            $this->notMatch = false;
            $this->check($this->customer, $model);

            // END CHECKING
            if ($this->notMatch === true) {
                $this->errors[] = "Don't match";
            }
            else {
                $this->messages[] = 'The information of order no '.$this->customer->getOrderNumber().' is exactly matched';
            }
        }
        else if ($model->act == 'markonly') {
            $this->customer->setIsDoubleChecked(1);
            $em->persist($this->customer);
            $em->flush();

            $this->messages[] = 'The customer has been double-checked';
        }
        else if ($model->act == 'override') {
            $r = $this->override($model);
            if ($r == false) {
                return false;
            }

            $this->customer->setIsDoubleChecked(1);
            $em->persist($this->customer);
            $em->flush();

            $this->messages[] = 'The customer has been overrided and marked as double-checked';
        }
    }

    public function check($customer, $model) {
        $mapping = $this->container->get('easy_mapping');
        $em = $this->container->get('doctrine')->getEntityManager();

        $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($customer->getIdCustomerInfo());

        $this->notMatch = false;

        // Check Customer Info
        $this->isMatch('externalBarcode',
            $customerInfo->getExternalBarcode(),
            $model->externalBarcode
        );
        $this->isMatch('webLoginGoesTo',
            $customerInfo->getWebLoginGoesTo(),
            $model->webLoginGoesTo);
        $this->isMatch('idDistributionChannel',
            $customerInfo->getIdDistributionChannel(),
            $model->idDistributionChannel,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_List', $customerInfo->getIdDistributionChannel())
        );
        $this->isMatch('genderId',
            $customerInfo->getGenderId(),
            $model->genderId,
            $mapping->getMappingTitle('LeepAdmin_Gender_List', $customerInfo->getGenderId())
        );
        $this->isMatch('dateOfBirth',
            $customerInfo->getDateOfBirth(),
            $model->dateOfBirth,
            $customerInfo->getDateOfBirth() != null ? $customerInfo->getDateOfBirth()->format('d/m/Y') : '[Empty]'
        );
        $this->isMatch('title',
            $customerInfo->getTitle(),
            $model->title
        );
        $this->isMatch('firstName',
            $customerInfo->getFirstName(),
            $model->firstName
        );
        $this->isMatch('surName',
            $customerInfo->getSurName(),
            $model->surName
        );
        $this->isMatch('postCode',
            $customerInfo->getPostCode(),
            $model->postCode
        );
        $this->isMatch('isCustomerBeContacted',
            $customerInfo->getIsCustomerBeContacted(),
            $model->isCustomerBeContacted,
            $customerInfo->getIsCustomerBeContacted() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isNutrimeOffered',
            $customerInfo->getIsNutrimeOffered(),
            $model->isNutrimeOffered,
            $customerInfo->getIsNutrimeOffered() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isNotContacted',
            $customerInfo->getIsNotContacted(),
            $model->isNotContacted,
            $customerInfo->getIsNotContacted() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isDestroySample',
            $customerInfo->getIsDestroySample(),
            $model->isDestroySample,
            $customerInfo->getIsDestroySample() ? "Checked" : "Non-checked"
        );
        $this->isMatch('sampleCanBeUsedForScience',
            $customerInfo->getSampleCanBeUsedForScience(),
            $model->sampleCanBeUsedForScience,
            $customerInfo->getSampleCanBeUsedForScience() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isFutureResearchParticipant',
            $customerInfo->getIsFutureResearchParticipant(),
            $model->isFutureResearchParticipant,
            $customerInfo->getIsFutureResearchParticipant() ? "Checked" : "Non-checked"
        );
        $this->isMatch('detailInformation',
            $customerInfo->getDetailInformation(),
            $model->detailInformation
        );

        // CHECKING
        $this->isMatch('previousOrderNumber',
            $customer->getPreviousOrderNumber(),
            $model->previousOrderNumber
        );
        $this->isMatch('trackingCode',
            $customer->getTrackingCode(),
            $model->trackingCode
        );
        $this->isMatch('orderInfoText',
            $customer->getOrderInfoText(),
            $model->orderInfoText
        );
        $this->isMatch('dnaSampleOrderNumberVario',
            $customer->getDnaSampleOrderNumberVario(),
            $model->dnaSampleOrderNumberVario
        );
        $this->isMatch('domain',
            $customer->getDomain(),
            $model->domain
        );
        $this->isMatch('dateOrdered',
            $customer->getDateOrdered(),
            $model->dateOrdered,
            $customer->getDateOrdered() != null ? $customer->getDateOrdered()->format('d/m/Y') : '[Empty]'
        );
        $this->isMatch('orderAge',
            $customer->getOrderAge(),
            $model->orderAge
        );
        $this->isMatch('street',
            $customer->getStreet(),
            $model->street
        );
        $this->isMatch('street2',
            $customer->getStreet2(),
            $model->street2
        );
        $this->isMatch('city',
            $customer->getCity(),
            $model->city
        );
        $this->isMatch('countryId',
            $customer->getCountryId(),
            $model->countryId,
            $mapping->getMappingTitle('LeepAdmin_Country_List', $customer->getCountryId())
        );
        $this->isMatch('email',
            $customerInfo->getEmail(),
            $model->email
        );

        $this->isMatch('telephone',
            $customer->getTelephone(),
            $model->telephone
        );
        $this->isMatch('fax',
            $customer->getFax(),
            $model->fax
        );

        // Invoice
        $this->isMatch('invoiceAddressCompanyName',
            $customer->getInvoiceAddressCompanyName(),
            $model->invoiceAddressCompanyName
        );
        $this->isMatch('invoiceAddressClientName',
            $customer->getInvoiceAddressClientName(),
            $model->invoiceAddressClientName
        );
        $this->isMatch('invoiceAddressStreet',
            $customer->getInvoiceAddressStreet(),
            $model->invoiceAddressStreet
        );
        $this->isMatch('invoiceAddressPostCode',
            $customer->getInvoiceAddressPostCode(),
            $model->invoiceAddressPostCode
        );
        $this->isMatch('invoiceAddressCity',
            $customer->getInvoiceAddressCity(),
            $model->invoiceAddressCity
        );
        $this->isMatch('invoiceAddressIdCountry',
            $customer->getInvoiceAddressIdCountry(),
            $model->invoiceAddressIdCountry,
            $mapping->getMappingTitle('LeepAdmin_Country_List', $customer->getInvoiceAddressIdCountry())
        );
        $this->isMatch('invoiceAddressTelephone',
            $customer->getInvoiceAddressTelephone(),
            $model->invoiceAddressTelephone
        );
        $this->isMatch('invoiceAddressFax',
            $customer->getInvoiceAddressFax(),
            $model->invoiceAddressFax
        );
        $this->isMatch('invoiceAddressUid',
            $customer->getInvoiceAddressUid(),
            $model->invoiceAddressUid
        );

        $this->isMatch('priceOverride',
            $customer->getPriceOverride(),
            $model->priceOverride
        );

        // Check setting
        $this->isMatch('languageId',
            $customer->getLanguageId(),
            $model->languageId,
            $mapping->getMappingTitle('LeepAdmin_Customer_Language', $customer->getLanguageId())
        );

        // Check report detail
        $this->isMatch('companyInfo',
            $customer->getCompanyInfo(),
            $model->companyInfo,
            nl2br($customer->getCompanyInfo())
        );
        $this->isMatch('laboratoryInfo',
            $customer->getLaboratoryInfo(),
            $model->laboratoryInfo,
            nl2br($customer->getLaboratoryInfo())
        );
        $this->isMatch('contactInfo',
            $customer->getContactInfo(),
            $model->contactInfo,
            nl2br($customer->getContactInfo())
        );
        $this->isMatch('letterExtraText',
            $customer->getLetterExtraText(),
            $model->letterExtraText
        );
        $this->isMatch('clinicianInfoText',
            $customer->getClinicianInfoText(),
            $model->clinicianInfoText
        );
        $this->isMatch('text7',
            $customer->getText7(),
            $model->text7
        );
        $this->isMatch('text8',
            $customer->getText8(),
            $model->text8
        );
        $this->isMatch('text9',
            $customer->getText9(),
            $model->text9
        );
        $this->isMatch('text10',
            $customer->getText10(),
            $model->text10
        );
        $this->isMatch('text11',
            $customer->getText11(),
            $model->text11
        );

        // Check GS Settings
        $this->isMatch('doctorsReporting',
            $customer->getDoctorsReporting(),
            $model->doctorsReporting
        );
        $this->isMatch('automaticReporting',
            $customer->getAutomaticReporting(),
            $model->automaticReporting
        );
        $this->isMatch('noReporting',
            $customer->getNoReporting(),
            $model->noReporting
        );
        $this->isMatch('invoicingAndPaymentInfo',
            $customer->getInvoicingAndPaymentInfo(),
            $model->invoicingAndPaymentInfo,
            nl2br($customer->getInvoicingAndPaymentInfo())
        );
        $this->isMatch('idDncReportType',
            $customer->getIdDncReportType(),
            $model->idDncReportType,
            $mapping->getMappingTitle('LeepAdmin_Report_DncReportType', $customer->getIdDncReportType())
        );

        // Check delivery
        $this->isMatch('priceCategoryId',
            $customer->getPriceCategoryId(),
            $model->priceCategoryId,
            $mapping->getMappingTitle('LeepAdmin_PriceCategory_List', $customer->getPriceCategoryId())
        );
        $this->isMatch('reportDeliveryEmail',
            $customer->getReportDeliveryEmail(),
            $model->reportDeliveryEmail
        );
        $this->isMatch('invoiceDeliveryEmail',
            $customer->getInvoiceDeliveryEmail(),
            $model->invoiceDeliveryEmail
        );
        $this->isMatch('invoiceGoesToId',
            $customer->getInvoiceGoesToId(),
            $model->invoiceGoesToId,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_InvoiceGoesTo', $customer->getInvoiceGoesToId())
        );
        $this->isMatch('reportGoesToId',
            $customer->getReportGoesToId(),
            $model->reportGoesToId,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportGoesTo', $customer->getReportGoesToId())
        );
        $this->isMatch('reportDeliveryId',
            $customer->getReportDeliveryId(),
            $model->reportDeliveryId,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_ReportDelivery', $customer->getReportDeliveryId())
        );
        $this->isMatch('idPreferredPayment',
            $customer->getIdPreferredPayment(),
            $model->idPreferredPayment,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_PreferredPayment', $customer->getIdPreferredPayment())
        );
        $this->isMatch('idMarginGoesTo',
            $customer->getIdMarginGoesTo(),
            $model->idMarginGoesTo,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_MarginGoesTo', $customer->getIdMarginGoesTo())
        );
        $this->isMatch('marginOverrideDc',
            floatval($customer->getMarginOverrideDc()),
            floatval($model->marginOverrideDc)
        );
        $this->isMatch('marginOverridePartner',
            floatval($customer->getMarginOverridePartner()),
            floatval($model->marginOverridePartner)
        );
        $this->isMatch('idNutrimeGoesTo',
            $customer->getIdNutrimeGoesTo(),
            $model->idNutrimeGoesTo,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_NutrimeGoesTo', $customer->getIdNutrimeGoesTo())
        );

        // Report delivery options
        $this->isMatch('isReportDeliveryDownloadAccess',
            $customer->getIsReportDeliveryDownloadAccess(),
            $model->isReportDeliveryDownloadAccess,
            $customer->getIsReportDeliveryDownloadAccess() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isReportDeliveryFtpServer',
            $customer->getIsReportDeliveryFtpServer(),
            $model->isReportDeliveryFtpServer,
            $customer->getIsReportDeliveryFtpServer() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isReportDeliveryPrintedBooklet',
            $customer->getIsReportDeliveryPrintedBooklet(),
            $model->isReportDeliveryPrintedBooklet,
            $customer->getIsReportDeliveryPrintedBooklet() ? "Checked" : "Non-checked"
        );
        $this->isMatch('isReportDeliveryDontChargeBooklet',
            $customer->getIsReportDeliveryDontChargeBooklet(),
            $model->isReportDeliveryDontChargeBooklet,
            $customer->getIsReportDeliveryDontChargeBooklet() ? "Checked" : "Non-checked"
        );

        $this->isMatch('canCustomersDownloadReports',
            $customer->getCanCustomersDownloadReports(),
            $model->canCustomersDownloadReports,
            $customer->getCanCustomersDownloadReports() ? "Checked" : "Non-checked"
        );

        $this->isMatch('isDelayDownloadReport',
            $customer->getIsDelayDownloadReport(),
            $model->isDelayDownloadReport
        );

        $this->isMatch('numberDateDelayDownloadReport',
            $customer->getNumberDateDelayDownloadReport(),
            $model->numberDateDelayDownloadReport
        );

        // Report settings
        $this->isMatch('idRecallGoesTo',
            $customer->getIdRecallGoesTo(),
            $model->idRecallGoesTo,
            $mapping->getMappingTitle('LeepAdmin_DistributionChannel_RecallGoesTo', $customer->getIdRecallGoesTo())
        );


        $this->isMatch('idAcquisiteur1',
            $customer->getIdAcquisiteur1(),
            $model->idAcquisiteur1,
            $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $customer->getIdAcquisiteur1())
        );
        $this->isMatch('acquisiteurCommissionRate1',
            floatval($customer->getAcquisiteurCommissionRate1()),
            floatval($model->acquisiteurCommissionRate1)
        );
        $this->isMatch('idAcquisiteur2',
            $customer->getIdAcquisiteur2(),
            $model->idAcquisiteur2,
            $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $customer->getIdAcquisiteur2())
        );
        $this->isMatch('acquisiteurCommissionRate2',
            floatval($customer->getAcquisiteurCommissionRate2()),
            floatval($model->acquisiteurCommissionRate2)
        );
        $this->isMatch('idAcquisiteur3',
            $customer->getIdAcquisiteur3(),
            $model->idAcquisiteur3,
            $mapping->getMappingTitle('LeepAdmin_Acquisiteur_List', $customer->getIdAcquisiteur3())
        );
        $this->isMatch('acquisiteurCommissionRate3',
            floatval($customer->getAcquisiteurCommissionRate3()),
            floatval($model->acquisiteurCommissionRate3)
        );
        $this->isMatch('isDisplayInCustomerDashboard',
            $customer->getIsDisplayInCustomerDashboard(),
            $model->isDisplayInCustomerDashboard
        );

        // Check categories
        $selectedCategories = array();
        $existingCategories = array();
        foreach ($model->orderInfo['categories'] as $idCategory) {
            $selectedCategories[$idCategory] = 1;
        }
        $categories = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedCategory')->findBy(array(
            'customerid' => $customer->getId()
        ));
        foreach ($categories as $category) {
            if (isset($selectedCategories[$category->getCategoryId()])) {
                unset($selectedCategories[$category->getCategoryId()]);
            }
            else {
                $selectedCategories[0] = 1;
            }
            $existingCategories[] = $mapping->getMappingTitle('LeepAdmin_Category_List', $category->getCategoryId());
        }
        if (!empty($selectedCategories)) {
            $this->notMatch = true;
            $this->getForm()->get('orderInfo')->get('categories')->addError(new FormError("Don't match!"));
        }

        // Check products
        $checkProduct = true;
        $selectedProducts = array();
        $existingProducts = array();
        $selectedProducts2 = array();
        foreach ($model->orderInfo['products'] as $idProduct) {
            $selectedProducts[$idProduct] = 1;
            $selectedProducts2[$idProduct] = 1;
        }
        $products = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedProduct')->findBy(array(
            'customerid' => $customer->getId()
        ));
        foreach ($products as $product) {
            if (isset($selectedProducts[$product->getProductId()])) {
                unset($selectedProducts[$product->getProductId()]);
            }
            else {
                $selectedProducts[0] = 1;
            }
            $existingProducts[] = $mapping->getMappingTitle('LeepAdmin_Product_List', $product->getProductId());
        }
        if (!empty($selectedProducts)) {
            $this->notMatch = true;
            $checkProduct = false;
            $this->getForm()->get('orderInfo')->get('products')->addError(new FormError("Don't match!"));
        }

        if ($checkProduct) {
            // Check questions
            $existingQuestions = array();
            $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CustomerQuestion')->findBy(array('customerid' => $customer->getId()));
            foreach ($result as $r) {
                $existingQuestions[$r->getQuestionId()] = $r->getAnswer();
            }
            $productQuestions = Business\Customer\Util::getProductQuestions($this->container);
            foreach ($selectedProducts2 as $idProduct => $tmp) {
                if (isset($productQuestions[$idProduct])) {
                    foreach ($productQuestions[$idProduct] as $question) {
                        $key = 'product_question_'.$idProduct.'_'.$question->getId();
                        $ans = isset($model->orderInfo[$key]) ? $model->orderInfo[$key] : 0;
                        if (is_array($ans)) {
                            $ans = json_encode($ans);
                        }

                        $curAns = isset($existingQuestions[$question->getId()]) ? $existingQuestions[$question->getId()] : 0;
                        if (!$this->compareData($curAns, $ans)) {
                            $this->notMatch = true;

                            $curAnsString = $curAns;
                            switch ($question->getControlTypeId()) {
                                case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_TEXTBOX:
                                    $curAnsString = $curAns;
                                    break;
                                case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_COMBOBOX:
                                case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_CHOICE_RADIO:
                                    $options = explode(';', $question->getControlConfig());
                                    $curAnsString = isset($options[$curAns]) ? $options[$curAns] : '';
                                    break;
                                case Business\Product\Constant::PRODUCT_QUESTION_CONTROL_TYPE_MULTI_CHOICE:
                                    $options = explode(';', $question->getControlConfig());
                                    $curAnsArr = json_decode($curAns, true);
                                    $curAnsString = array();
                                    foreach ($curAnsArr as $curAns) {
                                        $curAnsString[] = isset($options[$curAns]) ? $options[$curAns] : '';
                                    }
                                    $curAnsString = implode(', ', $curAnsString);

                                    break;
                            }
                            $this->getForm()->get('orderInfo')->get($key)->addError(new FormError("Don't match!"));
                        }
                    }
                }
            }

        }

        // Check special products
        $selectedSpecialProducts = array();
        $existingSpecialProducts = array();
        foreach ($model->orderInfo['specialProducts'] as $idSpecialProduct) {
            $selectedSpecialProducts[$idSpecialProduct] = 1;
        }
        $specialProducts = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderedSpecialProduct')->findBy(array(
            'idCustomer' => $customer->getId()
        ));
        foreach ($specialProducts as $specialProduct) {
            $idSpecialProduct = $specialProduct->getIdSpecialProduct();
            if (!empty($idSpecialProduct)) {
                if (isset($selectedSpecialProducts[$idSpecialProduct])) {
                    unset($selectedSpecialProducts[$idSpecialProduct]);
                }
                else {
                    $selectedSpecialProducts[0] = 1;
                }
                $existingSpecialProducts[] = $mapping->getMappingTitle('LeepAdmin_SpecialProduct_List', $idSpecialProduct);
            }
        }
        if (!empty($selectedSpecialProducts)) {
            $this->notMatch = true;
            $this->getForm()->get('orderInfo')->get('specialProducts')->addError(new FormError("Don't match! "));
        }

        // Check questions
        $formQuestions = Util::getFormQuestions($this->container);
        $formQuestionsChoices = array();

        foreach ($formQuestions as $formQuestion) {
            $formQuestionsChoices['form_question_'.$formQuestion['id']] = $formQuestion['choices'];
        }

        $customerFormQuestions = Util::loadFormQuestions($this->container, $em, $customer);
        foreach ($model->questions as $questionKey => $answer) {
            if (isset($customerFormQuestions[$questionKey])) {
                $currentAnswer = $customerFormQuestions[$questionKey];

                if (is_array($currentAnswer)) {
                    $filterCurrentAnswer = array();
                    foreach ($currentAnswer as $k => $v) {
                        if ($v != '') {
                            $filterCurrentAnswer[$k] = $v;
                        }
                    }

                    if (!$this->compareArray($answer, $filterCurrentAnswer)) {
                        $this->notMatch = true;
                        $text = array();
                        foreach ($filterCurrentAnswer as $v) {
                            if (isset($formQuestionsChoices[$questionKey][$v])) {
                                $text[] = $formQuestionsChoices[$questionKey][$v];
                            }
                        }
                        if (empty($text)) {
                            $text = array('[None selected]');
                        }

                        $this->getForm()->get('questions')->get($questionKey)->addError(new FormError("Don't matched!"));
                    }
                }
                else {
                    if (!$this->compareData($currentAnswer, $answer)) {
                        $this->notMatch = true;
                        $text = $currentAnswer;
                        if (isset($formQuestionsChoices[$questionKey][$currentAnswer])) {
                            $text = $formQuestionsChoices[$questionKey][$currentAnswer];
                        }
                        $this->getForm()->get('questions')->get($questionKey)->addError(new FormError("Don't matched!"));
                    }
                }
            }
            else {
                if (!empty($answer)) {
                    $this->getForm()->get('questions')->get($questionKey)->addError(new FormError("Don't matched!"));
                }
            }
        }
    }

    public $notMatch = false;
    public function isMatch($field, $current, $new, $display = FALSE) {
        if ($display === FALSE) $display = $current;
        if (is_string($current)) {
            if (strcmp(trim($current), trim($new)) !== 0) {
                $this->dontMatch($field, $display);
            }
        }
        else {
            if ($current != $new) {
                $this->dontMatch($field, $display);
            }
        }
    }

    public function compareArray($arr1, $arr2) {
        return $this->compareArrayLeft($arr1, $arr2) && $this->compareArrayLeft($arr2, $arr1);
    }
    private function compareArrayLeft($arr1, $arr2) {
        foreach ($arr1 as $v) {
            $found = false;
            foreach ($arr2 as $v2) {
                if ($v == $v2) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                return false;
            }
        }
        return true;
    }

    public function compareData($current, $new) {
        if (is_string($current)) {
            if (strcmp(trim($current), trim($new)) !== 0) {
                return false;
            }
        }
        else {
            if ($current != $new) {
                return false;
            }
        }
        return true;
    }
    public function dontMatch($field, $existingValue) {
        $this->notMatch = true;
        $this->getForm()->get($field)->addError(new FormError("Don't match!"));
    }

    public function override($model) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($this->customer->getIdCustomerInfo());
        $customerInfo->setExternalBarcode($model->externalBarcode);
        $customerInfo->setFirstName($model->firstName);
        $customerInfo->setSurName($model->surName);
        $customerInfo->setDateOfBirth($model->dateOfBirth);
        $customerInfo->setEmail($model->email);
        $customerInfo->setPostCode($model->postCode);
        $customerInfo->setGenderId($model->genderId);
        $customerInfo->setTitle($model->title);
        $customerInfo->setIsCustomerBeContacted($model->isCustomerBeContacted);
        $customerInfo->setIsNutrimeOffered($model->isNutrimeOffered);
        $customerInfo->setIsNotContacted($model->isNotContacted);
        $customerInfo->setIsDestroySample($model->isDestroySample);
        $customerInfo->setSampleCanBeUsedForScience($model->sampleCanBeUsedForScience);
        $customerInfo->setIsFutureResearchParticipant($model->isFutureResearchParticipant);
        $customerInfo->setWebLoginGoesTo($model->webLoginGoesTo);
        $customerInfo->setDetailInformation($model->detailInformation);
        $em->persist($customerInfo);
        $em->flush();

        $this->customer->setPreviousOrderNumber($model->previousOrderNumber);

        $this->customer->setDnaSampleOrderNumberVario($model->dnaSampleOrderNumberVario);
        $this->customer->setTrackingCode($model->trackingCode);
        $this->customer->setOrderInfoText($model->orderInfoText);
        $this->customer->setDomain($model->domain);
        $this->customer->setDateOrdered($model->dateOrdered);
        $this->customer->setOrderAge($model->orderAge);
        $this->customer->setLaboratoryDetails($model->laboratoryDetails);

        $this->customer->setStreet($model->street);
        $this->customer->setStreet2($model->street2);
        $this->customer->setCity($model->city);
        $this->customer->setCountryId($model->countryId);
        $this->customer->setTelephone($model->telephone);
        $this->customer->setFax($model->fax);
        $this->customer->setNotes($model->notes);

        $this->customer->setInvoiceAddressIsUsed($model->invoiceAddressIsUsed);
        $this->customer->setInvoiceAddressClientName($model->invoiceAddressClientName);
        $this->customer->setInvoiceAddressCompanyName($model->invoiceAddressCompanyName);
        $this->customer->setInvoiceAddressStreet($model->invoiceAddressStreet);
        $this->customer->setInvoiceAddressPostCode($model->invoiceAddressPostCode);
        $this->customer->setInvoiceAddressCity($model->invoiceAddressCity);
        $this->customer->setInvoiceAddressIdCountry($model->invoiceAddressIdCountry);
        $this->customer->setInvoiceAddressTelephone($model->invoiceAddressTelephone);
        $this->customer->setInvoiceAddressFax($model->invoiceAddressFax);
        $this->customer->setInvoiceAddressUid($model->invoiceAddressUid);
        $this->customer->setPriceOverride($model->priceOverride);

        $this->customer->setLanguageId($model->languageId);

        $this->customer->setCompanyInfo($model->companyInfo);
        $this->customer->setLaboratoryInfo($model->laboratoryInfo);
        $this->customer->setContactInfo($model->contactInfo);
        $this->customer->setLetterExtraText($model->letterExtraText);
        $this->customer->setClinicianInfoText($model->clinicianInfoText);
        $this->customer->setText7($model->text7);
        $this->customer->setText8($model->text8);
        $this->customer->setText9($model->text9);
        $this->customer->setText10($model->text10);
        $this->customer->setText11($model->text11);

        $this->customer->setDoctorsReporting($model->doctorsReporting);
        $this->customer->setAutomaticReporting($model->automaticReporting);
        $this->customer->setNoReporting($model->noReporting);
        $this->customer->setInvoicingAndPaymentInfo($model->invoicingAndPaymentInfo);
        $this->customer->setIdDncReportType($model->idDncReportType);

        $this->customer->setPriceCategoryId($model->priceCategoryId);
        $this->customer->setReportDeliveryEmail($model->reportDeliveryEmail);
        $this->customer->setInvoiceDeliveryEmail($model->invoiceDeliveryEmail);

        $this->customer->setInvoiceGoesToId($model->invoiceGoesToId);
        $this->customer->setReportGoesToId($model->reportGoesToId);
        $this->customer->setReportDeliveryId($model->reportDeliveryId);

        if ($model->ftpServer != null) {
            $this->customer->setFtpServerName($model->ftpServer->server);
            $this->customer->setFtpUsername($model->ftpServer->username);
            $this->customer->setFtpPassword($model->ftpServer->password);
        }

        $this->customer->setIdPreferredPayment($model->idPreferredPayment);
        $this->customer->setIdMarginGoesTo($model->idMarginGoesTo);
        $this->customer->setMarginOverrideDc($model->marginOverrideDc);
        $this->customer->setMarginOverridePartner($model->marginOverridePartner);

        $this->customer->setIdNutrimeGoesTo($model->idNutrimeGoesTo);

        // Report delivery options
        $this->customer->setIsReportDeliveryDownloadAccess($model->isReportDeliveryDownloadAccess);
        $this->customer->setisReportDeliveryFtpServer($model->isReportDeliveryFtpServer);
        $this->customer->setIsReportDeliveryPrintedBooklet($model->isReportDeliveryPrintedBooklet);
        $this->customer->setIsReportDeliveryDontChargeBooklet($model->isReportDeliveryDontChargeBooklet);

        $this->customer->setCanCustomersDownloadReports($model->canCustomersDownloadReports);

        $this->customer->setIdRecallGoesTo($model->idRecallGoesTo);

        $this->customer->setIdAcquisiteur1($model->idAcquisiteur1);
        $this->customer->setAcquisiteurCommissionRate1($model->acquisiteurCommissionRate1);
        $this->customer->setIdAcquisiteur2($model->idAcquisiteur2);
        $this->customer->setAcquisiteurCommissionRate2($model->acquisiteurCommissionRate2);
        $this->customer->setIdAcquisiteur3($model->idAcquisiteur3);
        $this->customer->setAcquisiteurCommissionRate3($model->acquisiteurCommissionRate3);
        $this->customer->setIsDisplayInCustomerDashboard($model->isDisplayInCustomerDashboard);

        $this->customer->setIsRecheckGeocoding(1);

        ////////////////////////////////////
        // x. Update other information
        Business\CustomerInfo\Utils::ensureDuplication($em, $customerInfo);

        // Save order information
        Util::removeOrderInformation($this->container, $em, $this->customer);
        Util::saveOrderInformation($this->container, $em, $this->customer, $model->orderInfo);
        Util::saveFormQuestions($this->container, $em, $this->customer, $model->questions);
        Util::updateRevenueEstimation($this->container, $this->customer->getId());

        $em->persist($this->customer);
        $em->flush();

        return true;
    }
}
