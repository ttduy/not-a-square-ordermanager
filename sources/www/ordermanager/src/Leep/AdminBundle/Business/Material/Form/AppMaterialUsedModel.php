<?php
namespace Leep\AdminBundle\Business\Material\Form;

class AppMaterialUsedModel {
	public $id;
	public $isSelected;
    public $quantity;
    public $dateTaken;
    public $usedFor;
    public $takenBy;
}
