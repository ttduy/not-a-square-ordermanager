<?php
namespace Leep\AdminBundle\Business\Material;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $attachment;
    public $attachmentKey;

    public $novoDescription;
    public $name;
    public $description;
    public $materialTypeId;
    public $supplier;
    public $price;
    public $unitsPerPrice;
    public $unitDescription;
    public $articleNumber;
    public $notes;
    public $expiryDate;
    public $expiryDateWarningThreshold;
    public $warningThreshold;
    public $extraNotes;
    public $fileAttachment;
    public $simpleFileAttachment;
}
