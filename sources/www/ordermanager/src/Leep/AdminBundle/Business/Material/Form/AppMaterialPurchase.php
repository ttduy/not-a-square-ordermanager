<?php
namespace Leep\AdminBundle\Business\Material\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppMaterialPurchase extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Material\Form\AppMaterialPurchaseModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_material_purchase';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();   
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('dateOrdered', 'datepicker', array(
            'label' => 'Date ordered',
            'required'  => false
        ));
        $builder->add('orderedReceived',     'checkbox', array(
            'label'  => 'Ordered received',
            'required' => false
        ));
        $builder->add('receivedBy', 'text', array(
            'label'  => 'Received by',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('receivedDate', 'datepicker', array(
            'label' => 'Date received',
            'required'  => false
        ));
        $builder->add('quantity', 'text', array(
            'label'  => 'Quantity',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('orderBy',    'text', array(
            'label'  => 'Order By',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('externalLot',    'text', array(
            'label'  => 'External Lot',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('lotNumber',    'text', array(
            'label'  => 'Novogenia Lot Number',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningDisabled', 'checkbox', array(
            'label'    => 'Disabled',
            'required' => false
        ));
        $builder->add('info',    'text', array(
            'label'  => 'Info',
            'required' => false,
            'attr' => array(
                'class' => 'longinput'
            )
        ));
    }
}