<?php
namespace Leep\AdminBundle\Business\Material;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public $attachmentKey = false;
    public function generateAttachmentKey() {
        $success = false;
        while (!$success) {
            $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
            $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
            if (!$duplicate) {
                $success = true;
            }
        }
    }


    public function getDefaultFormModel() {
        $this->generateAttachmentKey();
        $model = new CreateModel();
        $model->attachmentKey = $this->attachmentKey;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'materials/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        $builder->add('sectionMaterial', 'section', array(
            'label'    => 'Material',
            'property_path' => false
        ));

        $builder->add('novoDescription', 'text', array(
            'label'    => 'Novo Description',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => false
        ));
        $builder->add('materialTypeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_MaterialType_List')
        ));
        $builder->add('supplier', 'text', array(
            'label'    => 'Supplier',
            'required' => false
        ));
        $builder->add('price', 'text', array(
            'label'    => 'Price',
            'required' => false
        ));
        $builder->add('unitsPerPrice', 'text', array(
            'label'    => 'Units Per Price',
            'required' => false
        ));
        $builder->add('unitDescription', 'text', array(
            'label'    => 'Unit Description',
            'required' => false
        ));
        $builder->add('articleNumber', 'text', array(
            'label'    => 'Article Number',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));
        $builder->add('warningThreshold', 'text', array(
            'label'    => 'Warning threshold',
            'required' => false
        ));
        $builder->add('extraNotes', 'textarea', array(
            'label'    => 'Extra Notes',
            'required' => false,
            'attr'     => array(
                'rows' => 5, 'cols' => 70
            )
        ));
        $builder->add('simpleFileAttachment', 'app_file_uploader', array(
            'label'    => 'Attachment',
            'required' => false,
            'path'     => Constant::MATERIAL_FILE_DIR
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $material = new Entity\Materials();
        $material->setNovoDescription($model->novoDescription);
        $material->setName($model->name);
        $material->setDescription($model->description);
        $material->setMaterialTypeId($model->materialTypeId);
        $material->setSupplier($model->supplier);
        $material->setPrice($model->price);
        $material->setUnitsPerPrice($model->unitsPerPrice);
        $material->setUnitDescription($model->unitDescription);
        $material->setArticleNumber($model->articleNumber);
        $material->setNotes($model->notes);
        $material->setExpiryDate($model->expiryDate);
        $material->setExpiryDateWarningThreshold($model->expiryDateWarningThreshold);
        $material->setWarningThreshold($model->warningThreshold);
        $material->setExtraNotes($model->extraNotes);
        $material->setAttachmentKey($model->attachmentKey);
        $material->setStockStatus(0);
        // File upload
        // $simpleFileUpload = $model->simpleFileAttachment['attachment'];
        // if ($simpleFileUpload) {
        //     $attachmentDir = $this->container->getParameter('files_dir').'/'.Constant::MATERIAL_FILE_DIR;
        //     $name = $material->getId().'.'.$simpleFileUpload->guessExtension();
        //     $simpleFileUpload->move($attachmentDir, $name);
        //     $material->setSimpleFileAttachment($name);
        // }
        $em->persist($material);
        $em->flush();

        parent::onSuccess();
    }
}
