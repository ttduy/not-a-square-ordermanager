<?php
namespace Leep\AdminBundle\Business\Material\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppMultiMaterialPurchase extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Leep\AdminBundle\Business\Material\Form\AppMultiMaterialPurchaseModel'
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_multi_material_purchase';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['disable-delete'] = true;
        $view->vars['disable-insert'] = true;
        $view->vars['disable-add'] = true;
        parent::finishView($view, $form, $options);
        $data = $form->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
        $builder->add('id', 'hidden');

        $builder->add('section', 'label');

        $builder->add('purchaseList', 'collection', array(
            'type' => new AppMaterialPurchase($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));
    }
}
