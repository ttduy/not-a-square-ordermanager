<?php
namespace Leep\AdminBundle\Business\Material;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class MaterialGridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $em = $controller->get('doctrine')->getEntityManager();
        $stock = array();
        $waiting = array();
        $stockChanges = array();

        $today = new \DateTime();
        $thisYear = $today->format('Y');
        $lastYear = $thisYear - 1;
        $objLastDecember = \DateTime::createFromFormat('Y/m/d', $lastYear . '/12/1');

        $result = $em->getRepository('AppDatabaseMainBundle:MaterialsPurchase')->findAll();
        foreach ($result as $purchase) {
            $materialId = $purchase->getMaterialId();
            $receivedDate = $purchase->getReceivedDate();

            if ($receivedDate) {
                $interval = date_diff($objLastDecember, $receivedDate);
                $receivedDate = $receivedDate->format('Y/m/1');
                if (intval($interval->format('%R%s')) < 0) {
                    $receivedDate = null;
                }
            }

            if (empty($stock[$materialId])) {
                $stock[$materialId] = 0;
            }
            if (empty($waiting[$materialId])) {
                $waiting[$materialId] = 0;
            }

            if ($purchase->getOrderedReceived() == 1) {
                $stock[$materialId] += $purchase->getQuantity();

                if ($receivedDate) {
                    $objDate = \DateTime::createFromFormat('Y/m/d', $receivedDate);
                    $receivedDate = $objDate->format('Y/m/d');
                    if (empty($stockChanges[$receivedDate])) {
                        $stockChanges[$receivedDate] = array();
                    }

                    if (empty($stockChanges[$receivedDate][$materialId])) {
                        $stockChanges[$receivedDate][$materialId] = 0;
                    }

                    $stockChanges[$receivedDate][$materialId] += $purchase->getQuantity();
                }
            }
            else {
                $waiting[$materialId] += $purchase->getQuantity();
            }
        }

        $result = $em->getRepository('AppDatabaseMainBundle:MaterialsUsed')->findAll();
        foreach ($result as $used) {
            $materialId = $used->getMaterialId();
            $dateTaken = $used->getDatetaken();

            if ($dateTaken) {
                $interval = date_diff($objLastDecember, $dateTaken);
                $dateTaken = $dateTaken->format('Y/m/1');
                if (intval($interval->format('%R%s')) < 0) {
                    $dateTaken = null;
                }
            }

            if (empty($stock[$materialId])) {
                $stock[$materialId] = 0;
            }

            if ($dateTaken) {
                $objDate = \DateTime::createFromFormat('Y/m/d', $dateTaken);
                $dateTaken = $objDate->format('Y/m/d');
                if (empty($stockChanges[$dateTaken])) {
                    $stockChanges[$dateTaken] = array();
                }

                if (empty($stockChanges[$dateTaken][$materialId])) {
                    $stockChanges[$dateTaken][$materialId] = 0;
                }

                $stockChanges[$dateTaken][$materialId] -= $used->getQuantity();
            }

            $stock[$materialId] -= $used->getQuantity();
        }

        // Compute price
        $materialPrices = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Materials')->findAll();
        foreach ($result as $m) {
            $unit = floatval($m->getUnitsPerPrice());
            if (empty($unit)) {
                $unit = 1;
            }
            $materialPrices[$m->getId()] = floatval($m->getPrice()) / $unit;
        }

        $sumStock = 0;
        $sumWaiting = 0;
        $sumChanges = array();

        $date = $lastYear . '/12/1';
        $objDate = \DateTime::createFromFormat('Y/m/d', $date);
        $sumChanges[$objDate->format('Y/m/d')] = array(
                                    'view'      => 0,
                                    'change'    => 0,
                                    'sum'       => 0,
                                    'name'      => ucfirst($objDate->format('M Y'))
                            );

        for ($i = 1; $i <= 12; $i++) {
            $date = $thisYear . '/' . $i . '/1';
            $objDate = \DateTime::createFromFormat('Y/m/d', $date);
            $sumChanges[$objDate->format('Y/m/d')] = array(
                                        'view'      => 1,
                                        'change'    => 0,
                                        'sum'       => 0,
                                        'name'      => ucfirst($objDate->format('M Y'))
                                );
        }


        foreach ($stockChanges as $date => $arrMaterials) {
            foreach ($arrMaterials as $materialId => $num) {
                $price = isset($materialPrices[$materialId]) ? $materialPrices[$materialId] : 0;
                $sumChanges[$date]['sum'] += $num * $price;
            }

            $sumChanges[$date]['sum'] = number_format($sumChanges[$date]['sum'], 2);
        }

        $amountChangesByMonths = array();
        $preSumChange = 0;
        foreach ($sumChanges as $eachMonth) {
            $eachMonth['change'] = $eachMonth['sum'] - $preSumChange;
            $preSumChange = $eachMonth['sum'];
            $amountChangesByMonths[] = $eachMonth;
        }

        foreach ($stock as $materialId => $num) {
            $price = isset($materialPrices[$materialId]) ? $materialPrices[$materialId] : 0;
            $sumStock += $num * $price;
        }
        foreach ($waiting as $materialId => $num) {
            $price = isset($materialPrices[$materialId]) ? $materialPrices[$materialId] : 0;
            $sumWaiting += $num * $price;
        }

        $data['amountMaterialInStock'] = number_format($sumStock, 2);
        $data['amountMaterialWaiting'] = number_format($sumWaiting, 2);
        $data['amountChangesByMonths'] = $amountChangesByMonths;

        $sumMaterialInStock = $sumStock;
        $sumMaterialWaiting = $sumWaiting;


        //--------------------------------------------------------------------------------------------------------------
        // Calculate pellet
        $stockSection = Business\Pellet\Constant::getStockSection();
        $waitingSection = Business\Pellet\Constant::getWaitingSection();

        $result = $em->getRepository('AppDatabaseMainBundle:PelletEntry')->findAll();
        foreach ($result as $entry) {
            $pelletId = $entry->getIdPellet();
            $receivedDate = $entry->getReceived();

            if ($receivedDate) {
                $interval = date_diff($objLastDecember, $receivedDate);
                $receivedDate = $receivedDate->format('Y/m/1');
                if (intval($interval->format('%R%s')) < 0) {
                    $receivedDate = null;
                }
            }

            if (empty($stock[$pelletId])) {
                $stock[$pelletId] = 0;
            }
            if (empty($waiting[$pelletId])) {
                $waiting[$pelletId] = 0;
            }

            if ($entry->getReceivedStatus() == 1 &&
                !$entry->getIsSelected() &&
                in_array($entry->getSection(), $stockSection)) {
                $stock[$pelletId] += $entry->getCups();

                if ($receivedDate) {
                    $objDate = \DateTime::createFromFormat('Y/m/d', $receivedDate);
                    $receivedDate = $objDate->format('Y/m/d');
                    if (empty($stockChanges[$receivedDate])) {
                        $stockChanges[$receivedDate] = array();
                    }

                    if (empty($stockChanges[$receivedDate][$pelletId])) {
                        $stockChanges[$receivedDate][$pelletId] = 0;
                    }

                    $stockChanges[$receivedDate][$pelletId] += $entry->getCups();
                }
            } else if (!$entry->getReceivedStatus() &&
                in_array($entry->getSection(), $waitingSection)) {
                $waiting[$pelletId] += $entry->getCups();
            }
        }

        // Compute price
        $pelletPrices = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Pellet')->findAll();
        foreach ($result as $m) {
            $unit = floatval($m->getUnitsPerPrice());
            if (empty($unit)) {
                $unit = 1;
            }
            $pelletPrices[$m->getId()] = floatval($m->getPrice()) / $unit;
        }

        $sumStock = 0;
        $sumWaiting = 0;
        $sumChanges = array();

        $date = $lastYear . '/12/1';
        $objDate = \DateTime::createFromFormat('Y/m/d', $date);
        $sumChanges[$objDate->format('Y/m/d')] = array(
                                    'view'      => 0,
                                    'change'    => 0,
                                    'sum'       => 0,
                                    'name'      => ucfirst($objDate->format('M Y'))
                            );

        for ($i = 1; $i <= 12; $i++) {
            $date = $thisYear . '/' . $i . '/1';
            $objDate = \DateTime::createFromFormat('Y/m/d', $date);
            $sumChanges[$objDate->format('Y/m/d')] = array(
                                        'view'      => 1,
                                        'change'    => 0,
                                        'sum'       => 0,
                                        'name'      => ucfirst($objDate->format('M Y'))
                                );
        }


        foreach ($stockChanges as $date => $arrPellets) {
            foreach ($arrPellets as $pelletId => $num) {
                $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
                $sumChanges[$date]['sum'] += $num * $price;
            }

            $sumChanges[$date]['sum'] = number_format($sumChanges[$date]['sum'], 2);
        }

        $amountChangesByMonths = array();
        $preSumChange = 0;
        foreach ($sumChanges as $eachMonth) {
            $eachMonth['change'] = $eachMonth['sum'] - $preSumChange;
            $preSumChange = $eachMonth['sum'];
            $amountChangesByMonths[] = $eachMonth;
        }

        foreach ($stock as $pelletId => $num) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumStock += $num * $price;
        }
        foreach ($waiting as $pelletId => $num) {
            $price = isset($pelletPrices[$pelletId]) ? $pelletPrices[$pelletId] : 0;
            $sumWaiting += $num * $price;
        }

        $data['amountPelletInStock'] = number_format($sumStock, 2);
        $data['amountPelletWaiting'] = number_format($sumWaiting, 2);

        $amountInStock = $sumMaterialInStock + $sumStock;
        $data['amountInStock'] = number_format($amountInStock, 2);

        $amountWaiting = $sumMaterialWaiting + $sumWaiting;
        $data['amountWaiting'] = number_format($amountWaiting, 2);
    }
}
