<?php
namespace Leep\AdminBundle\Business\Material;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateMaterialUsedHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateMaterialUsedModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('quantity', 'text', array(
            'label'  => 'Quantity',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('dateTaken', 'datepicker', array(
            'label' => 'Date taken',
            'required'  => false
        ));
        $builder->add('usedFor',    'text', array(
            'label'  => 'Used For',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('takenBy',    'text', array(
            'label'  => 'Taken By',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();        
        $idMaterial = $this->container->get('request')->get('idMaterial', 0);

        $materialUsed = new Entity\MaterialsUsed();
        $materialUsed->setMaterialId($idMaterial);
        $materialUsed->setQuantity($model->quantity);
        $materialUsed->setDateTaken($model->dateTaken);
        $materialUsed->setUsedFor($model->usedFor);
        $materialUsed->setTakenBy($model->takenBy);

        $em->persist($materialUsed);
        $em->flush();

        parent::onSuccess();
    }
}
