<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppViewHandler;
use App\Database\MainBundle\Entity;

class ViewHandler extends AppViewHandler {   
    public function read() {
        $data = array();
        $mapping = $this->container->get('easy_mapping');


        $id = $this->container->get('request')->query->get('id', 0);
        $material = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials')->findOneById($id);
        $data[] = $this->addSection('Material');
        $data[] = $this->add('Novo Description', $material->getNovoDescription());
        $data[] = $this->add('Name', $material->getName());        
        $data[] = $this->add('Description', $material->getDescription());        
        $data[] = $this->add('Material Type', $mapping->getMappingTitle('LeepAdmin_MaterialType_List', $material->getMaterialTypeId()));        
        $data[] = $this->add('Supplier', $material->getSupplier());        
        $data[] = $this->add('Price', $material->getPrice());        
        $data[] = $this->add('Units Per Price', $material->getUnitsPerPrice());        
        $data[] = $this->add('Unit Description', $material->getUnitDescription());        
        $data[] = $this->add('Article Number', $material->getArticleNumber());        
        $data[] = $this->add('Notes', $material->getNotes());        

        return $data;
    }
}
