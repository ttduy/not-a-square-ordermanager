<?php
namespace Leep\AdminBundle\Business\Material\Form;

class AppMaterialPurchaseModel {
    public $dateOrdered;
    public $orderedReceived;
    public $receivedBy;
    public $receivedDate;
    public $quantity;
    public $orderBy;
    public $externalLot;
    public $lotNumber;
    public $expiryDate;
    public $expiryDateWarningDisabled;
    public $info;
}
