<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkEditStockHandler extends AppEditHandler {
    public $selectedId = [];

    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findBy(['id' => $this->selectedId]);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $listOfMaterial = '';
        $valueReturn = [];
        $valueReturn['materialList'] = '';
        $valueReturn['purchase'] = [];
        foreach ($entity as $key => $value) {
            $listOfMaterial .= $value->getName(). "\n";

            // get Purchase
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:MaterialsPurchase', 'p')
                ->andWhere('p.materialid = :materialId')
                ->setParameter('materialId', $value->getId())
                ->orderBy('p.dateordered', 'DESC');
            $result = $query->getQuery()->getResult();
            $purchaseList = [];
            foreach ($result as $purchase) {
                $m = new Form\AppMaterialPurchaseModel();
                $m->dateOrdered = $purchase->getDateOrdered();
                $m->orderedReceived = $purchase->getOrderedReceived();
                $m->receivedBy = $purchase->getReceivedBy();
                $m->receivedDate = $purchase->getReceivedDate();
                $m->quantity = $purchase->getQuantity();
                $m->orderBy = $purchase->getOrderBy();
                $m->lotNumber = $purchase->getLotNumber();
                $m->externalLot = $purchase->getExternalLot();
                $m->expiryDate = $purchase->getExpiryDate();
                $m->expiryDateWarningDisabled = $purchase->getExpiryDateWarningDisabled();
                $m->info = $purchase->getInfo();
                $purchaseList[] = $m;
            }

            $purchase = new Form\AppMultiMaterialPurchaseModel();
            $purchase->id = $value->getId();
            $purchase->section = $value->getName();
            $purchase->purchaseList = $purchaseList;
            $valueReturn['purchase'] [] = $purchase;

            // get Used
            $usedList = array();
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
                ->andWhere('p.materialid = :materialId')
                ->setParameter('materialId', $value->getId())
                ->orderBy('p.datetaken', 'DESC');
            $result = $query->getQuery()->getResult();
            foreach ($result as $used) {
                $m = new Form\AppMaterialUsedModel();
                $m->id = $used->getId();
                $m->isSelected = $used->getIsSelected();
                $m->quantity = $used->getQuantity();
                $m->dateTaken = $used->getDateTaken();
                $m->usedFor = $used->getUsedFor();
                $m->takenBy = $used->getTakenBy();
                $usedList[] = $m;
            }
            $stockUsed = new Form\AppMultiMaterialUsedModel();
            $stockUsed->id = $value->getId();
            $stockUsed->section = $value->getName();
            $stockUsed->usedList = $usedList;
            $valueReturn['used'][] = $stockUsed;
        }
        $valueReturn['materialList'] = $listOfMaterial;

        return $valueReturn;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $entity = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findBy(['id' => $this->selectedId]);

        $builder->add('isPurchase', 'checkbox', array(
            'label' => 'Purchase',
            'required'      => false,
            'attr'  => [
                'onChange' => "changeCheckBox();"
                ]
        ));
        $builder->add('isStock', 'checkbox', array(
            'label' => 'Stock',
            'required'      => false,
            'attr'  => [
                'onChange' => "changeCheckBox();"
                ]
        ));

        $builder->add('materialList', 'textarea', array(
            'label' => 'Material list',
            'required' => false,
            'attr'     => [
                'rows'          => 8,
                'style'     => 'width: 370px'
                ]
        ));

        $builder->add('section2', 'section', array(
            'label' => 'Purchase',
        ));

        $builder->add('purchase', 'container_collection', array(
            'type' => new Form\AppMultiMaterialPurchase($this->container)
        ));

        $builder->add('section3', 'section', array(
            'label' => 'Stock used up',
        ));

        $builder->add('used', 'container_collection', array(
            'type' => new Form\AppMultiMaterialUsed($this->container)
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $isPurchase = $model['isPurchase'];
        $isStock = $model['isStock'];
        $used = $model['used'];
        $purchase = $model['purchase'];

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();

        if ($isPurchase == true) {

            foreach ($purchase as $key => $row) {
                $idMaterial = $row->id;
                $purchaseList = $row->purchaseList;

                $filters = array('materialid' => $idMaterial);
                $dbHelper->delete($em, 'AppDatabaseMainBundle:MaterialsPurchase', $filters);

                foreach ($purchaseList as $purchaseRow) {
                    if (empty($purchaseRow)) continue;
                    $purchase = new Entity\MaterialsPurchase();
                    $purchase->setMaterialId($idMaterial);
                    $purchase->setDateOrdered($purchaseRow->dateOrdered);
                    $purchase->setOrderedReceived($purchaseRow->orderedReceived);
                    $purchase->setReceivedBy($purchaseRow->receivedBy);
                    $purchase->setReceivedDate($purchaseRow->receivedDate);
                    $purchase->setQuantity($purchaseRow->quantity);
                    $purchase->setOrderBy($purchaseRow->orderBy);
                    $purchase->setExternalLot($purchaseRow->externalLot);
                    $purchase->setLotNumber($purchaseRow->lotNumber);
                    $purchase->setExpiryDate($purchaseRow->expiryDate);
                    $purchase->setExpiryDateWarningDisabled($purchaseRow->expiryDateWarningDisabled);
                    $purchase->setInfo($purchaseRow->info);
                    $em->persist($purchase);
                }
            }
        }

        if ($isStock == true) {
            foreach ($used as $key => $row) {
                $idMaterial = $row->id;
                $usedList = $row->usedList;

                $filters = array('materialid' => $idMaterial);
                $dbHelper->delete($em, 'AppDatabaseMainBundle:MaterialsUsed', $filters);

                foreach ($usedList as $usedRow) {
                    if (empty($usedRow)) continue;
                    $used = new Entity\MaterialsUsed();
                    $used->setIsSelected($usedRow->isSelected);
                    $used->setMaterialId($idMaterial);
                    $used->setQuantity($usedRow->quantity);
                    $used->setDateTaken($usedRow->dateTaken);
                    $used->setUsedFor($usedRow->usedFor);
                    $used->setTakenBy($usedRow->takenBy);
                    $em->persist($used);
                }
            }
        }

        $em->flush();

        parent::onSuccess();

        $this->reloadForm();
    }
}
