<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function getSupplier() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.supplier')
            ->from('AppDatabaseMainBundle:Materials', 'p')
            ->andWhere('(p.isdeleted = 0) OR (p.isdeleted IS NULL)')
            ->orderBy('p.supplier', 'ASC');
        $materials = $query->getQuery()->getResult();

        $map = array();
        foreach ($materials as $m) {
            if (trim($m['supplier']) != '') {
                $map[$m['supplier']] = $m['supplier'];
            }
        }
        return $map;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

         $builder->add('novoDescription', 'text', array(
            'label'    => 'Novo Description',
            'required' => false
        ));
        $builder->add('name', 'textarea', array(
            'label'    => 'Name',
            'attr'     => [
                'rows'      => '5',
                'style'     => 'width: 370px'
                ],
            'required' => false
        ));
        $builder->add('materialTypeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_MaterialType_List'),
            'empty_value' => false
        ));
        $builder->add('supplier', 'searchable_box', array(
            'label'    => 'Supplier',
            'required' => false,
            'choices'  => array('' => 'All') + $this->getSupplier(),
            'empty_value' => false
        ));
        $builder->add('expireOnDate', 'datepicker', array(
            'label'    => 'Be expired on',
            'required' => false
        ));
        $builder->add('stockStatus', 'choice', array(
            'label'    => 'Stock status',
            'required' => false,
            'choices'  => array(0 => 'All', 1 => 'Low stock'),
            'empty_value' => false
        ));

        return $builder;
    }
}
