<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public $attachmentKey = null;

    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if (empty($this->attachmentKey)) {
            $success = false;
            while (!$success) {
                $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
                $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
                if (!$duplicate) {
                    $success = true;
                }
            }
        }
    }
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();

        $entityAttachmentKey = $entity->getAttachmentKey();
        if (empty($entityAttachmentKey)) {
            $this->generateAttachmentKey();
            $model->attachmentKey = $this->attachmentKey;
            $this->entity->setAttachmentKey($this->attachmentKey);
            $this->container->get('doctrine')->getEntityManager()->flush();
        }

        $model->attachmentKey = $entity->getAttachmentKey();
        $this->attachmentKey = $entity->getAttachmentKey();

        $model->novoDescription = $entity->getNovoDescription();
        $model->name = $entity->getName();
        $model->description = $entity->getDescription();
        $model->materialTypeId = $entity->getMaterialTypeId();
        $model->supplier = $entity->getSupplier();
        $model->price = $entity->getPrice();
        $model->unitsPerPrice = $entity->getUnitsPerPrice();
        $model->unitDescription = $entity->getUnitDescription();
        $model->articleNumber = $entity->getArticleNumber();
        $model->notes = $entity->getNotes();
        $model->expiryDate = $entity->getExpiryDate();
        $model->expiryDateWarningThreshold = $entity->getExpiryDateWarningThreshold();
        $model->warningThreshold = $entity->getWarningThreshold();
        $model->extraNotes = $entity->getExtraNotes();

        $model->fileAttachment = array();
        $model->fileAttachment['file'] = $entity->getFileAttachment();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'materials/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        $builder->add('sectionMaterial', 'section', array(
            'label'    => 'Material',
            'property_path' => false
        ));
        $builder->add('novoDescription', 'text', array(
            'label'    => 'Novo Description',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => false
        ));
        $builder->add('materialTypeId', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_MaterialType_List')
        ));
        $builder->add('supplier', 'text', array(
            'label'    => 'Supplier',
            'required' => false
        ));
        $builder->add('price', 'text', array(
            'label'    => 'Price',
            'required' => false
        ));
        $builder->add('unitsPerPrice', 'text', array(
            'label'    => 'Units Per Price',
            'required' => false
        ));
        $builder->add('unitDescription', 'text', array(
            'label'    => 'Unit Description',
            'required' => false
        ));
        $builder->add('articleNumber', 'text', array(
            'label'    => 'Article Number',
            'required' => false
        ));
        $builder->add('notes', 'textarea', array(
            'label'    => 'Notes',
            'required' => false
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
        $builder->add('expiryDateWarningThreshold', 'text', array(
            'label'    => 'Expiry date - warning threshold (days)',
            'required' => false
        ));
        $builder->add('warningThreshold', 'text', array(
            'label'    => 'Warning threshold',
            'required' => false
        ));
        $builder->add('extraNotes', 'textarea', array(
            'label'    => 'Extra Notes',
            'required' => false,
            'attr'     => array(
                'rows' => 5, 'cols' => 70
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->entity->setAttachmentKey($model->attachmentKey);

        $this->entity->setNovoDescription($model->novoDescription);
        $this->entity->setName($model->name);
        $this->entity->setDescription($model->description);
        $this->entity->setMaterialTypeId($model->materialTypeId);
        $this->entity->setSupplier($model->supplier);
        $this->entity->setPrice($model->price);
        $this->entity->setUnitsPerPrice($model->unitsPerPrice);
        $this->entity->setUnitDescription($model->unitDescription);
        $this->entity->setArticleNumber($model->articleNumber);
        $this->entity->setNotes($model->notes);
        $this->entity->setExpiryDate($model->expiryDate);
        $this->entity->setExpiryDateWarningThreshold($model->expiryDateWarningThreshold);
        $this->entity->setWarningThreshold($model->warningThreshold);
        $this->entity->setExtraNotes($model->extraNotes);

        parent::onSuccess();
        $this->reloadForm();
    }
}
