<?php
namespace Leep\AdminBundle\Business\Material;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateMaterialPurchaseHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateMaterialPurchaseModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('dateOrdered', 'datepicker', array(
            'label' => 'Date ordered',
            'required'  => false
        ));
        $builder->add('orderedReceived',     'checkbox', array(
            'label'  => 'Ordered received',
            'required' => false
        ));
        $builder->add('receivedDate', 'datepicker', array(
            'label' => 'Date received',
            'required'  => false
        ));
        $builder->add('quantity', 'text', array(
            'label'  => 'Quantity',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('orderBy',    'text', array(
            'label'  => 'Order By',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('lotNumber',    'text', array(
            'label'  => 'Novogenia Lot Number',
            'required' => false,
            'attr' => array(
                'class' => 'mediuminput'
            )
        ));
        $builder->add('expiryDate', 'datepicker', array(
            'label'    => 'Expiry date',
            'required' => false
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $idMaterial = $this->container->get('request')->get('idMaterial', 0);

        $materialPurchase = new Entity\MaterialsPurchase();
        $materialPurchase->setMaterialId($idMaterial);
        $materialPurchase->setDateOrdered($model->dateOrdered);
        $materialPurchase->setOrderedReceived($model->orderedReceived);
        $materialPurchase->setReceivedDate($model->receivedDate);
        $materialPurchase->setQuantity($model->quantity);
        $materialPurchase->setOrderBy($model->orderBy);
        $materialPurchase->setLotNumber($model->lotNumber);
        $materialPurchase->setExpiryDate($model->expiryDate);

        $em->persist($materialPurchase);
        $em->flush();

        parent::onSuccess();
    }
}
