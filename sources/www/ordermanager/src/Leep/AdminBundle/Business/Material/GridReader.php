<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public $warningThresholds = array();

    // PRE GET RESULT
    public $stock = array();
    public $waiting = array();

    public function getColumnMapping() {
        return array('novoDescription', 'name', 'materialTypeId', 'supplier', 'expiryDate', 'stock', 'waiting', 'action');
    }

    public function __construct($container) {
        parent::__construct($container);

        $results = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:MatType')->findAll();
        foreach ($results as $r) {
            $this->warningThresholds[$r->getId()] = intval($r->getWarningThreshold());
        }
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Novo Description', 'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Material',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Type',             'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Supplier',         'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Expiry date',      'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Stock',            'width' => '18%', 'sortable' => 'false'),
            array('title' => 'Waiting',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',           'width' => '7%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_MaterialGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:Materials', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');

        if (trim($this->filters->novoDescription) != '') {
            $queryBuilder->andWhere('p.novodescription LIKE :novoDescription')
                ->setParameter('novoDescription', '%'.trim($this->filters->novoDescription).'%');
        }
        if (trim($this->filters->name) != '') {
            $arrName = array_map('trim', explode("\n", $this->filters->name));
            $str = "p.name LIKE '%". $arrName[0]. "%'";
            for($i = 1; $i< count($arrName); $i++) {
                $str .= " or p.name LIKE '%". $arrName[$i]. "%'";
            }
            $queryBuilder->andWhere($str);

        }
        if ($this->filters->materialTypeId != 0) {
            $queryBuilder->andWhere('p.materialtypeid = :materialTypeId')
                ->setParameter('materialTypeId', $this->filters->materialTypeId);
        }
        if (trim($this->filters->supplier) != '') {
            $queryBuilder->andWhere('p.supplier LIKE :supplier')
                ->setParameter('supplier', '%'.trim($this->filters->supplier).'%');
        }
        if (!empty($this->filters->expireOnDate)) {
            $queryBuilder->andWhere('p.expiryDate <= :expireOnDate')
                ->setParameter('expireOnDate', $this->filters->expireOnDate);
        }
        if (!empty($this->filters->stockStatus) && $this->filters->stockStatus == 1) {
            $queryBuilder->andWhere('p.stockStatus = 1');
        }
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->orderBy('p.novodescription', 'ASC');
    }

    public function buildCellExpiryDate($row) {
        $expiryDate = $row->getExpiryDate();
        $html = "";

        if ($expiryDate) {
            $html = $expiryDate->format('d/m/Y');
        }

        // check Material Purchase
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('MIN(DATE_DIFF(p.expiryDate, CURRENT_DATE())) AS REMAINING_DAYS')
            ->from('AppDatabaseMainBundle:MaterialsPurchase', 'p')
            ->andWhere('p.materialid = :materialId')
            ->andWhere('DATE_DIFF(p.expiryDate, CURRENT_DATE()) <= :warningThreshold')
            ->andWhere('(p.expiryDateWarningDisabled = 0) OR (p.expiryDateWarningDisabled IS NULL)')
            ->setParameter('materialId', $row->getId())
            ->setParameter('warningThreshold', $row->getExpiryDateWarningThreshold());

        $expiredInDays = $query->getQuery()->getSingleScalarResult();

        if ($expiredInDays !== NULL) {
            if ($expiredInDays == 0) {
                $html = '<b style="color: red">Expired today</b>';
            }
            else if ($expiredInDays < 0) {
                $html = sprintf('<b style="color: red">Expired %s day(s) ago</b>', -$expiredInDays);
            }
            else {
                $html = sprintf('<b style="color: red">Expired in %s day(s)</b>', $expiredInDays);
            }
        }

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addPopupButton('View', $mgr->getUrl('leep_admin', 'material', 'view', 'view', array('id' => $row->getId())), 'button-detail');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('materialModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'material', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'material', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellStock($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $this->ensureStockWaiting($row->getId());
        $html = $this->stock[$row->getId()]['item'] .' / €'.number_format($this->stock[$row->getId()]['value'], 2);

        if ($row->getStockStatus() == 1) {
            $html = '<b style="color: red">'.$html."</b>";
        }

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('materialModify')) {
            $builder->addButton('Manage stock', $mgr->getUrl('leep_admin', 'material', 'edit_stock', 'edit', array('id' => $row->getId())), 'button-edit');
        }
        else if ($helperSecurity->hasPermission('materialAdd')) {
            $builder->addButton('Add material purchase', $mgr->getUrl('leep_admin', 'material', 'create_material_purchase', 'create', array('idMaterial' => $row->getId())), 'button-add');
            $builder->addButton('Add material used', $mgr->getUrl('leep_admin', 'material', 'create_material_used', 'create', array('idMaterial' => $row->getId())), 'button-submit');
        }

        return $html.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellWaiting($row) {
        $this->ensureStockWaiting($row->getId());
        $html = $this->waiting[$row->getId()]['item'] .' / €'.number_format($this->waiting[$row->getId()]['value'], 2);
        return $html;
    }

    private function ensureStockWaiting($materialId) {
        if (!isset($this->stock[$materialId])) {
            $this->stock[$materialId] = array(
                'item' => 0, 'value' => 0
            );
        }
        if (!isset($this->waiting[$materialId])) {
            $this->waiting[$materialId] = array(
                'item' => 0, 'value' => 0
            );
        }
    }

    public function preGetResult() {
        $this->stock = array();
        $this->waiting = array();

        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MaterialsPurchase')->findAll();
        foreach ($result as $purchase) {
            $materialId = $purchase->getMaterialId();
            $this->ensureStockWaiting($materialId);

            if ($purchase->getOrderedReceived() == 1) {
                $this->stock[$materialId]['item'] += $purchase->getQuantity();
            }
            else {
                $this->waiting[$materialId]['item'] += $purchase->getQuantity();
            }
        }

        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:MaterialsUsed')->findAll();
        foreach ($result as $used) {
            $materialId = $used->getMaterialId();
            $this->ensureStockWaiting($materialId);

            $this->stock[$materialId]['item'] -= $used->getQuantity();
        }

        // Compute price
        $materialPrices = array();
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials')->findAll();
        foreach ($result as $m) {
            $unit = floatval($m->getUnitsPerPrice());
            if (empty($unit)) {
                $unit = 1;
            }
            $materialPrices[$m->getId()] = floatval($m->getPrice()) / $unit;
        }

        foreach ($this->stock as $materialId => $v) {
            $price = isset($materialPrices[$materialId]) ? $materialPrices[$materialId] : 0;
            $this->stock[$materialId]['value'] = $price * $this->stock[$materialId]['item'];
        }

        foreach ($this->waiting as $materialId => $v) {
            $price = isset($materialPrices[$materialId]) ? $materialPrices[$materialId] : 0;
            $this->waiting[$materialId]['value'] = $price * $this->waiting[$materialId]['item'];
        }
    }
}
