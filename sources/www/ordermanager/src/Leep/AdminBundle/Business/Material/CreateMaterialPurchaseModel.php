<?php
namespace Leep\AdminBundle\Business\Material;

use Symfony\Component\Validator\ExecutionContext;

class CreateMaterialPurchaseModel {
    public $dateOrdered;
    public $orderedReceived;
    public $receivedDate;
    public $quantity;
    public $orderBy;
    public $lotNumber;
    public $expiryDate;
}
