<?php
namespace Leep\AdminBundle\Business\Material;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditStockHandler extends AppEditHandler {
    public $materialId = 0;
    public $attachmentKey = null;
    public function generateAttachmentKey() {
        $form = $this->container->get('request')->request->get('form', false);
        if ($form != false) {
            $this->attachmentKey = $form['attachmentKey'];
        }
        if (empty($this->attachmentKey)) {
            $success = false;
            while (!$success) {
                $this->attachmentKey = $this->container->get('leep_admin.helper.common')->generateRandAttachmentDir();
                $duplicate = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findOneByAttachmentKey($this->attachmentKey);
                if (!$duplicate) {
                    $success = true;
                }
            }
        }
    }


    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $this->materialId = $entity->getId();

        // Attachment key
        $entityAttachmentKey = $entity->getAttachmentKey();
        if (empty($entityAttachmentKey)) {
            $this->generateAttachmentKey();
            $entity->setAttachmentKey($this->attachmentKey);
            $em->flush();
        }
        $this->attachmentKey = $entity->getAttachmentKey();

        // PURCHASE
        $purchaseList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MaterialsPurchase', 'p')
            ->andWhere('p.materialid = :materialId')
            ->setParameter('materialId', $entity->getId())
            ->orderBy('p.dateordered', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $purchase) {
            $m = new Form\AppMaterialPurchaseModel();
            $m->dateOrdered = $purchase->getDateOrdered();
            $m->orderedReceived = $purchase->getOrderedReceived();
            $m->receivedBy = $purchase->getReceivedBy();
            $m->receivedDate = $purchase->getReceivedDate();
            $m->quantity = $purchase->getQuantity();
            $m->orderBy = $purchase->getOrderBy();
            $m->lotNumber = $purchase->getLotNumber();
            $m->externalLot = $purchase->getExternalLot();
            $m->expiryDate = $purchase->getExpiryDate();
            $m->expiryDateWarningDisabled = $purchase->getExpiryDateWarningDisabled();
            $m->info = $purchase->getInfo();
            $purchaseList[] = $m;
        }

        // USED
        $usedList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:MaterialsUsed', 'p')
            ->andWhere('p.materialid = :materialId')
            ->setParameter('materialId', $entity->getId())
            ->orderBy('p.datetaken', 'DESC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $used) {
            $m = new Form\AppMaterialUsedModel();
            $m->id = $used->getId();
            $m->isSelected = $used->getIsSelected();
            $m->quantity = $used->getQuantity();
            $m->dateTaken = $used->getDateTaken();
            $m->usedFor = $used->getUsedFor();
            $m->takenBy = $used->getTakenBy();
            $usedList[] = $m;
        }

        return array(
            'attachmentKey'   => $this->attachmentKey,
            'description'     => $entity->getNovoDescription(),
            'unitDescription' => $entity->getUnitDescription(),
            'purchaseList'    => $purchaseList,
            'usedList'        => $usedList
        );
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionAttachment', 'section', array(
            'label'    => 'Attachment',
            'property_path' => false
        ));
        $builder->add('attachmentKey', 'hidden');
        $builder->add('attachment', 'file_attachment', array(
            'label'    => 'Attachments',
            'required' => false,
            'key'      => 'materials/'.$this->attachmentKey,
            'snapshot' => false,
            'allowCloneDir' =>  true
        ));

        $builder->add('section1', 'section', array(
            'label' => 'Material',
            'property_path' => false
        ));
        $builder->add('description', 'label', array(
            'label' => 'Description',
            'required'      => false
        ));
        $builder->add('unitDescription', 'label', array(
            'label' => 'Unit Description',
            'required'      => false
        ));

        $builder->add('section2', 'section', array(
            'label' => 'Purchases',
            'property_path' => false
        ));
        $builder->add('purchaseList', 'collection', array(
            'type' => new Form\AppMaterialPurchase($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        $builder->add('section3', 'section', array(
            'label' => 'Stock used up',
            'property_path' => false
        ));
        $builder->add('usedList', 'collection', array(
            'type' => new Form\AppMaterialUsed($this->container),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'options' => array(
                'label' => 'Status record',
                'required' => false
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $dbHelper = $this->container->get('leep_admin.helper.database');
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->entity->setAttachmentKey($model['attachmentKey']);

        $filters = array('materialid' => $this->materialId);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MaterialsPurchase', $filters);
        $dbHelper->delete($em, 'AppDatabaseMainBundle:MaterialsUsed', $filters);

        foreach ($model['purchaseList'] as $purchaseRow) {
            if (empty($purchaseRow)) continue;
            $purchase = new Entity\MaterialsPurchase();
            $purchase->setMaterialId($this->materialId);
            $purchase->setDateOrdered($purchaseRow->dateOrdered);
            $purchase->setOrderedReceived($purchaseRow->orderedReceived);
            $purchase->setReceivedBy($purchaseRow->receivedBy);
            $purchase->setReceivedDate($purchaseRow->receivedDate);
            $purchase->setQuantity($purchaseRow->quantity);
            $purchase->setOrderBy($purchaseRow->orderBy);
            $purchase->setExternalLot($purchaseRow->externalLot);
            $purchase->setLotNumber($purchaseRow->lotNumber);
            $purchase->setExpiryDate($purchaseRow->expiryDate);
            $purchase->setExpiryDateWarningDisabled($purchaseRow->expiryDateWarningDisabled);
            $purchase->setInfo($purchaseRow->info);
            $em->persist($purchase);
        }

        foreach ($model['usedList'] as $usedRow) {
            if (empty($usedRow)) continue;
            $used = new Entity\MaterialsUsed();
            $used->setIsSelected($usedRow->isSelected);
            $used->setMaterialId($this->materialId);
            $used->setQuantity($usedRow->quantity);
            $used->setDateTaken($usedRow->dateTaken);
            $used->setUsedFor($usedRow->usedFor);
            $used->setTakenBy($usedRow->takenBy);
            $em->persist($used);
        }

        $em->flush();

        parent::onSuccess();

        $this->reloadForm();
    }
}
