<?php
namespace Leep\AdminBundle\Business\Material;

use Symfony\Component\Validator\ExecutionContext;

class CreateMaterialUsedModel {
    public $quantity;
    public $dateTaken;
    public $usedFor;
    public $takenBy;
}
