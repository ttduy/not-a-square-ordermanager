<?php 
namespace Leep\AdminBundle\Business\Material;

class Constant {
    const LB_MATERIAL_USED_UP_IN_USE_LIST = "Material Used Up - In Use";

    const MATERIAL_FILE_DIR = 'simple_materials_files';
}