<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('section1', 'section', array(
            'property_path' => false,
            'label'         => 'Collective Payment'
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => true
        ));
        $builder->add('paymentNumber', 'text', array(
            'label'    => 'Payment Number',
            'required' => true
        ));
        $builder->add('paymentDate', 'datepicker', array(
            'label'    => 'Payment Date',
            'required' => false
        ));
        $builder->add('paymentFromCompanyId', 'choice', array(
            'label'    => 'Payment From',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));
        $builder->add('isWithTax', 'checkbox', array(
            'label'    => 'Is With Tax',
            'required' => false
        ));
        $builder->add('destinationId', 'choice', array(
            'label'    => 'Destination',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CollectiveInvoice_Destination')
        ));
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('paymentStatus', 'choice', array(
            'label'    => 'Payment Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Invoice_Status')
        ));

        $builder->add('section2', 'section', array(
            'property_path' => false,
            'label'         => 'Payments'
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();

        $collectivePayment = new Entity\CollectivePayment();
        $collectivePayment->setName($model->name);
        $collectivePayment->setPaymentNumber($model->paymentNumber);
        $collectivePayment->setPaymentDate($model->paymentDate);
        $collectivePayment->setPaymentFromCompanyId($model->paymentFromCompanyId);
        $collectivePayment->setIsWithTax($model->isWithTax);
        $collectivePayment->setDestinationId($model->destinationId);
        $collectivePayment->setPartnerId($model->partnerId);
        $collectivePayment->setDistributionChannelId($model->distributionChannelId);
        $collectivePayment->setPaymentStatus($model->paymentStatus);

        $em->persist($collectivePayment);
        $em->flush();

        parent::onSuccess();
    }
}
