<?php 
namespace Leep\AdminBundle\Business\CollectivePayment;

class Constant {
    const DESTINATION_PARTNER = 1;
    const DESTINATION_DISTRIBUTION_CHANNEL = 2;

    public static function getDestination() {
        return array(
            self::DESTINATION_PARTNER     => 'Partner',
            self::DESTINATION_DISTRIBUTION_CHANNEL  => 'Distribution Channel'
        );
    }


    const PAYMENT_STATUS_TO_BE_DONE    = 1;
    const PAYMENT_STATUS_WAIT_WITH_PAYMENT = 2;
    const PAYMENT_STATUS_PAYMENT_SENT  = 3;
    const PAYMENT_STATUS_REMINDER_SENT = 4;
    const PAYMENT_STATUS_PAID          = 5; 
    const PAYMENT_STATUS_PAYMENT_CANCELLED = 6;
    public static function getPaymentStatus() {
        return array(
            self::PAYMENT_STATUS_TO_BE_DONE        => 'To be done',
            self::PAYMENT_STATUS_WAIT_WITH_PAYMENT => 'Wait with payment',
            self::PAYMENT_STATUS_PAYMENT_SENT      => 'Payment sent',
            self::PAYMENT_STATUS_REMINDER_SENT     => 'Reminders sent',
            self::PAYMENT_STATUS_PAID              => 'Paid',
            self::PAYMENT_STATUS_PAYMENT_CANCELLED => 'Payment cancelled'
        );
    }
    

    const ACTIVITY_TYPE_PAYMENT_CREATED = 1;
    const ACTIVITY_TYPE_PAYMENT_UPDATED = 2;
    const ACTIVITY_TYPE_EMAIL_SENT      = 3;
    const ACTIVITY_TYPE_OTHER           = 4;
    public static function getCollectivePaymentActivityTypes() {
        return array(
            self::ACTIVITY_TYPE_PAYMENT_CREATED     => 'Payment generated',
            //self::ACTIVITY_TYPE_INVOICE_UPDATED     => 'Invoice updated',
            //self::ACTIVITY_TYPE_EMAIL_SENT          => 'Email sent',
            self::ACTIVITY_TYPE_OTHER               => 'Other',
        );
    }
}