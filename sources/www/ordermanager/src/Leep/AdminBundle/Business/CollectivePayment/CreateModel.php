<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $paymentNumber;
    public $paymentDate;
    public $paymentFromCompanyId;
    public $isWithTax;
    public $destinationId;
    public $partnerId;
    public $distributionChannelId;
    public $paymentStatus;
}
