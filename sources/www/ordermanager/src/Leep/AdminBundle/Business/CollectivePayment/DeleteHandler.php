<?php 
namespace Leep\AdminBundle\Business\CollectivePayment;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\CollectiveInvoice\Constant;


class DeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $collectivePayment = $doctrine->getRepository('AppDatabaseMainBundle:CollectivePayment')->findOneById($id);
        $collectivePayment->setIsDeleted(1);

        if ($collectivePayment->getDestinationId() == Constant::DESTINATION_PARTNER) {
            $orders = $em->getRepository('AppDatabaseMainBundle:Customer')->findBy(array(
                'idCollectivePaymentPartner' => $id
            ));
            foreach ($orders as $order) {
                $order->setIdCollectivePaymentPartner(NULL);
            }
        }
        else {
            $orders = $em->getRepository('AppDatabaseMainBundle:Customer')->findBy(array(
                'idCollectivePaymentDc'      => $id
            )); 
            foreach ($orders as $order) {
                $order->setIdCollectivePaymentDc(NULL);
            }
        }        
        $em->flush();
    }
}