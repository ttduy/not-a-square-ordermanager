<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('paymentNumber', 'text', array(
            'label'    => 'Payment Number',
            'required' => false
        ));
        $builder->add('paymentDate', 'datepicker', array(
            'label'    => 'Payment Date',
            'required' => false
        ));
        $builder->add('paymentFromCompanyId', 'choice', array(
            'label'    => 'Payment From',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Company_List')
        ));                
        $builder->add('partnerId', 'searchable_box', array(
            'label'    => 'Partner',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_Partner_List')
        ));
        $builder->add('distributionChannelId', 'searchable_box', array(
            'label'    => 'Distribution Channel',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMappingFiltered('LeepAdmin_DistributionChannel_List')
        ));
        $builder->add('paymentStatus', 'choice', array(
            'label'    => 'Payment Status',
            'required' => false,
            'empty_value' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_CollectivePayment_PaymentStatus')
        ));

        return $builder;
    }
}