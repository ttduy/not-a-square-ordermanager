<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

use Leep\AdminBundle\Business;
use Symfony\Component\HttpFoundation\Response;

class Utils {
    private static $partnerRules = array(
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARNER          => 'payment_partner',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_DC              => 'payment_no_margin',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC  => 'payment_partner',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_PARTNER => 'payment_both',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_DC      => 'payment_no_margin',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_NO_MARGIN       => 'payment_no_margin',
    );
    private static $dcRules = array(
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARNER          => 'payment_no_margin',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_DC              => 'payment_dc',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_PARTNER_AND_DC  => 'payment_dc',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_PARTNER => 'payment_no_margin',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_BOTH_TO_DC      => 'payment_both',
        Business\DistributionChannel\Constant::MARGIN_GOES_TO_NO_MARGIN       => 'payment_no_margin',
    );

    public static function getPaymentApplied($idMarginGoesTo, $type) {
        if ($type == 'partner') {
            return isset(Utils::$partnerRules[$idMarginGoesTo]) ? Utils::$partnerRules[$idMarginGoesTo] : '';
        }
        else if ($type == 'dc') {
            return isset(Utils::$dcRules[$idMarginGoesTo]) ? Utils::$dcRules[$idMarginGoesTo] : '';
        }
        return '';
    }

    public static function buildPdf($controller, $idCollectivePayment) {   
        $mapping = $controller->get('easy_mapping');
        $formatter = $controller->get('leep_admin.helper.formatter');
        $collectivePayment = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:CollectivePayment')->findOneById($idCollectivePayment);
        $company = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Companies')->findOneById($collectivePayment->getPaymentFromCompanyId());
        if (!$company) {
            return new Response("Please select company for this payment");
        }
        $collectivePaymentType = '';
        if ($collectivePayment->getPartnerId() != 0) {
            $client = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Partner')->findOneById($collectivePayment->getPartnerId());
            $collectivePaymentType = 'partner';
        }
        else {
            $client = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($collectivePayment->getDistributionChannelId());
            $collectivePaymentType = 'dc';
        }

        $logo = $formatter->container->getParameter('kernel.root_dir').'/../logo.png';
        if ($company->getLogo()) {
            $logo = $formatter->container->getParameter('kernel.root_dir').'/../web/attachments/logo/'.$company->getLogo();
        }
        
        $data = array();
        $data['logoImagePath'] = $logo;
        $data['company'] = $company;
        $data['companyName'] = $company->getCompanyName();
        $data['companyStreet'] = $company->getStreet();
        $data['companyPostCode'] = $company->getPostCode();
        $data['companyCity'] = $company->getCity();
        $data['companyEmail'] = $company->getEmail();
        $data['companyTelephone'] = $company->getTelephone();
        $data['companyWebSite'] = $company->getWebSite();

        $data['client'] = $client;
        $data['clientName'] = $client->getFirstName().' '.$client->getSurName();        
        $data['clientCompanyName'] = $client->getInstitution();

        if ($client->getInvoiceAddressIsUsed()) {
            $data['clientStreet'] = $client->getInvoiceAddressStreet();
            $data['clientStreet2'] = '';
            $data['clientCity'] = $client->getInvoiceAddressCity();
            $data['clientPostCode'] = $client->getInvoiceAddressPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getInvoiceAddressIdCountry());

            $clientName = $client->getInvoiceClientName();
            $companyName = $client->getInvoiceCompanyName();
            if (trim($clientName) != '') {
                $data['clientName'] = $clientName;
                if ($clientName == '######') {
                    $data['clientName'] = '';
                }
            }
            if (trim($companyName) != '') {
                $data['clientCompanyName'] = $companyName;
                if ($companyName == '######') {
                    $data['clientCompanyName'] = '';
                }
            }
        }
        else {
            $data['clientStreet'] = $client->getStreet();
            $data['clientStreet2'] = method_exists($client, 'getStreet2') ? $client->getStreet2() : '';
            $data['clientCity'] = $client->getCity();
            $data['clientPostCode'] = $client->getPostCode();
            $data['clientCountry'] = $mapping->getMappingTitle('LeepAdmin_Country_List', $client->getCountryId());
        }

        $data['clientUidNumber'] = $client->getUIDNumber();

        $data['collectivePayment'] = $collectivePayment;

        $today = new \DateTime();
        $data['paymentDate'] = $formatter->format($collectivePayment->getPaymentDate(), 'date');
        $data['paymentNumber'] = $collectivePayment->getPaymentNumber();

        $order = array();
        $subTotal = 0;
        $criteria = array('id' => -1);
        $percentage = 100;
        if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_PARTNER) {
            $criteria = array('idCollectivePaymentPartner' => $collectivePayment->getId());
        }
        else if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
            $criteria = array('idCollectivePaymentDc' => $collectivePayment->getId());
        }
        $result = $controller->getDoctrine()->getRepository('AppDatabaseMainBundle:Customer')->findBy($criteria);
        foreach ($result as $customer) {
            $percentage = 100;
            if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_PARTNER) {
                if (is_numeric($customer->getMarginOverridePartner())) {
                    $percentage = floatval($customer->getMarginOverridePartner());
                }
            }
            else if ($collectivePayment->getDestinationId() == Business\CollectivePayment\Constant::DESTINATION_DISTRIBUTION_CHANNEL) {
                if (is_numeric($customer->getMarginOverrideDc())) {
                    $percentage = floatval($customer->getMarginOverrideDc());
                }
            }

            $applied = Utils::getPaymentApplied($customer->getIdMarginGoesTo(), $collectivePaymentType);
            $price = Business\Customer\Util::computePrice($controller, $customer->getId(), $applied);
            foreach ($price['items']['categories'] as $idCategory => $v) {
                $v = $v * $percentage / 100;
                $v = round($v, 2);

                $order[] = array(
                    'orderNumber' => $customer->getOrderNumber(),
                    'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                    'product'     => $mapping->getMappingTitle('LeepAdmin_Category_List', $idCategory),
                    'firstName'   => $customer->getFirstName(),
                    'lastName'    => $customer->getSurName(),
                    'sum'         => $formatter->format($v, 'money')
                );    
                $subTotal += $v;
            }
            foreach ($price['items']['products'] as $idProduct => $v) {
                $v = $v * $percentage / 100;
                $v = round($v, 2);
                
                $row = array(
                    'orderNumber' => $customer->getOrderNumber(),
                    'date'        => $formatter->format($customer->getDateOrdered(), 'date'),
                    'product'     => $mapping->getMappingTitle('LeepAdmin_Product_List', $idProduct),
                    'firstName'   => $customer->getFirstName(),
                    'lastName'    => $customer->getSurName(),
                    'sum'         => $formatter->format($v, 'money')
                );    
                $subTotal += $v;

                if (isset($price['specialProducts'][$idProduct])) {
                    $row['product'] = $mapping->getMappingTitle('LeepAdmin_SpecialProduct_List', $price['specialProducts'][$idProduct]);
                }                
                $order[] = $row;
            }     
        }        
        $data['order'] = $order;
        $data['subtotal'] = $formatter->format($subTotal, 'money');
        $discount = 0;
        $overrideAmount = floatval($collectivePayment->getOverrideAmount());
        if ($overrideAmount != 0) {
            $discount = $subTotal - $overrideAmount;
        }
        $total = $subTotal - $discount;
        $data['discount'] = $formatter->format($discount, 'money');
        $data['total']    = $formatter->format($total, 'money');

        $taxAmount = $total * floatval($collectivePayment->getTaxValue()) / 100;
        $data['taxAmount'] = $formatter->format($taxAmount, 'money');
        $data['totalWithTax'] = $formatter->format($total + $taxAmount, 'money');
        
        $html = $controller->get('templating')->render('LeepAdminBundle:CollectivePaymentForm:pdf.html.twig', $data);

        $dompdf = $controller->get('slik_dompdf');
        $dompdf->getpdf($html);

        // Save to file
        $filePath = $controller->get('service_container')->getParameter('files_dir').'/payments';
        $fileName = 'collective_payment_'.$collectivePayment->getId().'.pdf';
        $outputFile = $filePath.'/'.$fileName;
        file_put_contents($outputFile, $dompdf->output());

        $collectivePayment->setPaymentFile($fileName);
        $em = $controller->get('doctrine')->getEntityManager();
        $em->persist($collectivePayment);
        $em->flush();

        return TRUE;
    }
}
