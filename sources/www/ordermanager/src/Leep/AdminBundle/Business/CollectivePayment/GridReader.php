<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('name', 'paymentNumber', 'paymentDate', 'paymentFromCompanyId', 'partnerId', 'distributionChannelId', 'paymentStatus', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Name',     'width' => '15%', 'sortable' => 'true'),
            array('title' => 'Number',   'width' => '9%',  'sortable' => 'true'),
            array('title' => 'Date',     'width' => '9%',  'sortable' => 'true'),
            array('title' => 'From',     'width' => '9%',  'sortable' => 'true'),
            array('title' => 'Partner',  'width' => '9%',  'sortable' => 'true'),
            array('title' => 'Distribution Channel', 'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Status',   'width' => '9%',  'sortable' => 'true'),
            array('title' => 'Action',   'width' => '15%', 'sortable' => 'false')
            
        );
    }

    public function getColumnSortMapping() {
        $this->columnSortMapping = array();
        $this->columnSortMapping[] = 'p.name';
        $this->columnSortMapping[] = 'p.paymentnumber';
        $this->columnSortMapping[] = 'p.paymentdate';
        $this->columnSortMapping[] = 'p.paymentfromcompanyid';
        $this->columnSortMapping[] = 'p.partnerid';
        $this->columnSortMapping[] = 'p.distributionchannelid';
        $this->columnSortMapping[] = 'p.paymentstatus';

        return $this->columnSortMapping;
    }
    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CollectivePaymentGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:CollectivePayment', 'p');
        $queryBuilder->andWhere('p.isdeleted = 0');
        
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
        if (trim($this->filters->paymentNumber) != '') {
            $queryBuilder->andWhere('p.paymentnumber LIKE :paymentNumber')
                ->setParameter('paymentNumber', '%'.trim($this->filters->paymentNumber).'%');
        }
        if ($this->filters->paymentDate != null) {
            $queryBuilder->andWhere('p.paymentdate = :paymentDate')
                ->setParameter('paymentDate', $this->filters->paymentDate);
        }
        if ($this->filters->paymentFromCompanyId != 0) {
            $queryBuilder->andWhere('p.paymentfromcompanyid = :paymentFromCompanyId')
                ->setParameter('paymentFromCompanyId', $this->filters->paymentFromCompanyId);
        }
        if ($this->filters->partnerId != 0) {
            $queryBuilder->andWhere('p.partnerid = :partnerId')
                ->setParameter('partnerId', $this->filters->partnerId);
        }
        if ($this->filters->distributionChannelId != 0) {
            $queryBuilder->andWhere('p.distributionchannelid = :distributionChannelId')
                ->setParameter('distributionChannelId', $this->filters->distributionChannelId);
        }
        if ($this->filters->paymentStatus != 0) {
            $queryBuilder->andWhere('p.paymentstatus = :paymentStatus')
                ->setParameter('paymentStatus', $this->filters->paymentStatus);
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $builder->addNewTabButton('Pdf',  $mgr->getUrl('leep_admin', 'collective_payment', 'payment_form_pdf',  'pdf',  array('id' => $row->getId())), 'button-pdf');
        // $builder->addPopupButton('Email', $mgr->getUrl('leep_admin', 'collective_payment', 'email',  'create', array('id' => $row->getId())), 'button-email');
        $builder->addPopupButton('Activity', $mgr->getUrl('leep_admin', 'collective_payment', 'edit_activity',  'edit', array('id' => $row->getId())), 'button-activity');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('collectivePaymentModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'collective_payment', 'payment_form_edit', 'list', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'collective_payment', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }
        
        return $builder->getHtml();
    }
}
