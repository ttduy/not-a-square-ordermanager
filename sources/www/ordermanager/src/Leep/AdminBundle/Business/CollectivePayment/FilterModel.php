<?php
namespace Leep\AdminBundle\Business\CollectivePayment;

class FilterModel {
    public $name;
    public $paymentNumber;
    public $paymentDate;
    public $paymentFromCompanyId;
    public $partnerId;
    public $distributionChannelId;
    public $paymentStatus;
}