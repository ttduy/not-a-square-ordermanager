<?php
namespace Leep\AdminBundle\Business\BodShipment;

class EmailModel {
    public $emailTemplateId;
    public $emailList;
    public $subject;
    public $sendTo;
    public $cc;
    public $content;
}