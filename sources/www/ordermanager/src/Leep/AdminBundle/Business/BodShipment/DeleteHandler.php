<?php 
namespace Leep\AdminBundle\Business\BodShipment;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteHandler extends AppDeleteHandler {
    public $repository;
    public function __construct($container, $repository) {
        parent::__construct($container);
        $this->repository = $repository;
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('idBodShipment', 0);
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $bodShipment = $doctrine->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($id);
        $em->remove($bodShipment);
        $em->flush();

        $mgr = $this->container->get('easy_module.manager');
        $url = $mgr->getUrl('leep_admin', 'bod_shipment', 'grid', 'list', array(
            'id' => $this->container->get('request')->query->get('id', 0)
        ));

        return new RedirectResponse($url);
    }
}