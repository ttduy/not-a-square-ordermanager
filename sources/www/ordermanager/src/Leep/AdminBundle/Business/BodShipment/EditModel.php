<?php
namespace Leep\AdminBundle\Business\BodShipment;

class EditModel {
    public $idBodShipment;
    public $creationDatetime;
    public $customerReports;

    public $bodCustomerOrderNumber;
    public $bodFromCompany;
    public $bodFromPerson;
    public $bodFromEmail;

    public $bodContributorRole;
    public $bodContributorName;

    public $bodItemTitle;
    public $bodItemPaper;
    public $bodItemBinding;
    public $bodItemBack;
    public $bodItemFinish;
    public $bodItemJacket;

    public $bodOrderNumber;
    public $bodOrderCopies;
    public $bodOrderAddressLine1;
    public $bodOrderAddressLine2;
    public $bodOrderAddressLine3;
    public $bodOrderStreet;
    public $bodOrderZip;
    public $bodOrderCity;
    public $bodOrderCountry;
    public $bodOrderCustomsValue;
    public $bodOrderShipping;

    public $bodDeliveryNoteIdReport;
    public $bodDeliveryNoteIdLanguage;

    public $isUseNewQueue;
    public $customBodAccountNumber;
    public $customBodBarcodeNumber;
}