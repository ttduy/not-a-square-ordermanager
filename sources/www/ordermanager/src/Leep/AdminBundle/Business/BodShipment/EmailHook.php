<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class EmailHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $id   = $controller->getRequest()->query->get('id', 0);
        $formatter = $controller->get('leep_admin.helper.formatter');
        $mapping = $controller->get('easy_mapping');
        $config = $controller->get('leep_admin.helper.common')->getConfig();
        $mgr = $controller->get('easy_module.manager');

        $em = $controller->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();

        $query->select('a')
            ->from('AppDatabaseMainBundle:CustomerActivity', 'a')
            ->andWhere('a.customerid = :customerId')
            ->setParameter('customerId', $id);

        // DISTRIBUTION EMAIL TEMPLATE
        $data['preSelectTemplateId'] = $config->getIdDefaultShipmentEmail();


        // ACCESS KEY
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);
        $data['accessKey'] = $customer->getShipmentKey();
        $data['accessCode'] = $customer->getShipmentCode();
        $data['generateAccessKeyLink'] = $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'generateAccessKey', array('id' => $id));
        $data['disableAccessKeyLink'] = $mgr->getUrl('leep_admin', 'customer_report', 'feature', 'disableAccessKey', array('id' => $id));

        // ACTIVITIES
        $query
            ->andWhere('a.activitytypeid = :activityTypeEmailId')
            ->setParameter('activityTypeEmailId', Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SHIPMENT_SENT)
            ->orderBy('a.sortorder', 'ASC');
        $result = $query->getQuery()->getResult();
            
        $activities = array();
        foreach ($result as $row) {
            $activities[] = array(
                'time' => $formatter->format($row->getActivityDate(), 'date').' '.$mapping->getMappingTitle('LeepAdmin_Time_List', $row->getActivityTimeId()),
                'notes' => $row->getNotes()
            );
        }
        $data['activities'] = $activities;
    }
}
