<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    protected $idCustomer = 0;

    public function loadEntity($request) {
        $id = $request->query->get('idBodShipment');
        if (empty($id)) {
            $form = $request->request->get('form');
            $id = $form['idBodShipment'];
        }
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:BodShipment', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $this->idCustomer = $entity->getIdCustomer();

        $model = new EditModel();
        $model->idBodShipment = $entity->getId();
        $model->creationDatetime = $entity->getCreationDatetime();
        $model->bodCustomerOrderNumber = $entity->getBodCustomerOrderNumber();
        $model->bodFromCompany = $entity->getBodFromCompany();
        $model->bodFromPerson = $entity->getBodFromPerson();
        $model->bodFromEmail = $entity->getBodFromEmail();

        $model->bodContributorRole = $entity->getBodContributorRole();
        $model->bodContributorName = $entity->getBodContributorName();

        $model->bodItemTitle = $entity->getBodItemTitle();
        $model->bodItemPaper = $entity->getBodItemPaper();
        $model->bodItemBinding = $entity->getBodItemBinding();
        $model->bodItemBack = $entity->getBodItemBack();
        $model->bodItemFinish = $entity->getBodItemFinish();
        $model->bodItemJacket = $entity->getBodItemJacket();

        $model->bodOrderNumber = $entity->getBodOrderNumber();
        $model->bodOrderCopies = $entity->getBodOrderCopies();
        $model->bodOrderAddressLine1 = $entity->getBodOrderAddressLine1();
        $model->bodOrderAddressLine2 = $entity->getBodOrderAddressLine2();
        $model->bodOrderAddressLine3 = $entity->getBodOrderAddressLine3();
        $model->bodOrderStreet = $entity->getBodOrderStreet();
        $model->bodOrderZip = $entity->getBodOrderZip();
        $model->bodOrderCity = $entity->getBodOrderCity();
        $model->bodOrderCountry = $entity->getBodOrderCountry();
        $model->bodOrderCustomsValue = $entity->getBodOrderCustomsValue();
        $model->bodOrderShipping = $entity->getBodOrderShipping();

        $model->bodDeliveryNoteIdReport = $entity->getBodDeliveryNoteIdReport();
        $model->bodDeliveryNoteIdLanguage = $entity->getBodDeliveryNoteIdLanguage();
        $model->customBodAccountNumber = $entity->getCustomBodAccountNumber();
        $model->customBodBarcodeNumber = $entity->getCustomBodBarcodeNumber();

        // Books
        $model->customerReports = array();
        $books = $em->getRepository('AppDatabaseMainBundle:BodShipmentBook')->findByIdBodShipment($entity->getId());
        foreach ($books as $book) {
            $model->customerReports[] = $book->getIdReport();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('isUseNewQueue', 'checkbox', array(
            'label'    => 'Is use new queue?',
            'required' => false
        ));
        $builder->add('idBodShipment', 'hidden');
        $builder->add('sectionShipmentInfo', 'section', array(
            'label'    => 'Shipment info',
            'property_path' => false
        ));
        $builder->add('creationDatetime',  'datetimepicker', array(
            'label'    => 'Creation datetime',
            'required' => false
        ));
        $builder->add('customerReports', 'choice', array(
            'label'    => 'Report',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => Utils::getApplicableReports($this->container, $this->idCustomer)
        ));
        $builder->add('bodCustomerOrderNumber', 'text', array(
            'label'    => 'Customer order number',
            'required' => false
        ));
        $builder->add('bodFromCompany', 'text', array(
            'label'    => 'From company',
            'required' => false
        ));
        $builder->add('bodFromPerson', 'text', array(
            'label'    => 'From person',
            'required' => false
        ));
        $builder->add('bodFromEmail', 'text', array(
            'label'    => 'From email',
            'required' => false
        ));
        $builder->add('customBodAccountNumber', 'text', array(
            'label'    => 'Custom BOD Account number',
            'required' => false
        ));
        $builder->add('customBodBarcodeNumber', 'text', array(
            'label'    => 'Custom BOD Barcode number (2 digit)',
            'required' => false
        ));
        $builder->add('sectionContributor', 'section', array(
            'label'    => 'Contributor',
            'property_path' => false
        ));
        $builder->add('bodContributorRole', 'text', array(
            'label'    => 'Role',
            'required' => false
        ));
        $builder->add('bodContributorName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        $builder->add('sectionItem', 'section', array(
            'label'    => 'Item',
            'property_path' => false
        ));
        $builder->add('bodItemTitle', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('bodItemPaper', 'text', array(
            'label'    => 'Paper',
            'required' => false
        ));
        $builder->add('bodItemBinding', 'text', array(
            'label'    => 'Binding',
            'required' => false
        ));
        $builder->add('bodItemBack', 'text', array(
            'label'    => 'Back',
            'required' => false
        ));
        $builder->add('bodItemFinish', 'text', array(
            'label'    => 'Finish',
            'required' => false
        ));
        $builder->add('bodItemJacket', 'text', array(
            'label'    => 'Jacket',
            'required' => false
        ));

        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order',
            'property_path' => false
        ));
        $builder->add('bodOrderNumber', 'text', array(
            'label'    => 'Order number',
            'required' => false
        ));
        $builder->add('bodOrderCopies', 'text', array(
            'label'    => 'No. copies',
            'required' => false
        ));
        $builder->add('bodOrderAddressLine1', 'text', array(
            'label'    => 'Address line 1',
            'required' => false
        ));
        $builder->add('bodOrderAddressLine2', 'text', array(
            'label'    => 'Address line 2',
            'required' => false
        ));
        $builder->add('bodOrderAddressLine3', 'text', array(
            'label'    => 'Address line 3',
            'required' => false
        ));
        $builder->add('bodOrderStreet', 'text', array(
            'label'    => 'Street',
            'required' => false
        ));
        $builder->add('bodOrderZip', 'text', array(
            'label'    => 'ZIP',
            'required' => false
        ));
        $builder->add('bodOrderCity', 'text', array(
            'label'    => 'City',
            'required' => false
        ));
        $builder->add('bodOrderCountry', 'text', array(
            'label'    => 'Country',
            'required' => false
        ));
        $builder->add('bodOrderCustomsValue', 'text', array(
            'label'    => 'Customs value',
            'required' => false
        ));
        $builder->add('bodOrderShipping', 'text', array(
            'label'    => 'Shipping',
            'required' => false
        ));

        $builder->add('sectionDeliveryNote', 'section', array(
            'label'    => 'Delivery note',
            'property_path' => false
        ));
        $builder->add('bodDeliveryNoteIdReport', 'choice', array(
            'label'    => 'Delivery note report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('bodDeliveryNoteIdLanguage', 'choice', array(
            'label'    => 'Delivery note report language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));


        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $model = $this->getForm()->getData();
        $customerReports = Utils::getApplicableReports($this->container, $this->idCustomer);
        $bodAccountNumber = trim($model->customBodAccountNumber);
        $bodBarcodeNumber= trim($model->customBodBarcodeNumber);

        // Validation
        if (!empty($bodAccountNumber) && strlen($bodBarcodeNumber) != 2) {
            $this->errors[] = "Bod barcode number must have two digit";
        }
        if (strlen($model->bodOrderAddressLine1) > 35) {
            $this->errors[] = "Address line 1 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderAddressLine2) > 35) {
            $this->errors[] = "Address line 2 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderAddressLine3) > 35) {
            $this->errors[] = "Address line 3 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderStreet) > 30) {
            $this->errors[] = "Street is too long, max 30 characters";
        }
        if (strlen($model->bodOrderZip) > 10) {
            $this->errors[] = "Zip is too long, max 10 characters";
        }
        if (strlen($model->bodOrderCity) > 30) {
            $this->errors[] = "City is too long, max 30 characters";
        }
        if (!empty($this->errors)) {
            $this->messages = array();
            return false;
        }

        $this->entity->setCreationDatetime($model->creationDatetime);
        $this->entity->setBodCustomerOrderNumber($model->bodCustomerOrderNumber);
        $this->entity->setBodFromCompany($model->bodFromCompany);
        $this->entity->setBodFromPerson($model->bodFromPerson);
        $this->entity->setBodFromEmail($model->bodFromEmail);

        $this->entity->setBodContributorRole($model->bodContributorRole);
        $this->entity->setBodContributorName($model->bodContributorName);

        $this->entity->setBodItemTitle($model->bodItemTitle);
        $this->entity->setBodItemPaper($model->bodItemPaper);
        $this->entity->setBodItemBinding($model->bodItemBinding);
        $this->entity->setBodItemBack($model->bodItemBack);
        $this->entity->setBodItemFinish($model->bodItemFinish);
        $this->entity->setBodItemJacket($model->bodItemJacket);

        $this->entity->setBodOrderNumber($model->bodOrderNumber);
        $this->entity->setBodOrderCopies($model->bodOrderCopies);
        $this->entity->setBodOrderAddressLine1($model->bodOrderAddressLine1);
        $this->entity->setBodOrderAddressLine2($model->bodOrderAddressLine2);
        $this->entity->setBodOrderAddressLine3($model->bodOrderAddressLine3);
        $this->entity->setBodOrderStreet($model->bodOrderStreet);
        $this->entity->setBodOrderZip($model->bodOrderZip);
        $this->entity->setBodOrderCountry($model->bodOrderCountry);
        $this->entity->setBodOrderCustomsValue($model->bodOrderCustomsValue);
        $this->entity->setBodOrderShipping($model->bodOrderShipping);

        $this->entity->setBodIdDeliveryNoteReport($model->bodDeliveryNoteIdReport);
        $this->entity->setBodDeliveryNoteIdReport($model->bodDeliveryNoteIdReport);
        $this->entity->setBodDeliveryNoteIdLanguage($model->bodDeliveryNoteIdLanguage);
        $this->entity->setCustomBodAccountNumber($bodAccountNumber);
        $this->entity->setCustomBodBarcodeNumber($bodBarcodeNumber);

        if ($model->isUseNewQueue) {
            $this->entity->setStatus(Constant::BOD_SHIPMENT_STATUS_PENDING_IN_QUEUE);
        } else {
            $this->entity->setStatus(Constant::BOD_SHIPMENT_STATUS_OPEN);
        }

        $this->entity->setPackageFile('');

        // Book
        $filters = array('idBodShipment' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:BodShipmentBook', $filters);
        $customerReportNames = array();
        foreach ($model->customerReports as $idReport) {
            $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($idReport);

            $bodShipmentBook = new Entity\BodShipmentBook();
            $bodShipmentBook->setIdBodShipment($this->entity->getId());
            $bodShipmentBook->setIdReport($idReport);
            $bodShipmentBook->setReportFile($customerReport->getFilename());
            $em->persist($bodShipmentBook);

            $customerReportNames[] = isset($customerReports[$idReport]) ? $customerReports[$idReport] : '--- ERROR ---';
        }
        $this->entity->setCustomerReportName(implode("\n", $customerReportNames));
        $em->flush();

        parent::onSuccess();
    }
}