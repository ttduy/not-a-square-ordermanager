<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Leep\AdminBundle\Business;

class Constant {
    const BOD_SHIPMENT_STATUS_OPEN =                0;
    const BOD_SHIPMENT_STATUS_PROCESSING =          1;
    const BOD_SHIPMENT_STATUS_FINISHED =            2;
    const BOD_SHIPMENT_STATUS_UPLOADING =           3;
    const BOD_SHIPMENT_STATUS_UPLOAD_ERROR =        4;
    const BOD_SHIPMENT_STATUS_PENDING_IN_QUEUE =    5;
    const BOD_SHIPMENT_STATUS_GENERATE_ERROR =      6;
    const BOD_SHIPMENT_STATUS_GENERATE_FINISHED =   7;

    public static function getBodShipmentStatus() {
        return array(
            self::BOD_SHIPMENT_STATUS_OPEN =>               'Open',
            self::BOD_SHIPMENT_STATUS_PENDING_IN_QUEUE  =>  'Pending in queue',
            self::BOD_SHIPMENT_STATUS_PROCESSING =>         'Processing',
            self::BOD_SHIPMENT_STATUS_GENERATE_FINISHED =>  'Generate finished',
            self::BOD_SHIPMENT_STATUS_UPLOADING =>          'Uploading',
            self::BOD_SHIPMENT_STATUS_UPLOAD_ERROR =>       'Upload error',
            self::BOD_SHIPMENT_STATUS_GENERATE_ERROR =>     'Generate report has error',
            self::BOD_SHIPMENT_STATUS_FINISHED =>           'Finished',
        );
    }
}