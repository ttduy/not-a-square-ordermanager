<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EmailHandler extends BaseCreateHandler {
    public $customer;
    public function getDefaultFormModel() {
        $id = $this->container->get('request')->query->get('id', 0);
        $this->customer = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:Customer')->findOneById($id);

        $model = new EmailModel();     
        $model->customerId = $id;
        return $model;
    }

    public function getEmailList() {
        $emailList = array();
        $emailList[$this->customer->getEmail()] = 'Customer ('.$this->customer->getEmail().') <br/>';
        
        $doctrine = $this->container->get('doctrine');
        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($this->customer->getDistributionChannelId());
        if ($dc) {
            if ($dc->getReportDeliveryEmail() != '') {
                $emailList[$dc->getReportDeliveryEmail()] = 'Distribution Channel ('.$dc->getReportDeliveryEmail().') <br/>';
            }
            else {
                $emailList['(missing email)'] = 'Distribution Channel (missing email) <br/>';
            }
            $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
            if ($partner && $partner->getStatusDeliveryEmail() != '') {
                $emailList[$partner->getStatusDeliveryEmail()] = 'Partner ('.$partner->getStatusDeliveryEmail().') <br/>';
            }
        }
        return $emailList;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionTemplate', 'section', array(
            'label'    => 'Template',
            'property_path' => false
        ));
        $builder->add('customerId', 'hidden');
        $builder->add('emailTemplateId', 'searchable_box', array(
            'label'    => 'Email Template',
            'required' => false,
            'choices'  => $mapping->getMappingFiltered('LeepAdmin_EmailTemplate_List'),
            'attr'     => array(
                'onChange' => 'javascript:onChangeEmailTemplateId()'
            )
        ));
        $builder->add('emailList',   'choice', array(
            'label'    => 'Emails',
            'required' => false,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices'  => $this->getEmailList()
        ));
        $builder->add('sectionContent', 'section', array(
            'label'    => 'Email Content',
            'property_path' => false
        ));
        $builder->add('subject', 'text', array(
            'label'    => 'Subject',
            'required' => true
        ));
        $builder->add('sendTo', 'text', array(
            'label'    => 'Send To',
            'required' => true
        ));
        $builder->add('cc', 'text', array(
            'label'    => 'CC',
            'required' => false
        ));
        $builder->add('content', 'textarea', array(
            'label'    => 'Content',
            'required' => false,
            'attr'   => array(
                'rows'  => 15, 'cols' => 80
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $model = $this->getForm()->getData();
        $helperCommon = $this->container->get('leep_admin.helper.common');
        $helperActivity = $this->container->get('leep_admin.helper.activity');

        $isSuccess = false;
        try {
            $senderEmail = $this->container->getParameter('sender_email');
            $senderName  = $this->container->getParameter('sender_name');
            $message = \Swift_Message::newInstance()
                ->setSubject($model->subject)
                ->setFrom($senderEmail, $senderName)
                ->setTo($helperCommon->parseEmaiLList($model->sendTo))
                ->setCc($helperCommon->parseEmaiLList($model->cc))
                ->setBody($model->content, 'text/plain');
            $this->container->get('mailer')->send($message);
            $isSuccess = true;

            parent::onSuccess();
            $this->messages = array();
            $this->messages[] = 'The email has been sent';
            
            $notes = 'Send email "'.$model->subject.'" to '.$model->sendTo;
            if (!empty($model->cc)) { $notes.= ' (cc: '.$model->cc.') '; }
            $helperActivity->addCustomerActivity($this->customer->getId(), Business\Customer\Constant::ACTIVITY_TYPE_EMAIL_SHIPMENT_SENT, $notes);
        }
        catch (\Exception $e) {
            $isSuccess = false;
            $this->errors[] = $e->getMessage();
        }     

    }
}
