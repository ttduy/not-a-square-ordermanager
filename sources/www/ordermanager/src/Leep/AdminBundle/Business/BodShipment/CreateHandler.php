<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $idCustomer = $this->container->get('request')->get('id');
        $doctrine = $this->container->get('doctrine');
        $customer = $doctrine->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $formatter = $this->container->get('leep_admin.helper.formatter');

        $model = new CreateModel();
        $model->creationDatetime = new \DateTime();
        $model->bodCustomerOrderNumber = $customer->getOrderNumber();
        $model->bodFromCompany = $config->getBodFromCompany();
        $model->bodFromPerson = $config->getBodFromPerson();
        $model->bodFromEmail = $config->getBodFromEmail();

        $model->bodContributorRole = $config->getBodContributorRole();
        $model->bodContributorName = $config->getBodContributorName();

        $model->bodItemTitle = $config->getBodItemTitle();
        $model->bodItemPaper = $config->getBodItemPaper();
        $model->bodItemBinding = $config->getBodItemBinding();
        $model->bodItemBack = $config->getBodItemBack();
        $model->bodItemFinish = $config->getBodItemFinish();
        $model->bodItemJacket = $config->getBodItemJacket();

        $model->bodOrderNumber = $config->getBodItemBinding();
        $model->bodOrderCopies = 1;
        $model->bodOrderCustomsValue = $config->getBodOrderCustomsValue();
        $model->bodOrderShipping = $config->getBodOrderShipping();
        $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($customer->getDistributionChannelId());
        if ($dc) {
            $model->customBodAccountNumber = $dc->getCustomBodAccountNumber();
            $model->customBodBarcodeNumber = $dc->getCustomBodBarcodeNumber();
            if ($dc->getExpressShipment()) {
                $model->bodOrderShipping = "ParcelExpress";
            }
        }


        $model->bodDeliveryNoteIdReport = $config->getBodIdDeliveryNoteReport();
        $model->bodDeliveryNoteIdLanguage = $customer->getLanguageId();

        $model->isUseNewQueue = true;
        $model->isUploadToFTP = true;

        if ($customer->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_PARTNER) {
            if ($dc) {
                $partner = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findOneById($dc->getPartnerId());
                if ($partner) {
                    $model->bodOrderAddressLine1 = $partner->getPartner();
                    $model->bodOrderAddressLine3 = $partner->getTelephone();
                    $model->bodOrderStreet = $partner->getStreet();
                    $model->bodOrderZip = $partner->getPostCode();
                    $model->bodOrderCity = $partner->getCity();
                    $model->bodOrderCountry = $formatter->format($partner->getCountryId(), 'mapping', 'LeepAdmin_Country_Code');
                }
            }
        }
        else if ($customer->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CUSTOMER) {
            $model->bodOrderAddressLine1 = $customer->getFirstName().' '.$customer->getSurName();
            $model->bodOrderAddressLine3 = $customer->getTelephone();
            $model->bodOrderStreet = $customer->getStreet();
            $model->bodOrderZip = $customer->getPostCode();
            $model->bodOrderCity = $customer->getCity();
            $model->bodOrderCountry = $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_Code');
        }
        else if ($customer->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CHANNEL) {
            if ($dc) {
                $model->bodOrderAddressLine1 = $dc->getDistributionChannel();
                $model->bodOrderAddressLine3 = $dc->getTelephone();
                $model->bodOrderStreet = $dc->getStreet();
                $model->bodOrderZip = $dc->getPostCode();
                $model->bodOrderCity = $dc->getCity();
                $model->bodOrderCountry = $formatter->format($dc->getCountryId(), 'mapping', 'LeepAdmin_Country_Code');
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $idCustomer = $this->container->get('request')->get('id');

        $builder->add('isUseNewQueue', 'checkbox', array(
            'label'    => 'Use new queue?',
            'required' => false
        ));
        $builder->add('isUploadToFTP', 'checkbox', array(
            'label'    => 'Upload to FTP?',
            'required' => false
        ));
        $builder->add('sectionShipmentInfo', 'section', array(
            'label'    => 'Shipment info',
            'property_path' => false
        ));
        $builder->add('creationDatetime',  'datetimepicker', array(
            'label'    => 'Creation datetime',
            'required' => false
        ));
        $builder->add('customerReports', 'choice', array(
            'label'    => 'Report',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => Utils::getApplicableReports($this->container, $idCustomer)
        ));
        $builder->add('bodCustomerOrderNumber', 'text', array(
            'label'    => 'Customer order number',
            'required' => false
        ));
        $builder->add('bodFromCompany', 'text', array(
            'label'    => 'From company',
            'required' => false
        ));
        $builder->add('bodFromPerson', 'text', array(
            'label'    => 'From person',
            'required' => false
        ));
        $builder->add('bodFromEmail', 'text', array(
            'label'    => 'From email',
            'required' => false
        ));
        $builder->add('customBodAccountNumber', 'text', array(
            'label'    => 'Custom BOD Account number',
            'required' => false
        ));
        $builder->add('customBodBarcodeNumber', 'text', array(
            'label'    => 'Custom BOD Barcode number (2 digit)',
            'required' => false
        ));
        $builder->add('sectionContributor', 'section', array(
            'label'    => 'Contributor',
            'property_path' => false
        ));
        $builder->add('bodContributorRole', 'text', array(
            'label'    => 'Role',
            'required' => false
        ));
        $builder->add('bodContributorName', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));

        $builder->add('sectionItem', 'section', array(
            'label'    => 'Item',
            'property_path' => false
        ));
        $builder->add('bodItemTitle', 'text', array(
            'label'    => 'Title',
            'required' => false
        ));
        $builder->add('bodItemPaper', 'text', array(
            'label'    => 'Paper',
            'required' => false
        ));
        $builder->add('bodItemBinding', 'text', array(
            'label'    => 'Binding',
            'required' => false
        ));
        $builder->add('bodItemBack', 'text', array(
            'label'    => 'Back',
            'required' => false
        ));
        $builder->add('bodItemFinish', 'text', array(
            'label'    => 'Finish',
            'required' => false
        ));
        $builder->add('bodItemJacket', 'text', array(
            'label'    => 'Jacket',
            'required' => false
        ));

        $builder->add('sectionOrder', 'section', array(
            'label'    => 'Order',
            'property_path' => false
        ));
        $builder->add('bodOrderNumber', 'text', array(
            'label'    => 'Order number',
            'required' => false
        ));
        $builder->add('bodOrderCopies', 'text', array(
            'label'    => 'No. copies',
            'required' => false
        ));
        $builder->add('bodOrderAddressLine1', 'text', array(
            'label'    => 'Address line 1',
            'required' => true
        ));
        $builder->add('bodOrderAddressLine2', 'text', array(
            'label'    => 'Address line 2',
            'required' => false
        ));
        $builder->add('bodOrderAddressLine3', 'text', array(
            'label'    => 'Address line 3',
            'required' => false
        ));
        $builder->add('bodOrderStreet', 'text', array(
            'label'    => 'Street',
            'required' => true
        ));
        $builder->add('bodOrderZip', 'text', array(
            'label'    => 'ZIP',
            'required' => true
        ));
        $builder->add('bodOrderCity', 'text', array(
            'label'    => 'City',
            'required' => true
        ));
        $builder->add('bodOrderCountry', 'text', array(
            'label'    => 'Country',
            'required' => true
        ));
        $builder->add('bodOrderCustomsValue', 'text', array(
            'label'    => 'Customs value',
            'required' => false
        ));
        $builder->add('bodOrderShipping', 'text', array(
            'label'    => 'Shipping',
            'required' => false
        ));

        $builder->add('sectionDeliveryNote', 'section', array(
            'label'    => 'Delivery note',
            'property_path' => false
        ));
        $builder->add('bodDeliveryNoteIdReport', 'choice', array(
            'label'    => 'Delivery note report',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Report_List')
        ));
        $builder->add('bodDeliveryNoteIdLanguage', 'choice', array(
            'label'    => 'Delivery note report language',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List')
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();
        $idCustomer = $this->container->get('request')->get('id');
        $customerReports = Utils::getApplicableReports($this->container, $idCustomer);
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($idCustomer);

        $bodAccountNumber = trim($model->customBodAccountNumber);
        $bodBarcodeNumber= trim($model->customBodBarcodeNumber);

        if (!empty($bodAccountNumber) && strlen($bodBarcodeNumber) != 2) {
            $this->errors[] = "Bod barcode number must have two digit";
        }

        $addressLine1 = trim($model->bodOrderAddressLine1);
        if (empty($addressLine1)) {
            $this->errors[] = "Empty address line 1 field!";
        }

        $street = trim($model->bodOrderStreet);
        if (empty($street)) {
            $this->errors[] = "Empty street address field!";
        }

        $zip = trim($model->bodOrderZip);
        if (empty($zip)) {
            $this->errors[] = "Empty ZIP field!";
        }

        $city = trim($model->bodOrderCity);
        if (empty($city)) {
            $this->errors[] = "Empty City address field!";
        }

        $country = trim($model->bodOrderCountry);
        if (empty($country)) {
            $this->errors[] = "Empty Country field!";
        }

        // Validation
        if (strlen($model->bodOrderAddressLine1) > 35) {
            $this->errors[] = "Address line 1 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderAddressLine2) > 35) {
            $this->errors[] = "Address line 2 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderAddressLine3) > 35) {
            $this->errors[] = "Address line 3 is too long, max 35 characters";
        }
        if (strlen($model->bodOrderStreet) > 30) {
            $this->errors[] = "Street is too long, max 30 characters";
        }
        if (strlen($model->bodOrderZip) > 10) {
            $this->errors[] = "Zip is too long, max 10 characters";
        }
        if (strlen($model->bodOrderCity) > 30) {
            $this->errors[] = "City is too long, max 30 characters";
        }
        if (!empty($this->errors)) {
            $this->messages = array();
            return false;
        }
        // Proceed

        $shipment = new Entity\BodShipment();
        $shipment->setCreationDatetime($model->creationDatetime);
        $shipment->setIdCustomer($idCustomer);
        $shipment->setBodCustomerOrderNumber($model->bodCustomerOrderNumber);
        $shipment->setBodFromCompany($model->bodFromCompany);
        $shipment->setBodFromPerson($model->bodFromPerson);
        $shipment->setBodFromEmail($model->bodFromEmail);

        $shipment->setBodContributorRole($model->bodContributorRole);
        $shipment->setBodContributorName($model->bodContributorName);

        $shipment->setBodItemTitle($model->bodItemTitle);
        $shipment->setBodItemPaper($model->bodItemPaper);
        $shipment->setBodItemBinding($model->bodItemBinding);
        $shipment->setBodItemBack($model->bodItemBack);
        $shipment->setBodItemFinish($model->bodItemFinish);
        $shipment->setBodItemJacket($model->bodItemJacket);

        $shipment->setBodOrderNumber($model->bodOrderNumber);
        $shipment->setBodOrderCopies($model->bodOrderCopies);
        $shipment->setBodOrderAddressLine1($model->bodOrderAddressLine1);
        $shipment->setBodOrderAddressLine2($model->bodOrderAddressLine2);
        $shipment->setBodOrderAddressLine3($model->bodOrderAddressLine3);
        $shipment->setBodOrderStreet($model->bodOrderStreet);
        $shipment->setBodOrderZip($model->bodOrderZip);
        $shipment->setBodOrderCity($model->bodOrderCity);
        $shipment->setBodOrderCountry($model->bodOrderCountry);
        $shipment->setBodOrderCustomsValue($model->bodOrderCustomsValue);
        $shipment->setBodOrderShipping($model->bodOrderShipping);

        $shipment->setBodIdDeliveryNoteReport($model->bodDeliveryNoteIdReport);
        $shipment->setBodDeliveryNoteIdReport($model->bodDeliveryNoteIdReport);
        $shipment->setBodDeliveryNoteIdLanguage($model->bodDeliveryNoteIdLanguage);

        $shipment->setCustomBodAccountNumber($bodAccountNumber);
        $shipment->setCustomBodBarcodeNumber($bodBarcodeNumber);

        if ($model->isUseNewQueue) {
            $shipment->setStatus(Constant::BOD_SHIPMENT_STATUS_PENDING_IN_QUEUE);
        } else {
            $shipment->setStatus(Constant::BOD_SHIPMENT_STATUS_OPEN);
        }

        $shipment->setPackageFile('');

        $em->persist($shipment);
        $em->flush();

        // Save customer report
        $customerReportNames = array();
        foreach ($model->customerReports as $idReport) {
            $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($idReport);

            $bodShipmentBook = new Entity\BodShipmentBook();
            $bodShipmentBook->setIdBodShipment($shipment->getId());
            $bodShipmentBook->setIdReport($idReport);
            $bodShipmentBook->setReportFile($customerReport->getFilename());
            $em->persist($bodShipmentBook);

            $customerReportNames[] = isset($customerReports[$idReport]) ? $customerReports[$idReport] : '--- ERROR ---';
        }
        $shipment->setCustomerReportName(implode("\n", $customerReportNames));
        $em->flush();

        // Add report queue
        if ($model->isUseNewQueue) {
            $description = array(
                'Reports: <br/>'.implode("<br/>", $customerReportNames),
                'Order number: '.$customer->getOrderNumber()
            );

            if ($model->isUploadToFTP) {
                $description[] = 'Upload to FTP: Yes';
            }

            $input = array(
                'idBodShipment' => $shipment->getId(),
                'isUploadToFTP' => $model->isUploadToFTP
            );

            $reportQueue = new Entity\ReportQueue();
            $reportQueue->setIdWebUser($userManager->getUser()->getId());
            $reportQueue->setInputTime(new \DateTime());
            $reportQueue->setDescription(implode('<br/>', $description));
            $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
            $reportQueue->setInput(serialize($input));
            $reportQueue->setIdCustomer($idCustomer);
            $reportQueue->setIdJobType(Business\ReportQueue\Constant::JOB_TYPE_BUILD_SHIPMENT);
            if ($model->isUploadToFTP) {
                $reportQueue->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_PENDING);
            }
            $em->persist($reportQueue);
            $em->flush();
        }

        parent::onSuccess();
    }
}
