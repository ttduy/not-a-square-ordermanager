<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public function getColumnMapping() {
        return array('creationDatetime', 'customerReportName', 'status', 'package', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Creation datetime',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Customer report name',    'width' => '30%', 'sortable' => 'false'),
            array('title' => 'Status',                  'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Package',                 'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Action',                  'width' => '10%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_BodShipmentGridReader');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:BodShipment', 'p');
        $queryBuilder->andWhere('p.idCustomer = :idCustomer')
            ->setParameter('idCustomer', $this->container->get('request')->get('id', 0));
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        //$builder->addButton('Edit', $mgr->getUrl('leep_admin', 'bod_shipment', 'edit', 'edit', array('id' => $row->getIdCustomer(), 'idBodShipment' => $row->getId())), 'button-edit');
        $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'bod_shipment', 'delete', 'delete', array('id' => $row->getIdCustomer(), 'idBodShipment' => $row->getId())), 'button-delete');

        return $builder->getHtml();
    }

    public function buildCellStatus($row) {
        $status = Constant::getBodShipmentStatus()[$row->getStatus()];
        $style = "";
        if ($row->getStatus() == Constant::BOD_SHIPMENT_STATUS_GENERATE_ERROR ||
            $row->getStatus() == Constant::BOD_SHIPMENT_STATUS_UPLOAD_ERROR) {
            $style = "color:red; font-weight:bold;";
        }

        return "<span style='$style'>$status</span>";
    }

    public function buildCellPackage($row) {
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $mgr = $this->getModuleManager();

        $packageFile = $row->getPackageFile();
        if (empty($packageFile)) {
            if ($row->getStatus() == Constant::BOD_SHIPMENT_STATUS_GENERATE_FINISHED ||
                $row->getStatus() == Constant::BOD_SHIPMENT_STATUS_UPLOADING ||
                $row->getStatus() == Constant::BOD_SHIPMENT_STATUS_UPLOAD_ERROR ||
                $row->getStatus() == Constant::BOD_SHIPMENT_STATUS_FINISHED) {
                $builder = $this->container->get('leep_admin.helper.button_list_builder');

                $builder->addNewTabButton('Download pdf', $mgr->getUrl('leep_admin', 'report', 'report', 'downloadShipmentPackage', array('idBodShipment' => $row->getId())), 'button-down');
                $builder->addNewTabButton('Download low resolution pdf', $mgr->getUrl('leep_admin', 'report', 'report', 'downloadShipmentPackage', array('idBodShipment' => $row->getId(), 'isLowRes' => 1)), 'button-download-gray');

                return $builder->getHtml();
            }
            return $formatter->format($row->getStatus(), 'mapping', 'LeepAdmin_BodShipment_Status');
        }

        $url = $mgr->getUrl('leep_admin', 'report', 'report', 'downloadShipmentPackage', array('idBodShipment' => $row->getId()));
        return "<a href='".$url."'>Download</a>";
    }

    public function buildCellCustomerReportName($row) {
        return nl2br($row->getCustomerReportName());
    }
}