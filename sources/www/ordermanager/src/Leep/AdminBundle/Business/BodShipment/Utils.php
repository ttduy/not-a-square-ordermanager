<?php
namespace Leep\AdminBundle\Business\BodShipment;

use Leep\AdminBundle\Business;

class Utils {
    public static function getApplicableReports($container, $idCustomer) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();

        $reportMap = array();
        $reports = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findByIdCustomer($idCustomer);
        foreach ($reports as $report) {
            $name = sprintf("%s / %s / %s",
                $formatter->format($report->getInputTime(), 'datetime'),
                $formatter->format($report->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List'),
                $report->getName()
            );

            $reportMap[$report->getId()] = $name;
        }

        return $reportMap;
    }

    public static function getShipmentId($container, $idBodShipment) {
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p.id')
            ->from('AppDatabaseMainBundle:BodShipmentBook', 'p')
            ->andWhere('p.idBodShipment = :idBodShipment')
            ->setParameter('idBodShipment', $idBodShipment)
            ->setMaxResults(1);
        $results = $query->getQuery()->getResult();

        $shipmentId = 0;
        foreach ($results as $r) {
            $shipmentId = $r['id'];
        }

        while (strlen($shipmentId) < 8) { $shipmentId = '0'.$shipmentId; }

        return $shipmentId;
    }
}