<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveSample', 'app_main')->findOneById($id);
    }

    public function convertToFormModel($sample) { 
        $common = $this->container->get('leep_admin.helper.common');
        $queryWrapper = $this->container->get('leep_admin.cryosave_sample.business.query_wrapper');

        if (!$queryWrapper->isYourSample($sample)) {        
            $mgr = $this->container->get('easy_module.manager');
            $url = $mgr->getUrl('leep_admin', 'cryosave_sample', 'grid', 'list');
            header('Location: '.$url);
            exit;
        }
        
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new EditModel(); 
        $model->timestamp = $sample->getTimestamp();
        $model->sampleCode = $sample->getSampleCode();
        $model->idSex = $sample->getIdSex();
        $model->birthday = $sample->getBirthday();
        $model->idEthnicity = $sample->getIdEthnicity();
        $model->idOrder = $sample->getIdOrder();
        $model->idLanguage = $sample->getIdLanguage();
        $model->idSampleOrigin = $sample->getIdSampleOrigin();
        $model->dateOfSample = $sample->getDateOfSample();

        // Status
        $model->statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CryosaveSampleStatus', 'p')
            ->andWhere('p.idCryosaveSample = :idCryosaveSample')
            ->setParameter('idCryosaveSample', $sample->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new FormType\AppCryosaveSampleStatusModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $m->notes = $status->getNotes();
            $model->statusList[] = $m;
        }
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
            
        // Order information
        $builder->add('sectionOrderInformation',          'section', array(
            'label' => 'Sample information',
            'property_path' => false
        ));
        $builder->add('timestamp', 'datetimepicker', array(
            'label' => 'Timestamp',
            'required' => false
        ));        
        $builder->add('sampleCode', 'text', array(
            'label' => 'Sample code',
            'required' => true
        ));        
        $builder->add('idSex', 'choice', array(
            'label' => 'Sex',
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Sex'),
            'required' => false
        ));        
        $builder->add('birthday', 'datepicker', array(
            'label' => 'Birthday',
            'required' => false
        ));        
        $builder->add('idEthnicity', 'choice', array(
            'label' => 'Ethnicity',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Ethnicity'),
            'required' => false
        ));        
        $builder->add('idOrder', 'choice', array(
            'label' => 'Order',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Order'),
            'required' => false
        ));        
        $builder->add('idLanguage', 'choice', array(
            'label' => 'Language',
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'required' => false
        ));        
        $builder->add('idSampleOrigin', 'choice', array(
            'label' => 'Sample origin',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_SampleOrigin'),
            'required' => false
        ));          
        $builder->add('dateOfSample', 'datepicker', array(
            'label' => 'Date of sample collection',
            'required' => false
        ));     

        // Status
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'type' => new FormType\AppCryosaveSampleStatusRow($this->container),
            'label' => 'Status record',
            'required' => false
        ));


        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();   
        $dbHelper = $this->container->get('leep_admin.helper.database');
        
        $this->entity->setTimestamp($model->timestamp);
        $this->entity->setSampleCode($model->sampleCode);
        $this->entity->setIdSex($model->idSex);
        $this->entity->setBirthday($model->birthday);
        $this->entity->setIdEthnicity($model->idEthnicity);
        $this->entity->setIdOrder($model->idOrder);
        $this->entity->setIdLanguage($model->idLanguage);
        $this->entity->setIdSampleOrigin($model->idSampleOrigin);
        $this->entity->setDateOfSample($model->dateOfSample);
        $em->flush();

        // Update status
        $filters = array('idCryosaveSample' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:CryosaveSampleStatus', $filters);

        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\CryosaveSampleStatus();
            $status->setIdCryosaveSample($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
            
            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $this->entity->setCurrentStatus($currentStatus);
        $this->entity->setCurrentStatusDate($currentStatusDate);

        $em->flush();

        // Update list
        Utils::updateStatusList($this->container, $this->entity);
        parent::onSuccess();
    }
}