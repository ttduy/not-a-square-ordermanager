<?php 
namespace Leep\AdminBundle\Business\CryosaveSample;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $queryWrapper = $this->container->get('leep_admin.cryosave_sample.business.query_wrapper');

        $cryosaveSample = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneById($id);
        if (!$queryWrapper->isYourSample($cryosaveSample)) {
            die('Access denied');
        }

        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($cryosaveSample);
        $em->flush();
    }
}