<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $container;

    public $timestamp;
    public $sampleCode;
    public $idSex;
    public $birthday;
    public $idEthnicity;
    public $idOrder;
    public $idLanguage;
    public $idSampleOrigin;
    public $dateOfSample;
    
    public $statusList;

    public function isValid(ExecutionContext $context) {  
        $em = $this->container->get('doctrine')->getEntityManager();

        $cryosaveSample = $em->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneBySampleCode($this->sampleCode);
        if ($cryosaveSample) {
            $context->addViolationAtSubPath('sampleCode', "Sample code is already existed");                    
        }
    }
}