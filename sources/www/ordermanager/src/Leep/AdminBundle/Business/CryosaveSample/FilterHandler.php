<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class FilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $common = $this->container->get('leep_admin.helper.common');

        if (!$common->isExternalLab()) {
            $builder->add('idSampleOrigin', 'choice', array(
                'label'       => 'Sample origin',
                'required'    => false,
                'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_CryosaveSample_SampleOrigin'),
                'empty_value' => false
            ));
            $builder->add('idLaboratory', 'choice', array(
                'label'       => 'Laboratory',
                'required'    => false,
                'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_CryosaveSample_Laboratory'),
                'empty_value' => false
            ));
        }
        $builder->add('sampleCode', 'text', array(
            'label'       => 'Sample code',
            'required'    => false
        ));
        $builder->add('currentStatus', 'choice', array(
            'label'       => 'Last status',
            'required'    => false,
            'choices'     => array(0 => 'All') + $mapping->getMapping('LeepAdmin_CryosaveSample_Status'),
            'empty_value' => false
        ));
        $builder->add('status', 'choice', array(
            'label'       => 'Has status',
            'required'    => false,
            'choices'     => array(0 => 'All', -1 => 'None') + $mapping->getMapping('LeepAdmin_CryosaveSample_Status'),
            'empty_value' => false
        ));


        return $builder;
    }
}