<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class BulkEditHandler extends AppEditHandler {
    public $selectedId;

    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) {
        $mapping = $this->container->get('easy_mapping');
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $em = $this->container->get('doctrine')->getEntityManager();

        // Get selected sample codes
        $model = new BulkEditModel();
        $model->selectedSampleCodes = array();
        $result = $this->getSelectedSamples();
        foreach ($result as $r) {
            $model->selectedSampleCodes[] = $r->getSampleCode();
        }
        $model->selectedSampleCodes = implode("\n", $model->selectedSampleCodes);

        $model->statusDate = new \DateTime();

        return $model;
    }

    protected function getSelectedSamples() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $result = $query->select('d')
            ->from('AppDatabaseMainBundle:CryosaveSample', 'd')
            ->andWhere('d.id in (:arr)')
            ->setParameter('arr', $this->selectedId)
            ->getQuery()->getResult();
        return $result;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $common = $this->container->get('leep_admin.helper.common');

        $builder->add('selectedSampleCodes', 'textarea', array(
            'label'    => 'Sample codes',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 70
            )
        ));
        $builder->add('currentSamples', 'label', array(
            'label'    => 'Selected samples',
            'required' => false
        ));
        if ($common->isExternalLab()) {
            $builder->add('action',   'choice', array(
                'label'    => 'Action',
                'required' => false,
                'choices'  => Constant::getExternalLabBulkEditActions()
            ));
        }
        else {
            $builder->add('action',   'choice', array(
                'label'    => 'Action',
                'required' => false,
                'choices'  => Constant::getBulkEditActions()
            ));
        }
        $builder->add('status',   'choice', array(
            'label'    => 'Status',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Status')
        ));
        $builder->add('statusDate',     'datepicker', array(
            'label'    => 'Date',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $userManager = $this->container->get('leep_admin.web_user.business.user_manager');
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $config = $this->container->get('leep_admin.helper.common')->getConfig();
        $common = $this->container->get('leep_admin.helper.common');

        // UPDATE STATUS
        if ($model->action == Constant::BULK_EDIT_ACTION_UPDATE_STATUS) {
            $samples = $this->getSelectedSamplesByCode($model->selectedSampleCodes);
            foreach ($samples as $sample) {
                $status = new Entity\CryosaveSampleStatus();
                $status->setIdCryosaveSample($sample->getId());
                $status->setStatusDate($model->statusDate);
                $status->setStatus($model->status);
                $status->setSortOrder($this->getMaxSortOrder($sample->getId()) + 1);
                $em->persist($status);

                $sample->setCurrentStatus($model->status);
                $sample->setCurrentStatusDate($model->statusDate);
            }
            $em->flush();

            foreach ($samples as $sample) {
                Utils::updateStatusList($this->container, $sample);
            }

            $this->messages[] = 'Status has been changed successfully';
        }
        // BUILD REPORT
        else if ($model->action == Constant::BULK_EDIT_ACTION_BUILD_REPORT) {
            if ($common->isExternalLab()) {
                die("Access denied");
            }

            $samples = $this->getSelectedSamplesByCode($model->selectedSampleCodes);
            foreach ($samples as $sample) {
                $language = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($sample->getIdLanguage());
                $idReport = Constant::getOrderReport($sample->getIdOrder(), $config);
                $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($idReport);

                if (empty($language) || empty($report)) {
                    $this->errors[] = "Can't generate report for sample ".$sample->getSampleCode();
                    continue;
                }

                $description = array();
                $description[] = "Sample code: ".$sample->getSampleCode();
                $description[] = "Report: ".$report->getName();
                $description[] = "Language: ".$language->getName();

                $input = array(
                    'idCryosaveSample'   => $sample->getId(),
                    'idReport'           => $report->getId(),
                    'idLanguage'         => $language->getId(),
                    'status'             => $model->status,
                    'idFormulaTemplate'  => $report->getIdFormulaTemplate(),
                    'customerData'       => Utils::loadSampleData($this->container, $sample, true)
                );

                $reportQueue = new Entity\ReportQueue();
                $reportQueue->setIdWebUser($userManager->getUser()->getId());
                $reportQueue->setInputTime(new \DateTime());
                $reportQueue->setDescription(implode('<br/>', $description));
                $reportQueue->setStatus(Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING);
                $reportQueue->setInput(serialize($input));
                $reportQueue->setIdReport($report->getId());
                $reportQueue->setIdCustomer($sample->getId());
                $reportQueue->setIsSaved(1);
                $reportQueue->setIdJobType(Business\ReportQueue\Constant::JOB_TYPE_BUILD_CRYOSAVE_REPORT);
                $em->persist($reportQueue);
            }
            $em->flush();
        }
        // Upload report
        else if ($model->action == Constant::BULK_EDIT_ACTION_UPLOAD_REPORT) {
            if ($common->isExternalLab()) {
                die("Access denied");
            }

            set_time_limit(240);

            $reportDir = $this->container->getParameter('kernel.root_dir').'/../web/report';
            $tempDir = $this->container->getParameter('kernel.root_dir').'/../files/temp';
            $ftpServer = $config->getCryosaveFtpServer();
            $ftpUsername = $config->getCryosaveFtpUsername();
            $ftpPassword = $config->getCryosaveFtpPassword();
            $ftpReportFolder = '/';

            if (!$connId = ftp_connect($ftpServer)) {
                $this->errors[] = "Can't connect via FTP";
                return;
            }
            if (!$loginResult = ftp_login($connId, $ftpUsername, $ftpPassword)) {
                $this->errors[] = "Can't login to FTP server";
                return;
            }
            ftp_pasv($connId, true);

            $samples = $this->getSelectedSamplesByCode($model->selectedSampleCodes);
            foreach ($samples as $sample) {
                $reportFile = $reportDir.'/'.$sample->getReportFilename();
                if (!is_file($reportFile)) {
                    $awsHelper = $this->container->get('leep_admin.helper.aws_backup_files_helper');
                    $result = $awsHelper->temporaryDownloadBackupFiles('report', $sample->getReportFilename());
                    if ($result) {
                        $reportFile = "$tempDir/".$sample->getReportFilename();
                    }
                }

                if (is_file($reportFile)) {
                    if (ftp_put($connId, $ftpReportFolder.'/'.$sample->getSampleCode().'.pdf', $reportFile, FTP_BINARY)) {
                        $sample->setIsUploaded(1);
                        if ($model->status != 0) {
                            $status = new Entity\CryosaveSampleStatus();
                            $status->setIdCryosaveSample($sample->getId());
                            $status->setStatusDate($model->statusDate);
                            $status->setStatus($model->status);
                            $status->setSortOrder($this->getMaxSortOrder($sample->getId()) + 1);
                            $em->persist($status);

                            $sample->setCurrentStatus($model->status);
                            $sample->setCurrentStatusDate($model->statusDate);
                        }
                    }
                    else {
                        $this->errors[] = "Can't upload ".$sample->getSampleCode();
                    }
                } else {
                    $this->errors[] = $sample->getSampleCode()." doesn't have report yet";
                }
            }
            $em->flush();
            ftp_close($connId);

            foreach ($samples as $sample) {
                Utils::updateStatusList($this->container, $sample);
            }
        }
    }

    protected function getMaxSortOrder($idSample) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('MAX(p.sortOrder)')
            ->from('AppDatabaseMainBundle:CryosaveSampleStatus', 'p')
            ->andWhere('p.idCryosaveSample = '.$idSample);
        return $query->getQuery()->getSingleScalarResult();
    }

    protected function getSelectedSamplesByCode($selectedSampleCodes) {
        $common = $this->container->get('leep_admin.helper.common');
        $queryWrapper = $this->container->get('leep_admin.cryosave_sample.business.query_wrapper');

        $codes = explode("\n", $selectedSampleCodes);
        $samplesCodes = array('0');
        foreach ($codes as $code) {
            $samplesCodes[] = trim($code);
        }

        $em = $this->container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('d')
            ->from('AppDatabaseMainBundle:CryosaveSample', 'd')
            ->andWhere('d.sampleCode in (:arr)')
            ->setParameter('arr', $samplesCodes);

        $queryWrapper->filterBulkSamples($query);

        $result = $query->getQuery()->getResult();
        return $result;
    }
}