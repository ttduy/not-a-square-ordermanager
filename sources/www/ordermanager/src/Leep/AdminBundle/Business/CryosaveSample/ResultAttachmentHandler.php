<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class ResultAttachmentHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        return array();
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $doctrine = $this->container->get('doctrine');
        $common = $this->container->get('leep_admin.helper.common');

        if ($common->isExternalLab()) {
            $laboratory = $doctrine->getRepository('AppDatabaseMainBundle:CryosaveLaboratory')->findOneById($common->getIdLaboratory());

            $builder->add('attachment', 'file_attachment', array(
                'label'    => 'Cryosave',
                'required' => false,
                'key'      => 'third_party_pdf/Cryosave/'.$laboratory->getCode(),
                'snapshot' => false,
                'mode'     => 'full'
            ));
        }
        else {
            $builder->add('attachment', 'file_attachment', array(
                'label'    => 'Cryosave',
                'required' => false,
                'key'      => 'third_party_pdf/Cryosave',
                'snapshot' => false,
                'mode'     => 'full'
            ));
        }
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();     
        parent::onSuccess();
    }
}
