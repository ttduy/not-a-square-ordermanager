<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

class EditModel {
    public $timestamp;
    public $sampleCode;
    public $idSex;
    public $birthday;
    public $idEthnicity;
    public $idOrder;
    public $idLanguage;
    public $idSampleOrigin;
    public $dateOfSample;
    
    public $statusList;
}