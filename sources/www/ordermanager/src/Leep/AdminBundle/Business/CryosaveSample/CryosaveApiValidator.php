<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

class CryosaveApiValidator {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function login($username, $password) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('AppDatabaseMainBundle:CryosaveUser')->findOneByUsername($username);
        if ($user) {
            if (strcmp($user->getPassword(), $password) === 0) {
                return $user;
            }
        }
        return false;
    }

    public function process($data) {
        // SAMPLEBARCODE_SEX_DATEOFBIRTH_ETHNICITY_ORDER_LANGUAGE_ORIGIN_TIMESTAMP_DATEOFSAMPLECOLLECTION
        // CRY0000001_M_02112013_CAUC_BABY111_EN_EU_1398426049000_15012015
        if (count($data) != 9) {
            return array('result' => false, 'message' => array('Invalid data, expecting SAMPLEBARCODE_SEX_DATEOFBIRTH_ETHNICITY_ORDER_LANGUAGE_ORIGIN_TIMESTAMP_DATEOFSAMPLECOLLECTION'));
        }

        $result = array('error' => array(), 'data' => array());
        $this->validateSampleCode($result, $data[0]);
        $this->validateMapping($result, 'Sex', $data[1], 'LeepAdmin_CryosaveSample_SexCode');
        $this->validateBirthday($result, $data[2]);
        $this->validateMapping($result, 'Ethnicity', $data[3], 'LeepAdmin_CryosaveSample_EthnicityCode');
        $this->validateMapping($result, 'Order', $data[4], 'LeepAdmin_CryosaveSample_OrderCode');
        $this->validateMapping($result, 'Language', $data[5], 'LeepAdmin_Language_List');
        $this->validateMapping($result, 'SampleOrigin', $data[6], 'LeepAdmin_CryosaveSample_SampleOriginCode');
        $this->validateTimestamp($result, $data[7]);
        $this->validateDateOfSample($result, $data[8]);

        if (!empty($result['error'])) {
            return array('result' => false, 'message' => $result['error']);
        }

        return array('result' => true, 'data' => $result['data']);
    }

    public function validateSampleCode(&$result, $sampleCode) {
        $sampleCode = trim($sampleCode);

        $doctrine = $this->container->get('doctrine');
        $sample = $doctrine->getRepository('AppDatabaseMainBundle:CryosaveSample')->findOneBySampleCode($sampleCode);
        if ($sample) {
            $result['error'][] = "Sample code is already existed";
        }
        else {
            $result['data']['SampleCode'] = $sampleCode;
        }
    }

    public function validateMapping(&$result, $name, $value, $mappingCode) {
        $value = trim($value);

        $mapping = $this->container->get('easy_mapping');
        $arr = $mapping->getMapping($mappingCode);
        $flippedArr = array_flip($arr);

        if (!isset($flippedArr[$value])) {
            $result['error'][] = $name.' is invalid, expecting '.implode(', ', $arr);
        }
        else {
            $result['data'][$name] = $flippedArr[$value];
        }
    }

    public function validateBirthday(&$result, $birthday) {
        // 02112013
        if (strlen($birthday) != 8) {
            $result['error'][] = "Birthday is invalid, expecting DDMMYYYY";
            return;
        }

        $birthday = \DateTime::createFromFormat('dmY', $birthday);
        if (!$birthday) {
            $result['error'][] = "Birthday is invalid, expecting DDMMYYYY";
            return;
        }

        $result['data']['Birthday'] = $birthday;
    }

    public function validateDateOfSample(&$result, $date) {
        // 02112013
        if (strlen($date) != 8) {
            $result['error'][] = "Date of sample is invalid, expecting DDMMYYYY";
            return;
        }

        $date = \DateTime::createFromFormat('dmY', $date);
        if (!$date) {
            $result['error'][] = "Date of sample is invalid, expecting DDMMYYYY";
            return;
        }

        $result['data']['DateOfSample'] = $date;
    }


    public function validateTimestamp(&$result, $timestamp) {
        $timestamp = intval($timestamp / 1000);
        $timestamp = \DateTime::createFromFormat('U', $timestamp);
        if (!$timestamp) {
            $result['error'][] = "Timestamp is invalid";
            return;
        }

        $result['data']['Timestamp'] = $timestamp;
    }
}