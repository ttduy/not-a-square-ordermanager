<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $todayTs;
    public $externalPdfPath;
    public $attachmentUrl;
    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
        $this->externalPdfPath = $container->getParameter('kernel.root_dir').'/../web/attachments/external_pdf';
        $this->attachmentUrl = $container->getParameter('attachment_url');
    }

    public function getColumnMapping() {
        return array('timestamp', 'idSampleOrigin', 'idLaboratory', 'sampleCode', 'dateOfSample', 'currentStatus', 'resultPdf', 'report', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Timestamp',            'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Sample Origin',        'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Laboratory',           'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Sample code',          'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Date of sample',       'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Current status',       'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Result PDF',           'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Report',               'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',               'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_CryosaveSampleGridReader');
    }

    public function buildQuery($queryBuilder) {
        $common = $this->container->get('leep_admin.helper.common');
        $queryWrapper = $this->container->get('leep_admin.cryosave_sample.business.query_wrapper');

        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');   
        
        $queryBuilder
            ->select('p.id, p.idSampleOrigin, p.sampleCode, p.timestamp, p.idLaboratory, p.currentStatus, 
                p.isUploaded, l.code, p.dateOfSample,
                p.currentStatusDate, p.reportFilename, q.id AS idReportQueue')
            ->from('AppDatabaseMainBundle:CryosaveSample', 'p')
            ->leftJoin('AppDatabaseMainBundle:ReportQueue', 'q', 'WITH', 
                'p.id = q.idCustomer AND (q.status = :queueStatusPending OR q.status = :queueStatusProcessing) AND q.idJobType = :jobTypeCryosave')
            ->leftJoin('AppDatabaseMainBundle:CryosaveLaboratory', 'l', 'WITH', 'p.idLaboratory = l.id')
            ->setParameter('queueStatusPending', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PENDING)
            ->setParameter('queueStatusProcessing', Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_PROCESSING)
            ->setParameter('jobTypeCryosave', Business\ReportQueue\Constant::JOB_TYPE_BUILD_CRYOSAVE_REPORT);
        if (trim($this->filters->sampleCode) != '') {
            $queryBuilder->andWhere('p.sampleCode LIKE :sampleCode')
                ->setParameter('sampleCode', '%'.trim($this->filters->sampleCode).'%');
        }
        if ($this->filters->currentStatus != 0) {
            $queryBuilder->andWhere('p.currentStatus = :currentStatus')
                ->setParameter('currentStatus', intval($this->filters->currentStatus));
        }

        if ($this->filters->idLaboratory != 0) {
            $queryBuilder->andWhere('p.idLaboratory = :idLaboratory')
                ->setParameter('idLaboratory', intval($this->filters->idLaboratory));
        }
        if ($this->filters->idSampleOrigin != 0) {
            $queryBuilder->andWhere('p.idSampleOrigin = :idSampleOrigin')
                ->setParameter('idSampleOrigin', intval($this->filters->idSampleOrigin));
        }
        if ($this->filters->status != 0) {
            if ($this->filters->status == -1) {
                $queryBuilder->andWhere('(p.statusList = :empty) OR (p.statusList IS NULL)')
                    ->setParameter('empty', '');
            }
            else {
                $queryBuilder->andWhere('FIND_IN_SET('.$this->filters->status.', p.statusList) > 0');
            }
        }


        $queryWrapper->filterGridReader($queryBuilder);
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('cryosaveSampleModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'cryosave_sample', 'edit', 'edit', array('id' => $row['id'])), 'button-edit');    
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'cryosave_sample', 'delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }   
        return $age;
    }

    public function buildCellCurrentStatus($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_CryosaveSample_Status', $row['currentStatus']);
        
        $age = $this->getAge($row['currentStatusDate']);        
        return (($age === '') ? '': "(${age}d) ").$status.'&nbsp;&nbsp;'.$builder->getHtml();
    }

    public function buildCellResultPdf($row) {               
        $result = file_exists($this->externalPdfPath.'/Cryosave/'.$row['code'].'/'.$row['sampleCode'].'.pdf') ? true : false;
        if ($result) {
            $url = $this->attachmentUrl.'/external_pdf/Cryosave/'.$row['code'].'/'.$row['sampleCode'].'.pdf';
            return '<a target="_blank" href="'.$url.'">View</a>';
        }
        return '';
    }

    public function buildCellReport($row) {
        // Report
        $str = '';
        $idReportQueue = $row['idReportQueue'];
        $reportFile = $row['reportFilename'];
        if (!empty($reportFile)) {
            $mgr = $this->getModuleManager();
            $url = $mgr->getUrl('leep_admin', 'cryosave_sample', 'feature', 'downloadReport', array('id' => $row['id']));
            $str .= "<a href='".$url."'>Download</a>";

            if ($row['isUploaded'] == 1) {
                $str .= '&nbsp;(Uploaded)';
            }
        }
        else {
            if (empty($idReportQueue)) {
                $str .= "";
            }
            else {
                $str .= "Building";
            }
        }
        return $str;
    }
}
