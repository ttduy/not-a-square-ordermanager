<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

class Utils {
    public static function updateStatusList($container, $cryosaveSample) {
        $em = $container->get('doctrine')->getEntityManager();
        $status = array();
        $results = $em->getRepository('AppDatabaseMainBundle:CryosaveSampleStatus')->findByIdCryosaveSample($cryosaveSample->getId());
        foreach ($results as $r) {            
            $status[] = $r->getStatus();
        }

        $cryosaveSample->setStatusList(implode(',', $status));
        $em->persist($cryosaveSample);
        $em->flush();
    }

    public static function loadSampleData($container, $cryosaveSample, $returnArray = false) {
        $formatter = $container->get('leep_admin.helper.formatter');
        $em = $container->get('doctrine')->getEntityManager();
        $externalPdfPath = $container->getParameter('kernel.root_dir').'/../web/attachments/third_party_pdf';

        $results = array();
        $results['SampleCode'] = $cryosaveSample->getSampleCode();
        $results['Laboratory'] = $formatter->format($cryosaveSample->getIdLaboratory(), 'mapping', 'LeepAdmin_CryosaveSample_Laboratory');
        $results['Sex'] = $formatter->format($cryosaveSample->getIdSex(), 'mapping', 'LeepAdmin_CryosaveSample_SexCode');
        $results['Birthday'] = $formatter->format($cryosaveSample->getBirthday(), 'date');
        $results['Ethnicity'] = $formatter->format($cryosaveSample->getIdEthnicity(), 'mapping', 'LeepAdmin_CryosaveSample_EthnicityCode');
        $results['Order'] = $formatter->format($cryosaveSample->getIdOrder(), 'mapping', 'LeepAdmin_CryosaveSample_OrderCode');
        $results['Language'] = $formatter->format($cryosaveSample->getIdLanguage(), 'mapping', 'LeepAdmin_Language_List');
        $results['SampleOrigin'] = $formatter->format($cryosaveSample->getIdSampleOrigin(), 'mapping', 'LeepAdmin_CryosaveSample_SampleOriginCode');
                        
        $lab = $em->getRepository('AppDatabaseMainBundle:CryosaveLaboratory')->findOneById($cryosaveSample->getIdLaboratory());
        $hasResult = file_exists($externalPdfPath.'/Cryosave/'.$lab->getCode().'/'.$cryosaveSample->getSampleCode().'.pdf') ? true : false;
        $results['ResultPdf'] = $hasResult ? '/Cryosave/'.$lab->getCode().'/'.$cryosaveSample->getSampleCode().'.pdf' : '';

        if ($returnArray) {
            return $results;
        }

        $rows = array();
        foreach ($results as $k => $v) {
            $rows[] = $k.' = '.$v;
        }
        return implode("\n", $rows);
    }
}