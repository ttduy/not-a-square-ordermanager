<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Symfony\Component\Validator\ExecutionContext;

class BulkEditModel {
    public $selectedSampleCodes;
    public $currentSamples;
    public $action;
    public $status;
    public $statusDate;
}
