<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

class QueryWrapper {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function filterGridReader($queryBuilder) {
        $common = $this->container->get('leep_admin.helper.common');

        if ($common->isExternalLab()) {
            $lab = $common->getYourLaboratory();

            $queryBuilder->andWhere('p.idSampleOrigin = :idSampleOrigin')
                ->setParameter('idSampleOrigin', $lab->getIdSampleOrigin());
        }
    }

    public function isYourSample($cryosaveSample) {
        $common = $this->container->get('leep_admin.helper.common');
        if ($common->isExternalLab()) {
            $lab = $common->getYourLaboratory();

            if ($cryosaveSample->getIdSampleOrigin() != $lab->getIdSampleOrigin()) {
                return false;
            }
        }
        return true;
    }

    public function filterBulkSamples($query) {
        $common = $this->container->get('leep_admin.helper.common');

        if ($common->isExternalLab()) {
            $lab = $common->getYourLaboratory();

            $query
                ->andWhere('d.idSampleOrigin = :idSampleOrigin')
                ->setParameter('idSampleOrigin', $lab->getIdSampleOrigin());
        }
    }

    public function addSearchCriteria(&$criteria) {
        $common = $this->container->get('leep_admin.helper.common');

        if ($common->isExternalLab()) {
            $lab = $common->getYourLaboratory();
            $criteria['idSampleOrigin'] = $lab->getIdSampleOrigin();
        }
    }
}