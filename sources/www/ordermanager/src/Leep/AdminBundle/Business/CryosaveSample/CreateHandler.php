<?php
namespace Leep\AdminBundle\Business\CryosaveSample;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->timestamp = new \DateTime();
        $model->container = $this->container;

        return $model;
    }

    public function buildForm($builder) {    
        $mapping = $this->container->get('easy_mapping');
            
        // Order information
        $builder->add('sectionOrderInformation',          'section', array(
            'label' => 'Sample information',
            'property_path' => false
        ));
        $builder->add('timestamp', 'datetimepicker', array(
            'label' => 'Timestamp',
            'required' => false
        ));        
        $builder->add('sampleCode', 'text', array(
            'label' => 'Sample code',
            'required' => true
        ));        
        $builder->add('idSex', 'choice', array(
            'label' => 'Sex',
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Sex'),
            'required' => false
        ));        
        $builder->add('birthday', 'datepicker', array(
            'label' => 'Birthday',
            'required' => false
        ));        
        $builder->add('idEthnicity', 'choice', array(
            'label' => 'Ethnicity',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Ethnicity'),
            'required' => false
        ));        
        $builder->add('idOrder', 'choice', array(
            'label' => 'Order',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_Order'),
            'required' => false
        ));        
        $builder->add('idLanguage', 'choice', array(
            'label' => 'Language',
            'choices'  => $mapping->getMapping('LeepAdmin_Language_List'),
            'required' => false
        ));        
        $builder->add('idSampleOrigin', 'choice', array(
            'label' => 'Sample origin',
            'choices'  => $mapping->getMapping('LeepAdmin_CryosaveSample_SampleOrigin'),
            'required' => false
        ));        
        $builder->add('dateOfSample', 'datepicker', array(
            'label' => 'Date of sample collection',
            'required' => false
        ));        

        // Status
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));
        $builder->add('statusList', 'container_collection', array(
            'type' => new FormType\AppCryosaveSampleStatusRow($this->container),
            'label' => 'Status record',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();    
        $common = $this->container->get('leep_admin.helper.common');

        $sample = new Entity\CryosaveSample();
        $sample->setIdLaboratory($common->getIdLaboratory());
        $sample->setTimestamp($model->timestamp);
        $sample->setSampleCode($model->sampleCode);
        $sample->setIdSex($model->idSex);
        $sample->setBirthday($model->birthday);
        $sample->setIdEthnicity($model->idEthnicity);
        $sample->setIdOrder($model->idOrder);
        $sample->setIdLanguage($model->idLanguage);
        $sample->setIdSampleOrigin($model->idSampleOrigin);
        $sample->setDateOfSample($model->dateOfSample);
        $em->persist($sample);
        $em->flush();

        // Update status
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\CryosaveSampleStatus();
            $status->setIdCryosaveSample($sample->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setNotes($statusRow->notes);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);
            
            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $sample->setCurrentStatus($currentStatus);
        $sample->setCurrentStatusDate($currentStatusDate);

        $em->flush();

        // Update list
        Utils::updateStatusList($this->container, $sample);

        parent::onSuccess();
    }
}