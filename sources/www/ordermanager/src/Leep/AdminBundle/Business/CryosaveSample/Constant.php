<?php 
namespace Leep\AdminBundle\Business\CryosaveSample;

class Constant {
    // Sex
    const SEX_MALE         = 1;
    const SEX_FEMALE       = 2;
    public static function getSex() {
        return array(
            self::SEX_MALE     => 'Male',
            self::SEX_FEMALE   => 'Female'
        );
    }
    public static function getSexCode() {
        return array(
            self::SEX_MALE   => 'M',
            self::SEX_FEMALE => 'F'
        );
    }

    // Ethnicity
    const ETHNICITY_CAUC         = 1;
    const ETHNICITY_ASIA         = 2;
    const ETHNICITY_AFRI         = 3;
    const ETHNICITY_HISP         = 4;
    const ETHNICITY_ARAB         = 5;
    const ETHNICITY_OTHE         = 6;
    public static function getEthnicity() {
        return array(
            self::ETHNICITY_CAUC     => 'Caucasian',
            self::ETHNICITY_ASIA     => 'Asian',
            self::ETHNICITY_AFRI     => 'African',
            self::ETHNICITY_HISP     => 'Hispanic',
            self::ETHNICITY_ARAB     => 'Arabs',
            self::ETHNICITY_OTHE     => 'Other'
        );
    }
    public static function getEthnicityCode() {
        return array(
            self::ETHNICITY_CAUC     => 'CAUC',
            self::ETHNICITY_ASIA     => 'ASIA',
            self::ETHNICITY_AFRI     => 'AFRI',
            self::ETHNICITY_HISP     => 'HISP',
            self::ETHNICITY_ARAB     => 'ARAB',
            self::ETHNICITY_OTHE     => 'OTHE'
        );
    }

    // Order 
    const ORDER_BABY111         = 1;
    const ORDER_MILKOME         = 2;
    const ORDER_BLOODOM         = 3;
    public static function getOrder() {
        return array(
            self::ORDER_BABY111   => 'BABY111',
            self::ORDER_MILKOME   => 'MILKOME',
            self::ORDER_BLOODOM   => 'BLOODOM'
        );
    }
    public static function getOrderCode() {
        return array(
            self::ORDER_BABY111   => 'BABY111',
            self::ORDER_MILKOME   => 'MILKOME',
            self::ORDER_BLOODOM   => 'BLOODOM'
        );
    }
    public static function getOrderReport($idOrder, $config) {
        $arr = array(
            self::ORDER_BABY111   => $config->getIdCryosaveOrderBaby111(),
            self::ORDER_MILKOME   => $config->getIdCryosaveOrderMilkome(),
            self::ORDER_BLOODOM   => $config->getIdCryosaveOrderBloodom(),
        );
        return isset($arr[$idOrder]) ? $arr[$idOrder] : 0;
    }

    // Language 
    const LANGUAGE_EN         = 1;
    const LANGUAGE_AR         = 2;
    const LANGUAGE_IT         = 3;
    public static function getLanguage() {
        return array(
            self::LANGUAGE_EN   => 'English',
            self::LANGUAGE_AR   => 'Arabic',
            self::LANGUAGE_IT   => 'Italian'
        );
    }
    public static function getLanguageCode() {
        return array(
            self::LANGUAGE_EN   => 'EN',
            self::LANGUAGE_AR   => 'AR',
            self::LANGUAGE_IT   => 'IT'
        );
    }
    public static function getLanguageName() {
        return array(
            self::LANGUAGE_EN   => 'ENGLISH',
            self::LANGUAGE_AR   => 'ARABIC',
            self::LANGUAGE_IT   => 'ITALIAN'
        );
    }

    // Sample origin 
    const SAMPLE_ORIGIN_EU         = 1;
    const SAMPLE_ORIGIN_PE         = 2;
    const SAMPLE_ORIGIN_AF         = 3;
    public static function getSampleOrigin() {
        return array(
            self::SAMPLE_ORIGIN_EU   => 'Europe',
            self::SAMPLE_ORIGIN_PE   => 'Portugal and Espana',
            self::SAMPLE_ORIGIN_AF   => 'Africa'
        );
    }
    public static function getSampleOriginCode() {
        return array(
            self::SAMPLE_ORIGIN_EU   => 'EU',
            self::SAMPLE_ORIGIN_PE   => 'PE',
            self::SAMPLE_ORIGIN_AF   => 'AF'
        );
    }

    // Bulk-edit actions
    const BULK_EDIT_ACTION_UPDATE_STATUS    = 1;
    const BULK_EDIT_ACTION_BUILD_REPORT     = 2;
    const BULK_EDIT_ACTION_UPLOAD_REPORT    = 3;
    public static function getBulkEditActions() {
        return array(
            self::BULK_EDIT_ACTION_UPDATE_STATUS    => 'Update status',
            self::BULK_EDIT_ACTION_BUILD_REPORT     => 'Build report, then update status (if specified)',
            self::BULK_EDIT_ACTION_UPLOAD_REPORT    => 'Upload report, then update status (if specified)',
        );
    }

    public static function getExternalLabBulkEditActions() {
        return array(
            self::BULK_EDIT_ACTION_UPDATE_STATUS    => 'Update status'
        );
    }
}