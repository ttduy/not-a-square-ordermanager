<?php
namespace Leep\AdminBundle\Business\Rating;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class Utils {
    public static function filter($container, $from, $to, $rules, $type = Business\Rating\Constant::RATING_TYPE_DISTRIBUTION_CHANNEL, $isNoRating = false, $distributionChannels = [], $partners = []) {
        $emConfig = $container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('IF', 'Leep\AdminBundle\Business\Customer\DoctrineExt\IfElse');

        $mapping = $container->get('easy_mapping');

        $arr = array();
        $query = $container->get('doctrine')->getEntityManager()->createQueryBuilder();

        switch ($type) {
            case Business\Rating\Constant::RATING_TYPE_DISTRIBUTION_CHANNEL:
                $query->select('
                                    SUM(IF(c.id IS NOT NULL, 1, 0)) AS totalOfOrders,
                                    d.id,
                                    d.rating,
                                    d.distributionchannel AS name
                                ')
                    ->from('AppDatabaseMainBundle:DistributionChannels', 'd')
                    ->leftJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'd.id = c.distributionchannelid AND c.dateordered >= :from AND c.dateordered <= :to');
                if (!empty($distributionChannels)) {
                    $query = $query->where('d.id IN ('.implode(',', $distributionChannels).')');
                }

                if (!empty($partners)) {
                    $query->andWhere('d.partnerid IN (:partners)')
                        ->setParameter('partners', $partners);
                }

                $query = $query->groupBy('d.id');
                if ($isNoRating) {
                    $query->andWhere('(d.rating IS NULL) OR (d.rating = 0)');
                }

                break;

            case Business\Rating\Constant::RATING_TYPE_PARTNER:
                $query->select('
                                    SUM(IF(c.id IS NOT NULL, 1, 0)) AS totalOfOrders,
                                    p.id AS id,
                                    p.rating,
                                    p.partner AS name
                                ')
                    ->from('AppDatabaseMainBundle:Partner', 'p')
                    ->leftJoin('AppDatabaseMainBundle:DistributionChannels', 'd', 'WITH', 'd.partnerid = p.id')
                    ->leftJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'd.id = c.distributionchannelid AND c.dateordered >= :from AND c.dateordered <= :to')
                    ->groupBy('p.id');
                if ($isNoRating) {
                    $query->andWhere('(p.rating IS NULL) OR (p.rating = 0)');
                }
                break;

            case Business\Rating\Constant::RATING_TYPE_ACQUISITEUR:
                $query->select('
                                    SUM(IF(c.id IS NOT NULL, 1, 0)) AS totalOfOrders,
                                    a.id AS id,
                                    a.rating,
                                    a.acquisiteur AS name
                                ')
                    ->from('AppDatabaseMainBundle:Acquisiteur', 'a')
                    ->leftJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', '(c.idAcquisiteur1 = a.id OR c.idAcquisiteur2 = a.id OR c.idAcquisiteur3 = a.id) AND c.dateordered >= :from AND c.dateordered <= :to')
                    ->groupBy('a.id');
                if ($isNoRating) {
                    $query->andWhere('(a.rating IS NULL) OR (a.rating = 0)');
                }
            break;
        }


        $result = $query->setParameter('from', $from)
            ->setParameter('to', $to)
            ->orderBy('totalOfOrders', 'DESC')
            ->getQuery()
            ->execute();

        if ($result) {
            foreach ($result as $entry) {
                $rating = self::getRatingLevel($entry['totalOfOrders'], $rules);
                $arr[] = array(
                    'id'                        => $entry['id'],
                    'name'                      => $entry['name'],
                    'totalOfOrders'             => intval($entry['totalOfOrders']),
                    'currentRating'             => $entry['rating'],
                    'currentRatingLabel'        => $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $entry['rating']),
                    'rating'                    => $rating,
                    'ratingLabel'               => $mapping->getMappingTitle('LeepAdmin_Rating_Levels', $rating)
                );
            }
        }

        return $arr;
    }

    public static function getRatingLevel($totalOfOrders, $rules) {
        $ratingLevel = null;
        foreach ($rules as $level => $rule) {
            if ($totalOfOrders >= $rule['min'] && $totalOfOrders <= $rule['max']) {
                $ratingLevel = $level;
                break;
            }
        }

        return $ratingLevel;
    }

    public static function getDefaultRating($container, $type) {
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $mapping = $container->get('easy_mapping');
        $ratingMap = $mapping->getMapping('LeepAdmin_Rating_Levels');
        $ratingRevertMap = array();
        foreach ($ratingMap as $k => $v) {
            $ratingRevertMap[$v] = $k;
        }

        $defaultRatingConfig = '';
        switch ($type) {
            case Constant::RATING_TYPE_DISTRIBUTION_CHANNEL:
                $defaultRatingConfig = $config->getDefaultRatingDistributionChannel();
                break;
            case Constant::RATING_TYPE_PARTNER:
                $defaultRatingConfig = $config->getDefaultRatingPartner();
                break;
            case Constant::RATING_TYPE_ACQUISITEUR:
                $defaultRatingConfig = $config->getDefaultRatingAcquisiteur();
                break;
        }

        $defaultRating = array();
        $rows = explode("\n", $defaultRatingConfig);
        foreach ($rows as $r) {
            $arr = explode(':', $r);
            if (count($arr) == 2) {
                if (isset($ratingRevertMap[trim($arr[0])])) {
                    $idRating = $ratingRevertMap[trim($arr[0])];
                    $period = explode('-', $arr[1]);
                    if (count($period) == 2) {
                        $defaultRating[$idRating] = array(
                            'from' => intval($period[0]),
                            'to'   => intval($period[1])
                        );
                    }
                }
            }
        }

        return $defaultRating;
    }
}