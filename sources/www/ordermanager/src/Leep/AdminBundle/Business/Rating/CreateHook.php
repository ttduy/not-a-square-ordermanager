<?php
namespace Leep\AdminBundle\Business\Rating;

use Easy\ModuleBundle\Module\AbstractHook;
use Leep\AdminBundle\Business;

class CreateHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
    	$mapping = $controller->get('easy_mapping');
        $mgr = $controller->get('easy_module.manager');
        $type = $controller->get('request')->get('type', null);
        
        $nameMap = Constant::getRatingTypeEntityNames();
        $data['ratingEntity'] = $nameMap[$type];
        $createHandler = $controller->get('leep_admin.rating.business.create_handler');
        $data['ratingResult'] = $createHandler->ratingResult;

        $data['ratingList'] = $mapping->getMapping('LeepAdmin_Rating_Levels');
        $data['ratingLevelMinPrefix'] = $createHandler->ratingLevelMinPrefix;
        $data['ratingLevelMaxPrefix'] = $createHandler->ratingLevelMaxPrefix;

    }
}
