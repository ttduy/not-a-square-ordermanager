<?php
namespace Leep\AdminBundle\Business\Rating;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public $ratingList = array();
    public $ratingLevelMinPrefix = 'ratingLevelMin_';
    public $ratingLevelMaxPrefix = 'ratingLevelMax_';
    public $ratingRuleDefaultMinValue = -999999;
    public $ratingRuleDefaultMaxValue = 999999;
    public $ratingResult = array();

    public function getDefaultFormModel() {
        $mapping = $this->container->get('easy_mapping');
        $type = $this->container->get('request')->get('type', Business\Rating\Constant::RATING_TYPE_DISTRIBUTION_CHANNEL);
        $today = new \DateTime();
        $lastSixMonthDate = new \DateTime();
        $lastSixMonthDate->sub(new \DateInterval('P6M'));

        $arrData = array(
                'startDate'         => $lastSixMonthDate,
                'endDate'           => $today,
                'createdAt'         => $today,
                'filteredResult'    => null,
                'type'              => $type
            );

        $this->ratingList = $mapping->getMapping('LeepAdmin_Rating_Levels');
        $defaultRating = Utils::getDefaultRating($this->container, $type);
        foreach ($this->ratingList as $ratingkey => $ratingLabel) {
            $arrData[$this->ratingLevelMinPrefix . $ratingkey] = isset($defaultRating[$ratingkey]) ? $defaultRating[$ratingkey]['from'] : '';
            $arrData[$this->ratingLevelMaxPrefix . $ratingkey] = isset($defaultRating[$ratingkey]) ? $defaultRating[$ratingkey]['to'] : '';
        }

        return $arrData;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('type', 'hidden');
        $builder->add('filteredResult', 'hidden');

        $builder->add('startDate', 'datepicker', array(
            'label' => 'startDate',
            'required' => false
        ));

        $builder->add('endDate', 'datepicker', array(
            'label' => 'to',
            'required' => false
        ));

        $builder->add('createdAt', 'datepicker', array(
            'label' => 'Rating Date',
            'required' => false
        ));

        $builder->add('partners', 'choice', array(
            'label'     => 'Partner',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_Partner_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('distributionChannel', 'choice', array(
            'label'     => 'Distribution Channel',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'multiple'  => 'multiple',
            'attr'      => array('style' => 'height:300px')
        ));

        $builder->add('isNoRating', 'checkbox', array(
            'label' => 'Show No-Rating only?',
            'required' => false
        ));

        foreach ($this->ratingList as $ratingkey => $ratingLabel) {
            $builder->add($this->ratingLevelMinPrefix . $ratingkey, 'text', array(
                'label'     => 'From',
                'required'  => false,
                'attr'      => array('style' => 'width: 200px')
            ));

            $builder->add($this->ratingLevelMaxPrefix . $ratingkey, 'text', array(
                'label'     => 'To',
                'required'  => false,
                'attr'      => array('style' => 'width: 200px')
            ));
        }

        $builder->add('idMode', 'choice', array(
            'label'        => 'Mode',
            'required'     => false,
            'choices'      => array(
                'preview'     => 'Preview',
                'apply'       => 'Apply now'
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $idMode = $model['idMode'];

        $ratingRules = array();
        foreach ($model as $k => $v) {
            if (strpos($k, $this->ratingLevelMinPrefix) === 0) {
                $arr = explode('_', $k);
                if (count($arr) == 2) {
                    $idRating = intval($arr[1]);
                    $ratingRules[$idRating]['min'] = $v;
                }
            }
            if (strpos($k, $this->ratingLevelMaxPrefix) === 0) {
                $arr = explode('_', $k);
                if (count($arr) == 2) {
                    $idRating = intval($arr[1]);
                    $ratingRules[$idRating]['max'] = $v;
                }
            }
        }
        $ratingResult = Utils::filter(
            $this->container,
            $model['startDate'],
            $model['endDate'],
            $ratingRules,
            $model['type'],
            $model['isNoRating'],
            $model['distributionChannel'],
            $model['partners']
        );

        $this->ratingResult = $ratingResult;

        if ($idMode == 'apply') {
            $rating = new Entity\Rating();
            $rating->setStartDate($model['startDate']);
            $rating->setEndDate($model['endDate']);
            $rating->setRules(json_encode($ratingRules));
            $rating->setCreatedAt($model['createdAt']);
            $em->persist($rating);
            $em->flush();

            $type = $model['type'];
            foreach ($ratingResult as $entry) {
                $typeName = Business\Rating\Constant::getRatingTypeEntityNames()[$type];
                $targetObjectTable = $typeName . ($typeName == 'DistributionChannel' ? 's' : '');
                $targetFieldSetMethod = 'set' . ucfirst('id' . $typeName);

                // update rating
                $instance = $em->getRepository('AppDatabaseMainBundle:' . $targetObjectTable)->findOneById($entry['id']);

                if ($instance) {
                    $ratingHistory = clone Business\Rating\Constant::getRatingHistoryEntityInstances()[$type];
                    $ratingHistory->setIdRating($rating->getId());
                    $ratingHistory->$targetFieldSetMethod($entry['id']);
                    $ratingHistory->setNewRating($entry['rating']);
                    $ratingHistory->setTotalOfOrders($entry['totalOfOrders']);
                    $ratingHistory->setCreatedAt($model['createdAt']);
                    $em->persist($ratingHistory);

                    // update rating
                    $instance->setRating($entry['rating']);
                }
            }
            $em->flush();


            $this->ratingResult = Utils::filter(
                $this->container,
                $model['startDate'],
                $model['endDate'],
                $ratingRules,
                $model['type'],
                $model['isNoRating'],
                $model['distributionChannel'],
                $model['partners']
            );

            $this->messages = array('The rating has been updated!');
        }
    }
}