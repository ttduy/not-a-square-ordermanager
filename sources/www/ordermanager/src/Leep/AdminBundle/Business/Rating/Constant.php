<?php 
namespace Leep\AdminBundle\Business\Rating;

use App\Database\MainBundle\Entity;

class Constant {
    const RATING_TYPE_DISTRIBUTION_CHANNEL   = 10;
    const RATING_TYPE_PARTNER                = 20;
    const RATING_TYPE_ACQUISITEUR            = 30;

    public static function getRatingTypes() {
        return array(
            self::RATING_TYPE_DISTRIBUTION_CHANNEL      => 'Distribution Channel',
            self::RATING_TYPE_PARTNER                   => 'Partner',
            self::RATING_TYPE_ACQUISITEUR               => 'Acquisiteur'
        );
    }

    public static function getRatingHistoryTables() {
        return array(
            self::RATING_TYPE_DISTRIBUTION_CHANNEL      => 'DistributionChannelRatingHistory',
            self::RATING_TYPE_PARTNER                   => 'PartnerRatingHistory',
            self::RATING_TYPE_ACQUISITEUR               => 'AcquisiteurRatingHistory'
        );
    }

    public static function getRatingTypeEntityNames() {
        return array(
            self::RATING_TYPE_DISTRIBUTION_CHANNEL      => 'DistributionChannel',
            self::RATING_TYPE_PARTNER                   => 'Partner',
            self::RATING_TYPE_ACQUISITEUR               => 'Acquisiteur'
        );
    }

    public static function getRatingHistoryEntityInstances() {
        return array(
            self::RATING_TYPE_DISTRIBUTION_CHANNEL      => new Entity\DistributionChannelRatingHistory(),
            self::RATING_TYPE_PARTNER                   => new Entity\PartnerRatingHistory(),
            self::RATING_TYPE_ACQUISITEUR               => new Entity\AcquisiteurRatingHistory()
        );
    }

    public static function getRatingFilterUrl($controller) {
        $mgr = $controller->get('easy_module.manager');
        $mapping = $controller->get('easy_mapping');
        return array(
            self::RATING_TYPE_DISTRIBUTION_CHANNEL      => $mgr->getUrl('leep_admin', 'rating', 'feature', 'filterDCRating'),
            self::RATING_TYPE_PARTNER                   => $mgr->getUrl('leep_admin', 'rating', 'feature', 'filterPartnerRating'),
            self::RATING_TYPE_ACQUISITEUR               => $mgr->getUrl('leep_admin', 'rating', 'feature', 'filterAcquisiteurRating')
        );
    }
}
