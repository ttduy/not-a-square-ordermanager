<?php
namespace Leep\AdminBundle\Business\Rating;

class CreateModel {
    public $from;
    public $to;
    public $rules;
    public $createdAt;
    public $isNoRating;
    public $distributionChannel;
    public $partners;
}