<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $todayTs = 0;
    public function __construct($container) {
        parent::__construct($container);

        $today = new \DateTime();
        $this->todayTs = $today->getTimestamp();
    }

    public function getColumnMapping() {
        return array('orderNumber', 'customerName', 'supplementDaysOrder', 'daysLeft', 'currentStatus', 'action');
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Order Number',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Customer Name',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Days Ordered',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Days Left',       'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Status',          'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',          'width' => '20%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('AppAdmin_RoomGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:GenoNutriMe', 'p');

        $emConfig = $this->container->get('doctrine')->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');

        if (trim($this->filters->orderNumber) != '') {
            $queryBuilder->andWhere('p.orderNumber LIKE :orderNumber')
                ->setParameter('orderNumber', '%'.trim($this->filters->orderNumber).'%');
        }
        if (trim($this->filters->customerName) != '') {
            $queryBuilder->andWhere('p.customerName LIKE :customerName')
                ->setParameter('customerName', '%'.trim($this->filters->customerName).'%');
        }
        if (intval($this->filters->currentStatus) != 0) {
            $queryBuilder->andWhere('p.currentStatus = :currentStatus')
                ->setParameter('currentStatus', intval($this->filters->currentStatus));
        }
        if (!empty($this->filters->mustBeStatus) || !empty($this->filters->mustNotBeStatus)) {
            foreach ($this->filters->mustBeStatus as $mustBeStatus) {
                $queryBuilder->andWhere('FIND_IN_SET('.$mustBeStatus.', p.statusList) > 0');
            }
            foreach ($this->filters->mustNotBeStatus as $mustNotBeStatus) {
                $queryBuilder->andWhere('FIND_IN_SET('.$mustNotBeStatus.', p.statusList) = 0');
            }
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('genoNutrimeModify')) {
            if ($row->getActualUnit() == 0) {
                $builder->addButton('Detail', $mgr->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'startProduction', array('id' => $row->getId())), 'button-redo');
            }

            if ($row->getIdCustomer() > 0) {
                $builder->addPopupButton('View reports', $mgr->getUrl('leep_admin', 'customer_report', 'grid', 'list', array('id' => $row->getIdCustomer())), 'button-pdf');
            }

            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'geno_nutri_me', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'geno_nutri_me', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellDaysLeft($row) {
        $creationDateTs = $row->getCreationDate()->getTimestamp();

        $days = round(($this->todayTs - $creationDateTs) / 86400);
        $daysLeft = $row->getSupplementDaysOrder() - $days;

        if ($daysLeft <= 0) {
            return "<b style='color: red'>".$daysLeft.'</b>';
        }
        return $daysLeft;
    }

    private function getAge($date) {
        $age = '';
        if ($date) {
            $age = intval(($this->todayTs - $date->getTimestamp()) / 86400);
        }
        return $age;
    }

    public function buildCellCurrentStatus($row) {
        $status = $this->container->get('easy_mapping')->getMappingTitle('LeepAdmin_GenoNutriMe_Status', $row->getCurrentStatus());

        $age = $this->getAge($row->getCurrentStatusDate());
        return (($age === '') ? '': "(${age}d) ").$status;
    }
}
