<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoNutriMe', 'app_main')->findOneById($id);
    }

    private function getStockInUse($id){
        $em = $this->container->get('doctrine')->getEntityManager();
        $stocks = $em->getRepository("AppDatabaseMainBundle:GenoNutriMeSnapshot")->findBy(
            ['nutriMeId' => $id]);
        $stocksInUse = [];

        foreach ($stocks as $stock) {
            $startTh = $stock->getStartTh();
            $currentStock = [
                'pelletName' => $stock->getPelletName(),
                'pelletType' => $stock->getPelletType(),
                'pelletAmount' => $stock->getTypeAmount(),
                'pelletShipment' => $stock->getPelletShipment(),
                'pelletActiveIngredientName' => $stock->getPelletActiveIngredientName(),
            ];

            if (array_key_exists(strval($startTh), $stocksInUse)) {
               array_push($stocksInUse[$startTh]['stocks'], $currentStock);
            }
            else{
                // get user name
                $userId = $stock->getWebUserId();
                $userName_bonus = "";
                if ($userId) {
                    $query = $em->createQueryBuilder();
                    $query->select('p.username')
                            ->from("AppDatabaseMainBundle:WebUsers", 'p')
                            ->andWhere('p.id = :userId')
                            ->setParameter('userId', $userId);
                    $userName = $query->getQuery()->getSingleScalarResult();
                    $userName_bonus = ", by {$userName}";
                }

                // get start process time
                $startedTime = $stock->getCreatedDate();
                $startedTime_str = $startedTime->format("l jS \of F Y h:i:s A");
                $orderStr = "";
                switch ($startTh) {
                    case 1:
                        $orderStr .= "First started at: "."{$startedTime_str}{$userName_bonus}";
                        break;
                    case 2:
                        $orderStr .= "Second started at: "."{$startedTime_str}{$userName_bonus}";
                        break;
                    case 3:
                        $orderStr .= "Third started at: "."{$startedTime_str}{$userName_bonus}";
                        break;
                    default:
                        $orderStr .= "{$startTh}th started at: "."{$startedTime_str}{$userName_bonus}";
                        break;
                }
                $tmp = [];
                array_push($tmp, $currentStock);
                $stocksInUse[strval($startTh)] = ['order' => $orderStr, 'stocks' => $tmp];
            }
        }
        return $stocksInUse;
    }

    public function convertToFormModel($entity) {
        $em = $this->container->get('doctrine')->getEntityManager();
        $formatter = $this->container->get('leep_admin.helper.formatter');
        $codeParser = $this->container->get('leep_admin.geno_nutri_me.business.code_parser');
        $model = new EditModel();
        $model->barcode = $entity->getBarcode();

        $d = $codeParser->parse($model->barcode);
        $d['data']['creationDate'] = $entity->getCreationDate();
        $d['data']['id'] = $entity->getId();
        $d['data']['actualUnits'] = $entity->getActualUnit();
        $d['data']['processedOn'] = $formatter->format($entity->getProcessedOn(), 'datetime');
        $d['data']['processedBy'] = $formatter->format($entity->getProcessedBy(), 'mapping', 'LeepAdmin_WebUser_List');
        $d['data']['currentLots'] = @unserialize($entity->getCurrentLots());
        $d['data']['stocksInUse'] = $this->getStockInUse($entity->getId());
        $model->nutriMeParsed = $d;

        // Status
        $model->statusList = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:GenoNutriMeStatus', 'p')
            ->andWhere('p.idNutriMe = :idNutriMe')
            ->setParameter('idNutriMe', $entity->getId())
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $status) {
            $m = new FormType\AppGenoNutriMeStatusModel();
            $m->statusDate = $status->getStatusDate();
            $m->status = $status->getStatus();
            $model->statusList[] = $m;
        }
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('barcode', 'textarea', array(
            'label'    => 'Code',
            'attr'     => array(
                'rows'        => 30,
                'cols'        => 70
            ),
            'required' => true,
        ));

        // Status
        $builder->add('sectionStatus', 'section', array(
            'label' => 'Status',
            'property_path' => false
        ));

        $builder->add('statusList', 'container_collection', array(
            'type' => new FormType\AppGenoNutriMeStatusRow($this->container),
            'label' => 'Status record',
            'required' => false
        ));

        $builder->add('nutriMeParsed', 'app_geno_nutri_me_parsed', array(
            'label' => 'Code Parsed'
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        $codeParser = $this->container->get('leep_admin.geno_nutri_me.business.code_parser');
        $d = $codeParser->parse($model->barcode);

        if ($d['error'] != '') {
            $this->errors = array($d['error']);
            return false;
        }
        $d = $d['data'];

        $id = $this->container->get('request')->get('id', 0);
        $n = $em->getRepository('AppDatabaseMainBundle:GenoNutriMe')->findOneByOrderNumber($d['orderNumber']);
        if ($n && $n->getId() != $id) {
            $this->errors = array('Order number '.$d['orderNumber'].' is already existed');
            return false;
        }

        $this->entity->setBarcode($model->barcode);
        $this->entity->setOrderNumber($d['orderNumber']);
        $this->entity->setCustomerName($d['customerName']);
        $this->entity->setSupplementDaysOrder($d['supplementDaysOrder']);


        // Update status
        $filters = array('idNutriMe' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:GenoNutriMeStatus', $filters);

        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\GenoNutriMeStatus();
            $status->setIdNutriMe($this->entity->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $this->entity->setCurrentStatus($currentStatus);
        $this->entity->setCurrentStatusDate($currentStatusDate);
        $em->flush();

        Utils::updateStatusList($em, $this->entity->getId());

        parent::onSuccess();

        $this->reloadForm();
    }
}
