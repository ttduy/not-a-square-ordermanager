<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

use Leep\AdminBundle\Business\Common\RealDeleteHandler as BaseDeleteHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DeleteHandler extends BaseDeleteHandler {
    public function __construct($container, $repository) {
        parent::__construct($container, $repository);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        // Remove status
        $statusList = $em->getRepository('AppDatabaseMainBundle:GenoNutriMeStatus')->findByIdNutriMe($id);
        foreach ($statusList as $status) {
            $em->remove($status);
        }

        $em->flush();

        parent::execute();
    }
}
