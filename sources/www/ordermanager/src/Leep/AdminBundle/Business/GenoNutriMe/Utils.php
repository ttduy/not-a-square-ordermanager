<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

class Utils {
    public static function updateStatusList($em, $id) {
        $nutriMe = $em->getRepository('AppDatabaseMainBundle:GenoNutriMe')->findOneById($id);
        $status = array();
        $results = $em->getRepository('AppDatabaseMainBundle:GenoNutriMeStatus')->findByIdNutriMe($id);
        foreach ($results as $r) {
            $status[] = $r->getStatus();
        }

        $nutriMe->setStatusList(implode(',', $status));
        $em->persist($nutriMe);
        $em->flush();
    }
}