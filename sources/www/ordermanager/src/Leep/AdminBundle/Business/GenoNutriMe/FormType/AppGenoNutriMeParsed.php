<?php
namespace Leep\AdminBundle\Business\GenoNutriMe\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppGenoNutriMeParsed extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return [];
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_geno_nutri_me_parsed';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $formatter = $this->container->get('leep_admin.helper.formatter');
        $mgr = $this->container->get('easy_module.manager');

        $formData = $form->getData();
        if ($formData['error'] != '') {
            $view->vars['error'] = $formData['error'];
        }
        else {
            $data = $formData['data'];
            $view->vars['startProductionLink'] = $mgr->getUrl('leep_admin', 'geno_nutri_me', 'feature', 'startProduction', array('id' => $data['id']));
            $view->vars['info'] = array(
                array(
                    'label' => 'Order Number',
                    'value' => $data['orderNumber']
                ),
                array(
                    'label' => 'Date of creation',
                    'value' => $formatter->format($data['creationDate'], 'date')
                ),
                array(
                    'label' => 'Supplement days ordered',
                    'value' => $data['supplementDaysOrder'].'d'
                ),
                array(
                    'label' => 'Product',
                    'value' => $data['product']
                ),
                array(
                    'label' => 'Language',
                    'value' => $data['language']
                ),
                array(
                    'label' => 'Name',
                    'value' => $data['customerName']
                ),
                array(
                    'label' => 'Street and number',
                    'value' => $data['street']
                ),
                array(
                    'label' => 'Post code and city',
                    'value' => $data['city']
                ),
                array(
                    'label' => 'Country',
                    'value' => $data['country']
                ),
                array(
                    'label' => '&nbsp;',
                    'value' => '',
                ),
                array(
                    'label' => 'Target Amount',
                    'value' => $data['total']
                ),
                array(
                    'label' => 'Total Pack',
                    'value' => $data['numPack']
                ),
            );

            $view->vars['drugs'] = $data['drugs'];
        }

        $data = $formData['data'];

        $view->vars['currentLots'] = $data['currentLots'];
        $view->vars['stocksInUse'] = $data['stocksInUse'];

        if ($data['actualUnits'] > 0) {
            $view->vars['isMade'] = true;

            if ($data['total'] > 0) {
                $percentage = $data['actualUnits'] * 100 /  $data['total'];
                $divergence = $percentage - 100;
            }
            else {
                $percentage = 0;
                $divergence = 'INF';
            }


            $view->vars['history'] = array(
                array(
                    'label'   => 'Processed on',
                    'value'   => $data['processedOn']
                ),
                array(
                    'label'   => 'Processed by',
                    'value'   => $data['processedBy']
                ),
                array(
                    'label'   => 'Target units',
                    'value'   => $data['total']
                ),
                array(
                    'label'   => 'Actual units',
                    'value'   => $data['actualUnits']
                ),
                array(
                    'label'   => 'Divergence',
                    'value'   => ($divergence > 0 ? '+' : '').number_format($divergence, 2).'%'
                )
            );
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');
    }
}