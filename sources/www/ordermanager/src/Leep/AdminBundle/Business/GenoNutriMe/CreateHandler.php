<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('barcode', 'textarea', array(
            'label'    => 'Code',
            'attr'     => array(
                'rows'        => 30,
                'cols'        => 70
            ),
            'required' => true,
        ));

        // Status
        $builder->add('sectionStatus',          'section', array(
            'label' => 'Status',
            'property_path' => false
        ));

        $builder->add('statusList', 'container_collection', array(
            'type' => new FormType\AppGenoNutriMeStatusRow($this->container),
            'label' => 'Status record',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $today = new \DateTime();

        // Parse code
        $codeParser = $this->container->get('leep_admin.geno_nutri_me.business.code_parser');
        $d = $codeParser->parse($model->barcode);
        if ($d['error'] != '') {
            $this->errors = array($d['error']);
            return false;
        }
        $d = $d['data'];

        // Check existings
        $n = $em->getRepository('AppDatabaseMainBundle:GenoNutriMe')->findOneByOrderNumber($d['orderNumber']);
        if ($n) {
            $this->errors = array('Order number '.$d['orderNumber'].' is already existed in NutriME List');
            return false;
        }

        // Resolve order number
        $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneByordernumber($d['orderNumber']);
        if (!$order) {
            $this->errors = array('Order number '.$d['orderNumber'].' is not existed');
            return false;
        }

        // Insert
        $nutriMe = new Entity\GenoNutriMe();
        $nutriMe->setBarcode($model->barcode);
        $nutriMe->setIdCustomer($order->getId());
        $nutriMe->setCreationDate($today);
        $nutriMe->setOrderNumber($d['orderNumber']);
        $nutriMe->setCustomerName($d['customerName']);
        $nutriMe->setSupplementDaysOrder($d['supplementDaysOrder']);
        $em->persist($nutriMe);
        $em->flush();

        // Update status
        $sortOrder = 1;
        $currentStatus = 0;
        $currentStatusDate = new \DateTime();
        $currentStatusIdWebUser = 0;
        foreach ($model->statusList as $statusRow) {
            if (empty($statusRow)) continue;
            $status = new Entity\GenoNutriMeStatus();
            $status->setIdNutriMe($nutriMe->getId());
            $status->setStatusDate($statusRow->statusDate);
            $status->setStatus($statusRow->status);
            $status->setSortOrder($sortOrder++);
            $em->persist($status);

            $currentStatus = $statusRow->status;
            $currentStatusDate = $statusRow->statusDate;
        }
        $nutriMe->setCurrentStatus($currentStatus);
        $nutriMe->setCurrentStatusDate($currentStatusDate);
        $em->flush();

        Utils::updateStatusList($em, $nutriMe->getId());

        $this->newId = $nutriMe->getId();

        parent::onSuccess();
    }
}