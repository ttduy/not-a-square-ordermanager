<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

class Constant {
    const STATUS_DETAILS_REGISTERED    = 10;
    const STATUS_CREATED               = 20;
    const STATUS_PRODUCED              = 30;
    const STATUS_SHIPPED               = 40;
    const STATUS_REORDER_REQUEST_SENT  = 50;
    const STATUS_ON_HOLD               = 60;
    const STATUS_DELETED               = 70;
    public static function getStatus() {
        return array(
            self::STATUS_DETAILS_REGISTERED     => 'Details registered',
            self::STATUS_CREATED                => 'Created',
            self::STATUS_PRODUCED               => 'Produced',
            self::STATUS_SHIPPED                => 'Shipped',
            self::STATUS_REORDER_REQUEST_SENT   => 'Reorder request sent',
            self::STATUS_ON_HOLD                => 'On hold',
            self::STATUS_DELETED                => 'Deleted',
        );
    }
}