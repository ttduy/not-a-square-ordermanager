<?php
namespace Leep\AdminBundle\Business\GenoNutriMe;

class CodeParser {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function parse($code) {
        $data = array(
            'data'  => array(),
            'error' => false
        );
        //$data['orderNumber'] = 'Test';
        //$data['customerName'] = 'Test';
        //$data['supplementDaysOrder'] = '27';

        $a = explode('***', $code);
        if (count($a) != 2) {
            $data['error'] = "There should be one *** in the code";
            return $data;
        }

        ////////////////////////////////////
        $part_1 = [];
        $part_2 = [];
        // check if this code use the old format
        if (substr_count($code, '$') > substr_count($code, '/')) {
            $part_1 = explode('$', trim($a[0]));
            $part_2 = explode('$', trim($a[1]));
        } else{
            $part_1 = explode('/', trim($a[0]));
            $part_2 = explode('/', trim($a[1]));
        }

        // x. First parts
        $p = $part_1;
        if (count($p) != 10) {
            $data['error'] = "Order information part should contain exactly 10 tokens";
            return $data;
        }

        $data['data']['orderNumber'] = trim($p[0]);
        $data['data']['supplementDaysOrder'] = intval($p[1]);
        $data['data']['product'] = trim($p[2]);
        $data['data']['language'] = trim($p[3]);
        $data['data']['customerName'] = trim($p[4]);
        $data['data']['street'] = trim($p[5]);
        $data['data']['city'] = trim($p[6]);
        $data['data']['country'] = trim($p[7]);

        $p[8] = trim($p[8]);
        $tPart = substr($p[8], 0, 5);
        if (strcmp($tPart, 'TOTAL') != 0 || (strlen($p[8]) <= 5)) {
            $data['error'] = 'Token 8th should be TOTALxxx';
            return $data;
        }
        $data['data']['total'] = floatval(substr($p[8], 5));

        $p[9] = trim($p[9]);
        $tPart = substr($p[9], -3, 3);
        if (strcmp($tPart, 'BTL') != 0 || (strlen($p[9]) <= 3)) {
            $data['error'] = 'Token 9th should be xxBTL';
            return $data;
        }
        $data['data']['numPack'] = intval(substr($p[9], 0, strlen($p[9])-3));

        ////////////////////////////////////
        // x. Second parts
        $p = $part_2;
        if (count($p) == 0) {
            $data['error'] = "Second part is missing";
            return $data;
        }

        $drugs = array();
        $i = 0;
        foreach ($p as $token) {
            $drug = $this->parseDrugToken($token);

            if (!$drug) {
                $data['error'] = "Token ".$token." is invalid";
                return $data;
            }

            if ((strlen($drug['type']) > 0) && (floatval($drug['amount']) > 0)) {
                $drug['position'] = $i;
                $i++;
                $drugs[] = $drug;
            }
        }

        usort($drugs, function ($a, $b) {
                if ($a['type'] == 'I') {
                    return -1;
                }
                if ($b['type'] == 'I') {
                    return 1;
                }
                return $a['position'] > $b['position'] ? 1 : -1;
            });

        $data['data']['drugs'] = $drugs;

        return $data;
    }

    private function parseDrugToken($token) {
        $token = trim($token);
        $n = strlen($token);
        for ($i = 0; $i < $n; $i++) {
            if (!($token[$i] >= 'A' && $token[$i] <= 'Z')) {
                $d = substr($token, 0, $i);
                $amount = floatval(substr($token, $i));
                return array('type' => $d, 'amount' => $amount);
            }
        }

        return false;
    }
}
