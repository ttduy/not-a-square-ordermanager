<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Intl;

class CashInCreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CashInCreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {   
        // recorded_date is required 
        $builder->add('recorded_date', 'datepicker', array(
            'label'    => 'Recorded Date'
        ));

        // cash_amount is required 
        $builder->add('cash_amount', 'money', array(
            'label'    => 'Cash Amount'
        ));
        $builder->add('designation', 'text', array(
            'label'    => 'Designation',
            'required' => false
        ));
        $builder->add('posting_date', 'datepicker', array(
            'label'    => 'Posting Date',
            'required' => false
        ));
        $builder->add('currency', 'text', array(
            'label'    => 'Currency',
            'required' => false
        ));
        $builder->add('partner_name', 'text', array(
            'label'    => 'Partner Name',
            'required' => false
        ));
        $builder->add('reference', 'text', array(
            'label'    => 'Reference',
            'required' => false
        ));
        $builder->add('sale_target', 'text', array(
            'label'    => 'Sale Target',
            'required' => false
        ));
        $builder->add('partner', 'text', array(
            'label'    => 'Partner',
            'required' => false
        ));
        $builder->add('transaction_content', 'text', array(
            'label'    => 'Transaction Content',
            'required' => false
        ));
        $builder->add('transaction_id', 'text', array(
            'label'    => 'Transaction ID',
            'required' => false
        ));
        $builder->add('company_name', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('company_id', 'text', array(
            'label'    => 'Company ID',
            'required' => false
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        // check duplication on Transactin ID
        // get submitted TransactionId from CashIn
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');
        $this->errors = $utils->validateTransactionId($model->transaction_id, $this->errors);

        if (sizeof($this->errors) == 0) {
            $em = $this->container->get('doctrine')->getEntityManager();

            $cashIn = new Entity\CashIn();
            $cashIn->setRecordedDate($model->recorded_date);
            $cashIn->setCashAmount($model->cash_amount);
            $cashIn->setDesignation($model->designation);
            $cashIn->setRecordedMonth($model->recorded_date->format('m'));
            $cashIn->setRecordedYear($model->recorded_date->format('Y'));
            
            if ($model->posting_date) {
                $cashIn->setPostingDate($model->posting_date);
                $cashIn->setPostingMonth($model->posting_date->format('m'));
                $cashIn->setPostingYear($model->posting_date->format('Y'));                
            }

            $cashIn->setCurrency($model->currency);
            $cashIn->setPartnerName($model->partner_name);
            $cashIn->setReference($model->reference);
            $cashIn->setSaleTarget($model->sale_target);
            $cashIn->setPartner($model->partner);
            $cashIn->setTransactionContent($model->transaction_content);
            $cashIn->setTransactionId($model->transaction_id);
            $cashIn->setCompanyName($model->company_name);
            $cashIn->setCompanyId($model->company_id);

            $em->persist($cashIn);
            $em->flush();

            parent::onSuccess();
        }
    }
}
