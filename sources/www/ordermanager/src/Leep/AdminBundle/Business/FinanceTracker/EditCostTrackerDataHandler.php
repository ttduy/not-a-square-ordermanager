<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;


class EditCostTrackerDataHandler extends AppEditHandler {
    public $year = 0;
    public function loadEntity($request) {
        $filter = $this->container->get('leep_admin.finance_tracker.business.cost_tracker_filter_handler');
        $filterData = $filter->getCurrentFilter();
        $this->year = $filterData->year;
        return null;
    }

    private function getCostTrackerSectors() {
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
    }

    public function convertToFormModel($entity) { 
        return $this->getCostTrackerPerMonth();
    }

    // get cost tracker per month info
    private function getCostTrackerPerMonth() {
        $cost = array();

        $costs = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSectorPerMonth')->findByYear($this->year);
        foreach ($costs as $c) {
            $cost['sector_'.$c->getCostTrackerSectorId().'_'.$c->getMonth()] = $c->getCost();
            $cost['sector_prognosis_'.$c->getCostTrackerSectorId().'_'.$c->getMonth()] = $c->getCostPrognosis();
        }

        return $cost;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        $monthShort = $mapping->getMapping('LeepAdmin_Month_Short');
        $costTrackerSectors = $this->getCostTrackerSectors();

        foreach ($costTrackerSectors as $costTrackerSector) {
            for ($i = 1; $i <= 12; $i++) {
                $builder->add('sector_cashout_reset_'.$costTrackerSector->getId().'_'.$i, 'hidden');

                $builder->add('sector_'.$costTrackerSector->getId().'_'.$i, 'text', array(
                    'label'    => $monthShort[$i],
                    'required' => false,
                    'attr' => array(
                        'style' => 'min-width: 60px; max-width: 60px'
                    )
                ));
                $builder->add('sector_prognosis_'.$costTrackerSector->getId().'_'.$i, 'text', array(
                    'label'    => $monthShort[$i],
                    'required' => false,
                    'attr' => array(
                        'style' => 'min-width: 60px; max-width: 60px'
                    )
                ));
            }
        }
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();
        $dbHelper = $this->container->get('leep_admin.helper.database');

        // get cost tracker per month before changes submitted
        $previousData = $this->getCostTrackerPerMonth();

        // check changes
        $sectors = $this->getCostTrackerSectors();
        $historyLog = array();
        $historyLogFormat = "%s;%s;%s";
        $historyDateTime = new \DateTime();
        $historyLogDate = $historyDateTime->format('d/m/Y');

        $sectors = $this->getCostTrackerSectors();
        foreach ($sectors as $sector) {
            $sectorId = intval($sector->getId());
            for ($i = 1; $i <= 12; $i++) {
                // get submitted data
                $cashOutReset = isset($model['sector_cashout_reset_'.$sectorId.'_'.$i]) ? $model['sector_cashout_reset_'.$sectorId.'_'.$i] : '';
                $cost = isset($model['sector_'.$sectorId.'_'.$i]) ? floatval($model['sector_'.$sectorId.'_'.$i]) : '';
                $costPrognosis = isset($model['sector_prognosis_'.$sectorId.'_'.$i]) ? floatval($model['sector_prognosis_'.$sectorId.'_'.$i]) : '';                

                // check if cashOutReset is reset
                if ($cashOutReset) {
                    // remove all related data of specific month-year
                    $filters = array(
                        'recordedMonth' => intval($i),
                        'recordedYear' => intval($this->year),
                        'idCostTrackerSector' => intval($sectorId)
                    );
                    $dbHelper->delete($em, 'AppDatabaseMainBundle:CashOut', $filters);
                }

                // check the difference between existing data and submitted data
                $changeInCost = 0;
                $changeInPrognosis = 0;
                $keyCost = 'sector_'.$sectorId.'_'.$i;
                $keyPrognosis = 'sector_prognosis_'.$sectorId.'_'.$i;

                // just need to check either $keyCost or $keyPrognosis to see if existing data is available
                if (array_key_exists($keyCost, $previousData)) {
                    $changeInCost = $cost - $previousData[$keyCost];
                    $changeInPrognosis = $costPrognosis - $previousData[$keyPrognosis];
                    $changeInCostDate = sprintf('%s/%s/%s', $historyDateTime->format('d'), $i, $this->year);

                    // prepare data for history if there's any change in Cost
                    if ($changeInCost) {
                        $historyLog[] = sprintf($historyLogFormat, $changeInCost, $changeInCostDate, $sector->getTxt());
                    }
                }

                // delete existing cost tracker per month
                $filters = array(
                    'month' => intval($i),
                    'year' => intval($this->year),
                    'costTrackerSectorId' => intval($sectorId)
                );
                $dbHelper->delete($em, 'AppDatabaseMainBundle:CostTrackerSectorPerMonth', $filters);

                // update cost tracker per month
                $costTrackerSectorPerMonth = new Entity\CostTrackerSectorPerMonth();
                $costTrackerSectorPerMonth->setCostTrackerSectorId($sectorId);
                $costTrackerSectorPerMonth->setCost($cost);
                $costTrackerSectorPerMonth->setCostPrognosis($costPrognosis);
                $costTrackerSectorPerMonth->setYear($this->year);
                $costTrackerSectorPerMonth->setMonth($i);
                $em->persist($costTrackerSectorPerMonth);
            }
        }

        // update history if need
        if (sizeof($historyLog) > 0) {
            // concat string by \r\n
            $historyToBeSavedData = implode("\r\n", $historyLog);

            // save history
            $submitDataHistory = new Entity\CostTrackerSubmitDataHistory();
            $submitDataHistory->setTimestamp($historyDateTime);
            $submitDataHistory->setData($historyToBeSavedData);
            $submitDataHistory->setPreviewData(substr($historyToBeSavedData, 0, 100));
            $submitDataHistory->setNumFormatError(0);
            $submitDataHistory->setNumDataError(0);
            $submitDataHistory->setNumSuccess(sizeof($historyLog));
            $em->persist($submitDataHistory);
        }

        $em->flush();

        $this->messages[] = "The records has been updated successfully";
    }
}