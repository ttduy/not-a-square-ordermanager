<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class SubmitCostTrackerHandler extends BaseCreateHandler {
    public $requestedCostTrackerHistoryId = null;

    public function getDefaultFormModel() {
        $model = new SubmitCostTrackerModel();        
        $em = $this->container->get('doctrine')->getEntityManager();
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');

        $requestedId = $this->container->get('request')->get('id');
        if ($requestedId) {
            $currentEntry = $em->getRepository('AppDatabaseMainBundle:CostTrackerSubmitDataHistory')->findOneById($requestedId);
            if ($currentEntry) {
                // set default data
                $model->result = $currentEntry->getData();

                // save last submitted date to be displayed on layout later
                $cacheBox->putKey('lastSubmitDate', $currentEntry->getTimestamp()->format('d/m/Y H:i:s'));

                // save requested cost tracker history for update
                $this->requestedCostTrackerHistoryId = $requestedId;
            }
        }

        return $model;
    }



    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('result', 'textarea', array(
            'label'    => 'Result',
            'required' => true,
            'attr'     => array(
                'style'   => 'width: 800px;height: 400px'
            )
        ));
    }

    // get total cost from history of per sector-month-year except specific cost tracker history Id
    public function getTotalCostFromHistory($arrSubmittededCost) {
        $em = $this->container->get('doctrine')->getEntityManager();

        // get Cost Tracker Sector list
        $costTrackerSectors = array();
        $result = $em->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
        foreach ($result as $r) {
            $costTrackerSectors[strtolower($r->getTxt())] = intval($r->getId());
        }

        // sum all cost for per sector-month-year recorded in Cost Tracker History
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.data')
            ->from('AppDatabaseMainBundle:CostTrackerSubmitDataHistory', 'p')
            ->andWhere('p.numFormatError = 0')
            ->andWhere('p.numDataError = 0')
            ->andWhere('p.numSuccess > 0');

        $result = $query->getQuery()->getResult();
        $totalCosts = array();
        foreach ($result as $key => $eachResult) {
            // parse data
            $costRows = explode("\r\n", $eachResult['data']);
            foreach ($costRows as $row) {
                $arrData = explode(';', $row);
                $cost = $arrData[0];
                $recordDate = $arrData[1];
                $sectorName = trim(strtolower($arrData[2]));
                // check if sector still exists
                if (array_key_exists($sectorName, $costTrackerSectors)) {
                    $sectorId = $costTrackerSectors[$sectorName];
                    // check if this sector is in count
                    if (array_key_exists($sectorId, $arrSubmittededCost)) {
                        // explode $date to month and year
                        $arrDate = explode('/', $recordDate);
                        $monthYear = intval($arrDate[1]) . '-' . intval($arrDate[2]);
                        // if month-year is in count
                        if (array_key_exists($monthYear, $arrSubmittededCost[$sectorId])) {
                            // count cost
                            if(!array_key_exists($sectorId, $totalCosts)) {
                                $totalCosts[$sectorId] = array();
                            }

                            if(!array_key_exists($monthYear, $totalCosts[$sectorId])) {
                                $totalCosts[$sectorId][$monthYear] = 0;
                            }

                            $totalCosts[$sectorId][$monthYear] += floatval($cost);
                        }
                    }
                }
            }
        }

        return $totalCosts;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $parser = $this->container->get('leep_admin.finance_tracker.business.submit_cost_tracker_parser');
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');

        $doctrine = $this->container->get('doctrine');
        $model = $this->getForm()->getData();
        
        $parser->init();
        $result = $parser->parseResult($model->result);

        $errorRecords = array(
            Constants::ERROR_TYPE_FORMAT_ERROR      => array(),
            Constants::ERROR_TYPE_DATA_ERROR        => array(),
            Constants::TYPE_SUCCESS                 => array()
        );

        // number of records in $result > 0, we have data to process
        if (sizeof($result) > 0) {
            $successRecords = array();
            foreach ($result as $i => $row) {
                if ($row['error'] == '') {
                    // Records
                    $cost = $row['cost'];
                    $costMonthYear = $row['costMonthYear'];
                    $categoryId = $row['categoryId'];

                    // sum by year
                    if(!array_key_exists($categoryId, $successRecords)) {
                        $successRecords[$categoryId] = array();
                    }

                    if(!array_key_exists($costMonthYear, $successRecords[$categoryId])) {
                        $successRecords[$categoryId][$costMonthYear] = 0;
                    }

                    $successRecords[$categoryId][$costMonthYear] += floatval($cost);

                    // Mark as added
                    $result[$i]['isAdded'] = true;

                    $errorRecords[Constants::TYPE_SUCCESS][] = $row;
                }
                else if ($row['error'] != '') {
                    $idType = intval($row['error_type']);
                    $errorRecords[$idType][] = $row;
                }
            }

            $dbHelper = $this->container->get('leep_admin.helper.database');

            // get total of cost from history, except specific history id
            if ($this->requestedCostTrackerHistoryId) {
                // update history record
                $historyRecord = $em->getRepository('AppDatabaseMainBundle:CostTrackerSubmitDataHistory')->findOneById($this->requestedCostTrackerHistoryId);
                if ($historyRecord) {
                    $historyRecord->setTimestamp(new \DateTime());
                    $historyRecord->setData($model->result);
                    $historyRecord->setPreviewData(substr($model->result, 0, 100));
                    $historyRecord->setNumFormatError(count($errorRecords[Constants::ERROR_TYPE_FORMAT_ERROR]));
                    $historyRecord->setNumDataError(count($errorRecords[Constants::ERROR_TYPE_DATA_ERROR]));
                    $historyRecord->setNumSuccess(count($errorRecords[Constants::TYPE_SUCCESS]));
                    $em->persist($historyRecord);
                    $em->flush();

                    // remove history error record
                    $filters = array(
                        'idCostTrackerSubmitDataHistory' => intval($this->requestedCostTrackerHistoryId)
                    );
                    $dbHelper->delete($em, 'AppDatabaseMainBundle:CostTrackerSubmitDataHistoryError', $filters);

                    // insert new history error record 
                    foreach ($errorRecords as $idType => $errors) {
                        foreach ($errors as $error) {
                            $submitDataHistoryError = new Entity\CostTrackerSubmitDataHistoryError();
                            $submitDataHistoryError->setIdCostTrackerSubmitDataHistory($this->requestedCostTrackerHistoryId);
                            $submitDataHistoryError->setIdErrorType($idType);
                            $submitDataHistoryError->setErrorData(serialize($error));
                            $em->persist($submitDataHistoryError);    
                        }
                    }
                    $em->flush();                                 
                }
            } else {
                // Save history
                $submitDataHistory = new Entity\CostTrackerSubmitDataHistory();
                $submitDataHistory->setTimestamp(new \DateTime());
                $submitDataHistory->setData($model->result);
                $submitDataHistory->setPreviewData(substr($model->result, 0, 100));
                $submitDataHistory->setNumFormatError(count($errorRecords[Constants::ERROR_TYPE_FORMAT_ERROR]));
                $submitDataHistory->setNumDataError(count($errorRecords[Constants::ERROR_TYPE_DATA_ERROR]));
                $submitDataHistory->setNumSuccess(count($errorRecords[Constants::TYPE_SUCCESS]));
                $em->persist($submitDataHistory);
                $em->flush();

                // Save error
                foreach ($errorRecords as $idType => $errors) {
                    foreach ($errors as $error) {
                        $submitDataHistoryError = new Entity\CostTrackerSubmitDataHistoryError();
                        $submitDataHistoryError->setIdCostTrackerSubmitDataHistory($submitDataHistory->getId());
                        $submitDataHistoryError->setIdErrorType($idType);
                        $submitDataHistoryError->setErrorData(serialize($error));
                        $em->persist($submitDataHistoryError);    
                    }
                }
                $em->flush();
            }

            // sum total cost
            $totalCost = $this->getTotalCostFromHistory($successRecords);

            // overwrite current cost tracker sector per month
            foreach ($totalCost as $sectorId => $sector) {
                foreach ($sector as $monthYear => $cost) {
                    $tmpArr = explode('-', $monthYear);
                    $month = $tmpArr[0];
                    $year = $tmpArr[1];

                    // remove
                    // NEED TO BE IMPROVED
                    $filters = array(
                        'month' => intval($month),
                        'year' => intval($year),
                        'costTrackerSectorId' => intval($sectorId)
                    );
                    $currentEntry = $em->getRepository('AppDatabaseMainBundle:CostTrackerSectorPerMonth')->findOneBy($filters);
                    $currentCostPrognosis = empty($currentEntry) ? NULL : $currentEntry->getCostPrognosis();
                    $dbHelper->delete($em, 'AppDatabaseMainBundle:CostTrackerSectorPerMonth', $filters);

                    // add
                    $costTrackerSectorPerMonth = new Entity\CostTrackerSectorPerMonth();
                    $costTrackerSectorPerMonth->setCostTrackerSectorId(intval($sectorId));
                    $costTrackerSectorPerMonth->setCost(floatval($cost));
                    $costTrackerSectorPerMonth->setCostPrognosis($currentCostPrognosis);
                    $costTrackerSectorPerMonth->setYear(intval($year));
                    $costTrackerSectorPerMonth->setMonth(intval($month));
                    $em->persist($costTrackerSectorPerMonth);
                }
            }
            $em->flush();
        }

        $cacheBox->putKey('costTrackerResult', $result);
    }
}
