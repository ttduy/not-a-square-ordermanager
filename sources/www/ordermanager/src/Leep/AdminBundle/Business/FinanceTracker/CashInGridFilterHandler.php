<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class CashInGridFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new CashInGridFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));
        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));

        return $builder;
    }
}