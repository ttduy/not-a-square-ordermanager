<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class SubmitCostTrackerParser {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $costTrackerSectors = array();

    public function init() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // get Cost Tracker Sector list
        $result = $em->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
        foreach ($result as $r) {
            $this->costTrackerSectors[strtolower($r->getTxt())] = intval($r->getId());
        }
    }

    public function parseResult($result) {
        $rowResult = array();

        $rows = explode("\n", $result);
        foreach ($rows as $row) {
            if (trim($row) != '') {
                $rowContent = $this->parseRow($row);
                $rowResult[] = $rowContent;
            }
        }

        return $rowResult;
    }

    /**
    * Amount;Date;Category
    * 100;10/12/2015;Salary
    */
    public function parseRow($row) {
        $tokens = explode(Constants::COST_TRACKER_DATA_SEPARATOR, $row);

        $rowResult = array(
            'raw'         => $row,
            'tokens'      => $tokens,
            'error'       => '',
            'error_type'  => ''
        );

        /////////////////////////////////
        // x. Validate row formats
        if (count($tokens) != 3) {
            $rowResult['error'] = 'Invalid format, expecting 3 tokens, '.count($tokens).' found';
            $rowResult['error_type'] = Constants::ERROR_TYPE_FORMAT_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Validate tokens
        $cost = trim($tokens[0]);
        $costDate = trim($tokens[1]);
        $category = trim($tokens[2]);

        /////////////////////////////////
        // x. Check cost, must be float
        if (!is_numeric($cost)) {
            $rowResult['error'] = "Invalid Amount '".$cost."'";
            $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }
        $rowResult['cost'] = floatval($cost);

        /////////////////////////////////
        // x. Check costDate, must be date ('dd/mm/yyyy')
        $date_regex = Constants::COST_TRACKER_DATA_DATE_FORMAT_REGEX;

        if (!preg_match($date_regex, $costDate)) {
            $rowResult['error'] = "Invalid Date '".$costDate."'";
            $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }
        // save as year for ease in calculating later
        $tmpCostDateArr = explode('/', $costDate);
        $rowResult['costMonthYear'] = intval($tmpCostDateArr[1]).'-'.intval($tmpCostDateArr[2]);
        $rowResult['costDate'] = $costDate;

        /////////////////////////////////
        // x. Check Category, must be string and is a valid Sector
        if (!array_key_exists(strtolower($category), $this->costTrackerSectors)) {
            $rowResult['error'] = "Invalid Category '".$category."'";
            $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }
        $rowResult['category'] = $category;
        $rowResult['categoryId'] = $this->costTrackerSectors[strtolower($category)];

        return $rowResult;
    }
}
