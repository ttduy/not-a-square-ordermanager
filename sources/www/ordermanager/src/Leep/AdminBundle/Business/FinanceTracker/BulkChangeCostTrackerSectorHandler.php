<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;

class BulkChangeCostTrackerSectorHandler extends AppEditHandler {
    public $selectedId;
    public $selectedStatus;
    public $status;

    public function loadEntity($request) {
        return false;
    }
    public function convertToFormModel($entity) { 
        $em = $this->container->get('doctrine')->getEntityManager();

        $model = new BulkChangeCostTrackerSectorModel(); 
        $model->selectedCashOutInfo = "";
        $model->selectedCashOutIds = "";
        $selectedIdRaw = $this->container->get('request')->get('selectedId', '');
        $tmp = explode(",", $selectedIdRaw);
        $arr = array();
        foreach ($tmp as $v) {
            $v = trim($v);
            if (!empty($v)) {
                $arr[] = $v;
            }
        }

        if (sizeOf($arr) > 0) {
            $model->selectedCashOutIds = implode(',', $arr);

            // find some useful info for each entry
            $query = $em->createQueryBuilder();
            $query->select('p.id, p.designation, p.recordedDate, p.cashAmount, p.currency, p.partner')
                ->from('AppDatabaseMainBundle:CashOut', 'p')
                ->andWhere('p.id in (:ids)')
                ->orderBy('p.recordedDate', 'DESC')
                ->setParameter('ids', $arr);
            $result = $query->getQuery()->getResult();
            $model->selectedCashOutInfo = "Recorded Date | Cash | Currency | Designation | Partner";
            foreach ($result as $entry) {
                $model->selectedCashOutInfo .= "<br/>" . sprintf('%s | %s | %s | %s | %s', 
                                                            $entry['recordedDate']->format('Y-m-d'), 
                                                            $entry['cashAmount'],
                                                            $entry['currency'],
                                                            $entry['designation'],
                                                            $entry['partner']);
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('selectedCashOutInfo', 'label', array(
            'label'    => 'Selected Cash-Out Entries',
            'required' => true,
            'attr'     => array(
                'rows'    => 10, 'cols' => 100
            )
        ));

        $builder->add('costTrackerSectorId',   'searchable_box', array(
            'label'    => 'Cost Tracker Sector',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Cost_Tracker_Sectors')
        ));

        $builder->add('selectedCashOutIds', 'hidden');
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        $doctrine = $this->container->get('doctrine');

        // get selected entries
        $arrOfEntries = explode(',', $model->selectedCashOutIds);

        // get selected Cost Tracker Sector
        $costTrackerSector = $model->costTrackerSectorId;

        $errors = array();

        if (sizeof($arrOfEntries) > 0) {
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:CashOut', 'p')
                ->set('p.idCostTrackerSector', $model->costTrackerSectorId)
                ->andWhere('p.id in (:ids)')
                ->setParameter('ids', $arrOfEntries)
                ->getQuery()->execute();
        }

        $this->messages[] = 'Status has been changed successfully';
    }
}