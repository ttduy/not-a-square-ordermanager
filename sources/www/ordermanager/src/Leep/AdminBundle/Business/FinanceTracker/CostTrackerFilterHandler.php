<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class CostTrackerFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new CostTrackerFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));

        return $model;
    }

    public function buildForm($builder) {
        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));

        return $builder;
    }
}