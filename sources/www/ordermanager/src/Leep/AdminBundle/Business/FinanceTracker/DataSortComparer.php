<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class DataSortComparer {
    public $month = 1;
    public $compareBy = 'amount';
    public $sortDirection = 'asc';

    public function compare($a, $b) {
        $vA = $a['data'][$this->month][$this->compareBy];
        $vB = $b['data'][$this->month][$this->compareBy];


        if ($vA == $vB) {
            return 0;
        }
        $i = ($vA < $vB) ? -1 : 1;
        if ($this->sortDirection == 'desc') $i *= -1;
        return $i;
    }
}