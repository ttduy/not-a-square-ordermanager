<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business;
use Easy\ModuleBundle\Module\AbstractHook;

class EditCostTrackerDataHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mapping = $controller->get('easy_mapping');
        $utils = $controller->get('leep_admin.finance_tracker.business.utils');
        $sectors = $controller->get('doctrine')->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
        $map = array();
        foreach ($sectors as $sector) {
            $map[$sector->getId()] = $sector->getTxt();
        }

        $filter = $controller->get('leep_admin.finance_tracker.business.cost_tracker_filter_handler');
        $filterData = $filter->getCurrentFilter();

        $data['year'] = $filterData->year;
        $data['sectors'] = $map;
        $data['months'] = $mapping->getMapping('LeepAdmin_Month_Short');

        $linkedCashOut = $utils->summarizeLinkedCashOut($filterData->year);
        $data['linkedCashOut'] = $linkedCashOut;
    }
}
