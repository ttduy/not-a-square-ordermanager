<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class PartnerFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new PartnerFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));
        $model->sortDirection = 'asc';

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');

        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('sortBy', 'choice', array(
            'label'    => 'Sort by',
            'required' => false,
            'choices'  => array('name' => 'Partner') + $utils->getSortOptions()
        ));
        $builder->add('sortDirection', 'choice', array(
            'label'    => 'Sort direction',
            'required' => false,
            'choices'  => array('asc' => 'Ascending order', 'desc' => 'Descending order'),
            'empty_value' => false
        ));

        return $builder;
    }
}