<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class DistributionChannelFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new DistributionChannelFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));
        $model->sortDirection = 'asc';

        return $model;
    }



    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');

        $builder->add('name', 'text', array(
            'label'    => 'Name',
            'required' => false
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Type',
            'required' => false,
            'choices'  => array(0 => 'All') + $mapping->getMapping('LeepAdmin_DistributionChannel_Type'),
            'empty_value' => false
        ));
        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));
        $builder->add('distributionChannels', 'choice', array(
            'label'    => 'Distribution channels',
            'required' => false,
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('LeepAdmin_DistributionChannel_List'),
            'empty_value' => false,
            'attr'     => array(
                'style'  => 'height: 130px'
            )
        ));
        $builder->add('sortBy', 'choice', array(
            'label'    => 'Sort by',
            'required' => false,
            'choices'  => array('name' => 'Distribution channel') + $utils->getSortOptions()
        ));
        $builder->add('sortDirection', 'choice', array(
            'label'    => 'Sort direction',
            'required' => false,
            'choices'  => array('asc' => 'Ascending order', 'desc' => 'Descending order'),
            'empty_value' => false
        ));
        return $builder;
    }
}