<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class DistributionChannelFilterModel {
    public $year;
    public $name;
    public $idType;
    public $distributionChannels;
    public $sortBy;
    public $sortDirection;
}