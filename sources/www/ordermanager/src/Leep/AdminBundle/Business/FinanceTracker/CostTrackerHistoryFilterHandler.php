<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class CostTrackerHistoryFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new CostTrackerHistoryFilterModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date From',
            'required' => false
        ));

        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => false
        ));

        return $builder;
    }
}