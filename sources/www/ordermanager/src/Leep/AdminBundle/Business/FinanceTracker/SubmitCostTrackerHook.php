<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\ModuleBundle\Module\AbstractHook;

class SubmitCostTrackerHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $cacheBox = $controller->get('leep_admin.helper.cache_box_data');

        $data['parseResult'] = $cacheBox->getKey('costTrackerResult');

        // warning after submitting empty data
        $data['warningSubmitEmptyData'] = false;
        if (is_array($data['parseResult']) && empty($data['parseResult'])) {
        	$data['warningSubmitEmptyData'] = true;
        }

        $data['lastSubmitDate'] = ($cacheBox->getKey('lastSubmitDate')) ? $cacheBox->getKey('lastSubmitDate') : null;
    }
}
