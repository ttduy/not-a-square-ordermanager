<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class Constants {
    const ERROR_TYPE_FORMAT_ERROR     = 1;
    const ERROR_TYPE_DATA_ERROR       = 2;

    const TYPE_SUCCESS                = 98;
    const ERROR_TYPE_ANY              = 99;

    const COST_TRACKER_DATA_SEPARATOR         = ';';
    const COST_TRACKER_DATA_DATE_FORMAT_REGEX = '/^(0[1-9]|[12][0-9]|3[01])(\/)(0[1-9]|1[012])(\/)(19|20)\d\d$/';
    const CASH_DATA_SEPARATOR                 = ';';
    const CASH_DATA_FORMAT_REGEX              = '/^(0[1-9]|[12][0-9]|3[01])(\/)(0[1-9]|1[012])(\/)(19|20)\d\d$/';
    
    public static function getMonthShorts() {
        return array(
            1  => 'Jan',
            2  => 'Feb',
            3  => 'Mar',
            4  => 'Apr',
            5  => 'May',
            6  => 'Jun',
            7  => 'Jul',
            8  => 'Aug',
            9  => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec',
        );
    }
}