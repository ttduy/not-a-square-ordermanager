<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class PartnerFilterModel {
    public $year;
    public $name;
    public $sortBy;
    public $sortDirection;
}