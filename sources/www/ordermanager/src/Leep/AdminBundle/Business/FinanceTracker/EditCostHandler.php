<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;


class EditCostHandler extends AppEditHandler {
    public $year = 0;
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) { 
        $cost = array();
        $costs = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:FinanceCostPerMonth')->findByYear($this->year);
        foreach ($costs as $c) {
            $cost['cost_'.$c->getMonth()] = $c->getCost();
            $cost['cash_'.$c->getMonth()] = $c->getCash();
        }
        return $cost;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $monthShort = $mapping->getMapping('LeepAdmin_Month_Short');


        $builder->add('sectionCost', 'section', array(
            'label'    => 'Cost per month - Year: '.$this->year,            
            'property_path' => false
        ));
        for ($i = 1; $i <= 12; $i++) {
            $builder->add('cost_'.$i, 'text', array(
                'label'    => $monthShort[$i],
                'required' => false
            ));
        }

        $builder->add('sectionCash', 'section', array(
            'label'    => 'Cash in account - Year: '.$this->year,            
            'property_path' => false
        ));
        for ($i = 1; $i <= 12; $i++) {
            $builder->add('cash_'.$i, 'text', array(
                'label'    => $monthShort[$i],
                'required' => false
            ));
        }
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->container->get('doctrine')->getEntityManager();

        // Clear old year
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:FinanceCostPerMonth', 'p')
            ->andWhere('p.year = :year')
            ->setParameter('year', $this->year)
            ->getQuery()->execute();

        // Insert new        
        for ($i = 1; $i <= 12; $i++) {
            $cost = floatval($model['cost_'.$i]);
            $cash = floatval($model['cash_'.$i]);
        
            $finance = new Entity\FinanceCostPerMonth();
            $finance->setYear($this->year);
            $finance->setMonth($i);
            $finance->setCost($cost);
            $finance->setCash($cash);
            $em->persist($finance);
        }
        $em->flush();

        $this->messages[] = 'Saved!';
    }
}