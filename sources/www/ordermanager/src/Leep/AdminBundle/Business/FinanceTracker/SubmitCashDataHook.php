<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\ModuleBundle\Module\AbstractHook;

class SubmitCashDataHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $cacheBox = $controller->get('leep_admin.helper.cache_box_data');

        $data['parseResult'] = $cacheBox->getKey('cashDataResult');
    }
}
