<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\ModuleBundle\Module\AbstractHook;

class CashInGridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $data['submitCashDataUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'submit_cash_data', 'create');
        $data['createCashInUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'cash_in_create', 'create');
    }
}
