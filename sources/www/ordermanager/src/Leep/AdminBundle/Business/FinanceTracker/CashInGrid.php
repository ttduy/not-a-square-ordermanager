<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class CashInGrid extends AppGridDataReader {
    public $filters;
    public $currentTimestamp;

    public function __construct($container) {

        parent::__construct($container);
        $this->formatter = $this->container->get('leep_admin.helper.formatter');

        $today = new \DateTime();
        $this->currentTimestamp = $today->getTimestamp();
    }
    
    public function getColumnMapping() {
        return array('designation', 'recordedDate', 'cashAmount', 'currency', 'partner', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Designation',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Recorded Date',       'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Cash Amount',         'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Currency',            'width' => '5%', 'sortable' => 'false'),
            array('title' => 'Partner',             'width' => '10%', 'sortable' => 'true'),
            array('title' => 'Action',              'width' => '10%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CashInGridReader');
    }

    public function postQueryBuilder($queryBuilder) {
        $queryBuilder->addOrderBy('p.recordedDate', 'DESC');      
    }

    public function buildQuery($queryBuilder) {
        // Compute statistic
        $queryBuilder->select('p.id, p.designation, p.recordedDate, p.cashAmount, p.currency, p.partner')
            ->from('AppDatabaseMainBundle:CashIn', 'p');

        if (!empty($this->filters->dateFrom)) {
            $queryBuilder->andWhere('p.recordedDate >= :dateFrom')
                ->setParameter('dateFrom', $this->filters->dateFrom);
        }
        if (!empty($this->filters->dateTo)) {
            $queryBuilder->andWhere('p.recordedDate <= :dateTo')
                ->setParameter('dateTo', $this->filters->dateTo);
        }
    }

    public function buildCellRecordedDate($row) {
        return  $row['recordedDate']->format('Y-m-d');
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('financeTrackerView')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'finance_tracker', 'cash_in_edit', 'edit', array('id' => $row['id'])), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'finance_tracker', 'cash_in_delete', 'delete', array('id' => $row['id'])), 'button-delete');
        }

        return $builder->getHtml();
    }
}
