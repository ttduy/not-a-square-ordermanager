<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class Utils {    
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getSortOptions() {
        $mapping = $this->container->get('easy_mapping');
        $months = $mapping->getMapping('LeepAdmin_Month_Short');

        $options = array();

        foreach ($months as $no => $month) {
            $options['order_'.$no] = $month.' - By the number of order';
            $options['amount_'.$no] = $month.' - By the total amount';
        }
        return $options;
    }

    public function generateRandomColor() {
        $str = '#'; 
        for($i = 0 ; $i < 6 ; $i++) { 
            $randNum = rand(0 , 15); 
            switch ($randNum) { 
                case 10: $randNum = 'A'; break; 
                case 11: $randNum = 'B'; break; 
                case 12: $randNum = 'C'; break; 
                case 13: $randNum = 'D'; break; 
                case 14: $randNum = 'E'; break; 
                case 15: $randNum = 'F'; break; 
            } 
            $str .= $randNum; 
        } 
        return $str; 
    }

    public function summarizeLinkedCashOut($year) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p.idCostTrackerSector, p.recordedMonth, SUM(p.cashAmount) as totalAmount')
            ->from('AppDatabaseMainBundle:CashOut', 'p')
            ->andWhere('p.recordedYear = :year')
            ->groupBy('p.idCostTrackerSector, p.recordedMonth')
            ->setParameter('year', $year);

        $results = $query->getQuery()->getResult();

        $summary = array();
        foreach ($results as $r) {
            $idCostTrackerSector = intval($r['idCostTrackerSector']);
            $idMonth = intval($r['recordedMonth']);
            $totalAmount = floatval($r['totalAmount']);
            if (!isset($summary[$idCostTrackerSector])) {
                $summary[$idCostTrackerSector] = array();
                for ($i = 1; $i <= 12; $i++) {
                    $summary[$idCostTrackerSector][$i] = 0;
                }
            }
            $summary[$idCostTrackerSector][$idMonth] = abs($totalAmount);
        }

        return $summary;
    }

    public function validateTransactionId($transactionId, $errors, $ignoreCashId = null) {
        $em = $this->container->get('doctrine')->getEntityManager();

        $arrBeingSubmitedTransactionIds = array();
        
        // check duplication on Transactin ID
        // get submitted TransactionId from CashIn
        $query = $em->createQueryBuilder();
        $query->select('p.transactionId')
            ->from('AppDatabaseMainBundle:CashIn', 'p')
            ->andWhere("p.transactionId IS NOT NULL AND p.transactionId <> ''");

        // exclude TransactionId of ignoreCashId if it's set (this is aimed for modification process)
        if ($ignoreCashId) {
            $query->andWhere("p.id <> :cashId")
                ->setParameter("cashId", $ignoreCashId);
        }

        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $arrBeingSubmitedTransactionIds[$r['transactionId']] = 1;
        }

        // get submitted TransactionId from CashOut
        $query = $em->createQueryBuilder();
        $query->select('p.transactionId')
            ->from('AppDatabaseMainBundle:CashOut', 'p')
            ->andWhere("p.transactionId IS NOT NULL AND p.transactionId <> ''");

        // exclude TransactionId of ignoreCashId if it's set (this is aimed for modification process)
        if ($ignoreCashId) {
            $query->andWhere("p.id <> :cashId")
                ->setParameter("cashId", $ignoreCashId);
        }

        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $arrBeingSubmitedTransactionIds[$r['transactionId']] = 1;
        }

        // Check if there's any duplication on Transaction Ids
        if ($transactionId) {
            if (array_key_exists($transactionId, $arrBeingSubmitedTransactionIds)) {
                // mark error
                $errors[] = "Duplication on TransactionId '".$transactionId."'";
            }
        }

        return $errors;
    }

}