<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Symfony\Component\Intl;

class CashOutCreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CashOutCreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // recorded_date is required 
        $builder->add('recorded_date', 'datepicker', array(
            'label'    => 'Recorded Date'
        ));

        // cash_amount is required 
        $builder->add('cash_amount', 'money', array(
            'label'    => 'Cash Amount'
        ));

        $builder->add('id_cost_tracker_sector',   'searchable_box', array(
            'label'    => 'Cost Tracker Sector',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Cost_Tracker_Sectors')
        ));
        $builder->add('designation', 'text', array(
            'label'    => 'Designation',
            'required' => false
        ));
        $builder->add('posting_date', 'datepicker', array(
            'label'    => 'Posting Date',
            'required' => false
        ));
        $builder->add('currency', 'text', array(
            'label'    => 'Currency',
            'required' => false
        ));
        $builder->add('partner_name', 'text', array(
            'label'    => 'Partner Name',
            'required' => false
        ));
        $builder->add('reference', 'text', array(
            'label'    => 'Reference',
            'required' => false
        ));
        $builder->add('sale_target', 'text', array(
            'label'    => 'Sale Target',
            'required' => false
        ));
        $builder->add('partner', 'text', array(
            'label'    => 'Partner',
            'required' => false
        ));
        $builder->add('transaction_content', 'text', array(
            'label'    => 'Transaction Content',
            'required' => false
        ));
        $builder->add('transaction_id', 'text', array(
            'label'    => 'Transaction ID',
            'required' => false
        ));
        $builder->add('company_name', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('company_id', 'text', array(
            'label'    => 'Company ID',
            'required' => false
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        // check duplication on Transactin ID
        // get submitted TransactionId from CashIn
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');
        $this->errors = $utils->validateTransactionId($model->transaction_id, $this->errors);

        if (sizeof($this->errors) == 0) {
            $em = $this->container->get('doctrine')->getEntityManager();

            $cashOut = new Entity\CashOut();
            $cashOut->setIdCostTrackerSector($model->id_cost_tracker_sector);
            $cashOut->setRecordedDate($model->recorded_date);
            $cashOut->setCashAmount($model->cash_amount);
            $cashOut->setDesignation($model->designation);
            $cashOut->setRecordedMonth($model->recorded_date->format('m'));
            $cashOut->setRecordedYear($model->recorded_date->format('Y'));

            if ($model->posting_date) {
                $cashOut->setPostingDate($model->posting_date);
                $cashOut->setPostingMonth($model->posting_date->format('m'));
                $cashOut->setPostingYear($model->posting_date->format('Y'));
            }
            
            $cashOut->setCurrency($model->currency);
            $cashOut->setPartnerName($model->partner_name);
            $cashOut->setReference($model->reference);
            $cashOut->setSaleTarget($model->sale_target);
            $cashOut->setPartner($model->partner);
            $cashOut->setTransactionContent($model->transaction_content);
            $cashOut->setTransactionId($model->transaction_id);
            $cashOut->setCompanyName($model->company_name);
            $cashOut->setCompanyId($model->company_id);

            $em->persist($cashOut);
            $em->flush();

            parent::onSuccess();
        }
    }
}
