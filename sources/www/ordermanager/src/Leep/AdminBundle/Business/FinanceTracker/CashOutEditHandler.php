<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CashOutEditHandler extends AppEditHandler {
    private $beingProcessedCashId = null;
    
    public function loadEntity($request) {
        $this->beingProcessedCashId = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:CashOut', 'app_main')->findOneById($this->beingProcessedCashId);
    }

    public function convertToFormModel($entity) {
        $model = new CashOutEditModel();
        $model->id_cost_tracker_sector = $entity->getIdCostTrackerSector();
        $model->recorded_date = $entity->getRecordedDate();
        $model->cash_amount = $entity->getCashAmount();
        $model->designation = $entity->getDesignation();
        $model->posting_date = $entity->getPostingDate();
        $model->currency = $entity->getCurrency();
        $model->partner_name = $entity->getPartnerName();
        $model->reference = $entity->getReference();
        $model->sale_target = $entity->getSaleTarget();
        $model->partner = $entity->getPartner();
        $model->transaction_content = $entity->getTransactionContent();
        $model->transaction_id = $entity->getTransactionId();
        $model->company_name = $entity->getCompanyName();
        $model->company_id = $entity->getCompanyId();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        // recorded_date is required 
        $builder->add('recorded_date', 'datepicker', array(
            'label'    => 'Recorded Date'
        ));

        // cash_amount is required 
        $builder->add('cash_amount', 'money', array(
            'label'    => 'Cash Amount'
        ));

        $builder->add('id_cost_tracker_sector',   'searchable_box', array(
            'label'    => 'Cost Tracker Sector',
            'required' => false,
            'choices'  => $mapping->getMapping('LeepAdmin_Cost_Tracker_Sectors')
        ));
        $builder->add('designation', 'text', array(
            'label'    => 'Designation',
            'required' => false
        ));
        $builder->add('posting_date', 'datepicker', array(
            'label'    => 'Posting Date',
            'required' => false
        ));
        $builder->add('currency', 'text', array(
            'label'    => 'Currency',
            'required' => false
        ));
        $builder->add('partner_name', 'text', array(
            'label'    => 'Partner Name',
            'required' => false
        ));
        $builder->add('reference', 'text', array(
            'label'    => 'Reference',
            'required' => false
        ));
        $builder->add('sale_target', 'text', array(
            'label'    => 'Sale Target',
            'required' => false
        ));
        $builder->add('partner', 'text', array(
            'label'    => 'Partner',
            'required' => false
        ));
        $builder->add('transaction_content', 'text', array(
            'label'    => 'Transaction Content',
            'required' => false
        ));
        $builder->add('transaction_id', 'text', array(
            'label'    => 'Transaction ID',
            'required' => false
        ));
        $builder->add('company_name', 'text', array(
            'label'    => 'Company Name',
            'required' => false
        ));
        $builder->add('company_id', 'text', array(
            'label'    => 'Company ID',
            'required' => false
        ));

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        // check duplication on Transactin ID
        // get submitted TransactionId from CashIn
        $utils = $this->container->get('leep_admin.finance_tracker.business.utils');
        $this->errors = $utils->validateTransactionId($model->transaction_id, $this->errors, $this->beingProcessedCashId);

        if (sizeof($this->errors) == 0) {
            $this->entity->setIdCostTrackerSector($model->id_cost_tracker_sector);
            $this->entity->setRecordedDate($model->recorded_date);
            $this->entity->setCashAmount($model->cash_amount);
            $this->entity->setDesignation($model->designation);
            $this->entity->setRecordedMonth($model->recorded_date->format('m'));
            $this->entity->setRecordedYear($model->recorded_date->format('Y'));

            if ($model->posting_date) {
                $this->entity->setPostingDate($model->posting_date);
                $this->entity->setPostingMonth($model->posting_date->format('m'));
                $this->entity->setPostingYear($model->posting_date->format('Y'));                
            }

            $this->entity->setCurrency($model->currency);
            $this->entity->setPartnerName($model->partner_name);
            $this->entity->setReference($model->reference);
            $this->entity->setSaleTarget($model->sale_target);
            $this->entity->setPartner($model->partner);
            $this->entity->setTransactionContent($model->transaction_content);
            $this->entity->setTransactionId($model->transaction_id);
            $this->entity->setCompanyName($model->company_name);
            $this->entity->setCompanyId($model->company_id);

            parent::onSuccess();
        }
    }
}
