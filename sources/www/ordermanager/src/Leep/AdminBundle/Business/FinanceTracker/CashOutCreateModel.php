<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Symfony\Component\Validator\ExecutionContext;

class CashOutCreateModel {
    public $id_cost_tracker_sector;
    public $designation;
    public $recorded_date;
    public $recorded_month;
    public $recorded_year;
    public $posting_date;
    public $posting_month;
    public $posting_year;
    public $cash_amount;
    public $currency;
    public $partner_name;
    public $reference;
    public $sale_target;
    public $partner;
    public $transaction_content;
    public $transaction_id;
    public $company_name;
    public $company_id;
}