<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

class SubmitCashDataParser {
    // this one will be used to prevent the duplication of transactionId
    private $arrBeingSubmitedTransactionIds = array();
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public $costTrackerSectors = array();

    public function init() {
        $em = $this->container->get('doctrine')->getEntityManager();

        // get Cost Tracker Sector list
        $result = $em->getRepository('AppDatabaseMainBundle:CostTrackerSector')->findAll();
        foreach ($result as $r) {
            $this->costTrackerSectors[$r->getTxt()] = intval($r->getId());
        }

        // get submitted TransactionId from CashIn
        $query = $em->createQueryBuilder();
        $query->select('p.transactionId')
            ->from('AppDatabaseMainBundle:CashIn', 'p')
            ->andWhere("p.transactionId IS NOT NULL AND p.transactionId <> ''");
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $this->arrBeingSubmitedTransactionIds[$r['transactionId']] = 1;
        }

        // get submitted TransactionId from CashOut
        $query = $em->createQueryBuilder();
        $query->select('p.transactionId')
            ->from('AppDatabaseMainBundle:CashOut', 'p')
            ->andWhere("p.transactionId IS NOT NULL AND p.transactionId <> ''");
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $this->arrBeingSubmitedTransactionIds[$r['transactionId']] = 1;
        }
    }

    public function parseResult($result) {
        $rowResult = array();

        $rows = explode("\n", $result);
        foreach ($rows as $row) {
            if (trim($row) != '') {
                $rowContent = $this->parseRow($row);
                $rowResult[] = $rowContent;
            }
        }

        return $rowResult;
    }

    /**
    * Amount;Date;Category
    * 100;10/12/2015;Salary
    */
    public function parseRow($row) {
        $tokens = explode(Constants::CASH_DATA_SEPARATOR, $row);

        $rowResult = array(
            'raw'         => $row,
            'tokens'      => $tokens,
            'error'       => '',
            'error_type'  => ''
        );

        /////////////////////////////////
        // x. Validate row formats
        // column order
        // 1. designation
        // 2. recorded_date
        // 3. cash_amount
        // 4. currency
        // 5. partner_name
        // 6. reference
        // 7. posting_date
        // 8. sale_target
        // 9. partner
        // 10. transaction_content
        // 11. transaction_id
        // 12. company_name
        // 13. company_id
        if (count($tokens) != 13) {
            $rowResult['error'] = 'Invalid format, expecting 13 tokens, '.count($tokens).' found';
            $rowResult['error_type'] = Constants::ERROR_TYPE_FORMAT_ERROR;
            return $rowResult;
        }

        /////////////////////////////////
        // x. Validate tokens
        $designation = trim($tokens[0]);
        $recorded_date = trim($tokens[1]);
        $cash_amount = trim($tokens[2]);
        $currency = trim($tokens[3]);
        $partner_name = trim($tokens[4]);
        $reference = trim($tokens[5]);
        $posting_date = trim($tokens[6]);
        $sale_target = trim($tokens[7]);
        $partner = trim($tokens[8]);
        $transaction_content = trim($tokens[9]);
        $transaction_id = trim($tokens[10]);
        $company_name = trim($tokens[11]);
        $company_id = trim($tokens[12]);

        // Check if there's any duplication on Transaction Ids
        if ($transaction_id) {
            if (array_key_exists($transaction_id, $this->arrBeingSubmitedTransactionIds)) {
                // mark error
                $rowResult['error'] = "Duplication on TransactionId '".$transaction_id."'";
                $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
                return $rowResult;
            } else {
                // add to existing list of available transactionIds for temporary use
                $this->arrBeingSubmitedTransactionIds[$transaction_id] = 1;
            }
        }

        /////////////////////////////////
        // x. Check designation, must be string
        $rowResult['designation'] = ($designation) ? $designation : null;

        /////////////////////////////////
        // x. Check recorded_date, must be date ('dd/mm/yyyy')
        $date_regex = Constants::CASH_DATA_FORMAT_REGEX;

        if (!preg_match($date_regex, $recorded_date)) {
            $rowResult['error'] = "Invalid Recorded Date '".$recorded_date."'";
            $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }
        // save as year for ease in calculating later
        $tmpRecordedDateArr = explode('/', $recorded_date);
        $rowResult['recorded_month'] = intval($tmpRecordedDateArr[1]);
        $rowResult['recorded_year'] = intval($tmpRecordedDateArr[2]);
        $rowResult['recorded_date'] = date_create($tmpRecordedDateArr[2] . '-' . $tmpRecordedDateArr[1] . '-' . $tmpRecordedDateArr[0]);

        /////////////////////////////////
        // x. Check cash_amount, must be float
        if (!is_numeric($cash_amount)) {
            $rowResult['error'] = "Invalid Cash Amount '".$cash_amount."'";
            $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
            return $rowResult;
        }
        $rowResult['cash_amount'] = floatval($cash_amount);

        /////////////////////////////////
        // x. Check currency, must be string
        $rowResult['currency'] = ($currency) ? $currency : null;

        /////////////////////////////////
        // x. Check partner_name, must be string
        $rowResult['partner_name'] = ($partner_name) ? $partner_name : null;

        /////////////////////////////////
        // x. Check reference, must be string
        $rowResult['reference'] = ($reference) ? $reference : null;

        /////////////////////////////////
        // x. Check posting_date, must be date ('dd/mm/yyyy')
        $rowResult['posting_year'] = $rowResult['posting_month'] = null;
        $rowResult['posting_date'] = ($posting_date) ? $posting_date : null;
        if ($posting_date) {
            if (!preg_match($date_regex, $posting_date)) {
                $rowResult['error'] = "Invalid Posting Date '".$posting_date."'";
                $rowResult['error_type'] = Constants::ERROR_TYPE_DATA_ERROR;
                return $rowResult;
            }
            // save as year for ease in calculating later
            $tmpPostingDateArr = explode('/', $posting_date);
            $rowResult['posting_month'] = intval($tmpPostingDateArr[1]);
            $rowResult['posting_year'] = intval($tmpPostingDateArr[2]);
            $rowResult['posting_date'] = date_create($tmpPostingDateArr[2] . '-' . $tmpPostingDateArr[1] . '-' . $tmpPostingDateArr[0]);
        }

        /////////////////////////////////
        // x. Check sale_target, must be string
        $rowResult['sale_target'] = ($sale_target) ? $sale_target : null;

        /////////////////////////////////
        // x. Check partner, must be string
        $rowResult['partner'] = ($partner) ? $partner : null;

        /////////////////////////////////
        // x. Check transaction_content, must be string
        $rowResult['transaction_content'] = ($transaction_content) ? $transaction_content : null;

        /////////////////////////////////
        // x. Check transaction_id, must be string
        $rowResult['transaction_id'] = ($transaction_id) ? $transaction_id : null;

        /////////////////////////////////
        // x. Check company_name, must be string
        $rowResult['company_name'] = ($company_name) ? $company_name : null;

        /////////////////////////////////
        // x. Check company_id, must be string
        $rowResult['company_id'] = ($company_id) ? $company_id : null;

        return $rowResult;
    }
}
