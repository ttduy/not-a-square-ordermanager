<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppFilterHandler;

class SummaryFilterHandler extends AppFilterHandler {
    public function getDefaultFormModel() {
        $model = new SummaryFilterModel();
        $today = new \DateTime();
        $model->year = intval($today->format('Y'));

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');  

        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => false
        ));

        return $builder;
    }
}