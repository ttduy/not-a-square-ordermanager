<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;

class SubmitCashDataHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new SubmitCashDataModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');
        
        $builder->add('result', 'textarea', array(
            'label'    => 'Result',
            'required' => true,
            'attr'     => array(
                'style'   => 'width: 800px;height: 400px'
            )
        ));
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();        
        $parser = $this->container->get('leep_admin.finance_tracker.business.submit_cash_data_parser');
        $cacheBox = $this->container->get('leep_admin.helper.cache_box_data');

        $doctrine = $this->container->get('doctrine');
        $model = $this->getForm()->getData();
        
        $parser->init();
        $result = $parser->parseResult($model->result);

        $errorRecords = array(
            Constants::ERROR_TYPE_FORMAT_ERROR      => array(),
            Constants::ERROR_TYPE_DATA_ERROR        => array(),
            Constants::TYPE_SUCCESS                 => array()
        );

        foreach ($result as $i => $row) {
            if ($row['error'] == '') {
                if ($row['cash_amount'] >= 0) {
                    $record_entry = new Entity\CashIn();
                } else {
                    $record_entry = new Entity\CashOut();
                }
                $record_entry->setDesignation($row['designation']);
                $record_entry->setRecordedDate($row['recorded_date']);
                $record_entry->setRecordedMonth($row['recorded_month']);
                $record_entry->setRecordedYear($row['recorded_year']);
                $record_entry->setCashAmount($row['cash_amount']);
                $record_entry->setCurrency($row['currency']);
                $record_entry->setPartnerName($row['partner_name']);
                $record_entry->setReference($row['reference']);
                $record_entry->setPostingDate($row['posting_date']);
                $record_entry->setPostingMonth($row['posting_month']);
                $record_entry->setPostingYear($row['posting_year']);
                $record_entry->setSaleTarget($row['sale_target']);
                $record_entry->setPartner($row['partner']);
                $record_entry->setTransactionContent($row['transaction_content']);
                $record_entry->setTransactionId($row['transaction_id']);
                $record_entry->setCompanyName($row['company_name']);
                $record_entry->setCompanyId($row['company_id']);
                $em->persist($record_entry);

                // Mark as added
                $result[$i]['isAdded'] = true;

                $errorRecords[Constants::TYPE_SUCCESS][] = $row;
            }
            else if ($row['error'] != '') {
                $idType = intval($row['error_type']);
                $errorRecords[$idType][] = $row;
            }
        }
        $em->flush();
            
        // Save history
        $submitDataHistory = new Entity\CashFlowSubmitDataHistory();
        $submitDataHistory->setTimestamp(new \DateTime());
        $submitDataHistory->setData($model->result);
        $submitDataHistory->setPreviewData(substr($model->result, 0, 100));
        $submitDataHistory->setNumFormatError(count($errorRecords[Constants::ERROR_TYPE_FORMAT_ERROR]));
        $submitDataHistory->setNumDataError(count($errorRecords[Constants::ERROR_TYPE_DATA_ERROR]));
        $submitDataHistory->setNumSuccess(count($errorRecords[Constants::TYPE_SUCCESS]));
        $em->persist($submitDataHistory);
        $em->flush();

        // Save error
        foreach ($errorRecords as $idType => $errors) {
            foreach ($errors as $error) {
                $submitDataHistoryError = new Entity\CashFlowSubmitDataHistoryError();
                $submitDataHistoryError->setIdCashFlowSubmitDataHistory($submitDataHistory->getId());
                $submitDataHistoryError->setIdErrorType($idType);
                $submitDataHistoryError->setErrorData(serialize($error));
                $em->persist($submitDataHistoryError);    
            }
        }
        $em->flush();

        $cacheBox->putKey('cashDataResult', $result);
    }
}
