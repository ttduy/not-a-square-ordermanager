<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Leep\AdminBundle\Business\Base\AppGridDataReader;
use Leep\AdminBundle\Business;

class CostTrackerHistoryGridReader extends AppGridDataReader {
    public $filters;
    public $currentTimestamp;

    public function __construct($container) {
        parent::__construct($container);
        $this->formatter = $this->container->get('leep_admin.helper.formatter');

        $today = new \DateTime();
        $this->currentTimestamp = $today->getTimestamp();
    }
    
    public function getColumnMapping() {
        return array('timestamp', 'previewData', 'action');
    }
    
    public function getTableHeader() {
        return array(        
            array('title' => 'Date Time',   'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Data',        'width' => '70%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false')
        );
    }

    public function getFormatter() {
        return $this->container->get('easy_formatter')->getFormatter('AppAdmin_CostTrackerHistoryGrid');
    }

    public function postQueryBuilder($queryBuilder) {
        //echo $this->filters->dateFrom;die();
        $queryBuilder->orderBy('p.timestamp', 'DESC');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, p.timestamp, p.previewData')
            ->from('AppDatabaseMainBundle:CostTrackerSubmitDataHistory', 'p');

        if (!empty($this->filters->dateFrom)) {
            $queryBuilder->andWhere('p.timestamp >= :dateFrom')
                ->setParameter('dateFrom', $this->filters->dateFrom);
        }

        if (!empty($this->filters->dateTo)) {
            $queryBuilder->andWhere('p.timestamp <= :dateTo')
                ->setParameter('dateTo', $this->filters->dateTo);
        }
    }

    public function buildCellPreviewData($row) {
        $mgr = $this->getModuleManager();

        $html = nl2br($row['previewData']).'...<br/>';
        $html .= '<a href="'.$mgr->getUrl('leep_admin', 'finance_tracker', 'finance_tracker', 'downloadHistory', array('id' => $row['id'])).'">Download</a>';

        return $html;
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->container->get('easy_module.manager');

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('financeTrackerView')) {
            $builder->addButton('Update', $mgr->getUrl('leep_admin', 'finance_tracker', 'submit_cost_tracker', 'create', array('id' => $row['id'])), 'button-edit');
        }

        return $builder->getHtml();
    }
}
