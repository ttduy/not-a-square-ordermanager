<?php
namespace Leep\AdminBundle\Business\FinanceTracker;

use Easy\ModuleBundle\Module\AbstractHook;

class CashOutGridHook extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $mgr = $controller->get('easy_module.manager');
        $data['submitCashDataUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'submit_cash_data', 'create');
		$data['bulkChangeCostTrackerSectorURL'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'bulk_change_cost_tracker_sector', 'create');
		$data['createCashOutUrl'] = $mgr->getUrl('leep_admin', 'finance_tracker', 'cash_out_create', 'create');
    }
}
