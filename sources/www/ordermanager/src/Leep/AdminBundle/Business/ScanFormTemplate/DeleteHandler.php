<?php 
namespace Leep\AdminBundle\Business\ScanFormTemplate;

use Leep\AdminBundle\Business\Base\AppDeleteHandler;
use App\Database\MainBundle\Entity;

class DeleteHandler extends AppDeleteHandler {
    public function __construct($container) {
        parent::__construct($container);
    }

    public function execute() {
        $id = $this->container->get('request')->query->get('id', 0);
        $em = $this->container->get('doctrine')->getEntityManager();

        $scanFormTemplate = $em->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($id);
        
        // remove related zones
        $queryDel = $em->createQueryBuilder();
        $queryDel->delete('AppDatabaseMainBundle:ScanFormTemplateZone', 'p')
                ->andWhere('p.idScanFormTemplate = :id')
                ->setParameter('id', $scanFormTemplate->getId())
                ->getQuery()
                ->execute();

        // remove template file
        $attachmentDir = $this->container->getParameter('attachment_dir') . '/'. Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;
        @unlink($attachmentDir . '/' . $scanFormTemplate->getTemplateFile());

        $em->remove($scanFormTemplate);
        $em->flush();
    }
}