<?php
namespace Leep\AdminBundle\Business\ScanFormTemplate;

use Leep\AdminBundle\Business\Base\AppEditHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EditHandler extends AppEditHandler {
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormTemplate')->findOneById($id);
    }
    public function convertToFormModel($entity) { 
        $model = new EditModel(); 
        $model->name = $entity->getName();
        $model->idFormulaTemplateCreate = $entity->getIdFormulaTemplateCreate();
        $model->idFormulaTemplateUpdate = $entity->getIdFormulaTemplateUpdate();

        $model->templateFile = array();
        $model->templateFile['file'] = $entity->getTemplateFile();

        $model->zones = $this->getExistingRegisteredZones($entity->getId());

        return $model;
    }

    private function getExistingRegisteredZones($id) {
        $result = $this->container->get('doctrine')->getRepository('AppDatabaseMainBundle:ScanFormTemplateZone')->findByIdScanFormTemplate($id);
        $arrZones = array();
        foreach ($result as $entry) {
            $arrZones[$entry->getId()] = array(
                'id'                    => $entry->getId(),
                'pageNumber'            => $entry->getPageNumber(),
                'zonesDefinition'       => $entry->getZonesDefinition()
            );
        }

        return $arrZones;
    }

    public function buildForm($builder) {
        $mapping = $this->container->get('easy_mapping');

        $builder->add('sectionScanFormInfo', 'section', array(
            'label'     => 'Scan Form Info'
        ));

        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => true
        ));        

        $builder->add('templateFile', 'app_file_uploader', array(
            'label' => 'Template File',
            'required' => false,
            'path'     => Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER,
            'info'     => 'Upload the pdf template file',
            'isShowViewFile' => false
        ));

        $builder->add('idFormulaTemplateCreate', 'choice', array(
            'label'     => 'Formula Template - Create order',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_FormulaTemplate_List')
        ));

        $builder->add('idFormulaTemplateUpdate', 'choice', array(
            'label'     => 'Formula Template - Update order',
            'required'  => false,
            'choices'   => $mapping->getMapping('LeepAdmin_FormulaTemplate_List')
        ));

        $builder->add('sectionZone', 'section', array(
            'label'     => 'Extraction Zones'
        ));

        $builder->add('zones', 'container_collection', array(
            'label'     => 'Zones',
            'required'  => false,
            'type'      =>  'app_zone_block'
        ));

        return $builder;
    }

    public function onSuccess() {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $model = $this->getForm()->getData();
        
        $this->entity->setName($model->name);

        if ($model->idFormulaTemplateCreate) {
            $this->entity->setIdFormulaTemplateCreate($model->idFormulaTemplateCreate);
        } else {
            $this->entity->setIdFormulaTemplateCreate(null);
        }

        if ($model->idFormulaTemplateUpdate) {
            $this->entity->setIdFormulaTemplateUpdate($model->idFormulaTemplateUpdate);
        } else {
            $this->entity->setIdFormulaTemplateUpdate(null);
        }

        // check file upload
        $fileUpload = $model->templateFile['attachment'];
        if ($fileUpload) {
            $attachmentDir = $this->container->getParameter('files_dir') . '/'. Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;
            $name = $this->entity->getId() .'.'.pathinfo($fileUpload->getClientOriginalName(), PATHINFO_EXTENSION);

            if ($fileUpload->getMimeType() != 'application/pdf') {
                $this->errors[] = 'Template File must be PDF';
                return false;
            }

            $fileUpload->move($attachmentDir, $name);
            $this->entity->setTemplateFile($name);
        }

        // update zones
        $dbHelper = $this->container->get('leep_admin.helper.database');
        $filters = array('idScanFormTemplate' => $this->entity->getId());
        $dbHelper->delete($em, 'AppDatabaseMainBundle:ScanFormTemplateZone', $filters);
        if ($model->zones) {
            foreach ($model->zones as $entry) {
                $zone = new Entity\ScanFormTemplateZone();
                $zone->setIdScanFormTemplate($this->entity->getId());
                $zone->setPageNumber($entry['pageNumber']);
                $zone->setZonesDefinition($entry['zonesDefinition']);                
                $em->persist($zone);
                $em->flush();                
            }
            $em->flush();
        }  

        // On create / edit, set is_parse_expected_zone = false
        $this->entity->setIsParseExpectedZone(0);

        parent::onSuccess();
    }
}