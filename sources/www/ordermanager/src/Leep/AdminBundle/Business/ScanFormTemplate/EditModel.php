<?php
namespace Leep\AdminBundle\Business\ScanFormTemplate;

class EditModel {
    public $name;
    public $templateFile;
    public $pageWidth;
    public $idFormularTemplateCreate;
    public $idFormularTemplateUpdate;
    public $zones;
}