<?php
namespace Leep\AdminBundle\Business\ScanFormTemplate;
         
use Leep\AdminBundle\Business\Base\AppGridDataReader;

class GridReader extends AppGridDataReader {
    public $filters;
    public function getColumnMapping() {
        return array('id', 'name', 'templateFile', 'action');
    }
    
    public function getTableHeader() {
        return array(
            array('title' => 'Id',          'width' => '10%', 'sortable' => 'false'),
            array('title' => 'Name',        'width' => '55%', 'sortable' => 'false'),
            array('title' => 'Template',    'width' => '15%', 'sortable' => 'false'),
            array('title' => 'Action',      'width' => '15%', 'sortable' => 'false'),
        );
    }

    public function getFormatter() {
        //return $this->container->get('easy_formatter')->getFormatter('LeepAdmin_AcquisiteurGridReader');
        return null;
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('AppDatabaseMainBundle:ScanFormTemplate', 'p');
        
        if (trim($this->filters->name) != '') {
            $queryBuilder->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.trim($this->filters->name).'%');
        }
    }

    public function buildCellAction($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormModify')) {
            $builder->addButton('Edit', $mgr->getUrl('leep_admin', 'scan_form_template', 'edit', 'edit', array('id' => $row->getId())), 'button-edit');
            $builder->addConfirmButton('Delete', $mgr->getUrl('leep_admin', 'scan_form_template', 'delete', 'delete', array('id' => $row->getId())), 'button-delete');
        }

        return $builder->getHtml();
    }

    public function buildCellTemplateFile($row) {
        $builder = $this->container->get('leep_admin.helper.button_list_builder');
        $mgr = $this->getModuleManager();

        $helperSecurity = $this->container->get('leep_admin.helper.security');
        if ($helperSecurity->hasPermission('scanFormView')) {
            $builder->addButton('Download', $mgr->getUrl('leep_admin', 'scan_form_template', 'feature', 'downloadTemplate', array('id' => $row->getId())), 'button-pdf');
        }

        return $builder->getHtml();
    }
}