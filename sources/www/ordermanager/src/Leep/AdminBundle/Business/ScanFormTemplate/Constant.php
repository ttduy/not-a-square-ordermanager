<?php 
namespace Leep\AdminBundle\Business\ScanFormTemplate;

class Constant {
    const SCAN_FORM_TEMPLATE_FILE_FOLDER = 'scan_form/templates';

    const ZONE_TYPE_GENERAL_TEXTBOX             = 10;
    const ZONE_TYPE_GENERAL_TEXTAREA            = 20;
    const ZONE_TYPE_GENERAL_DATETIME            = 30;
    const ZONE_TYPE_GENERAL_CHECKBOX            = 40;
    const TEMPLATE_DETECTION_ZONE               = 50;
    const ZONE_TYPE_COMBOBOX                    = 60;
    const ZONE_TYPE_FORM_QUESTION               = 61;

    const ZONE_TYPE_COUNTRY                     = 70;
    const ZONE_TYPE_DISTRIBUTION_CHANNEL        = 71;
    const ZONE_TYPE_GENDER                      = 72;
    const ZONE_TYPE_LANGUAGE                    = 73;
    const ZONE_TYPE_PRODUCT                     = 74;
    const ZONE_TYPE_REPORT_TYPE                 = 75;



    public static function getZoneTypeList() {
        return array(
            self::ZONE_TYPE_GENERAL_TEXTBOX             => 'General - Text Box',
            self::ZONE_TYPE_GENERAL_TEXTAREA            => 'General - Text Area',
            self::ZONE_TYPE_GENERAL_DATETIME            => 'General - Datetime',
            self::ZONE_TYPE_GENERAL_CHECKBOX            => 'General - Checkbox',
            self::ZONE_TYPE_COMBOBOX                    => 'General - Combobox',
            self::ZONE_TYPE_FORM_QUESTION               => 'General - Form Question',
            self::TEMPLATE_DETECTION_ZONE               => 'Template detection zone',
            self::ZONE_TYPE_COUNTRY                     => 'List - Country',
            self::ZONE_TYPE_DISTRIBUTION_CHANNEL        => 'List - Distribution Channel',
            self::ZONE_TYPE_GENDER                      => 'List - Gender',
            self::ZONE_TYPE_LANGUAGE                    => 'List - Language',
            self::ZONE_TYPE_PRODUCT                     => 'List - Product',
            self::ZONE_TYPE_REPORT_TYPE                 => 'List - Report Type'
        );
    }

    public static function getZoneTypeHandlers() {
        return array(
            self::ZONE_TYPE_GENERAL_TEXTBOX             => 'leep_admin.scan_file.business.zone_type.general_text_box_type',
            self::ZONE_TYPE_GENERAL_TEXTAREA            => 'leep_admin.scan_file.business.zone_type.general_text_area_type',
            self::ZONE_TYPE_GENERAL_DATETIME            => 'leep_admin.scan_file.business.zone_type.general_date_time_type',
            self::ZONE_TYPE_GENERAL_CHECKBOX            => 'leep_admin.scan_file.business.zone_type.general_checkbox_box_type',
            self::ZONE_TYPE_COMBOBOX                    => 'leep_admin.scan_file.business.zone_type.combobox_type',
            self::ZONE_TYPE_FORM_QUESTION               => 'leep_admin.scan_file.business.zone_type.form_question_type',
            self::TEMPLATE_DETECTION_ZONE               => 'leep_admin.scan_file.business.zone_type.template_detection_type',
            self::ZONE_TYPE_COUNTRY                     => 'leep_admin.scan_file.business.zone_type.country_type',
            self::ZONE_TYPE_DISTRIBUTION_CHANNEL        => 'leep_admin.scan_file.business.zone_type.distribution_channel_type',
            self::ZONE_TYPE_GENDER                      => 'leep_admin.scan_file.business.zone_type.gender_type',
            self::ZONE_TYPE_LANGUAGE                    => 'leep_admin.scan_file.business.zone_type.language_type',
            self::ZONE_TYPE_PRODUCT                     => 'leep_admin.scan_file.business.zone_type.product_type',
            self::ZONE_TYPE_REPORT_TYPE                 => 'leep_admin.scan_file.business.zone_type.report_type_type',
        );
    }

    public static function getZoneTypeCode() {
        return array(
            self::ZONE_TYPE_GENERAL_TEXTBOX             => 'TextBox',
            self::ZONE_TYPE_GENERAL_TEXTAREA            => 'TextArea',
            self::ZONE_TYPE_GENERAL_DATETIME            => 'Date',
            self::ZONE_TYPE_GENERAL_CHECKBOX            => 'CheckBox',
            self::ZONE_TYPE_COMBOBOX                    => 'Combobox',
            self::ZONE_TYPE_FORM_QUESTION               => 'FormQuestion',
            self::TEMPLATE_DETECTION_ZONE               => 'TemplateDetectionZone',
            self::ZONE_TYPE_COUNTRY                     => 'Country',
            self::ZONE_TYPE_DISTRIBUTION_CHANNEL        => 'DistributionChannel',
            self::ZONE_TYPE_GENDER                      => 'Gender',
            self::ZONE_TYPE_LANGUAGE                    => 'Language',
            self::ZONE_TYPE_PRODUCT                     => 'Product',
            self::ZONE_TYPE_REPORT_TYPE                 => 'ReportType'
        );
    }

    const CHOICE_TYPE_DISTRIBUTION_CHANNEL = 10;
    const CHOICE_TYPE_COUNTRY              = 20;
    public static function getRevertChoiceType() {
        return array(
            self::CHOICE_TYPE_DISTRIBUTION_CHANNEL => 'DistributionChannel',
            self::CHOICE_TYPE_COUNTRY              => 'Country'
        );
    }

    const FORMULA_TEMPLATE_CREATE_ONE    = 10;
    const FORMULA_TEMPLATE_CREATE_TWO    = 20;

    public static function getFormulaTemplateCreateList() {
        return array(
            self::FORMULA_TEMPLATE_CREATE_ONE       => 'Formula Template Create 1',
            self::FORMULA_TEMPLATE_CREATE_TWO       => 'Formula Template Create 2'
        );
    }

    const FORMULA_TEMPLATE_UPDATE_ONE    = 10;
    const FORMULA_TEMPLATE_UPDATE_TWO    = 20;

    public static function getFormulaTemplateUpdateList() {
        return array(
            self::FORMULA_TEMPLATE_UPDATE_ONE       => 'Formula Template Update 1',
            self::FORMULA_TEMPLATE_UPDATE_TWO       => 'Formula Template Update 2'
        );
    }
}
