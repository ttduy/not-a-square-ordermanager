<?php
namespace Leep\AdminBundle\Business\ScanFormTemplate;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        
        return $model;
    }

    public function buildForm($builder) {        
        $builder->add('name', 'text', array(
            'label' => 'Name',
            'required' => true
        ));        

        $builder->add('templateFile', 'app_file_uploader', array(
            'label' => 'Template File',
            'required' => true,
            'path'     => Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER,
            'info'     => 'Upload the pdf template file'
        ));

        return $builder;
    }

    public function onSuccess() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $model = $this->getForm()->getData();

        $scanFormTemplate = new Entity\ScanFormTemplate();
        $scanFormTemplate->setName($model->name);
        $scanFormTemplate->setIsParseExpectedZone(0);
        $em->persist($scanFormTemplate);
        $em->flush();

        // check file upload
        $fileUpload = $model->templateFile['attachment'];
        if ($fileUpload) {
            $attachmentDir = $this->container->getParameter('files_dir') . '/'. Constant::SCAN_FORM_TEMPLATE_FILE_FOLDER;
            $name = $scanFormTemplate->getId() .'.'.pathinfo($fileUpload->getClientOriginalName(), PATHINFO_EXTENSION);
            $mimeType = $fileUpload->getMimeType();

            // check if it's pdf
            if ($fileUpload->getMimeType() != 'application/pdf') {
                $this->errors[] = 'Template File must be PDF';
                return false;
            }

            $fileUpload->move($attachmentDir, $name);

            $scanFormTemplate->setTemplateFile($name);
            $em->persist($scanFormTemplate);
            $em->flush();
        }

        parent::onSuccess();
    }
}