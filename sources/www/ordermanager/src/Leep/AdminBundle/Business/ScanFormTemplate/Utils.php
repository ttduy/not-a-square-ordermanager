<?php
namespace Leep\AdminBundle\Business\ScanFormTemplate;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class Utils {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function parseZonesDefinition($zoneDefinition) {
        $rows = explode("\n", $zoneDefinition);
        $zones = array();
        $errors = array();
        $i = 0;

        $revertCodeMap = array();
        $codes = Constant::getZoneTypeCode();
        foreach ($codes as $i => $code) {
            $revertCodeMap[strtolower($code)] = $i;
        }

        $revertChoiceTypeMap = array();
        $types = Constant::getRevertChoiceType();
        foreach ($types as $i => $t) {
            $revertChoiceTypeMap[strtolower($t)] = $i;
        }

        foreach ($rows as $r) {
            $zone = array();
            $i++;
            $tokens = explode("|", $r);
            $params = array();
            if (count($tokens) > 1) {
                if (isset($tokens[0])) {
                    $coords = explode(",", $tokens[0]);
                    if (count($coords) == 4) {
                        $zone['x'] = intval($coords[0]);
                        $zone['y'] = intval($coords[1]);
                        $zone['width'] = intval($coords[2]);
                        $zone['height'] = intval($coords[3]);
                    }
                }

                if (isset($tokens[1])) {
                    $zone['key'] = trim($tokens[1]);
                }

                if (isset($tokens[2])) {
                    $subValues = explode(",", $tokens[2]);
                    foreach ($subValues as $sub) {
                        $subTokens = explode('=', $sub);
                        if (count($subTokens) == 2) {
                            $key = trim($subTokens[0]);
                            $val = trim($subTokens[1]);
                            if ($key == 'Zone') {
                                $val = strtolower($val);
                                if (isset($revertCodeMap[$val])) {
                                    $idType = $revertCodeMap[$val];
                                    $zone['idType'] = $idType;
                                    $zone['zone'] = $val;
                                }
                                else {
                                    $errors[] = "Line ".$i.": ".$r." (WRONG Zone Code)";
                                    continue;
                                }
                            }
                            else if ($key == 'CreationRequired') {
                                $zone['isCreationRequired'] = true;
                            }
                            else {
                                $params[$key] = $val;
                            }
                        }
                    }
                }

                $zone['params'] = $params;

                if (!isset($zone['x']) ||
                    !isset($zone['y']) ||
                    !isset($zone['width']) ||
                    !isset($zone['height']) ||
                    !isset($zone['key']) ||
                    !isset($zone['idType'])) {
                    $errors[] = "Line ".$i.": ".$r;
                }
                else {
                    $zones[] = $zone;
                }
            }
        }

        return array(
            'zones'   => $zones,
            'errors'  => $errors
        );
    }
}
