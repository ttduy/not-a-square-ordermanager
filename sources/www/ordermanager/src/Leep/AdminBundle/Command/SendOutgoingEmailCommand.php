<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\EmailOutgoing\Constant;
use Leep\AdminBundle\Business\EmailMassSender\Util as MassSenderUtil;

class SendOutgoingEmailCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:send_outgoing_email')
            ->setDescription('Send Outgoing Email');
    }

    private function log($email, $message, $status = null) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Check number of log
        $allLog = $em->getRepository('AppDatabaseMainBundle:EmailOutgoingLog')->findBy(
            ['idEmailOutgoing' => $email->getId()],
            ['logTimestamp' => 'ASC']);

        // Only save 100 log message
        while (count($allLog) > 99) {
            $first = array_shift($allLog);
            $em->remove($first);
        }

        $em->flush();


        $log = new Entity\EmailOutgoingLog();
        $log->setIdEmailOutgoing($email->getId());
        $log->setLogMessage($message);
        $log->setLogTimestamp(new \DateTime());
        $em->persist($log);

        $email->setLastMessage($message);
        $email->setLastUpdated(new \DateTime());
        if ($status) {
            $email->setStatus($status);
        }

        $em->persist($email);
        $em->flush();
    }

    private function getMailerList() {
        $result = array();

        echo "Creating Mailer List.\n";

        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $emailMethodList = $em->getRepository('AppDatabaseMainBundle:EmailMethod')->findAll();
        if ($emailMethodList) {
            foreach ($emailMethodList as $emailMethod) {
                $transport = \Swift_SmtpTransport::newInstance()
                    ->setHost($emailMethod->getHost())
                    ->setPort($emailMethod->getPort())
                    ->setUsername($emailMethod->getUsername())
                    ->setPassword($emailMethod->getPassword())
                    ->setEncryption($emailMethod->getEncryption());

                echo "Host: ".$emailMethod->getHost().".\n";
                echo "Port: ".$emailMethod->getPort().".\n";
                echo "Username: ".$emailMethod->getUsername().".\n";
                echo "Password: ".$emailMethod->getPassword().".\n";
                echo "Encryption: ".$emailMethod->getEncryption().".\n";

                $mailer = \Swift_Mailer::newInstance($transport);
                $result[$emailMethod->getId()] = array(
                    'mailer' =>         $mailer,
                    'sender-name' =>    $emailMethod->getSenderName(),
                    'sender-email' =>   $emailMethod->getSenderEmail()
                );
            }
        }

        return $result;
    }

    private function getAttachments($email) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $attachmentList = $em->getRepository('AppDatabaseMainBundle:EmailOutgoingAttachment')
            ->findByIdEmailOutgoing($email->getId());

        $result = array();
        foreach ($attachmentList as $attachment) {
            $fileName = $attachment->getName();
            $folder = Constant::EMAIL_ATTACHMENT_FOLDER;
            $fileDir = "/var/www/ordermanager/files/$folder/$fileName";
            echo "Attach file: $fileDir.\n";

            if (!file_exists($fileDir)) {
                echo "Warning: Attachment files not existed. Skip file.\n";
                $this->log($email, "Warning: Attachment files not existed. Skip file.", Constant::EMAIL_STATUS_ERROR);
                continue;
            }

            $result[] = \Swift_Attachment::fromPath($fileDir)
                ->setFilename($attachment->getOriginalName());
        }

        return $result;
    }

    private function prepareEmailContent($idQueue, $body, $recipient, $type, $id) {
        $container = $this->getContainer();
        $helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $helperCommon = $container->get('leep_admin.helper.common');

        $optOutUrl = $container->getParameter('application_url').'/email_mass_sender/feature/optOut?';
        $webLoginOptOutUrl = $container->getParameter('application_url').'/web_user/feature/webLoginOptOut?';

        if (in_array($idQueue, Constant::getWebLoginQueues())) {
            $optOutUrl = $webLoginOptOutUrl;
        }

        $arrParams = array(
            'email' =>  $helperEncrypt->encrypt(implode('; ', $recipient['email'])),
            'name' =>   $helperEncrypt->encrypt($recipient['name']),
            'type' =>   $helperEncrypt->encrypt($type),
            'id' =>     $helperEncrypt->encrypt($id)
        );

        $optOutUrl .= http_build_query($arrParams);

        $webLoginOptOutUrl .= http_build_query($arrParams);

        // replace {{ OptOutLink }} tag if exists (Or OPTOUTLINK)
        $body = $helperCommon->replaceDoubleBracesTag('OPTOUTLINK', $optOutUrl, $body);
        $body = $helperCommon->replaceDoubleBracesTag('OptOutLink', $optOutUrl, $body);
        $body = $helperCommon->replaceDoubleBracesTag('OPTOUT_WEBLOGIN', $webLoginOptOutUrl, $body);
        // $body = str_replace("\r\n", "\n", $body);
        // if (strpos($body, "\n") !== false) {
        //     $body = "<p>".str_replace("\n", "</p><p>", $body)."</p>";
        //     $body = str_replace("<p></p>", "</br>", $body);
        // }
        return $body;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $mailerList = $this->getMailerList();

        $mailRate = $this->getContainer()->get('leep_admin.helper.common')->getConfig()->getMassMailerRate();
        $emailList = $em->getRepository('AppDatabaseMainBundle:EmailOutgoing')->findBy(
            array(
                'isSendNow' => 1
            ),
            array(
                'creationDatetime' =>   'DESC',
                'id' =>                 'DESC'
            ), $mailRate);

        foreach ($emailList as $email) {
            $this->log($email, "Begin to send email.", Constant::EMAIL_STATUS_SENDING);
            echo "Sending Email #".$email->getId()."\n";

            if (!array_key_exists($email->getIdEmailMethod(), $mailerList)) {
                echo "Error: Email #".$email->getId()." does not have a valid Email Method.\n";
                $this->log($email, "Error: Invalid Email Method.", Constant::EMAIL_STATUS_ERROR);

                if (!$email->getIsAutoRetry()) {
                    $email->setIsSendNow(0);
                } else {
                    $this->log($email, "Error occur, retry!");
                }

                $em->persist($email);
                $em->flush();
                continue;
            }

            $mailer = $mailerList[$email->getIdEmailMethod()];
            $senderEmail = $mailer['sender-email'];
            $senderName = $mailer['sender-name'];
            $mailer = $mailer['mailer'];

            echo "From $senderName - $senderEmail \n";
            $recipients = MassSenderUtil::extractRecipient($email->getRecipients());
            $ccRecipients = MassSenderUtil::extractRecipient($email->getCc());

            // Get out out list
            if (in_array($email->getIdQueue(), Constant::getWebLoginQueues())) {
                $optOutList = $em->getRepository('AppDatabaseMainBundle:EmailWebloginOptOutList')->findAll();
            } else {
                $optOutList = $em->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findAll();
            }
            $blackList = [];

            foreach ($optOutList as $optOut) {
                $blackList[] = $optOut->getEmail();
            }

            $toList = array();
            foreach ($recipients as $recipient) {
                $toName = $recipient['name'];
                echo "To: $toName <".implode('; ', $recipient['email']).">\n";
                foreach ($recipient['email'] as $emailAddress) {
                    if (empty($emailAddress))
                        continue;

                    if (in_array($emailAddress, $blackList)) {
                        $this->log($email, "Email $emailAddress is in blacklist. Skip!", Constant::EMAIL_STATUS_SENDING);
                        continue;
                    }

                    if (!empty($toName)) {
                        $toList[$emailAddress] = $toName;
                    } else {
                        $toList[] = $emailAddress;
                    }
                }
            }

            $ccList = array();
            foreach ($ccRecipients as $recipient) {
                $toName = $recipient['name'];
                echo "CC: $toName <".implode('; ', $recipient['email']).">\n";
                foreach ($recipient['email'] as $emailAddress) {
                    if (empty($emailAddress))
                        continue;

                    if (!empty($toName)) {
                        $ccList[$emailAddress] = $toName;
                    } else {
                        $ccList[] = $emailAddress;
                    }
                }
            }

            if (empty($toList) && empty($ccList)) {
                $this->log($email, "No email to sent.", Constant::EMAIL_STATUS_SENT);
            } else {
                $success = true;
                try {
                    $emailBodyContent = $this->prepareEmailContent(
                            $email->getIdQueue(),
                            $email->getBody(),
                            $recipient,
                            $email->getRecipientsType(),
                            $email->getRecipientsId());

                    $defaultStyle="<style type='text/css'>img { max-width: 100%; }</style>";
                    $emailBodyContent = "$defaultStyle<div>$emailBodyContent</div>";

                    $message = \Swift_Message::newInstance()
                        ->setSubject($email->getTitle())
                        ->setFrom($senderEmail, $senderName)
                        ->setBody($emailBodyContent, 'text/html')
                        ->setTo($toList)
                        ->setCc($ccList);

                    $attachments = $this->getAttachments($email);
                    $this->log($email, "Attach ".count($attachments)." files.", Constant::EMAIL_STATUS_SENDING);
                    echo "Attach ".count($attachments)." files.\n";
                    foreach ($attachments as $attachment) {
                        $message->attach($attachment);
                    }

                    $result = $mailer->send($message);
                } catch (\Swift_TransportException $sTe) {
                    echo "Swift Transport Exception has occur: ".$sTe->getMessage().".\n";
                    $this->log($email, "Swift Transport Exception has occur: ".$sTe->getMessage().".",
                        Constant::EMAIL_STATUS_ERROR);
                    $success = false;
                } catch (\Exception $e) {
                    echo "Exception has occur: ".$e->getMessage().".\n";
                    $this->log($email, "Exception has occur: ".$e->getMessage().".",
                        Constant::EMAIL_STATUS_ERROR);
                    $success = false;
                }

                if ($success) {
                    echo "Success.\n";
                    $this->log($email, "Success, email sent.", Constant::EMAIL_STATUS_SENT);
                }
            }

            if (!$email->getIsAutoRetry() || $email->getStatus() != Constant::EMAIL_STATUS_ERROR) {
                $email->setIsSendNow(0);
            } else {
                $this->log($email, "Error occur, retry!");
            }

            $em->persist($email);
            $em->flush();
        }

        echo "Finished.\n";
    }
}
