<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ScanAllTextBlockReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:scan_all_text_block_report')
            ->setDescription('Scan All Textblock Report');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $extractor = $container->get('leep_admin.report.business.text_block_extractor');
        // Clear old data
        $query = $em->createQueryBuilder();
        echo("========================== Start scan ====================================\n");
        $query->select('p')
            ->from('AppDatabaseMainBundle:Report', 'p');
        $reports = $query->getQuery()->getResult();
        $count = 0;
        foreach ($reports as $report) {
            $errorBlocks = array();
            $delQuery = $em->createQueryBuilder();
            $delQuery->delete('AppDatabaseMainBundle:ReportTextBlock', 'p')
                ->andWhere('p.idReport = :idReport')
                ->setParameter('idReport', $report->getId())
                ->getQuery()->getResult();
            $textBlocks = $extractor->extractTextBlockFromReport($report->getId());
            foreach ($textBlocks as $textBlockName => $textBlockId) {
                if ($textBlockId != 0) {
                    $reportBlock = new Entity\ReportTextBlock();
                    $reportBlock->setIdReport($report->getId());
                    $reportBlock->setIdTextBlock($textBlockId);
                    $em->persist($reportBlock);
                    $count++;
                }
            }
            if ($count > 500) {
                $em->flush();
                $em->clear();
                $count = 0;
            }
        }

        $em->flush();
        $em->clear();
        echo("======================= End scan =======================================\n");
    }
}
