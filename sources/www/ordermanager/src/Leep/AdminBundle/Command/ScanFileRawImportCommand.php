<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ScanFileRawImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:scan_file_raw_import')
            ->setDescription('Scan File Raw Import');
    }

    public $em;
    const THRESHOLD = 90;
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getEntityManager();
        $scanFileHelper = $container->get('leep_admin.scan_file.business.helper');
        $today = new \DateTime();

        $scanFileRawDir = $scanFileHelper->getFileRawDir();
        $scanFileDir = $scanFileHelper->getFileDir();

        $currentTimestamp = $today->getTimestamp();

        $files = scandir($scanFileRawDir);
        foreach ($files as $fileName) {
            if ($fileName != '..' && $fileName != '.') {
                $file = $scanFileRawDir.'/'.$fileName;
                $lastModifiedTs = filemtime($file);
                if ($lastModifiedTs) {
                    $e = $currentTimestamp - $lastModifiedTs;
                    if ($e > self::THRESHOLD) {
                        $tokens = explode('.', $fileName);
                        array_pop($tokens);

                        $scanFile = new Entity\ScanFile();
                        $scanFile->setTimestamp($today);
                        $scanFile->setFilename(implode('_', $tokens).'.pdf');
                        $scanFile->setStatus(Business\ScanFile\Constant::SCAN_FILE_STATUS_ORDER_CREATE_PENDING);
                        $this->em->persist($scanFile);
                        $this->em->flush();    

                        $result = $this->move($file, $scanFileDir.'/'.$scanFile->getId().'.pdf');
                        if (!$result) {            
                            $scanFile->setStatus(Business\ScanFormFile\Constant::SCAN_FILE_STATUS_ERROR);
                            $scanFile->setErrorMessage("Can't import file from raw --> file");
                            $this->em->flush();
                        }                    


                        echo "Import: ".$fileName."\n";
                    }
                }
            }
        }
    }

    protected function move($file, $targetFile) {
        $tokens = explode('.', $file);
        $ext = strtolower(array_pop($tokens));
        if ($ext == 'pdf') {
            $result = rename($file, $targetFile);
            @chmod($targetFile, 0777);

            return $result;
        }
        else if ($ext == 'jpg' || $ext == 'png') {
            $container = $this->getContainer();
            $pdf = $container->get('white_october.tcpdf')->create();   
            $pdf->setPrintFooter(false);
            $pdf->setPrintHeader(false);
            $pdf->setPageUnit('pt');
            $pdf->SetMargins(0,0,0,true);
            $pdf->setPageOrientation('P', false, 0);
            $pdf->addPage();
            $imageWidthResized = $pdf->getPageWidth();
            $imageSize = @getimagesize($file);
            if ($imageSize[0] == 0) {
                $imageSize[0] = $imageWidthResized;
            }
            $imageHeightResized = $imageWidthResized * $imageSize[1] / $imageSize[0];

            $pdf->image($file,
                $x = 0,
                $y = 0,
                $w = 0,
                $h = 0,
                $type = '',
                $link = '',
                $align = '',
                $resize = true,
                $dpi = 300,
                $palign = '',
                $ismask = false,
                $imgmask = false,
                $border = 0,
                $fitbox = false,
                $hidden = false,
                $fitonpage = true
            );

            $pdf->output($targetFile, 'F');
            @chmod($targetFile, 0777);

            unlink($file);            
            return true;
        }
        else {
            return false;
        }
    }
}
