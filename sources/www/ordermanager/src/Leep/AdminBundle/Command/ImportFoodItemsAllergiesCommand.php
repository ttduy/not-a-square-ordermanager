<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ImportFoodItemsAllergiesCommand extends ContainerAwareCommand {
    protected function configure() {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:import_food_item_allergies')
            ->setDescription('Import allergies ingredients into food item');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Load csv data
        $filePath = $container->getParameter('files_dir').'/temp/food_items_with_allergies_v2.csv';
        $csvData = file_get_contents($filePath);

        if (!$csvData) {
            echo "Unable to open file: $filePath\n";
            return;
        }

        $lines = explode("\n", $csvData);
        $head = array_shift($lines);
        $head = array_map('trim', explode(';', $head));
        $result = [];
        foreach ($lines as $line) {
            $line = trim($line);
            if (empty($line)) {
                continue;
            }

            // Parse food item data
            $data = array_map('trim', explode(';', $line));

            // Add food item to result list
            $foodItemId = $data[1];
            $result[$foodItemId] = [];
            echo "Get data for Food item: $foodItemId\n";

            // Update allergies
            foreach ($data as $index => $value) {
                // Skip first two value
                if ($index <= 1) {
                    continue;
                }

                // Conver empty string to zero
                if (empty($value)) {
                    $value = 0;
                }

                // Save data
                $allergies = $head[$index];
                $result[$foodItemId][] = "$allergies = $value";
            }

            // Merge result in to one string
            $result[$foodItemId] = implode("\n", $result[$foodItemId]);
        }

        // Update food table ingredient
        echo "Update ingredient list";
        foreach ($head as $index => $ingredientName) {
            if ($index <= 1) {
                continue;
            }

            $ingredient = $em->getRepository('AppDatabaseMainBundle:FoodTableIngredient')->findOneByVariableName($foodItemId);
            if ($ingredient) {
                continue;
            }

            // Add new ingredient
            $ingredient = new Entity\FoodTableIngredient();
            $ingredient->setName("[[$ingredientName]]");
            $ingredient->setVariableName($ingredientName);
            $ingredient->setRiskAmountMax(9999);
            $em->persist($ingredient);
            $em->flush();
        }

        echo "Begin update food item in database\n";
        $process = 0;
        $step = 100 / count($result);
        $errors = [];
        foreach ($result as $foodItemId => $newIngredientData) {
            $process += $step;
            echo "Processing: $process%\n";

            // Get food item
            $foodItem = $em->getRepository('AppDatabaseMainBundle:FoodTableFoodItem')->findOneByFoodId($foodItemId);
            if (!$foodItem) {
                $errors[] = "Error: Food item $foodItemId not existed\n";
                continue;
            }

            // Update ingredient data
            $ingredientData = $foodItem->getIngredientData();
            $foodItem->setIngredientData("$ingredientData\n$newIngredientData");
            $em->persist($foodItem);
            $em->flush();
        }

        // Display all error
        echo "Number of error: ".count($errors)."\n";
        foreach ($errors as $error) {
            echo $error;
        }

        echo "Finished!\n";
    }
}
