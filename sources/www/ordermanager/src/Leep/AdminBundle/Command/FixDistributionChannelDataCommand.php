<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixDistributionChannelDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_distribution_channel_data')
            ->setDescription('Fix distribution channel data');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Find error distribution channel
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Log', 'p')
            ->andWhere('p.logTime > :logTime')
            ->setParameter('logTime', '2015-09-01 00:00:00');
        $results = $query->getQuery()->getResult();

        $distributionChannels = array();
        foreach ($results as $r) {
            $log = json_decode($r->getLogData(), true);
            if (isset($log['uri'])) {
                if (strpos($log['uri'], '/distribution_channel/edit/edit') === 0) {
                    if (intval($log['data']['invoiceGoesToId']) == 0) {
                        $id = intval($log['data']['id']);
                        $distributionChannels[$id] = 1;
                    }
                }
            }
        }

        // Check previous value
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Log', 'p')
            ->andWhere('p.logTime > :logTime')
            ->andWhere('p.logTime < :startTime')
            ->setParameter('logTime', '2014-01-01 00:00:00')            
            ->setParameter('startTime', '2015-10-14 00:00:00')
            ->orderBy('p.logTime', 'DESC');
        $results = $query->getQuery()->getResult();

        $dcUpdated = array();
        foreach ($results as $r) {
            $log = json_decode($r->getLogData(), true);
            if (isset($log['uri'])) {
                if (strpos($log['uri'], '/distribution_channel/edit/edit') === 0) {
                    $id = intval($log['data']['id']);
                    if (isset($distributionChannels[$id])) {
                        if (!isset($dcUpdated[$id])) {
                            $dcUpdated[$id] = $log['data'];
                        }
                    }
                }
            }
        }

        $formatter = $container->get('leep_admin.helper.formatter');
        foreach ($dcUpdated as $id => $d) {
            $idInvoiceGoesTo = isset($d['invoiceGoesToId']) ? $d['invoiceGoesToId'] : 0;
            $idMarginGoesTo = isset($d['idMarginGoesTo']) ? $d['idMarginGoesTo'] : 0;
            $idMarginPaymentMode = isset($d['idMarginPaymentMode']) ? $d['idMarginPaymentMode'] : 0;
            $idNutrimeGoesTo = isset($d['idNutrimeGoesTo']) ? $d['idNutrimeGoesTo'] : 0;

            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
            echo "Process ".$dc->getDistributionChannel()."\n";

            $dc->setInvoiceGoesToId($idInvoiceGoesTo);
            $dc->setIdMarginGoesTo($idMarginGoesTo);
            $dc->setIdMarginPaymentMode($idMarginPaymentMode);
            $dc->setIdNutrimeGoesTo($idNutrimeGoesTo);
        }
        $em->flush();


        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('p.dateordered >= :ddate')
            ->setParameter('ddate', '2015-10-14 00:00:00');
        $results = $query->getQuery()->getResult();
        foreach ($results as $c) {
            if (isset($dcUpdated[$c->getDistributionChannelId()])) {
                $d = $dcUpdated[$c->getDistributionChannelId()];

                $idInvoiceGoesTo = isset($d['invoiceGoesToId']) ? $d['invoiceGoesToId'] : 0;
                $idMarginGoesTo = isset($d['idMarginGoesTo']) ? $d['idMarginGoesTo'] : 0;
                $idMarginPaymentMode = isset($d['idMarginPaymentMode']) ? $d['idMarginPaymentMode'] : 0;
                $idNutrimeGoesTo = isset($d['idNutrimeGoesTo']) ? $d['idNutrimeGoesTo'] : 0;

                echo " - Update ".$c->getOrderNumber()."\n";
                if (intval($c->getInvoiceGoesToId()) == 0) {
                    $c->setInvoiceGoesToId($idInvoiceGoesTo);
                }
                if (intval($c->getIdMarginGoesTo()) == 0) {
                    $c->setIdMarginGoesTo($idMarginGoesTo);
                }
                /*if (intval($c->getIdMarginPaymentMode()) == 0) {
                    $c->setIdMarginPaymentMode($idMarginPaymentMode);
                }*/
                if (intval($c->getIdNutrimeGoesTo()) == 0) {
                    $c->setIdNutrimeGoesTo($idNutrimeGoesTo);
                }
            }
        }
        
        $em->flush();
    }
}
