<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: 
php app/console leep_admin:update_form_question_i18n
*/
class UpdateFormQuestionI18nCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:update_form_question_i18n')
            ->setDescription('Form Question - Populate i18n data')
        ;
    }

    public $em;
    public $missingText = array();
    public $usedGroups = array();
    public $usedBlocks = array();
    public function resolveText($text) {
        $text = trim($text);
        if ($text == '') { return ''; }
        $block = $this->em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findOneBy(array(
            'body'         => $text,
            'idLanguage'   => 1
        ));
        if (!empty($block)) {
            $textBlock = $this->em->getRepository('AppDatabaseMainBundle:TextBlock')->findOneById($block->getIdTextBlock());
            if ($textBlock) {
                $this->usedGroups[$textBlock->getIdGroup()] = 1;
                $this->usedBlocks['[['.$textBlock->getName().']]'] = 1;
                return '[['.$textBlock->getName().']]';
            }
            else {
                $this->missingText[$text] = 1;
                return $text;
            }
        }
        else {
            $this->missingText[$text] = 1;
            return $text;
        }
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $this->em = $container->get('doctrine')->getEntityManager();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:FormQuestion')->findAll();

        if ($result) {
            $i = 1;
            foreach ($result as $entry) {
                if ($entry->getQuestion()) {
                    $entry->setQuestionI18n($this->resolveText($entry->getQuestion()));
                }

                switch($entry->getIdType()) {
                    case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_SECTION:
                    case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_TEXTBOX:
                        if ($entry->getQuestionData()) {
                            $entry->getQuestionData($this->resolveText($entry->getQuestionData()));
                        }
                        break;
                    case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_SINGLE_CHOICE:
                    case Business\FormQuestion\Constant::FORM_QUESTION_TYPE_MULTIPLE_CHOICE:
                        if ($entry->getQuestionData()) {
                            $choices = array();
                            $rows = explode("\n", $entry->getQuestionData());
                            foreach ($rows as $row) {
                                $arr = explode('|', $row);
                                if (count($arr) == 2) {
                                    $choices[] = trim($arr[0]) . ' | ' . $this->resolveText($arr[1]);
                                }
                            }
                            $entry->setQuestionDataI18n(implode("\n", $choices));
                        }
                        break;
                }

                //$this->em->persist($entry);
                echo "$i\n"; $i++;
            }
            //$this->em->flush();
            /*echo "\n";

            foreach ($this->missingText as $text => $t) {
                echo $text."\n";
            }

            echo "---------------------------";
            foreach ($this->usedGroups as $idGroup => $t) {
                $group = $this->em->getRepository('AppDatabaseMainBundle:TextBlockGroup')->findOneById($idGroup);
                if ($group) {
                    echo $group->getName()."\n";
                }
            }*/
            foreach ($this->usedBlocks as $text => $t) {
                echo $text."\n";
            }
        }
    }
}
