<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FindPartnerDuplicateCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:find_partner_duplicate')
            ->setDescription('Find DC duplicate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        $arrayName = [];
        $arrayPartner = [];
        $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findBy(['partner' => 'ASC']);
        foreach ($partner as $key => $value) {
            if (in_array($value->getPartner(), $arrayName)){
                $arrayPartner[] = $value;
            }
            $arrayName [] = $value->getPartner();
        }

        foreach ($arrayPartner as $pt) {
            echo $pt->getId().";\t;\t".$pt->getPartner()."\n";
        }

        $em->flush();
    }
}
