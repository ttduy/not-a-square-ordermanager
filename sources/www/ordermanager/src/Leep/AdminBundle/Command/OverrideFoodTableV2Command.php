<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class OverrideFoodTableV2Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:override_food_table_v2')
            ->setDescription('Override Food Table V2')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'What is ID of source language?'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // get input sourceId
        $file = $input->getArgument('file');

        // Parse assignment
        $assignment = array();
        $lines = explode("\n", file_get_contents($file));
        foreach ($lines as $line) {
            $arr = explode(";", $line);
            $textBlock = trim($arr[0]);
            $foodItemId = trim($arr[3]);

            $assignment[$foodItemId] = $textBlock;
        }

        foreach ($assignment as $foodItemId => $textBlockName) {
            $foodItem = $em->getRepository('AppDatabaseMainBundle:FoodTableFoodItem')->findOneByFoodId($foodItemId);
            if (empty($foodItem)) {
                echo $foodItemId."\n";
                continue;
            }
            $foodItem->setName($textBlockName);            
        }
        $em->flush();
    }
}
