<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixMwaSampleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:fix_mwa_sample_command')
            ->setDescription('Fix Mwa Sample');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        //////////////////////////////////////////////////
        // x. Grab data
        $curl = $container->get('leep_admin.helper.curl');
        $config = $container->get('leep_admin.helper.common')->getConfig();

        // Do login
        $post = array(
            'dateorg0' => '01/11/2016',
            'dateorg1' => (new \DateTime())->format('d/m/Y'),
            'codeorg'  => $config->getLytechUsername(),
            'codepas'  => $config->getLytechPassword()
        );

        echo "Get data!\n";
        $content = $curl->getPageContent($config->getLytechUrl(), 'POST', $post);
        $samples = [];
        if ($this->checkLytechSuccess($content)) {
            echo "SUCCESS!\n";
            $tdNote = '\<td\sclass\=\"style3\"\>(.*?)\<\/td\>';
            $pattern = '/'.str_repeat($tdNote, 9).'/ism';

            preg_match_all($pattern, $content, $matches);
            if (isset($matches[9])) {
                foreach ($matches[0] as $i => $m) {
                    $sampleId = trim($matches[1][$i]);
                    if (empty($sampleId)) {
                        continue;
                    }

                    $arrivedDate = $this->parseLytechDate($matches[2][$i]);
                    $sampleStatus = $matches[3][$i];
                    $resultGeneratedDate = $this->parseLytechDate($matches[4][$i]);

                    $gew1 = $this->convertCall('GEW1', $matches[5][$i]);
                    $gew2 = $this->convertCall('GEW2', $matches[6][$i]);
                    $gew3 = $this->convertCall('GEW3', $matches[7][$i]);
                    $gew4 = $this->convertCall('GEW4', $matches[8][$i]);
                    $gew5 = $this->convertCall('GEW5', $matches[9][$i]);

                    $error = '';
                    if ($arrivedDate === FALSE || $resultGeneratedDate === FALSE) {
                        $error = 'Datetime has wrong format. Expect: d.m.Y';
                    }
                    if ($gew1 === FALSE || $gew2 === FALSE || $gew2 === FALSE || $gew2 === FALSE || $gew2 === FALSE) {
                        $error = 'Can\'t convert GEW call value';
                    }

                    if (empty($error)) {
                        if (isset($samples[$sampleId])) {
                            if ($samples[$sampleId]['registeredDate'] < $arrivedDate) {
                                $samples[$sampleId]['registeredDate'] = $arrivedDate;
                            }
                        } else {
                            $samples[$sampleId] = array(
                                'registeredDate' => $arrivedDate,
                                'finishedDate' => $resultGeneratedDate,
                                'gew1'     => $gew1,
                                'gew2'     => $gew2,
                                'gew3'     => $gew3,
                                'gew4'     => $gew4,
                                'gew5'     => $gew5
                            );
                        }
                    } else {
                        echo $error."\n";
                    }
                }
            }
        }

        foreach ($samples as $sampleId => $sample) {
            echo "Update $sampleId\n";
            $entity = $em->getRepository('AppDatabaseMainBundle:MwaSample')->findOneBySampleId($sampleId);
            if ($entity) {
                echo "To: ".$sample['registeredDate']->format('d/m/Y')."\n";
                $entity->setRegisteredDate($sample['registeredDate']);
                $em->persist($entity);
            } else {
                echo "!!!Entity not existed!\n";
            }
        }

        $em->flush();
        echo "Finished!";
    }

    private function parseLytechDate($date) {
        if (empty($date)) {
            return null;
        }

        return \DateTime::createFromFormat('d.m.Y', $date);
    }

    private function checkLytechSuccess($content) {
        if (empty($content)) {
            echo "Login failed\n";
            return false;
        }

        $features = array(
            '<td class="style2">ID</td>',
            '<td class="style2">ARRIVED</td>',
            '<td class="style2">SAMPLE STATUS</td>',
            '<td class="style2">RESULTS GENERATED</td>',
            '<td class="style2">FABP2 (Ala54Thr,G/A) rs1799883</td>',
            '<td class="style2">ADRB2 (Gln27Glu, C/G) rs1042714</td>',
            '<title>RESULT</title>'
        );

        foreach ($features as $feature) {
            if (strpos($content, $feature) === FALSE) {
                echo "Can\'t recognize page structure\n";
                return false;
            }
        }

        return true;
    }

    private $callCache = array();
    private function convertCall($geneName, $call) {
        $call = trim($call);

        if (empty($call)) {
            return '';
        }
        if (!isset($this->callCache[$geneName])) {
            $doctrine = $this->getContainer()->get('doctrine');
            $criteria = array('genename' => trim($geneName), 'isdeleted' => 0);
            $gene = $doctrine->getRepository('AppDatabaseMainBundle:Genes')->findOneBy($criteria);
            if (!$gene) {
                die("Can\'t find gene ".$geneName);
            }

            $mapping = array();
            $results = $doctrine->getRepository('AppDatabaseMainBundle:Results')->findBygroupid($gene->getGroupId());
            foreach ($results as $r) {
                $mapping[$r->getCallResult()] = $r->getResult();
            }

            $this->callCache[$geneName] = $mapping;
        }

        if (isset($this->callCache[$geneName][$call])) {
            return $this->callCache[$geneName][$call];
        }
        return false;
    }
}
