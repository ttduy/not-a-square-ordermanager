<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ExecuteGeneManagerResultCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:execute_gene_manager_result')
            ->setDescription('Execute gene manager result');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:GeneManagerResultHistory', 'p')
            ->andWhere('p.status = :statusOpen')
            ->orderBy('p.id', 'ASC')
            ->setParameter('statusOpen', Business\GeneManager\Constant::STATUS_OPEN)
            ->setMaxResults(1);
        $geneResultHistory = null;
        try {
            $geneResultHistory = $query->getQuery()->getSingleResult();
        }
        catch (\Exception $e) {

        }

        if (empty($geneResultHistory)) {
            return false;
        }

        $geneResultHistory->setStatus(Business\GeneManager\Constant::STATUS_INPROGRESS);
        $em->flush();

        ////////////////////////////////
        // Processing
        $parser = $container->get('leep_admin.gene_manager.business.submit_result_parser');
        $parser->init();
        $result = $parser->parseResult($geneResultHistory->getResult());

        $errorRecords = array(
            Business\GeneManager\Constant::ERROR_TYPE_FORMAT_ERROR      => array(),
            Business\GeneManager\Constant::ERROR_TYPE_DATA_ERROR        => array(),
            Business\GeneManager\Constant::ERROR_TYPE_CONFLICT_ERROR    => array(),
            Business\GeneManager\Constant::ERROR_TYPE_WARNING           => array(),
            Business\GeneManager\Constant::TYPE_SUCCESS                 => array()
        );

        foreach ($result as $i => $row) {
            if ($row['error'] == '' && $row['warning'] == '') {
                // Records
                $idOrder = $row['idOrder'];
                $idCustomer = $row['idCustomer'];
                $idGenes = $row['idGene'];
                $geneResult = $row['geneResult'];

                // Reassure genes
                $this->reassureGenes($idOrder, $idCustomer);

                if (is_array($idGenes)) {
                    foreach ($idGenes as $idGene) {
                        // Add result
                        $cig = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findOneBy(array(
                            'idCustomer' => $idCustomer,
                            'idGene'     => $idGene
                        ));

                        $geneResult = $row['geneResults'][$idGene];
                        if ($cig) {
                            $cig->setResult($geneResult);
                        } else {
                            $newCig = new Entity\CustomerInfoGene();
                            $newCig->setIdCustomer($idCustomer);
                            $newCig->setIdGene($idGene);
                            $newCig->setResult($geneResult);
                            $em->persist($newCig);
                        }

                        $em->flush();
                    }
                } else {
                    $idGene = intval($idGenes);

                    // Add result
                    $cig = $em->getRepository('AppDatabaseMainBundle:CustomerInfoGene')->findOneBy(array(
                        'idCustomer' => $idCustomer,
                        'idGene'     => $idGene
                    ));
                    if ($cig) {
                        $cig->setResult($geneResult);
                    } else {
                        $newCig = new Entity\CustomerInfoGene();
                        $newCig->setIdCustomer($idCustomer);
                        $newCig->setIdGene($idGene);
                        $newCig->setResult($geneResult);
                        $em->persist($newCig);
                    }

                    $em->flush();
                }

                // Mark as added
                $result[$i]['isAdded'] = true;

                $errorRecords[Business\GeneManager\Constant::TYPE_SUCCESS][] = $row;
            } else if ($row['error'] != '') {
                $idType = intval($row['error_type']);
                $errorRecords[$idType][] = $row;
            } else if ($row['warning'] != '') {
                $idType = Business\GeneManager\Constant::ERROR_TYPE_WARNING;
                $errorRecords[$idType][] = $row;
            }
        }

        $geneResultHistory->setStatus(Business\GeneManager\Constant::STATUS_DONE);
        $geneResultHistory->setNumFormatError(count($errorRecords[Business\GeneManager\Constant::ERROR_TYPE_FORMAT_ERROR]));
        $geneResultHistory->setNumDataError(count($errorRecords[Business\GeneManager\Constant::ERROR_TYPE_DATA_ERROR]));
        $geneResultHistory->setNumConflictError(count($errorRecords[Business\GeneManager\Constant::ERROR_TYPE_CONFLICT_ERROR]));
        $geneResultHistory->setNumWarning(count($errorRecords[Business\GeneManager\Constant::ERROR_TYPE_WARNING]));
        $geneResultHistory->setNumSuccess(count($errorRecords[Business\GeneManager\Constant::TYPE_SUCCESS]));
        $em->flush();

        // Save error
        foreach ($errorRecords as $idType => $errors) {
            foreach ($errors as $error) {
                $resultHistoryError = new Entity\GeneManagerResultHistoryError();
                $resultHistoryError->setIdGeneManagerResultHistory($geneResultHistory->getId());
                $resultHistoryError->setIdErrorType($idType);
                $resultHistoryError->setErrorData(serialize($error));
                $em->persist($resultHistoryError);
            }
        }

        $em->flush();
    }

    protected $reassureGenesChecked = array();

    protected function reassureGenes($idOrder, $idCustomer) {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $expectedGenes = array();

        if (isset($this->reassureGenesChecked[$idOrder])) {
            return false;
        }
        $this->reassureGenesChecked[$idOrder] = 1;

        // Make sure all "missing" CustomerInfoGene record filled
        $query = $em->createQueryBuilder();
        $query->select('pgl.geneid')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'op')
            ->leftJoin('AppDatabaseMainBundle:ProdGeneLink', 'pgl', 'WITH', 'op.productid = pgl.productid')
            ->andWhere('op.customerid = '.$idOrder);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $expectedGenes[$r['geneid']] = 1;
        }

        // Unset "already" CustomerInfoGene
        $query = $em->createQueryBuilder();
        $query->select('cig')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'cig')
                ->andWhere('cig.idCustomer = '.$idCustomer);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            if (isset($expectedGenes[$r->getIdGene()])) {
                unset($expectedGenes[$r->getIdGene()]);
            }
        }

        // Fill "missing" CustomerInfoGene
        foreach ($expectedGenes as $geneId => $tmp) {
            $newCig = new Entity\CustomerInfoGene();
            $newCig->setIdCustomer($idCustomer);
            $newCig->setIdGene($geneId);
            $newCig->setResult('');
            $em->persist($newCig);
        }
        $em->flush();
    }
}
