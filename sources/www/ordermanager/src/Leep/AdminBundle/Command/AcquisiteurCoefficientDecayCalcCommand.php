<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class AcquisiteurCoefficientDecayCalcCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:acquisiteur_coef_decay_cal')
            ->setDescription('Acquisiteur Coefficient Decay Calculation');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');

        $today = new \DateTime('now');
        $todayOfNotification = $today->format('d/m/Y');

        $topicMsgFormat = 'DC %s: Commission rate of "%s" is reduced from %s -> %s';

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p');
        $result = $query->getQuery()->getResult();
        
        foreach ($result as $row) {
            if ($row->getCreationDate() == null) {
                continue;
            }

            $tsDiff = $today->getTimestamp() - $row->getCreationDate()->getTimestamp();
            $dayDiff = intval($tsDiff / (24*60*60));
            if (!(($dayDiff > 0) && ($dayDiff % 365 == 0))) {
                continue;
            }

            if ($row->getYearlyDecayPercentage1() != 0)    {
                $newCommission = $row->getCommission1() - $row->getYearlyDecayPercentage1();

                $topicMsg = sprintf($topicMsgFormat, 
                                        $row->getDistributionchannel(), 
                                        $formatter->format($row->getAcquisiteurid1(), 'mapping', 'LeepAdmin_Acquisiteur_List'), 
                                        $row->getCommission1(),
                                        ($newCommission >= 0 ? $newCommission : 0)
                                    );

                if ($newCommission > 0) {
                    $row->setCommission1($newCommission);
                } else {
                    $row->setAcquisiteurid1(null);
                    $row->setCommission1(null);
                    $row->setYearlyDecayPercentage1(null);
                }
                
                $notification1 = new Entity\Notification();
                $notification1->setIdTopic(Business\Notification\Constant::ACQUISITEUR_COMMISION_DECAY);
                $notification1->setMessage($topicMsg);
                $notification1->setTimestamp($today);
                $em->persist($notification1);
            }

            if ($row->getYearlyDecayPercentage2() != 0)    {
                $newCommission = $row->getCommission2() - $row->getYearlyDecayPercentage2();

                $topicMsg = sprintf($topicMsgFormat, 
                                        $row->getDistributionchannel(), 
                                        $formatter->format($row->getAcquisiteurid2(), 'mapping', 'LeepAdmin_Acquisiteur_List'), 
                                        $row->getCommission2(),
                                        ($newCommission >= 0 ? $newCommission : 0)
                                    );

                if ($newCommission > 0) {
                    $row->setCommission2($newCommission);
                } else {
                    $row->setAcquisiteurid2(null);
                    $row->setCommission2(null);
                    $row->setYearlyDecayPercentage2(null);
                }

                $notification2 = new Entity\Notification();
                $notification2->setIdTopic(Business\Notification\Constant::ACQUISITEUR_COMMISION_DECAY);
                $notification2->setMessage($topicMsg);
                $notification2->setTimestamp($today);
                $em->persist($notification2);
            }

            if ($row->getYearlyDecayPercentage3() != 0)    {
                $newCommission = $row->getCommission3() - $row->getYearlyDecayPercentage3();

                $topicMsg = sprintf($topicMsgFormat, 
                                        $row->getDistributionchannel(), 
                                        $formatter->format($row->getAcquisiteurid3(), 'mapping', 'LeepAdmin_Acquisiteur_List'), 
                                        $row->getCommission3(),
                                        ($newCommission >= 0 ? $newCommission : 0)
                                    );

                if ($newCommission > 0) {
                    $row->setCommission3($newCommission);
                } else {
                    $row->setAcquisiteurid3(null);
                    $row->setCommission3(null);
                    $row->setYearlyDecayPercentage3(null);
                }

                
                $notification3 = new Entity\Notification();
                $notification3->setIdTopic(Business\Notification\Constant::ACQUISITEUR_COMMISION_DECAY);
                $notification3->setMessage($topicMsg);
                $notification3->setTimestamp($today);
                $em->persist($notification3);
            }

            $em->flush();
        }
    }
}
