<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class AutoCustomerPricingCommand extends ContainerAwareCommand {
    public $errors = [];
    protected function configure() {
        $now = new \DateTime('now');
        $this->setName('leep_admin:auto_customer_pricing_command')
            ->setDescription('Auto pricing phace 1');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $customerFilterBuilder = $container->get('leep_admin.customer.business.customer_filter_builder');
        $em = $container->get('doctrine')->getEntityManager();

        // Only check lately order
        $checkDate = new \DateTime();
        $checkDate->modify('-2 day');
        $pricingHelper = $container->get('leep_admin.billing.business.pricing_helper');
        $billingUtils = $container->get('leep_admin.billing.business.utils');
        $config = Business\PricingSetting\Utils::getPricingConfig($container);
        $countSuccess = 0;
        if (empty($config->status)) {
            echo "Empty config, exit!\n";
            return;
        }

        // Get all status matched pricing config setting
        $results = [];
        $statusList = $em->createQueryBuilder()
                    ->select('p')
                    ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
                    ->andWhere('p.statusdate >= :checkDate')
                    ->andWhere('p.status IN (:status)')
                    ->setParameter('checkDate', $checkDate)
                    ->setParameter('status', $config->status)
                    ->getQuery()->getResult();

        // Get filtered order list with no duplicate
        $orderList = [];
        foreach ($statusList as $status) {
            if (!isset($orderList[$status->getCustomerId()])) {
                $orderList[$status->getCustomerId()] = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($status->getCustomerId());
            }
        }

        echo "Processing ".count($orderList)." orders!\n";
        $orderInfo = [];
        foreach ($orderList as $order) {
            if ($order->getIsAddedInvoiced() == 1) {
                echo "Order #".$order->getId()." already in parse 2\n";
            } else if ($billingUtils->generateCustomerBillingEntries($order, $pricingHelper, $this->errors) !== false) {
                $order->setIsAddedInvoiced(1);
                $em->persist($order);
                $em->flush();

                $orderInfo [] = [
                    'id' =>             $order->getId(),
                    'orderNumber' =>    $order->getOrderNumber(),
                    'name' =>           $order->getFirstName(). ' '. $order->getSurName(),
                    'link' =>           '#'
                ];
            } else {
                echo "Error in order #".$order->getId()."\n";
                var_dump($this->errors);
                echo "\n--------------------------------------------\n";
            }
        }

        echo "Done!\n";
        if (!empty($config->email) && !empty($orderInfo)) {
            echo "Sending email!\n";
            $emailData = Business\EmailOutgoing\Util::getPricingEmailDataFromTemplate($container, [
                'idTemplate' => $config->idTemplate,
                'orderInfo' =>  $orderInfo
            ]);

            // create Outgoing mail
            Business\EmailOutgoing\Util::createOutgoingEmail(
                $container,
                $config->email,
                $emailData,
                Business\EmailOutgoing\Constant::EMAIL_QUEUE_PRICING_DONE,
                Business\EmailOutgoing\Constant::EMAIL_STATUS_OPEN,
                $config->emailMethod,
                true,
                false);
        }

        echo "Finished!";
    }
}
