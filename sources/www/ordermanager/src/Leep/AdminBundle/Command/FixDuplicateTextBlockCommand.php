<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixDuplicateTextBlockCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_duplicate_text_block')
            ->setDescription('Fix duplicate text block')
            ->addOption(
               'delete',
               null,
               InputOption::VALUE_NONE
            );
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $isDelete = $input->getOption('delete');        

        $query = $em->createQueryBuilder();
        $query->select('p.name, COUNT(p) AS total')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->groupBy('p.name');

        $results = $query->getQuery()->getResult();
        $duplicates = array();
        foreach ($results as $r) {
            if ($r['total'] > 1) {
                $duplicates[$r['name']] = $r['total'];
            }
        }
        
        $textBlockIds = array();
        $query = $em->createQueryBuilder();
        $query->select('p.id')
            ->from('AppDatabaseMainBundle:TextBlock', 'p')
            ->andWhere('p.name in (:blockNames)')
            ->setParameter('blockNames', array_keys($duplicates));
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $textBlockIds[] = $r['id'];
        }

        $textBlockLanguages = array();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->andWhere('p.idTextBlock in (:arr)')
            ->setParameter('arr', $textBlockIds);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idTextBlock = $r->getIdTextBlock();
            if (!isset($textBlockLanguages[$idTextBlock])) {
                $textBlockLanguages[$idTextBlock] = array();
            }
            $textBlockLanguages[$idTextBlock][] = $r;
        }

        $tmpDir = $container->getParameter('kernel.root_dir').'/tmp/fix_duplicates';
        @mkdir($tmpDir, 0777, true);

        $languages = array();
        $results = $em->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($results as $r) {
            $languages[$r->getId()] = $r->getName();
        }

        $resolutions = array();
        foreach ($duplicates as $name => $d) {
            $textBlocks = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findByName($name);

            $textBlockIds = array();
            $conflicts = array();
            foreach ($textBlocks as $textBlock) {
                $textBlockIds[] = $textBlock->getId();
                $translations = $textBlockLanguages[$textBlock->getId()];
                foreach ($translations as $translation) {
                    $idTextBlock = $translation->getIdTextBlock();
                    $idLanguage = $translation->getIdLanguage();
                    $body = $translation->getBody();

                    if (!isset($conflicts[$idLanguage])) {
                        $conflicts[$idLanguage] = array(
                            'chosen' => -1,
                            'body'   => array()
                        );
                    }
                    $conflicts[$idLanguage]['body'][$idTextBlock] = $body;
                }
            }

            foreach ($conflicts as $idLanguage => $c) {
                $chosenId = -1;
                $length = 0;
                foreach ($c['body'] as $idTextBlock => $body) {
                    $bodyLength = mb_strlen($body);
                    if ($body == '-- No translation yet --') {
                        $bodyLength = 1;
                    }
                    if ($bodyLength > $length) {
                        $length = $bodyLength;
                        $chosenId = $idTextBlock;
                    }

                    if ($chosenId == -1) {
                        $chosenId = $idTextBlock;
                    }
                }
                $conflicts[$idLanguage]['chosen'] = $chosenId;
            }

            $resolutions[] = array(
                'name'        => $name,
                'textBlocks'  => $textBlockIds,
                'conflicts'   => $conflicts
            );
        }

        foreach ($languages as $idLanguage => $name) {
            $text = '';
            foreach ($resolutions as $resolution) {
                $text .= "\n\n\n\n";
                $text .= "*********************************************\n";
                $text .= "TEXT BLOCK: [[".$resolution['name']."]]\n";
                $text .= "\n";
                if (isset($resolution['conflicts'][$idLanguage])) {
                    foreach ($resolution['conflicts'][$idLanguage]['body'] as $idTextBlock => $body) {
                        $text .= "------------------------------\n";
                        if ($resolution['conflicts'][$idLanguage]['chosen'] == $idTextBlock) {
                            $text .= " >>>>> CHOSEN \n";                        
                        }
                        $text .= $body."\n";
                    }
                }
            }
            file_put_contents($tmpDir.'/'.$name.'.txt', $text);
        }

        if ($isDelete) {

        }
    }
}
