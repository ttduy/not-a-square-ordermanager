<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CleanupTemporaryAwsFilesCommand extends ContainerAwareCommand {
    protected function configure() {
        $this
            ->setName('leep_admin:cleanup_temporary_aws_files')
            ->setDescription('Cleanup');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        echo "Begin!\n";
        $checkDate = new \DateTime("-2 day");
        $container = $this->getContainer();
        $path = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
        if($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                $fileUrl = $path.'/' .$file;
                if ($file != '.' && $file != '..' && !is_link($fileUrl)) {
                    if (is_file($fileUrl)) {
                        if ($fileTime < $checkDate->getTimestamp()) {
                            echo "Remove old file: $fileUrl\n";
                            unlink($fileUrl);
                        }
                    }
                }
            }
        }

        closedir($handle);
        echo "Finished!\n";
    }
}
