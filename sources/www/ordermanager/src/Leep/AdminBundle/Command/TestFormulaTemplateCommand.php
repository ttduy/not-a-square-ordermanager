<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/**
*
*/
class TestFormulaTemplateCommand extends ContainerAwareCommand {

    protected function configure() {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:test_formula_template')
            ->setDescription('Test new include in formula template');
    }

    private function processInputValue($data) {
        $input = array();
        $rows = explode("\n", $data);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $input[trim($arr[0])] = trim($arr[1]);
            }
        }
        return $input;
    }

    private function getListOrder($em) {
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.ordernumber')
                ->from("AppDatabaseMainBundle:Customer", "p");
        $results = $query->getQuery()->getResult();
        $listOrderPos = array_rand($results, 10);
        $listOrder = [];
        foreach ($listOrderPos as $key => $value) {
            $listOrder[] = $results[$value];
        }
        return $listOrder;
    }

    private function checkFormula($formulaName, $orderId, $container) {
        $orderDataStr = Business\CustomerReport\Utils::loadOrderData($container, $orderId);
        $orderData = $this->processInputValue($orderDataStr);
        $orderData1 = $this->processInputValue($orderDataStr);
        $formulaExtension = $container->get('leep_admin.formula_template.business.formula_extension');
        $formulaExtension->includeFormulaTemplate($formulaName, $orderData);
        $formulaExtension->includeFormulaTemplateOld($formulaName, $orderData1);
        if ($orderData === $orderData1) {
            if (in_array("Error", $orderData) && count($orderData) == 1) {
                return "Matched;Error render";
            }
            else {
                return "Matched;";
            }
        }
        return "Not Matched;";
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();

        // get all list of formula template, for each fomula template, get randomly 10 diferrent orders number and
        // check if the formula template run ok in 2 way including
        $query = $em->createQueryBuilder();
        $query->select('p.name')
                ->from('AppDatabaseMainBundle:FormulaTemplate', 'p');
        $results = $query->getQuery()->getResult();

        $checkResults = [];
        $header = "Formula name;Order number;Is Matched?;Note\n";
        file_put_contents('/var/www/ordermanager/files/test/test_formula.csv', $header);
        $i = 0;
        foreach ($results as $formula) {
            $listOrder = $this->getListOrder($em);
            echo "Start formula: ".$formula['name']."\n";
            foreach ($listOrder as $order) {
                $orderNumber = $order['ordernumber'];
                $matched = $this->checkFormula($formula['name'], $order['id'], $container);
                $currentResult = $formula['name'].";".$orderNumber.";".$matched."\n";
                file_put_contents('/var/www/ordermanager/files/test/test_formula.csv', $currentResult, FILE_APPEND);
            }
            echo "Finish formula: ".$formula['name']."\n";
        }
    }
}