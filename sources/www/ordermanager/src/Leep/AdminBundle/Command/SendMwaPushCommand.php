<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SendMwaPushCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:send_mwa_push')
            ->setDescription('Send MWA Push');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $mapping = $container->get('easy_mapping');

        $url = $config->getMwaPushUrl();
        $noMessage = $config->getMwaMessagePerSession();
        $em = $container->get('doctrine')->getEntityManager();

        // Turn off so many failed
        $query = $em->createQueryBuilder();
        $result = $query->select('p')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('p.numFailed >= 20')
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $r->setIsSendNow(0);
            $r->setIsOverrideSending(0);
            $r->setNumFailed(0);
        }
        $em->flush();

        // Search
        $query = $em->createQueryBuilder();
        $result = $query->select('p')
            ->from('AppDatabaseMainBundle:MwaSample', 'p')
            ->andWhere('p.isSendNow = 1')
            ->andWhere('p.sendingStatus in (:activeStatus)')
            ->andWhere('(p.numFailed IS NULL) OR (p.numFailed < 20)')
			->andWhere('p.isDeleted = 0')
            ->setParameter('activeStatus', array(
                0,
				Business\MwaSample\Constants::SENDING_STATUS_SUCCESS,
                Business\MwaSample\Constants::SENDING_STATUS_ERROR
            ))
            ->orderBy('p.sendingStatus, p.id' ,'ASC')
            ->setMaxResults($noMessage)
            ->setFirstResult(0)
            ->getQuery()->getResult();
        foreach ($result as $r) {
            $r->setSendingStatus(Business\MwaSample\Constants::SENDING_STATUS_SEND);
        }
        $em->flush();

        $utcTimezone = new \DateTimeZone('UTC');
        $serverTimezone = new \DateTimeZone(date_default_timezone_get());

        date_default_timezone_set('UTC');
        foreach ($result as $r) {
            $pushDatetime = new \DateTime('now');
            $timestamp = ($pushDatetime->getTimestamp())* 1000; //($pushDatetime->getTimestamp() + 3600)* 1000

            $pushStatus = $r->getPushStatus();
            $pushStatusRecord = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:MwaStatus')->findOneById($pushStatus);
            $pushStatusName = $mapping->getMappingTitle('LeepAdmin_MwaStatus_List', $pushStatus);

            if ($pushStatusRecord->getIsSendPush() == 0 || $r->getIsOverrideSending() == 1 || $r->getIsLocal() == 1) {
                $r->setCurrentStatus($pushStatus);
                $r->setIsSendNow(0);
                $r->setIsOverrideSending(0);
                $r->setSendingStatus(Business\MwaSample\Constants::SENDING_STATUS_SUCCESS);
                $r->setPushStatus(0);
                $r->setPushDatetime(null);
                $r->setNumFailed(0);
                if ($pushStatusName == 'ANALYSIS_FINISHED') {
                    $r->setFinishedDate(new \DateTime());
                }
                $em->persist($r);
                $em->flush();

                // Record PUSH history
                $record = new Entity\MwaSamplePush();
                $record->setIdMwaSample($r->getId());
                $record->setStatus($pushStatus);
                $record->setPushDatetime($pushDatetime);
                $record->setTimestamp($timestamp);
                $record->setServerResponseCode(200);
                $record->setServerResponseRaw("Override PUSH: Change status to ".$pushStatusName);
                $em->persist($record);
                $em->flush();
            } else {
                if ($pushStatusName == 'ANALYSIS_FINISHED') {
                    $resultPattern = $r->getResultPattern();
                    if (empty($resultPattern)) {
                        $r->setSendingStatus(Business\MwaSample\Constants::SENDING_STATUS_ERROR);
                        $r->setLastError('Result pattern must be set');
                        $r->setIsSendNow(0);
                        $r->setIsOverrideSending(0);
                        $r->setNumFailed(0);
                        $em->persist($r);
                        $em->flush();
                        continue;
                    }
                    $targetUrl = $url.'/result/'.$r->getSampleId().'/ANALYSIS_FINISHED/'.$r->getResultPattern().'/'.$timestamp;
                }
                else {
                    $targetUrl = $url.'/status/'.$r->getSampleId().'/'.$pushStatusName.'/'.$timestamp;
                }

                $raw = '';
                $code = $this->call($targetUrl, $raw);
                $codeMessage = $this->getCodeMessage($code);
                if ($code == 200) {
                    $record = new Entity\MwaSamplePush();
                    $record->setIdMwaSample($r->getId());
                    $record->setStatus($pushStatus);
                    $record->setPushDatetime($pushDatetime);
                    $record->setTimestamp($timestamp);
                    $record->setServerResponseCode($codeMessage);
                    $record->setServerResponseRaw($raw);
                    $em->persist($record);
                    $em->flush();

                    $r->setCurrentStatus($pushStatus);
                    $r->setIsSendNow(0);
                    $r->setIsOverrideSending(0);
                    $r->setSendingStatus(Business\MwaSample\Constants::SENDING_STATUS_SUCCESS);
                    $r->setPushStatus(0);
                    $r->setPushDatetime(null);
                    $r->setNumFailed(0);
                    if ($pushStatusName == 'ANALYSIS_FINISHED') {
                        $r->setFinishedDate(new \DateTime());
                    }
                    $em->persist($r);
                    $em->flush();
                }
                else {
                    $r->setSendingStatus(Business\MwaSample\Constants::SENDING_STATUS_ERROR);
                    $r->setLastError($raw);
                    $numFailed = intval($r->getNumFailed());
                    $r->setNumFailed($numFailed + 1);
                    $em->persist($r);
                    $em->flush();
                }
            }
        }
    }

    protected function getCodeMessage($code) {
        $codes = array(
            200 => 'OK',
            403 => 'the status sent is invalid',
            404 => 'invalid Sample ID',
            406 => 'invalid timestamp',
            412 => 'incorrect gene type',
        );
        if (!isset($codes[$code])) {
            return 'Unknown code';
        }
        return $codes[$code];
    }

    protected function call($url, &$raw) {
        $cert = $this->getContainer()->getParameter('kernel.root_dir').'/certs';

        $raw = 'Call URL: '.$url."<br/>";

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c, CURLOPT_POSTFIELDS, array());
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($c, CURLOPT_USERPWD, 'amway-dna-service:Mein-DNA-Service2013');
        curl_setopt($c, CURLOPT_SSLKEY, $cert.'/key.pem');
        curl_setopt($c, CURLOPT_SSLCERT, $cert.'/client.pem');
        curl_setopt($c, CURLOPT_CAINFO, $cert.'/cacert.pem');
        curl_setopt($c, CURLOPT_SSLCERTPASSWD, 'xx-well');

        $response = curl_exec($c);
        if (!$response) {
            $raw .= "ERROR: ".curl_error($c);
            return false;
        }

        $httpCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);

        $raw .= "Response code: ".$httpCode.'<br/>';
        $raw .= "Raw response: ".$response;

        return $httpCode;
    }
}
