<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* 
this command is used to
- update field of table: distribution_channels.last_order

to RUN:
    php app/console leep_admin:dc_last_order_date_aggregation
*/
class DCLastOrderDateAggregationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:dc_last_order_date_aggregation')
            ->setDescription('Aggregate DC Last Order Date')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $dc = array();

        // aggregate data from ordered_product
        $q = $em->createQueryBuilder();
        $result = $q->select('p.distributionchannelid, MAX(p.dateordered) dateordered')
                                    ->from('AppDatabaseMainBundle:Customer', 'p')
                                    ->andWhere('p.distributionchannelid IS NOT NULL AND p.distributionchannelid <> 0')
                                    ->groupBy('p.distributionchannelid')
                                    ->getQuery()
                                    ->getResult();

        if ($result) {
            $counter = 0;
            foreach ($result as $entry) {
                $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($entry['distributionchannelid']);
                if ($dc) {
                    $dc->setLastOrderDate(new \DateTime($entry['dateordered']));
                    $em->persist($dc);

                    $counter++;
                }

                if ($counter % 1000) {
                    $em->flush();
                    $counter = 0;
                }
            }
             $em->flush();
        }
    }
}
