<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:email_processing
*/
class EmailProcessingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:email_processing')
            ->setDescription('Email Processing')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // Find email in status = 'Open', change to 'Processing
        $filter = array(
            'status'    =>  Business\MailSender\Util::EMAIL_POOL_STATUS_OPEN
        );
        $emailPools = $doctrine->getRepository('AppDatabaseMainBundle:EmailPool')->findBy($filter);

        // Create Spool
        if ($emailPools) {
            foreach ($emailPools as $emailPool) {
                // update status of Email Pool
                $emailPool->setStatus(Business\MailSender\Util::EMAIL_POOL_STATUS_PROCESSING);
                $em->persist($emailPool);
                $em->flush();

                // get related receivers
                $receivers = $doctrine->getRepository('AppDatabaseMainBundle:EmailPoolReceiver')->findByIdEmailPool($emailPool->getId());

                $counter = 0;
                if ($receivers) {
                    $maxPerFlush = 1000;
                    foreach ($receivers as $receiver) {
                        $newEmailSpooled = new Entity\EmailPoolSpool();
                        $newEmailSpooled->setIdEmailPool($emailPool->getId());
                        $newEmailSpooled->setIdEmailPoolReceiver($receiver->getId());
                        $newEmailSpooled->setSubject($emailPool->getSubject());
                        $newEmailSpooled->setContent($emailPool->getContent());
                        $newEmailSpooled->setCreatedAt($emailPool->getCreatedAt());
                        $em->persist($newEmailSpooled);

                        $counter++;

                        if ($counter % $maxPerFlush == 0) {
                            $em->flush();
                        }
                    }

                    $em->flush();
                }

                // update status of EmailPool
                $emailPool->setStatus(Business\MailSender\Util::EMAIL_POOL_STATUS_PENDING_FOR_SENDING);
                $emailPool->setNumPending($counter);
                $em->persist($emailPool);
                $em->flush();
            }
        }
    }
}
