<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SftpSyncDownloadFileCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('leep_admin:sftp_sync_download_file_command')
            ->setDescription('Pre download SFTP file from DC server');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $root = $container->get('service_container')->getParameter('kernel.root_dir').'/../files/sftpDownload/';

        $dcList = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findBy(['canDownloadViaSftp' => true]);
        foreach ($dcList as $dc) {
            echo "Download sftp files of DC #".$dc->getId()." -- ".$dc->getDistributionchannel()."\n";

            $host = $dc->getSftpDownloadHost();
            $username = $dc->getSftpDownloadUsername();
            $password = $dc->getSftpDownloadPassword();
            $dirPath = $dc->getSftpDownloadFolder();

            if(!$connection = ssh2_connect($host, 22)) {
                echo "Cannot connect to server: $host\n";
                continue;
            }

            if (!ssh2_auth_password($connection, $username, $password)) {
                echo "Authorize failed: $username >> $password\n";
            }

            $sftp = ssh2_sftp($connection);
            if (!$sftp) {
                echo "Failed to create a sftp connection.\n";
                continue;
            }

            if (!$dir = opendir("ssh2.sftp://$sftp/$dirPath/")) {
                echo "Failed to open the directory.\n";
                continue;
            }

            if (!is_dir($root.$dc->getId())) {
                mkdir($root.$dc->getId());
            }

            while (false !== ($fileName = readdir($dir))) {
                if ($fileName != '.' && $fileName != '..') {
                    $remoteFileUrl = "ssh2.sftp://$sftp/$dirPath/$fileName";
                    $localFileUrl = $root.$dc->getId().'/'.$fileName;
                    $modifiedDate = filemtime($remoteFileUrl);
                    $downloadedDate = null;
                    if (is_file($localFileUrl)) {
                        $downloadedDate = filemtime($localFileUrl);
                    }

                    if (!$modifiedDate || !$downloadedDate || $modifiedDate > $downloadedDate) {
                        $remote = fopen($remoteFileUrl, 'r');
                        if (!$remote) {
                            echo "Unable to open remote file $fileName\n";
                            continue;
                        }

                        $local = fopen($localFileUrl, 'w');
                        if (!$local) {
                            echo "Unable to create local file: $fileName\n";
                            continue;
                        }

                        $read = 0;
                        $fileSize = filesize($remoteFileUrl);
                        echo "DOWNLOADING $fileName :: $fileSize\n";
                        while ($read < $fileSize && ($buffer = fread($remote, $fileSize - $read))) {
                            $read += strlen($buffer);
                            if (fwrite($local, $buffer) === FALSE) {
                                echo "Unable to write to local file: $fileName\n";
                                continue;
                            }
                        }

                        fclose($local);
                        fclose($remote);
                    }
                }
            }
        }

        echo "Finished!";
    }
}
