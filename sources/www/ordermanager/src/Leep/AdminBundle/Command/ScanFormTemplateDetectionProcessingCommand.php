<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ScanFormTemplateDetectionProcessingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:scan_form_template_detection_processing')
            ->setDescription('Scan Form Template Detection Processing');
    }

    public $em;        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getEntityManager();
        $scanFormFolder = $container->getParameter('files_dir').'/scan_form';
        $utils = $container->get('leep_admin.scan_form_template.business.utils');
        $helper = $container->get('leep_admin.helper.common');

        // Load all template detection zones
        $templateZones = array();
        $i = 0;
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormTemplateZone', 'p')
            ->innerJoin('AppDatabaseMainBundle:ScanFormTemplate', 'j', 'WITH', 'p.idScanFormTemplate = j.id');
        $zones = $query->getQuery()->getResult();
        foreach ($zones as $zone) {            
            $zonesDef = $utils->parseZonesDefinition($zone->getZonesDefinition());
            foreach ($zonesDef['zones'] as $zoneDef) {
                if ($zoneDef['idType'] == Business\ScanFormTemplate\Constant::TEMPLATE_DETECTION_ZONE) {
                    if (!isset($templateZones[$zone->getIdScanFormTemplate()])) {
                        $templateZones[$zone->getIdScanFormTemplate()] = array(
                            'zones' => array(),
                            'files' => array()
                        );
                    }

                    $templateZones[$zone->getIdScanFormTemplate()]['zones'][] = sprintf("%s,%s,%s,%s,%s,%s", 
                        $zone->getPageNumber(),
                        $zoneDef['x'],
                        $zoneDef['y'],
                        $zoneDef['width'],
                        $zoneDef['height'],
                        $zone->getIdScanFormTemplate().'_'.$i
                    );   
                    $templateZones[$zone->getIdScanFormTemplate()]['files'][] = $zone->getIdScanFormTemplate().'_'.$i;
                    $i++;
                }
            }
        }

        // Process template files
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormFile', 'p')
            ->andWhere('p.status = '.Business\ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_OPEN);
        $files = $query->getQuery()->getResult();

        foreach ($files as $file) {
            $pdfFile = $scanFormFolder.'/files/'.$file->getId().'.pdf';
            $dir = $scanFormFolder.'/files_template_detection/'.$file->getId();
            @mkdir($dir);

            // Process against all template
            foreach ($templateZones as $idScanFormTemplate => $zones) {
                @mkdir($dir.'/'.$idScanFormTemplate);
                $jarFile = $container->getParameter('kernel.root_dir').'/../scripts/pdfImageExtractor/build/jar/pdfImageExtractor.jar';

                $cmd = sprintf("java -jar %s %s %s %s",
                    '"'.$jarFile.'"',
                    '"'.$pdfFile.'"',
                    '"'.$dir.'/'.$idScanFormTemplate.'"',
                    '"'.implode(';', $zones['zones']).'"'
                );
                $out = $helper->executeCommand($cmd);       

                // Make entry
                $query = $this->em->createQueryBuilder();
                $query->delete('AppDatabaseMainBundle:ScanFormFileActualTokens', 'p')
                    ->andWhere('p.idScanFormFile = '.$file->getId())
                    ->getQuery()
                    ->execute();
                foreach ($zones['files'] as $img) {
                    $actualToken = new Entity\ScanFormFileActualTokens();
                    $actualToken->setIdScanFormFile($file->getId());
                    $actualToken->setIdScanFormTemplate($idScanFormTemplate);
                    $actualToken->setImage('/files_template_detection/'.$file->getId().'/'.$idScanFormTemplate.'/'.$img.'.jpg');         
                    $this->em->persist($actualToken);
                }
            }
            $file->setStatus(Business\ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_DETECTING_TEMPLATE);
            $this->em->flush();
            echo "Done: ".$file->getId()."\n";
        }
    }
}
