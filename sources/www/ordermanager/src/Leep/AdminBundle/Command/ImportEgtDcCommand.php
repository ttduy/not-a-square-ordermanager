<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ImportEgtDcCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:import_egt_dc')
            ->setDescription('Import EGT DC Data from CSV file')
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                'Where is the csv file'
            )
            ->addArgument(
                'separator',
                InputArgument::OPTIONAL,
                'what is separator by "comma" as default'
            );
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        echo "Begin\n";

        // Init
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $sourceFile = $input->getArgument('sourceFile');
        $separator = ($input->getArgument('separator') ? $input->getArgument('separator') : '|');
        
        $totalDataRows = 0;        

        $partner = $em->getRepository('AppDatabaseMainBundle:Partner')->findOneById(60);

        // Read file && Prepare necessary arrays
        if (($inputHandle = fopen($sourceFile, "r")) !== FALSE) {                            
            $entryData = $this->parseDataTwo(file_get_contents($sourceFile));

            // Insert data to "Distribution Channel" table
            foreach ($entryData as $data) {
                $dc = new Entity\DistributionChannels();
                foreach($data as $key => $value) {       
                    $method = 'set'.ucfirst($key);                            
                    $dc->$method($value);
                    $em->persist($dc);   
                }             

                $dc->setInvoiceGoesToId($partner->getIdInvoicesGoesTo());
                $dc->setIdMarginGoesTo($partner->getIdMarginGoesTo());
                $dc->setIdMarginPaymentMode($partner->getIdMarginPaymentMode());
                $dc->setIdNutrimeGoesTo($partner->getIdNutrimeGoesTo());
                $dc->setIsCustomerBeContacted($partner->getIsCustomerBeContacted());
                $dc->setIsNutrimeOffered($partner->getIsNutrimeOffered());
                $dc->setIsNotContacted($partner->getIsNotContacted());
                $dc->setIsDestroySample($partner->getIsDestroySample());
                $dc->setSampleCanBeUsedForScience($partner->getSampleCanBeUsedForScience());
                $dc->setIsFutureResearchParticipant($partner->getIsFutureResearchParticipant());
                $dc->setCanViewShop($partner->getCanViewShop());
                $dc->setCanCustomersDownloadReports($partner->getCanCustomersDownloadReports());
                $dc->setIsPrintedReportSentToPartner($partner->getIsPrintedReportSentToPartner());
                $dc->setIsPrintedReportSentToDc($partner->getIsPrintedReportSentToDc());
                $dc->setIsPrintedReportSentToCustomer($partner->getIsPrintedReportSentToCustomer());
                $dc->setIsDigitalReportGoesToCustomer($partner->getIsDigitalReportGoesToCustomer());
                $dc->setIdRecallGoesTo($partner->getIdRecallGoesTo());

                $totalDataRows += 1;
            }
            $em->flush();
        }            

        echo "Total:".$totalDataRows." have been added to the database \n";
        echo "End\n";
    }

    protected function parseDataOne($arr) {    
        $arr2 = $this->prepareData($arr);        
        $data = [];
        $staticMappingRule = array(
            "usa" => 236, "mexico" => 114, "" => NULL
        );
        foreach ($arr2 as $a) {            
            $data[] = array(
                'setTypeId'                     =>  22, // Doctor Estrogen Gene Test
                'setFirstName'                  => (isset($a[1]) ? $a[1] : NULL),
                'setSurname'                    => (isset($a[2]) ? $a[2] : NULL),
                'setDistributionChannel'        => (isset($a[3]) ? $a[3] : NULL),
                'setContactEmail'               => (isset($a[4]) ? $a[4] : NULL).(!empty($a[19]) ? ';'.$a[19] : NULL),
                'setTitle'                      => (isset($a[5]) ? $a[5] : NULL),
                'setTelephone'                  => (isset($a[6]) ? $a[6] : NULL).(!empty($a[7]) ? '/'.$a[7] : NULL).(!empty($a[9]) ? '/'.$a[9] : NULL),
                'setFax'                        => (isset($a[8]) ? $a[8] : NULL),
                'setStreet'                     => (isset($a[10]) ? $a[10] : NULL).(!empty($a[14]) ? ' '.$a[14] : NULL),
                'setCity'                       => (isset($a[12]) ? $a[12] : NULL),
                'setPostCode'                   => (isset($a[15]) ? $a[15] : NULL),
                'setCountryId'                  => (isset($a[16]) ? $staticMappingRule[strtolower($a[16])] : NULL),
                'setOtherNotes'                 => (isset($a[17]) ? $a[17] : NULL),
                'setDomain'                     => (isset($a[24]) ? $a[24] : NULL),
                'setInstitution'                => (isset($a[25]) ? $a[25] : NULL),
            );                
        }                 
        return $data;
    }   

    protected function parseDataTwo($arr) {            
        $arr2 = $this->prepareData($arr);
        $data = [];
        $staticMappingRule = array(
            "usa" => 236, "mexico" => 114, "chile" => 45, "canada" => 40,"" => NULL
        );
        foreach ($arr2 as $a) {            
            (isset($a[2]) ? $fullname = explode(",", $a[2]) : NULL);            
            $r = array(                
                'typeId'                     =>  22, // Doctor Estrogen Gene Test
                'partnerId'                  =>  60,
                'firstName'                  => (isset($a[1]) ? $a[1] : NULL),
                'surname'                    => (isset($a[2]) ? $a[2] : NULL),
                'distributionChannel'        => (isset($a[3]) ? $a[3] : NULL),
                'contactEmail'               => (isset($a[4]) ? $a[4] : NULL).(!empty($a[19]) ? ';'.$a[19] : NULL),
                'title'                      => (isset($a[5]) ? $a[5] : NULL),
                'telephone'                  => (isset($a[6]) ? $a[6] : NULL).(!empty($a[7]) ? '/'.$a[7] : NULL).(!empty($a[9]) ? '/'.$a[9] : NULL),
                'fax'                        => (isset($a[8]) ? $a[8] : NULL),
                'street'                     => (isset($a[11]) ? $a[11] : NULL).(!empty($a[10]) ? ' '.$a[10] : NULL),
                'city'                       => (isset($a[12]) ? $a[12] : NULL).(!empty($a[14]) ? ' '.$a[14] : NULL),
                'postCode'                   => (isset($a[15]) ? $a[15] : NULL),
                'countryId'                  => (isset($a[16]) ? $staticMappingRule[strtolower($a[16])] : NULL),                
            );                

            if (trim($r['distributionChannel']) == '') {
                $r['distributionChannel'] = $r['surname'];
            }
            $data[] = $r;
        }                
        return $data;
    }

    protected function prepareData($data) {
        $arr1 = explode("\n", $data);
        $arr2 = [];
        foreach ($arr1 as $a) {
            $item = explode("|", $a);
            if (count($item) > 1) {
                $arr2[] = $item;    
            }            
        }
        return $arr2;
    }
}
