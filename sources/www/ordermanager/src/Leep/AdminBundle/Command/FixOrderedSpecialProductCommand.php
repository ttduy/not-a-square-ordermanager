<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixOrderedSpecialProductCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_ordered_special_product')
            ->setDescription('Fix ordered special product');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
            ->andWhere('p.specialproductid IS NOT NULL');
        $results = $query->getQuery()->getResult();

        foreach ($results as $r) {
            $orderedSpecialProduct = new Entity\OrderedSpecialProduct();
            $orderedSpecialProduct->setIdCustomer($r->getCustomerId());
            $orderedSpecialProduct->setIdSpecialProduct($r->getSpecialProductId());
            $em->persist($orderedSpecialProduct);
        }
        $em->flush();

    }
}
