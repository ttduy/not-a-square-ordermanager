<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateRevenueEstimationCommand extends ContainerAwareCommand {
    protected function configure() {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:update_revenue_estimation')
            ->setDescription('Update Revenue Estimation ');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQuery('select p from App\Database\MainBundle\Entity\Customer p');
        $iterableResult = $query->iterate();
        foreach ($iterableResult as $row) {
            $customer=current($row);
            if ($customer->getPriceOverride() !== null) {
                $customer->setRevenueEstimation($customer->getPriceOverride());
                $em->persist($customer);
                $em->flush();
            }

            $em->clear();
        }
    }
}
