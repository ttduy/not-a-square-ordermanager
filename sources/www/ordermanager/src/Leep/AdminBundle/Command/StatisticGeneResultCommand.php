<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;


class StatisticGeneResultCommand extends ContainerAwareCommand {
    const GENE_ID = 139;
    const DC_ID = 1540;

    protected function configure() {
        $this
            ->setName('leep_admin:statistic_gene_result')
            ->setDescription('Statistic gene result of customer ');
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        // Get all customer ids of expect DC
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from("AppDatabaseMainBundle:CustomerInfo", 'p')
                ->where($query->expr()->eq('p.idDistributionChannel', ':expectDC'))
                ->setParameter('expectDC', self::DC_ID);
        $results = $query->getQuery()->getResult();
        $idsCustomerInDc = [];
        foreach ($results as $key => $value) {
            $idsCustomerInDc[] = $value['id'];
        }

        // Get all customer ids without expect DC
        $query = $em->createQueryBuilder();
        $query->select('p.id')
                ->from("AppDatabaseMainBundle:CustomerInfo", 'p')
                ->where($query->expr()->neq('p.idDistributionChannel', ':expectDC'))
                ->setParameter('expectDC', self::DC_ID);
        $results = $query->getQuery()->getResult();
        $idsCustomerNotInDc = [];
        foreach ($results as $key => $value) {
            $idsCustomerNotInDc[] = $value['id'];
        }

        // Let statistic
        //// Statistic customer in Dc
        $fileName = "/var/www/ordermanager/files/gene/result_genes_customer_in_dc.txt";
        $this->statisticCustomerGene($idsCustomerInDc, $fileName, "in DC");

        //// Statistic customer not in Dc
        $fileName = "/var/www/ordermanager/files/gene/result_genes_customer__not_in_dc.txt";
        $this->statisticCustomerGene($idsCustomerNotInDc, $fileName, " not in DC ");
    }

    private function geneCompare($a, $b) {
        if (strpos($a, '/') === FALSE) {
            return strcmp($a, $b);
        }

        $arr = explode('/', $a);
        $a1 = $arr[1].'/'.$arr[0];
        if (strcmp($a, $b) === 0 ||
            strcmp($a1, $b) === 0) {
            return 0;
        }

        return 1;
    }

    private function statisticCustomerGene($customerIds, $fileName, $typeOutput) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $geneCompare = ['empty', 'C/C', 'T/C', 'T/T'];
        $query = $em->createQueryBuilder();
        $query->select('p.idCustomer, p.result')
                ->from("AppDatabaseMainBundle:CustomerInfoGene", 'p')
                ->where($query->expr()->andX(
                            $query->expr()->in('p.idCustomer', ':customerIds'),
                            $query->expr()->eq('p.idGene', ':geneId')
                    ))
                ->setParameter('customerIds', $customerIds)
                ->setParameter('geneId', self::GENE_ID);
        $geneResults = $query->getQuery()->getResult();

        $geneStatistic = ['count' => [], 'percentage' => [], 'percentageWhole' => []];
        foreach ($geneResults as $geneResult) {
            if (!empty($geneResult['result'])) {
                if (!isset($geneStatistic['count'][$geneResult['result']])) {
                    $geneStatistic['count'][$geneResult['result']] = 0;
                }
                $geneStatistic['count'][$geneResult['result']] += 1;
            }
        }
        $sumNotEmpty = array_sum($geneStatistic['count']);
        $geneStatistic['count']['empty'] = count($geneResults) - $sumNotEmpty;

        foreach ($geneCompare as $geneValue) {
            foreach ($geneStatistic['count'] as $key => $geneRersultCount) {
                if ($this->geneCompare($geneValue, $key) == 0) {
                    $geneStatistic['percentage'][$geneValue] = floatval($geneRersultCount / count($geneResults)) * 100;
                    // fix here;
                    $numberRes = strcmp('empty', $geneValue) === 0 ? ($geneRersultCount + count($customerIds) - count($geneResults)) : $geneRersultCount;
                    $geneStatistic['percentageWhole'][$geneValue] = floatval(($numberRes) / count($customerIds)) * 100;
                    break;
                }
            }
        }

        echo "\n============= ".$typeOutput." ==================\n";
        $noticeNumberCustomer = "\n>>>> Number customer: ".strval(count($customerIds));
        echo $noticeNumberCustomer;
        $noticeNumberCustomerContainGene = "\n>>>> Number customer contain gene LAK1: ".strval(count($geneResults));
        echo $noticeNumberCustomerContainGene;

        $printStrings = $noticeNumberCustomer.$noticeNumberCustomerContainGene;
        foreach ($geneStatistic['percentage'] as $geneRes => $numberGeneRes) {
            $name = $geneRes;
            if (strcmp('empty', $geneRes) === 0) {
                $name = "Empty";
                $printString = "\n".$name.": number customer have LAK1 but empty: ".strval($geneStatistic['count'][$geneRes]).
                                ", number whole customer (include customer have LAK1 but empty and customer haven't LAK1): ".strval($geneStatistic['count'][$geneRes] + count($customerIds) - count($geneResults)).
                                ", percentage on customer have LAK1: ".$numberGeneRes.
                                ", percentage on whole customers: ".$geneStatistic['percentageWhole'][$geneRes];
            }
            else {
                $printString = "\n".$name.": number customer: ".strval($geneStatistic['count'][$geneRes]).
                            ", percentage on customer have LAK1: ".$numberGeneRes.
                            ", percentage on whole customers: ".$geneStatistic['percentageWhole'][$geneRes];
            }
            echo $printString;
            $printStrings .= $printString;
        }
        echo "\n";

        file_put_contents($fileName, $printStrings);
    }
}

?>