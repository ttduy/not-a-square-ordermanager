<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:export_database
*/
class BackupDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:backup_database')
            ->setDescription('Backup Database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $dbCommand = 'sudo docker exec -t -i novogenia-ordermanager-dev-mysql bash';

        // $dbCommandFormat = 'mysqldump -u%s -pwd%s %s > %s/%s.sql';
        // $fileName = 'ordermanager_'.date('Ymd_His');
        // $dbCommand = sprintf($dbCommandFormat,
        //                 $container->getParameter('database_user'),
        //                 $container->getParameter('database_password'),
        //                 $container->getParameter('database_name'),
        //                 $container->getParameter('exported_database_dir'),
        //                 $fileName
        //             );
        //
        // // dump to a file in physical directory by exec mysqldump command line
        exec($dbCommand);
        //
        // $fileName = $container->getParameter('exported_database_dir'). "temp";
        // // Encrypt Command
        // $cm = "gpg --passphrase ". $container->getParameter('pwd_encrypt')." -c ". $fileName.".sql";
        // exec($cm);

    }
}
