<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ImportDrugFromExcelCommand extends ContainerAwareCommand
{
    const NUMBER_INGREDIENT = 469;
    const HIGHEST_ROW = 2695;
    const HIGHEST_COLUMN = 'RO';

    protected function configure() {
        $this
            ->setName('leep_admin:import_drug_from_excel')
            ->setDescription("Import drug from excel file")
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                "Where is the Excel file");

    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $sourceFile = $input->getArgument('sourceFile');

        $objReader = \PHPExcel_IOFactory::createReaderForFile($sourceFile);
        $objPHPExcel = $objReader->load($sourceFile);

        $sheetUploadTable = $objPHPExcel->getSheetByName('UPLOAD TABLE');
        $highestRow = self::HIGHEST_ROW;
        echo "Highest row: ".$highestRow."\n";
        $highestColumn = self::HIGHEST_COLUMN;
        $headers = $sheetUploadTable->rangeToArray('A1:RO1', NULL, TRUE, FALSE, TRUE)[1];
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheetUploadTable->rangeToArray('A' . $row.':'.$highestColumn.$row, NULL, TRUE, FALSE, TRUE);
            $this->updateRecord($rowData[$row], $headers);
            echo "\n>>>>> Done row: ".$row;
        }
    }

    private function updateRecord($rowData, $header) {
        $drug = new Entity\Drug();
        $drug->setDrugName(trim($rowData['B']));
        $drug->setCardioDrugName(trim($rowData['C']));
        $drug->setPubchemId(trim($rowData['D']));
        $drug->setDrugBankId(trim($rowData['E']));
        $drug->setMainPathwayLink(trim($rowData['F']));
        $drug->setLevel1($rowData['G']);
        $drug->setLevel2($rowData['H']);
        $drug->setLevel3($rowData['I']);
        $drug->setLevel4($rowData['J']);
        if ($rowData['K'] == 'x') {
            $drug->setCypInteraction(TRUE);
        }
        else {
            $drug->setCypInteraction(FALSE);
        }
        $drug->setCypBreakDown(floatval($rowData['L']));
        $drug->setHalfLifeH(floatval($rowData['M']));
        $drug->setReferencesCombined($rowData['N']);
        $container = $this->getContainer();
        $ingredients = Business\Drug\Utils::loadIngredientData($container, '');
        $secondPartColumn = 'O';
        $lastColumn = 'RO';
        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p.variableName')
                ->from('AppDatabaseMainBundle:DrugIngredient', 'p');
        $drugIngredients = $query->getQuery()->getResult();
        $ingredientsExcel = [];
        $listIngredient = [];
        foreach ($rowData as $key => $value) {
            if ($value == 'x') {
                $ingredientsExcel[trim($header[$key])] = 1;
            }
            elseif (empty($value)) {
                 $ingredientsExcel[trim($header[$key])] = 0;
            }
            else {
                $ingredientsExcel[trim($header[$key])] = $value;
            }
        }
        foreach ($drugIngredients as $key => $value) {
            $variableName = $value['variableName'];
            $listIngredient[] = $variableName.' = '.$ingredientsExcel[$variableName];
        }
        $drugIngredients_str = implode("\n", $listIngredient);
        $drug->setIngredients($drugIngredients_str);
        $em->persist($drug);
        $em->flush();
    }
}