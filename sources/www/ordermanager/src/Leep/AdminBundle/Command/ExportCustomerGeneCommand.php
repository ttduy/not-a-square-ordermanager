<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Customer\Util;

class ExportCustomerGeneCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:export_customer_gene')
            ->setDescription('Export customer order and gene info');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');

        $title = [
            "\"Customer\"",
            "\"Birthday\"",
            "\"Email\"",
            "\"Gender\"",
            "\"DistributionChannel\"",
            "\"Order Number\"",
            "\"Date ordered\"",
            "\"Address.\"",
            "\"Country\"",
            "\"Körpergewicht (kg)\"",
            "\"Körpergröße (cm)\"",
            "\"Leiden Sie an einer der folgenden Krankheiten?\"",
            "\"Medizinische Geschichte\"",
        ];

        // Extraction form question
        echo "Extracting Form Questions... \n";
        $questions = [
            "Körpergewicht (kg)",
            "Körpergröße (cm)",
            "Leiden Sie an einer der folgenden Krankheiten?",
            "Medizinische Geschichte"
        ];

        $query = $em->createQueryBuilder();
        $query->select('p.id, p.question, p.questionData')
            ->from('AppDatabaseMainBundle:FormQuestion', 'p')
            ->where('p.question IN (:questions)')
            ->setParameter('questions', $questions);

        $result = $query->getQuery()->getResult();
        $questions = [];
        foreach ($result as $row) {
            $questions[$row['question']] = [
                'id' =>     $row['id'],
                'data' =>   $row['questionData']
            ];
        }

        // Extract gene name
        $genes = $em->getRepository("AppDatabaseMainBundle:Genes")->createQueryBuilder('p')->getQuery()->iterate();
        $geneNameList = [];
        $titleGeneIndex = [];
        foreach ($genes as $gene) {
            $gene = $gene[0];
            $geneNameList[$gene->getId()] = $gene->getGenename();
            $titleGeneIndex[$gene->getGenename()] = count($title);
            $title[] = '"'.$gene->getGenename().'"';
            $em->detach($gene);
        }

        // Extract customer data
        $extractResult = [];
        $customerInfoList = $em->getRepository("AppDatabaseMainBundle:CustomerInfo")->createQueryBuilder('p')->getQuery()->iterate();
        $checkIndex = 0;
        $fileIndex = 0;
        echo "Extracting Customer Order... \n";
        foreach ($customerInfoList as $customerInfo) {
            $customerInfo = $customerInfo[0];

            $firstname = $customerInfo->getFirstName();
            $surname = $customerInfo->getSurName();
            $birthday = $formatter->format($customerInfo->getDateofbirth(), 'date');
            $gender = $formatter->format($customerInfo->getGenderid(), 'mapping', 'LeepAdmin_Gender_List');
            $email = $customerInfo->getEmail();
            $distributionchannel = $formatter->format($customerInfo->getIdDistributionChannel(),
                'mapping', 'LeepAdmin_DistributionChannel_List');

            // Create new record and save customer general information
            $newRecord = [
                'name' =>       "$firstname $surname",
                'birthday' =>   $birthday,
                'gender' =>     $gender,
                'email' =>      $email,
                'dc' =>         $distributionchannel,
                'orders' =>     [],
                'genes' =>      []
            ];

            foreach ($geneNameList as $geneId => $geneName) {
                $newRecord['genes'][$geneName] = '';
            }

            // Get customer genes data
            $customerInfoGenes = $em->getRepository("AppDatabaseMainBundle:CustomerInfoGene")
                ->createQueryBuilder('p')
                ->where('p.idCustomer = :idCustomer')
                ->setParameter('idCustomer', $customerInfo->getId())
                ->getQuery()
                ->iterate();

            foreach ($customerInfoGenes as $customerGene) {
                $customerGene = $customerGene[0];
                if (!array_key_exists($customerGene->getIdGene(), $geneNameList)) {
                    continue;
                }

                $newRecord['genes'][$geneNameList[$customerGene->getIdGene()]] = $customerGene->getResult();
                $em->detach($customerGene);
            }

            $customerList = $em->getRepository("AppDatabaseMainBundle:Customer")
                ->createQueryBuilder('p')
                ->where('p.idCustomerInfo = :idCustomerInfo')
                ->setParameter('idCustomerInfo', $customerInfo->getId())
                ->getQuery()
                ->iterate();

            foreach ($customerList as $customer) {
                $customer = $customer[0];
                $checkIndex++;

                // Save order general information
                $address = sprintf("%s, %s %s",
                $customer->getStreet(),
                $customer->getCity(),
                $customer->getPostCode());
                $order = [
                    'orderNumber' =>    $customer->getOrdernumber(),
                    'dateOrdered' =>    $formatter->format($customer->getDateordered(), 'date'),
                    'address' =>        $address,
                    'country' =>        $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List'),
                    'answers' =>         []
                ];

                // Get order answers
                $answerList = $em->getRepository("AppDatabaseMainBundle:OrderedQuestion")
                    ->createQueryBuilder('p')
                    ->where('p.idCustomer = :idCustomer')
                    ->setParameter('idCustomer', $customer->getId())
                    ->getQuery()
                    ->iterate();

                $answers = [];
                foreach ($answerList as $answer) {
                    $answer = $answer[0];
                    $answers[$answer->getIdFormQuestion()] = $answer->getAnswer();
                    $em->detach($answer);
                }

                // Match answer with required
                foreach ($questions as $question => $questionData) {
                    if (array_key_exists($questionData['id'], $answers)) {
                        $order['answers'][$question] = $answers[$questionData['id']];
                    } else {
                        $order['answers'][$question] = '';
                    }
                }

                // Save order
                $newRecord['orders'][] = $order;
                $em->detach($customer);
            }
            $em->detach($customerInfo);
            $extractResult[] = $newRecord;

            if ($checkIndex >= 2000) {
                $em->clear();
                $fileIndex++;
                $checkIndex = 0;
                echo "Exporting files... $fileIndex\n";

                $fileOutput = [implode(';', $title)];
                foreach ($extractResult as $key => $result) {
                    for ($index = 0; $index < count($result['orders']); $index++) {
                        $output = [
                            '"'.$result['name'].'"',
                            '"'.$result['birthday'].'"',
                            '"'.$result['email'].'"',
                            '"'.$result['gender'].'"',
                            '"'.$result['dc'].'"'
                        ];

                        $order = $result['orders'][$index];
                        $output[] = '"'.$order['orderNumber'].'"';
                        $output[] = '"'.$order['dateOrdered'].'"';
                        $output[] = '"'.$order['address'].'"';
                        $output[] = '"'.$order['country'].'"';

                        if (array_key_exists('Körpergewicht (kg)', $order['answers'])) {
                            $output[] = '"'.$order['answers']['Körpergewicht (kg)'].'"';
                        } else {
                            $output[] = '""';
                        }

                        if (array_key_exists('Körpergröße (cm)', $order['answers'])) {
                            $output[] = '"'.$order['answers']['Körpergröße (cm)'].'"';
                        } else {
                            $output[] = '""';
                        }

                        if (array_key_exists('Leiden Sie an einer der folgenden Krankheiten?', $order['answers'])) {
                            $output[] = '"'.$order['answers']['Leiden Sie an einer der folgenden Krankheiten?'].'"';
                        } else {
                            $output[] = '""';
                        }

                        if (array_key_exists('Medizinische Geschichte', $order['answers'])) {
                            $output[] = '"'.$order['answers']['Medizinische Geschichte'].'"';
                        } else {
                            $output[] = '""';
                        }

                        foreach ($titleGeneIndex as $geneName => $indexPosition) {
                            $output[$indexPosition] = '"'.$result['genes'][$geneName].'"';
                        }

                        $fileOutput[] = implode(',', $output);
                    }
                }

                if (file_exists("/var/www/ordermanager/files/temp/export_customer_gene_data_$fileIndex.csv")) {
                    unlink("/var/www/ordermanager/files/temp/export_customer_gene_data_$fileIndex.csv");
                }

                file_put_contents("/var/www/ordermanager/files/temp/export_customer_gene_data_$fileIndex.csv", implode("\n", $fileOutput));
                $extractResult = [];
            }
        }

        echo "Finished.\n";
    }
}