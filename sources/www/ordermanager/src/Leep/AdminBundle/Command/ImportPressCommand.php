<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: sourceId = '[file path]' | separator = '|' as default
php app/console leep_admin:importPress /var/www/ordermanager/database/PR_010815.csv
*/
class ImportPressCommand extends ContainerAwareCommand
{
    public $countryList = array();
    public $languageList = array();

    // consider mail.com which support over 200 domains FREE
    public $knownMailServiceDomains = array(
                                                'gmail.com',
                                                'aol.com',
                                                'msn.com',
                                                'hotmail.com',
                                                'mail.com',
                                                'outlook.com',
                                                'gmx.com',
                                                'facebook.com',
                                                'inbox.com',
                                                'yandex.com',
                                                'shortmail.com',
                                                'yahoo.com',
                                                'zoho.com',
                                                'hushmail.com',
                                                'hushmail.me',
                                                'hush.com',
                                                'hush.ai',
                                                'mac.hush.com',
                                                'gmail.de',
                                                'aol.de',
                                                'msn.de',
                                                'hotmail.de',
                                                'mail.de',
                                                'outlook.de',
                                                'gmx.de',
                                                'facebook.de',
                                                'inbox.de',
                                                'yandex.de',
                                                'shortmail.de',
                                                'yahoo.de',
                                                'zoho.de',
                                                'hushmail.de',
                                                'hushmail.de',
                                                'hush.de',
                                                'hush.de',
                                                'mac.hush.de',
                                                't-online.de',
                                                'gmail.com',
                                                'aol.com',
                                                'springer.com',
                                                'web.de',
                                                'thieme.de',
                                                'gmx.de',
                                                'yahoo.com',
                                                'newsrx.com',
                                                'hotmail.com',
                                                'urban-vogel.de',
                                                'freelancejournalism.com',
                                                'bild.de',
                                                'elsevier.com',
                                                'comcast.net',
                                                'hearst.com',
                                                'meredith.com',
                                                'gmx.net',
                                                'yahoo.de',
                                                'aarp.org',
                                                'sueddeutsche.de',
                                                'bauerverlag.de',
                                                'online.de',
                                                'hotmail.co.uk',
                                                'yahoo.co.uk',
                                                'msn.com'
                                            );

    protected function configure()
    {
        $this
            ->setName('leep_admin:importPress')
            ->setDescription('Import Press From CSV File')
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                'Where is the csv file'
            )
            ->addArgument(
                'separator',
                InputArgument::OPTIONAL,
                'what is separator, | as default'
            )
            ->addArgument(
                'creationDate',
                InputArgument::OPTIONAL,
                'Creation Date for new imported entry, format Y/m/d, default is 2015/08/01'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $sourceFile = $input->getArgument('sourceFile');
        $separator = ($input->getArgument('separator') ? $input->getArgument('separator') : '|');
        $creationDate = \DateTime::createFromFormat('Y/m/d', $input->getArgument('creationDate') ? $input->getArgument('creationDate') : '2015/08/01');

        $numberOfNewEntries = 0;
        $totalDataRows = 0;
        $counter = 0;

        //$daysMap = Business\Lead\Constants::getDayMaps();
        if (($inputHandle = fopen($sourceFile, "r")) !== FALSE) {
            $this->prepareLanguageList();
            $this->prepareCountryList();

            while (($arrFields = fgetcsv($inputHandle, 0, $separator)) !== FALSE) {
                $totalDataRows++;

                $entryData = $this->parseData($arrFields);

                if (!$this->existInDB($entryData)) {
                    $numberOfNewEntries++;
                    $counter++;

                    $newPress = new Entity\Press();

                    foreach ($entryData as $key => $value) {
                        $setMethod = 'set' . ucfirst($key);
                        $newPress->$setMethod($value);
                    }

                    $newPress->setCreationDate($creationDate);

                    $em->persist($newPress);
                }

                if ($counter % 500 == 0) {
                    $em->flush();
                }
                echo $counter."\n";
            }

            $em->flush();
        }

        echo "Number of rows in CSV: " . $totalDataRows . "\n";
        echo "Number of new Entries: " . $numberOfNewEntries . "\n";
    }

    protected function existInDB($entryData) {
        return false;
    }

    protected function parseData($arr) {
        // Contact ID|Language|Presse|press mailing list (filter 2)|company (filter 2)|domain (filter 3)|main focus (filter 4)|type (tv, radio, magazine, newspaper, blog, information service, organisation/foundaition)|scientific or non-scientific|source|weibl.|unisex|Titel des Kontakts|first name|second name|(publishing) company|street|zip|city|region|country|Email1|Email2|Email3|Email4|Email5/HP|Email6|Email7|Email8|Office Phone|Mobile|Fax
        $arrTmp = array();
        foreach ($arr as $entry) {
            $arrTmp[] = trim(self::cleanDoubleQuotes($entry));
        }
        $arr = $arrTmp;
        $this->processData($arr);

        $data = array(
                                        'idContact'                 => $arr[0],
                                        'idLanguage'                => $arr[1],
                                        'isPress'                   => $arr[2],
                                        'mailingList'               => $arr[3],
                                        'company'                   => $arr[4],
                                        'domain'                    => $arr[5],
                                        'mainFocus'                 => $arr[6],
                                        'type'                      => $arr[7],
                                        'isScientific'              => $arr[8],
                                        'source'                    => $arr[9],
                                        'idGender'                  => $arr[10],
                                        'isUnisex'                  => $arr[11],
                                        'title'                     => $arr[12],
                                        'firstName'                 => $arr[13],
                                        'surName'                   => $arr[14],
                                        'publishingCompany'         => $arr[15],
                                        'street'                    => $arr[16],
                                        'postalCode'                => $arr[17],
                                        'city'                      => $arr[18],
                                        'region'                    => $arr[19],
                                        'idCountry'                 => $arr[20],
                                        'email'                     => $arr[21],
                                        'officePhone'               => $arr[29],
                                        'mobilePhone'               => $arr[30],
                                        'fax'                       => $arr[31]
                                    );

        return $data;
    }

    protected function cleanDoubleQuotes($str) {
        if (substr($str, 0, 1) == '"') {
            $str = substr($str, 1);
        }

        if (substr($str, -1, 1) == '"') {
            $str = substr($str, -1);
        }

        return $str;
    }

    protected function prepareName($arr) {
        $name = null;

        if ($arr[9]) {
            $name .= $arr[9];
        }

        if ($arr[10]) {
            if ($name) {
                $name .= ' ';
            }

            $name .= $arr[10];
        }

        return $name;
    }

    protected function prepareSource($arr) {
        $sourceData = array();
        if ($arr[3]) {
            $sourceData[] = 'Job Description: ' . $arr[3];
        }
        if ($arr[4]) {
            $sourceData[] = 'Physician Specialty: ' . $arr[4];
        }
        if ($arr[5] && !is_int($arr[5])) {
            $sourceData[] = 'Partner: ' . $arr[5];
        }
        if ($arr[6] && !is_int($arr[6])) {
            $sourceData[] = 'Distribution Channel: ' . $arr[6];
        }
        if ($arr[7] && !is_int($arr[7])) {
            $sourceData[] = 'Acquisiteur: ' . $arr[7];
        }
        if ($arr[14] && !is_int($arr[14])) {
            $sourceData[] = 'Country: ' . $arr[14];
        }
        if ($arr[15]) {
            $sourceData[] = 'Email: ' . $arr[15];
        }
        if ($arr[17]) {
            $sourceData[] = 'Fax: ' . $arr[17];
        }
        if ($arr[18]) {
            $sourceData[] = 'Website: ' . $arr[18];
        }

        return (empty($sourceData) ? null : implode("\n", $sourceData));
    }

    protected function prepareCountryList() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrCountries = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Countries')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrCountries[strtolower($entry->getCountry())] = $entry->getId();
            }
        }

        $this->countryList = $arrCountries;
    }

    protected function prepareLanguageList() {
        /*$container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrLanguages = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrLanguages[strtolower($entry->getLocale())] = $entry->getId();
            }
        }

        $this->languageList = $arrLanguages;*/

        $this->languageList = array(
            'de' => 1,
            'en' => 2
        );
    }

    // 0
    protected function processContactId(&$arr) {
        $result = null;
        $value = $arr[0];
        if ($value) {
            $result = $value;
        }
        $arr[0] = $result;
    }

    // 1
    protected function processLanguage(&$arr) {
        $value = $arr[1];
        $result = $value;
        
        if ($value && array_key_exists(strtolower($value), $this->languageList)) {
            $result = $this->languageList[strtolower($value)];
        }
        else {
            if ($value) {
                echo "Language: ".strtolower($value)."\n";
                exit();
            }
        }
        $arr[1] = $result;
    }

    // 2
    protected function processPress(&$arr) {
        $result = null;
        $value = $arr[2];
        if ($value) {
            $result = 1;
        }
        $arr[2] = $result;
    }

    // 3
    protected function processPressMailingList(&$arr) {
        $value = $arr[3];
        $result = $value;
        $arrMailingList = Business\Press\Constant::getMailingListTypes();
        
        if ($value && in_array(ucwords($value), $arrMailingList)) {
            $result = array_search(ucwords($value), $arrMailingList);
        }
        else {
            if ($value) {
                echo "Mailing List: ".strtolower($value)."\n";
                exit();
            }
        }
        $arr[3] = $result;
    }

    // 4
    protected function processCompany(&$arr) {
        $result = null;
        $value = $arr[4];
        if ($value) {
            $result = $value;
        }
        $arr[4] = $result;
    }

    // 5
    protected function processDomain(&$arr) {
        $result = null;
        $value = $arr[5];
        if ($value) {
            $result = $value;
        }
        $arr[5] = $result;
    }

    // 6
    protected function processMainFocus(&$arr) {
        $result = null;
        $value = $arr[6];
        if ($value) {
            $result = $value;
        }
        $arr[6] = $result;
    }

    // 7
    protected function processType(&$arr) {
        $value = $arr[7];
        $result = $value;
        $arrTypes = Business\Press\Constant::getTypes();
        print_r($arrTypes);
        if ($value && in_array($value, $arrTypes)) {
            $result = array_search($value, $arrTypes);
        }
        else {
            if ($value) {
                echo "Type: ".$value."\n";
                exit();
            }
        }
        $arr[7] = $result;
    }

    // 8
    protected function processIsScientific(&$arr) {
        $result = null;
        $value = $arr[8];
        if ($value) {
            $result = 1;
        }
        $arr[8] = $result;
    }

    // 9
    protected function processSource(&$arr) {
        $result = null;
        $value = $arr[9];
        if ($value) {
            $result = $value;
        } 
        $arr[9] = $result;
    }

    // 10
    protected function processGender(&$arr) {
        $result = Business\Base\Constant::GENDER_MALE;
        $value = $arr[10];
        if ($value) {
            $result = Business\Base\Constant::GENDER_FEMALE;
        } 
        $arr[10] = $result;
    }

    // 11
    protected function processIsUnisex(&$arr) {
        $result = null;
        $value = $arr[11];
        if ($value) {
            $result = 1;
        } 
        $arr[11] = $result;
    }

    // 12
    protected function processTitle(&$arr) {
        $result = null;
        $value = $arr[12];
        if ($value) {
            $result = $value;
        } 
        $arr[12] = $result;
    }

    // 13
    protected function processFirstName(&$arr) {
        $result = null;
        $value = $arr[13];
        if ($value) {
            $result = $value;
        } 
        $arr[13] = $result;
    }

    // 14
    protected function processSurName(&$arr) {
        $result = null;
        $value = $arr[14];
        if ($value) {
            $result = $value;
        } 
        $arr[14] = $result;
    }

    // 15
    protected function processPublishingCompany(&$arr) {
        $result = null;
        $value = $arr[15];
        if ($value) {
            $result = $value;
        } 
        $arr[15] = $result;
    }

    // 16
    protected function processStreet(&$arr) {
        $result = null;
        $value = $arr[16];
        if ($value) {
            $result = $value;
        } 
        $arr[16] = $result;
    }

    // 17
    protected function processZip(&$arr) {
        $result = null;
        $value = $arr[17];
        if ($value) {
            $result = $value;
        } 
        $arr[17] = $result;
    }

    // 18
    protected function processCity(&$arr) {
        $result = null;
        $value = $arr[18];
        if ($value) {
            $result = $value;
        } 
        $arr[18] = $result;
    }

    // 19
    protected function processRegion(&$arr) {
        $result = null;
        $value = $arr[19];
        if ($value) {
            $result = $value;
        } 
        $arr[19] = $result;
    }

    // 20
    protected function processCountry(&$arr) {
        $value = $arr[20];
        $result = $value;
        
        if ($value && array_key_exists(strtolower($value), $this->countryList)) {
            $result = $this->countryList[strtolower($value)];
        }
        else {
            if ($value) {
                echo "Country: ".strtolower($value)."\n";
                exit();
            }
        }
        $arr[20] = $result;
    }

    // 21 -> 28
    protected function processEmail(&$arr) {
        $arrEmails = null;
        for ($i = 21; $i <= 28; $i++) {
            $value = $arr[$i];
            $result = $value;

            if ($value) {
                $pattern = '/([a-zA-Z0-9._%+-]+)@([a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/';
                preg_match_all($pattern, $value, $matches);

                // get emails
                if (sizeof($matches[0]) > 0) {
                    if (!$arrEmails) {
                        $arrEmails = array();
                    }

                    $arrEmails = array_merge($arrEmails, $matches[0]);
                }

                // get domains
                if (!$arr[5]) {
                    // get the FIRST detected Domain
                    $arrDomains = $this->analyzeDomains($matches[2]);
                    $arr[5] = (!empty($arrDomains) ? $arrDomains[0] : null);
                }
            }
        }

        $arr[21] = ($arrEmails ? implode(';', $arrEmails) : $arrEmails);
    }

    // get Domains
    protected function analyzeDomains($arrDomains) {
        $result = array();
        if (sizeof($arrDomains) > 0) {
            $result = array_values(array_diff($arrDomains, $this->knownMailServiceDomains));
        }
        return $result;
    }

    // 29
    protected function processOfficePhone(&$arr) {
        $result = null;
        $value = $arr[29];
        if ($value) {
            $result = $value;
        }

        $arr[29] = $result;
    }

    // 30
    protected function processMobilePhone(&$arr) {
        $result = null;
        $value = $arr[30];
        if ($value) {
            $result = $value;
        }

        $arr[30] = $result;
    }

    // 31
    protected function processFax(&$arr) {
        $result = null;
        $value = $arr[31];
        if ($value) {
            $result = $value;
        }

        $arr[31] = $result;
    }


    protected function processData(&$arr) {
        $this->processContactId($arr);
        $this->processLanguage($arr);
        $this->processPress($arr);
        $this->processPressMailingList($arr);
        $this->processCompany($arr);
        $this->processDomain($arr);
        $this->processMainFocus($arr);
        $this->processType($arr);
        $this->processIsScientific($arr);
        $this->processSource($arr);
        $this->processGender($arr);
        $this->processIsUnisex($arr);
        $this->processTitle($arr);
        $this->processFirstName($arr);
        $this->processSurName($arr);
        $this->processPublishingCompany($arr);
        $this->processStreet($arr);
        $this->processZip($arr);
        $this->processCity($arr);
        $this->processRegion($arr);
        $this->processCountry($arr);
        $this->processEmail($arr);
        $this->processOfficePhone($arr);
        $this->processMobilePhone($arr);
        $this->processFax($arr);
    }

}
