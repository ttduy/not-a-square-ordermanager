<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class GeneResultMigrationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:gene_result_migration')
            ->setDescription('Gene Result Migration');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $dbHelper = $container->get('leep_admin.helper.database');
        $em = $container->get('doctrine')->getEntityManager();

        // clear CustomerInfoGene
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:CustomerInfoGene')->getQuery()->execute();

        // get all orders
        $query = $em->createQueryBuilder();
        $query->select('p.id, p.idCustomerInfo')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('(p.idCustomerInfo IS NOT NULL) AND (p.idCustomerInfo != 0)')
            ->addOrderBy('p.idCustomerInfo', 'DESC')
            ->addOrderBy('p.dateordered', 'ASC');

        $orders = $query->getQuery()->getResult();

        $total = sizeof($orders);
        if ($orders && sizeof($orders) > 0) {
            // check each Order
            $i = 0;
            foreach ($orders as $order) {
                $idCustomer = $order['idCustomerInfo'];
                $idOrder = $order['id'];

                // get all gene result relating to being-checked Order
                $queryGeneResult = $em->createQueryBuilder();
                $queryGeneResult->select('p')
                                ->from('AppDatabaseMainBundle:AnalysisResults', 'p')
                                ->andWhere('p.customerid = :idOrder')
                                ->setParameter('idOrder', $idOrder);

                $geneResults = $queryGeneResult->getQuery()->getResult();

                if ($geneResults && sizeof($geneResults) > 0) {
                    // check each gene result
                    foreach($geneResults as $result) {
                        // delete existing one
                        $filters = array('idCustomer' => $idCustomer, 'idGene' => $result->getGeneId());
                        $dbHelper->delete($em, 'AppDatabaseMainBundle:CustomerInfoGene', $filters);

                        // add new one
                        $newGeneResult = new Entity\CustomerInfoGene();
                        $newGeneResult->setIdCustomer($idCustomer);
                        $newGeneResult->setIdGene($result->getGeneId());
                        $newGeneResult->setResult($result->getResult());

                        $em->persist($newGeneResult);
                    }
                }

                $i++;
                echo "Process ".$i.'/'.$total."\n";
                if ($i % 100 == 0) {
                    $em->flush();
                    $em->clear();
                }
            }

            $em->flush();
        }
    }
}
