<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Leep\AdminBundle\Business\OrderBackup\Util;
use Leep\AdminBundle\Business\OrderBackup\Constant;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

class UpdateGenoStorageCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('leep_admin:update_geno_storage')
            ->setDescription('Re-Update geno storage base on batch');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Clear all storage loading
        $em->createQueryBuilder()
            ->update('AppDatabaseMainBundle:GenoStorages', 'p')
            ->set('p.loading', 0)
            ->where('p.id > 0')
            ->getQuery()
            ->execute();

        $em->createQueryBuilder()
            ->update('AppDatabaseMainBundle:GenoStorageCell', 'p')
            ->set('p.loading', 0)
            ->where('p.id > 0')
            ->getQuery()
            ->execute();

        // Get all batch and then update storage
        $batchList = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoBatches')->findAll();
        foreach ($batchList as $batch) {
            // Get location
            $cell = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoStorageCell')->findOneById($batch->getLocation());
            if ($cell) {
                $storage = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:GenoStorages')->findOneById($cell->getStorageId());
                $cell->setLoading($batch->getAmountInStock() + $cell->getLoading());
                $storage->setLoading($batch->getAmountInStock() + $storage->getLoading());
                $em->persist($cell);
                $em->persist($storage);
                $em->flush();
            }
        }

        echo "Finished.\n";
    }
}
