<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:test')
            ->setDescription('Test');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $this->checkResultConflict();
        

    }

    public function checkResultConflict() {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $parser = $container->get('leep_admin.gene_manager.business.parser');
        $parser->init();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:GeneManagerResultHistory', 'p')
            ->orderBy('p.id', 'ASC');
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $parser->parseResult($r->getResult());
        }

        $conflicts = $parser->checkConflict();
        foreach ($conflicts as $orderNumber => $conflictGenes) {
            echo "------------------------------- \n";
            echo $orderNumber."\n";
            foreach ($conflictGenes as $rsNumber => $gene) {
                $resultData = array();
                foreach ($gene['result'] as $r) {
                    if (trim($r) == '') {
                        $resultData[] = '<empty>';
                    }
                    else {
                        $resultData[] = $r;
                    }
                }
                $resultData = array_reverse($resultData);

                echo '  '.$gene['name'].': '.implode(' ', $resultData)."\n";
            }
            echo "\n";
        }
    }

    public function analyseReportSpaces() {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Customer reports
        $result = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findAll();
        $inUse = array();
        foreach ($result as $cr) {
            $inUse[$cr->getFileName()] = 1;
            $inUse[$cr->getLowResolutionFilename()] = 1;
            $inUse[$cr->getWebCoverFilename()] = 1;
            $inUse[$cr->getWebCoverLowResolutionFilename()] = 1;
            $inUse[$cr->getBodCoverFilename()] = 1;
            $inUse[$cr->getBodCoverLowResolutionFilename()] = 1;            
        }

        // Check
        $time = new \DateTime();
        $reportFilePath = $container->getParameter('kernel.root_dir').'/../web/report';
        $files = scandir($reportFilePath);
        $total = 0;
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                if (!isset($inUse[$file])) {
                    $reportFile = $reportFilePath.'/'.$file;
                    $total += filesize($reportFile);

                    $time->setTimestamp(filemtime($reportFile));
                    echo $time->format('d/m/Y H:i:s')." ".$file."\n";                    
                }
            }
        }

        $gb = 1024*1024*1024;
        echo number_format($total / $gb, 2)."\n";
    }

    public function analyseData() {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $reportFilePath = $container->getParameter('kernel.root_dir').'/../web/report';
        $shipmentFilePath = $container->getParameter('kernel.root_dir').'/../files/shipment';
        for ($i = 1; $i <= 9; $i++) {
            $startDate = \DateTime::createFromFormat('Y-m-d', '2014-'.$i.'-1');
            $d = $startDate->format('t');
            $endDate = \DateTime::createFromFormat('Y-m-d', '2014-'.$i.'-'.$d);

            echo "From ".$startDate->format('d/m/Y').' to '.$endDate->format('d/m/Y').": ";
            
            // Report files
            $query = $em->createQueryBuilder();
            $query->select('r')
                ->from('AppDatabaseMainBundle:Customer', 'p')
                ->innerJoin('AppDatabaseMainBundle:CustomerReport', 'r', 'WITH', 'p.id = r.idCustomer')
                ->andWhere('(p.dateordered >= :startDate) AND (p.dateordered <= :endDate)')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate);
            $result = $query->getQuery()->getResult();
            $totalReport = 0;
            foreach ($result as $cr) {
                $this->addSize($totalReport, $reportFilePath, $cr->getFileName());
                $this->addSize($totalReport, $reportFilePath, $cr->getLowResolutionFilename());
                $this->addSize($totalReport, $reportFilePath, $cr->getWebCoverFilename());
                $this->addSize($totalReport, $reportFilePath, $cr->getWebCoverLowResolutionFilename());
                $this->addSize($totalReport, $reportFilePath, $cr->getBodCoverFilename());
                $this->addSize($totalReport, $reportFilePath, $cr->getBodCoverLowResolutionFilename());
            }

            // Shipment files
            $query = $em->createQueryBuilder();
            $query->select('b')
                ->from('AppDatabaseMainBundle:Customer', 'p')
                ->innerJoin('AppDatabaseMainBundle:BodShipment', 'm', 'WITH', 'p.id = m.idCustomer')
                ->innerJoin('AppDatabaseMainBundle:BodShipmentBook', 'b', 'WITH', 'm.id = b.idBodShipment')
                ->andWhere('(p.dateordered >= :startDate) AND (p.dateordered <= :endDate)')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate);
            $result = $query->getQuery()->getResult();
            $totalShipment = 0;
            foreach ($result as $b) {
                $this->addSize($totalShipment, $shipmentFilePath, 'shipment_'.$b->getId().'.zip');
                $this->addSize($totalShipment, $shipmentFilePath, 'shipment_'.$b->getId().'_lowres.zip');
            }

            // Grand total
            $total = $totalReport + $totalShipment;
            $gb = 1024*1024*1024;

            echo sprintf("      %s    %s     %s \n",
                number_format($totalReport / $gb, 2),
                number_format($totalShipment / $gb, 2),
                number_format($total / $gb, 2)
            );
        }
    }

    public function addSize(&$total, $path, $file) {
        if (!empty($file)) {
            if (file_exists($path.'/'.$file)) {
                $total += filesize($path.'/'.$file);
            }
        }
    }
}
