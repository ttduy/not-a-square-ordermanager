<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: sourceId = 1 and targetName = 'Swiss Italian'
php app/console leep_admin:copy_language_text_block 1 "Swiss Italian"
*/
class CopyLanguageTextBlockCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:copy_language_text_block')
            ->setDescription('Copy Language Text Block')
            ->addArgument(
                'sourceId',
                InputArgument::REQUIRED,
                'What is ID of source language?'
            )
            ->addArgument(
                'targetName',
                InputArgument::REQUIRED,
                'What is Name of target language?'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // get input sourceId
        $sourceId = $input->getArgument('sourceId');

        // check if source language exists (valid)
        $sourceLanguage = $doctrine->getRepository('AppDatabaseMainBundle:Language')->findOneById($sourceId);
        if (!$sourceLanguage) {
            exit('Source Language ID ' . $sourceId . ' is invalid or does not exist');
        }

        // get input targetName
        $targetLanguageName = $input->getArgument('targetName');

        $targetLanguage = new Entity\Language();
        $targetLanguage->setName($targetLanguageName);
        $targetLanguage->setSortOrder(99);
        $em->persist($targetLanguage);
        $em->flush();

        // now we have ID of targetLanguage, let copy all text block of sourceId to targetLanguage
        $result = $doctrine->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($sourceLanguage->getId());

        $limitInsert = 1000;
        $counter = 0;
        if ($result) {
            foreach ($result as $row) {
                $counter++;
                $inst = new Entity\TextBlockLanguage();
                $inst
                    ->setIdTextBlock($row->getIdTextBlock())
                    ->setIdLanguage($targetLanguage->getId())
                    ->setBody($row->getBody())
                    ->setIsCached(0)
                ;
                $em->persist($inst);

                if ($counter == $limitInsert) {
                    $em->flush();
                    $em->clear();
                    $counter = 0;
                }
            }

            if ($counter > 0) {
                $em->flush();
                $em->clear();
            }
        }
    }
}
