<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

use Leep\AdminBundle\Business\OrderBackup\Util;
use Leep\AdminBundle\Business\OrderBackup\Constant;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

class CreateOrderBackupFileCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('leep_admin:create_order_backup_file')
            ->setDescription('Create zip backup file for order backup');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();

        $orderBackup = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findByStatus([
            Constant::ORDER_BACKUP_STATUS_PROCESSING,
            Constant::ORDER_BACKUP_STATUS_UPLOADING,
        ]);

        if (count($orderBackup) > 0) {
            echo "Waiting for processing order to finished!\n";
            return true;
        }

        $orderBackup = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:OrderBackup')->findAll();

        foreach ($orderBackup as $backup) {
            if ($backup->getStatus() != Constant::ORDER_BACKUP_STATUS_OPEN) {
                continue;
            }

            echo 'Backup #'.$backup->getId().".\n";

            $backup->setStatus(Constant::ORDER_BACKUP_STATUS_PROCESSING);
            $em->persist($backup);
            $em->flush();

            $fileName = Util::createBackupFile($container, $backup->getId());
            if ($backup->getNumberOfOrders() == 0) {
                echo "Empty backup";
                $backup->setStatus(Constant::ORDER_BACKUP_STATUS_EMPTY);
            } else {
                $backup->setFile($fileName);

                $this->calculateMd5Hash($backup);

                $backup->setStatus(Constant::ORDER_BACKUP_STATUS_UPLOADING);
                $em->persist($backup);
                $em->flush();

                $isSuccess = $this->uploadToAmazonS3($backup);

                if ($isSuccess) {
                    $backup->setStatus(Constant::ORDER_BACKUP_STATUS_READY);
                } else {
                    $backup->setStatus(Constant::ORDER_BACKUP_STATUS_ERROR);
                }
            }

            $em->persist($backup);
            $em->flush();
        }

        echo "Finished.\n";
    }

    private function calculateMd5Hash($backup) {
        $backupDir = Business\OrderBackup\Util::getBackupDir($this->getContainer());
        $fileName = $backup->getFile();
        $fileDir = $backupDir.$fileName;

        echo "Calculate MD5 Hash for #".$backup->getId()."\n";
        $md5Hash = md5_file($fileDir);
        $backup->setMd5Hash($md5Hash);
    }

    private function uploadToAmazonS3($backup) {
        $backupDir = Business\OrderBackup\Util::getBackupDir($this->getContainer());
        $fileName = $backup->getFile();
        $fileDir = $backupDir.$fileName;

        echo "Upload #".$backup->getId()." to Amazon S3 Server\n";

        // Instantiate an S3 client
        $s3 = S3Client::factory([
            'credentials' => [
                'key'    => 'AKIAIVPJXDW5X5CQWNGA',
                'secret' => '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',
            ],
            'region' => 'us-east-1'
        ]);

        try {
            $s3->createBucket(['Bucket' => 'order-backup']);
            $s3->waitUntil('BucketExists', ['Bucket' => 'order-backup']);
        } catch (S3Exception $e) {
            // Skip
        }

        try {
            $uploader = UploadBuilder::newInstance()
                ->setClient($s3)
                ->setSource($fileDir)
                ->setBucket('order-backup')
                ->setKey($fileName)
                ->setOption('CacheControl', 'max-age=3600')
                ->build();

            $uploader->upload();
        } catch (MultipartUploadException $e) {
            $uploader->abort();
            echo "Exception: ".$e->getMessage().".\n";
            return false;
        }

        // Remove backup on server
        unlink($fileDir);
        return true;
    }
}
