<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

use Leep\AdminBundle\Business;

class FixBackupDiskAwsCommand extends ContainerAwareCommand {
    private $s3;

    protected function configure() {
        $this
            ->setName('leep_admin:fix_backup_disk_aws')
            ->setDescription('Backup files to S3 server');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        echo "Begin\n";

        // Instantiate an S3 client
        $this->s3 = S3Client::factory([
            'credentials' => [
                'key'    => 'AKIAINK6TVPLDQ4R6YQA',
                'secret' => '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',
            ],

            'region' => 'us-east-1'
        ]);

        $this->fixCustomerReport();
        $this->fixShipments();
        echo "Finished.\n";
    }

    private function fixShipments() {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $checkDate = new \DateTime('-2 month');
        $query = $em->createQueryBuilder();
        $shipmentList = $query->select('p')
                ->from('AppDatabaseMainBundle:BodShipment', 'p')
                ->where('p.status = :status')->setParameter('status', Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_FINISHED)
                ->orderBy('p.creationDatetime', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($shipmentList as $shipment) {
            $shipment = $shipment[0];
            $time = $shipment->getCreationDatetime();

            // Backup file list:
            $files = [
                $shipment->getHighResolutionFile(),
                $shipment->getLowResolutionFile()
            ];

            if ($time && $time >= $checkDate) {
                foreach ($files as $file) {
                    $file = trim($file);
                    if (empty($file)) {
                        continue;
                    }

                    if ($this->s3->doesObjectExist('ordermanager-disk-backup', "shipments/$file")) {
                        $this->getAwsFileBack("shipments", $file, "files/shipment");
                    } else if ($this->s3->doesObjectExist('ordermanager-disk-backup', "dead_shipments/$file")) {
                        $this->getAwsFileBack("dead_shipments", $file, "files/shipment");
                    }
                }
            }

            $em->detach($shipment);
        }
    }

    private function fixCustomerReport() {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $checkDate = new \DateTime('-2 month');
        $query = $em->createQueryBuilder();
        $customerReportList = $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerReport', 'p')
                ->orderBy('p.inputTime', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($customerReportList as $customerReport) {
            $customerReport = $customerReport[0];
            $reportTime = $customerReport->getInputTime();

            // Backup file list:
            $files = [
                // low resolution files
                $customerReport->getWebCoverLowResolutionFilename(),
                $customerReport->getLowResolutionFilename(),
                $customerReport->getBodCoverLowResolutionFilename(),

                // Excel
                $customerReport->getFoodTableExcelFilename(),

                // XML
                $customerReport->getXmlFilename(),

                // Other
                $customerReport->getWebCoverFilename(),
                $customerReport->getFilename(),
                $customerReport->getBodCoverFilename()
            ];

            if ($reportTime && $reportTime >= $checkDate) {
                foreach ($files as $file) {
                    $file = trim($file);
                    if (empty($file)) {
                        continue;
                    }

                    if ($this->s3->doesObjectExist('ordermanager-disk-backup', "customer_reports/$file")) {
                        $this->getAwsFileBack("customer_reports", $file, "web/report");
                    } else if ($this->s3->doesObjectExist('ordermanager-disk-backup', "dead_customer_reports/$file")) {
                        $this->getAwsFileBack("dead_customer_reports", $file, "web/report");
                    }
                }
            }

            $em->detach($customerReport);
        }

        $query = $em->createQueryBuilder();
        $sampleList = $query->select('p')
                ->from('AppDatabaseMainBundle:CryosaveSample', 'p')
                ->orderBy('p.timestamp', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($sampleList as $sample) {
            $sample = $sample[0];
            $timestamp = $sample->getTimestamp();
            $file = trim($sample->getReportFilename());
            if (!empty($file)) {
                if ($timestamp && $timestamp >= $checkDate) {
                    if ($this->s3->doesObjectExist('ordermanager-disk-backup', "cryo_sample_reports/$file")) {
                        $this->getAwsFileBack("cryo_sample_reports", $file, "web/report");
                    }
                }
            }

            $em->detach($sample);
        }
    }

    private function getAwsFileBack($folderKey, $fileName, $folder) {
        $fileDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../'.$folder;
        try {
            $remoteFile = $this->s3->getObject(array(
                'Bucket' => 'ordermanager-disk-backup',
                'Key' =>    "$folderKey/$fileName",
                'SaveAs' => "$fileDir/$fileName"
            ));
        } catch (\Exception $e) {
            echo "Exception: {$e->getMessage()}.\n";
            return;
        }
    }
}
