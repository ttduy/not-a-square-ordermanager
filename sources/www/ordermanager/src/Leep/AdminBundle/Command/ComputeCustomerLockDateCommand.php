<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:compute_customer_lock_date
*/
class ComputeCustomerLockDateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:compute_customer_lock_date')
            ->setDescription('Compute Customer Lock Date')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');  

        $dcSetting = array();
        $dcs = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        foreach ($dcs as $dc) {
            $dcSetting[$dc->getId()] = intval($dc->getDestroyInNumDays());
        }

        $query = $em->createQueryBuilder();
        $orders = $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->innerJoin('AppDatabaseMainBundle:CustomerInfo', 'c', 'WITH', 'p.idCustomerInfo = c.id')
            ->andWhere('FIND_IN_SET('.Business\Customer\Constant::ORDER_STATUS_REPORTED_SUBMITTED.', p.statusList) > 0')
            ->andWhere('c.isDestroySample = 1')
            ->andWhere('p.lockDate IS NULL')
            ->andWhere('(p.isLocked = 0) OR (p.isLocked IS NULL)')
            ->getQuery()
            ->getResult();

        $i = 0;
        foreach ($orders as $order) {
            $idDc = intval($order->getDistributionChannelId());
            $numDay = isset($dcSetting[$idDc]) ? $dcSetting[$idDc] : 0;

            $lockDate = new \DateTime();
            $lockDate->add(new \DateInterval('P'.$numDay.'D'));
            $order->setLockDate($lockDate);
            $i++;
            if ($i % 1000 == 0) {
                $em->flush();
            }

            echo $order->getOrderNumber().": ".$lockDate->format('d/m/Y')."\n";
        }
        $em->flush();
    }
}