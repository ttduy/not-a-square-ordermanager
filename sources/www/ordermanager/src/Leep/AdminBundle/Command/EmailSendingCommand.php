<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:email_sending
*/
class EmailSendingCommand extends ContainerAwareCommand
{
    // refer http://php.net/manual/en/function.image-type-to-mime-type.php
    protected $mimeTypeImages = array(
            'image/gif',
            'image/jpeg',
            'image/png',
            'image/bmp'
        );

    protected $receiverLogStatusList = null;

    protected function configure()
    {
        $this
            ->setName('leep_admin:email_sending')
            ->setDescription('Email Sending')
        ;

        $this->receiverLogStatusList = Business\MailSender\Util::getEmailPoolReceiverStatusList();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $rate = $config->getMassMailerRate();

        // Find email in status = 'PENDING_FOR_SENDING'
        $query = $em->createQueryBuilder();
        $query->select('
                p.id, p.subject, p.content, p.idEmailPoolReceiver,
                epr.email, epr.name,
                ep.id AS idEmailPool
            ')
            ->from('AppDatabaseMainBundle:EmailPoolSpool', 'p')
            ->innerJoin('AppDatabaseMainBundle:EmailPool', 'ep', 'WITH', 'ep.id = p.idEmailPool AND ep.status = :epStatus')
            ->innerJoin('AppDatabaseMainBundle:EmailPoolReceiver', 'epr', 'WITH', 'epr.id = p.idEmailPoolReceiver AND epr.status IS NULL')
            ->setParameter('epStatus', Business\MailSender\Util::EMAIL_POOL_STATUS_PENDING_FOR_SENDING)
            ->orderBy('p.createdAt', 'ASC')
            ->setMaxResults($rate);

        $spooledEmails = $query->getQuery()->getResult();

        $arrEmailPools = array();
        if ($spooledEmails) {
            // Change receiver status to "Sending"
            $emailPoolReceivers = array(0);
            foreach ($spooledEmails as $spooledEmail) {
                $emailPoolReceivers[] = $spooledEmail['idEmailPoolReceiver'];

                // initial counter for sending statuses
                if (!array_key_exists($spooledEmail['idEmailPool'], $arrEmailPools)) {
                    $arrEmailPools[$spooledEmail['idEmailPool']] = array_fill_keys(array_keys($this->receiverLogStatusList), 0);
                }

                $arrEmailPools[$spooledEmail['idEmailPool']][Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_SENDING]++;
            }
            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:EmailPoolReceiver', 'p')
                ->andWhere('p.id in (:sendingPoolReceivers)')
                ->setParameter('sendingPoolReceivers', $emailPoolReceivers)
                ->set('p.status', Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_SENDING);
            $query->getQuery()->execute();

            // Send now
            foreach ($spooledEmails as $spooledEmail) {
                // get related attachment if exist
                $attachments = $doctrine->getRepository('AppDatabaseMainBundle:EmailPoolAttachment')->findByIdEmailPool($spooledEmail['idEmailPool']);

                // prepare email content
                $spooledEmail['htmlContent'] = self::prepareEmailContent($spooledEmail);

                $idMessage = null;
                if ($em->getRepository('AppDatabaseMainBundle:EmailOptOutList')->findOneByEmail($spooledEmail['email'])) {
                    $receiverStatus = Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_IGNORED;
                    $receiverMessage = $this->receiverLogStatusList[$receiverStatus];

                    $arrEmailPools[$spooledEmail['idEmailPool']][$receiverStatus]++;

                    // log - receiver info
                    $logReceiver = $spooledEmail['email'];
                    if ($spooledEmail['name']) {
                        $logReceiver = sprintf('%s <%s>', $spooledEmail['name'], $spooledEmail['email']);
                    }

                    $sendingLog = new Entity\EmailPoolLog();
                    $sendingLog->setIdEmailPool($spooledEmail['idEmailPool']);
                    $sendingLog->setStatus($receiverStatus);
                    $sendingLog->setMessage($receiverMessage);
                    $sendingLog->setReceiver($logReceiver);
                    $sendingLog->setCreatedAt(new \DateTime());
                    $em->persist($sendingLog);
                    $em->flush();
                }
                else {
                    // send mail
                    list($idMessage, $receiverStatus) = self::sendMail($spooledEmail, $attachments);
                    $arrEmailPools[$spooledEmail['idEmailPool']][$receiverStatus]++;
                }

                // remove spooledEmail
                $em->createQueryBuilder()->delete('AppDatabaseMainBundle:EmailPoolSpool', 'p')
                                        ->andWhere('p.id = :id')
                                        ->setParameter('id', $spooledEmail['id'])
                                        ->getQuery()
                                        ->execute();

                // change status of email_pool_receiver
                $receiver = $doctrine->getRepository('AppDatabaseMainBundle:EmailPoolReceiver')->findOneById($spooledEmail['idEmailPoolReceiver']);
                $receiver->setStatus($receiverStatus);
                $receiver->setIdMessage($idMessage);
                $em->persist($receiver);
                $em->flush();

                $arrEmailPools[$spooledEmail['idEmailPool']][Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_SENDING]--;
            }
        }

        // update Email Pool
        foreach ($arrEmailPools as $idEmailPool => $emailPoolData) {
            // get total pending
            $query = $doctrine->getEntityManager()->createQueryBuilder();
            $totalPending = $query->select('count(p.id)')
                ->from('AppDatabaseMainBundle:EmailPoolSpool', 'p')
                ->andWhere('p.idEmailPool = :idEmailPool')
                ->setParameter('idEmailPool', $idEmailPool)
                ->getQuery()
                ->getSingleScalarResult();

            $emailPool = $doctrine->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($idEmailPool);

            foreach ($emailPoolData as $statusKey => $statusCounter) {
                $statusTitle = $this->receiverLogStatusList[$statusKey];
                $setMethod = 'setNum' . $statusTitle;
                $getMethod = 'getNum' . $statusTitle;
                $emailPool->$setMethod($emailPool->$getMethod() + $statusCounter);
            }

            $emailPool->setNumPending($totalPending);

            if ($totalPending == 0) {
                $emailPool->setStatus(Business\MailSender\Util::EMAIL_POOL_STATUS_DONE);
            }

            $em->persist($emailPool);
            $em->flush();
        }
    }

    private function sendMail($spooledEmail, $attachments) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $config = $container->get('leep_admin.helper.common')->getConfig();

        // send
        $logMessage = null;
        $logStatus = null;

        // get parameters
        $host = $config->getMassMailerHost();
        $port = $config->getMassMailerPort();
        $encryption = $config->getMassMailerEncryption();
        $user = $config->getMassMailerUsername();
        $password = $config->getMassMailerPassword();
        $mandrillApiKey = $config->getMandrillApiKey();
        $senderName = $config->getMassMailerSenderName();
        $senderEmail = $config->getMassMailerSenderEmail();
        $subject = $spooledEmail['subject'];
        $content = $spooledEmail['content'];
        $htmlContent = $spooledEmail['htmlContent'];
        $receiverName = $spooledEmail['name'];
        $receiverEmail = $spooledEmail['email'];
        $metaData = array(
            'id_email_pool_receiver'    => $spooledEmail['idEmailPoolReceiver'],
        );

        $async = false;
        $ipPool = null;
        $sendAt = null;

        $idMessage = null;

        try {
            $mandrill = new \Mandrill($mandrillApiKey);
            $message = array(
                'html'                      => $htmlContent,
                'text'                      => null,
                'subject'                   => $subject,
                'from_email'                => $senderEmail,
                'from_name'                 => $senderName,
                'to'                        => array(
                                                    array(
                                                        'email' => $receiverEmail,
                                                        'name'  => $receiverName
                                                    )
                                                ),
                'important'                 => false,
                'track_opens'               => null,
                'track_clicks'              => null,
                'auto_text'                 => true,
                'auto_html'                 => null,
                'inline_css'                => null,
                'url_strip_qs'              => null,
                'preserve_recipients'       => null,
                'view_content_link'         => null,
                'bcc_address'               => null,
                'tracking_domain'           => null,
                'signing_domain'            => null,
                'return_path_domain'        => null,
                'merge'                     => false,
                'merge_language'            => 'mailchimp',
                'global_merge_vars'         => null,
                'merge_vars'                => null,
                'tags'                      => array(Business\MailSender\Constants::MANDRILL_MESSAGE_TAG),
                'subaccount'                => null,
                'google_analytics_domains'  => null,
                'google_analytics_campaign' => null,
                'metadata'                  => $metaData,
                'recipient_metadata'        => null,
                'attachments'               => null,
                'images'                    => null
            );

            $this->fulfillAttachments($message, $attachments);
            $result = $mandrill->messages->send($message, $async, $ipPool, $sendAt);

            $result = $result[0];
            $logStatus = Business\MailSender\Util::getReceiverLogStatus($result['status']);

            $logMessage = (array_key_exists('reject_reason', $result) ? $result['reject_reason'] : null);
            $logMessage = ($logMessage ? $logMessage : ($logStatus ? $this->receiverLogStatusList[$logStatus] : null));

            $idMessage = array_key_exists('_id', $result) ? $result['_id'] : $idMessage;
        } catch(\Exception $e) {
            $logStatus = Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_FAILED;
            $logMessage = $e->getMessage();
        }

        // create log
        Business\MailSender\Util::createEmailPoolLog($container, $spooledEmail['idEmailPool'], $logStatus, $receiverEmail, $logMessage, $receiverName);

        return array($idMessage, $logStatus);
    }

    private function prepareEmailContent($spooledEmail) {
        $container = $this->getContainer();
        $helperEncrypt = $container->get('leep_admin.helper.encrypt');
        $helperCommon = $container->get('leep_admin.helper.common');

        $onePixelUrl = $container->getParameter('application_url').'/mail_sender/feature/createOnePixelImage?';
        $optOutUrl = $container->getParameter('application_url').'/mail_sender/feature/optOut?';

        $arrParams = array(
            'code'   => $helperEncrypt->encrypt($spooledEmail['idEmailPoolReceiver'])
        );

        $onePixelUrl .= http_build_query($arrParams);
        $optOutUrl .= http_build_query($arrParams);

        $emailContent = $spooledEmail['content'] . '<img src="' . $onePixelUrl . '" />';

        // replace {{ OptOutLink }} tag if exists
        $emailContent = $helperCommon->replaceDoubleBracesTag('OptOutLink', $optOutUrl, $emailContent);

        return $emailContent;
    }

    private function fulfillAttachments(&$message, $attachments, $supportImageFilter = false) {
        $attachmentDir = $this->getContainer()->getParameter('attachment_dir') . '/'. Business\MailSender\Util::EMAIL_ATTACHMENT_FOLDER;
        $imageData = null;
        $attachmentData = null;

        if (sizeof($attachments) > 0) {
            $imageCounter = 1;

            foreach ($attachments as $attachment) {
                $filePath = $attachmentDir . '/' . $attachment->getName();

                // create base64_encoded image string
                $data = file_get_contents($filePath);
                $base64 = base64_encode($data);

                $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                $mimeType = finfo_file($finfo, $filePath);
                finfo_close($finfo);

                $data = array(
                            'type'      => $mimeType,
                            'content'   => $base64
                        );

                if ($supportImageFilter == true && in_array($mimeType, $this->mimeTypeImages)) {
                    if (!is_array($imageData)) {
                        $imageData = array();
                    }

                    $data['name'] = 'IMAGECID_' . $imageCounter;

                    $imageData[] = $data;

                    $imageCounter++;
                } else {
                    if (!is_array($attachmentData)) {
                        $attachmentData = array();
                    }

                    $data['name'] = $attachment->getOriginalName();

                    $attachmentData[] = $data;
                }
            }
        }

        $message['images'] = $imageData;
        $message['attachments'] = $attachmentData;
    }
}
