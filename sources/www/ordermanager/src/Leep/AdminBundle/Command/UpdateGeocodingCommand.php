<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateGeocodingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:update_geocoding')
            ->setDescription('Update geocoding');
    }
        
    private function lookupGeocoding($address){
        $address = str_replace (" ", "+", urlencode($address));
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false";
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
        if ($response['status'] != 'OK') {
            return array(
                'result' => $response['status']
            );
        }

        $geometry = $response['results'][0]['geometry']; 
        return array(
            'result' => $response['status'],
            'pos'    => array(
                'long' => $geometry['location']['lng'],
                'lat'  => $geometry['location']['lat']
            )
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');

        // Update dc location
        $times = 5;
        for ($t = 0; $t < $times; $t++) {
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
                ->andWhere('p.isRecheckGeocoding = 1')
                ->andWhere('(p.isdeleted = 0) OR (p.isdeleted IS NULL)')
                ->setMaxResults(8)
                ->setFirstResult(0);        
            $results = $query->getQuery()->getResult();

            $format = '%s, %s %s, %s';
            foreach ($results as $dc) {                
                $address = sprintf($format,
                    $dc->getStreet(),
                    $dc->getCity(),
                    $dc->getPostCode(),
                    $formatter->format($dc->getCountryId(), 'mapping', 'LeepAdmin_Country_List')
                );
                $result = $this->lookupGeocoding($address);
                if ($result['result'] != 'OK') {                    
                    $dc->setGeocodingStatusCode($result['result']);
                    $dc->setIsRecheckGeocoding(2);
                }                
                else {
                    $dc->setGeocodingStatusCode($result['result']);                    
                    $dc->setIsRecheckGeocoding(0);

                    $dc->setLatitude($result['pos']['lat']);
                    $dc->setLongitude($result['pos']['long']);
                }
            }
            $em->flush();

            sleep(10);
        }

        // Update customers
        $times = 5;
        for ($t = 0; $t < $times; $t++) {
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:Customer', 'p')
                ->andWhere('p.isRecheckGeocoding = 1')
                ->andWhere('(p.isdeleted = 0) OR (p.isdeleted IS NULL)')
                ->orderBy('p.id', 'DESC')
                ->setMaxResults(8)
                ->setFirstResult(0);        
            $results = $query->getQuery()->getResult();

            $format = '%s, %s %s, %s';
            foreach ($results as $customer) {                
                $address = sprintf($format,
                    $customer->getStreet(),
                    $customer->getCity(),
                    $customer->getPostCode(),
                    $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List')
                );
                $result = $this->lookupGeocoding($address);
                if ($result['result'] != 'OK') {                    
                    $customer->setGeocodingStatusCode($result['result']);
                    $customer->setIsRecheckGeocoding(2);
                }                
                else {
                    $customer->setGeocodingStatusCode($result['result']);                    
                    $customer->setIsRecheckGeocoding(0);

                    $customer->setLatitude($result['pos']['lat']);
                    $customer->setLongitude($result['pos']['long']);
                }
            }
            $em->flush();

            sleep(10);
        }

    }
}
