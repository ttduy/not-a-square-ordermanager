<?php
    namespace Leep\AdminBundle\Command;

    use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Output\OutputInterface;
    use App\Database\MainBundle\Entity;
    use Leep\AdminBundle\Business;
    use Leep\AdminBundle\Business\EmailOutgoing\Util;
    use Leep\AdminBundle\Business\EmailOutgoing\Constant;

    class ExportRecipientsCommand extends ContainerAwareCommand{
        const TYPE_EMAIL = "Christmas special 2016";
        const STATUS_SEND = "Sent";
        const START_INDEX = 0;
        const ONE_UNIT = 1;
        const NUMBER_COLUMN = 4;
        const STD_WIDTH = 50;
        const STD_NUM_COL = 4;
        const FIRST_ROW = 2;
        const FIRST_COLUMN = 'B';
        const STD_CREATED_DATE = '2016-11-24';

        protected function configure() {
            $this->setName('leep_admin:export_recipients')
                    ->setDescription('Export recipients receive email with specific subject');
        }

        protected function execute(InputInterface $input, OutputInterface $output){
            $em = $this->getContainer()->get('doctrine')->getEntityManager();
            $config = $this->getContainer()->get('leep_admin.helper.common')->getConfig();

            // ====================
            // get all recipients who recieved emails outgoing having condition:
            /// sent, have words requested in subject. This data will be used to export to csv file
            $recipients = [];

            $statuses = Business\EmailOutgoing\Constant::getStatus();

            /// query to get some information of all recipients
            $query = $em->createQueryBuilder();
            $query->select('q.recipientsId, q.recipients, q.creationDatetime,
                            q.lastUpdated, q.title, q.recipientsId, q.recipientsType, q.status')
                    ->from('AppDatabaseMainBundle:EmailOutgoing','q')
                    ->where($query->expr()->andX(
                        $query->expr()->like('q.title', ':keyTitle')
                    ))
                    ->setParameter('keyTitle', self::TYPE_EMAIL);
            $recipientsInfo = $query->getQuery()->getResult();

            /// define structure save email information
            $recipients = ['customer' => $this->initCustomerArray(),
                            'distributionChannel' => $this->initDcArray(),
                            'other' => $this->initOtherArray()];

            $types = Business\EmailMassSender\Constant::getReceiverTypeList();
            $caseCustomer = array_search('Customer', $types);
            $caseDc = array_search('Distribution Channel', $types);
            print($caseCustomer." ".$caseDc."\n");
            foreach ($recipientsInfo as $recipient) {
                if (!$this->isCorrecDate($recipient['creationDatetime'])) {
                    continue;
                }
                switch ($recipient['recipientsType']) {
                    case $caseCustomer:
                        $recipients['customer']['content'][] = $this->getCustomer($em, $recipient);
                        break;
                    case $caseDc:
                        $recipients['distributionChannel']['content'][] = $this->getDistributionChannel($em, $recipient);
                        break;
                    default:
                        $recipients['other']['content'][] = $this->getOther($em, $recipient);
                        break;
                }
            }

            $objPHPExcel = new \PHPExcel();
            $index = self::START_INDEX;
            foreach ($recipients as $typeRecipient => $recipient) {
                // init
                $sheet = $objPHPExcel->createSheet();
                $sheet->setTitle($typeRecipient);
                $numberColumns = count($recipient['header']);
                $column = self::FIRST_COLUMN;
                foreach ($recipient['header'] as $head) {
                    $sheet->getColumnDimension($column)->setWidth(self::STD_WIDTH);
                    $column++;
                }

                // print headers
                $column = self::FIRST_COLUMN;
                $row = self::FIRST_ROW;
                foreach ($recipient['header'] as $head) {
                    $sheet->setCellValue($column.(strval($row)), $head);
                    $column++;
                }
                $sheet->getStyle(self::FIRST_COLUMN.(strval($row)).':'.chr(ord($column)-self::ONE_UNIT).($row))
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $sheet->getStyle(self::FIRST_COLUMN.(strval($row)).':'.chr(ord($column)-self::ONE_UNIT).($row))
                    ->applyFromArray(array(
                        'alignment' => array(
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '333333')
                        ),
                        'font' => array(
                            'bold' => true,
                            'color' => ['rgb' => 'ffffff']
                        )
                    ));

                // print content
                $row = self::FIRST_ROW + self::ONE_UNIT;
                foreach ($recipient['content'] as $currentRow) {
                    $column = self::FIRST_COLUMN;
                    foreach ($currentRow as $key => $value) {
                        $sheet->setCellValue($column.(strval($row)), $value);
                        $column++;
                    }
                    $row++;
                }

                $sheet->getStyle(self::FIRST_COLUMN.(strval(self::FIRST_ROW + self::ONE_UNIT)).':'.(chr(ord(self::FIRST_COLUMN) + self::STD_NUM_COL - 1)).($row)."\n")
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $sheet->getStyle(self::FIRST_COLUMN.(strval(self::FIRST_ROW + self::ONE_UNIT)).':'.(chr(ord(self::FIRST_COLUMN) + self::STD_NUM_COL - 1)).($row)."\n")
                    ->applyFromArray([
                        'alignment' => array(
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    ]);
                $index++;
            }

            /// Print
            $today = new \DateTime();
            $tmpFolder = $this->getContainer()->get('service_container')->getParameter('temp_dir');
            $tmpNameFile = str_replace(' ', '', ucwords(self::TYPE_EMAIL));
            $tmpFile = 'recipients_'.$tmpNameFile.'_'.$today->format('YmdHis').'.xlsx';

            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $objWriter->save($tmpFolder.'/'.$tmpFile);
        }

        private function initCustomerArray()
        {
            $result = ['header' =>['Order Number', 'Name', 'Email', 'Telephone', 'Distribution Channel', 'Created Date', "Last Updated", 'Status'],
                    'content' => []];
            return $result;
        }

        private function getCustomer($em, $recipient)
        {
            $index = strpos($recipient['recipients'], '<');
            //// get name
            $recipientName = substr($recipient['recipients'], self::START_INDEX, $index); // index start from 0, so length is index + 1
            //// get email
            $recipientEmail = substr($recipient['recipients'], $index, strlen($recipient['recipients']) - $index);
            /// get create, last update time
            $createDate = '';
            if (isset($recipient['creationDatetime'])) {
                $createDate = date_format($recipient['creationDatetime'], 'Y/m/d  H:i:s');
            }
            $lastUpdated = '';
            if (isset($recipient['lastUpdated'])) {
                $lastUpdated = date_format($recipient['lastUpdated'], 'Y/m/d  H:i:s');
            }
            $id = $recipient['recipientsId'];
            if (isset($id)) {
                $customer = $em->getRepository("AppDatabaseMainBundle:Customer")->findOneById($id);
                if($customer){
                    $dcId = $customer->getDistributionchannelid();
                    $dc = $em->getRepository("AppDatabaseMainBundle:DistributionChannels")->findOneById($dcId);
                    $dcName = '';
                    if ($dc) {
                        $dcName = $dc->getDistributionchannel();
                    }
                    $statuses = Business\EmailOutgoing\Constant::getStatus();
                    $result = [$customer->getOrdernumber(), $recipientName, $recipientEmail, trim($customer->getTelephone()), $dcName, $createDate, $lastUpdated, $statuses[$recipient['status']]];
                    return $result;
                }
            }
            return ['', $recipientName, $recipientEmail, '', '', $createDate, $lastUpdated];
        }

        private function initDcArray()
        {
            $result = ['header' => ['Distribution Channel', 'Name', 'Email', 'Telephone', 'Partner', 'Created Date', 'Last Updated', 'Status'],
                    'content' => []];
            return $result;
        }

        private function getDistributionChannel($em, $recipient)
        {
            $index = strpos($recipient['recipients'], '<');
            $doctrine = $this->getContainer()->get('doctrine');
            //// get name
            $recipientName = substr($recipient['recipients'], self::START_INDEX, $index); // index start from 0, so length is index + 1
            //// get email
            $recipientEmail = substr($recipient['recipients'], $index, strlen($recipient['recipients']) - $index);
            /// get create, last update time
            $createDate = '';
            if (isset($recipient['creationDatetime'])) {
                $createDate = date_format($recipient['creationDatetime'], 'Y/m/d  H:i:s');
            }
            $lastUpdated = '';
            if (isset($recipient['lastUpdated'])) {
                $lastUpdated = date_format($recipient['lastUpdated'], 'Y/m/d  H:i:s');
            }
            $id = $recipient['recipientsId'];
            if (isset($id)) {
                $dc = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($id);
                $dcName = $dc->getDistributionchannel();
                $idPartner = $dc->getPartnerid();
                $partner = $em->getRepository("AppDatabaseMainBundle:Partner")->findOneById($idPartner);
                $partnerName = "";
                if ($partner) {
                    $partnerName = $partner->getPartner();
                }
                $telephone = $dc->getTelephone();
                $statuses = Business\EmailOutgoing\Constant::getStatus();

                $result = [$dcName, $recipientName, $recipientEmail, $telephone, $partnerName, $createDate, $lastUpdated, $statuses[$recipient['status']]];;
                return $result;
            }
            $result = ['0', $recipientName, $recipientEmail, '0', '0', $createDate, $lastUpdated];
            return $result;
        }

        private function initOtherArray()
        {
            $result = ['header' => ['Name', 'Email', 'Created date', 'Last Updated', 'Status'],
                    'content' => []];
            return $result;
        }

        private function getOther($em, $recipient)
        {
            $index = strpos($recipient['recipients'], '<');
            //// get name
            $recipientName = substr($recipient['recipients'], self::START_INDEX, $index); // index start from 0, so length is index + 1
            //// get email
            $recipientEmail = substr($recipient['recipients'], $index, strlen($recipient['recipients']) - $index);
            /// get create, lst update time
            $createDate = '';
            if (isset($recipient['creationDatetime'])) {
                $createDate = date_format($recipient['creationDatetime'], 'Y/m/d  H:i:s');
            }
            $lastUpdated = '';
            if (isset($recipient['lastUpdated'])) {
                $lastUpdated = date_format($recipient['lastUpdated'], 'Y/m/d  H:i:s');
            }
            $statuses = Business\EmailOutgoing\Constant::getStatus();
            return [$recipientName, $recipientEmail, $createDate, $lastUpdated, $statuses[$recipient['status']]];
        }

        private function isCorrecDate($dateTime)
        {
            $date_fix = $dateTime->format('Y-m-d');
            if ($date_fix == self::STD_CREATED_DATE) {
                return true;
            }
            return false;
        }
    }
?>