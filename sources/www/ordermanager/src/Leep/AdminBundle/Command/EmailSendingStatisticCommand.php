<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:email_sending_statistic
*/
class EmailSendingStatisticCommand extends ContainerAwareCommand
{
    protected $startDate = null;
    protected $endDate =  null;

    protected function configure()
    {
        $this
            ->setName('leep_admin:email_sending_statistic')
            ->setDescription('Email Sending Statistic')
            ->addArgument(
                'startDate',
                InputArgument::OPTIONAL,
                'start date in Y-m-d format, first day of current month as default'
            )
            ->addArgument(
                'endDate',
                InputArgument::OPTIONAL,
                'end date in Y-m-d format, last day of current month as default'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();
        $config = $container->get('leep_admin.helper.common')->getConfig();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'Leep\AdminBundle\Business\Customer\DoctrineExt\Date');            

        $this->startDate = ($input->getArgument('startDate') ? $input->getArgument('startDate') : date('Y-m-01'));
        $this->endDate = ($input->getArgument('endDate') ? $input->getArgument('endDate') : date('Y-m-t'));

        $mandrillApiKey = $config->getMandrillApiKey();

        if ($mandrillApiKey) {
            $mandrill = new \Mandrill($mandrillApiKey);

            $query = '*';
            $tags = array(Business\MailSender\Constants::MANDRILL_MESSAGE_TAG);
            $senders = null;

            $result = $mandrill->messages->searchTimeSeries($query, $this->startDate, $this->endDate, $tags, $senders);

            $arrDelDate = array();
            $counter = 0;
            if (!empty($result)) {
                foreach ($result as $entry) {
                    list($date, $time) = explode(' ', $entry['time']);
                    if (!in_array($date, $arrDelDate)) {
                        $delQuery = $em->createQueryBuilder();
                        $delQuery->delete('AppDatabaseMainBundle:EmailPoolSendingStatistic', 'p')
                                ->andWhere('DATE(p.time) = :delDate')
                                ->setParameter('delDate', $date)
                                ->getQuery()
                                ->execute();

                        $arrDelDate[] = $date;
                    }

                    $newRecord = new Entity\EmailPoolSendingStatistic();
                    $newRecord->setTime(new \DateTime($entry['time']));
                    $newRecord->setSent($entry['sent']);
                    $newRecord->setOpens($entry['opens']);
                    $newRecord->setClicks($entry['clicks']);
                    $newRecord->setHardBounces($entry['hard_bounces']);
                    $newRecord->setSoftBounces($entry['soft_bounces']);
                    $newRecord->setRejects($entry['rejects']);
                    $newRecord->setComplaints($entry['complaints']);
                    $newRecord->setUnsubs($entry['unsubs']);
                    $newRecord->setUniqueOpens($entry['unique_opens']);
                    $newRecord->setUniqueClicks($entry['unique_clicks']);
                    $em->persist($newRecord);

                    $counter++;

                    if ($counter % 500) {
                        $em->flush();
                        $counter = 0;
                    }
                }
                $em->flush();
            }
        }
    }
}
