<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: sourceId = '[file path]' | group = group name or group id | separator = ';' as default
php app/console leep_admin:importFoodData /tmp/food.csv newGroup
php app/console leep_admin:importFoodData /tmp/food.csv 10
*/
class ImportFoodDataCommand extends ContainerAwareCommand
{
    const ID_LANGUAGE_GERMAN = 1;
    const ID_LANGUAGE_GERMAN_SWISS = 40;

    protected function configure()
    {
        $this
            ->setName('leep_admin:importFoodData')
            ->setDescription('Import Food Data')
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                'Where is the csv file'
            )
            ->addArgument(
                'group',
                InputArgument::REQUIRED,
                'what is textblock group name or group id'
            )
            ->addArgument(
                'separator',
                InputArgument::OPTIONAL,
                'what is separator, comma as default'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // get input sourceFile
        $sourceFile = $input->getArgument('sourceFile');

        // get input group
        $group = $input->getArgument('group');
        $idGroup = is_numeric($group) ? $group : null;

        // get input separator
        $separator = ($input->getArgument('separator') ? $input->getArgument('separator') : ';');

        // get file content
        $parseResult = self::parseData(file_get_contents($sourceFile), $separator);
        $data = $parseResult[0];
        $arrDuplicatedTextBlockInCSV = $parseResult[1];

        $arrDuplicatedTextBlockInDB = array();
        $newTextBlocks = array();
        $notExistedFoodIds = array();
        
        $languages = array();
        $result = $em->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($result as $r) {
            $languages[$r->getId()] = '';
        }

        $total = count($data);
        $i = 0;
        foreach ($data as $textBlock => $entry) {
            // check if $textBlock is already registered
            $text = $doctrine->getRepository('AppDatabaseMainBundle:TextBlock')->findOneByName($textBlock);

            if (!$text) {
                // create Group if not defined
                if (!$idGroup) {
                    $newGroup = new Entity\TextBlockGroup();
                    $newGroup->setName($group);
                    $em->persist($newGroup);
                    $em->flush();

                    $idGroup = $newGroup->getId();
                }


                // Create text block
                $newTextBlock = new Entity\TextBlock();
                $newTextBlock->setName($textBlock);
                $newTextBlock->setIdGroup($idGroup);
                $em->persist($newTextBlock);
                $em->flush();
                
                $languages[self::ID_LANGUAGE_GERMAN] = $entry['german'];
                $languages[self::ID_LANGUAGE_GERMAN_SWISS] = $entry['swiss'];
                foreach ($languages as $idLanguage => $body) {
                    $tbLang = new Entity\TextBlockLanguage();
                    $tbLang->setIdLanguage($idLanguage);
                    $tbLang->setIdTextBlock($newTextBlock->getId());
                    $tbLang->setBody($body);
                    $em->persist($tbLang);
                }
                $em->flush();                

                // update to related Food Item ID
                $foodItem = $em->getRepository('AppDatabaseMainBundle:FoodTableFoodItem')->findOneByFoodId($entry['foodItemId']);
                if ($foodItem) {
                    $foodItem->setName('[['.$textBlock.']]');
                    $em->persist($foodItem);
                    $em->flush();                    
                } else {
                    $notExistedFoodIds[] = $entry['foodItemId'];
                }

                $newTextBlocks[$textBlock] = $newTextBlock->getId();
            } else {
                $arrDuplicatedTextBlockInDB[] = $textBlock;
            }                

            $i++;
            echo "Done $i/$total\n";
        }

        // summary
        //echo "Duplicated Text Block " . implode(" | ", $arrDuplicatedTextBlockInDB) . "\n";
        echo "Duplicated Text Block in CSV: " . sizeof($arrDuplicatedTextBlockInCSV) . implode(" | ", $arrDuplicatedTextBlockInCSV) . "\n";
        echo "Duplicated Text Block in DB: " . sizeof($arrDuplicatedTextBlockInDB) . implode(" | ", $arrDuplicatedTextBlockInDB) . "\n";
        echo "Not Existed Food Id: " . sizeof($notExistedFoodIds) . "\n" . implode(" | ", $notExistedFoodIds) . "\n";
        echo "New Group ID: " . $idGroup . "\n";
        echo "New Text Block added: " . sizeof($newTextBlocks) . "\n";
    }

    protected function parseData($inputData, $separator) {
        $data = array();
        $rows = explode("\n", $inputData);
        $arrRedundantTextBlocks = array();

        foreach ($rows as $row) {
            $arr = explode($separator, $row);

            $textBlock = trim(self::cleanDoubleQuotes($arr[0]));
            $textBlock = substr($textBlock, 2, -2);

            $german = trim(self::cleanDoubleQuotes($arr[1]));
            $swiss = trim(self::cleanDoubleQuotes($arr[2]));
            $foodItemId = trim(self::cleanDoubleQuotes($arr[3]));


            if (array_key_exists($textBlock, $data)) {
                $arrRedundantTextBlocks[] = $textBlock;
            }

            // textBlock,German,Swiss,foodItemId
            $data[$textBlock] = array(
                                            'german'        =>  $german,
                                            'swiss'         =>  $swiss,
                                            'foodItemId'    =>  $foodItemId
                                        );



        }
        
        return array($data, $arrRedundantTextBlocks);
    }

    protected function cleanDoubleQuotes($str) {
        if (substr($str, 0, 1) == '"') {
            $str = substr($str, 1);
        }

        if (substr($str, -1, 1) == '"') {
            $str = substr($str, -1);
        }

        return $str;
    }
}
