<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EmailUpdateStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:email_update_stats')
            ->setDescription('Email Update Stats')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p.idEmailPool, COUNT(p) AS total')
            ->from('AppDatabaseMainBundle:EmailPoolReceiver', 'p')
            ->innerJoin('AppDatabaseMainBundle:EmailPoolReceiverView', 'v', 'WITH', 'p.id = v.idEmailPoolReceiver')
            ->groupBy('p.idEmailPool');
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            $idEmailPool = intval($r['idEmailPool']);
            $totalView = intval($r['total']);

            $p = $em->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($idEmailPool);
            if ($p) {
                $p->setNumView($totalView);
            }
        }
        $em->flush();
    }
}