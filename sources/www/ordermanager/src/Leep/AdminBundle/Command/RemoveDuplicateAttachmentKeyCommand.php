<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;

class RemoveDuplicateAttachmentKeyCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:remove_duplicate_attachment_key')
            ->setDescription('Remove materials duplicate attachment key (Key one only)');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $materials = $em->getRepository('AppDatabaseMainBundle:Materials')->findAll();
        $duplicateList = array();
        foreach ($materials as $material) {
            $attachmentKey = $material->getAttachmentKey();
            if (empty($attachmentKey))
                continue;

            if (array_key_exists($attachmentKey, $duplicateList))
                continue;

            $duplicates = $em->getRepository('AppDatabaseMainBundle:Materials')->findByAttachmentKey($attachmentKey);
            if (count($duplicates) > 1) {
                $duplicateList[$attachmentKey] = $duplicates;
            }
        }

        foreach ($duplicateList as $key => $duplicates) {
            echo "Duplicate at: $key (".count($duplicates).")\n";

            foreach ($duplicates as $index => $dup) {
                if ($index == 0) {
                    echo "Retain #".$dup->getId()."\n";
                } else {
                    echo "Clear #".$dup->getId()."\n";
                    $dup->setAttachmentKey(null);
                    $em->persist($dup);
                }
            }

            $em->flush();
        }
    }
}

