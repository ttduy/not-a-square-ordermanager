<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateIdCodeDCAndPartnerCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:update_id_code_dc_partner')
            ->setDescription('Update Id Code DC And Partner');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        print_r("start update Partner \n");
        $partners = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findAll();
        foreach ($partners as $key => $partner) {
            $partner->setIdCode(strtoupper($partner->getIdCode()));
            $em->persist($partner);
            print_r("=======================\n set code: ". $partner->getIdCode()."\n");
        }
        print_r("end update Partner \n");

        print_r("start update DC \n");
        $dcs = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findAll();
        foreach ($dcs as $key => $dc) {
            $dc->setIdCode(strtoupper($dc->getIdCode()));
            $em->persist($dc);
            print_r("=======================\n set code: ". $dc->getIdCode()."\n");
        }
        print_r("end update DC \n");


        $em->flush();
    }
}
