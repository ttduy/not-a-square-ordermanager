<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class BuildBodPackageCommand extends ContainerAwareCommand
{
    protected function configure() {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:build_bod_package')
            ->setDescription('Build BOD Package');
    }

    public $cleanupFiles = array();

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BodShipment', 'p')
            ->andWhere('(p.status IS NULL) OR (p.status = 0)')
            ->setMaxResults(1);
        $bodShipments = $query->getQuery()->getResult();
        $shipmentInfoDir = $container->getParameter('kernel.root_dir').'/../scripts/pdfshipment/files/shipmentinfo';
        $shipmentOutputDir = $container->getParameter('kernel.root_dir').'/../scripts/pdfshipment/files/output';
        $pdfDir = $container->getParameter('kernel.root_dir').'/../web/report';
        foreach ($bodShipments as $bodShipment) {
            $bodShipment->setStatus(1);
            $em->flush();

            $fromCompanyNumber = $bodShipment->getCustomBodAccountNumber();
            $ext = $bodShipment->getCustomBodBarcodeNumber();
            if (!empty($fromCompanyNumber)) {
                $fromCompanyNumber = "1$fromCompanyNumber";
            } else {
                $fromCompanyNumber = "110273274";
                $ext = "11";
            }


            $properties = array();
            $mainBookId = '';

            // Load books
            $books = $em->getRepository('AppDatabaseMainBundle:BodShipmentBook')->findByIdBodShipment($bodShipment->getId());
            $i = 0;
            foreach ($books as $bodShipmentBook) {
                $i++;

                $pdfFile = $pdfDir.'/'.$bodShipmentBook->getReportFile();
                if (empty($mainBookId)) {
                    $mainBookId = $bodShipmentBook->getId();
                }

                // Book ID
                $properties['bookId'.$i] = $bodShipmentBook->getId();

                // New cover with barcode
                $coverPdfFile = $shipmentInfoDir.'/cover_'.$bodShipmentBook->getId().'.pdf';
                $this->generateCoverPdf($pdfFile, $coverPdfFile.'.tmp', $bodShipmentBook, $ext);
                $this->convertPDFToCMYK($coverPdfFile.'.tmp', $coverPdfFile);
                echo "Build cover for book ".$bodShipmentBook->getId().': DONE'."\n";
                $properties['cover'.$i] = $coverPdfFile;
                $this->cleanupFiles[] = $coverPdfFile.'.tmp';
                $this->cleanupFiles[] = $coverPdfFile;

                // Main PDF
                $convertPdfFile = $shipmentInfoDir.'/'.$bodShipmentBook->getReportFile();
                $this->convertPDFToCMYK($pdfFile, $convertPdfFile);
                echo "Build cover for main book ".$bodShipmentBook->getId().': DONE'."\n";
                $this->cleanupFiles[] = $convertPdfFile;
                $properties['book'.$i] = $convertPdfFile;
            }

            $properties['numBooks'] = $i;

            // Delivery notes
            $deliveryNoteFile = $shipmentInfoDir.'/delivery_note_'.$bodShipment->getId().'.pdf';
            $this->generateDeliveryNotePdf($deliveryNoteFile.'.tmp', $bodShipment);
            $this->convertPDFToCMYK($deliveryNoteFile.'.tmp', $deliveryNoteFile);
            echo "Build delivery note : DONE"."\n";
            $properties['deliveryNote'] = $deliveryNoteFile;
            $this->cleanupFiles[] = $deliveryNoteFile.'.tmp';
            $this->cleanupFiles[] = $deliveryNoteFile;

            // Build properties job
            $properties['customerOrderNumber'] = $bodShipment->getBodCustomerOrderNumber();
            $properties['fromCompany'] = $bodShipment->getBodFromCompany();

            $properties['fromCompanyNumber'] = $fromCompanyNumber;

            $properties['fromPerson'] = $bodShipment->getBodFromPerson();
            $properties['fromEmail'] = $bodShipment->getBodFromEmail();
            $properties['itemTitle'] = $bodShipment->getBodItemTitle();
            $properties['contributorRole'] = $bodShipment->getBodContributorRole();
            $properties['contributorName'] = $bodShipment->getBodContributorName();
            $properties['itemPaper'] = $bodShipment->getBodItemPaper();
            $properties['itemBinding'] = $bodShipment->getBodItemBinding();
            $properties['itemBack'] = $bodShipment->getBodItemBack();
            $properties['itemFinish'] = $bodShipment->getBodItemFinish();
            $properties['itemJacket'] = $bodShipment->getBodItemJacket();

            $properties['orderNumber'] = $bodShipment->getBodOrderNumber();
            $properties['orderCopies'] = $bodShipment->getBodOrderCopies();
            $properties['orderAddressLine1'] = $bodShipment->getBodOrderAddressLine1();
            $properties['orderAddressLine2'] = $bodShipment->getBodOrderAddressLine2();
            $properties['orderAddressLine3'] = $bodShipment->getBodOrderAddressLine3();
            $properties['orderStreet'] = $bodShipment->getBodOrderStreet();
            $properties['orderZip'] = $bodShipment->getBodOrderZip();
            $properties['orderCity'] = $bodShipment->getBodOrderCity();
            $properties['orderCountry'] = $bodShipment->getBodOrderCountry();

            $properties['orderCustomsValue'] = $bodShipment->getBodOrderCustomsValue();
            $properties['orderShipping'] = $bodShipment->getBodOrderShipping();

            $content = $this->getPropertiesContent($properties);
            file_put_contents($shipmentInfoDir.'/'.$bodShipment->getId().'.properties', $content);

            // Execute command
            $connector = $container->getParameter('cmdConnectorString');
            $cmd = 'cd '.$container->getParameter('kernel.root_dir').'/../scripts/pdfshipment '.$connector.' java -jar build/jar/pdfshipment.jar '.$bodShipment->getId().' '.$ext;
            $output = array();
            $returnVar = false;
            exec($cmd, $output, $returnVar);

            // Check error
            $content = @file_get_contents($shipmentOutputDir.'/'.$bodShipment->getId().'.txt');
            if (trim($content) != '') {
                $bodShipment->setPackageFile($bodShipment->getId().'.txt');
            } else {
                $filename = sprintf("%s_%s.zip",
                    $bodShipment->getBodCustomerOrderNumber(),
                    $this->formatBookId($mainBookId)
                );
                $bodShipment->setPackageFile($filename);
            }
            $em->flush();

            // Clean up
            $this->cleanUp();
        }
    }

    protected function getPropertiesContent($properties) {
        $rows = array();
        foreach ($properties as $k => $v) {
            $rows[] = $k.' = '.$v;
        }
        return implode("\n", $rows);
    }
    protected function formatBookId($bookId) {
        while (strlen($bookId) < 8) {
            $bookId = '0'.$bookId;
        }
        return $bookId;
    }

    protected function convertPDFToCMYK($pdfFile, $convertPdfFile) {
        // Execute command
        $container = $this->getContainer();
        $convertCmykDir = $container->getParameter('kernel.root_dir').'/../scripts/convertcmyk';
        $output = array();
        $returnVar = false;

        $psDefFile = $container->getParameter('psDefFile');
        $sRGBICCProfile = $container->getParameter('iccFileRGB');
        $cmykICCProfile = $container->getParameter('iccFileCMYK');

        $convertPdfFileTmpPs = $convertPdfFile.'.ps';

        $cmd = 'pdftops '.$pdfFile.' '.$convertPdfFileTmpPs;
        exec($cmd, $output, $returnVar);

        $cmd = 'gs -dBATCH -dNOPAUSE -dNOCACHE -dPDFA -dOverrideICC=true -dPDFSETTINGS=/prepress '.
               ' -sDEVICE=pdfwrite '.
               ' -sDefaultRGBProfile="'.$sRGBICCProfile.'" '.
               ' -sOutputICCProfile="'.$cmykICCProfile.'" '.
               ' -dColorConversionStrategy=/CMYK -dProcessColorModel=/DeviceCMYK '.
               ' -sOutputFile='.$convertPdfFile.' '.$psDefFile.' '.$convertPdfFileTmpPs;
        exec($cmd, $output, $returnVar);

        $this->cleanupFiles[] = $convertPdfFileTmpPs;
    }

    protected function cleanUp() {
        foreach ($this->cleanupFiles as $file) {
            @unlink($file);
        }
    }

    protected function parseReportData($reportData) {
        $data = array();
        $rows = explode("\n", $reportData);
        foreach ($rows as $row) {
            $arr = explode('=', $row);
            if (isset($arr[1])) {
                $data[trim($arr[0])] = trim($arr[1]);
            }
        }
        return $data;
    }

    protected function encodeReportData($arr) {
        $results = array();
        foreach ($arr as $k => $v) {
            $results[] = $k.' = '.$v;
        }
        return implode("\n", $results);
    }
    protected function generateDeliveryNotePdf($deliveryNoteFile, $bodShipment) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        if ($bodShipment->getBodIdDeliveryNoteReport() != 0) {
            $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($bodShipment->getIdCustomer());

            $report = $em->getRepository('AppDatabaseMainBundle:Report')->findOneById($bodShipment->getBodIdDeliveryNoteReport());

            // Compute formula template
            $orderData = Business\CustomerReport\Utils::loadOrderData($container, $order->getId());
            $orderDataArr = $this->parseReportData($orderData);
            $formulaTemplate = $em->getRepository('AppDatabaseMainBundle:FormulaTemplate')->findOneById($report->getIdFormulaTemplate());
            $finalData = array();
            if ($formulaTemplate) {
                $finalData = $orderDataArr;

                $executor = $container->get('leep_admin.formula_template.business.executor');
                $executor->execute($formulaTemplate->getFormula(), $orderDataArr);
                $errors = $executor->errors;
                $output = $executor->output;

                if (!empty($errors)) {
                    $errorMessage = '';
                    $errorMessage .= "\n".'COMPILE ERROR -----------'."\n";
                    $errorMessage .= implode("\n", $errors);
                    $errorMessage .= "\n".'------------------------'."\n";
                }
                else {
                    foreach ($output as $k => $v) {
                        $finalData[$k] = $v;
                    }
                    /*$rows = explode("\n", $report->getTestData());
                    foreach ($rows as $row) {
                        $arr = explode('=', $row);
                        if (isset($arr[1])) {
                            if (!isset($finalData[trim($arr[0])])) {
                                $finalData[trim($arr[0])] = trim($arr[1]);
                            }
                        }
                    }*/

                    $isSuccess = true;
                }
            }
            else {
                $errorMessage = "No formula template selected";
            }

            $jobCR = new Entity\CustomerReport();
            $jobCR->setIdReport($bodShipment->getBodIdDeliveryNoteReport());
            if ($bodShipment->getBodDeliveryNoteIdLanguage() != 0) {
                $jobCR->setIdLanguage($bodShipment->getBodDeliveryNoteIdLanguage());
            }
            else {
                $jobCR->setIdLanguage($order->getLanguageId());
            }
            $jobCR->setReportFinalData($this->encodeReportData($finalData));

            $pdf = Business\CustomerReport\Utils::generateReportPdf($container, $jobCR, false);
            $pdf->Output($deliveryNoteFile, 'F');
        }
    }

    protected function generateCoverPdf($pdfFile, $coverPdf, $bodShipmentBook, $ext = '11') {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Search the final report section
        $reportSection = null;
        $customerReport = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($bodShipmentBook->getIdReport());
        $isOverride = false;
        if ($customerReport) {
            // Get current report num pages
            $pdf = $container->get('white_october.tcpdf')->create();
            $numPage = $pdf->setSourceFile($pdfFile);

            // Build report
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportSection', 'p')
                ->andWhere('p.idReport = :idReport')
                ->orderBy('p.sortOrder', 'DESC')
                ->setMaxResults(1)
                ->setParameter('idReport', $customerReport->getIdReport());
            $reportSections = $query->getQuery()->getResult();
            if ($reportSections) {
                foreach ($reportSections as $rp) {
                    $reportSection = $rp;
                    break;
                }
            }

            // Check report sections
            if ($reportSection->getIdType() == 2) {
                $idReportSection = $reportSection->getIdReportSectionLink();
            }
            else {
                $idReportSection = $reportSection->getId();
            }

            // Build the last 2 pages
            $builder = $container->get('leep_admin.report.business.builder');
            $builder->init();
            $builder->applyLanguage($customerReport->getIdLanguage());
            $builder->parseConfigData($customerReport->getReportFinalData());
            $builder->bodBarcodeBook = $this->buildBodBarcode($bodShipmentBook->getId(), "02", $ext);
            $builder->bodBarcodeCover = $this->buildBodBarcode($bodShipmentBook->getId(), "12", $ext);
            $builder->bodBarcodeOrderNumber = $this->formatNumberId($bodShipmentBook->getId());
            $builder->totalNumPage = $numPage;
            $builder->prepareNextSection();
            $builder->resetLayout();
            $builder->moveNextPage();

            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:ReportItem', 'p')
                ->andWhere('p.idReportSection = :idReportSection')
                ->setParameter('idReportSection', $idReportSection)
                ->orderBy('p.sortOrder', 'ASC');
            $result = $query->getQuery()->getResult();
            foreach ($result as $row) {
                $texttool = array(
                    'type' => $row->getIdTextToolType(),
                    'data' => json_decode($row->getData(), true)
                );
                $builder->addTextTool($texttool['type'], $texttool['data']);
            }
            $builder->endPage();

            // Output to file
            $builder->getPdf()->Output($coverPdf, 'F');
        }
    }

    protected function buildBodBarcode($idBodShipment, $code, $ext = '11') {
        $barcode = $this->formatBookId($idBodShipment).$ext.$code;
        $sum = 0;
        for ($i = 0; $i < strlen($barcode); $i++) {
            $d = intval($barcode[$i]);
            $mul = ($i % 2 == 0) ? 1 : 3;
            $sum += $d * $mul;
        }

        $checkSum = (10 - ($sum % 10)) % 10;

        return $barcode.$checkSum;
    }

    protected function formatNumberId($shipmentId) {
        while (strlen($shipmentId) < 8) $shipmentId = '0'.$shipmentId;
        return $shipmentId;
    }
}
