<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

use Leep\AdminBundle\Business;

class BackupDiskCommand extends ContainerAwareCommand {
    private $statistic;
    private $s3;

    protected function configure() {
        $this
            ->setName('leep_admin:backup_disk')
            ->setDescription('Backup files to S3 server');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        echo "Begin\n";
        $this->statistic = (object)[
            'reports' => 0,
            'cryosave' => 0,
            'dead_reports' => 0,
            'shipments' => 0,
            'dead_shipments' => 0
        ];

        // Instantiate an S3 client
        $this->s3 = S3Client::factory([
            'credentials' => [
                'key'    => 'AKIAINK6TVPLDQ4R6YQA',
                'secret' => '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',
            ],

            'region' => 'us-east-1'
        ]);

        try {
            $this->s3->createBucket(['Bucket' => 'ordermanager-disk-backup', 'ACL' => 'public-read-write']);
            $this->s3->waitUntil('BucketExists', ['Bucket' => 'ordermanager-disk-backup']);
        } catch (S3Exception $e) {
            echo "Exception: {$e->getMessage()}!\n";
            return;
        }

        $this->backupCustomerReport();
        $this->backupShipments();

        echo "\nDone!!!\nStatistic:\n";
        echo "Reports: {$this->statistic->reports}\n";
        echo "Cryosave reports: {$this->statistic->cryosave}\n";
        echo "Dead reports: {$this->statistic->dead_reports}\n";
        echo "Shipments: {$this->statistic->shipments}\n";
        echo "Dead shipments: {$this->statistic->dead_shipments}\n";
        echo "Finished.\n";
    }

    private function backupShipments() {
        $activeFiles = [];
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $checkDate = new \DateTime('-2 month');
        echo "Backup active shipments\n";

        $query = $em->createQueryBuilder();
        $shipmentList = $query->select('p')
                ->from('AppDatabaseMainBundle:BodShipment', 'p')
                ->where('p.status = :status')->setParameter('status', Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_FINISHED)
                ->orderBy('p.creationDatetime', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($shipmentList as $shipment) {
            $shipment = $shipment[0];
            echo "Backup Shipment #{$shipment->getId()}\n";
            $time = $shipment->getCreationDatetime();

            // Backup file list:
            $files = [
                $shipment->getHighResolutionFile(),
                $shipment->getLowResolutionFile()
            ];

            $activeFiles = array_merge(array_map('trim', $files), $activeFiles);

            if ($time && $time < $checkDate) {
                foreach ($files as $file) {
                    $file = trim($file);
                    if (empty($file)) {
                        continue;
                    }

                    // Upload file to server
                    $this->uploadToAmazonS3($file, "files/shipment", "shipments");
                    $this->statistic->shipments++;
                }
            }

            $em->detach($shipment);
        }

        echo "Backup dead shipments\n";
        $path = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../files/shipment';
        if($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..' && is_file($path.'/'.$file) && !in_array($file, $activeFiles)) {
                    $this->uploadToAmazonS3($file, "files/shipment", "dead_shipments");
                    $this->statistic->dead_shipments++;
                }
            }
        }

        closedir($handle);
    }

    private function backupCustomerReport() {
        $activeFiles = [];
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $checkDate = new \DateTime('-2 month');
        echo "Backup active customer reports\n";

        $query = $em->createQueryBuilder();
        $customerReportList = $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerReport', 'p')
                ->orderBy('p.inputTime', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($customerReportList as $customerReport) {
            $customerReport = $customerReport[0];
            echo "Backup Customer Report #{$customerReport->getId()}\n";
            $reportTime = $customerReport->getInputTime();

            // Backup file list:
            $files = [
                // low resolution files
                $customerReport->getWebCoverLowResolutionFilename(),
                $customerReport->getLowResolutionFilename(),
                $customerReport->getBodCoverLowResolutionFilename(),

                // Excel
                $customerReport->getFoodTableExcelFilename(),

                // XML
                $customerReport->getXmlFilename(),

                // Other
                $customerReport->getWebCoverFilename(),
                $customerReport->getFilename(),
                $customerReport->getBodCoverFilename()
            ];

            $activeFiles = array_merge(array_map('trim', $files), $activeFiles);

            if ($reportTime && $reportTime < $checkDate) {
                foreach ($files as $file) {
                    $file = trim($file);
                    if (empty($file)) {
                        continue;
                    }

                    // Upload file to server
                    $this->uploadToAmazonS3($file, "web/report", "customer_reports");
                    $this->statistic->reports++;
                }
            }

            $em->detach($customerReport);
        }

        echo "Backup Cryosave sample reports\n";
        $query = $em->createQueryBuilder();
        $sampleList = $query->select('p')
                ->from('AppDatabaseMainBundle:CryosaveSample', 'p')
                ->orderBy('p.timestamp', 'ASC')
                ->getQuery()
                ->iterate();

        foreach ($sampleList as $sample) {
            $sample = $sample[0];
            echo "Backup Sample #{$sample->getId()}\n";
            $timestamp = $sample->getTimestamp();
            $file = trim($sample->getReportFilename());
            if (!empty($file)) {
                $activeFiles[] = $file;
                if ($timestamp && $timestamp < $checkDate) {
                    // Upload file to server
                    $this->uploadToAmazonS3($file, "web/report", "cryo_sample_reports");
                    $this->statistic->cryosave++;
                }
            }

            $em->detach($sample);
        }

        echo "Backup dead customer reports\n";
        $path = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../web/report';
        if($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..' && is_file($path.'/'.$file) && !in_array($file, $activeFiles)) {
                    $this->uploadToAmazonS3($file, "web/report", "dead_customer_reports");
                    $this->statistic->dead_reports++;
                }
            }
        }

        closedir($handle);
    }

    private function uploadToAmazonS3($fileName, $folder, $folderKey) {
        $fileDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../'.$folder;
        echo "Upload file $fileName ($folderKey) to Amazon S3 Server\n";
        if (!is_file("$fileDir/$fileName")) {
            echo "Error: File $fileDir/$fileName not existed!\n";
            return;
        }

        try {
            try{
                $uploader = UploadBuilder::newInstance()
                    ->setClient($this->s3)
                    ->setSource("$fileDir/$fileName")
                    ->setBucket('ordermanager-disk-backup')
                    ->setKey("$folderKey/$fileName")
                    ->setOption('CacheControl', 'max-age=3600')
                    ->build();

                $uploader->upload();
            } catch (MultipartUploadException $e) {
                $uploader->abort();
                echo "Exception: {$e->getMessage()}.\n";
                return;
            }
        } catch (\Exception $e) {
            echo "Exception: {$e->getMessage()}.\n";
            return;
        }
    }
}
