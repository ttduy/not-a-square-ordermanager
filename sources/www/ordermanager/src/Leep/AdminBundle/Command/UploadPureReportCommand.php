<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UploadPureReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:upload_pure_report')
            ->setDescription('Upload PURE report');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        //N8B7026 
        $idOrder = 21689;
        $idCustomerReport = 14499;
        $idCustomerReport = 14498;
        $dir = $container->getParameter('kernel.root_dir').'/../web/report';

        $report = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findOneById($idCustomerReport);
        $xmlFile = $report->getXmlFilename();
        $excelFile = getFoodTableExcelFilename();

        $this->upload($dir.'/'.$xmlFile, '/N8B7026.xml');
        $this->upload($dir.'/'.$excelFile, '/N8B7026.xlsx');
    }

    public function upload($srcFile, $destFile) {
        $host = '188.66.16.89';
        $username = 'pure_ftp';
        $password = '3Bv2BMaU1kGcbDV=cPF/{6DAGU[vmh*"Bm$Z^^G(J:#QbT<[+LwIW4FL(ztM';

        $connection = ssh2_connect($host, 22);
        if($connection === false) {
            echo "Cannot connect to server: " . $host;
        }
        ssh2_auth_password($connection, $username, $password);
        if (ssh2_scp_send($connection, $srcFile, $destFile, 0644) === false) {
            echo "Cannot send the file";
        }
    }
}
