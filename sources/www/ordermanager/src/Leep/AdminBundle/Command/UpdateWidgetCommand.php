<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateWidgetCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:update_widget')
            ->setDescription('Update widget');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {      
        $widgets = $this->getContainer()->get('doctrine')->getRepository('AppDatabaseMainBundle:WidgetStatistic')->findAll();
        foreach ($widgets as $widget) {
            Business\WidgetStatistic\Utils::updateWidget($this->getContainer(), $widget->getId());
        }
    }
}
