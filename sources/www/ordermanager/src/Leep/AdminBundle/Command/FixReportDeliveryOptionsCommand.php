<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixReportDeliveryOptionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_report_delivery_options')
            ->setDescription('Fix Report Delivery Options (Customers - Distribution Channels)');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        echo "Fix Report Deliverty Options - Customer";        
        $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findAll();
        $this->setReportDeliveryOptions($customers);
        $em->flush();

        echo "Fix Report Deliverty Options - DC";        
        $dcs = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')
        ->findAll();
        $this->setReportDeliveryOptions($dcs);
        $em->flush();
        
    }

    private function setReportDeliveryOptions($array) {
        foreach ($array as $item) {
            $idReportDelivery = $item->getReportDeliveryId();
            if ($idReportDelivery == 1) {
                $item->setIsReportDeliveryDownloadAccess(1);
                $item->setIsReportDeliveryFtpServer(0);
                $item->setIsReportDeliveryPrintedBooklet(0);
                $item->setIsReportDeliveryDontChargeBooklet(0);
            }
            else if ($idReportDelivery == 3) {
                $item->setIsReportDeliveryDownloadAccess(0);
                $item->setIsReportDeliveryFtpServer(1);
                $item->setIsReportDeliveryPrintedBooklet(0);
                $item->setIsReportDeliveryDontChargeBooklet(0);     
            }
            else if ($idReportDelivery == 4 || $idReportDelivery == 5) {                
                $item->setIsReportDeliveryDownloadAccess(0);
                $item->setIsReportDeliveryFtpServer(0);
                $item->setIsReportDeliveryPrintedBooklet(1);
                $item->setIsReportDeliveryDontChargeBooklet(0);     
            } 
            else if ($idReportDelivery == 6 || $idReportDelivery == 7){                
                $item->setIsReportDeliveryDownloadAccess(0);
                $item->setIsReportDeliveryFtpServer(0);
                $item->setIsReportDeliveryPrintedBooklet(0);
                $item->setIsReportDeliveryDontChargeBooklet(1);     
            }
            else {
                $item->setIsReportDeliveryDownloadAccess(0);
                $item->setIsReportDeliveryFtpServer(0);
                $item->setIsReportDeliveryPrintedBooklet(0);
                $item->setIsReportDeliveryDontChargeBooklet(0);     
            }                    
        }
    }
}
