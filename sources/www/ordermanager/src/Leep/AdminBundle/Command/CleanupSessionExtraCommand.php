<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CleanupSessionExtraCommand extends ContainerAwareCommand
{
    public $recycleBinDir = '';
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:cleanup_session_extra')
            ->setDescription('Cleanup session extra');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {                
        $container = $this->getContainer();

        $now = new \DateTime();
        $ts = $now->getTimestamp();
        $now->setTimestamp($ts - 24*60*60);
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query
            ->delete('AppDatabaseMainBundle:SessionExtra', 'p')
            ->andWhere('p.sessionTime < :truncDate')
            ->setParameter('truncDate', $now)
            ->getQuery()
            ->execute();
    }
}
