<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ImportCustomFontCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:import_custom_font')
            ->setDescription('Import custom font');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $fontFile = $container->getParameter('kernel.root_dir').'/../files/fonts/custom1.ttf';
        $pdf = $container->get('white_october.tcpdf')->create();

        $code = $pdf->addTTFFont($fontFile);
        print_r($code);
    }
}
