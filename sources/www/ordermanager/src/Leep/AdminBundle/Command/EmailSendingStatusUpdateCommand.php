<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
//use Aws\Ses\SesClient;


/* to RUN:
php app/console leep_admin:email_sending_status_update
*/
class EmailSendingStatusUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:email_sending_status_update')
            ->setDescription('Email Sending Status Update')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        echo self::getStatistics();
    }

    private function getStatistics() {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $config = $container->get('leep_admin.helper.common')->getConfig();

        $errMsg = array();
        $maxResult = 500;
        $mandrillApiKey = $config->getMandrillApiKey();
        $arrStatus = array(
                            Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_QUEUED,
                            Business\MailSender\Util::EMAIL_POOL_RECEIVER_STATUS_SCHEDULED,
        );


        if ($mandrillApiKey) {
            $query = $em->createQueryBuilder();
            $result = $query->select('p')
                            ->from('AppDatabaseMainBundle:EmailPoolReceiver', 'p')
                            ->andWhere('p.status IN (:arrStatus)')
                            ->setParameter('arrStatus', $arrStatus)
                            ->getQuery()
                            ->setMaxResults($maxResult)
                            ->getResult();

            if ($result) {
                $mandrill = new \Mandrill($mandrillApiKey);
                $arrEmailPoolStats = array();
                foreach ($result as $entry) {
                    try {
                        $result = $mandrill->messages->info($entry->getIdMessage());
                        if (!empty($result)) {
                            $newStatus = Business\MailSender\Util::getReceiverLogStatus($result['state']);
                            $oldStatus = $entry->getStatus();

                            if ($newStatus != $oldStatus) {
                                // update status
                                $entry->setStatus($newStatus);

                                // create log
                                Business\MailSender\Util::createEmailPoolLog($container, $entry->getIdEmailPool(), $newStatus, $entry->getEmail(), null, $entry->getName());                            

                                if (!array_key_exists($entry->getIdEmailPool(), $arrEmailPoolStats)) {
                                    $arrEmailPoolStats[$entry->getIdEmailPool()] = array();
                                }

                                if (!array_key_exists($oldStatus, $arrEmailPoolStats[$entry->getIdEmailPool()])) {
                                    $arrEmailPoolStats[$entry->getIdEmailPool()][$oldStatus] = 0;
                                }

                                if (!array_key_exists($newStatus, $arrEmailPoolStats[$entry->getIdEmailPool()])) {
                                    $arrEmailPoolStats[$entry->getIdEmailPool()][$newStatus] = 0;
                                }

                                $arrEmailPoolStats[$entry->getIdEmailPool()][$oldStatus]--;
                                $arrEmailPoolStats[$entry->getIdEmailPool()][$newStatus]++;
                            }
                        }                        
                    } catch(\Exception $e) {
                        // error happens sometimes because of delay in synchronization on Mandrill 
                        $errMsg[] = sprintf("Error with %s: %s", $entry->getIdMessage(), $e->getMessage());
                    }
                }

                $em->flush();

                // update Email Pool stats (such as: numSent, numQueued)
                foreach ($arrEmailPoolStats as $idEmailPool => $arrStatus) {
                    $emailPool = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:EmailPool')->findOneById($idEmailPool);
                    if ($emailPool) {
                        foreach ($arrStatus as $statusKey => $val) {
                            $statusTitle = Business\MailSender\Util::getEmailPoolReceiverStatusList()[$statusKey];
                            $getMethod = 'getNum' . $statusTitle;
                            $setMethod = 'setNum' . $statusTitle;

                            $emailPool->$setMethod($emailPool->$getMethod() + $val);
                        }
                    }
                    $em->flush();
                }
            }
        }

        if (!empty($errMsg)) {
            print_r($errMsg);
        }
    }
}
