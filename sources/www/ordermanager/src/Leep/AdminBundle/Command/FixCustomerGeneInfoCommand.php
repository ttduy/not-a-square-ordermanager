<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixCustomerGeneInfoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_customer_gene_info')
            ->setDescription('Fix customer gene info');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p.idCustomer, p.idGene, COUNT(p) as total')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
            ->andWhere('p.idGene != 0')
            ->groupBy('p.idCustomer, p.idGene');
        $result = $query->getQuery()->getResult();

        $customers = array();
        foreach ($result as $r) {
            $total = intval($r['total']);
            $idCustomer = intval($r['idCustomer']);
            if ($total > 1) {
                $customers[$idCustomer] = 1;
            }
        }
        foreach ($customers as $idCustomer => $t) {
            echo "Process ".$idCustomer."\n";
            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
                ->andWhere('p.idCustomer = '.$idCustomer);
            $result = $query->getQuery()->getResult();

            $genes = array();
            foreach ($result as $r) {
                $idGene = $r->getIdGene();
                $data = $r->getResult();
                if (!isset($genes[$idGene])) {
                    $genes[$idGene] = array();
                }
                $genes[$idGene][] = $data;
            }

            $fixedGenes = array();
            foreach ($genes as $idGene => $arr) {
                $fixedGenes[$idGene] = $this->getValue($arr);
            }

            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:CustomerInfoGene', 'p')
                ->andWhere('p.idCustomer = '.$idCustomer)
                ->getQuery()
                ->execute();

            foreach ($fixedGenes as $idGene => $v) {
                $p = new Entity\CustomerInfoGene();
                $p->setIdCustomer($idCustomer);
                $p->setIdGene($idGene);
                $p->setResult($v);
                $em->persist($p);
            }
            $em->flush();
        }
    }

    protected function getValue($array) {
        foreach ($array as $t) {
            if (trim($t) != '') {
                return trim($t);
            }
        }
        return '';
    }
}
