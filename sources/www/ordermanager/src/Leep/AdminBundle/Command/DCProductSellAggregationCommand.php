<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* 
this command is used to
- aggregate data for table: distribution_channels_product_sell

to RUN:
    php app/console leep_admin:dc_product_sell_aggregation
*/
class DCProductSellAggregationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:dc_product_sell_aggregation')
            ->setDescription('Aggregate DC Product Sell')
            ->addArgument(
                'numberOfTopSellPerDC',
                InputArgument::OPTIONAL,
                'Number of top sell per Distribution Channel?'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $dc = array();

        // 3 as default
        $numberOfTopSellPerDC = ($input->getArgument('numberOfTopSellPerDC') ? $input->getArgument('numberOfTopSellPerDC') : 3);

        // aggregate data from ordered_product
        // sometimes found record with 0 for idDistributionChannel, to prevent it, I add "->andWhere('c.distributionchannelid <> 0')"
        $orderedProductQuery = $em->createQueryBuilder();
        $orderedProductResult = $orderedProductQuery->select('c.distributionchannelid, p.productid, count(p.id) totalProduct')
                                                    ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
                                                    ->innerJoin('AppDatabaseMainBundle:Customer', 'c', 'WITH', 'c.id = p.customerid')
                                                    ->andWhere('c.distributionchannelid IS NOT NULL AND c.distributionchannelid <> 0')
                                                    ->groupBy('c.distributionchannelid')
                                                    ->addGroupBy('p.productid')
                                                    ->getQuery()
                                                    ->getResult();

        if ($orderedProductResult) {
            foreach ($orderedProductResult as $entry) {
                if (!array_key_exists($entry['distributionchannelid'], $dc)) {
                    $dc[$entry['distributionchannelid']] = array();
                }
                $dc[$entry['distributionchannelid']][$entry['productid']] = $entry['totalProduct'];
            }
        }

        // aggregate data from ordered_special_product
        $orderedSpecialProductQuery = $em->createQueryBuilder();
        $orderedSpecialProductResult = $orderedSpecialProductQuery->select('sp.idDistributionChannel, sp.idProduct, count(p.id) totalProduct')
                                                                    ->from('AppDatabaseMainBundle:OrderedSpecialProduct', 'p')
                                                                    ->innerJoin('AppDatabaseMainBundle:SpecialProduct', 'sp', 'WITH', 'p.idSpecialProduct = sp.id')
                                                                    ->leftJoin('AppDatabaseMainBundle:OrderedProduct', 'op', 'WITH', 'op.customerid = p.idCustomer AND op.specialproductid = p.idSpecialProduct')
                                                                    ->andWhere('op.id IS NULL')
                                                                    ->groupBy('sp.idDistributionChannel')
                                                                    ->addGroupBy('sp.idProduct')
                                                                    ->getQuery()
                                                                    ->getResult();

        if ($orderedSpecialProductResult) {
            foreach ($orderedSpecialProductResult as $entry) {
                if (!array_key_exists($entry['idDistributionChannel'], $dc)) {
                    $dc[$entry['idDistributionChannel']] = array();
                }

                if (!array_key_exists($entry['idProduct'], $dc[$entry['idDistributionChannel']])) {
                    $dc[$entry['idDistributionChannel']][$entry['idProduct']] = 0;
                }
                $dc[$entry['idDistributionChannel']][$entry['idProduct']] += $entry['totalProduct'];
            }
        }

        // update table: distribution_channels_product_sell
        $q = $em->createQueryBuilder();
        $q->delete('AppDatabaseMainBundle:DistributionChannelsProductSell', 'p')
            ->getQuery()
            ->execute();

        foreach ($dc as $idDistributionChannel => $products) {
            $topSell = 0;
            // sort
            uasort($products, array('self', 'cmp'));
            foreach ($products as $idProduct => $totalProduct) {
                $topSell++;

                $productSell = new Entity\DistributionChannelsProductSell();
                $productSell->setIdDistributionChannel($idDistributionChannel);
                $productSell->setIdProduct($idProduct);
                $productSell->setTotal($totalProduct);
                $productSell->setIsTopSell(($topSell <= $numberOfTopSellPerDC) ? 1 : 0);

                $em->persist($productSell);
                $em->flush();
            }
        }
    }

    // Comparison function
    private function cmp($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? 1 : -1;
    }
}
