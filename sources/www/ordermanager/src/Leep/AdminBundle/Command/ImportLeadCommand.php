<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: sourceId = '[file path]' | separator = '|' as default
php app/console leep_admin:importLead /home/pat/Downloads/lead.csv
*/
class ImportLeadCommand extends ContainerAwareCommand
{
    public $partnerList = array();
    public $dcList = array();
    public $acquisiteurList = array();
    public $countryList = array();
    public $reminderInList = array();

    protected function configure()
    {
        $this
            ->setName('leep_admin:importLead')
            ->setDescription('Import Lead From CSV File')
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                'Where is the csv file'
            )
            ->addArgument(
                'separator',
                InputArgument::OPTIONAL,
                'what is separator, | as default'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $sourceFile = $input->getArgument('sourceFile');
        $separator = ($input->getArgument('separator') ? $input->getArgument('separator') : '|');

        $numberOfNewLeads = 0;
        $totalDataRows = 0;
        $counter = 0;

        $daysMap = Business\Lead\Constants::getDayMaps();
        if (($inputHandle = fopen($sourceFile, "r")) !== FALSE) {
            $this->preparePartnerList();
            $this->prepareDCList();
            $this->prepareAcquisiteurList();
            $this->prepareCountryList();
            $this->prepareReminderInList();

            while (($arrFields = fgetcsv($inputHandle, 0, $separator)) !== FALSE) {
                $totalDataRows++;

                $leadData = $this->parseData($arrFields);

                if (!$this->existInDB($leadData)) {
                    $numberOfNewLeads++;
                    $counter++;

                    $newLead = new Entity\Lead();
                    $newLead->setName($leadData['name']);
                    $newLead->setCreationDate($leadData['creationDate']);
                    $newLead->setIdReminderIn($leadData['idReminderIn']);
                    $newLead->setTitle($leadData['title']);
                    $newLead->setIdPartner($leadData['idPartner']);
                    $newLead->setIdDistributionChannel($leadData['idDistributionChannel']);
                    $newLead->setIdAcquisiteur($leadData['idAcquisiteur']);
                    $newLead->setCompany($leadData['company']);
                    $newLead->setFirstName($leadData['firstName']);
                    $newLead->setSurName($leadData['surName']);
                    $newLead->setStreet($leadData['street']);
                    $newLead->setPostcode($leadData['postcode']);
                    $newLead->setCity($leadData['city']);
                    $newLead->setIdCountry($leadData['idCountry']);
                    $newLead->setContactemail($leadData['contactEmail']);
                    $newLead->setSource($leadData['source']);
                    $newLead->setTelephone($leadData['telephone']);

                    if (isset($daysMap[$newLead->getIdReminderIn()])) {
                        $date = clone $newLead->getCreationDate();
                        if ($date) {
                            $numDays = $daysMap[$newLead->getIdReminderIn()];
                            $timestamp = $date->getTimestamp();
                            $timestamp += 24*60*60*$numDays;
                            $date->setTimestamp($timestamp);
                            $newLead->setReminderDate($date);
                        }
                    }

                    $em->persist($newLead);
                }

                if ($counter % 500 == 0) {
                    $em->flush();
                }
                echo $counter."\n";
            }

            $em->flush();
        }

        echo "Number of rows in CSV: " . $totalDataRows . "\n";
        echo "Number of new Leads: " . $numberOfNewLeads . "\n";
    }

    protected function existInDB($leadData) {
        return false;
    }

    protected function parseData($arr) {
        // Creation date|Reminder in|Titel|job descrition|physician specialty|PARTNER|DC|AKQUISITEUR|Company|First Name|Second Name|Street|ZIP|City|Country|E-Mail|Tel|Fax|Website
        $arrTmp = array();
        foreach ($arr as $entry) {
            $arrTmp[] = trim(self::cleanDoubleQuotes($entry));
        }
        $arr = $arrTmp;
        $this->processData($arr);

        $data = array(
                                        'creationDate'  => $arr[0],
                                        'idReminderIn'  => $arr[1],
                                        'title'         => $arr[2],
                                        'source'        => null,
                                        'idPartner'     => is_int($arr[5]) ? $arr[5] : null,
                                        'idDistributionChannel'     => is_int($arr[6]) ? $arr[6] : null,
                                        'idAcquisiteur' => is_int($arr[7]) ? $arr[7] : null,
                                        'company'       => $arr[8],
                                        'firstName'     => $arr[9],
                                        'surName'       => $arr[10],
                                        'street'        => $arr[11],
                                        'postcode'      => $arr[12],
                                        'city'          => $arr[13],
                                        'idCountry'     => is_int($arr[14]) ? $arr[14] : null,
                                        'contactEmail'  => $arr[15],
                                        'telephone'     => $arr[16]
                                    );

        $data['name'] = $this->prepareName($arr);
        $data['source'] = $this->prepareSource($arr);

        return $data;
    }

    protected function cleanDoubleQuotes($str) {
        if (substr($str, 0, 1) == '"') {
            $str = substr($str, 1);
        }

        if (substr($str, -1, 1) == '"') {
            $str = substr($str, -1);
        }

        return $str;
    }

    protected function prepareName($arr) {
        $name = null;

        if ($arr[9]) {
            $name .= $arr[9];
        }

        if ($arr[10]) {
            if ($name) {
                $name .= ' ';
            }

            $name .= $arr[10];
        }

        return $name;
    }

    protected function prepareSource($arr) {
        $sourceData = array();
        if ($arr[3]) {
            $sourceData[] = 'Job Description: ' . $arr[3];
        }
        if ($arr[4]) {
            $sourceData[] = 'Physician Specialty: ' . $arr[4];
        }
        if ($arr[5] && !is_int($arr[5])) {
            $sourceData[] = 'Partner: ' . $arr[5];
        }
        if ($arr[6] && !is_int($arr[6])) {
            $sourceData[] = 'Distribution Channel: ' . $arr[6];
        }
        if ($arr[7] && !is_int($arr[7])) {
            $sourceData[] = 'Acquisiteur: ' . $arr[7];
        }
        if ($arr[14] && !is_int($arr[14])) {
            $sourceData[] = 'Country: ' . $arr[14];
        }
        if ($arr[15]) {
            $sourceData[] = 'Email: ' . $arr[15];
        }
        if ($arr[17]) {
            $sourceData[] = 'Fax: ' . $arr[17];
        }
        if ($arr[18]) {
            $sourceData[] = 'Website: ' . $arr[18];
        }

        return (empty($sourceData) ? null : implode("\n", $sourceData));
    }


    protected function preparePartnerList() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrPartners = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Partner')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrPartners[strtolower($entry->getPartner())] = $entry->getId();
            }
        }

        $this->partnerList = $arrPartners;
    }

    protected function prepareDCList() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrDCs = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrDCs[strtolower($entry->getDistributionchannel())] = $entry->getId();
            }
        }

        $this->dcList = $arrDCs;
    }

    protected function prepareAcquisiteurList() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrAcquisiteurs = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Acquisiteur')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrAcquisiteurs[strtolower($entry->getAcquisiteur())] = $entry->getId();
            }
        }

        $this->acquisiteurList = $arrAcquisiteurs;
    }

    protected function prepareCountryList() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrCountries = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:Countries')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrCountries[strtolower($entry->getCountry())] = $entry->getId();
            }
        }

        $this->countryList = $arrCountries;
    }


    protected function prepareReminderInList() {
        $this->reminderInList = array();
        $arr = Business\Lead\Constants::getReminderIn();
        foreach ($arr as $k => $v) {
            $this->reminderInList[strtolower($v)] = $k;
        }
    }

    protected function processCreationDate(&$arr) {
        $result = null;
        $creationDate = $arr[0];
        if ($creationDate) {
            $dateTime = new \DateTime();
            $result = $dateTime->setTimestamp(strtotime($creationDate));
        }
        $arr[0] = $result;
    }

    protected function processReminderIn(&$arr) {
        $result = null;
        $value = $arr[1];
        if ($value && array_key_exists(strtolower($value), $this->reminderInList)) {
            $result = $this->reminderInList[strtolower($value)];
        }
        else {
            if ($value) echo "REMINDER IN: ".strtolower($value)."\n"; 
        }
        $arr[1] = $result;
    }

    protected function processTitle(&$arr) {
        $result = null;
        $value = $arr[2];
        if ($value) {
            $result = $value;
        }
        $arr[2] = $result;
    }

    protected function processJobDescription(&$arr) {
        $result = null;
        $value = $arr[3];
        if ($value) {
            $result = $value;
        }
        $arr[3] = $result;
    }

    protected function processPhysicianSpecialty(&$arr) {
        $result = null;
        $value = $arr[4];
        if ($value) {
            $result = $value;
        }
        $arr[4] = $result;
    }

    protected function processPartner(&$arr) {
        $value = $arr[5];
        $result = $value;
        if ($value && array_key_exists(strtolower($value), $this->partnerList)) {
            $result = $this->partnerList[strtolower($value)];
        }
        else {
            if ($value) echo "PARTNER: ".strtolower($value)."\n"; 
        }
        $arr[5] = $result;
    }

    protected function processDC(&$arr) {
        $value = $arr[6];
        $result = $value;
        
        if ($value && array_key_exists(strtolower($value), $this->dcList)) {
            $result = $this->dcList[strtolower($value)];
        }
        else {
            if ($value) echo "DC: ".strtolower($value)."\n";
        }

        $arr[6] = $result;
    }


    protected function processAcquisiteur(&$arr) {
        $value = $arr[7];
        $result = $value;
        
        if ($value && array_key_exists(strtolower($value), $this->acquisiteurList)) {
            $result = $this->acquisiteurList[strtolower($value)];
        }
        else {
            if ($value) echo "Acquisiteur: ".strtolower($value)."\n";
        }
        $arr[7] = $result;
    }

    protected function processCompany(&$arr) {
        $result = null;
        $value = $arr[8];
        if ($value) {
            $result = $value;
        }
        $arr[8] = $result;
    }

    protected function processFirstName(&$arr) {
        $result = null;
        $value = $arr[9];
        if ($value) {
            $result = $value;
        }

        $arr[9] = $result;
    }

    protected function processSecondName(&$arr) {
        $result = null;
        $value = $arr[10];
        if ($value) {
            $result = $value;
        }

        $arr[10] = $result;
    }

    protected function processStreet(&$arr) {
        $result = null;
        $value = $arr[11];
        if ($value) {
            $result = $value;
        }

        $arr[11] = $result;
    }

    protected function processZip(&$arr) {
        $result = null;
        $value = $arr[12];
        if ($value) {
            $result = $value;
        }

        $arr[12] = $result;
    }

    protected function processCity(&$arr) {
        $result = null;
        $value = $arr[13];
        if ($value) {
            $result = $value;
        }

        $arr[13] = $result;
    }

    protected function processCountry(&$arr) {
        $value = $arr[14];
        $result = $value;
        if ($value && array_key_exists(strtolower($value), $this->countryList)) {
            $result = $this->countryList[strtolower($value)];
        }
        else {
            if ($value) echo "COUNTRY: ".strtolower($value)."\n";
        }

        $arr[14] = $result;
    }

    protected function processEmail(&$arr) {
        $value = $arr[15];
        $result = $value;

        if ($value) {
            $pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/';
            preg_match_all($pattern, $value, $matches);
            if (sizeof($matches[0]) > 0) {
                $result = implode(';', $matches[0]);
            }
        }

        $arr[15] = $result;
    }

    protected function processTelephone(&$arr) {
        $result = null;
        $value = $arr[16];
        if ($value) {
            $result = $value;
        }

        $arr[16] = $result;
    }

    protected function processFax(&$arr) {
        $result = null;
        $value = $arr[17];
        if ($value) {
            $result = $value;
        }

        $arr[17] = $result;
    }

    protected function processWebsite(&$arr) {
        $result = null;
        $value = $arr[18];
        if ($value) {
            $result = $value;
        }

        $arr[18] = $result;
    }

    protected function processData(&$arr) {
        $this->processCreationDate($arr);
        $this->processReminderIn($arr);
        $this->processTitle($arr);
        $this->processJobDescription($arr);
        $this->processPhysicianSpecialty($arr);
        $this->processPartner($arr);
        $this->processDC($arr);
        $this->processAcquisiteur($arr);
        $this->processCompany($arr);
        $this->processFirstName($arr);
        $this->processSecondName($arr);
        $this->processStreet($arr);
        $this->processZip($arr);
        $this->processCity($arr);
        $this->processCountry($arr);
        $this->processEmail($arr);
        $this->processTelephone($arr);
        $this->processFax($arr);
        $this->processWebsite($arr);
    }

}
