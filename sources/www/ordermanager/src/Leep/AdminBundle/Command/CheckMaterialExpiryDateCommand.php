<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CheckMaterialExpiryDateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:check_material_expiry_date')
            ->setDescription('Check material expiry date');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $mapping = $container->get('easy_mapping');

        $today = new \DateTime('2013-10-31');

        $expiredMaterials = array();
        $materials = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:Materials')->findByExpiryDate($today);
        foreach ($materials as $material) {
            $expiredMaterials[] = array(
                'description' => $material->getNovoDescription(),
                'name'        => $material->getName(),
                'type'        => $mapping->getMappingTitle('LeepAdmin_MaterialType_List', $material->getMaterialTypeId()),
                'supplier'    => $material->getSupplier()
            );
        }

        if (!empty($expiredMaterials)) {           
            try {
                $data = array(
                    'today' => $today->format('d/m/Y'),
                    'expiredMaterials' => $expiredMaterials
                );
                $senderEmail = $container->getParameter('sender_email');
                $senderName  = $container->getParameter('sender_name');

                $content = $container->get('templating')->render('LeepAdminBundle:EmailTemplates:material_expiration_notification.html.twig', $data);

                $targetAddress = array();
                $list = explode(';', $config->getMaterialExpirationNotificationEmail());
                foreach ($list as $emailAddress) {
                    $targetAddress[] = trim($emailAddress);
                }

                $message = \Swift_Message::newInstance()
                    ->setSubject("[DNAPlus] Material expiration notification")
                    ->setFrom($senderEmail, $senderName)
                    ->setTo($targetAddress)
                    ->setBody($content, 'text/html');
                $container->get('mailer')->send($message);            
            }
            catch (\Exception $e) {
                echo $e->getMessage();
            }    
        }   
        
    }
}
