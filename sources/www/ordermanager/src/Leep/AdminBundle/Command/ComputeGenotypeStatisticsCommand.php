<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:compute_genotype_statistics
*/
class ComputeGenotypeStatisticsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:compute_genotype_statistics')
            ->setDescription('Compute genotype statistics')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $genes = array();
        $results = $em->getRepository('AppDatabaseMainBundle:Genes')->findAll();
        foreach ($results as $r) {
            $genes[$r->getId()] = array(
                'name'             => $r->getGeneName(),
                'scientificName'   => $r->getScientificName(),
                'statistics'       => array()
            );
        }

        $query = $em->createQueryBuilder();
        $query->select('p.idGene, p.result, COUNT(p) total')
            ->from('AppDatabaseMainBundle:CustomerInfoGene', 'p')
            ->groupBy('p.idGene, p.result');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $idGene = intval($r['idGene']);
            $genotype = trim($r['result']);
            $total = intval($r['total']);
            if (isset($genes[$idGene])) {
                if ($genotype != '' && $genotype != '0') {
                    $genes[$idGene]['statistics'][$genotype] = array(
                        'total'       => $total,
                        'percentage'  => 0
                    );
                }
            }
        }

        foreach ($genes as $idGene => $gene) {
            $sum = 0;
            foreach ($gene['statistics'] as $genotype => $stats) {
                $sum += $stats['total'];
            }


            foreach ($gene['statistics'] as $genotype => $stats) {
                if ($sum > 0) {
                    $genes[$idGene]['statistics'][$genotype]['percentage'] = round($stats['total'] * 100.0 / $sum, 2);
                }
            }            
        }

        $rows = array();
        foreach ($genes as $idGene => $gene) {
            $row = $gene['name'].' ('.$gene['scientificName'].'): ';
            foreach ($gene['statistics'] as $genotype => $stats) {
                $row .= $genotype.': '.$stats['total'].' ('.$stats['percentage'].'%)  | ';
            }
            $rows[] = $row;
        }
        echo implode("\n", $rows);
    }
}
