<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DeleteReportQueueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:delete_report_queue')
            ->setDescription('Delete report queue');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Compute retention date
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $retentionDate = new \DateTime();
        $ts = $retentionDate->getTimestamp();
        $ts = $ts - intval($config->getRetentionDay()) * 24 * 60 * 60;
        $retentionDate->setTimestamp($ts);

        // Latest jobs
        $maxId = 0;
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults(1);
        $result = $query->getQuery()->getResult();
        if (!empty($result)) {
            foreach ($result as $r) {
                $maxId = $r->getId();
                break;
            }
        }

        // Delete jobs
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->andWhere('p.inputTime < :retentionDate')
            ->andWhere('p.id != :maxId')
            ->setParameter('maxId', $maxId)
            ->setParameter('retentionDate', $retentionDate)
            ->orderBy('p.inputTime', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $em->remove($r);
        }
        $em->flush();

        // Check
        $idArr = array(0);
        $result = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findAll();
        foreach ($result as $r) {
            $idArr[] = $r->getId();
        }

        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:ReportQueueLog', 'p')
            ->andWhere('p.idReportQueue NOT IN (:idArr)')
            ->setParameter('idArr', $idArr)
            ->getQuery()
            ->getResult();

    }
}
