<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateCustomerProductListCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:update_customer_product_list')
            ->setDescription('Get Product Form Product Category to update Product List');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $categoryProductLink = Business\Customer\Util::getCategoryProductLink($container);

        $customerList =
            $em->getRepository("AppDatabaseMainBundle:Customer")
                ->createQueryBuilder('p')
                ->getQuery()
                ->iterate();

        foreach ($customerList as $customer) {
            $customer = $customer[0];
            echo 'Processing: #'.$customer->getId()."\n";

            $orderedCategories = $em->getRepository("AppDatabaseMainBundle:OrderedCategory")
                ->createQueryBuilder('p')
                ->select('p.categoryid')
                ->where('p.customerid = :customerId')
                ->setParameter('customerId', $customer->getId())
                ->getQuery()
                ->getResult();

            $orderedProducts = $em->getRepository("AppDatabaseMainBundle:OrderedProduct")
                ->createQueryBuilder('p')
                ->select('p.productid')
                ->where('p.customerid = :customerId')
                ->setParameter('customerId', $customer->getId())
                ->getQuery()
                ->getResult();

            $productList = [];
            foreach ($orderedProducts as $product) {
                $productList[] = $product['productid'];
            }

            $categoryList = [];
            foreach ($orderedCategories as $category) {
                $categoryId = $category['categoryid'];
                $categoryList[] = $categoryId;

                $categoryProducts = [];
                if (array_key_exists($categoryId, $categoryProductLink)) {
                    $categoryProducts = $categoryProductLink[$categoryId];
                    $productList = array_merge($productList, $categoryProducts);
                }
            }

            $customer->setCategoryList(implode(',', array_unique($categoryList)));
            $customer->setProductList(implode(',', array_unique($productList)));

            $em->persist($customer);
            $em->flush();
            $em->detach($customer);
        }

        echo "Finished.\n";
    }
}

