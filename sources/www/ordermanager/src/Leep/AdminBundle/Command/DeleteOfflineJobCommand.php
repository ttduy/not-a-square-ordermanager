<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class DeleteOfflineJobCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:delete_offline_job')
            ->setDescription('Delete offline job');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Compute retention date
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $retentionDate = new \DateTime();
        $ts = $retentionDate->getTimestamp();
        $ts = $ts - intval($config->getRetentionDay()) * 24 * 60 * 60;
        $retentionDate->setTimestamp($ts);

        // Process
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p')
            ->andWhere('p.inputTime < :retentionDate')
            ->setParameter('retentionDate', $retentionDate);
        $results = $query->getQuery()->getResult();
        foreach ($results as $offlineJob) {
            try {
                $handler = Business\OfflineJob\Constants::getHandler($offlineJob->getIdHandler(), $container);
                $handler->deleteJob($offlineJob);

                $em->remove($offlineJob);
                $em->flush();
            }
            catch (\Exception $e) {                
            }
        }
    }
}
