<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:lead_reminder
*/
class LeadReminderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:lead_reminder')
            ->setDescription('Lead Reminder')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // get all Leaders need to be reminded today
        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('AppDatabaseMainBundle:Lead', 'p')
                ->andWhere('p.reminderDate = CURRENT_DATE()');

        $leads = $query->getQuery()->getResult();
        if ($leads && sizeof($leads) > 0) {
            foreach ($leads as $lead) {
                $msg = "Lead - Reminder: Lead ID %s - has something new";
                $notification = new Entity\Notification();
                $notification
                    ->setIdTopic(Business\Notification\Constant::LEAD_REMINDER)
                    ->setMessage(sprintf($msg, $lead->getId()))
                    ->setTimestamp(new \DateTime())
                ;
                $em->persist($notification);
                $em->flush();          
            }
        }
    }
}
