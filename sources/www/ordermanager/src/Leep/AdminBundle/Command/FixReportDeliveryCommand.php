<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixReportDeliveryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_report_delivery')
            ->setDescription('Fix Report Delivery');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $dir = $container->getParameter('kernel.root_dir').'/../database';

        $dcRD = array();
        $rows = explode("\n", file_get_contents($dir.'/fix_dc_report_delivery.csv'));
        foreach ($rows as $r) {
            $arr = explode(";", $r);
            if (count($arr) == 2) {
                $dcRD[intval($arr[0])] = intval($arr[1]);
            }
        }

        $customerRD = array();
        $rows = explode("\n", file_get_contents($dir.'/fix_customer_report_delivery.csv'));
        foreach ($rows as $r) {
            $arr = explode(";", $r);
            if (count($arr) == 2) {
                $customerRD[intval($arr[0])] = intval($arr[1]);
            }
        }

        echo "Fix DC..";
        $distributionChannels = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        foreach ($distributionChannels as $dc) {
            if (isset($dcRD[$dc->getId()])) {
                $dc->setReportDeliveryId($dcRD[$dc->getId()]);
            }
        }
        $em->flush();

        echo "Fix customer..";
        $i = 0;
        $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findAll();
        foreach ($customers as $c) {
            if (isset($customerRD[$c->getId()])) {
                $c->setReportDeliveryId($customerRD[$c->getId()]);
            }
            $i++;
            if ($i % 1000 == 0) {
                echo $i."\n";
                $em->flush();
            }
        }
        $em->flush();
    }
}
