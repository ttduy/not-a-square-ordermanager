<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixTextBlockLanguageTableCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_text_block_language_table')
            ->setDescription('Fix text block language table');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $textBlocks = array();
        // Text blocks
        $result = $em->getRepository('AppDatabaseMainBundle:TextBlock')->findAll();
        foreach ($result as $r) {
            $textBlocks[$r->getId()] = 1;
        }
        $em->clear();

        // Language
        $languages = $em->getRepository('AppDatabaseMainBundle:Language')->findAll();
        foreach ($languages as $language) {
            echo "Check language: ".$language->getName()."\n";
            $textBlockMissing = $textBlocks;

            $textBlockLanguages = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($language->getId());
            foreach ($textBlockLanguages as $tbl) {
                $idTextBlock = $tbl->getIdTextBlock();
                if (isset($textBlockMissing[$idTextBlock])) {
                    unset($textBlockMissing[$idTextBlock]);
                }
            }
            $em->clear();

            echo "  Missing: ".count($textBlockMissing)."\n";

            // Fix
            foreach ($textBlockMissing as $idTextBlock => $tmp) {
                $textBlockLanguage = new Entity\TextBlockLanguage();
                $textBlockLanguage->setIdLanguage($language->getId());
                $textBlockLanguage->setIdTextBlock($idTextBlock);
                $textBlockLanguage->setBody('');
                $em->persist($textBlockLanguage);
            }
            $em->flush();
            $em->clear();
        }
    }
}
