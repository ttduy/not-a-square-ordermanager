<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

class AnalyseDiskUsageCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('leep_admin:analyse_disk_usage')
            ->setDescription('Analyse disk usage');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        if (!mysql_connect(
            $container->getParameter('database_host').":".$container->getParameter('database_port'),
            $container->getParameter('database_user'),
            $container->getParameter('database_password')
        )) {
            die("Unable to connect to MySQL!");
        }

        if (!mysql_select_db($container->getParameter('database_name'))) {
            die("Unable to connect to database!");
        }

        $sql = "
            SELECT table_schema 'DB Name', ROUND(SUM(data_length + index_length)/1024/1024, 1) 'DB Size in MB'
            FROM information_schema.tables
            GROUP BY table_schema
            LIMIT 0, 30";

        $query = mysql_query($sql);
        if (!$query) {
            die("Unable to query from mysql!");
        }

        $mask = "|%-40s |%20s |\n";
        echo "MySQL database usage:\n";
        echo "=================================================================\n";
        printf($mask, 'Name', 'Size (MB)');
        echo "=================================================================\n";
        $total = 0;
        while ($data = mysql_fetch_array($query)) {
            printf($mask, "{$data['DB Name']}", "{$data['DB Size in MB']}");
            $total += floatval($data['DB Size in MB']);
        }

        echo "=================================================================\n";

        $mask = "|%-30s |%15s |%15s |%15s |\n";
        echo "\nDisk usage:\n";
        echo "====================================================================================\n";
        printf($mask, 'Folder', 'Size (MB)', 'Num file', 'Num Folder');
        echo "====================================================================================\n";
        $folders = ['web/attachments', 'web/report', 'files', 'app', 'vendor', 'src', 'files/bills', 'files/email', 'files/fonts', 'files/invoices', 'files/materials_files', 'files/orderBackup', 'files/payments', 'files/product', 'files/recipe', 'files/recycle_bin', 'files/report', 'files/scan_form', 'files/sftpDownload', 'files/shipment', 'files/temp'];
        foreach ($folders as $folder) {
            $result = $this->getDirectorySize($container->get('service_container')->getParameter('kernel.root_dir').'/../'.$folder.'/');
            printf($mask, $folder, number_format($result['size'], 2), $result['fileCount'], $result['dirCount']);
        }

        echo "====================================================================================\n";
    }

    private function getDirectorySize($path) {
        $totalsize = 0;
        $totalcount = 0;
        $dircount = 0;
        if($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                $nextpath = $path . '/' . $file;
                if ($file != '.' && $file != '..' && !is_link($nextpath)) {
                    if (is_dir($nextpath)) {
                        $dircount++;
                        $result = $this->getDirectorySize($nextpath);
                        $totalsize += $result['size'];
                        $totalcount += $result['fileCount'];
                        $dircount += $result['dirCount'];
                    } else if (is_file($nextpath)) {
                        $totalsize += filesize ($nextpath) / 1024;
                        $totalcount++;
                    }
                }
            }
        }

        closedir($handle);
        $total['size'] = $totalsize;
        $total['fileCount'] = $totalcount;
        $total['dirCount'] = $dircount;
        return $total;
    }
}
