<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixRevenueEstimationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_revenue_estimation')
            ->setDescription('Fix revenue estimation');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Preload
        $catProdLink = array();
        $result = $em->getRepository('AppDatabaseMainBundle:CatProdLink', 'app_main')->findAll();
        foreach ($result as $r) {
            if (!isset($catProdLink[$r->getProductId()])) {
                $catProdLink[$r->getProductId()] = array();
            }
            $catProdLink[$r->getProductId()][] = $r->getCategoryId();
        }

        // Update
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('p.dateordered >= :orderDate')
            ->andWhere('p.dateordered < :nextDate')
            ->setParameter('orderDate', new \DateTime('2014-01-01'))
            ->setParameter('nextDate', new \DateTime('2014-09-01'))
            ->orderBy('p.dateordered', 'DESC');
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $date = $r->getDateOrdered();
            echo $r->getId().' ';
            if ($date) {
                echo $date->format('d/m/Y');
            }
            echo "\n";

            Business\Customer\Util::updateRevenueEstimation($container, $r->getId(), $catProdLink);
        }
        $em->flush();
    }
}
