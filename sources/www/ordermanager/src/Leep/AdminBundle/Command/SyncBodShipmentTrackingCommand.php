<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class SyncBodShipmentTrackingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:sync_bod_shipment_tracking')
            ->setDescription('Sync BOD Shipment Tracking');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        
        Business\BodShipmentTracking\Utils::syncShipment($container);
    }
}
