<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixPriceDuplicateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_price_duplicate')
            ->setDescription('Fix price duplicate');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $priceCategories = $em->getRepository('AppDatabaseMainBundle:PriceCategories')->findAll();
        foreach ($priceCategories as $pc) {
            if ($pc->getIsDeleted() == 1) {
                //continue;
            }

            echo "Check ".$pc->getPriceCategory()."\n";
            $prices = $em->getRepository('AppDatabaseMainBundle:Prices')->findBypricecatid($pc->getId());
            $records = array();
            $duplicateRecords = array();

            foreach ($prices as $p) {
                $idProduct = intval($p->getProductId());
                if (!isset($records[$idProduct])) {
                    $records[$idProduct] = 1;
                }
                else {
                    $duplicateRecords[] = $p;
                    $em->remove($p);
                    $em->flush();
                }
            }

            if (!empty($duplicateRecords)) {
                echo "--> Duplicated found..";
            }
        }
    }
}
