<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ScanFormFileProcessingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:scan_form_file_processing')
            ->setDescription('Scan Form File Processing');
    }

    public $em;        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $this->em = $container->get('doctrine')->getEntityManager();
        $scanFormFolder = $container->getParameter('files_dir').'/scan_form';
        $utils = $container->get('leep_admin.scan_form_template.business.utils');
        $helper = $container->get('leep_admin.helper.common');

        // Load all template detection zones
        $templateZones = array();
        $i = 0;
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormTemplateZone', 'p')
            ->innerJoin('AppDatabaseMainBundle:ScanFormTemplate', 'j', 'WITH', 'p.idScanFormTemplate = j.id');
        $zones = $query->getQuery()->getResult();
        foreach ($zones as $zone) {            
            $zonesDef = $utils->parseZonesDefinition($zone->getZonesDefinition());
            foreach ($zonesDef['zones'] as $zoneDef) {
                if (!isset($templateZones[$zone->getIdScanFormTemplate()])) {
                    $templateZones[$zone->getIdScanFormTemplate()] = array(
                        'zones'   => array(),
                        'tokens'  => array()
                    );
                }

                $templateZones[$zone->getIdScanFormTemplate()]['zones'][] = sprintf("%s,%s,%s,%s,%s,%s", 
                    $zone->getPageNumber(),
                    $zoneDef['x'],
                    $zoneDef['y'],
                    $zoneDef['width'],
                    $zoneDef['height'],
                    $zoneDef['key']
                );               

                $templateZones[$zone->getIdScanFormTemplate()]['tokens'][] = array(
                    'key'                  => $zoneDef['key'],
                    'idType'               => $zoneDef['idType'],
                    'isCreationRequired'   => isset($zoneDef['isCreationRequired']) ? true : false
                ); 
            }
        }

        // Processing
        $query = $this->em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ScanFormFile', 'p')
            ->andWhere('p.status = '.Business\ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_PROCESSING);
        $scanFormFiles = $query->getQuery()->getResult();
        foreach ($scanFormFiles as $scanFormFile) {
            if (isset($templateZones[$scanFormFile->getIdScanFormTemplate()])) {
                $pdfFile = $scanFormFolder.'/files/'.$scanFormFile->getId().'.pdf';
                $zones = $templateZones[$scanFormFile->getIdScanFormTemplate()];
                $fileDir = $scanFormFolder.'/files_token/'.$scanFormFile->getId();
                @mkdir($fileDir);

                $jarFile = $container->getParameter('kernel.root_dir').'/../scripts/pdfImageExtractor/build/jar/pdfImageExtractor.jar';
                $cmd = sprintf("java -jar %s %s %s %s",
                    '"'.$jarFile.'"',
                    '"'.$pdfFile.'"',
                    '"'.$fileDir.'"',
                    '"'.implode(';', $zones['zones']).'"'
                );
                $out = $helper->executeCommand($cmd);       

                // Update entry tokens                
                $query = $this->em->createQueryBuilder();
                $query->delete('AppDatabaseMainBundle:ScanFormFileTokens', 'p')
                    ->andWhere('p.idScanFormFile = '.$scanFormFile->getId())
                    ->getQuery()
                    ->execute();
                foreach ($zones['tokens'] as $token) {
                    $fileToken = new Entity\ScanFormFileTokens();
                    $fileToken->setIdScanFormFile($scanFormFile->getId());
                    $fileToken->setTokenKey($token['key']);
                    $fileToken->setIdType($token['idType']);
                    $fileToken->setIsCreationRequired($token['isCreationRequired']);
                    $fileToken->setTokenImage('/file_tokens/'.$scanFormFile->getId().'/'.$token['key'].'.jpg');
                    $this->em->persist($fileToken);
                }

                $scanFormFile->setStatus(Business\ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_PENDING_FOR_FILLING_BASIC_DATA);
            }
            else {
                $scanFormFile->setStatus(Business\ScanFormFile\Constant::SCAN_FORM_FILE_STATUS_ERROR);
            }
            $this->em->flush();
        }
    }
}
