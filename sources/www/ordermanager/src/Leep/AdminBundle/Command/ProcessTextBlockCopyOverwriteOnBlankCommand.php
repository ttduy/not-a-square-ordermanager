<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ProcessTextBlockCopyOverwriteOnBlankCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:process_text_block_copy_overwrite_on_blank')
            ->setDescription('Process Text Block Copy ON BLANK');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $untranslatedPatterns = explode("\n", $config->getUntranslatedPattern());
        $arr = array();
        foreach ($untranslatedPatterns as $p) {
            if (trim($p) != '') {
                $arr[] = trim($p);
            }
        }
        $untranslatedPatterns = $arr;

        $em = $container->get('doctrine')->getEntityManager();  

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BackgroundJob', 'p')
            ->andWhere('p.idJobType = :idJobType')
            ->andWhere('(p.isProcessing = 0) OR (p.isProcessing IS NULL)')
            ->setParameter('idJobType', Business\BackgroundJob\Constants::BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ON_BLANK)
            ->setMaxResults(1);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $r->setIsProcessing(1);
            $em->flush();

            $data = @json_decode($r->getData(), true);
            $idSourceLanguage = $data['idSourceLanguage'];
            $idTargetLanguage = $data['idTargetLanguage'];

            // Load language
            $text = array();
            $targetLanguages = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($idTargetLanguage);
            foreach ($targetLanguages as $l) {
                $text[$l->getIdTextBlock()] = $l->getBody();
            }
            $sourceLanguages = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($idSourceLanguage);
            foreach ($sourceLanguages as $l) {
                $idTextBlock = $l->getIdTextBlock();

                $isOverwrited = false;
                if (!isset($text[$idTextBlock])) {
                    $isOverwrited = true;
                }
                else {
                    if (trim($text[$idTextBlock]) == '') {
                        $isOverwrited = true;
                    }
                    if (in_array(trim($text[$idTextBlock]), $untranslatedPatterns)) {
                        $isOverwrited = true;
                    }
                }

                if ($isOverwrited) {
                    $text[$idTextBlock] = $l->getBody();            
                }
            }

            // Delete target language
            echo "Clear target..\n";
            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('idLanguage', $idTargetLanguage)
                ->getQuery()
                ->execute();

            // Insert
            echo "Updating..\n";
            $i = 0;
            foreach ($text as $idTextBlock => $body) {
                $textBlockLanguage = new Entity\TextBlockLanguage();
                $textBlockLanguage->setIdLanguage($idTargetLanguage);
                $textBlockLanguage->setIdTextBlock($idTextBlock);
                $textBlockLanguage->setBody($body);
                $textBlockLanguage->setIsTranslated(1);
                $em->persist($textBlockLanguage);

                $i++;
                if ($i % 1000 == 0) {
                    $em->flush();
                    echo "  ".$i."..\n";
                }
            }
            $em->flush();
            echo "Done..\n";

            $r->setIsDone(1);
            $em->flush();
        }
    }
}
