<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ComputeLowStockCommand extends ContainerAwareCommand
{
    private $stock = array();

    protected function configure()
    {
        $this
            ->setName('leep_admin:compute_low_stock')
            ->setDescription('Compute Low Stock Material');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $this->stock = array();

        $result = $em->getRepository('AppDatabaseMainBundle:MaterialsPurchase')->findAll();
        foreach ($result as $purchase) {
            $materialId = $purchase->getMaterialId();
            $this->ensureStock($materialId);

            if ($purchase->getOrderedReceived() == 1) {
                $this->stock[$materialId] += $purchase->getQuantity();
            }
        }

        $result = $em->getRepository('AppDatabaseMainBundle:MaterialsUsed')->findAll();
        foreach ($result as $used) {
            $materialId = $used->getMaterialId();
            $this->ensureStock($materialId);

            $this->stock[$materialId] -= $used->getQuantity();
        }

        $materials = $em->getRepository('AppDatabaseMainBundle:Materials')->findAll();

        foreach ($materials as $material) {
            $this->ensureStock($material->getId());
            $threshold = intval($material->getWarningThreshold());
            if ($this->stock[$material->getId()] < $threshold) {
                $material->setStockStatus(1);
            } else {
                $material->setStockStatus(0);
            }

            $em->persist($material);
        }

        $em->flush();
    }

    private function ensureStock($materialId) {
        if (!isset($this->stock[$materialId])) {
            $this->stock[$materialId] = 0;
        }
    }
}