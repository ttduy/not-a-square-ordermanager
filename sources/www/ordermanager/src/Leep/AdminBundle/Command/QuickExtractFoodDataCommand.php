<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class QuickExtractFoodDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:quick_extract_food_data')
            ->setDescription('QuickExtractFoodDataCommand');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();  

        $v = array('Meat', 'Fish', 'Shellfish', 'Animal_products', 'Gluten', 'Nuts', 'Citrus_fruits', 'milk_protein', 'Egg_products', 'Corn');

        $foods = $em->getRepository('AppDatabaseMainBundle:FoodTableFoodItem', 'p')->findAll();
        foreach ($foods as $f) {
            $data = explode("\n", $f->getIngredientData());
            foreach ($data as $r) {
                $arr = explode('=', $r);
                if (count($arr) == 2) {
                    $ingredient[trim($arr[0])] = trim($arr[1]);
                }
            }

            $x = array();
            $x[] = $f->getFoodId();
            $x[] = $f->getName();
            foreach ($v as $i) {
                if (isset($ingredient[$i])) {
                    $x[] = $ingredient[$i];
                }
                else {
                    echo "!!!";
                }
            }

            echo implode(";", $x)."\n";
        }
    }
}
