<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: sourceId = '[file path]' | separator = '|' as default
php app/console leep_admin:importGene /var/www/ordermanager/database/Genes_20150826.csv
*/
class ImportGeneCommand extends ContainerAwareCommand
{
    public $resultGroups = array();

    const DEFAULT_RELEVANT = 0;
    const DEFAULT_RECHECK = 0;

    protected function configure()
    {
        $this
            ->setName('leep_admin:importGene')
            ->setDescription('Import Genes From CSV File')
            ->addArgument(
                'sourceFile',
                InputArgument::REQUIRED,
                'Where is the csv file'
            )
            ->addArgument(
                'separator',
                InputArgument::OPTIONAL,
                'what is separator, | as default'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $sourceFile = $input->getArgument('sourceFile');
        $separator = ($input->getArgument('separator') ? $input->getArgument('separator') : '|');

        $numberOfNewEntries = 0;
        $existingEntries = array();
        $totalDataRows = 0;
        $counter = 0;

        if (($inputHandle = fopen($sourceFile, "r")) !== FALSE) {
            $this->prepareResultGroups();

            while (($arrFields = fgetcsv($inputHandle, 0, $separator)) !== FALSE) {
                $totalDataRows++;

                $entryData = $this->parseData($arrFields);

                if (!$this->existInDB($entryData)) {
                    $numberOfNewEntries++;
                    $counter++;

                    $newGene = new Entity\Genes();

                    foreach ($entryData as $key => $value) {
                        $setMethod = 'set' . ucfirst($key);
                        $newGene->$setMethod($value);
                    }

                    $newGene->setRelevant(self::DEFAULT_RELEVANT);
                    $newGene->setRecheck(self::DEFAULT_RECHECK);

                    $em->persist($newGene);
                } else {
                    $existingEntries[] = $entryData;
                }

                if ($counter % 500 == 0) {
                    $em->flush();
                }
                echo $counter."\n";
            }

            $em->flush();
        }

        echo "Number of rows in CSV: " . $totalDataRows . "\n";
        echo "Number of new Entries: " . $numberOfNewEntries . "\n";
        echo "Existing Entries: " . sizeof($existingEntries) . "\n";

        if (sizeof($existingEntries) > 0) {
            print_r($existingEntries);
        }
    }

    protected function existInDB($entryData) {
        // check gene name
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $blnExist = false;

        $gene = $em->getRepository('AppDatabaseMainBundle:Genes')->findOneByGenename($entryData['genename']);
        if ($gene) {
            $blnExist = true;
        }

        return $blnExist;
    }

    protected function parseData($arr) {
        // Gene Name|Scientific Name|Rs Number|Result Option
        $arrTmp = array();
        foreach ($arr as $entry) {
            $arrTmp[] = trim(self::cleanDoubleQuotes($entry));
        }
        $arr = $arrTmp;
        $this->processData($arr);

        $data = array(
                                        'genename'              => $arr[0],
                                        'scientificname'        => $arr[1],
                                        'rsnumber'              => $arr[2],
                                        'groupid'               => $arr[3]
                                    );

        return $data;
    }

    protected function cleanDoubleQuotes($str) {
        if (substr($str, 0, 1) == '"') {
            $str = substr($str, 1);
        }

        if (substr($str, -1, 1) == '"') {
            $str = substr($str, -1);
        }

        return $str;
    }

    protected function prepareResultGroups() {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        $arrData = array();

        $result = $doctrine->getRepository('AppDatabaseMainBundle:ResultGroup')->findAll();
        if ($result) {
            foreach ($result as $entry) {
                $arrData[strtolower($entry->getName())] = $entry->getId();
            }
        }

        $this->resultGroups = $arrData;
    }

    // 0
    protected function processGenename(&$arr) {
        $result = null;
        $value = $arr[0];
        if ($value) {
            $result = $value;
        }
        $arr[0] = $result;
    }

    // 1
    protected function processScientificName(&$arr) {
        $result = null;
        $value = $arr[1];
        if ($value) {
            $result = $value;
        }
        $arr[1] = $result;
    }

    // 2
    protected function processRsNumber(&$arr) {
        $result = null;
        $value = $arr[2];
        if ($value) {
            $result = $value;
        }
        $arr[2] = $result;
    }

    // 3
    protected function processGroupId(&$arr) {
        $value = $arr[3];
        $result = $value;
        
        if ($value && array_key_exists(strtolower($value), $this->resultGroups)) {
            $result = $this->resultGroups[strtolower($value)];
        }
        else {
            if ($value) {
                echo "Result Group Id: ".strtolower($value)."\n";
                exit();
            }
        }
        $arr[3] = $result;
    }

    protected function processData(&$arr) {
        $this->processGeneName($arr);
        $this->processScientificName($arr);
        $this->processRsNumber($arr);
        $this->processGroupId($arr);
    }

}
