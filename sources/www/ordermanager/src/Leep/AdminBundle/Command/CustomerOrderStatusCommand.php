<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\Customer\Util;

class CustomerOrderStatusCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:customer_order_status')
            ->setDescription('Send Outgoing Email');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');

        $customerList = $em->getRepository('AppDatabaseMainBundle:Customer')->findAll();
        $productList = $em->getRepository('AppDatabaseMainBundle:ProductsDnaPlus')->findAll();

        $productNameList = [];
        $productIsNutrimeList = [];
        foreach ($productList as $product) {
            $productNameList[$product->getId()] = $product->getName();
            $productIsNutrimeList[$product->getId()] = $product->getIsNutriMe();
        }

        print "Threre are ".count($productNameList)." product.\n";
        $today = new \DateTime();

        $customerOrderInfo = array();
        foreach ($customerList as $customer) {
            $firstname = $customer->getFirstName();
            $surname = $customer->getSurName();
            $birthday = $customer->getDateOfBirth();
            $orderNumber = $customer->getOrderNumber();
            $email = $customer->getEmail();
            $telephone = $customer->getTelephone();

            $dateOrder = $customer->getDateOrdered();
            $age = 0;
            if ($dateOrder) {
                $todayTs = $today->getTimestamp();
                $age = intval(($todayTs - $dateOrder->getTimestamp()) / 86400);
            }

            $address = sprintf("%s, %s %s, %s",
                $customer->getStreet(),
                $customer->getCity(),
                $customer->getPostCode(),
                $formatter->format($customer->getCountryId(), 'mapping', 'LeepAdmin_Country_List'));

            if ($birthday) {
                $birthday = $birthday->format('d-m-Y');
            }

            $customerKey = "$firstname $surname ($birthday)";

            if (!array_key_exists($customerKey, $customerOrderInfo)) {
                $customerOrderInfo[$customerKey] = [
                    'name' =>           "$firstname $surname",
                    'orderNumber' =>    $orderNumber,
                    'birthday' =>       $birthday,
                    'email' =>          $email,
                    'telephone' =>      $telephone,
                    'address' =>        $address,
                    'products' =>       []
                ];
            }

            // Map Product Category List
            $productCategoryIdList = array_map('trim', explode(',', $customer->getCategoryList()));
            $productList = [];
            foreach ($productCategoryIdList as $id) {
                $categoryEntity = $container->get('doctrine')
                    ->getRepository("AppDatabaseMainBundle:ProductCategoriesDnaPlus")
                    ->findOneById(intval($id));

                if (!$categoryEntity)
                    continue;

                $categoryProducts = Util::getCategoryProductLink($container)[$categoryEntity->getId()];
                $productList = array_merge($productList, $categoryProducts);
            }

            // Map Product List (Filter all product that has existed in Product Category List)
            $productIdList = array_map('trim', explode(',', $customer->getProductList()));
            foreach ($productIdList as $id) {
                $id = intval($id);
                if ($id == 0) {
                    continue;
                }

                // Skip products that already exist
                if (in_array($id, $productList)) {
                    continue;
                }

                $productList[] = $id;
            }

            foreach ($productList as $productId) {
                if (!is_numeric($productId)) {
                    print "ERROR: Product #$productId Invalid\n";
                }

                $productId = intval($productId);
                if (array_key_exists($productId, $customerOrderInfo[$customerKey]['products'])) {
                    $customerOrderInfo[$customerKey]['products'][$productId]['numorder'] += 1;
                    if ($customerOrderInfo[$customerKey]['products'][$productId]['age'] > $age) {
                        $customerOrderInfo[$customerKey]['products'][$productId]['age'] = $age;
                    }
                } else {
                    if (!array_key_exists($productId, $productNameList)) {
                        print "Error: Product #$productId not exist (Customer #$customerKey).\n";
                        continue;
                    }

                    $customerOrderInfo[$customerKey]['products'][$productId] = array(
                        'name' =>       $productNameList[$productId],
                        'isNutriMe' =>  $productIsNutrimeList[$productId],
                        'numorder' =>   1,
                        'age' =>        $age
                    );
                }
            }
        }

        $title = [
            "\"No.\"",
            "\"Product ID.\"",
            "\"Customer\"",
            "\"Product\"",
            "\"Last Order Time\"",
            "\"Number of order\"",
            "\"Date of birth\"",
            "\"Email\"",
            "\"Telephone\"",
            "\"Address\"",
            "\"Order Number\"",
        ];

        $fileOutput = [implode(';', $title)];
        $no = 0;
        foreach ($customerOrderInfo as $key => $customer) {
            foreach ($customer['products'] as $productId => $product) {
                if (!$product['isNutriMe'])
                    continue;

                $output = [
                    '"'.$no.'"',
                    '"'.$productId.'"',
                    '"'.$customer['name'].'"',
                    '"'.$product['name'].'"',
                    '"'.$product['age'].'d"',
                    '"'.$product['numorder'].'"',
                    '"'.$customer['birthday'].'"',
                    '"'.$customer['email'].'"',
                    '"'.$customer['telephone'].'"',
                    '"'.$customer['address'].'"',
                    '"'.$customer['orderNumber'].'"'
                ];

                $fileOutput[] = implode(';', $output);
                $no++;
            }
        }

        unlink('/var/www/ordermanager/files/temp/customer_order_status.csv');
        file_put_contents('/var/www/ordermanager/files/temp/customer_order_status.csv',
            implode("\n", $fileOutput));

        echo "Finished.\n";
    }
}