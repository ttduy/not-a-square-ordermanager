<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class OverrideTextBlockLanguageCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:override_text_block_language')
            ->setDescription('Override Text Block Language')
            ->addArgument(
                'sourceId',
                InputArgument::REQUIRED,
                'What is ID of source language?'
            )
            ->addArgument(
                'targetId',
                InputArgument::REQUIRED,
                'What is ID of target language?'
            )
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();

        // get input sourceId
        $sourceId = $input->getArgument('sourceId');
        $targetId = $input->getArgument('targetId');

        $sourceLanguage = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($sourceId);
        $targetLanguage = $em->getRepository('AppDatabaseMainBundle:Language')->findOneById($targetId);
        echo "Copy from ".$sourceLanguage->getName().' --> '.$targetLanguage->getName()."\n";

        // Load source languages
        $sourceBlocks = array();
        $result = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($sourceLanguage->getId());
        foreach ($result as $r) {
            $sourceBlocks[$r->getIdTextBlock()] = $r->getBody();
        }
        echo "Load ".$sourceLanguage->getName().': DONE'."\n";

        // Clean up target
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->andWhere('p.idLanguage = :idLanguage')
            ->setParameter('idLanguage', $targetLanguage->getId())
            ->getQuery()
            ->execute();
        echo "Clean-up ".$targetLanguage->getName().': DONE'."\n";

        // Update
        $i = 0;
        foreach ($sourceBlocks as $idTextBlock => $body) {
            $b = new Entity\TextBlockLanguage();
            $b->setIdTextBlock($idTextBlock);
            $b->setIdLanguage($targetLanguage->getId());
            $b->setBody($body);
            $b->setIsCached(0);
            $em->persist($b);

            $i++;
            if ($i % 5000 == 0) {
                $em->flush();
            }
        }
        echo "Copy ".$targetLanguage->getName().': DONE'."\n";
        $em->flush();
    }
}
