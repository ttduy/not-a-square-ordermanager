<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;
use Leep\AdminBundle\Business\EmailOutgoing\Constant;
use Leep\AdminBundle\Business\EmailOutgoing\Util;
use Leep\AdminBundle\Business\EmailMassSender\Util as MassSenderUtil;

class CreateWebloginEmailCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:create_weblogin_email')
            ->setDescription('Create Weblogin email');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();
        $formatter = $container->get('leep_admin.helper.formatter');
        $mapping = $container->get('easy_mapping');
        $config = $container->get('leep_admin.helper.common')->getConfig();
        $formatter = $container->get('leep_admin.helper.formatter');
        $sentStatus = $config->getLastEmailWebloginOrderedStatus();
        if (empty($sentStatus)) {
            $sentStatus = [];
        } else {
            $sentStatus = json_decode($sentStatus);
        }

        $checkDate = new \DateTime('-1 week');
        $emailConfig = $config->getEmailWebLoginFilterConfig();
        if ($emailConfig && trim($emailConfig) != "") {
            $emailConfig = json_decode($emailConfig);
        } else {
            $emailConfig = [
                'partners' =>   [],
                'dcList' =>     []
            ];
        }

        if (!empty($sentStatus)) {
            $sentStatusList = $em->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBy(['id' => $sentStatus]);
            $newSentStatus = [];
            foreach ($sentStatusList as $status) {
                if ($status->getStatusdate() > $checkDate) {
                    $newSentStatus[] = $status->getId();
                }
            }

            $sentStatus = $newSentStatus;
        }

        // Get weblogin email setting
        echo "Get weblogin setting...\n";
        $webloginEmailSettings = $em->getRepository('AppDatabaseMainBundle:WebloginEmailSetting')->findAll();
        $webloginSettings = [];
        $requiredStatus = [];
        foreach ($webloginEmailSettings as $setting) {
            $requiredStatus[] = $setting->getIdStatus();
            $newSetting = [
                'id' =>         $setting->getIdEmailTemplate(),
                'method' =>     $setting->getIdEmailMethod(),
                'sendNow' =>    $setting->getIsSendNow(),
                'autoRetry' =>  $setting->getIsAutoRetry(),
            ];

            if (!array_key_exists($setting->getIdStatus(), $webloginSettings)) {
                $webloginSettings[$setting->getIdStatus()] = [$newSetting];
            } else {
                $webloginSettings[$setting->getIdStatus()][] = $newSetting;
            }
        }

        // Get all order status fit setting expression
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OrderedStatus', 'p')
            ->andWhere('p.status IN (:requiredStatus)')
            ->andWhere('p.statusdate > :checkDate')
            ->setParameter('checkDate', $checkDate)
            ->setParameter('requiredStatus', $requiredStatus);

        if (!empty($sentStatus)) {
            $query
                ->andWhere('p.id NOT IN (:sentStatus)')
                ->setParameter('sentStatus', $sentStatus);
        }

        $orderStatusList = $query->orderBy('p.statusdate', 'ASC')->getQuery()->getResult();

        echo "Processing ".count($orderStatusList)." record...\n";
        foreach ($orderStatusList as $orderStatus) {
            // Check customer setting
            $order = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($orderStatus->getCustomerid());
            if (!$order) {
                continue;
            }


            echo "Check #".$orderStatus->getId()."\n";
            $dcId = $order->getDistributionChannelId();
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());
            if ($customerInfo) {
                $newDcId = $customerInfo->getIdDistributionChannel();
                if (!empty($newDcId)) {
                    $dcId = $customerInfo->getIdDistributionChannel();
                }
            }

            $partnerId = null;
            $dc = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findOneById($dcId);
            if ($dc) {
                $partnerId = $dc->getPartnerId();
            }

            if (!in_array($dcId, $emailConfig->dcList) && !in_array($partnerId, $emailConfig->partners)) {
                echo "Order not in the selected DC or Partner. Skip!";
                continue;
            }

            echo "Begin\n";
            $sentStatus[] = $orderStatus->getId();
            $descriptions = "Order ID: {$order->getId()}\n";
            $descriptions .= "Order Number: {$order->getOrdernumber()}\n";
            $descriptions .= "Status date: ".$formatter->format($orderStatus->getStatusdate(), 'date')."\n";
            $descriptions .= "Distribution Channel Id: $dcId\n";
            $descriptions .= "Partner Id: $partnerId\n";

            // Create email from order and setting template
            $settingList = $webloginSettings[$orderStatus->getStatus()];
            foreach ($settingList as $setting) {
                $templateData = Business\EmailOutgoing\Util::getWebloginEmailDataFromTemplate($container, $orderStatus->getCustomerid(), $setting['id']);
                if (empty($templateData)) {
                    continue;
                }

                foreach ($templateData as $data) {
                    Business\EmailOutgoing\Util::createOutgoingEmail(
                        $container,
                        $data['emailAddress'],
                        $data,
                        Business\EmailOutgoing\Constant::EMAIL_QUEUE_AUTO_WEBLOGIN,
                        Business\EmailOutgoing\Constant::EMAIL_STATUS_OPEN,
                        $setting['method'],
                        $setting['sendNow'],
                        $setting['autoRetry'],
                        $descriptions
                    );
                }
            }
        }

        $config->setLastEmailWebloginOrderedStatus(json_encode($sentStatus));
        $em->persist($config);
        $em->flush();

        echo "Finished.\n";
    }
}
