<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CustomerDashboardUserGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:customer_dashboard_user_generate')
            ->setDescription('Customer Dashboard - User Generate');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();
        $helperCommon = $container->get('leep_admin.helper.common');

        // Customers
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerInfo', 'p')
            ->andWhere("(p.webLoginUsername IS NULL) OR (p.webLoginUsername = '')")
            ->orderBy('p.id', 'DESC');
        $customers = $query->getQuery()->getResult();
        foreach ($customers as $c) {
            $username = $helperCommon->generateUsername($c->getCustomerNumber());
            $password = $helperCommon->generateRandPasscode(8);

            $c->setWebLoginUsername($username);
            $c->setWebLoginPassword($password);
            $em->flush();

            echo sprintf("Generate Customer: %s -> %s %s \n", $c->getCustomerNumber(), $username, $password);
        }

        // Distribution Channels
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->andWhere("(p.webLoginUsername IS NULL) OR (p.webLoginUsername = '')");
        $distributionChannels = $query->getQuery()->getResult();
        foreach ($distributionChannels as $dc) {
            $username = $helperCommon->generateUsername($dc->getDistributionChannel());
            $password = $helperCommon->generateRandPasscode(8);

            $dc->setWebLoginUsername($username);
            $dc->setWebLoginPassword($password);
            $em->flush();

            echo sprintf("Generate DC: %s -> %s %s \n", $dc->getDistributionChannel(), $username, $password);
        }

        // Partner
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Partner', 'p')
            ->andWhere("(p.webLoginUsername IS NULL) OR (p.webLoginUsername = '')");
        $partners = $query->getQuery()->getResult();
        foreach ($partners as $partner) {
            $username = $helperCommon->generateUsername($partner->getPartner());
            $password = $helperCommon->generateRandPasscode(8);

            $partner->setWebLoginUsername($username);
            $partner->setWebLoginPassword($password);
            $em->flush();

            echo sprintf("Generate Partner: %s -> %s %s \n", $partner->getPartner(), $username, $password);
        }
    }
}
