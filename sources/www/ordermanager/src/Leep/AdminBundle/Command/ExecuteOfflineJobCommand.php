<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ExecuteOfflineJobCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:execute_offline_job')
            ->setDescription('Execute offline job');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        // Check total processing job
        $maxConcurrent = 5;
        $query = $em->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p')
            ->andWhere('p.status = :processingStatus')
            ->setParameter('processingStatus', Business\OfflineJob\Constants::OFFLINE_JOB_PROCESSING);
        $totalProcessing = $query->getQuery()->getSingleScalarResult();
        if ($totalProcessing > $maxConcurrent) {
            return;
        }

        // Get a pending jobs
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:OfflineJob', 'p')
            ->andWhere('p.status = :pendingStatus')
            ->setParameter('pendingStatus', Business\OfflineJob\Constants::OFFLINE_JOB_PENDING)
            ->orderBy('p.inputTime', 'ASC')
            ->setMaxResults(1);
        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            foreach ($results as $r) {
                $offlineJob = $r;
                break;
            }

            // Process offline job
            $offlineJob->setStatus(Business\OfflineJob\Constants::OFFLINE_JOB_PROCESSING);
            $em->persist($offlineJob);
            $em->flush();

            // Handle
            $outputResult = array();
            try {
                $handler = Business\OfflineJob\Constants::getHandler($offlineJob->getIdHandler(), $container);
                $outputResult = $handler->process($offlineJob);            
            }
            catch (\Exception $e) {
                $outputResult['exception'] = $e->getMessage();
            }

            // Update
            $offlineJob->setStatus(Business\OfflineJob\Constants::OFFLINE_JOB_FINISHED);
            $offlineJob->setOutputResult(json_encode($outputResult));
            $em->flush();
        }
    }
}
