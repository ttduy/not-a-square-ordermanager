<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixTranslationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:fix_translation')
            ->setDescription('Gene Result Migration');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $untranslatedPattern = array(
            "",
            "###",
            "-- No translation yet --",
            "????",
            "***MISSING TEXT***",
            "--- TRANSLATE ME ---",
            "***MISSING TEXT****",
            "--- TRANSLATE ME ---",
            "####"
        );

        $i = 0;
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->andwhere(
                $query->expr()->eq('p.isTranslated', ':empty')
            )
            ->orderBy('p.id', 'DESC')
            ->setParameter('empty', NULL)
            ->setMaxResults(50000);
        $blocks = $query->getQuery()->getResult();
        echo "Start processing...";
        foreach ($blocks as $block) {
            $isTranslated = true;

            if (in_array(($block->getBody()), $untranslatedPattern)) {
                $isTranslated = false;
            }

            $block->setIsTranslated($isTranslated);
            $i++;
            if ($i % 1000 == 0) {
                echo $i."..\n";
                $em->flush();
            }
        }

        $em->clear();

        echo "\n###############################\n";
        $i = 0;
        $query1 = $em->createQueryBuilder();
        $query1->select('q')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'q')
            ->andwhere($query1->expr()->andX(
                $query1->expr()->orX(
                    $query1->expr()->isNull('q.isTranslated'),
                    $query1->expr()->eq('q.isTranslated', ':notTrue')),
                $query1->expr()->notIn('q.body', ':untranslated')
            ))
            ->orderBy('q.id', 'DESC')
            ->setParameter('notTrue', false)
            ->setParameter('untranslated', $untranslatedPattern)
            ->setMaxResults(50000);
        $block2s = $query1->getQuery()->getResult();
        echo "Start processing...";
        foreach ($block2s as $block) {
            $block->setIsTranslated(true);
            $i++;
            if ($i % 1000 == 0) {
                echo $i."..\n";
                $em->flush();
            }
        }
        $em->flush();
    }
}
