<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:import_text_block_language "/var/xxxx" "en" "new group"
php app/console leep_admin:import_text_block_language "/var/xxxx" "en" '' 1
*/
class ImportTextBlockLanguageCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:import_text_block_language')
            ->setDescription('Import Text Block Language')
            ->addArgument(
                'sourcePath',
                InputArgument::REQUIRED,
                'Source Path'
            )
            ->addArgument(
                'locale',
                InputArgument::REQUIRED,
                'locale'
            )
            ->addArgument(
                'textBlockGroupName',
                InputArgument::OPTIONAL,
                'Name of Text Block Group'
            )
            ->addArgument(
                'idTextBlockGroup',
                InputArgument::OPTIONAL,
                'Id of Text Block Group'
            );
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {                
        $container = $this->getContainer();

        // get input sourcePath
        $sourcePath = $input->getArgument('sourcePath');
        $languageName = $input->getArgument('locale');
        $textBlockGroupName = $input->getArgument('textBlockGroupName');
        $idTextBlockGroup = $input->getArgument('idTextBlockGroup');

        $em = $container->get('doctrine')->getEntityManager();

        $language = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:Language')->findOneByName($languageName);

        if (!$sourcePath) {
            exit('Source Path is missing');
        }
        if (!$language) {
            exit('Language Name is missing');
        }


        // create Text Block Group if need
        if ($textBlockGroupName) {
            $textBlockGroup = new Entity\TextBlockGroup();
            $textBlockGroup->setName($textBlockGroupName);
            $em->persist($textBlockGroup);
            $em->flush();

            $idTextBlockGroup = $textBlockGroup->getId();
        }

        if (!$idTextBlockGroup) {
            exit('Either TextBlockGroup ID or Name must be specified');
        }

        // get file content
        $data = file_get_contents($sourcePath);

        $arrData = self::parseResult($data);

        $counter = array(
                        'new'       => 0,
                        'modified'    => 0
                    );
        foreach ($arrData as $keyword => $entry) {
            // ensure no redundant record in text block
            $textBlock = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlock')->findOneByName($keyword);

            if (!$textBlock) {
                $textBlock = new Entity\TextBlock();
                $textBlock->setName($keyword);
                $textBlock->setIdGroup($idTextBlockGroup);

                $em->persist($textBlock);
                $em->flush();
            }
            
            $filters = array(
                    'idTextBlock'   => $textBlock->getId(),
                    'idLanguage' => $language->getId()
                );

            $textBlockLanguage = $container->get('doctrine')->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findOneBy($filters);

            if (!$textBlockLanguage) {
                $textBlockLanguage = new Entity\TextBlockLanguage();
                $textBlockLanguage->setIdTextBlock($textBlock->getId());
                $textBlockLanguage->setIdLanguage($language->getId());
                $textBlockLanguage->setBody($entry);
                $counter['new']++;
            } else if ($textBlockLanguage->getBody() != $entry) {
                $textBlockLanguage->setBody($entry);
                $counter['modified']++;
            }

            $em->persist($textBlockLanguage);
            echo "Done: ".$keyword."\n";
        }
        $em->flush();

        echo sprintf('Text Block Group ID %s; New: %s; Modified: %s', $idTextBlockGroup, $counter['new'], $counter['modified']) . "\n";
    }

    public function parseResult($result) {
        $rowResult = array();

        $rows = explode("\n", $result);
        foreach ($rows as $row) {
            if (trim($row) != '') {
                list($key,$value) = explode('|', $row);
                $rowResult[$key] = $value;
            }
        }

        return $rowResult;
    }
}
