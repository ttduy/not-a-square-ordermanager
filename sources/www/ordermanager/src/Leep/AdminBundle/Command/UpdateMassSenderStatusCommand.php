<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business\EmailOutgoing\Constant;
use Leep\AdminBundle\Business\EmailMassSender\Util;

class UpdateMassSenderStatusCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('leep_admin:update_mass_sender_status')
            ->setDescription('Send Outgoing Email');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        echo "Running.\n";
        $massSenderList = $em->getRepository("AppDatabaseMainBundle:EmailMassSender")
                ->createQueryBuilder('p')
                ->getQuery()
                ->iterate();

        foreach ($massSenderList as $massSender) {
            $massSender = $massSender[0];
            Util::updateMassSenderStatus($em, $massSender);
            $em->detach($massSender);
        }

        echo "Finished.\n";
    }
}