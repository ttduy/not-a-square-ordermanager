<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:complaint_and_feedback_migration
*/
class ComplaintAndFeedbackMigrationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:complaint_and_feedback_migration')
            ->setDescription('Complaint and Feedback Migration');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $dbHelper = $container->get('leep_admin.helper.database');
        $em = $container->get('doctrine')->getEntityManager();

        $numOfComplaintFeedbacks = array();

        // clear CustomerInfoComplaint
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:CustomerInfoComplaint')->getQuery()->execute();

        // clear CustomerInfoFeedback
        $query = $em->createQueryBuilder();
        $query->delete('AppDatabaseMainBundle:CustomerInfoFeedback')->getQuery()->execute();

        // migrate all complaints
        echo "Migrate complaints\n";
        $query = $em->createQueryBuilder();
        $query->select('
                p.complaintStatus,
                p.idCustomerInfo,
                c.problemTimestamp,
                c.problemIdWebUser,
                c.problemComplaintant,
                c.problemDescription,
                c.solutionTimestamp,
                c.solutionIdWebUser,
                c.solutionDescription,
                c.status,
                c.sortOrder,
                c.idTopic
            ')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:CustomerComplaint', 'c', 'WITH', 'c.idCustomer = p.id')
            ->andWhere('p.idCustomerInfo IS NOT NULL AND p.idCustomerInfo != 0 AND c.id IS NOT NULL')
            ->addOrderBy('p.idCustomerInfo');

        $complaints = $query->getQuery()->getResult();
        if ($complaints && sizeof($complaints) > 0) {
            // check each Complaint
            foreach ($complaints as $complaint) {
                $newComplaint = new Entity\CustomerInfoComplaint();
                $newComplaint->setIdCustomerInfo($complaint['idCustomerInfo']);
                $newComplaint->setProblemTimestamp($complaint['problemTimestamp']);
                $newComplaint->setProblemIdWebUser($complaint['problemIdWebUser']);
                $newComplaint->setProblemComplaintant($complaint['problemComplaintant']);
                $newComplaint->setProblemDescription($complaint['problemDescription']);
                $newComplaint->setSolutionTimestamp($complaint['solutionTimestamp']);
                $newComplaint->setSolutionIdWebUser($complaint['solutionIdWebUser']);
                $newComplaint->setSolutionDescription($complaint['solutionDescription']);
                $newComplaint->setStatus($complaint['status']);
                $newComplaint->setSortOrder($complaint['sortOrder']);
                $newComplaint->setIdTopic($complaint['idTopic']);
                $em->persist($newComplaint);
                $em->flush();

                if (!array_key_exists($complaint['idCustomerInfo'], $numOfComplaintFeedbacks)) {
                    $numOfComplaintFeedbacks[$complaint['idCustomerInfo']] = array('complaintStatus' => 0, 'feedback' => 0);
                }

                $numOfComplaintFeedbacks[$complaint['idCustomerInfo']]['complaintStatus'] = $complaint['complaintStatus'];
            }
        }

        // migrate all feedbacks
        echo "Migrate feedbacks\n";
        $query = $em->createQueryBuilder();
        $query->select('
                p.idCustomerInfo,
                c.fbTimestamp,
                c.idWebUser,
                c.feedbacker,
                c.description,
                c.sortOrder
            ')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->leftJoin('AppDatabaseMainBundle:CustomerFeedback', 'c', 'WITH', 'c.idCustomer = p.id')
            ->andWhere('p.idCustomerInfo IS NOT NULL AND p.idCustomerInfo != 0 AND c.id IS NOT NULL')
            ->addOrderBy('p.idCustomerInfo');

        $feedbacks = $query->getQuery()->getResult();

        if ($feedbacks && sizeof($feedbacks) > 0) {
            // check each Feedback
            foreach ($feedbacks as $feedback) {
                $newFeedback = new Entity\CustomerInfoFeedback();
                $newFeedback->setIdCustomerInfo($feedback['idCustomerInfo']);
                $newFeedback->setFbTimestamp($feedback['fbTimestamp']);
                $newFeedback->setIdWebUser($feedback['idWebUser']);
                $newFeedback->setFeedbacker($feedback['feedbacker']);
                $newFeedback->setDescription($feedback['description']);
                $newFeedback->setSortOrder($feedback['sortOrder']);
                $em->persist($newFeedback);
                $em->flush();

                if (!array_key_exists($feedback['idCustomerInfo'], $numOfComplaintFeedbacks)) {
                    $numOfComplaintFeedbacks[$feedback['idCustomerInfo']] = array('complaintStatus' => 0, 'feedback' => 0);
                }

                $numOfComplaintFeedbacks[$feedback['idCustomerInfo']]['feedback']++;
            }
        }

        // update number of complaints and number of feedbacks on each customer info
        echo "Update customer info complaints\n";
        foreach($numOfComplaintFeedbacks as $idCustomerInfo => $stats) {
            $customerInfo = $doctrine->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($idCustomerInfo);

            if ($customerInfo) {
                $customerInfo->setComplaintStatus($stats['complaintStatus']);
                $customerInfo->setNumberFeedback($stats['feedback']);
                $em->persist($customerInfo);
            }
        }
        $em->flush();

    }
}
