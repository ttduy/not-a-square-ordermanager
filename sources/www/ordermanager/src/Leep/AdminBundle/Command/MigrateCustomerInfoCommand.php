<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class MigrateCustomerInfoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:migrate_customer_info')
            ->setDescription('Migrate Customer Info');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('(p.idCustomerInfo = 0) OR (p.idCustomerInfo IS NULL)')
            ->setMaxResults(1000);
        $results = $query->getQuery()->getResult();

        $customerInfos = array();
        $i = 0;
        foreach ($results as $c) {
            $customerInfo = new Entity\CustomerInfo();
            $customerInfo->setCustomerNumber($c->getOrderNumber());
            $customerInfo->setIdDistributionChannel(intval($c->getDistributionChannelId()));
            $customerInfo->setFirstName($c->getFirstName());
            $customerInfo->setSurName($c->getSurName());
            $customerInfo->setPostCode($c->getPostCode());
            $customerInfo->setIsDestroySample(0);
            $customerInfo->setIsFutureResearchParticipant(0);
            $customerInfo->setIsNotContacted(0);
            $customerInfo->setDetailInformation('');
            $customerInfo->setDateOfBirth($c->getDateOfBirth());
            $customerInfo->setGenderId($c->getGenderId());
            $customerInfo->setTitle($c->getTitle());
            $em->persist($customerInfo);
            $customerInfos[$c->getId()] = $customerInfo;

            $i++;
            if ($i % 100 == 0) { echo $i."\n"; }
        }
        $em->flush();

        // Update ID
        $i = 0;
        foreach ($results as $c) {
            if (isset($customerInfos[$c->getId()])) {
                $idCustomerInfo = $customerInfos[$c->getId()]->getId();

                $c->setIdCustomerInfo($idCustomerInfo);
                $i++;
                if ($i % 100 == 0) { echo $i."\n"; }
            }
        }
        $em->flush();
    }
}
