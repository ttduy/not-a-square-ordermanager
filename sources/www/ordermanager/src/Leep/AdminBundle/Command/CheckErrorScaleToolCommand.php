<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CheckErrorScaleToolCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:check_error_scale_tool')
            ->setDescription('Check error scale tool');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $reports = array();
        $results = $em->getRepository('AppDatabaseMainBundle:Report')->findAll();
        foreach ($results as $r) {
            $reports[$r->getId()] = $r->getName();
        }

        $wrongSections = array();
        $items = $em->getRepository('AppDatabaseMainBundle:ReportItem')->findByIdTextToolType(Business\Report\Constants::TEXT_TOOL_TYPE_WIDGET_SCALE_TOOL);
        foreach ($items as $item) {
            $arr = json_decode($item->getData(), true);
            if (isset($arr['scaleDefinition'])) {
                $list = array();

                $rows = explode("\n", $arr['scaleDefinition']);
                foreach ($rows as $r) {
                    $t = explode(';', $r);
                    if (count($t) == 3) {
                        $list[] = floatval($t[0]);
                    }
                }

                $wrongSequences = array();
                for ($i = 1; $i < count($list); $i++) {
                    if ($list[$i] < $list[$i-1]) {
                        $wrongSequences[] = $list[$i-1].' -> '.$list[$i];
                    }
                }

                if (!empty($wrongSequences)) {
                    if (!isset($wrongSections[$item->getIdReportSection()])) {
                        $wrongSections[$item->getIdReportSection()] = array(
                            'idReport' => $item->getIdReport(),
                            'wrongList' => array()
                        );
                    }

                    $wrongSections[$item->getIdReportSection()]['wrongList'][] = array(
                        'left' => isset($arr['leftLabel']) ? ($arr['leftLabel']) : '',
                        'right' => isset($arr['rightLabel']) ? ($arr['rightLabel']) : '',
                        'selectedValue' => isset($arr['selectedValue']) ? ($arr['selectedValue']) : '',
                        'sequences' => $wrongSequences
                    );
                }
            }
        }

        foreach ($wrongSections as $idSection => $wrongList) {
            if (isset($reports[$wrongList['idReport']])) {
                echo "-----------------------------------------------------------------------------------\n";
                echo "Report: ".(isset($reports[$wrongList['idReport']]) ? ($reports[$wrongList['idReport']]) : '---')."\n";
                echo "Section: https://ordermanager.novogenialab.com/report/edit/edit?id=".$idSection."\n";
                foreach ($wrongList['wrongList'] as $wrongEntry) {
                    echo " -> Tool: Left label = ".$wrongEntry['left'].', Right label = '.$wrongEntry['right'].', Selected value = '.$wrongEntry['selectedValue']."\n";
                    echo " -> Wrong sequences: ".implode(', ', $wrongEntry['sequences'])."\n";
                    echo "\n";
                }
                echo "\n";
            }
        }
    }
}
