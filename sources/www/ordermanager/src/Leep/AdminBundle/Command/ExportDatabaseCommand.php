<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN: 
php app/console leep_admin:export_database
*/
class ExportDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:export_database')
            ->setDescription('Export Database');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $dbCommandFormat = 'mysqldump --user=%s --password=%s %s > %s/%s.sql';

        $dbCommand = sprintf($dbCommandFormat, 
                        $container->getParameter('database_user'),
                        $container->getParameter('database_password'),
                        $container->getParameter('database_name'),
                        $container->getParameter('exported_database_dir'),
                        date('Ymd_His')
                    );
        
        // dump to a file in physical directory by exec mysqldump command line
        exec($dbCommand);
    }
}
