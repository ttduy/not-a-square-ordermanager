<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\S3\Model\AcpBuilder;

use Leep\AdminBundle\Business;

class VerifyRemoveBackupFilesCommand extends ContainerAwareCommand
{
    private $s3 = null;
    private $errors = [
        'invalid' => [],
        'others' => []
    ];

    private $validFiles = [];

    protected function configure() {
        $this
            ->setName('leep_admin:verify_remove_backup_files')
            ->setDescription('Verify and remove backup files, please backup the disk before doing this');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        echo "Begin\n";

        // Instantiate an S3 client
        $this->s3 = S3Client::factory([
            'credentials' => [
                'key' =>    'AKIAINK6TVPLDQ4R6YQA',
                'secret' => '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',
            ],

            'region' => 'us-east-1'
        ]);

        $iterator = $this->s3->getIterator('ListObjects', array(
            'Bucket' => 'ordermanager-disk-backup'
        ));

        foreach ($iterator as $object) {
            $this->verify($object['Key']);
        }

        // Log files
        echo "Output log files:\n";
        $logDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../files/backup_disk_logs';
        $logTime = (new \DateTime())->format('d_m_Y_H_i_s');
        file_put_contents("$logDir/invalid_logs_$logTime", implode("\n", $this->errors['invalid']));
        file_put_contents("$logDir/error_logs_$logTime", implode("\n", $this->errors['others']));
        file_put_contents("$logDir/valid_logs_$logTime", implode("\n", $this->validFiles));
        echo "$logDir/invalid_logs_$logTime\n";
        echo "$logDir/error_logs_$logTime\n";
        echo "$logDir/valid_logs_$logTime\n";
        echo "Finished.\n";
    }

    private function verify($key) {
        $args = explode('/', $key);
        if (count($args) < 2) {
            return;
        }

        $folderKey = trim($args[0]);
        $fileName = trim($args[1]);
        $localDir = '';
        switch($folderKey) {
            case 'customer_reports':
            case 'dead_customer_reports':
            case 'cryo_sample_reports':
                $localDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../web/report';
                break;

            case 'shipments':
            case 'dead_shipments':
                $localDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../files/shipment';
                break;
        }

        echo "Verifing $localDir >> $fileName >> $folderKey\n";
        if (!is_file("$localDir/$fileName")) {
            echo "Local file $localDir/$fileName not existed! Skip.";
            return;
        }

        $message = $this->verifyS3File($localDir, $fileName, $folderKey);
        if ($message == 'Valid!') {
            $this->validFiles[] = "$localDir >> $fileName >> $folderKey";
            unlink("$localDir/$fileName");
        } else if ($message == 'Not valid!') {
            $this->errors['invalid'][] = "$message ($localDir >> $fileName >> $folderKey)";
        } else {
            $this->errors['others'][] = "$message";
        }

        echo "$message\n";
    }

    private function verifyS3File($localDir, $fileName, $folderKey) {
        $tmpDir = $this->getContainer()->get('service_container')->getParameter('kernel.root_dir').'/../files/temp';
        try {
            $remoteFile = $this->s3->getObject(array(
                'Bucket' => 'ordermanager-disk-backup',
                'Key' =>    "$folderKey/$fileName",
                'SaveAs' => "$tmpDir/$fileName"
            ));

            if (!is_file("$tmpDir/$fileName")) {
                return "Unable to locate remote file $tmpDir/$fileName!";
            }

            $localFileMd5 = md5_file("$localDir/$fileName");
            $remoteFileMd5 = md5_file("$tmpDir/$fileName");
            if ($localFileMd5 != $remoteFileMd5) {
                unlink("$tmpDir/$fileName");
                return "Not valid!";
            }

            unlink("$tmpDir/$fileName");
            return "Valid!";
        } catch (\Exception $e) {
            return "Exception: {$e->getMessage()}.\n";
        }
    }
}
