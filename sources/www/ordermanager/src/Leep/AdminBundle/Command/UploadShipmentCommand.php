<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UploadShipmentCommand extends ContainerAwareCommand
{
    protected function configure() {
        $now = new \DateTime('now');

        $this
            ->setName('leep_admin:upload_shipment')
            ->setDescription('Upload shipment');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $container = $this->getContainer();
        $config = $container->get('leep_admin.helper.common')->getConfig();

        $em = $container->get('doctrine')->getEntityManager();

        $reportJobs = $em->getRepository('AppDatabaseMainBundle:ReportQueue')->findBy(array(
            'idJobType'        => Business\ReportQueue\Constant::JOB_TYPE_BUILD_SHIPMENT,
            'status'           => Business\ReportQueue\Constant::REPORT_QUEUE_STATUS_FINISHED,
            'ftpUploadStatus'  => Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_PENDING
        ));
        // Mark
        foreach ($reportJobs as $reportJob) {
            $reportJob->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_INPROGRESS);
        }
        $em->flush();

        // Upload
        $shipmentDir = $container->getParameter('kernel.root_dir').'/../files/shipment';
        $tempDir = $container->getParameter('kernel.root_dir').'/../files/temp';

        $ftpServer = $config->getBodFtpServer();
        $ftpUsername = $config->getBodFtpUsername();
        $ftpPassword = $config->getBodFtpPassword();
        $ftpReportFolder = '/';

        // Login
        $error = '';
        $connId = ftp_connect($ftpServer);
        if ($connId) {
            $loginResult = ftp_login($connId, $ftpUsername, $ftpPassword);
            if ($loginResult) {
                if (!ftp_pasv($connId, true)) {
                    $error = "Can't use passive-mode";
                }
            }
            else {
                $error = "Can't login to FTP server";
            }
        }
        else {
            $error = "Can't connect to FTP server";
        }

        if (!empty($error)) {
            foreach ($reportJobs as $reportJob) {
                try {
                    $input = unserialize($reportJob->getInput());
                    $idBodShipment = $input['idBodShipment'];
                    $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($idBodShipment);
                    if ($bodShipment) {
                        $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_UPLOAD_ERROR);
                        $em->persist($bodShipment);
                        $em->flush();
                    }
                } catch (\Exception $e) {}


                // Error log
                $reportJob->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_ERROR);
                $em->flush();

                $reportLog = new Entity\ReportQueueLog();
                $reportLog->setIdReportQueue($reportJob->getId());
                $reportLog->setLogTime(new \DateTime());
                $reportLog->setLogText($error);
                $em->persist($reportLog);
                $em->flush();
            }
            return false;
        }

        // Process job
        foreach ($reportJobs as $reportJob) {
            try {
                $input = unserialize($reportJob->getInput());
                $output = unserialize($reportJob->getOutput());
                $localFilename = $shipmentDir.'/'.$output['filename'];
                if (!is_file($localFilename)) {
                    $awsHelper = $container->get('leep_admin.helper.aws_backup_files_helper');
                    $result = $awsHelper->temporaryDownloadBackupFiles('shipment', $output['filename']);
                    if ($result) {
                        $localFilename = "$tempDir/".$output['filename'];
                        @chmod($localFilename, 0777);
                    }
                }

                $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($input['idBodShipment']);
                if ($bodShipment) {
                    $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_UPLOADING);
                    $em->persist($bodShipment);
                    $em->flush();
                }

                $customer = $em->getRepository('AppDatabaseMainBundle:Customer')->findOneById($reportJob->getIdCustomer());
                $shipmentId = Business\BodShipment\Utils::getShipmentId($container, $input['idBodShipment']);
                $remoteFilename = $customer->getOrderNumber().'_'.$shipmentId.'.zip';
                // Upload
                if (!ftp_put($connId, $ftpReportFolder.'/'.$remoteFilename, $localFilename, FTP_BINARY)) {
                    throw new \Exception("Can't upload to server");
                }
                @ftp_chmod($connId, 0777, $ftpReportFolder.'/'.$remoteFilename);

                // Log
                $reportJob->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_DONE);
                $em->flush();

                $reportLog = new Entity\ReportQueueLog();
                $reportLog->setIdReportQueue($reportJob->getId());
                $reportLog->setLogTime(new \DateTime());
                $reportLog->setLogText("FTP Upload: Done");
                $em->persist($reportLog);
                //shiment log
                $shipmentLog = new Entity\ShipmentLog();
                $shipmentLog->setIdCustomer($customer->getId());
                $shipmentLog->setIdBodShipment($shipmentId);
                $shipmentLog->setFileName($remoteFilename);
                $shipmentLog->setLogTime(new \Datetime());
                $shipmentLog->setLogText('FTP Upload: Done');
                $shipmentLog->setIdReportQueue($reportJob->getId());
                $shipmentLog->setReportQueueDescription($reportJob->getDescription());
                $shipmentLog->setReportQueueInput($reportJob->getInput());
                $shipmentLog->setOrderNumber($customer->getOrderNumber());
                $shipmentLog->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_DONE);
                $em->persist($shipmentLog);
                $em->flush();

                if ($bodShipment) {
                    $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_FINISHED);
                    $em->persist($bodShipment);
                    $em->flush();
                }
            }
            catch (\Exception $e) {
                // // Error log
                $reportJob->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_ERROR);
                $em->flush();

                $reportLog = new Entity\ReportQueueLog();
                $reportLog->setIdReportQueue($reportJob->getId());
                $reportLog->setLogTime(new \DateTime());
                $reportLog->setLogText($e->getMessage());
                $em->persist($reportLog);

                //Error shiment log
                $shipmentLog = new Entity\ShipmentLog();
                $shipmentLog->setIdReportQueue($reportJob->getId());
                $shipmentLog->setLogTime(new \Datetime());
                $shipmentLog->setLogText($e->getMessage());
                $shipmentLog->setFtpUploadStatus(Business\ReportQueue\Constant::FTP_UPLOAD_STATUS_ERROR);
                $em->persist($shipmentLog);
                $em->flush();

                if (isset($input['idBodShipment'])) {
                    $idBodShipment = $input['idBodShipment'];
                    $bodShipment = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findOneById($idBodShipment);
                    if ($bodShipment) {
                        $bodShipment->setStatus(Business\BodShipment\Constant::BOD_SHIPMENT_STATUS_UPLOAD_ERROR);
                        $em->persist($bodShipment);
                        $em->flush();
                    }
                }
            }
        }

        ftp_close($connId);
    }
}
