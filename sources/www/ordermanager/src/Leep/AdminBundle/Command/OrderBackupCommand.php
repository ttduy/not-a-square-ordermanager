<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:order_backup
*/
class OrderBackupCommand extends ContainerAwareCommand
{
    protected $container;
    protected $doctrine;
    protected $em;
    protected $commonHelper;
    protected $startDate = null;
    protected $endDate =  null;
    protected $msgTplFinished = "%s : DONE \n";
    protected $msgTplFailed = '%s : FAILED \n';
    protected $orderFolderPaths = array();
    protected $zipCommand = 'cd %s;zip -r %s %s';
    protected $tmpRoot;
    protected $tmpBackupJob;
    protected $backedUpOrders = array();

    protected function configure()
    {
        $this
            ->setName('leep_admin:order_backup')
            ->setDescription('Order Backup')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getContainer();
        $this->doctrine = $this->container->get('doctrine');
        $this->em = $this->doctrine->getEntityManager();

        $this->commonHelper = $this->container->get('leep_admin.helper.common');

        $orderBackupQuery = $this->em->createQueryBuilder();
        $orderBackup = $orderBackupQuery->select('p')
                                    ->from('AppDatabaseMainBundle:OrderBackup', 'p')
                                    ->andWhere('p.status = :status')
                                    ->setParameter('status', Business\OrderBackup\Constant::ORDER_BACKUP_STATUS_OPEN)
                                    ->orderBy('p.id', 'ASC')
                                    ->getQuery()
                                    ->getResult();


        if ($orderBackup) {
            $orderBackup = $orderBackup[0];

            // start processing
            $orderBackup->setStatus(Business\OrderBackup\Constant::ORDER_BACKUP_STATUS_PROCESSING);
            $this->em->flush();

            if ($this->buildOrders($orderBackup)) {
                if (!$this->generateArchiveFile($orderBackup)
                    ||  !$this->updateOrders($orderBackup)) {
                    // reset backupJob STATUS
                    $orderBackup->setStatus(Business\OrderBackup\Constant::ORDER_BACKUP_STATUS_OPEN);
                } else {
                    // update backupJob STATUS
                    $orderBackup->setStatus(Business\OrderBackup\Constant::ORDER_BACKUP_STATUS_DONE);
                    $orderBackup->setNumberOfOrders(sizeof($this->backedUpOrders));
                }

                $this->em->flush();
            } else {
                break;
            }
        }
    }

    protected function buildOrders($orderBackup) {
        $isOke = true;

        $orderQuery = $this->em->createQueryBuilder();
        $orders = $orderQuery->select('p')
                            ->from('AppDatabaseMainBundle:Customer', 'p')
                            ->andWhere('p.dateordered >= :startDate AND p.dateordered <= :endDate')
                            ->setParameter('startDate', $orderBackup->getStartDate()->format('Y-m-d'))
                            ->setParameter('endDate', $orderBackup->getEndDate()->format('Y-m-d'))
                            ->getQuery()
                            ->getResult();

        if ($orders) {
            $this->tmpRoot = $this->container->getParameter('temp_dir');
            $this->tmpBackupJob = $this->tmpRoot . '/backupJob_' . $orderBackup->getId();

            if ($this->createFolder(array($this->tmpBackupJob))) {
                foreach ($orders as $order) {
                    $tmpOrder = $this->tmpBackupJob . '/' . $order->getOrdernumber();
                    $this->orderFolderPaths = array(
                                                    'tmpOrder'         =>  $tmpOrder,
                                                    'tmpReports'       =>  $tmpOrder . '/reports',
                                                    'tmpShipments'     =>  $tmpOrder . '/shipments',
                                                    'tmpAttachments'   =>  $tmpOrder . '/attachments',
                                                    'tmpInvoices'      =>  $tmpOrder . '/invoices'
                                                );

                    if ($order->getOrdernumber()) {
                        if (
                                !$this->createFolder($this->orderFolderPaths)
                                || !$this->buildXML($order)
                                || !$this->copyReports($order)
                                || !$this->copyShipments($order)
                                || !$this->copyAttachments($order)
                                || !$this->copyInvoices($order)
                            ) {
                            break;
                            $this->createMsg('Backing up Order ' . $order->getOrdernumber(), false);
                            $isOke = true;
                        } else {
                            $this->backedUpOrders[] = $order->getId();

                            $this->createMsg('Backing up Order ' . $order->getOrdernumber());
                        }
                    }
                }
            } else {
                $this->createMsg('Creating temporary folder for backJob...', false);
            }
        }

        return $isOke;
    }

    protected function createMsg($msg, $isSuccess = true) {
        $tpl = $this->msgTplFinished;
        if (!$isSuccess) {
            $tpl = $this->msgTplFailed;
        }

        echo sprintf($tpl, $msg);
    }

    protected function createFolder($arrFolderPaths) {
        $blnOk = true;

        foreach ($arrFolderPaths as $folderPath) {
            if (file_exists($folderPath)) {
                $this->commonHelper->removeDirectory($folderPath);
            }

            $blnCreateFolder = false;
            if (mkdir($folderPath)) {
                $blnCreateFolder = true;
            } else {
                $blnOk = false;
                break;
            }

            $this->createMsg('Creating folder ' . $folderPath, $blnCreateFolder);
        }

        return $blnOk;
    }

    protected function buildXML($order) {
        $orderData = $this->commonHelper->copyEntityToArray($order);
        $finalizedData = array();

        $arrPrepareWhat = array(
            array(
                'name'          => 'CustomerInfo',
                'getFields'     => 'prepareXMLCustomerInfoFields'
            ),
            array(
                'name'          => 'OrderInfo',
                'getFields'     => 'prepareXMLOrderInfoFields'
            ),
            array(
                'name'          => 'Address',
                'getFields'     => 'prepareXMLAddressFields',
            ),
            array(
                'name'          => 'InvoiceAddress',
                'getFields'     => 'prepareXMLInvoiceAddressFields',
            ),
            array(
                'name'          => 'Setting',
                'getFields'     => 'prepareXMLSettingFields',
            ),
            array(
                'name'          => 'InvoiceSetting',
                'getFields'     => 'prepareXMLInvoiceSettingFields',
            ),
            array(
                'name'          => 'Status',
                'hook'          => array(
                                    'method'    => 'prepareXMLStatusHook',
                                    'args'      => $orderData
                                )
            ),
            array(
                'name'          => 'Product',
                'hook'          => array(
                                    'method'    => 'prepareXMLProductHook',
                                    'args'      => $orderData
                                )
            ),
            array(
                'name'          => 'Question',
                'hook'          => array(
                                    'method'    => 'prepareXMLQuestionsHook',
                                    'args'      => $order
                                )
            ),
            array(
                'name'          => 'AnalysisResult',
                'hook'          => array(
                                            'method' => 'prepareXMLAnalysisResultHook',
                                            'args'   => $orderData
                                    )
            ),
            array(
                'name'          => 'SubOrder',
                'hook'          => array(
                                            'method' => 'prepareXMLSubOrderHook',
                                            'args'   => $orderData
                                    )
            )
        );

        foreach($arrPrepareWhat as $settings) {
            $sectionName = $settings['name'];
            $getFieldMethod = array_key_exists('getFields', $settings) ? $settings['getFields'] : null;
            $hook = array_key_exists('hook', $settings) ? $settings['hook'] : null;

            $arrData = array();
            if ($getFieldMethod) {
                $arrData = array_merge($arrData, $this->getMappingValue($this->$getFieldMethod(), $orderData));
            }

            if ($hook) {
                $arrData = array_merge($arrData, call_user_func(array($this, $hook['method']), $hook['args']));
            }

            $finalizedData[$sectionName] = $arrData;
        }

        $xmlOrder = new \SimpleXMLElement("<?xml version=\"1.0\"?><order></order>");
        $this->commonHelper->arrayToXML($finalizedData, $xmlOrder);

        $xmlFilePath = $this->orderFolderPaths['tmpOrder'] . '/' . $order->getOrdernumber() . '.xml';

        $isSuccess = $xmlOrder->asXML($xmlFilePath);

        $this->createMsg('Creating XML ' . $xmlFilePath, $isSuccess);

        return $isSuccess;
    }

    protected function getMappingValue($arrMappingFields, $sourceData) {
        $arr = array();
        foreach ($arrMappingFields as $field) {
            $arr[$field] = array_key_exists($field, $sourceData) ? $sourceData[$field] : null;
        }
        return $arr;
    }

    protected function prepareXMLCustomerInfoFields() {
        return array(
                    'idCustomerInfo',
                );
    }

    protected function prepareXMLOrderInfoFields() {
        return array(
                    'distributionchannelid',
                    'OrderNumber',
                    'trackingCode',
                    'dnaSampleOrderNumberVario',
                    'orderInfoText',
                    'domain',
                    'dateordered',
                    'orderage',
                    'laboratoryDetails'
                );
    }

    protected function prepareXMLAddressFields() {
        return array(
                    'street',
                    'street2',
                    'city',
                    'countryid',
                    'email',
                    'telephone',
                    'fax',
                    'notes'
                );
    }

    protected function prepareXMLInvoiceAddressFields() {
        return array(
                    'invoiceAddressIsUsed',
                    'invoiceAddressCompanyName',
                    'invoiceAddressClientName',
                    'invoiceAddressStreet',
                    'invoiceAddressPostCode',
                    'invoiceAddressCity',
                    'invoiceAddressIdCountry',
                    'invoiceAddressTelephone',
                    'invoiceAddressFax'
                );
    }

    protected function prepareXMLSettingFields() {
        return array(
                    'languageid',
                    'companyinfo',
                    'laboratoryinfo',
                    'contactinfo',
                    'contactus',
                    'letterextratext',
                    'clinicianinfotext',
                    'text7',
                    'text8',
                    'text9',
                    'text10',
                    'text11',
                    'doctorsreporting',
                    'automaticreporting',
                    'noreporting',
                    'invoicingandpaymentinfo',
                    'idDncReportType',
                    'rebrandingnick',
                    'headerfilename',
                    'footerfilename',
                    'titlelogofilename',
                    'boxlogofilename'
                );
    }

    protected function prepareXMLInvoiceSettingFields() {
        return array(
                    'pricecategoryid',
                    'reportdeliveryemail',
                    'invoicedeliveryemail',
                    'invoicegoestoid',
                    'reportgoestoid',
                    'reportdeliveryid',
                    'idPreferredPayment',
                    'idNutrimeGoesTo',
                    'idMarginGoesTo',
                    'marginOverrideDc',
                    'marginOverridePartner',
                    'priceoverride',
                    'idAcquisiteur1',
                    'acquisiteurCommissionRate1',
                    'idAcquisiteur2',
                    'acquisiteurCommissionRate2',
                    'idAcquisiteur3',
                    'acquisiteurCommissionRate3'
                );
    }

    protected function prepareXMLStatusHook($orderData) {
        $result = $this->doctrine->getRepository('AppDatabaseMainBundle:OrderedStatus')->findBy(array('customerid' => $orderData['id']));

        $arrInfo = array();
        if ($result) {
            foreach ($result as $entry) {
                $arrInfo[] = $this->commonHelper->copyEntityToArray($entry);
            }
        }

        return $arrInfo;
    }

    protected function prepareXMLProductHook($orderData) {
        $arrInfo = array();

        // get ordered caretories
        $queryCategory = $this->em->createQueryBuilder();
        $result = $queryCategory->select('
                                            c.id AS idCategory,
                                            c.categoryname As categoryName
                                        ')
                                ->from('AppDatabaseMainBundle:OrderedCategory', 'p')
                                ->innerJoin('AppDatabaseMainBundle:ProductCategoriesDnaPlus', 'c', 'WITH', 'c.id = p.categoryid')
                                ->andWhere('p.customerid = :idOrder')
                                ->setParameter('idOrder', $orderData['id'])
                                ->getQuery()
                                ->getResult();

        
        if ($result) {
            $name = 'OrderedCategories';
            foreach ($result as $entry) {
                if (!array_key_exists($name, $arrInfo)) {
                    $arrInfo[$name] = array();
                }
                $arrInfo[$name][] = array(
                                            'id'        => $entry['idCategory'],
                                            'name'      => $entry['categoryName']
                                        );
            }
        }

        // get ordered products
        $queryProduct = $this->em->createQueryBuilder();
        $result = $queryProduct->select('
                                            pn.id AS idProduct,
                                            pn.name AS productName,
                                            sp.id AS idSpecialProduct,
                                            sp.name AS specialProductName
                                        ')
                                ->from('AppDatabaseMainBundle:OrderedProduct', 'p')
                                ->innerJoin('AppDatabaseMainBundle:ProductsDnaPlus', 'pn', 'WITH', 'pn.id = p.productid')
                                ->leftJoin('AppDatabaseMainBundle:SpecialProduct', 'sp', 'WITH', 'sp.id = p.specialproductid')
                                ->andWhere('p.customerid = :idOrder')
                                ->setParameter('idOrder', $orderData['id'])
                                ->getQuery()
                                ->getResult();

        if ($result) {
            $name = 'OrderedProducts';
            foreach ($result as $entry) {
                if (!array_key_exists($name, $arrInfo)) {
                    $arrInfo[$name] = array();
                }
                $arrInfo[$name][] = array(
                                            'idProduct'             => $entry['idProduct'],
                                            'productName'           => $entry['productName'],
                                            'idSpecialProduct'      => $entry['idSpecialProduct'],
                                            'specialProductName'    => $entry['specialProductName']
                                        );
            }
        }

        // get ordered special products
        $querySpecialProduct = $this->em->createQueryBuilder();
        $result = $querySpecialProduct->select('
                                            sp.id AS idSpecialProduct,
                                            sp.name AS specialProductName
                                        ')
                                    ->from('AppDatabaseMainBundle:OrderedSpecialProduct', 'p')
                                    ->innerJoin('AppDatabaseMainBundle:SpecialProduct', 'sp', 'WITH', 'sp.id = p.idSpecialProduct')
                                    ->andWhere('p.idCustomer = :idOrder')
                                    ->setParameter('idOrder', $orderData['id'])
                                    ->getQuery()
                                    ->getResult();

        if ($result) {
            $name = 'OrderedSpecialProducts';
            foreach ($result as $entry) {
                if (!array_key_exists($name, $arrInfo)) {
                    $arrInfo[$name] = array();
                }
                $arrInfo[$name][] = array(
                                            'idSpecialProduct'      => $entry['idSpecialProduct'],
                                            'specialProductName'    => $entry['specialProductName']
                                        );
            }
        }

        return $arrInfo;
    }

    protected function prepareXMLQuestionsHook($order) {
        return Business\Customer\Util::loadFormQuestions($this->container, $this->em, $order, false, 'decodeAnswerToReport');
    }

    protected function prepareXMLAnalysisResultHook($orderData) {
        $result = $this->doctrine->getRepository('AppDatabaseMainBundle:AnalysisResults')->findBy(array('customerid' => $orderData['id']));

        $arrInfo = array();
        if ($result) {
            foreach ($result as $entry) {
                $arrInfo[] = $this->commonHelper->copyEntityToArray($entry);
            }
        }

        return $arrInfo;
    }

    protected function prepareXMLSubOrderHook($orderData) {
        $query = $this->doctrine->getEntityManager()->createQueryBuilder();
        $result = $query->select('
                        p.id,
                        p.idOrder,
                        p.idProductGroup,
                        p.barcode,
                        p.currentStatus,
                        p.currentStatusDate,
                        pg.name AS productGroupName,
                        sos.status,
                        sos.statusDate,
                        sos.sortOrder
                    ')
                    ->from('AppDatabaseMainBundle:SubOrder', 'p')
                    ->innerJoin('AppDatabaseMainBundle:ProductGroup', 'pg', 'WITH', 'pg.id = p.idProductGroup')
                    ->leftJoin('AppDatabaseMainBundle:SubOrderStatus', 'sos', 'WITH', 'sos.idSubOrder = p.id')
                    ->andWhere('p.idOrder = :idOrder')
                    ->setParameter('idOrder', $orderData['id'])
                    ->getQuery()
                    ->getResult();

        $arrSubOrders = array();
        foreach ($result as $row) {
            if (!array_key_exists($row['id'], $arrSubOrders)) {
                $arrSubOrders[$row['id']] = array(
                                                    'idOrder'           => $row['idOrder'],
                                                    'idProductGroup'    => $row['idProductGroup'],
                                                    'barcode'           => $row['barcode'],
                                                    'groupName'         => $row['productGroupName'],
                                                    'currentStatus'     => $row['currentStatus'],
                                                    'currentStatusDate' => ($row['currentStatusDate']) ? $row['currentStatusDate']->format('Y-m-d') : null,
                                                    //'status'            => $row['status'] NOT USED ?
                                                    'statuses'          => array()
                                                );                
            }
            $arrSubOrders[$row['id']]['statuses'][] = array(
                    'status'        => $row['status'],
                    'statusDate'    => ($row['statusDate'] ? $row['statusDate']->format('Y-m-d') : null),
                    'sortOrder'     => $row['sortOrder']
                );
        }

        return $arrSubOrders;
    }

    protected function copyReports($order) {
        $blnOk = true;
        // file path
        $fileDir = $this->container->getParameter('kernel.root_dir').'/../web/report/';

        // destination
        $destination = $this->orderFolderPaths['tmpReports'] . '/';

        $reports = $this->em->getRepository('AppDatabaseMainBundle:CustomerReport')->findByIdCustomer($order->getId());

        $arrSrc = array();
        if ($reports) {
            foreach ($reports as $entry) {
                $arrMethods = array(
                                        'getFilename', 
                                        'getLowResolutionFilename',
                                        'getWebCoverFilename',
                                        'getWebCoverLowResolutionFilename',
                                        'getBodCoverFilename',
                                        'getBodCoverLowResolutionFilename',
                                        'getFoodTableExcelFilename'
                                    );

                foreach ($arrMethods as $eachMethod) {
                    if ($entry->$eachMethod()) {
                        $arrSrc[] = $entry->$eachMethod();
                    }
                }
            }
        }

        if (!empty($arrSrc)) {
            foreach ($arrSrc as $eachSrc) {
                $src = $fileDir . $eachSrc;
                $des = $destination . '/' . $eachSrc;

                if (!$this->commonHelper->xcopy($src, $des)) {
                    $blnOk = false;
                    break;
                }
            }
        }

        return $blnOk;
    }

    protected function copyShipments($order) {
        $blnOk = true;
        // package path
        $packageFileDir = $this->container->getParameter('kernel.root_dir').'/../scripts/pdfshipment/files/output';

        // file path
        $fileDir = $this->container->getParameter('kernel.root_dir').'/../files/shipment';

        // destination
        $destination = $this->orderFolderPaths['tmpShipments'];

        $bodShipments = $this->em->getRepository('AppDatabaseMainBundle:BodShipment')->findByIdCustomer($order->getId());

        $arrSrc = array();
        if ($bodShipments) {
            foreach ($bodShipments as $entry) {
                if ($entry->getPackageFile()) {
                    $arrSrc[] = array(
                                        'isPackageFile'  => true, 
                                        'name'           => $entry->getPackageFile()
                                    );
                }

                if ($entry->getHighResolutionFile()) {
                    $arrSrc[] = array(
                                        'isPackageFile'  => false, 
                                        'name'           => $entry->getHighResolutionFile()
                                    );
                }

                if ($entry->getLowResolutionFile()) {
                    $arrSrc[] = array(
                                        'isPackageFile'  => false, 
                                        'name'           => $entry->getLowResolutionFile()
                                    );
                }
            }
        }

        if (!empty($arrSrc)) {
            foreach ($arrSrc as $eachSrc) {
                $des = $destination . '/' . $eachSrc['name'];
                if ($eachSrc['isPackageFile']) {
                    $src = $packageFileDir;
                } else {
                    $src = $fileDir;
                }
                $src .=  '/' . $eachSrc['name'];

                echo $src . "\n" . $des . "\n";
                if (!$this->commonHelper->xcopy($src, $des)) {
                    $blnOk = false;
                    break;
                }
            }
        }

        return $blnOk;
    }

    protected function copyAttachments($order) {
        $key = $order->getAttachmentkey();
        $source = $this->container->getParameter('attachment_dir') . '/customers/' . $key;
        $destination = $this->orderFolderPaths['tmpAttachments'];

        return $this->commonHelper->xcopy($source, $destination);
    }

    protected function copyInvoices($order) {
        $blnOk = true;

        // file path
        $source = $this->container->getParameter('kernel.root_dir').'/../files/invoices';

        // destination
        $destination = $this->orderFolderPaths['tmpInvoices'];

        if ($order->getInvoiceFile()) {
            $source .= '/' . $order->getInvoiceFile();
            $destination .= '/' . $order->getInvoiceFile();

            $blnOk = $this->commonHelper->xcopy($source, $destination);
        }

        return $blnOk;
    }

    protected function generateArchiveFile($orderBackup) {
        $blnOk = false;
        $root = $this->tmpRoot;
        $tmpBackupJobDir = $this->tmpBackupJob;
        $desDir = $this->container->getParameter('kernel.root_dir').'/../files/orderBackup';
        $desFile = 'orderBackup_' . $orderBackup->getId() . '.zip';
        $desFilePath = $desDir . '/' . $desFile;

        if (!is_dir($desDir)) {
            mkdir($desDir);
        }

        if (is_file($desFilePath)) {
            unlink($desFilePath);
        }

        exec(sprintf($this->zipCommand, $root, $desFilePath, $tmpBackupJobDir), $o, $r);

        if ($r == 0) {
            $blnOk = true;

            $orderBackup->setFile($desFile);
            $this->em->persist($orderBackup);
            $this->em->flush();
        }

        $this->createMsg('Creating Archive ' . $desFilePath, $blnOk);

        return $blnOk;
    }

    protected function updateOrders($orderBackup) {
        if (!empty($this->backedUpOrders)) {
            $updateQuery = $this->em->createQueryBuilder();
            $updateQuery->update('AppDatabaseMainBundle:Customer', 'p')
                        ->andWhere('p.id IN (:orderIds)')
                        ->setParameter('orderIds', $this->backedUpOrders)
                        ->set('p.idOrderBackup', $orderBackup->getId())
                        ->getQuery()
                        ->execute();

            $this->createMsg('Updating idOrderBackup of Orders...', true);            
        }

        return true;
    }
}
