<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix')
            ->setDescription('Fix');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $dcReportDelivery = array();
        $conn = $em->getConnection();
        $statement = $conn->prepare("SELECT id, ReportDeliveryId FROM ordermanager_bkp.distribution_channels");
        $statement->execute();
        $dcs = $statement->fetchAll();
        foreach ($dcs as $dc) {
            $dcReportDelivery[] = $dc['id'].';'.$dc['ReportDeliveryId'];
        }    
        file_put_contents("/var/www/ordermanager/database/fix_dc_report_delivery.csv", implode("\n", $dcReportDelivery));


        $customerReportDelivery = array();
        $conn = $em->getConnection();
        $statement = $conn->prepare("SELECT id, ReportDeliveryId FROM ordermanager_bkp.customer");
        $statement->execute();
        $customers = $statement->fetchAll();
        foreach ($customers as $c) {
            $customerReportDelivery[] = $c['id'].';'.$c['ReportDeliveryId'];
        }    
        file_put_contents("/var/www/ordermanager/database/fix_customer_report_delivery.csv", implode("\n", $customerReportDelivery));

        

    }
}
