<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class EmailMigrationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:email_migration')
            ->setDescription('Migrate Email from Customer to CustomerInfo');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p.id, p.idCustomerInfo, p.email')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('p.idCustomerInfo IS NOT NULL AND p.idCustomerInfo <> 0')
            ->andWhere('p.email IS NOT NULL');
        $result = $query->getQuery()->getResult();

        $i = 1;
        $total = count($result);
        foreach ($result as $r) {
            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($r['idCustomerInfo']);

            if ($customerInfo) {
                $customerInfo->setEmail($r['email']);
                $em->persist($customerInfo);
                echo $i++."/".$total."\n";
                if ($i % 1000 == 0) {
                    $em->flush();
                }
            }
        }
        $em->flush();
    }
}
