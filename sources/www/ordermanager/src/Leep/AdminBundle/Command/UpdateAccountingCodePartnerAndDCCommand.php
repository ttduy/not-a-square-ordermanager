<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateAccountingCodePartnerAndDCCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:update_accounting_code_partner_dc')
            ->setDescription('Update Id Code Partner, Acquisiteurs And DC');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        print_r("start update Partner \n");
        $accountingCode = 230000000;
        $partners = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findAll();
        foreach ($partners as $key => $partner) {
            $partner->setAccountingCode($accountingCode);
            $em->persist($partner);
            print_r("=======================\n set code: ". $accountingCode."\n");
            $accountingCode++;
        }
        print_r("end update Partner \n");

        print_r("start update DC \n");
        $dcs = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findAll();
        foreach ($dcs as $key => $dc) {
            $dc->setAccountingCode($accountingCode);
            $em->persist($dc);
            print_r("=======================\n set code: ". $accountingCode."\n");
            $accountingCode++;
        }
        print_r("end update DC \n");

        print_r("start update Acquisiteurs \n");
        $acquisiteurs = $em->getRepository('AppDatabaseMainBundle:Acquisiteur', 'app_main')->findAll();
        foreach ($acquisiteurs as $key => $value) {
            $value->setAccountingCode($accountingCode);
            $em->persist($value);
            print_r("=======================\n set code: ". $accountingCode."\n");
            $accountingCode++;
        }
        print_r("end update Acquisiteurs \n");
        $em->flush();
    }
}
