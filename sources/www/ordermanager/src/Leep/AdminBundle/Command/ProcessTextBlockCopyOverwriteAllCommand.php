<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ProcessTextBlockCopyOverwriteAllCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:process_text_block_copy_overwrite_all')
            ->setDescription('Process Text Block Copy ALL');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();  

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:BackgroundJob', 'p')
            ->andWhere('p.idJobType = :idJobType')
            ->andWhere('(p.isProcessing = 0) OR (p.isProcessing IS NULL)')
            ->setParameter('idJobType', Business\BackgroundJob\Constants::BACKGROUND_JOB_TEXT_BLOCK_OVERWRITE_ALL)
            ->setMaxResults(1);
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $r->setIsProcessing(1);
            $em->flush();

            $data = @json_decode($r->getData(), true);
            $idSourceLanguage = $data['idSourceLanguage'];
            $idTargetLanguage = $data['idTargetLanguage'];

            // Delete target language
            $query = $em->createQueryBuilder();
            $query->delete('AppDatabaseMainBundle:TextBlockLanguage', 'p')
                ->andWhere('p.idLanguage = :idLanguage')
                ->setParameter('idLanguage', $idTargetLanguage)
                ->getQuery()
                ->execute();

            // Copy source language --> target language
            $i = 0;
            $sourceLanguages = $em->getRepository('AppDatabaseMainBundle:TextBlockLanguage')->findByIdLanguage($idSourceLanguage);
            foreach ($sourceLanguages as $sl) {
                $textBlockLanguage = new Entity\TextBlockLanguage();
                $textBlockLanguage->setIdLanguage($idTargetLanguage);
                $textBlockLanguage->setIdTextBlock($sl->getIdTextBlock());
                $textBlockLanguage->setBody($sl->getBody());
                $textBlockLanguage->setIsTranslated($sl->getIsTranslated());
                $em->persist($textBlockLanguage);

                $i++;
                if ($i % 1000 == 0) {
                    $em->flush();
                }
            }
            $em->flush();

            $r->setIsDone(1);
            $em->flush();
        }
    }
}
