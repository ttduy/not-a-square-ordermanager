<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class RecoverCommand extends ContainerAwareCommand
{
    public $recycleBinDir = '';
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:recover')
            ->setDescription('Recover');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $recycleBinDir = $container->getParameter('kernel.root_dir').'/../files/recycle_bin';
        $reportDir = $container->getParameter('kernel.root_dir').'/../web/report';

        // Recover report
        $files = scandir($recycleBinDir.'/report');
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                rename($recycleBinDir.'/report/'.$file, $reportDir.'/'.$file);
            }
        }
    }
}
