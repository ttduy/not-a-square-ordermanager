<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixDcReportSettingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_dc_report_setting')
            ->setDescription('Fix DC Report Setting');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Partners
        $partnerMap = array();
        $partners = $em->getRepository('AppDatabaseMainBundle:Partner')->findAll();
        foreach ($partners as $p) {
            $partnerMap[$p->getId()] = $p;
        }

        // DC 
        $distributionChannels = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        $distributionChannelMap = array();
        foreach ($distributionChannels as $dc) {
            // Is printed report sent to DC
            if (($dc->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CHANNEL) &&
                ($dc->getReportDeliveryId() == Business\DistributionChannel\Constant::REPORT_DELIVERY_PRINT ||
                 $dc->getReportDeliveryId() == Business\DistributionChannel\Constant::REPORT_DELIVERY_PRINT_AND_EMAIL)) {
                $dc->setIsPrintedReportSentToDc(1);
            }
            else {
                $dc->setIsPrintedReportSentToDc(0);
            }

            // Is printed report sent to Customer
            if ($dc->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CUSTOMER) {
                $dc->setIsPrintedReportSentToCustomer(1);
            }
            else {
                $dc->setIsPrintedReportSentToCustomer(0);
            }

            // Recall
            if (isset($partnerMap[$dc->getPartnerId()])) {
                $dc->setIdRecallGoesTo($partnerMap[$dc->getPartnerId()]->getIdRecallGoesTo());
            }
            else {
                $dc->setIdRecallGoesTo(0);
            }

            $distributionChannelMap[$dc->getId()] = $dc;
        }
        $em->flush();

        // Customers
        $i = 0;
        $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findAll();
        foreach ($customers as $customer) {
            // Is printed report sent to customer
            if (($customer->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CUSTOMER) &&
                ($customer->getReportDeliveryId() == Business\DistributionChannel\Constant::REPORT_DELIVERY_PRINT ||
                 $customer->getReportDeliveryId() == Business\DistributionChannel\Constant::REPORT_DELIVERY_PRINT_AND_EMAIL)) {
                $customer->setIsPrintedReportSentToCustomer(1);
            }
            else {
                $customer->setIsPrintedReportSentToCustomer(0);
            }

            // Is digital report sent to customer
            if ($customer->getReportGoesToId() == Business\DistributionChannel\Constant::REPORT_GOES_TO_CUSTOMER) {
                $customer->setIsDigitalReportGoesToCustomer(1);
            }
            else {
                $customer->setIsDigitalReportGoesToCustomer(0);
            }

            // Id recall goes to
            if (isset($distributionChannelMap[$customer->getDistributionChannelId()])) {
                $dc = $distributionChannelMap[$customer->getDistributionChannelId()];
                $customer->setIdRecallGoesTo($dc->getIdRecallGoesTo());
            }
            else {
                $customer->setIdRecallGoesTo(0);
            }

            // Flush
            $i++;
            if ($i % 1000 == 0) {
                echo $i."\n";
                $em->flush();
            }
        }
        $em->flush();


    }
}
