<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FindDCDuplicateCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:find_dc_duplicate')
            ->setDescription('Find DC duplicate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        $arrayName = [];
        $arrayDC = [];
        $dcNoPartner = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findBy(['partnerid' => 52], ['distributionchannel' => 'ASC']);
        foreach ($dcNoPartner as $key => $value) {
            if (in_array($value->getDistributionChannel(), $arrayName)){
                $arrayDC[] = $value;
            }
            $arrayName [] = $value->getDistributionChannel();
        }

        foreach ($arrayDC as $dc) {
            echo $dc->getId().";\t". 'https://ordermanager.novogenialab.com/distribution_channel/edit/edit?id='.$dc->getId()."\t;\t".$dc->getDistributionChannel()."\n";
        }

        $em->flush();
    }
}
