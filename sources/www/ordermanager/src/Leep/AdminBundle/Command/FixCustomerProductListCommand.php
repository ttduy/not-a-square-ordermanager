<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class FixCustomerProductListCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:fix_customer_product_list')
            ->setDescription('Fix customer product list');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $customers = array();
        $result = $em->getRepository('AppDatabaseMainBundle:OrderedProduct')->findAll();
        foreach ($result as $r) {
            $idCustomer = intval($r->getCustomerId());
            $idProduct = intval($r->getProductId());
            if (!isset($customers[$idCustomer])) {
                $customers[$idCustomer] = array(
                    'product'   => array(),
                    'category'  => array()
                );
            }
            $customers[$idCustomer]['product'][] = $idProduct;
        }

        $result = $em->getRepository('AppDatabaseMainBundle:OrderedCategory')->findAll();
        foreach ($result as $r) {
            $idCustomer = intval($r->getCustomerId());
            $idCategory = intval($r->getCategoryId());
            if (!isset($customers[$idCustomer])) {
                $customers[$idCustomer] = array(
                    'product'   => array(),
                    'category'  => array()
                );
            }
            $customers[$idCustomer]['category'][] = $idCategory;
        }

        $total = count($customers);
        $i = 1;
        foreach ($customers as $idCustomer => $data) {
            $productList = "'".implode(',', $data['product'])."'";
            $categoryList = "'".implode(',', $data['category'])."'";

            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:Customer', 'p');
            $query->set('p.productList', $productList)
                ->set('p.categoryList', $categoryList)
                ->andWhere('p.id = '.$idCustomer);
            $query->getQuery()->execute();

            echo "$i / $total \n";
            $i++;
        }
    }
}
