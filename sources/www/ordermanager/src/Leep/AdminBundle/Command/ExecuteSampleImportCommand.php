<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class ExecuteSampleImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:execute_sample_import')
            ->setDescription('Execute sample import');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $config = $container->get('leep_admin.helper.common')->getConfig();

        $noJob = intval($config->getSampleImportJobMinute());
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:SampleImportJob', 'p')
            ->orderBy('p.lastUpdateTimestamp', 'ASC')
            ->andWhere('(p.isSelected IS NULL) OR (p.isSelected = 0)')
            ->setMaxResults($noJob)
            ->setFirstResult(0);        
        $results = $query->getQuery()->getResult();

        $now = new \DateTime();
        
        // Select
        $jobs = array();
        foreach ($results as $job) {
            echo "Process: ".$job->getSampleId()."\n";
            $jobs[$job->getId()] = $job;
            $job->setIsSelected(1);
            $job->setLastUpdateTimestamp($now);
        }
        $em->flush();

        // Processing
        foreach ($jobs as $job) {
            try {
                $handler = Business\SampleImportJob\Constants::getTaskHandler($job->getIdTask(), $container);
                $idNextAction = $handler->process($job);
                if ($idNextAction === -1) {
                    unset($jobs[$job->getId()]);
                    $em->remove($job);
                }
                else {
                    $job->setIdNextAction($idNextAction);
                }
            }
            catch (\Exception $e) {
                $job->setStatus(Business\SampleImportJob\Constants::STATUS_ERROR);
                $job->setLastUpdate($e->getMessage());
                echo $e->getMessage()."\n";
            }
        }
        $em->flush();

        // Unselect
        $now = new \DateTime();
        foreach ($jobs as $job) {
            $job->setIsSelected(0);
            $job->setLastUpdateTimestamp($now);
        }
        $em->flush();        
    }
}
