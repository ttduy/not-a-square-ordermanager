<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class MigrateBillingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:migrate_billing')
            ->setDescription('Migrate billing data');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $this->fixReportDeliveryId($em);
        $this->fixAcquisiteurs($em);
    }

    protected function fixReportDeliveryId($em) {     
        echo "Fix Report Delivery.. ";   
        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:DistributionChannels', 'p')
            ->set('p.reportdeliveryid', 4)
            ->andWhere('p.reportdeliveryid = 2')
            ->getQuery()
            ->execute();

        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:Customer', 'p')
            ->set('p.reportdeliveryid', 4)
            ->andWhere('p.reportdeliveryid = 2')
            ->getQuery()
            ->execute();

        echo "DONE\n";
    }

    protected function fixAcquisiteurs($em) {
        echo "Fix Acquisieurs..\n";
        // Load DC Acquisieurs
        $dcAcquisiteurs = array();

        $dcList = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findAll();
        foreach ($dcList as $dc) {
            $dcAcquisiteurs[$dc->getId()] = array(
                'idAcquisiteur1'          => $dc->getAcquisiteurId1(),
                'acquisiteurCommission1'  => $dc->getCommission1(),
                'idAcquisiteur2'          => $dc->getAcquisiteurId2(),
                'acquisiteurCommission2'  => $dc->getCommission2(),
                'idAcquisiteur3'          => $dc->getAcquisiteurId3(),
                'acquisiteurCommission3'  => $dc->getCommission3()
            );
        }

        // Load customers
        $customers = array();
        $query = $em->createQueryBuilder();
        $query->select('p.id')
            ->from('AppDatabaseMainBundle:Customer', 'p');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $customers[] = $r['id'];
        }
        $total = count($customers);

        // Update customers
        $customerChunks = array_chunk($customers, 100);
        $current = 0;
        foreach ($customerChunks as $customers) {
            $current += count($customers);
            echo "Process ..".$current."/".$total."\n";

            $query = $em->createQueryBuilder();
            $query->select('p')
                ->from('AppDatabaseMainBundle:Customer', 'p')
                ->andWhere('p.id in (:chunks)')
                ->setParameter('chunks', $customers);
            $result = $query->getQuery()->getResult();

            foreach ($result as $customer) {
                if (isset($dcAcquisiteurs[$customer->getDistributionChannelId()])) {
                    $a = $dcAcquisiteurs[$customer->getDistributionChannelId()];

                    $customer->setIdAcquisiteur1($a['idAcquisiteur1']);
                    $customer->setAcquisiteurCommissionRate1($a['acquisiteurCommission1']);
                    $customer->setIdAcquisiteur2($a['idAcquisiteur2']);
                    $customer->setAcquisiteurCommissionRate2($a['acquisiteurCommission2']);
                    $customer->setIdAcquisiteur3($a['idAcquisiteur3']);
                    $customer->setAcquisiteurCommissionRate3($a['acquisiteurCommission3']);                
                }    
            }
            $em->flush();
            $em->clear();
        }
        echo "DONE\n";
    }
}
