<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CacheTextBlockCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:cache_text_block')
            ->setDescription('Cache text block');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        $cache = $container->get('text_block_cache');
        
        $blockIdArr = array();
        $blockIdArr[] = 0;
        $query = $em->createQueryBuilder();
        $query->select('p.id, tb.name, p.idLanguage, p.body')
            ->from('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->leftJoin('AppDatabaseMainBundle:TextBlock', 'tb', 'WITH', 'tb.id = p.idTextBlock')
            ->andWhere('(p.isCached IS NULL) OR (p.isCached = 0)')
            ->setMaxResults(5000);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $cache->setLanguage($r['idLanguage']);
            $cache->saveBlock($r['name'], $r['body']);
            $blockIdArr[]= $r['id'];
        }

        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:TextBlockLanguage', 'p')
            ->set('p.isCached', 1)
            ->andWhere('p.id IN (:blockIdArr)')
            ->setParameter('blockIdArr', $blockIdArr)
            ->getQuery()
            ->execute();
    }
}
