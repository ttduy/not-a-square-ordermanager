<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class MergeDCDuplicateCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:merge_dc_duplicate')
            ->setDescription('Find DC duplicate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        // filter DC duplicate all field
        $arrayName = [];
        $arrayTemp = [];
        $dcNoPartner = $em->getRepository('AppDatabaseMainBundle:DistributionChannels')->findBy(['partnerid' => 52], ['distributionchannel' => 'ASC']);
        foreach ($dcNoPartner as $key => $value) {
            $arrayTemp[$value->getDistributionChannel()][] = $value;
        }
        $arrayDC = [];

        $dcEntity = new Entity\DistributionChannels();
        $reflect = new \ReflectionClass($dcEntity);
        $properties = [];
        foreach ($reflect->getProperties() as $key => $value) {
            if ($key != 0)
                $properties [] = ucfirst($value->getName());
        }
        $count = 0;
        foreach ($arrayTemp as $key => $dcDuplicate) {
            foreach ($dcDuplicate as $key => $dc) {
                if (!isset($arrayDC[$dc->getDistributionChannel()])) {
                    $arrayDC[$dc->getDistributionChannel()] = [
                        'first' => $dc,
                        'duplicate' => [],
                        'etc'   => ''
                    ];
                }
                else {
                    $dcFirst = $arrayDC[$dc->getDistributionChannel()]['first'];
                    $flag = true;
                    foreach ($properties as $key => $pro) {
                        $getFunc = "get$pro";
                        if ($dcFirst->$getFunc() != $dcFirst->$getFunc()) {
                            $flag = false;
                            break;
                        }
                    }
                    if ($flag) {
                        $arrayDC[$dc->getDistributionChannel()]['duplicate'] [] = $dc;
                    }
                    else {
                        $arrayDC[$dc->getDistributionChannel()]['etc'] [] = $dc;
                    }
                    $count += count($arrayDC[$dc->getDistributionChannel()]['duplicate']);
                }
            }
        }
        print_r("update ". $count. " duplicate \n");
        print_r("*********************** end filter **************** \n");
        // end filter
        /// update all foreign key
        foreach ($arrayDC as $key => $dcAll) {
            $dcUpdate = $dcAll['first'];
            foreach ($dcAll['duplicate'] as $key => $dc) {
                /// customer
                $customers = $em->getRepository('AppDatabaseMainBundle:Customer')->findBy(['distributionchannelid' => $dc->getId()]);
                if ($customers){
                    foreach ($customers as $key => $customer) {
                        $customer->setDistributionChannelId($dcUpdate->getId());
                        echo('update customer '. $customer->getFirstName(). "\n");

                    }
                }
                /// customer info
                $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($customerInfo){
                    foreach ($customerInfo as $key => $cusInfo) {
                        $cusInfo->setIdDistributionChannel($dcUpdate->getId());
                        echo('update customer info '. $cusInfo->getFirstName(). "\n");
                    }
                }
                /// Billing
                $billingInvoiceDC = $em->getRepository('AppDatabaseMainBundle:BillingInvoiceDistributionChannel')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($billingInvoiceDC){
                    foreach ($billingInvoiceDC as $key => $billingInvoiceDCItem) {
                        $billingInvoiceDCItem->setIdDistributionChannel($dcUpdate->getId());
                        echo('update billing invoice DC '. $billingInvoiceDCItem->getName(). "\n");
                    }
                }
                $billingPaymentDC = $em->getRepository('AppDatabaseMainBundle:BillingPaymentDistributionChannel')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($billingPaymentDC){
                    foreach ($billingPaymentDC as $key => $billingPaymentDCItem) {
                        $billingPaymentDCItem->setIdDistributionChannel($dcUpdate->getId());
                        echo('update billing payment DC '. $billingPaymentDCItem->getName(). "\n");
                    }
                }
                //// Collective
                $collInvoices = $em->getRepository('AppDatabaseMainBundle:CollectiveInvoice')->findBy(['distributionchannelid' => $dc->getId()]);
                if ($collInvoices){
                    foreach ($collInvoices as $key => $collInvoice) {
                        $collInvoice->setDistributionChannelId($dcUpdate->getId());
                        echo('update collective invoice '. $collInvoice->getName(). "\n");

                    }
                }
                $collPayments = $em->getRepository('AppDatabaseMainBundle:CollectivePayment')->findBy(['distributionchannelid' => $dc->getId()]);
                if ($collPayments){
                    foreach ($collPayments as $key => $collPayment) {
                        $collPayment->setDistributionChannelId($dcUpdate->getId());
                        echo('update collective payment '. $collPayment->getName(). "\n");

                    }
                }
                ///// DC
                $dcComplaints = $em->getRepository('AppDatabaseMainBundle:DistributionChannelsComplaint')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcComplaints){
                    foreach ($dcComplaints as $key => $dcComplaint) {
                        $dcComplaint->setIdDistributionChannel($dcUpdate->getId());
                        echo('update DC complaint Problem '. $dcComplaint->getProblemComplaintant(). "\n");
                    }
                }
                $dcEmailTemplate = $em->getRepository('AppDatabaseMainBundle:DistributionChannelsEmailTemplate')->findBy(['distributionchannelid' => $dc->getId()]);
                if ($dcEmailTemplate){
                    foreach ($dcEmailTemplate as $key => $emailTemplateItem) {
                        $emailTemplateItem->setDistributionChannelId($dcUpdate->getId());
                        echo('update DC email template '. $emailTemplateItem->getEmailTemplateId(). "\n");
                    }
                }
                $dcAccountEntries = $em->getRepository('AppDatabaseMainBundle:DistributionChannelAccountEntry')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcAccountEntries){
                    foreach ($dcAccountEntries as $key => $dcAccountEntry) {
                        $dcAccountEntry->setIdDistributionChannel($dcUpdate->getId());
                        echo('update DC Account Entry '. $dcAccountEntry->getId(). "\n");
                    }
                }
                $dcFeedBacks = $em->getRepository('AppDatabaseMainBundle:DistributionChannelFeedback')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcFeedBacks){
                    foreach ($dcFeedBacks as $key => $dcFeedBack) {
                        $dcFeedBack->setIdDistributionChannel($dcUpdate->getId());
                        echo('update DC Feed Back '. $dcFeedBack->getId(). "\n");
                    }
                }

                $dcMarketingStatus = $em->getRepository('AppDatabaseMainBundle:DistributionChannelMarketingStatus')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcMarketingStatus){
                    foreach ($dcMarketingStatus as $key => $dcMarketingStatusItem) {
                        $dcMarketingStatusItem->setIdDistributionChannel($dcUpdate->getId());
                        echo('update DC Marketing Status '. $dcMarketingStatusItem->getId(). "\n");
                    }
                }

                $dcRatingHistory = $em->getRepository('AppDatabaseMainBundle:DistributionChannelRatingHistory')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcRatingHistory){
                    foreach ($dcRatingHistory as $key => $dcRatingHistoryItem) {
                        $dcRatingHistoryItem->setIdDistributionChannel($dcUpdate->getId());
                        echo('update DC Rating History '. $dcRatingHistoryItem->getId(). "\n");
                    }
                }
                ///// order pending
                $dcOrderPending = $em->getRepository('AppDatabaseMainBundle:OrderPending')->findBy(['idDistributionChannel' => $dc->getId()]);
                if ($dcOrderPending){
                    foreach ($dcOrderPending as $key => $dcOrderPendingItem) {
                        $dcOrderPendingItem->setIdDistributionChannel($dcUpdate->getId());
                        echo('update Order Pending '. $dcOrderPendingItem->getOrderInfo(). "\n");
                    }
                }
            }
        }
        /// end update
        $em->flush();
        //// delete duplicate
        foreach ($arrayDC as $key => $dcAll) {
            foreach ($dcAll['duplicate'] as $key => $dc) {
                $em->remove($dc);
            }
        }
        $em->flush();
    }
}
