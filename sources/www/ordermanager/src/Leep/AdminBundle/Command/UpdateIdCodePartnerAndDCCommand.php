<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateIdCodePartnerAndDCCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('leep_admin:update_id_code_partner_dc')
            ->setDescription('Update Id Code Partner And DC');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine')->getEntityManager();
        print_r("start update Partner \n");
        $arrayLetterOld = [];
        $partners = $em->getRepository('AppDatabaseMainBundle:Partner', 'app_main')->findAll();
        foreach ($partners as $key => $partner) {
            $randomletter = '';
            do {
                $charset = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
                $randomletter = substr(str_shuffle($charset), 0, 5);
            }
            while (in_array($randomletter, $arrayLetterOld));
            $arrayLetterOld [] = $randomletter;
            $partner->setIdCode($randomletter);
            $em->persist($partner);
            print_r("=======================\n set code: ". $randomletter."\n");
        }
        print_r("end update Partner \n");

        print_r("start update DC \n");
        $dcs = $em->getRepository('AppDatabaseMainBundle:DistributionChannels', 'app_main')->findAll();
        foreach ($dcs as $key => $dc) {
            $randomletter = '';
            do {
                $charset = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
                $randomletter = substr(str_shuffle($charset), 0, 5);
            }
            while (in_array($randomletter, $arrayLetterOld));
            $arrayLetterOld [] = $randomletter;
            $dc->setIdCode($randomletter);
            $em->persist($partner);
            print_r("=======================\n set code: ". $randomletter."\n");
        }
        print_r("end update DC \n");


        $em->flush();
    }
}
