<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

/* to RUN:
php app/console leep_admin:lock_customer_order
*/
class LockCustomerOrderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leep_admin:lock_customer_order')
            ->setDescription('Lock Customer Order')
        ;
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        $em = $container->get('doctrine')->getEntityManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('FIND_IN_SET', 'Leep\AdminBundle\Business\Customer\DoctrineExt\FindInSet');            
        
        $query = $em->createQueryBuilder();

        $today = new \DateTime();
        $query->select('p')
            ->from('AppDatabaseMainBundle:Customer', 'p')
            ->andWhere('p.lockDate = :today')
            ->setParameter('today', $today->format('Y-m-d'));                

        $orders = $query->getQuery()->getResult();
        foreach ($orders as $order) {
            $order->setIsLocked(1);
            $order->setLockDate(null);
            Business\Customer\Util::addNewStatus($container, $order->getId(), Business\Customer\Constant::ORDER_STATUS_DELETE_DETAIL_AFTER_ANALYSIS);

            $customerInfo = $em->getRepository('AppDatabaseMainBundle:CustomerInfo')->findOneById($order->getIdCustomerInfo());
            if ($customerInfo) {
                $customerInfo->setIsLocked(1);
                $customerInfo->setLockDate(null);
            }

            $em->flush();

            echo "Locked: ".$order->getOrderNumber()."\n";
        }
    }
}
