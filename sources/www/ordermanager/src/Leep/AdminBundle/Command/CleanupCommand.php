<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class CleanupCommand extends ContainerAwareCommand
{
    public $recycleBinDir = '';
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:cleanup')
            ->setDescription('Cleanup');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {                
        $container = $this->getContainer();
        $this->recycleBinDir = $container->getParameter('kernel.root_dir').'/../files/recycle_bin';

        // Cleanup 
        $this->cleanupReport();
    }

    public function cleanupReport() {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        // Check currently in-use customer reports
        $customerReports = array();
        $results = $em->getRepository('AppDatabaseMainBundle:CustomerReport')->findAll();
        foreach ($results as $r) {
            $customerReports[$r->getFileName()] = 1;
            $customerReports[$r->getLowResolutionFilename()] = 1;
            $customerReports[$r->getWebCoverFilename()] = 1;
            $customerReports[$r->getWebCoverLowResolutionFilename()] = 1;
            $customerReports[$r->getBodCoverFilename()] = 1;
            $customerReports[$r->getBodCoverLowResolutionFilename()] = 1;      
            $customerReports[$r->getFoodTableExcelFilename()] = 1;            
            $customerReports[$r->getXmlFilename()] = 1;        
        }

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportQueue', 'p')
            ->andWhere('p.idJobType IN (:types)')
            ->setParameter('types', array(
                Business\ReportQueue\Constant::JOB_TYPE_BUILD_DEMO_REPORT,
                Business\ReportQueue\Constant::JOB_TYPE_BUILD_CUSTOMER_REPORT
            ));
        $results = $query->getQuery()->getResult();
        foreach ($results as $r) {
            $output = unserialize($r->getOutput());
            if (isset($output['filename'])) { 
                $customerReports[$output['filename']] = 1; 
            }
            if (isset($output['filename_low_resolution'])) { 
                $customerReports[$output['filename_low_resolution']] = 1; 
            }
            if (isset($output['filename_web_cover'])) { 
                $customerReports[$output['filename_web_cover']] = 1; 
            }
            if (isset($output['filename_web_cover_low_resolution'])) { 
                $customerReports[$output['filename_web_cover_low_resolution']] = 1; 
            }
            if (isset($output['filename_bod_cover'])) { 
                $customerReports[$output['filename_bod_cover']] = 1; 
            }
            if (isset($output['filename_bod_cover_low_resolution'])) { 
                $customerReports[$output['filename_bod_cover_low_resolution']] = 1; 
            }
        }

        // Check currently in-use customer shipments
        $customerShipments = array();
        $results = $em->getRepository('AppDatabaseMainBundle:BodShipment')->findAll();
        foreach ($results as $r) {
            $customerShipments[$r->getHighResolutionFile()] = 1;
            $customerShipments[$r->getLowResolutionFile()] = 1;
        }


        // Cleanup report
        $reportDir = $container->getParameter('kernel.root_dir').'/../web/report';
        $files = scandir($reportDir);
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                if (!isset($customerReports[$file])) {
                    if (strpos($file, '.pdf') !== 0) {
                        $this->moveToRecycleBin('report', $reportDir, $file);
                    }
                }
            }
        }

        // Cleanup shipment
        $shipmentDir = $container->getParameter('kernel.root_dir').'/../files/shipment';
        $files = scandir($shipmentDir);
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                if (!isset($customerShipments[$file])) {
                    $this->moveToRecycleBin('shipment', $shipmentDir, $file);
                }
            }
        }
    }

    public function moveToRecycleBin($dir, $fileDir, $file) {
        rename($fileDir.'/'.$file, $this->recycleBinDir.'/'.$dir.'/'.$file);
    }
}
