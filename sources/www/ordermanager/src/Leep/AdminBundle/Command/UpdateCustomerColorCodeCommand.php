<?php
namespace Leep\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Database\MainBundle\Entity;
use Leep\AdminBundle\Business;

class UpdateCustomerColorCodeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('leep_admin:update_customer_color_code')
            ->setDescription('Update customer color code');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $container = $this->getContainer();
        $customerFilterBuilder = $container->get('leep_admin.customer.business.customer_filter_builder');
        $em = $container->get('doctrine')->getEntityManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:CustomerColorFilter', 'p')
            ->orderBy('p.priority', 'DESC');
        $customerColorFilters = $query->getQuery()->getResult();

        $query = $em->createQueryBuilder();
        $query->update('AppDatabaseMainBundle:Customer', 'p')
               ->set('p.colorCode', "''")
               ->getQuery()->execute();

        foreach ($customerColorFilters as $customerColorFilter) {
            $filterData = json_decode($customerColorFilter->getFilter(), true);
            unset($filterData['name']);
            unset($filterData['priority']);
            unset($filterData['colorCode']);
            $filterModel = new Business\Customer\FilterModel();
            foreach ($filterData as $k => $v) {
                $filterModel->$k = $v;
            }

            $query = $em->createQueryBuilder();
            $query->update('AppDatabaseMainBundle:Customer', 'p')
                ->set('p.colorCode', "'".$customerColorFilter->getColorCode()."'");
            $customerFilterBuilder->buildQuery($query, $filterModel);
            $query->getQuery()->execute();
        }
    }
}
