<?php
namespace Easy\ModuleBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ModuleCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (false === $container->hasDefinition('easy_module.manager')) {
            return;
        }
 
        $moduleManager = $container->getDefinition('easy_module.manager');
 
        foreach ($container->findTaggedServiceIds('easy_module') as $id => $attributes) {
            $moduleManager->addMethodCall('addModule', array($attributes[0]['package'], $attributes[0]['code'], new Reference($id)));
        }
    }
} 