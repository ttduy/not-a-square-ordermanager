<?php

namespace Easy\ModuleBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
 
use Easy\ModuleBundle\DependencyInjection\Compiler\ModuleCompilerPass;
 
class EasyModuleBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
 
        $container->addCompilerPass(new ModuleCompilerPass());
    }
}
