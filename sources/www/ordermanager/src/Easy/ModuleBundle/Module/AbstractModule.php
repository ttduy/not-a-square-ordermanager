<?php
namespace Easy\ModuleBundle\Module;

use Symfony\Component\HttpFoundation\Response;

abstract class AbstractModule {
    public $container;
    public $manager;
    public $name;
    public $package;

    public $features = array();
    public $templates = array();

    public $hooks = array();
    public $hookPoints = array();
    public $hookFeatureOptions = array();

    public function __construct($container) {
        $this->container = $container;
    }
    abstract public function load();

    ///////////////////////////////////////////////
    // x. 
    public function getFeatures() {
        return $this->features;
    }
    public function getPackage() {
        return $this->package;
    }
    public function getName() {
        return $this->name;
    }
    public function getUrl($featureId, $action, $parameters = array()) {
        $parameters['module'] = $this->getName();
        $parameters['feature'] = $featureId;
        $parameters['action'] = $action;
        return $this->container->get('router')->generate($this->package, $parameters);
    }
    public function getModuleManager() {
        return $this->manager;
    }

    ///////////////////////////////////////////////
    // x. 
    public function addFeature($id, $options) {
        $class = $options['class'];
        if (!isset($options['options'])) {
            $options['options'] = array();
        }

        if (is_string($class)) {
            $class = $this->container->get($class);
        }
        $class->setModule($this);
        $class->setName($id);

        $featureOptions = $class->getDefaultOptions();
        foreach ($options['options'] as $k => $v) {
            $featureOptions[$k] = $v;
        }

        $this->features[$id] = array(
            'class'    => $class,
            'options'  => $featureOptions
        );
    }
    public function setFeatureOptions($id, $options) {
        foreach ($options as $k => $v) {
            $this->features[$id]['options'][$k] = $v;
        }
    }

    ///////////////////////////////////////////////
    // x.
    public function addHook($id, $options) {
        $class = $options['class'];
        $point = $options['point'];
        if (!isset($options['options'])) {
            $options['options'] = array();
        }

        if (is_string($class)) {
            $class = $this->container->get($class);
        }
        $class->setModule($this);

        $this->hooks[$id] = array(
            'class'      => $class,
            'options'    => $options['options'],
            'features'   => $options['features']
        );
        $this->hookFeatureOptions[$id] = array();
        if (!isset($this->hookPoints[$point])) {
            $this->hookPoints[$point] = array();
        }
        $this->hookPoints[$point][] = $id;        
    }

    public function setHookOptions($featureId, $hookId, $options) {
        if (!isset($this->hookFeatureOptions[$hookId][$featureId])) {
            $this->hookFeatureOptions[$hookId][$featureId] = array();
        }
        foreach ($options as $k => $v) {
            $this->hookFeatureOptions[$hookId][$featureId][$k] = $v;
        }
    }

    public function removeHook($hookId) {
        unset($this->hooks[$hookId]);
        foreach ($this->hookPoints as $point => $hookArr) {
            if (in_array($hookId, $hookArr)) {
                foreach ($hookArr as $k => $v) {
                    if ($v == $hookId) {
                        unset($this->hookPoints[$point][$k]);
                    }
                }
            }
        }
    }

    ///////////////////////////////////////////////
    // x.
    public function setTemplate($featureId, $template) {
        $this->templates[$featureId] = $template;
    }

    ///////////////////////////////////////////////
    // x.
    public function isHookAccept($hookFeaturePattern, $featureId) {
        if ($hookFeaturePattern === '*') {
            return true;
        }
        if (strpos($hookFeaturePattern, $featureId) !== FALSE) {
            return true;
        }
        return false;
    }

    public function processHook($featureId, $hookPoint, $controller, &$data) {
        if (isset($this->hookPoints[$hookPoint])) {
            foreach ($this->hookPoints[$hookPoint] as $hookId) {
                if ($this->isHookAccept($this->hooks[$hookId]['features'], $featureId)) {
                    $options = $this->hooks[$hookId]['options'];
                    if (isset($this->hookFeatureOptions[$hookId])) {
                        if (isset($this->hookFeatureOptions[$hookId]['*'])) {                            
                            foreach ($this->hookFeatureOptions[$hookId]['*'] as $k => $v) {
                                $options[$k] = $v;
                            }    
                        }
                        if (isset($this->hookFeatureOptions[$hookId][$featureId])) {
                            foreach ($this->hookFeatureOptions[$hookId][$featureId] as $k => $v) {
                                $options[$k] = $v;
                            }
                        }
                    }
                    $this->hooks[$hookId]['class']->handle($controller, $data, $options);
                }
            }
        }
    }

    public function processFeature($feature, $action, $controller, &$data) {
        $method = 'handle'.ucfirst($action);
        return $feature['class']->$method($controller, $data, $feature['options']);        
    }

    public function handle($controller, $featureId, $action) {
        $data = array();
        $feature = $this->features[$featureId];

        $this->processHook($featureId, 'hook_before', $controller, $data);
        $resp = $this->processFeature($feature, $action, $controller, $data);
        $this->processHook($featureId, 'hook_after', $controller, $data);                

        if ($resp instanceof Response) {
            return $resp;
        }
        $template = $this->templates[$featureId.'.'.$action];

        $resp = $controller->render($template, $data);          

        return $resp;
    }

    public function getPaths() {
        $features = $this->getFeatures();
        $paths = array();
        foreach ($features as $feature) {
            $featurePaths = $feature['class']->getPaths();
            foreach ($featurePaths as $name => $path) {
                $paths[$name] = $path;
            }
        }
        return $paths;
    }

}