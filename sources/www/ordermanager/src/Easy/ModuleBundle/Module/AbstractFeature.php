<?php
namespace Easy\ModuleBundle\Module;

abstract class AbstractFeature {
    public $module;
    public $name;

    public function setModule($module) {
        $this->module = $module;
    }
    public function getModule() {
        return $this->module;
    }

    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }

    public function getDefaultOptions() {
        return array();
    }

	abstract public function getActions();
    public function accept($action) {
        return in_array($action, $this->getActions());
    }
}