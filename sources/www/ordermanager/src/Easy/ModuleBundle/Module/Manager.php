<?php
namespace Easy\ModuleBundle\Module;

class Manager {
    protected $container;
    protected $modules = array();

    public function __construct($container) {
        $this->container = $container;
    }

    public function addModule($package, $id, $module) {
        $module->manager = $this;
        $module->name = $id;
        $module->package = $package;
        $module->load();

        if (!isset($this->modules[$package])) {
            $this->modules[$package] = array();
        }
        $this->modules[$package][$id] = $module;
    }

    public function getModule($package, $id) {
        return $this->modules[$package][$id];
    }
    public function getModules($package) {
        return $this->modules[$package];
    }

    public function getUrl($package, $moduleId, $featureId, $action, $parameters = array()) {
        return $this->getModule($package, $moduleId)->getUrl($featureId, $action, $parameters);
    }
}