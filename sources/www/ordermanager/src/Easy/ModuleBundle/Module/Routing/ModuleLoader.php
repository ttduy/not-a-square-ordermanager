<?php
namespace Easy\ModuleBundle\Module\Routing;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ModuleLoader implements LoaderInterface
{
    private $container;
    private $kernel;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->kernel = $container->get('kernel');
    }

    public function load($resource, $type = null)
    {
        $resourceFile = $this->kernel->locateResource($resource);
        $config = Yaml::parse($resourceFile);
        $prefix = $config['module']['prefix'];
        $package = $config['module']['package'];
        $controller = $config['module']['controller'];

        $manager = $this->container->get('easy_module.manager');

        $routes = new RouteCollection();
        $defaults = array(
            '_controller' => $controller.':handle',
            'action' => 'default'
        );
        $requirements = array();

        $route = new Route($prefix.'{module}/{feature}/{action}', $defaults, $requirements);
        $routes->add($package, $route);

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'module' === $type;
    }

    public function getResolver()
    {
    }

    public function setResolver(LoaderResolverInterface $resolver)
    {
    }
}