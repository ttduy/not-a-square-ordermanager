<?php
namespace Easy\ModuleBundle\Module;

abstract class AbstractHook {
    public $module;

    public function setModule($module) {
        $this->module = $module;
    }
    public function getModule() {
        return $this->module;
    }

    abstract function handle($controller, &$data, $options = array());
}
