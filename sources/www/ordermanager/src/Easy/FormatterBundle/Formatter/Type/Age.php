<?php
namespace Easy\FormatterBundle\Formatter\Type;

use Easy\FormatterBundle\Formatter\FormatterInterface;

class Age implements FormatterInterface {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function format($field, $entity, $config) {        
        if (is_array($entity)) {
            $data = $entity[$field];
        }
        else {
            $getMethod = 'get'.ucfirst($field);
            $data = $entity->$getMethod();
        }

        if ($data == null) return '';

        $today = new \DateTime();
        $diff = $today->getTimestamp() - $data->getTimestamp();
        return intval($diff / 86400).' day(s)';
    }
}
