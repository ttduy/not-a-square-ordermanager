<?php
namespace Easy\FormatterBundle\Formatter\Type;

use Easy\FormatterBundle\Formatter\FormatterInterface;

class Date implements FormatterInterface {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function format($field, $entity, $config) {        
        if (is_array($entity)) {
            $data = $entity[$field];
        }
        else {
            $getMethod = 'get'.ucfirst($field);
            $data = $entity->$getMethod();
        }

        if ($data == null) return '';

        //$format = $this->container->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');
        $format = 'd/m/Y';

        return $data->format($format);
    }
}
