<?php

namespace Easy\CrudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CrudController extends Controller
{
	protected $mgr;

	public function define() {
		$this->mgr->set('title',     '');
		$this->mgr->set('list.page', 'EasyCrudBundle:Crud:list.html.twig');
	}

	public function __construct() {
		$this->mgr = new \Easy\CrudBundle\Utils\Registry($this->container);
		$this->define();
	}

	public function getManager() {
		return $this->mgr;
	}

	public function listAction() {
		$data = array();

		if ($this->mgr->has('list.pre_process')) {
			$preProcess = $this->mgr->get('list.pre_process');
			if (is_callable($preProcess)) {
				call_user_func_array($preProcess, array($this, &$data));
			}
		}

		$data['pageTitle'] = $this->mgr->get('title');
		$data['grid'] = $this->mgr->get('list.grid')->getGridView($this);
		$data['grid']['title']      = 'Title';
		$data['grid']['ajaxSource'] = '#';

		if ($this->mgr->has('list.post_process')) {
			$postProcess = $this->mgr->get('list.post_process');
			if (is_callable($postProcess)) {
				call_user_func_array($postProcess, array($this, &$data));
			}
		}
    	return $this->render($this->mgr->get('list.page'), $data);
	}
}
