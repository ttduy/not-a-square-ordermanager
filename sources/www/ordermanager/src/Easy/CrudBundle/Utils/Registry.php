<?php
namespace Easy\CrudBundle\Utils;

class Registry {
	public $container;
	protected $mapping = array();

	public function __construct($container) {
		$this->container = $container;
		$this->mapping = array();
	}

	public function set($id, $data) {
		$this->mapping[$id] = $data;
	}

	public function setInstanceByService($id, $serviceId) {
		$this->mapping[$id] = $this->container->get($serviceId);
	}

	public function get($id) {
		return $this->mapping[$id];
	}

	public function has($id) {
		return isset($this->mapping[$id]);
	}
}