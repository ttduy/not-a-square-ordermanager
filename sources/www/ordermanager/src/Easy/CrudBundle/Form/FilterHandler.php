<?php
namespace Easy\CrudBundle\Form;

abstract class FilterHandler extends BaseHandler {
    protected $sessionId;
    public function __construct($container, $sessionId) {
        parent::__construct($container);
        $this->sessionId = $sessionId;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    abstract public function getDefaultFormModel();
    abstract function buildForm($builder);

    protected function setFilterData() {
        $filterModel = $this->getForm()->getData();
        $session = $this->container->get('request')->getSession();
        if (!is_object($filterModel)) {
            $filterModel = $this->getDefaultFormModel();
        }
        $session->set($this->sessionId, $filterModel);
    }

    public function onSuccess() {
        $this->setFilterData();
    }

    public function getCurrentFilter() {
        $session = $this->container->get('request')->getSession();

        if ($session->has($this->sessionId)) {
            $filterModel = $session->get($this->sessionId);
            if (!is_object($filterModel)) {
                $filterModel = $this->getDefaultFormModel();
            }
            return $filterModel;
        } else {
            $filterModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $filterModel);
            return $filterModel;
        }
    }

    public function prepareFormModel($defaultFormModel) {return $defaultFormModel;}
    public function execute() {
        $request = $this->container->get('request');
        $isClearAll = $request->get('__clear_all', 0);

        if ($isClearAll) {
            $session = $request->getSession();
            $defaultFormModel = $this->getDefaultFormModel();
            $session->set($this->sessionId, $defaultFormModel);
        }
        else {
            $defaultFormModel = $this->getCurrentFilter();
        }
        $defaultFormModel = $this->prepareFormModel($defaultFormModel);
        $this->builder = $this->container->get('form.factory')->createBuilder('form', $defaultFormModel);

        $this->buildForm($this->builder);

        if ($request->getMethod() == 'POST' && !$isClearAll) {
            $form = $this->builder->getForm();
            $form->bindRequest($request);
            $this->form = $form;
            if ($form->isValid()) {
                $this->onSuccess();
            }
            else {
                $this->onFailure();
            }
        }
        else {
            $this->form = $this->builder->getForm();
        }
    }
}
