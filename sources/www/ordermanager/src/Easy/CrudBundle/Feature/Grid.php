<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Grid extends AbstractFeature {
    public function getDefaultOptions() {
        return array(
            'id'    => 'Grid',
            'title' => 'Grid',
            'filter.submit_label' => 'Apply filter',
            'filter.visiblity'    => 'hide'
        );
    }
    public function getActions() { return array('list', 'ajaxSource'); }

    public function handleList($controller, &$data, $options = array()) {
        $reader = $options['reader'];
        if (is_string($reader)) {
            $reader = $controller->get($reader);
        }

        $data['grid'] = array(
            'id' => $options['id'],
            'title' => $options['title'],
            'header' => $reader->getTableHeader(),
            'ajaxSource' => $this->getModule()->getUrl($this->getName(), 'ajaxSource', $controller->get('request')->query->all())
        );

        if (isset($options['filter'])) {
            $filter = $options['filter'];
            if (is_string($filter)) {
                $filter = $controller->get($filter);
            }

            $filter->execute();
            $data['grid']['filter'] = array(
                'id'       => $options['id'].'_Filter',
                'view'     => $filter->getForm()->createView(),
                'submitLabel' => $options['filter.submit_label'],
                'messages' => $filter->getMessages(),
                'errors'   => $filter->getErrors(),
                'url'      => $this->getModule()->getUrl($this->getName(), 'list', $controller->get('request')->query->all()),
                'visiblity' => $options['filter.visiblity']
            );

            if ($controller->getRequest()->getMethod() == 'POST') {
                $data['grid']['filter']['visiblity'] = '';
            }
        }
    }

    public function handleAjaxSource($controller, &$data, $options = array()) {
        $reader = $options['reader'];
        if (is_string($reader)) {
            $reader = $controller->get($reader);
        }

        if (isset($options['filter'])) {
            $filter = $options['filter'];
            if (is_string($filter)) {
                $filter = $controller->get($filter);
            }

            $reader->filters = $filter->getCurrentFilter();
        }

        $resp = $controller->get('easy_crud.ajax_source_handler')->handle($reader);

        return new Response(json_encode($resp));
    }
}