<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class View extends AbstractFeature {
    public function getActions() { return array('view'); }
    public function getDefaultOptions() {
        return array(
            'title'     => '',
            'handler'   => ''
        );
    }

    public function handleView($controller, &$data, $options = array()) {
        $handler = $options['handler'];
        if (is_string($handler)) {
            $handler = $controller->get($handler);
        }
        $data['title'] = $options['title'];
        $data['data'] = $handler->read();

        $referer = $controller->getRequest()->headers->get('referer', 'a');
        if (strpos($referer, '/view/view') !== FALSE) {
            $data['backLink'] = $referer;
        }
    }
}
