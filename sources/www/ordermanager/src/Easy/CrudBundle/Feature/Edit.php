<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


class Edit extends AbstractFeature {
    public function getActions() { return array('edit'); }
    public function getDefaultOptions() {
        return array(
            'id'    => 'EditForm',
            'title' => '',
            'submit_label' => 'Save'
        );
    }

    public function handleEdit($controller, &$data, $options = array()) {
        $form = $options['handler'];
        if (is_string($form)) {
            $form = $controller->get($form);
        }

        $request = $controller->get('request');
        $params = array('id' => $request->get('id', 0));
        $idParent = $request->get('idParent', 0);
        if ($idParent != 0) {
            $params['idParent'] = $idParent;
        }

        $form->execute();
        $data['form'] = array(
            'id'       => $options['id'],
            'title'    => $options['title'],
            'view'     => $form->getForm()->createView(),
            'submitLabel' => $options['submit_label'],
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'edit', $params),
            'editId'   => $controller->get('request')->get('id', 0)
        );

        if ($request->getMethod() == 'POST' && method_exists($form, 'postProcessUpdateView')) {
            $form->postProcessUpdateView($data, $options);
        }

        if (isset($options['useFormRedirect'])) {
            if ($form->getRedirectUrl() != '') {
                return $controller->redirect($form->getRedirectUrl());
            }
        }
    }

    public function handleEditDesignReport($controller, &$data, $options = array())
    {
        $form = $options['handler'];
        if (is_string($form)) {
            $form = $controller->get($form);
        }

        $request = $controller->get('request');
        $em = $controller->get('doctrine')->getEntityManager();
        $id = $request->get('id');


        // Sections
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.idReport = :idReport')
            ->setParameter('idReport', $id)
            ->orderBy('p.sortOrder', 'ASC');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $name = $r->getName();
            if ($r->getIdType() == 2) {
                $reportSection = $em->getRepository('AppDatabaseMainBundle:ReportSection')->findOneById($r->getIdReportSectionLink());
                if (!empty($reportSection)) {
                    $name = $reportSection->getName();
                }
            }

            $sections[] = array(
                'id'   => $r->getId(),
                'name' => $name,
                'alias'  => $r->getAlias()
            );
        }

        $sections = array_chunk($sections, ceil(count($sections)/4));
        $data['sections'] = $sections;
        $data['idReport'] = $id;

        $url = 'LeepAdminBundle:Report:drap_drop_page.html.twig';
        $response = $controller->renderView($url, $data);
        return new Response($response);
    }

    public function handleSetOrderReportSection($controller, &$data, $options=array())
    {
        $request = $controller->get('request');
        $mgr = $controller->get('easy_module.manager');
        $newOrderedIds = json_decode($request->get('idList', ''));
        $idReport = $request->get('id', 0);

        // Update order
        $em = $controller->get('doctrine')->getManager();
        $query = $em->createQueryBuilder();
        $query->select('p')
            ->from('AppDatabaseMainBundle:ReportSection', 'p')
            ->andWhere('p.id in (:idList)')
            ->setParameter('idList', $newOrderedIds);
        $results = $query->getQuery()->getResult();

        $newOrder = array_flip($newOrderedIds);
        foreach ($results as $r) {
            if (isset($newOrder[$r->getId()])) {
                $r->setSortOrder($newOrder[$r->getId()]);
            }
        }
        $em->flush();
        // $editFuntionLink = $mgr->getUrl('') ;
        return new RedirectResponse('/report/edit_section/editDesignreport?id='.$idReport);

    }
}
