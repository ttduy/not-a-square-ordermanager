<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Create extends AbstractFeature {
    public function getActions() { return array('create'); }
    public function getDefaultOptions() {
        return array(
            'id'    => 'CreateForm',
            'title' => '',
            'submit_label' => 'Create'
        );
    }

    public function handleCreate($controller, &$data, $options = array()) {
        $form = $options['handler'];
        if (is_string($form)) {
            $form = $controller->get($form);
        }
        $form->execute();

        if (isset($options['redirectNow'])) {
            $url = $form->getRedirectUrl();
            if ($url) {
                return $controller->redirect($url);
            }
        }

        $data['form'] = array(
            'id'       => $options['id'],
            'title'    => $options['title'],
            'view'     => $form->getForm()->createView(),
            'submitLabel' => $options['submit_label'],
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->getModule()->getUrl($this->getName(), 'create', $controller->get('request')->query->all())
        );

        if (isset($options['toEdit'])) {
            if ($controller->get('request')->getMethod() == 'POST' && (count($form->getErrors()) == 0)) {
                $url = $this->getModule()->getUrl($options['toEditFeature'], 'edit', array('id' => $form->newId));
                return $controller->redirect($url);
            }
        }

        if (isset($options['toRedirect'])) {
            if ($controller->get('request')->getMethod() == 'POST' && (count($form->getErrors()) == 0)) {
                $url = null;

                if ($form->getRedirectUrl()) {
                    $url = $form->getRedirectUrl();
                } else if (isset($options['toCreateFeature'])) {
                    $url = $this->getModule()->getUrl($options['toCreateFeature'], 'create');
                }

                if ($url) {
                    return $controller->redirect($url);
                }
            }
        }
    }
}
