<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Delete extends AbstractFeature {
    public function getActions() { return array('view'); }
    public function getDefaultOptions() {
        return array(
            'title'     => '',
            'handler'   => '',
            'redirectFeature' => 'grid'
        );
    }

    public function handleDelete($controller, &$data, $options = array()) {
        $handler = $options['handler'];
        if (is_string($handler)) {
            $handler = $controller->get($handler);
        }

        $v = $handler->execute();
        if (!empty($v) && ($v instanceof RedirectResponse)) {
            return $v;
        }

        $redirectParams = array();
        if ($handler->idRedirect) {
            $redirectParams['id'] = $handler->idRedirect;
        }
        
        return $controller->redirect($this->getModule()->getUrl($options['redirectFeature'], 'list', $redirectParams));
    }
}
