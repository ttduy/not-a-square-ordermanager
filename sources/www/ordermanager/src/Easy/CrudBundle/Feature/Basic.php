<?php
namespace Easy\CrudBundle\Feature;

use Easy\ModuleBundle\Module\AbstractFeature;
use Symfony\Component\HttpFoundation\Response;

class Basic extends AbstractFeature {
    public function getActions() { return array('create'); }
    public function getDefaultOptions() {
        return array(
            'handler' => ''
        );
    }

    public function handleHandle($controller, &$data, $options = array()) {
        $handler = $options['handler'];
        if (is_string($handler)) {
            $handler = $controller->get($handler);
        }

        return $handler->process($controller, $data, $options);
    }
}
