<?php
namespace Easy\CrudBundle\Grid;

abstract class DoctrineGridDataReader extends BaseGridDataReader {
    protected $container;
    protected $columnMapping = array();
    protected $columnSortMapping = array();
    protected $result = NULL;
    protected $filter = NULL;
    protected $idArray = array();
    protected $iterate = false;

    public function getEntityManager() {
        return '';
    }

    public function setFilterData($filter) {
        $this->filter = $filter;
    }

    public function setIterateLargeData($iterate) {
        $this->iterate = $iterate;
    }

    public function __construct($container) {
        $this->container = $container;
        $this->columnMapping = $this->getColumnMapping();
        $this->columnSortMapping = $this->getColumnSortMapping();
    }

    public function getDefaultSort() {
        return array('columnNo' => 0, 'dir' => 'asc');
    }

    abstract function getColumnMapping();
    public function getColumnSortMapping() {
        $colMap = $this->getColumnMapping();
        foreach ($colMap as $map) {
            $this->columnSortMapping[] = 'p.'.$map;
        }
    }
    public function getFormatter() { return null; }
    abstract public function buildQuery($queryBuilder);

    public function preGetResult() {}
    public function postQueryBuilder($queryBuilder) {}
    public function setDefaultSort(&$sortColumnNo, &$sortDir) {
        $sortColumnNo = 1;
        $sortDir = 'asc';
    }

    public function loadData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $queryBuilder = $this->container->get('doctrine')->getEntityManager($this->getEntityManager())->createQueryBuilder();
        $this->buildQuery($queryBuilder);
        $columnId = isset($this->columnSortMapping[$sortColumnNo]) ? $this->columnSortMapping[$sortColumnNo] : 'p.id';

        $queryBuilder->orderBy($columnId, $sortDir);
        if ($displayLength != 0 && !$this->iterate) {
            $queryBuilder->setFirstResult($displayStart);
            $queryBuilder->setMaxResults($displayLength);
        }
        $this->postQueryBuilder($queryBuilder);

        if (!$this->iterate) {
            $this->result = $queryBuilder->getQuery()->getResult();
        } else {
            $this->result = $this->iterateLargeQueryResult($queryBuilder->getQuery()->iterate(), $displayStart, $displayLength);
        }
    }

    protected function iterateLargeQueryResult($result, $start, $length) {
        return [];
    }

    public function getCountSQL() {
        return 'COUNT(p)';
    }

    public function getTotalRecordsMatched() {
        // FIX ME
        $queryBuilder = $this->container->get('doctrine')->getEntityManager($this->getEntityManager())->createQueryBuilder();
        $this->buildQuery($queryBuilder);
        $queryBuilder = $queryBuilder->select($this->getCountSQL());
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function preLoading() {

    }

    public function getResult() {
        $this->preGetResult();
        $data = array();
        $formatter = $this->getFormatter();

        $this->idArray = array();
        foreach ($this->result as $row) {
            $rowId = is_array($row) ? $row['id'] : $row->getId();
            $this->idArray[] = $rowId;
        }
        $this->preLoading();

        foreach ($this->result as $row) {
            $rowData = array();
            foreach ($this->columnMapping as $columnId) {
                if ($formatter != null && $formatter->contains($columnId)) {
                    $rowData[] = $formatter->format($columnId, $row);
                }
                else {
                    $readMethod = 'buildCell'.ucfirst($columnId);
                    $getMethod = 'get'.ucfirst($columnId);

                    if (method_exists($this, $readMethod)) {
                        $rowData[] = $this->$readMethod($row);
                    }
                    else {
                        if (is_array($row) && isset($row[$columnId])) {
                            $rowData[] = $row[$columnId]    ;
                        }
                        else if (method_exists($row, $getMethod)) {
                            $rowData[] = $row->$getMethod();
                        }
                        else {
                            $rowData[] = '';
                        }
                    }
                }
            }
            $rowId = is_array($row) ? $row['id'] : $row->getId();
            $data[$rowId] = $rowData;
            //$data[] = $rowData;
        }
        return $data;
    }
}
