<?php
namespace Easy\ToolBundle\Module;

use Easy\ModuleBundle\Module\AbstractModule;

class Bundle extends AbstractModule {    
    public function load() {        
        // Grid feature
        $this->setFeature('grid', 'easy_crud.feature.grid', array(
            'grid_id'      => 'BundleGrid',
            'grid_title'   => 'Bundle',
            'grid_reader'  => 'easy_tool.bundle.business.grid_reader'
        ));
        $this->setHookBefore('grid', 'easy_tool.hook.header', array(
            'menuSelected' => 'Bundle',
            'title'        => 'Bundle'
        ));
        $this->setTemplate('grid', 'EasyToolBundle:Bundle:list.html.twig');        

        // Create features
        $this->setFeature('create', 'easy_crud.feature.create', array(
            'id'           => 'BundleCreate',
            'handler'      => 'easy_tool.bundle.business.create_handler'
        ));
        $this->setHookBefore('create', 'easy_tool.hook.header', array(
            'menuSelected' => 'Bundle',
            'title'        => 'Bundle'
        ));
        $this->setTemplate('create', 'EasyToolBundle:Bundle:create.html.twig');
    }
}

