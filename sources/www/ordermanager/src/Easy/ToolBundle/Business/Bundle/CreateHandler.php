<?php
namespace Easy\ToolBundle\Business\Bundle;

use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppCourierTrackerBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        //$mapping = $this->controller->get('easy_mapping');
        // SHIPPER
        $builder->add('namespace', 'text', array(
            'label'    => 'Namespace',
            'required' => true
        ));        
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        
    }
}