<?php
namespace Easy\ToolBundle\Business\Bundle;

use Easy\CrudBundle\Grid\StaticGridDataReader;

class Grid extends StaticGridDataReader {
    public $container;

    public function __construct($container) {
        $this->container = $container;
    }
    public function setData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $bundles = $this->container->getParameter('kernel.bundles');
        $this->data = array();
        foreach ($bundles as $key => $path) {
            if ($this->isApplicationBundle($key, $path)) {
                $this->data[] = array($key, $path);
            }
        }
    }

    public function isApplicationBundle($key, $path) {
        $srcDir = $this->container->getParameter('kernel.root_dir').'/../src';
        $srcFile = $srcDir.'/'.str_replace('\\', '/', $path).'.php';

        return file_exists($srcFile);
    }

    public function getTableHeader() {
        return array(
            array('title' => 'Name', 'width' => '30%', 'sortable' => 'true'),
            array('title' => 'Path', 'width' => '70%', 'sortable' => 'false'),
        );
    }
}