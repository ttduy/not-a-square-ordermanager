<?php

namespace Easy\ToolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleController extends Controller
{   
    public function handleAction($module = null, $action = null) {
        $moduleManager = $this->container->get('easy_module.manager');
        $module = $moduleManager->getModule('easy_tool', $module);
        return $module->handle($this, $action);
    }
}
