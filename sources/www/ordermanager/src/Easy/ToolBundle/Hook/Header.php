<?php
namespace Easy\ToolBundle\Hook;

use Easy\ModuleBundle\Module\AbstractHook;

class Header extends AbstractHook {
    public function handle($controller, &$data, $options = array()) {
        $manager = $controller->get('easy_module.manager');
        
        $data['pageTitle'] = 'Easy Tool';
        $data['menu'] = array(
            'content' => array(
                'Dashboard' => array(
                    'link' => '#',
                    'class' => 'dashboard',
                    'title' => 'Dashboard'
                ),
                'Bundle' => array(
                    'link' => '#',
                    'class' => 'dashboard',
                    'title' => 'Bundles',
                    'childs' => array(
                        array(
                            'link' => $manager->getModule('easy_tool', 'bundle')->getUrl('list'),
                            'title' => 'List all bundles'
                        ),
                        array(
                            'link' => '#',
                            'title' => 'Add new bundle'
                        )
                    )
                )
            ),
            'selected' => $options['menuSelected']
        );
        $data['mainTabs'] = array(
            'content' => array(
                array('title' => $options['title'], 'link' => '#')
            ),
            'selected' => '#'
        );
    }
}