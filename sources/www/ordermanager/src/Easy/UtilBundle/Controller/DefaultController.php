<?php

namespace Easy\UtilBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EasyUtilBundle:Default:index.html.twig', array('name' => $name));
    }
}
