<?php
namespace Easy\UtilBundle\Utils;

class Module {
	public $_container;
	public $_features;

	public function __construct($container) {
		$this->_container = $container;
		$this->_features = array();
		$this->_sharedData = array();
	}

	public function set($dataId, $data) {
		$this->_sharedData[$dataId] = $data;
	}

	public function setFeature($featureId, $feature) {
		$this->_features[$featureId] = $feature;
	}

	public function build() {
		
	}

	public function handle($action, $arguments) {
		echo $action;
	}
}