<?php
namespace Easy\UtilBundle\Utils;

class Registry {
	public $_container;
	public $_registry = array();

	public function __construct($container) {
		$this->container = $container;
	}

	public function set($id, $value) {
		$this->_registry[$id] = $value;
	}
	public function setByService($id, $serviceId) {
		$this->_registry[$id] = $this->_container->get($serviceId);
	}
	public function has($id) {
		return isset($id);
	}
	public function get($id) {
		return $this->_registry[$id];
	}
	
}