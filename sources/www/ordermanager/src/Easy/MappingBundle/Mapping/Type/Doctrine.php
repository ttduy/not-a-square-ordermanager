<?php
namespace Easy\MappingBundle\Mapping\Type;

use Easy\MappingBundle\Mapping\MappingTypeInterface;

class Doctrine implements MappingTypeInterface {
    protected $doctrine;
    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }
    public function getMapping($config) {
        $mapping = array();
        if (isset($config['em_namespace'])) {
            $em = $this->doctrine->getEntityManager($config['em_namespace']);
        }
        else {
            $em = $this->doctrine->getEntityManager();
        }
        $query = $em->createQueryBuilder();
        $query
            ->select('p')
            ->from($config['repository'], 'p');
        if (isset($config['orderBy']) && isset($config['orderDir'])) {
            $query->orderBy('p.'.$config['orderBy'], $config['orderDir']);
        }
        $result = $query->getQuery()->getResult();

        foreach ($result as $row) {            
            $getIdMethod = 'get'.ucfirst($config['id']);
            $id = $row->$getIdMethod();

            $title = '';
            if (isset($config['functionTitle'])) {
                $title = call_user_func($config['functionTitle'], $row);
            }
            else if (isset($config['title'])) {
                $getTitleMethod = 'get'.ucfirst($config['title']);
                $title = $row->$getTitleMethod();
            }            
            $mapping[$id] = $title;
        }
        return $mapping;
    }   

    public function getMappingFiltered($config) {
        $mapping = array();
        if (!isset($config['filters'])) {
            $config['filters'] = array();
        }
        if (isset($config['em_namespace'])) {
            $em = $this->doctrine->getEntityManager($config['em_namespace']);
        }
        else {
            $em = $this->doctrine->getEntityManager();
        }

        $query = $em->createQueryBuilder();
        $query
            ->select('p')
            ->from($config['repository'], 'p');
        foreach ($config['filters'] as $k => $v) {
            $query->andWhere('p.'.$k." = :".$k)
                ->setParameter($k, $v);
        }
        if (isset($config['orderBy']) && isset($config['orderDir'])) {
            $query->orderBy('p.'.$config['orderBy'], $config['orderDir']);
        }
        $result = $query->getQuery()->getResult();

        foreach ($result as $row) {            
            $getIdMethod = 'get'.ucfirst($config['id']);
            $id = $row->$getIdMethod();

            $title = '';
            if (isset($config['functionTitle'])) {
                $title = call_user_func($config['functionTitle'], $row);
            }
            else if (isset($config['title'])) {
                $getTitleMethod = 'get'.ucfirst($config['title']);
                $title = $row->$getTitleMethod();
            }            
            $mapping[$id] = $title;
        }
        return $mapping;
    }   

}
