<?php

namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class DatePicker extends AbstractType {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'calendar', 'style' => 'width: 90px'),
            'default_date' => ''
        );
    }
    
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['pattern'] = 'dd/mm/yy';

        $data = $form->getData();
        if (empty($data) && ($options['default_date'] != '')) {
            $view->vars['value'] = $options['default_date'];
        }
    }

    public function getParent()
    {
        return 'date';
    }

    public function getName()
    {
        return 'datepicker';
    }
}