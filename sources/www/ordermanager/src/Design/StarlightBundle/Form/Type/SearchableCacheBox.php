<?php

namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SearchableCacheBox extends AbstractType {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'width'  => '50%',
            'url'    => '',
            'dataKey' => ''
        );
    }
    
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['width'] = $options['width'];
        $dataKey = $options['dataKey'];
        if ($dataKey == '') {
            $dataKey = md5(uniqid());
        }
        $view->vars['dataKey'] = $dataKey;
        $view->vars['url'] = $options['url'];
    }

    public function getParent()
    {
        return 'hidden';
    }

    public function getName()
    {
        return 'searchable_cache_box';
    }
}