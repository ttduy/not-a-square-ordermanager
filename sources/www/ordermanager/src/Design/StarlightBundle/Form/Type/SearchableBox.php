<?php

namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SearchableBox extends AbstractType {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'width'  => '50%'
        );
    }
    
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['width'] = $options['width'];
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'searchable_box';
    }
}