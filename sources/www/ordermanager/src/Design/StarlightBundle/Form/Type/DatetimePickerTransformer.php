<?php
namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class DatetimePickerTransformer implements DataTransformerInterface
{
    public function transform($model)
    {
        $array = array('date' => null, 'hour' => 0, 'minute' => 0, 'second' => 0);
        if (!empty($model)) {
            $array['date'] = $model;
            $array['hour'] = $model->format('H');
            $array['minute'] = $model->format('i');
            $array['second'] = $model->format('s');
        }
        return $array;
    }

    public function reverseTransform($model)
    {
        $date = null;
        if (!empty($model)) {
            $date = $model['date'];
            if (!empty($date)) {
                $date->setTime($model['hour'], $model['minute'], $model['second']);
            }
        }
        return $date;
    }
}
