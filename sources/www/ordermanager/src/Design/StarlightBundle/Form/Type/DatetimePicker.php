<?php

namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class DatetimePicker extends AbstractType {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }
    
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'datetimepicker';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $hours = array();
        for ($i = 0; $i <= 23; $i++) $hours[$i] = $i;
        $minutes = array();
        for ($i = 0; $i <= 59; $i++) $minutes[$i] = $i;

        $builder->add('date', 'datepicker');
        $builder->add('hour', 'choice', array(
            'attr' => array(
                'style' => 'max-width: 60px; min-width: 60px'
            ),
            'choices' => $hours,
            'empty_value' => false
        ));
        $builder->add('minute', 'choice', array(
            'attr' => array(
                'style' => 'max-width: 60px; min-width: 60px'
            ),
            'choices' => $minutes,
            'empty_value' => false
        ));
        $builder->add('second', 'choice', array(
            'attr' => array(
                'style' => 'max-width: 60px; min-width: 60px'
            ),
            'choices' => $minutes,
            'empty_value' => false
        ));

        $transformer = new DatetimePickerTransformer();

        $builder->appendClientTransformer($transformer);
    }
}