jQuery(document).ready(function(){
    jQuery('.popupButton').colorbox({
        iframe: true,
        width:  "75%",
        height: "75%"
    });
});
        

function formSubmit(formId) {
    var element = document.getElementById(formId);
    if (element) {
        element.submit();
    }
}
        
function dataTableActionWithSelected(tableId, url) {
    var selected = '';
    jQuery('#' + tableId + ' input[type=checkbox]').each(function(){
        if (jQuery(this).is(':checked')) {
            selected += jQuery(this).attr('id').substring(4) + ",";
        }                   
    });

    url += '?selected=' + selected;
    document.location = url;
}        

function showAjaxUpdate(id) {
    jQuery('#' + id + '_textbox').css('display', 'inline-block');
    jQuery('#' + id + '_field').css('display', 'none');
    jQuery('#' + id + '_link').css('display', 'none');
}

function ajaxUpdate(id, targetUrl) {
    jQuery('#' + id + '_textbox').css('display', 'none');
    jQuery('#' + id + '_field').css('display', 'inline-block');
    jQuery('#' + id + '_link').css('display', 'inline-block');

    var val = jQuery('#' + id).val();    
    
    var jqxhr = jQuery.ajax({
        url:  targetUrl,
        type: 'GET',
        data: { fieldVal: val },
    }).done(function( msg ) {
        jQuery('#' + id + '_field').html(val);
    }).fail(function(jqXHR, textStatus) {
        alert("Failed: " + textStatus );
    });
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

